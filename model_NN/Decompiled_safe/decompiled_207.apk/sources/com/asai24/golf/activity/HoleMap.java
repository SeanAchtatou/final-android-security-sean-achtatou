package com.asai24.golf.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.PointF;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.Toast;
import android.widget.ZoomControls;
import com.asai24.golf.GolfApplication;
import com.asai24.golf.R;
import com.asai24.golf.a.b;
import com.asai24.golf.a.g;
import com.asai24.golf.a.s;
import com.asai24.golf.a.w;
import com.asai24.golf.d.e;
import com.asai24.golf.view.RotateView;
import com.asai24.golf.view.a;
import com.asai24.golf.view.c;
import com.asai24.golf.view.d;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.mobfox.sdk.MobFoxView;
import java.util.ArrayList;

public class HoleMap extends MapActivity implements SensorEventListener, View.OnClickListener {
    private boolean A;
    /* access modifiers changed from: private */
    public boolean B;
    /* access modifiers changed from: private */
    public boolean C;
    /* access modifiers changed from: private */
    public float D;
    private MobFoxView E;
    private b a;
    private GolfApplication b;
    private Handler c;
    /* access modifiers changed from: private */
    public Resources d;
    /* access modifiers changed from: private */
    public LocationManager e;
    /* access modifiers changed from: private */
    public LocationListener f;
    private com.asai24.golf.view.b g;
    /* access modifiers changed from: private */
    public a h;
    private ArrayList i;
    private Toast j;
    private SensorManager k;
    private Sensor l;
    private RotateView m;
    /* access modifiers changed from: private */
    public MapView n;
    private Button o;
    private Button p;
    private Button q;
    private Button r;
    /* access modifiers changed from: private */
    public ProgressDialog s;
    /* access modifiers changed from: private */
    public boolean t = false;
    private long u;
    private long v;
    private long w;
    /* access modifiers changed from: private */
    public int x = 0;
    /* access modifiers changed from: private */
    public boolean y = true;
    /* access modifiers changed from: private */
    public GeoPoint z;

    private void a() {
        this.k = (SensorManager) getSystemService("sensor");
        if (!this.k.getSensorList(3).isEmpty()) {
            this.l = this.k.getSensorList(3).get(0);
        }
    }

    /* access modifiers changed from: private */
    public void a(GeoPoint geoPoint) {
        try {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            double pow = 3932160.0d / Math.pow(2.0d, (double) (this.n.getZoomLevel() - 1));
            if (this.A) {
                for (double d2 = pow; d2 <= 360.0d; d2 += pow) {
                    Point pixels = this.n.getProjection().toPixels(e.a(geoPoint, com.asai24.golf.f.a.b(d2)), (Point) null);
                    arrayList.add(new PointF((float) pixels.x, (float) pixels.y));
                    arrayList2.add(Double.valueOf(d2));
                }
            } else {
                for (double d3 = pow; d3 <= 360.0d; d3 += pow) {
                    Point pixels2 = this.n.getProjection().toPixels(e.a(geoPoint, d3), (Point) null);
                    arrayList.add(new PointF((float) pixels2.x, (float) pixels2.y));
                    arrayList2.add(Double.valueOf(d3));
                }
            }
            Point pixels3 = this.n.getProjection().toPixels(geoPoint, (Point) null);
            this.h.a(new PointF((float) pixels3.x, (float) pixels3.y), arrayList, arrayList2);
        } catch (Exception e2) {
            Log.e("HoleMap", "failed setDistanceCircles.", e2);
        }
    }

    /* JADX WARN: Type inference failed for: r7v0, types: [android.content.Context, com.asai24.golf.activity.HoleMap] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void a(com.google.android.maps.GeoPoint r8, com.google.android.maps.GeoPoint r9, java.lang.String r10, java.lang.String r11, java.lang.String r12, int r13) {
        /*
            r7 = this;
            r4 = 2131230737(0x7f080011, float:1.8077535E38)
            com.asai24.golf.e.a r0 = new com.asai24.golf.e.a
            r0.<init>(r7)
            if (r9 != 0) goto L_0x0050
            android.content.res.Resources r1 = r7.d
            java.lang.String r1 = r1.getString(r4)
            r3 = r1
        L_0x0011:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = java.lang.String.valueOf(r11)
            r1.<init>(r2)
            java.lang.String r2 = "⇒"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r12)
            java.lang.String r5 = r1.toString()
            if (r10 == 0) goto L_0x0032
            java.lang.String r1 = ""
            boolean r1 = r10.equals(r1)
            if (r1 == 0) goto L_0x008a
        L_0x0032:
            android.content.res.Resources r0 = r7.d
            java.lang.String r0 = r0.getString(r4)
            r4 = r0
        L_0x0039:
            com.asai24.golf.view.c r0 = new com.asai24.golf.view.c
            r1 = r7
            r2 = r8
            r6 = r13
            r0.<init>(r1, r2, r3, r4, r5, r6)
            com.google.android.maps.MapView r1 = r7.n
            java.util.List r1 = r1.getOverlays()
            r1.add(r0)
            java.util.ArrayList r1 = r7.i
            r1.add(r0)
            return
        L_0x0050:
            double r1 = com.asai24.golf.d.e.a(r8, r9)
            boolean r3 = r7.A
            if (r3 == 0) goto L_0x0071
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            int r1 = com.asai24.golf.f.a.a(r1)
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r3.<init>(r1)
            java.lang.String r1 = "yds"
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            r3 = r1
            goto L_0x0011
        L_0x0071:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            long r1 = com.asai24.golf.d.m.a(r1)
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r3.<init>(r1)
            java.lang.String r1 = "m"
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            r3 = r1
            goto L_0x0011
        L_0x008a:
            r1 = 2131230904(0x7f0800b8, float:1.8077874E38)
            java.lang.String r1 = r7.getString(r1)
            boolean r1 = r10.equals(r1)
            if (r1 == 0) goto L_0x00a4
            r1 = 2131230903(0x7f0800b7, float:1.8077872E38)
            java.lang.String r1 = r7.getString(r1)
            java.lang.String r0 = r0.a(r1)
            r4 = r0
            goto L_0x0039
        L_0x00a4:
            java.lang.String r0 = r0.a(r10)
            r4 = r0
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.asai24.golf.activity.HoleMap.a(com.google.android.maps.GeoPoint, com.google.android.maps.GeoPoint, java.lang.String, java.lang.String, java.lang.String, int):void");
    }

    private void a(Float f2) {
        this.D = f2.floatValue();
        this.m.a(f2.floatValue());
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.i.size()) {
                this.h.a(f2.floatValue());
                this.n.invalidate();
                return;
            }
            ((c) this.i.get(i3)).a(f2.floatValue());
            i2 = i3 + 1;
        }
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [android.content.Context, com.asai24.golf.activity.HoleMap] */
    /* access modifiers changed from: private */
    public void a(String str) {
        if (this.j == null) {
            this.j = Toast.makeText((Context) this, (CharSequence) null, 0);
        }
        this.j.cancel();
        this.j.setText(str);
        this.j.setDuration(1);
        this.j.show();
    }

    private void b() {
        if (this.l != null && this.C) {
            this.k.registerListener(this, this.l, 2);
        }
    }

    private void c() {
        if (this.l != null) {
            this.k.unregisterListener(this);
        }
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [android.content.Context, com.asai24.golf.activity.HoleMap] */
    private void d() {
        this.c = new Handler();
        this.n.getController().setZoom(18);
        this.n.setBuiltInZoomControls(true);
        ZoomControls zoomControls = (ZoomControls) this.n.getZoomButtonsController().getZoomControls();
        zoomControls.setGravity(80);
        zoomControls.setPadding(0, 0, 0, getWindowManager().getDefaultDisplay().getHeight() / 5);
        this.n.setClickable(true);
        this.n.setEnabled(true);
        this.s = new ProgressDialog(this);
        this.s.setIndeterminate(true);
        this.s.setMessage(this.d.getString(R.string.msg_now_loading));
        if (!this.n.getOverlays().isEmpty()) {
            this.n.getOverlays().clear();
        }
        this.g.enableMyLocation();
        this.n.getOverlays().add(this.g);
        this.n.getOverlays().add(this.h);
    }

    private void e() {
        GeoPoint geoPoint;
        s e2 = this.a.e(this.w);
        w a2 = this.a.a(this.v, this.w, this.u);
        g m2 = this.a.m(a2.b());
        if (!e2.q()) {
            int i2 = 0;
            while (true) {
                if (i2 < m2.getCount()) {
                    m2.moveToPosition(i2);
                    double g2 = m2.g();
                    double h2 = m2.h();
                    if (g2 != 0.0d && h2 != 0.0d) {
                        geoPoint = new GeoPoint((int) (g2 * 1000000.0d), (int) (h2 * 1000000.0d));
                        this.t = true;
                        break;
                    }
                    i2++;
                } else {
                    geoPoint = null;
                    break;
                }
            }
        } else {
            geoPoint = new GeoPoint((int) (e2.m() * 1000000.0d), (int) (e2.n() * 1000000.0d));
            this.t = true;
        }
        if (geoPoint != null) {
            this.n.getController().setCenter(geoPoint);
            this.z = geoPoint;
        }
        m2.close();
        a2.close();
        e2.close();
    }

    private void f() {
        int i2;
        try {
            ArrayList arrayList = new ArrayList();
            w a2 = this.a.a(this.v, this.w, this.u);
            long b2 = a2.b();
            a2.close();
            g m2 = this.a.m(b2);
            if (m2.getCount() == 0) {
                m2.close();
                return;
            }
            int i3 = 0;
            while (true) {
                i2 = i3 + 1;
                if (!(m2.g() == 0.0d && m2.h() == 0.0d)) {
                    ArrayList arrayList2 = new ArrayList();
                    GeoPoint geoPoint = new GeoPoint((int) (m2.g() * 1000000.0d), (int) (m2.h() * 1000000.0d));
                    arrayList2.add(Integer.valueOf(m2.d()));
                    arrayList2.add(geoPoint);
                    arrayList2.add(m2.e());
                    arrayList.add(arrayList2);
                }
                if (!m2.moveToNext()) {
                    break;
                }
                i3 = i2;
            }
            m2.close();
            for (int i4 = 0; i4 < arrayList.size(); i4++) {
                if (i4 != arrayList.size() - 1) {
                    this.n.getOverlays().add(new d((GeoPoint) ((ArrayList) arrayList.get(i4)).get(1), (GeoPoint) ((ArrayList) arrayList.get(i4 + 1)).get(1)));
                    a((GeoPoint) ((ArrayList) arrayList.get(i4)).get(1), (GeoPoint) ((ArrayList) arrayList.get(i4 + 1)).get(1), (String) ((ArrayList) arrayList.get(i4)).get(2), ((ArrayList) arrayList.get(i4)).get(0).toString(), ((ArrayList) arrayList.get(i4 + 1)).get(0).toString(), i4);
                } else {
                    a((GeoPoint) ((ArrayList) arrayList.get(i4)).get(1), null, (String) ((ArrayList) arrayList.get(i4)).get(2), ((ArrayList) arrayList.get(i4)).get(0).toString(), i2 == Integer.valueOf(new StringBuilder().append(((ArrayList) arrayList.get(i4)).get(0)).toString()).intValue() ? "Fin" : "？", i4);
                }
            }
        } catch (Exception e2) {
            Log.e("HoleMap", "failed dropShotMarkers.", e2);
        }
    }

    private void g() {
        if (this.p.getVisibility() == 0) {
            this.o.setText(getString(R.string.display_settings));
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, ((float) (-this.p.getHeight())) * 1.5f);
            translateAnimation.setDuration(1000);
            this.p.startAnimation(translateAnimation);
            TranslateAnimation translateAnimation2 = new TranslateAnimation(0.0f, 0.0f, 0.0f, ((float) (-this.q.getHeight())) * 3.0f);
            translateAnimation2.setDuration(1000);
            this.q.startAnimation(translateAnimation2);
            TranslateAnimation translateAnimation3 = new TranslateAnimation(0.0f, 0.0f, 0.0f, ((float) (-this.r.getHeight())) * 4.5f);
            translateAnimation3.setDuration(1000);
            this.r.startAnimation(translateAnimation3);
            this.p.setVisibility(4);
            this.q.setVisibility(4);
            this.r.setVisibility(4);
            return;
        }
        this.o.setText(getString(R.string.close));
        TranslateAnimation translateAnimation4 = new TranslateAnimation(0.0f, 0.0f, ((float) (-this.p.getHeight())) * 1.5f, 0.0f);
        translateAnimation4.setDuration(1000);
        this.p.startAnimation(translateAnimation4);
        TranslateAnimation translateAnimation5 = new TranslateAnimation(0.0f, 0.0f, ((float) (-this.q.getHeight())) * 2.5f, 0.0f);
        translateAnimation5.setDuration(1000);
        this.q.startAnimation(translateAnimation5);
        TranslateAnimation translateAnimation6 = new TranslateAnimation(0.0f, 0.0f, ((float) (-this.r.getHeight())) * 3.5f, 0.0f);
        translateAnimation6.setDuration(1000);
        this.r.startAnimation(translateAnimation6);
        this.p.setVisibility(0);
        this.q.setVisibility(0);
        this.r.setVisibility(0);
    }

    private void h() {
        if (this.n.isSatellite()) {
            this.p.setText(getString(R.string.map_view_satellite));
        } else {
            this.p.setText(getString(R.string.map_view_map));
        }
    }

    private void i() {
        if (this.B) {
            this.q.setBackgroundResource(R.drawable.square_toggle_on_selector);
        } else {
            this.q.setBackgroundResource(R.drawable.square_toggle_off_selector);
        }
    }

    private void j() {
        if (this.C) {
            this.r.setBackgroundResource(R.drawable.square_toggle_on_selector);
        } else {
            this.r.setBackgroundResource(R.drawable.square_toggle_off_selector);
        }
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    public void onAccuracyChanged(Sensor sensor, int i2) {
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [android.content.Context, com.asai24.golf.activity.HoleMap] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void onClick(android.view.View r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = 0
            android.content.SharedPreferences r0 = android.preference.PreferenceManager.getDefaultSharedPreferences(r5)
            android.content.SharedPreferences$Editor r0 = r0.edit()
            int r1 = r6.getId()
            switch(r1) {
                case 2131427446: goto L_0x0016;
                case 2131427447: goto L_0x003d;
                case 2131427448: goto L_0x006d;
                case 2131427449: goto L_0x0012;
                default: goto L_0x0011;
            }
        L_0x0011:
            return
        L_0x0012:
            r5.g()
            goto L_0x0011
        L_0x0016:
            com.google.android.maps.MapView r1 = r5.n
            com.google.android.maps.MapView r2 = r5.n
            boolean r2 = r2.isSatellite()
            if (r2 == 0) goto L_0x003b
            r2 = r3
        L_0x0021:
            r1.setSatellite(r2)
            r5.h()
            r1 = 2131231019(0x7f08012b, float:1.8078107E38)
            java.lang.String r1 = r5.getString(r1)
            com.google.android.maps.MapView r2 = r5.n
            boolean r2 = r2.isSatellite()
            r0.putBoolean(r1, r2)
            r0.commit()
            goto L_0x0011
        L_0x003b:
            r2 = r4
            goto L_0x0021
        L_0x003d:
            boolean r1 = r5.B
            if (r1 == 0) goto L_0x0065
            r1 = r3
        L_0x0042:
            r5.B = r1
            r5.i()
            boolean r1 = r5.B
            if (r1 == 0) goto L_0x0067
            com.asai24.golf.view.a r1 = r5.h
            r1.a(r4)
        L_0x0050:
            com.google.android.maps.MapView r1 = r5.n
            r1.invalidate()
            r1 = 2131231020(0x7f08012c, float:1.807811E38)
            java.lang.String r1 = r5.getString(r1)
            boolean r2 = r5.B
            r0.putBoolean(r1, r2)
            r0.commit()
            goto L_0x0011
        L_0x0065:
            r1 = r4
            goto L_0x0042
        L_0x0067:
            com.asai24.golf.view.a r1 = r5.h
            r1.a(r3)
            goto L_0x0050
        L_0x006d:
            boolean r1 = r5.C
            if (r1 == 0) goto L_0x008e
            r1 = r3
        L_0x0072:
            r5.C = r1
            r5.j()
            boolean r1 = r5.C
            if (r1 == 0) goto L_0x0090
            r5.b()
        L_0x007e:
            r1 = 2131231021(0x7f08012d, float:1.8078111E38)
            java.lang.String r1 = r5.getString(r1)
            boolean r2 = r5.C
            r0.putBoolean(r1, r2)
            r0.commit()
            goto L_0x0011
        L_0x008e:
            r1 = r4
            goto L_0x0072
        L_0x0090:
            r5.c()
            goto L_0x007e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.asai24.golf.activity.HoleMap.onClick(android.view.View):void");
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [android.content.Context, com.asai24.golf.activity.HoleMap, android.view.View$OnClickListener, com.google.android.maps.MapActivity] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onCreate(android.os.Bundle r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = 0
            com.asai24.golf.activity.HoleMap.super.onCreate(r6)
            android.content.Context r0 = r5.getApplicationContext()
            com.asai24.golf.d.c.a(r0)
            r0 = 2130903058(0x7f030012, float:1.7412923E38)
            r5.setContentView(r0)
            r0 = 2131427443(0x7f0b0073, float:1.8476502E38)
            android.view.View r0 = r5.findViewById(r0)
            com.asai24.golf.view.RotateView r0 = (com.asai24.golf.view.RotateView) r0
            r5.m = r0
            r0 = 2131427444(0x7f0b0074, float:1.8476504E38)
            android.view.View r0 = r5.findViewById(r0)
            com.google.android.maps.MapView r0 = (com.google.android.maps.MapView) r0
            r5.n = r0
            com.asai24.golf.a.b r0 = com.asai24.golf.a.b.a(r5)
            r5.a = r0
            android.app.Application r0 = r5.getApplication()
            com.asai24.golf.GolfApplication r0 = (com.asai24.golf.GolfApplication) r0
            r5.b = r0
            android.content.res.Resources r0 = r5.getResources()
            r5.d = r0
            android.content.Intent r0 = r5.getIntent()
            android.os.Bundle r0 = r0.getExtras()
            java.lang.String r1 = "playing_player_id"
            long r1 = r0.getLong(r1)
            r5.u = r1
            java.lang.String r1 = "playing_round_id"
            long r1 = r0.getLong(r1)
            r5.v = r1
            java.lang.String r1 = "playing_hole_id"
            long r0 = r0.getLong(r1)
            r5.w = r0
            r0 = 2131427449(0x7f0b0079, float:1.8476515E38)
            android.view.View r0 = r5.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r5.o = r0
            r0 = 2131427446(0x7f0b0076, float:1.8476508E38)
            android.view.View r0 = r5.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r5.p = r0
            r0 = 2131427447(0x7f0b0077, float:1.847651E38)
            android.view.View r0 = r5.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r5.q = r0
            r0 = 2131427448(0x7f0b0078, float:1.8476513E38)
            android.view.View r0 = r5.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r5.r = r0
            android.widget.Button r0 = r5.o
            r0.setOnClickListener(r5)
            android.widget.Button r0 = r5.p
            r0.setOnClickListener(r5)
            android.widget.Button r0 = r5.q
            r0.setOnClickListener(r5)
            android.widget.Button r0 = r5.r
            r0.setOnClickListener(r5)
            android.content.SharedPreferences r0 = android.preference.PreferenceManager.getDefaultSharedPreferences(r5)
            com.google.android.maps.MapView r1 = r5.n
            r2 = 2131231019(0x7f08012b, float:1.8078107E38)
            java.lang.String r2 = r5.getString(r2)
            boolean r2 = r0.getBoolean(r2, r3)
            r1.setSatellite(r2)
            r1 = 2131231020(0x7f08012c, float:1.807811E38)
            java.lang.String r1 = r5.getString(r1)
            boolean r1 = r0.getBoolean(r1, r4)
            r5.B = r1
            r1 = 2131231021(0x7f08012d, float:1.8078111E38)
            java.lang.String r1 = r5.getString(r1)
            boolean r1 = r0.getBoolean(r1, r3)
            r5.C = r1
            r1 = 2131231014(0x7f080126, float:1.8078097E38)
            java.lang.String r1 = r5.getString(r1)
            boolean r1 = r0.getBoolean(r1, r3)
            if (r1 != 0) goto L_0x00da
            r5.showDialog(r4)
        L_0x00da:
            r5.h()
            r5.i()
            r5.j()
            r1 = 2131230966(0x7f0800f6, float:1.8078E38)
            java.lang.String r1 = r5.getString(r1)
            java.lang.String r2 = "yard"
            java.lang.String r0 = r0.getString(r1, r2)
            java.lang.String r1 = "yard"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x014b
            r5.A = r4
        L_0x00fa:
            r5.a()
            r5.b()
            com.asai24.golf.activity.dh r0 = new com.asai24.golf.activity.dh
            r0.<init>(r5)
            r5.f = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r5.i = r0
            com.asai24.golf.view.a r0 = new com.asai24.golf.view.a
            r0.<init>()
            r5.h = r0
            com.asai24.golf.activity.dg r0 = new com.asai24.golf.activity.dg
            android.content.Context r1 = r5.getApplicationContext()
            com.google.android.maps.MapView r2 = r5.n
            r0.<init>(r5, r1, r2)
            r5.g = r0
            r0 = 2131427338(0x7f0b000a, float:1.847629E38)
            android.view.View r0 = r5.findViewById(r0)
            jp.co.nobot.libAdMaker.libAdMaker r0 = (jp.co.nobot.libAdMaker.libAdMaker) r0
            r1 = 2131230752(0x7f080020, float:1.8077566E38)
            java.lang.String r1 = r5.getString(r1)
            r0.a = r1
            r1 = 2131230753(0x7f080021, float:1.8077568E38)
            java.lang.String r1 = r5.getString(r1)
            r0.b = r1
            r1 = 2131230754(0x7f080022, float:1.807757E38)
            java.lang.String r1 = r5.getString(r1)
            r0.a(r1)
            r0.d()
            return
        L_0x014b:
            r5.A = r3
            goto L_0x00fa
        */
        throw new UnsupportedOperationException("Method not decompiled: com.asai24.golf.activity.HoleMap.onCreate(android.os.Bundle):void");
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [android.content.Context, com.asai24.golf.activity.HoleMap, com.google.android.maps.MapActivity] */
    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        switch (i2) {
            case 1:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle(this.d.getString(R.string.dialog_map_warn_title)).setMessage(this.d.getString(R.string.dialog_map_warn_text)).setPositiveButton(this.d.getString(R.string.ok), new dj(this)).create();
            default:
                return HoleMap.super.onCreateDialog(i2);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        HoleMap.super.onCreateOptionsMenu(menu);
        menu.add(10, 1, 0, (int) R.string.dialog_map_warn_title).setIcon(17301569);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.a.close();
        this.a = null;
        this.d = null;
        this.f = null;
        this.i.clear();
        this.i = null;
        this.g = null;
        this.h = null;
        this.j = null;
        this.s = null;
        this.m.removeAllViews();
        this.n.getOverlays().clear();
        this.n.removeAllViews();
        com.asai24.golf.d.d.a(findViewById(R.id.hole_map_layout));
        this.b.a();
        this.b = null;
        System.gc();
        HoleMap.super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 1:
                showDialog(1);
                break;
        }
        return HoleMap.super.onOptionsItemSelected(menuItem);
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [android.content.Context, com.asai24.golf.activity.HoleMap, com.google.android.maps.MapActivity] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onPause() {
        /*
            r4 = this;
            r3 = 0
            android.content.SharedPreferences r0 = android.preference.PreferenceManager.getDefaultSharedPreferences(r4)
            android.content.SharedPreferences$Editor r0 = r0.edit()
            r1 = 2131231021(0x7f08012d, float:1.8078111E38)
            java.lang.String r1 = r4.getString(r1)
            boolean r2 = r4.C
            r0.putBoolean(r1, r2)
            r0.commit()
            android.os.Handler r0 = r4.c
            if (r0 == 0) goto L_0x0022
            android.os.Handler r0 = r4.c
            r1 = 0
            r0.removeMessages(r1)
        L_0x0022:
            r4.c = r3
            r4.c()
            android.location.LocationManager r0 = r4.e
            android.location.LocationListener r1 = r4.f
            r0.removeUpdates(r1)
            r4.e = r3
            com.asai24.golf.view.b r0 = r4.g
            r0.disableMyLocation()
            com.mobfox.sdk.MobFoxView r0 = r4.E
            if (r0 == 0) goto L_0x003e
            com.mobfox.sdk.MobFoxView r0 = r4.E
            r0.c()
        L_0x003e:
            com.asai24.golf.activity.HoleMap.super.onPause()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.asai24.golf.activity.HoleMap.onPause():void");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.e = (LocationManager) getSystemService("location");
        this.e.requestLocationUpdates("gps", 0, 0.0f, this.f);
        b();
        d();
        f();
        e();
        if (!this.t) {
            this.s.show();
            this.c.postDelayed(new df(this), 15000);
        }
        this.n.invalidate();
        if (this.E != null) {
            this.E.d();
        }
        HoleMap.super.onResume();
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        float[] fArr = sensorEvent.values;
        if (fArr != null) {
            float f2 = fArr[0];
            if (Math.abs(f2 - this.D) >= 10.0f) {
                a(Float.valueOf(f2));
            }
        }
    }
}
