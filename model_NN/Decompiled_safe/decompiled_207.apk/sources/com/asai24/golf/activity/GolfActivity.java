package com.asai24.golf.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;
import com.asai24.golf.R;
import com.asai24.golf.d.c;
import com.mobfox.sdk.MobFoxView;
import jp.co.nobot.libAdMaker.libAdMaker;

public abstract class GolfActivity extends Activity {
    protected Toast a;
    private MobFoxView b;

    public void a(int i) {
        c(getString(i));
    }

    public void a(String str) {
        Intent intent = new Intent(this, BrowserActivity.class);
        intent.putExtra("URL", str);
        startActivity(intent);
    }

    public boolean a() {
        for (NetworkInfo isAvailable : ((ConnectivityManager) getSystemService("connectivity")).getAllNetworkInfo()) {
            if (isAvailable.isAvailable()) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        startActivity(new Intent("android.intent.action.SENDTO", Uri.fromParts("mailto", str, null)));
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        if (((LocationManager) getSystemService("location")).isProviderEnabled("gps")) {
            return true;
        }
        showDialog(10);
        return false;
    }

    /* access modifiers changed from: protected */
    public void c() {
        libAdMaker libadmaker = (libAdMaker) findViewById(R.id.admakerview);
        libadmaker.a = getString(R.string.admaker_siteId);
        libadmaker.b = getString(R.string.admaker_zoneId);
        libadmaker.a(getString(R.string.admaker_setUrl));
        libadmaker.d();
    }

    public void c(String str) {
        if (this.a == null) {
            this.a = Toast.makeText(this, (CharSequence) null, 0);
        }
        this.a.cancel();
        this.a.setText(str);
        this.a.setDuration(1);
        this.a.show();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 9:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.network_unavailable_title).setMessage((int) R.string.network_unavailable_message).setPositiveButton((int) R.string.ok, new dd(this)).create();
            case 10:
                return new AlertDialog.Builder(this).setIcon(17301543).setTitle((int) R.string.check_gps_title).setMessage((int) R.string.check_gps_message).setPositiveButton((int) R.string.setting, new dc(this)).setNegativeButton((int) R.string.close, new di(this)).create();
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.b != null) {
            this.b.c();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        c.a((Activity) this);
        if (this.b != null) {
            this.b.d();
        }
    }
}
