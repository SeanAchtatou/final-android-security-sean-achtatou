package com.asai24.golf.activity;

import android.content.DialogInterface;
import android.view.View;
import android.widget.EditText;
import com.asai24.golf.R;

class bn implements DialogInterface.OnClickListener {
    final /* synthetic */ SelectPlayers a;
    private final /* synthetic */ View b;

    bn(SelectPlayers selectPlayers, View view) {
        this.a = selectPlayers;
        this.b = view;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        EditText editText = (EditText) this.b.findViewById(R.id.name_edit);
        String editable = editText.getText().toString();
        if (editable.length() == 0) {
            this.a.a((int) R.string.invalid_input_no_value);
            return;
        }
        for (Long longValue : this.a.o) {
            long longValue2 = longValue.longValue();
            if (longValue2 == this.a.q) {
                this.a.g.b(longValue2, editable);
            }
        }
        this.a.d();
        editText.setText("");
    }
}
