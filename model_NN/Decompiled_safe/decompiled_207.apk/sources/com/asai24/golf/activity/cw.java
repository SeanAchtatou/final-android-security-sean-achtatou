package com.asai24.golf.activity;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.asai24.golf.R;

class cw implements DialogInterface.OnClickListener {
    final /* synthetic */ YourGolfAccountNew a;

    cw(YourGolfAccountNew yourGolfAccountNew) {
        this.a = yourGolfAccountNew;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (this.a.w == 1) {
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.a).edit();
            if (this.a.t == R.id.yourgolf_account_register || this.a.t == R.id.yourgolf_account_overwrite_password) {
                edit.putBoolean(this.a.getString(R.string.yourgolf_account_check_activation_key), true);
                this.a.setResult(-1);
            } else {
                edit.putBoolean(this.a.getString(R.string.yourgolf_account_check_activation_key), false);
            }
            edit.commit();
            this.a.finish();
        }
    }
}
