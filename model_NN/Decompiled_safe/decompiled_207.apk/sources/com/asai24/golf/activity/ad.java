package com.asai24.golf.activity;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.TextView;
import com.asai24.golf.R;
import com.asai24.golf.a.g;
import com.asai24.golf.e.a;

class ad extends CursorAdapter {
    final /* synthetic */ ScoreEdit a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ad(ScoreEdit scoreEdit, Context context, g gVar) {
        super(context, gVar);
        this.a = scoreEdit;
    }

    public void bindView(View view, Context context, Cursor cursor) {
        g gVar = (g) cursor;
        this.a.startManagingCursor(gVar);
        a aVar = new a(this.a);
        String sb = new StringBuilder().append(gVar.d()).toString();
        String a2 = aVar.a(gVar.e());
        String f = gVar.f();
        Double valueOf = Double.valueOf(gVar.g());
        Double valueOf2 = Double.valueOf(gVar.h());
        ((TextView) view.findViewById(R.id.score_detail_shot_no)).setText(sb);
        TextView textView = (TextView) view.findViewById(R.id.score_detail_shot_club);
        Log.v("ScoreEdit", gVar.e());
        Log.v("ScoreEdit", a2);
        if (a2.equals("ptt")) {
            textView.setText("pt");
        } else {
            textView.setText(a2);
        }
        if (a2.equals("")) {
            textView.setBackgroundDrawable(null);
        } else {
            textView.setBackgroundResource(R.drawable.club_bg);
        }
        ((TextView) view.findViewById(R.id.score_detail_shot_result)).setText(f);
        TextView textView2 = (TextView) view.findViewById(R.id.score_detail_shot_loc);
        if (valueOf.doubleValue() == 0.0d || valueOf2.doubleValue() == 0.0d) {
            textView2.setTextSize(18.0f);
            textView2.setTypeface(Typeface.DEFAULT);
            textView2.setText("N/A");
        } else {
            textView2.setTextSize(24.0f);
            textView2.setTypeface(Typeface.DEFAULT_BOLD);
            textView2.setText("◯");
        }
        Button button = (Button) view.findViewById(R.id.score_detail_edit_btn);
        Button button2 = (Button) view.findViewById(R.id.score_detail_delete_btn);
        button.setTag(Long.valueOf(gVar.b()));
        button2.setTag(Long.valueOf(gVar.b()));
        button.setOnClickListener(this.a);
        button2.setOnClickListener(this.a);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2 = super.getView(i, view, viewGroup);
        if (i % 2 == 0) {
            view2.setBackgroundColor(this.a.d.getColor(R.color.score_edit_list_bg));
        } else {
            view2.setBackgroundColor(this.a.d.getColor(R.color.transparent));
        }
        return view2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate((int) R.layout.score_edit_item, viewGroup, false);
    }
}
