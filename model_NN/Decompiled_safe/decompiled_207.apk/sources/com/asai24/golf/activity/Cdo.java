package com.asai24.golf.activity;

import android.content.DialogInterface;
import com.asai24.golf.R;

/* renamed from: com.asai24.golf.activity.do  reason: invalid class name */
class Cdo implements DialogInterface.OnClickListener {
    final /* synthetic */ ScoreEntryWithMap a;

    Cdo(ScoreEntryWithMap scoreEntryWithMap) {
        this.a = scoreEntryWithMap;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        String editable = this.a.J.getText().toString();
        if (editable.length() <= 0) {
            this.a.a((int) R.string.invalid_input_no_value);
            return;
        }
        for (long j : this.a.s) {
            if (j == this.a.H) {
                this.a.b.b(j, editable);
            }
        }
        this.a.f();
        this.a.J.setText("");
    }
}
