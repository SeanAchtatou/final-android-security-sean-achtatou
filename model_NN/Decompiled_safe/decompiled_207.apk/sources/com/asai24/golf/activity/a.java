package com.asai24.golf.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import com.asai24.golf.R;
import com.asai24.golf.c.k;

public class a extends AsyncTask {
    private static /* synthetic */ int[] c;
    final /* synthetic */ PlayHistories a;
    private Context b;

    public a(PlayHistories playHistories, Context context) {
        this.a = playHistories;
        this.b = context;
    }

    static /* synthetic */ int[] a() {
        int[] iArr = c;
        if (iArr == null) {
            iArr = new int[com.asai24.golf.a.values().length];
            try {
                iArr[com.asai24.golf.a.NONE.ordinal()] = 12;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[com.asai24.golf.a.OOB_ERROR.ordinal()] = 6;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[com.asai24.golf.a.OOB_INVALID_SESSION.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[com.asai24.golf.a.OOB_NO_PERMISSION.ordinal()] = 5;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[com.asai24.golf.a.OOB_NO_SETTINGS.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[com.asai24.golf.a.OOB_SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[com.asai24.golf.a.OOB_ZERO_SCORE.ordinal()] = 4;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[com.asai24.golf.a.YOURGOLF_ERROR.ordinal()] = 11;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[com.asai24.golf.a.YOURGOLF_INVALID_TOKEN.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[com.asai24.golf.a.YOURGOLF_NO_SETTINGS.ordinal()] = 8;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[com.asai24.golf.a.YOURGOLF_SUCCESS.ordinal()] = 7;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[com.asai24.golf.a.YOURGOLF_UNEXPECTED_ERROR.ordinal()] = 10;
            } catch (NoSuchFieldError e12) {
            }
            c = iArr;
        }
        return iArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.asai24.golf.c.k.a(long, boolean):com.asai24.golf.a
     arg types: [long, int]
     candidates:
      com.asai24.golf.c.k.a(long, java.lang.String):int
      com.asai24.golf.c.e.a(java.lang.String, java.lang.Object[]):void
      com.asai24.golf.c.e.a(org.apache.http.client.HttpClient, android.content.Context):void
      com.asai24.golf.c.k.a(long, boolean):com.asai24.golf.a */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public com.asai24.golf.a doInBackground(String... strArr) {
        try {
            k kVar = new k(this.b);
            return this.a.m ? kVar.a(this.a.l, true) : kVar.a(this.a.l);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(com.asai24.golf.a aVar) {
        super.onPostExecute(aVar);
        if (this.a.q) {
            this.a.r = aVar;
            if (this.a.s != com.asai24.golf.a.NONE) {
                this.a.h();
                this.a.j.dismiss();
                return;
            }
            return;
        }
        switch (a()[aVar.ordinal()]) {
            case 2:
                this.a.showDialog(1);
                break;
            case 3:
                Toast.makeText(this.a, (int) R.string.invalid_session, 1).show();
                break;
            case 4:
                Toast.makeText(this.a, (int) R.string.status_send_with_zero_score, 1).show();
                break;
            case 5:
                this.a.showDialog(4);
                break;
            case 6:
                Toast.makeText(this.a, (int) R.string.status_send_error, 1).show();
                break;
            default:
                Toast.makeText(this.a, (int) R.string.send_success, 1).show();
                break;
        }
        this.a.j.dismiss();
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        this.a.j = new ProgressDialog(this.a);
        this.a.j.setIndeterminate(true);
        this.a.j.setCancelable(false);
        this.a.j.setMessage(this.a.getString(R.string.msg_now_loading));
        if (!this.a.isFinishing()) {
            this.a.j.show();
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Object... objArr) {
        super.onProgressUpdate(objArr);
    }
}
