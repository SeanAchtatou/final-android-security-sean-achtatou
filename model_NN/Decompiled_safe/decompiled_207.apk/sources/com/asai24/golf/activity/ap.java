package com.asai24.golf.activity;

import android.content.DialogInterface;
import android.util.Log;
import com.asai24.golf.a.g;

class ap implements DialogInterface.OnClickListener {
    final /* synthetic */ ScoreEdit a;

    ap(ScoreEdit scoreEdit) {
        this.a = scoreEdit;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        g v = this.a.b.v((long) this.a.f);
        g m = this.a.b.m(v.c());
        int d = v.d();
        this.a.b.k((long) this.a.f);
        while (!m.isAfterLast()) {
            Log.d("ScoreEdit", "shot: " + m.b() + ", " + m.d() + ", " + d);
            if (m.d() > d) {
                this.a.b.e(m.b(), m.d() - 1);
            }
            m.moveToNext();
        }
        this.a.b.j(v.c());
        v.close();
        m.close();
        this.a.onResume();
    }
}
