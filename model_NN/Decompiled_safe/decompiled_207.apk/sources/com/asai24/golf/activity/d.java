package com.asai24.golf.activity;

import android.view.View;

class d implements View.OnClickListener {
    final /* synthetic */ GamePointEntry a;
    private int b;
    private int c;
    private View.OnClickListener d;

    public d(GamePointEntry gamePointEntry, int i, int i2) {
        this.a = gamePointEntry;
        this.b = i;
        this.c = i2;
    }

    public int a() {
        return this.b;
    }

    public void a(View.OnClickListener onClickListener) {
        this.d = onClickListener;
    }

    public int b() {
        return this.c;
    }

    public void onClick(View view) {
        if (this.d != null) {
            this.d.onClick(view);
        }
    }
}
