package com.asai24.golf.activity;

import android.view.View;
import com.asai24.golf.R;

class ek implements View.OnClickListener {
    final /* synthetic */ cj a;
    private final /* synthetic */ long b;

    ek(cj cjVar, long j) {
        this.a = cjVar;
        this.b = j;
    }

    public void onClick(View view) {
        if (this.b == 1) {
            this.a.a.a((int) R.string.cannnot_remove_member);
            return;
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.a.a.o.size()) {
                break;
            } else if (((Long) this.a.a.o.get(i2)).longValue() == this.b) {
                this.a.a.o.remove(i2);
                break;
            } else {
                i = i2 + 1;
            }
        }
        if (this.a.a.p.contains(Long.valueOf(this.b))) {
            this.a.a.p.remove(Long.valueOf(this.b));
        } else {
            this.a.a.g.p(this.b);
        }
        this.a.a.d();
    }
}
