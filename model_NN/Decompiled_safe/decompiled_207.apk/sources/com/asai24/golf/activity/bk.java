package com.asai24.golf.activity;

import android.view.View;

class bk implements View.OnClickListener {
    final /* synthetic */ About a;
    private int b;
    private int c;
    private int d;
    private View.OnClickListener e;

    public bk(About about, int i, int i2) {
        this.a = about;
        this.b = i;
        this.c = i2;
    }

    public int a() {
        return this.b;
    }

    public void a(int i) {
        this.d = i;
    }

    public void a(View.OnClickListener onClickListener) {
        this.e = onClickListener;
    }

    public int b() {
        return this.c;
    }

    public void onClick(View view) {
        if (this.e != null) {
            view.setTag(Integer.valueOf(this.d));
            this.e.onClick(view);
        }
    }
}
