package com.asai24.golf.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.asai24.golf.GolfApplication;
import com.asai24.golf.R;
import com.asai24.golf.d.d;

public class YourGolfAccountUserAgreement extends GolfActivity implements View.OnClickListener {
    private GolfApplication b;

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.user_agreement_ok /*2131428215*/:
                setResult(-1);
                finish();
                return;
            case R.id.user_agreement_cancel /*2131428216*/:
                setResult(0);
                finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.yourgolf_account_user_agreement);
        this.b = (GolfApplication) getApplication();
        ((Button) findViewById(R.id.user_agreement_ok)).setOnClickListener(this);
        ((Button) findViewById(R.id.user_agreement_cancel)).setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.b.a();
        this.b.c();
        d.a(findViewById(R.id.yourgolf_account_user_agreement));
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.b.b();
        super.onPause();
    }
}
