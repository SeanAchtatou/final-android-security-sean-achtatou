package com.asai24.golf.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.asai24.golf.R;
import com.asai24.golf.a.b;
import com.asai24.golf.a.d;
import com.asai24.golf.a.s;
import com.asai24.golf.a.w;
import java.util.ArrayList;

public class GamePointEntry extends GolfActivity implements View.OnClickListener {
    private b b;
    private Resources c;
    private long d;
    private long e;
    private long f;
    private long g;
    private ListView h;
    private ListView i;
    private ListView j;
    private ListView k;
    private TextView l;
    private ImageButton m;
    private ImageButton n;
    private s o;
    private x p;
    private ArrayAdapter q;
    private ArrayAdapter r;
    private ArrayAdapter s;
    private int t;
    /* access modifiers changed from: private */
    public int u;
    private int v;
    /* access modifiers changed from: private */
    public int w;

    private void e() {
        this.h = (ListView) findViewById(R.id.hole_summary);
        this.i = (ListView) findViewById(R.id.sign_picker);
        this.j = (ListView) findViewById(R.id.tenths_place_point_picker);
        this.k = (ListView) findViewById(R.id.ones_place_point_picker);
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < 2; i2++) {
            d dVar = new d(this, i2, 1);
            dVar.a(this);
            arrayList.add(dVar);
        }
        this.q = new el(this, this, arrayList);
        this.i.setAdapter((ListAdapter) this.q);
        ArrayList arrayList2 = new ArrayList();
        for (int i3 = 0; i3 < 10; i3++) {
            d dVar2 = new d(this, i3, 2);
            dVar2.a(this);
            arrayList2.add(dVar2);
        }
        this.r = new el(this, this, arrayList2);
        this.j.setAdapter((ListAdapter) this.r);
        ArrayList arrayList3 = new ArrayList();
        for (int i4 = 0; i4 < 10; i4++) {
            d dVar3 = new d(this, i4, 3);
            dVar3.a(this);
            arrayList3.add(dVar3);
        }
        this.s = new el(this, this, arrayList3);
        this.k.setAdapter((ListAdapter) this.s);
    }

    /* access modifiers changed from: private */
    public void f() {
        int i2 = this.w == 0 ? this.u : -this.u;
        w g2 = this.b.g(this.d);
        this.b.c(this.d, i2);
        g2.close();
        this.t = i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void g() {
        Intent intent = new Intent(this, ScoreEntryWithMap.class);
        intent.putExtra("playing_hole_id", this.f);
        intent.putExtra("SingleFlg", false);
        setResult(-1, intent);
        finish();
    }

    /* access modifiers changed from: private */
    public void h() {
        switch (this.v) {
            case 1:
                g();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void d() {
        if (this.o != null && !this.o.isClosed()) {
            this.o.close();
        }
        this.o = this.b.e(this.f);
        if (this.p == null) {
            this.p = new x(this, this.o, this);
        } else {
            this.p.changeCursor(this.o);
        }
        this.h.setAdapter((ListAdapter) this.p);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.point_entry_back /*2131427426*/:
                finish();
                return;
            case R.id.point_entry_register /*2131427427*/:
                f();
                finish();
                return;
            default:
                int id = view.getId();
                switch (Integer.parseInt(new StringBuilder().append(view.getTag()).toString())) {
                    case 1:
                        this.w = id / 100;
                        this.q.notifyDataSetChanged();
                        return;
                    case 2:
                        this.u = ((id / 10) * 10) + (this.u % 10);
                        this.r.notifyDataSetChanged();
                        return;
                    case 3:
                        this.u = id + ((this.u / 10) * 10);
                        this.s.notifyDataSetChanged();
                        return;
                    default:
                        return;
                }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.game_point_entry);
        this.b = b.a(this);
        this.c = getResources();
        this.d = getIntent().getExtras().getLong("score_id");
        w g2 = this.b.g(this.d);
        this.e = g2.f();
        this.f = g2.d();
        this.g = g2.e();
        this.l = (TextView) findViewById(R.id.point_entry_player);
        this.m = (ImageButton) findViewById(R.id.point_entry_back);
        this.n = (ImageButton) findViewById(R.id.point_entry_register);
        this.m.setOnClickListener(this);
        this.n.setOnClickListener(this);
        d b2 = this.b.b(this.e);
        this.l.setText(b2.c());
        b2.close();
        e();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        switch (i2) {
            case 1:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.confirmation).setMessage((int) R.string.unsaved_msg).setPositiveButton((int) R.string.yes, new eo(this)).setNegativeButton((int) R.string.no, new en(this)).create();
            default:
                return super.onCreateDialog(i2);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        com.asai24.golf.d.d.a(findViewById(R.id.game_point_entry));
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i2, Dialog dialog) {
        super.onPrepareDialog(i2, dialog);
        this.v = i2;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        w g2;
        super.onResume();
        if (this.d == -1) {
            g2 = this.b.a(this.g, this.f, this.e);
            this.d = g2.b();
        } else {
            g2 = this.b.g(this.d);
        }
        this.t = g2.g();
        g2.close();
        this.u = this.t;
        if (this.u >= 0) {
            this.w = 0;
        } else {
            this.w = 1;
            this.u = Math.abs(this.u);
        }
        this.i.setSelection(this.w);
        this.j.setSelection(this.u / 10);
        this.k.setSelection(this.u % 10);
        this.q.notifyDataSetChanged();
        this.r.notifyDataSetChanged();
        this.s.notifyDataSetChanged();
        d();
    }
}
