package com.asai24.golf.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.asai24.golf.GolfApplication;
import com.asai24.golf.R;
import com.asai24.golf.a.b;
import com.asai24.golf.a.g;
import com.asai24.golf.a.w;
import com.asai24.golf.d.d;

public class ScoreEdit extends GolfActivity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public b b;
    private GolfApplication c;
    /* access modifiers changed from: private */
    public Resources d;
    private long e;
    /* access modifiers changed from: private */
    public int f;
    private Button g;
    private Button h;
    private ListView i;
    private g j;
    private ad k;

    private void d() {
        this.j = this.b.m(this.e);
        if (this.k == null) {
            this.k = new ad(this, this, this.j);
        } else {
            this.k.changeCursor(this.j);
        }
        this.i.setAdapter((ListAdapter) this.k);
    }

    private Boolean e() {
        w g2 = this.b.g(this.e);
        int c2 = g2.c();
        g2.close();
        if (c2 < 15) {
            return false;
        }
        c(this.d.getString(R.string.warning_max_shot));
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_to_score_enter /*2131428095*/:
                finish();
                return;
            case R.id.score_detail_insert /*2131428096*/:
                this.c.a("/edit_shot_detail");
                this.c.b("/edit_shot_detail");
                if (!e().booleanValue()) {
                    Intent intent = new Intent(this, ScoreDetailEdit.class);
                    intent.putExtra("new_record_flg", true);
                    intent.putExtra("score_id", this.e);
                    startActivity(intent);
                    return;
                }
                return;
            case R.id.score_detail_edit_btn /*2131428105*/:
                this.c.a("/edit_shot_detail");
                this.c.b("/edit_shot_detail");
                this.f = Integer.valueOf(new StringBuilder().append(view.getTag()).toString()).intValue();
                Intent intent2 = new Intent(this, ScoreDetailEdit.class);
                intent2.putExtra("new_record_flg", false);
                intent2.putExtra("score_detail_id", this.f);
                startActivity(intent2);
                return;
            case R.id.score_detail_delete_btn /*2131428110*/:
                this.f = Integer.valueOf(new StringBuilder().append(view.getTag()).toString()).intValue();
                showDialog(0);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.score_edit);
        this.c = (GolfApplication) getApplication();
        this.b = b.a(this);
        this.d = getResources();
        this.e = getIntent().getExtras().getLong("score_id");
        this.g = (Button) findViewById(R.id.back_to_score_enter);
        this.h = (Button) findViewById(R.id.score_detail_insert);
        this.i = (ListView) findViewById(R.id.score_detail_list);
        this.g.setOnClickListener(this);
        this.h.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        switch (i2) {
            case 0:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.dialog_delete_title).setMessage((int) R.string.dialog_delete_text).setPositiveButton((int) R.string.ok, new ap(this)).setNegativeButton((int) R.string.cancel, new ao(this)).create();
            default:
                return super.onCreateDialog(i2);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.c.a();
        this.c.c();
        d.a(findViewById(R.id.score_edit));
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.j.close();
        this.j = null;
        this.c.b();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        d();
        super.onResume();
    }
}
