package com.asai24.golf.activity;

import android.view.View;

class cm implements View.OnClickListener {
    final /* synthetic */ ScoreEntryGroup a;
    private int b;
    private int c;
    private View.OnClickListener d;

    public cm(ScoreEntryGroup scoreEntryGroup, int i, int i2) {
        this.a = scoreEntryGroup;
        this.b = i;
        this.c = i2;
    }

    public int a() {
        return this.b;
    }

    public void a(View.OnClickListener onClickListener) {
        this.d = onClickListener;
    }

    public int b() {
        return this.c;
    }

    public void onClick(View view) {
        if (this.d != null) {
            this.d.onClick(view);
        }
    }
}
