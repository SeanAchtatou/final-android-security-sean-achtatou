package com.asai24.golf.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.text.InputFilter;
import android.text.method.PasswordTransformationMethod;
import android.widget.Toast;
import com.asai24.golf.R;
import com.asai24.golf.a.b;
import com.asai24.golf.d.a;

public class GolfSettingsDetail extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener, Preference.OnPreferenceClickListener, Runnable {
    private EditTextPreference a;
    private ListPreference b;
    private ListPreference c;
    private PreferenceScreen d;
    private CheckBoxPreference e;
    private CheckBoxPreference f;
    private EditTextPreference g;
    private EditTextPreference h;
    private b i;
    private String j;
    /* access modifiers changed from: private */
    public ProgressDialog k;
    private String l;
    private String m;
    /* access modifiers changed from: private */
    public boolean n;
    private Handler o = new ac(this);

    private void a() {
        if (this.j.equals(getString(R.string.setting_yourgolf_key))) {
            addPreferencesFromResource(R.xml.settings_yourgolf);
            this.a = (EditTextPreference) findPreference(getString(R.string.key_owner_name));
            this.b = (ListPreference) findPreference(getString(R.string.key_owner_gender));
            this.c = (ListPreference) findPreference(getString(R.string.key_owner_measure_unit));
            this.d = (PreferenceScreen) findPreference(getString(R.string.setting_yourgolf_account_key));
            this.d.setOnPreferenceClickListener(this);
            this.e = (CheckBoxPreference) findPreference(getString(R.string.key_owner_point));
            this.f = (CheckBoxPreference) findPreference(getString(R.string.key_owner_stableford));
            this.a.getEditText().setInputType(1);
            this.a.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(10), new a(18)});
        } else if (this.j.equals(getString(R.string.setting_oobgolf_key))) {
            addPreferencesFromResource(R.xml.settings_oobgolf);
            this.g = (EditTextPreference) findPreference(getString(R.string.key_oob_username));
            this.h = (EditTextPreference) findPreference(getString(R.string.key_oob_password));
            this.g.getEditText().setInputType(1);
            this.h.getEditText().setInputType(128);
            this.h.getEditText().setTransformationMethod(new PasswordTransformationMethod());
            InputFilter[] inputFilterArr = {new InputFilter.LengthFilter(30)};
            this.g.getEditText().setFilters(inputFilterArr);
            this.h.getEditText().setFilters(inputFilterArr);
        }
    }

    private void a(CheckBoxPreference checkBoxPreference, boolean z) {
        checkBoxPreference.setChecked(PreferenceManager.getDefaultSharedPreferences(this).getBoolean(checkBoxPreference.getKey(), z));
    }

    private void a(Preference preference) {
        String string = PreferenceManager.getDefaultSharedPreferences(this).getString(preference.getKey(), "");
        if (!string.equals("")) {
            preference.setSummary(string);
        }
    }

    private void a(Preference preference, int i2) {
        Resources resources = getResources();
        String string = PreferenceManager.getDefaultSharedPreferences(this).getString(preference.getKey(), "");
        if (!string.equals("")) {
            switch (i2) {
                case 0:
                    preference.setSummary(getString(resources.getIdentifier("gender_" + string, "string", getPackageName())));
                    return;
                case 1:
                    preference.setSummary(getString(resources.getIdentifier("measure_unit_" + string, "string", getPackageName())));
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, boolean z) {
        this.l = str;
        if (z) {
            this.m = getString(R.string.success);
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this).edit();
            edit.putBoolean(getString(R.string.yourgolf_account_check_activation_key), false);
            edit.commit();
            this.n = true;
        } else {
            this.m = getString(R.string.error);
            this.l = " " + getString(R.string.yourgolf_account_check_activation_error);
            this.n = false;
        }
        showDialog(1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.asai24.golf.activity.GolfSettingsDetail.a(android.preference.CheckBoxPreference, boolean):void
     arg types: [android.preference.CheckBoxPreference, int]
     candidates:
      com.asai24.golf.activity.GolfSettingsDetail.a(android.preference.Preference, int):void
      com.asai24.golf.activity.GolfSettingsDetail.a(com.asai24.golf.activity.GolfSettingsDetail, android.app.ProgressDialog):void
      com.asai24.golf.activity.GolfSettingsDetail.a(java.lang.String, boolean):void
      com.asai24.golf.activity.GolfSettingsDetail.a(android.preference.CheckBoxPreference, boolean):void */
    private void b() {
        if (this.j.equals(getString(R.string.setting_yourgolf_key))) {
            a(this.a);
            a(this.b, 0);
            a(this.c, 1);
            a(this.e, false);
            a(this.f, false);
        } else if (this.j.equals(getString(R.string.setting_oobgolf_key))) {
            a(this.g);
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        String string = PreferenceManager.getDefaultSharedPreferences(this).getString(getString(R.string.yourgolf_account_auth_token_key), "");
        if (string == null || string.equals("")) {
            startActivityForResult(new Intent(this, YourGolfAccountNew.class), 0);
        } else {
            startActivityForResult(new Intent(this, YourGolfAccountExist.class), 0);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1 && PreferenceManager.getDefaultSharedPreferences(this).getBoolean(getString(R.string.yourgolf_account_check_activation_key), false)) {
            showDialog(0);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().setBackgroundDrawableResource(R.drawable.yourgolf_bg_image2);
        getListView().setCacheColorHint(0);
        getListView().setDivider(null);
        getListView().setDividerHeight(3);
        this.j = getIntent().getExtras().getString(getString(R.string.setting_detail_mode));
        this.i = b.a(this);
        a();
        b();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        switch (i2) {
            case 0:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.yourgolf_account_check_activation_title).setMessage((int) R.string.yourgolf_account_check_activation_msg).setPositiveButton((int) R.string.yourgolf_account_login_btn, new aa(this)).setNegativeButton((int) R.string.cancel, new z(this)).create();
            case 1:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle(this.m).setMessage(this.l).setPositiveButton((int) R.string.ok, new ab(this)).create();
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    public boolean onPreferenceClick(Preference preference) {
        if (!preference.getKey().equals(getString(R.string.setting_yourgolf_account_key))) {
            return false;
        }
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(getString(R.string.yourgolf_account_check_activation_key), false)) {
            showDialog(0);
        } else {
            c();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i2, Dialog dialog) {
        switch (i2) {
            case 1:
                ((AlertDialog) dialog).setTitle(this.m);
                ((AlertDialog) dialog).setMessage(this.l);
                break;
        }
        super.onPrepareDialog(i2, dialog);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        if (str.equals(getString(R.string.key_owner_name))) {
            String string = sharedPreferences.getString(str, "");
            if (string.length() == 0) {
                Toast.makeText(this, (int) R.string.cannnot_set_name_of_length_zero, 0).show();
                return;
            }
            this.i.b(string);
        }
        b();
    }

    public void run() {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String string = defaultSharedPreferences.getString(getString(R.string.yourgolf_account_username_key), "");
        String string2 = defaultSharedPreferences.getString(getString(R.string.yourgolf_account_password_key), "");
        com.asai24.golf.c.a aVar = new com.asai24.golf.c.a(this);
        String a2 = aVar.a(string, string2, 3);
        Message message = new Message();
        Bundle bundle = new Bundle();
        bundle.putString("msg", a2);
        bundle.putBoolean("result", aVar.a());
        message.setData(bundle);
        this.o.sendMessage(message);
    }
}
