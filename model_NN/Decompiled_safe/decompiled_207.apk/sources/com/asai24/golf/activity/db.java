package com.asai24.golf.activity;

import android.view.View;
import com.asai24.golf.R;

class db implements View.OnClickListener {
    final /* synthetic */ da a;
    private final /* synthetic */ dz b;

    db(da daVar, dz dzVar) {
        this.a = daVar;
        this.b = dzVar;
    }

    public void onClick(View view) {
        if (this.a.a.z.booleanValue()) {
            return;
        }
        if (this.b.f.booleanValue()) {
            this.a.a.a((int) R.string.cannnot_edit_owner_name);
            return;
        }
        this.a.a.H = this.b.a;
        this.a.a.I = this.b.b;
        this.a.a.showDialog(5);
    }
}
