package com.asai24.golf.activity;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

class bl implements LocationListener {
    final /* synthetic */ SearchCourse a;

    bl(SearchCourse searchCourse) {
        this.a = searchCourse;
    }

    public void onLocationChanged(Location location) {
        this.a.a(location);
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
