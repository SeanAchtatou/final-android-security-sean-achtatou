package com.asai24.golf.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

class aq extends Handler {
    final /* synthetic */ YourGolfAccountNew a;

    aq(YourGolfAccountNew yourGolfAccountNew) {
        this.a = yourGolfAccountNew;
    }

    public void handleMessage(Message message) {
        this.a.s.dismiss();
        this.a.s = (ProgressDialog) null;
        Bundle data = message.getData();
        this.a.a(data.getString("msg"), data.getBoolean("result"));
    }
}
