package com.asai24.golf.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import com.asai24.golf.R;
import com.asai24.golf.d.f;

public class InteractDataActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.interact_data);
        Intent intent = new Intent();
        try {
            String str = Environment.getExternalStorageDirectory() + "/yourgolf/golf.db";
            f.a(getApplicationContext(), getDatabasePath("golf.db").getAbsolutePath(), str);
            SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            String string = defaultSharedPreferences.getString(getString(R.string.yourgolf_account_username_key), "");
            String string2 = defaultSharedPreferences.getString(getString(R.string.yourgolf_account_password_key), "");
            String string3 = defaultSharedPreferences.getString(getString(R.string.yourgolf_account_auth_token_key), "");
            intent.putExtra("data_path", str);
            intent.putExtra("email", string);
            intent.putExtra("pass", string2);
            intent.putExtra("token", string3);
            setResult(-1, intent);
        } catch (Exception e) {
            Log.e("InteractDataActivity", new StringBuilder().append(e).toString());
            setResult(0);
        }
        finish();
    }

    public void onDestroy() {
        super.onDestroy();
    }
}
