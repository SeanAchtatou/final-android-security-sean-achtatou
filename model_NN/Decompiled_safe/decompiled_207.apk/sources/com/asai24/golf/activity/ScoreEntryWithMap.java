package com.asai24.golf.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.asai24.golf.GolfApplication;
import com.asai24.golf.R;
import com.asai24.golf.a.aa;
import com.asai24.golf.a.b;
import com.asai24.golf.a.d;
import com.asai24.golf.a.g;
import com.asai24.golf.a.i;
import com.asai24.golf.a.m;
import com.asai24.golf.a.s;
import com.asai24.golf.a.w;
import com.asai24.golf.a.z;
import com.asai24.golf.e.a;
import java.util.ArrayList;

public class ScoreEntryWithMap extends GolfActivity implements View.OnClickListener {
    private Boolean A;
    private s B;
    private al C;
    private da D;
    private String E;
    /* access modifiers changed from: private */
    public int F;
    /* access modifiers changed from: private */
    public boolean G;
    /* access modifiers changed from: private */
    public long H;
    /* access modifiers changed from: private */
    public String I;
    /* access modifiers changed from: private */
    public EditText J;
    /* access modifiers changed from: private */
    public b b;
    private GolfApplication c;
    private Handler d;
    private Resources e;
    /* access modifiers changed from: private */
    public Location f;
    private LocationManager g;
    private LocationListener h;
    private ListView i;
    private ListView j;
    private Button k;
    private Button l;
    private Button m;
    private Button n;
    private Button o;
    private Button p;
    private ImageButton q;
    private ImageButton r;
    /* access modifiers changed from: private */
    public long[] s;
    private int t = 0;
    private long u;
    private long v;
    /* access modifiers changed from: private */
    public long w;
    private long x;
    private long y;
    /* access modifiers changed from: private */
    public Boolean z;

    private void a(long j2, View view) {
        aa a = this.b.a(this.v, this.y, new long[]{j2});
        i b2 = this.b.b(this.v, j2);
        w a2 = this.b.a(this.v, this.y, j2);
        z a3 = this.b.a(this.v, j2, this.e.getString(R.string.club_pt));
        TextView textView = (TextView) view.findViewById(R.id.player_total_score);
        if (b2.c() > 0) {
            textView.setText(b2.b() + " (+" + b2.c() + ")" + " - P" + a3.a());
        } else {
            textView.setText(b2.b() + " (" + b2.c() + ")" + " - P" + a3.a());
        }
        a3.close();
        ((Button) view.findViewById(R.id.hole_player_strokes)).setText(String.valueOf(a.b()));
        TextView textView2 = (TextView) view.findViewById(R.id.hole_player_putts);
        if (b(a2.b()) == 0) {
            textView2.setText("");
        } else {
            textView2.setText(new StringBuilder().append(b(a2.b())).toString());
        }
        a.close();
        b2.close();
        a2.close();
    }

    private void a(View view) {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String[] stringArray = this.e.getStringArray(R.array.dialogue_club_id);
        String[] stringArray2 = this.e.getStringArray(R.array.club_name);
        for (int i2 = 0; i2 < stringArray.length; i2++) {
            Button button = (Button) view.findViewById(this.e.getIdentifier(stringArray[i2], "id", getPackageName()));
            if (defaultSharedPreferences.getBoolean(stringArray2[i2], true) || i2 == stringArray.length - 1) {
                button.setOnClickListener(this);
            } else {
                button.setEnabled(false);
                button.setBackgroundResource(R.drawable.square_btn2_disabled);
                button.setTextColor(-12303292);
            }
        }
        CheckBox checkBox = (CheckBox) view.findViewById(R.id.club_gps_status);
        ((Button) view.findViewById(R.id.club_cancel_btn)).setOnClickListener(this);
        checkBox.setOnClickListener(this);
        checkBox.setChecked(this.A.booleanValue());
    }

    private void a(dz dzVar, long j2, String str, int i2, da daVar) {
        i t2 = this.b.t(this.v);
        dzVar.b = str;
        dzVar.c = i2;
        w a = this.b.a(this.v, this.y, j2);
        z a2 = this.b.a(this.v, j2, this.e.getString(R.string.club_pt));
        if (b(a.b()) == 0) {
            dzVar.e = "";
        } else {
            dzVar.e = new StringBuilder().append(b(a.b())).toString();
        }
        dzVar.i = a2.a();
        a.close();
        a2.close();
        t2.moveToFirst();
        while (true) {
            if (t2.isAfterLast()) {
                break;
            } else if (t2.a() == j2) {
                dzVar.g = t2.b();
                dzVar.h = t2.c();
                break;
            } else {
                t2.moveToNext();
            }
        }
        daVar.add(dzVar);
        t2.close();
    }

    private int b(long j2) {
        g m2 = this.b.m(j2);
        a aVar = new a(this);
        int i2 = 0;
        for (int i3 = 0; i3 < m2.getCount(); i3++) {
            m2.moveToPosition(i3);
            if (aVar.a(m2.e()).equals("pt")) {
                i2++;
            }
        }
        m2.close();
        return i2;
    }

    private void b(View view) {
        this.A = Boolean.valueOf(((CheckBox) view.findViewById(R.id.club_gps_status)).isChecked());
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this).edit();
        edit.putBoolean(getString(R.string.dialog_gps_status_key), this.A.booleanValue());
        edit.commit();
    }

    private void e() {
        if (this.D != null) {
            this.D.clear();
            this.D = null;
        }
        this.D = new da(this, this, new ArrayList());
        this.j.setAdapter((ListAdapter) this.D);
        aa a = this.b.a(this.v, this.y, this.s);
        for (long j2 : this.s) {
            if (j2 == this.s[this.t]) {
                a(new dz(this, null), j2, a.a(), a.b(), this.D);
                this.u = j2;
            }
            a.moveToNext();
        }
        a.close();
        this.D.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void f() {
        if (this.D != null) {
            this.D.clear();
            this.D = null;
        }
        this.D = new da(this, this, new ArrayList());
        this.j.setAdapter((ListAdapter) this.D);
        aa a = this.b.a(this.v, this.y, this.s);
        for (long j2 : this.s) {
            dz dzVar = new dz(this, null);
            a(dzVar, j2, a.a(), a.b(), this.D);
            dzVar.a = j2;
            dzVar.d = a.c();
            d b2 = this.b.b(j2);
            if (b2.d() == 1) {
                dzVar.f = true;
                this.u = j2;
            } else {
                dzVar.f = false;
            }
            b2.close();
            a.moveToNext();
        }
        a.close();
        this.D.notifyDataSetChanged();
    }

    private Boolean g() {
        w a = this.b.a(this.v, this.y, this.u);
        int c2 = a.c();
        a.close();
        if (c2 < 15) {
            return false;
        }
        c(this.e.getString(R.string.warning_max_shot));
        return true;
    }

    public void a(long j2) {
        this.y = j2;
    }

    /* access modifiers changed from: protected */
    public void a(String str, String str2) {
        try {
            Integer valueOf = Integer.valueOf(Integer.parseInt(str));
            if (this.E.equals("meter")) {
                valueOf = Integer.valueOf(com.asai24.golf.f.a.a(valueOf.intValue()));
            }
            Integer valueOf2 = Integer.valueOf(Integer.parseInt(str2));
            if (valueOf.intValue() <= 0 || valueOf2.intValue() <= 0) {
                throw new NumberFormatException();
            }
            this.b.a(this.y, valueOf, valueOf2);
            c(this.e.getString(R.string.msg_successfully_updated));
            onResume();
        } catch (NumberFormatException e2) {
            Log.e("ScoreEntryWithMap", "format error", e2);
            c(this.e.getString(R.string.invalid_input_not_number));
        }
    }

    /* access modifiers changed from: protected */
    public void d() {
        if (this.B != null && !this.B.isClosed()) {
            this.B.close();
            this.B = null;
        }
        this.B = this.b.e(this.y);
        if (this.C == null) {
            this.C = new al(this, this.B, this);
        } else {
            this.C.changeCursor(this.B);
        }
        this.i.setAdapter((ListAdapter) this.C);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (this.b == null) {
            this.b = b.a(this);
        }
        switch (i3) {
            case -1:
                Bundle extras = intent.getExtras();
                this.y = extras.getLong("playing_hole_id");
                this.z = Boolean.valueOf(extras.getBoolean("SingleFlg"));
                onResume();
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        w a;
        switch (view.getId()) {
            case R.id.club_dr_btn /*2131427358*/:
            case R.id.club_3w_btn /*2131427359*/:
            case R.id.club_4w_btn /*2131427360*/:
            case R.id.club_5w_btn /*2131427361*/:
            case R.id.club_7w_btn /*2131427362*/:
            case R.id.club_hy_btn /*2131427363*/:
            case R.id.club_2i_btn /*2131427364*/:
            case R.id.club_3i_btn /*2131427365*/:
            case R.id.club_4i_btn /*2131427366*/:
            case R.id.club_5i_btn /*2131427367*/:
            case R.id.club_6i_btn /*2131427368*/:
            case R.id.club_7i_btn /*2131427369*/:
            case R.id.club_8i_btn /*2131427370*/:
            case R.id.club_9i_btn /*2131427371*/:
            case R.id.club_pw_btn /*2131427372*/:
            case R.id.club_gw_btn /*2131427373*/:
            case R.id.club_sw_btn /*2131427374*/:
            case R.id.club_lw_btn /*2131427375*/:
            case R.id.club_pt_btn /*2131427376*/:
                dismissDialog(0);
                View view2 = (View) findViewById(R.id.hole_player_strokes).getParent();
                w a2 = this.b.a(this.v, this.y, this.u);
                if (!this.A.booleanValue()) {
                    this.b.a(a2.b(), view.getTag().toString());
                } else if (this.f != null) {
                    this.b.a(a2.b(), this.f.getLatitude(), this.f.getLongitude(), view.getTag().toString());
                } else {
                    showDialog(1);
                    return;
                }
                a2.close();
                a(this.u, view2);
                return;
            case R.id.club_cancel_btn /*2131427377*/:
                dismissDialog(0);
                return;
            case R.id.club_gps_status /*2131427379*/:
                b(view);
                return;
            case R.id.hole_back /*2131427434*/:
                s d2 = this.b.d(this.y);
                if (d2.getCount() == 0) {
                    d2.close();
                    return;
                }
                a(d2.b());
                d2.close();
                onResume();
                return;
            case R.id.hole_next /*2131427435*/:
                s c2 = this.b.c(this.y);
                if (c2.getCount() == 0) {
                    c2.close();
                    return;
                }
                a(c2.b());
                c2.close();
                onResume();
                return;
            case R.id.player_next /*2131427477*/:
                this.c.a("ScoreEntry", "MoveToSinglePlay", "MoveToSinglePlay", 1);
                this.z = true;
                onResume();
                return;
            case R.id.plus_stroke /*2131427478*/:
                View view3 = (View) view.getParent().getParent();
                w a3 = this.b.a(this.v, this.y, (long) view3.getId());
                if (a3.c() == 0) {
                    s e2 = this.b.e(this.y);
                    for (int i2 = 0; i2 < e2.e(); i2++) {
                        this.b.i(a3.b());
                    }
                    e2.close();
                } else {
                    this.b.i(a3.b());
                }
                a3.close();
                a((long) view3.getId(), view3);
                return;
            case R.id.hole_player_strokes /*2131427480*/:
                if (this.z.booleanValue()) {
                    a = this.b.a(this.v, this.y, this.u);
                } else {
                    a = this.b.a(this.v, this.y, (long) ((View) view.getParent().getParent()).getId());
                }
                if (this.z.booleanValue() || this.F != 1) {
                    this.c.a("/edit_score");
                    this.c.b("/edit_score");
                    Intent intent = new Intent(this, ScoreEdit.class);
                    intent.putExtra("score_id", a.b());
                    a.close();
                    startActivity(intent);
                    return;
                }
                this.c.a("group_score_entry");
                this.c.b("group_score_entry");
                Intent intent2 = new Intent(this, ScoreEntryGroup.class);
                intent2.putExtra("score_id", a.b());
                a.close();
                startActivityForResult(intent2, 0);
                return;
            case R.id.minus_stroke /*2131427481*/:
                View view4 = (View) view.getParent().getParent();
                w a4 = this.b.a(this.v, this.y, (long) view4.getId());
                this.b.j(a4.b());
                a4.close();
                a((long) view4.getId(), view4);
                return;
            case R.id.player_game_score_btn /*2131427483*/:
                w a5 = this.b.a(this.v, this.y, (long) ((View) view.getParent().getParent()).getId());
                this.c.a("game_score_entry");
                this.c.b("game_score_entry");
                Intent intent3 = new Intent(this, GamePointEntry.class);
                intent3.putExtra("score_id", a5.b());
                a5.close();
                startActivityForResult(intent3, 0);
                return;
            case R.id.player_back /*2131427484*/:
                this.c.a("ScoreEntry", "MoveToGroupPlay", "MoveToGroupPlay", 1);
                this.z = false;
                onResume();
                return;
            case R.id.top_back_btn /*2131428124*/:
                showDialog(3);
                return;
            case R.id.checkin_btn /*2131428125*/:
                showDialog(4);
                return;
            case R.id.top_scorecard_btn /*2131428126*/:
                this.c.a("/score_card");
                this.c.b("/score_card");
                Intent intent4 = new Intent(this, ScoreCard.class);
                intent4.putExtra("playing_round_id", this.v);
                intent4.putExtra("player_ids", this.s);
                intent4.putExtra("playing_tee_id", this.x);
                s a6 = this.b.a(this.x, 0);
                if (a6.getCount() <= 9) {
                    intent4.putExtra("play_nine", true);
                } else {
                    intent4.putExtra("play_nine", false);
                }
                a6.close();
                startActivityForResult(intent4, 0);
                System.gc();
                return;
            case R.id.top_analysis_btn /*2131428127*/:
                this.c.a("/score_analysis");
                this.c.b("/score_analysis");
                Intent intent5 = new Intent(this, ScoreAnalysis.class);
                intent5.putExtra("playing_round_id", this.v);
                intent5.putExtra("player_ids", this.s);
                intent5.putExtra("playing_tee_id", this.x);
                startActivity(intent5);
                return;
            case R.id.player_map_btn /*2131428131*/:
                this.c.a("/hole_map");
                this.c.b("/hole_map");
                Intent intent6 = new Intent(this, HoleMap.class);
                intent6.putExtra("playing_player_id", this.u);
                intent6.putExtra("playing_round_id", this.v);
                intent6.putExtra("playing_hole_id", this.y);
                this.b.close();
                this.b = null;
                startActivityForResult(intent6, 0);
                System.gc();
                return;
            case R.id.putt_btn /*2131428132*/:
                if (!g().booleanValue()) {
                    w a7 = this.b.a(this.v, this.y, this.u);
                    this.b.a(a7.b(), this.e.getString(R.string.club_pt));
                    a7.close();
                    a(this.u, (View) findViewById(R.id.hole_player_strokes).getParent());
                    return;
                }
                return;
            case R.id.penalty_btn /*2131428133*/:
                if (!g().booleanValue()) {
                    w a8 = this.b.a(this.v, this.y, this.u);
                    this.b.a(a8.b(), (String) null, this.e.getString(R.string.result_penalty));
                    a(this.u, (View) findViewById(R.id.hole_player_strokes).getParent());
                    a8.close();
                    return;
                }
                return;
            case R.id.shot_btn /*2131428134*/:
                if (!g().booleanValue()) {
                    showDialog(0);
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.score_entry_with_map);
        this.b = b.a(this);
        this.c = (GolfApplication) getApplication();
        this.e = getResources();
        this.z = false;
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.A = Boolean.valueOf(defaultSharedPreferences.getBoolean(getString(R.string.dialog_gps_status_key), true));
        this.E = defaultSharedPreferences.getString(getString(R.string.key_owner_measure_unit), "yard");
        this.F = defaultSharedPreferences.getInt(getString(R.string.pref_group_play_mode), 1);
        this.G = defaultSharedPreferences.getBoolean(getString(R.string.key_owner_point), false);
        Bundle extras = getIntent().getExtras();
        this.s = extras.getLongArray("player_ids");
        this.v = extras.getLong("playing_round_id");
        this.w = extras.getLong("playing_course_id");
        this.x = extras.getLong("playing_tee_id");
        this.y = extras.getLong("playing_hole_id");
        this.i = (ListView) findViewById(R.id.hole_summary);
        this.j = (ListView) findViewById(R.id.player_summary);
        this.k = (Button) findViewById(R.id.shot_btn);
        this.l = (Button) findViewById(R.id.penalty_btn);
        this.m = (Button) findViewById(R.id.putt_btn);
        this.n = (Button) findViewById(R.id.player_map_btn);
        this.o = (Button) findViewById(R.id.top_back_btn);
        this.p = (Button) findViewById(R.id.checkin_btn);
        this.q = (ImageButton) findViewById(R.id.top_analysis_btn);
        this.r = (ImageButton) findViewById(R.id.top_scorecard_btn);
        this.k.setOnClickListener(this);
        this.l.setOnClickListener(this);
        this.m.setOnClickListener(this);
        this.n.setOnClickListener(this);
        this.o.setOnClickListener(this);
        this.p.setOnClickListener(this);
        this.q.setOnClickListener(this);
        this.r.setOnClickListener(this);
        this.h = new ca(this);
        c();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        LayoutInflater from = LayoutInflater.from(this);
        switch (i2) {
            case 0:
                View inflate = from.inflate((int) R.layout.alert_dialog_select_club, (ViewGroup) null);
                a(inflate);
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle(this.e.getString(R.string.dialog_club_select_title)).setView(inflate).create();
            case 1:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle(this.e.getString(R.string.dialog_gps_off_title)).setMessage(this.e.getString(R.string.dialog_gps_off_msg)).setPositiveButton(this.e.getString(R.string.dialog_gps_setting), new cd(this)).setNegativeButton((int) R.string.cancel, new cb(this)).create();
            case 2:
                View inflate2 = from.inflate((int) R.layout.alert_dialog_edit_hole, (ViewGroup) null);
                TextView textView = (TextView) inflate2.findViewById(R.id.course_name);
                TextView textView2 = (TextView) inflate2.findViewById(R.id.hole_number);
                TextView textView3 = (TextView) inflate2.findViewById(R.id.yard_unit);
                EditText editText = (EditText) inflate2.findViewById(R.id.yard_edit);
                EditText editText2 = (EditText) inflate2.findViewById(R.id.par_edit);
                m a = this.b.a(this.w);
                s e2 = this.b.e(this.y);
                String d2 = a.d();
                String c2 = a.c();
                a.close();
                String str = (d2 == null || d2.equals(c2)) ? "" : d2;
                textView.setText(String.valueOf(str.length() > 0 ? String.valueOf(str) + "\n" : "") + c2);
                if (textView.getText().toString().length() == 0) {
                    textView.setVisibility(8);
                }
                textView2.setText("Hole #" + e2.d());
                int l2 = e2.l();
                editText2.setText(new StringBuilder().append(e2.e()).toString());
                e2.close();
                if (this.E.equals("yard")) {
                    editText.setText(new StringBuilder().append(l2).toString());
                    textView3.setText(" yards");
                } else {
                    editText.setText(new StringBuilder().append(com.asai24.golf.f.a.b(l2)).toString());
                    textView3.setText(" meters");
                }
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.dialog_edit_hole_title).setView(inflate2).setPositiveButton((int) R.string.save, new ch(this, editText, editText2)).setNegativeButton((int) R.string.cancel, new cf(this)).create();
            case 3:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle(this.e.getString(R.string.confirmation)).setMessage(this.e.getString(R.string.dialog_finish_round_msg)).setPositiveButton(this.e.getString(R.string.finish), new bt(this)).setNegativeButton((int) R.string.cancel, new bu(this)).create();
            case 4:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle(this.e.getString(R.string.confirmation)).setMessage(this.e.getString(R.string.dialog_checkin_msg)).setPositiveButton(this.e.getString(R.string.dialog_checkin), new bv(this)).setNegativeButton((int) R.string.cancel, new bw(this)).create();
            case 5:
                View inflate3 = LayoutInflater.from(this).inflate((int) R.layout.alert_dialog_text_entry, (ViewGroup) null);
                InputFilter[] inputFilterArr = {new InputFilter.LengthFilter(10), new com.asai24.golf.d.a(18)};
                this.J = (EditText) inflate3.findViewById(R.id.name_edit);
                this.J.setFilters(inputFilterArr);
                ((TextView) inflate3.findViewById(R.id.note_view)).setText(getString(R.string.dialog_edit_name_note));
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.name_dialog_title).setView(inflate3).setPositiveButton((int) R.string.ok, new Cdo(this)).setNegativeButton((int) R.string.cancel, new dp(this)).create();
            default:
                return super.onCreateDialog(i2);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(20, 2, 0, (int) R.string.menu_edit_hole).setIcon((int) R.drawable.ic_menu_edit);
        menu.add(40, 3, 0, (int) R.string.menu_input_mode).setIcon(17301586);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.b != null) {
            this.b.close();
        }
        this.b = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.c.a();
        this.c.c();
        com.asai24.golf.d.d.a(findViewById(R.id.score_entry_with_map));
        System.gc();
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 2:
                this.c.a("ScoreEntry", "EditHole", "EditHole", 1);
                showDialog(2);
                break;
            case 3:
                if (this.F == 0) {
                    this.F = 1;
                } else {
                    this.F = 0;
                }
                SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this).edit();
                edit.putInt(getString(R.string.pref_group_play_mode), this.F);
                edit.commit();
                onResume();
                break;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.d != null) {
            this.d.removeMessages(0);
        }
        if (this.g != null) {
            this.g.removeUpdates(this.h);
        }
        this.g = null;
        if (this.B != null && !this.B.isClosed()) {
            this.B.close();
            this.B = null;
        }
        this.c.b();
        super.onPause();
        System.gc();
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i2, Dialog dialog) {
        switch (i2) {
            case 5:
                this.J.setText(this.I);
                return;
            default:
                return;
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        if (this.z.booleanValue()) {
            menu.findItem(3).setVisible(false);
        } else {
            menu.findItem(3).setVisible(true);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.z.booleanValue()) {
            if (this.g == null) {
                this.g = (LocationManager) getSystemService("location");
            }
            this.g.requestLocationUpdates("gps", 0, 0.0f, this.h);
        }
        d();
        if (this.z.booleanValue()) {
            setTitle(this.e.getString(R.string.single_play_title));
            e();
            findViewById(R.id.player_map_btn).setVisibility(0);
            this.k.setVisibility(0);
            this.l.setVisibility(0);
            this.m.setVisibility(0);
        } else {
            setTitle(this.e.getString(R.string.group_play_title));
            f();
            findViewById(R.id.player_map_btn).setVisibility(4);
            this.k.setVisibility(4);
            this.l.setVisibility(4);
            this.m.setVisibility(4);
        }
        super.onResume();
        System.gc();
    }
}
