package com.asai24.golf.activity;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.asai24.golf.R;

class dj implements DialogInterface.OnClickListener {
    final /* synthetic */ HoleMap a;

    dj(HoleMap holeMap) {
        this.a = holeMap;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [android.content.Context, com.asai24.golf.activity.HoleMap] */
    public void onClick(DialogInterface dialogInterface, int i) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.a).edit();
        edit.putBoolean(this.a.getString(R.string.preference_map_warn_flg), true);
        edit.commit();
    }
}
