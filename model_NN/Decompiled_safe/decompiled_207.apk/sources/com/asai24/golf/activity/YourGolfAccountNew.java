package com.asai24.golf.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.InputFilter;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.asai24.golf.GolfApplication;
import com.asai24.golf.R;
import com.asai24.golf.c.a;
import com.asai24.golf.d.d;
import com.asai24.golf.d.i;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class YourGolfAccountNew extends GolfActivity implements View.OnClickListener, AdapterView.OnItemClickListener, Runnable {
    private Resources b;
    private GolfApplication c;
    private ListView d;
    private Button e;
    private Button f;
    private Button g;
    private Button h;
    private EditText i;
    private EditText j;
    private EditText k;
    private EditText l;
    private String m;
    private String n;
    private String o;
    private String p;
    private a q;
    private ArrayAdapter r;
    /* access modifiers changed from: private */
    public ProgressDialog s;
    /* access modifiers changed from: private */
    public int t;
    private String u;
    private String v;
    /* access modifiers changed from: private */
    public int w;
    private Handler x = new aq(this);

    private String a(String str, String str2) {
        return (str == null || str.equals("")) ? str2 : str;
    }

    private void a(int i2, String str) {
        String b2;
        switch (i2) {
            case 0:
                b2 = a(str, this.b.getString(R.string.yourgolf_account_username_summary));
                break;
            case 1:
                b2 = a(str, this.b.getString(R.string.yourgolf_account_username_confirm_summary));
                break;
            case 2:
                b2 = b(str, this.b.getString(R.string.yourgolf_account_password_summary));
                break;
            case 3:
                b2 = b(str, this.b.getString(R.string.yourgolf_account_password_confirm_summary));
                break;
            default:
                b2 = str;
                break;
        }
        ((dk) this.r.getItem(i2)).a(b2);
        this.r.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void a(String str, boolean z) {
        if (z) {
            this.w = 1;
        } else {
            this.w = 2;
        }
        d(str);
    }

    private String b(String str, String str2) {
        if (str == null || str.equals("")) {
            return str2;
        }
        char[] cArr = new char[str.length()];
        for (int i2 = 0; i2 < cArr.length; i2++) {
            cArr[i2] = '*';
        }
        return new String(cArr);
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        String str;
        String string;
        String str2;
        String str3;
        boolean z = false;
        if (i2 == 0) {
            String editable = this.i.getText().toString();
            this.m = editable;
            String str4 = this.n;
            String string2 = this.b.getString(R.string.yourgolf_account_username_key);
            str = editable;
            string = this.b.getString(R.string.yourgolf_account_username_confirm_key);
            String str5 = str4;
            str2 = string2;
            str3 = str5;
        } else {
            String editable2 = this.j.getText().toString();
            this.n = editable2;
            String str6 = this.m;
            String string3 = this.b.getString(R.string.yourgolf_account_username_confirm_key);
            str = editable2;
            string = this.b.getString(R.string.yourgolf_account_username_key);
            String str7 = str6;
            str2 = string3;
            str3 = str7;
        }
        if (!Pattern.compile("[\\w\\.\\-]+@(?:[\\w\\-]+\\.)+[\\w\\-]+").matcher(str).matches()) {
            d(this.b.getString(R.string.yourgolf_account_invalid_email));
        } else if (str3 == null || str3.equals("")) {
            z = true;
        } else if (!str.equals(str3)) {
            d(this.b.getString(R.string.yourgolf_account_email_unmatch));
        } else {
            z = true;
        }
        if (z) {
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this).edit();
            edit.putString(str2, str);
            if (str3 != null && !str3.equals("")) {
                edit.putString(string, str3);
            }
            edit.commit();
        }
        a(i2, str);
        g();
    }

    /* access modifiers changed from: private */
    public void c(int i2) {
        String str;
        String string;
        String str2;
        String str3;
        boolean z = false;
        if (i2 == 2) {
            String editable = this.k.getText().toString();
            this.o = editable;
            String str4 = this.p;
            String string2 = this.b.getString(R.string.yourgolf_account_password_key);
            str = editable;
            string = this.b.getString(R.string.yourgolf_account_password_confirm_key);
            String str5 = str4;
            str2 = string2;
            str3 = str5;
        } else {
            String editable2 = this.l.getText().toString();
            this.p = editable2;
            String str6 = this.o;
            String string3 = this.b.getString(R.string.yourgolf_account_password_confirm_key);
            str = editable2;
            string = this.b.getString(R.string.yourgolf_account_password_key);
            String str7 = str6;
            str2 = string3;
            str3 = str7;
        }
        if (!Pattern.compile("[a-zA-Z0-9_.-]{6,}").matcher(str).matches()) {
            d(this.b.getString(R.string.yourgolf_account_invalid_password));
        } else if (str3 == null || str3.equals("")) {
            z = true;
        } else if (!str.equals(str3)) {
            d(this.b.getString(R.string.yourgolf_account_password_unmatch));
        } else {
            z = true;
        }
        if (z) {
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this).edit();
            edit.putString(str2, str);
            if (str3 != null && !str3.equals("")) {
                edit.putString(string, str3);
            }
            edit.commit();
        }
        a(i2, str);
        g();
    }

    private boolean c(String str, String str2) {
        Pattern compile = Pattern.compile("[\\w\\.\\-]+@(?:[\\w\\-]+\\.)+[\\w\\-]+");
        Matcher matcher = compile.matcher(str);
        Matcher matcher2 = compile.matcher(str2);
        if (!matcher.matches() || !matcher2.matches()) {
            return false;
        }
        return str.equals(str2);
    }

    private void d() {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.m = defaultSharedPreferences.getString(this.b.getString(R.string.yourgolf_account_username_key), "");
        this.n = defaultSharedPreferences.getString(this.b.getString(R.string.yourgolf_account_username_confirm_key), "");
        this.o = defaultSharedPreferences.getString(this.b.getString(R.string.yourgolf_account_password_key), "");
        this.p = defaultSharedPreferences.getString(this.b.getString(R.string.yourgolf_account_password_confirm_key), "");
    }

    private void d(String str) {
        this.u = str;
        switch (this.w) {
            case 0:
                this.v = this.b.getString(R.string.warning);
                break;
            case 1:
                this.v = this.b.getString(R.string.success);
                break;
            case 2:
                this.v = this.b.getString(R.string.error);
                break;
        }
        showDialog(4);
    }

    private boolean d(String str, String str2) {
        Pattern compile = Pattern.compile("[a-zA-Z0-9_.-]{6,}");
        Matcher matcher = compile.matcher(str);
        Matcher matcher2 = compile.matcher(str2);
        if (!matcher.matches() || !matcher2.matches()) {
            return false;
        }
        return str.equals(str2);
    }

    private void e() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new dk(this, R.string.yourgolf_account_username, a(this.m, this.b.getString(R.string.yourgolf_account_username_summary))));
        arrayList.add(new dk(this, R.string.yourgolf_account_username_confirm, a(this.n, this.b.getString(R.string.yourgolf_account_username_confirm_summary))));
        arrayList.add(new dk(this, R.string.yourgolf_account_password, b(this.o, this.b.getString(R.string.yourgolf_account_password_summary))));
        arrayList.add(new dk(this, R.string.yourgolf_account_password_confirm, b(this.p, this.b.getString(R.string.yourgolf_account_password_confirm_summary))));
        this.r = new dr(this, this, arrayList);
        this.d.setAdapter((ListAdapter) this.r);
        this.d.setOnItemClickListener(this);
    }

    private void f() {
        this.s = new ProgressDialog(this);
        this.s.setIndeterminate(true);
        this.s.setCancelable(false);
        this.s.setMessage(this.b.getString(R.string.msg_now_loading));
        this.s.show();
        new Thread(this).start();
    }

    private void g() {
        if (!c(this.m, this.n) || !d(this.o, this.p)) {
            this.e.setBackgroundResource(R.drawable.square_long_btn_disabled);
            this.g.setBackgroundResource(R.drawable.square_btn1_disabled);
            this.h.setBackgroundResource(R.drawable.square_btn1_disabled);
            this.e.setClickable(false);
            this.g.setClickable(false);
            this.h.setClickable(false);
            return;
        }
        this.e.setBackgroundResource(R.drawable.search_nearby_btn_selector);
        this.g.setBackgroundResource(R.drawable.square_btn1_selector);
        this.h.setBackgroundResource(R.drawable.square_btn1_selector);
        this.e.setClickable(true);
        this.g.setClickable(true);
        this.h.setClickable(true);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1) {
            f();
        }
    }

    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.yourgolf_account_register /*2131428197*/:
                this.t = id;
                startActivityForResult(new Intent(this, YourGolfAccountUserAgreement.class), 0);
                return;
            case R.id.yourgolf_account_cancel /*2131428198*/:
                finish();
                return;
            case R.id.yourgolf_account_login /*2131428199*/:
            case R.id.yourgolf_account_overwrite_password /*2131428200*/:
                this.t = id;
                f();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.yourgolf_account_new);
        this.c = (GolfApplication) getApplication();
        this.b = getResources();
        this.q = new a(this);
        this.d = (ListView) findViewById(R.id.yourgolf_account_list);
        this.e = (Button) findViewById(R.id.yourgolf_account_register);
        this.f = (Button) findViewById(R.id.yourgolf_account_cancel);
        this.g = (Button) findViewById(R.id.yourgolf_account_login);
        this.h = (Button) findViewById(R.id.yourgolf_account_overwrite_password);
        this.e.setOnClickListener(this);
        this.f.setOnClickListener(this);
        this.g.setOnClickListener(this);
        this.h.setOnClickListener(this);
        this.w = 0;
        this.v = "";
        d();
        e();
        g();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        InputFilter[] inputFilterArr = new InputFilter[2];
        switch (i2) {
            case 0:
                inputFilterArr[0] = new InputFilter.LengthFilter(100);
                inputFilterArr[1] = new i();
                this.i = new EditText(this);
                this.i.setText(this.m);
                this.i.setFilters(inputFilterArr);
                this.i.setInputType(32);
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.yourgolf_account_username).setView(this.i).setPositiveButton((int) R.string.ok, new at(this)).setNegativeButton((int) R.string.cancel, new ar(this)).create();
            case 1:
                inputFilterArr[0] = new InputFilter.LengthFilter(100);
                inputFilterArr[1] = new i();
                this.j = new EditText(this);
                this.j.setText(this.n);
                this.j.setFilters(inputFilterArr);
                this.j.setInputType(32);
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.yourgolf_account_username_confirm).setView(this.j).setPositiveButton((int) R.string.ok, new ag(this)).setNegativeButton((int) R.string.cancel, new ah(this)).create();
            case 2:
                inputFilterArr[0] = new InputFilter.LengthFilter(32);
                inputFilterArr[1] = new i();
                this.k = new EditText(this);
                this.k.setText(this.o);
                this.k.setFilters(inputFilterArr);
                this.k.setInputType(128);
                this.k.setTransformationMethod(PasswordTransformationMethod.getInstance());
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.yourgolf_account_password).setView(this.k).setPositiveButton((int) R.string.ok, new aj(this)).setNegativeButton((int) R.string.cancel, new ak(this)).create();
            case 3:
                inputFilterArr[0] = new InputFilter.LengthFilter(32);
                inputFilterArr[1] = new i();
                this.l = new EditText(this);
                this.l.setText(this.p);
                this.l.setFilters(inputFilterArr);
                this.l.setInputType(128);
                this.l.setTransformationMethod(PasswordTransformationMethod.getInstance());
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.yourgolf_account_password_confirm).setView(this.l).setPositiveButton((int) R.string.ok, new ae(this)).setNegativeButton((int) R.string.cancel, new af(this)).create();
            case 4:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle(this.v).setMessage(this.u).setPositiveButton((int) R.string.ok, new cw(this)).create();
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.c.a();
        this.c.c();
        d.a(findViewById(R.id.yourgolf_account_new));
        super.onDestroy();
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        switch (i2) {
            case 0:
            case 1:
            case 2:
            case 3:
                showDialog(i2);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.c.b();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i2, Dialog dialog) {
        switch (i2) {
            case 4:
                ((AlertDialog) dialog).setTitle(this.v);
                ((AlertDialog) dialog).setMessage(this.u);
                break;
        }
        super.onPrepareDialog(i2, dialog);
    }

    public void onWindowFocusChanged(boolean z) {
        int dimensionPixelSize;
        int i2 = 0;
        if (z) {
            int i3 = 0;
            while (true) {
                View childAt = this.d.getChildAt(i2);
                if (childAt == null) {
                    break;
                }
                i3 += childAt.getHeight();
                i2++;
            }
            if (i2 < 4) {
                int dimensionPixelSize2 = this.b.getDimensionPixelSize(R.dimen.yourgolf_account_list_height);
                while (i2 < 4) {
                    i3 += dimensionPixelSize2;
                    i2++;
                }
                dimensionPixelSize = i3;
            } else {
                dimensionPixelSize = this.b.getDimensionPixelSize(R.dimen.yourgolf_account_list_margin) + i3;
            }
            if (this.d.getHeight() != dimensionPixelSize) {
                this.d.getLayoutParams().height = dimensionPixelSize;
                this.d.requestLayout();
            }
        }
    }

    public void run() {
        String str = "";
        switch (this.t) {
            case R.id.yourgolf_account_register /*2131428197*/:
                str = this.q.a(this.m, this.o, 0);
                break;
            case R.id.yourgolf_account_login /*2131428199*/:
                str = this.q.a(this.m, this.o, 3);
                break;
            case R.id.yourgolf_account_overwrite_password /*2131428200*/:
                str = this.q.a(this.m, this.o, 2);
                break;
        }
        Message message = new Message();
        Bundle bundle = new Bundle();
        bundle.putString("msg", str);
        bundle.putBoolean("result", this.q.a());
        message.setData(bundle);
        this.x.sendMessage(message);
    }
}
