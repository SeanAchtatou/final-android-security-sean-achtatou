package com.asai24.golf.activity;

import android.content.DialogInterface;
import android.widget.EditText;

class ch implements DialogInterface.OnClickListener {
    final /* synthetic */ ScoreEntryWithMap a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ EditText c;

    ch(ScoreEntryWithMap scoreEntryWithMap, EditText editText, EditText editText2) {
        this.a = scoreEntryWithMap;
        this.b = editText;
        this.c = editText2;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.a.a(this.b.getText().toString().trim(), this.c.getText().toString().trim());
    }
}
