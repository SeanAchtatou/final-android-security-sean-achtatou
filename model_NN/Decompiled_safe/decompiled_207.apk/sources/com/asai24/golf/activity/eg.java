package com.asai24.golf.activity;

import android.view.View;
import com.asai24.golf.R;

class eg implements View.OnClickListener {
    final /* synthetic */ cj a;
    private final /* synthetic */ long b;
    private final /* synthetic */ String c;

    eg(cj cjVar, long j, String str) {
        this.a = cjVar;
        this.b = j;
        this.c = str;
    }

    public void onClick(View view) {
        if (this.b == 1) {
            this.a.a.a((int) R.string.cannnot_edit_owner_name);
            return;
        }
        this.a.a.q = this.b;
        this.a.a.r = this.c;
        this.a.a.showDialog(2);
    }
}
