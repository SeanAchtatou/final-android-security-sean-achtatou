package com.asai24.golf.activity;

import android.content.DialogInterface;
import android.content.Intent;
import com.asai24.golf.R;
import com.asai24.golf.a.e;
import com.asai24.golf.a.s;
import com.asai24.golf.e.b;
import com.asai24.golf.e.c;
import java.util.ArrayList;

class ez implements DialogInterface.OnClickListener {
    final /* synthetic */ SearchCourse a;

    ez(SearchCourse searchCourse) {
        this.a = searchCourse;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        int i2 = this.a.w.c() == 0 ? 2 : 1;
        e q = this.a.b.q(this.a.w.b());
        ArrayList arrayList = new ArrayList();
        while (!q.isAfterLast()) {
            s f = this.a.b.f(q.c());
            ArrayList arrayList2 = new ArrayList();
            while (!f.isAfterLast()) {
                c cVar = new c();
                cVar.a(f.d());
                cVar.b(f.f());
                cVar.d(f.g());
                cVar.c(f.l());
                cVar.f(f.i());
                cVar.e(f.j());
                cVar.a(f.m());
                cVar.b(f.n());
                arrayList2.add(cVar);
                f.moveToNext();
            }
            f.close();
            b bVar = new b();
            bVar.a(q.d());
            String e = q.e();
            if (e == null || e.equals("")) {
                bVar.a(0);
            } else {
                bVar.a(Long.parseLong(q.e()));
            }
            bVar.a(arrayList2);
            arrayList.add(bVar);
            q.moveToNext();
        }
        this.a.w.a(arrayList);
        q.close();
        this.a.c.a("/choose_tee");
        this.a.c.b("/choose_tee");
        Intent intent = new Intent(this.a, CourseDetail.class);
        intent.putExtra(this.a.d.getString(R.string.intent_course), this.a.w);
        intent.putExtra(this.a.d.getString(R.string.intent_course_mode), i2);
        this.a.startActivityForResult(intent, 0);
    }
}
