package com.asai24.golf.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.asai24.golf.R;
import java.util.List;

class ei extends ArrayAdapter {
    LayoutInflater a;
    final /* synthetic */ ScoreEntryGroup b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ei(ScoreEntryGroup scoreEntryGroup, Context context, List list) {
        super(context, 0, list);
        this.b = scoreEntryGroup;
        this.a = LayoutInflater.from(context);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.a.inflate((int) R.layout.score_entry_group_item, (ViewGroup) null) : view;
        cm cmVar = (cm) getItem(i);
        inflate.setId(i);
        inflate.setTag(Integer.valueOf(cmVar.b()));
        int a2 = cmVar.a();
        int a3 = (a2 - this.b.t) + 6;
        String string = i <= 0 ? this.b.c.getString(R.string.score_clear) : (i <= 0 || a3 > 7) ? String.valueOf((a3 - 7) + 1) + " " + this.b.s[7] : this.b.s[a3];
        TextView textView = (TextView) inflate.findViewById(R.id.score_entry_group_item2);
        ((TextView) inflate.findViewById(R.id.score_entry_group_item1)).setText(string);
        if (i > 0) {
            textView.setText(new StringBuilder().append(a2).toString());
        } else {
            textView.setText("");
        }
        if (this.b.w == i) {
            inflate.setBackgroundResource(R.drawable.gradation_lightyellow);
        } else {
            inflate.setBackgroundResource(R.drawable.gradation_yellowgreen);
        }
        inflate.setOnClickListener(cmVar);
        return inflate;
    }
}
