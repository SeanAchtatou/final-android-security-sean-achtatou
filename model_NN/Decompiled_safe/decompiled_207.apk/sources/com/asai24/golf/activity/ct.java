package com.asai24.golf.activity;

import android.content.DialogInterface;
import com.asai24.golf.R;

class ct implements DialogInterface.OnClickListener {
    final /* synthetic */ YourGolfAccountUpdate a;

    ct(YourGolfAccountUpdate yourGolfAccountUpdate) {
        this.a = yourGolfAccountUpdate;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.asai24.golf.activity.YourGolfAccountUpdate.a(com.asai24.golf.activity.YourGolfAccountUpdate, boolean):void
     arg types: [com.asai24.golf.activity.YourGolfAccountUpdate, int]
     candidates:
      com.asai24.golf.activity.YourGolfAccountUpdate.a(java.lang.String, java.lang.String):java.lang.String
      com.asai24.golf.activity.YourGolfAccountUpdate.a(int, java.lang.String):void
      com.asai24.golf.activity.YourGolfAccountUpdate.a(com.asai24.golf.activity.YourGolfAccountUpdate, int):void
      com.asai24.golf.activity.YourGolfAccountUpdate.a(com.asai24.golf.activity.YourGolfAccountUpdate, android.app.ProgressDialog):void
      com.asai24.golf.activity.YourGolfAccountUpdate.a(com.asai24.golf.activity.YourGolfAccountUpdate, java.lang.String):void
      com.asai24.golf.activity.YourGolfAccountUpdate.a(java.lang.String, boolean):void
      com.asai24.golf.activity.YourGolfAccountUpdate.a(com.asai24.golf.activity.YourGolfAccountUpdate, boolean):void */
    public void onClick(DialogInterface dialogInterface, int i) {
        if (this.a.s.equals(this.a.p.getText().toString())) {
            this.a.j.setText(this.a.b(this.a.s, this.a.b.getString(R.string.yourgolf_account_password_summary)));
            this.a.A = true;
        } else {
            this.a.d(this.a.b.getString(R.string.yourgolf_account_wrong_password));
            this.a.p.setText("");
            this.a.A = false;
        }
        this.a.i();
    }
}
