package com.asai24.golf.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.asai24.golf.R;
import java.util.List;

class eh extends ArrayAdapter {
    LayoutInflater a;
    final /* synthetic */ About b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public eh(About about, Context context, List list) {
        super(context, 0, list);
        this.b = about;
        this.a = LayoutInflater.from(context);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.a.inflate((int) R.layout.about_list_item, (ViewGroup) null) : view;
        bk bkVar = (bk) getItem(i);
        inflate.setId(i);
        ((ImageView) inflate.findViewById(R.id.favicon)).setImageResource(bkVar.a());
        ((TextView) inflate.findViewById(R.id.site_name)).setText(bkVar.b());
        inflate.setOnClickListener(bkVar);
        return inflate;
    }
}
