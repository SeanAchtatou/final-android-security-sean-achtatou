package com.asai24.golf.activity;

import android.content.DialogInterface;
import android.view.View;
import android.widget.EditText;
import com.asai24.golf.R;

class bm implements DialogInterface.OnClickListener {
    final /* synthetic */ SelectPlayers a;
    private final /* synthetic */ View b;

    bm(SelectPlayers selectPlayers, View view) {
        this.a = selectPlayers;
        this.b = view;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        EditText editText = (EditText) this.b.findViewById(R.id.name_edit);
        String editable = editText.getText().toString();
        if (editable.length() == 0) {
            this.a.a((int) R.string.invalid_input_no_value);
            return;
        }
        this.a.o.add(Long.valueOf(this.a.g.a(editable)));
        this.a.d();
        editText.setText("");
    }
}
