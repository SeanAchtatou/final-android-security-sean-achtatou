package com.asai24.golf.activity;

import android.content.Context;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;
import com.asai24.golf.R;
import com.asai24.golf.a.s;
import com.asai24.golf.f.a;

public class x extends CursorAdapter {
    View.OnClickListener a;
    private boolean b;

    public x(Context context, s sVar, View.OnClickListener onClickListener) {
        super(context, sVar);
        this.a = onClickListener;
        if (PreferenceManager.getDefaultSharedPreferences(context).getString(context.getString(R.string.key_owner_measure_unit), "yard").equals("yard")) {
            this.b = true;
        } else {
            this.b = false;
        }
    }

    public boolean areAllItemsEnabled() {
        return false;
    }

    public void bindView(View view, Context context, Cursor cursor) {
        s sVar = (s) cursor;
        String sb = new StringBuilder().append(sVar.d()).toString();
        String sb2 = new StringBuilder().append(sVar.e()).toString();
        String str = sVar.k();
        ((TextView) view.findViewById(R.id.hole_number)).setText(sb);
        TextView textView = (TextView) view.findViewById(R.id.hole_yard);
        TextView textView2 = (TextView) view.findViewById(R.id.hole_yard_label);
        int l = sVar.l();
        if (this.b) {
            textView.setText(new StringBuilder().append(l).toString());
            textView2.setText(" yards");
        } else {
            textView.setText(new StringBuilder().append(a.b(l)).toString());
            textView2.setText(" meters");
        }
        ((TextView) view.findViewById(R.id.hole_par)).setText(sb2);
        ((TextView) view.findViewById(R.id.hole_handicap)).setText(str);
    }

    public boolean isEnabled(int i) {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate((int) R.layout.hole_info_static_item, viewGroup, false);
    }
}
