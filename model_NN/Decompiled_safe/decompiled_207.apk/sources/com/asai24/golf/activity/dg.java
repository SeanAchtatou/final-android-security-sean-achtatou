package com.asai24.golf.activity;

import android.content.Context;
import android.graphics.Canvas;
import android.view.MotionEvent;
import com.asai24.golf.view.b;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;

class dg extends b {
    final /* synthetic */ HoleMap a;
    private long b;
    private int c = 0;
    private int d = 0;
    private int e = 0;
    private int f = 0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    dg(HoleMap holeMap, Context context, MapView mapView) {
        super(context, mapView);
        this.a = holeMap;
    }

    public boolean draw(Canvas canvas, MapView mapView, boolean z, long j) {
        GeoPoint myLocation;
        boolean draw = super.draw(canvas, mapView, z, j);
        if (!z && this.a.y && this.a.B && (myLocation = getMyLocation()) != null) {
            int zoomLevel = mapView.getZoomLevel();
            if (zoomLevel != this.a.x) {
                if (getMyLocation() != null) {
                    this.b = 0;
                }
                this.a.x = zoomLevel;
            }
            this.b++;
            if (this.b <= 5) {
                this.a.a(myLocation);
                this.a.n.invalidate();
            }
            if (this.b >= 10000) {
                this.b = 10;
            }
        }
        return draw;
    }

    public boolean onTouchEvent(MotionEvent motionEvent, MapView mapView) {
        GeoPoint myLocation;
        if (this.a.C) {
            switch (motionEvent.getAction()) {
                case 0:
                    this.c = (int) motionEvent.getX();
                    this.d = (int) motionEvent.getY();
                    this.e = this.c;
                    this.f = this.d;
                    break;
                case 2:
                    int x = ((int) motionEvent.getX()) - this.c;
                    int y = ((int) motionEvent.getY()) - this.d;
                    int j = (int) this.a.D;
                    if (j >= 0 && j <= 90) {
                        this.e = ((this.e + x) - ((x * j) / 90)) - ((y * j) / 90);
                        this.f = ((x * j) / 90) + ((this.f + y) - ((y * j) / 90));
                    } else if (j > 90 && j <= 180) {
                        this.e = ((this.e + x) - ((x * j) / 90)) - (((180 - j) * y) / 90);
                        this.f = ((x * (180 - j)) / 90) + ((this.f + y) - ((y * j) / 90));
                    } else if (j <= 180 || j > 270) {
                        int i = j - 180;
                        this.e = (this.e - x) + ((x * i) / 90) + (((180 - i) * y) / 90);
                        this.f = (((y * i) / 90) + (this.f - y)) - ((x * (180 - i)) / 90);
                    } else {
                        int i2 = j - 180;
                        this.e = (this.e - x) + ((x * i2) / 90) + ((y * i2) / 90);
                        this.f = (((y * i2) / 90) + (this.f - y)) - ((x * i2) / 90);
                    }
                    this.c = (int) motionEvent.getX();
                    this.d = (int) motionEvent.getY();
                    motionEvent.setLocation((float) this.e, (float) this.f);
                    break;
            }
        }
        if (this.a.y && this.a.B && (myLocation = getMyLocation()) != null) {
            switch (motionEvent.getAction()) {
                case 1:
                    this.a.a(myLocation);
                    this.a.n.invalidate();
                    break;
            }
        }
        return super.onTouchEvent(motionEvent, mapView);
    }
}
