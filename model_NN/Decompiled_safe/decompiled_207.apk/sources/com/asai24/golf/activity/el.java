package com.asai24.golf.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.asai24.golf.R;
import java.util.List;

class el extends ArrayAdapter {
    LayoutInflater a;
    final /* synthetic */ GamePointEntry b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public el(GamePointEntry gamePointEntry, Context context, List list) {
        super(context, 0, list);
        this.b = gamePointEntry;
        this.a = LayoutInflater.from(context);
    }

    private boolean a(int i, int i2) {
        switch (i) {
            case 1:
                if (i2 == this.b.w) {
                    return true;
                }
                break;
            case 2:
                if (this.b.u / 10 == i2) {
                    return true;
                }
                break;
            case 3:
                if (this.b.u % 10 == i2) {
                    return true;
                }
                break;
        }
        return false;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.a.inflate((int) R.layout.game_point_entry_item, (ViewGroup) null) : view;
        d dVar = (d) getItem(i);
        int b2 = dVar.b();
        inflate.setTag(Integer.valueOf(b2));
        int a2 = dVar.a();
        switch (b2) {
            case 1:
                inflate.setId(i * 100);
                TextView textView = (TextView) inflate.findViewById(R.id.point_entry_item);
                if (a2 != 0) {
                    textView.setText("-");
                    break;
                } else {
                    textView.setText("+");
                    break;
                }
            case 2:
                inflate.setId(i * 10);
                ((TextView) inflate.findViewById(R.id.point_entry_item)).setText(new StringBuilder().append(a2).toString());
                break;
            case 3:
                inflate.setId(i);
                ((TextView) inflate.findViewById(R.id.point_entry_item)).setText(new StringBuilder().append(a2).toString());
                break;
        }
        if (a(b2, i)) {
            inflate.setBackgroundResource(R.drawable.gradation_lightyellow);
        } else {
            inflate.setBackgroundResource(R.drawable.gradation_yellowgreen);
        }
        inflate.setOnClickListener(dVar);
        return inflate;
    }
}
