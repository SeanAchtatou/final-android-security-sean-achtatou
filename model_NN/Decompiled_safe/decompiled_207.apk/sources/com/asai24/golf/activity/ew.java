package com.asai24.golf.activity;

import android.view.View;
import android.widget.AdapterView;
import com.asai24.golf.e.d;

class ew implements AdapterView.OnItemClickListener {
    final /* synthetic */ SearchCourse a;

    ew(SearchCourse searchCourse) {
        this.a = searchCourse;
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        d dVar = (d) adapterView.getItemAtPosition(i);
        if (dVar == null) {
            view.performClick();
            return;
        }
        new cv(this.a, null).execute(dVar);
    }
}
