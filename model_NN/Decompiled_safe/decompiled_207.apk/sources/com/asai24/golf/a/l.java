package com.asai24.golf.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public abstract class l extends SQLiteOpenHelper {
    protected l(Context context) {
        super(context, "golf.db", (SQLiteDatabase.CursorFactory) null, 30);
    }

    private void b(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE players (_id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT,ownner_flag INTEGER,created INTEGER,modified INTEGER);");
    }

    private void c(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE scores (_id INTEGER PRIMARY KEY AUTOINCREMENT,player_id INTEGER,round_id INTEGER,hole_id INTEGER,hole_score INTEGER,game_score INTEGER,created INTEGER,modified INTEGER);");
    }

    private void d(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE score_details (_id INTEGER PRIMARY KEY AUTOINCREMENT,score_id INTEGER,shot_number INTEGER,gps_latitude REAL,gps_longitude REAL,club TEXT,shot_result TEXT,created INTEGER,modified INTEGER);");
    }

    private void e(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE rounds (_id INTEGER PRIMARY KEY AUTOINCREMENT,course_id INTEGER,tee_id INTEGER,result_id INTEGER,yourgolf_id TEXT,created INTEGER,modified INTEGER);");
    }

    private void f(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE courses (_id INTEGER PRIMARY KEY AUTOINCREMENT,course_name TEXT,club_name TEXT,course_oob_id INTEGER,course_yourgolf_id INTEGER,created INTEGER,modified INTEGER);");
    }

    private void g(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE tees (_id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT,tee_oob_id INTEGER,course_id INTEGER,created INTEGER,modified INTEGER);");
    }

    private void h(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE holes (_id INTEGER PRIMARY KEY AUTOINCREMENT,tee_id INTEGER,hole_number INTEGER,par INTEGER,women_par INTEGER,yard INTEGER,handicap INTEGER,women_handicap INTEGER,latitude REAL,longitude REAL,created INTEGER,modified INTEGER);");
    }

    private void i(SQLiteDatabase sQLiteDatabase) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("INSERT");
        stringBuffer.append("    INTO");
        stringBuffer.append("        COURSES(");
        stringBuffer.append("            club_name");
        stringBuffer.append("            ,course_oob_id");
        stringBuffer.append("            ,created");
        stringBuffer.append("            ,modified");
        stringBuffer.append("        )");
        stringBuffer.append("    VALUES");
        stringBuffer.append("        (");
        stringBuffer.append("            'Your Golf Course(Sample)'");
        stringBuffer.append("            ,123456");
        stringBuffer.append("            ,1239583605006");
        stringBuffer.append("            ,1239583605006");
        stringBuffer.append("        )");
        sQLiteDatabase.execSQL(stringBuffer.toString());
        StringBuffer stringBuffer2 = new StringBuffer();
        stringBuffer2.append("INSERT INTO TEES ( name, tee_oob_id, course_id, created, modified ) VALUES ('regular',46363,1,1241859989706,1241859989706)");
        sQLiteDatabase.execSQL(stringBuffer2.toString());
        StringBuffer stringBuffer3 = new StringBuffer();
        stringBuffer3.append("INSERT INTO TEES ( name, tee_oob_id, course_id, created, modified ) VALUES ('back',46362,1,1241859989706,1241859989706)");
        sQLiteDatabase.execSQL(stringBuffer3.toString());
        for (int i = 0; i < 2; i++) {
            for (int i2 = 0; i2 < 18; i2++) {
                StringBuffer stringBuffer4 = new StringBuffer();
                stringBuffer4.append("INSERT");
                stringBuffer4.append("    INTO");
                stringBuffer4.append("        HOLES(");
                stringBuffer4.append("            tee_id");
                stringBuffer4.append("            ,hole_number");
                stringBuffer4.append("            ,par");
                stringBuffer4.append("            ,women_par");
                stringBuffer4.append("            ,yard");
                stringBuffer4.append("            ,handicap");
                stringBuffer4.append("            ,women_handicap");
                stringBuffer4.append("            ,latitude");
                stringBuffer4.append("            ,longitude");
                stringBuffer4.append("            ,created");
                stringBuffer4.append("            ,modified");
                stringBuffer4.append("        )");
                stringBuffer4.append("    VALUES");
                stringBuffer4.append("        (");
                stringBuffer4.append("            " + (i + 1));
                stringBuffer4.append("            ," + (i2 + 1));
                stringBuffer4.append("            ,4");
                stringBuffer4.append("            ,4");
                stringBuffer4.append("            ,400");
                stringBuffer4.append("            ,10");
                stringBuffer4.append("            ,10");
                stringBuffer4.append("            ,35.48384187479905");
                stringBuffer4.append("            ,139.50010299682617");
                stringBuffer4.append("            ,1239583605141");
                stringBuffer4.append("            ,1239583605141");
                stringBuffer4.append("        )");
                sQLiteDatabase.execSQL(stringBuffer4.toString());
            }
        }
        StringBuffer stringBuffer5 = new StringBuffer();
        stringBuffer5.append("INSERT INTO ROUNDS (course_id, tee_id, created, modified ) VALUES (1,1,1241859985826,1241859985826)");
        sQLiteDatabase.execSQL(stringBuffer5.toString());
        StringBuffer stringBuffer6 = new StringBuffer();
        stringBuffer6.append("INSERT INTO SCORES (player_id, round_id, hole_id, hole_score, game_score, created, modified ) VALUES (1,1,1,5,0,1241859989623,1241859992405)");
        sQLiteDatabase.execSQL(stringBuffer6.toString());
        StringBuffer stringBuffer7 = new StringBuffer();
        stringBuffer7.append("INSERT INTO SCORE_DETAILS ( score_id, shot_number, gps_latitude, gps_longitude, created, modified ) VALUES (1,1,35.48249757022695,139.4997100532055,1241859989706,1241859989707)");
        sQLiteDatabase.execSQL(stringBuffer7.toString());
        StringBuffer stringBuffer8 = new StringBuffer();
        stringBuffer8.append("INSERT INTO SCORE_DETAILS ( score_id, shot_number, gps_latitude, gps_longitude, created, modified ) VALUES (1,2,35.48331769697405,139.50008153915405,1241859990176,1241859990176)");
        sQLiteDatabase.execSQL(stringBuffer8.toString());
        StringBuffer stringBuffer9 = new StringBuffer();
        stringBuffer9.append("INSERT INTO SCORE_DETAILS ( score_id, shot_number, gps_latitude, gps_longitude, created, modified ) VALUES (1,3,35.48424374214978,139.5001244544983,1241859991010,1241859991010)");
        sQLiteDatabase.execSQL(stringBuffer9.toString());
        StringBuffer stringBuffer10 = new StringBuffer();
        stringBuffer10.append("INSERT INTO SCORE_DETAILS ( score_id, shot_number, gps_latitude, gps_longitude, created, modified ) VALUES (1,4,35.48494263710419,139.50006008148193,1241859991763,1241859991763)");
        sQLiteDatabase.execSQL(stringBuffer10.toString());
        StringBuffer stringBuffer11 = new StringBuffer();
        stringBuffer11.append("INSERT INTO SCORE_DETAILS ( score_id, shot_number, gps_latitude, gps_longitude, created, modified ) VALUES (1,5,35.48514356827877,139.5000386238098,1241859992418,1241859992418)");
        sQLiteDatabase.execSQL(stringBuffer11.toString());
    }

    private void j(SQLiteDatabase sQLiteDatabase) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("UPDATE holes SET handicap = ? WHERE handicap = 0 OR handicap = -2147483648");
        sQLiteDatabase.execSQL(stringBuffer.toString(), new String[]{"-1"});
    }

    private void k(SQLiteDatabase sQLiteDatabase) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("ALTER TABLE holes ADD COLUMN women_par INTEGER");
        sQLiteDatabase.execSQL(stringBuffer.toString());
        StringBuffer stringBuffer2 = new StringBuffer();
        stringBuffer2.append("ALTER TABLE holes ADD COLUMN women_handicap INTEGER");
        sQLiteDatabase.execSQL(stringBuffer2.toString());
        StringBuffer stringBuffer3 = new StringBuffer();
        stringBuffer3.append("UPDATE holes SET women_par = par ,women_handicap = handicap");
        sQLiteDatabase.execSQL(stringBuffer3.toString());
    }

    private void l(SQLiteDatabase sQLiteDatabase) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("alter table score_details rename to temp;");
        sQLiteDatabase.execSQL(stringBuffer.toString());
        StringBuffer stringBuffer2 = new StringBuffer();
        stringBuffer2.append("CREATE TABLE score_details (_id INTEGER PRIMARY KEY AUTOINCREMENT,score_id INTEGER,shot_number INTEGER,gps_latitude REAL,gps_longitude REAL,club TEXT,shot_result TEXT,created INTEGER,modified INTEGER);");
        sQLiteDatabase.execSQL(stringBuffer2.toString());
        StringBuffer stringBuffer3 = new StringBuffer();
        stringBuffer3.append("insert into score_details select _id,score_id,shot_number,gps_latitude,gps_longitude,club,shot_result,created,modified from temp;");
        sQLiteDatabase.execSQL(stringBuffer3.toString());
        StringBuffer stringBuffer4 = new StringBuffer();
        stringBuffer4.append("drop table temp;");
        sQLiteDatabase.execSQL(stringBuffer4.toString());
    }

    private void m(SQLiteDatabase sQLiteDatabase) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("ALTER TABLE courses ADD COLUMN course_yourgolf_id INTEGER");
        sQLiteDatabase.execSQL(stringBuffer.toString());
    }

    private void n(SQLiteDatabase sQLiteDatabase) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("CREATE TABLE player_delete (");
        stringBuffer.append("    deleted_id INTEGER NOT NULL");
        stringBuffer.append("    , deleted_date INTEGER NOT NULL");
        stringBuffer.append(");");
        sQLiteDatabase.execSQL(stringBuffer.toString());
        StringBuffer stringBuffer2 = new StringBuffer();
        stringBuffer2.append("CREATE TABLE round_delete (");
        stringBuffer2.append("    deleted_id INTEGER NOT NULL");
        stringBuffer2.append("    , deleted_date INTEGER NOT NULL");
        stringBuffer2.append(");");
        sQLiteDatabase.execSQL(stringBuffer2.toString());
        StringBuffer stringBuffer3 = new StringBuffer();
        stringBuffer3.append("CREATE TABLE course_delete (");
        stringBuffer3.append("    deleted_id INTEGER NOT NULL");
        stringBuffer3.append("    , deleted_date INTEGER NOT NULL");
        stringBuffer3.append(");");
        sQLiteDatabase.execSQL(stringBuffer3.toString());
        StringBuffer stringBuffer4 = new StringBuffer();
        stringBuffer4.append("CREATE TABLE tee_delete (");
        stringBuffer4.append("    deleted_id INTEGER NOT NULL");
        stringBuffer4.append("    , deleted_date INTEGER NOT NULL");
        stringBuffer4.append(");");
        sQLiteDatabase.execSQL(stringBuffer4.toString());
        StringBuffer stringBuffer5 = new StringBuffer();
        stringBuffer5.append("CREATE TABLE hole_delete (");
        stringBuffer5.append("    deleted_id INTEGER NOT NULL");
        stringBuffer5.append("    , deleted_date INTEGER NOT NULL");
        stringBuffer5.append(");");
        sQLiteDatabase.execSQL(stringBuffer5.toString());
        StringBuffer stringBuffer6 = new StringBuffer();
        stringBuffer6.append("CREATE TABLE score_delete (");
        stringBuffer6.append("    deleted_id INTEGER NOT NULL");
        stringBuffer6.append("    , deleted_date INTEGER NOT NULL");
        stringBuffer6.append(");");
        sQLiteDatabase.execSQL(stringBuffer6.toString());
        StringBuffer stringBuffer7 = new StringBuffer();
        stringBuffer7.append("CREATE TABLE score_detail_delete (");
        stringBuffer7.append("    deleted_id INTEGER NOT NULL");
        stringBuffer7.append("    , deleted_date INTEGER NOT NULL");
        stringBuffer7.append(");");
        sQLiteDatabase.execSQL(stringBuffer7.toString());
    }

    private void o(SQLiteDatabase sQLiteDatabase) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("CREATE TRIGGER trace_player_delete AFTER DELETE ON players");
        stringBuffer.append(" FOR EACH ROW");
        stringBuffer.append(" BEGIN");
        stringBuffer.append("   INSERT INTO");
        stringBuffer.append("     player_delete");
        stringBuffer.append("     (deleted_id, deleted_date)");
        stringBuffer.append("     VALUES");
        stringBuffer.append("     (old._id, STRFTIME('%s000', DATETIME('now')))");
        stringBuffer.append("   ;");
        stringBuffer.append(" END;");
        sQLiteDatabase.execSQL(stringBuffer.toString());
        StringBuffer stringBuffer2 = new StringBuffer();
        stringBuffer2.append("CREATE TRIGGER trace_round_delete AFTER DELETE ON rounds");
        stringBuffer2.append(" FOR EACH ROW");
        stringBuffer2.append(" BEGIN");
        stringBuffer2.append("   INSERT INTO");
        stringBuffer2.append("     round_delete");
        stringBuffer2.append("     (deleted_id, deleted_date)");
        stringBuffer2.append("     VALUES");
        stringBuffer2.append("     (old._id, STRFTIME('%s000', DATETIME('now')))");
        stringBuffer2.append("   ;");
        stringBuffer2.append(" END;");
        sQLiteDatabase.execSQL(stringBuffer2.toString());
        StringBuffer stringBuffer3 = new StringBuffer();
        stringBuffer3.append("CREATE TRIGGER trace_course_delete AFTER DELETE ON courses");
        stringBuffer3.append(" FOR EACH ROW");
        stringBuffer3.append(" BEGIN");
        stringBuffer3.append("   INSERT INTO");
        stringBuffer3.append("     course_delete");
        stringBuffer3.append("     (deleted_id, deleted_date)");
        stringBuffer3.append("     VALUES");
        stringBuffer3.append("     (old._id, STRFTIME('%s', DATETIME('now')))");
        stringBuffer3.append("   ;");
        stringBuffer3.append(" END;");
        sQLiteDatabase.execSQL(stringBuffer3.toString());
        StringBuffer stringBuffer4 = new StringBuffer();
        stringBuffer4.append("CREATE TRIGGER trace_tee_delete AFTER DELETE ON tees");
        stringBuffer4.append(" FOR EACH ROW");
        stringBuffer4.append(" BEGIN");
        stringBuffer4.append("   INSERT INTO");
        stringBuffer4.append("     tee_delete");
        stringBuffer4.append("     (deleted_id, deleted_date)");
        stringBuffer4.append("     VALUES");
        stringBuffer4.append("     (old._id, STRFTIME('%s000', DATETIME('now')))");
        stringBuffer4.append("   ;");
        stringBuffer4.append(" END;");
        sQLiteDatabase.execSQL(stringBuffer4.toString());
        StringBuffer stringBuffer5 = new StringBuffer();
        stringBuffer5.append("CREATE TRIGGER trace_hole_delete AFTER DELETE ON holes");
        stringBuffer5.append(" FOR EACH ROW");
        stringBuffer5.append(" BEGIN");
        stringBuffer5.append("   INSERT INTO");
        stringBuffer5.append("     hole_delete");
        stringBuffer5.append("     (deleted_id, deleted_date)");
        stringBuffer5.append("     VALUES");
        stringBuffer5.append("     (old._id, STRFTIME('%s000', DATETIME('now')))");
        stringBuffer5.append("   ;");
        stringBuffer5.append(" END;");
        sQLiteDatabase.execSQL(stringBuffer5.toString());
        StringBuffer stringBuffer6 = new StringBuffer();
        stringBuffer6.append("CREATE TRIGGER trace_score_delete AFTER DELETE ON scores");
        stringBuffer6.append(" FOR EACH ROW");
        stringBuffer6.append(" BEGIN");
        stringBuffer6.append("   INSERT INTO");
        stringBuffer6.append("     score_delete");
        stringBuffer6.append("     (deleted_id, deleted_date)");
        stringBuffer6.append("     VALUES");
        stringBuffer6.append("     (old._id, STRFTIME('%s000', DATETIME('now')))");
        stringBuffer6.append("   ;");
        stringBuffer6.append(" END;");
        sQLiteDatabase.execSQL(stringBuffer6.toString());
        StringBuffer stringBuffer7 = new StringBuffer();
        stringBuffer7.append("CREATE TRIGGER trace_score_detail_delete AFTER DELETE ON score_details");
        stringBuffer7.append(" FOR EACH ROW");
        stringBuffer7.append(" BEGIN");
        stringBuffer7.append("   INSERT INTO");
        stringBuffer7.append("     score_detail_delete");
        stringBuffer7.append("     (deleted_id, deleted_date)");
        stringBuffer7.append("     VALUES");
        stringBuffer7.append("     (old._id, STRFTIME('%s000', DATETIME('now')))");
        stringBuffer7.append("   ;");
        stringBuffer7.append(" END;");
        sQLiteDatabase.execSQL(stringBuffer7.toString());
    }

    private void p(SQLiteDatabase sQLiteDatabase) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("ALTER TABLE scores ADD COLUMN game_score INTEGER");
        sQLiteDatabase.execSQL(stringBuffer.toString());
    }

    private void q(SQLiteDatabase sQLiteDatabase) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("ALTER TABLE rounds ADD COLUMN yourgolf_id TEXT");
        sQLiteDatabase.execSQL(stringBuffer.toString());
    }

    /* access modifiers changed from: protected */
    public void a(SQLiteDatabase sQLiteDatabase) {
        f(sQLiteDatabase);
        g(sQLiteDatabase);
        h(sQLiteDatabase);
        b(sQLiteDatabase);
        e(sQLiteDatabase);
        c(sQLiteDatabase);
        d(sQLiteDatabase);
        n(sQLiteDatabase);
        o(sQLiteDatabase);
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        b(sQLiteDatabase);
        c(sQLiteDatabase);
        d(sQLiteDatabase);
        e(sQLiteDatabase);
        f(sQLiteDatabase);
        g(sQLiteDatabase);
        h(sQLiteDatabase);
        n(sQLiteDatabase);
        o(sQLiteDatabase);
        i(sQLiteDatabase);
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (i < 27) {
            j(sQLiteDatabase);
            k(sQLiteDatabase);
        }
        if (i < 28) {
            l(sQLiteDatabase);
            m(sQLiteDatabase);
        }
        if (i < 29) {
            n(sQLiteDatabase);
            o(sQLiteDatabase);
        }
        if (i < 30) {
            p(sQLiteDatabase);
            q(sQLiteDatabase);
        }
    }
}
