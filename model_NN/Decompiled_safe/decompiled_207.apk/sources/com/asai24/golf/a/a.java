package com.asai24.golf.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;

class a implements SQLiteDatabase.CursorFactory {
    private a() {
    }

    /* synthetic */ a(a aVar) {
        this();
    }

    public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        return new y(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }
}
