package com.asai24.golf.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;

class o implements SQLiteDatabase.CursorFactory {
    private o() {
    }

    /* synthetic */ o(o oVar) {
        this();
    }

    public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        return new z(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }
}
