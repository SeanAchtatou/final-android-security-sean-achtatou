package com.asai24.golf.a;

import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.database.sqlite.SQLiteQueryBuilder;
import com.asai24.golf.GolfApplication;
import java.util.HashMap;

public class y extends SQLiteCursor {
    private static HashMap a = new HashMap();

    static {
        a.put("_id", "h1._id AS hole_id");
        a.put("tee_id", "tee_id");
        a.put("hole_number", "hole_number");
        a.put("par", "par");
        a.put("women_par", "women_par");
        a.put("yard", "yard");
        a.put("hole_score", "hole_score");
        a.put("player_id", "player_id");
        a.put("round_id", "round_id");
        a.put("game_score", "game_score");
        a.put("s1._id", "s1._id AS score_id");
    }

    public y(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        super(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }

    public static SQLiteQueryBuilder a(HashMap hashMap) {
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setProjectionMap(a);
        sQLiteQueryBuilder.setTables("holes h1 LEFT OUTER JOIN scores s1 ON (h1._id=s1.hole_id AND " + ((String) hashMap.get("SQLOnClause")) + ")");
        sQLiteQueryBuilder.setCursorFactory(new a(null));
        return sQLiteQueryBuilder;
    }

    public long a() {
        return getLong(getColumnIndexOrThrow("hole_id"));
    }

    public int b() {
        return getInt(getColumnIndexOrThrow("hole_score"));
    }

    public int c() {
        return getInt(getColumnIndexOrThrow("hole_number"));
    }

    public int d() {
        return GolfApplication.f() ? getInt(getColumnIndexOrThrow("par")) : getInt(getColumnIndexOrThrow("women_par"));
    }

    public long e() {
        return getLong(getColumnIndexOrThrow("score_id"));
    }

    public int f() {
        return getInt(getColumnIndexOrThrow("yard"));
    }

    public int g() {
        return getInt(getColumnIndexOrThrow("game_score"));
    }
}
