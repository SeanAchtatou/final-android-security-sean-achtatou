package com.asai24.golf.a;

import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.database.sqlite.SQLiteQueryBuilder;
import com.asai24.golf.GolfApplication;
import java.util.HashMap;

public class s extends SQLiteCursor {
    private static HashMap a = new HashMap();
    private static HashMap b = new HashMap();
    private static String c = "holes h1 JOIN holes h2 ON h1.tee_id = h2.tee_id AND (h1.hole_number + 1) = h2.hole_number";
    private static String d = "holes h1 JOIN holes h2 ON h1.tee_id = h2.tee_id AND (h1.hole_number - 1) = h2.hole_number";

    static {
        a.put("_id", "_id");
        a.put("tee_id", "tee_id");
        a.put("hole_number", "hole_number");
        a.put("par", "par");
        a.put("women_par", "women_par");
        a.put("handicap", "handicap");
        a.put("women_handicap", "women_handicap");
        a.put("yard", "yard");
        a.put("latitude", "latitude");
        a.put("longitude", "longitude");
        a.put("created", "created");
        a.put("modified", "modified");
        b.put("_id", "h2._id");
        b.put("tee_id", "h2.tee_id");
        b.put("hole_number", "h2.hole_number");
        b.put("par", "h2.par");
        b.put("women_par", "h2.women_par");
        b.put("handicap", "h2.handicap");
        b.put("women_handicap", "h2.women_handicap");
        b.put("yard", "h2.yard");
        b.put("latitude", "h2.latitude");
        b.put("longitude", "h2.longitude");
        b.put("created", "h2.created");
        b.put("modified", "h2.modified");
    }

    public s(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        super(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }

    public static SQLiteQueryBuilder a() {
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setProjectionMap(a);
        sQLiteQueryBuilder.setTables("holes");
        sQLiteQueryBuilder.setCursorFactory(new v(null));
        return sQLiteQueryBuilder;
    }

    public static SQLiteQueryBuilder a(long j) {
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setProjectionMap(b);
        sQLiteQueryBuilder.setTables(c);
        sQLiteQueryBuilder.setCursorFactory(new v(null));
        sQLiteQueryBuilder.appendWhere("h1._id = " + j);
        return sQLiteQueryBuilder;
    }

    public static SQLiteQueryBuilder b(long j) {
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setProjectionMap(b);
        sQLiteQueryBuilder.setTables(d);
        sQLiteQueryBuilder.setCursorFactory(new v(null));
        sQLiteQueryBuilder.appendWhere("h1._id = " + j);
        return sQLiteQueryBuilder;
    }

    public long b() {
        return getLong(getColumnIndexOrThrow("_id"));
    }

    public long c() {
        return getLong(getColumnIndexOrThrow("tee_id"));
    }

    public int d() {
        return getInt(getColumnIndexOrThrow("hole_number"));
    }

    public int e() {
        return GolfApplication.f() ? getInt(getColumnIndexOrThrow("par")) : getInt(getColumnIndexOrThrow("women_par"));
    }

    public int f() {
        return getInt(getColumnIndexOrThrow("par"));
    }

    public int g() {
        return getInt(getColumnIndexOrThrow("women_par"));
    }

    public int h() {
        return GolfApplication.f() ? getInt(getColumnIndexOrThrow("handicap")) : getInt(getColumnIndexOrThrow("women_handicap"));
    }

    public int i() {
        return getInt(getColumnIndexOrThrow("handicap"));
    }

    public int j() {
        return getInt(getColumnIndexOrThrow("women_handicap"));
    }

    public String k() {
        int i = GolfApplication.f() ? getInt(getColumnIndexOrThrow("handicap")) : getInt(getColumnIndexOrThrow("women_handicap"));
        return i == -1 ? "-" : new StringBuilder(String.valueOf(i)).toString();
    }

    public int l() {
        return getInt(getColumnIndexOrThrow("yard"));
    }

    public double m() {
        return getDouble(getColumnIndexOrThrow("latitude"));
    }

    public double n() {
        return getDouble(getColumnIndexOrThrow("longitude"));
    }

    public long o() {
        return getLong(getColumnIndexOrThrow("created"));
    }

    public long p() {
        return getLong(getColumnIndexOrThrow("modified"));
    }

    public boolean q() {
        if (isNull(getColumnIndexOrThrow("latitude")) || isNull(getColumnIndexOrThrow("longitude"))) {
            return false;
        }
        return (m() == 0.0d && n() == 0.0d) ? false : true;
    }
}
