package com.asai24.golf.a;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.text.TextUtils;
import android.util.Log;
import com.asai24.golf.GolfApplication;
import com.asai24.golf.e.c;
import com.asai24.golf.e.d;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class b extends l {
    private static b c = null;
    final String a = "0";
    final String b = "1";

    private b(Context context) {
        super(context);
    }

    public static synchronized b a(Context context) {
        b bVar;
        synchronized (b.class) {
            if (c == null) {
                Log.v("GolfDatabase", "Create new GolfDatabase");
                c = new b(context);
            } else {
                Log.v("GolfDatabase", "GolfDatabase is alread in use!!");
            }
            bVar = c;
        }
        return bVar;
    }

    private void a(SQLiteDatabase sQLiteDatabase, ArrayList arrayList) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < arrayList.size()) {
                HashMap hashMap = (HashMap) arrayList.get(i2);
                ContentValues contentValues = new ContentValues();
                contentValues.put("_id", (String) hashMap.get("id"));
                contentValues.put("club_name", (String) hashMap.get("club_name"));
                if (((String) hashMap.get("ext_type")).equals("oobgolf")) {
                    contentValues.put("course_oob_id", (String) hashMap.get("ext_id"));
                } else {
                    contentValues.put("course_yourgolf_id", (String) hashMap.get("ext_id"));
                }
                contentValues.put("club_name", (String) hashMap.get("club_name"));
                contentValues.put("course_name", (String) hashMap.get("course_name"));
                contentValues.put("modified", (String) hashMap.get("modified"));
                contentValues.put("created", (String) hashMap.get("created"));
                sQLiteDatabase.insert("courses", null, contentValues);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private void b(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS courses");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS tees");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS holes");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS players");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS rounds");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS scores");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS score_details");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS player_delete");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS round_delete");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS course_delete");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS tee_delete");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS hole_delete");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS score_delete");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS score_detail_delete");
    }

    private void b(SQLiteDatabase sQLiteDatabase, ArrayList arrayList) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < arrayList.size()) {
                HashMap hashMap = (HashMap) arrayList.get(i2);
                ContentValues contentValues = new ContentValues();
                contentValues.put("_id", (String) hashMap.get("id"));
                contentValues.put("name", (String) hashMap.get("name"));
                contentValues.put("tee_oob_id", (String) hashMap.get("ext_id"));
                contentValues.put("course_id", (String) hashMap.get("course_id"));
                contentValues.put("created", (String) hashMap.get("created"));
                contentValues.put("modified", (String) hashMap.get("modified"));
                sQLiteDatabase.insert("tees", null, contentValues);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private long c(String str) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", str);
        contentValues.put("ownner_flag", "1");
        contentValues.put("created", e());
        contentValues.put("modified", e());
        return readableDatabase.insert("players", null, contentValues);
    }

    private void c(SQLiteDatabase sQLiteDatabase, ArrayList arrayList) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < arrayList.size()) {
                HashMap hashMap = (HashMap) arrayList.get(i2);
                ContentValues contentValues = new ContentValues();
                contentValues.put("_id", (String) hashMap.get("id"));
                contentValues.put("hole_number", (String) hashMap.get("hole_number"));
                contentValues.put("yard", (String) hashMap.get("yard"));
                contentValues.put("par", (String) hashMap.get("par"));
                contentValues.put("women_par", (String) hashMap.get("women_par"));
                contentValues.put("handicap", (String) hashMap.get("handicap"));
                contentValues.put("women_handicap", (String) hashMap.get("women_handicap"));
                contentValues.put("latitude", (String) hashMap.get("lat"));
                contentValues.put("longitude", (String) hashMap.get("lng"));
                contentValues.put("tee_id", (String) hashMap.get("tee_id"));
                contentValues.put("created", (String) hashMap.get("created"));
                contentValues.put("modified", (String) hashMap.get("modified"));
                sQLiteDatabase.insert("holes", null, contentValues);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private void d(SQLiteDatabase sQLiteDatabase, ArrayList arrayList) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < arrayList.size()) {
                HashMap hashMap = (HashMap) arrayList.get(i2);
                ContentValues contentValues = new ContentValues();
                contentValues.put("_id", (String) hashMap.get("id"));
                contentValues.put("name", (String) hashMap.get("name"));
                contentValues.put("ownner_flag", (String) hashMap.get("ownner_flag"));
                contentValues.put("created", (String) hashMap.get("created"));
                contentValues.put("modified", (String) hashMap.get("modified"));
                sQLiteDatabase.insert("players", null, contentValues);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private boolean d(long j, long j2) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = w.a();
        a2.appendWhere("player_id = " + j);
        a2.appendWhere(" AND round_id <> " + j2);
        w wVar = (w) a2.query(readableDatabase, null, null, null, null, null, null);
        boolean moveToFirst = wVar.moveToFirst();
        wVar.close();
        return moveToFirst;
    }

    private Long e() {
        return Long.valueOf(System.currentTimeMillis());
    }

    private void e(SQLiteDatabase sQLiteDatabase, ArrayList arrayList) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < arrayList.size()) {
                HashMap hashMap = (HashMap) arrayList.get(i2);
                String str = (String) hashMap.get("tee_id");
                e r = r(Long.valueOf(str).longValue());
                long h = r.h();
                r.close();
                ContentValues contentValues = new ContentValues();
                contentValues.put("_id", (String) hashMap.get("id"));
                contentValues.put("tee_id", str);
                contentValues.put("course_id", Long.valueOf(h));
                contentValues.put("result_id", (String) hashMap.get("result_id"));
                if (hashMap.get("yourgolf_id") != null && !((String) hashMap.get("yourgolf_id")).equals("null")) {
                    contentValues.put("yourgolf_id", (String) hashMap.get("yourgolf_id"));
                }
                contentValues.put("created", (String) hashMap.get("created"));
                contentValues.put("modified", (String) hashMap.get("modified"));
                sQLiteDatabase.insert("rounds", null, contentValues);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private void f(SQLiteDatabase sQLiteDatabase, ArrayList arrayList) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < arrayList.size()) {
                HashMap hashMap = (HashMap) arrayList.get(i2);
                ContentValues contentValues = new ContentValues();
                contentValues.put("_id", (String) hashMap.get("id"));
                contentValues.put("hole_score", (String) hashMap.get("hole_score"));
                contentValues.put("round_id", (String) hashMap.get("round_id"));
                contentValues.put("hole_id", (String) hashMap.get("hole_id"));
                contentValues.put("player_id", (String) hashMap.get("player_id"));
                contentValues.put("game_score", (String) hashMap.get("game_score"));
                contentValues.put("created", (String) hashMap.get("created"));
                contentValues.put("modified", (String) hashMap.get("modified"));
                sQLiteDatabase.insert("scores", null, contentValues);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private void g(long j, int i) {
        getWritableDatabase().delete("score_details", "score_id = ? AND shot_number = ?", new String[]{new StringBuilder().append(j).toString(), new StringBuilder().append(i).toString()});
    }

    private void g(SQLiteDatabase sQLiteDatabase, ArrayList arrayList) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < arrayList.size()) {
                HashMap hashMap = (HashMap) arrayList.get(i2);
                ContentValues contentValues = new ContentValues();
                contentValues.put("_id", (String) hashMap.get("id"));
                contentValues.put("shot_number", (String) hashMap.get("shot_number"));
                contentValues.put("gps_latitude", (String) hashMap.get("lat"));
                contentValues.put("gps_longitude", (String) hashMap.get("lng"));
                contentValues.put("club", (String) hashMap.get("club"));
                contentValues.put("shot_result", (String) hashMap.get("shot_result"));
                contentValues.put("score_id", (String) hashMap.get("score_id"));
                contentValues.put("created", (String) hashMap.get("created"));
                contentValues.put("modified", (String) hashMap.get("modified"));
                sQLiteDatabase.insert("score_details", null, contentValues);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void A(long j) {
        getWritableDatabase().delete("holes", "tee_id = ?", new String[]{new StringBuilder().append(j).toString()});
    }

    public void B(long j) {
        getWritableDatabase().delete("scores", "_id = ?", new String[]{new StringBuilder().append(j).toString()});
    }

    public int C(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("scores s1");
        stringBuffer.append("    LEFT JOIN holes h1");
        stringBuffer.append("        ON s1.hole_id = h1._id");
        String[] strArr = {""};
        StringBuffer stringBuffer2 = new StringBuffer();
        stringBuffer2.append(" CASE");
        stringBuffer2.append("     WHEN COUNT(*) = 18 THEN 0");
        stringBuffer2.append("     WHEN COUNT(*) = 9");
        stringBuffer2.append("     AND MAX(h1.hole_number) = 9 THEN 1");
        stringBuffer2.append("     WHEN COUNT(*) = 9");
        stringBuffer2.append("     AND MIN(h1.hole_number) = 10 THEN 2");
        stringBuffer2.append("     ELSE 3");
        stringBuffer2.append(" END");
        strArr[0] = stringBuffer2.toString();
        StringBuffer stringBuffer3 = new StringBuffer();
        stringBuffer3.append("s1.player_id = 1");
        stringBuffer3.append(" AND s1.round_id = ?");
        stringBuffer3.append(" AND s1.hole_score > 0");
        Cursor query = readableDatabase.query(stringBuffer.toString(), strArr, stringBuffer3.toString(), new String[]{String.valueOf(j)}, "s1.round_id", null, null);
        int i = query.moveToFirst() ? query.getInt(0) : 3;
        query.close();
        return i;
    }

    public m D(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = m.a();
        a2.appendWhere("modified > " + j);
        m mVar = (m) a2.query(readableDatabase, null, null, null, null, null, null);
        mVar.moveToFirst();
        return mVar;
    }

    public e E(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder b2 = e.b();
        b2.appendWhere(" AND t1.modified > " + j);
        e eVar = (e) b2.query(readableDatabase, null, null, null, null, null, null);
        eVar.moveToFirst();
        return eVar;
    }

    public s F(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = s.a();
        a2.appendWhere("modified > " + j);
        s sVar = (s) a2.query(readableDatabase, null, null, null, null, null, null);
        sVar.moveToFirst();
        return sVar;
    }

    public p G(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = p.a();
        a2.appendWhere(" AND r1.modified > " + j);
        p pVar = (p) a2.query(readableDatabase, null, null, null, null, null, null);
        pVar.moveToFirst();
        return pVar;
    }

    public w H(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = w.a();
        a2.appendWhere("modified > " + j);
        w wVar = (w) a2.query(readableDatabase, null, null, null, null, null, null);
        wVar.moveToFirst();
        return wVar;
    }

    public g I(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = g.a();
        a2.appendWhere("modified > " + j);
        g gVar = (g) a2.query(readableDatabase, null, null, null, null, null, null);
        gVar.moveToFirst();
        return gVar;
    }

    public d J(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = d.a();
        a2.appendWhere("modified > " + j);
        d dVar = (d) a2.query(readableDatabase, null, null, null, null, null, null);
        dVar.moveToFirst();
        return dVar;
    }

    public long a(long j, int i, double d, double d2) {
        return a(j, Integer.MIN_VALUE, i, d, d2);
    }

    public long a(long j, int i, double d, double d2, String str) {
        return a(j, Integer.MIN_VALUE, i, d, d2, str, (String) null);
    }

    public long a(long j, int i, int i2, double d, double d2) {
        return a(j, i, i2, d, d2, (String) null, (String) null);
    }

    public long a(long j, int i, int i2, double d, double d2, String str, String str2) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("score_id", Long.valueOf(j));
        if (str != null && str.length() > 0) {
            contentValues.put("club", str);
        }
        if (str2 != null && str2.length() > 0) {
            contentValues.put("shot_result", str2);
        }
        contentValues.put("shot_number", Integer.valueOf(i2));
        if (!(d == -1.0d || d2 == -1.0d)) {
            contentValues.put("gps_latitude", Double.valueOf(d));
            contentValues.put("gps_longitude", Double.valueOf(d2));
        }
        contentValues.put("created", e());
        contentValues.put("modified", e());
        return getWritableDatabase().insert("score_details", null, contentValues);
    }

    public long a(long j, int i, String str) {
        return a(j, Integer.MIN_VALUE, i, -1.0d, -1.0d, str, (String) null);
    }

    public long a(long j, long j2, long j3, int i, int i2) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("round_id", Long.valueOf(j));
        contentValues.put("hole_id", Long.valueOf(j2));
        contentValues.put("player_id", Long.valueOf(j3));
        contentValues.put("hole_score", Integer.valueOf(i));
        contentValues.put("game_score", Integer.valueOf(i2));
        contentValues.put("created", e());
        contentValues.put("modified", e());
        return getWritableDatabase().insert("scores", null, contentValues);
    }

    public long a(long j, long j2, List list) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("course_id", Long.valueOf(j));
        contentValues.put("tee_id", Long.valueOf(j2));
        contentValues.put("created", e());
        contentValues.put("modified", e());
        long insert = writableDatabase.insert("rounds", null, contentValues);
        s a2 = a(j2, 1);
        long b2 = a2.b();
        a2.close();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            a(insert, b2, ((Long) it.next()).longValue(), 0, 0);
        }
        return insert;
    }

    public long a(d dVar, long j, int i) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        if (i == 1) {
            contentValues.put("course_oob_id", Long.valueOf(dVar.c()));
        } else {
            contentValues.put("course_yourgolf_id", Long.valueOf(dVar.d()));
        }
        contentValues.put("club_name", dVar.e());
        contentValues.put("course_name", dVar.f());
        contentValues.put("modified", e());
        contentValues.put("created", e());
        long insert = writableDatabase.insert("courses", null, contentValues);
        com.asai24.golf.e.b bVar = null;
        for (com.asai24.golf.e.b bVar2 : dVar.k()) {
            if (j == bVar2.a()) {
                bVar = bVar2;
            }
        }
        ContentValues contentValues2 = new ContentValues();
        contentValues2.put("tee_oob_id", Long.valueOf(bVar.a()));
        contentValues2.put("name", bVar.b());
        contentValues2.put("course_id", Long.valueOf(insert));
        contentValues2.put("modified", e());
        contentValues2.put("created", e());
        long insert2 = writableDatabase.insert("tees", null, contentValues2);
        for (c cVar : bVar.c()) {
            ContentValues contentValues3 = new ContentValues();
            contentValues3.put("tee_id", Long.valueOf(insert2));
            contentValues3.put("hole_number", Integer.valueOf(cVar.a()));
            contentValues3.put("par", Integer.valueOf(cVar.b()));
            contentValues3.put("women_par", Integer.valueOf(cVar.d()));
            contentValues3.put("yard", Integer.valueOf(cVar.c()));
            contentValues3.put("handicap", Integer.valueOf(cVar.f()));
            contentValues3.put("women_handicap", Integer.valueOf(cVar.e()));
            contentValues3.put("modified", e());
            contentValues3.put("created", e());
            writableDatabase.insert("holes", null, contentValues3);
        }
        return insert;
    }

    public long a(String str) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", str);
        contentValues.put("ownner_flag", "0");
        contentValues.put("created", e());
        contentValues.put("modified", e());
        return readableDatabase.insert("players", null, contentValues);
    }

    public Cursor a(Long l) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setTables("course_delete");
        sQLiteQueryBuilder.appendWhere("deleted_date > " + l);
        Cursor query = sQLiteQueryBuilder.query(readableDatabase, null, null, null, null, null, null);
        query.moveToFirst();
        return query;
    }

    public aa a(long j, long j2, long[] jArr) {
        aa aaVar = (aa) aa.a(j, j2, jArr).query(getReadableDatabase(), null, null, null, null, null, null);
        aaVar.moveToFirst();
        return aaVar;
    }

    public d a(List list) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = d.a();
        if (list != null) {
            ArrayList arrayList = new ArrayList();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                arrayList.add(Long.toString(((Long) it.next()).longValue()));
            }
            a2.appendWhere("_id IN (");
            a2.appendWhere(TextUtils.join(",", arrayList));
            a2.appendWhere(")");
        }
        d dVar = (d) a2.query(readableDatabase, null, null, null, null, null, null);
        dVar.moveToFirst();
        return dVar;
    }

    public e a(long j, long j2) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = e.a();
        a2.appendWhere("tee_oob_id = " + j2);
        a2.appendWhere(" AND course_id = " + j);
        e eVar = (e) a2.query(readableDatabase, null, null, null, null, null, null);
        eVar.moveToFirst();
        return eVar;
    }

    public m a() {
        m mVar = (m) m.a().query(getReadableDatabase(), null, null, null, null, null, " created DESC");
        mVar.moveToFirst();
        return mVar;
    }

    public m a(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = m.a();
        a2.appendWhere("_id = " + j);
        m mVar = (m) a2.query(readableDatabase, null, null, null, null, null, null);
        mVar.moveToFirst();
        return mVar;
    }

    public s a(long j, int i) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = s.a();
        a2.appendWhere("tee_id = " + j);
        if (i != 0) {
            a2.appendWhere(" AND hole_number = " + i);
        }
        s sVar = (s) a2.query(readableDatabase, null, null, null, null, null, null);
        sVar.moveToFirst();
        return sVar;
    }

    public w a(long j, long j2, long j3) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = w.a();
        a2.appendWhere("round_id = " + j);
        a2.appendWhere(" AND hole_id = " + j2);
        a2.appendWhere(" AND player_id = " + j3);
        w wVar = (w) a2.query(readableDatabase, null, null, null, null, null, null);
        if (wVar.getCount() == 0) {
            wVar.close();
            w g = g(a(j, j2, j3, 0, 0));
            g.moveToFirst();
            return g;
        }
        wVar.moveToFirst();
        return wVar;
    }

    public z a(long j, long j2, String str) {
        z zVar = (z) z.a(j, j2, str).query(getReadableDatabase(), null, null, null, null, null, null);
        zVar.moveToFirst();
        return zVar;
    }

    public Boolean a(long j, int i, String str, String str2) {
        boolean z = false;
        w g = g(j);
        int c2 = g.c() + 1;
        if (c2 <= 15) {
            long b2 = g.b();
            b(b2, c2);
            g m = m(b2);
            while (!m.isAfterLast()) {
                if (m.d() >= i) {
                    e(m.b(), m.d() + 1);
                }
                m.moveToNext();
            }
            m.close();
            b(b2, i, str, str2);
            z = true;
        }
        g.close();
        return z;
    }

    public void a(long j, double d, double d2) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("gps_latitude", Double.valueOf(d));
        contentValues.put("gps_longitude", Double.valueOf(d2));
        contentValues.put("modified", e());
        getWritableDatabase().update("score_details", contentValues, "_id = ?", new String[]{Long.toString(j)});
    }

    public void a(long j, double d, double d2, String str) {
        w g = g(j);
        int c2 = g.c() + 1;
        if (c2 <= 15) {
            long b2 = g.b();
            b(b2, c2);
            a(b2, c2, d, d2, str);
        }
        g.close();
    }

    public void a(long j, Integer num, Integer num2) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("yard", num);
        if (GolfApplication.f()) {
            contentValues.put("par", num2);
        } else {
            contentValues.put("women_par", num2);
        }
        contentValues.put("modified", e());
        getWritableDatabase().update("holes", contentValues, "_id = ?", new String[]{Long.toString(j)});
    }

    public void a(long j, String str) {
        w g = g(j);
        int c2 = g.c() + 1;
        if (c2 <= 15) {
            long b2 = g.b();
            b(b2, c2);
            a(b2, c2, str);
        }
        g.close();
    }

    public void a(long j, String str, String str2) {
        w g = g(j);
        int c2 = g.c() + 1;
        if (c2 <= 15) {
            long b2 = g.b();
            b(b2, c2);
            b(b2, c2, str, str2);
        }
        g.close();
    }

    public void a(ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3, ArrayList arrayList4, ArrayList arrayList5, ArrayList arrayList6, ArrayList arrayList7) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        b(writableDatabase);
        a(writableDatabase);
        a(writableDatabase, arrayList);
        b(writableDatabase, arrayList2);
        c(writableDatabase, arrayList3);
        d(writableDatabase, arrayList7);
        e(writableDatabase, arrayList4);
        f(writableDatabase, arrayList5);
        g(writableDatabase, arrayList6);
    }

    public long b(long j, int i, String str, String str2) {
        return a(j, Integer.MIN_VALUE, i, -1.0d, -1.0d, str, str2);
    }

    public long b(String str) {
        long b2;
        d d = d();
        if (d.getCount() == 0) {
            b2 = c(str);
        } else {
            b2 = d.b();
            b(b2, str);
        }
        d.close();
        return b2;
    }

    public Cursor b(Long l) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setTables("tee_delete");
        sQLiteQueryBuilder.appendWhere("deleted_date > " + l);
        Cursor query = sQLiteQueryBuilder.query(readableDatabase, null, null, null, null, null, null);
        query.moveToFirst();
        return query;
    }

    public d b() {
        d dVar = (d) d.a().query(getReadableDatabase(), null, null, null, null, null, null);
        dVar.moveToFirst();
        return dVar;
    }

    public d b(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = d.a();
        a2.appendWhere("_id = " + j);
        d dVar = (d) a2.query(readableDatabase, null, null, null, null, null, null);
        dVar.moveToFirst();
        return dVar;
    }

    public i b(long j, long j2) {
        i iVar = (i) i.a(j, j2).query(getReadableDatabase(), null, null, null, "s1.player_id", null, null);
        iVar.moveToFirst();
        return iVar;
    }

    public w b(long j, long j2, long j3) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = w.a();
        a2.appendWhere("round_id = " + j);
        a2.appendWhere(" AND hole_id = " + j2);
        a2.appendWhere(" AND player_id = " + j3);
        w wVar = (w) a2.query(readableDatabase, null, null, null, null, null, null);
        wVar.moveToFirst();
        return wVar;
    }

    public void b(long j, int i) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("hole_score", Integer.valueOf(i));
        contentValues.put("modified", e());
        getWritableDatabase().update("scores", contentValues, "_id = ?", new String[]{Long.toString(j)});
    }

    public void b(long j, String str) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", str);
        contentValues.put("modified", e());
        getWritableDatabase().update("players", contentValues, "_id = ?", new String[]{Long.toString(j)});
    }

    public void b(long j, String str, String str2) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("club", str);
        contentValues.put("shot_result", str2);
        contentValues.put("modified", e());
        getWritableDatabase().update("score_details", contentValues, "_id = ?", new String[]{Long.toString(j)});
    }

    public Cursor c(Long l) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setTables("hole_delete");
        sQLiteQueryBuilder.appendWhere("deleted_date > " + l);
        Cursor query = sQLiteQueryBuilder.query(readableDatabase, null, null, null, null, null, null);
        query.moveToFirst();
        return query;
    }

    public p c() {
        p pVar = (p) p.a().query(getReadableDatabase(), null, null, null, null, null, "r1.created DESC");
        pVar.moveToFirst();
        return pVar;
    }

    public s c(long j) {
        s sVar = (s) s.a(j).query(getReadableDatabase(), null, null, null, null, null, null);
        sVar.moveToFirst();
        return sVar;
    }

    public y c(long j, long j2, long j3) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        HashMap hashMap = new HashMap();
        hashMap.put("SQLOnClause", "round_id = " + j + " AND " + "player_id" + " = " + j2 + "  ");
        SQLiteQueryBuilder a2 = y.a(hashMap);
        a2.appendWhere("tee_id=" + j3);
        y yVar = (y) a2.query(readableDatabase, null, null, null, null, null, " hole_number ");
        yVar.moveToFirst();
        return yVar;
    }

    public void c(long j, int i) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("game_score", Integer.valueOf(i));
        contentValues.put("modified", e());
        getWritableDatabase().update("scores", contentValues, "_id = ?", new String[]{Long.toString(j)});
    }

    public void c(long j, long j2) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("created", Long.valueOf(j2));
        contentValues.put("modified", e());
        getWritableDatabase().update("rounds", contentValues, "_id = ?", new String[]{Long.toString(j)});
    }

    public void c(long j, String str) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("yourgolf_id", str);
        contentValues.put("modified", e());
        getWritableDatabase().update("rounds", contentValues, "_id = ?", new String[]{Long.toString(j)});
    }

    public synchronized void close() {
        super.close();
    }

    public long d(long j, int i) {
        return a(j, i, -1.0d, -1.0d);
    }

    public Cursor d(Long l) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setTables("round_delete");
        sQLiteQueryBuilder.appendWhere("deleted_date > " + l);
        Cursor query = sQLiteQueryBuilder.query(readableDatabase, null, null, null, null, null, null);
        query.moveToFirst();
        return query;
    }

    public d d() {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = d.a();
        a2.appendWhere("ownner_flag='1'");
        d dVar = (d) a2.query(readableDatabase, null, null, null, null, null, null);
        dVar.moveToFirst();
        return dVar;
    }

    public s d(long j) {
        s sVar = (s) s.b(j).query(getReadableDatabase(), null, null, null, null, null, null);
        sVar.moveToFirst();
        return sVar;
    }

    public Cursor e(Long l) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setTables("score_delete");
        sQLiteQueryBuilder.appendWhere("deleted_date > " + l);
        Cursor query = sQLiteQueryBuilder.query(readableDatabase, null, null, null, null, null, null);
        query.moveToFirst();
        return query;
    }

    public s e(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = s.a();
        a2.appendWhere("_id = " + j);
        s sVar = (s) a2.query(readableDatabase, null, null, null, null, null, null);
        sVar.moveToFirst();
        return sVar;
    }

    public void e(long j, int i) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("shot_number", Integer.valueOf(i));
        contentValues.put("modified", e());
        getWritableDatabase().update("score_details", contentValues, "_id = ?", new String[]{Long.toString(j)});
    }

    public Cursor f(Long l) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setTables("score_detail_delete");
        sQLiteQueryBuilder.appendWhere("deleted_date > " + l);
        Cursor query = sQLiteQueryBuilder.query(readableDatabase, null, null, null, null, null, null);
        query.moveToFirst();
        return query;
    }

    public s f(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = s.a();
        a2.appendWhere("tee_id = " + j);
        s sVar = (s) a2.query(readableDatabase, null, null, null, null, null, null);
        sVar.moveToFirst();
        return sVar;
    }

    public void f(long j, int i) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("result_id", Integer.valueOf(i));
        contentValues.put("modified", e());
        getWritableDatabase().update("rounds", contentValues, "_id = ?", new String[]{Long.toString(j)});
    }

    public Cursor g(Long l) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setTables("player_delete");
        sQLiteQueryBuilder.appendWhere("deleted_date > " + l);
        Cursor query = sQLiteQueryBuilder.query(readableDatabase, null, null, null, null, null, null);
        query.moveToFirst();
        return query;
    }

    public w g(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = w.a();
        a2.appendWhere("_id = " + j);
        w wVar = (w) a2.query(readableDatabase, null, null, null, null, null, null);
        wVar.moveToFirst();
        return wVar;
    }

    public w h(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = w.a();
        a2.appendWhere("round_id = " + j);
        w wVar = (w) a2.query(readableDatabase, null, null, null, null, null, null);
        wVar.moveToFirst();
        return wVar;
    }

    public void i(long j) {
        w g = g(j);
        int c2 = g.c() + 1;
        if (c2 <= 15) {
            long b2 = g.b();
            b(b2, c2);
            d(b2, c2);
        }
        g.close();
    }

    public long j(long j) {
        int c2;
        w g = g(j);
        if (g.c() == 0) {
            c2 = 0;
        } else {
            c2 = g.c() - 1;
            g(j, c2 + 1);
            b(g.b(), c2);
        }
        g.close();
        return (long) c2;
    }

    public void k(long j) {
        getWritableDatabase().delete("score_details", "_id = ?", new String[]{new StringBuilder().append(j).toString()});
    }

    public void l(long j) {
        getWritableDatabase().delete("score_details", "score_id = ?", new String[]{new StringBuilder().append(j).toString()});
    }

    public g m(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = g.a();
        a2.appendWhere("score_id = " + j);
        g gVar = (g) a2.query(readableDatabase, null, null, null, null, null, "shot_number");
        gVar.moveToFirst();
        return gVar;
    }

    public p n(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = p.a();
        a2.appendWhere(" AND r1.created >= " + j);
        p pVar = (p) a2.query(readableDatabase, null, null, null, null, null, "r1.created DESC");
        pVar.moveToFirst();
        return pVar;
    }

    public p o(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        p pVar = (p) p.a().query(readableDatabase, null, "r1._id = ?", new String[]{new StringBuilder().append(j).toString()}, null, null, null);
        pVar.moveToFirst();
        return pVar;
    }

    public void p(long j) {
        getWritableDatabase().delete("players", "_id = ?", new String[]{new StringBuilder().append(j).toString()});
    }

    public e q(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = e.a();
        a2.appendWhere("course_id = " + j);
        e eVar = (e) a2.query(readableDatabase, null, null, null, null, null, null);
        eVar.moveToFirst();
        return eVar;
    }

    public e r(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = e.a();
        a2.appendWhere("_id = " + j);
        e eVar = (e) a2.query(readableDatabase, null, null, null, null, null, null);
        eVar.moveToFirst();
        return eVar;
    }

    public e s(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder b2 = e.b();
        b2.appendWhere(" AND t1._id = " + j);
        e eVar = (e) b2.query(readableDatabase, null, null, null, null, null, null);
        eVar.moveToFirst();
        return eVar;
    }

    public i t(long j) {
        i iVar = (i) i.a(j).query(getReadableDatabase(), null, null, null, "s1.player_id", null, null);
        iVar.moveToFirst();
        return iVar;
    }

    public long[] u(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = w.a();
        a2.appendWhere("round_id= " + j);
        w wVar = (w) a2.query(readableDatabase, null, null, null, null, null, "round_id ASC");
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < wVar.getCount(); i++) {
            wVar.moveToPosition(i);
            if (!arrayList.contains(Long.valueOf(wVar.f()))) {
                arrayList.add(Long.valueOf(wVar.f()));
            }
        }
        long[] jArr = new long[arrayList.size()];
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            jArr[i2] = ((Long) arrayList.get(i2)).longValue();
        }
        wVar.close();
        Arrays.sort(jArr);
        return jArr;
    }

    public g v(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        SQLiteQueryBuilder a2 = g.a();
        a2.appendWhere("_id = " + j);
        g gVar = (g) a2.query(readableDatabase, null, null, null, null, null, "shot_number");
        gVar.moveToFirst();
        return gVar;
    }

    public void w(long j) {
        p o = o(j);
        if (!o.moveToFirst()) {
            o.close();
            return;
        }
        long c2 = o.c();
        long d = o.d();
        x(j);
        y(c2);
        z(d);
        A(d);
        o.close();
        w h = h(j);
        ArrayList arrayList = new ArrayList();
        while (!h.isAfterLast()) {
            long b2 = h.b();
            long f = h.f();
            B(b2);
            l(b2);
            if (!arrayList.contains(Long.valueOf(f))) {
                arrayList.add(Long.valueOf(f));
                d d2 = d();
                if (f != d2.b() && !d(f, j)) {
                    p(f);
                }
                d2.close();
            }
            h.moveToNext();
        }
        h.close();
    }

    public void x(long j) {
        getWritableDatabase().delete("rounds", "_id = ?", new String[]{new StringBuilder().append(j).toString()});
    }

    public void y(long j) {
        getWritableDatabase().delete("courses", "_id = ?", new String[]{new StringBuilder().append(j).toString()});
    }

    public void z(long j) {
        getWritableDatabase().delete("tees", "_id = ?", new String[]{new StringBuilder().append(j).toString()});
    }
}
