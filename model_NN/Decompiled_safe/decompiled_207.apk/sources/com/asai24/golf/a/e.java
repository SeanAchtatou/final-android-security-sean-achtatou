package com.asai24.golf.a;

import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.database.sqlite.SQLiteQueryBuilder;
import java.util.HashMap;

public class e extends SQLiteCursor {
    private static HashMap a = new HashMap();
    private static HashMap b = new HashMap();

    static {
        a.put("_id", "_id");
        a.put("name", "name");
        a.put("tee_oob_id", "tee_oob_id");
        a.put("course_id", "course_id");
        a.put("created", "created");
        a.put("modified", "modified");
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(" CASE");
        stringBuffer.append("     WHEN c1.course_oob_id IS NOT NULL THEN '" + k.OobGolf + "'");
        stringBuffer.append("     WHEN c1.course_yourgolf_id IS NOT NULL THEN '" + k.YourGolf + "'");
        stringBuffer.append("     ELSE NULL");
        stringBuffer.append(" END");
        b.put("_id", "t1._id AS _id");
        b.put("name", "t1.name AS name");
        b.put("tee_oob_id", "t1.tee_oob_id AS tee_oob_id");
        b.put("course_id", "t1.course_id AS course_id");
        b.put("created", "t1.created AS created");
        b.put("modified", "t1.modified AS modified");
        b.put("ext_type", ((Object) stringBuffer) + " AS " + "ext_type");
    }

    private e(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        super(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }

    /* synthetic */ e(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, e eVar) {
        this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }

    public static SQLiteQueryBuilder a() {
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setProjectionMap(a);
        sQLiteQueryBuilder.setTables("tees");
        sQLiteQueryBuilder.setCursorFactory(new c(null));
        return sQLiteQueryBuilder;
    }

    public static SQLiteQueryBuilder b() {
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setProjectionMap(b);
        sQLiteQueryBuilder.setTables("tees t1, courses c1");
        sQLiteQueryBuilder.appendWhere("t1.course_id = c1._id");
        sQLiteQueryBuilder.setCursorFactory(new c(null));
        return sQLiteQueryBuilder;
    }

    public long c() {
        return getLong(getColumnIndexOrThrow("_id"));
    }

    public String d() {
        return getString(getColumnIndexOrThrow("name"));
    }

    public String e() {
        return getString(getColumnIndexOrThrow("tee_oob_id"));
    }

    public String f() {
        return getString(getColumnIndexOrThrow("tee_oob_id"));
    }

    public String g() {
        return getString(getColumnIndexOrThrow("ext_type"));
    }

    public long h() {
        return getLong(getColumnIndexOrThrow("course_id"));
    }

    public String i() {
        return getString(getColumnIndexOrThrow("created"));
    }

    public long j() {
        return getLong(getColumnIndexOrThrow("modified"));
    }
}
