package com.asai24.golf.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;

class x implements SQLiteDatabase.CursorFactory {
    private x() {
    }

    /* synthetic */ x(x xVar) {
        this();
    }

    public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        return new w(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }
}
