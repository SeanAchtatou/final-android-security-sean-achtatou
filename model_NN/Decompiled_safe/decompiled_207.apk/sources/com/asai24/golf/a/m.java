package com.asai24.golf.a;

import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.database.sqlite.SQLiteQueryBuilder;
import java.util.HashMap;

public class m extends SQLiteCursor {
    private static HashMap a = new HashMap();

    static {
        a.put("_id", "_id");
        a.put("club_name", "club_name");
        a.put("course_name", "course_name");
        a.put("course_oob_id", "course_oob_id");
        a.put("course_yourgolf_id", "course_yourgolf_id");
        a.put("created", "created");
        a.put("modified", "modified");
    }

    private m(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        super(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }

    /* synthetic */ m(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, m mVar) {
        this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }

    public static SQLiteQueryBuilder a() {
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setProjectionMap(a);
        sQLiteQueryBuilder.setTables("courses");
        sQLiteQueryBuilder.setCursorFactory(new q(null));
        return sQLiteQueryBuilder;
    }

    public long b() {
        return getLong(getColumnIndexOrThrow("_id"));
    }

    public String c() {
        String string = getString(getColumnIndexOrThrow("club_name"));
        return string == null ? "" : string;
    }

    public String d() {
        String string = getString(getColumnIndexOrThrow("course_name"));
        return string == null ? "" : string;
    }

    public String e() {
        String c = c();
        String d = d();
        return (d == null || d.equals("")) ? c : String.valueOf(c) + " - " + d;
    }

    public String f() {
        return getString(getColumnIndexOrThrow("course_oob_id"));
    }

    public String g() {
        return getString(getColumnIndexOrThrow("course_yourgolf_id"));
    }

    public String h() {
        if (f() != null) {
            return f();
        }
        if (g() != null) {
            return g();
        }
        return null;
    }

    public String i() {
        if (f() != null) {
            return k.OobGolf.toString();
        }
        if (g() != null) {
            return k.YourGolf.toString();
        }
        return null;
    }

    public long j() {
        return getLong(getColumnIndexOrThrow("created"));
    }

    public long k() {
        return getLong(getColumnIndexOrThrow("modified"));
    }
}
