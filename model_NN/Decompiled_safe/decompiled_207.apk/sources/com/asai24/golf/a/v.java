package com.asai24.golf.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;

class v implements SQLiteDatabase.CursorFactory {
    private v() {
    }

    /* synthetic */ v(v vVar) {
        this();
    }

    public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        return new s(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }
}
