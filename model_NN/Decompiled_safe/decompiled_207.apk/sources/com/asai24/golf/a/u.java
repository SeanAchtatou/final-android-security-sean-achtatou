package com.asai24.golf.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;

class u implements SQLiteDatabase.CursorFactory {
    private u() {
    }

    /* synthetic */ u(u uVar) {
        this();
    }

    public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        return new i(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }
}
