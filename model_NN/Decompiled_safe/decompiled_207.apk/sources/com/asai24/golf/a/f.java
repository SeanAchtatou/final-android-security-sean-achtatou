package com.asai24.golf.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;

class f implements SQLiteDatabase.CursorFactory {
    private f() {
    }

    /* synthetic */ f(f fVar) {
        this();
    }

    public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        return new g(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }
}
