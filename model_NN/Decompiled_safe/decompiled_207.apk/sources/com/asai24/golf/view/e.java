package com.asai24.golf.view;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DrawFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Picture;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import javax.microedition.khronos.opengles.GL;

final class e extends Canvas {
    Canvas a;
    private final Paint b = new Paint(2);

    e() {
    }

    public boolean clipPath(Path path) {
        return this.a.clipPath(path);
    }

    public boolean clipPath(Path path, Region.Op op) {
        return this.a.clipPath(path, op);
    }

    public boolean clipRect(float f, float f2, float f3, float f4) {
        return this.a.clipRect(f, f2, f3, f4);
    }

    public boolean clipRect(float f, float f2, float f3, float f4, Region.Op op) {
        return this.a.clipRect(f, f2, f3, f4, op);
    }

    public boolean clipRect(int i, int i2, int i3, int i4) {
        return this.a.clipRect(i, i2, i3, i4);
    }

    public boolean clipRect(Rect rect) {
        return this.a.clipRect(rect);
    }

    public boolean clipRect(Rect rect, Region.Op op) {
        return this.a.clipRect(rect, op);
    }

    public boolean clipRect(RectF rectF) {
        return this.a.clipRect(rectF);
    }

    public boolean clipRect(RectF rectF, Region.Op op) {
        return this.a.clipRect(rectF, op);
    }

    public boolean clipRegion(Region region) {
        return this.a.clipRegion(region);
    }

    public boolean clipRegion(Region region, Region.Op op) {
        return this.a.clipRegion(region, op);
    }

    public void concat(Matrix matrix) {
        this.a.concat(matrix);
    }

    public void drawARGB(int i, int i2, int i3, int i4) {
        this.a.drawARGB(i, i2, i3, i4);
    }

    public void drawArc(RectF rectF, float f, float f2, boolean z, Paint paint) {
        this.a.drawArc(rectF, f, f2, z, paint);
    }

    public void drawBitmap(Bitmap bitmap, float f, float f2, Paint paint) {
        Paint paint2;
        if (paint == null) {
            paint2 = this.b;
        } else {
            paint.setFilterBitmap(true);
            paint2 = paint;
        }
        this.a.drawBitmap(bitmap, f, f2, paint2);
    }

    public void drawBitmap(Bitmap bitmap, Matrix matrix, Paint paint) {
        Paint paint2;
        if (paint == null) {
            paint2 = this.b;
        } else {
            paint.setFilterBitmap(true);
            paint2 = paint;
        }
        this.a.drawBitmap(bitmap, matrix, paint2);
    }

    public void drawBitmap(Bitmap bitmap, Rect rect, Rect rect2, Paint paint) {
        Paint paint2;
        if (paint == null) {
            paint2 = this.b;
        } else {
            paint.setFilterBitmap(true);
            paint2 = paint;
        }
        this.a.drawBitmap(bitmap, rect, rect2, paint2);
    }

    public void drawBitmap(Bitmap bitmap, Rect rect, RectF rectF, Paint paint) {
        Paint paint2;
        if (paint == null) {
            paint2 = this.b;
        } else {
            paint.setFilterBitmap(true);
            paint2 = paint;
        }
        this.a.drawBitmap(bitmap, rect, rectF, paint2);
    }

    public void drawBitmap(int[] iArr, int i, int i2, int i3, int i4, int i5, int i6, boolean z, Paint paint) {
        Paint paint2;
        if (paint == null) {
            paint2 = this.b;
        } else {
            paint.setFilterBitmap(true);
            paint2 = paint;
        }
        this.a.drawBitmap(iArr, i, i2, i3, i4, i5, i6, z, paint2);
    }

    public void drawBitmapMesh(Bitmap bitmap, int i, int i2, float[] fArr, int i3, int[] iArr, int i4, Paint paint) {
        this.a.drawBitmapMesh(bitmap, i, i2, fArr, i3, iArr, i4, paint);
    }

    public void drawCircle(float f, float f2, float f3, Paint paint) {
        this.a.drawCircle(f, f2, f3, paint);
    }

    public void drawColor(int i) {
        this.a.drawColor(i);
    }

    public void drawColor(int i, PorterDuff.Mode mode) {
        this.a.drawColor(i, mode);
    }

    public void drawLine(float f, float f2, float f3, float f4, Paint paint) {
        this.a.drawLine(f, f2, f3, f4, paint);
    }

    public void drawLines(float[] fArr, int i, int i2, Paint paint) {
        this.a.drawLines(fArr, i, i2, paint);
    }

    public void drawLines(float[] fArr, Paint paint) {
        this.a.drawLines(fArr, paint);
    }

    public void drawOval(RectF rectF, Paint paint) {
        this.a.drawOval(rectF, paint);
    }

    public void drawPaint(Paint paint) {
        this.a.drawPaint(paint);
    }

    public void drawPath(Path path, Paint paint) {
        this.a.drawPath(path, paint);
    }

    public void drawPicture(Picture picture) {
        this.a.drawPicture(picture);
    }

    public void drawPicture(Picture picture, Rect rect) {
        this.a.drawPicture(picture, rect);
    }

    public void drawPicture(Picture picture, RectF rectF) {
        this.a.drawPicture(picture, rectF);
    }

    public void drawPoint(float f, float f2, Paint paint) {
        this.a.drawPoint(f, f2, paint);
    }

    public void drawPoints(float[] fArr, int i, int i2, Paint paint) {
        this.a.drawPoints(fArr, i, i2, paint);
    }

    public void drawPoints(float[] fArr, Paint paint) {
        this.a.drawPoints(fArr, paint);
    }

    public void drawPosText(String str, float[] fArr, Paint paint) {
        this.a.drawPosText(str, fArr, paint);
    }

    public void drawPosText(char[] cArr, int i, int i2, float[] fArr, Paint paint) {
        this.a.drawPosText(cArr, i, i2, fArr, paint);
    }

    public void drawRGB(int i, int i2, int i3) {
        this.a.drawRGB(i, i2, i3);
    }

    public void drawRect(float f, float f2, float f3, float f4, Paint paint) {
        this.a.drawRect(f, f2, f3, f4, paint);
    }

    public void drawRect(Rect rect, Paint paint) {
        this.a.drawRect(rect, paint);
    }

    public void drawRect(RectF rectF, Paint paint) {
        this.a.drawRect(rectF, paint);
    }

    public void drawRoundRect(RectF rectF, float f, float f2, Paint paint) {
        this.a.drawRoundRect(rectF, f, f2, paint);
    }

    public void drawText(CharSequence charSequence, int i, int i2, float f, float f2, Paint paint) {
        this.a.drawText(charSequence, i, i2, f, f2, paint);
    }

    public void drawText(String str, float f, float f2, Paint paint) {
        this.a.drawText(str, f, f2, paint);
    }

    public void drawText(String str, int i, int i2, float f, float f2, Paint paint) {
        this.a.drawText(str, i, i2, f, f2, paint);
    }

    public void drawText(char[] cArr, int i, int i2, float f, float f2, Paint paint) {
        this.a.drawText(cArr, i, i2, f, f2, paint);
    }

    public void drawTextOnPath(String str, Path path, float f, float f2, Paint paint) {
        this.a.drawTextOnPath(str, path, f, f2, paint);
    }

    public void drawTextOnPath(char[] cArr, int i, int i2, Path path, float f, float f2, Paint paint) {
        this.a.drawTextOnPath(cArr, i, i2, path, f, f2, paint);
    }

    public void drawVertices(Canvas.VertexMode vertexMode, int i, float[] fArr, int i2, float[] fArr2, int i3, int[] iArr, int i4, short[] sArr, int i5, int i6, Paint paint) {
        this.a.drawVertices(vertexMode, i, fArr, i2, fArr2, i3, iArr, i4, sArr, i5, i6, paint);
    }

    public boolean getClipBounds(Rect rect) {
        return this.a.getClipBounds(rect);
    }

    public DrawFilter getDrawFilter() {
        return this.a.getDrawFilter();
    }

    public GL getGL() {
        return this.a.getGL();
    }

    public int getHeight() {
        return this.a.getHeight();
    }

    public void getMatrix(Matrix matrix) {
        this.a.getMatrix(matrix);
    }

    public int getSaveCount() {
        return this.a.getSaveCount();
    }

    public int getWidth() {
        return this.a.getWidth();
    }

    public boolean isOpaque() {
        return this.a.isOpaque();
    }

    public boolean quickReject(float f, float f2, float f3, float f4, Canvas.EdgeType edgeType) {
        return this.a.quickReject(f, f2, f3, f4, edgeType);
    }

    public boolean quickReject(Path path, Canvas.EdgeType edgeType) {
        return this.a.quickReject(path, edgeType);
    }

    public boolean quickReject(RectF rectF, Canvas.EdgeType edgeType) {
        return this.a.quickReject(rectF, edgeType);
    }

    public void restore() {
        this.a.restore();
    }

    public void restoreToCount(int i) {
        this.a.restoreToCount(i);
    }

    public void rotate(float f) {
        this.a.rotate(f);
    }

    public int save() {
        return this.a.save();
    }

    public int save(int i) {
        return this.a.save(i);
    }

    public int saveLayer(float f, float f2, float f3, float f4, Paint paint, int i) {
        return this.a.saveLayer(f, f2, f3, f4, paint, i);
    }

    public int saveLayer(RectF rectF, Paint paint, int i) {
        return this.a.saveLayer(rectF, paint, i);
    }

    public int saveLayerAlpha(float f, float f2, float f3, float f4, int i, int i2) {
        return this.a.saveLayerAlpha(f, f2, f3, f4, i, i2);
    }

    public int saveLayerAlpha(RectF rectF, int i, int i2) {
        return this.a.saveLayerAlpha(rectF, i, i2);
    }

    public void scale(float f, float f2) {
        this.a.scale(f, f2);
    }

    public void setBitmap(Bitmap bitmap) {
        this.a.setBitmap(bitmap);
    }

    public void setDrawFilter(DrawFilter drawFilter) {
        this.a.setDrawFilter(drawFilter);
    }

    public void setMatrix(Matrix matrix) {
        this.a.setMatrix(matrix);
    }

    public void setViewport(int i, int i2) {
        this.a.setViewport(i, i2);
    }

    public void skew(float f, float f2) {
        this.a.skew(f, f2);
    }

    public void translate(float f, float f2) {
        this.a.translate(f, f2);
    }
}
