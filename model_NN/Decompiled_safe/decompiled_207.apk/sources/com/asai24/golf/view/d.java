package com.asai24.golf.view;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;

public class d extends Overlay {
    private GeoPoint a;
    private GeoPoint b;
    private Paint c;

    public d(GeoPoint geoPoint, GeoPoint geoPoint2) {
        this.a = geoPoint;
        this.b = geoPoint2;
        a();
    }

    private void a() {
        this.c = new Paint(1);
        this.c.setStyle(Paint.Style.STROKE);
        this.c.setAntiAlias(true);
        this.c.setStrokeWidth(2.0f);
        this.c.setColor(-65536);
    }

    public void draw(Canvas canvas, MapView mapView, boolean z) {
        d.super.draw(canvas, mapView, z);
        if (!z) {
            Path path = new Path();
            Projection projection = mapView.getProjection();
            Point pixels = projection.toPixels(this.a, (Point) null);
            Point pixels2 = projection.toPixels(this.b, (Point) null);
            path.moveTo((float) pixels.x, (float) pixels.y);
            path.lineTo((float) pixels2.x, (float) pixels2.y);
            canvas.drawPath(path, this.c);
        }
    }
}
