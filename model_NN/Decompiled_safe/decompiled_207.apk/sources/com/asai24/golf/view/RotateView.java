package com.asai24.golf.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

public class RotateView extends ViewGroup {
    private final e a = new e();
    private float b = 0.0f;

    public RotateView(Context context) {
        super(context);
    }

    public RotateView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void a(float f) {
        synchronized (this) {
            this.b = f;
            invalidate();
        }
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        canvas.save(1);
        canvas.rotate(-this.b, ((float) getWidth()) * 0.5f, ((float) getHeight()) * 0.5f);
        this.a.a = canvas;
        super.dispatchDraw(this.a);
        canvas.restore();
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        return super.dispatchTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int width = getWidth();
        int height = getHeight();
        int childCount = getChildCount();
        for (int i5 = 0; i5 < childCount; i5++) {
            View childAt = getChildAt(i5);
            int measuredWidth = childAt.getMeasuredWidth();
            int measuredHeight = childAt.getMeasuredHeight();
            int i6 = (width - measuredWidth) / 2;
            int i7 = (height - measuredHeight) / 2;
            childAt.layout(i6, i7, measuredWidth + i6, measuredHeight + i7);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int defaultSize = getDefaultSize(getSuggestedMinimumWidth(), i);
        int defaultSize2 = getDefaultSize(getSuggestedMinimumHeight(), i2);
        int makeMeasureSpec = defaultSize > defaultSize2 ? View.MeasureSpec.makeMeasureSpec((int) (((float) defaultSize) * 1.4142135f), 1073741824) : View.MeasureSpec.makeMeasureSpec((int) (((float) defaultSize2) * 1.4142135f), 1073741824);
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            getChildAt(i3).measure(makeMeasureSpec, makeMeasureSpec);
        }
        super.onMeasure(i, i2);
    }
}
