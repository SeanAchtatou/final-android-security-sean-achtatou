package com.asai24.golf.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class c extends Overlay {
    private GeoPoint a;
    private int b;
    private String c;
    private Paint d;
    private Paint e;
    private Paint f;
    private Paint g;
    private float h;
    private Paint.FontMetrics i;
    private float j;
    private float k = 0.0f;
    private Bitmap l;
    private int m;
    private int n;

    public c(Context context, GeoPoint geoPoint, String str, String str2, String str3, int i2) {
        this.a = geoPoint;
        this.b = i2;
        this.c = String.valueOf(str3) + ": " + str + " / " + str2;
        a();
        Resources resources = context.getResources();
        this.l = BitmapFactory.decodeResource(resources, resources.getIdentifier("marker" + (i2 + 1), "drawable", context.getPackageName()));
        this.m = this.l.getWidth() / 2;
        this.n = this.l.getHeight();
    }

    private void a() {
        this.d = new Paint(1);
        this.d.setAntiAlias(true);
        this.e = new Paint(this.d);
        this.e.setStyle(Paint.Style.STROKE);
        this.e.setStrokeWidth(0.0f);
        this.e.setColor(-16776961);
        this.f = new Paint(this.d);
        this.f.setColor(-16777216);
        this.f.setTextSize(12.0f);
        this.g = new Paint(this.d);
        this.g.setColor(-1);
        this.g.setAlpha(255);
        this.h = this.f.measureText(this.c) + 10.0f;
        this.i = this.f.getFontMetrics();
        this.j = (this.i.descent - this.i.ascent) + 10.0f;
    }

    public void a(float f2) {
        this.k = f2;
    }

    public void draw(Canvas canvas, MapView mapView, boolean z) {
        if (!z) {
            Path path = new Path();
            Point pixels = mapView.getProjection().toPixels(this.a, (Point) null);
            path.moveTo((float) pixels.x, (float) pixels.y);
            canvas.rotate(this.k, (float) pixels.x, (float) pixels.y);
            if (this.b % 2 == 0) {
                int i2 = pixels.x + 50;
                path.lineTo((float) i2, (float) pixels.y);
                canvas.drawPath(path, this.e);
                canvas.drawRoundRect(new RectF((float) i2, ((float) pixels.y) - (this.j / 2.0f), ((float) i2) + this.h, ((float) pixels.y) + (this.j / 2.0f)), 5.0f, 5.0f, this.g);
                canvas.drawText(this.c, (float) (i2 + 5), (float) (pixels.y + 5), this.f);
            } else {
                int i3 = pixels.x - 50;
                path.lineTo((float) i3, (float) pixels.y);
                canvas.drawPath(path, this.e);
                canvas.drawRoundRect(new RectF(((float) i3) - this.h, ((float) pixels.y) - (this.j / 2.0f), (float) i3, ((float) pixels.y) + (this.j / 2.0f)), 5.0f, 5.0f, this.g);
                canvas.drawText(this.c, (((float) i3) - this.h) + 5.0f, (float) (pixels.y + 5), this.f);
            }
            canvas.drawBitmap(this.l, (float) (pixels.x - this.m), (float) (pixels.y - this.n), (Paint) null);
            canvas.rotate(-this.k, (float) pixels.x, (float) pixels.y);
        }
        c.super.draw(canvas, mapView, z);
    }
}
