package com.asai24.golf.d;

import android.app.Activity;
import android.database.Cursor;
import android.provider.ContactsContract;

public class h {
    public static int a(Cursor cursor) {
        return cursor.getColumnIndexOrThrow("display_name");
    }

    public static Cursor a(Activity activity) {
        return activity.managedQuery(ContactsContract.Contacts.CONTENT_URI, null, null, null, "display_name ASC");
    }
}
