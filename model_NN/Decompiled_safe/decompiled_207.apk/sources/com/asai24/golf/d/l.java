package com.asai24.golf.d;

import android.app.Activity;
import android.database.Cursor;
import android.provider.Contacts;

public class l {
    public static int a(Cursor cursor) {
        return cursor.getColumnIndexOrThrow("name");
    }

    public static Cursor a(Activity activity) {
        return activity.managedQuery(Contacts.People.CONTENT_URI, null, null, null, "name ASC");
    }
}
