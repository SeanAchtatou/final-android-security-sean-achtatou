package com.asai24.golf.d;

import android.text.InputFilter;
import android.text.Spanned;

public class a implements InputFilter {
    private int a;

    public a(int i) {
        this.a = i;
    }

    public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        if (i3 == i4) {
            return new StringBuilder(String.valueOf(spanned.toString())).append(charSequence.toString()).toString().getBytes().length > this.a ? "" : charSequence;
        }
        String str = String.valueOf(spanned.toString().substring(0, i3).toString()) + charSequence.toString();
        int i5 = 0;
        while (str.getBytes().length > this.a) {
            str = str.substring(0, str.length() - 1);
            i5++;
        }
        return charSequence.subSequence(i, i2 - i5);
    }
}
