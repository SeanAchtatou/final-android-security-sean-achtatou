package com.asai24.golf.d;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import com.asai24.golf.R;

class j implements DialogInterface.OnClickListener {
    private final /* synthetic */ Activity a;
    private final /* synthetic */ String b;
    private final /* synthetic */ StringBuilder c;

    j(Activity activity, String str, StringBuilder sb) {
        this.a = activity;
        this.b = str;
        this.c = sb;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.a.startActivity(new Intent("android.intent.action.SENDTO", Uri.parse("mailto:" + c.a.getString(R.string.mail_your_golf))).putExtra("android.intent.extra.SUBJECT", this.b).putExtra("android.intent.extra.TEXT", this.c.toString()));
    }
}
