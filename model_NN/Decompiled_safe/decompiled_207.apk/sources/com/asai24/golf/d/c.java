package com.asai24.golf.d;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import com.asai24.golf.R;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.lang.Thread;

public class c implements Thread.UncaughtExceptionHandler {
    /* access modifiers changed from: private */
    public static Context a = null;
    private static PackageInfo b = null;
    private static ActivityManager.MemoryInfo c = new ActivityManager.MemoryInfo();
    private static final Thread.UncaughtExceptionHandler d = Thread.getDefaultUncaughtExceptionHandler();

    public static void a(Activity activity) {
        String str;
        File fileStreamPath = activity.getFileStreamPath("BUG");
        if (fileStreamPath.exists()) {
            File fileStreamPath2 = activity.getFileStreamPath("BUG.txt");
            fileStreamPath.renameTo(fileStreamPath2);
            StringBuilder sb = new StringBuilder();
            String str2 = null;
            try {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(fileStreamPath2));
                str = null;
                while (true) {
                    try {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        } else if (str == null) {
                            str = readLine;
                        } else {
                            sb.append(readLine).append("\n");
                        }
                    } catch (Exception e) {
                        Exception exc = e;
                        str2 = str;
                        e = exc;
                    }
                }
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                str = str2;
                new AlertDialog.Builder(activity).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.bug_report).setMessage((int) R.string.bug_report_message).setPositiveButton((int) R.string.bug_report_positive_button, new j(activity, str, sb)).setNegativeButton((int) R.string.cancel, new k()).show();
            }
            new AlertDialog.Builder(activity).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.bug_report).setMessage((int) R.string.bug_report_message).setPositiveButton((int) R.string.bug_report_positive_button, new j(activity, str, sb)).setNegativeButton((int) R.string.cancel, new k()).show();
        }
    }

    public static void a(Context context) {
        try {
            b = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        a = context;
        Thread.setDefaultUncaughtExceptionHandler(new c());
    }

    public static void a(Throwable th) {
        th.printStackTrace();
        try {
            PrintWriter printWriter = new PrintWriter(a.openFileOutput("BUG", 1));
            if (b != null) {
                printWriter.printf("[BUG][%s] versionName:%s, versionCode:%d\n", b.packageName, b.versionName, Integer.valueOf(b.versionCode));
            } else {
                printWriter.printf("[BUG][Unkown]\n", new Object[0]);
            }
            try {
                printWriter.printf("Runtime Memory: total: %dKB, free: %dKB, used: %dKB\n", Long.valueOf(Runtime.getRuntime().totalMemory() / 1024), Long.valueOf(Runtime.getRuntime().freeMemory() / 1024), Long.valueOf((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024));
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                ((ActivityManager) a.getSystemService("activity")).getMemoryInfo(c);
                printWriter.printf("availMem: %dKB, lowMemory: %b\n", Long.valueOf(c.availMem / 1024), Boolean.valueOf(c.lowMemory));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            printWriter.printf("DEVICE: %s\n", Build.DEVICE);
            printWriter.printf("MODEL: %s\n", Build.MODEL);
            printWriter.printf("VERSION.SDK: %s\n", Build.VERSION.SDK);
            printWriter.println("");
            th.printStackTrace(printWriter);
            printWriter.close();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    public void uncaughtException(Thread thread, Throwable th) {
        a(th);
        d.uncaughtException(thread, th);
    }
}
