package com.asai24.golf.d;

import android.text.InputFilter;
import android.text.Spanned;

public class i implements InputFilter {
    public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        return charSequence.toString().matches("^[^([^ -~｡-ﾟ]|\\s)]+$") ? charSequence : "";
    }
}
