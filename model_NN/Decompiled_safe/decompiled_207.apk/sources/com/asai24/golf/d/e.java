package com.asai24.golf.d;

import android.location.Location;
import com.google.android.maps.GeoPoint;

public class e {
    public static double a(double d, double d2, double d3, double d4) {
        float[] fArr = new float[2];
        Location.distanceBetween(d, d2, d3, d4, fArr);
        return (double) fArr[0];
    }

    public static double a(GeoPoint geoPoint, GeoPoint geoPoint2) {
        return a(((double) geoPoint.getLatitudeE6()) / 1000000.0d, ((double) geoPoint.getLongitudeE6()) / 1000000.0d, ((double) geoPoint2.getLatitudeE6()) / 1000000.0d, ((double) geoPoint2.getLongitudeE6()) / 1000000.0d);
    }

    public static GeoPoint a(GeoPoint geoPoint, double d) {
        double latitudeE6 = ((double) geoPoint.getLatitudeE6()) / 1000000.0d;
        return new GeoPoint((int) (latitudeE6 * 1000000.0d), (int) (((((double) geoPoint.getLongitudeE6()) / 1000000.0d) + (((d / 6378137.0d) * 57.29577951308232d) / Math.cos(0.017453292519943295d * latitudeE6))) * 1000000.0d));
    }
}
