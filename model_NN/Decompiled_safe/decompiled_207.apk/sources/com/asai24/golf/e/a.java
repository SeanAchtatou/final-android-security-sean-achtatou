package com.asai24.golf.e;

import android.content.Context;
import com.asai24.golf.R;
import java.util.HashMap;

public class a {
    HashMap a = new HashMap();
    HashMap b = new HashMap();
    String[] c;
    String[] d;

    public a(Context context) {
        this.c = context.getResources().getStringArray(R.array.club_value);
        this.d = context.getResources().getStringArray(R.array.club_name);
        int length = this.c.length;
        for (int i = 0; i < length; i++) {
            this.a.put(this.d[i], this.c[i]);
            this.b.put(this.c[i], this.d[i]);
        }
    }

    public String a(String str) {
        return (str == null || str.equals("")) ? "" : (String) this.a.get(str);
    }

    public String b(String str) {
        return (str == null || str.equals("")) ? "" : (String) this.b.get(str);
    }
}
