package com.google.android.apps.analytics;

import android.os.Handler;
import android.os.HandlerThread;

class j extends HandlerThread {
    private Handler a;
    /* access modifiers changed from: private */
    public final d b;
    /* access modifiers changed from: private */
    public final String c;
    /* access modifiers changed from: private */
    public final String d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public long g;
    /* access modifiers changed from: private */
    public h h;
    /* access modifiers changed from: private */
    public final Handler i;

    private j(Handler handler, d dVar, String str, String str2) {
        super("DispatcherThread");
        this.f = 30;
        this.h = null;
        this.i = handler;
        this.c = str;
        this.d = str2;
        this.b = dVar;
        this.b.a(new o(this));
    }

    private j(Handler handler, String str, String str2) {
        this(handler, new d(l.a), str, str2);
    }

    static /* synthetic */ long a(j jVar, long j) {
        long j2 = jVar.g * j;
        jVar.g = j2;
        return j2;
    }

    public void a(m[] mVarArr) {
        if (this.a != null) {
            this.a.post(new h(this, mVarArr));
        }
    }

    /* access modifiers changed from: protected */
    public void onLooperPrepared() {
        this.a = new Handler();
    }
}
