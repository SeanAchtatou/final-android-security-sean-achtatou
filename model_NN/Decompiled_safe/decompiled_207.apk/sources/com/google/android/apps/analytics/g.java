package com.google.android.apps.analytics;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import java.security.SecureRandom;
import java.util.ArrayList;

class g implements k {
    private e a;
    private int b;
    private long c;
    private long d;
    private long e;
    private int f;
    private int g;
    private boolean h;
    private SQLiteStatement i;

    public g(Context context) {
        this(context, null);
    }

    public g(Context context, String str) {
        this.i = null;
        if (str != null) {
            this.a = new e(context, str);
        } else {
            this.a = new e(context);
        }
    }

    private void f() {
        SQLiteDatabase writableDatabase = this.a.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("timestamp_previous", Long.valueOf(this.d));
        contentValues.put("timestamp_current", Long.valueOf(this.e));
        contentValues.put("visits", Integer.valueOf(this.f));
        writableDatabase.update("session", contentValues, "timestamp_first=?", new String[]{Long.toString(this.c)});
        this.h = true;
    }

    public void a(long j) {
        if (this.a.getWritableDatabase().delete("events", "event_id=" + j, null) != 0) {
            this.g--;
        }
    }

    public void a(m mVar) {
        if (this.g >= 1000) {
            Log.w("googleanalytics", "Store full. Not storing last event.");
            return;
        }
        if (!this.h) {
            f();
        }
        SQLiteDatabase writableDatabase = this.a.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("user_id", Integer.valueOf(mVar.b));
        contentValues.put("account_id", mVar.c);
        contentValues.put("random_val", Integer.valueOf((int) (Math.random() * 2.147483647E9d)));
        contentValues.put("timestamp_first", Long.valueOf(this.c));
        contentValues.put("timestamp_previous", Long.valueOf(this.d));
        contentValues.put("timestamp_current", Long.valueOf(this.e));
        contentValues.put("visits", Integer.valueOf(this.f));
        contentValues.put("category", mVar.i);
        contentValues.put("action", mVar.j);
        contentValues.put("label", mVar.k);
        contentValues.put("value", Integer.valueOf(mVar.l));
        contentValues.put("screen_width", Integer.valueOf(mVar.m));
        contentValues.put("screen_height", Integer.valueOf(mVar.n));
        if (writableDatabase.insert("events", "event_id", contentValues) != -1) {
            this.g++;
        }
    }

    public void a(String str) {
        SQLiteDatabase writableDatabase = this.a.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("referrer", str);
        writableDatabase.insert("install_referrer", null, contentValues);
    }

    public m[] a() {
        return a(1000);
    }

    public m[] a(int i2) {
        Cursor query = this.a.getReadableDatabase().query("events", null, null, null, null, null, "event_id", Integer.toString(i2));
        ArrayList arrayList = new ArrayList();
        while (query.moveToNext()) {
            arrayList.add(new m(query.getLong(0), query.getInt(1), query.getString(2), query.getInt(3), query.getInt(4), query.getInt(5), query.getInt(6), query.getInt(7), query.getString(8), query.getString(9), query.getString(10), query.getInt(11), query.getInt(12), query.getInt(13)));
        }
        query.close();
        return (m[]) arrayList.toArray(new m[arrayList.size()]);
    }

    public int b() {
        if (this.i == null) {
            this.i = this.a.getReadableDatabase().compileStatement("SELECT COUNT(*) from events");
        }
        return (int) this.i.simpleQueryForLong();
    }

    public int c() {
        return this.b;
    }

    public void d() {
        this.h = false;
        this.g = b();
        SQLiteDatabase writableDatabase = this.a.getWritableDatabase();
        Cursor query = writableDatabase.query("session", null, null, null, null, null, null);
        if (!query.moveToFirst()) {
            long currentTimeMillis = System.currentTimeMillis() / 1000;
            this.c = currentTimeMillis;
            this.d = currentTimeMillis;
            this.e = currentTimeMillis;
            this.f = 1;
            this.b = new SecureRandom().nextInt() & Integer.MAX_VALUE;
            ContentValues contentValues = new ContentValues();
            contentValues.put("timestamp_first", Long.valueOf(this.c));
            contentValues.put("timestamp_previous", Long.valueOf(this.d));
            contentValues.put("timestamp_current", Long.valueOf(this.e));
            contentValues.put("visits", Integer.valueOf(this.f));
            contentValues.put("store_id", Integer.valueOf(this.b));
            writableDatabase.insert("session", "timestamp_first", contentValues);
        } else {
            this.c = query.getLong(0);
            this.d = query.getLong(2);
            this.e = System.currentTimeMillis() / 1000;
            this.f = query.getInt(3) + 1;
            this.b = query.getInt(4);
        }
        query.close();
    }

    public String e() {
        Cursor query = this.a.getReadableDatabase().query("install_referrer", new String[]{"referrer"}, null, null, null, null, null);
        String string = query.moveToFirst() ? query.getString(0) : null;
        query.close();
        return string;
    }
}
