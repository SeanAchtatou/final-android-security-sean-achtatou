package com.google.android.apps.analytics;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;

class i {
    i() {
    }

    public static String a(m mVar, String str) {
        String str2 = "";
        if (mVar.j != null) {
            str2 = mVar.j;
        }
        if (!str2.startsWith("/")) {
            str2 = "/" + str2;
        }
        String a = a(str2);
        Locale locale = Locale.getDefault();
        StringBuilder sb = new StringBuilder();
        sb.append("/__utm.gif");
        sb.append("?utmwv=4.3");
        sb.append("&utmn=").append(mVar.d);
        sb.append("&utmcs=UTF-8");
        sb.append(String.format("&utmsr=%dx%d", Integer.valueOf(mVar.m), Integer.valueOf(mVar.n)));
        sb.append(String.format("&utmul=%s-%s", locale.getLanguage(), locale.getCountry()));
        sb.append("&utmp=").append(a);
        sb.append("&utmac=").append(mVar.c);
        sb.append("&utmcc=").append(c(mVar, str));
        return sb.toString();
    }

    private static String a(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public static String b(m mVar, String str) {
        Locale locale = Locale.getDefault();
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(String.format("5(%s*%s", mVar.i, mVar.j));
        if (mVar.k != null) {
            sb2.append("*").append(mVar.k);
        }
        sb2.append(")");
        if (mVar.l > -1) {
            sb2.append(String.format("(%d)", Integer.valueOf(mVar.l)));
        }
        sb.append("/__utm.gif");
        sb.append("?utmwv=4.3");
        sb.append("&utmn=").append(mVar.d);
        sb.append("&utmt=event");
        sb.append("&utme=").append(sb2.toString());
        sb.append("&utmcs=UTF-8");
        sb.append(String.format("&utmsr=%dx%d", Integer.valueOf(mVar.m), Integer.valueOf(mVar.n)));
        sb.append(String.format("&utmul=%s-%s", locale.getLanguage(), locale.getCountry()));
        sb.append("&utmac=").append(mVar.c);
        sb.append("&utmcc=").append(c(mVar, str));
        return sb.toString();
    }

    public static String c(m mVar, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("__utma=");
        sb.append("999").append(".");
        sb.append(mVar.b).append(".");
        sb.append(mVar.e).append(".");
        sb.append(mVar.f).append(".");
        sb.append(mVar.g).append(".");
        sb.append(mVar.h);
        if (str != null) {
            sb.append("+__utmz=");
            sb.append("999").append(".");
            sb.append(mVar.e).append(".");
            sb.append("1.1.");
            sb.append(str);
        }
        return a(sb.toString());
    }
}
