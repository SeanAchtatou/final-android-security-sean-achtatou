package com.google.android.apps.analytics;

import android.util.Log;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.message.BasicHttpRequest;

class h implements Runnable {
    final /* synthetic */ j a;
    private final LinkedList b = new LinkedList();

    public h(j jVar, m[] mVarArr) {
        this.a = jVar;
        Collections.addAll(this.b, mVarArr);
    }

    private void b() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.b.size() || i2 >= this.a.f) {
                this.a.b.a();
            } else {
                m mVar = (m) this.b.get(i2);
                BasicHttpRequest basicHttpRequest = new BasicHttpRequest("GET", "__##GOOGLEPAGEVIEW##__".equals(mVar.i) ? i.a(mVar, this.a.c) : i.b(mVar, this.a.c));
                basicHttpRequest.addHeader("Host", l.a.getHostName());
                basicHttpRequest.addHeader("User-Agent", this.a.d);
                this.a.b.a((HttpRequest) basicHttpRequest);
                i = i2 + 1;
            }
        }
        this.a.b.a();
    }

    public m a() {
        return (m) this.b.poll();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.apps.analytics.j.a(com.google.android.apps.analytics.j, long):long
     arg types: [com.google.android.apps.analytics.j, int]
     candidates:
      com.google.android.apps.analytics.j.a(com.google.android.apps.analytics.j, int):int
      com.google.android.apps.analytics.j.a(com.google.android.apps.analytics.j, com.google.android.apps.analytics.h):com.google.android.apps.analytics.h
      com.google.android.apps.analytics.j.a(com.google.android.apps.analytics.j, long):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.apps.analytics.j.b(com.google.android.apps.analytics.j, long):long
     arg types: [com.google.android.apps.analytics.j, int]
     candidates:
      com.google.android.apps.analytics.j.b(com.google.android.apps.analytics.j, int):int
      com.google.android.apps.analytics.j.b(com.google.android.apps.analytics.j, long):long */
    public void run() {
        h unused = this.a.h = this;
        int i = 0;
        while (i < 5 && this.b.size() > 0) {
            long j = 0;
            try {
                if (this.a.e == 500 || this.a.e == 503) {
                    j = (long) (Math.random() * ((double) this.a.g));
                    if (this.a.g < 256) {
                        j.a(this.a, 2L);
                    }
                } else {
                    long unused2 = this.a.g = 2L;
                }
                Thread.sleep(j * 1000);
                b();
                i++;
            } catch (InterruptedException e) {
                Log.w("googleanalytics", "Couldn't sleep.", e);
            } catch (IOException e2) {
                Log.w("googleanalytics", "Problem with socket or streams.", e2);
            } catch (HttpException e3) {
                Log.w("googleanalytics", "Problem with http streams.", e3);
            }
        }
        this.a.b.b();
        this.a.i.sendMessage(this.a.i.obtainMessage(13651479));
        h unused3 = this.a.h = (h) null;
    }
}
