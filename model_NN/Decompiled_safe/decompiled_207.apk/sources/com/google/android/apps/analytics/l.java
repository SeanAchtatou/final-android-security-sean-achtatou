package com.google.android.apps.analytics;

import android.os.Build;
import android.os.Handler;
import java.util.Locale;
import org.apache.http.HttpHost;

class l implements a {
    /* access modifiers changed from: private */
    public static final HttpHost a = new HttpHost("www.google-analytics.com", 80);
    private final String b;
    private j c;

    public l() {
        Locale locale = Locale.getDefault();
        Object[] objArr = new Object[6];
        objArr[0] = "1.0";
        objArr[1] = Build.VERSION.RELEASE;
        objArr[2] = locale.getLanguage() != null ? locale.getLanguage().toLowerCase() : "en";
        objArr[3] = locale.getCountry() != null ? locale.getCountry().toLowerCase() : "";
        objArr[4] = Build.MODEL;
        objArr[5] = Build.ID;
        this.b = String.format("GoogleAnalytics/%s (Linux; U; Android %s; %s-%s; %s; Build/%s)", objArr);
    }

    public void a() {
        if (this.c != null && this.c.getLooper() != null) {
            this.c.getLooper().quit();
        }
    }

    public void a(Handler handler, String str) {
        a();
        this.c = new j(handler, str, this.b);
        this.c.start();
    }

    public void a(m[] mVarArr) {
        this.c.a(mVarArr);
    }
}
