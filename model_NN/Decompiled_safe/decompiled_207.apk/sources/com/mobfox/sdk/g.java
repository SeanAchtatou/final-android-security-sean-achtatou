package com.mobfox.sdk;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

class g implements View.OnTouchListener {
    final /* synthetic */ MobFoxView a;
    private float b;
    private float c;

    g(MobFoxView mobFoxView) {
        this.a = mobFoxView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobfox.sdk.MobFoxView.a(com.mobfox.sdk.MobFoxView, boolean):boolean
     arg types: [com.mobfox.sdk.MobFoxView, int]
     candidates:
      com.mobfox.sdk.MobFoxView.a(com.mobfox.sdk.MobFoxView, com.mobfox.sdk.a.d):com.mobfox.sdk.a.d
      com.mobfox.sdk.MobFoxView.a(com.mobfox.sdk.MobFoxView, java.lang.Thread):java.lang.Thread
      com.mobfox.sdk.MobFoxView.a(com.mobfox.sdk.MobFoxView, boolean):boolean */
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            boolean unused = this.a.w = false;
            this.b = motionEvent.getX();
            this.c = motionEvent.getY();
        }
        if (motionEvent.getAction() == 2) {
            if (Math.abs(this.b - motionEvent.getX()) > 30.0f) {
                boolean unused2 = this.a.w = true;
            }
            if (Math.abs(this.c - motionEvent.getY()) > 30.0f) {
                boolean unused3 = this.a.w = true;
            }
            if (Log.isLoggable("MOBFOX", 3)) {
                Log.d("MOBFOX", "touchMove: " + this.a.w);
            }
            return true;
        } else if (motionEvent.getAction() != 1) {
            return false;
        } else {
            if (Log.isLoggable("MOBFOX", 3)) {
                Log.d("MOBFOX", "size x: " + motionEvent.getX());
                Log.d("MOBFOX", "getHistorySize: " + motionEvent.getHistorySize());
            }
            if (this.a.k != null && !this.a.w) {
                this.a.a();
            }
            return false;
        }
    }
}
