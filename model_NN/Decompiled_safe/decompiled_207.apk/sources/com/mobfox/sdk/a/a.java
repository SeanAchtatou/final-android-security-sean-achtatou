package com.mobfox.sdk.a;

import com.mobfox.sdk.m;

public class a {
    private String a;
    private String b;
    private m c;
    private String d;
    private double e;
    private double f;

    public String a() {
        return this.a == null ? "" : this.a;
    }

    public void a(double d2) {
        this.e = d2;
    }

    public void a(m mVar) {
        this.c = mVar;
    }

    public void a(String str) {
        this.a = str;
    }

    public String b() {
        return this.b == null ? "" : this.b;
    }

    public void b(double d2) {
        this.f = d2;
    }

    public void b(String str) {
        this.b = str;
    }

    public m c() {
        if (this.c == null) {
            this.c = m.LIVE;
        }
        return this.c;
    }

    public void c(String str) {
        this.d = str;
    }

    public String d() {
        return this.d == null ? "" : this.d;
    }

    public double e() {
        return this.e;
    }

    public double f() {
        return this.f;
    }
}
