package com.mobfox.sdk.a;

import com.mobfox.sdk.l;

public class d {
    private l a;
    private int b;
    private int c;
    private String d;
    private String e;
    private b f;
    private String g;
    private c h;
    private int i;
    private boolean j;
    private boolean k;

    public l a() {
        return this.a;
    }

    public void a(int i2) {
        this.b = i2;
    }

    public void a(b bVar) {
        this.f = bVar;
    }

    public void a(l lVar) {
        this.a = lVar;
    }

    public void a(String str) {
        this.e = str;
    }

    public void a(boolean z) {
        this.j = z;
    }

    public int b() {
        return this.b;
    }

    public void b(int i2) {
        this.c = i2;
    }

    public void b(String str) {
        this.d = str;
    }

    public void b(boolean z) {
        this.k = z;
    }

    public int c() {
        return this.c;
    }

    public void c(int i2) {
        this.i = i2;
    }

    public void c(String str) {
        this.g = str;
    }

    public String d() {
        return this.e;
    }

    public String e() {
        return this.d;
    }

    public b f() {
        return this.f;
    }

    public String g() {
        return this.g;
    }

    public int h() {
        return this.i;
    }

    public boolean i() {
        return this.k;
    }

    public String toString() {
        return "Response [refresh=" + this.i + ", type=" + this.a + ", bannerWidth=" + this.b + ", bannerHeight=" + this.c + ", text=" + this.d + ", imageUrl=" + this.e + ", clickType=" + this.f + ", clickUrl=" + this.g + ", urlType=" + this.h + ", scale=" + this.j + ", skipPreflight=" + this.k + "]";
    }
}
