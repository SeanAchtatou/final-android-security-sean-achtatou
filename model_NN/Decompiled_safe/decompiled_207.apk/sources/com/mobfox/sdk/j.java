package com.mobfox.sdk;

import android.util.Log;
import com.mobfox.sdk.a.d;

class j implements Runnable {
    final /* synthetic */ MobFoxView a;

    j(MobFoxView mobFoxView) {
        this.a = mobFoxView;
    }

    public void run() {
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "starting request thread");
        }
        try {
            d unused = this.a.k = new e().a(this.a.e());
            if (this.a.k != null) {
                if (Log.isLoggable("MOBFOX", 3)) {
                    Log.d("MOBFOX", "response received");
                }
                if (Log.isLoggable("MOBFOX", 3)) {
                    Log.d("MOBFOX", "getVisibility: " + this.a.getVisibility());
                }
                this.a.b.post(this.a.c);
            }
        } catch (k e) {
            if (this.a.v != null) {
                Log.d("MOBFOX", "notify bannerListener: " + this.a.v.getClass().getName());
                this.a.v.a(e);
            }
            Log.e("MOBFOX", e.getMessage(), e);
        }
        Thread unused2 = this.a.x = (Thread) null;
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "finishing request thread");
        }
    }
}
