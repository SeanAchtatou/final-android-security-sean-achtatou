package com.mobfox.sdk;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

class h implements LocationListener {
    final /* synthetic */ MobFoxView a;

    h(MobFoxView mobFoxView) {
        this.a = mobFoxView;
    }

    public void onLocationChanged(Location location) {
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "location is longitude: " + this.a.q + ", latitude: " + this.a.r);
        }
        this.a.o.b(location.getLatitude());
        this.a.o.a(location.getLongitude());
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
