package com.mobfox.sdk;

public class k extends Exception {
    public k() {
    }

    public k(String str) {
        super(str);
    }

    public k(String str, Throwable th) {
        super(str, th);
    }

    public k(Throwable th) {
        super(th);
    }
}
