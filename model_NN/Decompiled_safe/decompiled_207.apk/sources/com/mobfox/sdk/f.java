package com.mobfox.sdk;

import android.content.Context;
import android.widget.ViewFlipper;

class f extends ViewFlipper {
    final /* synthetic */ MobFoxView a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    f(MobFoxView mobFoxView, Context context) {
        super(context);
        this.a = mobFoxView;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        try {
            super.onDetachedFromWindow();
        } catch (IllegalArgumentException e) {
            stopFlipping();
        }
    }
}
