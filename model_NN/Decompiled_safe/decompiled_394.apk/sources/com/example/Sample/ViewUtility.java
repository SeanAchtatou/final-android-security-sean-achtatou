package com.example.Sample;

import android.content.Context;

public class ViewUtility {
    public static float getScale(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }
}
