package com.example.Sample;

import android.content.Intent;
import android.os.Bundle;
import jp.co.arttec.satbox.choice.BaseActivity;

public class SampleActivity extends BaseActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(getApplicationContext(), SATBOXActivity.class);
        intent.setFlags(268435456);
        startActivity(intent);
        finish();
    }
}
