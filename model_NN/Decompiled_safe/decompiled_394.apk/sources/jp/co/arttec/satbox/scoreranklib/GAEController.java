package jp.co.arttec.satbox.scoreranklib;

import android.app.Activity;

public interface GAEController {
    void getHighScore();

    void registScore();

    void setActivity(Activity activity);

    void setOnFinishListener(HttpCommunicationListener httpCommunicationListener);
}
