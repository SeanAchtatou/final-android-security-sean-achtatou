package jp.co.arttec.satbox.choice;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class BMRank extends Activity {
    private boolean flg_end = false;
    /* access modifiers changed from: private */
    public int intGetLevel = 35;
    private int intItem;
    private int intRankno;
    private ListView mListView;
    private int soundIds;
    private SoundPool soundPool;
    private float spVolume = 0.5f;
    private String strDatName;
    private String strItem;
    private String strItem1;
    private String strItem21;
    private String strItem22;
    private String strScrName;
    private TextView txtRankName;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.rank);
        this.mListView = (ListView) findViewById(R.id.ranklistview);
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                Intent intent = new Intent(BMRank.this, BMRank2.class);
                intent.putExtra("PlayLevel", BMRank.this.intGetLevel);
                BMRank.this.startActivity(intent);
                BMRank.this.finish();
            }
        });
        this.soundPool = new SoundPool(1, 3, 0);
        onScoreEntry();
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.soundPool != null) {
            this.soundPool.stop(this.soundIds);
            this.soundPool.release();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.soundPool.play(this.soundIds, this.spVolume, this.spVolume, 1, 0, 1.0f);
        startActivity(new Intent(this, Pen_tukami.class));
        finish();
        return true;
    }

    public synchronized boolean onTouchEvent(MotionEvent event) {
        boolean z;
        if (!this.flg_end) {
            this.flg_end = true;
            if (event.getAction() != 0) {
                z = false;
            } else {
                this.soundPool.play(this.soundIds, this.spVolume, this.spVolume, 1, 0, 1.0f);
                Intent intent = new Intent(this, BMRank2.class);
                intent.putExtra("PlayLevel", this.intGetLevel);
                startActivity(intent);
                finish();
                z = true;
            }
        } else {
            z = false;
        }
        return z;
    }

    private synchronized void onScoreEntry() {
        this.txtRankName = (TextView) findViewById(R.id.rankname);
        this.txtRankName.setText("MyRanking Top 10");
        SharedPreferences pref = getSharedPreferences("prefkey", 0);
        List<RankStrDelivery> dataList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            this.intRankno = i + 1;
            this.strItem1 = String.valueOf(pref.getInt("Score" + String.valueOf(this.intRankno), 99999));
            this.strItem = pref.getString("Data" + String.valueOf(this.intRankno), "9999/99/99 99:99:99");
            this.intItem = Integer.valueOf(this.strItem1).intValue();
            if (this.intItem < 100000) {
                this.strItem1 = String.format("%06.03f", Double.valueOf(((double) this.intItem) / 1000.0d));
            } else {
                this.strItem1 = String.format("%07.03f", Double.valueOf(((double) this.intItem) / 1000.0d));
            }
            this.strItem21 = this.strItem.substring(0, 10);
            this.strItem22 = this.strItem.substring(11, 19);
            dataList.add(new RankStrDelivery(this.intRankno, this.strItem1, this.strItem21, this.strItem22));
        }
        ((ListView) findViewById(R.id.ranklistview)).setAdapter((ListAdapter) new RankAdapter(this, R.layout.rankrow, dataList));
    }
}
