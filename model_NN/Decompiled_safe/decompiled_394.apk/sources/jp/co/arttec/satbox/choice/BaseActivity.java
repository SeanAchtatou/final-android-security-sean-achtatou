package jp.co.arttec.satbox.choice;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;

public class BaseActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(1024);
        requestWindowFeature(1);
        String titlename = String.valueOf(getTitle());
        String versionName = "";
        try {
            versionName = getPackageManager().getPackageInfo("jp.co.arttec.satbox.choice", 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
        }
        setTitle(String.valueOf(titlename) + "   Ver." + versionName);
    }
}
