package jp.co.arttec.satbox.choice;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;

public class RankAdapter extends ArrayAdapter<RankStrDelivery> {
    private LayoutInflater inflater;
    private int intRid;
    private List<RankStrDelivery> items;

    public RankAdapter(Context context, int resourceId, List<RankStrDelivery> items2) {
        super(context, resourceId, items2);
        this.items = items2;
        this.inflater = (LayoutInflater) context.getSystemService("layout_inflater");
        this.intRid = resourceId;
    }

    public List<RankStrDelivery> test() {
        return this.items;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            v = this.inflater.inflate(this.intRid, (ViewGroup) null);
        }
        RankStrDelivery hoge = this.items.get(position);
        ((TextView) v.findViewById(R.id.txtRankNo)).setText(String.valueOf(hoge.getRankNo()));
        ((TextView) v.findViewById(R.id.txtItem1)).setText(hoge.getItem1());
        ((TextView) v.findViewById(R.id.txtItem21)).setText(hoge.getItem21());
        ((TextView) v.findViewById(R.id.txtItem22)).setText(hoge.getItem22());
        return v;
    }
}
