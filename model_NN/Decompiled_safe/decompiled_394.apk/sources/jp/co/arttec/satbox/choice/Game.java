package jp.co.arttec.satbox.choice;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import jp.co.arttec.satbox.choice.Game_pen;

public class Game extends BaseActivity {
    /* access modifiers changed from: private */
    public long getTime = 0;
    /* access modifiers changed from: private */
    public int nagaretagazou = 0;
    private Game_pen.OnSettingListener onSettingListener = new Game_pen.OnSettingListener() {
        public void onSetting() {
            Game_pen view = (Game_pen) Game.this.findViewById(R.id.play_view);
            Game.this.stage_clear = view.getScore();
            Intent intent = new Intent(Game.this, Question.class);
            intent.putExtra("ngrgz", Game.this.nagaretagazou);
            intent.putExtra("Stage_Number", Game.this.stage_number);
            intent.putExtra("IntTime", Game.this.getTime);
            intent.putExtra("btn4", 99);
            Game.this.startActivityForResult(intent, 0);
            view.destroyBITMAP();
            Game.this.finish();
        }

        public void onTime() {
            Game.this.getTime = ((Game_pen) Game.this.findViewById(R.id.play_view)).getTime();
            SharedPreferences.Editor editor = Game.this.getSharedPreferences("prefkey", 0).edit();
            editor.putLong("intTime", Game.this.getTime);
            editor.commit();
        }

        public void onScore() {
            Game.this.nagaretagazou = ((Game_pen) Game.this.findViewById(R.id.play_view)).getgazou();
        }
    };
    private int setbtnflg = 0;
    private int setgazou = 0;
    /* access modifiers changed from: private */
    public int stage_clear = 0;
    /* access modifiers changed from: private */
    public int stage_number = 1;

    public void onCreate(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();
        this.stage_number = extras.getInt("Stage_Number");
        this.setbtnflg = extras.getInt("btn4");
        this.setgazou = extras.getInt("NGR");
        this.getTime = extras.getLong("intTime");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.game);
        Game_pen view = (Game_pen) findViewById(R.id.play_view);
        view.getStage(this.stage_number);
        view.setbtnflg(this.setbtnflg);
        view.setgazou(this.setgazou);
        view.setOnSettingListener(this.onSettingListener);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        startActivity(new Intent(this, Pen_tukami.class));
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        ((Game_pen) findViewById(R.id.play_view)).destroyBITMAP();
    }
}
