package jp.co.arttec.satbox.choice;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.SoundPool;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import jp.co.arttec.satbox.scoreranklib.GAECommonData;
import jp.co.arttec.satbox.scoreranklib.HttpCommunicationListener;
import jp.co.arttec.satbox.scoreranklib.RegistScoreController;

public class Question extends Activity {
    int MSCNT = 3;
    private AlertDialog _alertDialog;
    /* access modifiers changed from: private */
    public EditText _editText;
    int[] _soundID = new int[this.MSCNT];
    int[] attack = new int[this.MSCNT];
    /* access modifiers changed from: private */
    public int b = 0;
    /* access modifiers changed from: private */
    public TextView countText;
    private String[] data = new String[10];
    private int haikei;
    private int intLvlId = 35;
    private int intScoreNo;
    /* access modifiers changed from: private */
    public long intTime = 0;
    /* access modifiers changed from: private */
    public int miss = 0;
    /* access modifiers changed from: private */
    public int miss_goukei = 0;
    /* access modifiers changed from: private */
    public int nagaretagazou = 0;
    private String nichiji;
    private int[] rdm_picture = {R.drawable.ball_1, R.drawable.ball_2, R.drawable.ball_3, R.drawable.ball_4, R.drawable.ball_5, R.drawable.ball_6, R.drawable.ball_7, R.drawable.ball_8, R.drawable.ball_9, R.drawable.ball_10};
    /* access modifiers changed from: private */
    public int score = 0;
    private int[] scoredate = new int[10];
    /* access modifiers changed from: private */
    public SoundPool sentaku;
    /* access modifiers changed from: private */
    public int stage_number = 1;

    public void onCreate(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();
        this.nagaretagazou = extras.getInt("ngrgz");
        this.stage_number = extras.getInt("Stage_Number");
        this.sentaku = new SoundPool(3, 3, 0);
        this._soundID[0] = this.sentaku.load(this, R.raw.se_maru, 1);
        this._soundID[1] = this.sentaku.load(this, R.raw.se_batu, 1);
        this._soundID[2] = this.sentaku.load(this, R.raw.finish, 1);
        this._editText = new EditText(this);
        this.intTime = getSharedPreferences("prefkey", 0).getLong("intTime", 99999);
        super.onCreate(savedInstanceState);
        getWindow().addFlags(1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.gamen);
        this.countText = (TextView) findViewById(R.id.TextView01);
        this.countText.setTextColor(-16777216);
        this.countText.setText("Choice!");
        final View haikei2 = findViewById(R.id.haikei);
        final Button Button01 = (Button) findViewById(R.id.Button01);
        final Button Button02 = (Button) findViewById(R.id.Button02);
        final Button Button03 = (Button) findViewById(R.id.Button03);
        final Button Button04 = (Button) findViewById(R.id.Button04);
        List<RankStrDelivery2> dataList = new ArrayList<>();
        for (int i = 0; i < this.rdm_picture.length; i++) {
            dataList.add(new RankStrDelivery2(i, this.rdm_picture[i]));
        }
        Collections.shuffle(dataList);
        int i2 = 0;
        while (true) {
            if (i2 >= 3) {
                break;
            } else if (this.nagaretagazou == ((RankStrDelivery2) dataList.get(i2)).getRankNo2()) {
                this.b = i2;
                break;
            } else {
                i2++;
            }
        }
        if (i2 == 3) {
            int index = (int) (Math.random() * ((double) i2));
            this.b = index;
            dataList.add(index, new RankStrDelivery2(this.nagaretagazou, this.rdm_picture[this.nagaretagazou]));
        }
        Button01.setBackgroundResource(((RankStrDelivery2) dataList.get(0)).getItem1_2());
        Button02.setBackgroundResource(((RankStrDelivery2) dataList.get(1)).getItem1_2());
        Button03.setBackgroundResource(((RankStrDelivery2) dataList.get(2)).getItem1_2());
        Button01.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Question.this.b == 0) {
                    Question question = Question.this;
                    question.stage_number = question.stage_number + 1;
                    if (Question.this.stage_number == 11) {
                        Question.this.sentaku.play(Question.this._soundID[2], 1.0f, 1.0f, 0, 0, 1.0f);
                        Button01.setEnabled(false);
                        Button02.setEnabled(false);
                        Button03.setEnabled(false);
                        Button04.setEnabled(false);
                        haikei2.setBackgroundResource(R.drawable.haikei_good);
                        Button02.setBackgroundResource(R.drawable.ball_batu);
                        Button03.setBackgroundResource(R.drawable.ball_batu);
                        Question.this.miss_goukei = GAECommonData.CONNECTION_TIMEOUT;
                        long Scr_time = (System.currentTimeMillis() - Question.this.intTime) + ((long) (Question.this.miss_goukei * Question.this.miss));
                        Question.this.score = (int) Scr_time;
                        String fmt = String.format("%05.03f", Double.valueOf(((double) Scr_time) / 1000.0d));
                        Question.this._editText.setFocusable(true);
                        Question.this._editText.setFocusableInTouchMode(true);
                        Question.this._editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
                        AlertDialog.Builder dialog = new AlertDialog.Builder(Question.this);
                        dialog.setTitle("Result is『" + fmt + "』!");
                        dialog.setMessage("Enter your name");
                        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Question.this.registHighScore(Question.this._editText.getText().toString());
                                Question.this.onMyScoreEntry("");
                                Question.this.startActivity(new Intent(Question.this, Pen_tukami.class));
                                Question.this.finish();
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Question.this.startActivity(new Intent(Question.this, Pen_tukami.class));
                                Question.this.finish();
                            }
                        }).setView(Question.this._editText).setCancelable(false).create();
                        dialog.show();
                        Question.this.countText.setText("Finish!");
                        return;
                    }
                    Question.this.countText.setText("Yes!");
                    Question.this.sentaku.play(Question.this._soundID[0], 1.0f, 1.0f, 0, 0, 1.0f);
                    haikei2.setBackgroundResource(R.drawable.haikei_good);
                    Button01.setEnabled(false);
                    Button02.setEnabled(false);
                    Button03.setEnabled(false);
                    Button04.setEnabled(false);
                    Button02.setBackgroundResource(R.drawable.ball_batu);
                    Button03.setBackgroundResource(R.drawable.ball_batu);
                    Intent intent = new Intent(Question.this, Game.class);
                    intent.putExtra("Stage_Number", Question.this.stage_number);
                    Question.this.startActivity(intent);
                    Question.this.finish();
                } else if (Question.this.b != 0) {
                    Question question2 = Question.this;
                    question2.miss = question2.miss + 1;
                    haikei2.setBackgroundResource(R.drawable.haikei_bad);
                    Button01.setBackgroundResource(R.drawable.ball_batu);
                    Question.this.sentaku.play(Question.this._soundID[1], 1.0f, 1.0f, 0, 0, 1.0f);
                    Question.this.countText.setText("No!");
                    Button01.setEnabled(false);
                }
            }
        });
        Button02.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Question.this.b == 1) {
                    Question question = Question.this;
                    question.stage_number = question.stage_number + 1;
                    if (Question.this.stage_number == 11) {
                        Question.this.sentaku.play(Question.this._soundID[2], 1.0f, 1.0f, 0, 0, 1.0f);
                        Button01.setEnabled(false);
                        Button02.setEnabled(false);
                        Button03.setEnabled(false);
                        Button04.setEnabled(false);
                        haikei2.setBackgroundResource(R.drawable.haikei_good);
                        Button01.setBackgroundResource(R.drawable.ball_batu);
                        Button03.setBackgroundResource(R.drawable.ball_batu);
                        Question.this.miss_goukei = GAECommonData.CONNECTION_TIMEOUT;
                        long Scr_time = (System.currentTimeMillis() - Question.this.intTime) + ((long) (Question.this.miss_goukei * Question.this.miss));
                        Question.this.score = (int) Scr_time;
                        String fmt = String.format("%05.03f", Double.valueOf(((double) Scr_time) / 1000.0d));
                        Question.this._editText.setFocusable(true);
                        Question.this._editText.setFocusableInTouchMode(true);
                        Question.this._editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
                        AlertDialog.Builder dialog = new AlertDialog.Builder(Question.this);
                        dialog.setTitle("Result is『" + fmt + "』!");
                        dialog.setMessage("Enter your name");
                        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Question.this.registHighScore(Question.this._editText.getText().toString());
                                Question.this.onMyScoreEntry("");
                                Question.this.startActivity(new Intent(Question.this, Pen_tukami.class));
                                Question.this.finish();
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Question.this.startActivity(new Intent(Question.this, Pen_tukami.class));
                                Question.this.finish();
                            }
                        }).setView(Question.this._editText).setCancelable(false).create();
                        dialog.show();
                        Question.this.countText.setText("Finish!");
                        return;
                    }
                    Question.this.countText.setText("Alright!");
                    haikei2.setBackgroundResource(R.drawable.haikei_good);
                    Question.this.sentaku.play(Question.this._soundID[0], 1.0f, 1.0f, 0, 0, 1.0f);
                    Button01.setEnabled(false);
                    Button02.setEnabled(false);
                    Button03.setEnabled(false);
                    Button04.setEnabled(false);
                    Button01.setBackgroundResource(R.drawable.ball_batu);
                    Button03.setBackgroundResource(R.drawable.ball_batu);
                    Intent intent = new Intent(Question.this, Game.class);
                    intent.putExtra("Stage_Number", Question.this.stage_number);
                    Question.this.startActivity(intent);
                    Question.this.finish();
                } else if (Question.this.b != 1) {
                    Question question2 = Question.this;
                    question2.miss = question2.miss + 1;
                    haikei2.setBackgroundResource(R.drawable.haikei_bad);
                    Button02.setBackgroundResource(R.drawable.ball_batu);
                    Question.this.sentaku.play(Question.this._soundID[1], 1.0f, 1.0f, 0, 0, 1.0f);
                    Question.this.countText.setText("Mistake!");
                    Button02.setEnabled(false);
                }
            }
        });
        Button03.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Question.this.b == 2) {
                    Question question = Question.this;
                    question.stage_number = question.stage_number + 1;
                    if (Question.this.stage_number == 11) {
                        Question.this.sentaku.play(Question.this._soundID[2], 1.0f, 1.0f, 0, 0, 1.0f);
                        Button01.setEnabled(false);
                        Button02.setEnabled(false);
                        Button03.setEnabled(false);
                        Button04.setEnabled(false);
                        haikei2.setBackgroundResource(R.drawable.haikei_good);
                        Button01.setBackgroundResource(R.drawable.ball_batu);
                        Button02.setBackgroundResource(R.drawable.ball_batu);
                        Question.this.miss_goukei = GAECommonData.CONNECTION_TIMEOUT;
                        long Scr_time = (System.currentTimeMillis() - Question.this.intTime) + ((long) (Question.this.miss_goukei * Question.this.miss));
                        Question.this.score = (int) Scr_time;
                        String fmt = String.format("%05.03f", Double.valueOf(((double) Scr_time) / 1000.0d));
                        Question.this._editText.setFocusable(true);
                        Question.this._editText.setFocusableInTouchMode(true);
                        Question.this._editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
                        AlertDialog.Builder dialog = new AlertDialog.Builder(Question.this);
                        dialog.setTitle("Result is『" + fmt + "』!");
                        dialog.setMessage("Enter your name");
                        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Question.this.registHighScore(Question.this._editText.getText().toString());
                                Question.this.onMyScoreEntry("");
                                Question.this.startActivity(new Intent(Question.this, Pen_tukami.class));
                                Question.this.finish();
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Question.this.startActivity(new Intent(Question.this, Pen_tukami.class));
                                Question.this.finish();
                            }
                        }).setView(Question.this._editText).setCancelable(false).create();
                        dialog.show();
                        Question.this.countText.setText("Finish!");
                        return;
                    }
                    Question.this.countText.setText("OK!");
                    Question.this.sentaku.play(Question.this._soundID[0], 1.0f, 1.0f, 0, 0, 1.0f);
                    haikei2.setBackgroundResource(R.drawable.haikei_good);
                    Button01.setBackgroundResource(R.drawable.ball_batu);
                    Button02.setBackgroundResource(R.drawable.ball_batu);
                    Button01.setEnabled(false);
                    Button02.setEnabled(false);
                    Button03.setEnabled(false);
                    Button04.setEnabled(false);
                    Intent intent = new Intent(Question.this, Game.class);
                    intent.putExtra("Stage_Number", Question.this.stage_number);
                    Question.this.startActivity(intent);
                    Question.this.finish();
                } else if (Question.this.b != 2) {
                    Question question2 = Question.this;
                    question2.miss = question2.miss + 1;
                    haikei2.setBackgroundResource(R.drawable.haikei_bad);
                    Button03.setBackgroundResource(R.drawable.ball_batu);
                    Question.this.sentaku.play(Question.this._soundID[1], 1.0f, 1.0f, 0, 0, 1.0f);
                    Question.this.countText.setText("Different!");
                    Button03.setEnabled(false);
                }
            }
        });
        Button04.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Question.this, Game.class);
                intent.putExtra("Stage_Number", Question.this.stage_number);
                intent.putExtra("btn4", 99);
                intent.putExtra("NGR", Question.this.nagaretagazou);
                Question.this.startActivity(intent);
                Question.this.finish();
            }
        });
        if (dataList != null) {
            dataList.clear();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        startActivity(new Intent(this, Pen_tukami.class));
        finish();
        return true;
    }

    private void onScoreEntry() {
        this._alertDialog.show();
    }

    /* access modifiers changed from: private */
    public void registHighScore(String name) {
        RegistScoreController controller = new RegistScoreController(this.intLvlId, (long) this.score, name);
        controller.setActivity(this);
        controller.setOnFinishListener(new HttpCommunicationListener() {
            public void onFinish(boolean result) {
                Toast toast;
                if (result) {
                    toast = Toast.makeText(Question.this, "Score entry completion.", 1);
                } else {
                    toast = Toast.makeText(Question.this, "Score entry failure.", 1);
                }
                toast.show();
            }
        });
        controller.registScore();
    }

    /* access modifiers changed from: private */
    public void onMyScoreEntry(String ranklevel) {
        SharedPreferences pref = getSharedPreferences("prefkey", 0);
        ArrayList<RankStrDelivery> itemSort = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            this.intScoreNo = i + 1;
            this.scoredate[i] = pref.getInt("Score" + ranklevel + String.valueOf(this.intScoreNo), 99999);
            this.data[i] = pref.getString("Data" + ranklevel + String.valueOf(this.intScoreNo), "9999/99/99 99:99:99");
            itemSort.add(new RankStrDelivery(this.scoredate[i], this.data[i], "", ""));
        }
        setNichiji();
        itemSort.add(new RankStrDelivery(this.score, this.nichiji, "", ""));
        Collections.sort(itemSort, new Comparator<RankStrDelivery>() {
            public int compare(RankStrDelivery o1, RankStrDelivery o2) {
                return o1.getRankNo() - o2.getRankNo();
            }
        });
        SharedPreferences.Editor editor = pref.edit();
        this.intScoreNo = 0;
        for (int i2 = 0; i2 < 10; i2++) {
            this.intScoreNo = i2 + 1;
            RankStrDelivery rsd = (RankStrDelivery) itemSort.get(i2);
            editor.putInt("Score" + ranklevel + String.valueOf(this.intScoreNo), rsd.getRankNo());
            editor.putString("Data" + ranklevel + String.valueOf(this.intScoreNo), rsd.getItem1());
        }
        editor.commit();
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.sentaku != null) {
            this.sentaku.release();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    private void setNichiji() {
        String strMonth;
        String strday;
        String strHour;
        String strMinute;
        String strSecond;
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(1);
        int month = calendar.get(2);
        int day = calendar.get(5);
        int hour = calendar.get(11);
        int minute = calendar.get(12);
        int second = calendar.get(13);
        String strYear = String.valueOf(year);
        if (month < 10) {
            strMonth = "0" + String.valueOf(month + 1);
        } else {
            strMonth = String.valueOf(month + 1);
        }
        if (day < 10) {
            strday = "0" + String.valueOf(day);
        } else {
            strday = String.valueOf(day);
        }
        if (hour < 10) {
            strHour = "0" + String.valueOf(hour);
        } else {
            strHour = String.valueOf(hour);
        }
        if (minute < 10) {
            strMinute = "0" + String.valueOf(minute);
        } else {
            strMinute = String.valueOf(minute);
        }
        if (second < 10) {
            strSecond = "0" + String.valueOf(second);
        } else {
            strSecond = String.valueOf(second);
        }
        this.nichiji = String.valueOf(strYear) + "/" + strMonth + "/" + strday + " " + strHour + ":" + strMinute + ":" + strSecond;
    }
}
