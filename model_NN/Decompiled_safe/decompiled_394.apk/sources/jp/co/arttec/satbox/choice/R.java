package jp.co.arttec.satbox.choice;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int refreshAnimation = 2130771970;
        public static final int requestInterval = 2130771969;
        public static final int testMode = 2130771972;
        public static final int visibility = 2130771971;
    }

    public static final class color {
        public static final int white = 2131099648;
    }

    public static final class drawable {
        public static final int ball_1 = 2130837504;
        public static final int ball_10 = 2130837505;
        public static final int ball_11 = 2130837506;
        public static final int ball_12 = 2130837507;
        public static final int ball_13 = 2130837508;
        public static final int ball_14 = 2130837509;
        public static final int ball_15 = 2130837510;
        public static final int ball_16 = 2130837511;
        public static final int ball_17 = 2130837512;
        public static final int ball_18 = 2130837513;
        public static final int ball_19 = 2130837514;
        public static final int ball_2 = 2130837515;
        public static final int ball_20 = 2130837516;
        public static final int ball_3 = 2130837517;
        public static final int ball_4 = 2130837518;
        public static final int ball_5 = 2130837519;
        public static final int ball_6 = 2130837520;
        public static final int ball_7 = 2130837521;
        public static final int ball_8 = 2130837522;
        public static final int ball_9 = 2130837523;
        public static final int ball_batu = 2130837524;
        public static final int choice_title_3 = 2130837525;
        public static final int haikei_bad = 2130837526;
        public static final int haikei_good = 2130837527;
        public static final int haikei_normal = 2130837528;
        public static final int icon = 2130837529;
        public static final int pc_satbox = 2130837530;
        public static final int sat_logo_yoko = 2130837531;
        public static final int start_text_2 = 2130837532;
        public static final int tuitate_hidari = 2130837533;
        public static final int tuitate_migi = 2130837534;
        public static final int white_out = 2130837535;
    }

    public static final class id {
        public static final int Button01 = 2131230726;
        public static final int Button02 = 2131230727;
        public static final int Button03 = 2131230728;
        public static final int Button04 = 2131230729;
        public static final int LinearLayout01 = 2131230725;
        public static final int LinearLayout02 = 2131230723;
        public static final int TextView01 = 2131230724;
        public static final int adproxy = 2131230730;
        public static final int credit = 2131230720;
        public static final int easy_stage1 = 2131230743;
        public static final int easy_stage1_check = 2131230742;
        public static final int easy_stage2 = 2131230745;
        public static final int easy_stage2_check = 2131230744;
        public static final int easy_stage3 = 2131230747;
        public static final int easy_stage3_check = 2131230746;
        public static final int easy_stage4 = 2131230749;
        public static final int easy_stage4_check = 2131230748;
        public static final int easy_stage5 = 2131230751;
        public static final int easy_stage5_check = 2131230750;
        public static final int easy_visibility = 2131230741;
        public static final int footer = 2131230734;
        public static final int haikei = 2131230722;
        public static final int hard_stage1 = 2131230765;
        public static final int hard_stage1_check = 2131230764;
        public static final int hard_stage2 = 2131230767;
        public static final int hard_stage2_check = 2131230766;
        public static final int hard_stage3 = 2131230769;
        public static final int hard_stage3_check = 2131230768;
        public static final int hard_stage4 = 2131230771;
        public static final int hard_stage4_check = 2131230770;
        public static final int hard_stage5 = 2131230773;
        public static final int hard_stage5_check = 2131230772;
        public static final int hard_visibility = 2131230763;
        public static final int header = 2131230731;
        public static final int menu_apk = 2131230774;
        public static final int menu_end = 2131230776;
        public static final int menu_hp = 2131230775;
        public static final int normal_stage1 = 2131230754;
        public static final int normal_stage1_check = 2131230753;
        public static final int normal_stage2 = 2131230756;
        public static final int normal_stage2_check = 2131230755;
        public static final int normal_stage3 = 2131230758;
        public static final int normal_stage3_check = 2131230757;
        public static final int normal_stage4 = 2131230760;
        public static final int normal_stage4_check = 2131230759;
        public static final int normal_stage5 = 2131230762;
        public static final int normal_stage5_check = 2131230761;
        public static final int normal_visibility = 2131230752;
        public static final int play_view = 2131230721;
        public static final int rank_tx_touch = 2131230735;
        public static final int ranklistview = 2131230733;
        public static final int rankname = 2131230732;
        public static final int text_nanido = 2131230740;
        public static final int txtItem1 = 2131230737;
        public static final int txtItem21 = 2131230738;
        public static final int txtItem22 = 2131230739;
        public static final int txtRankNo = 2131230736;
    }

    public static final class layout {
        public static final int credit = 2130903040;
        public static final int game = 2130903041;
        public static final int gamen = 2130903042;
        public static final int main = 2130903043;
        public static final int rank = 2130903044;
        public static final int rankrow = 2130903045;
        public static final int rankrow2 = 2130903046;
        public static final int setting = 2130903047;
    }

    public static final class menu {
        public static final int menu = 2131165184;
    }

    public static final class raw {
        public static final int bgm_title = 2130968576;
        public static final int finish = 2130968577;
        public static final int pen_botan = 2130968578;
        public static final int pen_dame = 2130968579;
        public static final int pen_hakusyu = 2130968580;
        public static final int pen_ochiru = 2130968581;
        public static final int se_batu = 2130968582;
        public static final int se_batu2 = 2130968583;
        public static final int se_finish = 2130968584;
        public static final int se_maru = 2130968585;
        public static final int se_nagare = 2130968586;
        public static final int timer = 2130968587;
    }

    public static final class string {
        public static final int TextView01 = 2131034114;
        public static final int aboutmsg = 2131034116;
        public static final int abouttitle = 2131034115;
        public static final int app_name = 2131034113;
        public static final int hello = 2131034112;
    }

    public static final class styleable {
        public static final int[] mediba_ad_sdk_android_MasAdView = {R.attr.backgroundColor, R.attr.requestInterval, R.attr.refreshAnimation, R.attr.visibility, R.attr.testMode};
        public static final int mediba_ad_sdk_android_MasAdView_backgroundColor = 0;
        public static final int mediba_ad_sdk_android_MasAdView_refreshAnimation = 2;
        public static final int mediba_ad_sdk_android_MasAdView_requestInterval = 1;
        public static final int mediba_ad_sdk_android_MasAdView_testMode = 4;
        public static final int mediba_ad_sdk_android_MasAdView_visibility = 3;
    }
}
