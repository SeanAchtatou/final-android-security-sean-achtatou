package jp.co.arttec.satbox.choice;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.SoundPool;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class Game_pen extends View {
    int MSCNT = 1;
    int[] attack = new int[this.MSCNT];
    boolean bDrawable = false;
    private Bitmap ball_batu = BitmapFactory.decodeResource(getResources(), R.drawable.ball_batu);
    private Bitmap bmpPen_easy = BitmapFactory.decodeResource(getResources(), this.rdm_picture[this.rdm]);
    private Bitmap bmpPen_hard = BitmapFactory.decodeResource(getResources(), this.rdm_picture[this.rdm]);
    private Bitmap bmpPen_normal = BitmapFactory.decodeResource(getResources(), this.rdm_picture[this.rdm]);
    private Bitmap bmpStart_text = BitmapFactory.decodeResource(getResources(), R.drawable.start_text_2);
    private Bitmap bmpWhite_out = BitmapFactory.decodeResource(getResources(), R.drawable.white_out);
    int dispX;
    int dispY;
    int game_flg = 0;
    Bitmap gazou_easy = BitmapFactory.decodeResource(getResources(), this.rdm_picture[this.setGazou]);
    Bitmap gazou_hard = BitmapFactory.decodeResource(getResources(), this.rdm_picture[this.setGazou]);
    Bitmap gazou_normal = BitmapFactory.decodeResource(getResources(), this.rdm_picture[this.setGazou]);
    private int getStage = 0;
    int haba = 0;
    int habakotei = -60;
    int hand2H = this.tuitate_hidari.getHeight();
    int hand2W = this.tuitate_hidari.getWidth();
    int handH = this.tuitate_migi.getHeight();
    int handTH = this.tuitate_hidari.getHeight();
    int handTW = this.tuitate_hidari.getWidth();
    int handW = this.tuitate_migi.getWidth();
    int hand_bottom;
    int hand_top;
    RedrawHandler handler = new RedrawHandler(this, 30);
    private long intTime = 0;
    private int listener_judgment = 0;
    private int nagaretagazou = 0;
    private OnSettingListener onSettingListener;
    int penEH = this.bmpPen_easy.getHeight();
    int penEW = this.bmpPen_easy.getWidth();
    int penHH = this.bmpPen_hard.getHeight();
    int penHW = this.bmpPen_hard.getWidth();
    int penNH = this.bmpPen_normal.getHeight();
    int penNW = this.bmpPen_normal.getWidth();
    int penY = 1000;
    int pen_bottomE;
    int pen_bottomH;
    int pen_bottomN;
    private int pen_slow = 0;
    int pen_speed = 0;
    final int pen_speed_stage1 = -30;
    final int pen_speed_stage2 = -45;
    final int pen_speed_stage3 = -60;
    final int pen_speed_stage4 = -75;
    final int pen_speed_stage5 = -90;
    int pen_top;
    int rdm = ((int) Math.floor(Math.random() * 9.0d));
    private int[] rdm_picture = {R.drawable.ball_11, R.drawable.ball_12, R.drawable.ball_13, R.drawable.ball_14, R.drawable.ball_15, R.drawable.ball_16, R.drawable.ball_17, R.drawable.ball_18, R.drawable.ball_19, R.drawable.ball_20};
    int rdm_start = 0;
    int result = 0;
    private int result_score = 0;
    private int setBtnflg = 0;
    private int setGazou = 0;
    SoundPool soundPool;
    private boolean sound_ochiru = false;
    private int stage_score = 0;
    int start_textH = this.bmpStart_text.getHeight();
    int start_textW = this.bmpStart_text.getWidth();
    int time = 0;
    private Bitmap tuitate_hidari = BitmapFactory.decodeResource(getResources(), R.drawable.tuitate_hidari);
    private Bitmap tuitate_migi = BitmapFactory.decodeResource(getResources(), R.drawable.tuitate_migi);

    public interface OnSettingListener {
        void onScore();

        void onSetting();

        void onTime();
    }

    public Game_pen(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public Game_pen(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Game_pen(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        this.dispX = w;
        this.dispY = h;
    }

    /* access modifiers changed from: protected */
    public synchronized void onDraw(Canvas canvas) {
        if (!this.bDrawable) {
            this.bDrawable = true;
            this.soundPool = new SoundPool(this.MSCNT, 3, 0);
            this.attack[0] = this.soundPool.load(getContext(), R.raw.se_nagare, 1);
            this.handler.start();
        }
        canvas.drawColor(-1);
        if (this.setBtnflg == 99) {
            this.bmpPen_easy = BitmapFactory.decodeResource(getResources(), this.rdm_picture[this.setGazou]);
            this.bmpPen_normal = BitmapFactory.decodeResource(getResources(), this.rdm_picture[this.setGazou]);
            this.bmpPen_hard = BitmapFactory.decodeResource(getResources(), this.rdm_picture[this.setGazou]);
            this.rdm = this.setGazou;
        }
        if (this.game_flg != 2) {
            if (this.getStage == 1 || this.getStage == 2 || this.getStage == 3 || this.getStage == 4 || this.getStage == 5) {
                canvas.drawBitmap(this.bmpPen_easy, (float) ((this.dispX / 2) - (this.penEW / 2)), (float) (this.penY + 10), (Paint) null);
            } else if (this.getStage == 6 || this.getStage == 7 || this.getStage == 8 || this.getStage == 9 || this.getStage == 10) {
                canvas.drawBitmap(this.bmpPen_normal, (float) ((this.dispX / 2) - (this.penNW / 2)), (float) (this.penY + 10), (Paint) null);
            } else if (this.getStage == 11 || this.getStage == 12 || this.getStage == 13 || this.getStage == 14 || this.getStage == 15) {
                canvas.drawBitmap(this.bmpPen_hard, (float) ((this.dispX / 2) - (this.penHW / 2)), (float) (this.penY + 10), (Paint) null);
            }
            if (this.getStage > 0 && this.getStage < 4) {
                this.haba = this.habakotei * 2;
            } else if (this.getStage > 3 && this.getStage < 7) {
                this.haba = this.habakotei;
            } else if (this.getStage > 6) {
                this.haba = 0;
            }
            canvas.drawBitmap(this.tuitate_migi, 0.0f, (float) (this.dispY - (this.handH + this.haba)), (Paint) null);
            if (this.getStage > 0 && this.getStage < 4) {
                this.haba = this.habakotei * 2;
            } else if (this.getStage > 3 && this.getStage < 7) {
                this.haba = this.habakotei;
            } else if (this.getStage > 6) {
                this.haba = 0;
            }
            canvas.drawBitmap(this.tuitate_hidari, 0.0f, (float) this.haba, (Paint) null);
        }
        if (this.game_flg == 0) {
            this.time = 0;
            this.result = 0;
            this.rdm_start = 35;
            if (this.getStage == 1 || this.getStage == 2) {
                this.pen_speed = -30;
            } else if (this.getStage == 3 || this.getStage == 4) {
                this.pen_speed = -45;
            } else if (this.getStage == 5 || this.getStage == 6) {
                this.pen_speed = -60;
            } else if (this.getStage == 7 || this.getStage == 8) {
                this.pen_speed = -75;
            } else if (this.getStage == 9 || this.getStage == 10) {
                this.pen_speed = -90;
            }
            if (this.game_flg == 2 || this.setBtnflg == 99) {
                this.game_flg = 1;
            } else if (this.getStage > 1) {
                this.game_flg = 1;
            } else if (this.getStage == 1 || this.game_flg == 0 || this.setBtnflg != 99) {
                canvas.drawBitmap(this.bmpWhite_out, new Rect(0, 0, this.dispX, this.dispY), new Rect(0, 0, this.dispX, this.dispY), (Paint) null);
                canvas.drawBitmap(this.bmpStart_text, (float) ((this.dispX / 2) - (this.start_textW / 2)), (float) ((this.dispY / 2) - (this.start_textH / 2)), (Paint) null);
            } else {
                this.game_flg = 1;
            }
        } else if (this.game_flg == 1) {
            this.time++;
            if (this.time >= this.rdm_start) {
                if (-100 > this.penY) {
                    this.game_flg = 2;
                    this.nagaretagazou = this.rdm;
                    getgazou();
                    this.listener_judgment = 1;
                    OnSettingEvent();
                } else {
                    if (this.pen_slow == 0) {
                        this.pen_slow = 1;
                    }
                    this.penY += this.pen_speed;
                    if (!this.sound_ochiru) {
                        this.soundPool.play(this.attack[0], 1.0f, 1.0f, 0, 0, 1.0f);
                        this.sound_ochiru = true;
                    }
                }
            }
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == 0 && this.game_flg == 0) {
            this.game_flg = 1;
            this.intTime = System.currentTimeMillis();
            getTime();
            OnSettingEvent();
        }
        return true;
    }

    public void destroyBITMAP() {
        if (this.soundPool != null) {
            this.soundPool.release();
        }
        if (this.tuitate_migi != null) {
            this.tuitate_migi.recycle();
            this.tuitate_migi = null;
        }
        if (this.bmpPen_easy != null) {
            this.bmpPen_easy.recycle();
            this.bmpPen_easy = null;
        }
        if (this.bmpPen_normal != null) {
            this.bmpPen_normal.recycle();
            this.bmpPen_normal = null;
        }
        if (this.bmpPen_hard != null) {
            this.bmpPen_hard.recycle();
            this.bmpPen_hard = null;
        }
        if (this.tuitate_hidari != null) {
            this.tuitate_hidari.recycle();
            this.tuitate_hidari = null;
        }
        if (this.bmpStart_text != null) {
            this.bmpStart_text.recycle();
            this.bmpStart_text = null;
        }
        if (this.bmpWhite_out != null) {
            this.bmpWhite_out.recycle();
            this.bmpWhite_out = null;
        }
        if (this.ball_batu != null) {
            this.ball_batu.recycle();
            this.ball_batu = null;
        }
        System.gc();
    }

    public void getStage(int getstage) {
        this.getStage = getstage;
    }

    public void intTime(long IntTime) {
        this.intTime = IntTime;
    }

    public long getTime() {
        return this.intTime;
    }

    public int getScore() {
        this.result_score = this.stage_score;
        return this.result_score;
    }

    public int getgazou() {
        return this.nagaretagazou;
    }

    public void setgazou(int setgazou) {
        this.setGazou = setgazou;
    }

    public void setbtnflg(int setbtnflg) {
        this.setBtnflg = setbtnflg;
    }

    public void setOnSettingListener(OnSettingListener listener) {
        this.onSettingListener = listener;
    }

    private void OnSettingEvent() {
        if (this.onSettingListener == null) {
            return;
        }
        if (this.listener_judgment == 0) {
            this.onSettingListener.onTime();
            return;
        }
        this.onSettingListener.onScore();
        this.onSettingListener.onSetting();
    }
}
