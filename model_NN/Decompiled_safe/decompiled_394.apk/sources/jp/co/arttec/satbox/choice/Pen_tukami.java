package jp.co.arttec.satbox.choice;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

public class Pen_tukami extends BaseActivity {
    int MSCNT = 5;
    private MediaPlayer _BGM;
    int[] attack = new int[this.MSCNT];
    SoundPool soundPool;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.main);
        this._BGM = null;
        playSound();
        this.soundPool = new SoundPool(this.MSCNT, 3, 0);
        this.attack[0] = this.soundPool.load(this, R.raw.pen_botan, 1);
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() != 0) {
            return super.onTouchEvent(event);
        }
        Intent intent = new Intent(this, Game.class);
        intent.putExtra("Stage_Number", 1);
        startActivity(intent);
        this.soundPool.play(this.attack[0], 1.0f, 1.0f, 0, 0, 1.0f);
        finish();
        return true;
    }

    public void onClickRANKING(View v) {
        startActivity(new Intent(this, BMRank.class));
        finish();
    }

    public void onClickHELP(View v) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle((int) R.string.abouttitle);
        dialog.setMessage((int) R.string.aboutmsg);
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialog.show();
    }

    public void onClickSATBOX(View v) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:SAT-BOX")));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public synchronized boolean onMenuItemSelected(int featureId, MenuItem item) {
        boolean result;
        result = super.onMenuItemSelected(featureId, item);
        switch (item.getItemId()) {
            case R.id.menu_apk /*2131230774*/:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:SAT-BOX")));
                break;
            case R.id.menu_hp /*2131230775*/:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.arttec.co.jp/sat-box/")));
                break;
            case R.id.menu_end /*2131230776*/:
                finish();
                break;
        }
        return result;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this._BGM != null && !this._BGM.isPlaying()) {
            this._BGM.start();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this._BGM != null && this._BGM.isPlaying()) {
            this._BGM.pause();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        stopSound();
    }

    private void stopSound() {
        if (this._BGM != null) {
            if (this._BGM.isPlaying()) {
                this._BGM.stop();
            }
            this._BGM.release();
            this._BGM = null;
        }
    }

    private void playSound() {
        stopSound();
        this._BGM = MediaPlayer.create(this, (int) R.raw.bgm_title);
        this._BGM.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
                mp.seekTo(0);
                mp.start();
            }
        });
    }
}
