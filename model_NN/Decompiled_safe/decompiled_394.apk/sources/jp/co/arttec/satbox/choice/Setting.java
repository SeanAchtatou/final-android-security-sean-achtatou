package jp.co.arttec.satbox.choice;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TableLayout;
import android.widget.TextView;

public class Setting extends BaseActivity {
    private static final int EASY = 0;
    private static final int HARD = 2;
    private static final int NORMAL = 1;
    private Button easy_stage1_button;
    private Button easy_stage2_button;
    private Button easy_stage3_button;
    private Button easy_stage4_button;
    private Button easy_stage5_button;
    private Button hard_stage1_button;
    private Button hard_stage2_button;
    private Button hard_stage3_button;
    private Button hard_stage4_button;
    private Button hard_stage5_button;
    private int mLevel;
    private SharedPreferences mPref;
    private TextView nanido;
    private Button normal_stage1_button;
    private Button normal_stage2_button;
    private Button normal_stage3_button;
    private Button normal_stage4_button;
    private Button normal_stage5_button;
    private int stage_score = 0;
    private int stage_score1;
    private int stage_score10;
    private int stage_score11;
    private int stage_score12;
    private int stage_score13;
    private int stage_score14;
    private int stage_score15;
    private int stage_score2;
    private int stage_score3;
    private int stage_score4;
    private int stage_score5;
    private int stage_score6;
    private int stage_score7;
    private int stage_score8;
    private int stage_score9;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.setting);
    }

    public void onResume() {
        super.onResume();
        this.mPref = getSharedPreferences("pref", 0);
        this.stage_score1 = this.mPref.getInt("key1", 0);
        this.stage_score2 = this.mPref.getInt("key2", 0);
        this.stage_score3 = this.mPref.getInt("key3", 0);
        this.stage_score4 = this.mPref.getInt("key4", 0);
        this.stage_score5 = this.mPref.getInt("key5", 0);
        this.stage_score6 = this.mPref.getInt("key6", 0);
        this.stage_score7 = this.mPref.getInt("key7", 0);
        this.stage_score8 = this.mPref.getInt("key8", 0);
        this.stage_score9 = this.mPref.getInt("key9", 0);
        this.stage_score10 = this.mPref.getInt("key10", 0);
        this.stage_score11 = this.mPref.getInt("key11", 0);
        this.stage_score12 = this.mPref.getInt("key12", 0);
        this.stage_score13 = this.mPref.getInt("key13", 0);
        this.stage_score14 = this.mPref.getInt("key14", 0);
        this.stage_score15 = this.mPref.getInt("key15", 0);
        this.mLevel = this.mPref.getInt("Level", 0);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.stage_score = extras.getInt("SCORE");
            if (this.stage_score == 1) {
                this.stage_score1 = 1;
            }
            if (this.stage_score == 2) {
                this.stage_score2 = 2;
            }
            if (this.stage_score == 3) {
                this.stage_score3 = 3;
            }
            if (this.stage_score == 4) {
                this.stage_score4 = 4;
            }
            if (this.stage_score == 5) {
                this.stage_score5 = 5;
            }
            if (this.stage_score == 6) {
                this.stage_score6 = 6;
            }
            if (this.stage_score == 7) {
                this.stage_score7 = 7;
            }
            if (this.stage_score == 8) {
                this.stage_score8 = 8;
            }
            if (this.stage_score == 9) {
                this.stage_score9 = 9;
            }
            if (this.stage_score == 10) {
                this.stage_score10 = 10;
            }
            if (this.stage_score == 11) {
                this.stage_score11 = 11;
            }
            if (this.stage_score == 12) {
                this.stage_score12 = 12;
            }
            if (this.stage_score == 13) {
                this.stage_score13 = 13;
            }
            if (this.stage_score == 14) {
                this.stage_score14 = 14;
            }
            if (this.stage_score == 15) {
                this.stage_score15 = 15;
            }
        }
        SharedPreferences.Editor e = this.mPref.edit();
        e.putInt("key1", this.stage_score1);
        e.putInt("key2", this.stage_score2);
        e.putInt("key3", this.stage_score3);
        e.putInt("key4", this.stage_score4);
        e.putInt("key5", this.stage_score5);
        e.putInt("key6", this.stage_score6);
        e.putInt("key7", this.stage_score7);
        e.putInt("key8", this.stage_score8);
        e.putInt("key9", this.stage_score9);
        e.putInt("key10", this.stage_score10);
        e.putInt("key11", this.stage_score11);
        e.putInt("key12", this.stage_score12);
        e.putInt("key13", this.stage_score13);
        e.putInt("key14", this.stage_score14);
        e.putInt("key15", this.stage_score15);
        e.commit();
        if (this.stage_score1 == 1) {
            CheckBox checkBox = (CheckBox) findViewById(R.id.easy_stage1_check);
            checkBox.setChecked(true);
            checkBox.invalidate();
        }
        if (this.stage_score2 == 2) {
            CheckBox checkBox2 = (CheckBox) findViewById(R.id.easy_stage2_check);
            checkBox2.setChecked(true);
            checkBox2.invalidate();
        }
        if (this.stage_score3 == 3) {
            CheckBox checkBox3 = (CheckBox) findViewById(R.id.easy_stage3_check);
            checkBox3.setChecked(true);
            checkBox3.invalidate();
        }
        if (this.stage_score4 == 4) {
            CheckBox checkBox4 = (CheckBox) findViewById(R.id.easy_stage4_check);
            checkBox4.setChecked(true);
            checkBox4.invalidate();
        }
        if (this.stage_score5 == 5) {
            CheckBox checkBox5 = (CheckBox) findViewById(R.id.easy_stage5_check);
            checkBox5.setChecked(true);
            checkBox5.invalidate();
        }
        if (this.stage_score6 == 6) {
            CheckBox checkBox6 = (CheckBox) findViewById(R.id.normal_stage1_check);
            checkBox6.setChecked(true);
            checkBox6.invalidate();
        }
        if (this.stage_score7 == 7) {
            CheckBox checkBox7 = (CheckBox) findViewById(R.id.normal_stage2_check);
            checkBox7.setChecked(true);
            checkBox7.invalidate();
        }
        if (this.stage_score8 == 8) {
            CheckBox checkBox8 = (CheckBox) findViewById(R.id.normal_stage3_check);
            checkBox8.setChecked(true);
            checkBox8.invalidate();
        }
        if (this.stage_score9 == 9) {
            CheckBox checkBox9 = (CheckBox) findViewById(R.id.normal_stage4_check);
            checkBox9.setChecked(true);
            checkBox9.invalidate();
        }
        if (this.stage_score10 == 10) {
            CheckBox checkBox10 = (CheckBox) findViewById(R.id.normal_stage5_check);
            checkBox10.setChecked(true);
            checkBox10.invalidate();
        }
        if (this.stage_score11 == 11) {
            CheckBox checkBox11 = (CheckBox) findViewById(R.id.hard_stage1_check);
            checkBox11.setChecked(true);
            checkBox11.invalidate();
        }
        if (this.stage_score12 == 12) {
            CheckBox checkBox12 = (CheckBox) findViewById(R.id.hard_stage2_check);
            checkBox12.setChecked(true);
            checkBox12.invalidate();
        }
        if (this.stage_score13 == 13) {
            CheckBox checkBox13 = (CheckBox) findViewById(R.id.hard_stage3_check);
            checkBox13.setChecked(true);
            checkBox13.invalidate();
        }
        if (this.stage_score14 == 14) {
            CheckBox checkBox14 = (CheckBox) findViewById(R.id.hard_stage4_check);
            checkBox14.setChecked(true);
            checkBox14.invalidate();
        }
        if (this.stage_score15 == 15) {
            CheckBox checkBox15 = (CheckBox) findViewById(R.id.hard_stage5_check);
            checkBox15.setChecked(true);
            checkBox15.invalidate();
        }
        this.easy_stage1_button = (Button) findViewById(R.id.easy_stage1);
        this.easy_stage2_button = (Button) findViewById(R.id.easy_stage2);
        this.easy_stage3_button = (Button) findViewById(R.id.easy_stage3);
        this.easy_stage4_button = (Button) findViewById(R.id.easy_stage4);
        this.easy_stage5_button = (Button) findViewById(R.id.easy_stage5);
        this.normal_stage1_button = (Button) findViewById(R.id.normal_stage1);
        this.normal_stage2_button = (Button) findViewById(R.id.normal_stage2);
        this.normal_stage3_button = (Button) findViewById(R.id.normal_stage3);
        this.normal_stage4_button = (Button) findViewById(R.id.normal_stage4);
        this.normal_stage5_button = (Button) findViewById(R.id.normal_stage5);
        this.hard_stage1_button = (Button) findViewById(R.id.hard_stage1);
        this.hard_stage2_button = (Button) findViewById(R.id.hard_stage2);
        this.hard_stage3_button = (Button) findViewById(R.id.hard_stage3);
        this.hard_stage4_button = (Button) findViewById(R.id.hard_stage4);
        this.hard_stage5_button = (Button) findViewById(R.id.hard_stage5);
        this.nanido = (TextView) findViewById(R.id.text_nanido);
        changeLevel(this.mLevel);
        this.easy_stage1_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this, Game.class);
                intent.putExtra("Stage_Number", 1);
                Setting.this.startActivity(intent);
                Setting.this.finish();
            }
        });
        this.easy_stage2_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this, Game.class);
                intent.putExtra("Stage_Number", 2);
                Setting.this.startActivity(intent);
                Setting.this.finish();
            }
        });
        this.easy_stage3_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this, Game.class);
                intent.putExtra("Stage_Number", 3);
                Setting.this.startActivity(intent);
                Setting.this.finish();
            }
        });
        this.easy_stage4_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this, Game.class);
                intent.putExtra("Stage_Number", 4);
                Setting.this.startActivity(intent);
                Setting.this.finish();
            }
        });
        this.easy_stage5_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this, Game.class);
                intent.putExtra("Stage_Number", 5);
                Setting.this.startActivity(intent);
                Setting.this.finish();
            }
        });
        this.normal_stage1_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this, Game.class);
                intent.putExtra("Stage_Number", 6);
                Setting.this.startActivity(intent);
                Setting.this.finish();
            }
        });
        this.normal_stage2_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this, Game.class);
                intent.putExtra("Stage_Number", 7);
                Setting.this.startActivity(intent);
                Setting.this.finish();
            }
        });
        this.normal_stage3_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this, Game.class);
                intent.putExtra("Stage_Number", 8);
                Setting.this.startActivity(intent);
                Setting.this.finish();
            }
        });
        this.normal_stage4_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this, Game.class);
                intent.putExtra("Stage_Number", 9);
                Setting.this.startActivity(intent);
                Setting.this.finish();
            }
        });
        this.normal_stage5_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this, Game.class);
                intent.putExtra("Stage_Number", 10);
                Setting.this.startActivity(intent);
                Setting.this.finish();
            }
        });
        this.hard_stage1_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this, Game.class);
                intent.putExtra("Stage_Number", 11);
                Setting.this.startActivity(intent);
                Setting.this.finish();
            }
        });
        this.hard_stage2_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this, Game.class);
                intent.putExtra("Stage_Number", 12);
                Setting.this.startActivity(intent);
                Setting.this.finish();
            }
        });
        this.hard_stage3_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this, Game.class);
                intent.putExtra("Stage_Number", 13);
                Setting.this.startActivity(intent);
                Setting.this.finish();
            }
        });
        this.hard_stage4_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this, Game.class);
                intent.putExtra("Stage_Number", 14);
                Setting.this.startActivity(intent);
                Setting.this.finish();
            }
        });
        this.hard_stage5_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this, Game.class);
                intent.putExtra("Stage_Number", 15);
                Setting.this.startActivity(intent);
                Setting.this.finish();
            }
        });
    }

    public void onClickEasy(View v) {
        changeLevel(0);
    }

    public void onClickNormal(View v) {
        changeLevel(1);
    }

    public void onClickHard(View v) {
        changeLevel(2);
    }

    private void changeLevel(int level) {
        TableLayout easy = (TableLayout) findViewById(R.id.easy_visibility);
        TableLayout normal = (TableLayout) findViewById(R.id.normal_visibility);
        TableLayout hard = (TableLayout) findViewById(R.id.hard_visibility);
        easy.setVisibility(8);
        normal.setVisibility(8);
        hard.setVisibility(8);
        switch (level) {
            case 0:
                easy.setVisibility(0);
                this.nanido.setText("Easy");
                break;
            case 1:
                normal.setVisibility(0);
                this.nanido.setText("Normal");
                break;
            case 2:
                hard.setVisibility(0);
                this.nanido.setText("Hard");
                break;
        }
        this.mLevel = level;
        SharedPreferences.Editor e = this.mPref.edit();
        e.putInt("Level", this.mLevel);
        e.commit();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public synchronized boolean onMenuItemSelected(int featureId, MenuItem item) {
        boolean result;
        result = super.onMenuItemSelected(featureId, item);
        switch (item.getItemId()) {
            case R.id.menu_apk:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:SAT-BOX")));
                break;
            case R.id.menu_hp:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.arttec.co.jp/sat-box/")));
                break;
            case R.id.menu_end:
                finish();
                break;
        }
        return result;
    }
}
