package jp.co.arttec.satbox.choice;

import android.app.Activity;
import android.content.Intent;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import jp.co.arttec.satbox.scoreranklib.GetScoreController;
import jp.co.arttec.satbox.scoreranklib.HttpCommunicationListener;
import jp.co.arttec.satbox.scoreranklib.Score;

public class BMRank2 extends Activity {
    /* access modifiers changed from: private */
    public GetScoreController controller;
    private boolean flg_end = false;
    private int intGetLevel = 35;
    /* access modifiers changed from: private */
    public int intRankno;
    private ListView mListView;
    private int soundIds;
    private SoundPool soundPool;
    private float spVolume = 0.5f;
    /* access modifiers changed from: private */
    public String strItem;
    /* access modifiers changed from: private */
    public TextView txtRankName;
    private TextView txtRankTouch;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.rank);
        this.intGetLevel = getIntent().getExtras().getInt("PlayLevel");
        this.mListView = (ListView) findViewById(R.id.ranklistview);
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                BMRank2.this.startActivity(new Intent(BMRank2.this, Pen_tukami.class));
                BMRank2.this.finish();
            }
        });
        this.txtRankTouch = new TextView(getApplication());
        this.txtRankTouch = (TextView) findViewById(R.id.rank_tx_touch);
        this.txtRankTouch.setVisibility(8);
        this.soundPool = new SoundPool(1, 3, 0);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        onScoreEntry();
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.soundPool != null) {
            this.soundPool.stop(this.soundIds);
            this.soundPool.release();
        }
    }

    private synchronized void onScoreEntry() {
        this.controller = new GetScoreController(this.intGetLevel);
        this.controller.setActivity(this);
        this.controller.setRank(99);
        this.controller.setReverse(true);
        this.controller.setOnFinishListener(new HttpCommunicationListener() {
            public void onFinish(boolean result) {
                BMRank2.this.txtRankName = (TextView) BMRank2.this.findViewById(R.id.rankname);
                BMRank2.this.txtRankName.setText("WorldRanking");
                if (result) {
                    Toast toast = Toast.makeText(BMRank2.this, "High score acquisition completion.", 1);
                    List<Score> _score = BMRank2.this.controller.getHighScoreList();
                    if (_score != null) {
                        List<RankStrDelivery> dataList = new ArrayList<>();
                        int j = 0;
                        for (Score data : _score) {
                            BMRank2.this.intRankno = j + 1;
                            if (data.getScore() < 100000) {
                                BMRank2.this.strItem = String.format("%06.03f", Double.valueOf(((double) data.getScore()) / 1000.0d));
                            } else {
                                BMRank2.this.strItem = String.format("%07.03f", Double.valueOf(((double) data.getScore()) / 1000.0d));
                            }
                            dataList.add(new RankStrDelivery(BMRank2.this.intRankno, data.getName(), BMRank2.this.strItem, ""));
                            j++;
                        }
                        ((ListView) BMRank2.this.findViewById(R.id.ranklistview)).setAdapter((ListAdapter) new RankAdapter(BMRank2.this, R.layout.rankrow2, dataList));
                    }
                    toast.show();
                    return;
                }
                BMRank2.this.txtRankName.setVisibility(8);
                BMRank2.this.startActivity(new Intent(BMRank2.this, Pen_tukami.class));
                BMRank2.this.finish();
            }
        });
        this.controller.getHighScore();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.soundPool.play(this.soundIds, this.spVolume, this.spVolume, 1, 0, 1.0f);
        Intent intent = new Intent(this, BMRank.class);
        intent.putExtra("PlayLevel", this.intGetLevel);
        startActivity(intent);
        finish();
        return true;
    }

    public synchronized boolean onTouchEvent(MotionEvent event) {
        boolean z;
        if (!this.flg_end) {
            this.flg_end = true;
            if (event.getAction() != 0) {
                z = false;
            } else {
                this.soundPool.play(this.soundIds, this.spVolume, this.spVolume, 1, 0, 1.0f);
                Intent intent = new Intent(this, Pen_tukami.class);
                intent.putExtra("PlayLevel", this.intGetLevel);
                startActivity(intent);
                finish();
                z = true;
            }
        } else {
            z = false;
        }
        return z;
    }
}
