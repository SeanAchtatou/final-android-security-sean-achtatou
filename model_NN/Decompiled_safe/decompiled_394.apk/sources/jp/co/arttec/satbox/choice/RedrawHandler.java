package jp.co.arttec.satbox.choice;

import android.os.Handler;
import android.os.Message;
import android.view.View;

public class RedrawHandler extends Handler {
    private int delayTime;
    private int frameRate;
    private View view;

    public RedrawHandler(View view2, int frameRate2) {
        this.view = view2;
        this.frameRate = frameRate2;
    }

    public void start() {
        this.delayTime = 1000 / this.frameRate;
        sendMessageDelayed(obtainMessage(0), (long) this.delayTime);
    }

    public void stop() {
        this.delayTime = 0;
    }

    public void handleMessage(Message msg) {
        this.view.invalidate();
        if (this.delayTime != 0) {
            sendMessageDelayed(obtainMessage(0), (long) this.delayTime);
        }
    }
}
