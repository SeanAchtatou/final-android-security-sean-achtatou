package jp.co.arttec.satbox.choice;

public class RankStrDelivery2 {
    private int item1;
    private int rankno;

    public RankStrDelivery2(int arankno, int rdmPicture2) {
        this.rankno = arankno;
        this.item1 = rdmPicture2;
    }

    public int getRankNo2() {
        return this.rankno;
    }

    public int getItem1_2() {
        return this.item1;
    }
}
