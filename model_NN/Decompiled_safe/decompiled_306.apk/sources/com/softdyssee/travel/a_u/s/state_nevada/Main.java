package com.softdyssee.travel.a_u.s.state_nevada;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.SpannableString;
import android.text.format.Time;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.InflateException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.flurry.android.AppCircle;
import com.flurry.android.FlurryAgent;
import com.flurry.android.Offer;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.mobfox.sdk.BannerListener;
import com.mobfox.sdk.Const;
import com.mobfox.sdk.MobFoxView;
import com.mobfox.sdk.RequestException;
import com.nullwire.trace.ExceptionHandler;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class Main extends Activity {
    private static final int MENU_APPCIRCLE = 4;
    private static final int MENU_QUIT = 1;
    private static final int MENU_VIEW_OTHER_APPLICATIONS = 2;
    static Integer currentGalleryPosition = null;
    /* access modifiers changed from: private */
    public static Flickr flickr;
    static boolean toggleButtonAllIsOn = true;
    public AlertDialog alertDialog;
    AppCircle appCircle;
    ImageButton arrowLeft;
    ImageButton arrowRight;
    DecimalFormat decimalFormat;
    ImageButton displayAllButton;
    ImageButton flurryAppcircleCloseButton;
    RelativeLayout flurryCatalogView;
    String flurryHookName;
    public boolean flurryIsActivated = false;
    List<Offer> flurryOffers;
    public ProgressDialog fullScreenPhotoProgressDialog;
    /* access modifiers changed from: private */
    public Gallery gallery;
    private TextView infoButtonView;
    public AdView mAd = null;
    public AlertDialog messageDialog;
    MobFoxView mobfoxView;
    public String publisherName;
    public ProgressDialog retrievePhotoFromFlickrProgressDialog;
    ImageButton saveToSD;
    ImageButton setAsWallpaper;
    int showFlurryCatalogAfterXPictures = -1;
    Animation slideInAnimation;
    Animation slideOutAnimation;
    public boolean testMode = true;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ExceptionHandler.register(this, "http://www.appastrophe.com/android-remote-stacktrace.php");
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.main);
        ngyhhqzdmxqvjwt();
        if (!connectedToInternet()) {
            this.alertDialog = new AlertDialog.Builder(this).create();
            this.alertDialog.setTitle("Internet");
            this.alertDialog.setMessage("This app fetches photographs from the Internet and it appears you are not connected. Please check your connection and relaunch.");
            this.alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    Main.this.finish();
                }
            });
            this.alertDialog.show();
            return;
        }
        if (flickr == null) {
            if (internetConnectionIsLowSpeed()) {
                this.alertDialog = new AlertDialog.Builder(this).create();
                this.alertDialog.setTitle("Internet");
                this.alertDialog.setMessage("You are on a low speed internet connection. \r\nImages loading might be slow and their quality might be poor depending on your ISP restriction.");
                this.alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                this.alertDialog.show();
            }
            String flurryId = "VCBQ8UW8B8SE283P19DH";
            if (0 != 0) {
                flurryId = "VCBQ8UW8B8SE283P19DH";
            }
            if (flurryId != "flurry_id") {
                this.testMode = false;
                if (0 != 0) {
                    this.testMode = true;
                }
                this.flurryIsActivated = true;
                this.flurryHookName = "com.appastrophe.flurry.catalog_hook";
                FlurryAgent.setCatalogIntentName("com.appastrophe.flurry.catalog_intent");
                FlurryAgent.enableAppCircle();
                FlurryAgent.setContinueSessionMillis(30000);
                FlurryAgent.setCaptureUncaughtExceptions(false);
                FlurryAgent.onStartSession(this, flurryId);
                this.appCircle = FlurryAgent.getAppCircle();
                String configurationContent = callSynchronousUrl("http://www.appastrophe.com/configuration.php");
                if (configurationContent == null) {
                    this.showFlurryCatalogAfterXPictures = -1;
                } else {
                    FlurryAgent.onEvent("Configuration file downloaded", null);
                    this.showFlurryCatalogAfterXPictures = Integer.valueOf(configurationContent.toString()).intValue();
                }
                this.flurryCatalogView = (RelativeLayout) findViewById(R.id.flurryCatalog);
                this.flurryAppcircleCloseButton = (ImageButton) findViewById(R.id.flurry_catalog_close_button);
                this.flurryAppcircleCloseButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Main.this.flurryCatalogView.startAnimation(Main.this.slideOutAnimation);
                    }
                });
                this.slideInAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_in);
                this.slideInAnimation.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationStart(Animation animation) {
                        Main.this.flurryCatalogView.setVisibility(0);
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationEnd(Animation animation) {
                    }
                });
                this.slideOutAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_out);
                this.slideOutAnimation.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationEnd(Animation animation) {
                        Main.this.flurryCatalogView.setVisibility(8);
                        Main.this.flurryCatalogView.clearAnimation();
                        if (Main.this.flurryIsActivated) {
                            FlurryAgent.onEvent("AppCircle - Closed catalog", null);
                        }
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationStart(Animation animation) {
                    }
                });
            }
            flickr = new Flickr();
        }
        this.gallery = (Gallery) findViewById(R.id.gallery);
        this.gallery.setAnimationDuration(1000);
        this.gallery.setAdapter((SpinnerAdapter) new WebViewAdapter(this, flickr));
        this.gallery.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        this.gallery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View arg1, int position, long id) {
                Main.currentGalleryPosition = Integer.valueOf(position);
                if (position == 0) {
                    Main.this.arrowLeft.setVisibility(8);
                }
                if (position > 0 && Main.this.gallery.getCount() > 1) {
                    Main.this.arrowLeft.setVisibility(0);
                }
                if (position == Main.this.gallery.getCount() - 1) {
                    Main.this.arrowRight.setVisibility(8);
                }
                if (position <= Main.this.gallery.getCount() - 2) {
                    Main.this.arrowRight.setVisibility(0);
                }
                if (Main.this.showFlurryCatalogAfterXPictures > -1 && position == Main.this.showFlurryCatalogAfterXPictures && Main.this.flurryIsActivated && Main.isIntentAvailable(Main.this, "android.intent.action.VIEW", "market://search?q=pub:" + Main.this.publisherName) && Main.toggleButtonAllIsOn) {
                    Main.this.showFlurryAppcircleCatalog("Tired of this app ?\r\nTry one of these !");
                }
                if ((position == Main.MENU_APPCIRCLE || position == 9 || position == 14 || position == 19 || position == 24 || position == 49 || position == 99 || position == 149 || position == 199 || position == 249) && Main.this.flurryIsActivated && Main.this.flurryIsActivated) {
                    FlurryAgent.onEvent("Viewed " + (position + 1) + "th picture", null);
                }
                if (position == Main.this.gallery.getCount() - 1 && Main.isIntentAvailable(Main.this, "android.intent.action.VIEW", "market://details?id=com.kayenko.awof") && Main.toggleButtonAllIsOn) {
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            new AlertDialog.Builder(Main.this).setIcon((int) R.drawable.awof).setTitle("a World of Faces").setMessage("You've reach the end but wait !\r\n\r\nIf you have a little more time to kill, you should definitely discover this incredible project application by Kayenko \r\n\r\nThey are trying to collect enough portraits of people from all around the world that if stitched together would physically wrap around the world. That's 40 millions portraits !!\r\n\r\nYou too can be part of this World Record attempt by trying this incredible app now ! ").setPositiveButton("Yes !", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    if (Main.this.flurryIsActivated) {
                                        FlurryAgent.onEvent("Menu - a World of Faces", null);
                                    }
                                    Main.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.kayenko.awof")));
                                }
                            }).setNegativeButton("No, thanks you.", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.cancel();
                                }
                            }).create().show();
                        }
                    }, 1500);
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.decimalFormat = new DecimalFormat("###,###");
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setGroupingSeparator(" ".toCharArray()[0]);
        this.decimalFormat.setDecimalFormatSymbols(decimalFormatSymbols);
        this.infoButtonView = (TextView) findViewById(R.id.infoView);
        this.infoButtonView.setOnClickListener(new View.OnClickListener() {
            /* Debug info: failed to restart local var, previous not found, register: 9 */
            public void onClick(View v) {
                Flickr.Photo currentLocalPhoto = Main.flickr.photoSet.getPhotoAtThisIndex(Main.this.gallery.getSelectedItemPosition());
                if (currentLocalPhoto != null) {
                    SpannableString linkifiedMessage = new SpannableString(("Author : " + currentLocalPhoto.author + "\n\n" + "Page : \n" + currentLocalPhoto.imageFlickrPageUrl + "\n\n" + "Viewed : " + Main.this.decimalFormat.format(Integer.valueOf(currentLocalPhoto.views)) + " times").trim());
                    Linkify.addLinks(linkifiedMessage, 1);
                    Main.this.messageDialog = new AlertDialog.Builder(Main.this).setIcon((int) R.drawable.icon).setTitle("Informations").setMessage(linkifiedMessage).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    }).create();
                    try {
                        Main.this.messageDialog.show();
                    } catch (Exception e) {
                        Log.e("Error", "Error : " + e.getMessage());
                        try {
                            Toast.makeText(Main.this, "An error occured, please try to restart the application", 1);
                        } catch (Exception e2) {
                            Log.e("Error", "Error : " + e2.getMessage());
                        }
                    }
                    ((TextView) Main.this.messageDialog.findViewById(16908299)).setMovementMethod(LinkMovementMethod.getInstance());
                }
            }
        });
        setNavigationBarListeners();
        if (!toggleButtonAllIsOn) {
            this.displayAllButton.setBackgroundResource(R.drawable.display_favorite);
            ((ImageButton) findViewById(R.id.favorite)).setBackgroundResource(R.drawable.favorite_disabled);
        }
        this.mAd = (AdView) findViewById(R.id.adView);
        this.mAd.setVisibility(8);
        this.mobfoxView = (MobFoxView) findViewById(R.id.mobFoxView);
        this.mobfoxView.setBannerListener(new BannerListener() {
            public void bannerLoadFailed(RequestException arg0) {
                Main.this.mobfoxView.post(new Runnable() {
                    public void run() {
                        Main.this.mobfoxView.setVisibility(8);
                        Main.this.mAd.loadAd(new AdRequest());
                        Main.this.mAd.setVisibility(0);
                    }
                });
            }

            public void noAdFound() {
                Main.this.mobfoxView.post(new Runnable() {
                    public void run() {
                        Main.this.mobfoxView.setVisibility(8);
                        Main.this.mAd.loadAd(new AdRequest());
                        Main.this.mAd.setVisibility(0);
                    }
                });
            }

            public void bannerLoadSucceeded() {
            }
        });
        this.mobfoxView.postDelayed(new Runnable() {
            public void run() {
                if (Main.this.mAd.getVisibility() == 8) {
                    Main.this.mobfoxView.setVisibility(0);
                }
            }
        }, 10000);
        this.fullScreenPhotoProgressDialog = new ProgressDialog(this);
        this.fullScreenPhotoProgressDialog.setIcon((int) R.drawable.icon);
        this.fullScreenPhotoProgressDialog.setTitle("Please wait while saving...");
        this.fullScreenPhotoProgressDialog.setProgressStyle(0);
        this.fullScreenPhotoProgressDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (event.getKeyCode() != Main.MENU_APPCIRCLE) {
                    return true;
                }
                Main.this.fullScreenPhotoProgressDialog.hide();
                return true;
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void showFlurryAppcircleCatalog(String title) {
        ((TextView) findViewById(R.id.flurryAppcircleTitle)).setText(title);
        this.flurryOffers = this.appCircle.getAllOffers(this.flurryHookName);
        if (this.flurryOffers.size() > 0) {
            ListView flurryList = (ListView) findViewById(R.id.flurry_list);
            flurryList.setAdapter((ListAdapter) new flurryCatalogAdapter(this, this.flurryOffers));
            flurryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View myView, int offerPosition, long mylng) {
                    if (Main.this.flurryIsActivated) {
                        Map<String, String> lArgs = new HashMap<>();
                        lArgs.put("stackTrace", Main.this.flurryOffers.get(offerPosition).getName());
                        FlurryAgent.onEvent("AppCircle - Accepted an offer", lArgs);
                    }
                    Main.this.appCircle.acceptOffer(Main.this, Main.this.flurryOffers.get(offerPosition).getId());
                }
            });
            if (this.flurryIsActivated) {
                FlurryAgent.onEvent("AppCircle - Showed catalog", null);
            }
            this.flurryCatalogView.setVisibility(0);
            this.flurryCatalogView.startAnimation(this.slideInAnimation);
        }
    }

    private void ngyhhqzdmxqvjwt() {
        int backgroundColor;
        RelativeLayout backgroundLayout = (RelativeLayout) findViewById(R.id.backgroundLayout);
        if (this.testMode) {
            backgroundColor = Color.rgb(0, 0, 0);
        } else {
            backgroundColor = Color.rgb(Integer.parseInt("52"), Integer.parseInt("26"), Integer.parseInt("93"));
        }
        backgroundLayout.setBackgroundColor(backgroundColor);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        if (!(currentGalleryPosition == null || this.gallery == null)) {
            this.gallery.setSelection(currentGalleryPosition.intValue());
            this.gallery.refreshDrawableState();
        }
        this.publisherName = getApplicationContext().getApplicationInfo().packageName;
        String[] parsedPackageName = this.publisherName.split("[.]");
        this.publisherName = parsedPackageName[1].substring(0, 1).toUpperCase() + parsedPackageName[1].substring(1);
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.mobfoxView != null) {
            this.mobfoxView.pause();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.mobfoxView != null) {
            this.mobfoxView.resume();
        }
    }

    public boolean onTrackballEvent(MotionEvent event) {
        return true;
    }

    private void setNavigationBarListeners() {
        this.arrowLeft = (ImageButton) findViewById(R.id.arrow_left);
        this.arrowLeft.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Main.this.gallery.onKeyDown(21, new KeyEvent(0, 0));
            }
        });
        this.arrowRight = (ImageButton) findViewById(R.id.arrow_right);
        this.arrowRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Main.this.gallery.onKeyDown(22, new KeyEvent(0, 0));
            }
        });
        ((ImageButton) findViewById(R.id.share)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Flickr.Photo currentLocalPhoto = Main.flickr.photoSet.getPhotoAtThisIndex(Main.this.gallery.getSelectedItemPosition());
                if (currentLocalPhoto != null) {
                    String author = currentLocalPhoto.author;
                    String url = currentLocalPhoto.imageFlickrPageUrl;
                    HashMap<String, String> parameters = new HashMap<>();
                    parameters.put("url", url);
                    if (Main.this.flurryIsActivated) {
                        FlurryAgent.onEvent("Share", parameters);
                    }
                    Intent intent = new Intent("android.intent.action.SEND");
                    intent.setType("text/plain");
                    intent.putExtra("android.intent.extra.SUBJECT", "Awsome picture, take a look !");
                    intent.putExtra("android.intent.extra.TEXT", "I found this awsome picture by " + author + " on my android phone : " + url + "\n\nCheck it out !!\n\n");
                    Main.this.startActivity(Intent.createChooser(intent, "Share this picture with..."));
                }
            }
        });
        ((ImageButton) findViewById(R.id.favorite)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Flickr.Photo currentLocalPhoto = Main.flickr.photoSet.getPhotoAtThisIndex(Main.this.gallery.getSelectedItemPosition());
                if (currentLocalPhoto == null) {
                    Toast.makeText(Main.this, "Sorry a problem occured, could not save the picture as favourite", 0).show();
                } else if (Main.toggleButtonAllIsOn) {
                    SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(Main.this.getBaseContext()).edit();
                    Time time = new Time();
                    time.setToNow();
                    prefEditor.putString(time.toString(), currentLocalPhoto.toString());
                    prefEditor.commit();
                    Toast.makeText(Main.this, "This picture has been added to your favourites. Toggle the 'ALL' button to browse only your favourite photos.", 1).show();
                }
            }
        });
        this.displayAllButton = (ImageButton) findViewById(R.id.display_all);
        this.displayAllButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!Main.toggleButtonAllIsOn) {
                    Main.toggleButtonAllIsOn = true;
                    Main.this.displayAllButton.setBackgroundResource(R.drawable.display_all);
                    ((ImageButton) Main.this.findViewById(R.id.favorite)).setBackgroundResource(R.drawable.favorite);
                    Main.flickr.retrievePhotos();
                    Main.this.gallery.setAdapter((SpinnerAdapter) new WebViewAdapter(Main.this, Main.flickr));
                    Main.this.gallery.setSelection(0);
                    Toast.makeText(Main.this, "You are now browsing the complete collection of photographs.", 0).show();
                } else if (PreferenceManager.getDefaultSharedPreferences(Main.this.getBaseContext()).getAll().size() == 0) {
                    Toast.makeText(Main.this, "You have not set any favourite yet. Use the Star button to add a picture to the favourites.", 1).show();
                } else {
                    Main.toggleButtonAllIsOn = false;
                    Main.this.displayAllButton.setBackgroundResource(R.drawable.display_favorite);
                    ((ImageButton) Main.this.findViewById(R.id.favorite)).setBackgroundResource(R.drawable.favorite_disabled);
                    Main.flickr.retrievePhotoSetFromFavourite();
                    Main.this.gallery.setAdapter((SpinnerAdapter) new WebViewAdapter(Main.this, Main.flickr));
                    Main.this.gallery.setSelection(0);
                    Toast.makeText(Main.this, "You are now browsing your favourite photographs.", 0).show();
                }
            }
        });
        this.saveToSD = (ImageButton) findViewById(R.id.save_to_sd);
        this.saveToSD.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Main.this.messageDialog = new AlertDialog.Builder(Main.this).setIcon((int) R.drawable.icon).setTitle("Save to SD card").setMessage("This will save the current picture to your SD card. Do you wish to proceed ?").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Flickr.Photo currentLocalPhoto = Main.flickr.photoSet.getPhotoAtThisIndex(Main.this.gallery.getSelectedItemPosition());
                        if (currentLocalPhoto != null) {
                            String title = currentLocalPhoto.title;
                            String url = currentLocalPhoto.imageUrl;
                            BitmapDownloader bitmapDownloader = new BitmapDownloader();
                            String[] strArr = new String[Main.MENU_APPCIRCLE];
                            strArr[0] = "SD";
                            strArr[1] = title;
                            strArr[2] = "Author : " + currentLocalPhoto.author + "\n\nLink to flickr page : " + currentLocalPhoto.imageFlickrPageUrl;
                            strArr[3] = url;
                            bitmapDownloader.execute(strArr);
                        }
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create();
                Main.this.messageDialog.show();
            }
        });
        this.setAsWallpaper = (ImageButton) findViewById(R.id.set_as_wallpaper);
        this.setAsWallpaper.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Main.this.messageDialog = new AlertDialog.Builder(Main.this).setIcon((int) R.drawable.icon).setTitle("Set as Wallpaper").setMessage("This will set the current picture as your wallpaper. Depending on your home screen your device might stretch the picture to fit the multiple screens. Do you wish to proceed ?").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Flickr.Photo currentLocalPhoto = Main.flickr.photoSet.getPhotoAtThisIndex(Main.this.gallery.getSelectedItemPosition());
                        if (currentLocalPhoto != null) {
                            String title = currentLocalPhoto.title;
                            String url = currentLocalPhoto.imageUrl;
                            BitmapDownloader bitmapDownloader = new BitmapDownloader();
                            String[] strArr = new String[Main.MENU_APPCIRCLE];
                            strArr[0] = "WP";
                            strArr[1] = title;
                            strArr[2] = "Author : " + currentLocalPhoto.author + "\n\nLink to flickr page : " + currentLocalPhoto.imageFlickrPageUrl;
                            strArr[3] = url;
                            bitmapDownloader.execute(strArr);
                        }
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create();
                Main.this.messageDialog.show();
            }
        });
    }

    private boolean connectedToInternet() {
        NetworkInfo networkInformation = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkInformation == null) {
            return false;
        }
        return networkInformation.isConnectedOrConnecting();
    }

    private boolean internetConnectionIsLowSpeed() {
        ConnectivityManager connec = (ConnectivityManager) getSystemService("connectivity");
        if (connec.getActiveNetworkInfo() == null) {
            return false;
        }
        if (connec.getActiveNetworkInfo().getType() == 1) {
            return false;
        }
        if (connec.getActiveNetworkInfo().getType() != 0) {
            return false;
        }
        switch (connec.getActiveNetworkInfo().getSubtype()) {
            case 0:
                return false;
            case 1:
                return true;
            case 2:
                return true;
            case 3:
                return false;
            default:
                return false;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == MENU_APPCIRCLE) {
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public static boolean isIntentAvailable(Context context, String action, String localUri) {
        return context.getPackageManager().queryIntentActivities(new Intent(action, Uri.parse(localUri)), 65536).size() > 0;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        if (isIntentAvailable(this, "android.intent.action.VIEW", "http://market.android.com/search?q=pname:com.kayenko.awof") && this.appCircle != null && this.flurryIsActivated) {
            menu.add(0, (int) MENU_APPCIRCLE, 1, "Recommended apps").setIcon((int) R.drawable.other_apps);
        }
        if (isIntentAvailable(this, "android.intent.action.VIEW", "market://search?q=pub:" + this.publisherName)) {
            menu.add(1, 2, 0, "Our other apps").setIcon((int) R.drawable.other_apps);
        }
        menu.add(1, 1, 1, "Quit").setIcon((int) R.drawable.quit);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                exitApplication();
                return true;
            case 2:
                if (this.flurryIsActivated) {
                    FlurryAgent.onEvent("Menu - View our other apps", null);
                }
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:" + this.publisherName)));
                return true;
            case 3:
            default:
                return false;
            case MENU_APPCIRCLE /*4*/:
                if (this.flurryIsActivated) {
                    FlurryAgent.onEvent("Menu - AppCircle", null);
                    showFlurryAppcircleCatalog("Recommended applications");
                }
                return true;
        }
    }

    private void exitApplication() {
        if (this.messageDialog != null) {
            this.messageDialog.dismiss();
        }
        if (this.alertDialog != null) {
            this.alertDialog.dismiss();
        }
        FlurryAgent.onEndSession(this);
        finish();
        System.exit(0);
    }

    public void consumeBitmap(Bitmap bitmap, String purpose) {
        if (purpose == "SD") {
            if (bitmap != null) {
                Toast.makeText(this, "This picture has been saved to your SD card", 1).show();
            } else {
                Toast.makeText(this, "An error has occured, the picture could not be saved.\r\nIs your SD card already in use or full ?", 1).show();
            }
        }
        if (purpose == "WP") {
            try {
                setWallpaper(bitmap);
                Toast.makeText(this, "This picture was set as your wallpaper.", 1).show();
            } catch (Exception e) {
                Log.e("consumeBitmap", "error", e.getCause());
                Toast.makeText(this, "An error occured, could not set this picture as a wallpaper", 1).show();
            }
        }
    }

    public class WebViewAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        Flickr mflickr;

        public WebViewAdapter(Context context, Flickr f) {
            this.mflickr = f;
            this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
        }

        public int getCount() {
            return Main.flickr.photoSet.photosList.size();
        }

        public Object getItem(int position) {
            return Main.flickr.photoSet.photosList.get(Main.this.gallery.getSelectedItemPosition());
        }

        public long getItemId(int position) {
            return Main.this.gallery.getSelectedItemId();
        }

        /* Debug info: failed to restart local var, previous not found, register: 9 */
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            Flickr.Photo currentLocalPhoto = Main.flickr.photoSet.getPhotoAtThisIndex(position);
            if (convertView == null) {
                try {
                    convertView = this.mInflater.inflate((int) R.layout.gallery_webview_item, (ViewGroup) null);
                    holder = new ViewHolder();
                    holder.webView = (WebView) convertView.findViewById(R.id.imageHolder);
                    if (currentLocalPhoto != null) {
                        holder.imageUrl = currentLocalPhoto.imageUrl;
                    } else {
                        holder.imageUrl = null;
                    }
                    holder.webView.setHorizontalScrollBarEnabled(false);
                    holder.webView.setVerticalScrollBarEnabled(false);
                    holder.webView.getSettings().setJavaScriptEnabled(true);
                    convertView.setTag(holder);
                } catch (InflateException e) {
                    TextView errorMessageView = new TextView(Main.this.getApplicationContext());
                    errorMessageView.setGravity(17);
                    errorMessageView.setText("A fatal error occured. \r\nUse the quit menu and relaunch the app.");
                    return errorMessageView;
                }
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            if (currentLocalPhoto == null) {
                holder.webView.loadData("<html><body style=\"background-color:#000000;margin:0px;padding:0px;text-align:center;\"><table align=\"center\" width=\"250\" id=\"loader\" cellpadding=\"0\" cellspacing=\"0\" ><tr><td valign=\"middle\" align=\"center\" bgcolor=\"#000000\"><font size=\"2\" color=\"#FFFFFF\">An error occured...</font></td></tr></table></body></html>", "text/html", Const.ENCODING);
            } else {
                holder.webView.loadData("<html><body style=\"background-color:#000000;margin:0px;padding:0px;text-align:center;\" onload=\"document.getElementById('loader').style.display='none';document.getElementById('photo').style.display='';\"><table align=\"center\" width=\"250\" id=\"loader\" cellpadding=\"0\" cellspacing=\"0\" ><tr><td valign=\"middle\" align=\"center\" bgcolor=\"#000000\"><font size=\"2\" color=\"#FFFFFF\">Please wait while loading...</font></td></tr></table><img id=\"photo\" style=\"display:none;\" width=\"100%25\" src=\"" + holder.imageUrl + "\"></body></html>", "text/html", Const.ENCODING);
            }
            return convertView;
        }
    }

    static class ViewHolder {
        String imageUrl;
        WebView webView;

        ViewHolder() {
        }
    }

    public class Flickr {
        private static final String FLICKR_PAGE_URL_FORMAT = "http://m.flickr.com/#/photos/{user-id}/{photo-id}";
        private static final String FLICKR_PICTURE_URL_FORMAT = "http://farm{farm-id}.static.flickr.com/{server-id}/{id}_{secret}_[mstb].jpg";
        public PhotoSet allPhotoSet;
        public PhotoSet photoSet = new PhotoSet();

        public Flickr() {
            retrievePhotos();
        }

        public void retrievePhotos() {
            if (this.allPhotoSet != null) {
                this.photoSet = this.allPhotoSet.clone();
                return;
            }
            InputStream xmlContent = Main.this.getResources().openRawResource(R.raw.photos);
            if (xmlContent == null) {
                Main.this.messageDialog = new AlertDialog.Builder(Main.this).setIcon((int) R.drawable.icon).setTitle("Internet error").setMessage("There was an error getting the photos.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        Main.this.finish();
                    }
                }).create();
                Main.this.messageDialog.show();
                return;
            }
            this.photoSet.reset();
            boolean exceptionError = false;
            try {
                NodeList pictureNodeList = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(xmlContent).getDocumentElement().getElementsByTagName("photo");
                for (int counter = 0; counter < pictureNodeList.getLength(); counter++) {
                    Element pE = (Element) pictureNodeList.item(counter);
                    this.photoSet.photosList.add(new Photo(pE.getAttribute("title"), pE.getAttribute("id"), pE.getAttribute("secret"), pE.getAttribute("server"), pE.getAttribute("farm"), pE.getAttribute("ownername"), pE.getAttribute("owner"), pE.getAttribute("views"), pE.getAttribute("o_width"), pE.getAttribute("o_height")));
                }
            } catch (Exception e) {
                exceptionError = true;
                Log.e("getPicturesList", "Exception " + e.getMessage());
            }
            if (this.photoSet.photosList.size() == 0 || exceptionError) {
                Main.this.messageDialog = new AlertDialog.Builder(Main.this).setIcon((int) R.drawable.icon).setTitle("Internet error").setMessage("There was an error getting the photos from Flickr. Please try again later.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        Main.this.finish();
                    }
                }).create();
                Main.this.messageDialog.show();
                return;
            }
            this.allPhotoSet = this.photoSet.clone();
            if (Main.this.flurryIsActivated) {
                FlurryAgent.onEvent("Get pictures list from flickr - OK", null);
            }
        }

        public void retrievePhotoSetFromFavourite() {
            this.photoSet.reset();
            Map<String, ?> allFavourites = PreferenceManager.getDefaultSharedPreferences(Main.this.getBaseContext()).getAll();
            Object[] keys = allFavourites.keySet().toArray();
            for (Object obj : keys) {
                Photo photo = new Photo();
                photo.loadFromString((String) allFavourites.get((String) obj));
                this.photoSet.photosList.add(photo);
            }
            if (Main.this.flurryIsActivated) {
                FlurryAgent.onEvent("Get pictures list from favourite - OK", null);
            }
        }

        class PhotoSet {
            ArrayList<Photo> photosList = new ArrayList<>();

            PhotoSet() {
            }

            public void reset() {
                this.photosList.clear();
            }

            /* Debug info: failed to restart local var, previous not found, register: 1 */
            public Photo getPhotoAtThisIndex(int index) {
                if (index >= this.photosList.size() || index == -1) {
                    return null;
                }
                return this.photosList.get(index);
            }

            public PhotoSet clone() {
                PhotoSet clonedPhotoSet = new PhotoSet();
                clonedPhotoSet.photosList = (ArrayList) this.photosList.clone();
                return clonedPhotoSet;
            }
        }

        class Photo {
            public String author;
            public String farmId;
            public int height;
            public String imageFlickrPageUrl;
            public String imageUrl;
            public String photoId;
            public String secret;
            public String serverId;
            public String title;
            public String userId;
            public String views;
            public int width;

            public Photo() {
            }

            public Photo(String title2, String id, String secret2, String serverId2, String farmId2, String author2, String userId2, String views2, String width2, String height2) {
                String[] values = new String[10];
                values[0] = title2;
                values[1] = id;
                values[2] = secret2;
                values[3] = serverId2;
                values[Main.MENU_APPCIRCLE] = farmId2;
                values[5] = author2;
                values[6] = userId2;
                values[7] = views2;
                values[8] = width2;
                values[9] = height2;
                setValues(values);
            }

            private void setValues(String[] values) {
                this.title = values[0];
                this.photoId = values[1];
                this.secret = values[2];
                this.serverId = values[3];
                this.farmId = values[Main.MENU_APPCIRCLE];
                this.author = values[5];
                this.userId = values[6];
                this.views = values[7];
                try {
                    this.width = Integer.parseInt(values[8]);
                    this.height = Integer.parseInt(values[9]);
                } catch (NumberFormatException e) {
                    this.width = 0;
                    this.height = 0;
                }
                this.imageUrl = computeImageUrl();
                this.imageFlickrPageUrl = computeImageFlickrPageUrl();
            }

            public String computeImageUrl() {
                return Flickr.FLICKR_PICTURE_URL_FORMAT.replace("{farm-id}", this.farmId).replace("{server-id}", this.serverId).replace("{id}", this.photoId).replace("{secret}", this.secret).replace("_[mstb]", "");
            }

            public String computeImageFlickrPageUrl() {
                return Flickr.FLICKR_PAGE_URL_FORMAT.replace("{user-id}", this.userId).replace("{photo-id}", this.photoId);
            }

            public String toString() {
                return ((((((((((this.title + "%$@%") + this.photoId + "%$@%") + this.secret + "%$@%") + this.serverId + "%$@%") + this.farmId + "%$@%") + this.author + "%$@%") + this.userId + "%$@%") + this.views + "%$@%") + this.width + "%$@%") + this.height + "%$@%").replace("\t", " ").replace("%$@%", "\t");
            }

            public void loadFromString(String fromString) {
                setValues(fromString.split("\t"));
            }
        }
    }

    class BitmapDownloader extends AsyncTask<String, Void, Bitmap> {
        String purpose;

        BitmapDownloader() {
        }

        /* access modifiers changed from: protected */
        public Bitmap doInBackground(String... data) {
            Bitmap bitmapFromUrl = null;
            this.purpose = data[0];
            try {
                HttpURLConnection connection = (HttpURLConnection) new URL(data[3]).openConnection();
                connection.setDoInput(true);
                connection.connect();
                bitmapFromUrl = BitmapFactory.decodeStream(connection.getInputStream());
                if (this.purpose == "SD" && bitmapFromUrl != null && saveBitmapToSD(bitmapFromUrl, data) == null) {
                    return null;
                }
            } catch (IOException e) {
                Log.e("doinbackground", e.getMessage());
            }
            return bitmapFromUrl;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            Main.this.fullScreenPhotoProgressDialog.show();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Bitmap bitmap) {
            Main.this.consumeBitmap(bitmap, this.purpose);
            Main.this.fullScreenPhotoProgressDialog.hide();
        }

        public Uri saveBitmapToSD(Bitmap bitmapToSave, String... data) {
            String title = data[1];
            String description = data[2];
            try {
                String path = Environment.getExternalStorageDirectory().toString() + "/" + Main.this.publisherName;
                if (new File(path).exists() || new File(path).mkdirs()) {
                    File file = new File(path, "flickr_" + System.currentTimeMillis() + ".png");
                    OutputStream fOut = new FileOutputStream(file);
                    bitmapToSave.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                    fOut.flush();
                    fOut.close();
                    Uri libraryBitmapUrl = Uri.parse(MediaStore.Images.Media.insertImage(Main.this.getContentResolver(), file.getAbsolutePath(), title, description));
                    Main.this.sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse("file://" + Environment.getExternalStorageDirectory())));
                    return libraryBitmapUrl;
                }
                Log.e("savetosd", "Dang : Wrong path " + path);
                return null;
            } catch (FileNotFoundException e) {
                return null;
            } catch (IOException e2) {
                return null;
            } catch (Exception e3) {
                return null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public String callSynchronousUrl(String url) {
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            httpClient.getParams().setParameter("http.socket.timeout", 3000);
            URI uri = new URI(url);
            HttpGet requestObject = new HttpGet();
            requestObject.setURI(uri);
            HttpResponse response = httpClient.execute(requestObject);
            if (response.getStatusLine().getStatusCode() != 200) {
                FlurryAgent.onError("CallSynchronousUrl", "There was an error getting file : " + url, getClass().getName());
                return null;
            }
            Header[] headers = response.getHeaders("showFlurryCatalogAfterXPictures");
            if (headers.length > 0) {
                return headers[0].getValue();
            }
            return null;
        } catch (IOException e) {
            Log.e("CallUrl", "I/O error");
            FlurryAgent.onError("callSynchronousUrl", "There was a I/O error getting the url : " + url, getClass().getName());
            return null;
        } catch (URISyntaxException e2) {
            Log.e("CallUrl", "URI syntax is invalid");
            FlurryAgent.onError("callSynchronousUrl", "There was an 'URI syntax is invalid' getting the url : " + url, getClass().getName());
            return null;
        }
    }

    class flurryCatalogAdapter extends BaseAdapter {
        List<Offer> flurryOffers;
        LayoutInflater inflater;

        flurryCatalogAdapter(Context context, List<Offer> offers) {
            this.flurryOffers = offers;
            this.inflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return this.flurryOffers.size();
        }

        public Offer getItem(int position) {
            return this.flurryOffers.get(position);
        }

        public long getItemId(int position) {
            return this.flurryOffers.get(position).getId();
        }

        /* Debug info: failed to restart local var, previous not found, register: 11 */
        public View getView(int position, View convertView, ViewGroup parent) {
            Drawable iconDrawable;
            String offerPrice;
            if (convertView == null) {
                convertView = this.inflater.inflate((int) R.layout.flurry_catalog_list_item, (ViewGroup) null);
            }
            Offer currentOffer = this.flurryOffers.get(position);
            byte[] iconByteArray = currentOffer.getImage().getImageData();
            Bitmap icon = BitmapFactory.decodeByteArray(iconByteArray, 0, iconByteArray.length);
            if (icon != null) {
                iconDrawable = new BitmapDrawable(icon);
            } else {
                iconDrawable = Main.this.getResources().getDrawable(R.drawable.flurry_default_icon);
            }
            ((ImageView) convertView.findViewById(R.id.offerImage)).setBackgroundDrawable(iconDrawable);
            ((TextView) convertView.findViewById(R.id.offerTitle)).setText(currentOffer.getName());
            String description = currentOffer.getDescription();
            if (description.length() > 0) {
                String description2 = description.replace("\r\n", " ");
                int dotPos = description2.indexOf(". ");
                if (dotPos == -1) {
                    dotPos = description2.length() - 1;
                }
                description = description2.substring(0, dotPos + 1).trim();
            }
            ((TextView) convertView.findViewById(R.id.offerDescription)).setText(description);
            if (currentOffer.getPrice() == 0) {
                offerPrice = "FREE !";
            } else {
                offerPrice = "$" + String.valueOf(((float) currentOffer.getPrice()) / 100.0f);
            }
            ((TextView) convertView.findViewById(R.id.offerPrice)).setText(offerPrice);
            return convertView;
        }
    }
}
