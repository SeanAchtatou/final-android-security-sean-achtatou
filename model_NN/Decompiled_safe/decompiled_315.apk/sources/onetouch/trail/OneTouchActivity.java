package onetouch.trail;

import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class OneTouchActivity extends Activity {
    private final String AD_UNIT_ID = "a14e41455da4113";
    public int ThemeId;
    private AdView adView;
    /* access modifiers changed from: private */
    public Dialog alertDialog;
    private Thread alertThread;
    /* access modifiers changed from: private */
    public AudioManager audioSilent;
    /* access modifiers changed from: private */
    public RadioButton blue;
    /* access modifiers changed from: private */
    public BluetoothAdapter bluetooth;
    /* access modifiers changed from: private */
    public Chronometer clock;
    /* access modifiers changed from: private */
    public Dialog dialog;
    private Drawable drawable;
    /* access modifiers changed from: private */
    public boolean flightEnable;
    /* access modifiers changed from: private */
    public Intent flightIntent;
    /* access modifiers changed from: private */
    public RadioButton green;
    private ImageView im_blue;
    private ImageView im_bright;
    private ImageView im_flight;
    private ImageView im_gps;
    private ImageView im_manage;
    private ImageView im_silent;
    private ImageView im_sync;
    private ImageView im_wifi;
    private Button manage;
    /* access modifiers changed from: private */
    public RadioButton orange;
    /* access modifiers changed from: private */
    public RadioButton pink;
    private LocationManager provider;
    /* access modifiers changed from: private */
    public RadioButton red;
    /* access modifiers changed from: private */
    public Button save;
    /* access modifiers changed from: private */
    public SeekBar seek_bright;
    /* access modifiers changed from: private */
    public TextView st_bluetooth;
    /* access modifiers changed from: private */
    public TextView st_flight;
    /* access modifiers changed from: private */
    public TextView st_gps;
    /* access modifiers changed from: private */
    public TextView st_silent;
    /* access modifiers changed from: private */
    public TextView st_sync;
    /* access modifiers changed from: private */
    public TextView st_wifi;
    private int storedPreference;
    /* access modifiers changed from: private */
    public TextView t_battery_stat;
    /* access modifiers changed from: private */
    public ToggleButton tg_bluetooth;
    private ToggleButton tg_flight;
    private ToggleButton tg_gps;
    /* access modifiers changed from: private */
    public ToggleButton tg_silent;
    private ToggleButton tg_sync;
    /* access modifiers changed from: private */
    public ToggleButton tg_wifi;
    private Button theme;
    /* access modifiers changed from: private */
    public RadioButton violet;
    /* access modifiers changed from: private */
    public RadioButton white;
    /* access modifiers changed from: private */
    public WifiManager wifi;
    /* access modifiers changed from: private */
    public RadioButton yellow;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        trailAlert();
        this.im_flight = (ImageView) findViewById(R.id.im_flight);
        this.im_blue = (ImageView) findViewById(R.id.im_bluetooth);
        this.im_wifi = (ImageView) findViewById(R.id.im_wifi);
        this.im_gps = (ImageView) findViewById(R.id.im_gps);
        this.im_bright = (ImageView) findViewById(R.id.im_bright);
        this.im_sync = (ImageView) findViewById(R.id.im_sync);
        this.im_silent = (ImageView) findViewById(R.id.im_silent);
        this.im_manage = (ImageView) findViewById(R.id.im_manage);
        this.theme = (Button) findViewById(R.id.bt_theme);
        this.manage = (Button) findViewById(R.id.bt_manage);
        this.t_battery_stat = (TextView) findViewById(R.id.t_battery_stat);
        this.seek_bright = (SeekBar) findViewById(R.id.seek_bright);
        this.storedPreference = getPreferences(0).getInt("ThemeId", 219);
        switch (this.storedPreference) {
            case 0:
                whiteTheme();
                break;
            case 1:
                blueTheme();
                break;
            case 2:
                redTheme();
                break;
            case 3:
                greenTheme();
                break;
            case 4:
                yellowTheme();
                break;
            case 5:
                pinkTheme();
                break;
            case 6:
                orangeTheme();
                break;
            case 7:
                violetTheme();
                break;
            default:
                redTheme();
                break;
        }
        this.st_flight = (TextView) findViewById(R.id.st_flight);
        this.st_bluetooth = (TextView) findViewById(R.id.st_bluetooth);
        this.st_wifi = (TextView) findViewById(R.id.st_wifi);
        this.st_gps = (TextView) findViewById(R.id.st_gps);
        this.st_sync = (TextView) findViewById(R.id.st_sync);
        this.st_silent = (TextView) findViewById(R.id.st_silent);
        this.tg_flight = (ToggleButton) findViewById(R.id.tg_flight);
        this.tg_bluetooth = (ToggleButton) findViewById(R.id.tg_bluetooth);
        this.tg_wifi = (ToggleButton) findViewById(R.id.tg_wifi);
        this.tg_gps = (ToggleButton) findViewById(R.id.tg_gps);
        this.tg_sync = (ToggleButton) findViewById(R.id.tg_sync);
        this.tg_silent = (ToggleButton) findViewById(R.id.tg_silent);
        this.dialog = new Dialog(this);
        this.dialog.setContentView((int) R.layout.theme_layout);
        this.white = (RadioButton) this.dialog.findViewById(R.id.white);
        this.blue = (RadioButton) this.dialog.findViewById(R.id.blue);
        this.red = (RadioButton) this.dialog.findViewById(R.id.red);
        this.green = (RadioButton) this.dialog.findViewById(R.id.green);
        this.yellow = (RadioButton) this.dialog.findViewById(R.id.yellow);
        this.pink = (RadioButton) this.dialog.findViewById(R.id.pink);
        this.orange = (RadioButton) this.dialog.findViewById(R.id.orange);
        this.violet = (RadioButton) this.dialog.findViewById(R.id.violet);
        this.save = (Button) this.dialog.findViewById(R.id.bt_ok);
        try {
            this.provider = (LocationManager) getSystemService("location");
            this.bluetooth = BluetoothAdapter.getDefaultAdapter();
            this.flightIntent = new Intent("android.intent.action.AIRPLANE_MODE");
            this.wifi = (WifiManager) getApplicationContext().getSystemService("wifi");
            this.audioSilent = (AudioManager) getApplicationContext().getSystemService("audio");
            if (this.provider.isProviderEnabled("gps")) {
                this.tg_gps.setChecked(true);
                this.st_gps.setText((int) R.string.st_gps_status);
            }
            if (Settings.System.getInt(getApplicationContext().getContentResolver(), "airplane_mode_on", 0) != 0) {
                this.tg_flight.setChecked(true);
                this.st_flight.setText((int) R.string.st_flight_status);
            }
            if (this.wifi.isWifiEnabled()) {
                this.tg_wifi.setChecked(true);
                this.st_wifi.setText((int) R.string.st_wifi_status);
            }
            if (ContentResolver.getMasterSyncAutomatically()) {
                this.tg_sync.setChecked(true);
                this.st_sync.setText((int) R.string.st_sync_status);
            }
            this.seek_bright.setProgress(Settings.System.getInt(getApplicationContext().getContentResolver(), "screen_brightness", 0) - 30);
            if (this.audioSilent.getRingerMode() == 1) {
                this.tg_silent.setChecked(true);
                this.st_silent.setText((int) R.string.st_silent_status);
            }
            registerReceiver(new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    if (!intent.getAction().contentEquals("android.bluetooth.adapter.action.STATE_CHANGED")) {
                        return;
                    }
                    if (OneTouchActivity.this.bluetooth.isEnabled()) {
                        OneTouchActivity.this.tg_bluetooth.setChecked(true);
                        OneTouchActivity.this.st_bluetooth.setText((int) R.string.st_bluetooth_status);
                        return;
                    }
                    OneTouchActivity.this.tg_bluetooth.setChecked(false);
                    if (OneTouchActivity.this.bluetooth.getState() != 11) {
                        OneTouchActivity.this.st_bluetooth.setText((int) R.string.st_bluetooth);
                    }
                }
            }, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
            if (this.bluetooth.isEnabled()) {
                this.tg_bluetooth.setChecked(true);
                this.st_bluetooth.setText((int) R.string.st_bluetooth_status);
            }
            registerReceiver(new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    if (intent.getAction().contains("WIFI_STATE_CHANGED")) {
                        if (OneTouchActivity.this.wifi.isWifiEnabled()) {
                            OneTouchActivity.this.tg_wifi.setChecked(true);
                            OneTouchActivity.this.st_wifi.setText((int) R.string.st_wifi_status);
                        }
                        if (OneTouchActivity.this.wifi.getWifiState() == 0) {
                            OneTouchActivity.this.tg_wifi.setChecked(false);
                            OneTouchActivity.this.st_wifi.setText((int) R.string.st_wifi);
                        }
                    }
                }
            }, new IntentFilter("android.net.wifi.WIFI_STATE_CHANGED"));
            registerReceiver(new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    if (!intent.getAction().contentEquals("android.media.RINGER_MODE_CHANGED")) {
                        return;
                    }
                    if (OneTouchActivity.this.audioSilent.getRingerMode() == 1) {
                        OneTouchActivity.this.tg_silent.setChecked(true);
                        OneTouchActivity.this.st_silent.setText((int) R.string.st_silent_status);
                        return;
                    }
                    OneTouchActivity.this.tg_silent.setChecked(false);
                    OneTouchActivity.this.st_silent.setText((int) R.string.st_silent);
                }
            }, new IntentFilter("android.media.RINGER_MODE_CHANGED"));
            registerReceiver(new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    if (intent.getAction().contains("screen_brightness")) {
                        OneTouchActivity.this.seek_bright.setProgress(Settings.System.getInt(OneTouchActivity.this.getApplicationContext().getContentResolver(), "screen_brightness", 0));
                    }
                }
            }, new IntentFilter("screen_brightness"));
            registerReceiver(new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    if (intent.getAction().equals("android.intent.action.BATTERY_CHANGED")) {
                        OneTouchActivity.this.t_battery_stat.setText("Available Battery : " + intent.getIntExtra("level", 0) + "%");
                    }
                }
            }, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            this.tg_flight.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        OneTouchActivity.this.flightEnable = true;
                        Settings.System.putInt(OneTouchActivity.this.getApplicationContext().getContentResolver(), "airplane_mode_on", 1);
                        OneTouchActivity.this.flightIntent.putExtra("state", OneTouchActivity.this.flightEnable);
                        OneTouchActivity.this.sendBroadcast(OneTouchActivity.this.flightIntent);
                        OneTouchActivity.this.st_flight.setText((int) R.string.st_flight_status);
                        return;
                    }
                    OneTouchActivity.this.flightEnable = false;
                    Settings.System.putInt(OneTouchActivity.this.getApplicationContext().getContentResolver(), "airplane_mode_on", 0);
                    OneTouchActivity.this.flightIntent.putExtra("state", OneTouchActivity.this.flightEnable);
                    OneTouchActivity.this.sendBroadcast(OneTouchActivity.this.flightIntent);
                    OneTouchActivity.this.st_flight.setText((int) R.string.st_flight);
                }
            });
            this.tg_bluetooth.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        OneTouchActivity.this.bluetooth.enable();
                        OneTouchActivity.this.st_bluetooth.setText((int) R.string.st_on);
                        return;
                    }
                    OneTouchActivity.this.bluetooth.disable();
                }
            });
            this.tg_wifi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        OneTouchActivity.this.wifi.setWifiEnabled(true);
                        OneTouchActivity.this.st_wifi.setText((int) R.string.st_on);
                        return;
                    }
                    OneTouchActivity.this.wifi.setWifiEnabled(false);
                }
            });
            this.tg_gps.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        if (!Settings.Secure.getString(OneTouchActivity.this.getContentResolver(), "location_providers_allowed").contains("gps")) {
                            Intent poke = new Intent();
                            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
                            poke.addCategory("android.intent.category.ALTERNATIVE");
                            poke.setData(Uri.parse("3"));
                            OneTouchActivity.this.sendBroadcast(poke);
                            OneTouchActivity.this.st_gps.setText((int) R.string.st_gps_status);
                        }
                    } else if (Settings.Secure.getString(OneTouchActivity.this.getContentResolver(), "location_providers_allowed").contains("gps")) {
                        Intent poke2 = new Intent();
                        poke2.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
                        poke2.addCategory("android.intent.category.ALTERNATIVE");
                        poke2.setData(Uri.parse("3"));
                        OneTouchActivity.this.sendBroadcast(poke2);
                        OneTouchActivity.this.st_gps.setText((int) R.string.st_gps);
                    }
                }
            });
            this.seek_bright.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                public void onStopTrackingTouch(SeekBar seekBar) {
                }

                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    Settings.System.putInt(OneTouchActivity.this.getApplicationContext().getContentResolver(), "screen_brightness", progress);
                }
            });
            this.tg_sync.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        ContentResolver.setMasterSyncAutomatically(true);
                        OneTouchActivity.this.st_sync.setText((int) R.string.st_sync_status);
                        return;
                    }
                    ContentResolver.setMasterSyncAutomatically(false);
                    OneTouchActivity.this.st_sync.setText((int) R.string.st_sync);
                }
            });
            this.tg_silent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        OneTouchActivity.this.audioSilent.setRingerMode(1);
                        OneTouchActivity.this.st_silent.setText((int) R.string.st_silent_status);
                        return;
                    }
                    OneTouchActivity.this.audioSilent.setRingerMode(2);
                    OneTouchActivity.this.st_silent.setText((int) R.string.st_silent);
                }
            });
            this.manage.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ComponentName toLaunch = new ComponentName("com.android.settings", "com.android.settings.ManageApplications");
                    Intent intent = new Intent("android.intent.action.MAIN");
                    intent.addCategory("android.intent.category.LAUNCHER");
                    intent.setComponent(toLaunch);
                    intent.setFlags(270532608);
                    OneTouchActivity.this.startActivity(intent);
                }
            });
            this.theme.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    OneTouchActivity.this.dialog.show();
                    OneTouchActivity.this.trailAlert();
                    OneTouchActivity.this.save.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            if (OneTouchActivity.this.white.isChecked()) {
                                OneTouchActivity.this.whiteTheme();
                                OneTouchActivity.this.dialog.dismiss();
                            } else if (OneTouchActivity.this.blue.isChecked()) {
                                OneTouchActivity.this.blueTheme();
                                OneTouchActivity.this.dialog.dismiss();
                            } else if (OneTouchActivity.this.red.isChecked()) {
                                OneTouchActivity.this.redTheme();
                                OneTouchActivity.this.dialog.dismiss();
                            } else if (OneTouchActivity.this.green.isChecked()) {
                                OneTouchActivity.this.greenTheme();
                                OneTouchActivity.this.dialog.dismiss();
                            } else if (OneTouchActivity.this.yellow.isChecked()) {
                                OneTouchActivity.this.yellowTheme();
                                OneTouchActivity.this.dialog.dismiss();
                            } else if (OneTouchActivity.this.pink.isChecked()) {
                                OneTouchActivity.this.pinkTheme();
                                OneTouchActivity.this.dialog.dismiss();
                            } else if (OneTouchActivity.this.orange.isChecked()) {
                                OneTouchActivity.this.orangeTheme();
                                OneTouchActivity.this.dialog.dismiss();
                            } else if (OneTouchActivity.this.violet.isChecked()) {
                                OneTouchActivity.this.violetTheme();
                                OneTouchActivity.this.dialog.dismiss();
                            }
                        }
                    });
                }
            });
        } catch (Exception e) {
        }
    }

    public void whiteTheme() {
        this.im_flight.setImageResource(R.drawable.w_flight_mode);
        this.im_blue.setImageResource(R.drawable.w_bluetooth);
        this.im_wifi.setImageResource(R.drawable.w_wifi);
        this.im_gps.setImageResource(R.drawable.w_gps);
        this.im_bright.setImageResource(R.drawable.w_brightness);
        this.im_sync.setImageResource(R.drawable.w_sync);
        this.im_silent.setImageResource(R.drawable.w_silent);
        this.im_manage.setImageResource(R.drawable.w_manage);
        this.ThemeId = 0;
        this.theme.getBackground().setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.MULTIPLY);
        this.manage.getBackground().setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.MULTIPLY);
        this.theme.setTextColor(-16777216);
        this.manage.setTextColor(-16777216);
        this.drawable = getResources().getDrawable(R.drawable.cyan_seekbar);
        this.seek_bright.setProgressDrawable(this.drawable);
    }

    public void blueTheme() {
        this.im_flight.setImageResource(R.drawable.b_flight_mode);
        this.im_blue.setImageResource(R.drawable.b_bluetooth);
        this.im_wifi.setImageResource(R.drawable.b_wifi);
        this.im_gps.setImageResource(R.drawable.b_gps);
        this.im_bright.setImageResource(R.drawable.b_brightness);
        this.im_sync.setImageResource(R.drawable.b_sync);
        this.im_silent.setImageResource(R.drawable.b_silent);
        this.im_manage.setImageResource(R.drawable.b_manage);
        this.ThemeId = 1;
        this.theme.getBackground().setColorFilter(Color.parseColor("#1151C6"), PorterDuff.Mode.MULTIPLY);
        this.manage.getBackground().setColorFilter(Color.parseColor("#1151C6"), PorterDuff.Mode.MULTIPLY);
        this.theme.setTextColor(-1);
        this.manage.setTextColor(-1);
        this.drawable = getResources().getDrawable(R.drawable.blue_seekbar);
        this.seek_bright.setProgressDrawable(this.drawable);
    }

    public void redTheme() {
        this.im_flight.setImageResource(R.drawable.r_flight_mode);
        this.im_blue.setImageResource(R.drawable.r_bluetooth);
        this.im_wifi.setImageResource(R.drawable.r_wifi);
        this.im_gps.setImageResource(R.drawable.r_gps);
        this.im_bright.setImageResource(R.drawable.r_brightness);
        this.im_sync.setImageResource(R.drawable.r_sync);
        this.im_silent.setImageResource(R.drawable.r_silent);
        this.im_manage.setImageResource(R.drawable.r_manage);
        this.ThemeId = 2;
        this.theme.getBackground().setColorFilter(Color.parseColor("#F60516"), PorterDuff.Mode.MULTIPLY);
        this.manage.getBackground().setColorFilter(Color.parseColor("#F60516"), PorterDuff.Mode.MULTIPLY);
        this.theme.setTextColor(-1);
        this.manage.setTextColor(-1);
        this.drawable = getResources().getDrawable(R.drawable.red_seekbar);
        this.seek_bright.setProgressDrawable(this.drawable);
    }

    public void greenTheme() {
        this.im_flight.setImageResource(R.drawable.flight_mode);
        this.im_blue.setImageResource(R.drawable.bluetooth);
        this.im_wifi.setImageResource(R.drawable.wifi);
        this.im_gps.setImageResource(R.drawable.gps);
        this.im_bright.setImageResource(R.drawable.brightness);
        this.im_sync.setImageResource(R.drawable.sync);
        this.im_silent.setImageResource(R.drawable.silent);
        this.im_manage.setImageResource(R.drawable.manage);
        this.ThemeId = 3;
        this.theme.getBackground().setColorFilter(Color.parseColor("#40C611"), PorterDuff.Mode.MULTIPLY);
        this.manage.getBackground().setColorFilter(Color.parseColor("#40C611"), PorterDuff.Mode.MULTIPLY);
        this.theme.setTextColor(-1);
        this.manage.setTextColor(-1);
        this.drawable = getResources().getDrawable(R.drawable.green_seekbar);
        this.seek_bright.setProgressDrawable(this.drawable);
    }

    public void yellowTheme() {
        this.im_flight.setImageResource(R.drawable.y_flight_mode);
        this.im_blue.setImageResource(R.drawable.y_bluetooth);
        this.im_wifi.setImageResource(R.drawable.y_wifi);
        this.im_gps.setImageResource(R.drawable.y_gps);
        this.im_bright.setImageResource(R.drawable.y_brightness);
        this.im_sync.setImageResource(R.drawable.y_sync);
        this.im_silent.setImageResource(R.drawable.y_silent);
        this.im_manage.setImageResource(R.drawable.y_manage);
        this.ThemeId = 4;
        this.theme.getBackground().setColorFilter(Color.parseColor("#B3B507"), PorterDuff.Mode.MULTIPLY);
        this.manage.getBackground().setColorFilter(Color.parseColor("#B3B507"), PorterDuff.Mode.MULTIPLY);
        this.theme.setTextColor(-1);
        this.manage.setTextColor(-1);
        this.drawable = getResources().getDrawable(R.drawable.yellow_seekbar);
        this.seek_bright.setProgressDrawable(this.drawable);
    }

    public void pinkTheme() {
        this.im_flight.setImageResource(R.drawable.p_flight_mode);
        this.im_blue.setImageResource(R.drawable.p_bluetooth);
        this.im_wifi.setImageResource(R.drawable.p_wifi);
        this.im_gps.setImageResource(R.drawable.p_gps);
        this.im_bright.setImageResource(R.drawable.p_brightness);
        this.im_sync.setImageResource(R.drawable.p_sync);
        this.im_silent.setImageResource(R.drawable.p_silent);
        this.im_manage.setImageResource(R.drawable.p_manage);
        this.ThemeId = 5;
        this.theme.getBackground().setColorFilter(Color.parseColor("#B507AB"), PorterDuff.Mode.MULTIPLY);
        this.manage.getBackground().setColorFilter(Color.parseColor("#B507AB"), PorterDuff.Mode.MULTIPLY);
        this.theme.setTextColor(-1);
        this.manage.setTextColor(-1);
        this.drawable = getResources().getDrawable(R.drawable.pink_seekbar);
        this.seek_bright.setProgressDrawable(this.drawable);
    }

    public void orangeTheme() {
        this.im_flight.setImageResource(R.drawable.o_flight_mode);
        this.im_blue.setImageResource(R.drawable.o_bluetooth);
        this.im_wifi.setImageResource(R.drawable.o_wifi);
        this.im_gps.setImageResource(R.drawable.o_gps);
        this.im_bright.setImageResource(R.drawable.o_brightness);
        this.im_sync.setImageResource(R.drawable.o_sync);
        this.im_silent.setImageResource(R.drawable.o_silent);
        this.im_manage.setImageResource(R.drawable.o_manage);
        this.ThemeId = 6;
        this.theme.getBackground().setColorFilter(Color.parseColor("#D98500"), PorterDuff.Mode.MULTIPLY);
        this.manage.getBackground().setColorFilter(Color.parseColor("#D98500"), PorterDuff.Mode.MULTIPLY);
        this.theme.setTextColor(-1);
        this.manage.setTextColor(-1);
        this.drawable = getResources().getDrawable(R.drawable.orange_seekbar);
        this.seek_bright.setProgressDrawable(this.drawable);
    }

    public void violetTheme() {
        this.im_flight.setImageResource(R.drawable.v_flight_mode);
        this.im_blue.setImageResource(R.drawable.v_bluetooth);
        this.im_wifi.setImageResource(R.drawable.v_wifi);
        this.im_gps.setImageResource(R.drawable.v_gps);
        this.im_bright.setImageResource(R.drawable.v_brightness);
        this.im_sync.setImageResource(R.drawable.v_sync);
        this.im_silent.setImageResource(R.drawable.v_silent);
        this.im_manage.setImageResource(R.drawable.v_manage);
        this.ThemeId = 7;
        this.theme.getBackground().setColorFilter(Color.parseColor("#921EEC"), PorterDuff.Mode.MULTIPLY);
        this.manage.getBackground().setColorFilter(Color.parseColor("#921EEC"), PorterDuff.Mode.MULTIPLY);
        this.theme.setTextColor(-1);
        this.manage.setTextColor(-1);
        this.drawable = getResources().getDrawable(R.drawable.violet_seekbar);
        this.seek_bright.setProgressDrawable(this.drawable);
    }

    public void onStop() {
        super.onStop();
        SharedPreferences.Editor editor = getPreferences(0).edit();
        editor.putInt("ThemeId", this.ThemeId);
        editor.commit();
    }

    public void trailAlert() {
        this.alertDialog = new Dialog(this);
        this.alertDialog.setContentView((int) R.layout.alertdialog);
        this.alertDialog.setCancelable(false);
        this.clock = (Chronometer) this.alertDialog.findViewById(R.id.timer);
        this.adView = new AdView(this, AdSize.BANNER, "a14e41455da4113");
        ((LinearLayout) this.alertDialog.findViewById(R.id.linearLayout)).addView(this.adView);
        this.adView.loadAd(new AdRequest());
        this.alertDialog.show();
        ((Button) this.alertDialog.findViewById(R.id.buy)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse("market://details?id=onetouch.pro"));
                OneTouchActivity.this.startActivity(intent);
            }
        });
        this.alertThread = new Thread() {
            public void run() {
                try {
                    OneTouchActivity.this.clock.setBase(SystemClock.elapsedRealtime());
                    OneTouchActivity.this.clock.start();
                    sleep(16000);
                    OneTouchActivity.this.clock.stop();
                    OneTouchActivity.this.alertDialog.dismiss();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        this.alertThread.start();
    }
}
