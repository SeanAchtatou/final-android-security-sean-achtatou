package com.omniture;

import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

public class AppMeasurementBaseSE extends AppMeasurementBaseSE13 {
    /* Debug info: failed to restart local var, previous not found, register: 3 */
    /* access modifiers changed from: protected */
    public HttpURLConnection requestConnect(String url) {
        try {
            URL requestURL = new URL(url);
            if (url.indexOf("https://") < 0) {
                return (HttpURLConnection) requestURL.openConnection();
            }
            HttpsURLConnection connectionSecure = (HttpsURLConnection) requestURL.openConnection();
            connectionSecure.setHostnameVerifier(new HostnameVerifier(this) {
                private final AppMeasurementBaseSE this$0;

                {
                    this.this$0 = r1;
                }

                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            return connectionSecure;
        } catch (Exception e) {
            return null;
        }
    }
}
