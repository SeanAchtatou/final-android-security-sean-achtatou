package com.omniture;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class AppMeasurementBaseSE13 extends AppMeasurementBase {
    Method java15_HttpURLConnection_setConnectTimeout = null;
    Method java15_HttpURLConnection_setReadTimeout = null;

    public AppMeasurementBaseSE13() {
        try {
            this.java15_HttpURLConnection_setConnectTimeout = Class.forName("HttpURLConnection").getMethod("setConnectTimeout", Integer.TYPE);
        } catch (Exception e) {
            this.java15_HttpURLConnection_setConnectTimeout = null;
        }
        try {
            this.java15_HttpURLConnection_setReadTimeout = Class.forName("HttpURLConnection").getMethod("setReadTimeout", Integer.TYPE);
        } catch (Exception e2) {
            this.java15_HttpURLConnection_setReadTimeout = null;
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        if (this.requestList != null) {
            synchronized (this.requestList) {
                this.requestList.notifyAll();
            }
        }
        super.finalize();
    }

    /* access modifiers changed from: protected */
    public Calendar getCalendar() {
        return new GregorianCalendar();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void offlineRequestListRead() {
        /*
            r7 = this;
            r4 = r7
            r1 = 0
            java.lang.String r5 = r4.offlineFilename
            if (r5 == 0) goto L_0x0033
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0034 }
            java.lang.String r5 = r4.offlineFilename     // Catch:{ Exception -> 0x0034 }
            r1.<init>(r5)     // Catch:{ Exception -> 0x0034 }
        L_0x000d:
            if (r1 == 0) goto L_0x0033
            boolean r5 = r1.exists()
            if (r5 == 0) goto L_0x0033
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0032 }
            java.io.FileReader r5 = new java.io.FileReader     // Catch:{ Exception -> 0x0032 }
            r5.<init>(r1)     // Catch:{ Exception -> 0x0032 }
            r2.<init>(r5)     // Catch:{ Exception -> 0x0032 }
        L_0x001f:
            java.lang.String r3 = r2.readLine()     // Catch:{ Exception -> 0x0032 }
            if (r3 == 0) goto L_0x0038
            java.util.Vector r5 = r4.requestList     // Catch:{ Exception -> 0x0032 }
            monitor-enter(r5)     // Catch:{ Exception -> 0x0032 }
            java.util.Vector r6 = r4.requestList     // Catch:{ all -> 0x002f }
            r6.add(r3)     // Catch:{ all -> 0x002f }
            monitor-exit(r5)     // Catch:{ all -> 0x002f }
            goto L_0x001f
        L_0x002f:
            r6 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x002f }
            throw r6     // Catch:{ Exception -> 0x0032 }
        L_0x0032:
            r5 = move-exception
        L_0x0033:
            return
        L_0x0034:
            r5 = move-exception
            r0 = r5
            r1 = 0
            goto L_0x000d
        L_0x0038:
            r2.close()     // Catch:{ Exception -> 0x0032 }
            r1.delete()     // Catch:{ Exception -> 0x0032 }
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.omniture.AppMeasurementBaseSE13.offlineRequestListRead():void");
    }

    /* access modifiers changed from: protected */
    public void offlineRequestListWrite() {
        File offlineFile;
        if (this.offlineFilename != null) {
            try {
                offlineFile = new File(this.offlineFilename);
            } catch (Exception e) {
                Exception exc = e;
                offlineFile = null;
            }
            if (offlineFile != null) {
                try {
                    BufferedWriter offlineOut = new BufferedWriter(new FileWriter(offlineFile));
                    for (int requestNum = 0; requestNum < this.requestList.size(); requestNum++) {
                        String request = (String) this.requestList.get(requestNum);
                        offlineOut.write(request, 0, request.length());
                        offlineOut.newLine();
                    }
                    offlineOut.close();
                } catch (Exception e2) {
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void offlineRequestListDelete() {
        File offlineFile;
        if (this.offlineFilename != null) {
            try {
                offlineFile = new File(this.offlineFilename);
            } catch (Exception e) {
                offlineFile = null;
            }
            if (offlineFile != null && offlineFile.exists()) {
                try {
                    offlineFile.delete();
                } catch (Exception e2) {
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public HttpURLConnection requestConnect(String url) {
        try {
            return (HttpURLConnection) new URL(url).openConnection();
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public boolean requestRequest(String request) {
        String value;
        HttpURLConnection connection = null;
        boolean requestSent = false;
        String[] requestParts = splitString("\t", request);
        if (requestParts.length > 0 && requestParts[0].length() > 0) {
            try {
                connection = requestConnect(requestParts[0]);
                if (connection != null) {
                    if (this.java15_HttpURLConnection_setConnectTimeout != null) {
                        this.java15_HttpURLConnection_setConnectTimeout.invoke(connection, new Integer(5000));
                    }
                    if (this.java15_HttpURLConnection_setReadTimeout != null) {
                        this.java15_HttpURLConnection_setReadTimeout.invoke(connection, new Integer(5000));
                    }
                    for (int requestPartNum = 1; requestPartNum < requestParts.length; requestPartNum += 2) {
                        String key = requestParts[requestPartNum];
                        if (!(key == null || key == "" || requestPartNum >= requestParts.length - 1 || (value = requestParts[requestPartNum + 1]) == null || value == "")) {
                            connection.setRequestProperty(key, value);
                        }
                    }
                    connection.getResponseCode();
                    requestSent = true;
                }
            } catch (Exception e) {
            }
            if (connection != null) {
                try {
                    connection.getInputStream().close();
                } catch (Exception e2) {
                }
            }
        }
        return requestSent;
    }
}
