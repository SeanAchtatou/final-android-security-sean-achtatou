package com.omniture;

import java.util.Calendar;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Random;
import java.util.Vector;

public class AppMeasurementBase extends AppMeasurement_Variables {
    protected int _1_referrer = 0;
    public String imageDimensions;
    public int maxRequestThreads = 1;
    public int maxRequestsPerThread = 50;
    public boolean offline = false;
    protected Vector requestList = null;
    protected int requestThreadID = 0;
    protected Hashtable requestThreads = null;
    public boolean sendFromServer = false;
    protected String target = "";
    public boolean usePlugins = false;
    protected String version = "JAVA-1.2";

    /* access modifiers changed from: protected */
    public boolean _hasDoPlugins() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void _doPlugins() {
    }

    /* access modifiers changed from: protected */
    public boolean _hasDoRequest() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean _doRequest(String url, Hashtable headers) {
        return true;
    }

    public void forceOffline() {
        this.offline = true;
        if (this.requestList != null) {
            synchronized (this.requestList) {
                this.requestList.notifyAll();
            }
        }
    }

    public void forceOnline() {
        this.offline = false;
        if (this.requestList != null) {
            synchronized (this.requestList) {
                this.requestList.notifyAll();
            }
        }
    }

    public AppMeasurementBase() {
        this.ssl = false;
        this.linkLeaveQueryString = false;
        this.debugTracking = false;
        this.charSet = "UTF-8";
        this.sendFromServer = true;
        this.contextData = new Hashtable();
        this.retrieveLightData = new Hashtable();
    }

    /* access modifiers changed from: protected */
    public void modulesUpdate() {
    }

    public void logDebug(String msg) {
        System.out.println(msg);
    }

    /* access modifiers changed from: protected */
    public boolean isString(Object value) {
        try {
            if (((String) value) != null) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public String toString(Object value) {
        return (String) value;
    }

    /* access modifiers changed from: protected */
    public boolean isInteger(Object value) {
        try {
            if (((Integer) value) != null) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public int toInteger(Object value) {
        return ((Integer) value).intValue();
    }

    /* access modifiers changed from: protected */
    public boolean isBoolean(Object value) {
        try {
            if (((Boolean) value) != null) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean toBoolean(Object value) {
        return ((Boolean) value).booleanValue();
    }

    public boolean isSet(boolean val) {
        return val;
    }

    public boolean isSet(int val) {
        if (val == 0) {
            return false;
        }
        return true;
    }

    public boolean isSet(float val) {
        if (((double) val) == 0.0d) {
            return false;
        }
        return true;
    }

    public boolean isSet(double val) {
        if (val == 0.0d) {
            return false;
        }
        return true;
    }

    public boolean isSet(String val) {
        if (val == null) {
            return false;
        }
        if (val.length() == 0) {
            return false;
        }
        return true;
    }

    public boolean isSet(Object val) {
        if (val == null) {
            return false;
        }
        if (isString(val)) {
            return isSet(toString(val));
        }
        if (isInteger(val)) {
            return isSet(toInteger(val));
        }
        if (isBoolean(val)) {
            return isSet(toBoolean(val));
        }
        return true;
    }

    public boolean isNumber(int num) {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean isNumber(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public String[] splitString(String delim, String str) {
        int delimPos = 0;
        int partNum = 1;
        while (true) {
            int delimPos2 = str.indexOf(delim, delimPos);
            if (delimPos2 < 0) {
                break;
            }
            partNum++;
            delimPos = delimPos2 + delim.length();
        }
        String[] partList = new String[partNum];
        if (partNum == 1) {
            partList[0] = str;
        } else {
            int partNum2 = 0;
            while (true) {
                int delimPos3 = str.indexOf(delim);
                if (delimPos3 < 0) {
                    break;
                }
                partList[partNum2] = str.substring(0, delimPos3);
                str = str.substring(delim.length() + delimPos3);
                partNum2++;
            }
            partList[partNum2] = str;
        }
        return partList;
    }

    public String joinArray(String delim, String[] partList) {
        String str = "";
        boolean isFirstValue = true;
        for (String append : partList) {
            if (!isFirstValue) {
                str = new StringBuffer().append(str).append(delim).toString();
            }
            str = new StringBuffer().append(str).append(append).toString();
            isFirstValue = false;
        }
        return str;
    }

    public String replace(String x, String o, String n) {
        String y = x;
        if (!isSet(y) || y.indexOf(o) < 0) {
            return y;
        }
        return joinArray(n, splitString(o, y));
    }

    public static String escape(String str) {
        if (str == null) {
            return null;
        }
        try {
            String str2 = new String(str.getBytes("UTF-8"), "ISO-8859-1");
            String newStr = "";
            int chNum = 0;
            while (chNum < str2.length()) {
                try {
                    char ch = str2.charAt(chNum);
                    if ((ch < 'a' || ch > 'z') && ((ch < 'A' || ch > 'Z') && ((ch < '0' || ch > '9') && ".-*_".indexOf(ch) <= -1))) {
                        String chHex = Integer.toString(ch, 16).toUpperCase();
                        if (chHex.length() == 1) {
                            chHex = new StringBuffer().append('0').append(chHex).toString();
                        }
                        newStr = new StringBuffer().append(newStr).append('%').append(chHex).toString();
                    } else {
                        newStr = new StringBuffer().append(newStr).append(ch).toString();
                    }
                    chNum++;
                } catch (Exception e) {
                    return null;
                }
            }
            return newStr;
        } catch (Exception e2) {
            Exception exc = e2;
            return null;
        }
    }

    public static String unescape(String str) {
        char c;
        if (str == null) {
            return null;
        }
        if (str.indexOf(37) < 0) {
            return str;
        }
        int chNum = 0;
        String newStr = "";
        while (chNum < str.length()) {
            try {
                char ch = str.charAt(chNum);
                if (ch == '%') {
                    String chHex = str.substring(chNum + 1, chNum + 3);
                    chNum += 3;
                    newStr = new StringBuffer().append(newStr).append((char) Integer.parseInt(chHex, 16)).toString();
                } else {
                    StringBuffer append = new StringBuffer().append(newStr);
                    if (ch == '+') {
                        c = ' ';
                    } else {
                        c = ch;
                    }
                    chNum++;
                    newStr = append.append(c).toString();
                }
            } catch (Exception e) {
                return null;
            }
        }
        return new String(newStr.getBytes("ISO-8859-1"), "UTF-8");
    }

    /* access modifiers changed from: protected */
    public Calendar getCalendar() {
        return Calendar.getInstance();
    }

    public double getTime() {
        return (double) getCalendar().getTime().getTime();
    }

    /* access modifiers changed from: protected */
    public boolean requestRequest(String request) {
        logDebug(request);
        return true;
    }

    /* access modifiers changed from: protected */
    public void offlineRequestListRead() {
    }

    /* access modifiers changed from: protected */
    public void offlineRequestListWrite() {
    }

    /* access modifiers changed from: protected */
    public void offlineRequestListDelete() {
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x006a, code lost:
        if (r0.trackOffline == false) goto L_0x0074;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0072, code lost:
        if (r0.offline != false) goto L_0x013e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x007a, code lost:
        if (r0.trackOffline == false) goto L_0x00b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0080, code lost:
        if (r10 <= 0.0d) goto L_0x00b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0088, code lost:
        if (r0.offlineThrottleDelay <= 0) goto L_0x00b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x008a, code lost:
        r5 = getTime() - r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x009d, code lost:
        if (r5 >= ((double) r0.offlineThrottleDelay)) goto L_0x00b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        java.lang.Thread.currentThread();
        java.lang.Thread.sleep((long) (((double) r0.offlineThrottleDelay) - r5));
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleRequestList() {
        /*
            r23 = this;
            r16 = r23
            java.lang.String r9 = ""
            r10 = 0
        L_0x0006:
            r15 = 0
            r0 = r16
            java.util.Vector r0 = r0.requestList
            r19 = r0
            monitor-enter(r19)
        L_0x000e:
            r0 = r16
            java.util.Vector r0 = r0.requestList     // Catch:{ all -> 0x0047 }
            r20 = r0
            boolean r20 = r20.isEmpty()     // Catch:{ all -> 0x0047 }
            if (r20 == 0) goto L_0x004a
            r0 = r16
            java.util.Hashtable r0 = r0.requestThreads     // Catch:{ Exception -> 0x01d2 }
            r20 = r0
            int r20 = r20.size()     // Catch:{ Exception -> 0x01d2 }
            r21 = 1
            r0 = r20
            r1 = r21
            if (r0 <= r1) goto L_0x002e
            monitor-exit(r19)     // Catch:{ all -> 0x0047 }
        L_0x002d:
            return
        L_0x002e:
            r0 = r16
            java.util.Vector r0 = r0.requestList     // Catch:{ Exception -> 0x01d2 }
            r20 = r0
            r21 = 1000(0x3e8, double:4.94E-321)
            r20.wait(r21)     // Catch:{ Exception -> 0x01d2 }
        L_0x0039:
            r0 = r16
            java.util.Vector r0 = r0.requestList     // Catch:{ all -> 0x0047 }
            r20 = r0
            boolean r20 = r20.isEmpty()     // Catch:{ all -> 0x0047 }
            if (r20 == 0) goto L_0x000e
            monitor-exit(r19)     // Catch:{ all -> 0x0047 }
            goto L_0x002d
        L_0x0047:
            r20 = move-exception
            monitor-exit(r19)     // Catch:{ all -> 0x0047 }
            throw r20
        L_0x004a:
            r0 = r16
            java.util.Vector r0 = r0.requestList     // Catch:{ all -> 0x0047 }
            r20 = r0
            r21 = 0
            java.lang.Object r12 = r20.elementAt(r21)     // Catch:{ all -> 0x0047 }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ all -> 0x0047 }
            r0 = r16
            java.util.Vector r0 = r0.requestList     // Catch:{ all -> 0x0047 }
            r20 = r0
            r21 = 0
            r20.removeElementAt(r21)     // Catch:{ all -> 0x0047 }
            monitor-exit(r19)     // Catch:{ all -> 0x0047 }
            r0 = r16
            boolean r0 = r0.trackOffline
            r19 = r0
            if (r19 == 0) goto L_0x0074
            r0 = r16
            boolean r0 = r0.offline
            r19 = r0
            if (r19 != 0) goto L_0x013e
        L_0x0074:
            r0 = r16
            boolean r0 = r0.trackOffline
            r19 = r0
            if (r19 == 0) goto L_0x00b7
            r19 = 0
            int r19 = (r10 > r19 ? 1 : (r10 == r19 ? 0 : -1))
            if (r19 <= 0) goto L_0x00b7
            r0 = r16
            int r0 = r0.offlineThrottleDelay
            r19 = r0
            if (r19 <= 0) goto L_0x00b7
            double r3 = r16.getTime()
            double r5 = r3 - r10
            r0 = r16
            int r0 = r0.offlineThrottleDelay
            r19 = r0
            r0 = r19
            double r0 = (double) r0
            r19 = r0
            int r19 = (r5 > r19 ? 1 : (r5 == r19 ? 0 : -1))
            if (r19 >= 0) goto L_0x00b7
            java.lang.Thread.currentThread()     // Catch:{ Exception -> 0x01cf }
            r0 = r16
            int r0 = r0.offlineThrottleDelay     // Catch:{ Exception -> 0x01cf }
            r19 = r0
            r0 = r19
            double r0 = (double) r0     // Catch:{ Exception -> 0x01cf }
            r19 = r0
            double r19 = r19 - r5
            r0 = r19
            long r0 = (long) r0     // Catch:{ Exception -> 0x01cf }
            r19 = r0
            java.lang.Thread.sleep(r19)     // Catch:{ Exception -> 0x01cf }
        L_0x00b7:
            r17 = 0
            r7 = 0
            boolean r19 = r16._hasDoRequest()
            if (r19 == 0) goto L_0x01b8
            java.lang.String r19 = "\t"
            r0 = r23
            r1 = r19
            r2 = r12
            java.lang.String[] r14 = r0.splitString(r1, r2)
            r0 = r14
            int r0 = r0.length
            r19 = r0
            if (r19 <= 0) goto L_0x012c
            r19 = 0
            r19 = r14[r19]
            int r19 = r19.length()
            if (r19 <= 0) goto L_0x012c
            r19 = 0
            r17 = r14[r19]
            r13 = 1
        L_0x00e0:
            r0 = r14
            int r0 = r0.length
            r19 = r0
            r0 = r13
            r1 = r19
            if (r0 >= r1) goto L_0x0120
            r8 = r14[r13]
            if (r8 == 0) goto L_0x011d
            java.lang.String r19 = ""
            r0 = r8
            r1 = r19
            if (r0 == r1) goto L_0x011d
            r0 = r14
            int r0 = r0.length
            r19 = r0
            r20 = 1
            int r19 = r19 - r20
            r0 = r13
            r1 = r19
            if (r0 >= r1) goto L_0x011d
            int r19 = r13 + 1
            r18 = r14[r19]
            if (r18 == 0) goto L_0x011d
            java.lang.String r19 = ""
            r0 = r18
            r1 = r19
            if (r0 == r1) goto L_0x011d
            if (r7 != 0) goto L_0x0116
            java.util.Hashtable r7 = new java.util.Hashtable
            r7.<init>()
        L_0x0116:
            r0 = r7
            r1 = r8
            r2 = r18
            r0.put(r1, r2)
        L_0x011d:
            int r13 = r13 + 2
            goto L_0x00e0
        L_0x0120:
            r0 = r16
            r1 = r17
            r2 = r7
            boolean r19 = r0._doRequest(r1, r2)
            if (r19 == 0) goto L_0x012c
            r15 = 1
        L_0x012c:
            if (r15 == 0) goto L_0x013e
            r0 = r16
            java.util.Vector r0 = r0.requestList
            r19 = r0
            int r19 = r19.size()
            if (r19 <= 0) goto L_0x01c4
            double r10 = r16.getTime()
        L_0x013e:
            if (r15 != 0) goto L_0x01c8
            r0 = r16
            boolean r0 = r0.trackOffline
            r19 = r0
            if (r19 == 0) goto L_0x0006
            r0 = r16
            java.util.Vector r0 = r0.requestList
            r19 = r0
            monitor-enter(r19)
            r0 = r16
            java.util.Vector r0 = r0.requestList     // Catch:{ all -> 0x01b5 }
            r20 = r0
            r21 = 0
            r0 = r20
            r1 = r12
            r2 = r21
            r0.insertElementAt(r1, r2)     // Catch:{ all -> 0x01b5 }
            r0 = r16
            java.util.Vector r0 = r0.requestList     // Catch:{ all -> 0x01b5 }
            r20 = r0
            boolean r20 = r20.isEmpty()     // Catch:{ all -> 0x01b5 }
            if (r20 != 0) goto L_0x01a7
            r0 = r16
            java.util.Vector r0 = r0.requestList     // Catch:{ all -> 0x01b5 }
            r20 = r0
            r0 = r16
            java.util.Vector r0 = r0.requestList     // Catch:{ all -> 0x01b5 }
            r21 = r0
            int r21 = r21.size()     // Catch:{ all -> 0x01b5 }
            r22 = 1
            int r21 = r21 - r22
            java.lang.Object r20 = r20.elementAt(r21)     // Catch:{ all -> 0x01b5 }
            r0 = r20
            r1 = r9
            if (r0 == r1) goto L_0x01a7
            r0 = r16
            java.util.Vector r0 = r0.requestList     // Catch:{ all -> 0x01b5 }
            r20 = r0
            r0 = r16
            java.util.Vector r0 = r0.requestList     // Catch:{ all -> 0x01b5 }
            r21 = r0
            int r21 = r21.size()     // Catch:{ all -> 0x01b5 }
            r22 = 1
            int r21 = r21 - r22
            java.lang.Object r12 = r20.elementAt(r21)     // Catch:{ all -> 0x01b5 }
            r0 = r12
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x01b5 }
            r9 = r0
            r16.offlineRequestListWrite()     // Catch:{ all -> 0x01b5 }
        L_0x01a7:
            r0 = r16
            java.util.Vector r0 = r0.requestList     // Catch:{ Exception -> 0x01cd }
            r20 = r0
            r21 = 500(0x1f4, double:2.47E-321)
            r20.wait(r21)     // Catch:{ Exception -> 0x01cd }
        L_0x01b2:
            monitor-exit(r19)     // Catch:{ all -> 0x01b5 }
            goto L_0x0006
        L_0x01b5:
            r20 = move-exception
            monitor-exit(r19)     // Catch:{ all -> 0x01b5 }
            throw r20
        L_0x01b8:
            r0 = r16
            r1 = r12
            boolean r19 = r0.requestRequest(r1)
            if (r19 == 0) goto L_0x012c
            r15 = 1
            goto L_0x012c
        L_0x01c4:
            r10 = 0
            goto L_0x013e
        L_0x01c8:
            r16.offlineRequestListDelete()
            goto L_0x0006
        L_0x01cd:
            r20 = move-exception
            goto L_0x01b2
        L_0x01cf:
            r19 = move-exception
            goto L_0x00b7
        L_0x01d2:
            r20 = move-exception
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.omniture.AppMeasurementBase.handleRequestList():void");
    }

    /* access modifiers changed from: protected */
    public void requestThreadStart() {
        if (!isSet(this.maxRequestThreads)) {
            this.maxRequestThreads = 1;
        }
        if (this.requestThreads == null) {
            this.requestThreads = new Hashtable();
        }
        int threadsNeeded = ((int) Math.ceil((double) (this.requestList.size() / this.maxRequestsPerThread))) + 1;
        synchronized (this.requestThreads) {
            while (this.requestThreads.size() < threadsNeeded && this.requestThreads.size() < this.maxRequestThreads) {
                if (this.requestThreads.size() <= 0) {
                    this.requestThreadID = 0;
                }
                Thread requestThread = new Thread(this, this, this.requestThreadID) {
                    public AppMeasurementBase _s = this.val$s;
                    private int _threadID = this.val$threadID;
                    private final AppMeasurementBase this$0;
                    private final AppMeasurementBase val$s;
                    private final int val$threadID;

                    {
                        this.this$0 = r2;
                        this.val$s = r3;
                        this.val$threadID = r4;
                    }

                    public void run() {
                        AppMeasurementBase s = this._s;
                        s.handleRequestList();
                        synchronized (s.requestThreads) {
                            s.requestThreads.remove(new Integer(this._threadID));
                        }
                    }
                };
                this.requestThreads.put(new Integer(this.requestThreadID), requestThread);
                requestThread.start();
                this.requestThreadID++;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void sendRequest(String request) {
        String value;
        if (this.requestList == null) {
            this.requestList = new Vector();
            offlineRequestListRead();
        }
        requestThreadStart();
        synchronized (this.requestList) {
            if (isSet(this.trackOffline)) {
                if (!isSet(this.offlineLimit)) {
                    this.offlineLimit = 10;
                }
                if (this.requestList.size() >= this.offlineLimit) {
                    this.requestList.removeElementAt(0);
                }
            }
            this.requestList.addElement(request);
            this.requestList.notifyAll();
        }
        if (isSet(this.debugTracking)) {
            String[] requestParts = splitString("\t", request);
            if (requestParts.length > 0 && requestParts[0].length() > 0) {
                String url = requestParts[0];
                String debug = new StringBuffer().append("AppMeasurement Debug: ").append(url).toString();
                String[] debugLines = splitString("&", splitString("\t", url)[0]);
                for (String unescape : debugLines) {
                    debug = new StringBuffer().append(debug).append("\n\t").append(unescape(unescape)).toString();
                }
                for (int requestPartNum = 1; requestPartNum < requestParts.length; requestPartNum += 2) {
                    String key = requestParts[requestPartNum];
                    if (!(key == null || key == "" || requestPartNum >= requestParts.length - 1 || (value = requestParts[requestPartNum + 1]) == null || value == "")) {
                        debug = new StringBuffer().append(debug).append("\n\t").append(key).append(": ").append(value).toString();
                    }
                }
                logDebug(debug);
            }
        }
    }

    /* access modifiers changed from: protected */
    public String makeRequest(String cacheBusting, String queryString) {
        String request;
        String dc;
        String trackingServer = this.trackingServer;
        String dc2 = this.dc;
        String prefix = this.visitorNamespace;
        if (!isSet(trackingServer)) {
            if (!isSet(prefix)) {
                String prefix2 = this.account;
                int firstComma = prefix2.indexOf(",");
                if (firstComma != -1) {
                    prefix2 = prefix2.substring(0, firstComma);
                }
                prefix = replace(replace(prefix2, "_", "-"), ".", "-");
            }
            if (isSet(dc2)) {
                String dc3 = dc2.toLowerCase();
                if (dc3.equals("dc2") || dc3.equals("122")) {
                    dc = "122";
                } else {
                    dc = "112";
                }
            } else {
                dc = "112";
            }
            trackingServer = new StringBuffer().append(prefix).append(".").append(dc).append(".2o7.net").toString();
        } else if (isSet(this.ssl) && isSet(this.trackingServerSecure)) {
            trackingServer = this.trackingServerSecure;
        }
        if (isSet(this.ssl)) {
            request = "https://";
        } else {
            request = "http://";
        }
        String request2 = new StringBuffer().append(request).append(trackingServer).append("/b/ss/").append(this.account).append("/").append(this.mobile ? "5." : "").append(this.sendFromServer ? "0" : "1").append("/").append(this.version).append(isSet(this.target) ? new StringBuffer().append("-").append(this.target).toString() : "").append("/").append(cacheBusting).append("?AQB=1&ndh=1&").append(queryString).append("&AQE=1").toString();
        if (!this.sendFromServer) {
            return new StringBuffer().append("<img src=\"").append(request2).append("\" width=\"1\" height=\"1\" border=\"0\" alt=\"\" />").toString();
        }
        if (isSet(this.userAgent)) {
            request2 = new StringBuffer().append(request2).append("\tUser-Agent\t").append(replace(replace(replace(this.userAgent, "\t", " "), "\n", " "), "\r", " ")).toString();
        }
        if (isSet(this.acceptLanguage)) {
            request2 = new StringBuffer().append(request2).append("\tAccept-Language\t").append(replace(replace(replace(this.acceptLanguage, "\t", " "), "\n", " "), "\r", " ")).toString();
        }
        sendRequest(request2);
        return "";
    }

    /* access modifiers changed from: protected */
    public void handleLinkTracking() {
        int queryStringStart;
        String linkType = this.linkType;
        String linkURL = this.linkURL;
        String linkName = this.linkName;
        if (!isSet(linkType)) {
            return;
        }
        if (isSet(linkURL) || isSet(linkName)) {
            String linkType2 = linkType.toLowerCase();
            if (!linkType2.equals("d") && !linkType2.equals("e")) {
                linkType2 = "o";
            }
            if (isSet(linkURL) && !this.linkLeaveQueryString && (queryStringStart = linkURL.indexOf("?")) != -1) {
                linkURL = linkURL.substring(0, queryStringStart);
            }
            this.pe = new StringBuffer().append("lnk_").append(escape(linkType2)).toString();
            this.pev1 = escape(linkURL);
            this.pev2 = escape(linkName);
        }
    }

    /* access modifiers changed from: protected */
    public void handleTechnology() {
    }

    private String serializeToQueryString(String varKey, Hashtable varValue, String varFilter, String varFilterPrefix, String filter) {
        String queryString = "";
        Vector nestedFilterList = null;
        if (varKey.equals("contextData")) {
            varKey = "c";
        }
        if (varValue == null) {
            return queryString;
        }
        Enumeration keys = varValue.keys();
        while (keys.hasMoreElements()) {
            String subVarKey = (String) keys.nextElement();
            Object subVarValue = varValue.get(subVarKey);
            if ((filter == null || subVarKey.substring(0, filter.length()).equals(filter)) && isSet(subVarValue)) {
                if (isSet(varFilter)) {
                    if (varFilter.indexOf(new StringBuffer().append(",").append(isSet(varFilterPrefix) ? new StringBuffer().append(varFilterPrefix).append(".").toString() : "").append(subVarKey).append(",").toString()) < 0) {
                    }
                }
                boolean nestedFilterMatch = false;
                if (nestedFilterList != null) {
                    for (int nestedFilterNum = 0; nestedFilterNum < nestedFilterList.size(); nestedFilterNum++) {
                        if (subVarKey.substring(0, ((String) nestedFilterList.elementAt(nestedFilterNum)).length()).equals((String) nestedFilterList.elementAt(nestedFilterNum))) {
                            nestedFilterMatch = true;
                        }
                    }
                }
                if (!nestedFilterMatch) {
                    if (queryString.equals("")) {
                        queryString = new StringBuffer().append(queryString).append("&").append(varKey).append(".").toString();
                    }
                    if (filter != null) {
                        subVarKey = subVarKey.substring(filter.length());
                    }
                    if (subVarKey.length() > 0) {
                        int nestedKeyEnd = subVarKey.indexOf(".");
                        if (nestedKeyEnd > 0) {
                            String nestedKey = subVarKey.substring(0, nestedKeyEnd);
                            String nestedFilter = new StringBuffer().append(filter != null ? filter : "").append(nestedKey).append(".").toString();
                            if (nestedFilterList == null) {
                                nestedFilterList = new Vector();
                            }
                            nestedFilterList.addElement(nestedFilter);
                            queryString = new StringBuffer().append(queryString).append(serializeToQueryString(nestedKey, varValue, varFilter, varFilterPrefix, nestedFilter)).toString();
                        } else {
                            if (isBoolean(subVarValue)) {
                                if (toBoolean(subVarValue)) {
                                    subVarValue = "true";
                                } else {
                                    subVarValue = "false";
                                }
                            }
                            if (isSet(subVarValue)) {
                                if (varFilterPrefix.equals("retrieveLightData") && filter.indexOf(".contextData.") < 0) {
                                    String subVarPrefix = subVarKey.substring(0, 4);
                                    String subVarSuffix = subVarKey.substring(4);
                                    if (subVarKey.equals("transactionID")) {
                                        subVarKey = "xact";
                                    } else if (subVarKey.equals("channel")) {
                                        subVarKey = "ch";
                                    } else if (subVarKey.equals("campaign")) {
                                        subVarKey = "v0";
                                    } else if (isNumber(subVarSuffix)) {
                                        if (subVarPrefix.equals("prop")) {
                                            subVarKey = new StringBuffer().append("c").append(subVarSuffix).toString();
                                        } else if (subVarPrefix.equals("eVar")) {
                                            subVarKey = new StringBuffer().append("v").append(subVarSuffix).toString();
                                        } else if (subVarPrefix.equals("list")) {
                                            subVarKey = new StringBuffer().append("l").append(subVarSuffix).toString();
                                        } else if (subVarPrefix.equals("hier")) {
                                            subVarKey = new StringBuffer().append("h").append(subVarSuffix).toString();
                                            subVarValue = ((String) subVarValue).substring(0, 255);
                                        }
                                    }
                                }
                                queryString = new StringBuffer().append(queryString).append("&").append(escape(subVarKey)).append("=").append(escape(new StringBuffer().append("").append(subVarValue).toString())).toString();
                            }
                        }
                    }
                }
            }
        }
        if (queryString != "") {
            return new StringBuffer().append(queryString).append("&.").append(varKey).toString();
        }
        return queryString;
    }

    private String serializeToQueryString(String varKey, Hashtable varValue, String varFilter, String varFilterPrefix) {
        return serializeToQueryString(varKey, varValue, varFilter, varFilterPrefix, null);
    }

    /* access modifiers changed from: protected */
    public String getQueryString() {
        String[] varList;
        String varPrefix;
        String varSuffix;
        String str;
        String str2;
        String queryString = "";
        String varFilter = "";
        String eventFilter = "";
        String events = "";
        if (isSet(this.lightProfileID)) {
            varList = this.lightVarList;
            varFilter = this.lightTrackVars;
            if (isSet(varFilter)) {
                varFilter = new StringBuffer().append(",").append(varFilter).append(",").append(joinArray(",", this.lightRequiredVarList)).append(",").toString();
            }
        } else {
            varList = this.accountVarList;
            if (isSet(this.linkType)) {
                varFilter = this.linkTrackVars;
                eventFilter = this.linkTrackEvents;
            }
            if (isSet(varFilter)) {
                varFilter = new StringBuffer().append(",").append(varFilter).append(",").append(joinArray(",", this.requiredVarList)).append(",").toString();
            }
            if (isSet(eventFilter)) {
                eventFilter = new StringBuffer().append(",").append(eventFilter).append(",").toString();
            }
            if (isSet(this.events2)) {
                events = new StringBuffer().append(events).append(events != "" ? "," : "").append(this.events2).toString();
            }
        }
        for (int varNum = 0; varNum < varList.length; varNum++) {
            String varKey = varList[varNum];
            String varValue = getAccountVar(varKey);
            if (varKey.length() > 4) {
                varPrefix = varKey.substring(0, 4);
                varSuffix = varKey.substring(4);
            } else {
                varPrefix = null;
                varSuffix = null;
            }
            if (!isSet(varValue) && varKey.equals("events") && isSet(events)) {
                varValue = events;
                events = "";
            }
            if (isSet(varValue) && (!isSet(varFilter) || varFilter.indexOf(new StringBuffer().append(",").append(varKey).append(",").toString()) >= 0)) {
                if (varKey.equals("timestamp")) {
                    varKey = "ts";
                } else if (varKey.equals("dynamicVariablePrefix")) {
                    varKey = "D";
                } else if (varKey.equals("visitorID")) {
                    varKey = "vid";
                } else if (varKey.equals("pageURL")) {
                    varKey = "g";
                } else if (varKey.equals("referrer")) {
                    varKey = "r";
                } else if (varKey.equals("vmk") || varKey.equals("visitorMigrationKey")) {
                    varKey = "vmt";
                } else if (varKey.equals("visitorMigrationServer")) {
                    varKey = "vmf";
                    if (isSet(this.ssl) && isSet(this.visitorMigrationServerSecure)) {
                        varValue = "";
                    }
                } else if (varKey.equals("visitorMigrationServerSecure")) {
                    varKey = "vmf";
                    if (!isSet(this.ssl) && isSet(this.visitorMigrationServer)) {
                        varValue = "";
                    }
                } else if (varKey.equals("charSet")) {
                    varKey = "ce";
                } else if (varKey.equals("visitorNamespace")) {
                    varKey = "ns";
                } else if (varKey.equals("cookieDomainPeriods")) {
                    varKey = "cdp";
                } else if (varKey.equals("cookieLifetime")) {
                    varKey = "cl";
                } else if (varKey.equals("variableProvider")) {
                    varKey = "vvp";
                } else if (varKey.equals("currencyCode")) {
                    varKey = "cc";
                } else if (varKey.equals("channel")) {
                    varKey = "ch";
                } else if (varKey.equals("transactionID")) {
                    varKey = "xact";
                } else if (varKey.equals("campaign")) {
                    varKey = "v0";
                } else if (varKey.equals("resolution")) {
                    varKey = "s";
                } else if (varKey.equals("events")) {
                    if (isSet(events)) {
                        varValue = new StringBuffer().append(varValue).append(varValue != "" ? "," : "").append(events).toString();
                    }
                    if (isSet(eventFilter)) {
                        String[] varValueParts = splitString(",", varValue);
                        varValue = "";
                        for (int varSubNum = 0; varSubNum < varValueParts.length; varSubNum++) {
                            String varValuePart = varValueParts[varSubNum];
                            int varValuePartPos = varValuePart.indexOf("=");
                            if (varValuePartPos >= 0) {
                                varValuePart = varValuePart.substring(0, varValuePartPos);
                            }
                            int varValuePartPos2 = varValuePart.indexOf(":");
                            if (varValuePartPos2 >= 0) {
                                varValuePart = varValuePart.substring(0, varValuePartPos2);
                            }
                            if (eventFilter.indexOf(new StringBuffer().append(",").append(varValuePart).append(",").toString()) >= 0) {
                                StringBuffer append = new StringBuffer().append(varValue);
                                if (isSet(varValue)) {
                                    str = ",";
                                } else {
                                    str = "";
                                }
                                varValue = append.append(str).append(varValueParts[varSubNum]).toString();
                            }
                        }
                    }
                } else if (varKey.equals("events2")) {
                    varValue = "";
                } else if (varKey.equals("contextData")) {
                    queryString = new StringBuffer().append(queryString).append(serializeToQueryString("c", this.contextData, varFilter, varKey, null)).toString();
                    varValue = "";
                } else if (varKey.equals("lightProfileID")) {
                    varKey = "mtp";
                } else if (varKey.equals("lightStoreForSeconds")) {
                    varKey = "mtss";
                    if (!isSet(this.lightProfileID)) {
                        varValue = "";
                    }
                } else if (varKey.equals("lightIncrementBy")) {
                    varKey = "mti";
                    if (!isSet(this.lightProfileID)) {
                        varValue = "";
                    }
                } else if (varKey.equals("retrieveLightProfiles")) {
                    varKey = "mtsr";
                } else if (varKey.equals("deleteLightProfiles")) {
                    varKey = "mtsd";
                } else if (varKey.equals("retrieveLightData")) {
                    if (isSet(this.retrieveLightProfiles)) {
                        queryString = new StringBuffer().append(queryString).append(serializeToQueryString("mts", this.retrieveLightData, varFilter, varKey, null)).toString();
                    }
                    varValue = "";
                } else if (isNumber(varSuffix)) {
                    if (varPrefix.equals("prop")) {
                        varKey = new StringBuffer().append("c").append(varSuffix).toString();
                    } else if (varPrefix.equals("eVar")) {
                        varKey = new StringBuffer().append("v").append(varSuffix).toString();
                    } else if (varPrefix.equals("list")) {
                        varKey = new StringBuffer().append("l").append(varSuffix).toString();
                    } else if (varPrefix.equals("hier")) {
                        varKey = new StringBuffer().append("h").append(varSuffix).toString();
                        if (varValue.length() > 255) {
                            varValue = varValue.substring(0, 255);
                        }
                    }
                }
                if (isSet(varValue)) {
                    StringBuffer append2 = new StringBuffer().append(queryString).append("&").append(escape(varKey)).append("=");
                    if (varKey.length() <= 3 || varKey.substring(0, 3) != "pev") {
                        str2 = escape(varValue);
                    } else {
                        str2 = varValue;
                    }
                    queryString = append2.append(str2).toString();
                }
            }
        }
        return queryString;
    }

    /* access modifiers changed from: protected */
    public void variableOverridesApply(Hashtable variableOverrides) {
        variableOverridesApply(variableOverrides, false);
    }

    /* access modifiers changed from: protected */
    public void variableOverridesApply(Hashtable variableOverrides, boolean restoring) {
        Object varValue;
        Object varValue2;
        for (String varKey : this.accountVarList) {
            if ((variableOverrides.containsKey(varKey) && ((isString(variableOverrides.get(varKey)) && isSet(toString(variableOverrides.get(varKey)))) || isSet(variableOverrides.get(varKey)))) || (variableOverrides.containsKey(new StringBuffer().append("!").append(varKey).toString()) && isSet((String) variableOverrides.get(new StringBuffer().append("!").append(varKey).toString())))) {
                if (variableOverrides.containsKey(varKey)) {
                    varValue2 = variableOverrides.get(varKey);
                } else {
                    varValue2 = null;
                }
                if (!restoring && (varKey.equals("contextData") || varKey.equals("retrieveLightData"))) {
                    Hashtable varValueHashTable = (Hashtable) varValue2;
                    if (isSet(this.contextData)) {
                        Enumeration keys = this.contextData.keys();
                        while (keys.hasMoreElements()) {
                            String subVarKey = (String) keys.nextElement();
                            if (!varValueHashTable.containsKey(subVarKey) || !isSet(varValueHashTable.get(subVarKey))) {
                                varValueHashTable.put(subVarKey, this.contextData.get(subVarKey));
                            }
                        }
                    } else if (isSet(this.retrieveLightData)) {
                        Enumeration keys2 = this.retrieveLightData.keys();
                        while (keys2.hasMoreElements()) {
                            String subVarKey2 = (String) keys2.nextElement();
                            if (!varValueHashTable.containsKey(subVarKey2) || !isSet(varValueHashTable.get(subVarKey2))) {
                                varValueHashTable.put(subVarKey2, this.retrieveLightData.get(subVarKey2));
                            }
                        }
                    }
                }
                if (varKey.equals("contextData")) {
                    this.contextData = (Hashtable) varValue2;
                } else if (varKey.equals("retrieveLightData")) {
                    this.retrieveLightData = (Hashtable) varValue2;
                } else {
                    setAccountVar(varKey, (String) varValue2);
                }
            }
        }
        for (String varKey2 : this.accountConfigList) {
            if ((variableOverrides.containsKey(varKey2) && isSet(variableOverrides.get(varKey2))) || (variableOverrides.containsKey(new StringBuffer().append("!").append(varKey2).toString()) && isSet((String) variableOverrides.get(new StringBuffer().append("!").append(varKey2).toString())))) {
                if (variableOverrides.containsKey(varKey2)) {
                    varValue = variableOverrides.get(varKey2);
                } else {
                    varValue = null;
                }
                setAccountVar(varKey2, (String) varValue);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void variableOverridesBuild(Hashtable variableOverrides) {
        for (String varKey : this.accountVarList) {
            if (!variableOverrides.containsKey(varKey) || !isSet((String) variableOverrides.get(varKey))) {
                String accountVar = getAccountVar(varKey);
                if (varKey.equals("contextData") && isSet(this.contextData)) {
                    variableOverrides.put(varKey, this.contextData);
                } else if (!varKey.equals("retrieveLightData") || !isSet(this.retrieveLightData)) {
                    String varValue = getAccountVar(varKey);
                    if (isSet(varValue)) {
                        variableOverrides.put(varKey, varValue);
                    }
                } else {
                    variableOverrides.put(varKey, this.retrieveLightData);
                }
                if ((!variableOverrides.containsKey(varKey) || !isSet(variableOverrides.get(varKey))) && !variableOverrides.containsKey(new StringBuffer().append("!").append(varKey).toString())) {
                    variableOverrides.put(new StringBuffer().append("!").append(varKey).toString(), "1");
                }
            }
        }
        for (String varKey2 : this.accountConfigList) {
            if (!variableOverrides.containsKey(varKey2) || !isSet((String) variableOverrides.get(varKey2))) {
                String varValue2 = getAccountVar(varKey2);
                if (isSet(varValue2)) {
                    variableOverrides.put(varKey2, varValue2);
                }
                if ((!variableOverrides.containsKey(varKey2) || !isSet(variableOverrides.get(varKey2))) && !variableOverrides.containsKey(new StringBuffer().append("!").append(varKey2).toString())) {
                    variableOverrides.put(new StringBuffer().append("!").append(varKey2).toString(), "1");
                }
            }
        }
    }

    public void clearVars() {
        String varPrefix;
        for (String varKey : this.accountVarList) {
            if (varKey.length() > 4) {
                varPrefix = varKey.substring(0, 4);
            } else {
                varPrefix = "";
            }
            if (varKey.equals("channel") || varKey.equals("events") || varKey.equals("purchaseID") || varKey.equals("transactionID") || varKey.equals("products") || varKey.equals("state") || varKey.equals("zip") || varKey.equals("campaign") || varPrefix.equals("prop") || varPrefix.equals("eVar") || varPrefix.equals("hier")) {
                setAccountVar(varKey, null);
            }
        }
    }

    public String track(Hashtable variableOverrides) {
        String code = "";
        Hashtable variableOverridesBackup = null;
        Calendar tm = getCalendar();
        String cacheBusting = new StringBuffer().append("s").append((int) Math.floor(new Random().nextDouble() * 1.0E8d)).toString();
        String queryString = new StringBuffer().append("t=").append(escape(new StringBuffer().append("").append(tm.get(5)).append("/").append(tm.get(2)).append("/").append(tm.get(1)).append(" ").append(tm.get(11)).append(":").append(tm.get(12)).append(":").append(tm.get(13)).append(" ").append(tm.get(7) - 1).append(" ").append((tm.getTimeZone().getRawOffset() / 60000) * -1).toString())).toString();
        if (variableOverrides != null) {
            variableOverridesBackup = new Hashtable();
            variableOverridesBuild(variableOverridesBackup);
            variableOverridesApply(variableOverrides);
        }
        if (this.usePlugins && _hasDoPlugins()) {
            _doPlugins();
        }
        if (isSet(this.account)) {
            if (isSet(this.trackOffline) && !isSet(this.timestamp)) {
                this.timestamp = (int) Math.floor(getTime() / 1000.0d);
            }
            setDefaults();
            handleLinkTracking();
            handleTechnology();
            code = makeRequest(cacheBusting, new StringBuffer().append(queryString).append(getQueryString()).toString());
        }
        if (variableOverrides != null) {
            variableOverridesApply(variableOverridesBackup);
        }
        this.referrer = null;
        this.pe = null;
        this.pev1 = null;
        this.pev2 = null;
        this.pev3 = null;
        this.linkURL = null;
        this.linkName = null;
        this.linkType = null;
        this.lightProfileID = null;
        this.retrieveLightProfiles = null;
        this.deleteLightProfiles = null;
        return code;
    }

    public String track() {
        return track(null);
    }

    public String trackLink(String linkURL, String linkType, String linkName, Hashtable variableOverrides) {
        this.linkURL = linkURL;
        this.linkType = linkType;
        this.linkName = linkName;
        return track(variableOverrides);
    }

    public String trackLight(String profileID, int storeForSeconds, int incrementBy, Hashtable variableOverrides) {
        this.lightProfileID = profileID;
        this.lightStoreForSeconds = storeForSeconds;
        this.lightIncrementBy = incrementBy;
        return track(variableOverrides);
    }

    public String trackLink(String linkURL, String linkType, String linkName) {
        return trackLink(linkURL, linkType, linkName, null);
    }

    public String trackLight(String profileID, int storeForSeconds, int incrementBy) {
        return trackLight(profileID, storeForSeconds, incrementBy, null);
    }

    public String trackLight(String profileID, int storeForSeconds) {
        return trackLight(profileID, storeForSeconds, 0, null);
    }

    public String trackLight(String profileID) {
        return trackLight(profileID, 0, 0, null);
    }

    /* access modifiers changed from: protected */
    public void setDefaults() {
        if (!isSet(this.userAgent)) {
            this.userAgent = getDefaultUserAgent();
        }
        if (!isSet(this.acceptLanguage)) {
            this.acceptLanguage = getDefaultAcceptLanguage();
        }
        if (!isSet(this.visitorID)) {
            this.visitorID = getDefaultVisitorID();
        }
        if (!isSet(this.pageURL)) {
            this.pageURL = getDefaultPageURL();
        }
        if (isSet(this.pageURL) && this.pageURL.toLowerCase().indexOf("https://") >= 0) {
            this.ssl = true;
        }
        if (!isSet(this.referrer) && !isSet(this._1_referrer)) {
            this.referrer = getDefaultReferrer();
            this._1_referrer = 1;
        }
    }

    /* access modifiers changed from: protected */
    public String getDefaultUserAgent() {
        return "";
    }

    /* access modifiers changed from: protected */
    public String getDefaultAcceptLanguage() {
        return "";
    }

    /* access modifiers changed from: protected */
    public String getDefaultVisitorID() {
        return "";
    }

    /* access modifiers changed from: protected */
    public String getDefaultPageURL() {
        return "";
    }

    /* access modifiers changed from: protected */
    public String getDefaultReferrer() {
        return "";
    }
}
