package com.omniture;

import android.app.Application;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import java.io.File;
import java.util.Hashtable;
import java.util.Locale;

public class AppMeasurement extends AppMeasurementBaseSE {
    protected Application application;
    public DoPlugins doPlugins;
    public DoRequest doRequest;

    public interface DoPlugins {
        void doPlugins(AppMeasurement appMeasurement);
    }

    public interface DoRequest {
        boolean doRequest(AppMeasurement appMeasurement, String str, Hashtable hashtable);
    }

    /* access modifiers changed from: protected */
    public boolean _hasDoPlugins() {
        return this.doPlugins != null;
    }

    /* access modifiers changed from: protected */
    public void _doPlugins() {
        if (this.doPlugins != null) {
            this.doPlugins.doPlugins(this);
        }
    }

    /* access modifiers changed from: protected */
    public boolean _hasDoRequest() {
        return this.doRequest != null;
    }

    /* access modifiers changed from: protected */
    public boolean _doRequest(String url, Hashtable headers) {
        if (this.doRequest != null) {
            return this.doRequest.doRequest(this, url, headers);
        }
        return true;
    }

    public AppMeasurement() {
        this.doPlugins = null;
        this.doRequest = null;
        this.application = null;
        this.target = "AN";
    }

    public AppMeasurement(Application currentApplication) {
        this();
        this.application = currentApplication;
        if (this.application != null && this.offlineFilename == null) {
            this.offlineFilename = new File(this.application.getCacheDir(), "AppMeasurement.offline").getPath();
        }
    }

    public void logDebug(String msg) {
        Log.d("AppMeasurement", msg);
    }

    /* access modifiers changed from: protected */
    public void handleTechnology() {
        try {
            DisplayMetrics displayMetrics = this.application.getResources().getDisplayMetrics();
            this.resolution = new StringBuffer().append(displayMetrics.widthPixels).append("x").append(displayMetrics.heightPixels).toString();
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public String getDefaultUserAgent() {
        String acceptLanguage = getDefaultAcceptLanguage();
        String applicationID = getApplicationID();
        return new StringBuffer().append("Mozilla/5.0 (Linux; U; Android ").append(Build.VERSION.RELEASE).append("; ").append(isSet(acceptLanguage) ? acceptLanguage : "en_US").append("; ").append(Build.MODEL).append(" Build/").append(Build.ID).append(")").append(isSet(applicationID) ? new StringBuffer().append(" ").append(applicationID).toString() : "").toString();
    }

    /* access modifiers changed from: protected */
    public String getDefaultVisitorID() {
        try {
            return ((TelephonyManager) this.application.getSystemService("phone")).getSubscriberId();
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public String getDefaultAcceptLanguage() {
        try {
            Locale currentLocale = this.application.getResources().getConfiguration().locale;
            return new StringBuffer().append(currentLocale.getLanguage()).append('-').append(currentLocale.getCountry().toLowerCase()).toString();
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public String getApplicationID() {
        try {
            PackageManager packageManager = this.application.getPackageManager();
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(this.application.getPackageName(), 0);
            PackageInfo packageInfo = packageManager.getPackageInfo(this.application.getPackageName(), 0);
            String applicationName = (String) packageManager.getApplicationLabel(applicationInfo);
            String applicationVersion = packageInfo.versionName;
            if (isSet(applicationName)) {
                return new StringBuffer().append(applicationName).append(isSet(applicationVersion) ? new StringBuffer().append("/").append(applicationVersion).toString() : "").toString();
            }
        } catch (Exception e) {
        }
        return null;
    }
}
