package com.ap.sacramento.opt;

import android.app.Application;
import com.ap.sacramento.common.Logger;
import com.ap.sacramento.managers.PageviewHandler;
import com.omniture.AppMeasurement;
import com.vervewireless.capi.ContentItem;
import com.vervewireless.capi.DisplayBlock;
import com.vervewireless.capi.UserPreferences;
import com.vervewireless.capi.Verve;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractOmniturePageViewHandler implements PageviewHandler {
    protected Verve api;
    protected String apiId;
    protected Application app;
    private Map<Integer, DisplayBlock> displayBlocks = new HashMap();
    protected String localeName;
    protected AppMeasurement omniture;
    protected String publication;
    protected String pubname;

    public void init(Application app2, Verve api2) {
        this.app = app2;
        this.omniture = new AppMeasurement(app2);
        this.omniture.debugTracking = Logger.isDebugEnabled();
        this.api = api2;
    }

    protected static boolean isValid(String value) {
        return value != null && value.length() > 0;
    }

    /* access modifiers changed from: protected */
    public DisplayBlock getDisplayBlock(int blockId) {
        return this.displayBlocks.get(Integer.valueOf(blockId));
    }

    /* access modifiers changed from: protected */
    public boolean ensureConfiguration() {
        if (!isValid(this.localeName) || !isValid(this.apiId) || this.api.getLocale() == null) {
            return configure();
        }
        String newLocale = this.api.getLocale().getName();
        String newApiId = this.api.getApiInfo().getRegistrationId();
        if (this.localeName.equals(newLocale) && newApiId.equals(this.apiId)) {
            return true;
        }
        Logger.logDebug("Reconfiguring Omniture");
        return configure();
    }

    /* access modifiers changed from: protected */
    public boolean configure() {
        UserPreferences prefs = this.api.getUserPreferences();
        String suite = prefs.getValue("omniture_suite", null);
        String server = prefs.getValue("omniture_server", null);
        this.pubname = prefs.getValue("omniture_pubname", null);
        if (!isValid(this.pubname)) {
            this.pubname = this.api.getLocale().getName();
        }
        if (!isValid(suite) || !isValid(server) || !isValid(this.pubname)) {
            Logger.logWarning("No suite, server, or pubnam available; disabling App Measurement");
            return false;
        }
        Logger.logDebug("Omnistore session for suite:" + suite + ", server:" + server + ", pubname:" + this.pubname);
        this.omniture.account = suite;
        this.omniture.server = server;
        this.omniture.trackingServer = server;
        this.omniture.trackingServerSecure = server;
        this.omniture.visitorID = this.api.getApiInfo().getRegistrationId();
        this.omniture.trackOffline = false;
        this.omniture.offlineThrottleDelay = 1000;
        if (Logger.isDebugEnabled()) {
            this.omniture.debugTracking = true;
        }
        this.publication = this.pubname;
        this.localeName = this.api.getLocale().getName();
        this.apiId = this.api.getApiInfo().getRegistrationId();
        return true;
    }

    public void setHierarchy(DisplayBlock hierarchy) {
        this.displayBlocks.clear();
        fill(hierarchy);
    }

    public void reportShare(DisplayBlock displayBlock, ContentItem contentItem, String shareType) {
    }

    public void reportCustomPageview(String guid, Integer displayBlockId, Integer partnerModuleId) {
    }

    private void fill(DisplayBlock block) {
        this.displayBlocks.put(Integer.valueOf(block.getId()), block);
        for (DisplayBlock child : block.getDisplayBlocks()) {
            fill(child);
        }
    }
}
