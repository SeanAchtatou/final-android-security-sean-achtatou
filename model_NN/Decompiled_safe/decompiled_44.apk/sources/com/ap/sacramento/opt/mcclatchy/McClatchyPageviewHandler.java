package com.ap.sacramento.opt.mcclatchy;

import com.ap.sacramento.common.Logger;
import com.ap.sacramento.managers.PageviewHandler;
import com.ap.sacramento.opt.AbstractOmniturePageViewHandler;
import com.vervewireless.capi.ContentItem;
import com.vervewireless.capi.DisplayBlock;

public class McClatchyPageviewHandler extends AbstractOmniturePageViewHandler implements PageviewHandler {
    protected String pubunit;

    /* access modifiers changed from: protected */
    public boolean configure() {
        if (!super.configure()) {
            return false;
        }
        this.omniture.dc = "112";
        this.omniture.server = String.format("%s: Vendor: VerveAPP", this.publication);
        this.pubunit = this.api.getUserPreferences().getValue("omniture_pubunit", "");
        return true;
    }

    public void reportPageview(DisplayBlock block, ContentItem contentItem) {
        String pagename;
        String section;
        if (!ensureConfiguration()) {
            Logger.logWarning("Omniture not configured, page reporting disabled");
            return;
        }
        String title = contentItem == null ? null : contentItem.getTitle();
        String category = block.getName();
        String parent = null;
        DisplayBlock blockParent = getDisplayBlock(block.getParentId());
        if (!(blockParent == null || blockParent.getParentId() == 0)) {
            parent = blockParent.getName();
        }
        if (!isValid(category)) {
            category = String.format("Category %d (Name Unknown)", Integer.valueOf(block.getId()));
        }
        if (!isValid(title)) {
            pagename = category;
        } else {
            pagename = String.format("%s - %s", category, title);
        }
        if (parent != null) {
            section = parent;
        } else {
            section = category;
        }
        this.omniture.pageName = String.format("Vendor: VerveAPP: %s", pagename);
        this.omniture.channel = String.format("%s: VerveAPP: %s", this.publication, section);
        this.omniture.prop3 = "VendorAPP";
        this.omniture.prop4 = String.format("%s: Verve", this.publication);
        this.omniture.hier1 = String.format("%s|%s|News|||||%s", this.pubunit, this.publication, this.omniture.channel);
        Logger.logDebug("Report page view for " + block.getName() + ", with id:" + block.getId() + ", parent:" + parent + ", return:" + this.omniture.track());
    }
}
