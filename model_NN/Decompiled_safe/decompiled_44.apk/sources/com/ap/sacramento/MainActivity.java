package com.ap.sacramento;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ViewFlipper;
import com.ap.sacramento.common.Logger;
import com.ap.sacramento.managers.PageviewHandler;
import com.ap.sacramento.managers.ScreenManager;
import com.ap.sacramento.managers.VerveManager;
import com.ap.sacramento.views.SplashScreen;
import com.vervewireless.capi.CapiStatusListener;
import com.vervewireless.capi.Verve;
import com.vervewireless.capi.VerveImpl;
import java.io.IOException;

public class MainActivity extends Activity implements CapiStatusListener {
    private static final long PAGE_VIEW_FLUSH_TIMEOUT = 1800000;
    private Verve api;
    private Handler handler = new Handler();
    private boolean isPaused = false;
    private ViewFlipper mainFlipperPanel;
    /* access modifiers changed from: private */
    public boolean pendingFlush;

    public void onCreate(Bundle savedInstanceState) {
        boolean z;
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        ScreenManager.getInstance().setContext(this);
        ScreenManager.getInstance().init();
        this.mainFlipperPanel = (ViewFlipper) findViewById(R.id.mainFlipperPanel);
        ScreenManager.getInstance().setMainFlipperPanel(this.mainFlipperPanel);
        Confuguration confuguration = new Confuguration();
        try {
            confuguration.load();
            String value = confuguration.getProperties().getProperty("mobileapp.showemptythumbs");
            VerveManager instance = VerveManager.getInstance();
            if (value == null || Boolean.parseBoolean(value)) {
                z = true;
            } else {
                z = false;
            }
            instance.setShowEmptyThumbs(z);
            this.api = new VerveImpl(this, getString(R.string.app_reg_key), getString(R.string.app_reg_version));
            this.api.setCapiStatusListener(this);
            VerveManager mng = VerveManager.getInstance();
            mng.setVerveApi(this.api);
            for (PageviewHandler handler2 : confuguration.getPageviewHandlers()) {
                handler2.init(getApplication(), this.api);
                mng.addPageviewHandler(handler2);
            }
            this.api.setCapiStatusListener(this);
            ScreenManager.getInstance().splash(new SplashScreen().getView());
        } catch (IOException e) {
            throw new IllegalStateException("Invalid configuration");
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public void onDestroy() {
        try {
            this.api.close();
            super.onDestroy();
            ScreenManager.getInstance().setContext(null);
            ScreenManager.getInstance().clearImageCache();
            ScreenManager.getInstance().clearImageGalleryCache();
        } catch (Throwable th) {
            super.onDestroy();
            throw th;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!ScreenManager.getInstance().IsExternalActivity() && keyCode == 4 && ScreenManager.getInstance().isValid()) {
            ScreenManager.getInstance().back();
            return true;
        } else if (keyCode != 84) {
            return super.onKeyDown(keyCode, event);
        } else {
            Logger.logDebug("KeyEvent.KEYCODE_SEARCH");
            ScreenManager.getInstance().onSearchKey();
            return true;
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        ScreenManager.getInstance().onConfigurationChanged(newConfig);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        return super.onPrepareOptionsMenu(ScreenManager.getInstance().getOptions(menu));
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        ScreenManager.getInstance().onOptionsMenuItem(item.getTitle().toString());
        return super.onMenuItemSelected(featureId, item);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ScreenManager.getInstance().closeProgress();
        ScreenManager.getInstance().onActivityResult(requestCode, resultCode, data);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.isPaused) {
            if (ScreenManager.getInstance().getLocationMode() == 0) {
                VerveManager.getInstance().getLocation(true);
            } else if (ScreenManager.getInstance().getLastPostal().length() < 1 && !VerveManager.getInstance().isPostalDialog()) {
                VerveManager.getInstance().showPostalDialog(null, true);
            }
        }
        this.isPaused = false;
        super.onResume();
        flushPageViews();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.isPaused = true;
        super.onPause();
    }

    public void hiearachyUpdated() {
    }

    public void reregistrationRequired() {
        ScreenManager.getInstance().registerLocale(null);
    }

    public void updateAvailable() {
        Context context = ScreenManager.getInstance().getContext();
        String updateStr = context.getResources().getString(R.string.updateAvailable);
        String appName = context.getResources().getString(R.string.app_name);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(String.format(updateStr, appName));
        builder.show();
    }

    /* access modifiers changed from: package-private */
    public void flushPageViews() {
        if (this.pendingFlush) {
            Logger.logDebug("Flush pending");
            return;
        }
        Logger.logDebug("Flushin page reports");
        this.api.flushPageviews();
        this.handler.postDelayed(new Runnable() {
            public void run() {
                boolean unused = MainActivity.this.pendingFlush = false;
                MainActivity.this.flushPageViews();
            }
        }, PAGE_VIEW_FLUSH_TIMEOUT);
        this.pendingFlush = true;
    }
}
