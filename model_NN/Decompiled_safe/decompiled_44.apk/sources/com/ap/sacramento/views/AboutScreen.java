package com.ap.sacramento.views;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.ap.sacramento.R;
import com.ap.sacramento.common.BaseScreen;
import com.ap.sacramento.common.Logger;
import com.ap.sacramento.managers.ScreenManager;
import com.ap.sacramento.managers.VerveManager;
import com.vervewireless.capi.Locale;
import com.vervewireless.capi.PartnerModules;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

public class AboutScreen extends BaseScreen {
    String applicationLegal;
    String applicationName;
    String applicationRef;
    String applicationVersion;
    View category_row;
    private ListView listSettings;
    List<View> listViews;
    List<Locale> locales;
    private int originalLocaleIndex = -1;
    private int selectedIndex = -1;
    private Locale selectedLocale;
    private SettingsAdapter settingsAdapter;
    View settings_edition;
    View settings_single_about;
    View settings_single_contact;
    View settings_single_logo;
    View settings_single_recommend;
    View settings_single_send;

    public AboutScreen(boolean showTitle) {
        this.showTitle = showTitle;
        this.container = (ViewGroup) ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.settingsscreen, (ViewGroup) null);
        this.container.setTag(this);
        this.listViews = new LinkedList();
        this.selectedLocale = VerveManager.getInstance().getVerveApi().getLocale();
        this.listSettings = (ListView) this.container.findViewById(R.id.listSettings);
        this.settingsAdapter = new SettingsAdapter();
        this.listSettings.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                switch (arg2) {
                    case 3:
                        ScreenManager.getInstance().showProgress("Loading ...", "");
                        AboutScreen.this.applicationLegal = AboutScreen.this.readLegal();
                        Logger.logDebug("Legal " + AboutScreen.this.applicationLegal);
                        ScreenManager.getInstance().showDialogOK("Legal", AboutScreen.this.applicationLegal);
                        ScreenManager.getInstance().closeProgress();
                        return;
                    default:
                        return;
                }
            }
        });
        this.titlePanel = this.container.findViewById(R.id.title);
        if (showTitle) {
            this.textLeft = (TextView) this.container.findViewById(R.id.textLeft);
            this.textLeft.setText("About");
            this.textRight = (TextView) this.container.findViewById(R.id.textRight);
            this.textRight.setVisibility(8);
            this.imgHeader = (ImageView) this.container.findViewById(R.id.imgHeader);
            Drawable header = VerveManager.getInstance().getTitleHeader();
            if (header != null) {
                this.imgHeader.setImageDrawable(header);
            }
        } else {
            this.titlePanel.setVisibility(8);
        }
        this.applicationName = ScreenManager.getInstance().getContext().getString(R.string.app_name);
        this.applicationVersion = VerveManager.getInstance().getVerveApi().getApiInfo().getApplicationVersion();
        this.applicationRef = VerveManager.getInstance().getVerveApi().getApiInfo().getRegistrationId();
        ScreenManager.getInstance().showProgress("Loading ...", "");
    }

    public void closed(boolean isHidden) {
    }

    public void displayed(boolean isBack) {
        if (!isBack) {
            VerveManager.getInstance().reportCustomPageview(null, null, Integer.valueOf(PartnerModules.ABOUT));
            this.category_row = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.category_row, (ViewGroup) null);
            ((TextView) this.category_row.findViewById(R.id.textCategoryTitle)).setText("Application info");
            this.settings_edition = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.settings_row, (ViewGroup) null);
            ((TextView) this.settings_edition.findViewById(R.id.textTitle)).setText("Application");
            ((TextView) this.settings_edition.findViewById(R.id.textSubtitle)).setText(this.applicationName);
            ((ImageView) this.settings_edition.findViewById(R.id.imgIcon)).setVisibility(8);
            this.settings_edition.setClickable(false);
            this.settings_single_recommend = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.settings_row, (ViewGroup) null);
            ((TextView) this.settings_single_recommend.findViewById(R.id.textTitle)).setText("Version");
            ((TextView) this.settings_single_recommend.findViewById(R.id.textSubtitle)).setText(this.applicationVersion);
            ((ImageView) this.settings_single_recommend.findViewById(R.id.imgIcon)).setVisibility(8);
            this.settings_single_recommend.setClickable(false);
            this.settings_single_contact = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.settings_row, (ViewGroup) null);
            ((TextView) this.settings_single_contact.findViewById(R.id.textTitle)).setText("Ref #");
            ((TextView) this.settings_single_contact.findViewById(R.id.textSubtitle)).setText(this.applicationRef);
            ((ImageView) this.settings_single_contact.findViewById(R.id.imgIcon)).setVisibility(8);
            this.settings_single_contact.setClickable(false);
            this.settings_single_send = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.settings_single_row, (ViewGroup) null);
            ((TextView) this.settings_single_send.findViewById(R.id.textTitle)).setText("Legal");
            ((ImageView) this.settings_single_send.findViewById(R.id.imgIcon)).setVisibility(0);
            this.settings_single_logo = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.verve_branding, (ViewGroup) null);
            this.listViews.add(this.settings_edition);
            this.listViews.add(this.settings_single_recommend);
            this.listViews.add(this.settings_single_contact);
            this.listViews.add(this.settings_single_send);
            this.listViews.add(this.settings_single_logo);
            this.listSettings.setAdapter((ListAdapter) this.settingsAdapter);
            ScreenManager.getInstance().closeProgress();
        }
    }

    public String readLegal() {
        try {
            InputStream is = ScreenManager.getInstance().getContext().getAssets().open("Bundle/legal.txt");
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            is.close();
            return new String(buffer);
        } catch (Exception e) {
            return "No legal information.";
        }
    }

    class SettingsAdapter extends BaseAdapter {
        SettingsAdapter() {
        }

        public int getCount() {
            return AboutScreen.this.listViews.size();
        }

        public Object getItem(int position) {
            return AboutScreen.this.listViews.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            Logger.logDebug("SettingsScreen getView");
            return AboutScreen.this.listViews.get(position);
        }
    }
}
