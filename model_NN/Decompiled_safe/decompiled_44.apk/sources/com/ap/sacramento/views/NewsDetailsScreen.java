package com.ap.sacramento.views;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ap.sacramento.R;
import com.ap.sacramento.common.AdWebView;
import com.ap.sacramento.common.BaseScreen;
import com.ap.sacramento.common.CustomWebView;
import com.ap.sacramento.common.Logger;
import com.ap.sacramento.managers.AdManager;
import com.ap.sacramento.managers.ScreenManager;
import com.ap.sacramento.managers.ShareManager;
import com.ap.sacramento.managers.VerveManager;
import com.vervewireless.capi.ContentItem;
import com.vervewireless.capi.DisplayBlock;
import java.util.List;

public class NewsDetailsScreen extends BaseScreen implements View.OnTouchListener {
    private AdManager adManager;
    private ImageView btnNext;
    private ImageView btnPrev;
    /* access modifiers changed from: private */
    public List<ContentItem> contentItems;
    private boolean controlsVisible = false;
    private int count;
    private final DisplayBlock displayBlock;
    private RelativeLayout layoutAdvert;
    private Handler mHandler = new Handler();
    private final Animation mHideNextAnimation = new AlphaAnimation(1.0f, 0.0f);
    private final Animation mHidePrevAnimation = new AlphaAnimation(1.0f, 0.0f);
    private final Animation mShowNextAnimation = new AlphaAnimation(0.0f, 1.0f);
    private final Animation mShowPrevAnimation = new AlphaAnimation(0.0f, 1.0f);
    private Runnable mTimerTask;
    /* access modifiers changed from: private */
    public int position = 0;
    private AdWebView webAdvert;
    private CustomWebView webView;

    public NewsDetailsScreen(DisplayBlock displayBlock2, List<ContentItem> contentItems2, int position2) {
        this.displayBlock = displayBlock2;
        this.contentItems = contentItems2;
        this.position = position2;
        this.count = contentItems2.size() - 1;
        this.container = (ViewGroup) ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.newsdetailsscreen, (ViewGroup) null);
        this.container.setTag(this);
        this.webView = (CustomWebView) this.container.findViewById(R.id.webView);
        this.btnPrev = (ImageView) this.container.findViewById(R.id.btnPrev);
        this.btnPrev.setVisibility(8);
        this.btnPrev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NewsDetailsScreen.this.refresh(false);
            }
        });
        this.btnNext = (ImageView) this.container.findViewById(R.id.btnNext);
        this.btnNext.setVisibility(8);
        this.btnNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NewsDetailsScreen.this.refresh(true);
            }
        });
        this.textLeft = (TextView) this.container.findViewById(R.id.textLeft);
        this.textLeft.setText(ScreenManager.getInstance().getTitle());
        this.textRight = (TextView) this.container.findViewById(R.id.textRight);
        this.textRight.setText(String.format("%d of %d", Integer.valueOf(position2 + 1), Integer.valueOf(this.count + 1)));
        this.imgHeader = (ImageView) this.container.findViewById(R.id.imgHeader);
        this.titlePanel = this.container.findViewById(R.id.title);
        Drawable header = VerveManager.getInstance().getTitleHeader();
        if (header != null) {
            this.imgHeader.setImageDrawable(header);
        }
        this.webView.getSettings().setUseWideViewPort(true);
        this.webView.getSettings().setBuiltInZoomControls(true);
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.setWebViewClient(new NewsWebViewClient());
        this.webView.setParent(this);
        this.adManager = new AdManager();
        this.webAdvert = (AdWebView) this.container.findViewById(R.id.webAdvert);
        this.adManager.setAd(this.webAdvert);
        this.adManager.setDisplayBlock(displayBlock2);
        this.adManager.hide();
        ScreenManager.getInstance().showProgress("Loading news ...", "");
    }

    public void closed(boolean isHidden) {
        if (!isHidden) {
        }
    }

    public void displayed(boolean isBack) {
        if (!isBack) {
            display();
        }
    }

    public void onTouch(MotionEvent event) {
        Logger.logDebug("NewsDetails on touch");
        showControls();
        super.onTouch(event);
    }

    public void display() {
        String html;
        String html2;
        ContentItem item = this.contentItems.get(this.position);
        String html3 = ScreenManager.getInstance().readAsset("storydetail.html").replace("###linedir###", "");
        if (item.getTitle() != null) {
            html3 = html3.replace("###TITLE###", item.getTitle());
        }
        if (item.getCreator() != null) {
            html = html3.replace("###CREATOR###", item.getCreator());
        } else {
            html = html3.replace("###CREATOR###", "");
        }
        if (item.getPubDate() != null) {
            html = html.replace("###DATE###", item.getPubDate().toLocaleString());
        }
        if (item.getBody() != null) {
            html = html.replace("###BODY###", item.getBody());
        }
        if (3 > 0) {
            html2 = html.replace("###HAVEPHOTO###", "block").replace("###NUMPHOTOS###", String.valueOf(item.getMediaItems().size())).replace("###PHOTOURL###", VerveManager.getInstance().getMediaItem(item, false));
        } else {
            html2 = html.replace("###HAVEPHOTO###", "none");
        }
        this.webView.loadDataWithBaseURL("file:///android_asset/", html2, "text/html", "utf-8", null);
        showControls();
        this.adManager.loadAd();
        VerveManager.getInstance().reportPageView(this.displayBlock, item);
    }

    /* access modifiers changed from: private */
    public void refresh(boolean isNext) {
        ScreenManager.getInstance().showProgress("Loading news ...", "");
        if (isNext) {
            if (this.position == this.count) {
                this.position = 0;
            } else {
                this.position++;
            }
        } else if (this.position == 0) {
            this.position = this.count;
        } else {
            this.position--;
        }
        this.textRight.setText(String.format("%d of %d", Integer.valueOf(this.position + 1), Integer.valueOf(this.count + 1)));
        display();
    }

    public class NewsWebViewClient extends WebViewClient {
        public NewsWebViewClient() {
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
            if (url.contains("http://showgallery")) {
                ContentItem item = (ContentItem) NewsDetailsScreen.this.contentItems.get(NewsDetailsScreen.this.position);
                if (item.getMediaItems().size() > 1) {
                    GalleryThumbsScreen thumbs = new GalleryThumbsScreen(item.getMediaItems(), "Story Photos");
                    thumbs.setShowTitle(true);
                    ScreenManager.getInstance().add(thumbs.getView());
                } else {
                    ScreenManager.getInstance().add(new GalleryScreen(item.getMediaItems(), 0, "Story Photos").getView());
                }
            } else {
                ShareManager.getInstance().openBrowser(url);
            }
            return true;
        }

        public void onPageFinished(WebView webView, String url) {
            ScreenManager.getInstance().closeProgress();
        }
    }

    public Menu getOptions(Menu menu) {
        ContentItem contentItem = this.contentItems.get(this.position);
        MenuItem item = menu.add("Share");
        item.setIcon((int) R.drawable.ic_menu_share);
        if (contentItem.getLink() == null) {
            item.setEnabled(false);
        } else {
            item.setEnabled(true);
        }
        if (!ScreenManager.getInstance().isSavedStory()) {
            if (!contentItem.isSaved()) {
                menu.add("Save").setIcon((int) R.drawable.ic_menu_save);
            }
        } else if (contentItem.isSaved()) {
            menu.add("Remove").setIcon((int) R.drawable.ic_menu_delete);
        }
        return menu;
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public void onOptionsMenuItem(String title) {
        if (title.contains("Share")) {
            ContentItem item = this.contentItems.get(this.position);
            ShareManager.getInstance().simpleShare(item.getTitle(), item.getLink());
        } else if (title.contains("Save")) {
            Logger.logDebug("Saving content item");
            ScreenManager.getInstance().showProgress("Saving ...", "");
            VerveManager.getInstance().getVerveApi().save(this.contentItems.get(this.position));
            this.contentItems.get(this.position).setSaved(true);
            ScreenManager.getInstance().closeProgress();
            ScreenManager.getInstance().setSavedStoryChange(true);
        } else if (title.contains("Remove")) {
            Logger.logDebug("Deleting saved item");
            ScreenManager.getInstance().showProgress("Removing ...", "");
            VerveManager.getInstance().getVerveApi().unsave(this.contentItems.get(this.position));
            this.contentItems.remove(this.position);
            ScreenManager.getInstance().closeProgress();
            ScreenManager.getInstance().setSavedStoryChange(true);
            this.count = this.contentItems.size() - 1;
            if (this.count == -1) {
                ScreenManager.getInstance().back();
                return;
            }
            this.position--;
            refresh(true);
        }
    }

    public void onShareItem(int shareId) {
    }

    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    public void showControls() {
        if (this.contentItems.size() >= 2) {
            if (this.btnPrev.getVisibility() != 0) {
                Animation a = this.mShowPrevAnimation;
                a.setDuration(500);
                this.btnPrev.startAnimation(a);
                this.btnPrev.setVisibility(0);
                this.btnNext.startAnimation(a);
                this.btnNext.setVisibility(0);
            }
            this.mHandler.removeCallbacks(this.mTimerTask);
            scheduleControls();
        }
    }

    /* access modifiers changed from: private */
    public void hideControls() {
        Animation a = this.mHidePrevAnimation;
        a.setDuration(500);
        this.btnPrev.startAnimation(a);
        this.btnPrev.setVisibility(8);
        this.btnNext.startAnimation(a);
        this.btnNext.setVisibility(8);
    }

    private void scheduleControls() {
        this.mTimerTask = new Runnable() {
            public void run() {
                NewsDetailsScreen.this.hideControls();
            }
        };
        this.mHandler.postDelayed(this.mTimerTask, 3000);
    }
}
