package com.ap.sacramento.views;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import com.ap.sacramento.R;
import com.ap.sacramento.common.BaseScreen;
import com.ap.sacramento.common.CustomWebView;
import com.ap.sacramento.common.Logger;
import com.ap.sacramento.managers.ScreenManager;
import com.ap.sacramento.managers.ShareManager;
import com.ap.sacramento.managers.VerveManager;
import com.vervewireless.capi.ContentItem;
import com.vervewireless.capi.MediaItem;
import java.util.List;

public class VideoDetailsScreen extends BaseScreen {
    private ImageView btnNext;
    private ImageView btnPrev;
    /* access modifiers changed from: private */
    public List<ContentItem> contentItems;
    private boolean controlsVisible = false;
    private int count;
    private Handler mHandler = new Handler();
    private final Animation mHideNextAnimation = new AlphaAnimation(1.0f, 0.0f);
    private final Animation mHidePrevAnimation = new AlphaAnimation(1.0f, 0.0f);
    private final Animation mShowNextAnimation = new AlphaAnimation(0.0f, 1.0f);
    private final Animation mShowPrevAnimation = new AlphaAnimation(0.0f, 1.0f);
    private Runnable mTimerTask;
    /* access modifiers changed from: private */
    public MediaItem mediaItem;
    /* access modifiers changed from: private */
    public int position = 0;
    private CustomWebView webView;

    public VideoDetailsScreen(List<ContentItem> contentItems2, int position2) {
        this.contentItems = contentItems2;
        this.position = position2;
        this.count = contentItems2.size() - 1;
        this.container = (ViewGroup) ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.newsdetailsscreen, (ViewGroup) null);
        this.container.setTag(this);
        this.webView = (CustomWebView) this.container.findViewById(R.id.webView);
        this.btnPrev = (ImageView) this.container.findViewById(R.id.btnPrev);
        this.btnPrev.setVisibility(8);
        this.btnPrev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                VideoDetailsScreen.this.refresh(false);
            }
        });
        this.btnNext = (ImageView) this.container.findViewById(R.id.btnNext);
        this.btnNext.setVisibility(8);
        this.btnNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                VideoDetailsScreen.this.refresh(true);
            }
        });
        this.textLeft = (TextView) this.container.findViewById(R.id.textLeft);
        this.textLeft.setText(ScreenManager.getInstance().getTitle());
        this.textRight = (TextView) this.container.findViewById(R.id.textRight);
        this.textRight.setText(String.format("%d of %d", Integer.valueOf(position2 + 1), Integer.valueOf(this.count + 1)));
        this.imgHeader = (ImageView) this.container.findViewById(R.id.imgHeader);
        this.titlePanel = this.container.findViewById(R.id.title);
        Drawable header = VerveManager.getInstance().getTitleHeader();
        if (header != null) {
            this.imgHeader.setImageDrawable(header);
        }
        this.webView.getSettings().setUseWideViewPort(true);
        this.webView.getSettings().setBuiltInZoomControls(true);
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.setWebViewClient(new NewsWebViewClient());
        this.webView.setParent(this);
        ScreenManager.getInstance().showProgress("Loading video ...", "");
    }

    public void closed(boolean isHidden) {
        if (!isHidden) {
        }
    }

    public void displayed(boolean isBack) {
        if (!isBack) {
            display();
        }
    }

    public void onTouch(MotionEvent event) {
        showControls();
        super.onTouch(event);
    }

    public void display() {
        String html;
        ContentItem item = this.contentItems.get(this.position);
        String html2 = ScreenManager.getInstance().readAsset("videodetail.html").replace("###linedir###", "");
        if (item.getTitle() != null) {
            html2 = html2.replace("###TITLE###", item.getTitle());
        }
        if (item.getCreator() != null) {
            html = html2.replace("###CREATOR###", item.getCreator());
        } else {
            html = html2.replace("###CREATOR###", "");
        }
        if (item.getPubDate() != null) {
            html = html.replace("###DATE###", item.getPubDate().toLocaleString());
        }
        if (item.getBody() != null) {
            html = html.replace("###BODY###", item.getBody());
        }
        this.mediaItem = VerveManager.getInstance().getVideoMediaItem(item);
        if (this.mediaItem != null) {
            html = html.replace("###VIDEOURL###", this.mediaItem.getThumbUrl());
        }
        this.webView.loadDataWithBaseURL("file:///android_asset/", html, "text/html", "utf-8", null);
    }

    /* access modifiers changed from: private */
    public void refresh(boolean isNext) {
        ScreenManager.getInstance().showProgress("Loading video ...", "");
        if (isNext) {
            if (this.position == this.count) {
                this.position = 0;
            } else {
                this.position++;
            }
        } else if (this.position == 0) {
            this.position = this.count;
        } else {
            this.position--;
        }
        this.textRight.setText(String.format("%d of %d", Integer.valueOf(this.position + 1), Integer.valueOf(this.count + 1)));
        display();
    }

    public class NewsWebViewClient extends WebViewClient {
        public NewsWebViewClient() {
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
            if (!url.contains("http://playvideo")) {
                return true;
            }
            ScreenManager.getInstance().showProgress("Opening video player ...", "");
            ContentItem contentItem = (ContentItem) VideoDetailsScreen.this.contentItems.get(VideoDetailsScreen.this.position);
            Intent playerIntent = new Intent("android.intent.action.VIEW");
            if (VideoDetailsScreen.this.mediaItem == null) {
                ScreenManager.getInstance().showDialogOK("Unable to play", "No supported media formats available.");
                return true;
            }
            ScreenManager.getInstance().showProgress("Opening video player ...", "");
            Logger.logDebug("Video trying to play: " + VideoDetailsScreen.this.mediaItem.getUrl());
            if (VideoDetailsScreen.this.mediaItem.getMediaType().startsWith("video/mp4")) {
                playerIntent.setDataAndType(Uri.parse(VideoDetailsScreen.this.mediaItem.getUrl()), "video/mp4");
            } else {
                playerIntent.setDataAndType(Uri.parse(VideoDetailsScreen.this.mediaItem.getUrl()), "video/3gpp");
            }
            playerIntent.putExtra("android.intent.extra.fullScreen", "true");
            ScreenManager.getInstance().getContext().startActivityForResult(playerIntent, 0);
            return true;
        }

        public void onPageFinished(WebView webView, String url) {
            ScreenManager.getInstance().closeProgress();
        }
    }

    public Menu getOptions(Menu menu) {
        MenuItem item = menu.add("Share");
        item.setIcon((int) R.drawable.ic_menu_share);
        if (this.contentItems.get(this.position).getLink() == null) {
            item.setEnabled(false);
        } else {
            item.setEnabled(true);
        }
        return menu;
    }

    public void onOptionsMenuItem(String title) {
        if (title.contains("Share")) {
            ContentItem item = this.contentItems.get(this.position);
            ShareManager.getInstance().simpleShare(item.getTitle(), item.getLink());
        }
    }

    public void onShareItem(int shareId) {
    }

    public void showControls() {
        if (this.btnPrev.getVisibility() != 0) {
            Animation a = this.mShowPrevAnimation;
            a.setDuration(500);
            this.btnPrev.startAnimation(a);
            this.btnPrev.setVisibility(0);
            this.btnNext.startAnimation(a);
            this.btnNext.setVisibility(0);
        }
        this.mHandler.removeCallbacks(this.mTimerTask);
        scheduleControls();
    }

    /* access modifiers changed from: private */
    public void hideControls() {
        Animation a = this.mHidePrevAnimation;
        a.setDuration(500);
        this.btnPrev.startAnimation(a);
        this.btnPrev.setVisibility(8);
        this.btnNext.startAnimation(a);
        this.btnNext.setVisibility(8);
    }

    private void scheduleControls() {
        this.mTimerTask = new Runnable() {
            public void run() {
                VideoDetailsScreen.this.hideControls();
            }
        };
        this.mHandler.postDelayed(this.mTimerTask, 3000);
    }
}
