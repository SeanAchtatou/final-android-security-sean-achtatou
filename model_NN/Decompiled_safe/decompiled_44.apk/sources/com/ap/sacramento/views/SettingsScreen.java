package com.ap.sacramento.views;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.ap.sacramento.R;
import com.ap.sacramento.common.BaseScreen;
import com.ap.sacramento.common.Logger;
import com.ap.sacramento.managers.ScreenManager;
import com.ap.sacramento.managers.ShareManager;
import com.ap.sacramento.managers.VerveManager;
import com.vervewireless.capi.Locale;
import com.vervewireless.capi.LocaleListener;
import com.vervewireless.capi.PartnerModules;
import com.vervewireless.capi.VerveError;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class SettingsScreen extends BaseScreen {
    View category_row;
    /* access modifiers changed from: private */
    public int currentLocationMode;
    private ListView listSettings;
    List<View> listViews;
    List<Locale> locales;
    /* access modifiers changed from: private */
    public int originalLocaleIndex = -1;
    /* access modifiers changed from: private */
    public int selectedIndex = -1;
    /* access modifiers changed from: private */
    public Locale selectedLocale;
    /* access modifiers changed from: private */
    public int selectedLocationMode;
    /* access modifiers changed from: private */
    public SettingsAdapter settingsAdapter;
    String[] settingsItems = {"Post-Dispatch", "Edition", "Recommend", "Contact Support", "Send a Story", "About"};
    View settings_edition;
    View settings_location;
    View settings_reset;
    View settings_single_about;
    View settings_single_contact;
    View settings_single_recommend;
    View settings_single_send;

    public SettingsScreen() {
        this.container = (ViewGroup) ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.settingsscreen, (ViewGroup) null);
        this.container.setTag(this);
        this.listViews = new LinkedList();
        this.selectedLocale = VerveManager.getInstance().getVerveApi().getLocale();
        this.listSettings = (ListView) this.container.findViewById(R.id.listSettings);
        this.settingsAdapter = new SettingsAdapter();
        this.listSettings.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                switch (arg2) {
                    case CapiChangeListener.CAPI_STATUS_REREG /*1*/:
                        ScreenManager.getInstance().showProgress("Loading locales ...", "");
                        VerveManager.getInstance().getVerveApi().getLocales(new LocaleListener() {
                            public void onLocalesRecieved(List<Locale> locales) {
                                int count = 0;
                                Iterator i$ = locales.iterator();
                                while (true) {
                                    if (!i$.hasNext()) {
                                        break;
                                    } else if (i$.next().getName().compareTo(SettingsScreen.this.selectedLocale.getName()) == 0) {
                                        int unused = SettingsScreen.this.originalLocaleIndex = SettingsScreen.this.selectedIndex = count;
                                        break;
                                    } else {
                                        count++;
                                    }
                                }
                                ScreenManager.getInstance().closeProgress();
                                SettingsScreen.this.showLocales(locales);
                            }

                            public void onLocalesFailed(VerveError error) {
                                ScreenManager.getInstance().closeProgress();
                                ScreenManager.getInstance().showDialog("Show locales", error.toString());
                            }
                        });
                        return;
                    case CapiChangeListener.CAPI_STATUS_HIER /*2*/:
                        ShareManager.getInstance().sendEmail("Recommended App: " + ScreenManager.getInstance().getContext().getString(R.string.app_name), String.format("I've been using the %s app. Try it out by downloading it from the Android market http://market.android.com/details?id=%s", ScreenManager.getInstance().getContext().getString(R.string.app_name), ScreenManager.getInstance().getContext().getPackageName().toString()), "Recommend", "");
                        return;
                    case 3:
                        ShareManager.getInstance().sendEmail(ScreenManager.getInstance().getContext().getString(R.string.app_name) + " Support Request", "", "Contact Support", "appsupport@vervewireless.com");
                        return;
                    case 4:
                        ScreenManager.getInstance().add(new SendStoryScreen(true).getView());
                        return;
                    case 5:
                        ScreenManager.getInstance().add(new AboutScreen(true).getView());
                        return;
                    case 6:
                        AlertDialog.Builder builder = new AlertDialog.Builder(ScreenManager.getInstance().getContext());
                        builder.setTitle("Warning");
                        builder.setIcon(17301543);
                        builder.setMessage("Are you sure you would like to reset all settings and cache?");
                        builder.setNegativeButton("Cancel", (DialogInterface.OnClickListener) null);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                ScreenManager.getInstance().setMainScreenIndex(0);
                                ScreenManager.getInstance().setCustomizeStates(null);
                                ScreenManager.getInstance().setLocationMode(3);
                                VerveManager.getInstance().getVerveApi().reset();
                                ScreenManager.getInstance().saveMenuItems(new int[0]);
                                ScreenManager.getInstance().registerLocale(null);
                            }
                        });
                        builder.show();
                        return;
                    case 7:
                        int unused = SettingsScreen.this.selectedLocationMode = SettingsScreen.this.currentLocationMode = ScreenManager.getInstance().getLocationMode();
                        AlertDialog.Builder builder2 = new AlertDialog.Builder(ScreenManager.getInstance().getContext());
                        builder2.setTitle("Select location mode");
                        builder2.setCancelable(true);
                        new DialogButtonHandler();
                        builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (SettingsScreen.this.currentLocationMode != SettingsScreen.this.selectedLocationMode) {
                                    ScreenManager.getInstance().setLocationMode(SettingsScreen.this.selectedLocationMode);
                                    SettingsScreen.this.setLocationTitle();
                                    if (SettingsScreen.this.selectedLocationMode == 0) {
                                        if (!VerveManager.getInstance().isProviderEnabled()) {
                                            ScreenManager.getInstance().setLocationMode(1);
                                            SettingsScreen.this.setLocationTitle();
                                        }
                                        VerveManager.getInstance().getLocation(false);
                                        return;
                                    }
                                    VerveManager.getInstance().showPostalDialog(null, true);
                                }
                            }
                        });
                        builder2.setNegativeButton("Cancel", (DialogInterface.OnClickListener) null);
                        builder2.setSingleChoiceItems(new ArrayAdapter(ScreenManager.getInstance().getContext(), 17367058, new String[]{"GPS", "Postal code"}), SettingsScreen.this.currentLocationMode, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                int unused = SettingsScreen.this.selectedLocationMode = which;
                            }
                        });
                        builder2.create().show();
                        return;
                    default:
                        return;
                }
            }
        });
        this.titlePanel = this.container.findViewById(R.id.title);
        this.imgHeader = (ImageView) this.container.findViewById(R.id.imgHeader);
        this.textLeft = (TextView) this.container.findViewById(R.id.textLeft);
        this.textLeft.setText("Settings");
        this.textRight = (TextView) this.container.findViewById(R.id.textRight);
        this.textRight.setVisibility(8);
        Drawable header = VerveManager.getInstance().getTitleHeader();
        if (header != null) {
            this.imgHeader.setImageDrawable(header);
        }
        ScreenManager.getInstance().showProgress("Loading ...", "");
    }

    public void closed(boolean isHidden) {
    }

    public void displayed(boolean isBack) {
        if (!isBack) {
            VerveManager mng = VerveManager.getInstance();
            mng.reportCustomPageview(null, null, Integer.valueOf(PartnerModules.SETTINGS));
            this.selectedLocale = mng.getVerveApi().getLocale();
            this.category_row = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.category_row, (ViewGroup) null);
            ((TextView) this.category_row.findViewById(R.id.textCategoryTitle)).setText("Post-Dispatch");
            this.settings_edition = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.settings_row, (ViewGroup) null);
            ((TextView) this.settings_edition.findViewById(R.id.textTitle)).setText("Edition");
            ((TextView) this.settings_edition.findViewById(R.id.textSubtitle)).setText(this.selectedLocale.getName());
            this.settings_single_recommend = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.settings_single_row, (ViewGroup) null);
            ((TextView) this.settings_single_recommend.findViewById(R.id.textTitle)).setText("Recommend");
            this.settings_single_contact = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.settings_single_row, (ViewGroup) null);
            ((TextView) this.settings_single_contact.findViewById(R.id.textTitle)).setText("Contact Support");
            this.settings_single_send = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.settings_single_row, (ViewGroup) null);
            ((TextView) this.settings_single_send.findViewById(R.id.textTitle)).setText("Send a Story");
            ((ImageView) this.settings_single_send.findViewById(R.id.imgIcon)).setVisibility(0);
            this.settings_single_about = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.settings_single_row, (ViewGroup) null);
            ((TextView) this.settings_single_about.findViewById(R.id.textTitle)).setText("About");
            this.settings_reset = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.settings_row, (ViewGroup) null);
            ((TextView) this.settings_reset.findViewById(R.id.textTitle)).setText("Reset");
            ((TextView) this.settings_reset.findViewById(R.id.textSubtitle)).setText("Resets the settings and cache");
            ((ImageView) this.settings_reset.findViewById(R.id.imgIcon)).setVisibility(8);
            this.settings_location = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.settings_row, (ViewGroup) null);
            ((TextView) this.settings_location.findViewById(R.id.textTitle)).setText("Location");
            setLocationTitle();
            this.listViews.add(this.category_row);
            this.listViews.add(this.settings_edition);
            this.listViews.add(this.settings_single_recommend);
            this.listViews.add(this.settings_single_contact);
            this.listViews.add(this.settings_single_send);
            this.listViews.add(this.settings_single_about);
            this.listViews.add(this.settings_reset);
            this.listViews.add(this.settings_location);
            this.listSettings.setAdapter((ListAdapter) this.settingsAdapter);
            ScreenManager.getInstance().closeProgress();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    /* access modifiers changed from: private */
    public void setLocationTitle() {
        if (ScreenManager.getInstance().getLocationMode() == 0) {
            ((TextView) this.settings_location.findViewById(R.id.textSubtitle)).setText("GPS");
        } else {
            ((TextView) this.settings_location.findViewById(R.id.textSubtitle)).setText("Postal code");
        }
    }

    public void display() {
    }

    class SettingsAdapter extends BaseAdapter {
        SettingsAdapter() {
        }

        public int getCount() {
            return SettingsScreen.this.listViews.size();
        }

        public Object getItem(int position) {
            return SettingsScreen.this.listViews.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            Logger.logDebug("SettingsScreen getView");
            return SettingsScreen.this.listViews.get(position);
        }
    }

    public void showLocales(List<Locale> locales2) {
        this.locales = locales2;
        AlertDialog.Builder builder = new AlertDialog.Builder(ScreenManager.getInstance().getContext());
        builder.setTitle("Select locale");
        builder.setCancelable(true);
        DialogButtonHandler handler = new DialogButtonHandler();
        builder.setPositiveButton("OK", handler);
        builder.setNegativeButton("Cancel", handler);
        builder.setSingleChoiceItems(new ArrayAdapter(ScreenManager.getInstance().getContext(), 17367058, locales2), this.selectedIndex, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                int unused = SettingsScreen.this.selectedIndex = which;
            }
        });
        builder.create().show();
    }

    public void changeLocale() {
    }

    class DialogButtonHandler implements DialogInterface.OnClickListener {
        DialogButtonHandler() {
        }

        public void onClick(DialogInterface dialog, int button) {
            if (button != -1) {
                if (button == -2) {
                }
            } else if (SettingsScreen.this.originalLocaleIndex != SettingsScreen.this.selectedIndex) {
                Locale unused = SettingsScreen.this.selectedLocale = SettingsScreen.this.locales.get(SettingsScreen.this.selectedIndex);
                int unused2 = SettingsScreen.this.originalLocaleIndex = SettingsScreen.this.selectedIndex;
                SettingsScreen.this.settingsAdapter.notifyDataSetChanged();
                ScreenManager.getInstance().setMainScreenIndex(0);
                ScreenManager.getInstance().setCustomizeStates(null);
                ScreenManager.getInstance().saveMenuItems(new int[0]);
                ScreenManager.getInstance().registerLocale(SettingsScreen.this.selectedLocale);
            }
        }
    }
}
