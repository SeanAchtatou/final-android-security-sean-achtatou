package com.ap.sacramento.views;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.ap.sacramento.R;
import com.ap.sacramento.common.BaseScreen;
import com.ap.sacramento.common.ImageDownloader;
import com.ap.sacramento.common.Logger;
import com.ap.sacramento.managers.ScreenManager;
import com.ap.sacramento.managers.VerveManager;
import com.vervewireless.capi.ContentItem;
import com.vervewireless.capi.ContentListener;
import com.vervewireless.capi.ContentRequest;
import com.vervewireless.capi.ContentResponse;
import com.vervewireless.capi.DisplayBlock;
import com.vervewireless.capi.MediaItem;
import com.vervewireless.capi.VerveError;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class VideoScreen extends BaseScreen implements ContentListener {
    final float SCALE = ScreenManager.getInstance().getContext().getResources().getDisplayMetrics().density;
    /* access modifiers changed from: private */
    public List<ContentItem> contentItems;
    private DisplayBlock displayBlock;
    /* access modifiers changed from: private */
    public ImageDownloader imageDownloader;
    private Date lastUpdate;
    private ListView listVideos;
    String strValuePixels;
    int valuePixels;
    private VideoAdapter videoAdapter;

    public VideoScreen(DisplayBlock displayBlock2) {
        this.displayBlock = displayBlock2;
        this.lastUpdate = new Date();
        this.container = (ViewGroup) ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.videoscreen, (ViewGroup) null);
        this.container.setTag(this);
        this.contentItems = new LinkedList();
        this.listVideos = (ListView) this.container.findViewById(R.id.listVideos);
        this.listVideos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                VideoScreen.this.onVideoItem(arg2);
            }
        });
        this.imageDownloader = new ImageDownloader();
        this.imageDownloader.setMode(ImageDownloader.Mode.CORRECT);
        this.titlePanel = this.container.findViewById(R.id.title);
        this.titlePanel.setVisibility(8);
        this.valuePixels = (int) ((60.0f * this.SCALE) + 0.5f);
        this.strValuePixels = String.valueOf(this.valuePixels);
    }

    public void closed(boolean isHidden) {
        super.closed(isHidden);
        if (!isHidden) {
            this.imageDownloader.clearCache();
            this.imageDownloader = null;
        }
    }

    public void displayed(boolean isBack) {
        super.displayed(isBack);
        if (!isBack) {
            this.state = 1;
            this.videoAdapter = new VideoAdapter();
            try {
                this.listVideos.setAdapter((ListAdapter) this.videoAdapter);
                this.videoAdapter.notifyDataSetChanged();
            } catch (Exception e) {
            }
            ScreenManager.getInstance().showProgress("Loading videos ...", "");
            VerveManager.getInstance().getVerveApi().getContent(new ContentRequest(this.displayBlock), this);
        }
        VerveManager.getInstance().getMainAdManager().setDisplayBlock(this.displayBlock);
        VerveManager.getInstance().getMainAdManager().loadAd();
    }

    public void reportPageView() {
        reportPageView(this.displayBlock, this.contentItems);
    }

    /* access modifiers changed from: private */
    public void onVideoItem(int position) {
        ScreenManager.getInstance().add(new VideoDetailsScreen(this.contentItems, position).getView());
    }

    public void onContentFailed(VerveError e) {
        ScreenManager.getInstance().closeProgress();
        ScreenManager.getInstance().showDialog("Failed", e.toString());
        showEmptyView();
        this.listVideos.setEmptyView(this.empty);
        display();
    }

    public void onContentRecieved(ContentResponse response) {
        ScreenManager.getInstance().closeProgress();
        this.contentItems = response.getItems();
        if (this.contentItems.size() == 0) {
            showEmptyView();
            this.listVideos.setEmptyView(this.empty);
        }
        this.lastUpdate = response.getLastUpdate();
        reportPageView(this.displayBlock, this.contentItems);
        display();
    }

    public void display() {
        this.videoAdapter.notifyDataSetChanged();
    }

    class VideoAdapter extends BaseAdapter {
        VideoAdapter() {
        }

        public int getCount() {
            return VideoScreen.this.contentItems.size();
        }

        public Object getItem(int position) {
            return VideoScreen.this.contentItems.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            NewsViewHolder holder;
            ContentItem item = (ContentItem) VideoScreen.this.contentItems.get(position);
            if (convertView == null) {
                convertView = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.news_row, (ViewGroup) null);
                holder = new NewsViewHolder();
                holder.textNews = (TextView) convertView.findViewById(R.id.textNews);
                holder.textNewsDate = (TextView) convertView.findViewById(R.id.textNewsDate);
                holder.imgIcon = (ImageView) convertView.findViewById(R.id.imgIcon);
                holder.imgIcon.setBackgroundDrawable(VerveManager.getInstance().getThumb());
                convertView.setTag(holder);
            } else {
                holder = (NewsViewHolder) convertView.getTag();
            }
            holder.textNews.setText(item.getTitle());
            holder.textNewsDate.setText(item.getPubDate().toLocaleString());
            if (VideoScreen.this.isDisplayed) {
                MediaItem media = VerveManager.getInstance().getVideoMediaItem(item);
                if (media != null) {
                    holder.imgIcon.setVisibility(0);
                    Logger.logDebug("VideoScreen Download image " + media.getThumbUrl());
                    VideoScreen.this.imageDownloader.download(media.getThumbUrl() + "&width=" + VideoScreen.this.strValuePixels + "&height=" + VideoScreen.this.strValuePixels + "&crop=true", holder.imgIcon);
                } else {
                    holder.imgIcon.setVisibility(8);
                }
            }
            return convertView;
        }
    }

    class NewsViewHolder {
        public ImageView imgIcon;
        public TextView textNews;
        public TextView textNewsDate;

        NewsViewHolder() {
        }
    }

    public void refresh() {
        removeEmptyView();
        ScreenManager.getInstance().showProgress("Refresing videos ...", "");
        VerveManager.getInstance().getVerveApi().getContent(new ContentRequest(this.displayBlock), this);
    }

    public Menu getOptions(Menu menu) {
        Date current = new Date();
        Logger.logDebug("Last update " + this.lastUpdate.toString());
        Logger.logDebug("Current " + current.toString());
        long delta = current.getTime() - this.lastUpdate.getTime();
        Logger.logDebug("Delta time " + String.valueOf(delta));
        if (delta >= 60000) {
            MenuItem item = menu.add("Refresh");
            item.setIcon((int) R.drawable.ic_menu_refresh);
            item.setEnabled(true);
        } else {
            MenuItem item2 = menu.add("Up-to-Date");
            item2.setIcon((int) R.drawable.ic_menu_refresh);
            item2.setEnabled(false);
        }
        menu.add("Search").setIcon((int) R.drawable.ic_menu_search);
        return menu;
    }

    public void onOptionsMenuItem(String title) {
        if (title.contains("Refresh")) {
            refresh();
        }
    }
}
