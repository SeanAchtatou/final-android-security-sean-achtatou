package com.ap.sacramento.views;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import com.ap.sacramento.R;
import com.ap.sacramento.common.BaseScreen;
import com.ap.sacramento.common.CustomWebView;
import com.ap.sacramento.common.Logger;
import com.ap.sacramento.common.TransparentPanel;
import com.ap.sacramento.managers.ScreenManager;
import com.ap.sacramento.managers.ShareManager;
import com.ap.sacramento.managers.VerveManager;
import com.vervewireless.capi.ContentItem;
import com.vervewireless.capi.MediaItem;
import java.util.List;

public class GalleryScreen extends BaseScreen {
    private Animation animHide;
    private Animation animShow;
    private ImageView btnNext;
    private ImageView btnPrev;
    List<ContentItem> contentItems = null;
    private boolean controlsVisible = false;
    int count = 0;
    private Handler mHandler = new Handler();
    private final Animation mHideNextAnimation = new AlphaAnimation(1.0f, 0.0f);
    private final Animation mHidePrevAnimation = new AlphaAnimation(1.0f, 0.0f);
    private final Animation mShowNextAnimation = new AlphaAnimation(0.0f, 1.0f);
    private final Animation mShowPrevAnimation = new AlphaAnimation(0.0f, 1.0f);
    private Runnable mTimerTask;
    List<MediaItem> mediaItems = null;
    int mode = 0;
    private TransparentPanel popup;
    int popupTouchAction = 0;
    int position = 0;
    TextView txtCaption;
    int webTouchAction = 0;
    private CustomWebView webView;

    public GalleryScreen(List<MediaItem> mediaItems2, int position2, String title) {
        init(mediaItems2, position2, title);
    }

    public GalleryScreen(List<MediaItem> mediaItems2, List<ContentItem> contentItems2, int position2, String title) {
        this.contentItems = contentItems2;
        this.mode = 1;
        init(mediaItems2, position2, title);
    }

    private void init(List<MediaItem> mediaItems2, int position2, String title) {
        this.mediaItems = mediaItems2;
        this.position = position2;
        this.container = (ViewGroup) ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.galleryscreen, (ViewGroup) null);
        this.container.setTag(this);
        this.webView = (CustomWebView) this.container.findViewById(R.id.webView);
        this.webView.setBackgroundColor(Color.parseColor("#FF191919"));
        this.webView.setWebViewClient(new GalleryWebViewClient());
        this.webView.getSettings().setBuiltInZoomControls(true);
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.setWebChromeClient(new GalleryWebChromeClient());
        this.webView.setParent(this);
        this.btnPrev = (ImageView) this.container.findViewById(R.id.btnPrev);
        this.btnPrev.setVisibility(8);
        this.btnPrev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GalleryScreen.this.refresh(false);
            }
        });
        this.btnNext = (ImageView) this.container.findViewById(R.id.btnNext);
        this.btnNext.setVisibility(8);
        this.btnNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GalleryScreen.this.refresh(true);
            }
        });
        this.count = mediaItems2.size() - 1;
        this.textLeft = (TextView) this.container.findViewById(R.id.textLeft);
        this.textLeft.setText(title);
        this.textRight = (TextView) this.container.findViewById(R.id.textRight);
        this.textRight.setText(String.format("%d of %d", Integer.valueOf(position2 + 1), Integer.valueOf(this.count + 1)));
        this.imgHeader = (ImageView) this.container.findViewById(R.id.imgHeader);
        this.titlePanel = this.container.findViewById(R.id.title);
        Drawable header = VerveManager.getInstance().getTitleHeader();
        if (header != null) {
            this.imgHeader.setImageDrawable(header);
        }
        ScreenManager.getInstance().showProgress("Loading photo ...", "");
        this.popup = (TransparentPanel) this.container.findViewById(R.id.popup_window);
        this.popup.setVisibility(8);
        this.txtCaption = (TextView) this.container.findViewById(R.id.txtcaption);
        this.animShow = AnimationUtils.loadAnimation(ScreenManager.getInstance().getContext(), R.anim.popup_show);
        this.animHide = AnimationUtils.loadAnimation(ScreenManager.getInstance().getContext(), R.anim.popup_hide);
        this.popup.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                Logger.logDebug("TransparentPanel onTouchEvent " + event.toString());
                return true;
            }
        });
    }

    public void closed(boolean isHidden) {
        if (!isHidden) {
        }
    }

    public void displayed(boolean isBack) {
        if (!isBack) {
            display();
        }
    }

    public void onTouch(MotionEvent event) {
        Log.i("GalleryScreen", "onTouch:" + event.toString());
        showControls();
        showCaption();
        this.webTouchAction = event.getAction();
        super.onTouch(event);
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    private String getTitle(int position2) {
        if (this.contentItems.get(position2).getTitle() != null) {
            return this.contentItems.get(position2).getTitle();
        }
        return null;
    }

    private String getBody(int position2) {
        String body = this.contentItems.get(position2).getBody();
        if (body != null) {
            return body;
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void showCaption() {
        displayCaption();
    }

    private void displayCaption() {
        String caption;
        if (this.popup.getVisibility() == 0) {
            this.popup.setVisibility(8);
            this.txtCaption.setText("");
            this.webTouchAction = 0;
            this.webView.requestFocus();
            return;
        }
        if (this.mode == 1) {
            caption = getBody(this.position);
        } else {
            caption = this.mediaItems.get(this.position).getCaption();
        }
        Logger.logDebug("displayCaption " + caption);
        if (caption == null) {
            caption = "No caption available.";
        } else if (caption.length() < 2) {
            caption = "No caption available.";
        }
        this.txtCaption.setText(caption.replace("<p>", "").replace("</p>", ""));
        this.popup.setVisibility(0);
    }

    public void display() {
        MediaItem item = this.mediaItems.get(this.position);
        String html = ScreenManager.getInstance().readAsset("imagepreview.html");
        if (item.getUrl() != null) {
            html = html.replace("###PHOTOURL###", item.getUrl());
        }
        if (this.mode == 1) {
            this.textLeft.setText(ScreenManager.getInstance().getTitle());
            this.textLeft.setMarqueeRepeatLimit(-1);
        }
        this.webView.loadDataWithBaseURL("file:///android_asset/", html, "text/html", "utf-8", null);
    }

    public void reportPageView() {
    }

    /* access modifiers changed from: private */
    public void refresh(boolean isNext) {
        if (this.popup.getVisibility() == 0) {
            showCaption();
        }
        ScreenManager.getInstance().showProgress("Loading photo ...", "");
        if (isNext) {
            if (this.position == this.count) {
                this.position = 0;
            } else {
                this.position++;
            }
        } else if (this.position == 0) {
            this.position = this.count;
        } else {
            this.position--;
        }
        this.textRight.setText(String.format("%d of %d", Integer.valueOf(this.position + 1), Integer.valueOf(this.count + 1)));
        display();
    }

    public class GalleryWebViewClient extends WebViewClient {
        public GalleryWebViewClient() {
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
            if (url.contains("http://shownext")) {
                GalleryScreen.this.refresh(true);
            } else if (url.contains("http://showprev")) {
                GalleryScreen.this.refresh(false);
            }
            return true;
        }

        public void onPageFinished(WebView webView, String url) {
            ScreenManager.getInstance().closeProgress();
            GalleryScreen.this.showCaption();
        }
    }

    public class GalleryWebChromeClient extends WebChromeClient {
        public GalleryWebChromeClient() {
        }

        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            if (message.contains("done")) {
            }
            return true;
        }
    }

    public void showControls() {
        if (this.mediaItems.size() >= 2) {
            if (this.btnPrev.getVisibility() != 0) {
                Animation a = this.mShowPrevAnimation;
                a.setDuration(500);
                this.btnPrev.startAnimation(a);
                this.btnPrev.setVisibility(0);
                this.btnNext.startAnimation(a);
                this.btnNext.setVisibility(0);
            }
            this.mHandler.removeCallbacks(this.mTimerTask);
            scheduleControls();
        }
    }

    /* access modifiers changed from: private */
    public void hideControls() {
        Animation a = this.mHidePrevAnimation;
        a.setDuration(500);
        this.btnPrev.startAnimation(a);
        this.btnPrev.setVisibility(8);
        this.btnNext.startAnimation(a);
        this.btnNext.setVisibility(8);
    }

    private void scheduleControls() {
        this.mTimerTask = new Runnable() {
            public void run() {
                GalleryScreen.this.hideControls();
            }
        };
        this.mHandler.postDelayed(this.mTimerTask, 3000);
    }

    public Menu getOptions(Menu menu) {
        if (this.mode == 1) {
            MenuItem item = menu.add("Share");
            item.setIcon((int) R.drawable.ic_menu_share);
            if (this.contentItems.get(this.position).getLink() == null) {
                item.setEnabled(false);
            } else {
                item.setEnabled(true);
            }
        }
        return menu;
    }

    public void onOptionsMenuItem(String title) {
        if (title.contains("Caption")) {
            displayCaption();
        } else if (title.contains("Share")) {
            ContentItem contentItem = this.contentItems.get(this.position);
            ShareManager.getInstance().simpleShare(contentItem.getTitle(), contentItem.getLink());
        }
    }
}
