package com.ap.sacramento.views;

import android.content.res.Configuration;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ap.sacramento.R;
import com.ap.sacramento.common.BaseScreen;
import com.ap.sacramento.common.Logger;
import com.ap.sacramento.managers.ScreenManager;
import com.ap.sacramento.managers.VerveManager;
import com.vervewireless.capi.DisplayBlock;
import java.util.ArrayList;
import java.util.List;

public class CustomizeScreen extends BaseScreen {
    final float SCALE = ScreenManager.getInstance().getContext().getResources().getDisplayMetrics().density;
    private List<DisplayBlock> displayBlocks;
    /* access modifiers changed from: private */
    public Button dragItem;
    private FrameLayout dragPanel;
    /* access modifiers changed from: private */
    public int dragWidth = 70;
    View.OnTouchListener dragm = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            int index = Integer.parseInt(v.getTag().toString());
            int position = CustomizeScreen.this.selectedMenuItems[index];
            if (event.getAction() == 0) {
                CustomizeScreen.this.createDragItem(v.getLeft(), CustomizeScreen.this.menu.getTop() - 20, position);
                FrameLayout.LayoutParams dragLayout = (FrameLayout.LayoutParams) CustomizeScreen.this.dragItem.getLayoutParams();
                dragLayout.height = CustomizeScreen.this.dragWidth;
                dragLayout.width = CustomizeScreen.this.dragWidth;
                CustomizeScreen.this.dragItem.setLayoutParams(dragLayout);
            } else if (event.getAction() == 2) {
                FrameLayout.LayoutParams dragLayout2 = (FrameLayout.LayoutParams) CustomizeScreen.this.dragItem.getLayoutParams();
                dragLayout2.topMargin = (((int) event.getRawY()) - v.getHeight()) - (v.getWidth() / 2);
                dragLayout2.leftMargin = ((int) event.getRawX()) - (v.getWidth() / 2);
                CustomizeScreen.this.dragItem.setLayoutParams(dragLayout2);
                CustomizeScreen.this.checkOver(index);
            } else if (event.getAction() == 1) {
                FrameLayout.LayoutParams dragLayout3 = (FrameLayout.LayoutParams) CustomizeScreen.this.dragItem.getLayoutParams();
                dragLayout3.topMargin = (((int) event.getRawY()) - v.getHeight()) - (v.getWidth() / 2);
                dragLayout3.leftMargin = ((int) event.getRawX()) - (v.getWidth() / 2);
                CustomizeScreen.this.dragItem.setLayoutParams(dragLayout3);
                CustomizeScreen.this.checkHit(index);
                CustomizeScreen.this.removeDragItem();
                CustomizeScreen.this.deselectMenuItems(-1);
            }
            return true;
        }
    };
    View.OnTouchListener dragt = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) v.getLayoutParams();
            switch (v.getId()) {
                case R.id.dragItem:
                    switch (event.getAction()) {
                        case 0:
                            CustomizeScreen.this.createDragItem(v.getLeft() - (v.getWidth() / 3), v.getTop(), Integer.parseInt(v.getTag().toString()));
                            FrameLayout.LayoutParams dragLayout = (FrameLayout.LayoutParams) CustomizeScreen.this.dragItem.getLayoutParams();
                            dragLayout.height = CustomizeScreen.this.dragWidth;
                            dragLayout.width = CustomizeScreen.this.dragWidth;
                            CustomizeScreen.this.dragItem.setLayoutParams(dragLayout);
                            CustomizeScreen.this.dragItem.setTag(v);
                            return true;
                        case CapiChangeListener.CAPI_STATUS_REREG /*1*/:
                            FrameLayout.LayoutParams dragLayout2 = (FrameLayout.LayoutParams) CustomizeScreen.this.dragItem.getLayoutParams();
                            dragLayout2.topMargin = (((int) event.getRawY()) - v.getHeight()) - (v.getWidth() / 2);
                            dragLayout2.leftMargin = ((int) event.getRawX()) - v.getWidth();
                            CustomizeScreen.this.dragItem.setLayoutParams(dragLayout2);
                            CustomizeScreen.this.checkHit(-1);
                            CustomizeScreen.this.removeDragItem();
                            CustomizeScreen.this.deselectMenuItems(-1);
                            return true;
                        case CapiChangeListener.CAPI_STATUS_HIER /*2*/:
                            FrameLayout.LayoutParams dragLayout3 = (FrameLayout.LayoutParams) CustomizeScreen.this.dragItem.getLayoutParams();
                            dragLayout3.topMargin = ((int) event.getRawY()) - v.getHeight();
                            dragLayout3.leftMargin = ((int) event.getRawX()) - v.getWidth();
                            CustomizeScreen.this.dragItem.setLayoutParams(dragLayout3);
                            CustomizeScreen.this.checkOver(-1);
                            return true;
                        default:
                            return true;
                    }
                default:
                    return true;
            }
        }
    };
    private int itemHeight = 60;
    private int itemWidth = 60;
    private List<Button> listDragItems;
    private List<Button> listMenuButtons;
    /* access modifiers changed from: private */
    public LinearLayout menu;
    private int menuDeltaWidth;
    /* access modifiers changed from: private */
    public int[] selectedMenuItems;

    public CustomizeScreen(int[] selectedMenuItems2, List<DisplayBlock> displayBlocks2) {
        this.selectedMenuItems = selectedMenuItems2;
        this.displayBlocks = displayBlocks2;
        this.itemWidth = (int) ((((float) this.itemWidth) * this.SCALE) + 0.5f);
        this.itemHeight = (int) ((((float) this.itemHeight) * this.SCALE) + 0.5f);
        this.menuDeltaWidth = this.itemWidth / 3;
        this.dragWidth = (int) ((((float) this.dragWidth) * this.SCALE) + 0.5f);
        this.container = (ViewGroup) ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.customizescreen, (ViewGroup) null);
        this.container.setTag(this);
        this.textLeft = (TextView) this.container.findViewById(R.id.textLeft);
        this.textLeft.setText("Customize");
        this.textRight = (TextView) this.container.findViewById(R.id.textRight);
        this.imgHeader = (ImageView) this.container.findViewById(R.id.imgHeader);
        this.titlePanel = this.container.findViewById(R.id.title);
        Drawable header = VerveManager.getInstance().getTitleHeader();
        if (header != null) {
            this.imgHeader.setImageDrawable(header);
        }
        this.textRight.setVisibility(8);
        this.dragPanel = (FrameLayout) this.container.findViewById(R.id.dragPanel);
        this.menu = (LinearLayout) this.container.findViewById(R.id.mainMenu);
        this.menu.getBackground().setAlpha(70);
        this.listMenuButtons = new ArrayList();
        Button btn = (Button) this.container.findViewById(R.id.btnMenu1);
        this.listMenuButtons.add(btn);
        btn.setOnTouchListener(this.dragm);
        Button btn2 = (Button) this.container.findViewById(R.id.btnMenu2);
        this.listMenuButtons.add(btn2);
        btn2.setOnTouchListener(this.dragm);
        Button btn3 = (Button) this.container.findViewById(R.id.btnMenu3);
        this.listMenuButtons.add(btn3);
        btn3.setOnTouchListener(this.dragm);
        Button btn4 = (Button) this.container.findViewById(R.id.btnMenu4);
        this.listMenuButtons.add(btn4);
        btn4.setOnTouchListener(this.dragm);
        ((Button) this.container.findViewById(R.id.btnMenu5)).setVisibility(8);
        initMenu();
        this.listDragItems = new ArrayList();
        initDragItems();
        positionDragItems();
    }

    public void closed(boolean isHidden) {
        super.closed(isHidden);
        if (!isHidden) {
            this.listDragItems.clear();
            this.listMenuButtons.clear();
        }
    }

    public void displayed(boolean isBack) {
        super.displayed(isBack);
        if (!isBack) {
        }
    }

    private void initMenu() {
        for (int i = 0; i < this.selectedMenuItems.length; i++) {
            DisplayBlock block = this.displayBlocks.get(this.selectedMenuItems[i]);
            this.listMenuButtons.get(i).setText(block.getName());
            this.listMenuButtons.get(i).setCompoundDrawablesWithIntrinsicBounds((Drawable) null, VerveManager.getInstance().getIcon(block.getIconStyle(), block.getType()), (Drawable) null, (Drawable) null);
            this.listMenuButtons.get(i).setTag(String.valueOf(i));
            this.listMenuButtons.get(i).setClickable(false);
        }
    }

    private boolean isMenuItem(int position) {
        for (int i : this.selectedMenuItems) {
            if (i == position) {
                return true;
            }
        }
        return false;
    }

    private void initDragItems() {
        int size = this.displayBlocks.size();
        int count = 0;
        for (int i = 0; i < size; i++) {
            DisplayBlock block = this.displayBlocks.get(i);
            Drawable icon = VerveManager.getInstance().getIcon(block.getIconStyle(), block.getType());
            icon.clearColorFilter();
            if (!isMenuItem(i)) {
                Button item = (Button) ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.dragitem, (ViewGroup) null);
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(this.itemWidth, this.itemHeight, 48);
                layoutParams.setMargins(5, 5, 0, 0);
                item.setLayoutParams(layoutParams);
                item.setText(block.getName());
                item.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, icon, (Drawable) null, (Drawable) null);
                this.dragPanel.addView(item, count, layoutParams);
                item.setOnTouchListener(this.dragt);
                item.setTag(String.valueOf(i));
                this.listDragItems.add(item);
                count++;
            }
        }
    }

    private void positionDragItems() {
        int top = 60;
        int left = 5;
        int width = ScreenManager.getInstance().getContext().getWindowManager().getDefaultDisplay().getWidth();
        int numItems = (int) ((float) (width / this.itemWidth));
        int delta = ((width - (this.itemWidth * numItems)) - 5) / numItems;
        int count = 0;
        for (Button btn : this.listDragItems) {
            FrameLayout.LayoutParams par = (FrameLayout.LayoutParams) btn.getLayoutParams();
            par.setMargins(left, top, 0, 0);
            btn.setLayoutParams(par);
            count++;
            if (count == numItems) {
                left = 5;
                top += this.itemHeight;
                count = 0;
            } else {
                left += this.itemWidth + delta;
            }
        }
    }

    /* access modifiers changed from: private */
    public void createDragItem(int left, int top, int position) {
        Button item = (Button) ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.dragitem, (ViewGroup) null);
        item.getBackground().setAlpha(90);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(this.itemWidth, -2, 48);
        layoutParams.setMargins(left, top, 0, 0);
        DisplayBlock block = this.displayBlocks.get(position);
        item.setText(block.getName());
        item.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, VerveManager.getInstance().getIcon(block.getIconStyle(), block.getType()), (Drawable) null, (Drawable) null);
        this.dragPanel.addView(item, layoutParams);
        this.dragItem = item;
    }

    /* access modifiers changed from: private */
    public void removeDragItem() {
        if (this.dragItem != null) {
            this.dragPanel.removeView(this.dragItem);
            this.dragItem = null;
        }
    }

    /* access modifiers changed from: private */
    public void checkOver(int index) {
        int count = 0;
        for (Button btn : this.listMenuButtons) {
            if (count == index) {
                deselectMenuItems(-1);
                count++;
            } else {
                Rect rect = new Rect();
                btn.getHitRect(rect);
                rect.top += this.menu.getTop();
                rect.bottom += this.menu.getTop();
                rect.left += this.menuDeltaWidth;
                rect.right -= this.menuDeltaWidth;
                Rect dragRect = new Rect();
                this.dragItem.getHitRect(dragRect);
                if (rect.top > dragRect.bottom) {
                    deselectMenuItems(-1);
                    return;
                } else if (Rect.intersects(rect, dragRect)) {
                    Logger.logDebug("Intersect menu item : " + btn.getTag().toString());
                    btn.setSelected(true);
                    deselectMenuItems(count);
                    return;
                } else {
                    count++;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void deselectMenuItems(int position) {
        int count = 0;
        for (Button btn : this.listMenuButtons) {
            if (count != position) {
                btn.setSelected(false);
            }
            count++;
        }
    }

    /* access modifiers changed from: private */
    public void checkHit(int index) {
        int count = 0;
        for (Button btn : this.listMenuButtons) {
            if (count == index) {
                deselectMenuItems(-1);
                count++;
            } else {
                Rect rect = new Rect();
                btn.getHitRect(rect);
                rect.top += this.menu.getTop();
                rect.bottom += this.menu.getTop();
                rect.left += this.menuDeltaWidth;
                rect.right -= this.menuDeltaWidth;
                Rect dragRect = new Rect();
                this.dragItem.getHitRect(dragRect);
                if (rect.top > dragRect.bottom) {
                    deselectMenuItems(-1);
                    return;
                } else if (Rect.intersects(rect, dragRect)) {
                    btn.setSelected(true);
                    deselectMenuItems(count);
                    if (index == -1) {
                        replaceMenuItem(btn, count);
                        return;
                    } else {
                        switchMenuItem(index, count);
                        return;
                    }
                } else {
                    count++;
                }
            }
        }
    }

    private void replaceMenuItem(Button btn, int pos1) {
        Button original = (Button) this.dragItem.getTag();
        int displayId = Integer.parseInt(original.getTag().toString());
        int temp = this.selectedMenuItems[pos1];
        this.selectedMenuItems[pos1] = displayId;
        initMenu();
        DisplayBlock block = this.displayBlocks.get(temp);
        original.setText(block.getName());
        original.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, VerveManager.getInstance().getIcon(block.getIconStyle(), block.getType()), (Drawable) null, (Drawable) null);
        original.setTag(Integer.valueOf(temp));
        Logger.logDebug("CS : Item 1 " + String.valueOf(this.selectedMenuItems[0]) + " Item 2 " + String.valueOf(this.selectedMenuItems[1]));
        ScreenManager.getInstance().setCustomized(true);
        ScreenManager.getInstance().setCustomizedItems(this.selectedMenuItems);
    }

    private void switchMenuItem(int pos1, int pos2) {
        int temp = this.selectedMenuItems[pos1];
        this.selectedMenuItems[pos1] = this.selectedMenuItems[pos2];
        this.selectedMenuItems[pos2] = temp;
        initMenu();
        Logger.logDebug("CS : Item 1 " + String.valueOf(this.selectedMenuItems[0]) + " Item 2 " + String.valueOf(this.selectedMenuItems[1]));
        ScreenManager.getInstance().setCustomized(true);
        ScreenManager.getInstance().setCustomizedItems(this.selectedMenuItems);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        positionDragItems();
    }
}
