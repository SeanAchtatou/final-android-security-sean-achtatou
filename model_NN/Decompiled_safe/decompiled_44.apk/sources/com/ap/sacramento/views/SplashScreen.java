package com.ap.sacramento.views;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import com.ap.sacramento.R;
import com.ap.sacramento.common.BaseScreen;
import com.ap.sacramento.common.Logger;
import com.ap.sacramento.managers.ScreenManager;
import com.ap.sacramento.managers.VerveManager;
import com.vervewireless.capi.DisplayBlock;
import com.vervewireless.capi.HierarchyListener;
import com.vervewireless.capi.Locale;
import com.vervewireless.capi.RegistrationListener;
import com.vervewireless.capi.VerveError;
import java.util.List;

public class SplashScreen extends BaseScreen implements RegistrationListener, HierarchyListener {
    private ImageView imgSplash = ((ImageView) this.container.findViewById(R.id.imgSplash));

    public SplashScreen() {
        ScreenManager.getInstance().getContext().setRequestedOrientation(1);
        this.container = (ViewGroup) ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.splashscreen, (ViewGroup) null);
        this.container.setTag(this);
        Drawable splash = VerveManager.getInstance().getSplash();
        if (splash != null) {
            this.imgSplash.setBackgroundDrawable(splash);
        }
        VerveManager.getInstance().getVerveApi().registerWithVerve(this);
    }

    public void closed(boolean isHidden) {
        if (isHidden) {
        }
    }

    public void displayed(boolean isBack) {
    }

    public void register(Locale selectedLocale) {
        ScreenManager.getInstance().showTitleProgress();
        if (selectedLocale != null) {
            ScreenManager.getInstance().getContext().setRequestedOrientation(1);
            VerveManager.getInstance().getVerveApi().registerWithVerve(selectedLocale, this);
            return;
        }
        VerveManager.getInstance().getVerveApi().registerWithVerve(this);
    }

    public void onRegistrationFailed(VerveError error) {
        ScreenManager.getInstance().hideTitleProgress();
        AlertDialog.Builder builder = new AlertDialog.Builder(ScreenManager.getInstance().getContext());
        builder.setTitle("Registration");
        builder.setMessage(error.toString());
        if (error.getCause() == VerveError.Cause.NetworkConnectionError) {
            builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    SplashScreen.this.register(null);
                }
            });
        }
        builder.setOnCancelListener(new FinishActivity());
        builder.show();
    }

    public void onRegistrationFinished() {
        VerveManager.getInstance().getVerveApi().getContetHierarchy(this);
    }

    public void onRegistrationLocaleNeeded(List<Locale> locales) {
        if (locales.size() == 1) {
            VerveManager.getInstance().getVerveApi().registerWithVerve(locales.get(0), this);
            return;
        }
        showLocales(locales);
    }

    public void showLocales(final List<Locale> locales) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ScreenManager.getInstance().getContext());
        builder.setTitle("Select locale");
        builder.setCancelable(false);
        builder.setSingleChoiceItems(new ArrayAdapter(ScreenManager.getInstance().getContext(), 17367057, locales), -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                VerveManager.getInstance().getVerveApi().registerWithVerve((Locale) locales.get(which), SplashScreen.this);
                dialog.dismiss();
                ScreenManager.getInstance().setExternalActivity(false);
            }
        });
        ScreenManager.getInstance().setExternalActivity(true);
        builder.create().show();
    }

    public void onHierarchyFailed(VerveError error) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ScreenManager.getInstance().getContext());
        builder.setMessage(error.toString());
        builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                SplashScreen.this.register(null);
            }
        });
        builder.setOnCancelListener(new FinishActivity());
        builder.show();
    }

    public void onHierarchyRecieved(DisplayBlock result) {
        VerveManager.getInstance().clearStorageCache();
        VerveManager.getInstance().loadIcons();
        Logger.logDebug("getLocation()");
        VerveManager mng = VerveManager.getInstance();
        mng.setHierarchy(result);
        Location lastLocation = ScreenManager.getInstance().getLastLocation();
        if (lastLocation != null) {
            Logger.logDebug("Setting last known location " + String.valueOf(lastLocation.getLatitude()) + " " + String.valueOf(lastLocation.getLongitude()));
            mng.getVerveApi().setLocation(lastLocation);
        }
        if (ScreenManager.getInstance().getLocationMode() == 0) {
            mng.getLocation(false);
        } else if (ScreenManager.getInstance().getLastPostal().length() < 2) {
            mng.showPostalDialog(null, true);
        }
        HomeScreen home = new HomeScreen(result.getDisplayBlocks());
        ScreenManager.getInstance().hideTitleProgress();
        ScreenManager.getInstance().hideTitle();
        ScreenManager.getInstance().setExternalActivity(false);
        ScreenManager.getInstance().getContext().setRequestedOrientation(4);
        ScreenManager.getInstance().add(home.getView());
    }

    private class FinishActivity implements DialogInterface.OnCancelListener {
        private FinishActivity() {
        }

        public void onCancel(DialogInterface dialog) {
            ScreenManager.getInstance().getContext().finish();
        }
    }
}
