package com.ap.sacramento.views;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.ap.sacramento.R;
import com.ap.sacramento.common.AdWebView;
import com.ap.sacramento.common.BaseScreen;
import com.ap.sacramento.common.Logger;
import com.ap.sacramento.managers.AdManager;
import com.ap.sacramento.managers.ScreenManager;
import com.ap.sacramento.managers.VerveManager;
import com.vervewireless.capi.DisplayBlock;
import com.vervewireless.capi.PartnerModules;
import java.util.ArrayList;
import java.util.List;

public class HomeScreen extends BaseScreen {
    private AdManager adManager;
    Button btnMenu1;
    Button btnMenu2;
    Button btnMenu3;
    Button btnMenu4;
    Button btnMenu5;
    private List<DisplayBlock> displayBlocks;
    private RelativeLayout layoutAdvert;
    private List<Button> listMenuButtons;
    /* access modifiers changed from: private */
    public List<BaseScreen> listScreens;
    /* access modifiers changed from: private */
    public ViewFlipper mainPanel;
    private MenuOnClickListener menuClickListener;
    private List<DisplayBlock> menuDisplayBlocks;
    /* access modifiers changed from: private */
    public List<DisplayBlock> moreDisplayBlocks;
    private Button selectedButton = null;
    /* access modifiers changed from: private */
    public int selectedMenuItem = 0;
    private int[] selectedMenuItems = {0, 1, 2, 3};
    private int selectedMoreItem = -1;
    private AdWebView webAdvert;

    public HomeScreen(List<DisplayBlock> displayBlocks2) {
        this.displayBlocks = VerveManager.getInstance().clean(displayBlocks2);
        this.container = (ViewGroup) ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.homescreen, (ViewGroup) null);
        this.container.setTag(this);
        this.menuClickListener = new MenuOnClickListener();
        this.btnMenu1 = (Button) this.container.findViewById(R.id.btnMenu1);
        this.btnMenu1.setOnClickListener(this.menuClickListener);
        this.btnMenu2 = (Button) this.container.findViewById(R.id.btnMenu2);
        this.btnMenu2.setOnClickListener(this.menuClickListener);
        this.btnMenu3 = (Button) this.container.findViewById(R.id.btnMenu3);
        this.btnMenu3.setOnClickListener(this.menuClickListener);
        this.btnMenu4 = (Button) this.container.findViewById(R.id.btnMenu4);
        this.btnMenu4.setOnClickListener(this.menuClickListener);
        this.btnMenu5 = (Button) this.container.findViewById(R.id.btnMenu5);
        this.btnMenu5.setOnClickListener(this.menuClickListener);
        this.listMenuButtons = new ArrayList();
        this.listMenuButtons.add(this.btnMenu1);
        this.listMenuButtons.add(this.btnMenu2);
        this.listMenuButtons.add(this.btnMenu3);
        this.listMenuButtons.add(this.btnMenu4);
        this.listMenuButtons.add(this.btnMenu5);
        this.mainPanel = (ViewFlipper) this.container.findViewById(R.id.mainPanel);
        this.menuDisplayBlocks = new ArrayList();
        int[] loadedItems = ScreenManager.getInstance().loadMenuItems();
        if (loadedItems != null) {
            this.selectedMenuItems = loadedItems;
        } else {
            Logger.logDebug("No loaded menu items");
        }
        this.adManager = new AdManager();
        VerveManager.getInstance().setMainAdManager(this.adManager);
        initMainMenu();
        this.listScreens = new ArrayList();
        initScreens();
        this.moreDisplayBlocks = new ArrayList();
        initMoreScreens();
        this.layoutAdvert = (RelativeLayout) this.container.findViewById(R.id.adview);
        this.layoutAdvert.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Logger.logDebug("layoutAdvert onClick");
                VerveManager.getInstance().getMainAdManager().onClicked();
            }
        });
        this.webAdvert = (AdWebView) this.container.findViewById(R.id.webAdvert);
        VerveManager.getInstance().getMainAdManager().setAd(this.webAdvert);
        this.textLeft = (TextView) this.container.findViewById(R.id.textLeft);
        this.textRight = (TextView) this.container.findViewById(R.id.textRight);
        this.textRight.setVisibility(8);
        this.imgHeader = (ImageView) this.container.findViewById(R.id.imgHeader);
        this.titlePanel = this.container.findViewById(R.id.title);
        Drawable header = VerveManager.getInstance().getTitleHeader();
        if (header != null) {
            this.imgHeader.setImageDrawable(header);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public void closed(boolean isHidden) {
        if (isHidden) {
            this.listScreens.get(this.mainPanel.getDisplayedChild()).closed(isHidden);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public void displayed(boolean isBack) {
        if (!isBack) {
            display();
            initFirstScreen();
        } else if (ScreenManager.getInstance().isCustomized()) {
            ScreenManager.getInstance().showProgress("Customizing ...", "");
            this.mainPanel.postDelayed(new Runnable() {
                public void run() {
                    HomeScreen.this.customizeScreen();
                }
            }, 50);
        } else {
            this.listScreens.get(this.mainPanel.getDisplayedChild()).displayed(isBack);
        }
        if (ScreenManager.getInstance().getMainScreenIndex() > 0) {
            ScreenManager.getInstance().setMainScreenIndex(0);
        }
    }

    public void activate() {
        if (this.mainPanel.getDisplayedChild() >= 0) {
            this.listScreens.get(this.mainPanel.getDisplayedChild()).displayed(true);
        }
        super.activate();
    }

    public void deactivate() {
        if (this.listScreens.size() == 5) {
            this.listScreens.remove(4);
            this.mainPanel.removeViewAt(4);
            return;
        }
        if (this.mainPanel.getDisplayedChild() >= 0) {
            this.listScreens.get(this.mainPanel.getDisplayedChild()).closed(true);
        }
        super.deactivate();
    }

    public void display() {
    }

    /* Debug info: failed to restart local var, previous not found, register: 11 */
    private void initMainMenu() {
        int length = 4;
        this.menuDisplayBlocks.clear();
        if (this.displayBlocks.size() < 4) {
            length = this.displayBlocks.size();
        }
        for (int i = 0; i < length; i++) {
            DisplayBlock block = this.displayBlocks.get(this.selectedMenuItems[i]);
            this.listMenuButtons.get(i).setText(block.getName());
            this.listMenuButtons.get(i).setCompoundDrawablesWithIntrinsicBounds((Drawable) null, VerveManager.getInstance().getIcon(block.getIconStyle(), block.getType()), (Drawable) null, (Drawable) null);
            this.listMenuButtons.get(i).setTag(String.valueOf(i));
            this.menuDisplayBlocks.add(block);
        }
        if (this.displayBlocks.size() > 4) {
            this.listMenuButtons.get(4).setText("More");
            Drawable icon = VerveManager.getInstance().getIcon("more.png", "");
            this.listMenuButtons.get(4).setTag("4");
            this.listMenuButtons.get(4).setCompoundDrawablesWithIntrinsicBounds((Drawable) null, icon, (Drawable) null, (Drawable) null);
            return;
        }
        this.listMenuButtons.get(4).setVisibility(8);
    }

    private void initScreens() {
        int length = 4;
        this.mainPanel.removeAllViews();
        this.listScreens.clear();
        if (this.displayBlocks.size() < 4) {
            length = this.displayBlocks.size();
        }
        for (int i = 0; i < length; i++) {
            BaseScreen screen = getBaseScreen(this.displayBlocks.get(this.selectedMenuItems[i]));
            if (screen != null) {
                this.mainPanel.addView(screen.getView(), i);
                this.listScreens.add(screen);
            } else {
                BaseScreen screen2 = new BaseScreen();
                this.mainPanel.addView(screen2.getEmptyView(), i);
                this.listScreens.add(screen2);
            }
        }
        this.mainPanel.setDisplayedChild(0);
    }

    private void initMoreScreens() {
        this.moreDisplayBlocks.clear();
        for (DisplayBlock block : this.displayBlocks) {
            if (!this.menuDisplayBlocks.contains(block)) {
                this.moreDisplayBlocks.add(block);
            }
        }
    }

    private void initFirstScreen() {
        menuSelect();
        handleDisplayBlock(this.selectedMenuItem);
    }

    /* access modifiers changed from: private */
    public void menuSelect() {
        if (this.selectedMenuItem >= 0) {
            this.selectedButton = this.listMenuButtons.get(this.selectedMenuItem);
            this.selectedButton.setSelected(true);
        }
    }

    /* access modifiers changed from: private */
    public void menuDeselect() {
        if (this.selectedButton != null) {
            this.selectedButton.setSelected(false);
        }
    }

    /* access modifiers changed from: private */
    public BaseScreen getBaseScreen(DisplayBlock block) {
        String type = block.getType();
        String cType = block.getContentType();
        if ("Editorial".equals(cType) || type.contains("contentModule")) {
            return new NewsScreen(block);
        }
        if ("multiSourceModule".equals(type)) {
            return new ItemsScreen(block);
        }
        if (type.contains("Grouping")) {
            return new ItemsScreen(block);
        }
        if (type.contains("savedNews")) {
            return new NewsScreen(block);
        }
        if (type.contains("NewsModule")) {
            return new NewsScreen(block);
        }
        if (type.contains("photo")) {
            return new GalleryThumbsScreen(block, ScreenManager.getInstance().getTitle());
        }
        if (type.contains("video")) {
            return new VideoScreen(block);
        }
        if (type.contains("staticLink")) {
            String url = block.getXmlUrl();
            if (!url.contains("wlappurl")) {
                return new BrowserScreen(url, block);
            }
            Logger.logDebug("wlappurl " + url);
            if (!url.contains("usernews")) {
                return null;
            }
            String url2 = url.substring(url.indexOf(":") + 1);
            SendStoryScreen story = new SendStoryScreen(false);
            story.setSendTo(url2);
            return story;
        }
        Log.w("verveapi", "Unsupported type:" + type + ", cType:" + cType);
        return null;
    }

    /* access modifiers changed from: private */
    public void handleDisplayBlock(int position) {
        DisplayBlock block = this.displayBlocks.get(this.selectedMenuItems[position]);
        this.textLeft.setText(block.getName());
        ScreenManager.getInstance().setTitle(block.getName());
        deactivate();
        this.mainPanel.setDisplayedChild(position);
        BaseScreen screen = this.listScreens.get(this.selectedMenuItem);
        if (screen.getState() == 0) {
            screen.displayed(false);
            return;
        }
        screen.reportPageView();
        activate();
    }

    public void reportPageView() {
    }

    public void showMoreDialog() {
        deactivate();
        menuDeselect();
        final int oldSelectedMenuItem = this.selectedMenuItem;
        this.selectedMenuItem = 4;
        menuSelect();
        AlertDialog.Builder builder = new AlertDialog.Builder(ScreenManager.getInstance().getContext());
        builder.setTitle("More");
        builder.setCancelable(false);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                HomeScreen.this.menuDeselect();
                int unused = HomeScreen.this.selectedMenuItem = oldSelectedMenuItem;
                HomeScreen.this.menuSelect();
            }
        });
        builder.setSingleChoiceItems(new ArrayAdapter<DisplayBlock>(ScreenManager.getInstance().getContext(), R.layout.share_row, this.moreDisplayBlocks) {
            /* Debug info: failed to restart local var, previous not found, register: 6 */
            public View getView(int position, View convertView, ViewGroup parent) {
                RowHolder holder;
                if (convertView == null) {
                    convertView = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.share_row, (ViewGroup) null);
                    holder = new RowHolder();
                    holder.textShare = (TextView) convertView.findViewById(R.id.textShare);
                    holder.imgShare = (ImageView) convertView.findViewById(R.id.imgShare);
                    convertView.setTag(holder);
                } else {
                    holder = (RowHolder) convertView.getTag();
                }
                holder.textShare.setText(((DisplayBlock) HomeScreen.this.moreDisplayBlocks.get(position)).getName());
                Drawable icon = VerveManager.getInstance().getIcon(((DisplayBlock) HomeScreen.this.moreDisplayBlocks.get(position)).getIconStyle(), ((DisplayBlock) HomeScreen.this.moreDisplayBlocks.get(position)).getType());
                if (icon != null) {
                    holder.imgShare.setColorFilter(R.color.solid_black, PorterDuff.Mode.SRC_ATOP);
                    holder.imgShare.setImageDrawable(icon);
                }
                return convertView;
            }

            /* renamed from: com.ap.sacramento.views.HomeScreen$4$RowHolder */
            class RowHolder {
                public ImageView imgShare;
                public TextView textShare;

                RowHolder() {
                }
            }
        }, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (HomeScreen.this.listScreens.size() == 5) {
                    HomeScreen.this.listScreens.remove(4);
                    HomeScreen.this.mainPanel.removeViewAt(4);
                }
                HomeScreen.this.textLeft.setText(((DisplayBlock) HomeScreen.this.moreDisplayBlocks.get(which)).getName());
                ScreenManager.getInstance().setTitle(((DisplayBlock) HomeScreen.this.moreDisplayBlocks.get(which)).getName());
                BaseScreen screen = HomeScreen.this.getBaseScreen((DisplayBlock) HomeScreen.this.moreDisplayBlocks.get(which));
                if (screen != null) {
                    HomeScreen.this.mainPanel.addView(screen.getView(), 4);
                    HomeScreen.this.listScreens.add(screen);
                    HomeScreen.this.mainPanel.setDisplayedChild(4);
                    screen.displayed(false);
                    return;
                }
                HomeScreen.this.activate();
            }
        });
        builder.create().show();
        VerveManager.getInstance().reportCustomPageview(null, null, Integer.valueOf(PartnerModules.TOP_LEVEL));
    }

    public class MenuOnClickListener implements View.OnClickListener {
        public MenuOnClickListener() {
        }

        public void onClick(View v) {
            if (v instanceof Button) {
                final int position = Integer.valueOf(v.getTag().toString()).intValue();
                if (position == 4) {
                    HomeScreen.this.showMoreDialog();
                } else if (HomeScreen.this.selectedMenuItem != position) {
                    Logger.logDebug("Menu " + v.getTag().toString());
                    HomeScreen.this.menuDeselect();
                    int unused = HomeScreen.this.selectedMenuItem = position;
                    HomeScreen.this.menuSelect();
                    HomeScreen.this.mainPanel.postDelayed(new Runnable() {
                        public void run() {
                            HomeScreen.this.handleDisplayBlock(position);
                        }
                    }, 10);
                }
            }
        }
    }

    public Menu getOptions(Menu menu) {
        Menu menu2 = this.listScreens.get(this.mainPanel.getDisplayedChild()).getOptions(menu);
        if (ScreenManager.getInstance().getMainScreenIndex() == 0) {
            menu2.add("Settings").setIcon((int) R.drawable.ic_menu_preferences);
            if (this.displayBlocks.size() > 4) {
                menu2.add("Customize").setIcon((int) R.drawable.ic_menu_edit);
            }
        }
        return menu2;
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public void onOptionsMenuItem(String title) {
        if (title.contains("Settings")) {
            ScreenManager.getInstance().add(new SettingsScreen().getView());
        } else if (title.contains("Customize")) {
            ScreenManager.getInstance().add(new CustomizeScreen((int[]) this.selectedMenuItems.clone(), this.displayBlocks).getView());
        } else {
            this.listScreens.get(this.mainPanel.getDisplayedChild()).onOptionsMenuItem(title);
        }
    }

    /* access modifiers changed from: private */
    public void customizeScreen() {
        this.selectedMenuItems = ScreenManager.getInstance().getCustomizedItems();
        BaseScreen moreScreen = null;
        if (this.selectedMenuItem == 4) {
            moreScreen = this.listScreens.get(this.selectedMenuItem);
        }
        initMainMenu();
        initScreens();
        initMoreScreens();
        if (moreScreen != null) {
            this.listScreens.add(moreScreen);
            this.mainPanel.addView(moreScreen.getView(), this.selectedMenuItem);
        }
        ScreenManager.getInstance().closeProgress();
        this.mainPanel.setDisplayedChild(this.selectedMenuItem);
        if (!this.listScreens.get(this.selectedMenuItem).isDisplayed) {
            this.listScreens.get(this.mainPanel.getDisplayedChild()).displayed(false);
        } else {
            this.listScreens.get(this.mainPanel.getDisplayedChild()).displayed(true);
        }
        ScreenManager.getInstance().setCustomized(false);
        ScreenManager.getInstance().saveMenuItems(this.selectedMenuItems);
    }

    public void onShareItem(int shareId) {
        this.listScreens.get(this.mainPanel.getDisplayedChild()).onShareItem(shareId);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        this.listScreens.get(this.mainPanel.getDisplayedChild()).onActivityResult(requestCode, resultCode, data);
    }

    public void onTouch(MotionEvent event) {
        this.listScreens.get(this.mainPanel.getDisplayedChild()).onTouch(event);
    }

    public void onSearchKey() {
        this.listScreens.get(this.mainPanel.getDisplayedChild()).onSearchKey();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        this.listScreens.get(this.mainPanel.getDisplayedChild()).onConfigurationChanged(newConfig);
    }
}
