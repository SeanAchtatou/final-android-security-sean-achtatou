package com.ap.sacramento.views;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.ap.sacramento.R;
import com.ap.sacramento.common.BaseScreen;
import com.ap.sacramento.common.ImageDownloader;
import com.ap.sacramento.common.Logger;
import com.ap.sacramento.managers.ScreenManager;
import com.ap.sacramento.managers.VerveManager;
import com.vervewireless.capi.ContentItem;
import com.vervewireless.capi.ContentListener;
import com.vervewireless.capi.ContentRequest;
import com.vervewireless.capi.ContentResponse;
import com.vervewireless.capi.DisplayBlock;
import com.vervewireless.capi.PartnerModules;
import com.vervewireless.capi.SearchListener;
import com.vervewireless.capi.SearchRequest;
import com.vervewireless.capi.SearchResponse;
import com.vervewireless.capi.VerveError;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class NewsScreen extends BaseScreen implements ContentListener {
    final float SCALE = ScreenManager.getInstance().getContext().getResources().getDisplayMetrics().density;
    /* access modifiers changed from: private */
    public List<ContentItem> contentItems;
    /* access modifiers changed from: private */
    public DisplayBlock displayBlock;
    /* access modifiers changed from: private */
    public ImageDownloader imageDownloader;
    private Date lastUpdate = new Date();
    private NewsAdapter newsAdapter;
    /* access modifiers changed from: private */
    public ListView newsList;
    String query = null;
    String strValuePixels;
    int valuePixels;

    public NewsScreen(DisplayBlock displayBlock2) {
        this.displayBlock = displayBlock2;
        this.container = (ViewGroup) ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.newsscreen, (ViewGroup) null);
        this.container.setTag(this);
        this.contentItems = new LinkedList();
        this.newsList = (ListView) this.container.findViewById(R.id.listNews);
        this.newsList.setSelector(17301602);
        this.newsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                NewsScreen.this.onNewsItem(arg2);
            }
        });
        this.imageDownloader = new ImageDownloader();
        this.imageDownloader.setMode(ImageDownloader.Mode.CORRECT);
        this.valuePixels = (int) ((60.0f * this.SCALE) + 0.5f);
        this.strValuePixels = String.valueOf(this.valuePixels);
        this.titlePanel = this.container.findViewById(R.id.title);
        this.titlePanel.setVisibility(8);
    }

    public void closed(boolean isHidden) {
        super.closed(isHidden);
        if (!isHidden) {
            this.imageDownloader.clearCache();
        }
    }

    public void displayed(boolean isBack) {
        super.displayed(isBack);
        String type = this.displayBlock.getType();
        if (type.contains("savedNews")) {
            ScreenManager.getInstance().setSavedStory(true);
        } else {
            ScreenManager.getInstance().setSavedStory(false);
        }
        if (!isBack) {
            ScreenManager.getInstance().showProgress("Loading content ...", "");
            if (type.contains("savedNews")) {
                this.newsList.setLongClickable(true);
                this.newsList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    public boolean onItemLongClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                        NewsScreen.this.onDeleteItem(arg2);
                        return true;
                    }
                });
            }
            this.state = 1;
            this.newsAdapter = new NewsAdapter();
            VerveManager.getInstance().getVerveApi().getContent(new ContentRequest(this.displayBlock), this);
        } else if (ScreenManager.getInstance().isSavedStoryChange()) {
            ScreenManager.getInstance().setSavedStoryChange(false);
            this.newsAdapter.notifyDataSetChanged();
            if (this.contentItems.size() == 0) {
                showEmptyView();
                this.newsList.setEmptyView(this.empty);
            }
        } else if (type.contains("savedNews")) {
            refresh();
        }
        VerveManager.getInstance().getMainAdManager().setDisplayBlock(this.displayBlock);
        VerveManager.getInstance().getMainAdManager().loadAd();
    }

    public void onContentFailed(VerveError e) {
        ScreenManager.getInstance().closeProgress();
        ScreenManager.getInstance().showDialog("Failed", e.toString());
        this.newsList.setAdapter((ListAdapter) this.newsAdapter);
        showEmptyView();
        this.newsList.setEmptyView(this.empty);
        display();
    }

    public void onContentRecieved(ContentResponse response) {
        setItems(response.getItems(), false);
        this.lastUpdate = response.getLastUpdate() == null ? new Date() : response.getLastUpdate();
        if (this.lastUpdate != null) {
            Logger.logDebug("Last update: " + this.lastUpdate.toString());
        }
        reportPageView(response.getDisplayBlock(), response.getItems());
    }

    /* access modifiers changed from: package-private */
    public void setItems(List<ContentItem> items, boolean append) {
        Logger.logDebug("Items received: " + String.valueOf(items.size()));
        ScreenManager.getInstance().closeProgress();
        this.newsList.setAdapter((ListAdapter) this.newsAdapter);
        if (!append) {
            this.contentItems = items;
        } else if (items.size() == 0) {
            this.contentItems.addAll(this.contentItems.size(), items);
        } else {
            for (ContentItem item : items) {
                if (!VerveManager.getInstance().containsContentItem(this.contentItems, item.getGuid())) {
                    this.contentItems.add(item);
                }
            }
        }
        if (this.contentItems.size() == 0) {
            showEmptyView();
            this.newsList.setEmptyView(this.empty);
        }
        display();
    }

    public void display() {
        this.newsAdapter.notifyDataSetChanged();
    }

    public void refresh() {
        removeEmptyView();
        ScreenManager.getInstance().showProgress("Refresing content ...", "");
        VerveManager.getInstance().getVerveApi().getContent(new ContentRequest(this.displayBlock), this);
    }

    /* access modifiers changed from: private */
    public void onNewsItem(int position) {
        if (this.contentItems.get(position).getExternalId() == "Search") {
            onOnlineSearch();
            return;
        }
        ScreenManager.getInstance().add(new NewsDetailsScreen(this.displayBlock, this.contentItems, position).getView());
    }

    /* access modifiers changed from: private */
    public void onDeleteItem(final int position) {
        Logger.logDebug("Delete saved item: " + String.valueOf(position));
        AlertDialog.Builder builder = new AlertDialog.Builder(ScreenManager.getInstance().getContext());
        builder.setTitle("Saved stories");
        builder.setMessage("Are you sure you want to remove this article ?");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                VerveManager.getInstance().getVerveApi().unsave((ContentItem) NewsScreen.this.contentItems.get(position));
                NewsScreen.this.contentItems.remove(position);
                NewsScreen.this.display();
                if (NewsScreen.this.contentItems.size() == 0) {
                    NewsScreen.this.showEmptyView();
                    NewsScreen.this.newsList.setEmptyView(NewsScreen.this.empty);
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        builder.show();
    }

    class NewsAdapter extends BaseAdapter {
        NewsAdapter() {
        }

        public int getCount() {
            return NewsScreen.this.contentItems.size();
        }

        public Object getItem(int position) {
            return NewsScreen.this.contentItems.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            NewsViewHolder holder;
            ContentItem item = (ContentItem) NewsScreen.this.contentItems.get(position);
            if (convertView == null) {
                convertView = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.news_row, (ViewGroup) null);
                holder = new NewsViewHolder();
                holder.textNews = (TextView) convertView.findViewById(R.id.textNews);
                holder.textNewsDate = (TextView) convertView.findViewById(R.id.textNewsDate);
                holder.imgIcon = (ImageView) convertView.findViewById(R.id.imgIcon);
                holder.imgIcon.setBackgroundDrawable(VerveManager.getInstance().getThumb());
                convertView.setTag(holder);
            } else {
                holder = (NewsViewHolder) convertView.getTag();
            }
            if (item.getExternalId() == "Search") {
                holder.textNews.setText("Search");
                holder.textNewsDate.setText("Continue searching online ...");
                if (NewsScreen.this.isDisplayed) {
                    holder.imgIcon.setImageResource(R.drawable.ic_menu_search);
                    holder.imgIcon.setVisibility(8);
                }
            } else {
                holder.textNews.setText(item.getTitle());
                holder.textNewsDate.setText(item.getPubDate().toLocaleString());
                if (NewsScreen.this.isDisplayed) {
                    String image = VerveManager.getInstance().getMediaItem(item, true);
                    if (holder.imgIcon.getVisibility() != 0) {
                        holder.imgIcon.setVisibility(0);
                    }
                    if (image.length() != 0) {
                        holder.imgIcon.setVisibility(0);
                        Logger.logDebug("NewsScreen Download image " + image + " Name " + NewsScreen.this.displayBlock.getName() + " Is displayed " + String.valueOf(NewsScreen.this.isDisplayed));
                        NewsScreen.this.imageDownloader.download(image, holder.imgIcon);
                    } else if (VerveManager.getInstance().showEmptyThumbs()) {
                        holder.imgIcon.setImageDrawable(null);
                    } else {
                        holder.imgIcon.setVisibility(8);
                    }
                }
            }
            return convertView;
        }
    }

    class NewsViewHolder {
        public ImageView imgIcon;
        public TextView textNews;
        public TextView textNewsDate;

        NewsViewHolder() {
        }
    }

    public Menu getOptions(Menu menu) {
        if (new Date().getTime() - this.lastUpdate.getTime() >= 60000) {
            MenuItem item = menu.add("Refresh");
            item.setIcon((int) R.drawable.ic_menu_refresh);
            item.setEnabled(true);
        } else {
            MenuItem item2 = menu.add("Up-to-Date");
            item2.setIcon((int) R.drawable.ic_menu_refresh);
            item2.setEnabled(false);
        }
        menu.add("Search").setIcon((int) R.drawable.ic_menu_search);
        return menu;
    }

    public void onOptionsMenuItem(String title) {
        if (title.contains("Refresh")) {
            refresh();
        } else if (title.contains("Search")) {
            onSearch();
        }
    }

    public void reportPageView() {
        super.reportPageView(this.displayBlock, this.contentItems);
    }

    public void onOnlineSearch() {
        if (this.query.length() > 0) {
            this.contentItems.remove(this.contentItems.size() - 1);
            ScreenManager.getInstance().showProgress("Online searching ...", "");
            SearchRequest request = new SearchRequest(this.displayBlock, this.query);
            request.setSearchOffline(false);
            VerveManager.getInstance().getVerveApi().search(request, new SearchListener() {
                public void onSearchFinished(SearchResponse response) {
                    NewsScreen.this.setItems(response.getItems(), true);
                    NewsScreen.this.query = null;
                    VerveManager.getInstance().reportCustomPageview(null, Integer.valueOf(NewsScreen.this.displayBlock.getId()), Integer.valueOf(PartnerModules.SEARCH));
                }

                public void onSearchFailed(VerveError e) {
                    NewsScreen.this.onContentFailed(e);
                    NewsScreen.this.query = null;
                }
            });
        }
    }

    public void onSearch() {
        if (this.query != null) {
            this.contentItems.remove(this.contentItems.size() - 1);
            this.query = null;
        }
        View searchView = (ViewGroup) ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.search, (ViewGroup) null);
        final EditText searchText = (EditText) searchView.findViewById(R.id.editSearch);
        AlertDialog.Builder builder = new AlertDialog.Builder(ScreenManager.getInstance().getContext());
        builder.setIcon((int) R.drawable.ic_menu_search);
        builder.setTitle(String.format("Search %s", this.displayBlock.getName()));
        builder.setView(searchView);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                NewsScreen.this.query = searchText.getText().toString();
                if (NewsScreen.this.query.length() > 0) {
                    ScreenManager.getInstance().showProgress("Local searching ...", "");
                    SearchRequest request = new SearchRequest(NewsScreen.this.displayBlock, NewsScreen.this.query);
                    request.setSearchOffline(true);
                    VerveManager.getInstance().getVerveApi().search(request, new SearchListener() {
                        public void onSearchFinished(SearchResponse response) {
                            List<ContentItem> items = response.getItems();
                            ContentItem searchItem = new ContentItem();
                            searchItem.setExternalId("Search");
                            items.add(searchItem);
                            NewsScreen.this.setItems(response.getItems(), false);
                            VerveManager.getInstance().reportCustomPageview(null, Integer.valueOf(NewsScreen.this.displayBlock.getId()), Integer.valueOf(PartnerModules.SEARCH));
                        }

                        public void onSearchFailed(VerveError e) {
                            NewsScreen.this.onContentFailed(e);
                        }
                    });
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        builder.show();
    }

    public void onSearchKey() {
        if (!this.displayBlock.getType().contains("savedNews")) {
            Logger.logDebug("News Screen KeyEvent.KEYCODE_SEARCH");
            onSearch();
        }
    }
}
