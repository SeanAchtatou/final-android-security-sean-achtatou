package com.ap.sacramento.views;

import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.ap.sacramento.R;
import com.ap.sacramento.common.BaseScreen;
import com.ap.sacramento.common.ImageDownloader;
import com.ap.sacramento.common.Logger;
import com.ap.sacramento.managers.ScreenManager;
import com.ap.sacramento.managers.VerveManager;
import com.vervewireless.capi.ContentItem;
import com.vervewireless.capi.ContentListener;
import com.vervewireless.capi.ContentRequest;
import com.vervewireless.capi.ContentResponse;
import com.vervewireless.capi.DisplayBlock;
import com.vervewireless.capi.MediaItem;
import com.vervewireless.capi.VerveError;
import java.util.ArrayList;
import java.util.List;

public class GalleryThumbsScreen extends BaseScreen implements ContentListener {
    final float SCALE = ScreenManager.getInstance().getContext().getResources().getDisplayMetrics().density;
    List<ContentItem> contentItems = null;
    private DisplayBlock displayBlock = null;
    GridView gridThumbs;
    /* access modifiers changed from: private */
    public ImageDownloader imageDownloader;
    List<MediaItem> mediaItems = null;
    int mode = 0;
    int position = 0;
    String strValuePixels;
    String[] tempList = new String[0];
    private GalleryThumbsAdapter thumbsAdapter;
    String title;
    int valuePixels;

    public GalleryThumbsScreen(List<MediaItem> mediaItems2, String title2) {
        this.mediaItems = mediaItems2;
        init(title2);
    }

    public GalleryThumbsScreen(DisplayBlock displayBlock2, String title2) {
        this.displayBlock = displayBlock2;
        this.mode = 1;
        init(title2);
    }

    public void init(final String title2) {
        this.container = (ViewGroup) ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.gallerythumbsscreen, (ViewGroup) null);
        this.container.setTag(this);
        this.title = title2;
        this.titlePanel = this.container.findViewById(R.id.title);
        if (this.mode == 0) {
            this.textLeft = (TextView) this.container.findViewById(R.id.textLeft);
            this.textLeft.setText(title2);
            this.textRight = (TextView) this.container.findViewById(R.id.textRight);
            this.textRight.setVisibility(8);
            this.imgHeader = (ImageView) this.container.findViewById(R.id.imgHeader);
            Drawable header = VerveManager.getInstance().getTitleHeader();
            if (header != null) {
                this.imgHeader.setImageDrawable(header);
            }
        } else {
            this.titlePanel.setVisibility(8);
        }
        if (this.mode == 1) {
            VerveManager.getInstance().getMainAdManager().setDisplayBlock(this.displayBlock);
            VerveManager.getInstance().getMainAdManager().loadAd();
        }
        this.gridThumbs = (GridView) this.container.findViewById(R.id.gridThumbs);
        this.gridThumbs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                if (GalleryThumbsScreen.this.mode == 0) {
                    ScreenManager.getInstance().add(new GalleryScreen(GalleryThumbsScreen.this.mediaItems, position, title2).getView());
                    return;
                }
                ScreenManager.getInstance().add(new GalleryScreen(GalleryThumbsScreen.this.mediaItems, GalleryThumbsScreen.this.contentItems, position, title2).getView());
            }
        });
        this.imageDownloader = new ImageDownloader();
        this.imageDownloader.setMode(ImageDownloader.Mode.CORRECT);
        this.valuePixels = (int) ((60.0f * this.SCALE) + 0.5f);
        this.strValuePixels = String.valueOf(this.valuePixels);
    }

    public void closed(boolean isHidden) {
        super.closed(isHidden);
        if (!isHidden) {
            this.imageDownloader.clearCache();
            this.gridThumbs = null;
            this.imageDownloader = null;
            System.gc();
        }
    }

    public void reportPageView() {
    }

    public void displayed(boolean isBack) {
        super.displayed(isBack);
        if (!isBack) {
            this.state = 1;
            if (this.mode == 0) {
                this.thumbsAdapter = new GalleryThumbsAdapter();
                this.gridThumbs.setAdapter((ListAdapter) this.thumbsAdapter);
                this.thumbsAdapter.notifyDataSetChanged();
                display();
                return;
            }
            ScreenManager.getInstance().showProgress("Loading photos ...", "");
            VerveManager.getInstance().getVerveApi().getContent(new ContentRequest(this.displayBlock), this);
        }
    }

    public void display() {
        this.thumbsAdapter.notifyDataSetChanged();
        ScreenManager.getInstance().closeProgress();
    }

    class GalleryThumbsAdapter extends BaseAdapter {
        GalleryThumbsAdapter() {
        }

        public int getCount() {
            return GalleryThumbsScreen.this.mediaItems.size();
        }

        public Object getItem(int position) {
            return GalleryThumbsScreen.this.mediaItems.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView;
            MediaItem item = GalleryThumbsScreen.this.mediaItems.get(position);
            if (convertView == null) {
                imageView = new ImageView(ScreenManager.getInstance().getContext());
                imageView.setLayoutParams(new AbsListView.LayoutParams(GalleryThumbsScreen.this.valuePixels, GalleryThumbsScreen.this.valuePixels));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setTag(item);
                imageView.setBackgroundDrawable(VerveManager.getInstance().getThumb());
            } else {
                imageView = (ImageView) convertView;
            }
            if (GalleryThumbsScreen.this.isDisplayed) {
                Logger.logDebug("GalleryThumbScreen Download image " + item.getUrl());
                GalleryThumbsScreen.this.imageDownloader.download(item.getUrl() + "&width=" + GalleryThumbsScreen.this.strValuePixels + "&height=" + GalleryThumbsScreen.this.strValuePixels + "&crop=true", imageView);
            }
            return imageView;
        }
    }

    public Menu getOptions(Menu menu) {
        return menu;
    }

    public void onContentFailed(VerveError e) {
        ScreenManager.getInstance().closeProgress();
        ScreenManager.getInstance().showDialog("Failed", e.toString());
    }

    public void onContentRecieved(ContentResponse response) {
        ScreenManager.getInstance().closeProgress();
        this.contentItems = response.getItems();
        if (this.contentItems.size() == 0) {
            this.container.findViewById(R.id.textEmpty).setVisibility(0);
        }
        this.mediaItems = new ArrayList();
        for (ContentItem content : this.contentItems) {
            if (content.getMediaItems().size() > 0) {
                this.mediaItems.add(content.getMediaItems().get(0));
            }
        }
        this.thumbsAdapter = new GalleryThumbsAdapter();
        try {
            this.gridThumbs.setAdapter((ListAdapter) this.thumbsAdapter);
        } catch (Exception e) {
        }
        display();
    }
}
