package com.ap.sacramento.views;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.ap.sacramento.R;
import com.ap.sacramento.common.BaseScreen;
import com.ap.sacramento.common.Logger;
import com.ap.sacramento.managers.ScreenManager;
import com.ap.sacramento.managers.VerveManager;
import com.vervewireless.capi.DisplayBlock;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MenuScreen extends BaseScreen {
    /* access modifiers changed from: private */
    public boolean[] customizeStates = null;
    private String[] customizeTitles = null;
    /* access modifiers changed from: private */
    public List<DisplayBlock> displayBlocks;
    private DisplayBlock hierarchyBlock = null;
    private boolean isSavedNewsModule = false;
    /* access modifiers changed from: private */
    public MenuAdapter menuAdapter = null;
    private ListView menuList;
    private BaseScreen nextScreen = null;
    private List<DisplayBlock> originalDisplayBlocks;

    public MenuScreen(List<DisplayBlock> displayBlocks2) {
        this.originalDisplayBlocks = clean(displayBlocks2);
        this.displayBlocks = new ArrayList();
        prepareCustomizeData();
        this.container = (ViewGroup) ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.menuscreen, (ViewGroup) null);
        this.container.setTag(this);
        this.menuList = (ListView) this.container.findViewById(R.id.listMenu);
        this.menuAdapter = new MenuAdapter();
        this.menuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                MenuScreen.this.onMenuItem((DisplayBlock) MenuScreen.this.displayBlocks.get(arg2));
            }
        });
        setAdapter();
    }

    private void setAdapter() {
        this.menuList.setAdapter((ListAdapter) this.menuAdapter);
        if (ScreenManager.getInstance().getMainScreenIndex() == 0) {
            prepareAdapter();
        } else {
            this.displayBlocks = this.originalDisplayBlocks;
        }
        this.menuAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void prepareAdapter() {
        this.displayBlocks.clear();
        for (int i = 0; i < this.customizeStates.length; i++) {
            if (this.customizeStates[i]) {
                this.displayBlocks.add(this.originalDisplayBlocks.get(i));
            }
        }
    }

    private void prepareCustomizeData() {
        if (ScreenManager.getInstance().getMainScreenIndex() == 0) {
            this.customizeTitles = new String[this.originalDisplayBlocks.size()];
            this.customizeStates = ScreenManager.getInstance().getCustomizeStates();
            if (this.customizeStates == null) {
                this.customizeStates = new boolean[this.originalDisplayBlocks.size()];
                for (int i = 0; i < this.customizeStates.length; i++) {
                    this.customizeStates[i] = true;
                    this.customizeTitles[i] = this.originalDisplayBlocks.get(i).getName();
                }
                return;
            }
            for (int i2 = 0; i2 < this.customizeStates.length; i2++) {
                this.customizeTitles[i2] = this.originalDisplayBlocks.get(i2).getName();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void setHierarchyBlock(DisplayBlock block) {
        this.hierarchyBlock = block;
    }

    private List<DisplayBlock> clean(List<DisplayBlock> src) {
        if (src.isEmpty()) {
            return Collections.emptyList();
        }
        List<DisplayBlock> dest = new ArrayList<>(src.size());
        for (DisplayBlock block : src) {
            String type = block.getType();
            Logger.logDebug("Type: " + type);
            if ("contentModule".equals(block.getType()) || type.contains("NewsModule") || type.contains("photo") || type.contains("video") || type.contains("staticLink")) {
                dest.add(block);
            } else if (type.contains("savedNews")) {
                this.isSavedNewsModule = true;
            } else if (!clean(block.getDisplayBlocks()).isEmpty()) {
                dest.add(block);
            }
        }
        return dest;
    }

    public void closed(boolean isHidden) {
        super.closed(isHidden);
    }

    public void displayed(boolean isBack) {
        super.displayed(isBack);
        if (isBack) {
            if (ScreenManager.getInstance().getMainScreenIndex() > 0) {
                ScreenManager.getInstance().setMainScreenIndex(ScreenManager.getInstance().getMainScreenIndex() - 1);
            }
            this.nextScreen = null;
            System.gc();
        }
    }

    /* access modifiers changed from: private */
    public void onMenuItem(DisplayBlock block) {
        String type = block.getType();
        if (ScreenManager.getInstance().getMainScreenIndex() == 0) {
            Logger.logDebug("Changing title" + block.getName());
            ScreenManager.getInstance().setTitle(block.getName());
        }
        ScreenManager.getInstance().setMainScreenIndex(ScreenManager.getInstance().getMainScreenIndex() + 1);
        if (type.contains("Grouping")) {
            Logger.logDebug("Display Block " + block.getName());
            ScreenManager.getInstance().add(new MenuScreen(block.getDisplayBlocks()).getView());
        } else if (type.contains("contentModule")) {
            this.nextScreen = new NewsScreen(block);
            ScreenManager.getInstance().add(this.nextScreen.getView());
        } else if (type.contains("savedNews")) {
            this.nextScreen = new NewsScreen(block);
            ScreenManager.getInstance().add(this.nextScreen.getView());
        } else if (type.contains("NewsModule")) {
            this.nextScreen = new NewsScreen(block);
            ScreenManager.getInstance().add(this.nextScreen.getView());
        } else if (type.contains("photo")) {
            this.nextScreen = new GalleryThumbsScreen(block, ScreenManager.getInstance().getTitle());
            ScreenManager.getInstance().add(this.nextScreen.getView());
        } else if (type.contains("video")) {
            this.nextScreen = new VideoScreen(block);
            ScreenManager.getInstance().add(this.nextScreen.getView());
        } else if (type.contains("staticLink")) {
            String url = block.getXmlUrl();
            if (!url.contains("wlappurl")) {
                ScreenManager.getInstance().add(new BrowserScreen(url, block).getView());
                return;
            }
            ScreenManager.getInstance().showDialog("Content module", "In app navigation not supported at this time");
        } else {
            ScreenManager.getInstance().showDialog("Content module", type + " not supported at this point");
        }
    }

    class MenuAdapter extends BaseAdapter {
        MenuAdapter() {
        }

        public int getCount() {
            return MenuScreen.this.displayBlocks.size();
        }

        public Object getItem(int position) {
            return MenuScreen.this.displayBlocks.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        /* Debug info: failed to restart local var, previous not found, register: 6 */
        public View getView(int position, View convertView, ViewGroup parent) {
            MainRowHolder holder;
            if (convertView == null) {
                convertView = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.menu_row, (ViewGroup) null);
                holder = new MainRowHolder();
                holder.textLocale = (TextView) convertView.findViewById(R.id.textLocale);
                holder.imgMore = (ImageView) convertView.findViewById(R.id.imgMore);
                holder.imgIcon = (ImageView) convertView.findViewById(R.id.imgIcon);
                convertView.setTag(holder);
            } else {
                holder = (MainRowHolder) convertView.getTag();
            }
            if (((DisplayBlock) MenuScreen.this.displayBlocks.get(position)).getType().contains("Grouping")) {
                holder.imgMore.setVisibility(0);
            } else {
                holder.imgMore.setVisibility(4);
            }
            holder.textLocale.setText(((DisplayBlock) MenuScreen.this.displayBlocks.get(position)).getName());
            Drawable icon = VerveManager.getInstance().getIcon(((DisplayBlock) MenuScreen.this.displayBlocks.get(position)).getIconStyle(), ((DisplayBlock) MenuScreen.this.displayBlocks.get(position)).getType());
            if (icon != null) {
                holder.imgIcon.setImageDrawable(icon);
            }
            return convertView;
        }
    }

    class MainRowHolder {
        public ImageView imgIcon;
        public ImageView imgMore;
        public TextView textLocale;

        MainRowHolder() {
        }
    }

    private void customizeScreen() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ScreenManager.getInstance().getContext());
        builder.setTitle("Customize home screen");
        builder.setCancelable(false);
        builder.setMultiChoiceItems(this.customizeTitles, this.customizeStates, new DialogInterface.OnMultiChoiceClickListener() {
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
            }
        });
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                MenuScreen.this.prepareAdapter();
                if (MenuScreen.this.displayBlocks.size() == 0) {
                    ScreenManager.getInstance().showDialogOK("Information", "At least one category has to be visible, enabling the first category.");
                    MenuScreen.this.customizeStates[0] = true;
                    MenuScreen.this.prepareAdapter();
                }
                ScreenManager.getInstance().setCustomizeStates(MenuScreen.this.customizeStates);
                MenuScreen.this.menuAdapter.notifyDataSetChanged();
            }
        });
        builder.create().show();
    }

    public Menu getOptions(Menu menu) {
        menu.add("Settings").setIcon((int) R.drawable.ic_menu_preferences);
        if (ScreenManager.getInstance().getMainScreenIndex() == 0) {
            menu.add("Customize").setIcon((int) R.drawable.ic_menu_edit);
        }
        return menu;
    }

    public void onOptionsMenuItem(String title) {
        if (title.contains("Settings")) {
            ScreenManager.getInstance().add(new SettingsScreen().getView());
        } else if (title.contains("Customize") && ScreenManager.getInstance().getMainScreenIndex() == 0) {
            customizeScreen();
        }
    }
}
