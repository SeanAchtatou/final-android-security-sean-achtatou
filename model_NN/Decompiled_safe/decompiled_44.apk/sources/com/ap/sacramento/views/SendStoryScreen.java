package com.ap.sacramento.views;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.ap.sacramento.R;
import com.ap.sacramento.common.BaseScreen;
import com.ap.sacramento.common.Logger;
import com.ap.sacramento.managers.ScreenManager;
import com.ap.sacramento.managers.VerveManager;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

public class SendStoryScreen extends BaseScreen {
    public static final int CAMERA_IMAGE_REQUEST = 1337;
    public static final int CAMERA_VIDEO_REQUEST = 1338;
    public static final int GALLERY_IMAGE_REQUEST = 1339;
    public static final int GALLERY_VIDEO_REQUEST = 1339;
    View category_row;
    private ListView listSettings;
    List<View> listViews;
    Uri outputFileUri = null;
    private int responseCode = 0;
    private String sendTo;
    View settings_choose;
    View settings_email;
    View settings_info;
    View settings_take;
    View settings_tell;
    private StoryAdapter storyAdapter;

    public SendStoryScreen(boolean showTitle) {
        this.showTitle = showTitle;
        this.container = (ViewGroup) ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.settingsscreen, (ViewGroup) null);
        this.container.setTag(this);
        this.listViews = new LinkedList();
        this.listSettings = (ListView) this.container.findViewById(R.id.listSettings);
        this.storyAdapter = new StoryAdapter();
        this.listSettings.setAdapter((ListAdapter) this.storyAdapter);
        this.listSettings.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                switch (arg2) {
                    case CapiChangeListener.CAPI_STATUS_REREG /*1*/:
                        SendStoryScreen.this.openGallery();
                        return;
                    case CapiChangeListener.CAPI_STATUS_HIER /*2*/:
                        SendStoryScreen.this.openCamera();
                        return;
                    case 3:
                        SendStoryScreen.this.sendEmail("");
                        return;
                    default:
                        return;
                }
            }
        });
        this.titlePanel = this.container.findViewById(R.id.title);
        if (showTitle) {
            this.textLeft = (TextView) this.container.findViewById(R.id.textLeft);
            this.textLeft.setText("Send a story");
            this.textRight = (TextView) this.container.findViewById(R.id.textRight);
            this.textRight.setVisibility(8);
            this.imgHeader = (ImageView) this.container.findViewById(R.id.imgHeader);
            Drawable header = VerveManager.getInstance().getTitleHeader();
            if (header != null) {
                this.imgHeader.setImageDrawable(header);
            }
        } else {
            this.titlePanel.setVisibility(8);
        }
        ScreenManager.getInstance().showProgress("Loading ...", "");
    }

    public void closed(boolean isHidden) {
    }

    public void displayed(boolean isBack) {
        if (!isBack) {
            this.category_row = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.category_row, (ViewGroup) null);
            ((TextView) this.category_row.findViewById(R.id.textCategoryTitle)).setText(String.format("Tell %s your story", VerveManager.getInstance().getVerveApi().getLocale().getName()));
            this.settings_choose = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.settings_single_row, (ViewGroup) null);
            ((TextView) this.settings_choose.findViewById(R.id.textTitle)).setText("Choose Photo or Video");
            this.settings_take = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.settings_single_row, (ViewGroup) null);
            ((TextView) this.settings_take.findViewById(R.id.textTitle)).setText("Take Photo or Video");
            this.settings_email = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.settings_single_row, (ViewGroup) null);
            ((TextView) this.settings_email.findViewById(R.id.textTitle)).setText("Compose E-mail");
            this.settings_info = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.settings_single_row, (ViewGroup) null);
            ((TextView) this.settings_info.findViewById(R.id.textTitle)).setText("Info");
            this.listViews.add(this.category_row);
            this.listViews.add(this.settings_choose);
            this.listViews.add(this.settings_take);
            this.listViews.add(this.settings_email);
            this.storyAdapter.notifyDataSetChanged();
            ScreenManager.getInstance().closeProgress();
        }
    }

    public void display() {
    }

    public void openCamera() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ScreenManager.getInstance().getContext());
        builder.setTitle("Select media");
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == -2) {
                    dialog.dismiss();
                }
            }
        });
        builder.setItems(new String[]{"Photo", "Video"}, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    ScreenManager.getInstance().showProgress("Opening photo camera ...", "");
                    Intent cameraIntent = new Intent("android.media.action.IMAGE_CAPTURE");
                    File file = new File(Environment.getExternalStorageDirectory(), "attachment.jpg");
                    SendStoryScreen.this.outputFileUri = Uri.fromFile(file);
                    cameraIntent.putExtra("output", SendStoryScreen.this.outputFileUri);
                    ScreenManager.getInstance().getContext().startActivityForResult(cameraIntent, SendStoryScreen.CAMERA_IMAGE_REQUEST);
                } else {
                    ScreenManager.getInstance().showProgress("Opening video camera ...", "");
                    Intent cameraIntent2 = new Intent("android.media.action.VIDEO_CAPTURE");
                    File file2 = new File(Environment.getExternalStorageDirectory(), "attachment.3gp");
                    SendStoryScreen.this.outputFileUri = Uri.fromFile(file2);
                    cameraIntent2.putExtra("output", SendStoryScreen.this.outputFileUri);
                    ScreenManager.getInstance().getContext().startActivityForResult(cameraIntent2, SendStoryScreen.CAMERA_VIDEO_REQUEST);
                }
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    public void openGallery() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ScreenManager.getInstance().getContext());
        builder.setTitle("Select media");
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == -2) {
                    dialog.dismiss();
                }
            }
        });
        builder.setItems(new String[]{"Photo", "Video"}, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                ScreenManager.getInstance().showProgress("Opening media gallery ...", "");
                if (item == 0) {
                    Intent galleryIntent = new Intent("android.intent.action.GET_CONTENT");
                    galleryIntent.setType("image/*");
                    ScreenManager.getInstance().getContext().startActivityForResult(Intent.createChooser(galleryIntent, "Select Picture"), 1339);
                } else {
                    Intent galleryIntent2 = new Intent("android.intent.action.GET_CONTENT");
                    galleryIntent2.setType("video/*");
                    ScreenManager.getInstance().getContext().startActivityForResult(Intent.createChooser(galleryIntent2, "Select Video"), 1339);
                }
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    class StoryAdapter extends BaseAdapter {
        StoryAdapter() {
        }

        public int getCount() {
            return SendStoryScreen.this.listViews.size();
        }

        public Object getItem(int position) {
            return SendStoryScreen.this.listViews.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            Logger.logDebug("SettingsScreen getView");
            return SendStoryScreen.this.listViews.get(position);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != -1) {
            return;
        }
        if (requestCode == 1337) {
            if (this.outputFileUri != null) {
                sendEmail(this.outputFileUri.toString());
            }
        } else if (requestCode == 1338) {
            sendEmail(this.outputFileUri.toString());
        } else if (requestCode == 1339) {
            String[] filePathColumn = {"_data"};
            Cursor cursor = ScreenManager.getInstance().getContext().getContentResolver().query(data.getData(), filePathColumn, null, null, null);
            cursor.moveToFirst();
            String filePath = cursor.getString(cursor.getColumnIndex(filePathColumn[0]));
            cursor.close();
            sendEmail("file://" + filePath);
        } else if (requestCode == 1339) {
            String[] filePathColumn2 = {"_data"};
            Cursor cursor2 = ScreenManager.getInstance().getContext().getContentResolver().query(data.getData(), filePathColumn2, null, null, null);
            cursor2.moveToFirst();
            String filePath2 = cursor2.getString(cursor2.getColumnIndex(filePathColumn2[0]));
            cursor2.close();
            sendEmail("file://" + filePath2);
        }
    }

    public void sendEmail(String attachment) {
        ScreenManager.getInstance().showProgress("Opening ...", "");
        Intent emailIntent = new Intent("android.intent.action.SEND");
        emailIntent.setType("text/html");
        if ("appsupport@vervewireless.com".length() > 0) {
            emailIntent.putExtra("android.intent.extra.EMAIL", new String[]{this.sendTo});
        }
        emailIntent.putExtra("android.intent.extra.SUBJECT", "Send us a story - Verve Publisher Application on Android");
        emailIntent.putExtra("android.intent.extra.TEXT", "");
        if (attachment.length() > 0) {
            emailIntent.putExtra("android.intent.extra.STREAM", Uri.parse(attachment));
        }
        ScreenManager.getInstance().getContext().startActivityForResult(Intent.createChooser(emailIntent, "Send a story via E-mail"), 0);
    }

    /* access modifiers changed from: package-private */
    public void setSendTo(String email) {
        this.sendTo = email;
    }
}
