package com.ap.sacramento.views;

import android.content.Intent;
import android.content.res.Configuration;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ViewFlipper;
import com.ap.sacramento.R;
import com.ap.sacramento.common.BaseScreen;
import com.ap.sacramento.common.CustomHorizontalScrollView;
import com.ap.sacramento.common.Logger;
import com.ap.sacramento.managers.ScreenManager;
import com.ap.sacramento.managers.VerveManager;
import com.vervewireless.capi.DisplayBlock;
import java.util.List;

public class ItemsScreen extends BaseScreen {
    final float SCALE = ScreenManager.getInstance().getContext().getResources().getDisplayMetrics().density;
    private ImageView arrLeft;
    private ImageView arrRight;
    private List<DisplayBlock> categories;
    private BaseScreen currentScreen = null;
    private DisplayBlock displayBlock;
    private ViewFlipper itemsPanel;
    private LinearLayout layoutCategories;
    private CustomHorizontalScrollView scrollCategories;
    /* access modifiers changed from: private */
    public Button selectedButton = null;
    /* access modifiers changed from: private */
    public int selectedIndex = -1;

    public ItemsScreen(DisplayBlock displayBlock2) {
        this.displayBlock = displayBlock2;
        this.container = (ViewGroup) ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.itemsscreen, (ViewGroup) null);
        this.container.setTag(this);
        this.scrollCategories = (CustomHorizontalScrollView) this.container.findViewById(R.id.scrollCategories);
        this.arrLeft = (ImageView) this.container.findViewById(R.id.arrLeft);
        this.arrRight = (ImageView) this.container.findViewById(R.id.arrRight);
        this.scrollCategories.setFadingEdgeLength((int) ((45.0f * this.SCALE) + 0.5f));
        this.scrollCategories.init(this.arrLeft, this.arrRight);
        this.scrollCategories.setSmoothScrollingEnabled(true);
        this.itemsPanel = (ViewFlipper) this.container.findViewById(R.id.itemsPanel);
        this.layoutCategories = (LinearLayout) this.container.findViewById(R.id.layoutCategories);
        initCategories();
        initArrows();
    }

    public void closed(boolean isHidden) {
        super.closed(isHidden);
        if (!isHidden) {
        }
        if (this.currentScreen != null) {
            this.currentScreen.closed(isHidden);
        }
    }

    public void displayed(boolean isBack) {
        super.displayed(isBack);
        if (!isBack) {
            this.state = 1;
            initFirstItem();
        } else if (this.currentScreen != null) {
            this.currentScreen.displayed(isBack);
        }
    }

    private void initArrows() {
    }

    public void activate() {
        super.activate();
        if (this.currentScreen != null) {
            this.currentScreen.activate();
        }
    }

    public void deactivate() {
        super.deactivate();
        if (this.currentScreen != null) {
            this.currentScreen.deactivate();
        }
    }

    public void display() {
    }

    private void initFirstItem() {
        if (this.layoutCategories.getChildCount() > 0) {
            this.selectedButton = (Button) this.layoutCategories.getChildAt(0);
            this.selectedIndex = -1;
            this.selectedButton.requestFocus();
            onCategoryItem(0);
        }
    }

    private void select() {
        if (this.selectedButton != null) {
            this.selectedButton.setSelected(true);
        }
    }

    /* access modifiers changed from: private */
    public void deselect() {
        if (this.selectedButton != null) {
            this.selectedButton.setSelected(false);
        }
    }

    private void initCategories() {
        CategoryOnClickListener categoryListener = new CategoryOnClickListener();
        this.categories = VerveManager.getInstance().clean(this.displayBlock.getDisplayBlocks());
        int count = 0;
        if (this.categories.size() != 0) {
            for (DisplayBlock block : this.categories) {
                Button btn = (Button) ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.category_menuitem, (ViewGroup) null);
                if (count == 0) {
                    this.selectedButton = btn;
                }
                btn.setText(block.getName());
                btn.setTag(String.valueOf(String.valueOf(count)));
                btn.setOnClickListener(categoryListener);
                this.layoutCategories.addView(btn);
                count++;
            }
        } else {
            this.layoutCategories.setVisibility(8);
        }
        Logger.logDebug("initCategories " + String.valueOf(this.layoutCategories.getWidth()));
    }

    private void handleCategory(int position) {
        this.itemsPanel.removeAllViews();
        this.currentScreen = null;
        DisplayBlock block = this.categories.get(position);
        String type = block.getType();
        Logger.logDebug("handleCategory " + type);
        BaseScreen screen = null;
        if (type.contains("contentModule") || "multiSourceModule".equals(type) || "Editorial".equals(block.getContentType())) {
            screen = new NewsScreen(block);
        } else if (type.contains("savedNews")) {
            screen = new NewsScreen(block);
        } else if (type.contains("NewsModule")) {
            screen = new NewsScreen(block);
        } else if (type.contains("photo")) {
            screen = new GalleryThumbsScreen(block, ScreenManager.getInstance().getTitle());
        } else if (type.contains("video")) {
            screen = new VideoScreen(block);
        } else if (type.contains("staticLink")) {
            String url = block.getXmlUrl();
            if (!url.contains("wlappurl")) {
                screen = new BrowserScreen(url, block);
            }
        }
        if (screen != null) {
            this.currentScreen = screen;
            this.itemsPanel.addView(this.currentScreen.getView());
            this.currentScreen.displayed(false);
            this.itemsPanel.setDisplayedChild(0);
        }
        Logger.logDebug("handleCategory finished" + type);
    }

    /* access modifiers changed from: private */
    public void onCategoryItem(int position) {
        if (this.selectedIndex != position) {
            this.selectedIndex = position;
            select();
            handleCategory(position);
        }
    }

    public class CategoryOnClickListener implements View.OnClickListener {
        public CategoryOnClickListener() {
        }

        public void onClick(View v) {
            if (v instanceof Button) {
                int position = Integer.valueOf(v.getTag().toString()).intValue();
                Logger.logDebug("onClick " + v.getTag().toString());
                if (ItemsScreen.this.selectedIndex != position) {
                    ItemsScreen.this.deselect();
                    Button unused = ItemsScreen.this.selectedButton = (Button) v;
                    ItemsScreen.this.onCategoryItem(position);
                }
            }
        }
    }

    private void setArrows() {
    }

    public void onShareItem(int shareId) {
        if (this.currentScreen != null) {
            this.currentScreen.onShareItem(shareId);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (this.currentScreen != null) {
            this.currentScreen.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void onTouch(MotionEvent event) {
        if (this.currentScreen != null) {
            this.currentScreen.onTouch(event);
        }
    }

    public Menu getOptions(Menu menu) {
        if (this.currentScreen != null) {
            return this.currentScreen.getOptions(menu);
        }
        return menu;
    }

    public void onOptionsMenuItem(String title) {
        if (this.currentScreen != null) {
            this.currentScreen.onOptionsMenuItem(title);
        }
    }

    public void reportPageView() {
        if (this.currentScreen != null) {
            this.currentScreen.reportPageView();
        }
    }

    public void onSearchKey() {
        Logger.logDebug("Items Screen KeyEvent.KEYCODE_SEARCH");
        if (this.currentScreen != null) {
            this.currentScreen.onSearchKey();
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        Logger.logDebug("Items Screen onConfigurationChanged");
        super.onConfigurationChanged(newConfig);
        this.scrollCategories.setScreenChange();
        if (this.scrollCategories != null) {
            this.scrollCategories.update();
        }
    }
}
