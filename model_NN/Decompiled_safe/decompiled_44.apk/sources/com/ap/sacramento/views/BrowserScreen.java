package com.ap.sacramento.views;

import android.graphics.Bitmap;
import android.view.ViewGroup;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import com.ap.sacramento.R;
import com.ap.sacramento.common.BaseScreen;
import com.ap.sacramento.common.CustomWebView;
import com.ap.sacramento.managers.ScreenManager;
import com.ap.sacramento.managers.VerveManager;
import com.vervewireless.capi.DisplayBlock;

public class BrowserScreen extends BaseScreen {
    private DisplayBlock displayBlock;
    private String staticLink;
    private CustomWebView webView = ((CustomWebView) this.container.findViewById(R.id.webView));

    public BrowserScreen(String staticLink2, DisplayBlock block) {
        this.staticLink = staticLink2;
        this.displayBlock = block;
        this.container = (ViewGroup) ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.newsdetailsscreen, (ViewGroup) null);
        this.container.setTag(this);
        this.titlePanel = this.container.findViewById(R.id.title);
        this.titlePanel.setVisibility(8);
        ((ImageView) this.container.findViewById(R.id.btnPrev)).setVisibility(8);
        ((ImageView) this.container.findViewById(R.id.btnNext)).setVisibility(8);
        this.webView.getSettings().setUseWideViewPort(false);
        this.webView.getSettings().setBuiltInZoomControls(true);
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.setWebViewClient(new BrowserWebViewClient());
        this.webView.setScrollBarStyle(0);
        this.webView.requestFocus(130);
        this.webView.setWebChromeClient(new WebChromeClient() {
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                callback.invoke(origin, true, false);
            }
        });
        this.webView.setParent(this);
    }

    public void closed(boolean isHidden) {
        if (!isHidden) {
        }
    }

    public void displayed(boolean isBack) {
        if (!isBack) {
            this.state = 1;
            ScreenManager.getInstance().showProgress("Loading...", "");
            display();
        }
        VerveManager.getInstance().getMainAdManager().setDisplayBlock(this.displayBlock);
        VerveManager.getInstance().getMainAdManager().loadAd();
    }

    public void display() {
        this.webView.loadUrl(this.staticLink);
    }

    public class BrowserWebViewClient extends WebViewClient {
        public BrowserWebViewClient() {
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        public void onPageFinished(WebView webView, String url) {
            ScreenManager.getInstance().closeProgress();
        }
    }
}
