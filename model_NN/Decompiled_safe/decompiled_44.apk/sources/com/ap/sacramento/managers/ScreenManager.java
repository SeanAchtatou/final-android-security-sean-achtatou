package com.ap.sacramento.managers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ViewFlipper;
import com.ap.sacramento.R;
import com.ap.sacramento.common.BaseScreen;
import com.ap.sacramento.common.CustomProgressDialog;
import com.ap.sacramento.common.Logger;
import com.ap.sacramento.views.SplashScreen;
import com.vervewireless.capi.Locale;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

public class ScreenManager {
    private static ScreenManager me = new ScreenManager();
    public Activity context;
    /* access modifiers changed from: private */
    public View currentView = null;
    private int[] customizedMenuItems;
    private boolean displayTitle = false;
    private boolean externalActivity = false;
    private HashMap<String, Drawable> gallerycache = new HashMap<>();
    private HashMap<String, Drawable> imagecache = new HashMap<>();
    private boolean isCustomized = false;
    /* access modifiers changed from: private */
    public boolean isWorking = false;
    /* access modifiers changed from: private */
    public ViewFlipper mainFlipperPanel;
    private int mainScreenIndex = 0;
    /* access modifiers changed from: private */
    public View previousView = null;
    private CustomProgressDialog progress;
    private boolean savedStory = false;
    private boolean savedStoryChange = false;
    private Animation slideLeftIn;
    private Animation slideLeftOut;
    private Animation slideRightIn;
    private Animation slideRightOut;
    private String titleMessage;

    public static ScreenManager getInstance() {
        return me;
    }

    public boolean IsExternalActivity() {
        return this.externalActivity;
    }

    public void setExternalActivity(boolean externalActivity2) {
        this.externalActivity = externalActivity2;
    }

    public boolean isDisplayTitle() {
        return this.displayTitle;
    }

    public void setDisplayTitle(boolean title) {
        this.displayTitle = title;
    }

    public boolean isValid() {
        return this.mainFlipperPanel.getChildCount() > 2;
    }

    public void init() {
        this.slideLeftIn = AnimationUtils.loadAnimation(getContext(), R.anim.slide_left_in);
        this.slideLeftOut = AnimationUtils.loadAnimation(getContext(), R.anim.slide_left_out);
        this.slideRightIn = AnimationUtils.loadAnimation(getContext(), R.anim.slide_right_in);
        this.slideRightOut = AnimationUtils.loadAnimation(getContext(), R.anim.slide_right_out);
        this.slideLeftIn.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                ScreenManager.this.mainFlipperPanel.postDelayed(new Runnable() {
                    public void run() {
                        ((BaseScreen) ScreenManager.this.previousView.getTag()).closed(true);
                        ((BaseScreen) ScreenManager.this.currentView.getTag()).displayed(false);
                        ScreenManager.this.previousView.setVisibility(4);
                    }
                }, 200);
                boolean unused = ScreenManager.this.isWorking = false;
            }
        });
        this.slideRightIn.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
                ScreenManager.this.currentView.setVisibility(0);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                ((BaseScreen) ScreenManager.this.previousView.getTag()).closed(false);
                ((BaseScreen) ScreenManager.this.currentView.getTag()).displayed(true);
                ScreenManager.this.mainFlipperPanel.clearAnimation();
                ScreenManager.this.mainFlipperPanel.post(new Runnable() {
                    public void run() {
                        ScreenManager.getInstance().getContext().runOnUiThread(new Runnable() {
                            public void run() {
                                try {
                                    ScreenManager.this.mainFlipperPanel.removeView(ScreenManager.this.previousView);
                                } catch (Exception e) {
                                }
                            }
                        });
                    }
                });
                boolean unused = ScreenManager.this.isWorking = false;
            }
        });
    }

    public void setContext(Activity context2) {
        this.context = context2;
    }

    public Activity getContext() {
        return this.context;
    }

    public void setMainFlipperPanel(ViewFlipper panel) {
        this.mainFlipperPanel = panel;
    }

    public void splash(View view) {
        this.mainFlipperPanel.addView(view);
        this.currentView = view;
        this.mainFlipperPanel.showNext();
    }

    public void removeSplash() {
        this.mainFlipperPanel.setInAnimation(this.slideLeftIn);
        this.mainFlipperPanel.setOutAnimation(this.slideLeftOut);
        if (this.mainFlipperPanel.getChildCount() == 2 && (this.mainFlipperPanel.getChildAt(0).getTag() instanceof SplashScreen)) {
            this.mainFlipperPanel.removeViewAt(0);
        }
    }

    public void add(View view) {
        if (!this.isWorking) {
            this.isWorking = true;
            if (view != this.mainFlipperPanel.getCurrentView()) {
                this.previousView = this.mainFlipperPanel.getCurrentView();
                this.previousView.setClickable(false);
                this.mainFlipperPanel.addView(view);
                this.mainFlipperPanel.setInAnimation(this.slideLeftIn);
                this.mainFlipperPanel.setOutAnimation(this.slideLeftOut);
                this.currentView = view;
                this.mainFlipperPanel.showNext();
            }
        }
    }

    public void back() {
        if (!this.isWorking) {
            this.isWorking = true;
            this.previousView = this.mainFlipperPanel.getCurrentView();
            this.previousView.setClickable(false);
            this.mainFlipperPanel.setInAnimation(this.slideRightIn);
            this.mainFlipperPanel.setOutAnimation(this.slideRightOut);
            this.mainFlipperPanel.showPrevious();
            this.currentView = this.mainFlipperPanel.getCurrentView();
        }
    }

    public void showDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getInstance().getContext());
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }

    public void showDialogOK(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getInstance().getContext());
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", (DialogInterface.OnClickListener) null);
        builder.show();
    }

    public void showCaption(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getInstance().getContext());
        String message2 = message.replace("<p>", "").replace("</p>", "");
        builder.setTitle(title);
        builder.setMessage(message2);
        AlertDialog dialog = builder.show();
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.alpha = 0.7f;
        dialog.getWindow().setAttributes(lp);
    }

    public synchronized void addToImageCache(String key, Drawable icon) {
        if (!this.imagecache.containsKey(key)) {
            this.imagecache.put(key, icon);
        }
    }

    public synchronized Drawable getFromImageCache(String key) {
        Drawable drawable;
        if (!this.imagecache.containsKey(key)) {
            drawable = null;
        } else {
            drawable = this.imagecache.get(key);
        }
        return drawable;
    }

    public void clearImageCache() {
        this.imagecache.clear();
    }

    public synchronized void addToImageGalleryCache(String key, Drawable icon) {
        if (!this.gallerycache.containsKey(key)) {
            this.gallerycache.put(key, icon);
        }
    }

    public synchronized Drawable getFromImageGalleryCache(String key) {
        Drawable drawable;
        if (!this.gallerycache.containsKey(key)) {
            drawable = null;
        } else {
            drawable = this.gallerycache.get(key);
        }
        return drawable;
    }

    public synchronized void clearImageGalleryCache() {
        this.gallerycache.clear();
    }

    public String readAsset(String file) {
        try {
            InputStream is = getInstance().getContext().getAssets().open(file);
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            is.close();
            return new String(buffer);
        } catch (IOException e) {
            return null;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Menu getOptions(Menu menu) {
        if (this.currentView != null) {
            return ((BaseScreen) this.currentView.getTag()).getOptions(menu);
        }
        return null;
    }

    public void onOptionsMenuItem(String title) {
        if (this.currentView != null) {
            ((BaseScreen) this.currentView.getTag()).onOptionsMenuItem(title);
        }
    }

    public void onShareItem(int shareId) {
        if (this.currentView != null) {
            ((BaseScreen) this.currentView.getTag()).onShareItem(shareId);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (this.currentView != null) {
            ((BaseScreen) this.currentView.getTag()).onActivityResult(requestCode, resultCode, data);
        }
    }

    public void registerLocale(Locale selectedLocale) {
        this.mainFlipperPanel.setInAnimation(null);
        this.mainFlipperPanel.setOutAnimation(null);
        VerveManager.getInstance().hideTitle();
        try {
            this.mainFlipperPanel.removeViews(1, this.mainFlipperPanel.getChildCount() - 1);
            this.currentView = this.mainFlipperPanel.getChildAt(0);
            this.previousView = null;
            getInstance().setExternalActivity(false);
            if (this.currentView.getTag() instanceof SplashScreen) {
                ((SplashScreen) this.currentView.getTag()).register(selectedLocale);
            }
        } catch (Exception e) {
        }
    }

    public void showProgress(String message, String title) {
        if (this.progress != null && this.progress.isShowing()) {
            this.progress.dismiss();
            this.progress = null;
        }
        this.progress = new CustomProgressDialog(getInstance().getContext());
        this.progress.setMessage(message);
        if (title.length() > 0) {
            this.progress.setTitle(title);
        }
        this.progress.show();
    }

    public void changeProgress(String message) {
        if (this.progress != null) {
            this.progress.setMessage(message);
        }
    }

    public void closeProgress() {
        try {
            this.progress.dismiss();
            this.progress = null;
        } catch (Exception e) {
        }
    }

    public void setTitle(String title) {
        this.titleMessage = title;
    }

    public String getTitle() {
        return this.titleMessage;
    }

    public void setMainScreenIndex(int index) {
        this.mainScreenIndex = index;
    }

    public int getMainScreenIndex() {
        return this.mainScreenIndex;
    }

    public void setSavedStory(boolean saved) {
        this.savedStory = saved;
    }

    public boolean isSavedStory() {
        return this.savedStory;
    }

    public void setSavedStoryChange(boolean saved) {
        this.savedStoryChange = saved;
    }

    public boolean isSavedStoryChange() {
        return this.savedStoryChange;
    }

    public boolean[] getCustomizeStates() {
        SharedPreferences settings = getContext().getSharedPreferences("CustomizeStates", 0);
        if (!settings.contains("states")) {
            return null;
        }
        String temp = settings.getString("states", "");
        Logger.logDebug("getCustomizeStates " + temp);
        if (temp.length() == 0) {
            return null;
        }
        String[] values = temp.split(",");
        if (values.length == 0) {
            return null;
        }
        boolean[] states = new boolean[values.length];
        for (int i = 0; i < values.length; i++) {
            if (values[i].contains("true")) {
                states[i] = true;
            } else {
                states[i] = false;
            }
        }
        Logger.logDebug("getCustomizeStates items size " + states.length);
        return states;
    }

    public void setCustomizeStates(boolean[] states) {
        SharedPreferences.Editor editor = getContext().getSharedPreferences("CustomizeStates", 0).edit();
        if (states == null) {
            editor.clear();
            editor.commit();
            return;
        }
        String temp = "";
        for (int i = 0; i < states.length; i++) {
            if (states[i]) {
                temp = temp + "true";
            } else {
                temp = temp + "false";
            }
            if (i < states.length - 1) {
                temp = temp + ",";
            }
        }
        editor.clear();
        Logger.logDebug("setCustomizeStates " + temp);
        editor.putString("states", temp);
        editor.commit();
    }

    public void setLocationMode(int mode) {
        SharedPreferences.Editor editor = getContext().getSharedPreferences("LocationMode", 0).edit();
        if (mode == 3) {
            editor.clear();
            editor.commit();
            return;
        }
        editor.putString("mode", String.valueOf(mode));
        editor.commit();
    }

    public int getLocationMode() {
        SharedPreferences settings = getContext().getSharedPreferences("LocationMode", 0);
        if (settings.contains("mode")) {
            return Integer.parseInt(settings.getString("mode", "0"));
        }
        return 0;
    }

    public void setLastPostal(String postal) {
        SharedPreferences.Editor editor = getContext().getSharedPreferences("LocationMode", 0).edit();
        editor.putString("postal", postal);
        editor.commit();
    }

    public String getLastPostal() {
        SharedPreferences settings = getContext().getSharedPreferences("LocationMode", 0);
        if (settings.contains("postal")) {
            return settings.getString("postal", "0");
        }
        return "";
    }

    public void setLastLocation(Location location) {
        SharedPreferences.Editor editor = getContext().getSharedPreferences("LocationMode", 0).edit();
        editor.putString("Lat", String.valueOf(location.getLatitude()));
        editor.putString("Long", String.valueOf(location.getLongitude()));
        editor.commit();
    }

    public Location getLastLocation() {
        SharedPreferences settings = getContext().getSharedPreferences("LocationMode", 0);
        if (!settings.contains("Lat")) {
            return null;
        }
        Location loc = new Location("Storage");
        loc.setLatitude(Double.parseDouble(settings.getString("Lat", "0")));
        loc.setLongitude(Double.parseDouble(settings.getString("Long", "0")));
        return loc;
    }

    public void clearLocationMode() {
    }

    public void saveMenuItems(int[] items) {
        SharedPreferences.Editor editor = getContext().getSharedPreferences("MenuItems", 0).edit();
        String value = "";
        int length = items.length;
        for (int i = 0; i < length; i++) {
            value = value + String.valueOf(items[i]);
            if (i != length - 1) {
                value = value + ",";
            }
        }
        Logger.logDebug("Saving menu items " + value);
        editor.putString("MenuItems", value);
        editor.commit();
    }

    public int[] loadMenuItems() {
        SharedPreferences settings = getContext().getSharedPreferences("MenuItems", 0);
        if (settings.contains("MenuItems")) {
            String value = settings.getString("MenuItems", "0");
            Logger.logDebug("Loaded menu items " + value);
            String[] values = value.split(",");
            try {
                int[] items = new int[values.length];
                for (int i = 0; i < values.length; i++) {
                    items[i] = Integer.parseInt(values[i]);
                }
                if (values.length == 0) {
                    return null;
                }
                return items;
            } catch (Exception e) {
            }
        }
        return null;
    }

    public void setCustomized(boolean customized) {
        this.isCustomized = customized;
    }

    public boolean isCustomized() {
        return this.isCustomized;
    }

    public void setCustomizedItems(int[] items) {
        this.customizedMenuItems = items;
    }

    public int[] getCustomizedItems() {
        return this.customizedMenuItems;
    }

    public void showTitle() {
    }

    public void hideTitle() {
    }

    public void showTitleProgress() {
    }

    public void hideTitleProgress() {
    }

    public void onConfigurationChanged(Configuration newConfig) {
        if (this.currentView != null) {
            ((BaseScreen) this.currentView.getTag()).onConfigurationChanged(newConfig);
        }
    }

    public void onSearchKey() {
        if (this.currentView != null) {
            ((BaseScreen) this.currentView.getTag()).onSearchKey();
        }
    }
}
