package com.ap.sacramento.managers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.ap.sacramento.R;
import com.ap.sacramento.common.Logger;
import com.vervewireless.capi.ContentItem;
import com.vervewireless.capi.DisplayBlock;
import com.vervewireless.capi.MediaItem;
import com.vervewireless.capi.Verve;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;

public class VerveManager {
    private static VerveManager me = new VerveManager();
    String HTMLNoTrackingAd = null;
    String HTMLRawAd = null;
    String HTMLTrackingAd = null;
    final float SCALE = ScreenManager.getInstance().getContext().getResources().getDisplayMetrics().density;
    int ScreenHeight = 0;
    int ScreenWidth = 0;
    private AdManager adManager = null;
    private Verve api;
    private Drawable drawableHeader = null;
    private Drawable drawableSplash = null;
    private Drawable drawableThumb = null;
    private boolean emptyThumbs = true;
    private ImageView imgHeader;
    /* access modifiers changed from: private */
    public boolean isPostalDialogVisible = false;
    boolean isWide = false;
    /* access modifiers changed from: private */
    public LocationManager locationManager;
    private String locationProvider = null;
    private List<PageviewHandler> pageviewHandlers = new ArrayList(1);
    String strValueThumbPixels;
    private TextView textLeft;
    private TextView textRight;
    private View title;
    int valuePixels;

    public static VerveManager getInstance() {
        return me;
    }

    public void setVerveApi(Verve api2) {
        this.api = api2;
        Display display = ScreenManager.getInstance().getContext().getWindowManager().getDefaultDisplay();
        this.ScreenHeight = display.getHeight();
        this.ScreenWidth = display.getWidth();
        if ((this.ScreenHeight >= 800) || (this.ScreenWidth >= 800)) {
            this.isWide = true;
        }
        ScreenManager.getInstance().getContext();
        this.locationManager = (LocationManager) ScreenManager.getInstance().getContext().getSystemService("location");
        this.locationProvider = "network";
        this.valuePixels = (int) ((60.0f * this.SCALE) + 0.5f);
        this.strValueThumbPixels = String.valueOf(this.valuePixels);
    }

    public void addPageviewHandler(PageviewHandler handler) {
        this.pageviewHandlers.add(handler);
    }

    public Verve getVerveApi() {
        return this.api;
    }

    public String getMediaItem(ContentItem item, boolean thumb) {
        for (MediaItem mItem : item.getMediaItems()) {
            if (mItem.getMediaType().startsWith("image/")) {
                String url = mItem.getUrl();
                if (thumb) {
                    url = url + "&width=" + this.strValueThumbPixels + "&height=" + this.strValueThumbPixels + "&crop=true";
                }
                return url;
            }
        }
        return "";
    }

    public MediaItem getVideoMediaItem(ContentItem item) {
        List<MediaItem> mediaItems = item.getMediaItems();
        MediaItem mediaItem = null;
        for (MediaItem mItem : mediaItems) {
            if (mItem.getMediaType().startsWith("video/mp4") && mItem.getUrl().contains("http")) {
                mediaItem = mItem;
            }
        }
        if (mediaItem == null) {
            Logger.logDebug("getVideoMediaItem - 3gpp");
            for (MediaItem mItem2 : mediaItems) {
                if (mItem2.getMediaType().startsWith("video/3gpp") && mItem2.getUrl().contains("http")) {
                    mediaItem = mItem2;
                }
            }
        }
        return mediaItem;
    }

    private List<String> getIconList(String[] icons, boolean hdpi) {
        Pattern hdpiPat = Pattern.compile(".*@2x\\..*");
        List<String> iconList = new ArrayList<>(icons.length);
        Arrays.sort(icons);
        for (String icon : icons) {
            int index = icon.lastIndexOf(46);
            if (index >= 0 && !hdpiPat.matcher(icon).matches()) {
                iconList.add(icon);
                if (!hdpi) {
                    iconList.add(icon);
                } else {
                    String hdpiName = icon.substring(0, index) + "@2x" + icon.substring(index);
                    if (Arrays.binarySearch(icons, hdpiName) >= 0) {
                        iconList.add(hdpiName);
                    } else {
                        Logger.logWarning("No HDPI icon for " + icon);
                        iconList.add(icon);
                    }
                }
            }
        }
        return iconList;
    }

    private void cacheCategoryIcons(AssetManager assets, String iconPath, List<String> iconList) throws IOException {
        for (int i = 0; i < iconList.size(); i += 2) {
            String name = iconList.get(i);
            String fileName = iconList.get(i + 1);
            if (!name.contains("@")) {
                ScreenManager.getInstance().addToImageCache("Categories/" + name, Drawable.createFromStream(assets.open(iconPath + "/" + fileName), null));
            }
        }
    }

    public void loadIcons() {
        boolean hdpi;
        DisplayMetrics metrics = new DisplayMetrics();
        Activity context = ScreenManager.getInstance().getContext();
        context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        if (metrics.densityDpi >= 240) {
            hdpi = true;
        } else {
            hdpi = false;
        }
        Logger.logDebug("Loading icons. High density." + hdpi);
        AssetManager assets = context.getAssets();
        try {
            String[] customIcons = assets.list("Bundle/Categories");
            String[] defaultIcons = assets.list("Categories");
            List<String> customIconList = getIconList(customIcons, hdpi);
            List<String> defaultIconList = getIconList(defaultIcons, hdpi);
            cacheCategoryIcons(assets, "Bundle/Categories", customIconList);
            cacheCategoryIcons(assets, "Categories", defaultIconList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Drawable getIcon(String name, String contentType) {
        Drawable d = ScreenManager.getInstance().getFromImageCache("Categories/" + name);
        if (d != null) {
            return d;
        }
        if (contentType.contains("EditorialSaved")) {
            return ScreenManager.getInstance().getFromImageCache("Categories/Editorial.png");
        }
        if (contentType.contains("Editorial")) {
            return ScreenManager.getInstance().getFromImageCache("Categories/Editorial.png");
        }
        if (contentType.contains("video")) {
            return ScreenManager.getInstance().getFromImageCache("Categories/Video.png");
        }
        if (contentType.contains("photo")) {
            return ScreenManager.getInstance().getFromImageCache("Categories/Photos.png");
        }
        if (contentType.contains("Grouping")) {
            return ScreenManager.getInstance().getFromImageCache("Categories/Grouping.png");
        }
        if (contentType.contains("Static")) {
            return ScreenManager.getInstance().getFromImageCache("Categories/StaticLink.png");
        }
        return ScreenManager.getInstance().getFromImageCache("Categories/Editorial.png");
    }

    public Drawable getSplash() {
        if (this.drawableSplash == null) {
            try {
                if (!this.isWide) {
                    this.drawableSplash = Drawable.createFromStream(ScreenManager.getInstance().getContext().getAssets().open("Bundle/Default.png"), null);
                } else {
                    this.drawableSplash = Drawable.createFromStream(ScreenManager.getInstance().getContext().getAssets().open("Bundle/Default@2x.png"), null);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return this.drawableSplash;
    }

    public Drawable getThumb() {
        if (this.drawableThumb == null) {
            try {
                if (!this.isWide) {
                    this.drawableThumb = Drawable.createFromStream(ScreenManager.getInstance().getContext().getAssets().open("Bundle/thumbplaceholder.png"), null);
                } else {
                    this.drawableThumb = Drawable.createFromStream(ScreenManager.getInstance().getContext().getAssets().open("Bundle/thumbplaceholder@2x.png"), null);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return this.drawableThumb;
    }

    public void setTitle(View title2, TextView textLeft2, TextView textRight2, ImageView imgHeader2) {
        this.title = title2;
        this.textLeft = textLeft2;
        this.textRight = textRight2;
        this.imgHeader = imgHeader2;
    }

    public Drawable getTitleHeader() {
        if (this.drawableHeader == null) {
            try {
                if (!this.isWide) {
                    this.drawableHeader = Drawable.createFromStream(ScreenManager.getInstance().getContext().getAssets().open("Bundle/icon-small.png"), null);
                } else {
                    this.drawableHeader = Drawable.createFromStream(ScreenManager.getInstance().getContext().getAssets().open("Bundle/icon-small@2x.png"), null);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return this.drawableHeader;
    }

    public void hideTitle() {
    }

    public void displayTitle(String center, String left) {
        this.textLeft.setText(center);
        this.textRight.setText(left);
    }

    public void clearHeader() {
        this.imgHeader.clearAnimation();
    }

    public boolean containsContentItem(List<ContentItem> items, String id) {
        for (ContentItem item : items) {
            if (item.getGuid().contains(id)) {
                return true;
            }
        }
        return false;
    }

    public String getHTMLNoTrackingAd() {
        if (this.HTMLNoTrackingAd == null) {
            this.HTMLNoTrackingAd = ScreenManager.getInstance().readAsset("HTMLNoTrackingAd.html");
        }
        return this.HTMLNoTrackingAd;
    }

    public String getHTMLTrackingAd() {
        if (this.HTMLTrackingAd == null) {
            this.HTMLTrackingAd = ScreenManager.getInstance().readAsset("HTMLTrackingAd.html");
        }
        return this.HTMLTrackingAd;
    }

    public String getHTMLRawAd() {
        if (this.HTMLRawAd == null) {
            this.HTMLRawAd = ScreenManager.getInstance().readAsset("HTMLRawAd.html");
        }
        return this.HTMLRawAd;
    }

    public void clearStorageCache() {
        int count = 0;
        File[] files = ScreenManager.getInstance().getContext().getCacheDir().listFiles();
        Logger.logDebug("Storage cache items " + files.length);
        long value = 0;
        if (files.length < 220 - 20) {
            Logger.logDebug("Storage not being cleared.");
            return;
        }
        Logger.logDebug("Storage will be cleared - old items.");
        List<File> filesList = Arrays.asList(files);
        Collections.sort(filesList, new FileLastModifiedComparator());
        Collections.reverse(filesList);
        for (File f : filesList) {
            if (f.getName().contains("png")) {
                value += f.length();
                if (count < 80) {
                    f.delete();
                    count++;
                    Logger.logDebug("Deleting file from storage " + f.getName());
                }
            }
        }
    }

    public class FileLastModifiedComparator implements Comparator<File> {
        public FileLastModifiedComparator() {
        }

        public int compare(File o1, File o2) {
            if (o1.lastModified() > o2.lastModified()) {
                return -1;
            }
            return o1.lastModified() == o2.lastModified() ? 0 : 1;
        }
    }

    public boolean isProviderEnabled() {
        if (!this.locationManager.isProviderEnabled(this.locationProvider)) {
            return false;
        }
        return true;
    }

    public void getLocation(final boolean isResume) {
        Logger.logDebug("getLocation " + String.valueOf(ScreenManager.getInstance().getLocationMode()));
        if (!this.locationManager.isProviderEnabled(this.locationProvider)) {
            Logger.logDebug("Provider not enabled " + this.locationProvider);
            showPostalDialog("Unable to retrieve your location, please enter your current Postal code.", true);
            return;
        }
        this.locationManager.requestLocationUpdates(this.locationProvider, 0, 0.0f, new LocationListener() {
            public void onLocationChanged(Location location) {
                if (location != null) {
                    VerveManager.this.locationManager.removeUpdates(this);
                    ScreenManager.getInstance().setLocationMode(0);
                    Logger.logDebug("setLastLocation " + String.valueOf(location.getLatitude()) + " " + String.valueOf(location.getLongitude()));
                    ScreenManager.getInstance().setLastLocation(location);
                    VerveManager.getInstance().getVerveApi().setLocation(location);
                } else if (isResume) {
                } else {
                    if (ScreenManager.getInstance().getLastPostal().length() < 2) {
                        VerveManager.this.showPostalDialog("Unable to retrieve location.", true);
                    } else if (ScreenManager.getInstance().getLastLocation() == null && ScreenManager.getInstance().getLastPostal().length() == 0) {
                        VerveManager.this.showPostalDialog("Unable to retrieve location.", true);
                    }
                }
            }

            public void onProviderDisabled(String provider) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }
        });
    }

    private void showLocationDialog(final Location location, boolean isResume) {
        if (!isResume) {
            View location_dialog = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.locationdialog, (ViewGroup) null);
            ((EditText) location_dialog.findViewById(R.id.editLatitude)).setText(String.valueOf(location.getLatitude()));
            ((EditText) location_dialog.findViewById(R.id.editLongitude)).setText(String.valueOf(location.getLongitude()));
            AlertDialog.Builder builder = new AlertDialog.Builder(ScreenManager.getInstance().getContext());
            builder.setTitle("Is this the location to use for your local news?");
            builder.setView(location_dialog);
            builder.setCancelable(false);
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    VerveManager.this.showPostalDialog(null, true);
                }
            });
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    ScreenManager.getInstance().setLocationMode(0);
                    ScreenManager.getInstance().setLastLocation(location);
                    VerveManager.getInstance().getVerveApi().setLocation(location);
                }
            });
            builder.show();
        }
    }

    public boolean isPostalDialog() {
        return this.isPostalDialogVisible;
    }

    public void showPostalDialog(String errorMessage, boolean display) {
        if (display) {
            View postal_dialog = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.postaldialog, (ViewGroup) null);
            if (errorMessage != null) {
                TextView txtError = (TextView) postal_dialog.findViewById(R.id.txtError);
                txtError.setText(errorMessage);
                txtError.setVisibility(0);
            }
            final EditText editPostal = (EditText) postal_dialog.findViewById(R.id.editPostal);
            editPostal.setText(ScreenManager.getInstance().getLastPostal());
            AlertDialog.Builder builder = new AlertDialog.Builder(ScreenManager.getInstance().getContext());
            builder.setTitle("Manual location");
            builder.setView(postal_dialog);
            builder.setCancelable(false);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (editPostal.length() < 2) {
                        VerveManager.this.showPostalDialog("Postal code cannot be empty.", true);
                        return;
                    }
                    ScreenManager.getInstance().setLocationMode(1);
                    ScreenManager.getInstance().setLastPostal(editPostal.getText().toString());
                    VerveManager.getInstance().getVerveApi().setPostCode(editPostal.getText().toString());
                    boolean unused = VerveManager.this.isPostalDialogVisible = false;
                }
            });
            builder.show();
            this.isPostalDialogVisible = true;
        }
    }

    public List<DisplayBlock> clean(List<DisplayBlock> src) {
        if (src.isEmpty()) {
            return Collections.emptyList();
        }
        List<DisplayBlock> dest = new ArrayList<>(src.size());
        for (DisplayBlock block : src) {
            String cType = block.getContentType();
            String type = block.getType();
            if ("Editorial".equals(cType) || "Synthetic".equals(cType) || "contentModule".equals(block.getType()) || type.contains("NewsModule") || type.contains("photo") || type.contains("video") || type.contains("staticLink")) {
                dest.add(block);
            } else if (!clean(block.getDisplayBlocks()).isEmpty()) {
                dest.add(block);
            } else {
                Logger.logDebug("Ignoring:" + block.getName() + ", type: " + type + ", " + block.getContentType());
            }
        }
        return dest;
    }

    public void setMainAdManager(AdManager adManager2) {
        this.adManager = adManager2;
    }

    public AdManager getMainAdManager() {
        return this.adManager;
    }

    public void resetUserSettings() {
        Logger.logDebug("resetUserSettings");
        ScreenManager.getInstance().setMainScreenIndex(0);
        ScreenManager.getInstance().setCustomizeStates(null);
        ScreenManager.getInstance().setLocationMode(3);
        ScreenManager.getInstance().saveMenuItems(new int[0]);
    }

    public void reportPageView(DisplayBlock displayBlock, ContentItem item) {
        for (PageviewHandler listener : this.pageviewHandlers) {
            listener.reportPageview(displayBlock, item);
        }
    }

    public void reportSharing(DisplayBlock displayBlock, ContentItem item, String shareType) {
        for (PageviewHandler listener : this.pageviewHandlers) {
            listener.reportShare(displayBlock, item, shareType);
        }
    }

    public void reportCustomPageview(String guid, Integer displayBlockId, Integer partnerModuleId) {
        for (PageviewHandler listener : this.pageviewHandlers) {
            listener.reportCustomPageview(guid, displayBlockId, partnerModuleId);
        }
    }

    public void setHierarchy(DisplayBlock hierarchy) {
        for (PageviewHandler listener : this.pageviewHandlers) {
            listener.setHierarchy(hierarchy);
        }
    }

    public boolean showEmptyThumbs() {
        return this.emptyThumbs;
    }

    public void setShowEmptyThumbs(boolean value) {
        this.emptyThumbs = value;
    }
}
