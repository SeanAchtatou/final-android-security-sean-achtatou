package com.ap.sacramento.managers;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ap.sacramento.R;
import com.ap.sacramento.common.DialogWebView;
import com.ap.sacramento.common.Logger;

public class ShareManager {
    private static final int SHARE_EMAIL = 102;
    private static final int SHARE_FACEBOOK = 100;
    private static final int SHARE_SHARETEXT = 103;
    private static final int SHARE_TWITTER = 101;
    private static ShareManager me = new ShareManager();
    /* access modifiers changed from: private */
    public int shareAction = 0;
    String[] shares = {"Facebook", "Twitter", "E-mail", "Text"};

    public static ShareManager getInstance() {
        return me;
    }

    public void simpleShare(String title, String url) {
        Logger.logDebug("Sharing with title:" + title + ", and url:" + url);
        Intent shareIntent = new Intent("android.intent.action.SEND");
        shareIntent.setType("text/plain");
        shareIntent.putExtra("android.intent.extra.SUBJECT", title);
        shareIntent.putExtra("android.intent.extra.TEXT", url);
        ScreenManager.getInstance().getContext().startActivity(Intent.createChooser(shareIntent, "Share using:"));
    }

    public void share(final String title, final String url) {
        AnonymousClass1DialogButtonHandler handler = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int button) {
                if (button == -1) {
                    dialog.dismiss();
                } else if (button == -2) {
                    dialog.dismiss();
                } else {
                    int unused = ShareManager.this.shareAction = button + ShareManager.SHARE_FACEBOOK;
                    switch (ShareManager.this.shareAction) {
                        case ShareManager.SHARE_FACEBOOK /*100*/:
                            ShareManager.this.shareFacebook(title, url);
                            break;
                        case ShareManager.SHARE_TWITTER /*101*/:
                            ShareManager.this.shareTwitter(title, url);
                            break;
                        case ShareManager.SHARE_EMAIL /*102*/:
                            ShareManager.this.shareEmail(title, url);
                            break;
                        case ShareManager.SHARE_SHARETEXT /*103*/:
                            ShareManager.this.shareSms(title, url);
                            break;
                    }
                    dialog.dismiss();
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(ScreenManager.getInstance().getContext());
        builder.setTitle("Share story via");
        builder.setNegativeButton("Cancel", handler);
        builder.setSingleChoiceItems(new ArrayAdapter<String>(ScreenManager.getInstance().getContext(), R.layout.share_row, this.shares) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View share_row = ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.share_row, (ViewGroup) null);
                ((TextView) share_row.findViewById(R.id.textShare)).setText(ShareManager.this.shares[position]);
                ((ImageView) share_row.findViewById(R.id.imgShare)).setVisibility(8);
                return share_row;
            }
        }, -1, handler);
        builder.create().show();
    }

    /* access modifiers changed from: package-private */
    public void shareTwitter(String title, String url) {
        Logger.logDebug("shareTwitter " + title);
        int len = title.length();
        int delta = 140 - url.length();
        if (len > delta) {
            title = title.substring(0, delta - 7) + "...";
        }
        final DialogWebView wv = new DialogWebView(ScreenManager.getInstance().getContext());
        wv.getSettings().setUseWideViewPort(true);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setPluginsEnabled(true);
        wv.getSettings().setSavePassword(true);
        wv.getSettings().setSaveFormData(true);
        AlertDialog.Builder builder = new AlertDialog.Builder(ScreenManager.getInstance().getContext());
        builder.setTitle("Twitter loading ...");
        builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                wv.destroy();
            }
        });
        builder.setView(wv);
        final AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
        wv.setWebViewClient(new WebViewClient() {
            boolean isVisible = false;

            public void onPageFinished(WebView view, String url) {
                Logger.logDebug("onPageFinished " + url);
                if (url.contains("twitter") && !this.isVisible) {
                    dialog.setTitle("Share story via Twitter");
                    this.isVisible = true;
                }
            }

            public boolean shouldOverrideUrlLoading(WebView webView, String url) {
                Logger.logDebug("shouldOverrideUrlLoading " + url);
                return false;
            }
        });
        wv.setWebChromeClient(new WebChromeClient() {
            public void onExceededDatabaseQuota(String url, String databaseIdentifier, long currentQuota, long estimatedSize, long totalUsedQuota, WebStorage.QuotaUpdater quotaUpdater) {
                Logger.logDebug("onExceededDatabaseQuota " + url);
                quotaUpdater.updateQuota(2 * estimatedSize);
            }
        });
        Logger.logDebug("getUserAgentString " + wv.getSettings().getUserAgentString());
        wv.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 2.0; ld-us; sdk Build/ECLAIR) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17");
        wv.loadUrl("http://mobile.twitter.com/home?status=" + (title + " - " + url));
    }

    /* access modifiers changed from: package-private */
    public void shareFacebook(String title, String url) {
        final DialogWebView wv = new DialogWebView(ScreenManager.getInstance().getContext());
        wv.getSettings().setUseWideViewPort(false);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.requestFocus(130);
        AlertDialog.Builder builder = new AlertDialog.Builder(ScreenManager.getInstance().getContext());
        builder.setTitle("Facebook loading ...");
        builder.setView(wv);
        builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                wv.destroy();
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
        wv.setWebViewClient(new WebViewClient() {
            boolean isVisible = false;

            public void onPageFinished(WebView view, String url) {
                if (url.contains("facebook") && !this.isVisible) {
                    dialog.setTitle("Share story via Facebook");
                    this.isVisible = true;
                }
            }
        });
        wv.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 2.0; ld-us; sdk Build/ECLAIR) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17");
        wv.loadUrl("http://m.facebook.com/sharer.php?u=" + url + "&t=" + title);
    }

    /* access modifiers changed from: package-private */
    public void shareEmail(String title, String url) {
        ScreenManager.getInstance().showProgress("Opening ...", "");
        String content = ScreenManager.getInstance().readAsset("storyemail.html");
        if (title != null) {
            content = content.replace("###TITLE###", title);
        }
        if (url != null) {
            content = content.replace("###LINK###", url);
        }
        Intent emailIntent = new Intent("android.intent.action.SEND", Uri.parse("mailto:"));
        emailIntent.setType("text/html");
        emailIntent.putExtra("android.intent.extra.SUBJECT", title);
        emailIntent.putExtra("android.intent.extra.TEXT", Html.fromHtml(content));
        ScreenManager.getInstance().getContext().startActivityForResult(Intent.createChooser(emailIntent, "Share story via E-mail"), 0);
    }

    /* access modifiers changed from: package-private */
    public void shareSms(String title, String url) {
        ScreenManager.getInstance().showProgress("Opening ...", "");
        Intent sendIntent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:"));
        sendIntent.putExtra("sms_body", title + " - " + url);
        ScreenManager.getInstance().getContext().startActivityForResult(Intent.createChooser(sendIntent, "Share story via Text"), 0);
    }

    public void sendEmail(String title, String content, String caption, String to) {
        Logger.logDebug(String.format("sendEmail Subject: %s  Caption: %s Content: %s  To: %s", title, caption, content, to));
        ScreenManager.getInstance().showProgress("Opening ...", "");
        Uri parse = Uri.parse(to);
        Intent emailIntent = new Intent("android.intent.action.SEND");
        emailIntent.setType("text/html");
        if (to.length() > 1) {
            emailIntent.putExtra("android.intent.extra.EMAIL", new String[]{to});
        }
        emailIntent.putExtra("android.intent.extra.SUBJECT", title);
        emailIntent.putExtra("android.intent.extra.TEXT", Html.fromHtml(content));
        ScreenManager.getInstance().getContext().startActivityForResult(Intent.createChooser(emailIntent, caption), 0);
    }

    public void openBrowser(String url) {
        Uri uri = Uri.parse(url);
        ScreenManager.getInstance().showProgress("Opening browser ...", "");
        ScreenManager.getInstance().getContext().startActivityForResult(Intent.createChooser(new Intent("android.intent.action.VIEW", uri), "Advertisement"), 0);
    }
}
