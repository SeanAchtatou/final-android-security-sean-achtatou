package com.ap.sacramento.managers;

import android.app.Application;
import com.vervewireless.capi.ContentItem;
import com.vervewireless.capi.DisplayBlock;
import com.vervewireless.capi.Verve;

public interface PageviewHandler {
    void init(Application application, Verve verve);

    void reportCustomPageview(String str, Integer num, Integer num2);

    void reportPageview(DisplayBlock displayBlock, ContentItem contentItem);

    void reportShare(DisplayBlock displayBlock, ContentItem contentItem, String str);

    void setHierarchy(DisplayBlock displayBlock);
}
