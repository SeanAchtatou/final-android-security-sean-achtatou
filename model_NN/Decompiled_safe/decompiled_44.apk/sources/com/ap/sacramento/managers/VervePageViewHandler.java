package com.ap.sacramento.managers;

import android.app.Application;
import com.ap.sacramento.common.Logger;
import com.vervewireless.capi.ContentItem;
import com.vervewireless.capi.DisplayBlock;
import com.vervewireless.capi.Verve;

public class VervePageViewHandler implements PageviewHandler {
    private Verve api;
    private DisplayBlock root;

    public void init(Application app, Verve api2) {
        this.api = api2;
    }

    public void reportPageview(DisplayBlock displayBlock, ContentItem contentItem) {
        if (contentItem == null) {
            Logger.logDebug("Reporting " + displayBlock.getName() + " page view");
            this.api.reportContentListing(displayBlock);
            return;
        }
        Logger.logDebug("Reporting " + displayBlock.getName() + " - " + contentItem.getTitle() + " page view");
        this.api.reportContentView(displayBlock, contentItem);
    }

    public void reportCustomPageview(String guid, Integer displayBlockId, Integer partnerModuleId) {
        if (Logger.isDebugEnabled()) {
            Logger.logDebug("Reporting page view, guid:" + guid + ", dbId:" + displayBlockId + ", partner:" + partnerModuleId);
        }
        this.api.reportCustomView(guid, Integer.valueOf(displayBlockId == null ? this.root.getId() : displayBlockId.intValue()), partnerModuleId);
    }

    public void setHierarchy(DisplayBlock hierarchy) {
        this.root = hierarchy;
    }

    public void reportShare(DisplayBlock displayBlock, ContentItem contentItem, String shareType) {
    }
}
