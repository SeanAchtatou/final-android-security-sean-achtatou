package com.ap.sacramento.managers;

import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.ap.sacramento.common.AdWebView;
import com.ap.sacramento.common.Logger;
import com.vervewireless.advert.Ad;
import com.vervewireless.advert.AdError;
import com.vervewireless.advert.AdListener;
import com.vervewireless.advert.AdRequest;
import com.vervewireless.advert.AdResponse;
import com.vervewireless.advert.Category;
import com.vervewireless.advert.VerveAdApi;
import com.vervewireless.capi.DisplayBlock;

public class AdManager implements AdListener {
    private Ad currentAd;
    private AdRequest currentAdRequest;
    private DisplayBlock displayBlock;
    private boolean isLoaded = true;
    /* access modifiers changed from: private */
    public AdWebView webAdvert;

    public void setAd(AdWebView web) {
        this.webAdvert = web;
        this.webAdvert.setScrollBarStyle(0);
        WebSettings settings = this.webAdvert.getSettings();
        settings.setUseWideViewPort(false);
        settings.setJavaScriptEnabled(true);
        settings.setCacheMode(2);
        this.webAdvert.requestFocus(130);
        this.webAdvert.setBackgroundColor(0);
        this.webAdvert.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                if (AdManager.this.webAdvert.getVisibility() != 0) {
                    Logger.logDebug("onPageFinished " + url);
                }
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Logger.logDebug("shouldOverrideUrlLoading " + url);
                AdManager.this.openAdUrl(url);
                return true;
            }
        });
    }

    public void setDisplayBlock(DisplayBlock block) {
        this.displayBlock = block;
    }

    public void hide() {
        Logger.logDebug("!! Hidding ad !!");
        this.webAdvert.setVisibility(4);
        this.webAdvert.setVisibility(8);
    }

    public void show() {
        this.webAdvert.setVisibility(0);
    }

    public void start() {
        this.webAdvert.setVisibility(0);
    }

    public void onClicked() {
        if (this.isLoaded) {
            Logger.logDebug("adClicked");
        }
    }

    public void loadAd() {
        Logger.logDebug("!!! Loading new ad !!!");
        VerveAdApi adApi = VerveManager.getInstance().getVerveApi().getAdApi();
        if (this.currentAdRequest != null) {
            adApi.cancelRequest(this.currentAdRequest);
            this.currentAdRequest = null;
        }
        AdRequest request = new AdRequest();
        request.setDisplayBlockId(this.displayBlock.getId());
        request.setCategory(Category.valueOf(this.displayBlock.getAdCategoryId()));
        adApi.getAd(request, this);
        this.currentAdRequest = request;
        if (this.webAdvert != null) {
            this.webAdvert.stopLoading();
        }
    }

    public void onAdError(AdError arg0) {
        this.currentAdRequest = null;
        Logger.logDebug("Ad request failed. Won't show the ad");
    }

    public void onAdLoaded(AdResponse adResponse) {
        this.currentAdRequest = null;
        Logger.logDebug("onAdRecieved");
        if (adResponse.isEmpty()) {
            Logger.logDebug("Empty ad, nothing to show");
            return;
        }
        this.currentAd = adResponse.getAd();
        showAd();
    }

    public void showAd() {
        String html;
        if (this.currentAd.useRawResponse()) {
            html = VerveManager.getInstance().getHTMLRawAd();
            if (this.currentAd.getRawResponse() != null) {
                html = html.replace("###RAW###", this.currentAd.getRawResponse());
            }
            Logger.logDebug("getHTMLRawAd");
        } else if (this.currentAd.getTrackingUrl() != null) {
            String html2 = VerveManager.getInstance().getHTMLTrackingAd();
            if (this.currentAd.getBannerImageUrl() != null) {
                html2 = html2.replace("###PHOTO###", this.currentAd.getBannerImageUrl().toString());
            }
            if (this.currentAd.getAlternateText() != null) {
                html2 = html2.replace("###TEXT###", this.currentAd.getAlternateText());
            }
            if (this.currentAd.getTrackingUrl() != null) {
                html2 = html2.replace("###TRACKING###", this.currentAd.getTrackingUrl().toString());
            }
            Logger.logDebug("getHTMLTrackingAd");
        } else {
            Logger.logDebug("getHTMLNoTrackingAd");
            String html3 = VerveManager.getInstance().getHTMLNoTrackingAd();
            if (this.currentAd.getBannerImageUrl() != null) {
                html3 = html3.replace("###PHOTO###", this.currentAd.getBannerImageUrl().toString());
            }
            if (this.currentAd.getAlternateText() != null) {
                html = html.replace("###TEXT###", this.currentAd.getAlternateText());
            }
        }
        this.webAdvert.loadDataWithBaseURL("file:///android_asset/", html, "text/html", "utf-8", null);
        this.webAdvert.setVisibility(0);
    }

    public void openAdUrl(String url) {
        if (this.currentAd.useRawResponse()) {
            ShareManager.getInstance().openBrowser(url);
        } else {
            ShareManager.getInstance().openBrowser(this.currentAd.getClickthroughUrl().toString());
        }
    }
}
