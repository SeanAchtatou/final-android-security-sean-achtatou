package com.ap.sacramento;

import com.ap.sacramento.common.Logger;
import com.ap.sacramento.managers.PageviewHandler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Confuguration {
    private List<PageviewHandler> pageviewHandlers = new ArrayList();
    private Properties properties = new Properties();

    public Properties getProperties() {
        return this.properties;
    }

    public List<PageviewHandler> getPageviewHandlers() {
        return this.pageviewHandlers;
    }

    /* access modifiers changed from: package-private */
    public void load() throws IOException {
        this.properties.load(Confuguration.class.getResourceAsStream("config.properties"));
        for (Object foo : this.properties.keySet()) {
            String key = foo.toString();
            String value = this.properties.getProperty(key);
            Logger.logDebug("Config[" + key + "] = '" + this.properties.getProperty(key.toString()) + '\'');
            if (key.startsWith("pageviewhandler.")) {
                loadPageviewHandler(value);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void loadPageviewHandler(String className) {
        Logger.logDebug("Loading pageview handler:" + className);
        try {
            PageviewHandler handler = null;
            try {
                handler = (PageviewHandler) Class.forName(className).newInstance();
            } catch (IllegalAccessException e) {
                Logger.logWarning("Cannot create an instance of :" + className, e);
            } catch (InstantiationException e2) {
                Logger.logWarning("Cannot create an instance of :" + className, e2);
            }
            this.pageviewHandlers.add(handler);
        } catch (ClassNotFoundException e3) {
            Logger.logWarning("Failed to load pageview handler with classname:" + className);
        }
    }
}
