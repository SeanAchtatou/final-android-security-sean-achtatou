package com.ap.sacramento.common;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.WebView;

public class CustomWebView extends WebView {
    private BaseScreen parentScreen = null;

    public CustomWebView(Context context) {
        super(context);
    }

    public CustomWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public CustomWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean onTouchEvent(MotionEvent ev) {
        boolean consumed = super.onTouchEvent(ev);
        if (isClickable() && ev.getAction() == 0 && !hasFocus()) {
            requestFocus();
        }
        if (this.parentScreen != null && ev.getAction() == 0) {
            this.parentScreen.onTouch(ev);
        }
        return consumed;
    }

    public void setParent(BaseScreen parent) {
        this.parentScreen = parent;
    }

    public void updateViewLayout(View view, ViewGroup.LayoutParams params) {
        Logger.logDebug("CustomWebView updateViewLayout");
        super.updateViewLayout(view, params);
    }

    public ViewParent invalidateChildInParent(int[] location, Rect dirty) {
        Logger.logDebug("CustomWebView invalidateChildInParent");
        return super.invalidateChildInParent(location, dirty);
    }
}
