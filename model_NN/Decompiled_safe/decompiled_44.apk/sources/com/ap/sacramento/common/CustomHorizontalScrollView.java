package com.ap.sacramento.common;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;

public class CustomHorizontalScrollView extends HorizontalScrollView {
    int childWidth = 0;
    int current = 0;
    private boolean isFirst = false;
    /* access modifiers changed from: private */
    public boolean isScreenChange = false;
    private ImageView left;
    /* access modifiers changed from: private */
    public ImageView right;
    int scrollRange = 0;

    public CustomHorizontalScrollView(Context context) {
        super(context);
    }

    public CustomHorizontalScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public CustomHorizontalScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void init(ImageView left2, ImageView right2) {
        this.left = left2;
        this.right = right2;
    }

    public void update() {
        this.scrollRange = Math.max(0, getChildAt(0).getWidth() - getWidth());
        Logger.logDebug("update " + String.valueOf(this.scrollRange) + " CanScroll " + String.valueOf(canScroll()));
        if (this.left != null) {
            if (!canScroll()) {
                this.left.setVisibility(8);
                this.right.setVisibility(8);
                return;
            }
            showArrow();
        }
    }

    public boolean canScroll() {
        View child = getChildAt(0);
        if (child == null) {
            return false;
        }
        if (getWidth() < child.getWidth()) {
            return true;
        }
        return false;
    }

    public void showArrow() {
        this.left.setVisibility(8);
        this.right.setVisibility(8);
        Logger.logDebug("showArrow current " + String.valueOf(this.current) + " " + String.valueOf(this.scrollRange));
        if (canScroll()) {
            if (this.current == 0) {
                this.right.setVisibility(0);
                this.left.setVisibility(8);
            } else if (this.current > 0 && this.current < this.scrollRange - 10) {
                this.right.setVisibility(0);
                this.left.setVisibility(0);
            } else if (this.current > this.scrollRange - 10) {
                this.right.setVisibility(8);
                this.left.setVisibility(0);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int l, int t, int oldl, int oldt) {
        Logger.logDebug("EdgeLength " + String.valueOf(getLeftFadingEdgeStrength()));
        this.current = l;
        super.onScrollChanged(l, t, oldl, oldt);
        update();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (getChildCount() > 0 && !this.isFirst && this.childWidth < getChildAt(0).getMeasuredWidth()) {
            this.childWidth = getChildAt(0).getMeasuredWidth();
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        Logger.logDebug("onSizeChanged " + String.valueOf(w));
        if (w > 0 && !this.isFirst) {
            this.scrollRange = Math.max(0, this.childWidth - w);
            this.isFirst = true;
            if (w < this.childWidth) {
                post(new Runnable() {
                    public void run() {
                        CustomHorizontalScrollView.this.right.setVisibility(0);
                    }
                });
            }
        } else if (w > 0 && this.isScreenChange) {
            post(new Runnable() {
                public void run() {
                    CustomHorizontalScrollView.this.update();
                    boolean unused = CustomHorizontalScrollView.this.isScreenChange = false;
                }
            });
        }
    }

    public void setScreenChange() {
        this.isScreenChange = true;
    }
}
