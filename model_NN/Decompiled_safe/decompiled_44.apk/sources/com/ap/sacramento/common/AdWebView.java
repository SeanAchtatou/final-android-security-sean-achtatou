package com.ap.sacramento.common;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.webkit.WebView;

public class AdWebView extends WebView {
    public AdWebView(Context context) {
        super(context);
    }

    public AdWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public AdWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean onTouchEvent(MotionEvent ev) {
        Logger.logDebug("AdWebView onTouchEvent");
        if (ev.getAction() == 0) {
            Logger.logDebug("AdWebView onTouchEvent ACTION_DOWN");
            requestFocus();
        }
        return super.onTouchEvent(ev);
    }
}
