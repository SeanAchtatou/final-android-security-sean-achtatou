package com.ap.sacramento.common;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.widget.ImageView;
import com.ap.sacramento.managers.ScreenManager;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ImageDownloader {
    private static final int DELAY_BEFORE_PURGE = 30000;
    private static final int HARD_CACHE_CAPACITY = 10;
    private static final String LOG_TAG = "ImageDownloader";
    /* access modifiers changed from: private */
    public static final ConcurrentHashMap<String, SoftReference<Bitmap>> sSoftBitmapCache = new ConcurrentHashMap<>(5);
    private File cacheDir = ScreenManager.getInstance().getContext().getCacheDir();
    /* access modifiers changed from: private */
    public Mode mode = Mode.NO_ASYNC_TASK;
    private final Handler purgeHandler = new Handler();
    private final Runnable purger = new Runnable() {
        public void run() {
            ImageDownloader.this.clearCache();
        }
    };
    private final HashMap<String, Bitmap> sHardBitmapCache = new LinkedHashMap<String, Bitmap>(5, 0.75f, true) {
        /* access modifiers changed from: protected */
        public boolean removeEldestEntry(Map.Entry<String, Bitmap> eldest) {
            if (size() <= ImageDownloader.HARD_CACHE_CAPACITY) {
                return false;
            }
            ImageDownloader.sSoftBitmapCache.put(eldest.getKey(), new SoftReference(eldest.getValue()));
            return true;
        }
    };

    public enum Mode {
        NO_ASYNC_TASK,
        NO_DOWNLOADED_DRAWABLE,
        CORRECT
    }

    public ImageDownloader() {
        if (!this.cacheDir.exists()) {
            this.cacheDir.mkdirs();
        }
    }

    private Bitmap getBitmapFromStorage(String url) {
        Bitmap bitmap = BitmapFactory.decodeFile(this.cacheDir + "/" + url.substring(url.indexOf("=") + 1, url.indexOf("&")) + ".png");
        if (bitmap != null) {
        }
        return bitmap;
    }

    /* access modifiers changed from: private */
    public void addBitmapToStorage(String url, Bitmap bitmap) {
        try {
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, new FileOutputStream(this.cacheDir + "/" + url.substring(url.indexOf("=") + 1, url.indexOf("&")) + ".png"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void download(String url, ImageView imageView) {
        resetPurgeTimer();
        Bitmap bitmap = getBitmapFromCache(url);
        if (bitmap == null) {
            Bitmap bitmap2 = getBitmapFromStorage(url);
            if (bitmap2 == null) {
                forceDownload(url, imageView);
                return;
            }
            addBitmapToCache(url, bitmap2);
            imageView.setImageBitmap(bitmap2);
            return;
        }
        cancelPotentialDownload(url, imageView);
        imageView.setImageBitmap(bitmap);
    }

    private void forceDownload(String url, ImageView imageView) {
        if (url == null) {
            imageView.setImageDrawable(null);
        } else if (cancelPotentialDownload(url, imageView)) {
            switch (AnonymousClass3.$SwitchMap$com$ap$sacramento$common$ImageDownloader$Mode[this.mode.ordinal()]) {
                case CapiChangeListener.CAPI_STATUS_REREG /*1*/:
                    Bitmap bitmap = downloadBitmap(url);
                    addBitmapToCache(url, bitmap);
                    imageView.setImageBitmap(bitmap);
                    return;
                case CapiChangeListener.CAPI_STATUS_HIER /*2*/:
                    new BitmapDownloaderTask(imageView).execute(url);
                    return;
                case 3:
                    BitmapDownloaderTask task = new BitmapDownloaderTask(imageView);
                    imageView.setImageDrawable(new DownloadedDrawable(task));
                    task.execute(url);
                    return;
                default:
                    return;
            }
        }
    }

    /* renamed from: com.ap.sacramento.common.ImageDownloader$3  reason: invalid class name */
    static /* synthetic */ class AnonymousClass3 {
        static final /* synthetic */ int[] $SwitchMap$com$ap$sacramento$common$ImageDownloader$Mode = new int[Mode.values().length];

        static {
            try {
                $SwitchMap$com$ap$sacramento$common$ImageDownloader$Mode[Mode.NO_ASYNC_TASK.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$ap$sacramento$common$ImageDownloader$Mode[Mode.NO_DOWNLOADED_DRAWABLE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$ap$sacramento$common$ImageDownloader$Mode[Mode.CORRECT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    private static boolean cancelPotentialDownload(String url, ImageView imageView) {
        BitmapDownloaderTask bitmapDownloaderTask = getBitmapDownloaderTask(imageView);
        if (bitmapDownloaderTask != null) {
            String bitmapUrl = bitmapDownloaderTask.url;
            if (bitmapUrl != null && bitmapUrl.equals(url)) {
                return false;
            }
            bitmapDownloaderTask.cancel(true);
        }
        return true;
    }

    /* access modifiers changed from: private */
    public static BitmapDownloaderTask getBitmapDownloaderTask(ImageView imageView) {
        if (imageView != null) {
            Drawable drawable = imageView.getDrawable();
            if (drawable instanceof DownloadedDrawable) {
                return ((DownloadedDrawable) drawable).getBitmapDownloaderTask();
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap downloadBitmap(java.lang.String r11) {
        /*
            r10 = this;
            r9 = 0
            r0 = 4096(0x1000, float:5.74E-42)
            org.apache.http.impl.client.DefaultHttpClient r1 = new org.apache.http.impl.client.DefaultHttpClient
            r1.<init>()
            org.apache.http.client.methods.HttpGet r4 = new org.apache.http.client.methods.HttpGet
            r4.<init>(r11)
            org.apache.http.HttpResponse r6 = r1.execute(r4)     // Catch:{ IOException -> 0x003c, IllegalStateException -> 0x004d, Exception -> 0x0055 }
            org.apache.http.StatusLine r8 = r6.getStatusLine()     // Catch:{ IOException -> 0x003c, IllegalStateException -> 0x004d, Exception -> 0x0055 }
            int r7 = r8.getStatusCode()     // Catch:{ IOException -> 0x003c, IllegalStateException -> 0x004d, Exception -> 0x0055 }
            r8 = 200(0xc8, float:2.8E-43)
            if (r7 == r8) goto L_0x001f
            r8 = r9
        L_0x001e:
            return r8
        L_0x001f:
            org.apache.http.HttpEntity r3 = r6.getEntity()     // Catch:{ IOException -> 0x003c, IllegalStateException -> 0x004d, Exception -> 0x0055 }
            if (r3 == 0) goto L_0x0041
            r5 = 0
            java.io.InputStream r5 = r3.getContent()     // Catch:{ all -> 0x0043 }
            com.ap.sacramento.common.ImageDownloader$FlushedInputStream r8 = new com.ap.sacramento.common.ImageDownloader$FlushedInputStream     // Catch:{ all -> 0x0043 }
            r8.<init>(r5)     // Catch:{ all -> 0x0043 }
            android.graphics.Bitmap r8 = android.graphics.BitmapFactory.decodeStream(r8)     // Catch:{ all -> 0x0043 }
            if (r5 == 0) goto L_0x0038
            r5.close()     // Catch:{ IOException -> 0x003c, IllegalStateException -> 0x004d, Exception -> 0x0055 }
        L_0x0038:
            r3.consumeContent()     // Catch:{ IOException -> 0x003c, IllegalStateException -> 0x004d, Exception -> 0x0055 }
            goto L_0x001e
        L_0x003c:
            r8 = move-exception
            r2 = r8
            r4.abort()     // Catch:{ all -> 0x0053 }
        L_0x0041:
            r8 = r9
            goto L_0x001e
        L_0x0043:
            r8 = move-exception
            if (r5 == 0) goto L_0x0049
            r5.close()     // Catch:{ IOException -> 0x003c, IllegalStateException -> 0x004d, Exception -> 0x0055 }
        L_0x0049:
            r3.consumeContent()     // Catch:{ IOException -> 0x003c, IllegalStateException -> 0x004d, Exception -> 0x0055 }
            throw r8     // Catch:{ IOException -> 0x003c, IllegalStateException -> 0x004d, Exception -> 0x0055 }
        L_0x004d:
            r8 = move-exception
            r2 = r8
            r4.abort()     // Catch:{ all -> 0x0053 }
            goto L_0x0041
        L_0x0053:
            r8 = move-exception
            throw r8
        L_0x0055:
            r8 = move-exception
            r2 = r8
            r4.abort()     // Catch:{ all -> 0x0053 }
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ap.sacramento.common.ImageDownloader.downloadBitmap(java.lang.String):android.graphics.Bitmap");
    }

    static class FlushedInputStream extends FilterInputStream {
        public FlushedInputStream(InputStream inputStream) {
            super(inputStream);
        }

        public long skip(long n) throws IOException {
            long totalBytesSkipped = 0;
            while (totalBytesSkipped < n) {
                long bytesSkipped = this.in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0) {
                    if (read() < 0) {
                        break;
                    }
                    bytesSkipped = 1;
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }

    class BitmapDownloaderTask extends AsyncTask<String, Void, Bitmap> {
        private final WeakReference<ImageView> imageViewReference;
        /* access modifiers changed from: private */
        public String url;

        public BitmapDownloaderTask(ImageView imageView) {
            this.imageViewReference = new WeakReference<>(imageView);
        }

        /* access modifiers changed from: protected */
        public Bitmap doInBackground(String... params) {
            this.url = params[0];
            return ImageDownloader.this.downloadBitmap(this.url);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Bitmap bitmap) {
            if (isCancelled()) {
                bitmap = null;
            }
            ImageDownloader.this.addBitmapToCache(this.url, bitmap);
            ImageDownloader.this.addBitmapToStorage(this.url, bitmap);
            if (this.imageViewReference != null) {
                ImageView imageView = this.imageViewReference.get();
                if (this == ImageDownloader.getBitmapDownloaderTask(imageView) || ImageDownloader.this.mode != Mode.CORRECT) {
                    imageView.setImageBitmap(bitmap);
                }
            }
        }
    }

    static class DownloadedDrawable extends Drawable {
        private final WeakReference<BitmapDownloaderTask> bitmapDownloaderTaskReference;

        public DownloadedDrawable(BitmapDownloaderTask bitmapDownloaderTask) {
            this.bitmapDownloaderTaskReference = new WeakReference<>(bitmapDownloaderTask);
        }

        public BitmapDownloaderTask getBitmapDownloaderTask() {
            return this.bitmapDownloaderTaskReference.get();
        }

        public void draw(Canvas canvas) {
        }

        public int getOpacity() {
            return 0;
        }

        public void setAlpha(int alpha) {
        }

        public void setColorFilter(ColorFilter cf) {
        }
    }

    public void setMode(Mode mode2) {
        this.mode = mode2;
        clearCache();
    }

    /* access modifiers changed from: private */
    public void addBitmapToCache(String url, Bitmap bitmap) {
        if (bitmap != null) {
            synchronized (this.sHardBitmapCache) {
                this.sHardBitmapCache.put(url, bitmap);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0023, code lost:
        if (r1 == null) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
        r0 = r1.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002b, code lost:
        if (r0 == null) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0032, code lost:
        com.ap.sacramento.common.ImageDownloader.sSoftBitmapCache.remove(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0037, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001b, code lost:
        r1 = com.ap.sacramento.common.ImageDownloader.sSoftBitmapCache.get(r5);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.Bitmap getBitmapFromCache(java.lang.String r5) {
        /*
            r4 = this;
            java.util.HashMap<java.lang.String, android.graphics.Bitmap> r2 = r4.sHardBitmapCache
            monitor-enter(r2)
            java.util.HashMap<java.lang.String, android.graphics.Bitmap> r3 = r4.sHardBitmapCache     // Catch:{ all -> 0x002f }
            java.lang.Object r0 = r3.get(r5)     // Catch:{ all -> 0x002f }
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0     // Catch:{ all -> 0x002f }
            if (r0 == 0) goto L_0x001a
            java.util.HashMap<java.lang.String, android.graphics.Bitmap> r3 = r4.sHardBitmapCache     // Catch:{ all -> 0x002f }
            r3.remove(r5)     // Catch:{ all -> 0x002f }
            java.util.HashMap<java.lang.String, android.graphics.Bitmap> r3 = r4.sHardBitmapCache     // Catch:{ all -> 0x002f }
            r3.put(r5, r0)     // Catch:{ all -> 0x002f }
            monitor-exit(r2)     // Catch:{ all -> 0x002f }
            r2 = r0
        L_0x0019:
            return r2
        L_0x001a:
            monitor-exit(r2)     // Catch:{ all -> 0x002f }
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.lang.ref.SoftReference<android.graphics.Bitmap>> r2 = com.ap.sacramento.common.ImageDownloader.sSoftBitmapCache
            java.lang.Object r1 = r2.get(r5)
            java.lang.ref.SoftReference r1 = (java.lang.ref.SoftReference) r1
            if (r1 == 0) goto L_0x0037
            java.lang.Object r0 = r1.get()
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0
            if (r0 == 0) goto L_0x0032
            r2 = r0
            goto L_0x0019
        L_0x002f:
            r3 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x002f }
            throw r3
        L_0x0032:
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.lang.ref.SoftReference<android.graphics.Bitmap>> r2 = com.ap.sacramento.common.ImageDownloader.sSoftBitmapCache
            r2.remove(r5)
        L_0x0037:
            r2 = 0
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ap.sacramento.common.ImageDownloader.getBitmapFromCache(java.lang.String):android.graphics.Bitmap");
    }

    public void clearCache() {
        this.sHardBitmapCache.clear();
        sSoftBitmapCache.clear();
    }

    private void resetPurgeTimer() {
        this.purgeHandler.removeCallbacks(this.purger);
        this.purgeHandler.postDelayed(this.purger, 30000);
    }
}
