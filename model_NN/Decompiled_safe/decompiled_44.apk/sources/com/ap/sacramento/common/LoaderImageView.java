package com.ap.sacramento.common;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.ap.sacramento.managers.ScreenManager;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class LoaderImageView extends ImageView {
    /* access modifiers changed from: private */
    public String imageUrl = null;
    /* access modifiers changed from: private */
    public boolean isGalleryCache = false;

    public LoaderImageView(Context context) {
        super(context);
    }

    public LoaderImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void useGalleryCache() {
        this.isGalleryCache = true;
    }

    public void loadImage(final String url) {
        Drawable cache;
        this.imageUrl = url;
        if (url == null) {
            displayImage(url, null);
            return;
        }
        if (this.isGalleryCache) {
            cache = ScreenManager.getInstance().getFromImageGalleryCache(url);
        } else {
            cache = ScreenManager.getInstance().getFromImageCache(url);
        }
        if (cache != null) {
            displayImage(url, cache);
        } else {
            new Thread() {
                public void run() {
                    Drawable image = null;
                    try {
                        image = Drawable.createFromStream((InputStream) new URL(url).getContent(), "name");
                        if (LoaderImageView.this.isGalleryCache) {
                            ScreenManager.getInstance().addToImageGalleryCache(url, image);
                        } else {
                            ScreenManager.getInstance().addToImageCache(url, image);
                        }
                    } catch (IOException | MalformedURLException e) {
                    }
                    LoaderImageView.this.displayImage(url, image);
                }
            }.start();
        }
    }

    /* access modifiers changed from: private */
    public void displayImage(String url, final Drawable image) {
        ScreenManager.getInstance().getContext().runOnUiThread(new Runnable() {
            public void run() {
                if (LoaderImageView.this.imageUrl != null) {
                    LoaderImageView.this.setImageDrawable(image);
                } else {
                    LoaderImageView.this.setImageDrawable(null);
                }
            }
        });
    }
}
