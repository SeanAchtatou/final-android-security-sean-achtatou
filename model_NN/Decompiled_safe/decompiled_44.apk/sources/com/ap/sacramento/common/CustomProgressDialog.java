package com.ap.sacramento.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.KeyEvent;
import com.ap.sacramento.managers.ScreenManager;

public class CustomProgressDialog extends ProgressDialog {
    public CustomProgressDialog(Context context) {
        super(context);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (ScreenManager.getInstance().IsExternalActivity() || keyCode != 4 || !ScreenManager.getInstance().isValid()) {
            return super.onKeyDown(keyCode, event);
        }
        dismiss();
        ScreenManager.getInstance().back();
        return true;
    }
}
