package com.ap.sacramento.common;

import android.content.Intent;
import android.content.res.Configuration;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ap.sacramento.R;
import com.ap.sacramento.managers.ScreenManager;
import com.ap.sacramento.managers.VerveManager;
import com.vervewireless.capi.ContentItem;
import com.vervewireless.capi.DisplayBlock;
import java.util.Iterator;
import java.util.List;

public class BaseScreen {
    public ViewGroup container;
    public TextView empty = null;
    public ImageView imgHeader;
    public boolean isDisplayed = false;
    public Menu menu = null;
    public boolean showTitle = false;
    public int state = 0;
    public TextView textLeft;
    public TextView textRight;
    public View titlePanel;

    public ViewGroup getView() {
        return this.container;
    }

    public void displayed(boolean isBack) {
        this.isDisplayed = true;
        System.gc();
    }

    public void closed(boolean isHidden) {
        this.isDisplayed = false;
    }

    public void refresh() {
    }

    public void activate() {
        this.isDisplayed = true;
    }

    public void deactivate() {
        this.isDisplayed = false;
    }

    public Menu getOptions(Menu menu2) {
        return menu2;
    }

    public void display() {
    }

    public void onOptionsMenuItem(String title) {
    }

    public void onShareItem(int shareId) {
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    }

    public void onTouch(MotionEvent event) {
    }

    public void showEmptyView() {
        this.empty = (TextView) ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.noitems, (ViewGroup) null);
        this.empty.setVisibility(8);
        this.empty.setGravity(17);
        this.container.addView(this.empty);
    }

    public TextView getEmptyView() {
        this.empty = (TextView) ScreenManager.getInstance().getContext().getLayoutInflater().inflate((int) R.layout.noitems, (ViewGroup) null);
        this.empty.setVisibility(8);
        this.empty.setGravity(17);
        return this.empty;
    }

    public void removeEmptyView() {
        if (this.empty != null) {
            this.container.removeView(this.empty);
            this.empty = null;
        }
    }

    public int getState() {
        return this.state;
    }

    public void onConfigurationChanged(Configuration newConfig) {
    }

    public void setShowTitle(boolean res) {
        this.showTitle = res;
    }

    public void onSearchKey() {
    }

    /* access modifiers changed from: protected */
    public void reportPageView(DisplayBlock block, List<ContentItem> items) {
        boolean composite = false;
        if (items != null && !items.isEmpty()) {
            int partnerID = items.get(0).getPartnerModuleId();
            Iterator i$ = items.iterator();
            while (true) {
                if (i$.hasNext()) {
                    if (partnerID != i$.next().getPartnerModuleId()) {
                        composite = true;
                        break;
                    }
                } else {
                    break;
                }
            }
            Logger.logDebug("They all have " + partnerID + ", " + composite);
        }
        if (composite) {
            Logger.logDebug("Mixed content for " + block);
        }
        VerveManager.getInstance().reportPageView(block, null);
    }

    public void reportPageView() {
    }
}
