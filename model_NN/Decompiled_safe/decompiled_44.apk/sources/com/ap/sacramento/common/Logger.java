package com.ap.sacramento.common;

import android.content.ContentValues;
import android.util.Log;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

public class Logger {
    static final boolean DEBUG = true;
    private static final String VERVEAPI = "Verve";

    private Logger() {
    }

    public static void logDebug(String msg) {
        Log.d(VERVEAPI, msg);
    }

    public static void logDebug(String msg, Throwable exception) {
        Log.d(VERVEAPI, msg, exception);
    }

    public static void logWarning(String msg, Throwable th) {
        Log.w(VERVEAPI, msg, th);
    }

    public static void logWarning(String msg) {
        Log.w(VERVEAPI, msg);
    }

    public static boolean isDebugEnabled() {
        return DEBUG;
    }

    public static InputStream debugDump(String string, InputStream stream) throws IOException {
        logDebug(string);
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder builder = new StringBuilder();
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                builder.append(line).append("\r\n");
                logDebug(line);
            }
            return new ByteArrayInputStream(builder.toString().getBytes());
        } finally {
            stream.close();
        }
    }

    public static void assertTrue(boolean expresion) {
        if (!expresion) {
            throw new IllegalStateException("Assertion failed");
        }
    }

    public static void logDBInsert(String table, ContentValues values) {
        StringBuilder names = new StringBuilder();
        StringBuilder data = new StringBuilder();
        int i = 0;
        for (Map.Entry<String, Object> e : values.valueSet()) {
            int i2 = i + 1;
            if (i != 0) {
                names.append(", ");
                data.append(", ");
            }
            data.append((String) e.getKey()).append("='").append(e.getValue()).append("'");
            i = i2;
        }
        logDebug("INSERT INTO " + table + " " + ((Object) data));
    }

    public static void logDBUpdate(String table, ContentValues values) {
        StringBuilder names = new StringBuilder();
        StringBuilder data = new StringBuilder();
        int i = 0;
        for (Map.Entry<String, Object> e : values.valueSet()) {
            int i2 = i + 1;
            if (i != 0) {
                names.append(", ");
                data.append(", ");
            }
            data.append((String) e.getKey()).append("='").append(e.getValue()).append("'");
            i = i2;
        }
        logDebug("UPDATE INTO " + table + " " + ((Object) data));
    }
}
