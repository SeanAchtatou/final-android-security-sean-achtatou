package com.ap.sacramento.common;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class TransparentPanel extends LinearLayout {
    private Paint borderPaint;
    private Paint innerPaint;

    public TransparentPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TransparentPanel(Context context) {
        super(context);
        init();
    }

    private void init() {
        this.innerPaint = new Paint();
        this.innerPaint.setARGB(160, 0, 0, 0);
        this.innerPaint.setAntiAlias(true);
    }

    public void setInnerPaint(Paint innerPaint2) {
        this.innerPaint = innerPaint2;
    }

    public void setBorderPaint(Paint borderPaint2) {
        this.borderPaint = borderPaint2;
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        RectF drawRect = new RectF();
        drawRect.set(0.0f, 0.0f, (float) getMeasuredWidth(), (float) getMeasuredHeight());
        canvas.drawRoundRect(drawRect, 5.0f, 5.0f, this.innerPaint);
        super.dispatchDraw(canvas);
    }
}
