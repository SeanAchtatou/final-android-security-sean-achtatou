package com.ap.sacramento.common;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

public class DialogWebView extends WebView {
    public DialogWebView(Context context) {
        super(context);
    }

    public DialogWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public DialogWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean onCheckIsTextEditor() {
        return true;
    }
}
