package com.vervewireless.advert;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class AdView extends FrameLayout {
    private VerveAdApi api;
    private boolean autoHide = false;
    /* access modifiers changed from: private */
    public Ad currentAd;
    private AdRequest currentRequest;
    private AdListener listener;
    private WebView webView;

    public AdView(Context context, AttributeSet attributes) {
        super(context, attributes);
        this.webView = new WebView(context);
        initialize();
        if (!isInEditMode()) {
            this.api = new VerveAdApi(context);
            this.api.setBaseAdUrl(attributes.getAttributeValue("com.vervewireless.advert", "base_url"));
        }
    }

    public AdView(Context context, String base_url) {
        super(context);
        this.api = new VerveAdApi(context);
        this.api.setBaseAdUrl(base_url);
        this.webView = new WebView(context);
        initialize();
    }

    public AdView(Context context, VerveAdApi api2) {
        super(context);
        this.webView = new WebView(context);
        this.api = api2;
        initialize();
    }

    private void initialize() {
        addView(this.webView);
        this.webView.setScrollBarStyle(0);
        if (!isInEditMode()) {
            WebSettings settings = this.webView.getSettings();
            settings.setUseWideViewPort(false);
            settings.setJavaScriptEnabled(true);
            settings.setCacheMode(2);
            this.webView.setWebViewClient(new WebViewClient() {
                public void onPageFinished(WebView view, String url) {
                    if (AdView.this.getVisibility() != 0) {
                        Logger.logDebug("onPageFinished " + url);
                    }
                    Logger.logDebug("Juhej");
                }

                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    Logger.logDebug("shouldOverrideUrlLoading " + url);
                    if (AdView.this.currentAd != null) {
                        AdView.this.launchBrowser(url);
                        return true;
                    }
                    Logger.logDebug("currentAd is null");
                    return true;
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void launchBrowser(String url) {
        Intent intent;
        if (this.currentAd.useRawResponse()) {
            Logger.logDebug("currentAd.useRawResponse");
            intent = new Intent("android.intent.action.VIEW", Uri.parse(url));
        } else {
            Logger.logDebug("no currentAd.useRawResponse");
            intent = this.currentAd.getClickthroughUrl() == null ? null : new Intent("android.intent.action.VIEW", Uri.parse(this.currentAd.getClickthroughUrl().toString()));
        }
        if (intent != null) {
            getContext().startActivity(Intent.createChooser(intent, "Advertisement"));
        } else {
            Logger.logDebug("Intent was null");
        }
    }

    public boolean onTouchEvent(MotionEvent ev) {
        Logger.logDebug("AdWebView onTouchEvent");
        if (ev.getAction() == 0) {
            Logger.logDebug("AdWebView onTouchEvent ACTION_DOWN");
            requestFocus();
        }
        return super.onTouchEvent(ev);
    }

    public void setAdListener(AdListener listener2) {
        this.listener = listener2;
    }

    public void setBaseUrl(String base_url) {
        this.api.setBaseAdUrl(base_url);
    }

    public void requestAd(AdRequest adRequest) {
        if (this.api.getBaseAdUrl() == null) {
            throw new IllegalStateException("The base URL needs to be set first");
        }
        if (this.currentRequest != null) {
            this.api.cancelRequest(this.currentRequest);
            this.currentRequest = null;
        }
        this.currentRequest = adRequest;
        this.api.getAd(adRequest, new AdHandler());
    }

    public void loadAd(Ad ad) {
        this.webView.stopLoading();
        showAd(ad);
    }

    public boolean isAutoHide() {
        return this.autoHide;
    }

    public void setAutoHide(boolean autoHide2) {
        this.autoHide = autoHide2;
    }

    private void showAd(Ad ad) {
        this.webView.stopLoading();
        if (ad == null) {
            Logger.logDebug("No ad to display");
            return;
        }
        this.currentAd = ad;
        if (ad.useRawResponse()) {
            showHTMLAd(ad);
        } else if (ad.getTrackingUrl() != null) {
            showAdWithTracking(ad);
        } else {
            showRegularAd(ad);
        }
    }

    private void showHTMLAd(Ad ad) {
        this.webView.loadDataWithBaseURL(this.api.getBaseAdUrl(), loadTemplate("template_raw.html").replace("###RAW###", ad.getRawResponse()), "text/html", "utf-8", null);
    }

    private void showAdWithTracking(Ad ad) {
        this.webView.loadData(loadTemplate("template_tracking_ad.html").replace("###PHOTO###", ad.getBannerImageUrl().toString()).replace("###TEXT###", ad.getBannerImageUrl().toString()).replace("###TRACKING###", ad.getTrackingUrl().toString()), "text/html", "utf-8");
    }

    private void showRegularAd(Ad ad) {
        this.webView.loadData(loadTemplate("template_regular.html").replace("###PHOTO###", ad.getBannerImageUrl().toString()).replace("###TEXT###", ad.getBannerImageUrl().toString()), "text/html", "utf-8");
    }

    /* access modifiers changed from: protected */
    public String loadTemplate(String name) {
        Logger.logDebug("Loading template: " + name);
        InputStream input = AdView.class.getResourceAsStream(name);
        if (input == null) {
            throw new IllegalArgumentException("Cannot find the ad template:" + name);
        }
        StringBuilder ret = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                ret.append(line).append(10);
            }
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                }
            }
            return ret.toString();
        } catch (IOException e2) {
            throw new IllegalStateException(e2);
        } catch (Throwable th) {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e3) {
                }
            }
            throw th;
        }
    }

    private void adRequestFinished() {
        this.currentRequest = null;
    }

    /* access modifiers changed from: private */
    public void adFailed(AdError error) {
        adRequestFinished();
        if (isAutoHide()) {
            setVisibility(8);
        }
        if (this.listener != null) {
            this.listener.onAdError(error);
        }
    }

    /* access modifiers changed from: private */
    public void adLoaded(AdResponse response) {
        adRequestFinished();
        if (isAutoHide()) {
            setVisibility(response.isEmpty() ? 8 : 0);
        }
        if (this.listener != null) {
            this.listener.onAdLoaded(response);
        }
        if (!response.isEmpty()) {
            loadAd(response.getAd());
        }
    }

    private class AdHandler implements AdListener {
        private AdHandler() {
        }

        public void onAdLoaded(AdResponse response) {
            AdView.this.adLoaded(response);
        }

        public void onAdError(AdError error) {
            AdView.this.adFailed(error);
        }
    }
}
