package com.vervewireless.advert;

public class AdError {
    private final Throwable cause;
    private final Error error;

    public enum Error {
        NETWORK_ERROR,
        BAD_RESPONSE,
        INVALID_REQUEST,
        CANCELED
    }

    public AdError(Error error2, Throwable cause2) {
        this.error = error2;
        this.cause = cause2;
    }

    public Throwable getCause() {
        return this.cause;
    }

    public String toString() {
        if (this.error == null) {
            return "Unknown AdCel Error";
        }
        return new StringBuilder().append(this.error.toString()).append(", cause: [").append(this.cause).toString() == null ? "unknown" : this.cause.getMessage() + "]";
    }
}
