package com.vervewireless.advert;

import android.os.AsyncTask;
import com.vervewireless.advert.AdError;
import java.io.IOException;
import java.io.InterruptedIOException;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.xmlpull.v1.XmlPullParserException;

class GetAdsTask extends AsyncTask<AdRequest, Void, AdResponse> {
    private AdResponse adResponse;
    private final String baseUrl;
    private AdError error;
    private HttpClient httpClient;
    private final AdListener listener;
    private final String requestId;

    public GetAdsTask(String baseUrl2, String requestId2, AdListener listener2) {
        this.baseUrl = baseUrl2;
        this.requestId = requestId2;
        this.listener = listener2;
    }

    public HttpClient getHttpClient() {
        return this.httpClient;
    }

    public void setHttpClient(HttpClient httpClient2) {
        this.httpClient = httpClient2;
    }

    /* access modifiers changed from: protected */
    public AdResponse doInBackground(AdRequest... params) {
        try {
            return doRequest(params[0]);
        } catch (IOException ex) {
            this.error = new AdError(AdError.Error.NETWORK_ERROR, ex);
            Logger.logDebug("Failed to retrieve ads", ex);
            return null;
        } catch (XmlPullParserException e) {
            Logger.logDebug("Could not parse the XML", e);
            this.error = new AdError(AdError.Error.BAD_RESPONSE, e);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public AdResponse doRequest(AdRequest request) throws IOException, XmlPullParserException {
        try {
            Logger.logDebug("Requesting ad from " + this.baseUrl);
            HttpResponse response = this.httpClient.execute(request.createRequest(this.baseUrl, this.requestId));
            int code = response.getStatusLine().getStatusCode();
            Header contentType = response.getEntity().getContentType();
            Logger.logDebug("Got ad response with status " + code + ", and type:" + contentType);
            if (code < 200 || code >= 300) {
                Logger.logDebug("Ad request failed with status " + code);
                this.error = new AdError(AdError.Error.INVALID_REQUEST, null);
                return null;
            }
            this.adResponse = new AdResponse();
            if (contentType == null || !"text/plain".equals(contentType.getValue())) {
                this.adResponse.parse(response);
            }
            return this.adResponse;
        } catch (InterruptedIOException e) {
            Logger.logDebug("Ad request was canceled");
            this.error = new AdError(AdError.Error.CANCELED, e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(AdResponse result) {
        super.onPostExecute((Object) result);
        if (this.error != null) {
            Logger.logDebug("Ad request failed: " + this.error, this.error.getCause());
            this.listener.onAdError(this.error);
            return;
        }
        this.listener.onAdLoaded(this.adResponse);
    }
}
