package com.vervewireless.advert;

import android.location.Location;
import android.net.Uri;
import android.os.Build;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;

public class AdRequest {
    private Category contentCategoryId;
    private String deviceIp;
    private String dguid;
    private int displayBlockId;
    private Location location;
    private String partnerKeyword;
    private String partnerModuleId;
    private String portalKeyword;
    private String position = "top";
    private String userAgent;

    public String getPartnerKeyword() {
        return this.partnerKeyword;
    }

    public void setPartnerKeyword(String partnerKeyword2) {
        this.partnerKeyword = partnerKeyword2;
    }

    public Category getCategory() {
        return this.contentCategoryId;
    }

    public void setCategory(Category contentCategoryId2) {
        this.contentCategoryId = contentCategoryId2;
    }

    public int getDisplayBlockId() {
        return this.displayBlockId;
    }

    public void setDisplayBlockId(int displayBlockId2) {
        this.displayBlockId = displayBlockId2;
    }

    public String getPortalKeyword() {
        return this.portalKeyword;
    }

    public void setPortalKeyword(String portalKeyword2) {
        this.portalKeyword = portalKeyword2;
    }

    public String getPartnerModuleId() {
        return this.partnerModuleId;
    }

    public void setPartnerModuleId(String partnerModuleId2) {
        this.partnerModuleId = partnerModuleId2;
    }

    public String getDguid() {
        return this.dguid;
    }

    public void setDguid(String dguid2) {
        this.dguid = dguid2;
    }

    public String getDeviceIp() {
        return this.deviceIp;
    }

    public void setDeviceIp(String deviceIp2) {
        this.deviceIp = deviceIp2;
    }

    public String getPosition() {
        return this.position;
    }

    public void setPosition(String position2) {
        this.position = position2;
    }

    public String getUserAgent() {
        return this.userAgent;
    }

    public void setUserAgent(String userAgent2) {
        this.userAgent = userAgent2;
    }

    /* access modifiers changed from: package-private */
    public HttpUriRequest createRequest(String baseUrl, String requestId) {
        String categoryId;
        String block;
        String ua;
        Uri baseUri = Uri.parse(baseUrl);
        Uri.Builder builder = baseUri.buildUpon();
        Logger.logDebug("Creating a ad request with base url:" + baseUrl);
        if (this.contentCategoryId != null) {
            categoryId = String.valueOf(this.contentCategoryId.getId());
        } else {
            categoryId = null;
        }
        if (this.displayBlockId != 0) {
            block = String.valueOf(this.displayBlockId);
        } else {
            block = null;
        }
        String portal = null;
        if (baseUri.getQueryParameter("p") == null && this.portalKeyword == null) {
            portal = "def";
        }
        appendRequired(baseUri, builder, "b", this.partnerKeyword, "Missing publisher keyword");
        appendRequired(baseUri, builder, "c", categoryId, "Missing content category id");
        appendRequired(baseUri, builder, "p", portal, "Missing portal keyword");
        appendRequired(baseUri, builder, "s", requestId, "Impression request identifier");
        if (block != null) {
            builder.appendQueryParameter("db", block);
        }
        if (this.partnerModuleId != null) {
            builder.appendQueryParameter("pm", this.partnerModuleId);
        }
        if (this.dguid != null) {
            builder.appendQueryParameter("dguid", this.dguid);
        }
        if (this.deviceIp != null) {
            builder.appendQueryParameter("ip", this.deviceIp);
        }
        if (this.location != null) {
            builder.appendQueryParameter("ll", encodeLocation(this.location));
        }
        if (Logger.isDebugEnabled()) {
            Logger.logDebug("Requesting ad with " + builder.toString());
        }
        HttpGet get = new HttpGet(builder.toString());
        if (this.userAgent == null) {
            ua = "Mozilla/5.0 (Linux; U; " + Build.DISPLAY + "; " + Build.DEVICE + "; " + Build.PRODUCT + ") AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1";
        } else {
            ua = this.userAgent;
        }
        get.setHeader("User-Agent", ua);
        return get;
    }

    private void appendRequired(Uri baseUri, Uri.Builder builder, String param, String value, String errMsg) {
        if (value != null && value.length() != 0) {
            builder.appendQueryParameter(param, value);
        } else if (baseUri.getQueryParameter(param) == null) {
            throw new IllegalArgumentException(errMsg);
        }
    }

    public void setLocation(Location location2) {
        this.location = location2;
    }

    public Location getLocation() {
        return this.location;
    }

    public static String encodeLocation(Location location2) {
        return encodeDouble(location2.getLatitude()) + encodeDouble(location2.getLongitude());
    }

    private static String encodeDouble(double value) {
        int tmp = ((int) (100000.0d * value)) << 1;
        if (value < 0.0d) {
            tmp ^= -1;
        }
        StringBuilder foo = new StringBuilder();
        while (tmp >= 32) {
            foo.append(translateChar((char) (((tmp & 31) | 32) + 63)));
            tmp >>= 5;
        }
        foo.append(translateChar((char) (tmp + 63)));
        return foo.toString();
    }

    private static char translateChar(char c) {
        if (c < '?') {
            return c;
        }
        switch (c) {
            case '@':
                return '9';
            case '[':
                return '6';
            case '\\':
                return '3';
            case ']':
                return '7';
            case '^':
                return '4';
            case '`':
                return '8';
            case '{':
                return '0';
            case '|':
                return '2';
            case '}':
                return '1';
            case '~':
                return '5';
            default:
                return c;
        }
    }
}
