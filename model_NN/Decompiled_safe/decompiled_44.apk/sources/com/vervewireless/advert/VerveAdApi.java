package com.vervewireless.advert;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpParams;

public class VerveAdApi {
    private static final long LOCATION_MIN_DISTANCE = 500;
    private static final long LOCATION_MIN_TIME = 54000000;
    private List<RequestPair> activeRequests = new LinkedList();
    private String baseAdUrl;
    private Location currentLocation;
    private HttpClient httpClient;
    private Location location;
    private AtomicLong requestId = new AtomicLong();

    public VerveAdApi(Context context) {
        boolean coarse;
        boolean fine;
        this.requestId.set(System.currentTimeMillis());
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
            coarse = true;
        } else {
            coarse = false;
        }
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
            fine = true;
        } else {
            fine = false;
        }
        Logger.logDebug("ACCESS_COARSE_LOCATION:" + coarse);
        Logger.logDebug("ACCESS_FINE_LOCATION:" + fine);
        if (coarse || fine) {
            LocationListener ll = new LocationUpdateHandler();
            LocationManager locationManager = (LocationManager) context.getSystemService("location");
            if (coarse) {
                locationManager.requestLocationUpdates("network", (long) LOCATION_MIN_TIME, 500.0f, ll);
                setCurrentLocation(locationManager.getLastKnownLocation("network"));
                Logger.logDebug("Listening for network location updates");
            }
            if (fine) {
                locationManager.requestLocationUpdates("gps", (long) LOCATION_MIN_TIME, 500.0f, ll);
                setCurrentLocation(locationManager.getLastKnownLocation("gps"));
                Logger.logDebug("Listening for gps location updates");
            }
        }
    }

    /* access modifiers changed from: private */
    public void setCurrentLocation(Location location2) {
        if (location2 != null) {
            Location newLocation = null;
            if (this.currentLocation == null) {
                newLocation = location2;
            } else {
                try {
                    if ("gps".equals(location2.getProvider())) {
                        newLocation = location2;
                        Logger.logDebug("Using GPS");
                    } else if (this.currentLocation.hasAccuracy() && location2.hasAccuracy() && this.currentLocation.getAccuracy() > location2.getAccuracy()) {
                        newLocation = location2;
                        Logger.logDebug("Better accurancy:" + this.currentLocation.getAccuracy() + " > " + location2.getAccuracy());
                    } else if (this.currentLocation.distanceTo(location2) > 500.0f) {
                        newLocation = location2;
                        Logger.logDebug("Distance threshold:" + this.currentLocation.distanceTo(location2));
                    } else if (location2.getTime() - this.currentLocation.getTime() > LOCATION_MIN_TIME) {
                        newLocation = location2;
                        Logger.logDebug("Using newer location");
                    }
                } catch (NullPointerException e) {
                }
            }
            if (newLocation != null) {
                Logger.logDebug("Changed current location from:" + this.currentLocation + " to " + newLocation);
                this.currentLocation = newLocation;
            }
        }
    }

    public String getBaseAdUrl() {
        return this.baseAdUrl;
    }

    public void setBaseAdUrl(String baseAdUrl2) {
        this.baseAdUrl = baseAdUrl2;
    }

    public void getAd(AdRequest request, AdListener listener) {
        checkHttpClient();
        if (request.getLocation() == null) {
            request.setLocation(this.location == null ? this.currentLocation : this.location);
        }
        GetAdsTask task = new GetAdsTask(this.baseAdUrl, String.valueOf(this.requestId.incrementAndGet()), new AdListenerWrapper(request, listener));
        task.setHttpClient(this.httpClient);
        this.activeRequests.add(new RequestPair(request, task));
        task.execute(request);
    }

    /* access modifiers changed from: package-private */
    public void checkHttpClient() {
        if (this.httpClient == null) {
            DefaultHttpClient client = new DefaultHttpClient();
            ClientConnectionManager mgr = client.getConnectionManager();
            HttpParams params = client.getParams();
            this.httpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(params, mgr.getSchemeRegistry()), params);
        }
    }

    public void setLocation(Location location2) {
        this.location = location2;
    }

    public HttpClient getHttpClient() {
        return this.httpClient;
    }

    public void setHttpClient(HttpClient httpClient2) {
        this.httpClient = httpClient2;
    }

    public void cancelRequest(AdRequest request) {
        boolean canceled = false;
        Iterator i$ = this.activeRequests.iterator();
        while (true) {
            if (!i$.hasNext()) {
                break;
            }
            RequestPair p = i$.next();
            if (p.request == request) {
                this.activeRequests.remove(p);
                p.task.cancel(true);
                canceled = true;
                Logger.logDebug("Ad request canceled");
                break;
            }
        }
        if (!canceled) {
            Logger.logWarning("Could not cancel ad request. No such request in progress");
        }
    }

    /* access modifiers changed from: private */
    public void adRequestFinished(AdRequest request) {
        for (RequestPair p : this.activeRequests) {
            if (p.request == request) {
                this.activeRequests.remove(p);
                return;
            }
        }
    }

    private static class RequestPair {
        public AdRequest request;
        public GetAdsTask task;

        public RequestPair(AdRequest request2, GetAdsTask task2) {
            this.request = request2;
            this.task = task2;
        }
    }

    private class AdListenerWrapper implements AdListener {
        private final AdListener listener;
        private AdRequest request;

        public AdListenerWrapper(AdRequest request2, AdListener listener2) {
            this.request = request2;
            this.listener = listener2;
        }

        public void onAdLoaded(AdResponse response) {
            VerveAdApi.this.adRequestFinished(this.request);
            this.listener.onAdLoaded(response);
        }

        public void onAdError(AdError error) {
            VerveAdApi.this.adRequestFinished(this.request);
            this.listener.onAdError(error);
        }
    }

    private class LocationUpdateHandler implements LocationListener {
        private LocationUpdateHandler() {
        }

        public void onLocationChanged(Location location) {
            Logger.logDebug("Location changed to " + location);
            VerveAdApi.this.setCurrentLocation(location);
        }

        public void onProviderDisabled(String provider) {
            Logger.logDebug("Provider disabled, " + provider);
        }

        public void onProviderEnabled(String provider) {
            Logger.logDebug("Provider enabled, " + provider);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            Logger.logDebug("Status changed on " + provider + ", new status is :" + status);
        }
    }
}
