package com.vervewireless.advert;

public interface AdListener {
    void onAdError(AdError adError);

    void onAdLoaded(AdResponse adResponse);
}
