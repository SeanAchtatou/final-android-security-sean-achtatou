package com.vervewireless.advert;

import android.util.Xml;
import java.io.IOException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class AdResponse {
    private Ad ad;

    /* access modifiers changed from: package-private */
    public void parse(HttpResponse response) throws IOException, XmlPullParserException {
        HttpEntity entity = response.getEntity();
        long contentLength = entity.getContentLength();
        if (contentLength == 0) {
            Logger.logDebug("Got empty ad response");
            return;
        }
        XmlPullParser parser = Xml.newPullParser();
        parser.setInput(Logger.debugDump("Parsing ad response win len:" + contentLength, entity.getContent()), null);
        int e = parser.next();
        while (true) {
            if (e != 2 || !"ad".equals(parser.getName())) {
                e = parser.next();
            } else {
                Ad ad2 = new Ad();
                ad2.parse(parser);
                setAd(ad2);
                return;
            }
        }
    }

    private void setAd(Ad ad2) {
        this.ad = ad2;
    }

    public Ad getAd() {
        return this.ad;
    }

    public boolean isEmpty() {
        return this.ad == null;
    }
}
