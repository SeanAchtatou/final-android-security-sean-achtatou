package com.vervewireless.advert;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class Ad {
    private String alternateText;
    private URL bannerImageUrl;
    private URL clickthroughUrl;
    private String leadInText;
    private String rawResponse;
    private URL trackingUrl;
    private boolean useRawResponse;

    public URL getBannerImageUrl() {
        return this.bannerImageUrl;
    }

    public void setBannerImageUrl(URL bannerImageUrl2) {
        this.bannerImageUrl = bannerImageUrl2;
    }

    public URL getClickthroughUrl() {
        return this.clickthroughUrl;
    }

    public void setClickthroughUrl(URL clickthroughUrl2) {
        this.clickthroughUrl = clickthroughUrl2;
    }

    public String getLeadInText() {
        return this.leadInText;
    }

    public void setLeadInText(String leadInText2) {
        this.leadInText = leadInText2;
    }

    public String getAlternateText() {
        return this.alternateText;
    }

    public void setAlternateText(String alternateText2) {
        this.alternateText = alternateText2;
    }

    public URL getTrackingUrl() {
        return this.trackingUrl;
    }

    public void setTrackingUrl(URL trackingUrl2) {
        this.trackingUrl = trackingUrl2;
    }

    public String getRawResponse() {
        return this.rawResponse;
    }

    public void setRawResponse(String rawResponse2) {
        this.rawResponse = rawResponse2;
    }

    public boolean useRawResponse() {
        return this.useRawResponse;
    }

    public void setUseRawResponse(boolean useRawResponse2) {
        this.useRawResponse = useRawResponse2;
    }

    private URL parse(String url) {
        if (url != null) {
            try {
                if (url.length() != 0) {
                    return new URL(url);
                }
            } catch (MalformedURLException e) {
                Logger.logDebug("Malformed URL", e);
                return null;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void parse(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(2, null, "ad");
        int e = parser.next();
        while (true) {
            if (e != 3 || !parser.getName().equals("ad")) {
                if (e == 2) {
                    String tag = parser.getName();
                    if ("image_url".equals(tag)) {
                        setBannerImageUrl(parse(parser.nextText()));
                    } else if ("image_alt".equals(tag)) {
                        setAlternateText(parser.nextText());
                    } else if ("tracking_image_url".equals(tag)) {
                        setTrackingUrl(parse(parser.nextText()));
                    } else if ("leadin".equals(tag)) {
                        setLeadInText(parser.nextText());
                    } else if ("url".equals(tag)) {
                        setClickthroughUrl(parse(parser.nextText()));
                    } else if ("response".equals(tag)) {
                        setRawResponse(parser.nextText());
                    } else if ("useRawResponse".equals(tag)) {
                        setUseRawResponse(Boolean.valueOf(parser.nextText()).booleanValue());
                    }
                }
                e = parser.next();
            } else {
                return;
            }
        }
    }
}
