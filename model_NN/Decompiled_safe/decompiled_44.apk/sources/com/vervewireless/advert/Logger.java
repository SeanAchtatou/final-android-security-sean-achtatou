package com.vervewireless.advert;

import android.content.ContentValues;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;

class Logger {
    private static final String AD_CELL = "adcell";
    static final boolean DEBUG = false;

    private Logger() {
    }

    public static void logDebug(String msg) {
    }

    public static void logDebug(String msg, Throwable exception) {
    }

    public static void logWarning(String msg, Throwable th) {
        Log.w(AD_CELL, msg, th);
    }

    public static void logWarning(String msg) {
        Log.w(AD_CELL, msg);
    }

    public static boolean isDebugEnabled() {
        return false;
    }

    public static InputStream debugDump(String string, InputStream stream) throws IOException {
        return stream;
    }

    public static void assertTrue(boolean expresion) {
    }

    public static void logDBInsert(String table, ContentValues values) {
    }

    public static void logDBUpdate(String table, ContentValues values) {
    }
}
