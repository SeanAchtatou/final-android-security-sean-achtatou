package com.vervewireless.capi;

interface CapiChangeListener {
    public static final int CAPI_STATUS_DISABLE = 16;
    public static final int CAPI_STATUS_HIER = 2;
    public static final int CAPI_STATUS_RECALL = 8;
    public static final int CAPI_STATUS_REREG = 1;
    public static final int CAPI_STATUS_UPG = 4;

    void notifyCapiStatus(int i);
}
