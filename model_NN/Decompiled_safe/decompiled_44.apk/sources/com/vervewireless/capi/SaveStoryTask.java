package com.vervewireless.capi;

class SaveStoryTask extends AbstractVerveTask<Boolean> {
    private final ContentItem item;
    private final boolean save;

    public SaveStoryTask(ContentItem item2) {
        this(item2, true);
    }

    public SaveStoryTask(ContentItem item2, boolean save2) {
        this.save = save2;
        this.item = item2;
    }

    public Boolean call() throws Exception {
        DisplayBlock saveBlock = new DisplayBlock();
        saveBlock.setId(8951);
        ContentModel model = getContentModel();
        if (this.save) {
            model.addContentItem(saveBlock, this.item, -1);
        } else {
            model.removeContentItem(saveBlock, this.item);
        }
        return true;
    }

    public void finishedSuccessfully(Boolean result) {
        Logger.logDebug("Story saved successfully");
    }

    public void failed(Exception cause) {
        Logger.logWarning("Failed to save the story", cause);
    }
}
