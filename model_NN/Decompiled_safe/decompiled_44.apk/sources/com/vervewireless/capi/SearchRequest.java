package com.vervewireless.capi;

public class SearchRequest {
    private final DisplayBlock displayBlock;
    private String query;
    private boolean searchOffline;

    public SearchRequest(DisplayBlock displayBlock2, String query2) {
        this.displayBlock = displayBlock2;
        this.query = query2;
    }

    public String getQuery() {
        return this.query;
    }

    public DisplayBlock getDisplayBlock() {
        return this.displayBlock;
    }

    public boolean isSearchOffline() {
        return this.searchOffline;
    }

    public void setSearchOffline(boolean value) {
        this.searchOffline = value;
    }
}
