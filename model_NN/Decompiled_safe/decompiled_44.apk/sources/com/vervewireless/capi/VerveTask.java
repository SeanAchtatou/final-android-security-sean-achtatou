package com.vervewireless.capi;

import java.util.concurrent.Callable;

public interface VerveTask<T> extends Callable<T> {
    void failed(Exception exc);

    void finishedSuccessfully(Object obj);
}
