package com.vervewireless.capi;

import android.content.ContentValues;
import java.io.IOException;
import java.util.Date;
import org.xmlpull.v1.XmlSerializer;

class PageView {
    private static final String CONTENT_GUID = "contentGuid";
    private static final String DISPLAY_BLOCK_ID = "displayBlockId";
    private static final String PARTNER_MODULE_ID = "partnerModuleId";
    private String contentGuid;
    private Integer displayBlockId;
    private Integer partnerModuleId;
    private Date time;

    public PageView(String contentGuid2, Integer displayBlockId2, Integer partnerModuleId2) {
        this.contentGuid = contentGuid2;
        this.partnerModuleId = partnerModuleId2;
        this.displayBlockId = displayBlockId2;
        this.time = new Date();
    }

    public PageView() {
        this(null, null, null);
    }

    public void setTime(Date time2) {
        this.time = time2;
    }

    public Date getTime() {
        return this.time;
    }

    public void save(XmlSerializer serializer) throws IOException {
        serializer.startTag(VerveUtils.PAGE_VIEW_HISTORY_NS, "pageview");
        serializer.attribute(null, "cbmt_id", toString());
        serializer.endTag(VerveUtils.PAGE_VIEW_HISTORY_NS, "pageview");
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.contentGuid == null ? "" : this.contentGuid.replaceAll("&", "&amp"));
        builder.append(',');
        builder.append(this.displayBlockId == null ? "" : this.displayBlockId);
        builder.append(',');
        builder.append(this.partnerModuleId == null ? "" : this.partnerModuleId);
        builder.append(',');
        builder.append(VerveUtils.toISO(this.time));
        return builder.toString();
    }

    public void save(ContentValues values) {
        values.put(CONTENT_GUID, this.contentGuid);
        values.put(DISPLAY_BLOCK_ID, this.displayBlockId);
        values.put(PARTNER_MODULE_ID, this.partnerModuleId);
        values.put("time", VerveUtils.toISO(this.time));
    }

    public void load(ValueSource source) {
        this.contentGuid = source.getValue(CONTENT_GUID, null);
        int dbId = source.getInt(DISPLAY_BLOCK_ID, -1);
        this.displayBlockId = dbId == -1 ? null : Integer.valueOf(dbId);
        int pmId = source.getInt(PARTNER_MODULE_ID, -1);
        this.partnerModuleId = pmId == -1 ? null : Integer.valueOf(pmId);
        this.time = VerveUtils.fromIso(source.getValue("time", null));
    }
}
