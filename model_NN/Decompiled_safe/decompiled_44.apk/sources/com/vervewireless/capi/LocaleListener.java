package com.vervewireless.capi;

import java.util.List;

public interface LocaleListener {
    void onLocalesFailed(VerveError verveError);

    void onLocalesRecieved(List<Locale> list);
}
