package com.vervewireless.capi;

import android.content.SharedPreferences;
import android.os.Build;
import android.util.Xml;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.xmlpull.v1.XmlSerializer;

class RegistrationTask extends AbstractVerveTask<RegistrationInfo> {
    static final /* synthetic */ boolean $assertionsDisabled = (!RegistrationTask.class.desiredAssertionStatus() ? true : $assertionsDisabled);
    public static final String NEEDS_REGISTRATION = "registrationNeeded";
    private static final String REGISTER_URL = "https://clientreg.vervewireless.com/register";
    private String deviceId;
    /* access modifiers changed from: private */
    public RegistrationListener listener;
    private final Locale locale;
    private AtomicBoolean needsLocale = new AtomicBoolean($assertionsDisabled);
    /* access modifiers changed from: private */
    public RegistrationInfo registrationInfo;

    public RegistrationTask(Locale locale2, String deviceId2, RegistrationListener listener2) {
        this.locale = locale2;
        this.deviceId = deviceId2;
        this.listener = listener2;
    }

    static void setReregistrationRequired(SharedPreferences prefs, boolean value) {
        SharedPreferences.Editor editor = prefs.edit();
        if (!value) {
            editor.remove(NEEDS_REGISTRATION);
        } else {
            editor.putBoolean(NEEDS_REGISTRATION, true);
        }
        editor.commit();
    }

    static void clearActiveLocale(SharedPreferences preferences) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove("locale-key");
        editor.remove("locale-name");
        editor.remove("locale-iconUrl");
        editor.remove("locale-displayOrder");
        editor.commit();
    }

    private boolean needsRegistration() {
        boolean versionChanged;
        Locale activeLocale = getApi().getLocale();
        if (activeLocale == null) {
            Logger.logDebug("Not yet registered");
            return true;
        } else if (this.locale == null || activeLocale.equals(this.locale)) {
            SharedPreferences prefs = getPreferences();
            if (!prefs.getString("id", "none").equals(this.appInfo.getApplicationId())) {
                clearActiveLocale(getPreferences());
                Logger.logDebug("Application id ");
                return true;
            } else if (prefs.getBoolean(NEEDS_REGISTRATION, $assertionsDisabled)) {
                Logger.logDebug("Forced reregistration");
                return true;
            } else {
                String activeDeviceId = prefs.getString("uid", "");
                if (activeDeviceId.length() == 0 || !activeDeviceId.equals(this.deviceId)) {
                    Logger.logDebug("Device id changed");
                    return true;
                }
                String activeVersion = prefs.getString("version", "");
                if (!activeVersion.equals(getAppInfo().getApplicationVersion())) {
                    versionChanged = true;
                } else {
                    versionChanged = false;
                }
                if (activeVersion.length() != 0 && !versionChanged) {
                    return $assertionsDisabled;
                }
                Logger.logDebug("Active version changed");
                return true;
            }
        } else {
            Logger.logDebug("Changing locale from " + activeLocale + " to " + this.locale);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void setupRegistrationInfo(RegistrationInfo info) {
    }

    public RegistrationInfo call() throws Exception {
        RegistrationInfo ri = RegistrationInfo.load(getPreferences());
        if (ri == null || needsRegistration()) {
            Locale useLocale = this.locale == null ? getApi().getLocale() : this.locale;
            if (useLocale == null) {
                this.needsLocale.set(true);
                return null;
            }
            useLocale.save(getPreferences());
            SharedPreferences.Editor editor = getPreferences().edit();
            editor.putString("uid", this.deviceId);
            editor.putString("version", this.appInfo.getApplicationVersion());
            editor.putString("id", this.appInfo.getApplicationId());
            editor.commit();
            return doRequest(getAppInfo(), useLocale, ri);
        }
        ri.setPreferences(getContentModel().getPreferences());
        Logger.logDebug("No registration required");
        return ri;
    }

    /* access modifiers changed from: protected */
    public RegistrationInfo doRequest(ApiInfo appInfo, Locale useLocale, RegistrationInfo oldRegistration) throws IOException, VerveRegistrationError {
        HttpPost request = new HttpPost(REGISTER_URL);
        String reqData = createRegistration(appInfo, useLocale, oldRegistration);
        Logger.logDebug("Registering with server:" + reqData);
        request.setEntity(new StringEntity(reqData));
        request.setHeader("Content-Type", "application/xml");
        HttpResponse response = this.httpClient.execute(request);
        Logger.logDebug("Got registration response");
        RegistrationInfo.clear(getPreferences());
        RegistrationInfo ret = new RegistrationInfo();
        ret.parseXml(Logger.debugDump("Registration response:", getStream(response)));
        setReregistrationRequired(getPreferences(), $assertionsDisabled);
        getContentModel().setPreferences(ret.getPreferences());
        return ret;
    }

    public String createRegistration(ApiInfo app, Locale locale2, RegistrationInfo oldRegistration) throws IOException {
        StringWriter writer = new StringWriter();
        XmlSerializer xml = Xml.newSerializer();
        xml.setOutput(writer);
        xml.startTag("", "registerreq").attribute("", "version", "1.0");
        xml.startTag("", "mobileapp");
        xml.attribute("", "id", app.getApplicationId());
        xml.attribute("", "name", app.getApplicationName());
        xml.attribute("", "version", app.getApplicationVersion());
        xml.endTag("", "mobileapp");
        xml.startTag("", "mobiledevice");
        xml.attribute("", "uniqueid", this.deviceId);
        xml.attribute("", "vendor", Build.MANUFACTURER);
        xml.attribute("", "model", Build.MODEL);
        xml.attribute("", "firmware", Build.VERSION.RELEASE);
        xml.endTag("", "mobiledevice");
        xml.startTag("", "mobilelocale").attribute("", "locale", locale2.getKey()).endTag("", "mobilelocale");
        if (oldRegistration != null && VerveUtils.isValid(oldRegistration.getApiId()) && VerveUtils.isValid(oldRegistration.getAuthToken())) {
            Logger.logDebug("Re-registration of for " + oldRegistration.getApiId() + ", and token:" + oldRegistration.getAuthToken());
            xml.startTag("", "apiauth").attribute("", "id", oldRegistration.getApiId()).attribute("", "token", oldRegistration.getAuthToken()).endTag("", "apiauth");
        }
        xml.endDocument();
        return writer.toString();
    }

    public void failed(Exception cause) {
        Logger.logDebug("Registration failed.", cause);
        this.listener.onRegistrationFailed(VerveError.createFromException(cause));
    }

    public void finishedSuccessfully(RegistrationInfo result) {
        if (this.needsLocale.get()) {
            this.api.getLocales(new LocaleHandler());
            return;
        }
        setupRegistrationInfo(result);
        if ($assertionsDisabled || result != null) {
            this.registrationInfo = result;
            getApi().getContetHierarchy(new HierarchyHandler());
            return;
        }
        throw new AssertionError();
    }

    private class LocaleHandler implements LocaleListener {
        private LocaleHandler() {
        }

        public void onLocalesRecieved(List<Locale> locales) {
            RegistrationTask.this.listener.onRegistrationLocaleNeeded(locales);
        }

        public void onLocalesFailed(VerveError error) {
            RegistrationTask.this.listener.onRegistrationFailed(error);
        }
    }

    private class HierarchyHandler implements HierarchyListener {
        private HierarchyHandler() {
        }

        public void onHierarchyFailed(VerveError verveError) {
            RegistrationTask.this.listener.onRegistrationFailed(verveError);
            RegistrationTask.clearActiveLocale(RegistrationTask.this.getPreferences());
        }

        public void onHierarchyRecieved(DisplayBlock result) {
            RegistrationTask.this.registrationInfo.save(RegistrationTask.this.getPreferences());
            RegistrationTask.this.listener.onRegistrationFinished();
        }
    }
}
