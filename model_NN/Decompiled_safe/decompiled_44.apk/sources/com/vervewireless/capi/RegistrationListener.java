package com.vervewireless.capi;

import java.util.List;

public interface RegistrationListener {
    void onRegistrationFailed(VerveError verveError);

    void onRegistrationFinished();

    void onRegistrationLocaleNeeded(List<Locale> list);
}
