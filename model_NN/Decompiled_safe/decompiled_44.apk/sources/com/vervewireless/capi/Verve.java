package com.vervewireless.capi;

import android.location.Location;
import com.vervewireless.advert.VerveAdApi;

public interface Verve {
    void close();

    void flushPageviews();

    VerveAdApi getAdApi();

    ApiInfo getApiInfo();

    void getContent(ContentRequest contentRequest, ContentListener contentListener);

    void getContetHierarchy(HierarchyListener hierarchyListener);

    Locale getLocale();

    void getLocales(LocaleListener localeListener);

    UserPreferences getUserPreferences();

    boolean isRegistered();

    void registerWithVerve(Locale locale, RegistrationListener registrationListener);

    void registerWithVerve(RegistrationListener registrationListener);

    void reportContentListing(DisplayBlock displayBlock);

    void reportContentView(DisplayBlock displayBlock, ContentItem contentItem);

    void reportCustomView(String str, Integer num, Integer num2);

    void reportMediaView(ContentItem contentItem, MediaItem mediaItem);

    void reset();

    void save(ContentItem contentItem);

    void search(SearchRequest searchRequest, SearchListener searchListener);

    void setCapiStatusListener(CapiStatusListener capiStatusListener);

    void setLocation(Location location);

    void setPostCode(String str);

    void setPrefrences(UserPreferences userPreferences);

    void unsave(ContentItem contentItem);
}
