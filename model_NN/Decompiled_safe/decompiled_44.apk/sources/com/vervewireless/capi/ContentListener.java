package com.vervewireless.capi;

public interface ContentListener {
    void onContentFailed(VerveError verveError);

    void onContentRecieved(ContentResponse contentResponse);
}
