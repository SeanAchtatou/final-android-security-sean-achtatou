package com.vervewireless.capi;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Xml;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.xmlpull.v1.XmlPullParser;

public class GetContentTask extends AbstractVerveTask<ContentResponse> {
    private static final int CACHE_TIMEOUT = 60000;
    private String baseUrl;
    private final ContentListener listener;
    private final ContentRequest request;

    public /* bridge */ /* synthetic */ Verve getApi() {
        return super.getApi();
    }

    public /* bridge */ /* synthetic */ ApiInfo getAppInfo() {
        return super.getAppInfo();
    }

    public /* bridge */ /* synthetic */ ContentModel getContentModel() {
        return super.getContentModel();
    }

    public /* bridge */ /* synthetic */ SharedPreferences getPreferences() {
        return super.getPreferences();
    }

    public /* bridge */ /* synthetic */ void setApi(Verve x0) {
        super.setApi(x0);
    }

    public /* bridge */ /* synthetic */ void setAppInfo(ApiInfo x0) {
        super.setAppInfo(x0);
    }

    public /* bridge */ /* synthetic */ void setCapiChangeListener(CapiChangeListener x0) {
        super.setCapiChangeListener(x0);
    }

    public /* bridge */ /* synthetic */ void setContentModel(ContentModel x0) {
        super.setContentModel(x0);
    }

    public /* bridge */ /* synthetic */ void setContext(Context x0) {
        super.setContext(x0);
    }

    public /* bridge */ /* synthetic */ void setHttpClient(HttpClient x0) {
        super.setHttpClient(x0);
    }

    public /* bridge */ /* synthetic */ void setPreferences(SharedPreferences x0) {
        super.setPreferences(x0);
    }

    public GetContentTask(ContentListener listener2, String baseUrl2, ContentRequest request2) {
        this.listener = listener2;
        this.request = request2;
        this.baseUrl = baseUrl2;
    }

    public void finishedSuccessfully(ContentResponse result) {
        this.listener.onContentRecieved(result);
    }

    public void failed(Exception cause) {
        this.listener.onContentFailed(VerveError.createFromException(cause));
    }

    public ContentResponse call() throws Exception {
        ContentModel model = getContentModel();
        DisplayBlock block = this.request.getDisplayBlock();
        Locale locale = this.api.getLocale();
        if (block.getId() == 8951) {
            return model.getContentItems(getApi().getLocale(), block);
        }
        long now = System.currentTimeMillis();
        Date lastUpdate = model.getLastContentRequest(locale, block);
        if (lastUpdate == null || now - lastUpdate.getTime() >= 60000) {
            Uri.Builder urlBuilder = Uri.parse(this.baseUrl).buildUpon();
            urlBuilder.appendEncodedPath("contentlist/");
            urlBuilder.appendPath("db_" + block.getId());
            try {
                HttpResponse response = getRequestDisptacher().doGet(urlBuilder.toString());
                StatusLine status = response.getStatusLine();
                if (status.getStatusCode() != 200) {
                    Logger.debugDump("Failed", getStream(response));
                    throw new IOException("Request failed with " + status.getStatusCode());
                }
                InputStream stream = Logger.debugDump("Content response for " + block.getId() + ":", getStream(response));
                XmlPullParser parser = Xml.newPullParser();
                parser.setInput(stream, null);
                VerveUtils.skipToTag(parser, "channel");
                Channel channel = new Channel();
                channel.parse(parser);
                ContentResponse update = model.updateContentItems(getApi().getLocale(), block, channel.getItems());
                reportApiStatus(channel.getApiStatus());
                return update;
            } catch (IOException e) {
                IOException ex = e;
                Logger.logDebug("Failed to request data from " + urlBuilder.toString(), ex);
                if (this.request.isAllowOffline()) {
                    Logger.logDebug("Requesting data from cache");
                    ContentResponse cached = model.getContentItems(locale, block);
                    if (cached.getItemCount() == 0) {
                        Logger.logDebug("No data in cache for:" + urlBuilder.toString());
                        throw ex;
                    }
                    Logger.logDebug("Returning cached data:" + urlBuilder.toString());
                    return cached;
                }
                throw ex;
            }
        } else {
            Logger.logDebug("Cache not yet expired, ttl:" + (now - lastUpdate.getTime()) + " ms");
            return model.getContentItems(locale, block);
        }
    }
}
