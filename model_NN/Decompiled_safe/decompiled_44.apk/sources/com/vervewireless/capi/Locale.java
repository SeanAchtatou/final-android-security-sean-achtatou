package com.vervewireless.capi;

import android.content.SharedPreferences;

public class Locale {
    private int displayOrder;
    private String iconUrl;
    private String key;
    private String name;

    public Locale() {
    }

    public Locale(String key2, String name2, int displayOrder2, String iconUrl2) {
        if (key2 == null) {
            throw new IllegalArgumentException("Key must be non-null");
        }
        this.name = name2;
        this.key = key2;
        this.displayOrder = displayOrder2;
        this.iconUrl = iconUrl2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key2) {
        this.key = key2;
    }

    public int getDisplayOrder() {
        return this.displayOrder;
    }

    public void setDisplayOrder(int displayOrder2) {
        this.displayOrder = displayOrder2;
    }

    public String getIconUrl() {
        return this.iconUrl;
    }

    public void setIconUrl(String iconUrl2) {
        this.iconUrl = iconUrl2;
    }

    public String toString() {
        return this.name;
    }

    public int hashCode() {
        return this.key.hashCode();
    }

    public boolean equals(Object o) {
        if (!(o instanceof Locale)) {
            return false;
        }
        return this.key.equals(((Locale) o).key);
    }

    /* access modifiers changed from: package-private */
    public void save(SharedPreferences sh) {
        SharedPreferences.Editor editor = sh.edit();
        editor.putString("locale-key", getKey());
        editor.putString("locale-name", getName());
        editor.putInt("locale-displayOrder", getDisplayOrder());
        editor.putString("locale-iconUrl", getIconUrl());
        if (!editor.commit()) {
            throw new IllegalStateException("Cannot comit to preferences");
        }
    }

    static Locale createFromPreferences(SharedPreferences sh) {
        String key2 = sh.getString("locale-key", null);
        if (key2 == null) {
            return null;
        }
        return new Locale(key2, sh.getString("locale-name", "Unknown"), sh.getInt("locale-displayOrder", 0), sh.getString("locale-iconUrl", ""));
    }
}
