package com.vervewireless.capi;

import android.content.SharedPreferences;
import android.net.Uri;
import java.io.IOException;
import java.io.InputStream;
import org.apache.http.HttpResponse;

class HierachyTask extends AbstractVerveTask<DisplayBlock> {
    private static final long HIEARARCHY_TIMEOUT = 950400000;
    private final HierarchyListener listener;
    private final String url;

    public HierachyTask(HierarchyListener listener2, String url2, String pageName) {
        this.listener = listener2;
        Uri.Builder builder = Uri.parse(url2).buildUpon();
        builder.appendEncodedPath("hierarchy");
        builder.appendQueryParameter("pageName", pageName);
        this.url = builder.toString();
    }

    public void finishedSuccessfully(DisplayBlock result) {
        this.listener.onHierarchyRecieved(result);
    }

    public void failed(Exception cause) {
        this.listener.onHierarchyFailed(VerveError.createFromException(cause));
    }

    static void setLastUpdate(SharedPreferences sh, Locale activeLocale, long timestamp) {
        String cacheKey = "hierachy_update_" + activeLocale.getKey();
        SharedPreferences.Editor editor = sh.edit();
        if (timestamp <= 0) {
            editor.remove(cacheKey);
        } else {
            editor.putLong(cacheKey, timestamp);
        }
        editor.commit();
    }

    public DisplayBlock call() throws Exception {
        SharedPreferences sh = getPreferences();
        Locale activeLocale = getApi().getLocale();
        String cacheKey = "hierachy_update_" + activeLocale.getKey();
        long now = System.currentTimeMillis();
        ContentModel contentModel = getContentModel();
        DisplayBlock root = null;
        if (now - sh.getLong(cacheKey, 0) < HIEARARCHY_TIMEOUT) {
            root = contentModel.getContentHierachy(activeLocale);
        }
        if (root != null) {
            Logger.logDebug("Using local hierarachy, top level displayBlock:" + root.getId());
            return root;
        }
        HttpResponse response = getRequestDisptacher().doGet(this.url);
        Logger.logDebug("Fetching hiearchy from the server");
        InputStream stream = Logger.debugDump("Hieararchy response:", getStream(response));
        checkResponseForErrors(response);
        Hierarchy hiearchy = new Hierarchy();
        hiearchy.parseFromXml(stream);
        insertSavedStories(hiearchy);
        DisplayBlock newRoot = hiearchy.getRoot();
        Logger.assertTrue(newRoot != null);
        contentModel.updateContentHierachy(activeLocale, newRoot);
        setLastUpdate(sh, activeLocale, now);
        reportApiStatus(hiearchy.getApiStatus());
        return newRoot;
    }

    private void insertSavedStories(Hierarchy hiearchy) {
        DisplayBlock root = hiearchy.getRoot();
        DisplayBlock sb = new DisplayBlock();
        sb.setId(8951);
        sb.setAdCategoryId(999);
        sb.setName("Saved Stories");
        sb.setPartnerModuleId(PartnerModules.SAVED_STORIES);
        sb.setParentId(root.getId());
        sb.setType("savedNewsModule");
        sb.setIconStyle("EditorialSaved.png");
        int max = 1;
        for (DisplayBlock block : root.getDisplayBlocks()) {
            max = Math.max(block.getDisplayOrder(), max);
        }
        sb.setDisplayOrder(max);
        root.addDisplayBlock(sb);
    }

    private void checkResponseForErrors(HttpResponse response) throws IOException {
        if (response.getStatusLine().getStatusCode() != 200) {
            throw new IOException(response.getStatusLine().toString());
        }
    }
}
