package com.vervewireless.capi;

class ReportPageViewTask extends AbstractVerveTask<Void> {
    private final PageView pageView;

    public ReportPageViewTask(PageView pageView2) {
        this.pageView = pageView2;
    }

    public Void call() throws Exception {
        getContentModel().addPageView(this.pageView);
        return null;
    }

    public void finishedSuccessfully(Void result) {
    }

    public void failed(Exception cause) {
    }
}
