package com.vervewireless.capi;

import android.util.Xml;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Map;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.xmlpull.v1.XmlSerializer;

class SetUserPreferencesTask extends AbstractVerveTask<UserPreferences> {
    private final RegistrationInfo info;
    private final UserPreferences preferences;

    public SetUserPreferencesTask(RegistrationInfo info2, UserPreferences preferences2) {
        this.info = info2;
        this.preferences = preferences2;
    }

    public void finishedSuccessfully(UserPreferences result) {
    }

    public void failed(Exception cause) {
    }

    public UserPreferences call() throws Exception {
        XmlSerializer serilizer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        serilizer.setOutput(writer);
        serilizer.startDocument("UTF-8", true);
        serilizer.startTag("http://www.vervewireless.com/xsd/client/registration", "userprefsreq");
        serilizer.attribute(null, "version", "1.0");
        serilizer.startTag(null, "apiauth");
        serilizer.attribute(null, "id", this.info.getApiId());
        serilizer.attribute(null, "token", this.info.getAuthToken());
        serilizer.endTag(null, "apiauth");
        serilizer.startTag(null, "userprefs");
        for (Map.Entry<String, String> entry : this.preferences.entries().entrySet()) {
            serilizer.startTag(null, "userpref");
            serilizer.attribute(null, "name", (String) entry.getKey());
            serilizer.cdsect((String) entry.getValue());
            serilizer.endTag(null, "userpref");
        }
        serilizer.endTag(null, "userprefs");
        serilizer.endTag("http://www.vervewireless.com/xsd/client/registration", "userprefsreq");
        serilizer.endDocument();
        HttpPost post = new HttpPost("https://clientreg.vervewireless.com/userprefs");
        post.setEntity(new StringEntity(writer.toString()));
        Logger.logDebug("We are sending:" + writer.toString());
        InputStream input = Logger.debugDump("Preferences:", getStream(this.httpClient.execute(post)));
        UserPreferences ret = new UserPreferences();
        ret.parse(input);
        getContentModel().setPreferences(ret);
        return ret;
    }
}
