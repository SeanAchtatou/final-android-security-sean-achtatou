package com.vervewireless.capi;

import android.content.ContentValues;
import android.text.Html;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class ContentItem {
    private static final String AD_CATEGORY_ID = "adCategoryId";
    private static final String CREATOR = "creator";
    private static final String ENCODED = "encoded";
    private static final String EXTERNAL_ID = "externalId";
    static final String GUID = "guid";
    private static final String LINK = "link";
    private static final String PARTNER_MODULE_ID = "partnerModuleId";
    static final String PUB_DATE = "pubDate";
    private static final String RATING = "rating";
    private static final String RATING_COUNT = "ratingCount";
    private static final String TITLE = "title";
    private String body;
    private int categoryId;
    private String creator;
    private String externalId;
    private String guid;
    private String link;
    private List<MediaItem> mediaItems = new ArrayList(0);
    private int partnerModuleId;
    private Date pubDate;
    private double rating;
    private int ratingCount;
    private boolean saved;
    private String title;

    /* access modifiers changed from: package-private */
    public void load(ValueSource values) {
        this.title = values.getValue(TITLE, null);
        this.link = values.getValue(LINK, null);
        this.pubDate = values.getDate(PUB_DATE, new Date());
        this.creator = values.getValue(CREATOR, null);
        this.body = values.getValue(ENCODED, "");
        this.externalId = values.getValue(EXTERNAL_ID, null);
        this.ratingCount = values.getInt(RATING_COUNT, 0);
        this.rating = (double) values.getFloat(RATING, 0.0f);
        this.categoryId = values.getInt(AD_CATEGORY_ID, 0);
        this.guid = values.getValue(GUID, null);
        this.partnerModuleId = values.getInt(PARTNER_MODULE_ID, 0);
    }

    /* access modifiers changed from: package-private */
    public void save(ContentValues values) {
        values.put(TITLE, this.title);
        values.put(LINK, this.link);
        if (this.pubDate != null) {
            values.put(PUB_DATE, Long.valueOf(this.pubDate.getTime()));
        }
        values.put(CREATOR, this.creator);
        values.put(ENCODED, this.body);
        values.put(EXTERNAL_ID, this.externalId);
        values.put(RATING_COUNT, Integer.valueOf(this.ratingCount));
        values.put(RATING, Double.valueOf(this.rating));
        values.put(AD_CATEGORY_ID, Integer.valueOf(this.categoryId));
        values.put(GUID, this.guid);
        values.put(PARTNER_MODULE_ID, Integer.valueOf(this.partnerModuleId));
    }

    /* access modifiers changed from: package-private */
    public void parse(XmlPullParser parser) throws XmlPullParserException, IOException {
        Logger.assertTrue(parser.getName().equals("item") && parser.getEventType() == 2);
        StringMap map = new StringMap();
        while (true) {
            if (parser.getEventType() == 3 && parser.getName().equals("item")) {
                load(map);
                return;
            } else if (parser.nextTag() == 2) {
                String name = parser.getName();
                if (name.equals(TITLE)) {
                    map.put(name, Html.fromHtml(parser.nextText().trim()).toString());
                } else if (name.equals(RATING)) {
                    map.put(RATING, parser.getAttributeValue(null, RATING));
                    map.put(RATING_COUNT, parser.getAttributeValue(null, RATING_COUNT));
                } else if (name.equals("group")) {
                    MediaGroup group = new MediaGroup();
                    group.parse(parser);
                    this.mediaItems.add(group.getItem());
                } else if (!name.equals("content") || !"http://search.yahoo.com/mrss/".equals(parser.getNamespace())) {
                    map.put(parser.getName(), parser.nextText().trim());
                } else {
                    MediaItem mi = new MediaItem();
                    mi.parse(parser);
                    this.mediaItems.add(mi);
                }
            }
        }
    }

    public String getTitle() {
        return this.title;
    }

    public String getLink() {
        return this.link;
    }

    public void setPubDate(Date pubDate2) {
        this.pubDate = pubDate2;
    }

    public Date getPubDate() {
        return this.pubDate;
    }

    public String getCreator() {
        return this.creator;
    }

    public String getBody() {
        return this.body;
    }

    public String getExternalId() {
        return this.externalId;
    }

    public int getAdCategoryId() {
        return this.categoryId;
    }

    public void setAdCategoryId(int value) {
        this.categoryId = value;
    }

    public String getGuid() {
        return this.guid;
    }

    public double getRating() {
        return this.rating;
    }

    public int getRatingCount() {
        return this.ratingCount;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public void setLink(String link2) {
        this.link = link2;
    }

    public void setCreator(String creator2) {
        this.creator = creator2;
    }

    public void setBody(String body2) {
        this.body = body2;
    }

    public void setExternalId(String externalId2) {
        this.externalId = externalId2;
    }

    public void setGuid(String guid2) {
        this.guid = guid2;
    }

    public void setRating(double rating2) {
        this.rating = rating2;
    }

    public void setRatingCount(int ratingCount2) {
        this.ratingCount = ratingCount2;
    }

    public List<MediaItem> getMediaItems() {
        return this.mediaItems;
    }

    public int getPartnerModuleId() {
        return this.partnerModuleId;
    }

    public void setSaved(boolean saved2) {
        this.saved = saved2;
    }

    public boolean isSaved() {
        return this.saved;
    }
}
