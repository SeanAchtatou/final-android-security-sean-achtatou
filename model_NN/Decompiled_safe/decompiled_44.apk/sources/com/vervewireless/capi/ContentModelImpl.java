package com.vervewireless.capi;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

class ContentModelImpl extends SQLiteOpenHelper implements ContentModel {
    static final /* synthetic */ boolean $assertionsDisabled = (!ContentModelImpl.class.desiredAssertionStatus() ? true : $assertionsDisabled);
    private static final String CONTENT_REF_TABLE = "contentXrefs";
    private static final String CONTENT_TABLE = "contents";
    public static final int CURRENT_VERSION = 4;
    public static final String DB_NAME = "capi_content";
    private static final String DISPLAY_BLOCK_TABLE = "displayBlocks";
    private static final String MEDIA_TABLE = "media";
    private static final String PAGE_VIEWS_TABLE = "pageViews";
    private static final String PAGE_VIEW_SESSIONS_TABLE = "pageViewSessions";
    private static final String USER_PREFS_TABLE = "userPrefs";

    public ContentModelImpl(Context context, String name, int version) {
        super(context, name, (SQLiteDatabase.CursorFactory) null, version);
    }

    public ContentModelImpl(Context context) {
        this(context, DB_NAME, 4);
    }

    public void onCreate(SQLiteDatabase db) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(ContentModelImpl.class.getResourceAsStream("content_model.schema")), 8192);
        try {
            createFromStream(db, reader);
            try {
                reader.close();
            } catch (IOException e) {
            }
            Logger.logDebug("Database successfully created");
        } catch (IOException e2) {
            throw new IllegalStateException(e2);
        } catch (Throwable th) {
            try {
                reader.close();
            } catch (IOException e3) {
            }
            throw th;
        }
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVerion) {
        clear(db);
    }

    private void dropDb(SQLiteDatabase db) {
        Cursor c;
        List<String> tables = new ArrayList<>();
        try {
            db.beginTransaction();
            c = db.query("sqlite_master", new String[]{"name"}, "type = ?", new String[]{"table"}, null, null, null);
            while (c.moveToNext()) {
                tables.add(c.getString(0));
            }
            c.close();
            for (String table : tables) {
                db.execSQL("DROP TABLE " + table);
            }
            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (Throwable th) {
            db.endTransaction();
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void clear(SQLiteDatabase wrDb) {
        dropDb(wrDb);
        onCreate(wrDb);
    }

    public void clear() {
        clear(getWritableDatabase());
    }

    private void createFromStream(SQLiteDatabase db, BufferedReader reader) throws IOException {
        db.execSQL("PRAGMA foreign_keys=on;");
        StringBuilder content = new StringBuilder(1024);
        String line = reader.readLine();
        while (line != null) {
            String line2 = line.trim();
            if (!line2.startsWith("--")) {
                content.append(line2);
            }
            line = reader.readLine();
        }
        for (String statement : content.toString().split(";")) {
            db.execSQL(statement);
        }
    }

    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (Logger.isDebugEnabled()) {
            db.execSQL("PRAGMA foreign_keys=on;");
        }
    }

    /* JADX INFO: finally extract failed */
    public void updateContentHierachy(Locale locale, DisplayBlock root) {
        SQLiteDatabase readDb = getReadableDatabase();
        HashSet hashSet = new HashSet();
        Cursor oldValues = readDb.query(DISPLAY_BLOCK_TABLE, new String[]{"id"}, "locale = ? ", new String[]{locale.getKey()}, null, null, null);
        while (oldValues.moveToNext()) {
            try {
                hashSet.add(Integer.valueOf(oldValues.getInt(0)));
            } catch (Throwable th) {
                oldValues.close();
                throw th;
            }
        }
        oldValues.close();
        SQLiteDatabase writeDb = getWritableDatabase();
        writeDb.beginTransaction();
        try {
            Stack<DisplayBlock> stack = new Stack<>();
            stack.push(root);
            while (!stack.isEmpty()) {
                DisplayBlock block = (DisplayBlock) stack.pop();
                for (DisplayBlock child : block.getDisplayBlocks()) {
                    stack.push(child);
                }
                if (!hashSet.remove(Integer.valueOf(block.getId()))) {
                    Logger.logDebug("Adding new display block:" + block.getName() + "[" + block.getId() + "] for locale:" + locale.getKey());
                    addDisplayBlock(locale, block);
                } else {
                    Logger.logDebug("Updating display block:" + block.getName() + "[" + block.getId() + "] for locale:" + locale.getKey());
                    ContentValues values = new ContentValues();
                    block.save(values);
                    values.remove("id");
                    String[] params = {locale.getKey(), String.valueOf(block.getId())};
                    Logger.logDBUpdate(DISPLAY_BLOCK_TABLE, values);
                    Logger.assertTrue(writeDb.update(DISPLAY_BLOCK_TABLE, values, "locale = ? AND id = ?", params) == 1 ? true : $assertionsDisabled);
                }
            }
            if (!hashSet.isEmpty()) {
                Logger.logDebug("Removing stale display blocks: " + VerveUtils.join(hashSet, ","));
                writeDb.delete(DISPLAY_BLOCK_TABLE, "id in (" + VerveUtils.join(hashSet, ",") + ")", null);
            }
            writeDb.setTransactionSuccessful();
        } finally {
            writeDb.endTransaction();
        }
    }

    public void addDisplayBlock(Locale locale, DisplayBlock block) {
        ContentValues values = new ContentValues();
        block.save(values);
        values.put("locale", locale.getKey());
        Logger.logDBInsert(DISPLAY_BLOCK_TABLE, values);
        getWritableDatabase().insertOrThrow(DISPLAY_BLOCK_TABLE, null, values);
    }

    public DisplayBlock getDisplayBlock(Locale locale, int id) {
        Cursor c = getReadableDatabase().query(DISPLAY_BLOCK_TABLE, null, "id = ? AND locale = ?", new String[]{String.valueOf(id), locale.getKey()}, null, null, null);
        DisplayBlock block = null;
        try {
            if (c.moveToNext()) {
                DisplayBlock block2 = new DisplayBlock();
                try {
                    block2.load(new CursorSource(c));
                    block = block2;
                } catch (Throwable th) {
                    th = th;
                    c.close();
                    throw th;
                }
            }
            c.close();
            return block;
        } catch (Throwable th2) {
            th = th2;
            c.close();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public DisplayBlock getContentHierachy(Locale locale) {
        Cursor c = getReadableDatabase().query(DISPLAY_BLOCK_TABLE, null, "locale = ?", new String[]{locale.getKey()}, null, null, "displayOrder");
        Map<Integer, DisplayBlock> map = new LinkedHashMap<>();
        while (c.moveToNext()) {
            try {
                DisplayBlock block = new DisplayBlock();
                block.load(new CursorSource(c));
                Logger.logDebug("Loaded: " + block.getName() + "[" + block.getId() + "], partner:" + block.getParentId());
                map.put(Integer.valueOf(block.getId()), block);
            } catch (Throwable th) {
                c.close();
                throw th;
            }
        }
        c.close();
        DisplayBlock root = null;
        for (DisplayBlock block2 : map.values()) {
            DisplayBlock parent = (DisplayBlock) map.get(Integer.valueOf(block2.getParentId()));
            if (parent != null) {
                parent.addDisplayBlock(block2);
            } else if ($assertionsDisabled || root == null) {
                root = block2;
            } else {
                throw new AssertionError();
            }
        }
        Logger.assertTrue((map.isEmpty() || root != null) ? true : $assertionsDisabled);
        return root;
    }

    public void addContentItem(DisplayBlock block, ContentItem item, int displayOrder) {
        boolean exists;
        SQLiteDatabase writeDb = getReadableDatabase();
        writeDb.beginTransaction();
        Cursor cursor = null;
        SQLiteDatabase readDb = writeDb;
        try {
            Cursor cursor2 = readDb.query(CONTENT_TABLE, new String[]{"guid"}, "guid = ?", new String[]{item.getGuid()}, null, null, null);
            if (!cursor2.moveToFirst() || cursor2.getString(0) == null) {
                exists = false;
            } else {
                exists = true;
            }
            cursor2.close();
            if (!exists) {
                ContentValues values = new ContentValues();
                item.save(values);
                Logger.logDBInsert(CONTENT_TABLE, values);
                writeDb.insertOrThrow(CONTENT_TABLE, null, values);
                saveMediaItems(writeDb, item);
            }
            writeDb.delete(CONTENT_REF_TABLE, "dbId = ? AND guid = ?", new String[]{String.valueOf(block.getId()), item.getGuid()});
            if (displayOrder == -1) {
                cursor2 = readDb.query(CONTENT_REF_TABLE, new String[]{"min(displayOrder)"}, "dbId = ?", new String[]{String.valueOf(block.getId())}, null, null, null);
                if (cursor2.moveToFirst()) {
                    displayOrder = cursor2.getInt(0) - 1;
                } else {
                    displayOrder = 100;
                }
                cursor2.close();
            }
            ContentValues ref = createReference(block, item, displayOrder);
            Logger.logDBInsert(CONTENT_REF_TABLE, ref);
            writeDb.insertOrThrow(CONTENT_REF_TABLE, null, ref);
            writeDb.setTransactionSuccessful();
        } finally {
            writeDb.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void removeContentItem(DisplayBlock saveBlock, ContentItem item) {
        int count;
        SQLiteDatabase writeDb = getReadableDatabase();
        writeDb.beginTransaction();
        Cursor cursor = null;
        SQLiteDatabase readDb = writeDb;
        try {
            writeDb.delete(CONTENT_REF_TABLE, "guid = ? and dbId = ?", new String[]{item.getGuid(), String.valueOf(saveBlock.getId())});
            cursor = readDb.query(CONTENT_REF_TABLE, new String[]{"count(dbId)"}, "guid = ? ", new String[]{item.getGuid()}, "dbId", null, null);
            if (cursor.moveToNext()) {
                count = cursor.getInt(0);
            } else {
                count = 0;
            }
            if (count == 1) {
                writeDb.delete(CONTENT_TABLE, "guid = ?", new String[]{item.getGuid()});
                writeDb.delete(MEDIA_TABLE, "guid = ?", new String[]{item.getGuid()});
            }
            writeDb.setTransactionSuccessful();
        } finally {
            writeDb.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public ContentResponse updateContentItems(Locale activeLocale, DisplayBlock block, List<ContentItem> items) {
        Cursor cExisting;
        Cursor c;
        boolean z;
        SQLiteDatabase writeDb = getReadableDatabase();
        writeDb.beginTransaction();
        SQLiteDatabase readDb = writeDb;
        try {
            Set<String> contentIds = new HashSet<>();
            for (ContentItem item : items) {
                if (!(!contentIds.add(item.getGuid()))) {
                    z = true;
                } else {
                    z = $assertionsDisabled;
                }
                Logger.assertTrue(z);
            }
            HashSet hashSet = new HashSet();
            cExisting = readDb.query(CONTENT_TABLE, new String[]{"guid"}, String.format("guid in (%s)", VerveUtils.join(contentIds, ",", "'")), null, null, null, null);
            while (cExisting.moveToNext()) {
                hashSet.add(cExisting.getString(0));
            }
            cExisting.close();
            for (ContentItem item2 : items) {
                boolean exists = hashSet.contains(item2.getGuid());
                ContentValues itemValues = new ContentValues();
                item2.save(itemValues);
                itemValues.put("contentType", block.getContentType());
                if (!exists) {
                    Logger.logDBInsert(CONTENT_TABLE, itemValues);
                    writeDb.insertOrThrow(CONTENT_TABLE, null, itemValues);
                    saveMediaItems(writeDb, item2);
                } else {
                    int updateCount = writeDb.update(CONTENT_TABLE, itemValues, "guid = ? AND pubDate != ? ", new String[]{item2.getGuid(), Long.toString(item2.getPubDate().getTime())});
                    Logger.assertTrue(updateCount <= 1 ? true : $assertionsDisabled);
                    if (updateCount == 1) {
                        Logger.logDebug("Updated story:" + item2.getTitle() + ", pubDate:" + item2.getPubDate() + ", guid:" + item2.getGuid());
                        Logger.logDebug("Updating media items. Count:" + writeDb.delete(MEDIA_TABLE, "guid = ?", new String[]{item2.getGuid()}));
                        saveMediaItems(writeDb, item2);
                    }
                }
            }
            HashMap hashMap = new HashMap();
            c = readDb.query(CONTENT_REF_TABLE, new String[]{"guid", "displayOrder"}, "dbId = ? AND searchResult <> 1", new String[]{String.valueOf(block.getId())}, null, null, null);
            while (c.moveToNext()) {
                Logger.assertTrue(((Integer) hashMap.put(c.getString(0), Integer.valueOf(c.getInt(1)))) == null ? true : $assertionsDisabled);
            }
            c.close();
            int order = 1;
            for (ContentItem item3 : items) {
                Integer dpOldOrder = (Integer) hashMap.remove(item3.getGuid());
                ContentValues ref = createReference(block, item3, order);
                if (dpOldOrder == null) {
                    Logger.logDBInsert(CONTENT_REF_TABLE, ref);
                    writeDb.insertOrThrow(CONTENT_REF_TABLE, null, ref);
                } else if (dpOldOrder.intValue() != order) {
                    Logger.assertTrue(writeDb.update(CONTENT_REF_TABLE, ref, "guid = ? AND dbId = ?", new String[]{item3.getGuid(), String.valueOf(block.getId())}) == 1 ? true : $assertionsDisabled);
                }
                order++;
            }
            if (!hashMap.isEmpty()) {
                Logger.logDebug("Removing stale content items: " + VerveUtils.join(hashMap.keySet(), ",", "'"));
                writeDb.delete(CONTENT_REF_TABLE, "guid in (" + VerveUtils.join(hashMap.keySet(), ",", "'") + ") AND dbId = ?", new String[]{String.valueOf(block.getId())});
            }
            setLastContentRequest(activeLocale, block, new Date());
            writeDb.setTransactionSuccessful();
            writeDb.endTransaction();
            ContentResponse ret = getContentItems(activeLocale, block);
            ret.setCached($assertionsDisabled);
            return ret;
        } catch (Throwable th) {
            writeDb.endTransaction();
            throw th;
        }
    }

    public Date getLastContentRequest(Locale locale, DisplayBlock block) {
        Cursor c = getReadableDatabase().query(DISPLAY_BLOCK_TABLE, new String[]{DisplayBlock.LAST_CONTENT_REQUEST}, "id = ? AND locale = ?", new String[]{String.valueOf(block.getId()), locale.getKey()}, null, null, null);
        Date ret = null;
        try {
            if (c.moveToFirst()) {
                ret = c.isNull(0) ? null : new Date(c.getLong(0));
            }
            return ret;
        } finally {
            c.close();
        }
    }

    public void setLastContentRequest(Locale locale, DisplayBlock block, Date time) {
        SQLiteDatabase writeDb = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DisplayBlock.LAST_CONTENT_REQUEST, Long.valueOf(time.getTime()));
        writeDb.update(DISPLAY_BLOCK_TABLE, values, "id = ? AND locale = ?", new String[]{String.valueOf(block.getId()), locale.getKey()});
    }

    private void saveMediaItems(SQLiteDatabase writeDb, ContentItem item) {
        int mDbOrder = 1;
        for (MediaItem mi : item.getMediaItems()) {
            ContentValues mValues = new ContentValues();
            mi.save(mValues);
            mValues.put("displayOrder", Integer.valueOf(mDbOrder));
            mValues.put("guid", item.getGuid());
            Logger.logDBInsert(MEDIA_TABLE, mValues);
            writeDb.insertOrThrow(MEDIA_TABLE, null, mValues);
            mDbOrder++;
        }
    }

    private void loadMediaItems(SQLiteDatabase readDb, ContentItem item) {
        Cursor c = readDb.query(MEDIA_TABLE, null, "guid = ?", new String[]{item.getGuid()}, null, null, "displayOrder");
        while (c.moveToNext()) {
            try {
                MediaItem mi = new MediaItem();
                mi.load(new CursorSource(c));
                item.getMediaItems().add(mi);
            } finally {
                c.close();
            }
        }
    }

    private ContentValues createReference(DisplayBlock block, ContentItem item, int displayOrder) {
        ContentValues refs = new ContentValues();
        refs.put("guid", item.getGuid());
        refs.put("dbID", Integer.valueOf(block.getId()));
        refs.put("displayOrder", Integer.valueOf(displayOrder));
        refs.put("lastRefreshed", Long.valueOf(System.currentTimeMillis()));
        return refs;
    }

    public ContentItem getContentItem(String guid) {
        Cursor c = getReadableDatabase().query(CONTENT_TABLE, null, "guid = ?", new String[]{guid}, null, null, null);
        ContentItem ret = null;
        try {
            if (c.moveToNext()) {
                ContentItem ret2 = new ContentItem();
                try {
                    ret2.load(new CursorSource(c));
                    ret = ret2;
                } catch (Throwable th) {
                    th = th;
                    c.close();
                    throw th;
                }
            }
            c.close();
            return ret;
        } catch (Throwable th2) {
            th = th2;
            c.close();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public ContentResponse getContentItems(Locale locale, DisplayBlock block) {
        SQLiteDatabase readDb = getReadableDatabase();
        List<ContentItem> items = new ArrayList<>();
        Set<String> ids = new HashSet<>();
        Date lastUpdate = getLastContentRequest(locale, block);
        Cursor c = readDb.rawQuery("select * from contents inner join contentXrefs on contents.guid = contentXrefs.guid where contentXrefs.dbId = ? order by contentXrefs.displayOrder", new String[]{String.valueOf(block.getId())});
        try {
            CursorSource cursorSource = new CursorSource(c);
            while (c.moveToNext()) {
                ContentItem item = new ContentItem();
                item.load(cursorSource);
                items.add(item);
                ids.add(item.getGuid());
            }
            c.close();
            Cursor c2 = readDb.query(CONTENT_REF_TABLE, new String[]{"guid"}, "dbId = ? AND guid in (" + VerveUtils.join(ids, ",", "'") + ")", new String[]{String.valueOf(8951)}, null, null, null);
            while (c2.moveToNext()) {
                try {
                    ids.remove(c2.getString(0));
                } catch (Throwable th) {
                    c2.close();
                    throw th;
                }
            }
            c2.close();
            for (ContentItem item2 : items) {
                loadMediaItems(readDb, item2);
                item2.setSaved(!ids.contains(item2.getGuid()) ? true : $assertionsDisabled);
                if (item2.isSaved()) {
                    Logger.logDebug("Item:" + item2.getTitle() + " has been saved");
                }
            }
            return new ContentResponse(block, items, lastUpdate);
        } catch (Throwable th2) {
            c.close();
            throw th2;
        }
    }

    /* JADX INFO: finally extract failed */
    public List<ContentItem> search(DisplayBlock block, String searchTerms) {
        SQLiteDatabase readDb = getReadableDatabase();
        List<ContentItem> items = new ArrayList<>();
        String terms = "%" + searchTerms.replaceAll("%", "\\\\%").replaceAll("_", "\\\\%") + "%";
        Logger.logDebug("select * from contents inner join contentXrefs on contents.guid = contentXrefs.guid where contentXrefs.dbId = ? and (contents.encoded LIKE " + terms + " ESCAPE '\\' or contents.title LIKE " + terms + " ESCAPE '\\') order by contentXrefs.displayOrder;");
        Cursor c = readDb.rawQuery("select * from contents inner join contentXrefs on contents.guid = contentXrefs.guid where contentXrefs.dbId = ? and (contents.encoded LIKE ? ESCAPE '\\' or contents.title LIKE ? ESCAPE '\\') order by contentXrefs.displayOrder;", new String[]{String.valueOf(block.getId()), terms, terms});
        try {
            CursorSource source = new CursorSource(c);
            while (c.moveToNext()) {
                ContentItem item = new ContentItem();
                item.load(source);
                items.add(item);
            }
            c.close();
            for (ContentItem item2 : items) {
                loadMediaItems(readDb, item2);
            }
            return items;
        } catch (Throwable th) {
            c.close();
            throw th;
        }
    }

    public void setPreferences(UserPreferences preferences) {
        SQLiteDatabase writeDb = getReadableDatabase();
        writeDb.beginTransaction();
        Cursor cursor = null;
        try {
            writeDb.delete(USER_PREFS_TABLE, null, null);
            for (Map.Entry<String, String> entry : preferences.entries().entrySet()) {
                ContentValues values = new ContentValues();
                values.put("name", (String) entry.getKey());
                values.put("value", (String) entry.getValue());
                writeDb.insertOrThrow(USER_PREFS_TABLE, null, values);
            }
            writeDb.setTransactionSuccessful();
        } finally {
            writeDb.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public UserPreferences getPreferences() {
        UserPreferences prefs = new UserPreferences();
        Cursor cursor = getReadableDatabase().query(USER_PREFS_TABLE, new String[]{"name", "value"}, null, null, null, null, null);
        while (cursor.moveToNext()) {
            try {
                prefs.setValue(cursor.getString(0), cursor.getString(1));
            } finally {
                cursor.close();
            }
        }
        return prefs;
    }

    public PageViewSession createPageViewSession(String network) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.beginTransaction();
            Date now = new Date();
            ContentValues values = new ContentValues();
            values.put("id", Long.valueOf(now.getTime()));
            values.put("start", VerveUtils.toISO(now));
            values.put("network", network);
            Logger.logDBInsert(PAGE_VIEW_SESSIONS_TABLE, values);
            db.insertOrThrow(PAGE_VIEW_SESSIONS_TABLE, null, values);
            PageViewSession session = new PageViewSession(now.getTime(), network, now);
            try {
                db.setTransactionSuccessful();
                db.endTransaction();
                return session;
            } catch (Throwable th) {
                th = th;
                db.endTransaction();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            db.endTransaction();
            throw th;
        }
    }

    public void updatePageViewSession(String network, Location location) {
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = null;
        try {
            db.beginTransaction();
            cursor = db.query(PAGE_VIEW_SESSIONS_TABLE, new String[]{"max(id)"}, null, null, null, null, null);
            if (!cursor.moveToFirst()) {
                throw new IllegalStateException("No session");
            }
            ContentValues values = new ContentValues();
            if (network != null) {
                values.put("network", network);
            }
            if (location != null) {
                values.put("location", PageViewSession.encodeLocation(location));
            }
            db.update(PAGE_VIEW_SESSIONS_TABLE, values, "id = ?", new String[]{String.valueOf(cursor.getLong(0))});
            Logger.logDebug("Changed current network type to:" + network + " and location to:" + location);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public int addPageView(PageView pageView) {
        int count = 0;
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = null;
        try {
            db.beginTransaction();
            cursor = db.query(PAGE_VIEW_SESSIONS_TABLE, new String[]{"max(id)"}, null, null, null, null, null);
            if (!cursor.moveToFirst()) {
                throw new IllegalStateException("No session");
            }
            long sessionId = cursor.getLong(0);
            cursor.close();
            ContentValues values = new ContentValues();
            pageView.save(values);
            values.put("sessionId", Long.valueOf(sessionId));
            Logger.logDBInsert(PAGE_VIEWS_TABLE, values);
            db.insert(PAGE_VIEWS_TABLE, null, values);
            cursor = db.query(PAGE_VIEWS_TABLE, new String[]{"count(*)"}, null, null, null, null, null);
            if (cursor.moveToFirst()) {
                count = cursor.getInt(0);
                Logger.logDebug("We got " + count + " page entries");
            }
            db.setTransactionSuccessful();
            return count;
        } finally {
            db.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public List<PageView> getPageViews(long id) {
        SQLiteDatabase db = getReadableDatabase();
        List<PageView> views = new ArrayList<>();
        Cursor c = db.query(PAGE_VIEWS_TABLE, null, "sessionId = ?", new String[]{String.valueOf(id)}, null, null, null);
        while (c.moveToNext()) {
            try {
                PageView view = new PageView();
                view.load(new CursorSource(c));
                views.add(view);
            } finally {
                c.close();
            }
        }
        return views;
    }

    public List<PageViewSession> getPageViewSessions() {
        SQLiteDatabase db = getReadableDatabase();
        List<PageViewSession> sessions = new ArrayList<>();
        Cursor c = db.query(PAGE_VIEW_SESSIONS_TABLE, null, null, null, null, null, null);
        while (c.moveToNext()) {
            try {
                PageViewSession session = new PageViewSession();
                session.load(new CursorSource(c));
                sessions.add(session);
            } finally {
                c.close();
            }
        }
        return sessions;
    }

    public void removePageViewSession(long id) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(PAGE_VIEWS_TABLE, "sessionId = ?", new String[]{String.valueOf(id)});
        db.delete(PAGE_VIEW_SESSIONS_TABLE, "id = ?", new String[]{String.valueOf(id)});
    }
}
