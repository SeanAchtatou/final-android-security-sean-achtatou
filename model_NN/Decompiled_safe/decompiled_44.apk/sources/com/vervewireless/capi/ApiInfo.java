package com.vervewireless.capi;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

public class ApiInfo {
    private String applicationId;
    private String applicationName;
    private String applicationVersion;
    private String registrationId;

    public ApiInfo(String id, String version, String name) {
        if (id == null) {
            throw new IllegalArgumentException("id must be non-null");
        }
        this.applicationId = id;
        this.applicationVersion = version;
        this.applicationName = name;
    }

    public static ApiInfo createFromContext(Context context) {
        ApplicationInfo ai = context.getApplicationInfo();
        try {
            return new ApiInfo(ai.packageName, context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName, ai.name == null ? "" : ai.name);
        } catch (PackageManager.NameNotFoundException e) {
            throw new IllegalStateException("Cannot obtain the version");
        }
    }

    public String getApplicationId() {
        return this.applicationId;
    }

    public void setApplicationId(String id) {
        this.applicationId = id;
    }

    public String getApplicationVersion() {
        return this.applicationVersion;
    }

    public void setApplicationVersion(String version) {
        this.applicationVersion = version;
    }

    public String getApplicationName() {
        return this.applicationName;
    }

    public void setApplicationName(String name) {
        this.applicationName = name;
    }

    public void setRegistrationId(String registrationId2) {
        this.registrationId = registrationId2;
    }

    public String getRegistrationId() {
        return this.registrationId;
    }
}
