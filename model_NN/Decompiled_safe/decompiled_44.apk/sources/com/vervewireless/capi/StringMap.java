package com.vervewireless.capi;

import java.util.HashMap;
import java.util.Map;

class StringMap extends AbstractValueSource {
    private Map<String, String> map = new HashMap();

    StringMap() {
    }

    public void put(String key, String value) {
        this.map.put(key, value);
    }

    public String getValue(String key, String defValue) {
        String ret = this.map.get(key);
        return ret == null ? defValue : ret;
    }
}
