package com.vervewireless.capi;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.Html;
import android.util.Log;
import android.util.Xml;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class GetLocalesTask extends AbstractVerveTask<List<Locale>> {
    private static final String LOCALES_URL = "https://clientreg.vervewireless.com/locales";
    private static List<Locale> cachedLocale;
    private final LocaleListener listener;

    public /* bridge */ /* synthetic */ void finishedSuccessfully(Object x0) {
        finishedSuccessfully((List<Locale>) ((List) x0));
    }

    public /* bridge */ /* synthetic */ Verve getApi() {
        return super.getApi();
    }

    public /* bridge */ /* synthetic */ ApiInfo getAppInfo() {
        return super.getAppInfo();
    }

    public /* bridge */ /* synthetic */ ContentModel getContentModel() {
        return super.getContentModel();
    }

    public /* bridge */ /* synthetic */ SharedPreferences getPreferences() {
        return super.getPreferences();
    }

    public /* bridge */ /* synthetic */ void setApi(Verve x0) {
        super.setApi(x0);
    }

    public /* bridge */ /* synthetic */ void setAppInfo(ApiInfo x0) {
        super.setAppInfo(x0);
    }

    public /* bridge */ /* synthetic */ void setCapiChangeListener(CapiChangeListener x0) {
        super.setCapiChangeListener(x0);
    }

    public /* bridge */ /* synthetic */ void setContentModel(ContentModel x0) {
        super.setContentModel(x0);
    }

    public /* bridge */ /* synthetic */ void setContext(Context x0) {
        super.setContext(x0);
    }

    public /* bridge */ /* synthetic */ void setHttpClient(HttpClient x0) {
        super.setHttpClient(x0);
    }

    public /* bridge */ /* synthetic */ void setPreferences(SharedPreferences x0) {
        super.setPreferences(x0);
    }

    public GetLocalesTask(LocaleListener listener2) {
        this.listener = listener2;
    }

    public List<Locale> call() throws Exception {
        Log.d(GetLocalesTask.class.getName(), "Getting locales");
        if (cachedLocale != null) {
            return new ArrayList(cachedLocale);
        }
        List<Locale> locales = doRequest(this.httpClient, getAppInfo());
        cachedLocale = new ArrayList(locales);
        return locales;
    }

    /* access modifiers changed from: package-private */
    public List<Locale> doRequest(HttpClient client, ApiInfo appInfo) throws IOException, XmlPullParserException {
        Log.d(GetLocalesTask.class.getName(), "Requesting locales from server");
        HttpPost httpPost = new HttpPost(LOCALES_URL);
        String requestData = createRequest(appInfo);
        httpPost.setEntity(new StringEntity(requestData, "UTF-8"));
        Logger.logDebug("Locale request:" + requestData);
        HttpResponse response = client.execute(httpPost);
        XmlPullParser parser = Xml.newPullParser();
        parser.setInput(Logger.debugDump("Locale response:", getStream(response)), null);
        List<Locale> locales = new ArrayList<>();
        for (int eventType = parser.getEventType(); eventType != 1; eventType = parser.next()) {
            switch (eventType) {
                case CapiChangeListener.CAPI_STATUS_HIER:
                    if (!parser.getName().equals("locale")) {
                        break;
                    } else {
                        String name = Html.fromHtml(parser.getAttributeValue("", "name")).toString();
                        String key = parser.getAttributeValue("", "locale_key");
                        String iconUrl = parser.getAttributeValue("", "icon_url");
                        String order = parser.getAttributeValue("", "display_order");
                        int displayOrder = 0;
                        if (order != null) {
                            displayOrder = Integer.parseInt(order);
                        }
                        locales.add(new Locale(key, name, displayOrder, iconUrl));
                        break;
                    }
            }
        }
        Log.d(GetLocalesTask.class.getName(), "Retrieved " + locales.size() + " locales");
        return locales;
    }

    private String createRequest(ApiInfo appInfo) throws IOException {
        XmlSerializer xml = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        xml.setOutput(writer);
        xml.startTag("", "localereq").attribute("", "version", "1.0");
        xml.startTag("", "mobileapp");
        xml.attribute("", "id", appInfo.getApplicationId());
        xml.attribute("", "version", appInfo.getApplicationVersion());
        xml.endTag("", "mobileapp");
        xml.endDocument();
        return writer.toString();
    }

    public void finishedSuccessfully(List<Locale> result) {
        this.listener.onLocalesRecieved(result);
    }

    public void failed(Exception cause) {
        this.listener.onLocalesFailed(VerveError.createFromException(cause));
    }
}
