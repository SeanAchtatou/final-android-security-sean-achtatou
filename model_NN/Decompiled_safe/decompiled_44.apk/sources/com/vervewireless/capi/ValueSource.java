package com.vervewireless.capi;

import java.util.Date;

public interface ValueSource {
    boolean getBoolean(String str, boolean z);

    Date getDate(String str, Date date);

    float getFloat(String str, float f);

    int getInt(String str, int i);

    long getLong(String str, long j);

    String getValue(String str, String str2);
}
