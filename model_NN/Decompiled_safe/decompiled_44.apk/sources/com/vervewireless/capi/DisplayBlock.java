package com.vervewireless.capi;

import android.content.ContentValues;
import android.text.Html;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class DisplayBlock {
    private static final String AD_CATEGORY_ID = "adCategoryId";
    private static final String BLOCK_BG_COLOR = "blockBgColor";
    private static final String BLOCK_TEXT_COLOR = "blockTextColor";
    private static final String BREAKING = "breaking";
    static final String CONTENT_TYPE = "contentType";
    private static final String DISPLAY_DEFAULT = "displayDefault";
    static final String DISPLAY_ORDER = "displayOrder";
    private static final String FEATURED = "featured";
    private static final String HEADLINE = "headline";
    private static final String ICON_STYLE = "iconStyle";
    private static final String ICON_URL = "iconUrl";
    static final String ID = "id";
    public static final String LAST_CONTENT_REQUEST = "lastContentRequest";
    public static String LOCALE = "locale";
    private static final String NAME = "name";
    private static final String PARENT_ID = "parentDbId";
    private static final String PARTNER_MODULE_ID = "partnerModuleId";
    static final int SAVED_STORIES_ID = 8951;
    private static final String SPONSORED = "sponsored";
    private static final String SUMMARY_COUNT = "summaryCount";
    private static final String TYPE = "type";
    private static final String XML_URL = "xmlUrl";
    private int adCategoryId;
    private String blockBackgroundColor;
    private String blockTextColor;
    private boolean breaking;
    private String contentType;
    private List<DisplayBlock> displayBlocks;
    private boolean displayDefault;
    private int displayOrder = -1;
    private boolean featured;
    private boolean headline;
    private String iconStyle;
    private String iconUrl;
    private int id;
    private String name;
    private int parentId;
    private int partnerModuleId;
    private boolean sponsored;
    private int summaryCount = -1;
    private String type;
    private String xmlUrl;

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type2) {
        this.type = type2;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public String getIconStyle() {
        return this.iconStyle;
    }

    public void setIconStyle(String iconStyle2) {
        this.iconStyle = iconStyle2;
    }

    public int getAdCategoryId() {
        return this.adCategoryId;
    }

    public void setAdCategoryId(int adCategoryId2) {
        this.adCategoryId = adCategoryId2;
    }

    public int getDisplayOrder() {
        return this.displayOrder;
    }

    public void setDisplayOrder(int displayOrder2) {
        this.displayOrder = displayOrder2;
    }

    public String getIconUrl() {
        return this.iconUrl;
    }

    public void setIconUrl(String iconUrl2) {
        this.iconUrl = iconUrl2;
    }

    public boolean isDisplayDefault() {
        return this.displayDefault;
    }

    public void setDisplayDefault(boolean displayDefault2) {
        this.displayDefault = displayDefault2;
    }

    public String getXmlUrl() {
        return this.xmlUrl;
    }

    public void setXmlUrl(String xmlUrl2) {
        this.xmlUrl = xmlUrl2;
    }

    public int getPartnerModuleId() {
        return this.partnerModuleId;
    }

    public void setPartnerModuleId(int partnerModuleId2) {
        this.partnerModuleId = partnerModuleId2;
    }

    public boolean isHeadline() {
        return this.headline;
    }

    public void setHeadline(boolean headline2) {
        this.headline = headline2;
    }

    public boolean isBreaking() {
        return this.breaking;
    }

    public void setBreaking(boolean breaking2) {
        this.breaking = breaking2;
    }

    public String getContentType() {
        if (this.contentType != null) {
            return this.contentType;
        }
        String type2 = getType() == null ? "" : getType();
        if ("contentModule".equals(type2) || "apNewsModule".equals(type2) || "alertsModule".equals(type2)) {
            return "Editorial";
        }
        if ("photoGalleryModule".equals(type2)) {
            return "Photo";
        }
        if ("videoGalleryModule".equals(type2)) {
            return "Video";
        }
        if ("localNewsModule".equals(type2)) {
            return "Editorial";
        }
        if ("ratedNewsModule".equals(type2) || "mostSharedModule".equals(type2) || "multiSourceModule".equals(type2)) {
            return "Synthetic";
        }
        if ("curatedArtwork".equals(type2)) {
            return "Photo";
        }
        return null;
    }

    public void setContentType(String contentType2) {
        this.contentType = contentType2;
    }

    public int getSummaryCount() {
        return this.summaryCount;
    }

    public void setSummaryCount(int summaryCount2) {
        this.summaryCount = summaryCount2;
    }

    public String getBlockTextColor() {
        return this.blockTextColor;
    }

    public void setBlockTextColor(String blockTextColor2) {
        this.blockTextColor = blockTextColor2;
    }

    public String getBlockBackgroundColor() {
        return this.blockBackgroundColor;
    }

    public void setBlockBackgroundColor(String blockBackgroundColor2) {
        this.blockBackgroundColor = blockBackgroundColor2;
    }

    public boolean isFeatured() {
        return this.featured;
    }

    public void setFeatured(boolean featured2) {
        this.featured = featured2;
    }

    public boolean isSponsored() {
        return this.sponsored;
    }

    public void setSponsored(boolean sponsored2) {
        this.sponsored = sponsored2;
    }

    public int getParentId() {
        return this.parentId;
    }

    public void setParentId(int parentId2) {
        this.parentId = parentId2;
    }

    /* access modifiers changed from: package-private */
    public void save(ContentValues value) {
        value.put(ID, Integer.valueOf(this.id));
        if (this.parentId != 0) {
            value.put(PARENT_ID, Integer.valueOf(this.parentId));
        }
        value.put(NAME, this.name);
        value.put(AD_CATEGORY_ID, Integer.valueOf(this.adCategoryId));
        value.put(TYPE, this.type);
        value.put(DISPLAY_DEFAULT, Boolean.valueOf(this.displayDefault));
        value.put(ICON_STYLE, this.iconStyle);
        value.put(ICON_URL, this.iconUrl);
        value.put(DISPLAY_ORDER, Integer.valueOf(this.displayOrder));
        value.put(XML_URL, this.xmlUrl);
        value.put(PARTNER_MODULE_ID, Integer.valueOf(this.partnerModuleId));
        value.put(HEADLINE, Boolean.valueOf(this.headline));
        value.put(BREAKING, Boolean.valueOf(this.breaking));
        value.put(FEATURED, Boolean.valueOf(this.featured));
        value.put(SPONSORED, Boolean.valueOf(this.sponsored));
        value.put(CONTENT_TYPE, this.contentType);
        value.put(SUMMARY_COUNT, Integer.valueOf(this.summaryCount));
        value.put(BLOCK_BG_COLOR, this.blockBackgroundColor);
        value.put(BLOCK_TEXT_COLOR, this.blockTextColor);
    }

    /* access modifiers changed from: package-private */
    public void load(ValueSource source) {
        this.name = source.getValue(NAME, null);
        this.id = source.getInt(ID, -1);
        this.parentId = source.getInt(PARENT_ID, 0);
        this.adCategoryId = source.getInt(AD_CATEGORY_ID, 999);
        this.type = source.getValue(TYPE, null);
        this.contentType = source.getValue(CONTENT_TYPE, null);
        this.iconStyle = source.getValue(ICON_STYLE, null);
        this.iconUrl = source.getValue(ICON_URL, null);
        this.displayOrder = source.getInt(DISPLAY_ORDER, -1);
        this.displayDefault = source.getBoolean(DISPLAY_DEFAULT, false);
        this.xmlUrl = source.getValue(XML_URL, null);
        this.partnerModuleId = source.getInt(PARTNER_MODULE_ID, -1);
        this.headline = source.getBoolean(HEADLINE, false);
        this.breaking = source.getBoolean(BREAKING, false);
        this.featured = source.getBoolean(FEATURED, false);
        this.sponsored = source.getBoolean(SPONSORED, false);
        this.summaryCount = source.getInt(SUMMARY_COUNT, -1);
        this.blockBackgroundColor = getColor(source.getValue(BLOCK_BG_COLOR, null));
        this.blockTextColor = getColor(source.getValue(BLOCK_TEXT_COLOR, null));
    }

    /* access modifiers changed from: package-private */
    public void parse(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(2, null, "outline");
        load(new AttributeValueResolver(parser));
        int event = parser.next();
        while (event != 1) {
            if (event == 3) {
                parser.next();
                return;
            }
            if (event != 4) {
                if (event == 2 && parser.getName().equals("outline")) {
                    DisplayBlock outline = new DisplayBlock();
                    outline.parse(parser);
                    outline.setParentId(getId());
                    addDisplayBlock(outline);
                } else {
                    return;
                }
            }
            event = parser.next();
        }
    }

    public void addDisplayBlock(DisplayBlock outline) {
        if (this.displayBlocks == null) {
            this.displayBlocks = new ArrayList(5);
        }
        this.displayBlocks.add(outline);
    }

    public List<DisplayBlock> getDisplayBlocks() {
        if (this.displayBlocks == null || this.displayBlocks.isEmpty()) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList(this.displayBlocks);
    }

    private String getColor(String rgb) {
        if (rgb == null) {
            return null;
        }
        if (rgb.startsWith("#")) {
            return rgb;
        }
        if (rgb.startsWith("0x")) {
            return "#" + rgb.substring(2);
        }
        throw new IllegalArgumentException("Unsupported color format:" + rgb);
    }

    public String toString() {
        return getName();
    }

    private static class AttributeValueResolver extends AbstractValueSource {
        private XmlPullParser parser;

        public AttributeValueResolver(XmlPullParser parser2) {
            this.parser = parser2;
        }

        public String getValue(String key, String defValue) {
            String value = this.parser.getAttributeValue(null, key);
            if (DisplayBlock.NAME.equals(key) && value != null) {
                value = Html.fromHtml(value).toString();
            }
            return value == null ? defValue : value;
        }
    }
}
