package com.vervewireless.capi;

import android.os.Handler;

class VerveTaskWrapper<T> implements Runnable {
    private final Handler notifyExecutor;
    private final VerveTask<T> task;

    public VerveTaskWrapper(Handler notifyExecutor2, VerveTask<T> task2) {
        this.notifyExecutor = notifyExecutor2;
        this.task = task2;
    }

    public void run() {
        T result = null;
        boolean sucess = false;
        try {
            result = this.task.call();
            sucess = true;
        } catch (Exception e) {
            Exception e2 = e;
            Logger.logDebug("Task failed", e2);
            this.notifyExecutor.post(new NotifyFailure(this.task, e2));
        }
        if (sucess) {
            this.notifyExecutor.post(new NotifySuccess(this.task, result));
        }
    }

    private static class NotifyFailure<T> implements Runnable {
        private final Exception failureCause;
        private final VerveTask<T> task;

        public NotifyFailure(VerveTask<T> task2, Exception e) {
            this.failureCause = e;
            this.task = task2;
        }

        public void run() {
            this.task.failed(this.failureCause);
        }
    }

    private static class NotifySuccess<T> implements Runnable {
        private final T result;
        private final VerveTask<T> task;

        public NotifySuccess(VerveTask<T> task2, T result2) {
            this.result = result2;
            this.task = task2;
        }

        public void run() {
            this.task.finishedSuccessfully(this.result);
        }
    }
}
