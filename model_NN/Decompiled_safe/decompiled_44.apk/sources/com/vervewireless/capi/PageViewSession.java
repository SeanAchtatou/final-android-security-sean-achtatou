package com.vervewireless.capi;

import android.location.Location;
import android.net.NetworkInfo;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import org.xmlpull.v1.XmlSerializer;

class PageViewSession {
    private long id;
    private Location location;
    private String network;
    private Date startTime;

    public PageViewSession(long id2, String network2) {
        this(id2, network2, new Date());
    }

    public PageViewSession(long id2, String network2, Date now) {
        this.id = id2;
        this.network = network2;
        this.startTime = now;
    }

    public PageViewSession() {
    }

    public void load(ValueSource values) {
        this.id = values.getLong("id", -1);
        this.network = values.getValue("network", null);
        String start = values.getValue("start", null);
        if (start != null) {
            this.startTime = VerveUtils.fromIso(start);
        }
        this.location = decodeLocation(values.getValue("location", null));
    }

    public long getId() {
        return this.id;
    }

    public static String getNetworkName(NetworkInfo netInfo) {
        if (netInfo == null || !netInfo.isAvailable()) {
            return "OFFLINE";
        }
        if (netInfo.getType() == 1) {
            return "WIFI";
        }
        if (netInfo.getType() != 0) {
            return "UNKNOWN";
        }
        switch (netInfo.getSubtype()) {
            case CapiChangeListener.CAPI_STATUS_REREG:
                return "GPRS";
            case CapiChangeListener.CAPI_STATUS_HIER:
                return "EDGE";
            case 3:
                return "3G";
            default:
                return "CELLULAR";
        }
    }

    public void toXml(XmlSerializer xml, List<PageView> pageViews) throws IOException {
        xml.startTag(VerveUtils.PAGE_VIEW_HISTORY_NS, "session");
        xml.attribute(null, "session_id", String.valueOf(this.id));
        xml.attribute(null, "start_time", VerveUtils.toISO(this.startTime));
        xml.startTag(VerveUtils.PAGE_VIEW_HISTORY_NS, "networkType");
        xml.attribute(null, "network", this.network);
        xml.endTag(VerveUtils.PAGE_VIEW_HISTORY_NS, "networkType");
        xml.startTag(VerveUtils.PAGE_VIEW_HISTORY_NS, "location");
        if (getLocation() != null) {
            xml.attribute(null, "latitude", Location.convert(getLocation().getLatitude(), 0));
            xml.attribute(null, "longitude", Location.convert(getLocation().getLongitude(), 0));
        } else {
            xml.attribute(null, "latitude", "0.0");
            xml.attribute(null, "longitude", "0.0");
        }
        xml.endTag(VerveUtils.PAGE_VIEW_HISTORY_NS, "location");
        for (PageView pageView : pageViews) {
            pageView.save(xml);
        }
        xml.endTag(VerveUtils.PAGE_VIEW_HISTORY_NS, "session");
    }

    public void setLocation(Location location2) {
        this.location = location2;
    }

    public Location getLocation() {
        return this.location;
    }

    public static String encodeLocation(Location location2) {
        if (location2 == null) {
            return null;
        }
        return Location.convert(location2.getLatitude(), 0) + "|" + Location.convert(location2.getLongitude(), 0);
    }

    public static Location decodeLocation(String value) {
        if (value == null) {
            return null;
        }
        String[] split = value.split("\\|");
        if (split.length < 2) {
            return null;
        }
        Location location2 = new Location("network");
        location2.setLatitude(Location.convert(split[0]));
        location2.setLongitude(Location.convert(split[1]));
        return location2;
    }
}
