package com.vervewireless.capi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class Channel {
    private int apiStatus;
    private List<ContentItem> items = new ArrayList();

    /* access modifiers changed from: package-private */
    public void parse(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(2, null, "channel");
        int event = parser.nextTag();
        while (true) {
            if (event != 3 || !parser.getName().equals("channel")) {
                if (event != 3) {
                    String name = parser.getName();
                    if ("title".equals(name)) {
                        parser.nextText();
                    } else if ("link".equals(name)) {
                        parser.nextText();
                    } else if ("description".equals(name)) {
                        parser.nextText();
                    } else if ("item".equals(name)) {
                        ContentItem item = new ContentItem();
                        item.parse(parser);
                        this.items.add(item);
                    } else if ("apistatus".equals(name)) {
                        this.apiStatus = Integer.parseInt(parser.nextText());
                        Logger.logDebug("Api status set to " + this.apiStatus);
                    } else {
                        Logger.logWarning("Ignoring chanel tag:" + parser.getName() + " with value " + parser.nextText());
                    }
                }
                event = parser.nextTag();
            } else {
                return;
            }
        }
    }

    public int getItemCount() {
        return this.items.size();
    }

    public ContentItem getItemAt(int i) {
        return this.items.get(i);
    }

    public List<ContentItem> getItems() {
        return this.items;
    }

    public int getApiStatus() {
        return this.apiStatus;
    }
}
