package com.vervewireless.capi;

import java.util.List;

public class SearchResponse {
    private final boolean cached;
    private final List<ContentItem> items;

    public SearchResponse(List<ContentItem> items2, boolean cached2) {
        this.items = items2;
        this.cached = cached2;
    }

    public SearchResponse(List<ContentItem> items2) {
        this(items2, false);
    }

    public List<ContentItem> getItems() {
        return this.items;
    }

    public int getItemCount() {
        return this.items.size();
    }

    public ContentItem getItemAt(int index) {
        return this.items.get(index);
    }

    public boolean isCached() {
        return this.cached;
    }
}
