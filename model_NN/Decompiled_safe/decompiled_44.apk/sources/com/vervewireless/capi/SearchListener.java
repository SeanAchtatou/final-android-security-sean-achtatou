package com.vervewireless.capi;

public interface SearchListener {
    void onSearchFailed(VerveError verveError);

    void onSearchFinished(SearchResponse searchResponse);
}
