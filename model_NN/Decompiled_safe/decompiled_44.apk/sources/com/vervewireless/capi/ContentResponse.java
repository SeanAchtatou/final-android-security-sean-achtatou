package com.vervewireless.capi;

import java.util.Date;
import java.util.List;

public class ContentResponse {
    private final DisplayBlock block;
    private boolean cached = true;
    private final List<ContentItem> items;
    private Date lastUpdate;

    public Date getLastUpdate() {
        return this.lastUpdate;
    }

    public ContentResponse(DisplayBlock block2, List<ContentItem> items2, Date lastUpdate2) {
        this.block = block2;
        this.items = items2;
        this.lastUpdate = lastUpdate2;
    }

    public DisplayBlock getDisplayBlock() {
        return this.block;
    }

    public List<ContentItem> getItems() {
        return this.items;
    }

    public int getItemCount() {
        return this.items.size();
    }

    public ContentItem getItemAt(int index) {
        return this.items.get(index);
    }

    public boolean isCached() {
        return this.cached;
    }

    public void setCached(boolean cached2) {
        this.cached = cached2;
    }
}
