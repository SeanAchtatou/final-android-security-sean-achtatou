package com.vervewireless.capi;

import android.net.Uri;
import android.util.Xml;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;

class SearchTask extends AbstractVerveTask<SearchResponse> {
    private final String baseUrl;
    private final SearchListener listener;
    private final SearchRequest request;

    public SearchTask(SearchListener listener2, String baseUrl2, SearchRequest request2) {
        this.listener = listener2;
        this.baseUrl = baseUrl2;
        this.request = request2;
    }

    public void finishedSuccessfully(SearchResponse result) {
        this.listener.onSearchFinished(result);
    }

    public void failed(Exception cause) {
        this.listener.onSearchFailed(VerveError.createFromException(cause));
    }

    public SearchResponse call() throws Exception {
        if (this.request.isSearchOffline()) {
            return new SearchResponse(getContentModel().search(this.request.getDisplayBlock(), this.request.getQuery()));
        }
        DisplayBlock block = this.request.getDisplayBlock();
        Uri.Builder urlBuilder = Uri.parse(this.baseUrl).buildUpon();
        urlBuilder.appendEncodedPath("search/");
        urlBuilder.appendPath("db_" + block.getId());
        urlBuilder.appendQueryParameter("searchTerms", this.request.getQuery());
        InputStream stream = Logger.debugDump("Search response for " + block.getId() + ":", getStream(getRequestDisptacher().doGet(urlBuilder.toString())));
        XmlPullParser parser = Xml.newPullParser();
        parser.setInput(stream, null);
        VerveUtils.skipToTag(parser, "channel");
        Channel channel = new Channel();
        channel.parse(parser);
        reportApiStatus(channel.getApiStatus());
        return new SearchResponse(channel.getItems());
    }
}
