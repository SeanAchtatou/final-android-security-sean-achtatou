package com.vervewireless.capi;

public interface CapiStatusListener {
    void hiearachyUpdated();

    void reregistrationRequired();

    void updateAvailable();
}
