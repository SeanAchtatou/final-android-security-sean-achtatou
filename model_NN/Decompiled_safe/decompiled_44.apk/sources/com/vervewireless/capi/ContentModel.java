package com.vervewireless.capi;

import android.location.Location;
import java.util.Date;
import java.util.List;

interface ContentModel {
    void addContentItem(DisplayBlock displayBlock, ContentItem contentItem, int i);

    int addPageView(PageView pageView);

    void clear();

    void close();

    PageViewSession createPageViewSession(String str);

    DisplayBlock getContentHierachy(Locale locale);

    ContentItem getContentItem(String str);

    ContentResponse getContentItems(Locale locale, DisplayBlock displayBlock);

    Date getLastContentRequest(Locale locale, DisplayBlock displayBlock);

    List<PageViewSession> getPageViewSessions();

    List<PageView> getPageViews(long j);

    UserPreferences getPreferences();

    void removeContentItem(DisplayBlock displayBlock, ContentItem contentItem);

    void removePageViewSession(long j);

    List<ContentItem> search(DisplayBlock displayBlock, String str);

    void setPreferences(UserPreferences userPreferences);

    void updateContentHierachy(Locale locale, DisplayBlock displayBlock);

    ContentResponse updateContentItems(Locale locale, DisplayBlock displayBlock, List<ContentItem> list);

    void updatePageViewSession(String str, Location location);
}
