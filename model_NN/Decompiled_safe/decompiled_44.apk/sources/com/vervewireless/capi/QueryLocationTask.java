package com.vervewireless.capi;

import android.content.Context;
import android.content.SharedPreferences;
import org.apache.http.client.HttpClient;

public class QueryLocationTask extends AbstractVerveTask<QueryLocationResponse> {
    public /* bridge */ /* synthetic */ Verve getApi() {
        return super.getApi();
    }

    public /* bridge */ /* synthetic */ ApiInfo getAppInfo() {
        return super.getAppInfo();
    }

    public /* bridge */ /* synthetic */ ContentModel getContentModel() {
        return super.getContentModel();
    }

    public /* bridge */ /* synthetic */ SharedPreferences getPreferences() {
        return super.getPreferences();
    }

    public /* bridge */ /* synthetic */ void setApi(Verve x0) {
        super.setApi(x0);
    }

    public /* bridge */ /* synthetic */ void setAppInfo(ApiInfo x0) {
        super.setAppInfo(x0);
    }

    public /* bridge */ /* synthetic */ void setCapiChangeListener(CapiChangeListener x0) {
        super.setCapiChangeListener(x0);
    }

    public /* bridge */ /* synthetic */ void setContentModel(ContentModel x0) {
        super.setContentModel(x0);
    }

    public /* bridge */ /* synthetic */ void setContext(Context x0) {
        super.setContext(x0);
    }

    public /* bridge */ /* synthetic */ void setHttpClient(HttpClient x0) {
        super.setHttpClient(x0);
    }

    public /* bridge */ /* synthetic */ void setPreferences(SharedPreferences x0) {
        super.setPreferences(x0);
    }

    public void finishedSuccessfully(QueryLocationResponse result) {
    }

    public void failed(Exception cause) {
    }

    public QueryLocationResponse call() throws Exception {
        return null;
    }
}
