package com.vervewireless.capi;

public class VerveRegistrationError extends Exception {
    public VerveRegistrationError(String message) {
        super(message);
    }
}
