package com.vervewireless.capi;

import android.content.Context;
import android.content.SharedPreferences;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;

abstract class AbstractVerveTask<T> implements VerveTask<T> {
    protected Verve api;
    protected ApiInfo appInfo;
    private ContentModel contentModel;
    protected Context context;
    protected HttpClient httpClient;
    private SharedPreferences preferences;
    private RequestDisptacher requestDisptacher;
    private CapiChangeListener statusListener;

    AbstractVerveTask() {
    }

    public Verve getApi() {
        return this.api;
    }

    public void setApi(Verve api2) {
        this.api = api2;
    }

    public void setHttpClient(HttpClient client) {
        this.httpClient = client;
    }

    public void setContext(Context context2) {
        this.context = context2;
    }

    public ApiInfo getAppInfo() {
        return this.appInfo;
    }

    public void setAppInfo(ApiInfo appInfo2) {
        this.appInfo = appInfo2;
    }

    public SharedPreferences getPreferences() {
        return this.preferences;
    }

    public void setPreferences(SharedPreferences preferences2) {
        this.preferences = preferences2;
    }

    public ContentModel getContentModel() {
        return this.contentModel;
    }

    public void setContentModel(ContentModel contentModel2) {
        this.contentModel = contentModel2;
    }

    /* access modifiers changed from: package-private */
    public RequestDisptacher getRequestDisptacher() {
        return this.requestDisptacher;
    }

    /* access modifiers changed from: package-private */
    public void setRequestDisptacher(RequestDisptacher requestDisptacher2) {
        this.requestDisptacher = requestDisptacher2;
    }

    /* access modifiers changed from: protected */
    public void reportApiStatus(int apiStatus) {
        Logger.assertTrue(this.statusListener != null);
        Logger.logDebug("reporting API status:" + apiStatus);
        this.statusListener.notifyCapiStatus(apiStatus);
    }

    /* access modifiers changed from: protected */
    public InputStream getStream(HttpResponse response) throws IllegalStateException, IOException {
        Header h = response.getFirstHeader("Content-Encoding");
        Logger.logDebug("Got response with content " + h);
        if (h == null || h.getValue().toLowerCase().indexOf("gzip") < 0) {
            return response.getEntity().getContent();
        }
        return new GZIPInputStream(response.getEntity().getContent());
    }

    public void setCapiChangeListener(CapiChangeListener statusListener2) {
        this.statusListener = statusListener2;
    }
}
