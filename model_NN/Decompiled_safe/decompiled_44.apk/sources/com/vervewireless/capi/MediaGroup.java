package com.vervewireless.capi;

import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

class MediaGroup {
    private MediaItem item;

    MediaGroup() {
    }

    /* access modifiers changed from: package-private */
    public void parse(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(2, null, "group");
        this.item = new MediaItem();
        int event = parser.getEventType();
        while (true) {
            if (event != 3 || !"group".equals(parser.getName())) {
                event = parser.next();
                if (event == 2) {
                    String tagName = parser.getName();
                    if ("content".equals(tagName) && this.item.getUrl() == null) {
                        for (int i = 0; i < parser.getAttributeCount(); i++) {
                            String name = parser.getAttributeName(i);
                            String value = parser.getAttributeValue(i);
                            if (name.equals("width")) {
                                this.item.setWidth(Integer.parseInt(value));
                            } else if (name.equals("height")) {
                                this.item.setHeight(Integer.parseInt(value));
                            } else if (name.equals("url")) {
                                this.item.setUrl(value);
                            } else if (name.equals("type")) {
                                this.item.setMediaType(value);
                            } else {
                                Logger.logDebug("Unknown type:" + name + " with value: " + value);
                            }
                        }
                    } else if ("description".equals(tagName)) {
                        this.item.setCaption(parser.nextText());
                    }
                }
            } else {
                return;
            }
        }
    }

    public MediaItem getItem() {
        return this.item;
    }
}
