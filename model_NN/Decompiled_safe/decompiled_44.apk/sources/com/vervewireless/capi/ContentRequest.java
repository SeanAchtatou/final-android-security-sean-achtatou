package com.vervewireless.capi;

public class ContentRequest {
    private boolean allowOffline = true;
    private DisplayBlock displayBlock;

    public ContentRequest(DisplayBlock displayBlock2) {
        this.displayBlock = displayBlock2;
    }

    public DisplayBlock getDisplayBlock() {
        return this.displayBlock;
    }

    public boolean isAllowOffline() {
        return this.allowOffline;
    }

    public void setAllowOffline(boolean allowOffline2) {
        this.allowOffline = allowOffline2;
    }
}
