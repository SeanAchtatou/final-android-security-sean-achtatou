package com.vervewireless.capi;

import android.content.ContentValues;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class MediaItem {
    private static final String CAPTION = "caption";
    private static final String HEIGHT = "height";
    private static final String MEDIA_TYPE = "mediaType";
    private static final String THUMB_URL = "thumbUrl";
    private static final String URL = "url";
    private static final String WIDTH = "width";
    private String caption;
    private int height;
    private String mediaType;
    private String thumbUrl;
    private String url;
    private int width;

    /* access modifiers changed from: package-private */
    public void load(ValueSource values) {
        this.mediaType = values.getValue(MEDIA_TYPE, null);
        this.caption = values.getValue(CAPTION, null);
        this.width = values.getInt(WIDTH, 0);
        this.height = values.getInt(HEIGHT, 0);
        this.thumbUrl = values.getValue(THUMB_URL, null);
        this.url = values.getValue(URL, null);
    }

    public String getMediaType() {
        return this.mediaType;
    }

    public void setMediaType(String mediaType2) {
        this.mediaType = mediaType2;
    }

    public String getCaption() {
        return this.caption;
    }

    public void setCaption(String caption2) {
        this.caption = caption2;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int width2) {
        this.width = width2;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int height2) {
        this.height = height2;
    }

    public String getThumbUrl() {
        return this.thumbUrl;
    }

    public void setThumbUrl(String thumbUrl2) {
        this.thumbUrl = thumbUrl2;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public String getUrl() {
        return this.url;
    }

    /* access modifiers changed from: package-private */
    public void parse(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(2, "http://search.yahoo.com/mrss/", "content");
        int e = parser.getEventType();
        while (true) {
            if (e != 3 || !"content".equals(parser.getName())) {
                if (e == 2) {
                    String tag = parser.getName();
                    if ("content".equals(tag)) {
                        for (int idx = 0; idx < parser.getAttributeCount(); idx++) {
                            String attrName = parser.getAttributeName(idx);
                            String value = parser.getAttributeValue(idx);
                            if (attrName.equals(URL)) {
                                setUrl(value);
                            } else if ("type".equals(attrName)) {
                                setMediaType(value);
                            } else if (WIDTH.equals(attrName)) {
                                setWidth(Integer.parseInt(value));
                            } else if (HEIGHT.equals(attrName)) {
                                setHeight(Integer.parseInt(value));
                            }
                        }
                    } else if ("thumbnail".equals(tag)) {
                        setThumbUrl(parser.getAttributeValue(null, URL));
                    } else if ("description".equals(tag)) {
                        setCaption(parser.nextText());
                    } else {
                        Logger.logWarning("Unknown media item tag:" + tag);
                    }
                }
                e = parser.next();
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void save(ContentValues mValues) {
        mValues.put(URL, this.url);
        mValues.put(THUMB_URL, this.thumbUrl);
        mValues.put(WIDTH, Integer.valueOf(this.width));
        mValues.put(HEIGHT, Integer.valueOf(this.height));
        mValues.put(MEDIA_TYPE, this.mediaType);
        mValues.put(CAPTION, this.caption);
    }
}
