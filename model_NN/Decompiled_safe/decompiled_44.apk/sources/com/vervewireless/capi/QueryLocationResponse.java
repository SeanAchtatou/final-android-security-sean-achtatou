package com.vervewireless.capi;

public class QueryLocationResponse {
    private String city;
    private String state;
    private String stateAbbreviation;

    public String getCity() {
        return this.city;
    }

    public void setCity(String city2) {
        this.city = city2;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String state2) {
        this.state = state2;
    }

    public String getStateAbbreviation() {
        return this.stateAbbreviation;
    }

    public void setStateAbbreviation(String stateAbbreviation2) {
        this.stateAbbreviation = stateAbbreviation2;
    }
}
