package com.vervewireless.capi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.telephony.TelephonyManager;
import com.vervewireless.advert.VerveAdApi;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.http.client.HttpClient;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public class VerveImpl implements Verve, CapiChangeListener {
    /* access modifiers changed from: private */
    public VerveAdApi adApi;
    private final ApiInfo appInfo;
    private ExecutorService backgroundExecutor;
    /* access modifiers changed from: private */
    public CapiStatusListener capiStatusListener;
    /* access modifiers changed from: private */
    public boolean connectivity;
    private BroadcastReceiver connectivityReciever;
    private ContentModel contentModel;
    private final Context context;
    private Handler handler;
    private HttpClient httpClient;
    private Location location;
    private SharedPreferences preferences;
    /* access modifiers changed from: private */
    public RegistrationInfo registration;
    /* access modifiers changed from: private */
    public AtomicReference<RequestDisptacher> requestDisptacher;
    /* access modifiers changed from: private */
    public boolean updateNotified;

    public VerveImpl(Context context2) {
        this(context2, ApiInfo.createFromContext(context2));
    }

    public VerveImpl(Context context2, String verveId, String version) {
        this(context2, new ApiInfo(verveId, version, ""));
    }

    public VerveImpl(Context context2, ApiInfo appInfo2) {
        this.handler = new Handler();
        this.requestDisptacher = new AtomicReference<>();
        this.connectivity = false;
        if (context2 == null) {
            throw new IllegalArgumentException("Context must be non-null");
        }
        this.appInfo = appInfo2;
        this.context = context2;
        this.backgroundExecutor = Executors.newSingleThreadExecutor();
        DefaultHttpClient client = new DefaultHttpClient();
        ClientConnectionManager mgr = client.getConnectionManager();
        HttpParams httpParameters = client.getParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, 300000);
        HttpConnectionParams.setSoTimeout(httpParameters, 300000);
        HttpClientParams.setCookiePolicy(httpParameters, "compatibility");
        this.httpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(httpParameters, mgr.getSchemeRegistry()), httpParameters);
        this.contentModel = new ContentModelImpl(context2);
        this.preferences = context2.getSharedPreferences("verveapi", 0);
        this.adApi = new VerveAdApi(this.context);
        this.adApi.setHttpClient(this.httpClient);
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        this.connectivityReciever = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                boolean z;
                if (!intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                    Logger.assertTrue(false);
                    return;
                }
                VerveImpl verveImpl = VerveImpl.this;
                if (!intent.getBooleanExtra("noConnectivity", false)) {
                    z = true;
                } else {
                    z = false;
                }
                boolean unused = verveImpl.connectivity = z;
                NetworkInfo netInfo = (NetworkInfo) intent.getParcelableExtra("networkInfo");
                if (VerveImpl.this.connectivity) {
                    Logger.logDebug("Connected " + netInfo.getTypeName() + ", state:" + netInfo.getDetailedState());
                } else {
                    Logger.logDebug("No connectivity " + netInfo.getTypeName() + ", state:" + netInfo.getDetailedState());
                }
                if (VerveImpl.this.isRegistered()) {
                    VerveImpl.this.updatePageViewSession(netInfo);
                }
            }
        };
        context2.registerReceiver(this.connectivityReciever, filter);
    }

    public void close() {
        Logger.logDebug("Shuting down the API threads");
        if (isRegistered()) {
            startNewSession(this.registration);
        }
        this.context.unregisterReceiver(this.connectivityReciever);
        Logger.logDebug("Stopping background executor");
        this.backgroundExecutor.shutdown();
        try {
            Logger.logDebug("Awaiting termination");
            if (!this.backgroundExecutor.awaitTermination(5, TimeUnit.SECONDS)) {
                Logger.logWarning("Forcing shutdown");
                this.backgroundExecutor.shutdownNow();
            }
        } catch (InterruptedException e) {
            Logger.logDebug("Shutdown was interrupted");
            this.backgroundExecutor.shutdownNow();
            Thread.currentThread().interrupt();
        }
        Logger.logDebug("Shutdown completed");
        this.contentModel.close();
    }

    public Locale getLocale() {
        String key = this.preferences.getString("locale-key", null);
        if (key == null) {
            return null;
        }
        return new Locale(key, this.preferences.getString("locale-name", "Unknown"), this.preferences.getInt("locale-displayOrder", 0), this.preferences.getString("locale-iconUrl", ""));
    }

    public UserPreferences getUserPreferences() {
        if (this.registration == null) {
            return null;
        }
        return this.registration.getPreferences();
    }

    public void setPrefrences(UserPreferences prefs) {
        setupAndExecuteTask(new SetUserPreferencesTask(this.registration, prefs) {
            public void finishedSuccessfully(UserPreferences result) {
                VerveImpl.this.registration.setPreferences(result);
                super.finishedSuccessfully(result);
            }
        }, true);
    }

    public void getContetHierarchy(HierarchyListener listener) {
        setupAndExecuteTask(new HierachyTask(listener, this.registration.getBaseUrl(), this.registration.getHierachyPage()), true);
    }

    private String getDeviceId() {
        WifiInfo wifiInfo;
        String devId = ((TelephonyManager) this.context.getSystemService("phone")).getDeviceId();
        while (devId == null) {
            WifiManager mng = (WifiManager) this.context.getSystemService("wifi");
            if (mng == null || (wifiInfo = mng.getConnectionInfo()) == null) {
                break;
            }
            devId = wifiInfo.getMacAddress();
        }
        if (devId == null) {
            throw new IllegalStateException("Cannot obtain a unique device id");
        }
        try {
            byte[] digest = MessageDigest.getInstance("MD5").digest(devId.getBytes());
            StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < digest.length; i++) {
                hexString.append(Integer.toHexString((digest[i] >>> 4) & 15));
                hexString.append(Integer.toHexString(digest[i] & 15));
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("Could not get MD5 algorithem");
        }
    }

    private <T> void execute(VerveTask<T> task, boolean mustBeRegistered) {
        if (!isRegistered() && mustBeRegistered) {
            task.failed(new IllegalStateException("Not registered"));
        }
        if (mustBeRegistered) {
            ensureRegistered();
        }
        Logger.logDebug("Executing task:" + task.getClass().getCanonicalName() + " in " + this.backgroundExecutor.getClass());
        try {
            this.backgroundExecutor.execute(new VerveTaskWrapper(this.handler, task));
        } catch (RejectedExecutionException e) {
            RejectedExecutionException ex = e;
            Logger.logWarning("Cannot schedule a new task", ex);
            task.failed(ex);
        }
    }

    public void registerWithVerve(Locale locale, RegistrationListener listener) {
        this.registration = null;
        this.requestDisptacher.set(null);
        this.appInfo.setRegistrationId(null);
        setupAndExecuteTask(new RegistrationTask(locale, getDeviceId(), listener) {
            /* access modifiers changed from: protected */
            public void setupRegistrationInfo(RegistrationInfo info) {
                RegistrationInfo unused = VerveImpl.this.registration = info;
                this.appInfo.setRegistrationId(VerveImpl.this.registration.getApiId());
                VerveImpl.this.adApi.setBaseAdUrl(info.getAdBaseUrl());
                VerveImpl.this.requestDisptacher.set(new RequestDispatcherImpl(info.getApiId(), info.getAuthToken(), this.httpClient, VerveImpl.this));
                VerveImpl.this.startNewSession(info);
            }
        }, false);
    }

    public void reset() {
        this.registration = null;
        this.requestDisptacher.set(null);
        setupAndExecuteTask(new ResetTask(), false);
        this.appInfo.setRegistrationId(null);
    }

    /* access modifiers changed from: private */
    public void startNewSession(RegistrationInfo info) {
        setupAndExecuteTask(new StartPageSessionTask(((ConnectivityManager) this.context.getSystemService("connectivity")).getActiveNetworkInfo(), info), true);
    }

    /* access modifiers changed from: private */
    public void updatePageViewSession(NetworkInfo netInfo) {
        if (isRegistered()) {
            Logger.assertTrue(isRegistered());
            setupAndExecuteTask(new UpdatePageSessionTask(netInfo, this.location), true);
        }
    }

    public boolean isRegistered() {
        return (this.registration == null || this.registration.getAuthToken() == null) ? false : true;
    }

    public void registerWithVerve(RegistrationListener listener) {
        registerWithVerve(null, listener);
    }

    public void getLocales(LocaleListener listener) {
        setupAndExecuteTask(new GetLocalesTask(listener), false);
    }

    public void save(ContentItem item) {
        setupAndExecuteTask(new SaveStoryTask(item), false);
    }

    public void unsave(ContentItem item) {
        setupAndExecuteTask(new SaveStoryTask(item, false), false);
    }

    public void getContent(ContentRequest request, ContentListener listener) {
        setupAndExecuteTask(new GetContentTask(listener, this.registration.getBaseUrl(), request), true);
    }

    public void search(SearchRequest request, SearchListener listener) {
        setupAndExecuteTask(new SearchTask(listener, this.registration.getBaseUrl(), request), true);
    }

    public void setLocation(Location location2) {
        Logger.logDebug("Setting location:" + location2);
        this.location = location2;
        if (this.adApi != null) {
            this.adApi.setLocation(location2);
        }
        if (isRegistered()) {
            Logger.assertTrue(isRegistered());
            setupAndExecuteTask(new UpdatePageSessionTask(null, location2), true);
        }
    }

    public void setPostCode(String postCode) {
    }

    public VerveAdApi getAdApi() {
        return this.adApi;
    }

    public ApiInfo getApiInfo() {
        return this.appInfo;
    }

    private void ensureRegistered() {
        if (this.requestDisptacher.get() == null) {
            this.appInfo.setRegistrationId(this.registration.getApiId());
            this.requestDisptacher.set(new RequestDispatcherImpl(this.registration.getApiId(), this.registration.getAuthToken(), this.httpClient, this));
        }
    }

    /* access modifiers changed from: protected */
    public <T> void setupAndExecuteTask(AbstractVerveTask<T> task, boolean mustBeRegistered) {
        task.setRequestDisptacher(this.requestDisptacher.get());
        task.setHttpClient(this.httpClient);
        task.setContext(this.context);
        task.setAppInfo(this.appInfo);
        task.setContentModel(this.contentModel);
        task.setApi(this);
        task.setPreferences(this.preferences);
        task.setCapiChangeListener(this);
        execute(task, mustBeRegistered);
    }

    public void reportContentView(DisplayBlock displayBlock, ContentItem item) {
        setupAndExecuteTask(new ReportPageViewTask(new PageView(item.getGuid(), Integer.valueOf(displayBlock.getId()), Integer.valueOf(item.getPartnerModuleId()))), false);
    }

    public void reportCustomView(String guid, Integer displayBlockId, Integer partnerModuleId) {
        setupAndExecuteTask(new ReportPageViewTask(new PageView(guid, displayBlockId, partnerModuleId)), false);
    }

    public void reportContentListing(DisplayBlock displayBlock) {
        setupAndExecuteTask(new ReportPageViewTask(new PageView(null, Integer.valueOf(displayBlock.getId()), Integer.valueOf(displayBlock.getPartnerModuleId()))), false);
    }

    public void flushPageviews() {
        if (isRegistered()) {
            Logger.logDebug("Flusing page view sessions");
            startNewSession(this.registration);
        }
    }

    public void reportMediaView(ContentItem item, MediaItem media) {
    }

    public void setCapiStatusListener(CapiStatusListener listener) {
        this.capiStatusListener = listener;
    }

    public void notifyCapiStatus(int apiStatus) {
        final boolean update;
        final boolean hierUpdate;
        final boolean rereg;
        boolean disabled;
        if ((apiStatus & 4) == 4) {
            update = true;
        } else {
            update = false;
        }
        if ((apiStatus & 2) == 2) {
            hierUpdate = true;
        } else {
            hierUpdate = false;
        }
        if ((apiStatus & 1) == 1) {
            rereg = true;
        } else {
            rereg = false;
        }
        if ((apiStatus & 16) == 16) {
            disabled = true;
        } else {
            disabled = false;
        }
        if (hierUpdate) {
            Logger.logDebug("Hieararchy refresh");
            HierachyTask.setLastUpdate(this.preferences, getLocale(), -1);
        }
        if (rereg || disabled) {
            Logger.logDebug("Reregistration required");
            RegistrationTask.setReregistrationRequired(this.preferences, true);
        }
        if (this.capiStatusListener != null) {
            this.handler.post(new Runnable() {
                public void run() {
                    if (update && !VerveImpl.this.updateNotified) {
                        boolean unused = VerveImpl.this.updateNotified = true;
                        VerveImpl.this.capiStatusListener.updateAvailable();
                    }
                    if (hierUpdate) {
                        VerveImpl.this.capiStatusListener.hiearachyUpdated();
                    }
                    if (rereg) {
                        VerveImpl.this.capiStatusListener.reregistrationRequired();
                    }
                }
            });
        }
    }
}
