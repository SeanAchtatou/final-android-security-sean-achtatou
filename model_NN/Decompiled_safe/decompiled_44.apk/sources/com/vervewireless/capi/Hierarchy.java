package com.vervewireless.capi;

import android.util.Xml;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class Hierarchy {
    static final /* synthetic */ boolean $assertionsDisabled = (!Hierarchy.class.desiredAssertionStatus());
    private int apiStatus = 0;
    private String id;
    private String name;
    private List<DisplayBlock> outlines;
    private String title;
    private String type;

    public void parseFromXml(InputStream input) throws IOException, XmlPullParserException {
        XmlPullParser parser = Xml.newPullParser();
        parser.setInput(input, null);
        for (int event = parser.next(); event != 1; event = parser.next()) {
            switch (event) {
                case CapiChangeListener.CAPI_STATUS_HIER:
                    processStartTag(parser);
                    break;
            }
        }
    }

    private void processStartTag(XmlPullParser parser) throws XmlPullParserException, IOException {
        String name2 = parser.getName();
        if (name2.equals("title")) {
            this.title = parser.nextText();
        } else if (name2.equals("hierarchy")) {
            this.type = parser.getAttributeValue(null, "type");
            this.name = parser.getAttributeValue(null, "name");
            this.id = parser.getAttributeValue(null, "id");
        } else if (name2.equals("outline")) {
            DisplayBlock outline = new DisplayBlock();
            outline.parse(parser);
            addOutline(outline);
        } else if (name2.equals("apistatus")) {
            setApiStatus(Integer.parseInt(parser.nextText()));
        }
    }

    public void addOutline(DisplayBlock outline) {
        if (this.outlines == null) {
            this.outlines = new ArrayList(5);
        }
        this.outlines.add(outline);
    }

    public int getApiStatus() {
        return this.apiStatus;
    }

    public void setApiStatus(int apiStatus2) {
        this.apiStatus = apiStatus2;
    }

    public List<DisplayBlock> getOutlines() {
        if (this.outlines == null || this.outlines.isEmpty()) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList(this.outlines);
    }

    public String getTitle() {
        return this.title;
    }

    public String getType() {
        return this.type;
    }

    public String getName() {
        return this.name;
    }

    public String getId() {
        return this.id;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public DisplayBlock getRoot() {
        if (this.outlines == null || this.outlines.isEmpty()) {
            return null;
        }
        if ($assertionsDisabled || this.outlines.size() == 1) {
            return this.outlines.get(0);
        }
        throw new AssertionError();
    }
}
