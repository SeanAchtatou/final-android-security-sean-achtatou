package com.vervewireless.capi;

import android.content.SharedPreferences;
import android.util.Xml;
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class RegistrationInfo {
    private String adBaseUrl;
    private String apiId;
    private String authToken;
    private String baseurl;
    private String hierarchyPage;
    private String iconUrl;
    private UserPreferences preferences = new UserPreferences();

    public String getIconUrl() {
        return this.iconUrl;
    }

    public String getHierachyPage() {
        return this.hierarchyPage;
    }

    public String getBaseUrl() {
        return this.baseurl;
    }

    static void clear(SharedPreferences sh) {
        SharedPreferences.Editor editor = sh.edit();
        editor.remove("registerTime");
        editor.remove("apiId");
        editor.remove("baseUrl");
        editor.remove("hierachyPage");
        editor.remove("authToken");
        editor.remove("iconUrl");
        editor.remove("adBaseUrl");
        editor.commit();
    }

    /* access modifiers changed from: package-private */
    public void save(SharedPreferences sh) {
        SharedPreferences.Editor editor = sh.edit();
        editor.putLong("registerTime", System.currentTimeMillis());
        editor.putString("apiId", getApiId());
        editor.putString("baseUrl", getBaseUrl());
        editor.putString("hierachyPage", getHierachyPage());
        editor.putString("authToken", getAuthToken());
        editor.putString("iconUrl", getIconUrl());
        editor.putString("adBaseUrl", getAdBaseUrl());
        if (!editor.commit()) {
            throw new IllegalStateException("Cannot comit to preferences");
        }
    }

    public static RegistrationInfo load(SharedPreferences sh) {
        String apiId2 = sh.getString("apiId", null);
        String token = sh.getString("authToken", null);
        if (apiId2 == null || apiId2.length() == 0 || token == null || token.length() == 0) {
            Logger.logDebug("No valid registration information");
            return null;
        }
        RegistrationInfo ri = new RegistrationInfo();
        ri.apiId = apiId2;
        ri.authToken = token;
        ri.baseurl = sh.getString("baseUrl", null);
        ri.hierarchyPage = sh.getString("hierachyPage", null);
        ri.iconUrl = sh.getString("iconUrl", null);
        ri.adBaseUrl = sh.getString("adBaseUrl", null);
        return ri;
    }

    /* access modifiers changed from: package-private */
    public String getAuthToken() {
        return this.authToken;
    }

    /* access modifiers changed from: package-private */
    public String getApiId() {
        return this.apiId;
    }

    public String getAdBaseUrl() {
        return this.adBaseUrl;
    }

    /* access modifiers changed from: protected */
    public void parseXml(InputStream input) throws IOException, VerveRegistrationError {
        XmlPullParser parser = Xml.newPullParser();
        try {
            parser.setInput(input, null);
            parseXml(parser);
        } catch (XmlPullParserException e) {
            throw new IllegalStateException(e);
        }
    }

    private void parseXml(XmlPullParser parser) throws XmlPullParserException, IOException, VerveRegistrationError {
        int eventType = parser.next();
        while (eventType != 1) {
            switch (eventType) {
                case CapiChangeListener.CAPI_STATUS_HIER:
                    String tag = parser.getName();
                    if (!tag.equals("contentapi")) {
                        if (!tag.equals("apiauth")) {
                            if (!tag.equals("errors")) {
                                if (!tag.equals("onlineads")) {
                                    if (!tag.equals("userprefs")) {
                                        Logger.logDebug("Unknown registration tag:" + tag);
                                        break;
                                    } else {
                                        this.preferences.parse(parser);
                                        break;
                                    }
                                } else {
                                    this.adBaseUrl = parser.getAttributeValue(null, "baseurl");
                                    break;
                                }
                            } else {
                                String error = parser.getAttributeValue(null, "description");
                                throw new VerveRegistrationError(error == null ? "Unknown error" : error);
                            }
                        } else {
                            this.apiId = parser.getAttributeValue(null, "id");
                            this.authToken = parser.getAttributeValue(null, "token");
                            break;
                        }
                    } else {
                        this.baseurl = parser.getAttributeValue(null, "baseurl");
                        this.hierarchyPage = parser.getAttributeValue(null, "hierpage");
                        this.iconUrl = parser.getAttributeValue(null, "icon_url");
                        break;
                    }
            }
            eventType = parser.next();
        }
        if (this.apiId == null || this.apiId.length() == 0 || this.authToken == null || this.authToken.length() == 0 || this.baseurl == null || this.baseurl.length() == 0) {
            throw new IllegalStateException("Invalid response");
        }
    }

    public UserPreferences getPreferences() {
        return this.preferences;
    }

    public void setPreferences(UserPreferences preferences2) {
        this.preferences = preferences2;
    }
}
