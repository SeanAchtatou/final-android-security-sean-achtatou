package com.vervewireless.capi;

import java.io.IOException;
import java.net.UnknownHostException;

public class VerveError {
    private Cause cause;
    private String message;

    public enum Cause {
        NetworkConnectionError,
        RegistrationError,
        UnknownError
    }

    public VerveError(Cause cause2, String message2) {
        this.message = message2;
        this.cause = cause2;
    }

    private VerveError(Exception cause2) {
        this.message = cause2.getMessage() == null ? "Unknown error" : cause2.getMessage();
    }

    public VerveError(String error) {
        this.message = error;
    }

    public Cause getCause() {
        return this.cause;
    }

    public String toString() {
        return this.message;
    }

    public static VerveError createFromException(Exception exception) {
        if (exception instanceof UnknownHostException) {
            return new VerveError(Cause.NetworkConnectionError, "Cannot reach the news server. Please check your network connection and try again later");
        }
        if (exception instanceof IOException) {
            return new VerveError(Cause.NetworkConnectionError, "Network error. Please check your network connection and try again later");
        }
        if (exception instanceof VerveRegistrationError) {
            return new VerveError(Cause.RegistrationError, exception.getMessage());
        }
        return new VerveError(exception);
    }
}
