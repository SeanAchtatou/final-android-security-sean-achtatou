package com.vervewireless.capi;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

class VerveUtils {
    private static final SimpleDateFormat ISO_8601 = new SimpleDateFormat(ISO_8601_FRM);
    private static final String ISO_8601_FRM = "yyyy-MM-dd'T'HH:mm:ss";
    static final String MEDIA_NS = "http://search.yahoo.com/mrss/";
    static String PAGE_VIEW_HISTORY_NS = "http://www.vervewireless.com/xsd/capi/pageViewHistory";

    private VerveUtils() {
    }

    public static String toHexString(byte[] digest) {
        StringBuilder hash = new StringBuilder(digest.length * 2);
        for (int i = 0; i < digest.length; i++) {
            hash.append(Integer.toHexString((digest[i] >>> 4) & 15));
            hash.append(Integer.toHexString(digest[i] & 15));
        }
        return hash.toString();
    }

    public static String toISO(Date date) {
        return ISO_8601.format(date);
    }

    public static Date fromIso(String date) {
        try {
            return ISO_8601.parse(date);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Not in ISO 8601 format:" + date, e);
        }
    }

    public static <T> String join(Collection<T> collection, String delimiter, String quote) {
        StringBuilder sb = new StringBuilder();
        for (T t : collection) {
            if (sb.length() > 0) {
                sb.append(delimiter);
            }
            sb.append(quote).append((Object) t).append(quote);
        }
        return sb.toString();
    }

    public static <T> String join(Collection<T> collection, String delimiter) {
        return join(collection, delimiter, "");
    }

    public static void skipToTag(XmlPullParser parser, String tagName) throws XmlPullParserException, IOException {
        int e = parser.next();
        while (e != 1) {
            if (e != 2 || !parser.getName().equals(tagName)) {
                e = parser.next();
            } else {
                return;
            }
        }
        throw new IllegalArgumentException("No such tag:" + tagName);
    }

    public static boolean isValid(String str) {
        return str != null && str.length() > 0;
    }
}
