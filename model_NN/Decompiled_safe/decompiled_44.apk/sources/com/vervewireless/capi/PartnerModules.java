package com.vervewireless.capi;

public final class PartnerModules {
    public static int ABOUT = -304;
    public static int CATEGORY_INDEX = -302;
    public static int LEGAL = -305;
    public static int SAVED_STORIES = -310;
    public static int SEARCH = -303;
    public static int SETTINGS = -306;
    public static int TOP_LEVEL = -301;
}
