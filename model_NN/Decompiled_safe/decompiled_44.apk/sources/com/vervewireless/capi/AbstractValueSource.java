package com.vervewireless.capi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

abstract class AbstractValueSource implements ValueSource {
    AbstractValueSource() {
    }

    public Date getDate(String key, Date defValue) {
        String val = getValue(key, null);
        if (val == null) {
            return defValue;
        }
        try {
            return new SimpleDateFormat("EEE, dd MMM yyyy k:m:s zzz").parse(val);
        } catch (ParseException pe) {
            Logger.logWarning("Could not parse date:" + val, pe);
            return defValue;
        }
    }

    public int getInt(String key, int defValue) {
        String val = getValue(key, null);
        if (val == null) {
            return defValue;
        }
        int ret = defValue;
        try {
            ret = Integer.parseInt(val);
        } catch (NumberFormatException nfe) {
            Logger.logWarning("Failed to parse integer value:" + val, nfe);
        }
        return ret;
    }

    public long getLong(String key, long defValue) {
        String val = getValue(key, null);
        if (val == null) {
            return defValue;
        }
        long ret = defValue;
        try {
            ret = Long.parseLong(val);
        } catch (NumberFormatException nfe) {
            Logger.logWarning("Failed to parse integer value:" + val, nfe);
        }
        return ret;
    }

    public float getFloat(String key, float defValue) {
        String val = getValue(key, null);
        if (val == null) {
            return defValue;
        }
        float ret = defValue;
        try {
            ret = Float.parseFloat(val);
        } catch (NumberFormatException nfe) {
            Logger.logWarning("Failed to parse float value:" + val, nfe);
        }
        return ret;
    }

    public boolean getBoolean(String key, boolean defValue) {
        String val = getValue(key, null);
        if (val == null) {
            return defValue;
        }
        return "1".equals(val) || Boolean.parseBoolean(val);
    }
}
