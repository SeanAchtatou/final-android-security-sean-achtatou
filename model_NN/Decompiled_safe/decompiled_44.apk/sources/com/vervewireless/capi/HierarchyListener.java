package com.vervewireless.capi;

public interface HierarchyListener {
    void onHierarchyFailed(VerveError verveError);

    void onHierarchyRecieved(DisplayBlock displayBlock);
}
