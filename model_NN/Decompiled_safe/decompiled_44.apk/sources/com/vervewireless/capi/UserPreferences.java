package com.vervewireless.capi;

import android.util.Xml;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class UserPreferences extends AbstractValueSource {
    private Map<String, String> values = new HashMap();

    public /* bridge */ /* synthetic */ boolean getBoolean(String x0, boolean x1) {
        return super.getBoolean(x0, x1);
    }

    public /* bridge */ /* synthetic */ Date getDate(String x0, Date x1) {
        return super.getDate(x0, x1);
    }

    public /* bridge */ /* synthetic */ float getFloat(String x0, float x1) {
        return super.getFloat(x0, x1);
    }

    public /* bridge */ /* synthetic */ int getInt(String x0, int x1) {
        return super.getInt(x0, x1);
    }

    public /* bridge */ /* synthetic */ long getLong(String x0, long x1) {
        return super.getLong(x0, x1);
    }

    public void setValue(String key, String value) {
        if (value == null) {
            this.values.remove(key);
        } else {
            this.values.put(key, value);
        }
    }

    public String getValue(String key, String defValue) {
        String ret = this.values.get(key);
        return ret == null ? defValue : ret;
    }

    /* access modifiers changed from: package-private */
    public void parse(InputStream input) throws XmlPullParserException, IOException {
        XmlPullParser parser = Xml.newPullParser();
        parser.setInput(input, null);
        VerveUtils.skipToTag(parser, "userprefs");
        parse(parser);
    }

    /* access modifiers changed from: package-private */
    public void parse(XmlPullParser input) throws XmlPullParserException, IOException {
        input.require(2, null, "userprefs");
        while (true) {
            int event = input.next();
            if (event == 3 && "userprefs".equals(input.getName())) {
                return;
            }
            if (event == 2 && "userpref".equals(input.getName())) {
                setValue(input.getAttributeValue(null, "name"), input.nextText());
            }
        }
    }

    public Map<String, String> entries() {
        return this.values;
    }
}
