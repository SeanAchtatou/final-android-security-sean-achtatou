package com.vervewireless.capi;

import android.net.Uri;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

class RequestDispatcherImpl implements RequestDisptacher {
    private static AtomicBoolean hasTriedReregister = new AtomicBoolean(false);
    private final String apiId;
    private final String authToken;
    private final CapiChangeListener capiChangeListener;
    private final HttpClient httpClient;
    private AtomicLong nonce = new AtomicLong();

    public RequestDispatcherImpl(String apiId2, String authToken2, HttpClient httpClient2, CapiChangeListener capiChangeListener2) {
        this.apiId = apiId2;
        this.authToken = authToken2;
        this.httpClient = httpClient2;
        this.capiChangeListener = capiChangeListener2;
        this.nonce.set(System.currentTimeMillis());
    }

    public HttpResponse doPost(HttpPost postRequest) throws IOException {
        postRequest.setURI(URI.create(signRequest(postRequest.getURI().toString(), true)));
        Logger.logDebug("POSTING " + postRequest.getURI().toString());
        HttpResponse response = this.httpClient.execute(postRequest);
        if (response.getStatusLine().getStatusCode() == 403 && hasTriedReregister.compareAndSet(false, true)) {
            this.capiChangeListener.notifyCapiStatus(1);
        }
        return response;
    }

    public HttpResponse doGet(String url) throws IOException {
        HttpGet getRequest = new HttpGet(signRequest(url, false));
        getRequest.setHeader("Accept-Encoding", "gzip");
        Logger.logDebug("GETing " + getRequest.getURI().toString());
        HttpResponse response = this.httpClient.execute(getRequest);
        if (response.getStatusLine().getStatusCode() == 403 && hasTriedReregister.compareAndSet(false, true)) {
            this.capiChangeListener.notifyCapiStatus(1);
        }
        for (Header h : response.getAllHeaders()) {
            Logger.logDebug(h.getName() + ":" + h.getValue());
        }
        Logger.logDebug("Got response" + getRequest.getURI().toString());
        return response;
    }

    public String signRequest(String url, boolean isPost) {
        String baseUrl;
        long nonce2 = this.nonce.incrementAndGet();
        int queryIdx = url.indexOf(63);
        if (queryIdx > 0) {
            baseUrl = url.substring(0, queryIdx);
        } else {
            baseUrl = url;
        }
        Logger.logDebug("Signing " + baseUrl);
        StringBuilder signature = new StringBuilder();
        signature.append(this.apiId);
        signature.append(isPost ? ":POST:" : ":GET:");
        signature.append(baseUrl);
        signature.append(":").append(this.authToken).append(":");
        signature.append(nonce2);
        try {
            byte[] digest = MessageDigest.getInstance("SHA1").digest(signature.toString().getBytes("UTF8"));
            Uri.Builder uriBuilder = Uri.parse(url.toString()).buildUpon();
            uriBuilder.appendQueryParameter("vid", this.apiId);
            uriBuilder.appendQueryParameter("vnonce", String.valueOf(nonce2));
            uriBuilder.appendQueryParameter("vdigest", VerveUtils.toHexString(digest));
            return uriBuilder.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("SHA1 support is required");
        } catch (UnsupportedEncodingException e2) {
            throw new IllegalStateException("UTF8 support is required");
        }
    }
}
