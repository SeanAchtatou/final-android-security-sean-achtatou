package com.vervewireless.capi;

import android.content.ContentValues;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;

class Logger {
    static final boolean DEBUG = false;
    static final boolean DUMP_STREAM = false;
    private static final String VERVEAPI = "verveapi";

    private Logger() {
    }

    public static void logDebug(String msg) {
    }

    public static void logDebug(String msg, Throwable exception) {
    }

    public static void logWarning(String msg, Throwable th) {
        Log.w(VERVEAPI, msg, th);
    }

    public static void logWarning(String msg) {
        Log.w(VERVEAPI, msg);
    }

    public static boolean isDebugEnabled() {
        return false;
    }

    public static InputStream debugDump(String string, InputStream stream) throws IOException {
        return stream;
    }

    public static void assertTrue(boolean expresion) {
    }

    public static void logDBInsert(String table, ContentValues values) {
    }

    public static void logDBUpdate(String table, ContentValues values) {
    }
}
