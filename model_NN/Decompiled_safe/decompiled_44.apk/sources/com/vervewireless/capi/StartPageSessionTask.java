package com.vervewireless.capi;

import android.content.SharedPreferences;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Xml;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.xmlpull.v1.XmlSerializer;

class StartPageSessionTask extends AbstractVerveTask<PageViewSession> {
    private final String baseUrl;
    private final String clientId;
    private final String network;

    public StartPageSessionTask(NetworkInfo netInfo, RegistrationInfo info) {
        this.baseUrl = info.getBaseUrl();
        this.clientId = info.getApiId();
        this.network = PageViewSession.getNetworkName(netInfo);
    }

    public PageViewSession call() throws Exception {
        try {
            uploadReports();
        } catch (IOException ex) {
            Logger.logWarning("Failed to upload batch reports, will retry later", ex);
        }
        return getContentModel().createPageViewSession(this.network);
    }

    private void uploadReports() throws IOException {
        ContentModel contentModel = getContentModel();
        for (PageViewSession session : contentModel.getPageViewSessions()) {
            uploadSession(session, contentModel.getPageViews(session.getId()));
        }
    }

    private void uploadSession(PageViewSession session, List<PageView> pageViews) throws IOException {
        if (pageViews.isEmpty()) {
            Logger.logDebug("Empty session, not reporting to server:" + session.getId());
            getContentModel().removePageViewSession(session.getId());
            return;
        }
        int batch_id = getPreferences().getInt("batch_id", 1);
        ApiInfo app = getAppInfo();
        XmlSerializer xml = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        xml.setOutput(writer);
        xml.startDocument("UTF8", true);
        xml.startTag(VerveUtils.PAGE_VIEW_HISTORY_NS, "pageViewHistory");
        xml.attribute(null, "batch_id", String.valueOf(batch_id));
        xml.attribute(null, "client_id", this.clientId);
        xml.attribute(null, "app", app.getApplicationId() + ":" + app.getApplicationVersion());
        session.toXml(xml, pageViews);
        xml.endTag(VerveUtils.PAGE_VIEW_HISTORY_NS, "pageViewHistory");
        xml.endDocument();
        byte[] data = writer.toString().getBytes();
        InputStream body = Logger.debugDump("Uploading batch report", new ByteArrayInputStream(data));
        Uri.Builder builder = Uri.parse(this.baseUrl).buildUpon();
        builder.appendEncodedPath("batchviews");
        if (Logger.isDebugEnabled()) {
            builder.appendQueryParameter("showDebugMessage", "received");
        }
        HttpPost post = new HttpPost(builder.toString());
        post.setEntity(new InputStreamEntity(body, (long) data.length));
        post.setHeader("Content-Type", "application/xml");
        HttpResponse ret = getRequestDisptacher().doPost(post);
        Logger.debugDump("Uploading page views", getStream(ret));
        if (ret.getStatusLine().getStatusCode() == 201) {
            SharedPreferences.Editor editor = getPreferences().edit();
            editor.putInt("batch_id", batch_id + 1);
            editor.commit();
            getContentModel().removePageViewSession(session.getId());
            return;
        }
        Logger.logDebug("Failed with status " + ret.getStatusLine().getStatusCode());
    }

    public void finishedSuccessfully(PageViewSession result) {
    }

    public void failed(Exception cause) {
    }
}
