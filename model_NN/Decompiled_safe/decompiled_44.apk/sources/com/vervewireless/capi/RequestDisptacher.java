package com.vervewireless.capi;

import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;

public interface RequestDisptacher {
    HttpResponse doGet(String str) throws IOException;

    HttpResponse doPost(HttpPost httpPost) throws IOException;
}
