package com.vervewireless.capi;

import android.database.Cursor;
import java.util.Date;

class CursorSource extends AbstractValueSource {
    private final Cursor cursor;

    public CursorSource(Cursor cursor2) {
        this.cursor = cursor2;
    }

    public Date getDate(String key, Date defValue) {
        int index = this.cursor.getColumnIndex(key);
        if (index < 0) {
            return defValue;
        }
        return new Date(this.cursor.getLong(index));
    }

    public String getValue(String key, String defValue) {
        int index = this.cursor.getColumnIndex(key);
        if (index < 0) {
            return defValue;
        }
        return this.cursor.getString(index);
    }

    public int getInt(String key, int defValue) {
        int index = this.cursor.getColumnIndex(key);
        if (index < 0) {
            return defValue;
        }
        return this.cursor.getInt(index);
    }

    public float getFloat(String key, float defValue) {
        int index = this.cursor.getColumnIndex(key);
        if (index < 0) {
            return defValue;
        }
        return this.cursor.getFloat(index);
    }

    public long getLong(String key, long defValue) {
        int index = this.cursor.getColumnIndex(key);
        if (index < 0) {
            return defValue;
        }
        return this.cursor.getLong(index);
    }
}
