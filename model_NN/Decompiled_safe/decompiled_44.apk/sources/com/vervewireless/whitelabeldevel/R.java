package com.vervewireless.whitelabeldevel;

public final class R {

    public static final class anim {
        public static final int popup_hide = 2130968576;
        public static final int popup_hide_initial = 2130968577;
        public static final int popup_show = 2130968578;
        public static final int slide_left_in = 2130968579;
        public static final int slide_left_out = 2130968580;
        public static final int slide_right_in = 2130968581;
        public static final int slide_right_out = 2130968582;
        public static final int slide_top_to_bottom = 2130968583;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int app_navtint_center = 2131099649;
        public static final int app_navtint_finish = 2131099650;
        public static final int app_navtint_start = 2131099648;
        public static final int bright_foreground_dark = 2131099651;
        public static final int bright_foreground_dark_disabled = 2131099652;
        public static final int bright_foreground_dark_inverse = 2131099653;
        public static final int bright_foreground_light = 2131099666;
        public static final int bright_text_dark_focused = 2131099681;
        public static final int default_background = 2131099667;
        public static final int graph_bottom = 2131099665;
        public static final int graph_color_0 = 2131099668;
        public static final int graph_color_1 = 2131099669;
        public static final int graph_color_10 = 2131099678;
        public static final int graph_color_2 = 2131099670;
        public static final int graph_color_3 = 2131099671;
        public static final int graph_color_4 = 2131099672;
        public static final int graph_color_5 = 2131099673;
        public static final int graph_color_6 = 2131099674;
        public static final int graph_color_7 = 2131099675;
        public static final int graph_color_8 = 2131099676;
        public static final int graph_color_9 = 2131099677;
        public static final int graph_color_others = 2131099679;
        public static final int graph_top = 2131099664;
        public static final int solid_bg_gray = 2131099662;
        public static final int solid_black = 2131099657;
        public static final int solid_blue = 2131099655;
        public static final int solid_gray = 2131099658;
        public static final int solid_green = 2131099660;
        public static final int solid_orange = 2131099659;
        public static final int solid_red = 2131099654;
        public static final int solid_title_gray = 2131099663;
        public static final int solid_white = 2131099661;
        public static final int solid_yellow = 2131099656;
        public static final int translucent_black = 2131099680;
    }

    public static final class drawable {
        public static final int arrow = 2130837504;
        public static final int arrow_cat_left = 2130837505;
        public static final int arrow_cat_right = 2130837506;
        public static final int arrow_left = 2130837507;
        public static final int black_white_gradient = 2130837508;
        public static final int blue = 2130837579;
        public static final int btn_camera_arrow_left = 2130837509;
        public static final int btn_camera_arrow_left_default = 2130837510;
        public static final int btn_camera_arrow_left_press = 2130837511;
        public static final int btn_camera_arrow_left_selected = 2130837512;
        public static final int btn_camera_arrow_right = 2130837513;
        public static final int btn_camera_arrow_right_default = 2130837514;
        public static final int btn_camera_arrow_right_press = 2130837515;
        public static final int btn_camera_arrow_right_selected = 2130837516;
        public static final int btn_category_colors = 2130837517;
        public static final int btn_circle = 2130837518;
        public static final int btn_circle_disable = 2130837519;
        public static final int btn_circle_disable_focused = 2130837520;
        public static final int btn_circle_normal = 2130837521;
        public static final int btn_circle_pressed = 2130837522;
        public static final int btn_circle_selected = 2130837523;
        public static final int bundle_title_background = 2130837524;
        public static final int categories_background = 2130837525;
        public static final int category_menuitem_background = 2130837526;
        public static final int category_menuitem_colors = 2130837527;
        public static final int category_selector = 2130837528;
        public static final int category_selector_focused = 2130837529;
        public static final int dragitem_selector = 2130837530;
        public static final int gray = 2130837582;
        public static final int green = 2130837580;
        public static final int ic_btn_round_more = 2130837531;
        public static final int ic_btn_round_more_disabled = 2130837532;
        public static final int ic_btn_round_more_normal = 2130837533;
        public static final int ic_btn_search = 2130837534;
        public static final int ic_menu_add = 2130837535;
        public static final int ic_menu_back = 2130837536;
        public static final int ic_menu_block = 2130837537;
        public static final int ic_menu_camera = 2130837538;
        public static final int ic_menu_close_clear_cancel = 2130837539;
        public static final int ic_menu_delete = 2130837540;
        public static final int ic_menu_edit = 2130837541;
        public static final int ic_menu_forward = 2130837542;
        public static final int ic_menu_gallery = 2130837543;
        public static final int ic_menu_help = 2130837544;
        public static final int ic_menu_home = 2130837545;
        public static final int ic_menu_info_details = 2130837546;
        public static final int ic_menu_login = 2130837547;
        public static final int ic_menu_more = 2130837548;
        public static final int ic_menu_notifications = 2130837549;
        public static final int ic_menu_play_clip = 2130837550;
        public static final int ic_menu_preferences = 2130837551;
        public static final int ic_menu_refresh = 2130837552;
        public static final int ic_menu_revert = 2130837553;
        public static final int ic_menu_rotate = 2130837554;
        public static final int ic_menu_save = 2130837555;
        public static final int ic_menu_search = 2130837556;
        public static final int ic_menu_send = 2130837557;
        public static final int ic_menu_set_as = 2130837558;
        public static final int ic_menu_share = 2130837559;
        public static final int ic_menu_slideshow = 2130837560;
        public static final int ic_menu_sort_by_size = 2130837561;
        public static final int ic_menu_star = 2130837562;
        public static final int ic_menu_stop = 2130837563;
        public static final int ic_menu_upload = 2130837564;
        public static final int ic_menu_view = 2130837565;
        public static final int ic_menu_zoom = 2130837566;
        public static final int ic_more = 2130837567;
        public static final int icon = 2130837568;
        public static final int icon_more = 2130837569;
        public static final int main_menu_background = 2130837570;
        public static final int main_menu_selectedbackground = 2130837571;
        public static final int main_menuitem_colors = 2130837572;
        public static final int main_menuitem_selector = 2130837573;
        public static final int menu_selected = 2130837574;
        public static final int noitems = 2130837575;
        public static final int readme = 2130837576;
        public static final int red = 2130837578;
        public static final int screen_background_black = 2130837584;
        public static final int translucent_background = 2130837585;
        public static final int transparent_background = 2130837586;
        public static final int verve_identifier_white = 2130837577;
        public static final int white = 2130837583;
        public static final int yellow = 2130837581;
    }

    public static final class id {
        public static final int adview = 2131165204;
        public static final int arrLeft = 2131165187;
        public static final int arrRight = 2131165188;
        public static final int btnMenu1 = 2131165211;
        public static final int btnMenu2 = 2131165212;
        public static final int btnMenu3 = 2131165213;
        public static final int btnMenu4 = 2131165214;
        public static final int btnMenu5 = 2131165215;
        public static final int btnNext = 2131165198;
        public static final int btnPrev = 2131165197;
        public static final int dragItem = 2131165194;
        public static final int dragPanel = 2131165192;
        public static final int editLatitude = 2131165207;
        public static final int editLongitude = 2131165209;
        public static final int editPostal = 2131165225;
        public static final int editSearch = 2131165226;
        public static final int gridThumbs = 2131165202;
        public static final int imgHeader = 2131165233;
        public static final int imgIcon = 2131165216;
        public static final int imgMore = 2131165218;
        public static final int imgShare = 2131165230;
        public static final int imgSplash = 2131165232;
        public static final int itemsPanel = 2131165205;
        public static final int layoutButtons = 2131165189;
        public static final int layoutCategories = 2131165186;
        public static final int layoutTitle = 2131165190;
        public static final int listMenu = 2131165219;
        public static final int listNews = 2131165222;
        public static final int listSettings = 2131165229;
        public static final int listVideos = 2131165237;
        public static final int mainFlipperPanel = 2131165210;
        public static final int mainMenu = 2131165193;
        public static final int mainPanel = 2131165203;
        public static final int popup_window = 2131165199;
        public static final int scrollCategories = 2131165185;
        public static final int textCategoryTitle = 2131165191;
        public static final int textEmpty = 2131165201;
        public static final int textLeft = 2131165234;
        public static final int textLocale = 2131165217;
        public static final int textNews = 2131165220;
        public static final int textNewsDate = 2131165221;
        public static final int textRight = 2131165235;
        public static final int textShare = 2131165231;
        public static final int textSubtitle = 2131165228;
        public static final int textTitle = 2131165227;
        public static final int title = 2131165195;
        public static final int txtError = 2131165223;
        public static final int txtLatitude = 2131165206;
        public static final int txtLongitude = 2131165208;
        public static final int txtPostal = 2131165224;
        public static final int txtcaption = 2131165200;
        public static final int verve_identifier = 2131165236;
        public static final int webAdvert = 2131165184;
        public static final int webView = 2131165196;
    }

    public static final class layout {
        public static final int adview = 2130903040;
        public static final int category_menu = 2130903041;
        public static final int category_menuitem = 2130903042;
        public static final int category_row = 2130903043;
        public static final int customizescreen = 2130903044;
        public static final int dragitem = 2130903045;
        public static final int galleryscreen = 2130903046;
        public static final int gallerythumbsscreen = 2130903047;
        public static final int homescreen = 2130903048;
        public static final int itemsscreen = 2130903049;
        public static final int locale_row = 2130903050;
        public static final int locationdialog = 2130903051;
        public static final int main = 2130903052;
        public static final int main_menu = 2130903053;
        public static final int menu_row = 2130903054;
        public static final int menuscreen = 2130903055;
        public static final int news_row = 2130903056;
        public static final int newsdetailsscreen = 2130903057;
        public static final int newsscreen = 2130903058;
        public static final int noitems = 2130903059;
        public static final int postaldialog = 2130903060;
        public static final int search = 2130903061;
        public static final int settings_row = 2130903062;
        public static final int settings_single_row = 2130903063;
        public static final int settingsscreen = 2130903064;
        public static final int share_row = 2130903065;
        public static final int splashscreen = 2130903066;
        public static final int title = 2130903067;
        public static final int verve_branding = 2130903068;
        public static final int videoscreen = 2130903069;
    }

    public static final class string {
        public static final int app_copyright = 2131034115;
        public static final int app_name = 2131034112;
        public static final int app_reg_key = 2131034113;
        public static final int app_reg_version = 2131034114;
        public static final int updateAvailable = 2131034116;
    }
}
