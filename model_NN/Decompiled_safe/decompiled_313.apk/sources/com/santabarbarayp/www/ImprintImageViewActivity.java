package com.santabarbarayp.www;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.FloatMath;
import android.view.Display;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class ImprintImageViewActivity extends Activity implements View.OnTouchListener, GestureDetector.OnGestureListener {
    static final int DRAG = 1;
    static final float MAX_ZOOM = 10.0f;
    static final float MINIMUM_DISTANCE = 10.0f;
    static final float MIN_ZOOM = 0.1f;
    public static final int MenuEmail = 2;
    static final int NONE = 0;
    static final float SCREEN_MINIMUM_SPAN = 0.02f;
    private static final long TIME_SPAN_MILLS = 4000;
    static final int ZOOM = 2;
    static final float ZOOM_IN_NUMBER = 1.25f;
    static final float ZOOM_OUT_NUMBER = 0.8f;
    boolean IsFromMapListView = false;
    boolean isSecondDouble = true;
    boolean isZoomInControlShoubleBeVisible = true;
    boolean isZoomOutControlShoubleBeVisible = true;
    private Bitmap mBitmap;
    float mDeltaX = 0.0f;
    float mDeltaY = 0.0f;
    float mEventX = 0.0f;
    float mEventY = 0.0f;
    float mFirstOldDist = 1.0f;
    private GestureDetector mGestureDetector;
    ImageMatrix mImageMatrix = new ImageMatrix();
    ImageMatrix mInitialImageMatrix = new ImageMatrix();
    PointF mNewStart = new PointF();
    int mScreenHeight;
    int mScreenWidth;
    private Handler mShopLocalDataHandler = new Handler();
    RelativeLayout mZoomCcontrols;
    private Runnable mZoomControlsUpdateTask = new Runnable() {
        public void run() {
            ImprintImageViewActivity.this.mZoomCcontrols.setVisibility(8);
        }
    };
    ImageButton mZoomInButton;
    ImageButton mZoomOutButton;
    Matrix matrix = new Matrix();
    int mode = NONE;
    /* access modifiers changed from: private */
    public ImageView myAccessoryImageView;
    private ImprintAccessory myImprintAccessory;
    float prevOldDist = 1.0f;
    PointF prevStart = new PointF();
    Matrix savedMatrix = new Matrix();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.imprint_image_view);
        Display display = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        this.mScreenWidth = display.getWidth();
        this.mScreenHeight = display.getHeight();
        Bundle extras = getIntent().getExtras();
        this.myImprintAccessory = ImprintAccessory.fromBundle(extras);
        this.IsFromMapListView = extras.getBoolean("IsFromMapListView", false);
        this.mBitmap = BitmapFactory.decodeFile(this.myImprintAccessory.getPhysicalPath());
        calculateImageMatrix();
        this.isSecondDouble = true;
        setTitle(String.valueOf(this.myImprintAccessory.getTitle()) + " View");
        this.myAccessoryImageView = (ImageView) findViewById(R.id.imprint_accessory_imageView);
        this.mZoomCcontrols = (RelativeLayout) findViewById(R.id.zoom_control_view);
        this.mZoomInButton = (ImageButton) findViewById(R.id.zoom_in_button);
        this.mZoomOutButton = (ImageButton) findViewById(R.id.zoom_out_button);
        this.mZoomInButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ImprintImageViewActivity.this.zoomIn();
            }
        });
        this.mZoomOutButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ImprintImageViewActivity.this.zoomOut();
            }
        });
        this.myAccessoryImageView.setOnTouchListener(this);
        this.mGestureDetector = new GestureDetector(this);
        this.mGestureDetector.setOnDoubleTapListener(new GestureDetector.OnDoubleTapListener() {
            public boolean onDoubleTap(MotionEvent e) {
                ImprintImageViewActivity.this.isZoomInControlShoubleBeVisible = true;
                ImprintImageViewActivity.this.isZoomOutControlShoubleBeVisible = true;
                ImprintImageViewActivity.this.mZoomInButton.setEnabled(ImprintImageViewActivity.this.isZoomInControlShoubleBeVisible);
                ImprintImageViewActivity.this.mZoomOutButton.setEnabled(ImprintImageViewActivity.this.isZoomOutControlShoubleBeVisible);
                if (!ImprintImageViewActivity.this.isSecondDouble) {
                    ImprintImageViewActivity.this.mImageMatrix.setX(ImprintImageViewActivity.this.mInitialImageMatrix.getX());
                    ImprintImageViewActivity.this.mImageMatrix.setY(ImprintImageViewActivity.this.mInitialImageMatrix.getY());
                    ImprintImageViewActivity.this.mImageMatrix.setCenterX(ImprintImageViewActivity.this.mInitialImageMatrix.getCenterX());
                    ImprintImageViewActivity.this.mImageMatrix.setCenterY(ImprintImageViewActivity.this.mInitialImageMatrix.getCenterY());
                    ImprintImageViewActivity.this.mImageMatrix.setScale(ImprintImageViewActivity.this.mInitialImageMatrix.getScale());
                } else {
                    ImprintImageViewActivity.this.mImageMatrix.setX(ImprintImageViewActivity.this.mInitialImageMatrix.getDoubleX());
                    ImprintImageViewActivity.this.mImageMatrix.setY(ImprintImageViewActivity.this.mInitialImageMatrix.getDoubleY());
                    ImprintImageViewActivity.this.mImageMatrix.setCenterX(ImprintImageViewActivity.this.mInitialImageMatrix.getCenterX());
                    ImprintImageViewActivity.this.mImageMatrix.setCenterY(ImprintImageViewActivity.this.mInitialImageMatrix.getCenterY());
                    ImprintImageViewActivity.this.mImageMatrix.setScale(ImprintImageViewActivity.this.mInitialImageMatrix.getScale() * 2.0f);
                }
                ImprintImageViewActivity.this.matrix = new Matrix();
                ImprintImageViewActivity.this.matrix.postScale(ImprintImageViewActivity.this.mImageMatrix.getScale(), ImprintImageViewActivity.this.mImageMatrix.getScale());
                ImprintImageViewActivity.this.matrix.postTranslate(ImprintImageViewActivity.this.mImageMatrix.getX(), ImprintImageViewActivity.this.mImageMatrix.getY());
                ImprintImageViewActivity.this.myAccessoryImageView.setImageMatrix(ImprintImageViewActivity.this.matrix);
                ImprintImageViewActivity.this.savedMatrix.set(ImprintImageViewActivity.this.matrix);
                ImprintImageViewActivity.this.isSecondDouble = ImprintImageViewActivity.this.isSecondDouble ? false : ImprintImageViewActivity.DRAG;
                return true;
            }

            public boolean onDoubleTapEvent(MotionEvent e) {
                return true;
            }

            public boolean onSingleTapConfirmed(MotionEvent e) {
                return false;
            }
        });
        this.myAccessoryImageView.setImageBitmap(this.mBitmap);
        this.matrix = new Matrix();
        this.matrix.postScale(this.mInitialImageMatrix.getScale(), this.mInitialImageMatrix.getScale());
        this.matrix.postTranslate(this.mInitialImageMatrix.getX(), this.mInitialImageMatrix.getY());
        this.myAccessoryImageView.setImageMatrix(this.matrix);
        this.savedMatrix.set(this.matrix);
        this.mShopLocalDataHandler.postDelayed(this.mZoomControlsUpdateTask, TIME_SPAN_MILLS);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        getWindow().setFlags(1024, 1024);
        Display display = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        this.mScreenWidth = display.getWidth();
        this.mScreenHeight = display.getHeight();
        this.isSecondDouble = true;
        calculateImageMatrix();
        this.matrix = new Matrix();
        this.matrix.postScale(this.mInitialImageMatrix.getScale(), this.mInitialImageMatrix.getScale());
        this.matrix.postTranslate(this.mInitialImageMatrix.getX(), this.mInitialImageMatrix.getY());
        this.myAccessoryImageView.setImageMatrix(this.matrix);
        this.savedMatrix.set(this.matrix);
        super.onConfigurationChanged(newConfig);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        populateMenu(menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        return applyMenuChoice(item) || super.onOptionsItemSelected(item);
    }

    public void populateMenu(Menu menu) {
        menu.setQwertyMode(true);
        MenuItem itemEmail = menu.add((int) NONE, 2, (int) NONE, "Send");
        itemEmail.setAlphabeticShortcut('s');
        itemEmail.setIcon((int) R.drawable.menu_email);
    }

    public boolean applyMenuChoice(MenuItem item) {
        switch (item.getItemId()) {
            case 2:
                Intent sendIntent = new Intent("android.intent.action.SEND");
                sendIntent.putExtra("android.intent.extra.EMAIL", new String[NONE]);
                sendIntent.putExtra("android.intent.extra.CC", new String[NONE]);
                sendIntent.putExtra("android.intent.extra.SUBJECT", String.valueOf(this.myImprintAccessory.getTitle()) + " View");
                sendIntent.putExtra("android.intent.extra.TEXT", String.valueOf(this.myImprintAccessory.getTitle()) + " View attached");
                sendIntent.putExtra("android.intent.extra.STREAM", Uri.parse("file://" + this.myImprintAccessory.getPhysicalPath()));
                sendIntent.setType("image/*");
                startActivity(Intent.createChooser(sendIntent, "Sending " + this.myImprintAccessory.getTitle()));
                return true;
            default:
                return NONE;
        }
    }

    public boolean onTouch(View v, MotionEvent rawEvent) {
        WrapMotionEvent event = WrapMotionEvent.wrap(rawEvent);
        ImageView view = (ImageView) v;
        switch (event.getAction() & 255) {
            case NONE /*0*/:
                this.savedMatrix.set(this.matrix);
                this.mNewStart.set(event.getX(), event.getY());
                this.prevStart.set(event.getX(), event.getY());
                this.mode = DRAG;
                this.mDeltaX = NONE;
                this.mDeltaY = NONE;
                this.mEventX = event.getX();
                this.mEventY = event.getY();
                break;
            case DRAG /*1*/:
            case ImprintDetailActivity.MenuAdvertisement /*6*/:
                this.mode = NONE;
                break;
            case 2:
                float centerWidth = ((float) this.mScreenWidth) / 2.0f;
                float centerHeight = ((float) this.mScreenHeight) / 2.0f;
                if (this.mode != DRAG) {
                    if (this.mode == 2) {
                        float newDist = spacing(event);
                        if (newDist > 10.0f) {
                            float scale = newDist / this.mFirstOldDist;
                            float scale2 = newDist / this.prevOldDist;
                            float finalScale = this.mImageMatrix.getScale() * scale2;
                            if (finalScale > MIN_ZOOM && finalScale < 10.0f) {
                                float centerX = (this.mImageMatrix.getCenterX() * scale2) - ((scale2 - 1.0f) * centerWidth);
                                float centerY = (this.mImageMatrix.getCenterY() * scale2) - ((scale2 - 1.0f) * centerHeight);
                                float leftX = (this.mImageMatrix.getX() * scale2) - ((scale2 - 1.0f) * centerWidth);
                                float topY = (this.mImageMatrix.getY() * scale2) - ((scale2 - 1.0f) * centerHeight);
                                float bottomY = (2.0f * centerY) - topY;
                                if ((2.0f * centerX) - leftX >= centerWidth && leftX <= centerWidth && bottomY >= centerHeight && topY <= centerHeight) {
                                    this.prevOldDist = newDist;
                                    this.mImageMatrix.setScale(finalScale);
                                    this.mImageMatrix.setX(leftX);
                                    this.mImageMatrix.setY(topY);
                                    this.mImageMatrix.setCenterX(centerX);
                                    this.mImageMatrix.setCenterY(centerY);
                                    this.matrix.set(this.savedMatrix);
                                    this.matrix.postScale(scale, scale, centerWidth, centerHeight);
                                }
                            }
                            if (finalScale < 10.0f) {
                                this.isZoomInControlShoubleBeVisible = DRAG;
                            } else {
                                this.isZoomInControlShoubleBeVisible = NONE;
                            }
                            if (finalScale > MIN_ZOOM) {
                                this.isZoomOutControlShoubleBeVisible = DRAG;
                            } else {
                                this.isZoomOutControlShoubleBeVisible = NONE;
                            }
                            this.mZoomInButton.setEnabled(this.isZoomInControlShoubleBeVisible);
                            this.mZoomOutButton.setEnabled(this.isZoomOutControlShoubleBeVisible);
                            break;
                        }
                    }
                } else {
                    float deltaX = event.getX() - this.mNewStart.x;
                    float deltaY = event.getY() - this.mNewStart.y;
                    float deltaX2 = event.getX() - this.prevStart.x;
                    float deltaY2 = event.getY() - this.prevStart.y;
                    float imageLeftX = this.mImageMatrix.getX() + deltaX2;
                    float imageTopY = this.mImageMatrix.getY() + deltaY2;
                    float imageCenterX = this.mImageMatrix.getCenterX() + deltaX2;
                    float imageCenterY = this.mImageMatrix.getCenterY() + deltaY2;
                    float imageRightX = (2.0f * imageCenterX) - imageLeftX;
                    float imageBottomY = (2.0f * imageCenterY) - imageTopY;
                    if (imageRightX >= centerWidth && imageLeftX <= centerWidth) {
                        this.mDeltaX = deltaX;
                        this.mEventX = event.getX();
                    }
                    if (imageBottomY >= centerHeight && imageTopY <= centerHeight) {
                        this.mDeltaY = deltaY;
                        this.mEventY = event.getY();
                    }
                    if (imageRightX < centerWidth || imageLeftX > centerWidth) {
                        if (imageBottomY < centerHeight || imageTopY > centerHeight) {
                            this.mZoomOutButton.setEnabled(this.isZoomOutControlShoubleBeVisible);
                        } else {
                            this.matrix.set(this.savedMatrix);
                            this.prevStart.set(this.mEventX, event.getY());
                            this.mImageMatrix.setY(imageTopY);
                            this.mImageMatrix.setCenterY(imageCenterY);
                            this.matrix.postTranslate(this.mDeltaX, deltaY);
                            this.mZoomOutButton.setEnabled(this.isZoomOutControlShoubleBeVisible);
                        }
                    } else if (imageBottomY < centerHeight || imageTopY > centerHeight) {
                        this.matrix.set(this.savedMatrix);
                        this.prevStart.set(event.getX(), this.mEventY);
                        this.mImageMatrix.setX(imageLeftX);
                        this.mImageMatrix.setCenterX(imageCenterX);
                        this.matrix.postTranslate(deltaX, this.mDeltaY);
                        this.mZoomOutButton.setEnabled(this.isZoomOutControlShoubleBeVisible);
                    } else {
                        this.matrix.set(this.savedMatrix);
                        this.prevStart.set(event.getX(), event.getY());
                        this.mImageMatrix.setX(imageLeftX);
                        this.mImageMatrix.setY(imageTopY);
                        this.mImageMatrix.setCenterX(imageCenterX);
                        this.mImageMatrix.setCenterY(imageCenterY);
                        this.matrix.postTranslate(deltaX, deltaY);
                        this.mZoomOutButton.setEnabled(this.isZoomOutControlShoubleBeVisible);
                    }
                    if (FloatMath.sqrt((deltaY * deltaY) + (deltaY * deltaY)) > 10.0f) {
                        this.isSecondDouble = NONE;
                        break;
                    }
                }
                break;
            case ImprintDetailActivity.MenuMenu /*5*/:
                this.mFirstOldDist = spacing(event);
                this.prevOldDist = this.mFirstOldDist;
                if (this.mFirstOldDist > 10.0f) {
                    this.savedMatrix.set(this.matrix);
                    this.mode = 2;
                    this.isSecondDouble = NONE;
                    break;
                }
                break;
        }
        this.mZoomCcontrols.setVisibility(NONE);
        this.mShopLocalDataHandler.removeCallbacks(this.mZoomControlsUpdateTask);
        this.mShopLocalDataHandler.postDelayed(this.mZoomControlsUpdateTask, TIME_SPAN_MILLS);
        if (this.mGestureDetector.onTouchEvent(rawEvent)) {
            return true;
        }
        view.setImageMatrix(this.matrix);
        return true;
    }

    private float spacing(WrapMotionEvent event) {
        float x = event.getX(NONE) - event.getX(DRAG);
        float y = event.getY(NONE) - event.getY(DRAG);
        return FloatMath.sqrt((x * x) + (y * y));
    }

    private void calculateImageMatrix() {
        double imageScale;
        int imageWidth = DRAG;
        int imageHeight = DRAG;
        double screenRatio = (((double) this.mScreenWidth) * 1.0d) / ((double) this.mScreenHeight);
        double imageRatio = 1.0d;
        if (this.mBitmap != null) {
            imageWidth = this.mBitmap.getWidth();
            imageHeight = this.mBitmap.getHeight();
            imageRatio = (((double) imageWidth) * 1.0d) / ((double) imageHeight);
        }
        if (imageRatio > screenRatio) {
            imageScale = (double) ((((float) this.mScreenWidth) * 0.96f) / ((float) imageWidth));
        } else {
            imageScale = (double) ((((float) this.mScreenHeight) * 0.96f) / ((float) imageHeight));
        }
        this.mInitialImageMatrix.setX((float) ((((double) this.mScreenWidth) - (((double) imageWidth) * imageScale)) / 2.0d));
        this.mInitialImageMatrix.setY((float) ((((double) this.mScreenHeight) - (((double) imageHeight) * imageScale)) / 2.0d));
        this.mInitialImageMatrix.setCenterX((float) (((double) this.mScreenWidth) / 2.0d));
        this.mInitialImageMatrix.setCenterY((float) (((double) this.mScreenHeight) / 2.0d));
        this.mInitialImageMatrix.setScale((float) imageScale);
        this.mInitialImageMatrix.setDoubleX((float) ((((double) this.mScreenWidth) / 2.0d) - (((double) imageWidth) * imageScale)));
        this.mInitialImageMatrix.setDoubleY((float) ((((double) this.mScreenHeight) / 2.0d) - (((double) imageHeight) * imageScale)));
        this.mImageMatrix.setX((float) ((((double) this.mScreenWidth) - (((double) imageWidth) * imageScale)) / 2.0d));
        this.mImageMatrix.setY((float) ((((double) this.mScreenHeight) - (((double) imageHeight) * imageScale)) / 2.0d));
        this.mImageMatrix.setCenterX((float) (((double) this.mScreenWidth) / 2.0d));
        this.mImageMatrix.setCenterY((float) (((double) this.mScreenHeight) / 2.0d));
        this.mImageMatrix.setScale((float) imageScale);
        this.mImageMatrix.setDoubleX((float) ((((double) this.mScreenWidth) / 2.0d) - (((double) imageWidth) * imageScale)));
        this.mImageMatrix.setDoubleY((float) ((((double) this.mScreenHeight) / 2.0d) - (((double) imageHeight) * imageScale)));
    }

    public boolean onDown(MotionEvent e) {
        return false;
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        try {
            if (Math.abs(e1.getY() - e2.getY()) > 250.0f) {
                return NONE;
            }
            if (e1.getX() - e2.getX() <= 120.0f || Math.abs(velocityX) <= 200.0f) {
                if (e2.getX() - e1.getX() > 120.0f && Math.abs(velocityX) > 200.0f) {
                    if (this.IsFromMapListView) {
                        setResult(-1, new Intent(getApplicationContext(), ShopLocalMapListActivity.class));
                    }
                    finish();
                    return true;
                } else if ((e1.getY() - e2.getY() <= 120.0f || Math.abs(velocityX) <= 200.0f) && e2.getY() - e1.getY() > 120.0f) {
                    Math.abs(velocityX);
                }
            }
            return NONE;
        } catch (Exception e) {
        }
    }

    public void onLongPress(MotionEvent e) {
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    public void onShowPress(MotionEvent e) {
    }

    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.IsFromMapListView) {
            setResult(-1, new Intent(getApplicationContext(), ShopLocalMapListActivity.class));
        }
        finish();
        return true;
    }

    public void zoomIn() {
        float finalScale = this.mImageMatrix.getScale() * ZOOM_IN_NUMBER;
        float centerWidth = ((float) this.mScreenWidth) / 2.0f;
        float centerHeight = ((float) this.mScreenHeight) / 2.0f;
        if (finalScale < 10.0f) {
            this.isZoomInControlShoubleBeVisible = true;
            float centerX = (ZOOM_IN_NUMBER * this.mImageMatrix.getCenterX()) - (0.25f * centerWidth);
            float centerY = (ZOOM_IN_NUMBER * this.mImageMatrix.getCenterY()) - (0.25f * centerHeight);
            float leftX = (ZOOM_IN_NUMBER * this.mImageMatrix.getX()) - (0.25f * centerWidth);
            float topY = (ZOOM_IN_NUMBER * this.mImageMatrix.getY()) - (0.25f * centerHeight);
            float bottomY = (2.0f * centerY) - topY;
            if ((2.0f * centerX) - leftX < centerWidth || leftX > centerWidth || bottomY < centerHeight || topY > centerHeight) {
                this.mZoomInButton.setEnabled(false);
                return;
            }
            this.mImageMatrix.setX(leftX);
            this.mImageMatrix.setY(topY);
            this.mImageMatrix.setCenterX(centerX);
            this.mImageMatrix.setCenterY(centerY);
            this.mImageMatrix.setScale(finalScale);
            this.matrix.postScale(ZOOM_IN_NUMBER, ZOOM_IN_NUMBER, centerWidth, centerHeight);
            this.myAccessoryImageView.setImageMatrix(this.matrix);
            this.savedMatrix.set(this.matrix);
        } else {
            this.isZoomInControlShoubleBeVisible = false;
        }
        if (finalScale > MIN_ZOOM) {
            this.isZoomOutControlShoubleBeVisible = true;
        } else {
            this.isZoomOutControlShoubleBeVisible = false;
        }
        this.mZoomInButton.setEnabled(this.isZoomInControlShoubleBeVisible);
        this.mZoomOutButton.setEnabled(this.isZoomOutControlShoubleBeVisible);
        this.isSecondDouble = false;
        this.mZoomCcontrols.setVisibility(NONE);
        this.mShopLocalDataHandler.removeCallbacks(this.mZoomControlsUpdateTask);
        this.mShopLocalDataHandler.postDelayed(this.mZoomControlsUpdateTask, TIME_SPAN_MILLS);
    }

    public void zoomOut() {
        this.isSecondDouble = false;
        this.mZoomCcontrols.setVisibility(NONE);
        this.mShopLocalDataHandler.removeCallbacks(this.mZoomControlsUpdateTask);
        this.mShopLocalDataHandler.postDelayed(this.mZoomControlsUpdateTask, TIME_SPAN_MILLS);
        float centerWidth = ((float) this.mScreenWidth) / 2.0f;
        float centerHeight = ((float) this.mScreenHeight) / 2.0f;
        float finalScale = this.mImageMatrix.getScale() * ZOOM_OUT_NUMBER;
        if (finalScale > MIN_ZOOM) {
            this.isZoomOutControlShoubleBeVisible = true;
            float centerX = (ZOOM_OUT_NUMBER * this.mImageMatrix.getCenterX()) - (-0.19999999f * centerWidth);
            float centerY = (ZOOM_OUT_NUMBER * this.mImageMatrix.getCenterY()) - (-0.19999999f * centerHeight);
            float leftX = (ZOOM_OUT_NUMBER * this.mImageMatrix.getX()) - (-0.19999999f * centerWidth);
            float topY = (ZOOM_OUT_NUMBER * this.mImageMatrix.getY()) - (-0.19999999f * centerHeight);
            float bottomY = (2.0f * centerY) - topY;
            if ((2.0f * centerX) - leftX < centerWidth || leftX > centerWidth || bottomY < centerHeight || topY > centerHeight) {
                this.mZoomOutButton.setEnabled(false);
                return;
            }
            this.mImageMatrix.setScale(finalScale);
            this.mImageMatrix.setX(leftX);
            this.mImageMatrix.setY(topY);
            this.mImageMatrix.setCenterX(centerX);
            this.mImageMatrix.setCenterY(centerY);
            this.matrix.postScale(ZOOM_OUT_NUMBER, ZOOM_OUT_NUMBER, centerWidth, centerHeight);
            this.myAccessoryImageView.setImageMatrix(this.matrix);
            this.savedMatrix.set(this.matrix);
        } else {
            this.isZoomOutControlShoubleBeVisible = false;
        }
        this.mZoomOutButton.setEnabled(this.isZoomOutControlShoubleBeVisible);
        if (finalScale < 10.0f) {
            this.isZoomInControlShoubleBeVisible = true;
        } else {
            this.isZoomInControlShoubleBeVisible = false;
        }
        this.mZoomInButton.setEnabled(this.isZoomInControlShoubleBeVisible);
    }
}
