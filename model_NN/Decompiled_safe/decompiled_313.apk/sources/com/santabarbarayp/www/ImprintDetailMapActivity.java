package com.santabarbarayp.www;

import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.TextView;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;
import java.util.ArrayList;
import java.util.List;

public class ImprintDetailMapActivity extends MapActivity implements GestureDetector.OnGestureListener {
    private static final int MAPVIEW_LATITUDE_SPAN = 10000;
    private static final int MAPVIEW_LONGITUDE_SPAN = 10000;
    int currentImprintElementIndex;
    private MapView imprintElementMapView;
    private GestureDetector mGestureDetector;
    private TextView mImprintTitle;
    private int mLatitudeSpanE6 = 10000;
    private int mLongitudeSpanE6 = 10000;
    ImprintElementList myImprintElementList;
    private String myImprintName;

    public void onCreate(Bundle savedInstanceState) {
        ImprintDetailMapActivity.super.onCreate(savedInstanceState);
        if (getResources().getConfiguration().orientation == 2) {
            getWindow().setFlags(1024, 1024);
        } else {
            getWindow().clearFlags(1024);
        }
        setContentView(R.layout.imprint_element_mapview);
        Bundle extras = getIntent().getExtras();
        this.myImprintElementList = (ImprintElementList) extras.getParcelable("currentImprintElementList");
        this.myImprintName = extras.getString("imprintName");
        this.currentImprintElementIndex = extras.getInt("currentImprintElementIndex");
        this.mImprintTitle = (TextView) findViewById(R.id.imprint_element_map_title);
        this.mImprintTitle.setText(this.myImprintName);
        this.imprintElementMapView = findViewById(R.id.imprintSearch_mapview);
        this.imprintElementMapView.setBuiltInZoomControls(true);
        this.imprintElementMapView.setSatellite(false);
        this.imprintElementMapView.setStreetView(false);
        this.mGestureDetector = new GestureDetector(this);
        ImprintElement localImprintElment = (ImprintElement) this.myImprintElementList.get(this.currentImprintElementIndex);
        GeoPoint mMapCenterGeoPoint = new GeoPoint((int) (localImprintElment.getLat() * 1000000.0d), (int) (localImprintElment.getLng() * 1000000.0d));
        MapController imprintElementMapController = this.imprintElementMapView.getController();
        imprintElementMapController.setCenter(mMapCenterGeoPoint);
        imprintElementMapController.zoomToSpan(this.mLatitudeSpanE6, this.mLongitudeSpanE6);
        List<Overlay> overlays = this.imprintElementMapView.getOverlays();
        overlays.clear();
        overlays.add(new ImprintElementOverlay());
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    public void onConfigurationChanged(Configuration newConfig) {
        if (getResources().getConfiguration().orientation == 2) {
            getWindow().setFlags(1024, 1024);
        } else {
            getWindow().clearFlags(1024);
        }
        ImprintDetailMapActivity.super.onConfigurationChanged(newConfig);
    }

    class ImprintElementOverlay extends Overlay {
        private Bitmap arrowDown;
        private Paint arrowDownPaint;
        private Bitmap arrowUp;
        private Bitmap bubbleShadow;
        private Paint innerPaint;
        private ArrayList<PlaceMarker> items = new ArrayList<>();
        private Bitmap letterIcon;
        private int mAnnotationIndex = -1;
        private Paint outerBorderPaint;
        private Paint textPaint;

        public boolean onTap(GeoPoint p, MapView mapView) {
            boolean isRemovePriorPopup;
            mapView.getProjection().toPixels(p, new Point());
            if (ImprintDetailMapActivity.this.currentImprintElementIndex > -1) {
                isRemovePriorPopup = true;
            } else {
                isRemovePriorPopup = false;
            }
            int hitMapPlacemarkerIndex = getHitMapPlaceMarker(mapView, p);
            ImprintDetailMapActivity.this.currentImprintElementIndex = hitMapPlacemarkerIndex;
            if (isRemovePriorPopup || hitMapPlacemarkerIndex > -1) {
                mapView.invalidate();
            }
            if (hitMapPlacemarkerIndex > -1) {
                return true;
            }
            return false;
        }

        public ImprintElementOverlay() {
            this.letterIcon = BitmapFactory.decodeResource(ImprintDetailMapActivity.this.getResources(), R.drawable.pin_a);
            this.bubbleShadow = BitmapFactory.decodeResource(ImprintDetailMapActivity.this.getResources(), R.drawable.bubble_shadow);
            this.arrowDown = BitmapFactory.decodeResource(ImprintDetailMapActivity.this.getResources(), R.drawable.arrow_down);
            this.arrowUp = BitmapFactory.decodeResource(ImprintDetailMapActivity.this.getResources(), R.drawable.arrow_up);
            int markerIndex = 0;
            for (int i = 0; i < ImprintDetailMapActivity.this.myImprintElementList.size(); i++) {
                ImprintElement localImprintElment = (ImprintElement) ImprintDetailMapActivity.this.myImprintElementList.get(i);
                if (ImprintDetailMapActivity.this.currentImprintElementIndex == i) {
                    this.mAnnotationIndex = markerIndex;
                }
                if (localImprintElment.getDistance() > -1.0d) {
                    char markerChar = (char) (markerIndex + 97);
                    markerIndex++;
                    String elementAddress = localImprintElment.getStreet();
                    String elementCityAddress = localImprintElment.getCityStateZip();
                    PlaceMarker imOverItem = new PlaceMarker(new GeoPoint((int) (localImprintElment.getLat() * 1000000.0d), (int) (localImprintElment.getLng() * 1000000.0d)), localImprintElment.getName(), elementAddress);
                    imOverItem.setCityAddress(elementCityAddress);
                    imOverItem.setDrawableChar(markerChar);
                    imOverItem.setImprintIndex(i);
                    this.items.add(imOverItem);
                }
            }
        }

        public void draw(Canvas canvas, MapView mapView, boolean shadow) {
            drawMapMarkers(canvas, mapView, shadow);
            drawInfoWindow(canvas, mapView);
        }

        private void drawMapMarkers(Canvas canvas, MapView mapView, boolean shadow) {
            Point screenCoords = new Point();
            for (int k = 0; k < this.items.size(); k++) {
                PlaceMarker imOverItem = this.items.get(k);
                mapView.getProjection().toPixels(imOverItem.getPoint(), screenCoords);
                Bitmap markerIcon = BitmapFactory.decodeResource(ImprintDetailMapActivity.this.getResources(), ImprintDetailMapActivity.this.getResources().getIdentifier(String.format("pin_%c", Character.valueOf(imOverItem.getDrawableChar())), "drawable", "com.santabarbarayp.www"));
                canvas.drawBitmap(markerIcon, (float) screenCoords.x, (float) (screenCoords.y - markerIcon.getHeight()), (Paint) null);
            }
        }

        private void drawInfoWindow(Canvas canvas, MapView mapView) {
            Projection myprojection = mapView.getProjection();
            PlaceMarker selectedPlaceMarker = null;
            if (this.mAnnotationIndex > -1) {
                selectedPlaceMarker = this.items.get(this.mAnnotationIndex);
            }
            if (selectedPlaceMarker != null) {
                PlaceMarker localPlaceMarker = selectedPlaceMarker;
                int markResourceID = ImprintDetailMapActivity.this.getResources().getIdentifier(String.format("pin_%c_on", Character.valueOf(localPlaceMarker.getDrawableChar())), "drawable", "com.santabarbarayp.www");
                Point screenCoords = new Point();
                mapView.getProjection().toPixels(localPlaceMarker.getPoint(), screenCoords);
                Bitmap selectedMarkerIcon = BitmapFactory.decodeResource(ImprintDetailMapActivity.this.getResources(), markResourceID);
                canvas.drawBitmap(selectedMarkerIcon, (float) screenCoords.x, (float) (screenCoords.y - selectedMarkerIcon.getHeight()), (Paint) null);
                GeoPoint geoPoint = localPlaceMarker.getPoint();
                Point point = new Point();
                myprojection.toPixels(geoPoint, point);
                int cWidth = canvas.getWidth();
                int bubbleWidth = cWidth;
                if (bubbleWidth > 480) {
                    bubbleWidth = 480;
                }
                double fontFraction = ((double) bubbleWidth) / 480.0d;
                int two_ADJUST_INT = (int) (2.0d * fontFraction);
                int five_ADJUST_INT = (int) (5.0d * fontFraction);
                int seven_ADJUST_INT = (int) (7.0d * fontFraction);
                int eight_ADJUST_INT = (int) (8.0d * fontFraction);
                int ten_ADJUST_INT = (int) (10.0d * fontFraction);
                int thirteen_ADJUST_INT = (int) (13.0d * fontFraction);
                int thirty_ADJUST_INT = (int) (30.0d * fontFraction);
                int TITLE_FONT_SIZE = (int) (22.0d * fontFraction);
                int ADDRESS_FONT_SIZE = eight_ADJUST_INT + eight_ADJUST_INT;
                int INFO_WINDOW_HEIGHT = (int) ((65.0d * fontFraction) + 0.5d);
                int TEXT_OFFSET_X = ten_ADJUST_INT;
                int TEXT_OFFSET_Y = (int) (27.0d * fontFraction);
                int offsetWidthAdjust = this.letterIcon.getWidth() / 2;
                int offsetHeightAdjust = this.arrowDown.getHeight() - eight_ADJUST_INT;
                Paint titlePaint = new Paint();
                titlePaint.setTextSize((float) TITLE_FONT_SIZE);
                titlePaint.setStyle(Paint.Style.FILL_AND_STROKE);
                titlePaint.setARGB(255, 255, 255, 255);
                titlePaint.setTypeface(Typeface.DEFAULT_BOLD);
                titlePaint.setAntiAlias(true);
                String imprintTitle = localPlaceMarker.getTitle().trim();
                int largestWidth = (bubbleWidth - ten_ADJUST_INT) - thirty_ADJUST_INT;
                boolean isTitleLarger = false;
                int titleSize = imprintTitle.length();
                float titleWidth = titlePaint.measureText(imprintTitle, 0, titleSize);
                while (titleWidth > ((float) largestWidth) && titleSize > 4) {
                    isTitleLarger = true;
                    titleSize--;
                    titleWidth = titlePaint.measureText(imprintTitle, 0, titleSize);
                }
                if (isTitleLarger) {
                    imprintTitle = String.valueOf(imprintTitle.substring(0, titleSize - 3)) + "...";
                }
                float titleWidth2 = titlePaint.measureText(imprintTitle, 0, imprintTitle.length());
                Paint addressPaint = new Paint();
                addressPaint.setTextSize((float) ADDRESS_FONT_SIZE);
                addressPaint.setStyle(Paint.Style.FILL_AND_STROKE);
                addressPaint.setARGB(255, 255, 255, 255);
                addressPaint.setAntiAlias(true);
                String imprintStreetAddress = localPlaceMarker.getSnippet();
                if (imprintStreetAddress == null) {
                    imprintStreetAddress = "";
                }
                String cityAddress = localPlaceMarker.getCityAddress();
                if (cityAddress != null && cityAddress.trim().length() > 0) {
                    imprintStreetAddress = String.valueOf(imprintStreetAddress) + ", " + cityAddress.trim();
                }
                boolean isaddressLarger = false;
                int addressSize = imprintStreetAddress.length();
                float addressWidth = addressPaint.measureText(imprintStreetAddress, 0, addressSize);
                while (addressWidth > ((float) largestWidth) && addressSize > 4) {
                    isaddressLarger = true;
                    addressSize--;
                    addressWidth = addressPaint.measureText(imprintStreetAddress, 0, addressSize);
                }
                if (isaddressLarger) {
                    imprintStreetAddress = String.valueOf(imprintStreetAddress.substring(0, addressSize - 3)) + "...";
                }
                float addressWidth2 = addressPaint.measureText(imprintStreetAddress, 0, imprintStreetAddress.length());
                float largerWidth = addressWidth2;
                if (titleWidth2 > addressWidth2) {
                    largerWidth = titleWidth2;
                }
                int INFO_WINDOW_WIDTH = ((int) largerWidth) + thirty_ADJUST_INT;
                RectF rectF = new RectF(0.0f, 0.0f, (float) INFO_WINDOW_WIDTH, (float) INFO_WINDOW_HEIGHT);
                int infoWindowOffsetX = (point.x - (INFO_WINDOW_WIDTH / 2)) + offsetWidthAdjust;
                if (point.x <= 0 || point.x > cWidth - this.letterIcon.getWidth()) {
                    if (point.x <= 0) {
                        infoWindowOffsetX = point.x;
                    } else {
                        infoWindowOffsetX = (point.x - INFO_WINDOW_WIDTH) + this.letterIcon.getWidth();
                    }
                } else if (infoWindowOffsetX < 0) {
                    infoWindowOffsetX = 0;
                } else if (infoWindowOffsetX + INFO_WINDOW_WIDTH > cWidth) {
                    infoWindowOffsetX = cWidth - INFO_WINDOW_WIDTH;
                }
                int infoWindowOffsetY = ((point.y - INFO_WINDOW_HEIGHT) - selectedMarkerIcon.getHeight()) - offsetHeightAdjust;
                boolean bottomArrow = true;
                if (infoWindowOffsetY < 0) {
                    bottomArrow = false;
                    infoWindowOffsetY = (point.y + this.arrowUp.getHeight()) - five_ADJUST_INT;
                }
                rectF.offset((float) infoWindowOffsetX, (float) infoWindowOffsetY);
                canvas.drawBitmap(Bitmap.createScaledBitmap(this.bubbleShadow, INFO_WINDOW_WIDTH, INFO_WINDOW_HEIGHT, false), (float) (infoWindowOffsetX + seven_ADJUST_INT), (float) (infoWindowOffsetY + thirteen_ADJUST_INT), getInnerPaint());
                canvas.drawRoundRect(rectF, (float) five_ADJUST_INT, (float) five_ADJUST_INT, getInnerPaint());
                int largestHeightY = infoWindowOffsetY + TEXT_OFFSET_Y;
                canvas.drawText(imprintTitle, (float) (infoWindowOffsetX + TEXT_OFFSET_X), (float) largestHeightY, titlePaint);
                canvas.drawText(imprintStreetAddress, (float) (infoWindowOffsetX + TEXT_OFFSET_X), (float) (largestHeightY + TITLE_FONT_SIZE + two_ADJUST_INT), addressPaint);
                Paint pGray = new Paint();
                pGray.setARGB(255, 65, 64, 62);
                pGray.setStyle(Paint.Style.FILL_AND_STROKE);
                int arrowLeftPoint = (point.x + offsetWidthAdjust) - (this.arrowDown.getWidth() / 2);
                int arrowTopPoint = infoWindowOffsetY + INFO_WINDOW_HEIGHT;
                if (bottomArrow) {
                    canvas.drawBitmap(this.arrowDown, (float) arrowLeftPoint, (float) arrowTopPoint, getArrowDownPaint());
                    return;
                }
                canvas.drawBitmap(this.arrowUp, (float) arrowLeftPoint, (float) (infoWindowOffsetY - this.arrowUp.getHeight()), getInnerPaint());
            }
        }

        public Paint getInnerPaint() {
            if (this.innerPaint == null) {
                this.innerPaint = new Paint();
                this.innerPaint.setARGB(225, 72, 72, 72);
                this.innerPaint.setStyle(Paint.Style.FILL);
                this.innerPaint.setAntiAlias(true);
            }
            return this.innerPaint;
        }

        public Paint getArrowDownPaint() {
            if (this.arrowDownPaint == null) {
                this.arrowDownPaint = new Paint();
                this.arrowDownPaint.setARGB(170, 68, 68, 68);
                this.arrowDownPaint.setStyle(Paint.Style.FILL);
                this.arrowDownPaint.setAntiAlias(true);
            }
            return this.arrowDownPaint;
        }

        public Paint getBorderPaint() {
            if (this.outerBorderPaint == null) {
                this.outerBorderPaint = new Paint();
                this.outerBorderPaint.setARGB(255, 58, 57, 57);
                this.outerBorderPaint.setAntiAlias(true);
                this.outerBorderPaint.setStyle(Paint.Style.STROKE);
                this.outerBorderPaint.setStrokeWidth(2.0f);
            }
            return this.outerBorderPaint;
        }

        public Paint getTextPaint() {
            if (this.textPaint == null) {
                this.textPaint = new Paint();
                this.textPaint.setARGB(255, 255, 255, 255);
                this.textPaint.setAntiAlias(true);
            }
            return this.textPaint;
        }

        private int getHitMapPlaceMarker(MapView mapView, GeoPoint tapPoint) {
            int hitMapPlacemarkerIndex = -1;
            RectF hitTestRecr = new RectF();
            Point screenCoords = new Point();
            int startindex = -1;
            if (ImprintDetailMapActivity.this.currentImprintElementIndex > -1) {
                startindex = ImprintDetailMapActivity.this.currentImprintElementIndex - 1;
            }
            int markerLength = this.items.size();
            int k = 0;
            while (true) {
                if (k >= markerLength) {
                    break;
                }
                int index = ((markerLength - k) + startindex) % markerLength;
                PlaceMarker testPlacemarker = this.items.get(index);
                if (ImprintDetailMapActivity.this.currentImprintElementIndex != index) {
                    mapView.getProjection().toPixels(testPlacemarker.getPoint(), screenCoords);
                    hitTestRecr.set(0.0f, (float) (-this.letterIcon.getHeight()), (float) this.letterIcon.getWidth(), 0.0f);
                    hitTestRecr.offset((float) screenCoords.x, (float) screenCoords.y);
                    mapView.getProjection().toPixels(tapPoint, screenCoords);
                    if (hitTestRecr.contains((float) screenCoords.x, (float) screenCoords.y)) {
                        hitMapPlacemarkerIndex = index;
                        break;
                    }
                }
                k++;
            }
            return hitMapPlacemarkerIndex;
        }
    }

    public boolean onTouchEvent(MotionEvent me) {
        return this.mGestureDetector.onTouchEvent(me);
    }

    public boolean onDown(MotionEvent e) {
        return false;
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        try {
            if (Math.abs(e1.getY() - e2.getY()) > 250.0f) {
                return false;
            }
            if (e1.getX() - e2.getX() <= 120.0f || Math.abs(velocityX) <= 200.0f) {
                if (e2.getX() - e1.getX() > 120.0f && Math.abs(velocityX) > 200.0f) {
                    finish();
                    return true;
                } else if ((e1.getY() - e2.getY() <= 120.0f || Math.abs(velocityX) <= 200.0f) && e2.getY() - e1.getY() > 120.0f) {
                    Math.abs(velocityX);
                }
            }
            return false;
        } catch (Exception e) {
        }
    }

    public void onLongPress(MotionEvent e) {
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    public void onShowPress(MotionEvent e) {
    }

    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }
}
