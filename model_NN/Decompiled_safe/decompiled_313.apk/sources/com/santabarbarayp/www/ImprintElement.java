package com.santabarbarayp.www;

import java.util.ArrayList;

/* compiled from: ImprintElementList */
class ImprintElement {
    private String _city_state_zip;
    private double _distance = -1.0d;
    private ArrayList<String> _extra_line_list = null;
    private int _geoLocation_index = -1;
    private String _image_logo;
    private double _lat = Double.NaN;
    private double _lng = Double.NaN;
    private String _name;
    private ArrayList<String> _phone_list = null;
    private String _street;

    public ImprintElement() {
    }

    public ImprintElement(String name, String street, String citystatezip, ArrayList<String> extraLineList, ArrayList<String> phoneList, double distance, double latitude, double longitude, String imagelogo, int geoLocationIndex) {
        this._name = name;
        this._street = street;
        this._city_state_zip = citystatezip;
        this._extra_line_list = extraLineList;
        this._phone_list = phoneList;
        this._lat = latitude;
        this._lng = longitude;
        this._image_logo = imagelogo;
        this._distance = distance;
        this._geoLocation_index = geoLocationIndex;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String name) {
        this._name = name;
    }

    public String getImageLogo() {
        return this._image_logo;
    }

    public void setImageLogo(String imagelogo) {
        this._image_logo = imagelogo;
    }

    public double getDistance() {
        return this._distance;
    }

    public void setDistance(double distance) {
        this._distance = distance;
    }

    public double getLat() {
        return this._lat;
    }

    public void setLat(double lat) {
        this._lat = lat;
    }

    public double getLng() {
        return this._lng;
    }

    public void setLng(double lng) {
        this._lng = lng;
    }

    public void setStreet(String street) {
        this._street = street;
    }

    public String getStreet() {
        return this._street;
    }

    public void setCityStateZip(String citystatezip) {
        this._city_state_zip = citystatezip;
    }

    public String getCityStateZip() {
        return this._city_state_zip;
    }

    public void setPhoneList(ArrayList<String> phoneList) {
        this._phone_list = phoneList;
    }

    public ArrayList<String> getPhoneList() {
        return this._phone_list;
    }

    public void setExtraLineList(ArrayList<String> extraLineList) {
        this._extra_line_list = extraLineList;
    }

    public ArrayList<String> getExtraLineList() {
        return this._extra_line_list;
    }

    public void setGeoLocationIndex(int geoLocationindex) {
        this._geoLocation_index = geoLocationindex;
    }

    public int getGeoLocationIndex() {
        return this._geoLocation_index;
    }
}
