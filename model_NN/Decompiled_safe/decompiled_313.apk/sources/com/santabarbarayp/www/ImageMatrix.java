package com.santabarbarayp.www;

public class ImageMatrix {
    private float _scale = 1.0f;
    private float _x = 0.0f;
    private float _y = 0.0f;
    private float center_x = 0.0f;
    private float center_y = 0.0f;
    private float double_x = 0.0f;
    private float double_y = 0.0f;

    public ImageMatrix() {
    }

    public ImageMatrix(float leftX, float topY, float scale) {
        this._x = leftX;
        this._y = topY;
        this._scale = scale;
    }

    public float getScale() {
        return this._scale;
    }

    public void setScale(float scale) {
        this._scale = scale;
    }

    public float getX() {
        return this._x;
    }

    public void setX(float leftX) {
        this._x = leftX;
    }

    public float getY() {
        return this._y;
    }

    public void setY(float topY) {
        this._y = topY;
    }

    public float getCenterX() {
        return this.center_x;
    }

    public void setCenterX(float centerX) {
        this.center_x = centerX;
    }

    public float getCenterY() {
        return this.center_y;
    }

    public void setCenterY(float centerY) {
        this.center_y = centerY;
    }

    public float getDoubleX() {
        return this.double_x;
    }

    public void setDoubleX(float doubleX) {
        this.double_x = doubleX;
    }

    public float getDoubleY() {
        return this.double_y;
    }

    public void setDoubleY(float doubleY) {
        this.double_y = doubleY;
    }

    public void set(ImageMatrix srcMatrix) {
        this._x = srcMatrix._x;
        this._y = srcMatrix._y;
        this._scale = srcMatrix._scale;
        this.center_x = srcMatrix.center_x;
        this.center_y = srcMatrix.center_y;
        this.double_x = srcMatrix.double_x;
        this.double_y = srcMatrix.double_y;
    }
}
