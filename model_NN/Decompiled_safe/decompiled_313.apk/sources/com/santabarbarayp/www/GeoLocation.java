package com.santabarbarayp.www;

import android.os.Parcel;
import android.os.Parcelable;

public class GeoLocation implements Parcelable {
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public GeoLocation createFromParcel(Parcel in) {
            return new GeoLocation(in);
        }

        public GeoLocation[] newArray(int size) {
            return new GeoLocation[size];
        }
    };
    private double lat;
    private double lng;
    private String title;

    public GeoLocation() {
    }

    public GeoLocation(String citytitle, double latitude, double longitude) {
        this.title = citytitle;
        this.lat = latitude;
        this.lng = longitude;
    }

    public GeoLocation(Parcel in) {
        readFromParcel(in);
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public double getLat() {
        return this.lat;
    }

    public void setLat(double lat2) {
        this.lat = lat2;
    }

    public double getLng() {
        return this.lng;
    }

    public void setLng(double lng2) {
        this.lng = lng2;
    }

    private void readFromParcel(Parcel in) {
        setTitle(in.readString());
        setLat(in.readDouble());
        setLng(in.readDouble());
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeDouble(this.lat);
        dest.writeDouble(this.lng);
    }
}
