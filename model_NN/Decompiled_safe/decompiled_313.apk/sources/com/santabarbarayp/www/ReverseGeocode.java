package com.santabarbarayp.www;

import android.location.Address;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ReverseGeocode {
    /* JADX INFO: Multiple debug info for r7v8 org.json.JSONArray: [D('jsonLocationArray' org.json.JSONArray), D('jsonDictionary' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r7v10 org.json.JSONObject: [D('jsonLocationArray' org.json.JSONArray), D('firstLocationDetail' org.json.JSONObject)] */
    public static String retrieveLocation(double lat, double lon) {
        try {
            URLConnection urlConnection = new URL(String.format("http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=false", Double.valueOf(lat), Double.valueOf(lon))).openConnection();
            StringBuilder sb = new StringBuilder();
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "ISO-8859-1"));
                while (true) {
                    String line = reader.readLine();
                    if (line != null) {
                        sb.append(String.valueOf(line) + "\n");
                    }
                    break;
                }
            } catch (IOException e) {
            }
            JSONArray jsonLocationArray = new JSONObject(sb.toString()).getJSONArray("results");
            if (jsonLocationArray.length() > 0) {
                JSONArray locationDetailAddress = ((JSONObject) jsonLocationArray.get(0)).getJSONArray("address_components");
                String cityName = "";
                String streetNumber = "";
                String stateName = "";
                String routeName = "";
                for (int i = 0; i < locationDetailAddress.length(); i++) {
                    JSONObject tempAddress = (JSONObject) locationDetailAddress.get(i);
                    JSONArray typeAddressArray = tempAddress.getJSONArray("types");
                    for (int k = 0; k < typeAddressArray.length(); k++) {
                        String tempType = typeAddressArray.getString(k);
                        if (tempType.equalsIgnoreCase("street_number")) {
                            streetNumber = tempAddress.getString("short_name");
                        } else if (tempType.equalsIgnoreCase("route")) {
                            routeName = tempAddress.getString("short_name");
                        } else if (tempType.equalsIgnoreCase("locality")) {
                            cityName = tempAddress.getString("short_name");
                        } else if (tempType.equalsIgnoreCase("administrative_area_level_1")) {
                            stateName = tempAddress.getString("short_name");
                        }
                    }
                }
                return String.format("%s~~%s", String.format("%s %s", streetNumber, routeName).trim(), String.format("%s, %s", cityName, stateName).trim()).trim();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return null;
    }

    /* JADX INFO: Multiple debug info for r3v30 org.apache.http.HttpResponse: [D('client' org.apache.http.client.HttpClient), D('hr' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r4v9 org.apache.http.HttpEntity: [D('entity' org.apache.http.HttpEntity), D('urlStr' java.lang.String)] */
    public static List<Address> getFromLocation(double lat, double lon, int maxResults) {
        String buff;
        String urlStr = "http://maps.google.com/maps/geo?q=" + lat + "," + lon + "&output=json&sensor=false";
        String response = "";
        List<Address> results = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new DefaultHttpClient().execute(new HttpGet(urlStr)).getEntity().getContent()));
            while (true) {
                String buff2 = br.readLine();
                if (buff2 == null) {
                    break;
                }
                response = String.valueOf(response) + buff2;
            }
            buff = response;
        } catch (IOException e) {
            buff = response;
            e.printStackTrace();
        }
        try {
            JSONArray responseArray = new JSONObject(buff).getJSONArray("Placemark");
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < responseArray.length() && i2 < maxResults) {
                    Address addy = new Address(Locale.getDefault());
                    try {
                        JSONObject jsl = responseArray.getJSONObject(i2);
                        String addressLine = jsl.getString("address");
                        if (addressLine.contains(",")) {
                            addressLine = addressLine.split(",")[0];
                        }
                        addy.setAddressLine(0, addressLine);
                        JSONObject jsl2 = jsl.getJSONObject("AddressDetails").getJSONObject("Country");
                        addy.setCountryName(jsl2.getString("CountryName"));
                        addy.setCountryCode(jsl2.getString("CountryNameCode"));
                        JSONObject jsl3 = jsl2.getJSONObject("AdministrativeArea");
                        addy.setAdminArea(jsl3.getString("AdministrativeAreaName"));
                        JSONObject jsl4 = jsl3.getJSONObject("SubAdministrativeArea");
                        addy.setSubAdminArea(jsl4.getString("SubAdministrativeAreaName"));
                        JSONObject jsl5 = jsl4.getJSONObject("Locality");
                        addy.setLocality(jsl5.getString("LocalityName"));
                        addy.setPostalCode(jsl5.getJSONObject("PostalCode").getString("PostalCodeNumber"));
                        addy.setThoroughfare(jsl5.getJSONObject("Thoroughfare").getString("ThoroughfareName"));
                        results.add(addy);
                    } catch (JSONException e2) {
                        e2.printStackTrace();
                    }
                    i = i2 + 1;
                }
            }
            return results;
        } catch (JSONException e3) {
            return results;
        }
    }

    /* JADX INFO: Multiple debug info for r7v1 java.util.List<android.location.Address>: [D('maxResults' int), D('results' java.util.List<android.location.Address>)] */
    /* JADX INFO: Multiple debug info for r7v2 java.util.List<android.location.Address>: [D('maxResults' int), D('results' java.util.List<android.location.Address>)] */
    /* JADX INFO: Multiple debug info for r6v28 org.apache.http.HttpResponse: [D('client' org.apache.http.client.HttpClient), D('hr' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r0v7 org.apache.http.HttpEntity: [D('entity' org.apache.http.HttpEntity), D('urlStr' java.lang.String)] */
    public static List<Address> getFromLocation(String address, int maxResults) {
        String buff;
        String urlStr = "http://maps.google.com/maps/geo?q=" + address + "&output=json&sensor=false";
        String response = "";
        List<Address> results = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new DefaultHttpClient().execute(new HttpGet(urlStr)).getEntity().getContent()));
            while (true) {
                String buff2 = br.readLine();
                if (buff2 == null) {
                    break;
                }
                response = String.valueOf(response) + buff2;
            }
            buff = response;
        } catch (IOException e) {
            buff = response;
            e.printStackTrace();
        }
        try {
            JSONArray responseArray = new JSONObject(buff).getJSONArray("Placemark");
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < responseArray.length() && i2 < maxResults) {
                    Address addy = new Address(Locale.getDefault());
                    try {
                        JSONObject jsl = responseArray.getJSONObject(i2);
                        String addressLine = jsl.getString("address");
                        if (addressLine.contains(",")) {
                            addressLine = addressLine.split(",")[0];
                        }
                        addy.setAddressLine(0, addressLine);
                        JSONObject jsl2 = jsl.getJSONObject("AddressDetails").getJSONObject("Country");
                        addy.setCountryName(jsl2.getString("CountryName"));
                        addy.setCountryCode(jsl2.getString("CountryNameCode"));
                        JSONObject jsl3 = jsl2.getJSONObject("AdministrativeArea");
                        addy.setAdminArea(jsl3.getString("AdministrativeAreaName"));
                        JSONObject jsl4 = jsl3.getJSONObject("SubAdministrativeArea");
                        addy.setSubAdminArea(jsl4.getString("SubAdministrativeAreaName"));
                        JSONObject jsl5 = jsl4.getJSONObject("Locality");
                        addy.setLocality(jsl5.getString("LocalityName"));
                        addy.setPostalCode(jsl5.getJSONObject("PostalCode").getString("PostalCodeNumber"));
                        addy.setThoroughfare(jsl5.getJSONObject("Thoroughfare").getString("ThoroughfareName"));
                        results.add(addy);
                    } catch (JSONException e2) {
                        e2.printStackTrace();
                    }
                    i = i2 + 1;
                }
            }
            return results;
        } catch (JSONException e3) {
            return results;
        }
    }
}
