package com.santabarbarayp.www;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.FloatMath;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ZoomButtonsController;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;

public class ShopLocalMapListActivity extends MapActivity implements LocationListener, GestureDetector.OnGestureListener, View.OnTouchListener, ZoomButtonsController.OnZoomListener {
    /* access modifiers changed from: private */
    public ImageView bannerImgView1;
    /* access modifiers changed from: private */
    public ImageView bannerImgView2;
    /* access modifiers changed from: private */
    public ImageView bannerImgView3;
    /* access modifiers changed from: private */
    public ShopLocal_ViewFlipper bannerViewFlipper;
    int currentPageNumber = 0;
    int dataAnnotationNumber = 0;
    int fetchedAnnotationNumber = 0;
    int fetchedPageNumber = 0;
    /* access modifiers changed from: private */
    public ListView imprintSearchListView;
    /* access modifiers changed from: private */
    public MapView imprintSearchMapView;
    /* access modifiers changed from: private */
    public ShopLocal_ViewFlipper imprintSearchViewFlipper;
    AlertDialog internetConnectionAlertDialog;
    private boolean isChange;
    /* access modifiers changed from: private */
    public boolean isFirstBound = true;
    boolean isRetrievingData = false;
    private long locationUpdateSystemTime;
    int mBannerIndex = -1;
    final Runnable mBannerUpdateResults = new Runnable() {
        public void run() {
            if (ShopLocalMapListActivity.this.mImprintBannerList != null && ShopLocalMapListActivity.this.mImprintBannerList.size() > 0) {
                int bannerListSize = ShopLocalMapListActivity.this.mImprintBannerList.size();
                if (bannerListSize > 2) {
                    bannerListSize = 3;
                }
                if (bannerListSize != 2 || ShopLocalMapListActivity.this.mBannerIndex <= 0) {
                    ShopLocalMapListActivity.this.bannerViewFlipper.showNext();
                    ShopLocalMapListActivity.this.mBannerIndex++;
                    ShopLocalMapListActivity.this.mBannerIndex %= bannerListSize;
                } else {
                    ShopLocalMapListActivity.this.bannerViewFlipper.setDisplayedChild(0);
                    ShopLocalMapListActivity.this.mBannerIndex = 0;
                }
                ShopLocalMapListActivity.this.mShopLocalDataHandler.postDelayed(this, ShopLocalActivity.BANNER_DELAY_SPAN_MILLS);
            }
        }
    };
    private Runnable mBitmapUpdateTask = new Runnable() {
        /* JADX WARN: Type inference failed for: r2v1, types: [android.content.Context, com.santabarbarayp.www.ShopLocalMapListActivity] */
        public void run() {
            ShopLocalMapListActivity.this.isRetrievingData = false;
            Intent intent = new Intent((Context) ShopLocalMapListActivity.this, ImprintImageViewActivity.class);
            Bundle b = ShopLocalMapListActivity.this.mImprintAccessory.toBundle();
            b.putBoolean("IsFromMapListView", true);
            intent.putExtras(b);
            ShopLocalMapListActivity.this.startActivityForResult(intent, ShopLocalActivity.AD_BITMAP_REQUEST_CODE);
            ShopLocalMapListActivity.this.mProgressbar.setVisibility(8);
        }
    };
    GeoPoint mBottomLeftGeoPoint;
    String mDataRetrievedURL;
    String mDefaultHomeLocation;
    String mDefaultStreetLocation = null;
    int mFlingElementListIndex = 0;
    ArrayList<GeoLocation> mGeoLocationList;
    GeoPoint mGeoPoint_DefaultHomeLocation;
    GeoPoint mGeoPoint_SearchHomeLocation;
    GeoPoint mGeoPoint_SearchLocation;
    private GestureDetector mGestureDetector;
    ImageButton mHomeButton;
    /* access modifiers changed from: private */
    public ImprintAccessory mImprintAccessory = null;
    final Runnable mImprintBannerFinishUpdateResults = new Runnable() {
        public void run() {
            ShopLocalMapListActivity.this.isRetrievingData = false;
            ShopLocalMapListActivity.this.mProgressbar.setVisibility(4);
        }
    };
    ImprintBannerList mImprintBannerList;
    ArrayList<Imprint> mImprintList;
    final Runnable mImprintListFinishUpdateResults = new Runnable() {
        /* JADX WARN: Type inference failed for: r10v11, types: [android.content.Context, com.santabarbarayp.www.ShopLocalMapListActivity] */
        /* JADX WARN: Type inference failed for: r10v13, types: [android.content.Context, com.santabarbarayp.www.ShopLocalMapListActivity] */
        /* JADX WARN: Type inference failed for: r10v16, types: [android.content.Context, com.santabarbarayp.www.ShopLocalMapListActivity] */
        public void run() {
            ShopLocalMapListActivity.this.isRetrievingData = false;
            boolean isBoxBoundaryFixed = true;
            if (ShopLocalMapListActivity.this.mIsBoxSearch) {
                isBoxBoundaryFixed = true;
            } else if (ShopLocalMapListActivity.this.mImprintListSortOption.equals("Distance")) {
                isBoxBoundaryFixed = false;
            }
            ShopLocalMapListActivity.this.mSearchLocationTextView.setText(ShopLocalMapListActivity.this.mSearchLocation);
            ShopLocalMapListActivity.this.setupMapAnnotation(ShopLocalMapListActivity.this.mImprintList, ShopLocalMapListActivity.this.mSelectedImprintIndex, isBoxBoundaryFixed);
            ImprintSearchListAdapter isla = new ImprintSearchListAdapter(ShopLocalMapListActivity.this.mImprintList);
            ShopLocalMapListActivity.this.imprintSearchListView.setVisibility(0);
            ShopLocalMapListActivity.this.imprintSearchListView.setAdapter((ListAdapter) isla);
            ShopLocalMapListActivity.this.imprintSearchListView.setOnItemClickListener(isla);
            ShopLocalMapListActivity.this.imprintSearchListView.setOnTouchListener(ShopLocalMapListActivity.this);
            if (ShopLocalMapListActivity.this.mSelectedImprintIndex > 0) {
                ShopLocalMapListActivity.this.imprintSearchListView.setSelection(ShopLocalMapListActivity.this.mSelectedImprintIndex);
            }
            ShopLocalMapListActivity.this.imprintSearchListView.setOnScrollListener(new AbsListView.OnScrollListener() {
                private int priorFirst = -1;

                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    if (visibleItemCount >= 1 && visibleItemCount < totalItemCount) {
                        int firstOffsetTop = 0;
                        try {
                            firstOffsetTop = view.getChildAt(0).getTop();
                        } catch (Exception e) {
                        }
                        if (firstOffsetTop < -8) {
                            try {
                                ShopLocalMapListActivity.this.mSelectedImprintIndex = firstVisibleItem + 1;
                            } catch (Exception e2) {
                                e2.printStackTrace();
                                return;
                            }
                        } else {
                            ShopLocalMapListActivity.this.mSelectedImprintIndex = firstVisibleItem;
                        }
                        ShopLocalMapListActivity.this.mSelectAnnotationIndex = -1;
                        if (ShopLocalMapListActivity.this.mMaxTotalImprintCount > ShopLocalMapListActivity.this.mImprintList.size() && firstVisibleItem + visibleItemCount >= totalItemCount && firstVisibleItem != this.priorFirst) {
                            this.priorFirst = firstVisibleItem;
                            if (!ShopLocalMapListActivity.this.isRetrievingData) {
                                ShopLocalMapListActivity.this.mProgressbar.setVisibility(0);
                                ShopLocalMapListActivity.this.currentPageNumber++;
                                ShopLocalMapListActivity.this.fetchedAnnotationNumber = 0;
                                ShopLocalMapListActivity.this.fetchedPageNumber = 0;
                                ShopLocalMapListActivity.this.mDataRetrievedURL = ShopLocalMapListActivity.this.mDataRetrievedURL.replaceAll("&p=" + ShopLocalMapListActivity.this.currentPageNumber, "&p=" + ShopLocalMapListActivity.this.currentPageNumber);
                                ShopLocalMapListActivity.this.imprintSearchListView.setEnabled(false);
                                ShopLocalMapListActivity.this.updateResponseSearchResultDataAsyn(ShopLocalMapListActivity.this.mDataRetrievedURL);
                            }
                        }
                    }
                }

                public void onScrollStateChanged(AbsListView view, int scrollState) {
                }
            });
            ShopLocalMapListActivity.this.imprintSearchListView.setEnabled(true);
            if (ShopLocalMapListActivity.this.mIsFromHomeButton) {
                ShopLocalMapListActivity.this.imprintSearchMapView.setSatellite(false);
                ShopLocalMapListActivity.this.imprintSearchMapView.setStreetView(false);
                ShopLocalMapListActivity.this.mSearchLocationTextView.setText(ShopLocalMapListActivity.this.mSearchLocation);
                ShopLocalMapListActivity.this.resetFooterView();
                ShopLocalMapListActivity.this.mIsFromHomeButton = false;
            }
            ShopLocalMapListActivity.this.mBannerIndex = 0;
            ShopLocalMapListActivity.this.bannerImgView1.setImageBitmap(null);
            ShopLocalMapListActivity.this.bannerImgView2.setImageBitmap(null);
            ShopLocalMapListActivity.this.bannerImgView3.setImageBitmap(null);
            RelativeLayout.LayoutParams bvfPa = (RelativeLayout.LayoutParams) ShopLocalMapListActivity.this.bannerViewFlipper.getLayoutParams();
            if (ShopLocalMapListActivity.this.mImprintBannerList == null || ShopLocalMapListActivity.this.mImprintBannerList.size() <= 0) {
                bvfPa.height = 0;
            } else {
                try {
                    int bannerListSize = ShopLocalMapListActivity.this.mImprintBannerList.size();
                    if (bannerListSize > 2) {
                        bannerListSize = 3;
                    }
                    for (int bi = 0; bi < bannerListSize; bi++) {
                        Bitmap mIcon_val = BitmapFactory.decodeStream(new URL(((ImprintBanner) ShopLocalMapListActivity.this.mImprintBannerList.get(bi)).getImageUrl()).openConnection().getInputStream());
                        if (bi == 0) {
                            ShopLocalMapListActivity.this.bannerImgView1.setImageBitmap(mIcon_val);
                        } else if (bi == 1) {
                            ShopLocalMapListActivity.this.bannerImgView2.setImageBitmap(mIcon_val);
                        } else {
                            ShopLocalMapListActivity.this.bannerImgView3.setImageBitmap(mIcon_val);
                        }
                    }
                    bvfPa.height = -2;
                } catch (Exception e) {
                    bvfPa.height = 0;
                }
            }
            ShopLocalMapListActivity.this.bannerViewFlipper.setLayoutParams(bvfPa);
            ShopLocalMapListActivity.this.bannerViewFlipper.setInAnimation(null);
            ShopLocalMapListActivity.this.bannerViewFlipper.setOutAnimation(null);
            ShopLocalMapListActivity.this.bannerViewFlipper.setDisplayedChild(ShopLocalMapListActivity.this.mBannerIndex);
            ShopLocalMapListActivity.this.bannerViewFlipper.setInAnimation(AnimationUtils.loadAnimation(ShopLocalMapListActivity.this, R.anim.slide_left_in));
            ShopLocalMapListActivity.this.bannerViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(ShopLocalMapListActivity.this, R.anim.slide_left_out));
            ShopLocalMapListActivity.this.ShowBannerCircularPost();
            ShopLocalMapListActivity.this.mProgressbar.setVisibility(4);
            if (!ShopLocalMapListActivity.this.mIsFlipListViewOption) {
                return;
            }
            if (ShopLocalMapListActivity.this.mImprintList == null || ShopLocalMapListActivity.this.mImprintList.size() == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(ShopLocalMapListActivity.this).create();
                alertDialog.setTitle("No Search Results");
                alertDialog.setMessage("Please enter a keyword, business name or heading");
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        }
    };
    /* access modifiers changed from: private */
    public String mImprintListSortOption = "Relevance";
    private Spinner mImprintListSortSpinner;
    final Runnable mInitialShopLocalSearchTask = new Runnable() {
        public void run() {
            ShopLocalMapListActivity.this.setRequestedOrientation(-1);
            ShopLocalMapListActivity.this.imprintSearchMapView.invalidate();
            ShopLocalMapListActivity.this.mSearchLocationTextView.setText(ShopLocalMapListActivity.this.mSearchLocation);
            ShopLocalMapListActivity.this.mc.setZoom(ShopLocalMapListActivity.this.mZoomLevel);
            ShopLocalMapListActivity.this.mc.setCenter(ShopLocalMapListActivity.this.mGeoPoint_SearchLocation);
            ShopLocalMapListActivity.this.imprintSearchMapView.setSatellite(false);
            ShopLocalMapListActivity.this.imprintSearchMapView.setStreetView(false);
            ShopLocalMapListActivity.this.imprintSearchMapView.setReticleDrawMode(MapView.ReticleDrawMode.DRAW_RETICLE_NEVER);
            if (ShopLocalMapListActivity.this.mIsFlipListViewOption) {
                ShopLocalMapListActivity.this.mMapFlipButton.setImageResource(R.drawable.map_flip_globe);
                ShopLocalMapListActivity.this.imprintSearchViewFlipper.showNext();
            }
            ShopLocalMapListActivity.this.searchImprintResult();
        }
    };
    boolean mIsBoxSearch = false;
    private boolean mIsCurrentMode = false;
    private boolean mIsFirstTimeLocationUpdate = false;
    boolean mIsFlipListViewOption = false;
    /* access modifiers changed from: private */
    public boolean mIsFromHomeButton = false;
    /* access modifiers changed from: private */
    public boolean mIsPortrait = true;
    ArrayList<String> mKeywordList;
    /* access modifiers changed from: private */
    public ListView mKeywordListView;
    /* access modifiers changed from: private */
    public Runnable mKeywordThreadRetrieveUpdateTask = new Runnable() {
        public void run() {
            if (!ShopLocalMapListActivity.this.checkInternetConnection()) {
                if (!ShopLocalMapListActivity.this.internetConnectionAlertDialog.isShowing()) {
                    ShopLocalMapListActivity.this.internetConnectionAlertDialog.show();
                }
                ShopLocalMapListActivity.this.mProgressbar.setVisibility(4);
            } else if (!ShopLocalMapListActivity.this.isRetrievingData) {
                ShopLocalMapListActivity.this.mKeywordListView.setEnabled(false);
                new Thread() {
                    public void run() {
                        try {
                            String searchKeyword = ShopLocalMapListActivity.this.mSearchKeyworkEditText.getText().toString();
                            ShopLocalMapListActivity.this.getResponseKeywordData(String.format("http://%s/sys/pageserver.dll?gp=%s&pa=%s&f=what&go=%s", ShopLocalActivity.McGillSearchServerURL, ShopLocalActivity.DomainDeviceID, URLEncoder.encode(ShopLocalMapListActivity.this.mSearchLocation, "Latin-1").replaceAll("\\+", "%20"), URLEncoder.encode(searchKeyword, "Latin-1").replaceAll("\\+", "%20")));
                            ShopLocalMapListActivity.this.mShopLocalDataHandler.post(ShopLocalMapListActivity.this.mKeywordUpdateResults);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
            }
        }
    };
    final Runnable mKeywordUpdateResults = new Runnable() {
        public void run() {
            ShopLocalMapListActivity.this.isRetrievingData = false;
            KeywordListAdapter listViewArrayAdapter = new KeywordListAdapter(ShopLocalMapListActivity.this.mKeywordList);
            ShopLocalMapListActivity.this.mKeywordListView.setAdapter((ListAdapter) listViewArrayAdapter);
            ShopLocalMapListActivity.this.mKeywordListView.setOnItemClickListener(listViewArrayAdapter);
            ShopLocalMapListActivity.this.mKeywordListView.setOnScrollListener(new AbsListView.OnScrollListener() {
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                }

                public void onScrollStateChanged(AbsListView view, int scrollState) {
                    if (scrollState > 0) {
                        ShopLocalMapListActivity.this.hideKeyboard();
                        ShopLocalMapListActivity.this.mSearchKeyworkEditText.clearFocus();
                    }
                }
            });
            ShopLocalMapListActivity.this.mKeywordListView.setEnabled(true);
        }
    };
    /* access modifiers changed from: private */
    public ListView mLocationListView;
    private LocationManager mLocationManager;
    private String mLocationProviderString = null;
    /* access modifiers changed from: private */
    public Runnable mLocationThreadRetrieveUpdateTask = new Runnable() {
        public void run() {
            if (!ShopLocalMapListActivity.this.checkInternetConnection()) {
                if (!ShopLocalMapListActivity.this.internetConnectionAlertDialog.isShowing()) {
                    ShopLocalMapListActivity.this.internetConnectionAlertDialog.show();
                }
                ShopLocalMapListActivity.this.mProgressbar.setVisibility(4);
            } else if (!ShopLocalMapListActivity.this.isRetrievingData) {
                ShopLocalMapListActivity.this.mLocationListView.setEnabled(false);
                new Thread() {
                    public void run() {
                        try {
                            String searchLocation = ShopLocalMapListActivity.this.mSearchLocationEditText.getText().toString();
                            ShopLocalMapListActivity.this.getResponseLocationData(String.format("http://%s/sys/pageserver.dll?gp=%s&f=W%s", ShopLocalActivity.McGillSearchServerURL, ShopLocalActivity.DomainDeviceID, URLEncoder.encode(searchLocation, "Latin-1").replaceAll("\\+", "%20")));
                            ShopLocalMapListActivity.this.mShopLocalDataHandler.post(ShopLocalMapListActivity.this.mLocationUpdateResults);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
            }
        }
    };
    final Runnable mLocationUpdateResults = new Runnable() {
        public void run() {
            ShopLocalMapListActivity.this.isRetrievingData = false;
            GeoLocationAdapter gla = new GeoLocationAdapter(ShopLocalMapListActivity.this.mGeoLocationList);
            ShopLocalMapListActivity.this.mLocationListView.setAdapter((ListAdapter) gla);
            ShopLocalMapListActivity.this.mLocationListView.setOnItemClickListener(gla);
            ShopLocalMapListActivity.this.mLocationListView.setOnScrollListener(new AbsListView.OnScrollListener() {
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                }

                public void onScrollStateChanged(AbsListView view, int scrollState) {
                    if (scrollState > 0) {
                        ShopLocalMapListActivity.this.hideKeyboard();
                        ShopLocalMapListActivity.this.mSearchLocationEditText.clearFocus();
                    }
                }
            });
            ShopLocalMapListActivity.this.mLocationListView.setEnabled(true);
            ShopLocalMapListActivity.this.mSearchLocationEditText.requestFocus();
            ((InputMethodManager) ShopLocalMapListActivity.this.getSystemService("input_method")).showSoftInput(ShopLocalMapListActivity.this.mSearchLocationEditText, 2);
        }
    };
    private ImageButton mMapChangeButton;
    /* access modifiers changed from: private */
    public ImageButton mMapFlipButton;
    private ImageButton mMapMicButton;
    GeoPoint mMapcenterGeoPoint;
    int mMaxTotalImprintCount;
    ProgressBar mProgressbar;
    String mSearchHomeLocation;
    String mSearchKeyword = "";
    /* access modifiers changed from: private */
    public EditText mSearchKeyworkEditText;
    String mSearchLocation;
    /* access modifiers changed from: private */
    public EditText mSearchLocationEditText;
    /* access modifiers changed from: private */
    public TextView mSearchLocationTextView;
    int mSelectAnnotationIndex = -1;
    int mSelectedImprintIndex = -1;
    /* access modifiers changed from: private */
    public Handler mShopLocalDataHandler = new Handler();
    LayoutInflater mShopLocalInflater;
    SharedPreferences mShopLocalPrefs;
    final Runnable mShopLocalVoiceRecognitionTask = new Runnable() {
        public void run() {
            ShopLocalMapListActivity.this.HideBannerCircularPost();
            Intent intent = new Intent("android.speech.action.RECOGNIZE_SPEECH");
            intent.putExtra("android.speech.extra.LANGUAGE_MODEL", "free_form");
            intent.putExtra("android.speech.extra.PROMPT", "Example: Restaurants in Santa Barbara, CA");
            ShopLocalMapListActivity.this.startActivityForResult(intent, ShopLocalActivity.VOICE_RECOGNITION_REQUEST_CODE);
            ShopLocalMapListActivity.this.mProgressbar.setVisibility(4);
        }
    };
    GeoPoint mTopRightGeoPoint;
    int mZoomLevel = 15;
    RelativeLayout mapFooterView;
    RelativeLayout mapHeaderView;
    /* access modifiers changed from: private */
    public MapController mc;

    /* JADX WARN: Type inference failed for: r9v0, types: [android.content.Context, com.google.android.maps.MapActivity, com.santabarbarayp.www.ShopLocalMapListActivity] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void onCreate(android.os.Bundle r10) {
        /*
            r9 = this;
            r7 = 0
            r4 = 1
            r2 = 1024(0x400, float:1.435E-42)
            r3 = 0
            com.santabarbarayp.www.ShopLocalMapListActivity.super.onCreate(r10)
            android.content.res.Resources r0 = r9.getResources()
            android.content.res.Configuration r0 = r0.getConfiguration()
            int r0 = r0.orientation
            r1 = 2
            if (r0 != r1) goto L_0x0112
            r9.mIsPortrait = r3
            android.view.Window r0 = r9.getWindow()
            r0.setFlags(r2, r2)
        L_0x001f:
            r0 = 2130903052(0x7f03000c, float:1.7412911E38)
            r9.setContentView(r0)
            boolean r0 = r9.mIsPortrait
            if (r0 == 0) goto L_0x011d
            r9.setRequestedOrientation(r4)
        L_0x002c:
            android.content.SharedPreferences r0 = r9.getPreferences(r3)
            r9.mShopLocalPrefs = r0
            android.content.Intent r0 = r9.getIntent()
            android.os.Bundle r6 = r0.getExtras()
            java.lang.String r0 = "location_default_home"
            java.lang.String r0 = r6.getString(r0)
            r9.mDefaultHomeLocation = r0
            java.lang.String r0 = "location_default_street_address"
            java.lang.String r0 = r6.getString(r0)
            r9.mDefaultStreetLocation = r0
            java.lang.String r0 = "location_search_home"
            java.lang.String r0 = r6.getString(r0)
            r9.mSearchHomeLocation = r0
            com.google.android.maps.GeoPoint r0 = new com.google.android.maps.GeoPoint
            java.lang.String r1 = "location_default_home_latE6"
            int r1 = r6.getInt(r1)
            java.lang.String r2 = "location_default_home_lonE6"
            int r2 = r6.getInt(r2)
            r0.<init>(r1, r2)
            r9.mGeoPoint_DefaultHomeLocation = r0
            com.google.android.maps.GeoPoint r0 = new com.google.android.maps.GeoPoint
            java.lang.String r1 = "location_search_home_latE6"
            int r1 = r6.getInt(r1)
            java.lang.String r2 = "location_search_home_lonE6"
            int r2 = r6.getInt(r2)
            r0.<init>(r1, r2)
            r9.mGeoPoint_SearchHomeLocation = r0
            java.lang.String r0 = "keyword_search"
            java.lang.String r0 = r6.getString(r0)
            r9.mSearchKeyword = r0
            java.lang.String r0 = "location_search"
            java.lang.String r0 = r6.getString(r0)
            r9.mSearchLocation = r0
            com.google.android.maps.GeoPoint r0 = new com.google.android.maps.GeoPoint
            java.lang.String r1 = "location_search_latE6"
            int r1 = r6.getInt(r1)
            java.lang.String r2 = "location_search_lonE6"
            int r2 = r6.getInt(r2)
            r0.<init>(r1, r2)
            r9.mGeoPoint_SearchLocation = r0
            java.lang.String r0 = "is_from_list_view"
            boolean r0 = r6.getBoolean(r0)
            r9.mIsFlipListViewOption = r0
            java.lang.String r0 = "is_current_mode"
            boolean r0 = r6.getBoolean(r0)
            r9.mIsCurrentMode = r0
            r9.mIsFirstTimeLocationUpdate = r3
            r9.locationUpdateSystemTime = r7
            android.app.AlertDialog$Builder r0 = new android.app.AlertDialog$Builder
            r0.<init>(r9)
            java.lang.String r1 = "Network Connection Required"
            android.app.AlertDialog$Builder r0 = r0.setTitle(r1)
            java.lang.String r1 = "Ensure that airplane mode is turned off or Wi-Fi is enabled."
            android.app.AlertDialog$Builder r0 = r0.setMessage(r1)
            r1 = 2130837548(0x7f02002c, float:1.7280053E38)
            android.app.AlertDialog$Builder r0 = r0.setIcon(r1)
            java.lang.String r1 = "OK"
            com.santabarbarayp.www.ShopLocalMapListActivity$11 r2 = new com.santabarbarayp.www.ShopLocalMapListActivity$11
            r2.<init>()
            android.app.AlertDialog$Builder r0 = r0.setPositiveButton(r1, r2)
            android.app.AlertDialog r0 = r0.create()
            r9.internetConnectionAlertDialog = r0
            java.lang.String r0 = "location"
            java.lang.Object r0 = r9.getSystemService(r0)     // Catch:{ Exception -> 0x012a }
            android.location.LocationManager r0 = (android.location.LocationManager) r0     // Catch:{ Exception -> 0x012a }
            r9.mLocationManager = r0     // Catch:{ Exception -> 0x012a }
            android.location.LocationManager r0 = r9.mLocationManager     // Catch:{ Exception -> 0x012a }
            java.lang.String r0 = r9.getLocationProvider(r0)     // Catch:{ Exception -> 0x012a }
            r9.mLocationProviderString = r0     // Catch:{ Exception -> 0x012a }
            android.location.LocationManager r0 = r9.mLocationManager     // Catch:{ Exception -> 0x012a }
            java.lang.String r1 = r9.mLocationProviderString     // Catch:{ Exception -> 0x012a }
            r2 = 0
            r4 = 0
            r5 = r9
            r0.requestLocationUpdates(r1, r2, r4, r5)     // Catch:{ Exception -> 0x012a }
        L_0x00f5:
            r9.onInitialization()
            boolean r0 = r9.checkInternetConnection()
            if (r0 != 0) goto L_0x0122
            android.app.AlertDialog r0 = r9.internetConnectionAlertDialog
            boolean r0 = r0.isShowing()
            if (r0 != 0) goto L_0x010b
            android.app.AlertDialog r0 = r9.internetConnectionAlertDialog
            r0.show()
        L_0x010b:
            android.widget.ProgressBar r0 = r9.mProgressbar
            r1 = 4
            r0.setVisibility(r1)
        L_0x0111:
            return
        L_0x0112:
            r9.mIsPortrait = r4
            android.view.Window r0 = r9.getWindow()
            r0.clearFlags(r2)
            goto L_0x001f
        L_0x011d:
            r9.setRequestedOrientation(r3)
            goto L_0x002c
        L_0x0122:
            android.os.Handler r0 = r9.mShopLocalDataHandler
            java.lang.Runnable r1 = r9.mInitialShopLocalSearchTask
            r0.post(r1)
            goto L_0x0111
        L_0x012a:
            r0 = move-exception
            goto L_0x00f5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.santabarbarayp.www.ShopLocalMapListActivity.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        try {
            this.mLocationManager.removeUpdates(this);
        } catch (Exception e) {
        }
        ShopLocalMapListActivity.super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        try {
            this.mLocationManager.removeUpdates(this);
            this.mLocationProviderString = getLocationProvider(this.mLocationManager);
            this.mLocationManager.requestLocationUpdates(this.mLocationProviderString, 0, 0.0f, this);
        } catch (Exception e) {
        }
        ShopLocalMapListActivity.super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        try {
            this.mLocationManager.removeUpdates(this);
        } catch (Exception e) {
        }
        ShopLocalMapListActivity.super.onDestroy();
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        populateMenu(menu);
        return ShopLocalMapListActivity.super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        return applyMenuChoice(item) || ShopLocalMapListActivity.super.onOptionsItemSelected(item);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        if (getResources().getConfiguration().orientation == 2) {
            this.mIsPortrait = false;
            getWindow().setFlags(1024, 1024);
        } else {
            getWindow().clearFlags(1024);
            this.mIsPortrait = true;
        }
        if (((WindowManager) getSystemService("window")).getDefaultDisplay().getWidth() >= 800 || this.mIsPortrait) {
            this.mMapChangeButton.setVisibility(0);
        } else {
            this.mMapChangeButton.setVisibility(8);
        }
        resetFooterView();
        ShopLocalMapListActivity.super.onConfigurationChanged(newConfig);
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        SharedPreferences.Editor editor = this.mShopLocalPrefs.edit();
        editor.putString("SLHomeLocation", this.mDefaultHomeLocation);
        editor.putString("SLHomeStreetLocation", this.mDefaultStreetLocation);
        editor.putInt("SLHomeLocationLatitudeE6", this.mGeoPoint_DefaultHomeLocation.getLatitudeE6());
        editor.putInt("SLHomeLocationLongitudeE6", this.mGeoPoint_DefaultHomeLocation.getLongitudeE6());
        editor.putString("SLSearchHomeLocation", this.mSearchHomeLocation);
        editor.putInt("SLSearchHomeLocationLatitudeE6", this.mGeoPoint_SearchHomeLocation.getLatitudeE6());
        editor.putInt("SLSearchHomeLocationLongitudeE6", this.mGeoPoint_SearchHomeLocation.getLongitudeE6());
        editor.putString("SLSearchLocation", this.mSearchLocation);
        editor.putInt("SLSearchLocationLatitudeE6", this.mGeoPoint_SearchLocation.getLatitudeE6());
        editor.putInt("SLSearchLocationLongitudeE6", this.mGeoPoint_SearchLocation.getLongitudeE6());
        editor.putString("SLSearchKeyword", this.mSearchKeyworkEditText.getText().toString());
        editor.putBoolean("SLIsCurrentMode", this.mIsCurrentMode);
        editor.commit();
        ShopLocalMapListActivity.super.onSaveInstanceState(savedInstanceState);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            this.mSearchKeyworkEditText.clearFocus();
            this.imprintSearchViewFlipper.setVisibility(0);
            this.mProgressbar.setVisibility(4);
            if (this.mKeywordListView.getVisibility() == 0) {
                ShowBannerCircularPost();
                this.mKeywordListView.setVisibility(4);
                resetFooterView();
                this.imprintSearchMapView.invalidate();
                return true;
            }
            try {
                ShopLocalActivity.mIsFromListView = this.mIsFlipListViewOption;
                ShopLocalActivity.mSearchKeyworkEditText.setText(this.mSearchKeyworkEditText.getText().toString());
                ShopLocalActivity.mSearchKeyword = this.mSearchKeyword;
                ShopLocalActivity.mSearchLocationEditText.setText(this.mSearchHomeLocation);
                ShopLocalActivity.mSearchHomeLocation = this.mSearchHomeLocation;
                ShopLocalActivity.mSearchLocationTextView.setText(this.mSearchLocation);
                ShopLocalActivity.mSearchLocation = this.mSearchLocation;
                ShopLocalActivity.mDefaultHomeLocation = this.mDefaultHomeLocation;
                ShopLocalActivity.mDefaultStreetLocation = this.mDefaultStreetLocation;
                ShopLocalActivity.mGeoPoint_DefaultHomeLocation = this.mGeoPoint_DefaultHomeLocation;
                ShopLocalActivity.mGeoPoint_SearchHomeLocation = this.mGeoPoint_SearchHomeLocation;
                ShopLocalActivity.mGeoPoint_SearchLocation = this.mGeoPoint_SearchLocation;
                ShopLocalActivity.mIsCurrentMode = this.mIsCurrentMode;
            } catch (Exception e) {
            }
            HideBannerCircularPost();
            finish();
            return true;
        } else if (keyCode != 84) {
            return ShopLocalMapListActivity.super.onKeyDown(keyCode, event);
        } else {
            SharedPreferences.Editor editor = this.mShopLocalPrefs.edit();
            editor.putString("SLSearchKeyword", this.mSearchKeyworkEditText.getText().toString());
            editor.commit();
            this.imprintSearchListView.setVisibility(8);
            this.mProgressbar.setVisibility(0);
            if (this.mGeoLocationList == null || this.mGeoLocationList.size() < 1) {
                resetMap(((double) this.mGeoPoint_SearchLocation.getLatitudeE6()) / 1000000.0d, ((double) this.mGeoPoint_SearchLocation.getLongitudeE6()) / 1000000.0d, this.mSearchLocation);
            } else {
                GeoLocation gl = this.mGeoLocationList.get(0);
                resetMap(gl.getLat(), gl.getLng(), gl.getTitle());
            }
            return true;
        }
    }

    public String getLocationProvider(LocationManager locationManager) {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(1);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(1);
        return locationManager.getBestProvider(criteria, true);
    }

    public void onLocationChanged(Location location) {
        String localCity;
        setRequestedOrientation(-1);
        if (location != null) {
            long currentUpdateSystemTime = System.currentTimeMillis();
            if (currentUpdateSystemTime - this.locationUpdateSystemTime >= ShopLocalActivity.LOCATION_PROVIDER_TIME_SPAN_MILLS) {
                this.locationUpdateSystemTime = currentUpdateSystemTime;
                if (!this.mIsCurrentMode) {
                    this.mLocationManager.removeUpdates(this);
                } else {
                    if (!ShopLocalActivity.mIsTrackingCurrentMode) {
                        this.mLocationManager.removeUpdates(this);
                    }
                    if (!this.mIsFirstTimeLocationUpdate) {
                        if (ShopLocalActivity.mIsTrackingCurrentMode) {
                            try {
                                this.mLocationManager.removeUpdates(this);
                                this.mLocationProviderString = getLocationProvider(this.mLocationManager);
                                this.mLocationManager.requestLocationUpdates(this.mLocationProviderString, (long) ShopLocalActivity.LOCATION_PROVIDER_TIME_SPAN_MILLS, 20.0f, this);
                            } catch (Exception e) {
                            }
                        }
                        this.mIsFirstTimeLocationUpdate = true;
                    }
                }
                double lat = location.getLatitude();
                double lng = location.getLongitude();
                int latE6 = (int) (1000000.0d * lat);
                int longE6 = (int) (1000000.0d * lng);
                if (!((this.mGeoPoint_DefaultHomeLocation != null && this.mGeoPoint_DefaultHomeLocation.getLatitudeE6() == latE6 && this.mGeoPoint_DefaultHomeLocation.getLongitudeE6() == longE6) || (localCity = findCityAddress(lat, lng)) == null || localCity.length() <= 0)) {
                    this.mGeoPoint_DefaultHomeLocation = new GeoPoint(latE6, longE6);
                    int indexDelimiter = localCity.indexOf("~~");
                    if (indexDelimiter < 1) {
                        this.mDefaultHomeLocation = localCity;
                        this.mDefaultStreetLocation = null;
                    } else {
                        this.mDefaultHomeLocation = localCity.substring(indexDelimiter + 2);
                        this.mDefaultStreetLocation = localCity.substring(0, indexDelimiter);
                    }
                    SharedPreferences.Editor editor = this.mShopLocalPrefs.edit();
                    editor.putString("SLHomeLocation", this.mDefaultHomeLocation);
                    editor.putString("SLHomeStreetLocation", this.mDefaultStreetLocation);
                    editor.putInt("SLHomeLocationLatitudeE6", this.mGeoPoint_DefaultHomeLocation.getLatitudeE6());
                    editor.putInt("SLHomeLocationLongitudeE6", this.mGeoPoint_DefaultHomeLocation.getLongitudeE6());
                    editor.commit();
                }
                if (this.mIsCurrentMode) {
                    String str = this.mDefaultHomeLocation;
                    this.mSearchHomeLocation = str;
                    this.mSearchLocation = str;
                    this.mGeoPoint_SearchHomeLocation = new GeoPoint(this.mGeoPoint_DefaultHomeLocation.getLatitudeE6(), this.mGeoPoint_DefaultHomeLocation.getLongitudeE6());
                    this.mGeoPoint_SearchLocation = new GeoPoint(this.mGeoPoint_DefaultHomeLocation.getLatitudeE6(), this.mGeoPoint_DefaultHomeLocation.getLongitudeE6());
                    SharedPreferences.Editor editor2 = this.mShopLocalPrefs.edit();
                    editor2.putString("SLSearchHomeLocation", this.mSearchHomeLocation);
                    editor2.putInt("SLSearchHomeLocationLatitudeE6", this.mGeoPoint_SearchHomeLocation.getLatitudeE6());
                    editor2.putInt("SLSearchHomeLocationLongitudeE6", this.mGeoPoint_SearchHomeLocation.getLongitudeE6());
                    editor2.putString("SLSearchLocation", this.mSearchLocation);
                    editor2.putInt("SLSearchLocationLatitudeE6", this.mGeoPoint_SearchLocation.getLatitudeE6());
                    editor2.putInt("SLSearchLocationLongitudeE6", this.mGeoPoint_SearchLocation.getLongitudeE6());
                    editor2.commit();
                }
            }
        }
    }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    public void populateMenu(Menu menu) {
        menu.setQwertyMode(true);
        MenuItem itemMyLocation = menu.add(0, 12, 0, "My Location");
        itemMyLocation.setAlphabeticShortcut('q');
        itemMyLocation.setIcon((int) R.drawable.menu_mylocation);
        MenuItem itemChangeLocation = menu.add(0, 13, 1, "Change Location");
        itemChangeLocation.setAlphabeticShortcut('p');
        itemChangeLocation.setIcon(17301555);
        MenuItem itemSort = menu.add(0, 14, 2, "Sort");
        itemSort.setAlphabeticShortcut('s');
        itemSort.setIcon((int) R.drawable.menu_sort);
        MenuItem itemMic = menu.add(0, 16, 3, "Record");
        itemMic.setAlphabeticShortcut('r');
        itemMic.setIcon((int) R.drawable.menu_mic);
        if (!this.mIsFlipListViewOption) {
            MenuItem itemList = menu.add(0, 15, 4, "List View");
            itemList.setAlphabeticShortcut('l');
            itemList.setIcon((int) R.drawable.menu_list);
            return;
        }
        MenuItem itemMap = menu.add(0, 11, 4, "Map View");
        itemMap.setAlphabeticShortcut('m');
        itemMap.setIcon(17301561);
    }

    public boolean applyMenuChoice(MenuItem item) {
        switch (item.getItemId()) {
            case ShopLocalActivity.MenuMapView:
            case 15:
                mapViewFlipClick(null);
                return true;
            case 12:
                goMyLocation();
                return true;
            case ShopLocalActivity.MenuChangeLocation:
                this.isChange = true;
                this.mMapChangeButton.performClick();
                return true;
            case ShopLocalActivity.MenuSort:
                this.mImprintListSortSpinner.performClick();
                return true;
            case ShopLocalActivity.MenuMic:
                this.mMapMicButton.performClick();
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        GeoLocation locationWithCurrentState;
        ShowBannerCircularPost();
        if (requestCode == 1234 && resultCode == -1) {
            String recordedText = data.getStringArrayListExtra("android.speech.extra.RESULTS").get(0).replaceAll(" +", " ");
            String lowerRecordedText = recordedText.toLowerCase();
            String[] splitProps = {" in ", " near ", " at ", " by "};
            int largestIndexOfProp = -1;
            int wordIndex = -1;
            for (int i = 0; i < splitProps.length; i++) {
                int localindexOfProp = lowerRecordedText.lastIndexOf(splitProps[i]);
                if (localindexOfProp > largestIndexOfProp) {
                    largestIndexOfProp = localindexOfProp;
                    wordIndex = i;
                }
            }
            if (largestIndexOfProp > -1) {
                String keyProp = splitProps[wordIndex];
                String prefixKeyWord = recordedText.substring(0, largestIndexOfProp).trim();
                if (prefixKeyWord.length() > 0) {
                    this.mSearchKeyword = prefixKeyWord;
                }
                String localSearchLocation = recordedText.substring(keyProp.length() + largestIndexOfProp).trim();
                try {
                    GeoLocation firstLocation = findCityGeoLocation(localSearchLocation);
                    if (firstLocation != null) {
                        if (!isStateNameInside(localSearchLocation) && (locationWithCurrentState = findCityGeoLocation(String.valueOf(localSearchLocation) + " " + this.mDefaultHomeLocation.substring(this.mDefaultHomeLocation.indexOf(",") + 1))) != null) {
                            firstLocation = locationWithCurrentState;
                        }
                        double centerLatitude = firstLocation.getLat();
                        double centerLongitude = firstLocation.getLng();
                        this.mSearchLocation = firstLocation.getTitle();
                        boolean isSearchHomeChanged = false;
                        if (calculateDistance(((double) this.mGeoPoint_SearchHomeLocation.getLatitudeE6()) / 1000000.0d, ((double) this.mGeoPoint_SearchHomeLocation.getLongitudeE6()) / 1000000.0d, centerLatitude, centerLongitude) > 100.0d) {
                            isSearchHomeChanged = true;
                        }
                        if (isSearchHomeChanged) {
                            this.mSearchHomeLocation = this.mSearchLocation;
                            this.mGeoPoint_SearchHomeLocation = new GeoPoint((int) (1000000.0d * centerLatitude), (int) (1000000.0d * centerLongitude));
                        }
                        this.mGeoPoint_SearchLocation = new GeoPoint((int) (1000000.0d * centerLatitude), (int) (1000000.0d * centerLongitude));
                        this.mSearchLocationEditText.setText(this.mSearchLocation);
                        this.mSearchLocationTextView.setText(this.mSearchLocation);
                        this.mIsCurrentMode = false;
                        SharedPreferences.Editor editor = this.mShopLocalPrefs.edit();
                        editor.putString("SLSearchHomeLocation", this.mSearchHomeLocation);
                        editor.putInt("SLSearchHomeLocationLatitudeE6", this.mGeoPoint_SearchHomeLocation.getLatitudeE6());
                        editor.putInt("SLSearchHomeLocationLongitudeE6", this.mGeoPoint_SearchHomeLocation.getLongitudeE6());
                        editor.putString("SLSearchLocation", this.mSearchLocation);
                        editor.putInt("SLSearchLocationLatitudeE6", this.mGeoPoint_SearchLocation.getLatitudeE6());
                        editor.putInt("SLSearchLocationLongitudeE6", this.mGeoPoint_SearchLocation.getLongitudeE6());
                        editor.putBoolean("SLIsCurrentMode", this.mIsCurrentMode);
                        editor.commit();
                    } else {
                        this.mSearchKeyword = recordedText;
                    }
                } catch (Exception e) {
                }
            } else {
                int minimumNumber = 0;
                if (isStateNameInside(lowerRecordedText)) {
                    minimumNumber = 1;
                }
                boolean foundLocation = false;
                if (minimumNumber > 0) {
                    String[] splitwORDS = lowerRecordedText.split(" ");
                    ArrayList<String> addressWords = new ArrayList<>();
                    for (int i2 = splitwORDS.length - 1; i2 >= minimumNumber; i2--) {
                        for (int j = 0; j < splitwORDS.length; j++) {
                            String temp = "";
                            int lastIndex = j + i2;
                            if (lastIndex >= splitwORDS.length) {
                                break;
                            }
                            for (int k = j; k <= lastIndex; k++) {
                                temp = String.valueOf(temp) + " " + splitwORDS[k];
                            }
                            addressWords.add(temp.trim());
                        }
                    }
                    int m = 0;
                    while (true) {
                        if (m >= addressWords.size()) {
                            break;
                        }
                        String anAddressWord = (String) addressWords.get(m);
                        GeoLocation firstLocation2 = findCityGeoLocation(anAddressWord);
                        if (firstLocation2 != null) {
                            foundLocation = true;
                            int indexOfAddress = lowerRecordedText.indexOf(anAddressWord);
                            String prefixKeyWord2 = recordedText.substring(0, indexOfAddress).trim();
                            String suffixKeyWord = recordedText.substring(anAddressWord.length() + indexOfAddress);
                            if (suffixKeyWord.length() > 0) {
                                prefixKeyWord2 = (String.valueOf(prefixKeyWord2) + " " + suffixKeyWord.trim()).trim();
                            }
                            if (prefixKeyWord2.length() > 0) {
                                this.mSearchKeyword = prefixKeyWord2;
                            }
                            double centerLatitude2 = firstLocation2.getLat();
                            double centerLongitude2 = firstLocation2.getLng();
                            this.mSearchLocation = firstLocation2.getTitle();
                            boolean isSearchHomeChanged2 = false;
                            if (calculateDistance(((double) this.mGeoPoint_SearchHomeLocation.getLatitudeE6()) / 1000000.0d, ((double) this.mGeoPoint_SearchHomeLocation.getLongitudeE6()) / 1000000.0d, centerLatitude2, centerLongitude2) > 100.0d) {
                                isSearchHomeChanged2 = true;
                            }
                            if (isSearchHomeChanged2) {
                                this.mSearchHomeLocation = this.mSearchLocation;
                                this.mGeoPoint_SearchHomeLocation = new GeoPoint((int) (1000000.0d * centerLatitude2), (int) (1000000.0d * centerLongitude2));
                            }
                            this.mGeoPoint_SearchLocation = new GeoPoint((int) (firstLocation2.getLat() * 1000000.0d), (int) (firstLocation2.getLng() * 1000000.0d));
                            this.mSearchLocationEditText.setText(this.mSearchLocation);
                            this.mSearchLocationTextView.setText(this.mSearchLocation);
                            this.mIsCurrentMode = false;
                            SharedPreferences.Editor editor2 = this.mShopLocalPrefs.edit();
                            editor2.putString("SLSearchHomeLocation", this.mSearchHomeLocation);
                            editor2.putInt("SLSearchHomeLocationLatitudeE6", this.mGeoPoint_SearchHomeLocation.getLatitudeE6());
                            editor2.putInt("SLSearchHomeLocationLongitudeE6", this.mGeoPoint_SearchHomeLocation.getLongitudeE6());
                            editor2.putString("SLSearchLocation", this.mSearchLocation);
                            editor2.putInt("SLSearchLocationLatitudeE6", this.mGeoPoint_SearchLocation.getLatitudeE6());
                            editor2.putInt("SLSearchLocationLongitudeE6", this.mGeoPoint_SearchLocation.getLongitudeE6());
                            editor2.putBoolean("SLIsCurrentMode", this.mIsCurrentMode);
                            editor2.commit();
                        } else {
                            m++;
                        }
                    }
                }
                if (!foundLocation) {
                    this.mSearchKeyword = recordedText;
                }
            }
            this.mSearchKeyworkEditText.setText(this.mSearchKeyword);
            SharedPreferences.Editor editor3 = this.mShopLocalPrefs.edit();
            editor3.putString("SLSearchKeyword", this.mSearchKeyword);
            editor3.commit();
            this.imprintSearchListView.setVisibility(8);
            searchImprintResult();
        }
    }

    /* JADX WARN: Type inference failed for: r13v0, types: [android.content.Context, android.view.GestureDetector$OnGestureListener, com.santabarbarayp.www.ShopLocalMapListActivity, android.view.View$OnTouchListener, android.widget.ZoomButtonsController$OnZoomListener] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void onInitialization() {
        /*
            r13 = this;
            r12 = -1
            r11 = 4
            r10 = 1
            r9 = 0
            java.lang.String r6 = "layout_inflater"
            java.lang.Object r6 = r13.getSystemService(r6)
            android.view.LayoutInflater r6 = (android.view.LayoutInflater) r6
            r13.mShopLocalInflater = r6
            android.view.LayoutInflater r6 = r13.mShopLocalInflater
            r7 = 2130903053(0x7f03000d, float:1.7412913E38)
            r8 = 0
            android.view.View r3 = r6.inflate(r7, r8)
            android.widget.RelativeLayout$LayoutParams r6 = new android.widget.RelativeLayout$LayoutParams
            r6.<init>(r12, r12)
            r13.addContentView(r3, r6)
            r6 = 2131165266(0x7f070052, float:1.7944744E38)
            android.view.View r6 = r13.findViewById(r6)
            android.widget.ProgressBar r6 = (android.widget.ProgressBar) r6
            r13.mProgressbar = r6
            android.widget.ProgressBar r6 = r13.mProgressbar
            android.view.ViewGroup$LayoutParams r4 = r6.getLayoutParams()
            android.widget.RelativeLayout$LayoutParams r4 = (android.widget.RelativeLayout.LayoutParams) r4
            r6 = 13
            r4.addRule(r6, r12)
            r6 = 12
            r4.addRule(r6, r9)
            r4.bottomMargin = r9
            android.widget.ProgressBar r6 = r13.mProgressbar
            r6.setLayoutParams(r4)
            android.widget.ProgressBar r6 = r13.mProgressbar
            r6.setVisibility(r11)
            r6 = 2131165245(0x7f07003d, float:1.7944702E38)
            android.view.View r6 = r13.findViewById(r6)
            android.widget.EditText r6 = (android.widget.EditText) r6
            r13.mSearchKeyworkEditText = r6
            r6 = 2131165252(0x7f070044, float:1.7944716E38)
            android.view.View r6 = r13.findViewById(r6)
            android.widget.EditText r6 = (android.widget.EditText) r6
            r13.mSearchLocationEditText = r6
            r6 = 2131165251(0x7f070043, float:1.7944714E38)
            android.view.View r6 = r13.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r13.mSearchLocationTextView = r6
            r6 = 2131165243(0x7f07003b, float:1.7944698E38)
            android.view.View r6 = r13.findViewById(r6)
            android.widget.RelativeLayout r6 = (android.widget.RelativeLayout) r6
            r13.mapHeaderView = r6
            r6 = 2131165248(0x7f070040, float:1.7944708E38)
            android.view.View r6 = r13.findViewById(r6)
            android.widget.RelativeLayout r6 = (android.widget.RelativeLayout) r6
            r13.mapFooterView = r6
            r6 = 2131165260(0x7f07004c, float:1.7944732E38)
            android.view.View r6 = r13.findViewById(r6)
            com.santabarbarayp.www.ShopLocal_ViewFlipper r6 = (com.santabarbarayp.www.ShopLocal_ViewFlipper) r6
            r13.bannerViewFlipper = r6
            com.santabarbarayp.www.ShopLocal_ViewFlipper r6 = r13.bannerViewFlipper
            r6.setPersistentDrawingCache(r10)
            com.santabarbarayp.www.ShopLocal_ViewFlipper r6 = r13.bannerViewFlipper
            com.santabarbarayp.www.ShopLocalMapListActivity$12 r7 = new com.santabarbarayp.www.ShopLocalMapListActivity$12
            r7.<init>()
            r6.setOnClickListener(r7)
            com.santabarbarayp.www.ShopLocal_ViewFlipper r6 = r13.bannerViewFlipper
            r6.setOnTouchListener(r13)
            r6 = 2131165261(0x7f07004d, float:1.7944734E38)
            android.view.View r6 = r13.findViewById(r6)
            android.widget.ImageView r6 = (android.widget.ImageView) r6
            r13.bannerImgView1 = r6
            r6 = 2131165262(0x7f07004e, float:1.7944736E38)
            android.view.View r6 = r13.findViewById(r6)
            android.widget.ImageView r6 = (android.widget.ImageView) r6
            r13.bannerImgView2 = r6
            r6 = 2131165263(0x7f07004f, float:1.7944738E38)
            android.view.View r6 = r13.findViewById(r6)
            android.widget.ImageView r6 = (android.widget.ImageView) r6
            r13.bannerImgView3 = r6
            android.view.GestureDetector r6 = new android.view.GestureDetector
            r6.<init>(r13)
            r13.mGestureDetector = r6
            r6 = 2131165246(0x7f07003e, float:1.7944704E38)
            android.view.View r6 = r13.findViewById(r6)
            android.widget.ImageButton r6 = (android.widget.ImageButton) r6
            r13.mMapMicButton = r6
            r6 = 2131165259(0x7f07004b, float:1.794473E38)
            android.view.View r6 = r13.findViewById(r6)
            android.widget.ImageButton r6 = (android.widget.ImageButton) r6
            r13.mMapFlipButton = r6
            r6 = 2131165253(0x7f070045, float:1.7944718E38)
            android.view.View r6 = r13.findViewById(r6)
            android.widget.ImageButton r6 = (android.widget.ImageButton) r6
            r13.mMapChangeButton = r6
            r6 = 2131165249(0x7f070041, float:1.794471E38)
            android.view.View r6 = r13.findViewById(r6)
            android.widget.ImageButton r6 = (android.widget.ImageButton) r6
            r13.mHomeButton = r6
            r6 = 2131165256(0x7f070048, float:1.7944724E38)
            android.view.View r6 = r13.findViewById(r6)
            android.widget.ListView r6 = (android.widget.ListView) r6
            r13.mLocationListView = r6
            r6 = 2131165255(0x7f070047, float:1.7944722E38)
            android.view.View r6 = r13.findViewById(r6)
            android.widget.ListView r6 = (android.widget.ListView) r6
            r13.mKeywordListView = r6
            r6 = 2131165189(0x7f070005, float:1.7944588E38)
            android.view.View r6 = r13.findViewById(r6)
            com.google.android.maps.MapView r6 = (com.google.android.maps.MapView) r6
            r13.imprintSearchMapView = r6
            r6 = 2131165265(0x7f070051, float:1.7944742E38)
            android.view.View r6 = r13.findViewById(r6)
            android.widget.ListView r6 = (android.widget.ListView) r6
            r13.imprintSearchListView = r6
            r6 = 2131165264(0x7f070050, float:1.794474E38)
            android.view.View r6 = r13.findViewById(r6)
            com.santabarbarayp.www.ShopLocal_ViewFlipper r6 = (com.santabarbarayp.www.ShopLocal_ViewFlipper) r6
            r13.imprintSearchViewFlipper = r6
            com.santabarbarayp.www.ShopLocal_ViewFlipper r6 = r13.imprintSearchViewFlipper
            r6.setVisibility(r9)
            com.google.android.maps.MapView r6 = r13.imprintSearchMapView
            r6.setVisibility(r9)
            com.santabarbarayp.www.ShopLocal_ViewFlipper r6 = r13.imprintSearchViewFlipper
            r6.setPersistentDrawingCache(r10)
            android.widget.ListView r6 = r13.mKeywordListView
            r6.setVisibility(r11)
            r6 = 2131165258(0x7f07004a, float:1.7944728E38)
            android.view.View r6 = r13.findViewById(r6)
            android.widget.Spinner r6 = (android.widget.Spinner) r6
            r13.mImprintListSortSpinner = r6
            android.widget.ArrayAdapter r1 = new android.widget.ArrayAdapter
            r6 = 17367048(0x1090008, float:2.5162948E-38)
            r1.<init>(r13, r6)
            r6 = 17367049(0x1090009, float:2.516295E-38)
            r1.setDropDownViewResource(r6)
            java.lang.String r6 = "Relevance"
            r1.add(r6)
            java.lang.String r6 = "Distance"
            r1.add(r6)
            java.lang.String r6 = "Alphabetical"
            r1.add(r6)
            android.widget.Spinner r6 = r13.mImprintListSortSpinner
            r6.setAdapter(r1)
            android.widget.Spinner r6 = r13.mImprintListSortSpinner
            r7 = 2131034116(0x7f050004, float:1.767874E38)
            java.lang.String r7 = r13.getString(r7)
            r6.setPrompt(r7)
            android.widget.Spinner r6 = r13.mImprintListSortSpinner
            r6.setOnTouchListener(r13)
            android.widget.Spinner r6 = r13.mImprintListSortSpinner
            com.santabarbarayp.www.ShopLocalMapListActivity$13 r7 = new com.santabarbarayp.www.ShopLocalMapListActivity$13
            r7.<init>()
            r6.setOnItemSelectedListener(r7)
            com.google.android.maps.MapView r6 = r13.imprintSearchMapView
            r6.setBuiltInZoomControls(r10)
            com.google.android.maps.MapView r6 = r13.imprintSearchMapView
            android.widget.ZoomButtonsController r6 = r6.getZoomButtonsController()
            r6.setOnZoomListener(r13)
            com.google.android.maps.MapView r6 = r13.imprintSearchMapView
            com.google.android.maps.MapController r6 = r6.getController()
            r13.mc = r6
            android.widget.EditText r6 = r13.mSearchLocationEditText
            java.lang.String r7 = r13.mSearchHomeLocation
            r6.setText(r7)
            android.widget.TextView r6 = r13.mSearchLocationTextView
            java.lang.String r7 = r13.mSearchLocation
            r6.setText(r7)
            android.widget.EditText r6 = r13.mSearchKeyworkEditText
            com.santabarbarayp.www.ShopLocalMapListActivity$14 r7 = new com.santabarbarayp.www.ShopLocalMapListActivity$14
            r7.<init>()
            r6.setOnFocusChangeListener(r7)
            android.widget.EditText r6 = r13.mSearchKeyworkEditText
            com.santabarbarayp.www.ShopLocalMapListActivity$15 r7 = new com.santabarbarayp.www.ShopLocalMapListActivity$15
            r7.<init>()
            r6.addTextChangedListener(r7)
            android.widget.EditText r6 = r13.mSearchKeyworkEditText
            com.santabarbarayp.www.ShopLocalMapListActivity$16 r7 = new com.santabarbarayp.www.ShopLocalMapListActivity$16
            r7.<init>()
            r6.setOnKeyListener(r7)
            android.widget.EditText r6 = r13.mSearchKeyworkEditText
            java.lang.String r7 = r13.mSearchKeyword
            r6.setText(r7)
            android.widget.EditText r6 = r13.mSearchLocationEditText
            com.santabarbarayp.www.ShopLocalMapListActivity$17 r7 = new com.santabarbarayp.www.ShopLocalMapListActivity$17
            r7.<init>()
            r6.addTextChangedListener(r7)
            android.widget.EditText r6 = r13.mSearchLocationEditText
            com.santabarbarayp.www.ShopLocalMapListActivity$18 r7 = new com.santabarbarayp.www.ShopLocalMapListActivity$18
            r7.<init>()
            r6.setOnKeyListener(r7)
            android.widget.EditText r6 = r13.mSearchLocationEditText
            r6.setVisibility(r11)
            android.widget.ListView r6 = r13.mLocationListView
            r6.setVisibility(r11)
            r13.isChange = r10
            android.widget.ImageButton r6 = r13.mMapMicButton     // Catch:{ Exception -> 0x026d }
            r6.setOnTouchListener(r13)     // Catch:{ Exception -> 0x026d }
            android.content.pm.PackageManager r5 = r13.getPackageManager()     // Catch:{ Exception -> 0x026d }
            if (r5 == 0) goto L_0x026f
            android.content.Intent r6 = new android.content.Intent     // Catch:{ Exception -> 0x026d }
            java.lang.String r7 = "android.speech.action.RECOGNIZE_SPEECH"
            r6.<init>(r7)     // Catch:{ Exception -> 0x026d }
            r7 = 0
            java.util.List r0 = r5.queryIntentActivities(r6, r7)     // Catch:{ Exception -> 0x026d }
            int r6 = r0.size()     // Catch:{ Exception -> 0x026d }
            if (r6 <= 0) goto L_0x0266
            android.widget.ImageButton r6 = r13.mMapMicButton     // Catch:{ Exception -> 0x026d }
            com.santabarbarayp.www.ShopLocalMapListActivity$19 r7 = new com.santabarbarayp.www.ShopLocalMapListActivity$19     // Catch:{ Exception -> 0x026d }
            r7.<init>()     // Catch:{ Exception -> 0x026d }
            r6.setOnClickListener(r7)     // Catch:{ Exception -> 0x026d }
        L_0x0216:
            android.widget.ImageButton r6 = r13.mHomeButton
            com.santabarbarayp.www.ShopLocalMapListActivity$20 r7 = new com.santabarbarayp.www.ShopLocalMapListActivity$20
            r7.<init>()
            r6.setOnClickListener(r7)
            android.widget.ImageButton r6 = r13.mHomeButton
            r6.setOnTouchListener(r13)
            android.widget.ImageButton r6 = r13.mMapFlipButton
            com.santabarbarayp.www.ShopLocalMapListActivity$21 r7 = new com.santabarbarayp.www.ShopLocalMapListActivity$21
            r7.<init>()
            r6.setOnClickListener(r7)
            android.widget.ImageButton r6 = r13.mMapFlipButton
            r6.setOnTouchListener(r13)
            android.widget.ImageButton r6 = r13.mMapChangeButton
            com.santabarbarayp.www.ShopLocalMapListActivity$22 r7 = new com.santabarbarayp.www.ShopLocalMapListActivity$22
            r7.<init>()
            r6.setOnClickListener(r7)
            android.widget.ImageButton r6 = r13.mMapChangeButton
            r6.setOnTouchListener(r13)
            java.lang.String r6 = "window"
            java.lang.Object r6 = r13.getSystemService(r6)
            android.view.WindowManager r6 = (android.view.WindowManager) r6
            android.view.Display r2 = r6.getDefaultDisplay()
            int r6 = r2.getWidth()
            r7 = 800(0x320, float:1.121E-42)
            if (r6 >= r7) goto L_0x0276
            boolean r6 = r13.mIsPortrait
            if (r6 != 0) goto L_0x0276
            android.widget.ImageButton r6 = r13.mMapChangeButton
            r7 = 8
            r6.setVisibility(r7)
        L_0x0262:
            r13.resetFooterView()
            return
        L_0x0266:
            android.widget.ImageButton r6 = r13.mMapMicButton     // Catch:{ Exception -> 0x026d }
            r7 = 0
            r6.setEnabled(r7)     // Catch:{ Exception -> 0x026d }
            goto L_0x0216
        L_0x026d:
            r6 = move-exception
            goto L_0x0216
        L_0x026f:
            android.widget.ImageButton r6 = r13.mMapMicButton     // Catch:{ Exception -> 0x026d }
            r7 = 0
            r6.setEnabled(r7)     // Catch:{ Exception -> 0x026d }
            goto L_0x0216
        L_0x0276:
            android.widget.ImageButton r6 = r13.mMapChangeButton
            r6.setVisibility(r9)
            goto L_0x0262
        */
        throw new UnsupportedOperationException("Method not decompiled: com.santabarbarayp.www.ShopLocalMapListActivity.onInitialization():void");
    }

    public void getResponseLocationData(String url) {
        try {
            JSONArray jsonArray = new JSONArray(convertStreamToString(new URL(url).openConnection().getInputStream()));
            this.mGeoLocationList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONArray addressArray = jsonArray.getJSONArray(i);
                this.mGeoLocationList.add(new GeoLocation(addressArray.getString(0), addressArray.getDouble(1), addressArray.getDouble(2)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public GeoLocation getFirstResponseLocationData(String url) {
        try {
            JSONArray jsonArray = new JSONArray(convertStreamToString(new URL(url).openConnection().getInputStream()));
            if (jsonArray.length() > 0) {
                JSONArray addressArray = jsonArray.getJSONArray(0);
                return new GeoLocation(addressArray.getString(0), addressArray.getDouble(1), addressArray.getDouble(2));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void getResponseKeywordData(String url) {
        try {
            JSONArray jsonArray = new JSONArray(convertStreamToString(new URL(url).openConnection().getInputStream()));
            this.mKeywordList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                this.mKeywordList.add(jsonArray.getString(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateResponseSearchResultDataAsyn(String url) {
        final String reponseUrl = url;
        if (!checkInternetConnection()) {
            if (!this.internetConnectionAlertDialog.isShowing()) {
                this.internetConnectionAlertDialog.show();
            }
            this.mProgressbar.setVisibility(4);
            return;
        }
        this.isRetrievingData = true;
        new Thread() {
            public void run() {
                try {
                    ShopLocalMapListActivity.this.getResponseSearchResultData(reponseUrl);
                    ShopLocalMapListActivity.this.mShopLocalDataHandler.post(ShopLocalMapListActivity.this.mImprintListFinishUpdateResults);
                } catch (Exception e) {
                }
            }
        }.start();
    }

    public void getResponseSearchResultData(String url) {
        double imprintDistance;
        double imprintDistance2;
        try {
            JSONObject jSONObject = new JSONObject(convertStreamToString(new URL(url).openConnection().getInputStream()));
            if (jSONObject.isNull("notfound")) {
                this.mMaxTotalImprintCount = jSONObject.getInt("lct");
                if (this.mImprintBannerList != null) {
                    this.mImprintBannerList.clear();
                } else {
                    this.mImprintBannerList = new ImprintBannerList();
                }
                if (jSONObject.has("banners")) {
                    JSONArray jsonBannerArray = jSONObject.getJSONArray("banners");
                    for (int m = 0; m < jsonBannerArray.length(); m++) {
                        JSONObject imprintBannerDictionary = jsonBannerArray.getJSONObject(m);
                        this.mImprintBannerList.add(new ImprintBanner(imprintBannerDictionary.getString("name"), String.format("http://%s/sys/%s", ShopLocalActivity.McGillSearchServerURL, URLEncoder.encode(imprintBannerDictionary.getString("ban"), "Latin-1").replaceAll("\\+", "%20")), imprintBannerDictionary.getDouble("w"), imprintBannerDictionary.getDouble("h")));
                    }
                }
                JSONArray jsonCityArray = jSONObject.getJSONArray("city");
                if (jsonCityArray.length() > 0 && this.mIsBoxSearch) {
                    this.mSearchLocation = jsonCityArray.getString(0);
                    String localCenterCity = findCityAddress(((double) this.mGeoPoint_SearchLocation.getLatitudeE6()) / 1000000.0d, ((double) this.mGeoPoint_SearchLocation.getLongitudeE6()) / 1000000.0d);
                    if (localCenterCity != null) {
                        int indexDelimiter = localCenterCity.indexOf("~~");
                        if (indexDelimiter > 0) {
                            localCenterCity = localCenterCity.substring(indexDelimiter + 2).trim();
                        }
                        int nn = 0;
                        while (true) {
                            if (nn >= jsonCityArray.length()) {
                                break;
                            } else if (jsonCityArray.getString(nn).equalsIgnoreCase(localCenterCity)) {
                                this.mSearchLocation = localCenterCity;
                                break;
                            } else {
                                nn++;
                            }
                        }
                    }
                    SharedPreferences.Editor editor = this.mShopLocalPrefs.edit();
                    editor.putString("SLSearchLocation", this.mSearchLocation);
                    editor.putInt("SLSearchLocationLatitudeE6", this.mGeoPoint_SearchLocation.getLatitudeE6());
                    editor.putInt("SLSearchLocationLongitudeE6", this.mGeoPoint_SearchLocation.getLongitudeE6());
                    editor.putString("SLSearchKeyword", this.mSearchKeyworkEditText.getText().toString());
                    editor.commit();
                }
                if (this.currentPageNumber == 0) {
                    if (this.mImprintList != null) {
                        this.mImprintList.clear();
                    }
                    this.mImprintList = new ArrayList<>();
                    this.dataAnnotationNumber = 0;
                    this.fetchedAnnotationNumber = 0;
                    this.fetchedPageNumber = 0;
                    this.mSelectedImprintIndex = -1;
                }
                if (this.mMaxTotalImprintCount > 0) {
                    JSONArray jsonBoxArray = jSONObject.getJSONArray("box");
                    int bottomLeftLatitude = jsonBoxArray.getInt(0);
                    int bottomLeftLongitude = jsonBoxArray.getInt(1);
                    int topRightLatitude = jsonBoxArray.getInt(2);
                    int topRightLongitude = jsonBoxArray.getInt(3);
                    int cnterLatitudeE6 = (bottomLeftLatitude + topRightLatitude) / 2;
                    int cnterLongitudeE6 = (bottomLeftLongitude + topRightLongitude) / 2;
                    if (!this.mIsBoxSearch) {
                        this.mBottomLeftGeoPoint = new GeoPoint(bottomLeftLatitude, bottomLeftLongitude);
                        this.mTopRightGeoPoint = new GeoPoint(topRightLatitude, topRightLongitude);
                        this.mMapcenterGeoPoint = new GeoPoint(cnterLatitudeE6, cnterLongitudeE6);
                        this.mMapcenterGeoPoint = this.mGeoPoint_SearchLocation;
                        SharedPreferences.Editor editor2 = this.mShopLocalPrefs.edit();
                        editor2.putString("SLSearchLocation", this.mSearchLocation);
                        editor2.putInt("SLSearchLocationLatitudeE6", this.mGeoPoint_SearchLocation.getLatitudeE6());
                        editor2.putInt("SLSearchLocationLongitudeE6", this.mGeoPoint_SearchLocation.getLongitudeE6());
                        editor2.putString("SLSearchKeyword", this.mSearchKeyworkEditText.getText().toString());
                        editor2.commit();
                    }
                    JSONArray jsonArray = jSONObject.getJSONArray("l");
                    if (jsonArray == null || jsonArray.length() == 0) {
                        this.mMaxTotalImprintCount = this.mImprintList.size();
                    }
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject imprintDictionary = jsonArray.getJSONObject(i);
                        String imprintName = imprintDictionary.getString("n").replaceAll("&nbsp;", "");
                        String imprintHeaderName = imprintDictionary.getString("h").replaceAll("&nbsp;", "");
                        int premiumLevel = -1;
                        if (imprintDictionary.has("e")) {
                            premiumLevel = imprintDictionary.getInt("e");
                        }
                        if (imprintDictionary.has("d")) {
                            imprintDistance = imprintDictionary.getDouble("d");
                        } else {
                            imprintDistance = -1.0d;
                        }
                        int imprintGeoLocationIndex = -1;
                        boolean isWebSite = false;
                        boolean hasCoupon = false;
                        String imprintWebUrl = null;
                        String couponUrl = null;
                        String imprintLogo = null;
                        String adImageUrl = null;
                        String adDescription = null;
                        String adImageUrl2 = null;
                        String imprintEmailAddress = null;
                        String imprintMenu = null;
                        String imprintVideoUrl = null;
                        if (imprintDictionary.has("logo")) {
                            imprintLogo = String.format("http://%s/sys/%s", ShopLocalActivity.McGillSearchServerURL, URLEncoder.encode(imprintDictionary.getString("logo"), "Latin-1").replaceAll("\\+", "%20"));
                        }
                        if (imprintDictionary.has("cpn")) {
                            hasCoupon = true;
                            couponUrl = String.format("http://%s/sys/%s", ShopLocalActivity.McGillSearchServerURL, URLEncoder.encode(imprintDictionary.getString("cpn"), "Latin-1").replaceAll("\\+", "%20"));
                        }
                        if (imprintDictionary.has("menu")) {
                            imprintMenu = String.format("http://%s/sys/%s", ShopLocalActivity.McGillSearchServerURL, URLEncoder.encode(imprintDictionary.getString("menu"), "Latin-1").replaceAll("\\+", "%20"));
                        }
                        if (imprintDictionary.has("video")) {
                            imprintVideoUrl = String.format("http://%s/sys/%s", ShopLocalActivity.McGillSearchServerURL, URLEncoder.encode(imprintDictionary.getString("video"), "Latin-1").replaceAll("\\+", "%20"));
                        }
                        if (imprintDictionary.has("a")) {
                            adImageUrl = String.format("http://%s/sys/%s", ShopLocalActivity.McGillSearchServerURL, URLEncoder.encode(imprintDictionary.getString("a"), "Latin-1").replaceAll("\\+", "%20"));
                        }
                        if (imprintDictionary.has("desc")) {
                            adDescription = imprintDictionary.getString("desc");
                        }
                        if (imprintDictionary.has("a2")) {
                            adImageUrl2 = String.format("http://%s/sys/%s", ShopLocalActivity.McGillSearchServerURL, URLEncoder.encode(imprintDictionary.getString("a2"), "Latin-1").replaceAll("\\+", "%20"));
                        }
                        String imprintPhoneNumber = null;
                        ArrayList<String> phoneNumberList = new ArrayList<>();
                        String street = null;
                        String cityStateZip = null;
                        ArrayList<String> elementExtraLineList = new ArrayList<>();
                        double elmentLatitude = Double.NaN;
                        double elementLongitude = Double.NaN;
                        double elementDistance = -1.0d;
                        ImprintElementList elementList = new ImprintElementList();
                        boolean isNewAddress = false;
                        boolean isNewExtraLine = false;
                        boolean isNewElement = false;
                        int currentElementInsertPosition = -1;
                        if (imprintDictionary.has("el")) {
                            JSONArray jsonElementArray = imprintDictionary.getJSONArray("el");
                            for (int k = 0; k < jsonElementArray.length(); k++) {
                                if (k != jsonElementArray.length() - 1) {
                                    JSONObject imprintElmentDictionary = jsonElementArray.getJSONObject(k);
                                    switch (imprintElmentDictionary.getInt("d")) {
                                        case 3:
                                            if (isNewAddress || isNewElement) {
                                                isNewExtraLine = false;
                                                isNewElement = false;
                                                ImprintElement currentImprintElement = new ImprintElement(imprintName, street, cityStateZip, elementExtraLineList, phoneNumberList, elementDistance, elmentLatitude, elementLongitude, imprintLogo, -1);
                                                int currentElementInsertPosition2 = elementList.size();
                                                if (elementDistance > -1.0d) {
                                                    int j = 0;
                                                    while (true) {
                                                        if (j < elementList.size()) {
                                                            double localDistance = ((ImprintElement) elementList.get(j)).getDistance();
                                                            if (localDistance == -1.0d || localDistance > elementDistance) {
                                                                currentElementInsertPosition2 = j;
                                                            } else {
                                                                j++;
                                                            }
                                                        }
                                                    }
                                                    currentElementInsertPosition2 = j;
                                                }
                                                elementList.add(currentElementInsertPosition, currentImprintElement);
                                                cityStateZip = null;
                                                elementExtraLineList = new ArrayList<>();
                                                phoneNumberList = new ArrayList<>();
                                                elmentLatitude = Double.NaN;
                                                elementLongitude = Double.NaN;
                                                elementDistance = -1.0d;
                                            }
                                            isNewAddress = true;
                                            street = imprintElmentDictionary.getString("t");
                                            if (street != null) {
                                                street = street.replaceAll("<br>", " ").replaceAll("<BR>", " ").trim();
                                            }
                                            if (imprintElmentDictionary.has("c")) {
                                                cityStateZip = imprintElmentDictionary.getString("c");
                                            }
                                            if (!imprintElmentDictionary.has("g")) {
                                                break;
                                            } else {
                                                String geoLocation = imprintElmentDictionary.getString("g").trim();
                                                if (geoLocation.indexOf(",") > -1) {
                                                    geoLocation = geoLocation.replaceAll(" ", "");
                                                } else if (geoLocation.indexOf(" ") > -1) {
                                                    geoLocation = geoLocation.replaceFirst(" ", ",").replaceAll(" ", "");
                                                }
                                                String[] tempChunks = geoLocation.split(",");
                                                if (tempChunks.length <= 1) {
                                                    break;
                                                } else {
                                                    try {
                                                        elmentLatitude = Double.parseDouble(tempChunks[0]);
                                                        elementLongitude = Double.parseDouble(tempChunks[1]);
                                                        elementDistance = calculateDistance(((double) this.mGeoPoint_SearchHomeLocation.getLatitudeE6()) / 1000000.0d, ((double) this.mGeoPoint_SearchHomeLocation.getLongitudeE6()) / 1000000.0d, elmentLatitude, elementLongitude);
                                                        break;
                                                    } catch (Exception e) {
                                                        break;
                                                    }
                                                }
                                            }
                                            break;
                                        case 8:
                                            if (isNewAddress || isNewExtraLine) {
                                                isNewElement = true;
                                            }
                                            String phoneNumberString = imprintElmentDictionary.getString("t");
                                            if (phoneNumberString == null) {
                                                break;
                                            } else {
                                                String phoneNumberString2 = phoneNumberString.trim();
                                                String[] splitWords = phoneNumberString2.split("[0-9()]");
                                                if (splitWords.length > 0) {
                                                    phoneNumberString2 = phoneNumberString2.substring(splitWords[0].length());
                                                }
                                                if (phoneNumberString2.length() <= 0) {
                                                    break;
                                                } else {
                                                    String[] splitNumbers = phoneNumberString2.split("[,;]");
                                                    for (int kk = 0; kk < splitNumbers.length; kk++) {
                                                        String phonenumbers = splitNumbers[kk].trim();
                                                        if (phonenumbers.length() > 0) {
                                                            if (phonenumbers.startsWith("1-") || phonenumbers.startsWith("1 ")) {
                                                                phonenumbers = phonenumbers.substring(2).trim();
                                                            }
                                                            phoneNumberList.add(phonenumbers);
                                                        }
                                                    }
                                                    break;
                                                }
                                            }
                                        case 12:
                                            if (isNewElement) {
                                                isNewElement = false;
                                                isNewAddress = false;
                                                ImprintElement currentImprintElement2 = new ImprintElement(imprintName, street, cityStateZip, elementExtraLineList, phoneNumberList, elementDistance, elmentLatitude, elementLongitude, imprintLogo, -1);
                                                int currentElementInsertPosition3 = elementList.size();
                                                if (elementDistance > -1.0d) {
                                                    int j2 = 0;
                                                    while (true) {
                                                        if (j2 < elementList.size()) {
                                                            double localDistance2 = ((ImprintElement) elementList.get(j2)).getDistance();
                                                            if (localDistance2 == -1.0d || localDistance2 > elementDistance) {
                                                                currentElementInsertPosition3 = j2;
                                                            } else {
                                                                j2++;
                                                            }
                                                        }
                                                    }
                                                }
                                                elementList.add(currentElementInsertPosition, currentImprintElement2);
                                                street = null;
                                                cityStateZip = null;
                                                elementExtraLineList = new ArrayList<>();
                                                phoneNumberList = new ArrayList<>();
                                                elmentLatitude = Double.NaN;
                                                elementLongitude = Double.NaN;
                                                elementDistance = -1.0d;
                                            }
                                            isNewExtraLine = true;
                                            String elementExtraLine = imprintElmentDictionary.getString("t");
                                            if (elementExtraLine == null) {
                                                break;
                                            } else {
                                                String elementExtraLine2 = elementExtraLine.replaceAll("<br>", " ").replaceAll("<BR>", " ").trim();
                                                if (elementExtraLine2.length() <= 0) {
                                                    break;
                                                } else {
                                                    elementExtraLineList.add(elementExtraLine2);
                                                    break;
                                                }
                                            }
                                            break;
                                        case 15:
                                            imprintEmailAddress = imprintElmentDictionary.getString("t");
                                            break;
                                        case ShopLocalActivity.MenuMic:
                                            imprintWebUrl = imprintElmentDictionary.getString("t");
                                            isWebSite = true;
                                            break;
                                    }
                                } else if ((street != null && street.trim().length() != 0) || ((cityStateZip != null && cityStateZip.trim().length() != 0) || phoneNumberList.size() != 0)) {
                                    ImprintElement currentImprintElement3 = new ImprintElement(imprintName, street, cityStateZip, elementExtraLineList, phoneNumberList, elementDistance, elmentLatitude, elementLongitude, imprintLogo, -1);
                                    currentElementInsertPosition = elementList.size();
                                    if (elementDistance > -1.0d) {
                                        int j3 = 0;
                                        while (true) {
                                            if (j3 < elementList.size()) {
                                                double localDistance3 = ((ImprintElement) elementList.get(j3)).getDistance();
                                                if (localDistance3 == -1.0d || localDistance3 > elementDistance) {
                                                    currentElementInsertPosition = j3;
                                                } else {
                                                    j3++;
                                                }
                                            }
                                        }
                                        currentElementInsertPosition = j3;
                                    }
                                    elementList.add(currentElementInsertPosition, currentImprintElement3);
                                } else if (elementExtraLineList.size() > 0 && currentElementInsertPosition > -1) {
                                    ImprintElement currentImprintElement4 = (ImprintElement) elementList.get(currentElementInsertPosition);
                                    ArrayList<String> myExtraLineList = currentImprintElement4.getExtraLineList();
                                    if (myExtraLineList != null) {
                                        for (int index = myExtraLineList.size() - 1; index >= 0; index--) {
                                            elementExtraLineList.add(0, myExtraLineList.get(index));
                                        }
                                    }
                                    currentImprintElement4.setExtraLineList(elementExtraLineList);
                                }
                            }
                        }
                        int elementGeoLocationindex = 0;
                        for (int ii = 0; ii < elementList.size(); ii++) {
                            ImprintElement localElement = (ImprintElement) elementList.get(ii);
                            if (localElement.getDistance() > 0.0d) {
                                localElement.setGeoLocationIndex(elementGeoLocationindex);
                                elementGeoLocationindex++;
                            }
                        }
                        String imprintStreet = null;
                        String imprintCityStateZip = null;
                        double imprintLatitude = Double.NaN;
                        double imprintLongitude = Double.NaN;
                        if (elementList.size() > 0) {
                            ImprintElement firstElement = (ImprintElement) elementList.get(0);
                            imprintStreet = firstElement.getStreet();
                            imprintCityStateZip = firstElement.getCityStateZip();
                            ArrayList<String> phoneList = firstElement.getPhoneList();
                            if (phoneList != null && phoneList.size() > 0) {
                                imprintPhoneNumber = phoneList.get(0);
                            }
                            if (firstElement.getDistance() > -1.0d) {
                                imprintLatitude = firstElement.getLat();
                                imprintLongitude = firstElement.getLng();
                                imprintGeoLocationIndex = this.dataAnnotationNumber;
                                this.dataAnnotationNumber = imprintGeoLocationIndex + 1;
                                this.fetchedAnnotationNumber = this.fetchedAnnotationNumber + 1;
                            }
                            imprintDistance2 = firstElement.getDistance();
                        } else {
                            imprintDistance2 = imprintDistance;
                        }
                        if (elementList.size() > 0) {
                            this.mImprintList.add(new Imprint(imprintName, imprintHeaderName, imprintLogo, imprintDistance2, premiumLevel, imprintStreet, imprintCityStateZip, imprintPhoneNumber, imprintLatitude, imprintLongitude, isWebSite, hasCoupon, false, false, couponUrl, adDescription, adImageUrl, adImageUrl2, imprintWebUrl, imprintEmailAddress, imprintMenu, imprintVideoUrl, imprintGeoLocationIndex, elementList));
                        }
                    }
                }
                this.fetchedPageNumber = this.fetchedPageNumber + 1;
                if (3 >= this.fetchedPageNumber && 20 > this.fetchedAnnotationNumber && this.mMaxTotalImprintCount > this.mImprintList.size()) {
                    this.currentPageNumber = this.currentPageNumber + 1;
                    this.mDataRetrievedURL = url.replaceAll("&p=" + this.currentPageNumber, "&p=" + this.currentPageNumber);
                    getResponseSearchResultData(this.mDataRetrievedURL);
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void getFirstResponseSearchBannerData(String url) {
        double imprintDistance;
        double imprintDistance2;
        try {
            JSONObject jSONObject = new JSONObject(convertStreamToString(new URL(url).openConnection().getInputStream()));
            if (jSONObject.isNull("notfound") && jSONObject.getInt("lct") > 0) {
                JSONArray jsonArray = jSONObject.getJSONArray("l");
                if (jsonArray.length() > 0) {
                    JSONObject imprintDictionary = jsonArray.getJSONObject(0);
                    String imprintName = imprintDictionary.getString("n").replaceAll("&nbsp;", "");
                    String imprintHeaderName = imprintDictionary.getString("h").replaceAll("&nbsp;", "");
                    int premiumLevel = -1;
                    if (imprintDictionary.has("e")) {
                        premiumLevel = imprintDictionary.getInt("e");
                    }
                    if (imprintDictionary.has("d")) {
                        imprintDistance = imprintDictionary.getDouble("d");
                    } else {
                        imprintDistance = -1.0d;
                    }
                    boolean isWebSite = false;
                    boolean hasCoupon = false;
                    String imprintWebUrl = null;
                    String couponUrl = null;
                    String imprintLogo = null;
                    String adImageUrl = null;
                    String adDescription = null;
                    String adImageUrl2 = null;
                    String imprintEmailAddress = null;
                    String imprintMenu = null;
                    String imprintVideoUrl = null;
                    if (imprintDictionary.has("logo")) {
                        imprintLogo = String.format("http://%s/sys/%s", ShopLocalActivity.McGillSearchServerURL, URLEncoder.encode(imprintDictionary.getString("logo"), "Latin-1").replaceAll("\\+", "%20"));
                    }
                    if (imprintDictionary.has("cpn")) {
                        hasCoupon = true;
                        couponUrl = String.format("http://%s/sys/%s", ShopLocalActivity.McGillSearchServerURL, URLEncoder.encode(imprintDictionary.getString("cpn"), "Latin-1").replaceAll("\\+", "%20"));
                    }
                    if (imprintDictionary.has("menu")) {
                        imprintMenu = String.format("http://%s/sys/%s", ShopLocalActivity.McGillSearchServerURL, URLEncoder.encode(imprintDictionary.getString("menu"), "Latin-1").replaceAll("\\+", "%20"));
                    }
                    if (imprintDictionary.has("video")) {
                        imprintVideoUrl = String.format("http://%s/sys/%s", ShopLocalActivity.McGillSearchServerURL, URLEncoder.encode(imprintDictionary.getString("video"), "Latin-1").replaceAll("\\+", "%20"));
                    }
                    if (imprintDictionary.has("a")) {
                        adImageUrl = String.format("http://%s/sys/%s", ShopLocalActivity.McGillSearchServerURL, URLEncoder.encode(imprintDictionary.getString("a"), "Latin-1").replaceAll("\\+", "%20"));
                    }
                    if (imprintDictionary.has("desc")) {
                        adDescription = imprintDictionary.getString("desc");
                    }
                    if (imprintDictionary.has("a2")) {
                        adImageUrl2 = String.format("http://%s/sys/%s", ShopLocalActivity.McGillSearchServerURL, URLEncoder.encode(imprintDictionary.getString("a2"), "Latin-1").replaceAll("\\+", "%20"));
                    }
                    String imprintPhoneNumber = null;
                    ArrayList<String> phoneNumberList = new ArrayList<>();
                    String street = null;
                    String cityStateZip = null;
                    ArrayList<String> elementExtraLineList = new ArrayList<>();
                    double elmentLatitude = Double.NaN;
                    double elementLongitude = Double.NaN;
                    double elementDistance = -1.0d;
                    ImprintElementList elementList = new ImprintElementList();
                    boolean isNewAddress = false;
                    boolean isNewExtraLine = false;
                    boolean isNewElement = false;
                    int currentElementInsertPosition = -1;
                    if (imprintDictionary.has("el")) {
                        JSONArray jsonElementArray = imprintDictionary.getJSONArray("el");
                        for (int k = 0; k < jsonElementArray.length(); k++) {
                            if (k != jsonElementArray.length() - 1) {
                                JSONObject imprintElmentDictionary = jsonElementArray.getJSONObject(k);
                                switch (imprintElmentDictionary.getInt("d")) {
                                    case 3:
                                        if (isNewAddress || isNewElement) {
                                            isNewExtraLine = false;
                                            isNewElement = false;
                                            ImprintElement currentImprintElement = new ImprintElement(imprintName, street, cityStateZip, elementExtraLineList, phoneNumberList, elementDistance, elmentLatitude, elementLongitude, imprintLogo, -1);
                                            int currentElementInsertPosition2 = elementList.size();
                                            if (elementDistance > -1.0d) {
                                                int j = 0;
                                                while (true) {
                                                    if (j < elementList.size()) {
                                                        double localDistance = ((ImprintElement) elementList.get(j)).getDistance();
                                                        if (localDistance == -1.0d || localDistance > elementDistance) {
                                                            currentElementInsertPosition2 = j;
                                                        } else {
                                                            j++;
                                                        }
                                                    }
                                                }
                                                currentElementInsertPosition2 = j;
                                            }
                                            elementList.add(currentElementInsertPosition, currentImprintElement);
                                            cityStateZip = null;
                                            elementExtraLineList = new ArrayList<>();
                                            phoneNumberList = new ArrayList<>();
                                            elmentLatitude = Double.NaN;
                                            elementLongitude = Double.NaN;
                                            elementDistance = -1.0d;
                                        }
                                        isNewAddress = true;
                                        street = imprintElmentDictionary.getString("t");
                                        if (street != null) {
                                            street = street.replaceAll("<br>", " ").replaceAll("<BR>", " ").trim();
                                        }
                                        if (imprintElmentDictionary.has("c")) {
                                            cityStateZip = imprintElmentDictionary.getString("c");
                                        }
                                        if (imprintElmentDictionary.has("g")) {
                                            String geoLocation = imprintElmentDictionary.getString("g").trim();
                                            if (geoLocation.indexOf(",") > -1) {
                                                geoLocation = geoLocation.replaceAll(" ", "");
                                            } else if (geoLocation.indexOf(" ") > -1) {
                                                geoLocation = geoLocation.replaceFirst(" ", ",").replaceAll(" ", "");
                                            }
                                            String[] tempChunks = geoLocation.split(",");
                                            if (tempChunks.length <= 1) {
                                                break;
                                            } else {
                                                try {
                                                    elmentLatitude = Double.parseDouble(tempChunks[0]);
                                                    elementLongitude = Double.parseDouble(tempChunks[1]);
                                                    elementDistance = calculateDistance(((double) this.mGeoPoint_SearchHomeLocation.getLatitudeE6()) / 1000000.0d, ((double) this.mGeoPoint_SearchHomeLocation.getLongitudeE6()) / 1000000.0d, elmentLatitude, elementLongitude);
                                                    break;
                                                } catch (Exception e) {
                                                    break;
                                                }
                                            }
                                        } else {
                                            continue;
                                        }
                                    case 8:
                                        if (isNewAddress || isNewExtraLine) {
                                            isNewElement = true;
                                        }
                                        String phoneNumberString = imprintElmentDictionary.getString("t");
                                        if (phoneNumberString != null) {
                                            String phoneNumberString2 = phoneNumberString.trim();
                                            String[] splitWords = phoneNumberString2.split("[0-9()]");
                                            if (splitWords.length > 0) {
                                                phoneNumberString2 = phoneNumberString2.substring(splitWords[0].length());
                                            }
                                            if (phoneNumberString2.length() <= 0) {
                                                break;
                                            } else {
                                                String[] splitNumbers = phoneNumberString2.split("[,;]");
                                                for (int kk = 0; kk < splitNumbers.length; kk++) {
                                                    String phonenumbers = splitNumbers[kk].trim();
                                                    if (phonenumbers.length() > 0) {
                                                        if (phonenumbers.startsWith("1-") || phonenumbers.startsWith("1 ")) {
                                                            phonenumbers = phonenumbers.substring(2).trim();
                                                        }
                                                        phoneNumberList.add(phonenumbers);
                                                    }
                                                }
                                                break;
                                            }
                                        } else {
                                            continue;
                                        }
                                    case 12:
                                        if (isNewElement) {
                                            isNewElement = false;
                                            isNewAddress = false;
                                            ImprintElement currentImprintElement2 = new ImprintElement(imprintName, street, cityStateZip, elementExtraLineList, phoneNumberList, elementDistance, elmentLatitude, elementLongitude, imprintLogo, -1);
                                            int currentElementInsertPosition3 = elementList.size();
                                            if (elementDistance > -1.0d) {
                                                int j2 = 0;
                                                while (true) {
                                                    if (j2 < elementList.size()) {
                                                        double localDistance2 = ((ImprintElement) elementList.get(j2)).getDistance();
                                                        if (localDistance2 == -1.0d || localDistance2 > elementDistance) {
                                                            currentElementInsertPosition3 = j2;
                                                        } else {
                                                            j2++;
                                                        }
                                                    }
                                                }
                                                currentElementInsertPosition3 = j2;
                                            }
                                            elementList.add(currentElementInsertPosition, currentImprintElement2);
                                            street = null;
                                            cityStateZip = null;
                                            elementExtraLineList = new ArrayList<>();
                                            phoneNumberList = new ArrayList<>();
                                            elmentLatitude = Double.NaN;
                                            elementLongitude = Double.NaN;
                                            elementDistance = -1.0d;
                                        }
                                        isNewExtraLine = true;
                                        String elementExtraLine = imprintElmentDictionary.getString("t");
                                        if (elementExtraLine != null) {
                                            String elementExtraLine2 = elementExtraLine.replaceAll("<br>", " ").replaceAll("<BR>", " ").trim();
                                            if (elementExtraLine2.length() <= 0) {
                                                break;
                                            } else {
                                                elementExtraLineList.add(elementExtraLine2);
                                                break;
                                            }
                                        } else {
                                            continue;
                                        }
                                        break;
                                    case 15:
                                        imprintEmailAddress = imprintElmentDictionary.getString("t");
                                        continue;
                                    case ShopLocalActivity.MenuMic:
                                        imprintWebUrl = imprintElmentDictionary.getString("t");
                                        isWebSite = true;
                                        continue;
                                    default:
                                        continue;
                                }
                            } else if ((street != null && street.trim().length() != 0) || ((cityStateZip != null && cityStateZip.trim().length() != 0) || phoneNumberList.size() != 0)) {
                                ImprintElement currentImprintElement3 = new ImprintElement(imprintName, street, cityStateZip, elementExtraLineList, phoneNumberList, elementDistance, elmentLatitude, elementLongitude, imprintLogo, -1);
                                currentElementInsertPosition = elementList.size();
                                if (elementDistance > -1.0d) {
                                    int j3 = 0;
                                    while (true) {
                                        if (j3 < elementList.size()) {
                                            double localDistance3 = ((ImprintElement) elementList.get(j3)).getDistance();
                                            if (localDistance3 == -1.0d || localDistance3 > elementDistance) {
                                                currentElementInsertPosition = j3;
                                            } else {
                                                j3++;
                                            }
                                        }
                                    }
                                }
                                elementList.add(currentElementInsertPosition, currentImprintElement3);
                            } else if (elementExtraLineList.size() > 0 && currentElementInsertPosition > -1) {
                                ImprintElement currentImprintElement4 = (ImprintElement) elementList.get(currentElementInsertPosition);
                                ArrayList<String> myExtraLineList = currentImprintElement4.getExtraLineList();
                                if (myExtraLineList != null) {
                                    for (int index = myExtraLineList.size() - 1; index >= 0; index--) {
                                        elementExtraLineList.add(0, myExtraLineList.get(index));
                                    }
                                }
                                currentImprintElement4.setExtraLineList(elementExtraLineList);
                            }
                        }
                    }
                    int elementGeoLocationindex = 0;
                    for (int ii = 0; ii < elementList.size(); ii++) {
                        ImprintElement localElement = (ImprintElement) elementList.get(ii);
                        if (localElement.getDistance() > 0.0d) {
                            localElement.setGeoLocationIndex(elementGeoLocationindex);
                            elementGeoLocationindex++;
                        }
                    }
                    String imprintStreet = null;
                    String imprintCityStateZip = null;
                    if (elementList.size() > 0) {
                        ImprintElement firstElement = (ImprintElement) elementList.get(0);
                        imprintStreet = firstElement.getStreet();
                        imprintCityStateZip = firstElement.getCityStateZip();
                        ArrayList<String> phoneList = firstElement.getPhoneList();
                        if (phoneList != null && phoneList.size() > 0) {
                            imprintPhoneNumber = phoneList.get(0);
                        }
                        imprintDistance2 = firstElement.getDistance();
                    } else {
                        imprintDistance2 = imprintDistance;
                    }
                    if (elementList.size() > 0) {
                        this.mImprintAccessory = new ImprintAccessory("Advertisement", adDescription, "profile_ad", "profile_option_bg", new Imprint(imprintName, imprintHeaderName, imprintLogo, imprintDistance2, premiumLevel, imprintStreet, imprintCityStateZip, imprintPhoneNumber, Double.NaN, Double.NaN, isWebSite, hasCoupon, false, false, couponUrl, adDescription, adImageUrl, adImageUrl2, imprintWebUrl, imprintEmailAddress, imprintMenu, imprintVideoUrl, -1, elementList).getADImageUrl(), adImageUrl2);
                        String finalImageUrl = this.mImprintAccessory.getAccssoryUrl();
                        String finalImageUrl2 = this.mImprintAccessory.getAccssoryUrl2();
                        String imageFileName = "advertisement.png";
                        if (finalImageUrl.toLowerCase().endsWith(".jpg")) {
                            imageFileName = "advertisement.jpg";
                        } else if (finalImageUrl.toLowerCase().endsWith(".gif")) {
                            if (finalImageUrl2 == null || finalImageUrl2.length() == 0) {
                                imageFileName = "advertisement.gif";
                            } else {
                                imageFileName = "advertisement.png";
                            }
                        }
                        String finalImageFileName = imageFileName;
                        this.mImprintAccessory.setPhysicalPath(Environment.getExternalStorageDirectory() + "/shoplocal/download/" + finalImageFileName);
                        if (finalImageUrl2 != null) {
                            try {
                                if (finalImageUrl2.length() != 0) {
                                    this.mImprintAccessory.setBitmap(ImprintAccessory.DownloadImage(finalImageUrl, finalImageUrl2, finalImageFileName));
                                    this.mShopLocalDataHandler.post(this.mBitmapUpdateTask);
                                }
                            } catch (Exception e2) {
                                return;
                            }
                        }
                        this.mImprintAccessory.setBitmap(ImprintAccessory.DownloadImage(finalImageUrl, finalImageFileName));
                        this.mShopLocalDataHandler.post(this.mBitmapUpdateTask);
                    }
                }
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    /* JADX WARN: Type inference failed for: r13v0, types: [android.content.Context, com.santabarbarayp.www.ShopLocalMapListActivity, android.view.View$OnTouchListener] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void searchImprintResult() {
        /*
            r13 = this;
            r12 = 4
            android.widget.ProgressBar r6 = r13.mProgressbar     // Catch:{ Exception -> 0x0123 }
            r7 = 0
            r6.setVisibility(r7)     // Catch:{ Exception -> 0x0123 }
            r13.HideBannerCircularPost()     // Catch:{ Exception -> 0x0123 }
            r13.resetFooterView()     // Catch:{ Exception -> 0x0123 }
            com.google.android.maps.MapView r6 = r13.imprintSearchMapView     // Catch:{ Exception -> 0x0123 }
            r6.invalidate()     // Catch:{ Exception -> 0x0123 }
            r6 = 0
            r13.currentPageNumber = r6     // Catch:{ Exception -> 0x0123 }
            r6 = 0
            r13.dataAnnotationNumber = r6     // Catch:{ Exception -> 0x0123 }
            r6 = 0
            r13.mIsBoxSearch = r6     // Catch:{ Exception -> 0x0123 }
            java.util.ArrayList<com.santabarbarayp.www.Imprint> r6 = r13.mImprintList     // Catch:{ Exception -> 0x0123 }
            if (r6 == 0) goto L_0x0024
            java.util.ArrayList<com.santabarbarayp.www.Imprint> r6 = r13.mImprintList     // Catch:{ Exception -> 0x0123 }
            r6.clear()     // Catch:{ Exception -> 0x0123 }
        L_0x0024:
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch:{ Exception -> 0x0123 }
            r6.<init>()     // Catch:{ Exception -> 0x0123 }
            r13.mImprintList = r6     // Catch:{ Exception -> 0x0123 }
            android.widget.EditText r6 = r13.mSearchKeyworkEditText     // Catch:{ Exception -> 0x0123 }
            android.text.Editable r6 = r6.getText()     // Catch:{ Exception -> 0x0123 }
            java.lang.String r5 = r6.toString()     // Catch:{ Exception -> 0x0123 }
            if (r5 == 0) goto L_0x003d
            int r6 = r5.length()     // Catch:{ Exception -> 0x0123 }
            if (r6 != 0) goto L_0x009b
        L_0x003d:
            com.google.android.maps.MapView r6 = r13.imprintSearchMapView     // Catch:{ Exception -> 0x0123 }
            java.util.List r4 = r6.getOverlays()     // Catch:{ Exception -> 0x0123 }
            r4.clear()     // Catch:{ Exception -> 0x0123 }
            com.santabarbarayp.www.ShopLocalMapListActivity$ShopLocalOverlay r3 = new com.santabarbarayp.www.ShopLocalMapListActivity$ShopLocalOverlay     // Catch:{ Exception -> 0x0123 }
            r3.<init>()     // Catch:{ Exception -> 0x0123 }
            r4.add(r3)     // Catch:{ Exception -> 0x0123 }
            boolean r6 = r13.mIsFlipListViewOption     // Catch:{ Exception -> 0x0123 }
            if (r6 == 0) goto L_0x0074
            android.widget.ListView r6 = r13.imprintSearchListView     // Catch:{ Exception -> 0x0123 }
            r7 = 0
            r6.setEnabled(r7)     // Catch:{ Exception -> 0x0123 }
            com.santabarbarayp.www.ShopLocalMapListActivity$ImprintSearchListAdapter r2 = new com.santabarbarayp.www.ShopLocalMapListActivity$ImprintSearchListAdapter     // Catch:{ Exception -> 0x0123 }
            java.util.ArrayList<com.santabarbarayp.www.Imprint> r6 = r13.mImprintList     // Catch:{ Exception -> 0x0123 }
            r2.<init>(r6)     // Catch:{ Exception -> 0x0123 }
            android.widget.ListView r6 = r13.imprintSearchListView     // Catch:{ Exception -> 0x0123 }
            r6.setAdapter(r2)     // Catch:{ Exception -> 0x0123 }
            android.widget.ListView r6 = r13.imprintSearchListView     // Catch:{ Exception -> 0x0123 }
            r6.setOnItemClickListener(r2)     // Catch:{ Exception -> 0x0123 }
            android.widget.ListView r6 = r13.imprintSearchListView     // Catch:{ Exception -> 0x0123 }
            r6.setOnTouchListener(r13)     // Catch:{ Exception -> 0x0123 }
            android.widget.ListView r6 = r13.imprintSearchListView     // Catch:{ Exception -> 0x0123 }
            r7 = 1
            r6.setEnabled(r7)     // Catch:{ Exception -> 0x0123 }
        L_0x0074:
            android.widget.ProgressBar r6 = r13.mProgressbar     // Catch:{ Exception -> 0x0123 }
            r7 = 4
            r6.setVisibility(r7)     // Catch:{ Exception -> 0x0123 }
            android.app.AlertDialog$Builder r6 = new android.app.AlertDialog$Builder     // Catch:{ Exception -> 0x0123 }
            r6.<init>(r13)     // Catch:{ Exception -> 0x0123 }
            android.app.AlertDialog r0 = r6.create()     // Catch:{ Exception -> 0x0123 }
            java.lang.String r6 = "No Search Term Entered"
            r0.setTitle(r6)     // Catch:{ Exception -> 0x0123 }
            java.lang.String r6 = "Please enter a keyword, business name or heading"
            r0.setMessage(r6)     // Catch:{ Exception -> 0x0123 }
            java.lang.String r6 = "OK"
            com.santabarbarayp.www.ShopLocalMapListActivity$24 r7 = new com.santabarbarayp.www.ShopLocalMapListActivity$24     // Catch:{ Exception -> 0x0123 }
            r7.<init>()     // Catch:{ Exception -> 0x0123 }
            r0.setButton(r6, r7)     // Catch:{ Exception -> 0x0123 }
            r0.show()     // Catch:{ Exception -> 0x0123 }
        L_0x009a:
            return
        L_0x009b:
            android.widget.Spinner r6 = r13.mImprintListSortSpinner     // Catch:{ Exception -> 0x0123 }
            java.lang.Object r6 = r6.getSelectedItem()     // Catch:{ Exception -> 0x0123 }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ Exception -> 0x0123 }
            r13.mImprintListSortOption = r6     // Catch:{ Exception -> 0x0123 }
            boolean r6 = r13.mIsCurrentMode     // Catch:{ Exception -> 0x0123 }
            if (r6 == 0) goto L_0x01ee
            java.lang.String r6 = r13.mImprintListSortOption     // Catch:{ Exception -> 0x0123 }
            java.lang.String r7 = "Distance"
            boolean r6 = r6.equals(r7)     // Catch:{ Exception -> 0x0123 }
            if (r6 == 0) goto L_0x012c
            java.lang.String r6 = "http://%s/sys/pageserver.dll?gp=%s&pa=latlong&y=%d&x=%d&s=-20&go=%s&p=0&k=lat:%d;lon:%d;sort:d"
            r7 = 7
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x0123 }
            r8 = 0
            java.lang.String r9 = "www.localscout.com"
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 1
            java.lang.String r9 = "69.1"
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 2
            com.google.android.maps.GeoPoint r9 = r13.mGeoPoint_SearchLocation     // Catch:{ Exception -> 0x0123 }
            int r9 = r9.getLatitudeE6()     // Catch:{ Exception -> 0x0123 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 3
            com.google.android.maps.GeoPoint r9 = r13.mGeoPoint_SearchLocation     // Catch:{ Exception -> 0x0123 }
            int r9 = r9.getLongitudeE6()     // Catch:{ Exception -> 0x0123 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 4
            java.lang.String r9 = "Latin-1"
            java.lang.String r9 = java.net.URLEncoder.encode(r5, r9)     // Catch:{ Exception -> 0x0123 }
            java.lang.String r10 = "\\+"
            java.lang.String r11 = "%20"
            java.lang.String r9 = r9.replaceAll(r10, r11)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 5
            com.google.android.maps.GeoPoint r9 = r13.mGeoPoint_SearchHomeLocation     // Catch:{ Exception -> 0x0123 }
            int r9 = r9.getLatitudeE6()     // Catch:{ Exception -> 0x0123 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 6
            com.google.android.maps.GeoPoint r9 = r13.mGeoPoint_SearchHomeLocation     // Catch:{ Exception -> 0x0123 }
            int r9 = r9.getLongitudeE6()     // Catch:{ Exception -> 0x0123 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            java.lang.String r6 = java.lang.String.format(r6, r7)     // Catch:{ Exception -> 0x0123 }
            r13.mDataRetrievedURL = r6     // Catch:{ Exception -> 0x0123 }
        L_0x010d:
            android.widget.EditText r6 = r13.mSearchKeyworkEditText     // Catch:{ Exception -> 0x0123 }
            r6.clearFocus()     // Catch:{ Exception -> 0x0123 }
            boolean r6 = r13.isRetrievingData     // Catch:{ Exception -> 0x0123 }
            if (r6 != 0) goto L_0x0301
            android.widget.ListView r6 = r13.imprintSearchListView     // Catch:{ Exception -> 0x0123 }
            r7 = 0
            r6.setEnabled(r7)     // Catch:{ Exception -> 0x0123 }
            java.lang.String r6 = r13.mDataRetrievedURL     // Catch:{ Exception -> 0x0123 }
            r13.updateResponseSearchResultDataAsyn(r6)     // Catch:{ Exception -> 0x0123 }
            goto L_0x009a
        L_0x0123:
            r6 = move-exception
            r1 = r6
            android.widget.ProgressBar r6 = r13.mProgressbar
            r6.setVisibility(r12)
            goto L_0x009a
        L_0x012c:
            java.lang.String r6 = r13.mImprintListSortOption     // Catch:{ Exception -> 0x0123 }
            java.lang.String r7 = "Alphabetical"
            boolean r6 = r6.equals(r7)     // Catch:{ Exception -> 0x0123 }
            if (r6 == 0) goto L_0x0192
            java.lang.String r6 = "http://%s/sys/pageserver.dll?gp=%s&pa=latlong&y=%d&x=%d&s=-20&go=%s&p=0&k=lat:%d;lon:%d;sort:a"
            r7 = 7
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x0123 }
            r8 = 0
            java.lang.String r9 = "www.localscout.com"
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 1
            java.lang.String r9 = "69.1"
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 2
            com.google.android.maps.GeoPoint r9 = r13.mGeoPoint_SearchLocation     // Catch:{ Exception -> 0x0123 }
            int r9 = r9.getLatitudeE6()     // Catch:{ Exception -> 0x0123 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 3
            com.google.android.maps.GeoPoint r9 = r13.mGeoPoint_SearchLocation     // Catch:{ Exception -> 0x0123 }
            int r9 = r9.getLongitudeE6()     // Catch:{ Exception -> 0x0123 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 4
            java.lang.String r9 = "Latin-1"
            java.lang.String r9 = java.net.URLEncoder.encode(r5, r9)     // Catch:{ Exception -> 0x0123 }
            java.lang.String r10 = "\\+"
            java.lang.String r11 = "%20"
            java.lang.String r9 = r9.replaceAll(r10, r11)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 5
            com.google.android.maps.GeoPoint r9 = r13.mGeoPoint_SearchHomeLocation     // Catch:{ Exception -> 0x0123 }
            int r9 = r9.getLatitudeE6()     // Catch:{ Exception -> 0x0123 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 6
            com.google.android.maps.GeoPoint r9 = r13.mGeoPoint_SearchHomeLocation     // Catch:{ Exception -> 0x0123 }
            int r9 = r9.getLongitudeE6()     // Catch:{ Exception -> 0x0123 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            java.lang.String r6 = java.lang.String.format(r6, r7)     // Catch:{ Exception -> 0x0123 }
            r13.mDataRetrievedURL = r6     // Catch:{ Exception -> 0x0123 }
            goto L_0x010d
        L_0x0192:
            java.lang.String r6 = "http://%s/sys/pageserver.dll?gp=%s&pa=latlong&y=%d&x=%d&s=-20&go=%s&p=0&k=lat:%d;lon:%d;"
            r7 = 7
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x0123 }
            r8 = 0
            java.lang.String r9 = "www.localscout.com"
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 1
            java.lang.String r9 = "69.1"
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 2
            com.google.android.maps.GeoPoint r9 = r13.mGeoPoint_SearchLocation     // Catch:{ Exception -> 0x0123 }
            int r9 = r9.getLatitudeE6()     // Catch:{ Exception -> 0x0123 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 3
            com.google.android.maps.GeoPoint r9 = r13.mGeoPoint_SearchLocation     // Catch:{ Exception -> 0x0123 }
            int r9 = r9.getLongitudeE6()     // Catch:{ Exception -> 0x0123 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 4
            java.lang.String r9 = "Latin-1"
            java.lang.String r9 = java.net.URLEncoder.encode(r5, r9)     // Catch:{ Exception -> 0x0123 }
            java.lang.String r10 = "\\+"
            java.lang.String r11 = "%20"
            java.lang.String r9 = r9.replaceAll(r10, r11)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 5
            com.google.android.maps.GeoPoint r9 = r13.mGeoPoint_SearchHomeLocation     // Catch:{ Exception -> 0x0123 }
            int r9 = r9.getLatitudeE6()     // Catch:{ Exception -> 0x0123 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 6
            com.google.android.maps.GeoPoint r9 = r13.mGeoPoint_SearchHomeLocation     // Catch:{ Exception -> 0x0123 }
            int r9 = r9.getLongitudeE6()     // Catch:{ Exception -> 0x0123 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            java.lang.String r6 = java.lang.String.format(r6, r7)     // Catch:{ Exception -> 0x0123 }
            r13.mDataRetrievedURL = r6     // Catch:{ Exception -> 0x0123 }
            goto L_0x010d
        L_0x01ee:
            java.lang.String r6 = r13.mImprintListSortOption     // Catch:{ Exception -> 0x0123 }
            java.lang.String r7 = "Distance"
            boolean r6 = r6.equals(r7)     // Catch:{ Exception -> 0x0123 }
            if (r6 == 0) goto L_0x024d
            java.lang.String r6 = "http://%s/sys/pageserver.dll?gp=%s&pa=%s&s=-20&go=%s&p=0&k=lat:%d;lon:%d;sort:d"
            r7 = 6
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x0123 }
            r8 = 0
            java.lang.String r9 = "www.localscout.com"
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 1
            java.lang.String r9 = "69.1"
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 2
            java.lang.String r9 = r13.mSearchLocation     // Catch:{ Exception -> 0x0123 }
            java.lang.String r10 = "Latin-1"
            java.lang.String r9 = java.net.URLEncoder.encode(r9, r10)     // Catch:{ Exception -> 0x0123 }
            java.lang.String r10 = "\\+"
            java.lang.String r11 = "%20"
            java.lang.String r9 = r9.replaceAll(r10, r11)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 3
            java.lang.String r9 = "Latin-1"
            java.lang.String r9 = java.net.URLEncoder.encode(r5, r9)     // Catch:{ Exception -> 0x0123 }
            java.lang.String r10 = "\\+"
            java.lang.String r11 = "%20"
            java.lang.String r9 = r9.replaceAll(r10, r11)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 4
            com.google.android.maps.GeoPoint r9 = r13.mGeoPoint_SearchHomeLocation     // Catch:{ Exception -> 0x0123 }
            int r9 = r9.getLatitudeE6()     // Catch:{ Exception -> 0x0123 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 5
            com.google.android.maps.GeoPoint r9 = r13.mGeoPoint_SearchHomeLocation     // Catch:{ Exception -> 0x0123 }
            int r9 = r9.getLongitudeE6()     // Catch:{ Exception -> 0x0123 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            java.lang.String r6 = java.lang.String.format(r6, r7)     // Catch:{ Exception -> 0x0123 }
            r13.mDataRetrievedURL = r6     // Catch:{ Exception -> 0x0123 }
            goto L_0x010d
        L_0x024d:
            java.lang.String r6 = r13.mImprintListSortOption     // Catch:{ Exception -> 0x0123 }
            java.lang.String r7 = "Alphabetical"
            boolean r6 = r6.equals(r7)     // Catch:{ Exception -> 0x0123 }
            if (r6 == 0) goto L_0x02ac
            java.lang.String r6 = "http://%s/sys/pageserver.dll?gp=%s&pa=%s&s=-20&go=%s&p=0&k=lat:%d;lon:%d;sort:a"
            r7 = 6
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x0123 }
            r8 = 0
            java.lang.String r9 = "www.localscout.com"
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 1
            java.lang.String r9 = "69.1"
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 2
            java.lang.String r9 = r13.mSearchLocation     // Catch:{ Exception -> 0x0123 }
            java.lang.String r10 = "Latin-1"
            java.lang.String r9 = java.net.URLEncoder.encode(r9, r10)     // Catch:{ Exception -> 0x0123 }
            java.lang.String r10 = "\\+"
            java.lang.String r11 = "%20"
            java.lang.String r9 = r9.replaceAll(r10, r11)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 3
            java.lang.String r9 = "Latin-1"
            java.lang.String r9 = java.net.URLEncoder.encode(r5, r9)     // Catch:{ Exception -> 0x0123 }
            java.lang.String r10 = "\\+"
            java.lang.String r11 = "%20"
            java.lang.String r9 = r9.replaceAll(r10, r11)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 4
            com.google.android.maps.GeoPoint r9 = r13.mGeoPoint_SearchHomeLocation     // Catch:{ Exception -> 0x0123 }
            int r9 = r9.getLatitudeE6()     // Catch:{ Exception -> 0x0123 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 5
            com.google.android.maps.GeoPoint r9 = r13.mGeoPoint_SearchHomeLocation     // Catch:{ Exception -> 0x0123 }
            int r9 = r9.getLongitudeE6()     // Catch:{ Exception -> 0x0123 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            java.lang.String r6 = java.lang.String.format(r6, r7)     // Catch:{ Exception -> 0x0123 }
            r13.mDataRetrievedURL = r6     // Catch:{ Exception -> 0x0123 }
            goto L_0x010d
        L_0x02ac:
            java.lang.String r6 = "http://%s/sys/pageserver.dll?gp=%s&pa=%s&s=-20&go=%s&p=0&k=lat:%d;lon:%d;"
            r7 = 6
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x0123 }
            r8 = 0
            java.lang.String r9 = "www.localscout.com"
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 1
            java.lang.String r9 = "69.1"
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 2
            java.lang.String r9 = r13.mSearchLocation     // Catch:{ Exception -> 0x0123 }
            java.lang.String r10 = "Latin-1"
            java.lang.String r9 = java.net.URLEncoder.encode(r9, r10)     // Catch:{ Exception -> 0x0123 }
            java.lang.String r10 = "\\+"
            java.lang.String r11 = "%20"
            java.lang.String r9 = r9.replaceAll(r10, r11)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 3
            java.lang.String r9 = "Latin-1"
            java.lang.String r9 = java.net.URLEncoder.encode(r5, r9)     // Catch:{ Exception -> 0x0123 }
            java.lang.String r10 = "\\+"
            java.lang.String r11 = "%20"
            java.lang.String r9 = r9.replaceAll(r10, r11)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 4
            com.google.android.maps.GeoPoint r9 = r13.mGeoPoint_SearchHomeLocation     // Catch:{ Exception -> 0x0123 }
            int r9 = r9.getLatitudeE6()     // Catch:{ Exception -> 0x0123 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            r8 = 5
            com.google.android.maps.GeoPoint r9 = r13.mGeoPoint_SearchHomeLocation     // Catch:{ Exception -> 0x0123 }
            int r9 = r9.getLongitudeE6()     // Catch:{ Exception -> 0x0123 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0123 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0123 }
            java.lang.String r6 = java.lang.String.format(r6, r7)     // Catch:{ Exception -> 0x0123 }
            r13.mDataRetrievedURL = r6     // Catch:{ Exception -> 0x0123 }
            goto L_0x010d
        L_0x0301:
            android.widget.ProgressBar r6 = r13.mProgressbar     // Catch:{ Exception -> 0x0123 }
            r7 = 4
            r6.setVisibility(r7)     // Catch:{ Exception -> 0x0123 }
            goto L_0x009a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.santabarbarayp.www.ShopLocalMapListActivity.searchImprintResult():void");
    }

    public void searchImprintBanner() {
        try {
            this.mProgressbar.setVisibility(0);
            String searchKeyword = null;
            if (this.mBannerIndex >= 0 && this.mBannerIndex < this.mImprintBannerList.size()) {
                searchKeyword = ((ImprintBanner) this.mImprintBannerList.get(this.mBannerIndex)).getName();
            }
            if (searchKeyword == null || searchKeyword.length() == 0) {
                this.mProgressbar.setVisibility(4);
                return;
            }
            this.mImprintListSortOption = (String) this.mImprintListSortSpinner.getSelectedItem();
            if (this.mIsCurrentMode) {
                if (this.mImprintListSortOption.equals("Distance")) {
                    this.mDataRetrievedURL = String.format("http://%s/sys/pageserver.dll?gp=%s&pa=latlong&y=%d&x=%d&s=-20&go=%s&p=0&k=lat:%d;lon:%d;sort:d", ShopLocalActivity.McGillSearchServerURL, ShopLocalActivity.DomainDeviceID, Integer.valueOf(this.mGeoPoint_SearchLocation.getLatitudeE6()), Integer.valueOf(this.mGeoPoint_SearchLocation.getLongitudeE6()), URLEncoder.encode(searchKeyword, "Latin-1").replaceAll("\\+", "%20"), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLatitudeE6()), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLongitudeE6()));
                } else if (this.mImprintListSortOption.equals("Alphabetical")) {
                    this.mDataRetrievedURL = String.format("http://%s/sys/pageserver.dll?gp=%s&pa=latlong&y=%d&x=%d&s=-20&go=%s&p=0&k=lat:%d;lon:%d;sort:a", ShopLocalActivity.McGillSearchServerURL, ShopLocalActivity.DomainDeviceID, Integer.valueOf(this.mGeoPoint_SearchLocation.getLatitudeE6()), Integer.valueOf(this.mGeoPoint_SearchLocation.getLongitudeE6()), URLEncoder.encode(searchKeyword, "Latin-1").replaceAll("\\+", "%20"), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLatitudeE6()), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLongitudeE6()));
                } else {
                    this.mDataRetrievedURL = String.format("http://%s/sys/pageserver.dll?gp=%s&pa=latlong&y=%d&x=%d&s=-20&go=%s&p=0&k=lat:%d;lon:%d;", ShopLocalActivity.McGillSearchServerURL, ShopLocalActivity.DomainDeviceID, Integer.valueOf(this.mGeoPoint_SearchLocation.getLatitudeE6()), Integer.valueOf(this.mGeoPoint_SearchLocation.getLongitudeE6()), URLEncoder.encode(searchKeyword, "Latin-1").replaceAll("\\+", "%20"), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLatitudeE6()), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLongitudeE6()));
                }
            } else if (this.mImprintListSortOption.equals("Distance")) {
                this.mDataRetrievedURL = String.format("http://%s/sys/pageserver.dll?gp=%s&pa=%s&s=-20&go=%s&p=0&k=lat:%d;lon:%d;sort:d", ShopLocalActivity.McGillSearchServerURL, ShopLocalActivity.DomainDeviceID, URLEncoder.encode(this.mSearchLocation, "Latin-1").replaceAll("\\+", "%20"), URLEncoder.encode(searchKeyword, "Latin-1").replaceAll("\\+", "%20"), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLatitudeE6()), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLongitudeE6()));
            } else if (this.mImprintListSortOption.equals("Alphabetical")) {
                this.mDataRetrievedURL = String.format("http://%s/sys/pageserver.dll?gp=%s&pa=%s&s=-20&go=%s&p=0&k=lat:%d;lon:%d;sort:a", ShopLocalActivity.McGillSearchServerURL, ShopLocalActivity.DomainDeviceID, URLEncoder.encode(this.mSearchLocation, "Latin-1").replaceAll("\\+", "%20"), URLEncoder.encode(searchKeyword, "Latin-1").replaceAll("\\+", "%20"), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLatitudeE6()), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLongitudeE6()));
            } else {
                this.mDataRetrievedURL = String.format("http://%s/sys/pageserver.dll?gp=%s&pa=%s&s=-20&go=%s&p=0&k=lat:%d;lon:%d;", ShopLocalActivity.McGillSearchServerURL, ShopLocalActivity.DomainDeviceID, URLEncoder.encode(this.mSearchLocation, "Latin-1").replaceAll("\\+", "%20"), URLEncoder.encode(searchKeyword, "Latin-1").replaceAll("\\+", "%20"), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLatitudeE6()), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLongitudeE6()));
            }
            this.mSearchKeyworkEditText.clearFocus();
            if (this.isRetrievingData) {
                this.mProgressbar.setVisibility(4);
            } else if (!checkInternetConnection()) {
                if (!this.internetConnectionAlertDialog.isShowing()) {
                    this.internetConnectionAlertDialog.show();
                }
                this.mProgressbar.setVisibility(4);
            } else {
                HideBannerCircularPost();
                this.isRetrievingData = true;
                new Thread() {
                    public void run() {
                        ShopLocalMapListActivity.this.getFirstResponseSearchBannerData(ShopLocalMapListActivity.this.mDataRetrievedURL);
                    }
                }.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.mProgressbar.setVisibility(4);
        }
    }

    /* JADX WARN: Type inference failed for: r12v0, types: [android.content.Context, com.santabarbarayp.www.ShopLocalMapListActivity, android.view.View$OnTouchListener] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void searchImprintResultForBox(com.google.android.maps.GeoPoint r13, com.google.android.maps.GeoPoint r14) {
        /*
            r12 = this;
            r11 = 4
            r5 = 0
            r12.mIsCurrentMode = r5     // Catch:{ Exception -> 0x014e }
            android.content.SharedPreferences r5 = r12.mShopLocalPrefs     // Catch:{ Exception -> 0x014e }
            android.content.SharedPreferences$Editor r1 = r5.edit()     // Catch:{ Exception -> 0x014e }
            java.lang.String r5 = "SLIsCurrentMode"
            boolean r6 = r12.mIsCurrentMode     // Catch:{ Exception -> 0x014e }
            r1.putBoolean(r5, r6)     // Catch:{ Exception -> 0x014e }
            r1.commit()     // Catch:{ Exception -> 0x014e }
            r12.resetFooterView()     // Catch:{ Exception -> 0x014e }
            android.widget.EditText r5 = r12.mSearchKeyworkEditText     // Catch:{ Exception -> 0x014e }
            r5.clearFocus()     // Catch:{ Exception -> 0x014e }
            com.google.android.maps.MapView r5 = r12.imprintSearchMapView     // Catch:{ Exception -> 0x014e }
            r5.invalidate()     // Catch:{ Exception -> 0x014e }
            r5 = 0
            r12.currentPageNumber = r5     // Catch:{ Exception -> 0x014e }
            r5 = 0
            r12.dataAnnotationNumber = r5     // Catch:{ Exception -> 0x014e }
            r5 = 1
            r12.mIsBoxSearch = r5     // Catch:{ Exception -> 0x014e }
            android.widget.ProgressBar r5 = r12.mProgressbar     // Catch:{ Exception -> 0x014e }
            r6 = 0
            r5.setVisibility(r6)     // Catch:{ Exception -> 0x014e }
            android.widget.EditText r5 = r12.mSearchKeyworkEditText     // Catch:{ Exception -> 0x014e }
            android.text.Editable r5 = r5.getText()     // Catch:{ Exception -> 0x014e }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x014e }
            java.lang.String r4 = r5.trim()     // Catch:{ Exception -> 0x014e }
            int r5 = r4.length()     // Catch:{ Exception -> 0x014e }
            if (r5 != 0) goto L_0x009c
            boolean r5 = r12.mIsFlipListViewOption     // Catch:{ Exception -> 0x014e }
            if (r5 == 0) goto L_0x009c
            java.util.ArrayList<com.santabarbarayp.www.Imprint> r5 = r12.mImprintList     // Catch:{ Exception -> 0x014e }
            if (r5 == 0) goto L_0x0051
            java.util.ArrayList<com.santabarbarayp.www.Imprint> r5 = r12.mImprintList     // Catch:{ Exception -> 0x014e }
            r5.clear()     // Catch:{ Exception -> 0x014e }
        L_0x0051:
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ Exception -> 0x014e }
            r5.<init>()     // Catch:{ Exception -> 0x014e }
            r12.mImprintList = r5     // Catch:{ Exception -> 0x014e }
            android.widget.ListView r5 = r12.imprintSearchListView     // Catch:{ Exception -> 0x014e }
            r6 = 0
            r5.setEnabled(r6)     // Catch:{ Exception -> 0x014e }
            com.santabarbarayp.www.ShopLocalMapListActivity$ImprintSearchListAdapter r3 = new com.santabarbarayp.www.ShopLocalMapListActivity$ImprintSearchListAdapter     // Catch:{ Exception -> 0x014e }
            java.util.ArrayList<com.santabarbarayp.www.Imprint> r5 = r12.mImprintList     // Catch:{ Exception -> 0x014e }
            r3.<init>(r5)     // Catch:{ Exception -> 0x014e }
            android.widget.ListView r5 = r12.imprintSearchListView     // Catch:{ Exception -> 0x014e }
            r5.setAdapter(r3)     // Catch:{ Exception -> 0x014e }
            android.widget.ListView r5 = r12.imprintSearchListView     // Catch:{ Exception -> 0x014e }
            r5.setOnTouchListener(r12)     // Catch:{ Exception -> 0x014e }
            android.widget.ListView r5 = r12.imprintSearchListView     // Catch:{ Exception -> 0x014e }
            r6 = 1
            r5.setEnabled(r6)     // Catch:{ Exception -> 0x014e }
            android.app.AlertDialog$Builder r5 = new android.app.AlertDialog$Builder     // Catch:{ Exception -> 0x014e }
            r5.<init>(r12)     // Catch:{ Exception -> 0x014e }
            android.app.AlertDialog r0 = r5.create()     // Catch:{ Exception -> 0x014e }
            java.lang.String r5 = "No Search Term Entered"
            r0.setTitle(r5)     // Catch:{ Exception -> 0x014e }
            java.lang.String r5 = "Please enter a keyword, business name or heading"
            r0.setMessage(r5)     // Catch:{ Exception -> 0x014e }
            java.lang.String r5 = "OK"
            com.santabarbarayp.www.ShopLocalMapListActivity$26 r6 = new com.santabarbarayp.www.ShopLocalMapListActivity$26     // Catch:{ Exception -> 0x014e }
            r6.<init>()     // Catch:{ Exception -> 0x014e }
            r0.setButton(r5, r6)     // Catch:{ Exception -> 0x014e }
            r0.show()     // Catch:{ Exception -> 0x014e }
            android.widget.ProgressBar r5 = r12.mProgressbar     // Catch:{ Exception -> 0x014e }
            r6 = 4
            r5.setVisibility(r6)     // Catch:{ Exception -> 0x014e }
        L_0x009b:
            return
        L_0x009c:
            android.widget.Spinner r5 = r12.mImprintListSortSpinner     // Catch:{ Exception -> 0x014e }
            java.lang.Object r5 = r5.getSelectedItem()     // Catch:{ Exception -> 0x014e }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ Exception -> 0x014e }
            r12.mImprintListSortOption = r5     // Catch:{ Exception -> 0x014e }
            java.lang.String r5 = r12.mImprintListSortOption     // Catch:{ Exception -> 0x014e }
            java.lang.String r6 = "Distance"
            boolean r5 = r5.equals(r6)     // Catch:{ Exception -> 0x014e }
            if (r5 == 0) goto L_0x0157
            java.lang.String r5 = "http://%s/sys/pageserver.dll?gp=%s&pa=latlong&y=%d&x=%d&s=-20&go=%s&p=0&k=lat:%d;lon:%d;box:%d,%d,%d,%d;sort:d"
            r6 = 11
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x014e }
            r7 = 0
            java.lang.String r8 = "www.localscout.com"
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 1
            java.lang.String r8 = "69.1"
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 2
            com.google.android.maps.GeoPoint r8 = r12.mGeoPoint_SearchLocation     // Catch:{ Exception -> 0x014e }
            int r8 = r8.getLatitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 3
            com.google.android.maps.GeoPoint r8 = r12.mGeoPoint_SearchLocation     // Catch:{ Exception -> 0x014e }
            int r8 = r8.getLongitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 4
            java.lang.String r8 = "Latin-1"
            java.lang.String r8 = java.net.URLEncoder.encode(r4, r8)     // Catch:{ Exception -> 0x014e }
            java.lang.String r9 = "\\+"
            java.lang.String r10 = "%20"
            java.lang.String r8 = r8.replaceAll(r9, r10)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 5
            com.google.android.maps.GeoPoint r8 = r12.mGeoPoint_SearchHomeLocation     // Catch:{ Exception -> 0x014e }
            int r8 = r8.getLatitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 6
            com.google.android.maps.GeoPoint r8 = r12.mGeoPoint_SearchHomeLocation     // Catch:{ Exception -> 0x014e }
            int r8 = r8.getLongitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 7
            int r8 = r13.getLatitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 8
            int r8 = r13.getLongitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 9
            int r8 = r14.getLatitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 10
            int r8 = r14.getLongitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            java.lang.String r5 = java.lang.String.format(r5, r6)     // Catch:{ Exception -> 0x014e }
            r12.mDataRetrievedURL = r5     // Catch:{ Exception -> 0x014e }
        L_0x013a:
            boolean r5 = r12.isRetrievingData     // Catch:{ Exception -> 0x014e }
            if (r5 != 0) goto L_0x0279
            r12.HideBannerCircularPost()     // Catch:{ Exception -> 0x014e }
            android.widget.ListView r5 = r12.imprintSearchListView     // Catch:{ Exception -> 0x014e }
            r6 = 0
            r5.setEnabled(r6)     // Catch:{ Exception -> 0x014e }
            java.lang.String r5 = r12.mDataRetrievedURL     // Catch:{ Exception -> 0x014e }
            r12.updateResponseSearchResultDataAsyn(r5)     // Catch:{ Exception -> 0x014e }
            goto L_0x009b
        L_0x014e:
            r5 = move-exception
            r2 = r5
            android.widget.ProgressBar r5 = r12.mProgressbar
            r5.setVisibility(r11)
            goto L_0x009b
        L_0x0157:
            java.lang.String r5 = r12.mImprintListSortOption     // Catch:{ Exception -> 0x014e }
            java.lang.String r6 = "Alphabetical"
            boolean r5 = r5.equals(r6)     // Catch:{ Exception -> 0x014e }
            if (r5 == 0) goto L_0x01ed
            java.lang.String r5 = "http://%s/sys/pageserver.dll?gp=%s&pa=latlong&y=%d&x=%d&s=-20&go=%s&p=0&k=lat:%d;lon:%d;box:%d,%d,%d,%d;sort:a"
            r6 = 11
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x014e }
            r7 = 0
            java.lang.String r8 = "www.localscout.com"
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 1
            java.lang.String r8 = "69.1"
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 2
            com.google.android.maps.GeoPoint r8 = r12.mGeoPoint_SearchLocation     // Catch:{ Exception -> 0x014e }
            int r8 = r8.getLatitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 3
            com.google.android.maps.GeoPoint r8 = r12.mGeoPoint_SearchLocation     // Catch:{ Exception -> 0x014e }
            int r8 = r8.getLongitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 4
            java.lang.String r8 = "Latin-1"
            java.lang.String r8 = java.net.URLEncoder.encode(r4, r8)     // Catch:{ Exception -> 0x014e }
            java.lang.String r9 = "\\+"
            java.lang.String r10 = "%20"
            java.lang.String r8 = r8.replaceAll(r9, r10)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 5
            com.google.android.maps.GeoPoint r8 = r12.mGeoPoint_SearchHomeLocation     // Catch:{ Exception -> 0x014e }
            int r8 = r8.getLatitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 6
            com.google.android.maps.GeoPoint r8 = r12.mGeoPoint_SearchHomeLocation     // Catch:{ Exception -> 0x014e }
            int r8 = r8.getLongitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 7
            int r8 = r13.getLatitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 8
            int r8 = r13.getLongitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 9
            int r8 = r14.getLatitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 10
            int r8 = r14.getLongitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            java.lang.String r5 = java.lang.String.format(r5, r6)     // Catch:{ Exception -> 0x014e }
            r12.mDataRetrievedURL = r5     // Catch:{ Exception -> 0x014e }
            goto L_0x013a
        L_0x01ed:
            java.lang.String r5 = "http://%s/sys/pageserver.dll?gp=%s&pa=latlong&y=%d&x=%d&s=-20&go=%s&p=0&k=lat:%d;lon:%d;box:%d,%d,%d,%d;"
            r6 = 11
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x014e }
            r7 = 0
            java.lang.String r8 = "www.localscout.com"
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 1
            java.lang.String r8 = "69.1"
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 2
            com.google.android.maps.GeoPoint r8 = r12.mGeoPoint_SearchLocation     // Catch:{ Exception -> 0x014e }
            int r8 = r8.getLatitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 3
            com.google.android.maps.GeoPoint r8 = r12.mGeoPoint_SearchLocation     // Catch:{ Exception -> 0x014e }
            int r8 = r8.getLongitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 4
            java.lang.String r8 = "Latin-1"
            java.lang.String r8 = java.net.URLEncoder.encode(r4, r8)     // Catch:{ Exception -> 0x014e }
            java.lang.String r9 = "\\+"
            java.lang.String r10 = "%20"
            java.lang.String r8 = r8.replaceAll(r9, r10)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 5
            com.google.android.maps.GeoPoint r8 = r12.mGeoPoint_SearchHomeLocation     // Catch:{ Exception -> 0x014e }
            int r8 = r8.getLatitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 6
            com.google.android.maps.GeoPoint r8 = r12.mGeoPoint_SearchHomeLocation     // Catch:{ Exception -> 0x014e }
            int r8 = r8.getLongitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 7
            int r8 = r13.getLatitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 8
            int r8 = r13.getLongitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 9
            int r8 = r14.getLatitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            r7 = 10
            int r8 = r14.getLongitudeE6()     // Catch:{ Exception -> 0x014e }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x014e }
            r6[r7] = r8     // Catch:{ Exception -> 0x014e }
            java.lang.String r5 = java.lang.String.format(r5, r6)     // Catch:{ Exception -> 0x014e }
            r12.mDataRetrievedURL = r5     // Catch:{ Exception -> 0x014e }
            goto L_0x013a
        L_0x0279:
            android.widget.ProgressBar r5 = r12.mProgressbar     // Catch:{ Exception -> 0x014e }
            r6 = 4
            r5.setVisibility(r6)     // Catch:{ Exception -> 0x014e }
            goto L_0x009b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.santabarbarayp.www.ShopLocalMapListActivity.searchImprintResultForBox(com.google.android.maps.GeoPoint, com.google.android.maps.GeoPoint):void");
    }

    private double calculateDistance(double sourceLatitude, double sourceLongitude, double destLatitude, double destLongitude) {
        double dLat = destLatitude - sourceLatitude;
        double dLon = (destLongitude - sourceLongitude) * ((double) FloatMath.cos((float) ((3.141592653589793d * destLatitude) / 180.0d)));
        return ((double) FloatMath.sqrt((float) ((dLat * dLat) + (dLon * dLon)))) * 69.172d;
    }

    private String convertStreamToString(InputStream is) {
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "ISO-8859-1"));
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        break;
                    } catch (IOException e) {
                    }
                } else {
                    sb.append(String.valueOf(line) + "\n");
                }
            }
            is.close();
        } catch (IOException e2) {
            try {
                is.close();
            } catch (IOException e3) {
            }
        } catch (Throwable th) {
            try {
                is.close();
            } catch (IOException e4) {
            }
            throw th;
        }
        return sb.toString();
    }

    /* JADX WARN: Type inference failed for: r10v0, types: [android.content.Context, android.location.LocationListener, com.santabarbarayp.www.ShopLocalMapListActivity] */
    /* access modifiers changed from: private */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void goMyLocation() {
        /*
            r10 = this;
            r4 = 1
            r3 = 4
            r2 = 0
            android.widget.ProgressBar r0 = r10.mProgressbar
            r0.setVisibility(r2)
            boolean r0 = r10.checkInternetConnection()
            if (r0 != 0) goto L_0x0021
            android.app.AlertDialog r0 = r10.internetConnectionAlertDialog
            boolean r0 = r0.isShowing()
            if (r0 != 0) goto L_0x001b
            android.app.AlertDialog r0 = r10.internetConnectionAlertDialog
            r0.show()
        L_0x001b:
            android.widget.ProgressBar r0 = r10.mProgressbar
            r0.setVisibility(r3)
        L_0x0020:
            return
        L_0x0021:
            java.lang.String r0 = r10.mLocationProviderString
            if (r0 != 0) goto L_0x004b
            android.app.AlertDialog$Builder r0 = new android.app.AlertDialog$Builder
            r0.<init>(r10)
            android.app.AlertDialog r6 = r0.create()
            java.lang.String r0 = "Unable To Retrieve\nCurrent Location"
            r6.setTitle(r0)
            java.lang.String r0 = "To ensure the best user experience, be sure that GPS or wireless networks are enabled in location settings."
            r6.setMessage(r0)
            java.lang.String r0 = "OK"
            com.santabarbarayp.www.ShopLocalMapListActivity$27 r1 = new com.santabarbarayp.www.ShopLocalMapListActivity$27
            r1.<init>()
            r6.setButton(r0, r1)
            r6.show()
            android.widget.ProgressBar r0 = r10.mProgressbar
            r0.setVisibility(r3)
            goto L_0x0020
        L_0x004b:
            r10.HideBannerCircularPost()
            android.widget.EditText r0 = r10.mSearchKeyworkEditText
            android.text.Editable r0 = r0.getText()
            java.lang.String r0 = r0.toString()
            java.lang.String r9 = r0.trim()
            android.widget.ProgressBar r0 = r10.mProgressbar
            r0.setVisibility(r2)
            r0 = 15
            r10.mZoomLevel = r0
            boolean r0 = r10.isRetrievingData
            if (r0 != 0) goto L_0x00ed
            r10.currentPageNumber = r2
            r10.mIsBoxSearch = r2
            r10.mIsFromHomeButton = r4
            android.widget.ListView r0 = r10.imprintSearchListView
            r0.setEnabled(r2)
            android.location.LocationManager r0 = r10.mLocationManager
            java.lang.String r1 = r10.mLocationProviderString
            android.location.Location r8 = r0.getLastKnownLocation(r1)
            if (r8 != 0) goto L_0x00df
            android.app.AlertDialog$Builder r0 = new android.app.AlertDialog$Builder
            r0.<init>(r10)
            android.app.AlertDialog r6 = r0.create()
            java.lang.String r0 = "Unable To Retrieve\nCurrent Location"
            r6.setTitle(r0)
            java.lang.String r0 = "Using the last known location."
            r6.setMessage(r0)
            java.lang.String r0 = "OK"
            com.santabarbarayp.www.ShopLocalMapListActivity$28 r1 = new com.santabarbarayp.www.ShopLocalMapListActivity$28
            r1.<init>()
            r6.setButton(r0, r1)
            r6.show()
        L_0x009e:
            com.santabarbarayp.www.ShopLocalMapListActivity$29 r0 = new com.santabarbarayp.www.ShopLocalMapListActivity$29
            r0.<init>(r8, r9)
            r0.start()
        L_0x00a6:
            boolean r0 = r10.mIsCurrentMode
            if (r0 != 0) goto L_0x0100
            r10.mIsCurrentMode = r4
            android.content.SharedPreferences r0 = r10.mShopLocalPrefs
            android.content.SharedPreferences$Editor r7 = r0.edit()
            java.lang.String r0 = "SLIsCurrentMode"
            boolean r1 = r10.mIsCurrentMode
            r7.putBoolean(r0, r1)
            r7.commit()
            android.location.LocationManager r0 = r10.mLocationManager     // Catch:{ Exception -> 0x00dc }
            r0.removeUpdates(r10)     // Catch:{ Exception -> 0x00dc }
            android.location.LocationManager r0 = r10.mLocationManager     // Catch:{ Exception -> 0x00dc }
            java.lang.String r0 = r10.getLocationProvider(r0)     // Catch:{ Exception -> 0x00dc }
            r10.mLocationProviderString = r0     // Catch:{ Exception -> 0x00dc }
            boolean r0 = com.santabarbarayp.www.ShopLocalActivity.mIsTrackingCurrentMode     // Catch:{ Exception -> 0x00dc }
            if (r0 == 0) goto L_0x00f3
            android.location.LocationManager r0 = r10.mLocationManager     // Catch:{ Exception -> 0x00dc }
            java.lang.String r1 = r10.mLocationProviderString     // Catch:{ Exception -> 0x00dc }
            r2 = 90000(0x15f90, double:4.4466E-319)
            r4 = 1101004800(0x41a00000, float:20.0)
            r5 = r10
            r0.requestLocationUpdates(r1, r2, r4, r5)     // Catch:{ Exception -> 0x00dc }
            goto L_0x0020
        L_0x00dc:
            r0 = move-exception
            goto L_0x0020
        L_0x00df:
            android.content.Context r0 = r10.getBaseContext()
            java.lang.String r1 = "Current Location Found"
            android.widget.Toast r0 = android.widget.Toast.makeText(r0, r1, r2)
            r0.show()
            goto L_0x009e
        L_0x00ed:
            android.widget.ProgressBar r0 = r10.mProgressbar
            r0.setVisibility(r3)
            goto L_0x00a6
        L_0x00f3:
            android.location.LocationManager r0 = r10.mLocationManager     // Catch:{ Exception -> 0x00dc }
            java.lang.String r1 = r10.mLocationProviderString     // Catch:{ Exception -> 0x00dc }
            r2 = 0
            r4 = 0
            r5 = r10
            r0.requestLocationUpdates(r1, r2, r4, r5)     // Catch:{ Exception -> 0x00dc }
            goto L_0x0020
        L_0x0100:
            boolean r0 = com.santabarbarayp.www.ShopLocalActivity.mIsTrackingCurrentMode
            if (r0 != 0) goto L_0x0020
            android.location.LocationManager r0 = r10.mLocationManager     // Catch:{ Exception -> 0x011e }
            r0.removeUpdates(r10)     // Catch:{ Exception -> 0x011e }
            android.location.LocationManager r0 = r10.mLocationManager     // Catch:{ Exception -> 0x011e }
            java.lang.String r0 = r10.getLocationProvider(r0)     // Catch:{ Exception -> 0x011e }
            r10.mLocationProviderString = r0     // Catch:{ Exception -> 0x011e }
            android.location.LocationManager r0 = r10.mLocationManager     // Catch:{ Exception -> 0x011e }
            java.lang.String r1 = r10.mLocationProviderString     // Catch:{ Exception -> 0x011e }
            r2 = 0
            r4 = 0
            r5 = r10
            r0.requestLocationUpdates(r1, r2, r4, r5)     // Catch:{ Exception -> 0x011e }
            goto L_0x0020
        L_0x011e:
            r0 = move-exception
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.santabarbarayp.www.ShopLocalMapListActivity.goMyLocation():void");
    }

    /* access modifiers changed from: private */
    public void changeSearchLocation() {
        this.mIsCurrentMode = false;
        SharedPreferences.Editor editor = this.mShopLocalPrefs.edit();
        editor.putBoolean("SLIsCurrentMode", this.mIsCurrentMode);
        editor.commit();
        if (this.isChange) {
            this.isChange = false;
            HideBannerCircularPost();
            this.mSearchLocationTextView.setVisibility(4);
            this.mHomeButton.setVisibility(4);
            this.mLocationListView.setVisibility(0);
            this.mLocationListView.setEnabled(false);
            this.imprintSearchViewFlipper.setVisibility(4);
            this.mKeywordListView.setVisibility(4);
            if (this.mIsPortrait) {
                RelativeLayout.LayoutParams lp1 = (RelativeLayout.LayoutParams) this.mapFooterView.getLayoutParams();
                lp1.addRule(10, -1);
                lp1.addRule(12, 0);
                this.mapFooterView.setLayoutParams(lp1);
            }
            this.mMapChangeButton.setImageResource(R.drawable.map_cancel);
            this.mSearchLocationEditText.setVisibility(0);
            this.mSearchLocationEditText.requestFocus();
            this.mSearchLocationEditText.selectAll();
            this.mShopLocalDataHandler.post(this.mLocationThreadRetrieveUpdateTask);
            this.mSearchLocationEditText.requestFocus();
            ((InputMethodManager) getSystemService("input_method")).showSoftInput(this.mSearchLocationEditText, 2);
            return;
        }
        ShowBannerCircularPost();
        resetFooterView();
    }

    /* JADX WARN: Type inference failed for: r7v0, types: [android.content.Context, com.santabarbarayp.www.ShopLocalMapListActivity] */
    /* access modifiers changed from: private */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void mapViewFlipClick(android.view.View r8) {
        /*
            r7 = this;
            r6 = 0
            android.widget.EditText r4 = r7.mSearchKeyworkEditText     // Catch:{ Exception -> 0x00a9 }
            r4.clearFocus()     // Catch:{ Exception -> 0x00a9 }
            com.santabarbarayp.www.ShopLocal_ViewFlipper r4 = r7.imprintSearchViewFlipper     // Catch:{ Exception -> 0x00a9 }
            r5 = 0
            r4.setVisibility(r5)     // Catch:{ Exception -> 0x00a9 }
            android.widget.ListView r4 = r7.mKeywordListView     // Catch:{ Exception -> 0x00a9 }
            r5 = 4
            r4.setVisibility(r5)     // Catch:{ Exception -> 0x00a9 }
            android.widget.RelativeLayout r4 = r7.mapFooterView     // Catch:{ Exception -> 0x00a9 }
            r5 = 0
            r4.setVisibility(r5)     // Catch:{ Exception -> 0x00a9 }
            com.google.android.maps.MapView r4 = r7.imprintSearchMapView     // Catch:{ Exception -> 0x00a9 }
            r4.invalidate()     // Catch:{ Exception -> 0x00a9 }
            r7.hideKeyboard()     // Catch:{ Exception -> 0x00a9 }
            com.santabarbarayp.www.ShopLocal_ViewFlipper r4 = r7.imprintSearchViewFlipper     // Catch:{ Exception -> 0x00a9 }
            r4.showNext()     // Catch:{ Exception -> 0x00a9 }
            boolean r4 = r7.mIsFlipListViewOption     // Catch:{ Exception -> 0x00a9 }
            if (r4 != 0) goto L_0x008d
            android.widget.ImageButton r4 = r7.mMapFlipButton     // Catch:{ Exception -> 0x00a9 }
            r5 = 2130837559(0x7f020037, float:1.7280075E38)
            r4.setImageResource(r5)     // Catch:{ Exception -> 0x00a9 }
            int r4 = r7.mSelectedImprintIndex     // Catch:{ Exception -> 0x00a9 }
            r5 = -1
            if (r4 <= r5) goto L_0x003d
            android.widget.ListView r4 = r7.imprintSearchListView     // Catch:{ Exception -> 0x00a9 }
            int r5 = r7.mSelectedImprintIndex     // Catch:{ Exception -> 0x00a9 }
            r4.setSelection(r5)     // Catch:{ Exception -> 0x00a9 }
        L_0x003d:
            boolean r4 = r7.mIsFlipListViewOption     // Catch:{ Exception -> 0x00a9 }
            if (r4 == 0) goto L_0x00ab
            r4 = r6
        L_0x0042:
            r7.mIsFlipListViewOption = r4     // Catch:{ Exception -> 0x00a9 }
            boolean r4 = r7.mIsFlipListViewOption     // Catch:{ Exception -> 0x00a9 }
            if (r4 == 0) goto L_0x008c
            java.util.ArrayList<com.santabarbarayp.www.Imprint> r4 = r7.mImprintList     // Catch:{ Exception -> 0x00a9 }
            if (r4 == 0) goto L_0x0054
            java.util.ArrayList<com.santabarbarayp.www.Imprint> r4 = r7.mImprintList     // Catch:{ Exception -> 0x00a9 }
            int r4 = r4.size()     // Catch:{ Exception -> 0x00a9 }
            if (r4 != 0) goto L_0x008c
        L_0x0054:
            java.lang.String r1 = "No Search Results"
            android.widget.EditText r4 = r7.mSearchKeyworkEditText     // Catch:{ Exception -> 0x00a9 }
            android.text.Editable r4 = r4.getText()     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r3 = r4.trim()     // Catch:{ Exception -> 0x00a9 }
            if (r3 == 0) goto L_0x006c
            int r4 = r3.length()     // Catch:{ Exception -> 0x00a9 }
            if (r4 != 0) goto L_0x006e
        L_0x006c:
            java.lang.String r1 = "No Search Term Entered"
        L_0x006e:
            android.app.AlertDialog$Builder r4 = new android.app.AlertDialog$Builder     // Catch:{ Exception -> 0x00a9 }
            r4.<init>(r7)     // Catch:{ Exception -> 0x00a9 }
            android.app.AlertDialog r0 = r4.create()     // Catch:{ Exception -> 0x00a9 }
            r0.setTitle(r1)     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r4 = "Please enter a keyword, business name or heading"
            r0.setMessage(r4)     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r4 = "OK"
            com.santabarbarayp.www.ShopLocalMapListActivity$30 r5 = new com.santabarbarayp.www.ShopLocalMapListActivity$30     // Catch:{ Exception -> 0x00a9 }
            r5.<init>()     // Catch:{ Exception -> 0x00a9 }
            r0.setButton(r4, r5)     // Catch:{ Exception -> 0x00a9 }
            r0.show()     // Catch:{ Exception -> 0x00a9 }
        L_0x008c:
            return
        L_0x008d:
            android.widget.ImageButton r4 = r7.mMapFlipButton     // Catch:{ Exception -> 0x00a9 }
            r5 = 2130837558(0x7f020036, float:1.7280073E38)
            r4.setImageResource(r5)     // Catch:{ Exception -> 0x00a9 }
            r2 = 1
            java.lang.String r4 = r7.mImprintListSortOption     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r5 = "Distance"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x00a9 }
            if (r4 == 0) goto L_0x00a1
            r2 = 0
        L_0x00a1:
            java.util.ArrayList<com.santabarbarayp.www.Imprint> r4 = r7.mImprintList     // Catch:{ Exception -> 0x00a9 }
            int r5 = r7.mSelectedImprintIndex     // Catch:{ Exception -> 0x00a9 }
            r7.setupMapAnnotation(r4, r5, r2)     // Catch:{ Exception -> 0x00a9 }
            goto L_0x003d
        L_0x00a9:
            r4 = move-exception
            goto L_0x008c
        L_0x00ab:
            r4 = 1
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.santabarbarayp.www.ShopLocalMapListActivity.mapViewFlipClick(android.view.View):void");
    }

    /* access modifiers changed from: private */
    public void resetFooterView() {
        this.isChange = true;
        this.mapFooterView.setVisibility(0);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        RelativeLayout.LayoutParams isvf = (RelativeLayout.LayoutParams) this.imprintSearchViewFlipper.getLayoutParams();
        RelativeLayout.LayoutParams mshv = (RelativeLayout.LayoutParams) this.mapHeaderView.getLayoutParams();
        RelativeLayout.LayoutParams lpFooterview = (RelativeLayout.LayoutParams) this.mapFooterView.getLayoutParams();
        RelativeLayout.LayoutParams sket = (RelativeLayout.LayoutParams) this.mSearchKeyworkEditText.getLayoutParams();
        RelativeLayout.LayoutParams slet = (RelativeLayout.LayoutParams) this.mSearchLocationEditText.getLayoutParams();
        RelativeLayout.LayoutParams sltv = (RelativeLayout.LayoutParams) this.mSearchLocationTextView.getLayoutParams();
        if (this.mIsPortrait) {
            sket.width = (int) (186.0f * dm.density);
            slet.width = (int) (247.0f * dm.density);
            sltv.width = (int) (202.0f * dm.density);
            mshv.width = -1;
            lpFooterview.addRule(12, -1);
            lpFooterview.addRule(11, 0);
            lpFooterview.addRule(1, 0);
            lpFooterview.addRule(10, 0);
            isvf.addRule(2, R.id.footer_view);
        } else {
            int iScreenWidth = ((WindowManager) getSystemService("window")).getDefaultDisplay().getWidth();
            int widthOfView = ((int) (((double) (((((float) iScreenWidth) - (96.0f * dm.density)) - (44.0f * dm.density)) - (40.0f * dm.density))) - (4.7d * ((double) dm.density)))) / 2;
            if (iScreenWidth >= 800) {
                widthOfView -= (int) (30.0f * dm.density);
            }
            sket.width = widthOfView;
            slet.width = ((int) (46.7d * ((double) dm.density))) + widthOfView;
            sltv.width = widthOfView;
            mshv.width = -2;
            lpFooterview.addRule(10, -1);
            lpFooterview.addRule(1, R.id.header_view);
            lpFooterview.addRule(12, 0);
            lpFooterview.addRule(11, -1);
            isvf.addRule(2, 0);
        }
        this.mapHeaderView.setLayoutParams(mshv);
        this.mSearchKeyworkEditText.setLayoutParams(sket);
        this.mSearchLocationEditText.setLayoutParams(slet);
        this.mSearchLocationTextView.setLayoutParams(sltv);
        this.mapFooterView.setLayoutParams(lpFooterview);
        this.imprintSearchViewFlipper.setLayoutParams(isvf);
        this.mMapChangeButton.setImageResource(R.drawable.map_change);
        this.mSearchLocationEditText.clearFocus();
        this.mSearchKeyworkEditText.clearFocus();
        hideKeyboard();
        this.mKeywordListView.setVisibility(4);
        this.mSearchLocationEditText.setVisibility(4);
        this.mLocationListView.setVisibility(4);
        this.mSearchLocationTextView.setVisibility(0);
        this.mHomeButton.setVisibility(0);
        this.imprintSearchViewFlipper.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void hideKeyboard() {
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.mSearchKeyworkEditText.getWindowToken(), 0);
    }

    /* access modifiers changed from: private */
    public void resetMap(double centerLatitude, double centerLongitude, String title) {
        boolean isSearchHomeChanged = false;
        if (calculateDistance(((double) this.mGeoPoint_SearchHomeLocation.getLatitudeE6()) / 1000000.0d, ((double) this.mGeoPoint_SearchHomeLocation.getLongitudeE6()) / 1000000.0d, centerLatitude, centerLongitude) > 100.0d) {
            isSearchHomeChanged = true;
        }
        if (isSearchHomeChanged) {
            this.mSearchHomeLocation = title;
            this.mGeoPoint_SearchHomeLocation = new GeoPoint((int) (1000000.0d * centerLatitude), (int) (1000000.0d * centerLongitude));
        }
        this.mZoomLevel = 15;
        this.mGeoPoint_SearchLocation = new GeoPoint((int) (1000000.0d * centerLatitude), (int) (1000000.0d * centerLongitude));
        this.imprintSearchMapView.setSatellite(false);
        this.imprintSearchMapView.setStreetView(false);
        this.mSearchLocation = title;
        this.mSearchLocationEditText.setText(this.mSearchLocation);
        this.mSearchLocationTextView.setText(this.mSearchLocation);
        SharedPreferences.Editor editor = this.mShopLocalPrefs.edit();
        editor.putString("SLSearchHomeLocation", this.mSearchHomeLocation);
        editor.putInt("SLSearchHomeLocationLatitudeE6", this.mGeoPoint_SearchHomeLocation.getLatitudeE6());
        editor.putInt("SLSearchHomeLocationLongitudeE6", this.mGeoPoint_SearchHomeLocation.getLongitudeE6());
        editor.putString("SLSearchLocation", this.mSearchLocation);
        editor.putInt("SLSearchLocationLatitudeE6", this.mGeoPoint_SearchLocation.getLatitudeE6());
        editor.putInt("SLSearchLocationLongitudeE6", this.mGeoPoint_SearchLocation.getLongitudeE6());
        editor.commit();
        resetFooterView();
        String searchTerm = this.mSearchKeyworkEditText.getText().toString().trim();
        if (searchTerm.length() == 0) {
            this.mc.setZoom(this.mZoomLevel);
            this.mc.setCenter(this.mGeoPoint_SearchLocation);
            List<Overlay> overlays = this.imprintSearchMapView.getOverlays();
            overlays.clear();
            overlays.add(new ShopLocalOverlay());
            this.mProgressbar.setVisibility(4);
            return;
        }
        this.mProgressbar.setVisibility(0);
        this.currentPageNumber = 0;
        this.mIsBoxSearch = false;
        try {
            this.mImprintListSortOption = (String) this.mImprintListSortSpinner.getSelectedItem();
            if (this.mIsCurrentMode) {
                if (this.mImprintListSortOption.equals("Distance")) {
                    this.mDataRetrievedURL = String.format("http://%s/sys/pageserver.dll?gp=%s&pa=latlong&y=%d&x=%d&s=-20&go=%s&p=0&k=lat:%d;lon:%d;sort:d", ShopLocalActivity.McGillSearchServerURL, ShopLocalActivity.DomainDeviceID, Integer.valueOf(this.mGeoPoint_SearchLocation.getLatitudeE6()), Integer.valueOf(this.mGeoPoint_SearchLocation.getLongitudeE6()), URLEncoder.encode(searchTerm, "Latin-1").replaceAll("\\+", "%20"), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLatitudeE6()), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLongitudeE6()));
                } else if (this.mImprintListSortOption.equals("Alphabetical")) {
                    this.mDataRetrievedURL = String.format("http://%s/sys/pageserver.dll?gp=%s&pa=latlong&y=%d&x=%d&s=-20&go=%s&p=0&k=lat:%d;lon:%d;sort:a", ShopLocalActivity.McGillSearchServerURL, ShopLocalActivity.DomainDeviceID, Integer.valueOf(this.mGeoPoint_SearchLocation.getLatitudeE6()), Integer.valueOf(this.mGeoPoint_SearchLocation.getLongitudeE6()), URLEncoder.encode(searchTerm, "Latin-1").replaceAll("\\+", "%20"), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLatitudeE6()), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLongitudeE6()));
                } else {
                    this.mDataRetrievedURL = String.format("http://%s/sys/pageserver.dll?gp=%s&pa=latlong&y=%d&x=%d&s=-20&go=%s&p=0&k=lat:%d;lon:%d;", ShopLocalActivity.McGillSearchServerURL, ShopLocalActivity.DomainDeviceID, Integer.valueOf(this.mGeoPoint_SearchLocation.getLatitudeE6()), Integer.valueOf(this.mGeoPoint_SearchLocation.getLongitudeE6()), URLEncoder.encode(searchTerm, "Latin-1").replaceAll("\\+", "%20"), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLatitudeE6()), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLongitudeE6()));
                }
            } else if (this.mImprintListSortOption.equals("Distance")) {
                this.mDataRetrievedURL = String.format("http://%s/sys/pageserver.dll?gp=%s&pa=%s&s=-20&go=%s&p=0&k=lat:%d;lon:%d;sort:d", ShopLocalActivity.McGillSearchServerURL, ShopLocalActivity.DomainDeviceID, URLEncoder.encode(this.mSearchLocation, "Latin-1").replaceAll("\\+", "%20"), URLEncoder.encode(searchTerm, "Latin-1").replaceAll("\\+", "%20"), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLatitudeE6()), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLongitudeE6()));
            } else if (this.mImprintListSortOption.equals("Alphabetical")) {
                this.mDataRetrievedURL = String.format("http://%s/sys/pageserver.dll?gp=%s&pa=%s&s=-20&go=%s&p=0&k=lat:%d;lon:%d;sort:a", ShopLocalActivity.McGillSearchServerURL, ShopLocalActivity.DomainDeviceID, URLEncoder.encode(this.mSearchLocation, "Latin-1").replaceAll("\\+", "%20"), URLEncoder.encode(searchTerm, "Latin-1").replaceAll("\\+", "%20"), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLatitudeE6()), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLongitudeE6()));
            } else {
                this.mDataRetrievedURL = String.format("http://%s/sys/pageserver.dll?gp=%s&pa=%s&s=-20&go=%s&p=0&k=lat:%d;lon:%d;", ShopLocalActivity.McGillSearchServerURL, ShopLocalActivity.DomainDeviceID, URLEncoder.encode(this.mSearchLocation, "Latin-1").replaceAll("\\+", "%20"), URLEncoder.encode(searchTerm, "Latin-1").replaceAll("\\+", "%20"), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLatitudeE6()), Integer.valueOf(this.mGeoPoint_SearchHomeLocation.getLongitudeE6()));
            }
            if (!this.isRetrievingData) {
                HideBannerCircularPost();
                this.imprintSearchListView.setVisibility(8);
                this.imprintSearchListView.setEnabled(false);
                updateResponseSearchResultDataAsyn(this.mDataRetrievedURL);
                return;
            }
            this.mProgressbar.setVisibility(4);
        } catch (UnsupportedEncodingException e) {
            this.mProgressbar.setVisibility(4);
        }
    }

    /* access modifiers changed from: private */
    public void setupMapAnnotation(ArrayList<Imprint> imprintList, int startIndex, boolean isBoundaryFixed) {
        int latSpanE6 = 80000;
        int lonSpanE6 = 80000;
        int initialValue = startIndex - 5;
        if (initialValue < 0) {
            initialValue = 0;
        }
        int localAnnotationnumber = 0;
        int finalNumber = initialValue + 20;
        int deltaDiff = 0;
        if (finalNumber > imprintList.size()) {
            deltaDiff = finalNumber - imprintList.size();
        }
        int initialValue2 = initialValue - deltaDiff;
        if (initialValue2 < 0) {
            initialValue2 = 0;
        }
        double deltaFarthestLatitude = 5000.0d;
        double deltaFarthestLongitude = 5000.0d;
        int includedEndIndex = -1;
        int i = initialValue2;
        while (localAnnotationnumber < 20 && i < imprintList.size()) {
            Imprint localimprint = imprintList.get(i);
            if (localimprint != null && localimprint.getGeoLocationIndex() > -1) {
                localAnnotationnumber++;
                includedEndIndex = i;
                double deltaLatitude = (localimprint.getLat() * 1000000.0d) - ((double) this.mGeoPoint_SearchLocation.getLatitudeE6());
                double deltaLongitude = (localimprint.getLng() * 1000000.0d) - ((double) this.mGeoPoint_SearchLocation.getLongitudeE6());
                if (deltaLatitude < 0.0d) {
                    deltaLatitude = -deltaLatitude;
                }
                if (deltaLongitude < 0.0d) {
                    deltaLongitude = -deltaLongitude;
                }
                if (deltaLatitude > deltaFarthestLatitude) {
                    deltaFarthestLatitude = deltaLatitude;
                }
                if (deltaLongitude > deltaFarthestLongitude) {
                    deltaFarthestLongitude = deltaLongitude;
                }
            }
            i++;
        }
        if (isBoundaryFixed) {
            try {
                latSpanE6 = this.mTopRightGeoPoint.getLatitudeE6() - this.mBottomLeftGeoPoint.getLatitudeE6();
                lonSpanE6 = this.mTopRightGeoPoint.getLongitudeE6() - this.mBottomLeftGeoPoint.getLongitudeE6();
            } catch (Exception e) {
            }
        } else {
            latSpanE6 = (int) (1.2d * deltaFarthestLatitude);
            lonSpanE6 = (int) (1.2d * deltaFarthestLongitude);
            this.mBottomLeftGeoPoint = new GeoPoint(this.mGeoPoint_SearchLocation.getLatitudeE6() - (latSpanE6 / 2), this.mGeoPoint_SearchLocation.getLongitudeE6() - (lonSpanE6 / 2));
            this.mTopRightGeoPoint = new GeoPoint(this.mGeoPoint_SearchLocation.getLatitudeE6() + (latSpanE6 / 2), this.mGeoPoint_SearchLocation.getLongitudeE6() + (lonSpanE6 / 2));
        }
        if (!this.mIsBoxSearch) {
            this.mc.setCenter(this.mGeoPoint_SearchLocation);
            this.mc.zoomToSpan(latSpanE6, lonSpanE6);
            this.mZoomLevel = this.imprintSearchMapView.getZoomLevel();
        }
        this.imprintSearchMapView.setSatellite(false);
        this.imprintSearchMapView.setStreetView(false);
        this.mSearchLocationTextView.setText(this.mSearchLocation);
        List<Overlay> overlays = this.imprintSearchMapView.getOverlays();
        overlays.clear();
        overlays.add(new ShopLocalOverlay(initialValue2, includedEndIndex));
        resetFooterView();
    }

    /* access modifiers changed from: private */
    public String findCityAddress(double latitude, double longitude) {
        List<Address> myList;
        List<Address> myList2;
        String stateAbbre;
        if (!checkInternetConnection()) {
            return null;
        }
        String cityAddress = ReverseGeocode.retrieveLocation(latitude, longitude);
        if (cityAddress != null) {
            return cityAddress;
        }
        new ArrayList();
        try {
            Geocoder myLocationGeoCoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            try {
                myList = myLocationGeoCoder.getFromLocation(latitude, longitude, 1);
            } catch (IOException e) {
                myList = ReverseGeocode.getFromLocation(latitude, longitude, 1);
            }
            for (int numbers = 0; numbers < 4; numbers++) {
                if (!myList2.isEmpty()) {
                    break;
                }
                try {
                    myList2 = myLocationGeoCoder.getFromLocation(latitude, longitude, 1);
                } catch (IOException e2) {
                    myList2 = ReverseGeocode.getFromLocation(latitude, longitude, 1);
                }
            }
            if (myList2.isEmpty()) {
                try {
                    JSONArray jsonCityArray = new JSONObject(convertStreamToString(new URL(String.format("http://%s/sys/pageserver.dll?gp=%s&pa=latlong&y=%d&x=%d&s=-20", ShopLocalActivity.McGillSearchServerURL, ShopLocalActivity.DomainDeviceID, Integer.valueOf((int) (1000000.0d * latitude)), Integer.valueOf((int) (1000000.0d * longitude)))).openConnection().getInputStream())).getJSONArray("city");
                    if (jsonCityArray.length() > 0) {
                        cityAddress = jsonCityArray.getString(0);
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
                return cityAddress;
            }
            Address firstAddrestt = myList2.get(0);
            String countryCode = firstAddrestt.getCountryCode();
            if (countryCode == null) {
                return null;
            }
            if (!countryCode.equalsIgnoreCase("US") && !countryCode.equalsIgnoreCase("CA")) {
                return null;
            }
            String streetAddress = null;
            if (firstAddrestt.getMaxAddressLineIndex() > 0) {
                streetAddress = firstAddrestt.getAddressLine(0);
            }
            cityAddress = firstAddrestt.getLocality();
            if (cityAddress == null) {
                return null;
            }
            if (streetAddress != null) {
                cityAddress = String.valueOf(streetAddress) + "~~" + cityAddress;
            }
            String statename = firstAddrestt.getAdminArea();
            if (statename != null) {
                if (statename.length() > 2 && (stateAbbre = ShopLocalActivity.STATE_DICTIONARY.get(statename)) != null) {
                    statename = stateAbbre;
                }
                cityAddress = String.valueOf(cityAddress) + ", " + statename;
            }
            return cityAddress;
        } catch (Exception e4) {
            e4.printStackTrace();
        }
    }

    private GeoLocation findCityGeoLocation(String locationName) {
        List<Address> myList;
        List<Address> myList2;
        String stateAbbre;
        GeoLocation cityGeoLocation = null;
        new ArrayList();
        try {
            Geocoder myLocationGeoCoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            try {
                myList = myLocationGeoCoder.getFromLocationName(locationName, 1);
            } catch (IOException e) {
                myList = ReverseGeocode.getFromLocation(locationName, 1);
            }
            for (int numbers = 0; numbers < 4; numbers++) {
                if (!myList2.isEmpty()) {
                    break;
                }
                try {
                    myList2 = myLocationGeoCoder.getFromLocationName(locationName, 1);
                } catch (IOException e2) {
                    myList2 = ReverseGeocode.getFromLocation(locationName, 1);
                }
            }
            if (myList2.isEmpty()) {
                return null;
            }
            Address firstAddrestt = myList2.get(0);
            String countryCode = firstAddrestt.getCountryCode();
            if (countryCode == null) {
                return null;
            }
            if (!countryCode.equalsIgnoreCase("US") && !countryCode.equalsIgnoreCase("CA")) {
                return null;
            }
            String cityAddress = firstAddrestt.getLocality();
            if (cityAddress == null) {
                return null;
            }
            if (!locationName.toLowerCase().contains(cityAddress.toLowerCase())) {
                return null;
            }
            String statename = firstAddrestt.getAdminArea();
            if (statename != null) {
                if (statename.length() > 2 && (stateAbbre = ShopLocalActivity.STATE_DICTIONARY.get(statename)) != null) {
                    statename = stateAbbre;
                }
                cityAddress = String.valueOf(cityAddress) + ", " + statename;
            }
            GeoLocation cityGeoLocation2 = new GeoLocation();
            try {
                cityGeoLocation2.setTitle(cityAddress);
                cityGeoLocation2.setLat(firstAddrestt.getLatitude());
                cityGeoLocation2.setLng(firstAddrestt.getLongitude());
                cityGeoLocation = cityGeoLocation2;
            } catch (Exception e3) {
                cityGeoLocation = cityGeoLocation2;
            }
            return cityGeoLocation;
        } catch (Exception e4) {
        }
    }

    private boolean isStateNameInside(String locationName) {
        Enumeration<String> e = ShopLocalActivity.STATE_DICTIONARY.keys();
        while (e.hasMoreElements()) {
            if (locationName.indexOf(e.nextElement().toLowerCase()) > -1) {
                return true;
            }
        }
        return false;
    }

    class GeoLocationAdapter extends BaseAdapter implements AdapterView.OnItemClickListener {
        private final LayoutInflater mInflater;
        private final List<GeoLocation> mItems;

        public GeoLocationAdapter(List<GeoLocation> items) {
            this.mItems = items;
            this.mInflater = (LayoutInflater) ShopLocalMapListActivity.this.getSystemService("layout_inflater");
        }

        public int getCount() {
            if (this.mItems == null) {
                return 0;
            }
            return this.mItems.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            GeoLocation gl;
            View v = convertView;
            if (v == null) {
                v = this.mInflater.inflate((int) R.layout.location_list_row, (ViewGroup) null);
            }
            if (position < this.mItems.size() && (gl = this.mItems.get(position)) != null) {
                String inputstring = ShopLocalMapListActivity.this.mSearchLocationEditText.getText().toString().toLowerCase();
                String inputstringCondensed = inputstring.replaceAll(" ", "").replaceAll(",", "");
                String lowercaseTitle = gl.getTitle().toLowerCase();
                if (lowercaseTitle.replaceAll(" ", "").replaceAll(",", "").startsWith(inputstringCondensed)) {
                    int startIndex = 0;
                    for (int j = 0; j < inputstringCondensed.length() && startIndex < lowercaseTitle.length(); j++) {
                        int oneChar = lowercaseTitle.charAt(startIndex);
                        while (true) {
                            if ((oneChar == 32 || oneChar == 44) && startIndex < lowercaseTitle.length()) {
                                startIndex++;
                                oneChar = lowercaseTitle.charAt(startIndex);
                            }
                        }
                        if (oneChar != inputstringCondensed.charAt(j)) {
                            break;
                        }
                        startIndex++;
                    }
                    int partIndex = 0;
                    for (int j2 = 0; j2 < inputstringCondensed.length() && partIndex < inputstring.length(); j2++) {
                        int oneChar2 = inputstring.charAt(partIndex);
                        while (true) {
                            if ((oneChar2 == 32 || oneChar2 == 44) && partIndex < inputstring.length()) {
                                partIndex++;
                                oneChar2 = inputstring.charAt(partIndex);
                            }
                        }
                        if (oneChar2 != inputstringCondensed.charAt(j2)) {
                            break;
                        }
                        partIndex++;
                    }
                    while (partIndex < inputstring.length() && startIndex < lowercaseTitle.length() && inputstring.charAt(partIndex) == lowercaseTitle.charAt(startIndex)) {
                        partIndex++;
                        startIndex++;
                    }
                    TextView tt = (TextView) v.findViewById(R.id.locationFirstEntryText);
                    if (tt != null) {
                        tt.setText(gl.getTitle().substring(0, startIndex));
                    }
                    TextView st = (TextView) v.findViewById(R.id.locationSecondEntryText);
                    if (st != null) {
                        st.setText(gl.getTitle().substring(startIndex));
                    }
                }
            }
            return v;
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            ShopLocalMapListActivity.this.mProgressbar.setVisibility(0);
            GeoLocation gl = this.mItems.get(position);
            ShopLocalMapListActivity.this.resetMap(gl.getLat(), gl.getLng(), gl.getTitle());
        }
    }

    class KeywordListAdapter extends BaseAdapter implements AdapterView.OnItemClickListener {
        private final LayoutInflater mInflater;
        private final ArrayList<String> mItems;

        public KeywordListAdapter(ArrayList<String> items) {
            this.mItems = items;
            this.mInflater = (LayoutInflater) ShopLocalMapListActivity.this.getSystemService("layout_inflater");
        }

        public int getCount() {
            if (this.mItems == null) {
                return 0;
            }
            return this.mItems.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = this.mInflater.inflate((int) R.layout.keyword_list_row, (ViewGroup) null);
            }
            if (position < this.mItems.size()) {
                String title = this.mItems.get(position);
                TextView st = (TextView) v.findViewById(R.id.keywordSearchText);
                if (st != null) {
                    st.setText(title);
                }
            }
            return v;
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            String title = this.mItems.get(position);
            ShopLocalMapListActivity.this.mSearchKeyworkEditText.setText(title);
            SharedPreferences.Editor editor = ShopLocalMapListActivity.this.mShopLocalPrefs.edit();
            editor.putString("SLSearchKeyword", title);
            editor.commit();
            ShopLocalMapListActivity.this.mSelectedImprintIndex = -1;
            ShopLocalMapListActivity.this.imprintSearchListView.setVisibility(8);
            ShopLocalMapListActivity.this.searchImprintResult();
        }
    }

    class ImprintSearchListAdapter extends BaseAdapter implements AdapterView.OnItemClickListener {
        private final LayoutInflater mInflater;
        private final ArrayList<Imprint> mItems;

        public ImprintSearchListAdapter(ArrayList<Imprint> items) {
            this.mItems = items;
            this.mInflater = (LayoutInflater) ShopLocalMapListActivity.this.getSystemService("layout_inflater");
        }

        public int getCount() {
            if (this.mItems == null) {
                return 0;
            }
            return this.mItems.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = this.mInflater.inflate((int) R.layout.imprint_list_row, (ViewGroup) null);
            }
            if (position < this.mItems.size()) {
                Imprint cImprint = this.mItems.get(position);
                RelativeLayout imprintLayout = (RelativeLayout) v.findViewById(R.id.imprint_layout);
                int imprintLevel = cImprint.getPremiumLevel();
                if (imprintLevel > 2) {
                    imprintLevel = 3;
                }
                if (imprintLevel > 0) {
                    imprintLayout.setBackgroundResource(ShopLocalMapListActivity.this.getResources().getIdentifier(String.format("premium_bg%d", Integer.valueOf(imprintLevel)), "drawable", "com.santabarbarayp.www"));
                } else {
                    imprintLayout.setBackgroundColor(-1);
                }
                ((TextView) v.findViewById(R.id.imprint_title)).setText(cImprint.getName());
                String streetAddress = cImprint.getStreet();
                TextView imprintStreetView = (TextView) v.findViewById(R.id.imprint_street);
                ViewGroup.LayoutParams streetPa = imprintStreetView.getLayoutParams();
                if (streetAddress == null || streetAddress.length() <= 0) {
                    streetPa.height = 0;
                } else {
                    imprintStreetView.setText(streetAddress);
                    streetPa.height = -2;
                }
                imprintStreetView.setLayoutParams(streetPa);
                String cityAddress = cImprint.getCityStateZip();
                TextView imprintCityView = (TextView) v.findViewById(R.id.imprint_city);
                ViewGroup.LayoutParams cityPa = imprintCityView.getLayoutParams();
                if (cityAddress == null || cityAddress.length() <= 0) {
                    cityPa.height = 0;
                } else {
                    imprintCityView.setText(cityAddress);
                    cityPa.height = -2;
                }
                imprintCityView.setLayoutParams(cityPa);
                String phoneNumber = cImprint.getPhone();
                Button imprintPhoneButton = (Button) v.findViewById(R.id.imprint_phone);
                imprintPhoneButton.setOnTouchListener(ShopLocalMapListActivity.this);
                imprintPhoneButton.setTag("p" + position);
                ViewGroup.LayoutParams phonePa = imprintPhoneButton.getLayoutParams();
                if (phoneNumber == null || phoneNumber.length() <= 0) {
                    phonePa.height = 0;
                } else {
                    imprintPhoneButton.setText(phoneNumber);
                    final String str = phoneNumber;
                    imprintPhoneButton.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            try {
                                ShopLocalMapListActivity.this.HideBannerCircularPost();
                                ShopLocalMapListActivity.this.startActivityForResult(Intent.getIntent("tel:" + str.replaceAll("[^a-zA-Z0-9]", "")), ShopLocalActivity.PHONE_CALL_REQUEST_CODE);
                            } catch (Exception e) {
                            }
                        }
                    });
                    phonePa.height = -2;
                }
                imprintPhoneButton.setLayoutParams(phonePa);
                TextView imprintMoreView = (TextView) v.findViewById(R.id.imprint_more);
                ViewGroup.LayoutParams morePa = imprintMoreView.getLayoutParams();
                if (cImprint.getElementList().size() > 1) {
                    morePa.height = -2;
                } else {
                    morePa.height = 0;
                }
                imprintMoreView.setLayoutParams(morePa);
                ImageView imprintAnnotationButton = (ImageView) v.findViewById(R.id.imprint_annotation_image_button);
                imprintAnnotationButton.setOnTouchListener(ShopLocalMapListActivity.this);
                imprintAnnotationButton.setTag("p" + position);
                ViewGroup.LayoutParams annotationPa = imprintAnnotationButton.getLayoutParams();
                int geoIndex = cImprint.getGeoLocationIndex();
                if (geoIndex < 0) {
                    annotationPa.width = 0;
                } else {
                    annotationPa.width = -2;
                    if (geoIndex > 25) {
                        geoIndex %= 26;
                    }
                    imprintAnnotationButton.setImageResource(ShopLocalMapListActivity.this.getResources().getIdentifier(String.format("noarrow_%c", Character.valueOf((char) (geoIndex + 97))), "drawable", "com.santabarbarayp.www"));
                    final int i = position;
                    imprintAnnotationButton.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            try {
                                ShopLocalMapListActivity.this.mSelectedImprintIndex = i;
                                ShopLocalMapListActivity.this.mSelectAnnotationIndex = -1;
                                ShopLocalMapListActivity.this.mapViewFlipClick(v);
                            } catch (Exception e) {
                            }
                        }
                    });
                }
                imprintAnnotationButton.setLayoutParams(annotationPa);
                double imprintDistance = cImprint.getDistance();
                TextView imprintDistanceView = (TextView) v.findViewById(R.id.imprint_distance);
                ViewGroup.LayoutParams distancePa = imprintDistanceView.getLayoutParams();
                if (imprintDistance < 0.0d) {
                    distancePa.width = 0;
                    distancePa.height = 0;
                } else {
                    imprintDistanceView.setText(String.format("%.1f mi", Double.valueOf(0.005d + imprintDistance)));
                    distancePa.width = -2;
                    distancePa.height = -2;
                }
                imprintDistanceView.setLayoutParams(distancePa);
                ImageView globeView = (ImageView) v.findViewById(R.id.imprint_globe);
                ImageView adView = (ImageView) v.findViewById(R.id.imprint_ad);
                ImageView couponView = (ImageView) v.findViewById(R.id.imprint_coupon);
                ImageView menuView = (ImageView) v.findViewById(R.id.imprint_menu);
                ImageView videoView = (ImageView) v.findViewById(R.id.imprint_video);
                ImageView emailView = (ImageView) v.findViewById(R.id.imprint_email);
                ViewGroup.LayoutParams globePa = globeView.getLayoutParams();
                ViewGroup.LayoutParams adPa = adView.getLayoutParams();
                ViewGroup.LayoutParams couponPa = couponView.getLayoutParams();
                ViewGroup.LayoutParams menuPa = menuView.getLayoutParams();
                ViewGroup.LayoutParams videoPa = videoView.getLayoutParams();
                ViewGroup.LayoutParams emailPa = emailView.getLayoutParams();
                if (cImprint.isWebsite()) {
                    globePa.width = -2;
                    globePa.height = -2;
                } else {
                    globePa.height = 0;
                    globePa.width = 0;
                }
                if (cImprint.hasCoupon()) {
                    couponPa.width = -2;
                    couponPa.height = -2;
                } else {
                    couponPa.height = 0;
                    couponPa.width = 0;
                }
                String imprintMenu = cImprint.getMenu();
                if (imprintMenu == null || imprintMenu.length() <= 0) {
                    menuPa.height = 0;
                    menuPa.width = 0;
                } else {
                    menuPa.width = -2;
                    menuPa.height = -2;
                }
                String imprintVideo = cImprint.getVideoUrl();
                if (imprintVideo == null || imprintVideo.length() <= 0) {
                    videoPa.height = 0;
                    videoPa.width = 0;
                } else {
                    videoPa.width = -2;
                    videoPa.height = -2;
                }
                String adImageurl = cImprint.getADImageUrl();
                if (adImageurl == null || adImageurl.length() == 0) {
                    adPa.height = 0;
                    adPa.width = 0;
                } else {
                    adPa.width = -2;
                    adPa.height = -2;
                }
                String emailAddress = cImprint.getEmailAddress();
                if (emailAddress == null || emailAddress.length() == 0) {
                    emailPa.height = 0;
                    emailPa.width = 0;
                } else {
                    emailPa.width = -2;
                    emailPa.height = -2;
                }
                globeView.setLayoutParams(globePa);
                couponView.setLayoutParams(couponPa);
                menuView.setLayoutParams(menuPa);
                videoView.setLayoutParams(videoPa);
                adView.setLayoutParams(adPa);
                emailView.setLayoutParams(emailPa);
            }
            return v;
        }

        /* JADX WARN: Type inference failed for: r2v12, types: [android.content.Context, com.santabarbarayp.www.ShopLocalMapListActivity] */
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Imprint bImprint = this.mItems.get(position);
            ShopLocalMapListActivity.this.mSelectedImprintIndex = position;
            ShopLocalMapListActivity.this.mSelectAnnotationIndex = -1;
            ShopLocalMapListActivity.this.HideBannerCircularPost();
            String homeAllAddress = ShopLocalMapListActivity.this.mDefaultHomeLocation;
            if (ShopLocalMapListActivity.this.mDefaultStreetLocation != null) {
                homeAllAddress = String.valueOf(ShopLocalMapListActivity.this.mDefaultStreetLocation) + ", " + homeAllAddress;
            }
            GeoLocation localSearchLocation = new GeoLocation(homeAllAddress, ((double) ShopLocalMapListActivity.this.mGeoPoint_DefaultHomeLocation.getLatitudeE6()) / 1000000.0d, ((double) ShopLocalMapListActivity.this.mGeoPoint_DefaultHomeLocation.getLongitudeE6()) / 1000000.0d);
            Intent intent = new Intent((Context) ShopLocalMapListActivity.this, ImprintDetailActivity.class);
            Bundle b = bImprint.toBundle();
            b.putParcelable("ShopLocal_SearchHomeLocation", localSearchLocation);
            intent.putExtras(b);
            ShopLocalMapListActivity.this.startActivityForResult(intent, ShopLocalActivity.IMPRINT_PROFILE_REQUEST_CODE);
        }
    }

    class ShopLocalOverlay extends Overlay {
        boolean MoveMap;
        private Bitmap arrowDown;
        private Paint arrowDownPaint;
        private Bitmap arrowUp;
        private Bitmap bubbleShadow;
        private Bitmap detailButton;
        private Bitmap homeIcon;
        private Paint innerPaint;
        private boolean isDoubleTap = false;
        private ArrayList<PlaceMarker> items = new ArrayList<>();
        private Bitmap letterIcon;
        private RectF mDetailRectRange = new RectF();
        private long mLastTouchTime = -1;
        private Paint outerBorderPaint;
        private Bitmap pinDot;
        boolean pinchMoveMap;
        private PlaceMarker selectedPlaceMarker = null;
        private Paint textPaint;

        public ShopLocalOverlay() {
            ShopLocalMapListActivity.this.mSelectedImprintIndex = -1;
            this.letterIcon = BitmapFactory.decodeResource(ShopLocalMapListActivity.this.getResources(), R.drawable.pin_a);
            this.homeIcon = BitmapFactory.decodeResource(ShopLocalMapListActivity.this.getResources(), R.drawable.home_pin);
            this.pinDot = BitmapFactory.decodeResource(ShopLocalMapListActivity.this.getResources(), R.drawable.pin_0);
            this.bubbleShadow = BitmapFactory.decodeResource(ShopLocalMapListActivity.this.getResources(), R.drawable.bubble_shadow);
            this.detailButton = BitmapFactory.decodeResource(ShopLocalMapListActivity.this.getResources(), R.drawable.next);
            this.arrowDown = BitmapFactory.decodeResource(ShopLocalMapListActivity.this.getResources(), R.drawable.arrow_down);
            this.arrowUp = BitmapFactory.decodeResource(ShopLocalMapListActivity.this.getResources(), R.drawable.arrow_up);
            this.mDetailRectRange.set(0.0f, 0.0f, 0.0f, 0.0f);
            this.items.add(new PlaceMarker(new GeoPoint(ShopLocalMapListActivity.this.mGeoPoint_SearchHomeLocation.getLatitudeE6(), ShopLocalMapListActivity.this.mGeoPoint_SearchHomeLocation.getLongitudeE6()), "Current Location", ShopLocalMapListActivity.this.mSearchHomeLocation));
            this.selectedPlaceMarker = this.items.get(0);
        }

        public ShopLocalOverlay(int startIndex, int includedEndIndex) {
            this.letterIcon = BitmapFactory.decodeResource(ShopLocalMapListActivity.this.getResources(), R.drawable.pin_a);
            this.homeIcon = BitmapFactory.decodeResource(ShopLocalMapListActivity.this.getResources(), R.drawable.home_pin);
            this.pinDot = BitmapFactory.decodeResource(ShopLocalMapListActivity.this.getResources(), R.drawable.pin_0);
            this.bubbleShadow = BitmapFactory.decodeResource(ShopLocalMapListActivity.this.getResources(), R.drawable.bubble_shadow);
            this.detailButton = BitmapFactory.decodeResource(ShopLocalMapListActivity.this.getResources(), R.drawable.next);
            this.arrowDown = BitmapFactory.decodeResource(ShopLocalMapListActivity.this.getResources(), R.drawable.arrow_down);
            this.arrowUp = BitmapFactory.decodeResource(ShopLocalMapListActivity.this.getResources(), R.drawable.arrow_up);
            this.mDetailRectRange.set(0.0f, 0.0f, 0.0f, 0.0f);
            this.items.add(new PlaceMarker(new GeoPoint(ShopLocalMapListActivity.this.mGeoPoint_SearchHomeLocation.getLatitudeE6(), ShopLocalMapListActivity.this.mGeoPoint_SearchHomeLocation.getLongitudeE6()), "Current Location", ShopLocalMapListActivity.this.mSearchHomeLocation));
            for (int i = includedEndIndex; i >= startIndex; i--) {
                Imprint localImprint = ShopLocalMapListActivity.this.mImprintList.get(i);
                if (localImprint.getGeoLocationIndex() > -1) {
                    ImprintElementList imprintElementList = localImprint.getElementList();
                    for (int k = imprintElementList.size() - 1; k > 0; k--) {
                        ImprintElement localIE = (ImprintElement) imprintElementList.get(k);
                        if (localIE.getDistance() > -1.0d) {
                            String elementAddress = localIE.getStreet();
                            String elementCityAddress = localIE.getCityStateZip();
                            PlaceMarker placeMarker = new PlaceMarker(new GeoPoint((int) (localIE.getLat() * 1000000.0d), (int) (localIE.getLng() * 1000000.0d)), localIE.getName(), elementAddress);
                            placeMarker.setCityAddress(elementCityAddress);
                            placeMarker.setDrawableChar('0');
                            placeMarker.setImprintIndex(i);
                            this.items.add(placeMarker);
                        }
                    }
                }
            }
            for (int i2 = includedEndIndex; i2 >= startIndex; i2--) {
                Imprint localImprint2 = ShopLocalMapListActivity.this.mImprintList.get(i2);
                int geoIndex = localImprint2.getGeoLocationIndex();
                if (geoIndex > -1) {
                    char markerChar = (char) ((geoIndex > 25 ? geoIndex % 26 : geoIndex) + 97);
                    String imprintAddress = localImprint2.getStreet();
                    String imprintCityAddress = localImprint2.getCityStateZip();
                    PlaceMarker placeMarker2 = new PlaceMarker(new GeoPoint((int) (localImprint2.getLat() * 1000000.0d), (int) (localImprint2.getLng() * 1000000.0d)), localImprint2.getName(), imprintAddress);
                    placeMarker2.setCityAddress(imprintCityAddress);
                    placeMarker2.setDrawableChar(markerChar);
                    placeMarker2.setImprintIndex(i2);
                    this.items.add(placeMarker2);
                }
            }
            if (ShopLocalMapListActivity.this.mSelectAnnotationIndex > -1) {
                if (ShopLocalMapListActivity.this.mSelectAnnotationIndex < this.items.size()) {
                    this.selectedPlaceMarker = this.items.get(ShopLocalMapListActivity.this.mSelectAnnotationIndex);
                }
            } else if (ShopLocalMapListActivity.this.mSelectedImprintIndex > -1) {
                int k2 = this.items.size() - 1;
                while (true) {
                    if (k2 <= 0) {
                        break;
                    }
                    PlaceMarker temp = this.items.get(k2);
                    char markerChar2 = temp.getDrawableChar();
                    if (temp.getImprintIndex() >= ShopLocalMapListActivity.this.mSelectedImprintIndex && markerChar2 != '0') {
                        this.selectedPlaceMarker = temp;
                        break;
                    }
                    k2--;
                }
            }
            if (this.selectedPlaceMarker == null) {
                this.selectedPlaceMarker = this.items.get(this.items.size() - 1);
                ShopLocalMapListActivity.this.mSelectedImprintIndex = this.selectedPlaceMarker.getImprintIndex();
                ShopLocalMapListActivity.this.mSelectAnnotationIndex = -1;
            }
        }

        /* JADX WARN: Type inference failed for: r2v31, types: [android.content.Context, com.santabarbarayp.www.ShopLocalMapListActivity] */
        public boolean onTap(GeoPoint p, MapView mapView) {
            boolean isRemovePriorPopup;
            if (this.isDoubleTap) {
                return true;
            }
            Point tapScreenCoords = new Point();
            mapView.getProjection().toPixels(p, tapScreenCoords);
            if (this.mDetailRectRange.contains((float) tapScreenCoords.x, (float) tapScreenCoords.y)) {
                if (ShopLocalMapListActivity.this.mSelectedImprintIndex > -1) {
                    Imprint bImprint = ShopLocalMapListActivity.this.mImprintList.get(ShopLocalMapListActivity.this.mSelectedImprintIndex);
                    ShopLocalMapListActivity.this.HideBannerCircularPost();
                    String homeAllAddress = ShopLocalMapListActivity.this.mDefaultHomeLocation;
                    if (ShopLocalMapListActivity.this.mDefaultStreetLocation != null) {
                        homeAllAddress = String.valueOf(ShopLocalMapListActivity.this.mDefaultStreetLocation) + ", " + homeAllAddress;
                    }
                    GeoLocation localSearchLocation = new GeoLocation(homeAllAddress, ((double) ShopLocalMapListActivity.this.mGeoPoint_DefaultHomeLocation.getLatitudeE6()) / 1000000.0d, ((double) ShopLocalMapListActivity.this.mGeoPoint_DefaultHomeLocation.getLongitudeE6()) / 1000000.0d);
                    Intent intent = new Intent((Context) ShopLocalMapListActivity.this, ImprintDetailActivity.class);
                    Bundle b = bImprint.toBundle();
                    b.putParcelable("ShopLocal_SearchHomeLocation", localSearchLocation);
                    intent.putExtras(b);
                    ShopLocalMapListActivity.this.startActivityForResult(intent, ShopLocalActivity.IMPRINT_PROFILE_REQUEST_CODE);
                }
                return true;
            }
            if (this.selectedPlaceMarker != null) {
                isRemovePriorPopup = true;
            } else {
                isRemovePriorPopup = false;
            }
            int hitMapPlacemarkerIndex = getHitMapPlaceMarker(mapView, p);
            ShopLocalMapListActivity.this.mSelectAnnotationIndex = hitMapPlacemarkerIndex;
            if (hitMapPlacemarkerIndex > -1) {
                this.selectedPlaceMarker = (PlaceMarker) this.items.get(hitMapPlacemarkerIndex);
                ShopLocalMapListActivity.this.mSelectedImprintIndex = this.selectedPlaceMarker.getImprintIndex();
            } else {
                this.selectedPlaceMarker = null;
                ShopLocalMapListActivity.this.mSelectedImprintIndex = -1;
            }
            if (isRemovePriorPopup || this.selectedPlaceMarker != null) {
                mapView.invalidate();
            }
            if (this.selectedPlaceMarker != null) {
                return true;
            }
            return false;
        }

        public void draw(Canvas canvas, MapView mapView, boolean shadow) {
            drawMapMarkers(canvas, mapView, shadow);
            drawInfoWindow(canvas, mapView);
        }

        private void drawMapMarkers(Canvas canvas, MapView mapView, boolean shadow) {
            Point screenCoords = new Point();
            mapView.getProjection().toPixels(ShopLocalMapListActivity.this.mGeoPoint_SearchHomeLocation, screenCoords);
            canvas.drawBitmap(this.homeIcon, (float) (screenCoords.x - (this.homeIcon.getWidth() / 2)), (float) (screenCoords.y - this.homeIcon.getHeight()), (Paint) null);
            for (int k = 1; k < this.items.size(); k++) {
                PlaceMarker imOverItem = this.items.get(k);
                mapView.getProjection().toPixels(imOverItem.getPoint(), screenCoords);
                char markerChar = imOverItem.getDrawableChar();
                if (markerChar == '0') {
                    canvas.drawBitmap(this.pinDot, (float) (screenCoords.x - (this.pinDot.getWidth() / 2)), (float) (screenCoords.y - this.pinDot.getHeight()), (Paint) null);
                } else {
                    Bitmap markerIcon = BitmapFactory.decodeResource(ShopLocalMapListActivity.this.getResources(), ShopLocalMapListActivity.this.getResources().getIdentifier(String.format("pin_%c", Character.valueOf(markerChar)), "drawable", "com.santabarbarayp.www"));
                    canvas.drawBitmap(markerIcon, (float) screenCoords.x, (float) (screenCoords.y - markerIcon.getHeight()), (Paint) null);
                }
            }
        }

        private void drawInfoWindow(Canvas canvas, MapView mapView) {
            Bitmap selectedMarkerIcon;
            int markerType;
            Projection myprojection = mapView.getProjection();
            this.mDetailRectRange.set(0.0f, 0.0f, 0.0f, 0.0f);
            if (this.selectedPlaceMarker != null) {
                PlaceMarker localPlaceMarker = this.selectedPlaceMarker;
                if (localPlaceMarker.getTitle().equals("Current Location")) {
                    selectedMarkerIcon = this.homeIcon;
                    markerType = 0;
                } else {
                    char markerChar = localPlaceMarker.getDrawableChar();
                    int markResourceID = ShopLocalMapListActivity.this.getResources().getIdentifier(String.format("pin_%c_on", Character.valueOf(localPlaceMarker.getDrawableChar())), "drawable", "com.santabarbarayp.www");
                    Point screenCoords = new Point();
                    mapView.getProjection().toPixels(localPlaceMarker.getPoint(), screenCoords);
                    selectedMarkerIcon = BitmapFactory.decodeResource(ShopLocalMapListActivity.this.getResources(), markResourceID);
                    if (markerChar == '0') {
                        markerType = 1;
                        canvas.drawBitmap(selectedMarkerIcon, (float) (screenCoords.x - (this.pinDot.getWidth() / 2)), (float) (screenCoords.y - this.pinDot.getHeight()), (Paint) null);
                    } else {
                        markerType = 2;
                        canvas.drawBitmap(selectedMarkerIcon, (float) screenCoords.x, (float) (screenCoords.y - selectedMarkerIcon.getHeight()), (Paint) null);
                    }
                }
                GeoPoint geoPoint = localPlaceMarker.getPoint();
                Point point = new Point();
                myprojection.toPixels(geoPoint, point);
                int cWidth = canvas.getWidth();
                int cHeight = canvas.getHeight();
                int bubbleWidth = cWidth;
                if (bubbleWidth > cHeight) {
                    bubbleWidth = cHeight;
                }
                if (bubbleWidth > 480) {
                    bubbleWidth = 480;
                }
                double fontFraction = ((double) bubbleWidth) / 480.0d;
                int two_ADJUST_INT = (int) (2.0d * fontFraction);
                int five_ADJUST_INT = (int) (5.0d * fontFraction);
                int seven_ADJUST_INT = (int) (7.0d * fontFraction);
                int eight_ADJUST_INT = (int) (8.0d * fontFraction);
                int ten_ADJUST_INT = (int) (10.0d * fontFraction);
                int thirteen_ADJUST_INT = (int) (13.0d * fontFraction);
                int fifteen_ADJUST_INT = (int) (15.0d * fontFraction);
                int TITLE_FONT_SIZE = (int) (22.0d * fontFraction);
                int ADDRESS_FONT_SIZE = eight_ADJUST_INT + eight_ADJUST_INT;
                int INFO_WINDOW_WIDTH = bubbleWidth - ten_ADJUST_INT;
                int INFO_WINDOW_HEIGHT = (int) ((65.0d * fontFraction) + 0.5d);
                int TEXT_OFFSET_X = ten_ADJUST_INT;
                int TEXT_OFFSET_Y = (int) (27.0d * fontFraction);
                int offsetWidthAdjust = this.letterIcon.getWidth() / 2;
                int offsetHeightAdjust = this.arrowDown.getHeight() - eight_ADJUST_INT;
                if (markerType == 0) {
                    offsetWidthAdjust = 0;
                } else if (markerType == 1) {
                    offsetWidthAdjust = 0;
                    offsetHeightAdjust -= this.pinDot.getHeight() / 4;
                }
                Paint titlePaint = new Paint();
                titlePaint.setTextSize((float) TITLE_FONT_SIZE);
                titlePaint.setStyle(Paint.Style.FILL_AND_STROKE);
                titlePaint.setARGB(255, 255, 255, 255);
                titlePaint.setTypeface(Typeface.DEFAULT_BOLD);
                titlePaint.setAntiAlias(true);
                String imprintTitle = localPlaceMarker.getTitle().trim();
                int largestWidth = (INFO_WINDOW_WIDTH - this.detailButton.getWidth()) - fifteen_ADJUST_INT;
                boolean isTitleLarger = false;
                int titleSize = imprintTitle.length();
                float titleWidth = titlePaint.measureText(imprintTitle, 0, titleSize);
                while (titleWidth > ((float) largestWidth) && titleSize > 4) {
                    isTitleLarger = true;
                    titleSize--;
                    titleWidth = titlePaint.measureText(imprintTitle, 0, titleSize);
                }
                if (isTitleLarger) {
                    imprintTitle = String.valueOf(imprintTitle.substring(0, titleSize - 3)) + "...";
                }
                float titleWidth2 = titlePaint.measureText(imprintTitle, 0, imprintTitle.length());
                Paint addressPaint = new Paint();
                addressPaint.setTextSize((float) ADDRESS_FONT_SIZE);
                addressPaint.setStyle(Paint.Style.FILL_AND_STROKE);
                addressPaint.setARGB(255, 255, 255, 255);
                addressPaint.setAntiAlias(true);
                String imprintStreetAddress = localPlaceMarker.getSnippet();
                if (imprintStreetAddress == null) {
                    imprintStreetAddress = "";
                }
                String cityAddress = localPlaceMarker.getCityAddress();
                if (cityAddress != null && cityAddress.trim().length() > 0) {
                    imprintStreetAddress = String.valueOf(imprintStreetAddress) + ", " + cityAddress.trim();
                }
                boolean isaddressLarger = false;
                int addressSize = imprintStreetAddress.length();
                float addressWidth = addressPaint.measureText(imprintStreetAddress, 0, addressSize);
                while (addressWidth > ((float) largestWidth) && addressSize > 4) {
                    isaddressLarger = true;
                    addressSize--;
                    addressWidth = addressPaint.measureText(imprintStreetAddress, 0, addressSize);
                }
                if (isaddressLarger) {
                    imprintStreetAddress = String.valueOf(imprintStreetAddress.substring(0, addressSize - 3)) + "...";
                }
                float addressWidth2 = addressPaint.measureText(imprintStreetAddress, 0, imprintStreetAddress.length());
                float largerWidth = addressWidth2;
                if (titleWidth2 > addressWidth2) {
                    largerWidth = titleWidth2;
                }
                int INFO_WINDOW_WIDTH2 = ((int) largerWidth) + this.detailButton.getWidth() + fifteen_ADJUST_INT;
                RectF rectF = new RectF(0.0f, 0.0f, (float) INFO_WINDOW_WIDTH2, (float) INFO_WINDOW_HEIGHT);
                int infoWindowOffsetX = (point.x - (INFO_WINDOW_WIDTH2 / 2)) + offsetWidthAdjust;
                if (point.x <= 0 || point.x > cWidth - this.letterIcon.getWidth()) {
                    if (point.x <= 0) {
                        infoWindowOffsetX = point.x;
                    } else {
                        infoWindowOffsetX = (point.x - INFO_WINDOW_WIDTH2) + this.letterIcon.getWidth();
                    }
                } else if (infoWindowOffsetX < 0) {
                    infoWindowOffsetX = 0;
                } else if (infoWindowOffsetX + INFO_WINDOW_WIDTH2 > cWidth) {
                    infoWindowOffsetX = cWidth - INFO_WINDOW_WIDTH2;
                }
                int infoWindowOffsetY = ((point.y - INFO_WINDOW_HEIGHT) - selectedMarkerIcon.getHeight()) - offsetHeightAdjust;
                boolean bottomArrow = true;
                if (infoWindowOffsetY < 0) {
                    bottomArrow = false;
                    infoWindowOffsetY = (point.y + this.arrowUp.getHeight()) - ten_ADJUST_INT;
                    if (markerType == 1) {
                        infoWindowOffsetY -= this.pinDot.getHeight() / 4;
                    }
                }
                rectF.offset((float) infoWindowOffsetX, (float) infoWindowOffsetY);
                Bitmap copyBubble = Bitmap.createScaledBitmap(this.bubbleShadow, INFO_WINDOW_WIDTH2, INFO_WINDOW_HEIGHT, true);
                int shadowLeftPoint = infoWindowOffsetX + seven_ADJUST_INT;
                if (markerType == 0) {
                    shadowLeftPoint = infoWindowOffsetX + five_ADJUST_INT;
                }
                canvas.drawBitmap(copyBubble, (float) shadowLeftPoint, (float) (infoWindowOffsetY + thirteen_ADJUST_INT), getInnerPaint());
                canvas.drawRoundRect(rectF, (float) five_ADJUST_INT, (float) five_ADJUST_INT, getInnerPaint());
                int largestHeightY = infoWindowOffsetY + TEXT_OFFSET_Y;
                canvas.drawText(imprintTitle, (float) (infoWindowOffsetX + TEXT_OFFSET_X), (float) largestHeightY, titlePaint);
                canvas.drawText(imprintStreetAddress, (float) (infoWindowOffsetX + TEXT_OFFSET_X), (float) (largestHeightY + TITLE_FONT_SIZE + two_ADJUST_INT), addressPaint);
                int arrowLeftPoint = (point.x + offsetWidthAdjust) - (this.arrowDown.getWidth() / 2);
                int arrowTopPoint = infoWindowOffsetY + INFO_WINDOW_HEIGHT;
                if (bottomArrow) {
                    canvas.drawBitmap(this.arrowDown, (float) arrowLeftPoint, (float) arrowTopPoint, getArrowDownPaint());
                } else {
                    canvas.drawBitmap(this.arrowUp, (float) arrowLeftPoint, (float) (infoWindowOffsetY - this.arrowUp.getHeight()), getInnerPaint());
                }
                if (markerType > 0) {
                    int leftPoint = (infoWindowOffsetX + INFO_WINDOW_WIDTH2) - this.detailButton.getWidth();
                    int topPoint = infoWindowOffsetY + two_ADJUST_INT;
                    canvas.drawBitmap(this.detailButton, (float) leftPoint, (float) topPoint, getTextPaint());
                    this.mDetailRectRange.set((float) leftPoint, (float) topPoint, (float) (this.detailButton.getWidth() + leftPoint), (float) (this.detailButton.getHeight() + topPoint));
                }
            }
        }

        public Paint getInnerPaint() {
            if (this.innerPaint == null) {
                this.innerPaint = new Paint();
                this.innerPaint.setARGB(225, 72, 72, 72);
                this.innerPaint.setStyle(Paint.Style.FILL);
                this.innerPaint.setAntiAlias(true);
            }
            return this.innerPaint;
        }

        public Paint getArrowDownPaint() {
            if (this.arrowDownPaint == null) {
                this.arrowDownPaint = new Paint();
                this.arrowDownPaint.setARGB(170, 68, 68, 68);
                this.arrowDownPaint.setStyle(Paint.Style.FILL);
                this.arrowDownPaint.setAntiAlias(true);
            }
            return this.arrowDownPaint;
        }

        public Paint getBorderPaint() {
            if (this.outerBorderPaint == null) {
                this.outerBorderPaint = new Paint();
                this.outerBorderPaint.setARGB(225, 72, 72, 72);
                this.outerBorderPaint.setAntiAlias(true);
                this.outerBorderPaint.setStyle(Paint.Style.STROKE);
                this.outerBorderPaint.setStrokeWidth(2.0f);
            }
            return this.outerBorderPaint;
        }

        public Paint getTextPaint() {
            if (this.textPaint == null) {
                this.textPaint = new Paint();
                this.textPaint.setARGB(255, 255, 255, 255);
                this.textPaint.setAntiAlias(true);
            }
            return this.textPaint;
        }

        private int getHitMapPlaceMarker(MapView mapView, GeoPoint tapPoint) {
            int hitMapPlacemarkerIndex = -1;
            RectF hitTestRecr = new RectF();
            Point screenCoords = new Point();
            int startindex = -1;
            if (this.selectedPlaceMarker != null) {
                int j = 0;
                while (true) {
                    if (j >= this.items.size()) {
                        break;
                    }
                    if (this.selectedPlaceMarker == this.items.get(j)) {
                        startindex = j - 1;
                        break;
                    }
                    j++;
                }
            }
            int markerLength = this.items.size();
            int k = 0;
            while (true) {
                if (k >= markerLength) {
                    break;
                }
                int index = ((markerLength - k) + startindex) % markerLength;
                PlaceMarker testPlacemarker = this.items.get(index);
                if (this.selectedPlaceMarker != testPlacemarker) {
                    mapView.getProjection().toPixels(testPlacemarker.getPoint(), screenCoords);
                    if (testPlacemarker.getTitle().equals("Current Location")) {
                        hitTestRecr.set((float) ((-this.homeIcon.getWidth()) / 2), (float) (-this.homeIcon.getHeight()), (float) (this.homeIcon.getWidth() / 2), 0.0f);
                    } else if (testPlacemarker.getDrawableChar() == '0') {
                        hitTestRecr.set((float) ((-this.pinDot.getWidth()) / 2), (float) (-this.pinDot.getHeight()), (float) (this.pinDot.getWidth() / 2), 0.0f);
                    } else {
                        hitTestRecr.set(0.0f, (float) (-this.letterIcon.getHeight()), (float) this.letterIcon.getWidth(), 0.0f);
                    }
                    hitTestRecr.offset((float) screenCoords.x, (float) screenCoords.y);
                    mapView.getProjection().toPixels(tapPoint, screenCoords);
                    if (hitTestRecr.contains((float) screenCoords.x, (float) screenCoords.y)) {
                        hitMapPlacemarkerIndex = index;
                        break;
                    }
                }
                k++;
            }
            return hitMapPlacemarkerIndex;
        }

        public boolean onTouchEvent(MotionEvent arg0, MapView arg1) {
            MapView myMapView = arg1;
            int Action = arg0.getAction();
            if (Action == 1) {
                if (this.MoveMap || this.pinchMoveMap) {
                    GeoPoint mapcenter = myMapView.getMapCenter();
                    double latitudeE6 = (double) mapcenter.getLatitudeE6();
                    double longitudeE6 = (double) mapcenter.getLongitudeE6();
                    double latitudeSpan = ((double) ShopLocalMapListActivity.this.mGeoPoint_SearchLocation.getLatitudeE6()) - latitudeE6;
                    if (latitudeSpan < 0.0d) {
                        latitudeSpan = -latitudeSpan;
                    }
                    double longitudeSpan = ((double) ShopLocalMapListActivity.this.mGeoPoint_SearchLocation.getLongitudeE6()) - longitudeE6;
                    if (longitudeSpan < 0.0d) {
                        longitudeSpan = -longitudeSpan;
                    }
                    double deltaLatitude = ((double) myMapView.getLatitudeSpan()) / 4.0d;
                    double deltaLongitude = ((double) myMapView.getLongitudeSpan()) / 3.0d;
                    if (this.pinchMoveMap || latitudeSpan > deltaLatitude || longitudeSpan > deltaLongitude) {
                        ShopLocalMapListActivity.this.mIsBoxSearch = true;
                        ShopLocalMapListActivity.this.mGeoPoint_SearchLocation = mapcenter;
                        ShopLocalMapListActivity.this.mBottomLeftGeoPoint = new GeoPoint((int) (latitudeE6 - (((double) myMapView.getLatitudeSpan()) / 2.0d)), (int) (longitudeE6 - (((double) myMapView.getLongitudeSpan()) / 2.0d)));
                        ShopLocalMapListActivity.this.mTopRightGeoPoint = new GeoPoint((int) ((((double) myMapView.getLatitudeSpan()) / 2.0d) + latitudeE6), (int) ((((double) myMapView.getLongitudeSpan()) / 2.0d) + longitudeE6));
                        ShopLocalMapListActivity.this.searchImprintResultForBox(ShopLocalMapListActivity.this.mBottomLeftGeoPoint, ShopLocalMapListActivity.this.mTopRightGeoPoint);
                    }
                }
            } else if (Action == 0) {
                if (System.currentTimeMillis() - this.mLastTouchTime < 300) {
                    this.isDoubleTap = true;
                    this.mLastTouchTime = -1;
                    myMapView.getController().zoomInFixing((int) arg0.getX(), (int) arg0.getY());
                    GeoPoint mapcenter2 = myMapView.getMapCenter();
                    ShopLocalMapListActivity.this.mIsBoxSearch = true;
                    ShopLocalMapListActivity.this.mGeoPoint_SearchLocation = mapcenter2;
                    double latitudeE62 = (double) mapcenter2.getLatitudeE6();
                    double longitudeE62 = (double) mapcenter2.getLongitudeE6();
                    ShopLocalMapListActivity.this.mBottomLeftGeoPoint = new GeoPoint((int) (latitudeE62 - (((double) myMapView.getLatitudeSpan()) / 2.0d)), (int) (longitudeE62 - (((double) myMapView.getLongitudeSpan()) / 2.0d)));
                    ShopLocalMapListActivity.this.mTopRightGeoPoint = new GeoPoint((int) ((((double) myMapView.getLatitudeSpan()) / 2.0d) + latitudeE62), (int) ((((double) myMapView.getLongitudeSpan()) / 2.0d) + longitudeE62));
                    ShopLocalMapListActivity.this.searchImprintResultForBox(ShopLocalMapListActivity.this.mBottomLeftGeoPoint, ShopLocalMapListActivity.this.mTopRightGeoPoint);
                } else {
                    this.isDoubleTap = false;
                    this.mLastTouchTime = System.currentTimeMillis();
                }
                this.MoveMap = false;
                this.pinchMoveMap = false;
            } else if (Action == 2) {
                this.MoveMap = true;
            } else if (Action == 262) {
                this.pinchMoveMap = true;
            }
            return ShopLocalMapListActivity.super.onTouchEvent(arg0, arg1);
        }
    }

    public void onVisibilityChanged(boolean visible) {
    }

    public void onZoom(boolean zoomIn) {
        if (zoomIn) {
            this.mc.zoomIn();
            this.mZoomLevel++;
        } else {
            this.mc.zoomOut();
            this.mZoomLevel--;
        }
        GeoPoint mapcenter = this.imprintSearchMapView.getMapCenter();
        double latitudeE6 = (double) mapcenter.getLatitudeE6();
        double longitudeE6 = (double) mapcenter.getLongitudeE6();
        this.mIsBoxSearch = true;
        this.mGeoPoint_SearchLocation = mapcenter;
        this.mBottomLeftGeoPoint = new GeoPoint((int) (latitudeE6 - (((double) this.imprintSearchMapView.getLatitudeSpan()) / 2.0d)), (int) (longitudeE6 - (((double) this.imprintSearchMapView.getLongitudeSpan()) / 2.0d)));
        this.mTopRightGeoPoint = new GeoPoint((int) ((((double) this.imprintSearchMapView.getLatitudeSpan()) / 2.0d) + latitudeE6), (int) ((((double) this.imprintSearchMapView.getLongitudeSpan()) / 2.0d) + longitudeE6));
        searchImprintResultForBox(this.mBottomLeftGeoPoint, this.mTopRightGeoPoint);
    }

    public boolean onTouchEvent(MotionEvent me) {
        return this.mGestureDetector.onTouchEvent(me);
    }

    public boolean onDown(MotionEvent e) {
        return false;
    }

    /* JADX WARN: Type inference failed for: r10v0, types: [android.content.Context, com.santabarbarayp.www.ShopLocalMapListActivity] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean onFling(android.view.MotionEvent r11, android.view.MotionEvent r12, float r13, float r14) {
        /*
            r10 = this;
            float r2 = r11.getY()     // Catch:{ Exception -> 0x00d3 }
            float r3 = r12.getY()     // Catch:{ Exception -> 0x00d3 }
            float r2 = r2 - r3
            float r2 = java.lang.Math.abs(r2)     // Catch:{ Exception -> 0x00d3 }
            r3 = 1132068864(0x437a0000, float:250.0)
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 <= 0) goto L_0x0015
            r2 = 0
        L_0x0014:
            return r2
        L_0x0015:
            float r2 = r11.getX()     // Catch:{ Exception -> 0x00d3 }
            float r3 = r12.getX()     // Catch:{ Exception -> 0x00d3 }
            float r2 = r2 - r3
            r3 = 1123024896(0x42f00000, float:120.0)
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 <= 0) goto L_0x00e1
            float r2 = java.lang.Math.abs(r13)     // Catch:{ Exception -> 0x00d3 }
            r3 = 1128792064(0x43480000, float:200.0)
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 <= 0) goto L_0x00e1
            boolean r2 = r10.mIsFlipListViewOption     // Catch:{ Exception -> 0x00d3 }
            if (r2 == 0) goto L_0x0055
            android.widget.ListView r2 = r10.imprintSearchListView     // Catch:{ Exception -> 0x00d3 }
            float r3 = r11.getX()     // Catch:{ Exception -> 0x00d3 }
            float r4 = r12.getX()     // Catch:{ Exception -> 0x00d3 }
            float r3 = r3 + r4
            int r3 = (int) r3     // Catch:{ Exception -> 0x00d3 }
            int r3 = r3 / 2
            float r4 = r11.getY()     // Catch:{ Exception -> 0x00d3 }
            float r5 = r12.getY()     // Catch:{ Exception -> 0x00d3 }
            float r4 = r4 + r5
            int r4 = (int) r4     // Catch:{ Exception -> 0x00d3 }
            int r4 = r4 / 2
            int r9 = r2.pointToPosition(r3, r4)     // Catch:{ Exception -> 0x00d3 }
            r2 = -1
            if (r9 == r2) goto L_0x00ca
            r10.mSelectedImprintIndex = r9     // Catch:{ Exception -> 0x00d3 }
        L_0x0055:
            int r2 = r10.mSelectedImprintIndex     // Catch:{ Exception -> 0x00d3 }
            r3 = -1
            if (r2 <= r3) goto L_0x00d4
            int r2 = r10.mSelectedImprintIndex     // Catch:{ Exception -> 0x00d3 }
            java.util.ArrayList<com.santabarbarayp.www.Imprint> r3 = r10.mImprintList     // Catch:{ Exception -> 0x00d3 }
            int r3 = r3.size()     // Catch:{ Exception -> 0x00d3 }
            if (r2 >= r3) goto L_0x00d4
            java.util.ArrayList<com.santabarbarayp.www.Imprint> r2 = r10.mImprintList     // Catch:{ Exception -> 0x00d3 }
            int r3 = r10.mSelectedImprintIndex     // Catch:{ Exception -> 0x00d3 }
            java.lang.Object r7 = r2.get(r3)     // Catch:{ Exception -> 0x00d3 }
            com.santabarbarayp.www.Imprint r7 = (com.santabarbarayp.www.Imprint) r7     // Catch:{ Exception -> 0x00d3 }
            r10.HideBannerCircularPost()     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r1 = r10.mDefaultHomeLocation     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r2 = r10.mDefaultStreetLocation     // Catch:{ Exception -> 0x00d3 }
            if (r2 == 0) goto L_0x0090
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r3 = r10.mDefaultStreetLocation     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x00d3 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r3 = ", "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00d3 }
            java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r1 = r2.toString()     // Catch:{ Exception -> 0x00d3 }
        L_0x0090:
            com.santabarbarayp.www.GeoLocation r0 = new com.santabarbarayp.www.GeoLocation     // Catch:{ Exception -> 0x00d3 }
            com.google.android.maps.GeoPoint r2 = r10.mGeoPoint_DefaultHomeLocation     // Catch:{ Exception -> 0x00d3 }
            int r2 = r2.getLatitudeE6()     // Catch:{ Exception -> 0x00d3 }
            double r2 = (double) r2     // Catch:{ Exception -> 0x00d3 }
            r4 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            double r2 = r2 / r4
            com.google.android.maps.GeoPoint r4 = r10.mGeoPoint_DefaultHomeLocation     // Catch:{ Exception -> 0x00d3 }
            int r4 = r4.getLongitudeE6()     // Catch:{ Exception -> 0x00d3 }
            double r4 = (double) r4     // Catch:{ Exception -> 0x00d3 }
            r8 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            double r4 = r4 / r8
            r0.<init>(r1, r2, r4)     // Catch:{ Exception -> 0x00d3 }
            android.content.Intent r8 = new android.content.Intent     // Catch:{ Exception -> 0x00d3 }
            java.lang.Class<com.santabarbarayp.www.ImprintDetailActivity> r2 = com.santabarbarayp.www.ImprintDetailActivity.class
            r8.<init>(r10, r2)     // Catch:{ Exception -> 0x00d3 }
            android.os.Bundle r6 = r7.toBundle()     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r2 = "ShopLocal_SearchHomeLocation"
            r6.putParcelable(r2, r0)     // Catch:{ Exception -> 0x00d3 }
            r8.putExtras(r6)     // Catch:{ Exception -> 0x00d3 }
            r2 = 9012(0x2334, float:1.2629E-41)
            r10.startActivityForResult(r8, r2)     // Catch:{ Exception -> 0x00d3 }
            r2 = 1
            goto L_0x0014
        L_0x00ca:
            int r2 = r10.mFlingElementListIndex     // Catch:{ Exception -> 0x00d3 }
            if (r2 <= 0) goto L_0x00d7
            int r2 = r10.mFlingElementListIndex     // Catch:{ Exception -> 0x00d3 }
            r10.mSelectedImprintIndex = r2     // Catch:{ Exception -> 0x00d3 }
            goto L_0x0055
        L_0x00d3:
            r2 = move-exception
        L_0x00d4:
            r2 = 0
            goto L_0x0014
        L_0x00d7:
            android.widget.ListView r2 = r10.imprintSearchListView     // Catch:{ Exception -> 0x00d3 }
            int r2 = r2.getFirstVisiblePosition()     // Catch:{ Exception -> 0x00d3 }
            r10.mSelectedImprintIndex = r2     // Catch:{ Exception -> 0x00d3 }
            goto L_0x0055
        L_0x00e1:
            float r2 = r12.getX()     // Catch:{ Exception -> 0x00d3 }
            float r3 = r11.getX()     // Catch:{ Exception -> 0x00d3 }
            float r2 = r2 - r3
            r3 = 1123024896(0x42f00000, float:120.0)
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 <= 0) goto L_0x0148
            float r2 = java.lang.Math.abs(r13)     // Catch:{ Exception -> 0x00d3 }
            r3 = 1128792064(0x43480000, float:200.0)
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 <= 0) goto L_0x0148
            boolean r2 = r10.mIsFlipListViewOption     // Catch:{ Exception -> 0x0175 }
            com.santabarbarayp.www.ShopLocalActivity.mIsFromListView = r2     // Catch:{ Exception -> 0x0175 }
            android.widget.EditText r2 = com.santabarbarayp.www.ShopLocalActivity.mSearchKeyworkEditText     // Catch:{ Exception -> 0x0175 }
            android.widget.EditText r3 = r10.mSearchKeyworkEditText     // Catch:{ Exception -> 0x0175 }
            android.text.Editable r3 = r3.getText()     // Catch:{ Exception -> 0x0175 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0175 }
            r2.setText(r3)     // Catch:{ Exception -> 0x0175 }
            java.lang.String r2 = r10.mSearchKeyword     // Catch:{ Exception -> 0x0175 }
            com.santabarbarayp.www.ShopLocalActivity.mSearchKeyword = r2     // Catch:{ Exception -> 0x0175 }
            android.widget.EditText r2 = com.santabarbarayp.www.ShopLocalActivity.mSearchLocationEditText     // Catch:{ Exception -> 0x0175 }
            java.lang.String r3 = r10.mSearchHomeLocation     // Catch:{ Exception -> 0x0175 }
            r2.setText(r3)     // Catch:{ Exception -> 0x0175 }
            java.lang.String r2 = r10.mSearchHomeLocation     // Catch:{ Exception -> 0x0175 }
            com.santabarbarayp.www.ShopLocalActivity.mSearchHomeLocation = r2     // Catch:{ Exception -> 0x0175 }
            android.widget.TextView r2 = com.santabarbarayp.www.ShopLocalActivity.mSearchLocationTextView     // Catch:{ Exception -> 0x0175 }
            java.lang.String r3 = r10.mSearchLocation     // Catch:{ Exception -> 0x0175 }
            r2.setText(r3)     // Catch:{ Exception -> 0x0175 }
            java.lang.String r2 = r10.mSearchLocation     // Catch:{ Exception -> 0x0175 }
            com.santabarbarayp.www.ShopLocalActivity.mSearchLocation = r2     // Catch:{ Exception -> 0x0175 }
            java.lang.String r2 = r10.mDefaultHomeLocation     // Catch:{ Exception -> 0x0175 }
            com.santabarbarayp.www.ShopLocalActivity.mDefaultHomeLocation = r2     // Catch:{ Exception -> 0x0175 }
            java.lang.String r2 = r10.mDefaultStreetLocation     // Catch:{ Exception -> 0x0175 }
            com.santabarbarayp.www.ShopLocalActivity.mDefaultStreetLocation = r2     // Catch:{ Exception -> 0x0175 }
            com.google.android.maps.GeoPoint r2 = r10.mGeoPoint_DefaultHomeLocation     // Catch:{ Exception -> 0x0175 }
            com.santabarbarayp.www.ShopLocalActivity.mGeoPoint_DefaultHomeLocation = r2     // Catch:{ Exception -> 0x0175 }
            com.google.android.maps.GeoPoint r2 = r10.mGeoPoint_SearchHomeLocation     // Catch:{ Exception -> 0x0175 }
            com.santabarbarayp.www.ShopLocalActivity.mGeoPoint_SearchHomeLocation = r2     // Catch:{ Exception -> 0x0175 }
            com.google.android.maps.GeoPoint r2 = r10.mGeoPoint_SearchLocation     // Catch:{ Exception -> 0x0175 }
            com.santabarbarayp.www.ShopLocalActivity.mGeoPoint_SearchLocation = r2     // Catch:{ Exception -> 0x0175 }
            boolean r2 = r10.mIsCurrentMode     // Catch:{ Exception -> 0x0175 }
            com.santabarbarayp.www.ShopLocalActivity.mIsCurrentMode = r2     // Catch:{ Exception -> 0x0175 }
        L_0x013f:
            r10.HideBannerCircularPost()     // Catch:{ Exception -> 0x00d3 }
            r10.finish()     // Catch:{ Exception -> 0x00d3 }
            r2 = 1
            goto L_0x0014
        L_0x0148:
            float r2 = r11.getY()     // Catch:{ Exception -> 0x00d3 }
            float r3 = r12.getY()     // Catch:{ Exception -> 0x00d3 }
            float r2 = r2 - r3
            r3 = 1123024896(0x42f00000, float:120.0)
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 <= 0) goto L_0x0161
            float r2 = java.lang.Math.abs(r13)     // Catch:{ Exception -> 0x00d3 }
            r3 = 1128792064(0x43480000, float:200.0)
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 > 0) goto L_0x00d4
        L_0x0161:
            float r2 = r12.getY()     // Catch:{ Exception -> 0x00d3 }
            float r3 = r11.getY()     // Catch:{ Exception -> 0x00d3 }
            float r2 = r2 - r3
            r3 = 1123024896(0x42f00000, float:120.0)
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 <= 0) goto L_0x00d4
            java.lang.Math.abs(r13)     // Catch:{ Exception -> 0x00d3 }
            goto L_0x00d4
        L_0x0175:
            r2 = move-exception
            goto L_0x013f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.santabarbarayp.www.ShopLocalMapListActivity.onFling(android.view.MotionEvent, android.view.MotionEvent, float, float):boolean");
    }

    public void onLongPress(MotionEvent e) {
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    public void onShowPress(MotionEvent e) {
    }

    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    public boolean onTouch(View v, MotionEvent event) {
        this.mFlingElementListIndex = 0;
        Object viewTag = v.getTag();
        if (viewTag != null && viewTag.toString().startsWith("p")) {
            this.mFlingElementListIndex = Integer.parseInt(viewTag.toString().substring(1));
        }
        return this.mGestureDetector.onTouchEvent(event);
    }

    /* access modifiers changed from: private */
    public boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService("connectivity");
        if (cm.getActiveNetworkInfo() == null || !cm.getActiveNetworkInfo().isAvailable() || !cm.getActiveNetworkInfo().isConnected()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void ShowBannerCircularPost() {
        if (this.mImprintBannerList != null && this.mImprintBannerList.size() > 1) {
            this.mShopLocalDataHandler.postDelayed(this.mBannerUpdateResults, ShopLocalActivity.BANNER_DELAY_SPAN_MILLS);
        }
    }

    /* access modifiers changed from: private */
    public void HideBannerCircularPost() {
        this.mShopLocalDataHandler.removeCallbacks(this.mBannerUpdateResults);
    }
}
