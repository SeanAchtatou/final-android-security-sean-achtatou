package com.santabarbarayp.www;

import android.view.MotionEvent;

/* compiled from: WrapMotionEvent */
class EclairMotionEvent extends WrapMotionEvent {
    protected EclairMotionEvent(MotionEvent event) {
        super(event);
    }

    public float getX(int pointerIndex) {
        return this.event.getX(pointerIndex);
    }

    public float getY(int pointerIndex) {
        return this.event.getY(pointerIndex);
    }

    public int getPointerCount() {
        return this.event.getPointerCount();
    }

    public int getPointerId(int pointerIndex) {
        return this.event.getPointerId(pointerIndex);
    }
}
