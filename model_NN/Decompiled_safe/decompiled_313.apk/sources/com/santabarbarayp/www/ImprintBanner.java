package com.santabarbarayp.www;

/* compiled from: ImprintBannerList */
class ImprintBanner {
    private double _height = Double.NaN;
    private String _image_url;
    private String _name;
    private double _width = Double.NaN;

    public ImprintBanner() {
    }

    public ImprintBanner(String name, String imageUrl, double width, double height) {
        this._name = name;
        this._width = width;
        this._height = height;
        this._image_url = imageUrl;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String name) {
        this._name = name;
    }

    public String getImageUrl() {
        return this._image_url;
    }

    public void setImageUrl(String imageurl) {
        this._image_url = imageurl;
    }

    public double getWidth() {
        return this._width;
    }

    public void setWidth(double width) {
        this._width = width;
    }

    public double getHeight() {
        return this._height;
    }

    public void setHeight(double height) {
        this._height = height;
    }
}
