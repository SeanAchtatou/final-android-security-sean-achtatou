package com.santabarbarayp.www;

import android.os.Bundle;

public class Imprint {
    private String _ad_imag_url;
    private String _ad_imag_url2 = null;
    private String _ad_sentence;
    private String _city_state_zip;
    private String _coupon_image_url;
    private double _distance = -1.0d;
    private ImprintElementList _element_list;
    private String _email_address;
    private int _geoLocation_index = -1;
    private boolean _has_check_book;
    private boolean _has_coupon;
    private boolean _has_credit_card;
    private String _header_name;
    private String _image_logo;
    private boolean _is_website;
    private double _lat = Double.NaN;
    private double _lng = Double.NaN;
    private String _menu;
    private String _name;
    private String _phone;
    private int _premium_level;
    private String _street;
    private String _video_url;
    private String _website_url;

    public Imprint() {
    }

    public Imprint(String name, String headername, String imagelogo, double distance, int premiumlevel, String street, String citystatezip, String phone, double latitude, double longitude, boolean iswebsite, boolean hascoupon, boolean hascreditcard, boolean hascheckbook, String couponimageurl, String adsentence, String adimagurl, String adimagurl2, String websiteurl, String emailaddress, String menuurl, String videourl, int geoLocationindex, ImprintElementList elementlist) {
        this._name = name;
        this._header_name = headername;
        this._image_logo = imagelogo;
        this._distance = distance;
        this._premium_level = premiumlevel;
        this._street = street;
        this._city_state_zip = citystatezip;
        this._phone = phone;
        this._lat = latitude;
        this._lng = longitude;
        this._is_website = iswebsite;
        this._has_coupon = hascoupon;
        this._has_credit_card = hascreditcard;
        this._has_check_book = hascheckbook;
        this._coupon_image_url = couponimageurl;
        this._ad_sentence = adsentence;
        this._ad_imag_url = adimagurl;
        this._ad_imag_url2 = adimagurl2;
        this._website_url = websiteurl;
        this._email_address = emailaddress;
        this._menu = menuurl;
        this._video_url = videourl;
        this._geoLocation_index = geoLocationindex;
        this._element_list = elementlist;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String name) {
        this._name = name;
    }

    public String getHeaderName() {
        return this._header_name;
    }

    public void setHeaderName(String headername) {
        this._header_name = headername;
    }

    public String getImageLogo() {
        return this._image_logo;
    }

    public void setImageLogo(String imagelogo) {
        this._image_logo = imagelogo;
    }

    public double getDistance() {
        return this._distance;
    }

    public void setDistance(double distance) {
        this._distance = distance;
    }

    public double getLat() {
        return this._lat;
    }

    public void setLat(double lat) {
        this._lat = lat;
    }

    public double getLng() {
        return this._lng;
    }

    public void setLng(double lng) {
        this._lng = lng;
    }

    public void setPremiumLevel(int premiumlevel) {
        this._premium_level = premiumlevel;
    }

    public int getPremiumLevel() {
        return this._premium_level;
    }

    public void setStreet(String street) {
        this._street = street;
    }

    public String getStreet() {
        return this._street;
    }

    public void setCityStateZip(String citystatezip) {
        this._city_state_zip = citystatezip;
    }

    public String getCityStateZip() {
        return this._city_state_zip;
    }

    public void setPhone(String phone) {
        this._phone = phone;
    }

    public String getPhone() {
        return this._phone;
    }

    public void setIsWebsite(boolean iswebsite) {
        this._is_website = iswebsite;
    }

    public boolean isWebsite() {
        return this._is_website;
    }

    public void setHasCoupon(boolean hascoupon) {
        this._has_coupon = hascoupon;
    }

    public boolean hasCoupon() {
        return this._has_coupon;
    }

    public void setHasCreditCard(boolean hascreditcard) {
        this._has_credit_card = hascreditcard;
    }

    public boolean hasCreditCard() {
        return this._has_credit_card;
    }

    public void setHasCheckBook(boolean hascheckbook) {
        this._has_check_book = hascheckbook;
    }

    public boolean hasCheckBook() {
        return this._has_check_book;
    }

    public void setCouponImageUrl(String couponimageurl) {
        this._coupon_image_url = couponimageurl;
    }

    public String getCouponImageUrl() {
        return this._coupon_image_url;
    }

    public void setADSentence(String adsentence) {
        this._ad_sentence = adsentence;
    }

    public String getADSentence() {
        return this._ad_sentence;
    }

    public void setADImageUrl(String adimagurl) {
        this._ad_imag_url = adimagurl;
    }

    public void setADImageUrl2(String adimagurl) {
        this._ad_imag_url2 = adimagurl;
    }

    public String getADImageUrl() {
        return this._ad_imag_url;
    }

    public String getADImageUrl2() {
        return this._ad_imag_url2;
    }

    public void setWebSiteUrl(String websiteurl) {
        this._website_url = websiteurl;
    }

    public String getWebSiteUrl() {
        return this._website_url;
    }

    public void setEmailAddress(String emailaddress) {
        this._email_address = emailaddress;
    }

    public String getEmailAddress() {
        return this._email_address;
    }

    public void setMenu(String menuUrl) {
        this._menu = menuUrl;
    }

    public String getMenu() {
        return this._menu;
    }

    public void setVideoUrl(String videoUrl) {
        this._video_url = videoUrl;
    }

    public String getVideoUrl() {
        return this._video_url;
    }

    public void setGeoLocationIndex(int geoLocationindex) {
        this._geoLocation_index = geoLocationindex;
    }

    public int getGeoLocationIndex() {
        return this._geoLocation_index;
    }

    public void setElementList(ImprintElementList elementlist) {
        this._element_list = elementlist;
    }

    public ImprintElementList getElementList() {
        return this._element_list;
    }

    public Bundle toBundle() {
        Bundle b = new Bundle();
        b.putString("imprintName", this._name);
        b.putString("imprintHeaderName", this._header_name);
        b.putString("imprintLogo", this._image_logo);
        b.putDouble("imprintDistance", this._distance);
        b.putInt("imprintPremiumlevel", this._premium_level);
        b.putDouble("imprintLatitude", this._lat);
        b.putDouble("imprintLongitude", this._lng);
        b.putString("imprintStreet", this._street);
        b.putString("imprintCity", this._city_state_zip);
        b.putString("imprintPhone", this._phone);
        b.putBoolean("imprintIsWebsite", this._is_website);
        b.putBoolean("imprintHasCoupon", this._has_coupon);
        b.putBoolean("imprintHasCredit", this._has_credit_card);
        b.putBoolean("imprintHasCheck", this._has_check_book);
        b.putString("imprintCoupon", this._coupon_image_url);
        b.putString("imprintAdDescrip", this._ad_sentence);
        b.putString("imprintAdUrl", this._ad_imag_url);
        b.putString("imprintAdUrl2", this._ad_imag_url2);
        b.putString("imprintWebsiteUrl", this._website_url);
        b.putString("imprintEmailAddress", this._email_address);
        b.putString("imprintMenu", this._menu);
        b.putString("imprintVideoUrl", this._video_url);
        b.putInt("imprintGeoIndex", this._geoLocation_index);
        b.putParcelable("imprintElementList", this._element_list);
        return b;
    }

    public static Imprint fromBundle(Bundle b) {
        return new Imprint(b.getString("imprintName"), b.getString("imprintHeaderName"), b.getString("imprintLogo"), b.getDouble("imprintDistance"), b.getInt("imprintPremiumlevel"), b.getString("imprintStreet"), b.getString("imprintCity"), b.getString("imprintPhone"), b.getDouble("imprintLatitude"), b.getDouble("imprintLongitude"), b.getBoolean("imprintIsWebsite"), b.getBoolean("imprintHasCoupon"), b.getBoolean("imprintHasCredit"), b.getBoolean("imprintHasCheck"), b.getString("imprintCoupon"), b.getString("imprintAdDescrip"), b.getString("imprintAdUrl"), b.getString("imprintAdUrl2"), b.getString("imprintWebsiteUrl"), b.getString("imprintEmailAddress"), b.getString("imprintMenu"), b.getString("imprintVideoUrl"), b.getInt("imprintGeoIndex"), (ImprintElementList) b.getParcelable("imprintElementList"));
    }
}
