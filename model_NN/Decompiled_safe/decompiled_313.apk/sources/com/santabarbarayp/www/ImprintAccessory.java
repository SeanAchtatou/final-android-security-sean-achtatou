package com.santabarbarayp.www;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Environment;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.http.util.ByteArrayBuffer;

public class ImprintAccessory {
    private String _accssoryUrl;
    private String _accssoryUrl2;
    private String _backgroundImageName;
    private String _drawableImageName;
    private Bitmap _imageBitmap = null;
    private String _imagePhysicalPath = null;
    private String _subTitle;
    private String _title;

    public ImprintAccessory() {
    }

    public ImprintAccessory(String title, String subTitle, String drawableImageName, String backgroundImageName, String accssoryUrl) {
        this._title = title;
        this._subTitle = subTitle;
        this._drawableImageName = drawableImageName;
        this._backgroundImageName = backgroundImageName;
        this._accssoryUrl = accssoryUrl;
        this._imageBitmap = null;
        this._imagePhysicalPath = null;
    }

    public ImprintAccessory(String title, String subTitle, String drawableImageName, String backgroundImageName, String accssoryUrl, String accssoryUrl2) {
        this._title = title;
        this._subTitle = subTitle;
        this._drawableImageName = drawableImageName;
        this._backgroundImageName = backgroundImageName;
        this._accssoryUrl = accssoryUrl;
        this._accssoryUrl2 = accssoryUrl2;
        this._imageBitmap = null;
        this._imagePhysicalPath = null;
    }

    public void setTitle(String title) {
        this._title = title;
    }

    public String getTitle() {
        return this._title;
    }

    public void setSubTitle(String subTitle) {
        this._subTitle = subTitle;
    }

    public String getSubTitle() {
        return this._subTitle;
    }

    public void setDrawableImageName(String drawableImageName) {
        this._drawableImageName = drawableImageName;
    }

    public String getDrawableImageName() {
        return this._drawableImageName;
    }

    public void setBackgroundImageName(String backgroundImageName) {
        this._backgroundImageName = backgroundImageName;
    }

    public String getBackgroundImageName() {
        return this._backgroundImageName;
    }

    public void setAccssoryUrl(String accssoryUrl) {
        this._accssoryUrl = accssoryUrl;
    }

    public String getAccssoryUrl() {
        return this._accssoryUrl;
    }

    public void setAccssoryUrl2(String accssoryUrl) {
        this._accssoryUrl2 = accssoryUrl;
    }

    public String getAccssoryUrl2() {
        return this._accssoryUrl2;
    }

    public void setBitmap(Bitmap imageBitmap) {
        this._imageBitmap = imageBitmap;
    }

    public Bitmap getBitmap() {
        return this._imageBitmap;
    }

    public void setPhysicalPath(String imagePhysicalPath) {
        this._imagePhysicalPath = imagePhysicalPath;
    }

    public String getPhysicalPath() {
        return this._imagePhysicalPath;
    }

    public Bundle toBundle() {
        Bundle b = new Bundle();
        b.putString("imprintAccessoryTitle", this._title);
        b.putString("imprintAccessorySubTitle", this._subTitle);
        b.putString("imprintAccessoryImage", this._drawableImageName);
        b.putString("imprintAccessoryBackgroundImage", this._backgroundImageName);
        b.putString("imprintAccessoryUrl", this._accssoryUrl);
        b.putString("imprintAccessoryUrl2", this._accssoryUrl2);
        b.putString("imprintAccessoryImagePhysicalPath", this._imagePhysicalPath);
        return b;
    }

    public static ImprintAccessory fromBundle(Bundle b) {
        ImprintAccessory myImprintAccessory = new ImprintAccessory(b.getString("imprintAccessoryTitle"), b.getString("imprintAccessorySubTitle"), b.getString("imprintAccessoryImage"), b.getString("imprintAccessoryBackgroundImage"), b.getString("imprintAccessoryUrl"), b.getString("imprintAccessoryUrl2"));
        myImprintAccessory.setPhysicalPath(b.getString("imprintAccessoryImagePhysicalPath"));
        return myImprintAccessory;
    }

    /* JADX INFO: Multiple debug info for r6v7 java.net.HttpURLConnection: [D('fileUrl' java.lang.String), D('conn' java.net.HttpURLConnection)] */
    /* JADX INFO: Multiple debug info for r6v9 java.net.HttpURLConnection: [D('conn2' java.net.HttpURLConnection), D('conn' java.net.HttpURLConnection)] */
    /* JADX INFO: Multiple debug info for r6v10 java.io.InputStream: [D('conn2' java.net.HttpURLConnection), D('is' java.io.InputStream)] */
    /* JADX INFO: Multiple debug info for r7v6 java.io.FileOutputStream: [D('fos' java.io.FileOutputStream), D('bis' java.io.BufferedInputStream)] */
    public static Bitmap DownloadImage(String fileUrl, String fileName) {
        if (fileUrl == null || fileUrl.length() == 0) {
            return null;
        }
        try {
            new File(Environment.getExternalStorageDirectory() + "/shoplocal/download").mkdirs();
            File tempFile = new File(Environment.getExternalStorageDirectory() + "/shoplocal/download", fileName);
            tempFile.createNewFile();
            URL myFileUrl = new URL(fileUrl);
            try {
                HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
                conn.setDoInput(true);
                conn.connect();
                InputStream in = conn.getInputStream();
                Bitmap bitmap = BitmapFactory.decodeStream(in);
                in.close();
                HttpURLConnection conn2 = (HttpURLConnection) myFileUrl.openConnection();
                conn2.setDoInput(true);
                conn2.connect();
                BufferedInputStream bis = new BufferedInputStream(conn2.getInputStream());
                ByteArrayBuffer baf = new ByteArrayBuffer(1024);
                while (true) {
                    int current = bis.read();
                    if (current == -1) {
                        FileOutputStream fos = new FileOutputStream(tempFile);
                        fos.write(baf.toByteArray());
                        fos.close();
                        InputStream inputStream = in;
                        Bitmap bitmap2 = bitmap;
                        URL url = myFileUrl;
                        return bitmap2;
                    }
                    baf.append((byte) current);
                }
            } catch (Exception e) {
                return null;
            }
        } catch (Exception e2) {
            return null;
        }
    }

    /* JADX INFO: Multiple debug info for r20v7 java.net.HttpURLConnection: [D('fileUrl' java.lang.String), D('conn1' java.net.HttpURLConnection)] */
    /* JADX INFO: Multiple debug info for r20v10 android.graphics.Bitmap: [D('conn1' java.net.HttpURLConnection), D('bitmap1' android.graphics.Bitmap), D('fileUrl' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r21v9 java.net.HttpURLConnection: [D('conn2' java.net.HttpURLConnection), D('fileUrl2' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r21v12 android.graphics.Bitmap: [D('conn2' java.net.HttpURLConnection), D('bitmap2' android.graphics.Bitmap)] */
    /* JADX INFO: Multiple debug info for r6v10 java.io.FileOutputStream: [D('canvas' android.graphics.Canvas), D('fos' java.io.FileOutputStream)] */
    public static Bitmap DownloadImage(String fileUrl, String fileUrl2, String fileName) {
        if (fileUrl == null || fileUrl.length() == 0) {
            return null;
        }
        try {
            new File(Environment.getExternalStorageDirectory() + "/shoplocal/download").mkdirs();
            File file = new File(Environment.getExternalStorageDirectory() + "/shoplocal/download", fileName);
            file.createNewFile();
            URL url = new URL(fileUrl);
            try {
                HttpURLConnection conn1 = (HttpURLConnection) url.openConnection();
                conn1.setDoInput(true);
                conn1.connect();
                InputStream in1 = conn1.getInputStream();
                try {
                    Bitmap bitmap1 = BitmapFactory.decodeStream(in1);
                    try {
                        in1.close();
                        int imageWidth1 = bitmap1.getWidth();
                        int imageHeight = bitmap1.getHeight();
                        URL url2 = new URL(fileUrl2);
                        try {
                            HttpURLConnection conn2 = (HttpURLConnection) url2.openConnection();
                            conn2.setDoInput(true);
                            conn2.connect();
                            InputStream in2 = conn2.getInputStream();
                            try {
                                Bitmap bitmap2 = BitmapFactory.decodeStream(in2);
                                try {
                                    in2.close();
                                    int imageWidth2 = bitmap2.getWidth();
                                    Bitmap bitmapMerge = Bitmap.createBitmap(((imageWidth1 + imageWidth2) / 2) + 2, imageHeight / 2, Bitmap.Config.ARGB_8888);
                                    Canvas canvas = new Canvas(bitmapMerge);
                                    RectF mDetailRectRange = new RectF();
                                    mDetailRectRange.set(0.0f, 0.0f, (float) (imageWidth1 / 2), (float) (imageHeight / 2));
                                    canvas.drawBitmap(bitmap1, (Rect) null, mDetailRectRange, (Paint) null);
                                    mDetailRectRange.set((float) ((imageWidth1 / 2) + 2), 0.0f, (float) (((imageWidth1 + imageWidth2) / 2) + 2), (float) (imageHeight / 2));
                                    canvas.drawBitmap(bitmap2, (Rect) null, mDetailRectRange, (Paint) null);
                                    FileOutputStream fos = new FileOutputStream(file);
                                    if (fileName.endsWith(".jpg")) {
                                        bitmapMerge.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                                    } else {
                                        bitmapMerge.compress(Bitmap.CompressFormat.PNG, 100, fos);
                                    }
                                    fos.close();
                                    URL url3 = url2;
                                    URL url4 = url;
                                    InputStream inputStream = in2;
                                    InputStream inputStream2 = in1;
                                    return bitmap1;
                                } catch (Exception e) {
                                    return bitmap1;
                                }
                            } catch (Exception e2) {
                                return bitmap1;
                            }
                        } catch (Exception e3) {
                            return bitmap1;
                        }
                    } catch (Exception e4) {
                        return bitmap1;
                    }
                } catch (Exception e5) {
                    return null;
                }
            } catch (Exception e6) {
                return null;
            }
        } catch (Exception e7) {
            return null;
        }
    }
}
