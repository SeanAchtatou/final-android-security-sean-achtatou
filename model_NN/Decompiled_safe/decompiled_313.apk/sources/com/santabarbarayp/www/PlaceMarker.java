package com.santabarbarayp.www;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

public class PlaceMarker extends OverlayItem {
    private String _cityAddress;
    private char _drawableChar;
    private int _imprintIndex;

    public PlaceMarker(GeoPoint geoPoint, String title, String snippet) {
        super(geoPoint, title, snippet);
        this._imprintIndex = -1;
    }

    public PlaceMarker(double latitude, double longitude, String title, String snippet) {
        this(new GeoPoint((int) (latitude * 1000000.0d), (int) (longitude * 1000000.0d)), title, snippet);
    }

    public void setCityAddress(String cityAddress) {
        this._cityAddress = cityAddress;
    }

    public String getCityAddress() {
        return this._cityAddress;
    }

    public void setImprintIndex(int imprintIndex) {
        this._imprintIndex = imprintIndex;
    }

    public int getImprintIndex() {
        return this._imprintIndex;
    }

    public void setDrawableChar(char drawableChar) {
        this._drawableChar = drawableChar;
    }

    public char getDrawableChar() {
        return this._drawableChar;
    }
}
