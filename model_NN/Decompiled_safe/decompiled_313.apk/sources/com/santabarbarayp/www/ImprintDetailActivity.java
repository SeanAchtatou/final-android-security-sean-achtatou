package com.santabarbarayp.www;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentProviderOperation;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;

public class ImprintDetailActivity extends Activity implements GestureDetector.OnGestureListener, View.OnTouchListener {
    public static final int MenuAdvertisement = 6;
    public static final int MenuCoupon = 4;
    public static final int MenuEmail = 2;
    public static final int MenuMenu = 5;
    public static final int MenuVideo = 7;
    public static final int MenuWeb = 3;
    /* access modifiers changed from: private */
    public GeoLocation ls_SearchHomeLocation;
    private ImageButton mADButton;
    /* access modifiers changed from: private */
    public ArrayList<ImprintAccessory> mAccessoryList;
    /* access modifiers changed from: private */
    public Runnable mBitmapUpdateTask = new Runnable() {
        public void run() {
            Intent intent = new Intent(ImprintDetailActivity.this, ImprintImageViewActivity.class);
            Bundle b = ImprintDetailActivity.this.mImprintAccessory.toBundle();
            b.putBoolean("IsFromMapListView", false);
            intent.putExtras(b);
            ImprintDetailActivity.this.startActivity(intent);
            ImprintDetailActivity.this.mProgressbar.setVisibility(8);
        }
    };
    private ImageButton mCouponButton;
    private ImageButton mEmailButton;
    /* access modifiers changed from: private */
    public ImageView mExpandCollapseImage;
    private int mFlingElementListIndex = 0;
    private GestureDetector mGestureDetector;
    /* access modifiers changed from: private */
    public Runnable mImageLogoUpdateTask = new Runnable() {
        public void run() {
            ViewGroup.LayoutParams logoViewPa = ImprintDetailActivity.this.mImprintLogoImage.getLayoutParams();
            if (ImprintDetailActivity.this.mImprintLogo != null) {
                ImprintDetailActivity.this.mImprintLogoImage.setImageBitmap(ImprintDetailActivity.this.mImprintLogo);
                logoViewPa.height = -2;
            } else {
                logoViewPa.height = 0;
            }
            ImprintDetailActivity.this.mImprintLogoImage.setLayoutParams(logoViewPa);
            ImprintDetailActivity.this.mImprintElementListView.setAdapter((ListAdapter) new ImprintElementListAdapter(ImprintDetailActivity.this.mShowListNumber, ImprintDetailActivity.this.myImprint.getElementList()));
            ImprintDetailActivity.this.mImprintElementListView.setOnTouchListener(ImprintDetailActivity.this);
            ImprintDetailActivity.this.mProgressbar.setVisibility(8);
        }
    };
    /* access modifiers changed from: private */
    public ImprintAccessory mImprintAccessory = null;
    /* access modifiers changed from: private */
    public ImprintElement mImprintElement = null;
    /* access modifiers changed from: private */
    public ListView mImprintElementListView;
    /* access modifiers changed from: private */
    public Bitmap mImprintLogo = null;
    /* access modifiers changed from: private */
    public ImageView mImprintLogoImage;
    private TextView mImprintTitle;
    private Button mImprintWebUrlButton;
    private ImageButton mMenuButton;
    private RelativeLayout mMultipleLocationLayout;
    private TextView mMultipleLocationText;
    ProgressBar mProgressbar;
    /* access modifiers changed from: private */
    public Runnable mSendingEmailUpdateTask = new Runnable() {
        public void run() {
            Intent sendIntent = new Intent("android.intent.action.SEND");
            sendIntent.putExtra("android.intent.extra.EMAIL", new String[0]);
            sendIntent.putExtra("android.intent.extra.CC", new String[0]);
            sendIntent.putExtra("android.intent.extra.SUBJECT", ImprintDetailActivity.this.mImprintElement.getName());
            String bodyText = ImprintDetailActivity.this.mImprintElement.getName();
            String addressDetail = ImprintDetailActivity.this.mImprintElement.getStreet();
            if (addressDetail == null) {
                addressDetail = "";
            }
            String addressDetail2 = addressDetail.trim();
            String cityAddress = ImprintDetailActivity.this.mImprintElement.getCityStateZip();
            if (cityAddress == null) {
                cityAddress = "";
            }
            if (addressDetail2.length() == 0) {
                addressDetail2 = cityAddress;
            } else if (cityAddress.trim().length() > 0) {
                addressDetail2 = String.valueOf(addressDetail2) + ", " + cityAddress;
            }
            if (addressDetail2.trim().length() > 0) {
                bodyText = String.valueOf(bodyText) + "\n" + addressDetail2;
            }
            ArrayList<String> phoneList = ImprintDetailActivity.this.mImprintElement.getPhoneList();
            if (phoneList != null) {
                for (int k = 0; k < phoneList.size(); k++) {
                    bodyText = String.valueOf(bodyText) + "\n" + phoneList.get(k);
                }
            }
            if (ImprintDetailActivity.this.myImprint.getEmailAddress() != null) {
                bodyText = String.valueOf(bodyText) + "\n" + ImprintDetailActivity.this.myImprint.getEmailAddress();
            }
            String bodyText2 = String.valueOf(bodyText) + "\n\n";
            if (ImprintDetailActivity.this.myImprint.isWebsite()) {
                bodyText2 = String.valueOf(bodyText2) + "\n" + ImprintDetailActivity.this.myImprint.getWebSiteUrl() + "\n\n";
            }
            sendIntent.putExtra("android.intent.extra.TEXT", bodyText2);
            sendIntent.putExtra("sms_body", bodyText2);
            String mimieType = "text/plain";
            String bitmapPhysicalPath = null;
            if (ImprintDetailActivity.this.mImprintAccessory != null) {
                bitmapPhysicalPath = ImprintDetailActivity.this.mImprintAccessory.getPhysicalPath();
            }
            if (bitmapPhysicalPath != null && bitmapPhysicalPath.length() > 0) {
                mimieType = "image/*";
                sendIntent.putExtra("android.intent.extra.STREAM", Uri.parse("file://" + bitmapPhysicalPath));
            }
            sendIntent.setType(mimieType);
            ImprintDetailActivity.this.startActivity(Intent.createChooser(sendIntent, "Share Business Profile"));
            ImprintDetailActivity.this.mProgressbar.setVisibility(8);
        }
    };
    /* access modifiers changed from: private */
    public Handler mShopLocalDataHandler = new Handler();
    LayoutInflater mShopLocalInflater;
    private RelativeLayout mShowAllLayout;
    /* access modifiers changed from: private */
    public int mShowListNumber = 1;
    private ImageButton mVideoButton;
    private ImageButton mWebButton;
    /* access modifiers changed from: private */
    public Imprint myImprint;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getResources().getConfiguration().orientation == 2) {
            getWindow().setFlags(1024, 1024);
        } else {
            getWindow().clearFlags(1024);
        }
        setContentView(R.layout.imprint_element_list);
        this.mShopLocalInflater = (LayoutInflater) getSystemService("layout_inflater");
        addContentView(this.mShopLocalInflater.inflate((int) R.layout.progressbar_spin_center, (ViewGroup) null), new RelativeLayout.LayoutParams(-1, -1));
        this.mProgressbar = (ProgressBar) findViewById(R.id.progressbar_large);
        this.mGestureDetector = new GestureDetector(this);
        Bundle extras = getIntent().getExtras();
        this.myImprint = Imprint.fromBundle(extras);
        this.ls_SearchHomeLocation = (GeoLocation) extras.getParcelable("ShopLocal_SearchHomeLocation");
        this.mImprintTitle = (TextView) findViewById(R.id.imprint_element_title);
        this.mImprintWebUrlButton = (Button) findViewById(R.id.imprint_weburl_button);
        this.mADButton = (ImageButton) findViewById(R.id.ad_button);
        this.mCouponButton = (ImageButton) findViewById(R.id.coupon_button);
        this.mEmailButton = (ImageButton) findViewById(R.id.email_button);
        this.mMenuButton = (ImageButton) findViewById(R.id.menu_button);
        this.mVideoButton = (ImageButton) findViewById(R.id.video_button);
        this.mWebButton = (ImageButton) findViewById(R.id.web_button);
        this.mMultipleLocationLayout = (RelativeLayout) findViewById(R.id.multiple_location_layout);
        this.mMultipleLocationText = (TextView) findViewById(R.id.multiple_locations);
        this.mShowAllLayout = (RelativeLayout) findViewById(R.id.show_all_layout);
        this.mExpandCollapseImage = (ImageView) findViewById(R.id.expand_collapse_button);
        this.mImprintLogoImage = (ImageView) findViewById(R.id.imprint_logo_view);
        this.mImprintElementListView = (ListView) findViewById(R.id.imprintElement_listView);
        this.mAccessoryList = createAccesoryList();
        this.mImprintTitle.setText(this.myImprint.getName());
        ViewGroup.LayoutParams adPa = this.mADButton.getLayoutParams();
        this.mADButton.setOnTouchListener(this);
        String imprintADUrl = this.myImprint.getADImageUrl();
        if (imprintADUrl == null || imprintADUrl.length() <= 0) {
            adPa.height = 0;
            adPa.width = 0;
        } else {
            adPa.height = -2;
            adPa.width = -2;
            this.mADButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ImprintAccessory bImprintAccessory = new ImprintAccessory();
                    for (int i = 0; i < ImprintDetailActivity.this.mAccessoryList.size(); i++) {
                        bImprintAccessory = (ImprintAccessory) ImprintDetailActivity.this.mAccessoryList.get(i);
                        if (bImprintAccessory.getTitle().equalsIgnoreCase("Advertisement")) {
                            break;
                        }
                    }
                    ImprintDetailActivity.this.mImprintAccessory = bImprintAccessory;
                    if (ImprintDetailActivity.this.mImprintAccessory.getBitmap() == null) {
                        ImprintDetailActivity.this.mProgressbar.setVisibility(0);
                        final String finalImageUrl = ImprintDetailActivity.this.mImprintAccessory.getAccssoryUrl();
                        final String finalImageUrl2 = ImprintDetailActivity.this.mImprintAccessory.getAccssoryUrl2();
                        String imageFileName = "advertisement.png";
                        if (finalImageUrl.toLowerCase().endsWith(".jpg")) {
                            imageFileName = "advertisement.jpg";
                        } else if (finalImageUrl.toLowerCase().endsWith(".gif")) {
                            if (finalImageUrl2 == null || finalImageUrl2.length() == 0) {
                                imageFileName = "advertisement.gif";
                            } else {
                                imageFileName = "advertisement.png";
                            }
                        }
                        final String finalImageFileName = imageFileName;
                        ImprintDetailActivity.this.mImprintAccessory.setPhysicalPath(Environment.getExternalStorageDirectory() + "/shoplocal/download/" + finalImageFileName);
                        new Thread() {
                            public void run() {
                                try {
                                    if (finalImageUrl2 == null || finalImageUrl2.length() == 0) {
                                        ImprintDetailActivity.this.mImprintAccessory.setBitmap(ImprintAccessory.DownloadImage(finalImageUrl, finalImageFileName));
                                    } else {
                                        ImprintDetailActivity.this.mImprintAccessory.setBitmap(ImprintAccessory.DownloadImage(finalImageUrl, finalImageUrl2, finalImageFileName));
                                    }
                                    ImprintDetailActivity.this.mShopLocalDataHandler.post(ImprintDetailActivity.this.mBitmapUpdateTask);
                                } catch (Exception e) {
                                }
                            }
                        }.start();
                        return;
                    }
                    ImprintDetailActivity.this.mShopLocalDataHandler.post(ImprintDetailActivity.this.mBitmapUpdateTask);
                }
            });
        }
        this.mADButton.setLayoutParams(adPa);
        ViewGroup.LayoutParams couponPa = this.mCouponButton.getLayoutParams();
        this.mCouponButton.setOnTouchListener(this);
        String imprintCouponUrl = this.myImprint.getCouponImageUrl();
        if (imprintCouponUrl == null || imprintCouponUrl.length() <= 0) {
            couponPa.height = 0;
            couponPa.width = 0;
        } else {
            couponPa.height = -2;
            couponPa.width = -2;
            this.mCouponButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ImprintAccessory bImprintAccessory = new ImprintAccessory();
                    for (int i = 0; i < ImprintDetailActivity.this.mAccessoryList.size(); i++) {
                        bImprintAccessory = (ImprintAccessory) ImprintDetailActivity.this.mAccessoryList.get(i);
                        if (bImprintAccessory.getTitle().equalsIgnoreCase("Coupon")) {
                            break;
                        }
                    }
                    ImprintDetailActivity.this.mImprintAccessory = bImprintAccessory;
                    if (ImprintDetailActivity.this.mImprintAccessory.getBitmap() == null) {
                        ImprintDetailActivity.this.mProgressbar.setVisibility(0);
                        final String finalImageUrl = ImprintDetailActivity.this.mImprintAccessory.getAccssoryUrl();
                        String imageFileName = "coupon.gif";
                        if (finalImageUrl.toLowerCase().endsWith(".jpg")) {
                            imageFileName = "coupon.jpg";
                        } else if (finalImageUrl.toLowerCase().endsWith(".png")) {
                            imageFileName = "coupon.png";
                        }
                        final String finalImageFileName = imageFileName;
                        ImprintDetailActivity.this.mImprintAccessory.setPhysicalPath(Environment.getExternalStorageDirectory() + "/shoplocal/download/" + finalImageFileName);
                        new Thread() {
                            public void run() {
                                try {
                                    ImprintDetailActivity.this.mImprintAccessory.setBitmap(ImprintAccessory.DownloadImage(finalImageUrl, finalImageFileName));
                                    ImprintDetailActivity.this.mShopLocalDataHandler.post(ImprintDetailActivity.this.mBitmapUpdateTask);
                                } catch (Exception e) {
                                }
                            }
                        }.start();
                        return;
                    }
                    ImprintDetailActivity.this.mShopLocalDataHandler.post(ImprintDetailActivity.this.mBitmapUpdateTask);
                }
            });
        }
        this.mCouponButton.setLayoutParams(couponPa);
        ViewGroup.LayoutParams emailPa = this.mEmailButton.getLayoutParams();
        this.mEmailButton.setOnTouchListener(this);
        String imprintEmailAddress = this.myImprint.getEmailAddress();
        if (imprintEmailAddress == null || imprintEmailAddress.length() <= 0) {
            emailPa.height = 0;
            emailPa.width = 0;
        } else {
            emailPa.height = -2;
            emailPa.width = -2;
            this.mEmailButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent sendIntent = new Intent("android.intent.action.SEND");
                    sendIntent.putExtra("android.intent.extra.EMAIL", new String[]{ImprintDetailActivity.this.myImprint.getEmailAddress()});
                    sendIntent.setType("text/html");
                    ImprintDetailActivity.this.startActivity(Intent.createChooser(sendIntent, "Send Email "));
                }
            });
        }
        this.mEmailButton.setLayoutParams(emailPa);
        ViewGroup.LayoutParams menuPa = this.mMenuButton.getLayoutParams();
        this.mMenuButton.setOnTouchListener(this);
        String imprintMenuUrl = this.myImprint.getMenu();
        if (imprintMenuUrl == null || imprintMenuUrl.length() <= 0) {
            menuPa.height = 0;
            menuPa.width = 0;
        } else {
            menuPa.height = -2;
            menuPa.width = -2;
            this.mMenuButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ImprintAccessory bImprintAccessory = new ImprintAccessory();
                    for (int i = 0; i < ImprintDetailActivity.this.mAccessoryList.size(); i++) {
                        bImprintAccessory = (ImprintAccessory) ImprintDetailActivity.this.mAccessoryList.get(i);
                        if (bImprintAccessory.getTitle().equalsIgnoreCase("Menu")) {
                            break;
                        }
                    }
                    ImprintDetailActivity.this.mImprintAccessory = bImprintAccessory;
                    if (ImprintDetailActivity.this.mImprintAccessory.getBitmap() == null) {
                        ImprintDetailActivity.this.mProgressbar.setVisibility(0);
                        final String finalImageUrl = ImprintDetailActivity.this.mImprintAccessory.getAccssoryUrl();
                        String imageFileName = "menu.gif";
                        if (finalImageUrl.toLowerCase().endsWith(".jpg")) {
                            imageFileName = "menu.jpg";
                        } else if (finalImageUrl.toLowerCase().endsWith(".png")) {
                            imageFileName = "menu.png";
                        }
                        final String finalImageFileName = imageFileName;
                        ImprintDetailActivity.this.mImprintAccessory.setPhysicalPath(Environment.getExternalStorageDirectory() + "/shoplocal/download/" + imageFileName);
                        new Thread() {
                            public void run() {
                                try {
                                    ImprintDetailActivity.this.mImprintAccessory.setBitmap(ImprintAccessory.DownloadImage(finalImageUrl, finalImageFileName));
                                    ImprintDetailActivity.this.mShopLocalDataHandler.post(ImprintDetailActivity.this.mBitmapUpdateTask);
                                } catch (Exception e) {
                                }
                            }
                        }.start();
                        return;
                    }
                    ImprintDetailActivity.this.mShopLocalDataHandler.post(ImprintDetailActivity.this.mBitmapUpdateTask);
                }
            });
        }
        this.mMenuButton.setLayoutParams(menuPa);
        ViewGroup.LayoutParams videoPa = this.mVideoButton.getLayoutParams();
        this.mVideoButton.setOnTouchListener(this);
        String imprintVideoUrl = this.myImprint.getVideoUrl();
        if (imprintVideoUrl == null || imprintVideoUrl.length() <= 0) {
            videoPa.height = 0;
            videoPa.width = 0;
        } else {
            videoPa.height = -2;
            videoPa.width = -2;
            final String str = imprintVideoUrl;
            this.mVideoButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent tostart = new Intent("android.intent.action.VIEW");
                    tostart.setDataAndType(Uri.parse(str), "video/*");
                    ImprintDetailActivity.this.startActivity(tostart);
                }
            });
        }
        this.mVideoButton.setLayoutParams(videoPa);
        ViewGroup.LayoutParams webUrlPa = this.mImprintWebUrlButton.getLayoutParams();
        ViewGroup.LayoutParams webPa = this.mWebButton.getLayoutParams();
        this.mWebButton.setOnTouchListener(this);
        this.mImprintWebUrlButton.setOnTouchListener(this);
        String imprintWebUrl = this.myImprint.getWebSiteUrl();
        if (imprintWebUrl == null || imprintWebUrl.length() <= 0) {
            webUrlPa.height = 0;
            webPa.height = 0;
            webPa.width = 0;
        } else {
            webUrlPa.height = -2;
            this.mImprintWebUrlButton.setText(imprintWebUrl);
            webPa.height = -2;
            webPa.width = -2;
            this.mImprintWebUrlButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ImprintAccessory bImprintAccessory = new ImprintAccessory("Online", ImprintDetailActivity.this.myImprint.getWebSiteUrl(), "profile_web", "profile_option_bg", ImprintDetailActivity.this.myImprint.getWebSiteUrl(), null);
                    Intent intent = new Intent(ImprintDetailActivity.this, ImprintWebsiteViewActivity.class);
                    intent.putExtras(bImprintAccessory.toBundle());
                    ImprintDetailActivity.this.startActivity(intent);
                }
            });
            this.mWebButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ImprintAccessory bImprintAccessory = new ImprintAccessory("Online", ImprintDetailActivity.this.myImprint.getWebSiteUrl(), "profile_web", "profile_option_bg", ImprintDetailActivity.this.myImprint.getWebSiteUrl(), null);
                    Intent intent = new Intent(ImprintDetailActivity.this, ImprintWebsiteViewActivity.class);
                    intent.putExtras(bImprintAccessory.toBundle());
                    ImprintDetailActivity.this.startActivity(intent);
                }
            });
        }
        this.mWebButton.setLayoutParams(webPa);
        this.mImprintWebUrlButton.setLayoutParams(webUrlPa);
        ViewGroup.LayoutParams mutiplePa = this.mMultipleLocationLayout.getLayoutParams();
        if (this.myImprint.getElementList() == null || this.myImprint.getElementList().size() <= 1) {
            mutiplePa.height = 0;
        } else {
            mutiplePa.height = -2;
            this.mMultipleLocationText.setText(String.format("Mutiple Locations (%d)", Integer.valueOf(this.myImprint.getElementList().size())));
            this.mExpandCollapseImage.setImageResource(R.drawable.expand);
            this.mShowAllLayout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (ImprintDetailActivity.this.mShowListNumber == 1) {
                        ImprintDetailActivity.this.mShowListNumber = ImprintDetailActivity.this.myImprint.getElementList().size();
                        ImprintDetailActivity.this.mExpandCollapseImage.setImageResource(R.drawable.contract);
                    } else {
                        ImprintDetailActivity.this.mShowListNumber = 1;
                        ImprintDetailActivity.this.mExpandCollapseImage.setImageResource(R.drawable.expand);
                    }
                    ImprintDetailActivity.this.mImprintElementListView.setAdapter((ListAdapter) new ImprintElementListAdapter(ImprintDetailActivity.this.mShowListNumber, ImprintDetailActivity.this.myImprint.getElementList()));
                    ImprintDetailActivity.this.mImprintElementListView.setOnTouchListener(ImprintDetailActivity.this);
                }
            });
        }
        this.mMultipleLocationLayout.setLayoutParams(mutiplePa);
        String imageFileName = "logo.gif";
        String finalImageUrl = this.myImprint.getImageLogo();
        if (finalImageUrl == null || finalImageUrl.length() <= 0) {
            this.mShopLocalDataHandler.post(this.mImageLogoUpdateTask);
            return;
        }
        if (finalImageUrl.toLowerCase().endsWith(".jpg")) {
            imageFileName = "logo.jpg";
        } else if (finalImageUrl.toLowerCase().endsWith(".png")) {
            imageFileName = "logo.png";
        }
        final String str2 = finalImageUrl;
        final String str3 = imageFileName;
        new Thread() {
            public void run() {
                try {
                    ImprintDetailActivity.this.mImprintLogo = ImprintAccessory.DownloadImage(str2, str3);
                    ImprintDetailActivity.this.mShopLocalDataHandler.post(ImprintDetailActivity.this.mImageLogoUpdateTask);
                } catch (Exception e) {
                }
            }
        }.start();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        populateMenu(menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        return applyMenuChoice(item) || super.onOptionsItemSelected(item);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        if (getResources().getConfiguration().orientation == 2) {
            getWindow().setFlags(1024, 1024);
        } else {
            getWindow().clearFlags(1024);
        }
        super.onConfigurationChanged(newConfig);
    }

    public void populateMenu(Menu menu) {
        menu.setQwertyMode(true);
        if (this.myImprint.getEmailAddress() != null) {
            MenuItem itemEmail = menu.add(0, 2, 0, "Email");
            itemEmail.setAlphabeticShortcut('s');
            itemEmail.setIcon((int) R.drawable.menu_email);
        }
        if (this.myImprint.isWebsite()) {
            MenuItem itemWeb = menu.add(0, 3, 0, "Web");
            itemWeb.setAlphabeticShortcut('w');
            itemWeb.setIcon((int) R.drawable.menu_web);
        }
        if (this.myImprint.getMenu() != null) {
            MenuItem itemMenu = menu.add(0, 5, 0, "Menu");
            itemMenu.setAlphabeticShortcut('m');
            itemMenu.setIcon((int) R.drawable.menu_menu);
        }
        if (this.myImprint.getVideoUrl() != null) {
            MenuItem itemMenu2 = menu.add(0, 7, 0, "Video");
            itemMenu2.setAlphabeticShortcut('v');
            itemMenu2.setIcon((int) R.drawable.menu_video);
        }
        if (this.myImprint.hasCoupon()) {
            MenuItem itemCoupon = menu.add(0, 4, 0, "Coupon");
            itemCoupon.setAlphabeticShortcut('c');
            itemCoupon.setIcon((int) R.drawable.menu_coupon);
        }
        if (this.myImprint.getADImageUrl() != null) {
            MenuItem itemAdvertisement = menu.add(0, 6, 0, "Advertisement");
            itemAdvertisement.setAlphabeticShortcut('a');
            itemAdvertisement.setIcon((int) R.drawable.menu_ad);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0126  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0150  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x01d4  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x01fe  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0294  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean applyMenuChoice(android.view.MenuItem r22) {
        /*
            r21 = this;
            com.santabarbarayp.www.ImprintAccessory r5 = new com.santabarbarayp.www.ImprintAccessory
            r5.<init>()
            int r6 = r22.getItemId()
            switch(r6) {
                case 2: goto L_0x000e;
                case 3: goto L_0x0049;
                case 4: goto L_0x0086;
                case 5: goto L_0x0134;
                case 6: goto L_0x01e2;
                case 7: goto L_0x02a2;
                default: goto L_0x000c;
            }
        L_0x000c:
            r6 = 0
        L_0x000d:
            return r6
        L_0x000e:
            android.content.Intent r19 = new android.content.Intent
            java.lang.String r6 = "android.intent.action.SEND"
            r0 = r19
            r1 = r6
            r0.<init>(r1)
            java.lang.String r6 = "android.intent.extra.EMAIL"
            r7 = 1
            java.lang.String[] r7 = new java.lang.String[r7]
            r8 = 0
            r0 = r21
            com.santabarbarayp.www.Imprint r0 = r0.myImprint
            r9 = r0
            java.lang.String r9 = r9.getEmailAddress()
            r7[r8] = r9
            r0 = r19
            r1 = r6
            r2 = r7
            r0.putExtra(r1, r2)
            java.lang.String r6 = "text/html"
            r0 = r19
            r1 = r6
            r0.setType(r1)
            java.lang.String r6 = "Send Email "
            r0 = r19
            r1 = r6
            android.content.Intent r6 = android.content.Intent.createChooser(r0, r1)
            r0 = r21
            r1 = r6
            r0.startActivity(r1)
            r6 = 1
            goto L_0x000d
        L_0x0049:
            com.santabarbarayp.www.ImprintAccessory r5 = new com.santabarbarayp.www.ImprintAccessory
            java.lang.String r6 = "Online"
            r0 = r21
            com.santabarbarayp.www.Imprint r0 = r0.myImprint
            r7 = r0
            java.lang.String r7 = r7.getWebSiteUrl()
            java.lang.String r8 = "profile_web"
            java.lang.String r9 = "profile_option_bg"
            r0 = r21
            com.santabarbarayp.www.Imprint r0 = r0.myImprint
            r10 = r0
            java.lang.String r10 = r10.getWebSiteUrl()
            r11 = 0
            r5.<init>(r6, r7, r8, r9, r10, r11)
            android.content.Intent r18 = new android.content.Intent
            java.lang.Class<com.santabarbarayp.www.ImprintWebsiteViewActivity> r6 = com.santabarbarayp.www.ImprintWebsiteViewActivity.class
            r0 = r18
            r1 = r21
            r2 = r6
            r0.<init>(r1, r2)
            android.os.Bundle r6 = r5.toBundle()
            r0 = r18
            r1 = r6
            r0.putExtras(r1)
            r0 = r21
            r1 = r18
            r0.startActivity(r1)
            r6 = 1
            goto L_0x000d
        L_0x0086:
            r15 = 0
        L_0x0087:
            r0 = r21
            java.util.ArrayList<com.santabarbarayp.www.ImprintAccessory> r0 = r0.mAccessoryList
            r6 = r0
            int r6 = r6.size()
            if (r15 < r6) goto L_0x00fc
        L_0x0092:
            r0 = r5
            r1 = r21
            r1.mImprintAccessory = r0
            r0 = r21
            com.santabarbarayp.www.ImprintAccessory r0 = r0.mImprintAccessory
            r6 = r0
            android.graphics.Bitmap r6 = r6.getBitmap()
            if (r6 != 0) goto L_0x0126
            r0 = r21
            android.widget.ProgressBar r0 = r0.mProgressbar
            r6 = r0
            r7 = 0
            r6.setVisibility(r7)
            r0 = r21
            com.santabarbarayp.www.ImprintAccessory r0 = r0.mImprintAccessory
            r6 = r0
            java.lang.String r13 = r6.getAccssoryUrl()
            java.lang.String r16 = "coupon.gif"
            java.lang.String r6 = r13.toLowerCase()
            java.lang.String r7 = ".jpg"
            boolean r6 = r6.endsWith(r7)
            if (r6 == 0) goto L_0x0117
            java.lang.String r16 = "coupon.jpg"
        L_0x00c4:
            r12 = r16
            r0 = r21
            com.santabarbarayp.www.ImprintAccessory r0 = r0.mImprintAccessory
            r6 = r0
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.io.File r8 = android.os.Environment.getExternalStorageDirectory()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = "/shoplocal/download/"
            java.lang.StringBuilder r7 = r7.append(r8)
            r0 = r7
            r1 = r16
            java.lang.StringBuilder r7 = r0.append(r1)
            java.lang.String r7 = r7.toString()
            r6.setPhysicalPath(r7)
            com.santabarbarayp.www.ImprintDetailActivity$13 r6 = new com.santabarbarayp.www.ImprintDetailActivity$13
            r0 = r6
            r1 = r21
            r2 = r13
            r3 = r12
            r0.<init>(r2, r3)
            r6.start()
        L_0x00f9:
            r6 = 1
            goto L_0x000d
        L_0x00fc:
            r0 = r21
            java.util.ArrayList<com.santabarbarayp.www.ImprintAccessory> r0 = r0.mAccessoryList
            r6 = r0
            java.lang.Object r5 = r6.get(r15)
            com.santabarbarayp.www.ImprintAccessory r5 = (com.santabarbarayp.www.ImprintAccessory) r5
            java.lang.String r6 = r5.getTitle()
            java.lang.String r7 = "Coupon"
            boolean r6 = r6.equalsIgnoreCase(r7)
            if (r6 != 0) goto L_0x0092
            int r15 = r15 + 1
            goto L_0x0087
        L_0x0117:
            java.lang.String r6 = r13.toLowerCase()
            java.lang.String r7 = ".png"
            boolean r6 = r6.endsWith(r7)
            if (r6 == 0) goto L_0x00c4
            java.lang.String r16 = "coupon.png"
            goto L_0x00c4
        L_0x0126:
            r0 = r21
            android.os.Handler r0 = r0.mShopLocalDataHandler
            r6 = r0
            r0 = r21
            java.lang.Runnable r0 = r0.mBitmapUpdateTask
            r7 = r0
            r6.post(r7)
            goto L_0x00f9
        L_0x0134:
            r15 = 0
        L_0x0135:
            r0 = r21
            java.util.ArrayList<com.santabarbarayp.www.ImprintAccessory> r0 = r0.mAccessoryList
            r6 = r0
            int r6 = r6.size()
            if (r15 < r6) goto L_0x01aa
        L_0x0140:
            r0 = r5
            r1 = r21
            r1.mImprintAccessory = r0
            r0 = r21
            com.santabarbarayp.www.ImprintAccessory r0 = r0.mImprintAccessory
            r6 = r0
            android.graphics.Bitmap r6 = r6.getBitmap()
            if (r6 != 0) goto L_0x01d4
            r0 = r21
            android.widget.ProgressBar r0 = r0.mProgressbar
            r6 = r0
            r7 = 0
            r6.setVisibility(r7)
            r0 = r21
            com.santabarbarayp.www.ImprintAccessory r0 = r0.mImprintAccessory
            r6 = r0
            java.lang.String r13 = r6.getAccssoryUrl()
            java.lang.String r16 = "menu.gif"
            java.lang.String r6 = r13.toLowerCase()
            java.lang.String r7 = ".jpg"
            boolean r6 = r6.endsWith(r7)
            if (r6 == 0) goto L_0x01c5
            java.lang.String r16 = "menu.jpg"
        L_0x0172:
            r12 = r16
            r0 = r21
            com.santabarbarayp.www.ImprintAccessory r0 = r0.mImprintAccessory
            r6 = r0
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.io.File r8 = android.os.Environment.getExternalStorageDirectory()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = "/shoplocal/download/"
            java.lang.StringBuilder r7 = r7.append(r8)
            r0 = r7
            r1 = r16
            java.lang.StringBuilder r7 = r0.append(r1)
            java.lang.String r7 = r7.toString()
            r6.setPhysicalPath(r7)
            com.santabarbarayp.www.ImprintDetailActivity$14 r6 = new com.santabarbarayp.www.ImprintDetailActivity$14
            r0 = r6
            r1 = r21
            r2 = r13
            r3 = r12
            r0.<init>(r2, r3)
            r6.start()
        L_0x01a7:
            r6 = 1
            goto L_0x000d
        L_0x01aa:
            r0 = r21
            java.util.ArrayList<com.santabarbarayp.www.ImprintAccessory> r0 = r0.mAccessoryList
            r6 = r0
            java.lang.Object r5 = r6.get(r15)
            com.santabarbarayp.www.ImprintAccessory r5 = (com.santabarbarayp.www.ImprintAccessory) r5
            java.lang.String r6 = r5.getTitle()
            java.lang.String r7 = "Menu"
            boolean r6 = r6.equalsIgnoreCase(r7)
            if (r6 != 0) goto L_0x0140
            int r15 = r15 + 1
            goto L_0x0135
        L_0x01c5:
            java.lang.String r6 = r13.toLowerCase()
            java.lang.String r7 = ".png"
            boolean r6 = r6.endsWith(r7)
            if (r6 == 0) goto L_0x0172
            java.lang.String r16 = "menu.png"
            goto L_0x0172
        L_0x01d4:
            r0 = r21
            android.os.Handler r0 = r0.mShopLocalDataHandler
            r6 = r0
            r0 = r21
            java.lang.Runnable r0 = r0.mBitmapUpdateTask
            r7 = r0
            r6.post(r7)
            goto L_0x01a7
        L_0x01e2:
            r15 = 0
        L_0x01e3:
            r0 = r21
            java.util.ArrayList<com.santabarbarayp.www.ImprintAccessory> r0 = r0.mAccessoryList
            r6 = r0
            int r6 = r6.size()
            if (r15 < r6) goto L_0x025f
        L_0x01ee:
            r0 = r5
            r1 = r21
            r1.mImprintAccessory = r0
            r0 = r21
            com.santabarbarayp.www.ImprintAccessory r0 = r0.mImprintAccessory
            r6 = r0
            android.graphics.Bitmap r6 = r6.getBitmap()
            if (r6 != 0) goto L_0x0294
            r0 = r21
            android.widget.ProgressBar r0 = r0.mProgressbar
            r6 = r0
            r7 = 0
            r6.setVisibility(r7)
            r0 = r21
            com.santabarbarayp.www.ImprintAccessory r0 = r0.mImprintAccessory
            r6 = r0
            java.lang.String r13 = r6.getAccssoryUrl()
            r0 = r21
            com.santabarbarayp.www.ImprintAccessory r0 = r0.mImprintAccessory
            r6 = r0
            java.lang.String r14 = r6.getAccssoryUrl2()
            java.lang.String r16 = "advertisement.png"
            java.lang.String r6 = r13.toLowerCase()
            java.lang.String r7 = ".jpg"
            boolean r6 = r6.endsWith(r7)
            if (r6 == 0) goto L_0x027a
            java.lang.String r16 = "advertisement.jpg"
        L_0x0229:
            r12 = r16
            r0 = r21
            com.santabarbarayp.www.ImprintAccessory r0 = r0.mImprintAccessory
            r6 = r0
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.io.File r8 = android.os.Environment.getExternalStorageDirectory()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = "/shoplocal/download/"
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.StringBuilder r7 = r7.append(r12)
            java.lang.String r7 = r7.toString()
            r6.setPhysicalPath(r7)
            com.santabarbarayp.www.ImprintDetailActivity$15 r6 = new com.santabarbarayp.www.ImprintDetailActivity$15
            r0 = r6
            r1 = r21
            r2 = r14
            r3 = r13
            r4 = r12
            r0.<init>(r2, r3, r4)
            r6.start()
        L_0x025c:
            r6 = 1
            goto L_0x000d
        L_0x025f:
            r0 = r21
            java.util.ArrayList<com.santabarbarayp.www.ImprintAccessory> r0 = r0.mAccessoryList
            r6 = r0
            java.lang.Object r5 = r6.get(r15)
            com.santabarbarayp.www.ImprintAccessory r5 = (com.santabarbarayp.www.ImprintAccessory) r5
            java.lang.String r6 = r5.getTitle()
            java.lang.String r7 = "Advertisement"
            boolean r6 = r6.equalsIgnoreCase(r7)
            if (r6 != 0) goto L_0x01ee
            int r15 = r15 + 1
            goto L_0x01e3
        L_0x027a:
            java.lang.String r6 = r13.toLowerCase()
            java.lang.String r7 = ".gif"
            boolean r6 = r6.endsWith(r7)
            if (r6 == 0) goto L_0x0229
            if (r14 == 0) goto L_0x028e
            int r6 = r14.length()
            if (r6 != 0) goto L_0x0291
        L_0x028e:
            java.lang.String r16 = "advertisement.gif"
            goto L_0x0229
        L_0x0291:
            java.lang.String r16 = "advertisement.png"
            goto L_0x0229
        L_0x0294:
            r0 = r21
            android.os.Handler r0 = r0.mShopLocalDataHandler
            r6 = r0
            r0 = r21
            java.lang.Runnable r0 = r0.mBitmapUpdateTask
            r7 = r0
            r6.post(r7)
            goto L_0x025c
        L_0x02a2:
            r0 = r21
            com.santabarbarayp.www.Imprint r0 = r0.myImprint
            r6 = r0
            java.lang.String r17 = r6.getVideoUrl()
            if (r17 == 0) goto L_0x000c
            int r6 = r17.length()
            if (r6 <= 0) goto L_0x000c
            android.content.Intent r20 = new android.content.Intent
            java.lang.String r6 = "android.intent.action.VIEW"
            r0 = r20
            r1 = r6
            r0.<init>(r1)
            android.net.Uri r6 = android.net.Uri.parse(r17)
            java.lang.String r7 = "video/*"
            r0 = r20
            r1 = r6
            r2 = r7
            r0.setDataAndType(r1, r2)
            r0 = r21
            r1 = r20
            r0.startActivity(r1)
            r6 = 1
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.santabarbarayp.www.ImprintDetailActivity.applyMenuChoice(android.view.MenuItem):boolean");
    }

    private ArrayList<ImprintAccessory> createAccesoryList() {
        ArrayList<ImprintAccessory> accessoryList = new ArrayList<>();
        if (this.myImprint.hasCoupon()) {
            accessoryList.add(new ImprintAccessory("Coupon", "Save now!", "profile_coupon", "profile_option_bg", this.myImprint.getCouponImageUrl()));
        }
        if (this.myImprint.isWebsite()) {
            accessoryList.add(new ImprintAccessory("Online", this.myImprint.getWebSiteUrl(), "profile_web", "profile_option_bg", this.myImprint.getWebSiteUrl()));
        }
        if (this.myImprint.getADImageUrl() != null) {
            accessoryList.add(new ImprintAccessory("Advertisement", this.myImprint.getADSentence(), "profile_ad", "profile_option_bg", this.myImprint.getADImageUrl(), this.myImprint.getADImageUrl2()));
        }
        if (this.myImprint.getEmailAddress() != null) {
            accessoryList.add(new ImprintAccessory("Email", "Send now!", "profile_mail", "profile_option_bg", this.myImprint.getEmailAddress()));
        }
        if (this.myImprint.getMenu() != null) {
            accessoryList.add(new ImprintAccessory("Menu", "Open now!", "profile_menu", "profile_option_bg", this.myImprint.getMenu()));
        }
        if (this.myImprint.getVideoUrl() != null) {
            accessoryList.add(new ImprintAccessory("Video", "Play now!", "profile_video", "profile_option_bg", this.myImprint.getVideoUrl(), null));
        }
        return accessoryList;
    }

    class ImprintElementListAdapter extends BaseAdapter {
        private final LayoutInflater mInflater;
        private final ImprintElementList mItems;
        private int mShowNumbers = 0;

        public ImprintElementListAdapter(int showNumbers, ImprintElementList items) {
            this.mItems = items;
            this.mShowNumbers = showNumbers;
            this.mInflater = (LayoutInflater) ImprintDetailActivity.this.getSystemService("layout_inflater");
        }

        public int getCount() {
            return this.mShowNumbers;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            boolean isGeoLocation;
            View v = convertView;
            if (v == null) {
                v = this.mInflater.inflate((int) R.layout.imprint_element_row, (ViewGroup) null);
            }
            int currentImprintElementIndex = position;
            ImprintElement cImprintElement = (ImprintElement) this.mItems.get(currentImprintElementIndex);
            LinearLayout extralinelayout = (LinearLayout) v.findViewById(R.id.imprint_element_extraline_layout);
            RelativeLayout.LayoutParams extralineLayoutPa = (RelativeLayout.LayoutParams) extralinelayout.getLayoutParams();
            ArrayList<String> elementExtalineList = cImprintElement.getExtraLineList();
            extralinelayout.removeAllViews();
            if (elementExtalineList == null || elementExtalineList.size() <= 0) {
                extralineLayoutPa.height = 0;
            } else {
                for (int m = 0; m < elementExtalineList.size(); m++) {
                    TextView textView = new TextView(ImprintDetailActivity.this);
                    textView.setText(elementExtalineList.get(m));
                    textView.setTextAppearance(ImprintDetailActivity.this, R.style.ListText14Appearance);
                    extralinelayout.addView(textView);
                }
                extralineLayoutPa.height = -2;
            }
            extralinelayout.setLayoutParams(extralineLayoutPa);
            LinearLayout phoneContainer = (LinearLayout) v.findViewById(R.id.imprint_element_phone_container);
            RelativeLayout.LayoutParams phoneLayoutPa = (RelativeLayout.LayoutParams) phoneContainer.getLayoutParams();
            phoneContainer.removeAllViews();
            ArrayList<String> phoneNumberList = cImprintElement.getPhoneList();
            if (phoneNumberList == null || phoneNumberList.size() <= 0) {
                phoneLayoutPa.height = 0;
                phoneLayoutPa.leftMargin = 0;
            } else {
                for (int m2 = 0; m2 < phoneNumberList.size(); m2++) {
                    TextView textView2 = new TextView(ImprintDetailActivity.this);
                    String fPhoneNumber = phoneNumberList.get(m2).trim().replaceAll("[^0-9A-Za-z()]", " ").replaceAll(" ", "-");
                    textView2.setText(fPhoneNumber);
                    textView2.setTextAppearance(ImprintDetailActivity.this, R.style.Blue14TextAppearance);
                    final String str = fPhoneNumber;
                    textView2.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            try {
                                ImprintDetailActivity.this.startActivity(Intent.getIntent("tel:" + str.replaceAll("[^a-zA-Z0-9()]", "")));
                            } catch (Exception e) {
                            }
                        }
                    });
                    phoneContainer.addView(textView2);
                }
                if (phoneNumberList.size() == 1) {
                    final String replaceAll = phoneNumberList.get(0).trim().replaceAll("[^0-9A-Za-z()]", " ").replaceAll(" ", "-");
                    phoneContainer.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            try {
                                ImprintDetailActivity.this.startActivity(Intent.getIntent("tel:" + replaceAll.replaceAll("[^a-zA-Z0-9()]", "")));
                            } catch (Exception e) {
                            }
                        }
                    });
                } else {
                    phoneContainer.setOnClickListener(null);
                }
                phoneLayoutPa.height = -2;
                DisplayMetrics dm = new DisplayMetrics();
                ImprintDetailActivity.this.getWindowManager().getDefaultDisplay().getMetrics(dm);
                if (ImprintDetailActivity.this.getResources().getConfiguration().orientation == 2) {
                    phoneLayoutPa.leftMargin = (int) (80.0f * dm.density);
                } else {
                    phoneLayoutPa.leftMargin = (int) (4.0f * dm.density);
                }
            }
            phoneContainer.setLayoutParams(phoneLayoutPa);
            phoneContainer.setOnTouchListener(ImprintDetailActivity.this);
            phoneContainer.setTag("p" + position);
            ImageView imprintElement_annotation_button = (ImageView) v.findViewById(R.id.imprintelement_annotation_button);
            imprintElement_annotation_button.setOnTouchListener(ImprintDetailActivity.this);
            imprintElement_annotation_button.setTag("p" + position);
            ViewGroup.LayoutParams annotationPa = imprintElement_annotation_button.getLayoutParams();
            int geoIndex = cImprintElement.getGeoLocationIndex();
            if (geoIndex < 0) {
                annotationPa.width = 0;
            } else {
                annotationPa.width = -2;
                if (geoIndex > 25) {
                    geoIndex %= 26;
                }
                imprintElement_annotation_button.setImageResource(ImprintDetailActivity.this.getResources().getIdentifier(String.format("noarrow_%c", Character.valueOf((char) (geoIndex + 97))), "drawable", "com.santabarbarayp.www"));
                final int i = currentImprintElementIndex;
                imprintElement_annotation_button.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent intent = new Intent(ImprintDetailActivity.this, ImprintDetailMapActivity.class);
                        Bundle b = new Bundle();
                        b.putString("imprintName", ImprintDetailActivity.this.myImprint.getName());
                        b.putInt("currentImprintElementIndex", i);
                        b.putParcelable("currentImprintElementList", ImprintDetailActivity.this.myImprint.getElementList());
                        intent.putExtras(b);
                        ImprintDetailActivity.this.startActivity(intent);
                    }
                });
            }
            imprintElement_annotation_button.setLayoutParams(annotationPa);
            RelativeLayout addresslayout = (RelativeLayout) v.findViewById(R.id.imprint_element_address_layout);
            addresslayout.setOnTouchListener(ImprintDetailActivity.this);
            addresslayout.setTag("p" + position);
            if (cImprintElement.getDistance() > -1.0d) {
                isGeoLocation = true;
            } else {
                isGeoLocation = false;
            }
            if (isGeoLocation) {
                final int i2 = currentImprintElementIndex;
                addresslayout.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent intent = new Intent(ImprintDetailActivity.this, ImprintDetailMapActivity.class);
                        Bundle b = new Bundle();
                        b.putString("imprintName", ImprintDetailActivity.this.myImprint.getName());
                        b.putInt("currentImprintElementIndex", i2);
                        b.putParcelable("currentImprintElementList", ImprintDetailActivity.this.myImprint.getElementList());
                        intent.putExtras(b);
                        ImprintDetailActivity.this.startActivity(intent);
                    }
                });
            }
            TextView streetView = (TextView) v.findViewById(R.id.imprint_element_street);
            boolean isAddressAvailable = false;
            String streetAddress = cImprintElement.getStreet();
            ViewGroup.LayoutParams streetPa = streetView.getLayoutParams();
            if (streetAddress == null || streetAddress.length() <= 0) {
                streetPa.height = 0;
            } else {
                streetView.setText(streetAddress);
                if (isGeoLocation) {
                    streetView.setTextAppearance(ImprintDetailActivity.this, R.style.Blue14TextAppearance);
                } else {
                    streetView.setTextAppearance(ImprintDetailActivity.this, R.style.ListText14Appearance);
                }
                streetPa.height = -2;
                isAddressAvailable = true;
            }
            streetView.setLayoutParams(streetPa);
            String cityAddress = cImprintElement.getCityStateZip();
            TextView cityView = (TextView) v.findViewById(R.id.imprint_element_city);
            ViewGroup.LayoutParams cityPa = cityView.getLayoutParams();
            if (cityAddress == null || cityAddress.length() <= 0) {
                cityPa.height = 0;
            } else {
                cityView.setText(cityAddress);
                cityPa.height = -2;
                isAddressAvailable = true;
                if (isGeoLocation) {
                    cityView.setTextAppearance(ImprintDetailActivity.this, R.style.Blue14TextAppearance);
                } else {
                    cityView.setTextAppearance(ImprintDetailActivity.this, R.style.ListText14Appearance);
                }
            }
            cityView.setLayoutParams(cityPa);
            RelativeLayout.LayoutParams addressPa = (RelativeLayout.LayoutParams) addresslayout.getLayoutParams();
            if (isAddressAvailable) {
                addressPa.height = -2;
                DisplayMetrics dm2 = new DisplayMetrics();
                ImprintDetailActivity.this.getWindowManager().getDefaultDisplay().getMetrics(dm2);
                if (ImprintDetailActivity.this.getResources().getConfiguration().orientation == 2) {
                    addressPa.width = (int) (180.0f * dm2.density);
                } else {
                    addressPa.width = (int) (150.0f * dm2.density);
                }
            } else {
                addressPa.height = 0;
            }
            addresslayout.setLayoutParams(addressPa);
            RelativeLayout contactlayout = (RelativeLayout) v.findViewById(R.id.imprint_element_contact_layout);
            contactlayout.setOnTouchListener(ImprintDetailActivity.this);
            contactlayout.setTag("p" + position);
            final ImprintElement imprintElement = cImprintElement;
            contactlayout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ImprintDetailActivity.this.createContactEntry(imprintElement);
                }
            });
            RelativeLayout sharelayout = (RelativeLayout) v.findViewById(R.id.imprint_element_share_layout);
            sharelayout.setOnTouchListener(ImprintDetailActivity.this);
            sharelayout.setTag("p" + position);
            final ImprintElement imprintElement2 = cImprintElement;
            sharelayout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ImprintDetailActivity.this.mImprintElement = imprintElement2;
                    if (ImprintDetailActivity.this.myImprint.getADImageUrl() != null) {
                        ImprintAccessory bImprintAccessory = new ImprintAccessory();
                        for (int i = 0; i < ImprintDetailActivity.this.mAccessoryList.size(); i++) {
                            bImprintAccessory = (ImprintAccessory) ImprintDetailActivity.this.mAccessoryList.get(i);
                            if (bImprintAccessory.getTitle().equalsIgnoreCase("Advertisement")) {
                                break;
                            }
                        }
                        ImprintDetailActivity.this.mImprintAccessory = bImprintAccessory;
                        if (ImprintDetailActivity.this.mImprintAccessory.getBitmap() == null) {
                            ImprintDetailActivity.this.mProgressbar.setVisibility(0);
                            final String finalImageUrl = ImprintDetailActivity.this.mImprintAccessory.getAccssoryUrl();
                            final String finalImageUrl2 = ImprintDetailActivity.this.mImprintAccessory.getAccssoryUrl2();
                            String imageFileName = "advertisement.png";
                            if (finalImageUrl.toLowerCase().endsWith(".jpg")) {
                                imageFileName = "advertisement.jpg";
                            } else if (finalImageUrl.toLowerCase().endsWith(".gif")) {
                                if (finalImageUrl2 == null || finalImageUrl2.length() == 0) {
                                    imageFileName = "advertisement.gif";
                                } else {
                                    imageFileName = "advertisement.png";
                                }
                            }
                            final String finalImageFileName = imageFileName;
                            ImprintDetailActivity.this.mImprintAccessory.setPhysicalPath(Environment.getExternalStorageDirectory() + "/shoplocal/download/" + finalImageFileName);
                            new Thread() {
                                public void run() {
                                    try {
                                        if (finalImageUrl2 == null || finalImageUrl2.length() == 0) {
                                            ImprintDetailActivity.this.mImprintAccessory.setBitmap(ImprintAccessory.DownloadImage(finalImageUrl, finalImageFileName));
                                        } else {
                                            ImprintDetailActivity.this.mImprintAccessory.setBitmap(ImprintAccessory.DownloadImage(finalImageUrl, finalImageUrl2, finalImageFileName));
                                        }
                                        ImprintDetailActivity.this.mShopLocalDataHandler.post(ImprintDetailActivity.this.mSendingEmailUpdateTask);
                                    } catch (Exception e) {
                                    }
                                }
                            }.start();
                            return;
                        }
                        ImprintDetailActivity.this.mShopLocalDataHandler.post(ImprintDetailActivity.this.mSendingEmailUpdateTask);
                        return;
                    }
                    ImprintDetailActivity.this.mShopLocalDataHandler.post(ImprintDetailActivity.this.mSendingEmailUpdateTask);
                }
            });
            RelativeLayout drivingLayout = (RelativeLayout) v.findViewById(R.id.imprint_element_driving_layout);
            if (isGeoLocation) {
                drivingLayout.setVisibility(0);
                drivingLayout.setOnTouchListener(ImprintDetailActivity.this);
                drivingLayout.setTag("p" + position);
                final ImprintElement imprintElement3 = cImprintElement;
                drivingLayout.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent intent = new Intent(ImprintDetailActivity.this, DrivingDirectionsActivity.class);
                        Bundle b = new Bundle();
                        String elementAddress = imprintElement3.getStreet();
                        if (elementAddress == null) {
                            elementAddress = "";
                        }
                        String cityAddress = imprintElement3.getCityStateZip();
                        if (elementAddress.trim().length() == 0) {
                            elementAddress = cityAddress;
                            if (elementAddress == null) {
                                elementAddress = "";
                            }
                        } else if (cityAddress != null && cityAddress.length() > 0) {
                            elementAddress = String.valueOf(elementAddress) + ", " + cityAddress;
                        }
                        b.putParcelable("drivingDirection_DestinationPoint", new GeoLocation(elementAddress, imprintElement3.getLat(), imprintElement3.getLng()));
                        b.putParcelable("drivingDirection_SourcePoint", ImprintDetailActivity.this.ls_SearchHomeLocation);
                        intent.putExtras(b);
                        ImprintDetailActivity.this.startActivity(intent);
                    }
                });
            } else {
                drivingLayout.setVisibility(4);
            }
            return v;
        }
    }

    /* access modifiers changed from: protected */
    public void createContactEntry(ImprintElement cImprintElement) {
        try {
            String name = cImprintElement.getName();
            ArrayList<String> phoneList = cImprintElement.getPhoneList();
            String streetAddress = cImprintElement.getStreet();
            String cityStateZip = cImprintElement.getCityStateZip();
            boolean hasWebSite = this.myImprint.isWebsite();
            String email = this.myImprint.getEmailAddress();
            if (streetAddress == null) {
                streetAddress = "";
            }
            String wholeAddress = streetAddress;
            if (cityStateZip != null) {
                wholeAddress = String.format("%s %s", wholeAddress, cityStateZip.trim());
            }
            ArrayList<ContentProviderOperation> ops = new ArrayList<>();
            ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI).withValue("account_type", null).withValue("account_name", null).build());
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", 0).withValue("mimetype", "vnd.android.cursor.item/name").withValue("data1", name).build());
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", 0).withValue("mimetype", "vnd.android.cursor.item/postal-address_v2").withValue("data1", wholeAddress).withValue("data2", 2).build());
            if (phoneList != null && phoneList.size() > 0) {
                for (int k = 0; k < phoneList.size(); k++) {
                    String phone = phoneList.get(k).replaceAll("[^0-9A-Za-z()]", " ").trim();
                    if (phone.length() > 0) {
                        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", 0).withValue("mimetype", "vnd.android.cursor.item/phone_v2").withValue("data1", phone).withValue("data2", 3).build());
                    }
                }
            }
            if (email != null && email.trim().length() > 0) {
                ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", 0).withValue("mimetype", "vnd.android.cursor.item/email_v2").withValue("data1", email.trim()).withValue("data2", 2).build());
            }
            if (hasWebSite) {
                ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", 0).withValue("mimetype", "vnd.android.cursor.item/website").withValue("data1", this.myImprint.getWebSiteUrl()).withValue("data2", 5).build());
            }
            getContentResolver().applyBatch("com.android.contacts", ops);
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("New Contact");
            alertDialog.setMessage("New contact created successfully !");
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
        } catch (Exception e) {
            AlertDialog alertDialog2 = new AlertDialog.Builder(this).create();
            alertDialog2.setTitle("New Contact");
            alertDialog2.setMessage("Error creating new contact. please try again later");
            alertDialog2.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog2.show();
        }
    }

    public boolean onTouchEvent(MotionEvent me) {
        return this.mGestureDetector.onTouchEvent(me);
    }

    public boolean onDown(MotionEvent e) {
        return false;
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        try {
            if (Math.abs(e1.getY() - e2.getY()) > 250.0f) {
                return false;
            }
            if (e1.getX() - e2.getX() > 120.0f && Math.abs(velocityX) > 200.0f) {
                if (this.mImprintElementListView.pointToPosition(((int) (e1.getX() + e2.getX())) / 2, ((int) (e1.getY() + e2.getY())) / 2) == -1) {
                    if (this.mFlingElementListIndex > 0) {
                        int positionItem = this.mFlingElementListIndex;
                    } else {
                        int positionItem2 = this.mImprintElementListView.getFirstVisiblePosition();
                    }
                }
                String imprintADUrl = this.myImprint.getADImageUrl();
                if (imprintADUrl != null && imprintADUrl.length() > 0) {
                    ImprintAccessory bImprintAccessory = new ImprintAccessory();
                    for (int i = 0; i < this.mAccessoryList.size(); i++) {
                        bImprintAccessory = this.mAccessoryList.get(i);
                        if (bImprintAccessory.getTitle().equalsIgnoreCase("Advertisement")) {
                            break;
                        }
                    }
                    this.mImprintAccessory = bImprintAccessory;
                    if (this.mImprintAccessory.getBitmap() == null) {
                        this.mProgressbar.setVisibility(0);
                        final String finalImageUrl = this.mImprintAccessory.getAccssoryUrl();
                        final String finalImageUrl2 = this.mImprintAccessory.getAccssoryUrl2();
                        String imageFileName = "advertisement.png";
                        if (finalImageUrl.toLowerCase().endsWith(".jpg")) {
                            imageFileName = "advertisement.jpg";
                        } else if (finalImageUrl.toLowerCase().endsWith(".gif")) {
                            if (finalImageUrl2 == null || finalImageUrl2.length() == 0) {
                                imageFileName = "advertisement.gif";
                            } else {
                                imageFileName = "advertisement.png";
                            }
                        }
                        final String finalImageFileName = imageFileName;
                        this.mImprintAccessory.setPhysicalPath(Environment.getExternalStorageDirectory() + "/shoplocal/download/" + finalImageFileName);
                        new Thread() {
                            public void run() {
                                try {
                                    if (finalImageUrl2 == null || finalImageUrl2.length() == 0) {
                                        ImprintDetailActivity.this.mImprintAccessory.setBitmap(ImprintAccessory.DownloadImage(finalImageUrl, finalImageFileName));
                                    } else {
                                        ImprintDetailActivity.this.mImprintAccessory.setBitmap(ImprintAccessory.DownloadImage(finalImageUrl, finalImageUrl2, finalImageFileName));
                                    }
                                    ImprintDetailActivity.this.mShopLocalDataHandler.post(ImprintDetailActivity.this.mBitmapUpdateTask);
                                } catch (Exception e) {
                                }
                            }
                        }.start();
                    } else {
                        this.mShopLocalDataHandler.post(this.mBitmapUpdateTask);
                    }
                    return true;
                }
                return false;
            } else if (e2.getX() - e1.getX() <= 120.0f || Math.abs(velocityX) <= 200.0f) {
                if ((e1.getY() - e2.getY() <= 120.0f || Math.abs(velocityX) <= 200.0f) && e2.getY() - e1.getY() > 120.0f) {
                    Math.abs(velocityX);
                }
                return false;
            } else {
                setResult(-1, new Intent(getApplicationContext(), ShopLocalMapListActivity.class));
                finish();
                return true;
            }
        } catch (Exception e) {
        }
    }

    public void onLongPress(MotionEvent e) {
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    public void onShowPress(MotionEvent e) {
    }

    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        setResult(-1, new Intent(getApplicationContext(), ShopLocalMapListActivity.class));
        finish();
        return true;
    }

    public boolean onTouch(View v, MotionEvent event) {
        this.mFlingElementListIndex = 0;
        Object viewTag = v.getTag();
        if (viewTag != null && viewTag.toString().startsWith("p")) {
            this.mFlingElementListIndex = Integer.parseInt(viewTag.toString().substring(1));
        }
        return this.mGestureDetector.onTouchEvent(event);
    }
}
