package com.santabarbarayp.www;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public class ImprintBannerList extends ArrayList<ImprintBanner> implements Parcelable {
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public ImprintBannerList createFromParcel(Parcel in) {
            return new ImprintBannerList(in);
        }

        public Object[] newArray(int arg0) {
            return null;
        }
    };
    private static final long serialVersionUID = 663763471239879096L;

    public ImprintBannerList() {
    }

    public ImprintBannerList(Parcel in) {
        readFromParcel(in);
    }

    private void readFromParcel(Parcel in) {
        clear();
        int size = in.readInt();
        for (int i = 0; i < size; i++) {
            ImprintBanner ie = new ImprintBanner();
            ie.setName(in.readString());
            ie.setImageUrl(in.readString());
            ie.setWidth(in.readDouble());
            ie.setHeight(in.readDouble());
            add(ie);
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        int size = size();
        dest.writeInt(size);
        for (int i = 0; i < size; i++) {
            ImprintBanner ie = (ImprintBanner) get(i);
            dest.writeString(ie.getName());
            dest.writeString(ie.getImageUrl());
            dest.writeDouble(ie.getWidth());
            dest.writeDouble(ie.getHeight());
        }
    }
}
