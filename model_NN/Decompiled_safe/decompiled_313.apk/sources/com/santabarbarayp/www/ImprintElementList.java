package com.santabarbarayp.www;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public class ImprintElementList extends ArrayList<ImprintElement> implements Parcelable {
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public ImprintElementList createFromParcel(Parcel in) {
            return new ImprintElementList(in);
        }

        public Object[] newArray(int arg0) {
            return null;
        }
    };
    private static final long serialVersionUID = 663585476779879096L;

    public ImprintElementList() {
    }

    public ImprintElementList(Parcel in) {
        readFromParcel(in);
    }

    private void readFromParcel(Parcel in) {
        clear();
        int size = in.readInt();
        for (int i = 0; i < size; i++) {
            ImprintElement ie = new ImprintElement();
            ie.setName(in.readString());
            ie.setStreet(in.readString());
            ie.setCityStateZip(in.readString());
            ArrayList<String> extralineList = null;
            int extralineNumbers = in.readInt();
            if (extralineNumbers > 0) {
                extralineList = new ArrayList<>();
            }
            for (int n = 0; n < extralineNumbers; n++) {
                extralineList.add(in.readString());
            }
            ie.setExtraLineList(extralineList);
            int phoneNumbers = in.readInt();
            ArrayList<String> phoneList = null;
            if (phoneNumbers > 0) {
                phoneList = new ArrayList<>();
            }
            for (int k = 0; k < phoneNumbers; k++) {
                phoneList.add(in.readString());
            }
            ie.setPhoneList(phoneList);
            ie.setDistance(in.readDouble());
            ie.setLat(in.readDouble());
            ie.setLng(in.readDouble());
            ie.setImageLogo(in.readString());
            ie.setGeoLocationIndex(in.readInt());
            add(ie);
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        int size = size();
        dest.writeInt(size);
        for (int i = 0; i < size; i++) {
            ImprintElement ie = (ImprintElement) get(i);
            dest.writeString(ie.getName());
            dest.writeString(ie.getStreet());
            dest.writeString(ie.getCityStateZip());
            ArrayList<String> extralineList = ie.getExtraLineList();
            int extralineNumbers = 0;
            if (extralineList != null) {
                extralineNumbers = extralineList.size();
            }
            dest.writeInt(extralineNumbers);
            for (int n = 0; n < extralineNumbers; n++) {
                dest.writeString(extralineList.get(n));
            }
            ArrayList<String> phoneList = ie.getPhoneList();
            int phoneNumbers = 0;
            if (phoneList != null) {
                phoneNumbers = phoneList.size();
            }
            dest.writeInt(phoneNumbers);
            for (int k = 0; k < phoneNumbers; k++) {
                dest.writeString(phoneList.get(k));
            }
            dest.writeDouble(ie.getDistance());
            dest.writeDouble(ie.getLat());
            dest.writeDouble(ie.getLng());
            dest.writeString(ie.getImageLogo());
            dest.writeInt(ie.getGeoLocationIndex());
        }
    }
}
