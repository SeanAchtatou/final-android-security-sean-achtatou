package com.santabarbarayp.www;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ViewFlipper;

public class ShopLocal_ViewFlipper extends ViewFlipper {
    public ShopLocal_ViewFlipper(Context context) {
        super(context);
    }

    public ShopLocal_ViewFlipper(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        try {
            super.onDetachedFromWindow();
        } catch (IllegalArgumentException e) {
            stopFlipping();
        }
    }
}
