package com.santabarbarayp.www;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.FloatMath;
import android.view.Display;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.maps.GeoPoint;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;

public class ShopLocalActivity extends Activity implements LocationListener, GestureDetector.OnGestureListener, View.OnTouchListener {
    public static final int AD_BITMAP_REQUEST_CODE = 5678;
    public static final int ANNOTATION_NUMBER = 20;
    public static final long BANNER_DELAY_SPAN_MILLS = 5000;
    public static final String DomainDeviceID = "69.1";
    public static final int IMPRINT_PROFILE_REQUEST_CODE = 9012;
    public static final int INITIAL_START_ZOOM = 15;
    public static final long LOCATION_DISTANCE_SPAN_METER = 20;
    public static final int LOCATION_LOOP_NUMBER = 12;
    public static final long LOCATION_PROVIDER_TIME_SPAN_MILLS = 90000;
    public static final String McGillSearchServerURL = "www.localscout.com";
    public static final int MenuChangeLocation = 13;
    public static final int MenuCurrentLocation = 12;
    public static final int MenuListView = 15;
    public static final int MenuMapView = 11;
    public static final int MenuMic = 16;
    public static final int MenuSort = 14;
    public static final int PHONE_CALL_REQUEST_CODE = 3456;
    public static final int SCREEN_WIDTH_LIMIT = 800;
    public static final int STACK_FRAME_NUMBER = 3;
    public static Hashtable<String, String> STATE_DICTIONARY = null;
    public static final int SWIPE_MAX_OFF_PATH = 250;
    public static final int SWIPE_MIN_DISTANCE = 120;
    public static final int SWIPE_THRESHOLD_VELOCITY = 200;
    public static final String TAG = "ShopLocal";
    public static final long TIME_SPAN_MILLS = 150;
    public static final double USER_LOCATION_DISTANCE_MAX = 100.0d;
    public static final long VERSION_DELAY_SPAN_MILLS = 3000;
    public static final int VOICE_RECOGNITION_REQUEST_CODE = 1234;
    public static String mDefaultHomeLocation;
    public static String mDefaultStreetLocation = null;
    public static GeoPoint mGeoPoint_DefaultHomeLocation;
    public static GeoPoint mGeoPoint_SearchHomeLocation;
    public static GeoPoint mGeoPoint_SearchLocation;
    public static boolean mIsCurrentMode = false;
    public static boolean mIsFromListView = true;
    public static boolean mIsTrackingCurrentMode = false;
    public static String mSearchHomeLocation;
    public static String mSearchKeyword = "";
    public static EditText mSearchKeyworkEditText;
    public static String mSearchLocation;
    public static EditText mSearchLocationEditText;
    public static TextView mSearchLocationTextView;
    private String INIT_START_CITY = "Santa Barbara, CA";
    private double INIT_START_LAT = 34.4234d;
    private double INIT_START_LNG = -119.70442d;
    ImageButton iBtnShortcur1;
    ImageButton iBtnShortcur10;
    ImageButton iBtnShortcur11;
    ImageButton iBtnShortcur12;
    ImageButton iBtnShortcur2;
    ImageButton iBtnShortcur3;
    ImageButton iBtnShortcur4;
    ImageButton iBtnShortcur5;
    ImageButton iBtnShortcur6;
    ImageButton iBtnShortcur7;
    ImageButton iBtnShortcur8;
    ImageButton iBtnShortcur9;
    AlertDialog internetConnectionAlertDialog;
    /* access modifiers changed from: private */
    public boolean isChange;
    boolean isRetrievingData = false;
    AlertDialog locationAlertDialog;
    AlertDialog locationNoResultAlertDialog;
    long locationUpdateSystemTime;
    ArrayList<GeoLocation> mGeoLocationList;
    private GestureDetector mGestureDetector;
    ImageButton mHomeButton;
    final Runnable mInitialShopLocalSearchTask = new Runnable() {
        public void run() {
            ShopLocalActivity.this.setRequestedOrientation(-1);
            if (ShopLocalActivity.this.mLocationProviderString == null) {
                AlertDialog alertDialog = new AlertDialog.Builder(ShopLocalActivity.this).create();
                alertDialog.setTitle("Unable To Retrieve\nCurrent Location");
                alertDialog.setMessage("To ensure the best user experience, be sure that GPS or wireless networks are enabled in location settings.");
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
                ShopLocalActivity.mIsCurrentMode = false;
            }
            if (!ShopLocalActivity.mIsCurrentMode) {
                ShopLocalActivity.this.mSplashingLayout.setVisibility(8);
                ShopLocalActivity.this.mMainLayout.setVisibility(0);
                ShopLocalActivity.this.mProgressbar.setVisibility(4);
            } else if (!ShopLocalActivity.this.mIsFirstTimeLocationUpdate) {
                if (!ShopLocalActivity.this.locationAlertDialog.isShowing()) {
                    ShopLocalActivity.this.locationAlertDialog.show();
                }
                if (ShopLocalActivity.this.mLocationLoopNumber < 12) {
                    ShopLocalActivity.this.mLocationLoopNumber++;
                    ShopLocalActivity.this.mShopLocalDataHandler.postDelayed(ShopLocalActivity.this.mInitialShopLocalSearchTask, ShopLocalActivity.VERSION_DELAY_SPAN_MILLS);
                    return;
                }
                if (ShopLocalActivity.this.locationAlertDialog.isShowing()) {
                    ShopLocalActivity.this.locationAlertDialog.dismiss();
                }
                if (!ShopLocalActivity.this.locationNoResultAlertDialog.isShowing()) {
                    ShopLocalActivity.this.locationNoResultAlertDialog.show();
                }
            } else {
                Toast.makeText(ShopLocalActivity.this.getBaseContext(), "Current Location Found", 0).show();
                if (ShopLocalActivity.this.locationAlertDialog.isShowing()) {
                    ShopLocalActivity.this.locationAlertDialog.dismiss();
                }
                if (ShopLocalActivity.this.locationNoResultAlertDialog.isShowing()) {
                    ShopLocalActivity.this.locationNoResultAlertDialog.dismiss();
                }
                ShopLocalActivity.this.mSplashingLayout.setVisibility(8);
                ShopLocalActivity.this.mMainLayout.setVisibility(0);
                ShopLocalActivity.this.mProgressbar.setVisibility(4);
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean mIsFirstTimeLocationUpdate = false;
    /* access modifiers changed from: private */
    public boolean mIsPortrait = true;
    ArrayList<String> mKeywordList;
    /* access modifiers changed from: private */
    public ListView mKeywordListView;
    /* access modifiers changed from: private */
    public Runnable mKeywordThreadRetrieveUpdateTask = new Runnable() {
        public void run() {
            if (!ShopLocalActivity.this.checkInternetConnection()) {
                if (!ShopLocalActivity.this.internetConnectionAlertDialog.isShowing()) {
                    ShopLocalActivity.this.internetConnectionAlertDialog.show();
                }
                ShopLocalActivity.this.mProgressbar.setVisibility(4);
            } else if (!ShopLocalActivity.this.isRetrievingData) {
                ShopLocalActivity.this.mKeywordListView.setEnabled(false);
                new Thread() {
                    public void run() {
                        try {
                            String searchKeyword = ShopLocalActivity.mSearchKeyworkEditText.getText().toString();
                            ShopLocalActivity.this.getResponseKeywordData(String.format("http://%s/sys/pageserver.dll?gp=%s&pa=%s&f=what&go=%s", ShopLocalActivity.McGillSearchServerURL, ShopLocalActivity.DomainDeviceID, URLEncoder.encode(ShopLocalActivity.mSearchLocation, "Latin-1").replaceAll("\\+", "%20"), URLEncoder.encode(searchKeyword, "Latin-1").replaceAll("\\+", "%20")));
                            ShopLocalActivity.this.mShopLocalDataHandler.post(ShopLocalActivity.this.mKeywordUpdateResults);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
            }
        }
    };
    final Runnable mKeywordUpdateResults = new Runnable() {
        public void run() {
            ShopLocalActivity.this.isRetrievingData = false;
            KeywordListAdapter listViewArrayAdapter = new KeywordListAdapter(ShopLocalActivity.this.mKeywordList);
            ShopLocalActivity.this.mKeywordListView.setAdapter((ListAdapter) listViewArrayAdapter);
            ShopLocalActivity.this.mKeywordListView.setOnItemClickListener(listViewArrayAdapter);
            ShopLocalActivity.this.mKeywordListView.setOnScrollListener(new AbsListView.OnScrollListener() {
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                }

                public void onScrollStateChanged(AbsListView view, int scrollState) {
                    if (scrollState > 0) {
                        ShopLocalActivity.this.hideKeyboard();
                        ShopLocalActivity.mSearchKeyworkEditText.clearFocus();
                    }
                }
            });
            ShopLocalActivity.this.mKeywordListView.setEnabled(true);
        }
    };
    /* access modifiers changed from: private */
    public ListView mLocationListView;
    int mLocationLoopNumber = 0;
    /* access modifiers changed from: private */
    public LocationManager mLocationManager;
    /* access modifiers changed from: private */
    public String mLocationProviderString = null;
    /* access modifiers changed from: private */
    public Runnable mLocationThreadRetrieveUpdateTask = new Runnable() {
        public void run() {
            if (!ShopLocalActivity.this.checkInternetConnection()) {
                if (!ShopLocalActivity.this.internetConnectionAlertDialog.isShowing()) {
                    ShopLocalActivity.this.internetConnectionAlertDialog.show();
                }
                ShopLocalActivity.this.mProgressbar.setVisibility(4);
            } else if (!ShopLocalActivity.this.isRetrievingData) {
                ShopLocalActivity.this.mLocationListView.setEnabled(false);
                new Thread() {
                    public void run() {
                        try {
                            String searchLocation = ShopLocalActivity.mSearchLocationEditText.getText().toString();
                            ShopLocalActivity.this.getResponseLocationData(String.format("http://%s/sys/pageserver.dll?gp=%s&f=W%s", ShopLocalActivity.McGillSearchServerURL, ShopLocalActivity.DomainDeviceID, URLEncoder.encode(searchLocation, "Latin-1").replaceAll("\\+", "%20")));
                            ShopLocalActivity.this.mShopLocalDataHandler.post(ShopLocalActivity.this.mLocationUpdateResults);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
            }
        }
    };
    final Runnable mLocationUpdateResults = new Runnable() {
        public void run() {
            ShopLocalActivity.this.isRetrievingData = false;
            GeoLocationAdapter gla = new GeoLocationAdapter(ShopLocalActivity.this.mGeoLocationList);
            ShopLocalActivity.this.mLocationListView.setAdapter((ListAdapter) gla);
            ShopLocalActivity.this.mLocationListView.setOnItemClickListener(gla);
            ShopLocalActivity.this.mLocationListView.setOnScrollListener(new AbsListView.OnScrollListener() {
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                }

                public void onScrollStateChanged(AbsListView view, int scrollState) {
                    if (scrollState > 0) {
                        ShopLocalActivity.this.hideKeyboard();
                        ShopLocalActivity.mSearchLocationEditText.clearFocus();
                    }
                }
            });
            ShopLocalActivity.this.mLocationListView.setEnabled(true);
            ShopLocalActivity.mSearchLocationEditText.requestFocus();
            ((InputMethodManager) ShopLocalActivity.this.getSystemService("input_method")).showSoftInput(ShopLocalActivity.mSearchLocationEditText, 2);
        }
    };
    RelativeLayout mMainLayout;
    private ImageButton mMapChangeButton;
    private ImageButton mMapMicButton;
    private TextView mMobileVersionView;
    ProgressBar mProgressbar;
    private ImageButton mSearchGoButton;
    /* access modifiers changed from: private */
    public Handler mShopLocalDataHandler = new Handler();
    LayoutInflater mShopLocalInflater;
    SharedPreferences mShopLocalPrefs;
    final Runnable mShopLocalVoiceRecognitionTask = new Runnable() {
        public void run() {
            Intent intent = new Intent("android.speech.action.RECOGNIZE_SPEECH");
            intent.putExtra("android.speech.extra.LANGUAGE_MODEL", "free_form");
            intent.putExtra("android.speech.extra.PROMPT", "Example: Restaurants in Santa Barbara, CA");
            ShopLocalActivity.this.startActivityForResult(intent, ShopLocalActivity.VOICE_RECOGNITION_REQUEST_CODE);
            ShopLocalActivity.this.mProgressbar.setVisibility(4);
        }
    };
    RelativeLayout mSplashingLayout;
    LinearLayout mainShortcutView;
    RelativeLayout mapFooterView;
    RelativeLayout mapHeaderView;
    RelativeLayout shortcurLayout1;
    RelativeLayout shortcurLayout10;
    RelativeLayout shortcurLayout11;
    RelativeLayout shortcurLayout12;
    RelativeLayout shortcurLayout2;
    RelativeLayout shortcurLayout3;
    RelativeLayout shortcurLayout4;
    RelativeLayout shortcurLayout5;
    RelativeLayout shortcurLayout6;
    RelativeLayout shortcurLayout7;
    RelativeLayout shortcurLayout8;
    RelativeLayout shortcurLayout9;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getResources().getConfiguration().orientation == 2) {
            this.mIsPortrait = false;
            getWindow().setFlags(1024, 1024);
        } else {
            this.mIsPortrait = true;
            getWindow().clearFlags(1024);
        }
        setContentView((int) R.layout.main);
        if (this.mIsPortrait) {
            setRequestedOrientation(1);
        } else {
            setRequestedOrientation(0);
        }
        this.mShopLocalPrefs = getPreferences(0);
        mDefaultHomeLocation = this.mShopLocalPrefs.getString("SLHomeLocation", this.INIT_START_CITY);
        mDefaultStreetLocation = this.mShopLocalPrefs.getString("SLHomeStreetLocation", null);
        mSearchHomeLocation = this.mShopLocalPrefs.getString("SLSearchHomeLocation", this.INIT_START_CITY);
        mGeoPoint_DefaultHomeLocation = new GeoPoint(this.mShopLocalPrefs.getInt("SLHomeLocationLatitudeE6", (int) (this.INIT_START_LAT * 1000000.0d)), this.mShopLocalPrefs.getInt("SLHomeLocationLongitudeE6", (int) (this.INIT_START_LNG * 1000000.0d)));
        mGeoPoint_SearchHomeLocation = new GeoPoint(this.mShopLocalPrefs.getInt("SLSearchHomeLocationLatitudeE6", (int) (this.INIT_START_LAT * 1000000.0d)), this.mShopLocalPrefs.getInt("SLSearchHomeLocationLongitudeE6", (int) (this.INIT_START_LNG * 1000000.0d)));
        mSearchKeyword = this.mShopLocalPrefs.getString("SLSearchKeyword", "Restaurants");
        mSearchLocation = this.mShopLocalPrefs.getString("SLSearchLocation", mSearchHomeLocation);
        mGeoPoint_SearchLocation = new GeoPoint(this.mShopLocalPrefs.getInt("SLSearchLocationLatitudeE6", mGeoPoint_SearchHomeLocation.getLatitudeE6()), this.mShopLocalPrefs.getInt("SLSearchLocationLongitudeE6", mGeoPoint_SearchHomeLocation.getLongitudeE6()));
        mIsCurrentMode = false;
        this.mIsFirstTimeLocationUpdate = false;
        this.locationUpdateSystemTime = 0;
        this.mLocationLoopNumber = 0;
        this.locationAlertDialog = new AlertDialog.Builder(this).setTitle("Acquiring Current Location").setView(View.inflate(this, R.layout.progressbar_spin_location, null)).setPositiveButton("Choose Location", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                ShopLocalActivity.this.mShopLocalDataHandler.removeCallbacks(ShopLocalActivity.this.mInitialShopLocalSearchTask);
                ShopLocalActivity.this.mLocationManager.removeUpdates(ShopLocalActivity.this);
                ShopLocalActivity.this.mSplashingLayout.setVisibility(8);
                ShopLocalActivity.this.mMainLayout.setVisibility(0);
                ShopLocalActivity.this.mProgressbar.setVisibility(4);
                ShopLocalActivity.this.isChange = true;
                ShopLocalActivity.this.changeSearchLocation();
            }
        }).create();
        this.locationNoResultAlertDialog = new AlertDialog.Builder(this).setView(View.inflate(this, R.layout.custom_dialog_message, null)).setPositiveButton("Choose Location", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                ShopLocalActivity.this.mShopLocalDataHandler.removeCallbacks(ShopLocalActivity.this.mInitialShopLocalSearchTask);
                ShopLocalActivity.this.mLocationManager.removeUpdates(ShopLocalActivity.this);
                ShopLocalActivity.this.mSplashingLayout.setVisibility(8);
                ShopLocalActivity.this.mMainLayout.setVisibility(0);
                ShopLocalActivity.this.mProgressbar.setVisibility(4);
                ShopLocalActivity.this.isChange = true;
                ShopLocalActivity.this.changeSearchLocation();
            }
        }).create();
        this.internetConnectionAlertDialog = new AlertDialog.Builder(this).setTitle("Network Connection Required").setMessage("Ensure that airplane mode is turned off or Wi-Fi is enabled.").setIcon((int) R.drawable.ic_bullet_key_permission).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create();
        try {
            this.mLocationManager = (LocationManager) getSystemService("location");
            this.mLocationProviderString = getLocationProvider(this.mLocationManager);
            this.mLocationManager.requestLocationUpdates(this.mLocationProviderString, 0, 0.0f, this);
        } catch (Exception e) {
        }
        onInitialization();
        if (!checkInternetConnection()) {
            new AlertDialog.Builder(this).setTitle("Network Connection Required").setMessage("Ensure that airplane mode is turned off or Wi-Fi is enabled.").setIcon((int) R.drawable.ic_bullet_key_permission).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    ShopLocalActivity.this.finish();
                }
            }).create().show();
            this.mProgressbar.setVisibility(4);
            return;
        }
        this.mShopLocalDataHandler.postDelayed(this.mInitialShopLocalSearchTask, VERSION_DELAY_SPAN_MILLS);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        try {
            this.mLocationManager.removeUpdates(this);
        } catch (Exception e) {
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        try {
            this.mLocationManager.removeUpdates(this);
            this.mLocationProviderString = getLocationProvider(this.mLocationManager);
            this.mLocationManager.requestLocationUpdates(this.mLocationProviderString, 0, 0.0f, this);
        } catch (Exception e) {
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        try {
            this.mLocationManager.removeUpdates(this);
        } catch (Exception e) {
        }
        super.onDestroy();
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        populateMenu(menu);
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        return applyMenuChoice(item) || super.onOptionsItemSelected(item);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        if (getResources().getConfiguration().orientation == 2) {
            this.mIsPortrait = false;
            getWindow().setFlags(1024, 1024);
        } else {
            getWindow().clearFlags(1024);
            this.mIsPortrait = true;
        }
        if (((WindowManager) getSystemService("window")).getDefaultDisplay().getWidth() >= 800 || this.mIsPortrait) {
            this.mMapChangeButton.setVisibility(0);
        } else {
            this.mMapChangeButton.setVisibility(8);
        }
        resetFooterView();
        super.onConfigurationChanged(newConfig);
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        SharedPreferences.Editor editor = this.mShopLocalPrefs.edit();
        editor.putString("SLHomeLocation", mDefaultHomeLocation);
        editor.putString("SLHomeStreetLocation", mDefaultStreetLocation);
        editor.putInt("SLHomeLocationLatitudeE6", mGeoPoint_DefaultHomeLocation.getLatitudeE6());
        editor.putInt("SLHomeLocationLongitudeE6", mGeoPoint_DefaultHomeLocation.getLongitudeE6());
        editor.putString("SLSearchHomeLocation", mSearchHomeLocation);
        editor.putInt("SLSearchHomeLocationLatitudeE6", mGeoPoint_SearchHomeLocation.getLatitudeE6());
        editor.putInt("SLSearchHomeLocationLongitudeE6", mGeoPoint_SearchHomeLocation.getLongitudeE6());
        editor.putString("SLSearchLocation", mSearchLocation);
        editor.putInt("SLSearchLocationLatitudeE6", mGeoPoint_SearchLocation.getLatitudeE6());
        editor.putInt("SLSearchLocationLongitudeE6", mGeoPoint_SearchLocation.getLongitudeE6());
        editor.putString("SLSearchKeyword", mSearchKeyworkEditText.getText().toString());
        editor.putBoolean("SLIsCurrentMode", mIsCurrentMode);
        editor.commit();
        super.onSaveInstanceState(savedInstanceState);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            if (this.mKeywordListView.getVisibility() == 0) {
                this.mKeywordListView.setVisibility(4);
                resetFooterView();
                return true;
            }
        } else if (keyCode == 84) {
            SharedPreferences.Editor editor = this.mShopLocalPrefs.edit();
            editor.putString("SLSearchKeyword", mSearchKeyworkEditText.getText().toString());
            editor.commit();
            imprintSearch();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public String getLocationProvider(LocationManager locationManager) {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(1);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(1);
        return locationManager.getBestProvider(criteria, true);
    }

    public void onLocationChanged(Location location) {
        String localCity;
        setRequestedOrientation(-1);
        if (location != null) {
            long currentUpdateSystemTime = System.currentTimeMillis();
            if (currentUpdateSystemTime - this.locationUpdateSystemTime >= LOCATION_PROVIDER_TIME_SPAN_MILLS) {
                this.locationUpdateSystemTime = currentUpdateSystemTime;
                if (!mIsCurrentMode) {
                    this.mLocationManager.removeUpdates(this);
                } else {
                    if (!mIsTrackingCurrentMode) {
                        this.mLocationManager.removeUpdates(this);
                    }
                    if (!this.mIsFirstTimeLocationUpdate) {
                        if (mIsTrackingCurrentMode) {
                            try {
                                this.mLocationManager.removeUpdates(this);
                                this.mLocationProviderString = getLocationProvider(this.mLocationManager);
                                this.mLocationManager.requestLocationUpdates(this.mLocationProviderString, (long) LOCATION_PROVIDER_TIME_SPAN_MILLS, 20.0f, this);
                            } catch (Exception e) {
                            }
                        }
                        this.mIsFirstTimeLocationUpdate = true;
                    }
                }
                double lat = location.getLatitude();
                double lng = location.getLongitude();
                int latE6 = (int) (1000000.0d * lat);
                int longE6 = (int) (1000000.0d * lng);
                if (!((mGeoPoint_DefaultHomeLocation != null && mGeoPoint_DefaultHomeLocation.getLatitudeE6() == latE6 && mGeoPoint_DefaultHomeLocation.getLongitudeE6() == longE6) || (localCity = findCityAddress(lat, lng)) == null || localCity.length() <= 0)) {
                    mGeoPoint_DefaultHomeLocation = new GeoPoint(latE6, longE6);
                    int indexDelimiter = localCity.indexOf("~~");
                    if (indexDelimiter < 1) {
                        mDefaultHomeLocation = localCity;
                        mDefaultStreetLocation = null;
                    } else {
                        mDefaultHomeLocation = localCity.substring(indexDelimiter + 2);
                        mDefaultStreetLocation = localCity.substring(0, indexDelimiter);
                    }
                    SharedPreferences.Editor editor = this.mShopLocalPrefs.edit();
                    editor.putString("SLHomeLocation", mDefaultHomeLocation);
                    editor.putString("SLHomeStreetLocation", mDefaultStreetLocation);
                    editor.putInt("SLHomeLocationLatitudeE6", mGeoPoint_DefaultHomeLocation.getLatitudeE6());
                    editor.putInt("SLHomeLocationLongitudeE6", mGeoPoint_DefaultHomeLocation.getLongitudeE6());
                    editor.commit();
                }
                if (mIsCurrentMode) {
                    String str = mDefaultHomeLocation;
                    mSearchHomeLocation = str;
                    mSearchLocation = str;
                    mGeoPoint_SearchHomeLocation = new GeoPoint(mGeoPoint_DefaultHomeLocation.getLatitudeE6(), mGeoPoint_DefaultHomeLocation.getLongitudeE6());
                    mGeoPoint_SearchLocation = new GeoPoint(mGeoPoint_DefaultHomeLocation.getLatitudeE6(), mGeoPoint_DefaultHomeLocation.getLongitudeE6());
                    SharedPreferences.Editor editor2 = this.mShopLocalPrefs.edit();
                    editor2.putString("SLSearchHomeLocation", mSearchHomeLocation);
                    editor2.putInt("SLSearchHomeLocationLatitudeE6", mGeoPoint_SearchHomeLocation.getLatitudeE6());
                    editor2.putInt("SLSearchHomeLocationLongitudeE6", mGeoPoint_SearchHomeLocation.getLongitudeE6());
                    editor2.putString("SLSearchLocation", mSearchLocation);
                    editor2.putInt("SLSearchLocationLatitudeE6", mGeoPoint_SearchLocation.getLatitudeE6());
                    editor2.putInt("SLSearchLocationLongitudeE6", mGeoPoint_SearchLocation.getLongitudeE6());
                    editor2.commit();
                    mSearchLocationEditText.setText(mSearchHomeLocation);
                    mSearchLocationTextView.setText(mSearchLocation);
                }
            }
        }
    }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public void populateMenu(Menu menu) {
        menu.setQwertyMode(true);
        MenuItem itemMyLocation = menu.add(0, 12, 0, "My Location");
        itemMyLocation.setAlphabeticShortcut('q');
        itemMyLocation.setIcon((int) R.drawable.menu_mylocation);
        MenuItem itemChangeLocation = menu.add(0, 13, 1, "Change Location");
        itemChangeLocation.setAlphabeticShortcut('p');
        itemChangeLocation.setIcon(17301555);
        MenuItem itemMic = menu.add(0, 16, 2, "Record");
        itemMic.setAlphabeticShortcut('r');
        itemMic.setIcon((int) R.drawable.menu_mic);
    }

    public boolean applyMenuChoice(MenuItem item) {
        switch (item.getItemId()) {
            case 12:
                goMyLocation();
                return true;
            case MenuChangeLocation /*13*/:
                this.isChange = true;
                this.mMapChangeButton.performClick();
                return true;
            case MenuSort /*14*/:
            case 15:
            default:
                return false;
            case MenuMic /*16*/:
                this.mMapMicButton.performClick();
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        GeoLocation locationWithCurrentState;
        if (requestCode == 1234 && resultCode == -1) {
            String recordedText = data.getStringArrayListExtra("android.speech.extra.RESULTS").get(0).replaceAll(" +", " ");
            String lowerRecordedText = recordedText.toLowerCase();
            String[] splitProps = {" in ", " near ", " at ", " by "};
            int largestIndexOfProp = -1;
            int wordIndex = -1;
            for (int i = 0; i < splitProps.length; i++) {
                int localindexOfProp = lowerRecordedText.lastIndexOf(splitProps[i]);
                if (localindexOfProp > largestIndexOfProp) {
                    largestIndexOfProp = localindexOfProp;
                    wordIndex = i;
                }
            }
            if (largestIndexOfProp > -1) {
                String keyProp = splitProps[wordIndex];
                String prefixKeyWord = recordedText.substring(0, largestIndexOfProp).trim();
                if (prefixKeyWord.length() > 0) {
                    mSearchKeyword = prefixKeyWord;
                }
                String localSearchLocation = recordedText.substring(keyProp.length() + largestIndexOfProp).trim();
                try {
                    GeoLocation firstLocation = findCityGeoLocation(localSearchLocation);
                    if (firstLocation != null) {
                        if (!isStateNameInside(localSearchLocation) && (locationWithCurrentState = findCityGeoLocation(String.valueOf(localSearchLocation) + " " + mDefaultHomeLocation.substring(mDefaultHomeLocation.indexOf(",") + 1))) != null) {
                            firstLocation = locationWithCurrentState;
                        }
                        mSearchLocation = firstLocation.getTitle();
                        double centerLatitude = firstLocation.getLat();
                        double centerLongitude = firstLocation.getLng();
                        boolean isSearchHomeChanged = false;
                        if (calculateDistance(((double) mGeoPoint_SearchHomeLocation.getLatitudeE6()) / 1000000.0d, ((double) mGeoPoint_SearchHomeLocation.getLongitudeE6()) / 1000000.0d, centerLatitude, centerLongitude) > 100.0d) {
                            isSearchHomeChanged = true;
                        }
                        if (isSearchHomeChanged) {
                            mSearchHomeLocation = mSearchLocation;
                            mGeoPoint_SearchHomeLocation = new GeoPoint((int) (1000000.0d * centerLatitude), (int) (1000000.0d * centerLongitude));
                        }
                        mGeoPoint_SearchLocation = new GeoPoint((int) (1000000.0d * centerLatitude), (int) (1000000.0d * centerLongitude));
                        mSearchLocationEditText.setText(mSearchLocation);
                        mSearchLocationTextView.setText(mSearchLocation);
                        mIsCurrentMode = false;
                        SharedPreferences.Editor editor = this.mShopLocalPrefs.edit();
                        editor.putString("SLSearchHomeLocation", mSearchHomeLocation);
                        editor.putInt("SLSearchHomeLocationLatitudeE6", mGeoPoint_SearchHomeLocation.getLatitudeE6());
                        editor.putInt("SLSearchHomeLocationLongitudeE6", mGeoPoint_SearchHomeLocation.getLongitudeE6());
                        editor.putString("SLSearchLocation", mSearchLocation);
                        editor.putInt("SLSearchLocationLatitudeE6", mGeoPoint_SearchLocation.getLatitudeE6());
                        editor.putInt("SLSearchLocationLongitudeE6", mGeoPoint_SearchLocation.getLongitudeE6());
                        editor.putBoolean("SLIsCurrentMode", mIsCurrentMode);
                        editor.commit();
                    } else {
                        mSearchKeyword = recordedText;
                    }
                } catch (Exception e) {
                }
            } else {
                int minimumNumber = 0;
                if (isStateNameInside(lowerRecordedText)) {
                    minimumNumber = 1;
                }
                boolean foundLocation = false;
                if (minimumNumber > 0) {
                    String[] splitwORDS = lowerRecordedText.split(" ");
                    ArrayList<String> addressWords = new ArrayList<>();
                    for (int i2 = splitwORDS.length - 1; i2 >= minimumNumber; i2--) {
                        for (int j = 0; j < splitwORDS.length; j++) {
                            String temp = "";
                            int lastIndex = j + i2;
                            if (lastIndex >= splitwORDS.length) {
                                break;
                            }
                            for (int k = j; k <= lastIndex; k++) {
                                temp = String.valueOf(temp) + " " + splitwORDS[k];
                            }
                            addressWords.add(temp.trim());
                        }
                    }
                    int m = 0;
                    while (true) {
                        if (m >= addressWords.size()) {
                            break;
                        }
                        String anAddressWord = (String) addressWords.get(m);
                        GeoLocation firstLocation2 = findCityGeoLocation(anAddressWord);
                        if (firstLocation2 != null) {
                            foundLocation = true;
                            int indexOfAddress = lowerRecordedText.indexOf(anAddressWord);
                            String prefixKeyWord2 = recordedText.substring(0, indexOfAddress).trim();
                            String suffixKeyWord = recordedText.substring(anAddressWord.length() + indexOfAddress);
                            if (suffixKeyWord.length() > 0) {
                                prefixKeyWord2 = (String.valueOf(prefixKeyWord2) + " " + suffixKeyWord.trim()).trim();
                            }
                            if (prefixKeyWord2.length() > 0) {
                                mSearchKeyword = prefixKeyWord2;
                            }
                            mSearchLocation = firstLocation2.getTitle();
                            double centerLatitude2 = firstLocation2.getLat();
                            double centerLongitude2 = firstLocation2.getLng();
                            boolean isSearchHomeChanged2 = false;
                            if (calculateDistance(((double) mGeoPoint_SearchHomeLocation.getLatitudeE6()) / 1000000.0d, ((double) mGeoPoint_SearchHomeLocation.getLongitudeE6()) / 1000000.0d, centerLatitude2, centerLongitude2) > 100.0d) {
                                isSearchHomeChanged2 = true;
                            }
                            if (isSearchHomeChanged2) {
                                mSearchHomeLocation = mSearchLocation;
                                mGeoPoint_SearchHomeLocation = new GeoPoint((int) (1000000.0d * centerLatitude2), (int) (1000000.0d * centerLongitude2));
                            }
                            mGeoPoint_SearchLocation = new GeoPoint((int) (1000000.0d * centerLatitude2), (int) (1000000.0d * centerLongitude2));
                            mSearchLocationEditText.setText(mSearchLocation);
                            mSearchLocationTextView.setText(mSearchLocation);
                            mIsCurrentMode = false;
                            SharedPreferences.Editor editor2 = this.mShopLocalPrefs.edit();
                            editor2.putString("SLSearchHomeLocation", mSearchHomeLocation);
                            editor2.putInt("SLSearchHomeLocationLatitudeE6", mGeoPoint_SearchHomeLocation.getLatitudeE6());
                            editor2.putInt("SLSearchHomeLocationLongitudeE6", mGeoPoint_SearchHomeLocation.getLongitudeE6());
                            editor2.putString("SLSearchLocation", mSearchLocation);
                            editor2.putInt("SLSearchLocationLatitudeE6", mGeoPoint_SearchLocation.getLatitudeE6());
                            editor2.putInt("SLSearchLocationLongitudeE6", mGeoPoint_SearchLocation.getLongitudeE6());
                            editor2.putBoolean("SLIsCurrentMode", mIsCurrentMode);
                            editor2.commit();
                        } else {
                            m++;
                        }
                    }
                }
                if (!foundLocation) {
                    mSearchKeyword = recordedText;
                }
            }
            mSearchKeyworkEditText.setText(mSearchKeyword);
            SharedPreferences.Editor editor3 = this.mShopLocalPrefs.edit();
            editor3.putString("SLSearchKeyword", mSearchKeyword);
            editor3.commit();
            imprintSearch();
        }
    }

    private void onInitialization() {
        this.mShopLocalInflater = (LayoutInflater) getSystemService("layout_inflater");
        addContentView(this.mShopLocalInflater.inflate((int) R.layout.progressbar_spin, (ViewGroup) null), new RelativeLayout.LayoutParams(-1, -1));
        this.mProgressbar = (ProgressBar) findViewById(R.id.progressbar_large);
        RelativeLayout.LayoutParams lp1 = (RelativeLayout.LayoutParams) this.mProgressbar.getLayoutParams();
        lp1.addRule(13, -1);
        lp1.addRule(12, 0);
        lp1.bottomMargin = 0;
        this.mProgressbar.setLayoutParams(lp1);
        this.mProgressbar.setVisibility(4);
        createStateDictionary();
        mSearchKeyworkEditText = (EditText) findViewById(R.id.search_keyword_textfield);
        mSearchLocationEditText = (EditText) findViewById(R.id.search_location_textField);
        mSearchLocationTextView = (TextView) findViewById(R.id.search_location);
        this.mSplashingLayout = (RelativeLayout) findViewById(R.id.splashing_view);
        this.mMobileVersionView = (TextView) findViewById(R.id.mobile_version);
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo("com.santabarbarayp.www", 128);
            this.mMobileVersionView.setText(String.format("Version %s", pInfo.versionName));
        } catch (Exception e) {
        }
        this.mMainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        this.mapHeaderView = (RelativeLayout) findViewById(R.id.header_view);
        this.mapFooterView = (RelativeLayout) findViewById(R.id.footer_view);
        this.mainShortcutView = (LinearLayout) findViewById(R.id.mainShortcutView);
        this.shortcurLayout1 = (RelativeLayout) findViewById(R.id.shortcurLayout1);
        this.shortcurLayout2 = (RelativeLayout) findViewById(R.id.shortcurLayout2);
        this.shortcurLayout3 = (RelativeLayout) findViewById(R.id.shortcurLayout3);
        this.shortcurLayout4 = (RelativeLayout) findViewById(R.id.shortcurLayout4);
        this.shortcurLayout5 = (RelativeLayout) findViewById(R.id.shortcurLayout5);
        this.shortcurLayout6 = (RelativeLayout) findViewById(R.id.shortcurLayout6);
        this.shortcurLayout7 = (RelativeLayout) findViewById(R.id.shortcurLayout7);
        this.shortcurLayout8 = (RelativeLayout) findViewById(R.id.shortcurLayout8);
        this.shortcurLayout9 = (RelativeLayout) findViewById(R.id.shortcurLayout9);
        this.shortcurLayout10 = (RelativeLayout) findViewById(R.id.shortcurLayout10);
        this.shortcurLayout11 = (RelativeLayout) findViewById(R.id.shortcurLayout11);
        this.shortcurLayout12 = (RelativeLayout) findViewById(R.id.shortcurLayout12);
        this.iBtnShortcur1 = (ImageButton) findViewById(R.id.iBtnShortcur1);
        this.iBtnShortcur2 = (ImageButton) findViewById(R.id.iBtnShortcur2);
        this.iBtnShortcur3 = (ImageButton) findViewById(R.id.iBtnShortcur3);
        this.iBtnShortcur4 = (ImageButton) findViewById(R.id.iBtnShortcur4);
        this.iBtnShortcur5 = (ImageButton) findViewById(R.id.iBtnShortcur5);
        this.iBtnShortcur6 = (ImageButton) findViewById(R.id.iBtnShortcur6);
        this.iBtnShortcur7 = (ImageButton) findViewById(R.id.iBtnShortcur7);
        this.iBtnShortcur8 = (ImageButton) findViewById(R.id.iBtnShortcur8);
        this.iBtnShortcur9 = (ImageButton) findViewById(R.id.iBtnShortcur9);
        this.iBtnShortcur10 = (ImageButton) findViewById(R.id.iBtnShortcur10);
        this.iBtnShortcur11 = (ImageButton) findViewById(R.id.iBtnShortcur11);
        this.iBtnShortcur12 = (ImageButton) findViewById(R.id.iBtnShortcur12);
        this.mGestureDetector = new GestureDetector(this);
        this.mMapMicButton = (ImageButton) findViewById(R.id.mapmicbutton);
        this.mSearchGoButton = (ImageButton) findViewById(R.id.searchgobutton);
        this.mMapChangeButton = (ImageButton) findViewById(R.id.mapchangebutton);
        this.mHomeButton = (ImageButton) findViewById(R.id.homebutton);
        this.mLocationListView = (ListView) findViewById(R.id.location_listView);
        this.mKeywordListView = (ListView) findViewById(R.id.keyword_listView);
        this.iBtnShortcur1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShopLocalActivity.mSearchKeyworkEditText.setText("Attorneys");
                ShopLocalActivity.this.imprintSearch();
            }
        });
        this.iBtnShortcur1.setOnTouchListener(this);
        this.iBtnShortcur2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShopLocalActivity.mSearchKeyworkEditText.setText("Auto Repair");
                ShopLocalActivity.this.imprintSearch();
            }
        });
        this.iBtnShortcur2.setOnTouchListener(this);
        this.iBtnShortcur3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShopLocalActivity.mSearchKeyworkEditText.setText("Coffee");
                ShopLocalActivity.this.imprintSearch();
            }
        });
        this.iBtnShortcur3.setOnTouchListener(this);
        this.iBtnShortcur4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShopLocalActivity.mSearchKeyworkEditText.setText("Grocery");
                ShopLocalActivity.this.imprintSearch();
            }
        });
        this.iBtnShortcur4.setOnTouchListener(this);
        this.iBtnShortcur5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShopLocalActivity.mSearchKeyworkEditText.setText("Hotels");
                ShopLocalActivity.this.imprintSearch();
            }
        });
        this.iBtnShortcur5.setOnTouchListener(this);
        this.iBtnShortcur6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShopLocalActivity.mSearchKeyworkEditText.setText("Locksmiths");
                ShopLocalActivity.this.imprintSearch();
            }
        });
        this.iBtnShortcur6.setOnTouchListener(this);
        this.iBtnShortcur7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShopLocalActivity.mSearchKeyworkEditText.setText("Nightclubs");
                ShopLocalActivity.this.imprintSearch();
            }
        });
        this.iBtnShortcur7.setOnTouchListener(this);
        this.iBtnShortcur8.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShopLocalActivity.mSearchKeyworkEditText.setText("Pizza");
                ShopLocalActivity.this.imprintSearch();
            }
        });
        this.iBtnShortcur8.setOnTouchListener(this);
        this.iBtnShortcur9.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShopLocalActivity.mSearchKeyworkEditText.setText("Plumbers");
                ShopLocalActivity.this.imprintSearch();
            }
        });
        this.iBtnShortcur9.setOnTouchListener(this);
        this.iBtnShortcur10.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShopLocalActivity.mSearchKeyworkEditText.setText("Restaurants");
                ShopLocalActivity.this.imprintSearch();
            }
        });
        this.iBtnShortcur10.setOnTouchListener(this);
        this.iBtnShortcur11.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShopLocalActivity.mSearchKeyworkEditText.setText("Taxis");
                ShopLocalActivity.this.imprintSearch();
            }
        });
        this.iBtnShortcur11.setOnTouchListener(this);
        this.iBtnShortcur12.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShopLocalActivity.mSearchKeyworkEditText.setText("Towing");
                ShopLocalActivity.this.imprintSearch();
            }
        });
        this.iBtnShortcur12.setOnTouchListener(this);
        this.mKeywordListView.setVisibility(4);
        mSearchLocationEditText.setText(mSearchHomeLocation);
        mSearchLocationTextView.setText(mSearchLocation);
        mSearchKeyworkEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    ShopLocalActivity.this.mKeywordListView.setVisibility(0);
                    if (ShopLocalActivity.this.mIsPortrait) {
                        ShopLocalActivity.this.mapFooterView.setVisibility(4);
                    }
                    ShopLocalActivity.mSearchKeyworkEditText.selectAll();
                    ShopLocalActivity.this.mShopLocalDataHandler.post(ShopLocalActivity.this.mKeywordThreadRetrieveUpdateTask);
                }
            }
        });
        mSearchKeyworkEditText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                Selection.setSelection(s, s.length());
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ShopLocalActivity.this.mShopLocalDataHandler.removeCallbacks(ShopLocalActivity.this.mKeywordThreadRetrieveUpdateTask);
                ShopLocalActivity.this.mShopLocalDataHandler.postDelayed(ShopLocalActivity.this.mKeywordThreadRetrieveUpdateTask, 150);
            }
        });
        mSearchKeyworkEditText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != 0 || keyCode != 66) {
                    return false;
                }
                SharedPreferences.Editor editor = ShopLocalActivity.this.mShopLocalPrefs.edit();
                editor.putString("SLSearchKeyword", ShopLocalActivity.mSearchKeyworkEditText.getText().toString());
                editor.commit();
                ShopLocalActivity.this.imprintSearch();
                return true;
            }
        });
        mSearchKeyworkEditText.setText(mSearchKeyword);
        mSearchLocationEditText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                Selection.setSelection(s, s.length());
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s == null || s.length() == 0) {
                    ShopLocalActivity.this.mGeoLocationList = new ArrayList<>();
                    ShopLocalActivity.this.mLocationListView.setAdapter((ListAdapter) new GeoLocationAdapter(ShopLocalActivity.this.mGeoLocationList));
                    return;
                }
                int beforeKeycode = 32;
                if (s.length() > 1) {
                    beforeKeycode = s.charAt(s.length() - 2);
                }
                int lastKeyCode = s.charAt(s.length() - 1);
                if (lastKeyCode > 122 || lastKeyCode < 32 || ((lastKeyCode > 90 && lastKeyCode < 97) || ((lastKeyCode > 57 && lastKeyCode < 65) || ((lastKeyCode > 44 && lastKeyCode < 48) || (lastKeyCode > 32 && lastKeyCode < 44))))) {
                    ShopLocalActivity.mSearchLocationEditText.setText(s.subSequence(0, s.length() - 1));
                    return;
                }
                if (beforeKeycode == 32) {
                    if (lastKeyCode == 32) {
                        if (s.length() > 1) {
                            ShopLocalActivity.mSearchLocationEditText.setText(s.subSequence(0, s.length() - 2));
                            return;
                        } else {
                            ShopLocalActivity.mSearchLocationEditText.setText(s.subSequence(0, s.length() - 1));
                            return;
                        }
                    } else if (lastKeyCode == 44) {
                        ShopLocalActivity.mSearchLocationEditText.setText(s.subSequence(0, s.length() - 1));
                        return;
                    }
                }
                if (beforeKeycode == 44 && (lastKeyCode == 32 || lastKeyCode == 44)) {
                    ShopLocalActivity.mSearchLocationEditText.setText(s.subSequence(0, s.length() - 1));
                } else if (beforeKeycode == 32 && lastKeyCode >= 97 && lastKeyCode <= 122) {
                    ShopLocalActivity.mSearchLocationEditText.setText(String.format("%s%c", s.subSequence(0, s.length() - 1).toString(), Character.valueOf((char) (lastKeyCode - 32))));
                } else if (beforeKeycode == 32 || lastKeyCode < 65 || lastKeyCode > 90) {
                    ShopLocalActivity.this.mShopLocalDataHandler.removeCallbacks(ShopLocalActivity.this.mLocationThreadRetrieveUpdateTask);
                    ShopLocalActivity.this.mShopLocalDataHandler.postDelayed(ShopLocalActivity.this.mLocationThreadRetrieveUpdateTask, 150);
                } else {
                    ShopLocalActivity.mSearchLocationEditText.setText(String.format("%s%c", s.subSequence(0, s.length() - 1).toString(), Character.valueOf((char) (lastKeyCode + 32))));
                }
            }
        });
        mSearchLocationEditText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                GeoLocation gl;
                if (event.getAction() != 0 || keyCode != 66) {
                    return false;
                }
                if (ShopLocalActivity.this.mGeoLocationList == null || ShopLocalActivity.this.mGeoLocationList.size() < 1) {
                    gl = new GeoLocation(ShopLocalActivity.mSearchLocation, ((double) ShopLocalActivity.mGeoPoint_SearchLocation.getLatitudeE6()) / 1000000.0d, ((double) ShopLocalActivity.mGeoPoint_SearchLocation.getLongitudeE6()) / 1000000.0d);
                } else {
                    gl = ShopLocalActivity.this.mGeoLocationList.get(0);
                }
                ShopLocalActivity.mSearchLocation = gl.getTitle();
                double centerLatitude = gl.getLat();
                double centerLongitude = gl.getLng();
                boolean isSearchHomeChanged = false;
                if (ShopLocalActivity.this.calculateDistance(((double) ShopLocalActivity.mGeoPoint_SearchHomeLocation.getLatitudeE6()) / 1000000.0d, ((double) ShopLocalActivity.mGeoPoint_SearchHomeLocation.getLongitudeE6()) / 1000000.0d, centerLatitude, centerLongitude) > 100.0d) {
                    isSearchHomeChanged = true;
                }
                if (isSearchHomeChanged) {
                    ShopLocalActivity.mSearchHomeLocation = ShopLocalActivity.mSearchLocation;
                    ShopLocalActivity.mGeoPoint_SearchHomeLocation = new GeoPoint((int) (1000000.0d * centerLatitude), (int) (1000000.0d * centerLongitude));
                }
                ShopLocalActivity.mGeoPoint_SearchLocation = new GeoPoint((int) (1000000.0d * centerLatitude), (int) (1000000.0d * centerLongitude));
                ShopLocalActivity.mSearchLocationEditText.setText(ShopLocalActivity.mSearchLocation);
                ShopLocalActivity.mSearchLocationTextView.setText(ShopLocalActivity.mSearchLocation);
                SharedPreferences.Editor editor = ShopLocalActivity.this.mShopLocalPrefs.edit();
                editor.putString("SLSearchHomeLocation", ShopLocalActivity.mSearchHomeLocation);
                editor.putInt("SLSearchHomeLocationLatitudeE6", ShopLocalActivity.mGeoPoint_SearchHomeLocation.getLatitudeE6());
                editor.putInt("SLSearchHomeLocationLongitudeE6", ShopLocalActivity.mGeoPoint_SearchHomeLocation.getLongitudeE6());
                editor.putString("SLSearchLocation", ShopLocalActivity.mSearchLocation);
                editor.putInt("SLSearchLocationLatitudeE6", ShopLocalActivity.mGeoPoint_SearchLocation.getLatitudeE6());
                editor.putInt("SLSearchLocationLongitudeE6", ShopLocalActivity.mGeoPoint_SearchLocation.getLongitudeE6());
                editor.commit();
                ShopLocalActivity.this.resetFooterView();
                return true;
            }
        });
        mSearchLocationEditText.setVisibility(4);
        this.mLocationListView.setVisibility(4);
        this.isChange = true;
        try {
            this.mMapMicButton.setOnTouchListener(this);
            PackageManager pm = getPackageManager();
            if (pm == null) {
                this.mMapMicButton.setEnabled(false);
            } else if (pm.queryIntentActivities(new Intent("android.speech.action.RECOGNIZE_SPEECH"), 0).size() > 0) {
                this.mMapMicButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        ShopLocalActivity.this.mProgressbar.setVisibility(0);
                        new Thread() {
                            public void run() {
                                ShopLocalActivity.this.mShopLocalDataHandler.post(ShopLocalActivity.this.mShopLocalVoiceRecognitionTask);
                            }
                        }.start();
                    }
                });
            } else {
                this.mMapMicButton.setEnabled(false);
            }
        } catch (Exception e2) {
        }
        this.mHomeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShopLocalActivity.this.goMyLocation();
            }
        });
        this.mHomeButton.setOnTouchListener(this);
        this.mSearchGoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShopLocalActivity.this.imprintSearch();
            }
        });
        this.mSearchGoButton.setOnTouchListener(this);
        this.mMapChangeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShopLocalActivity.this.changeSearchLocation();
            }
        });
        this.mMapChangeButton.setOnTouchListener(this);
        if (((WindowManager) getSystemService("window")).getDefaultDisplay().getWidth() >= 800 || this.mIsPortrait) {
            this.mMapChangeButton.setVisibility(0);
        } else {
            this.mMapChangeButton.setVisibility(8);
        }
        resetFooterView();
    }

    public void getResponseLocationData(String url) {
        try {
            JSONArray jsonArray = new JSONArray(convertStreamToString(new URL(url).openConnection().getInputStream()));
            this.mGeoLocationList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONArray addressArray = jsonArray.getJSONArray(i);
                this.mGeoLocationList.add(new GeoLocation(addressArray.getString(0), addressArray.getDouble(1), addressArray.getDouble(2)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public GeoLocation getFirstResponseLocationData(String url) {
        try {
            JSONArray jsonArray = new JSONArray(convertStreamToString(new URL(url).openConnection().getInputStream()));
            if (jsonArray.length() > 0) {
                JSONArray addressArray = jsonArray.getJSONArray(0);
                return new GeoLocation(addressArray.getString(0), addressArray.getDouble(1), addressArray.getDouble(2));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void getResponseKeywordData(String url) {
        try {
            JSONArray jsonArray = new JSONArray(convertStreamToString(new URL(url).openConnection().getInputStream()));
            this.mKeywordList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                this.mKeywordList.add(jsonArray.getString(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String convertStreamToString(InputStream is) {
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "ISO-8859-1"));
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        break;
                    } catch (IOException e) {
                    }
                } else {
                    sb.append(String.valueOf(line) + "\n");
                }
            }
            is.close();
        } catch (IOException e2) {
            try {
                is.close();
            } catch (IOException e3) {
            }
        } catch (Throwable th) {
            try {
                is.close();
            } catch (IOException e4) {
            }
            throw th;
        }
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public void goMyLocation() {
        this.mProgressbar.setVisibility(0);
        if (!checkInternetConnection()) {
            if (!this.internetConnectionAlertDialog.isShowing()) {
                this.internetConnectionAlertDialog.show();
            }
            this.mProgressbar.setVisibility(4);
        } else if (this.mLocationProviderString == null) {
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("Unable To Retrieve\nCurrent Location");
            alertDialog.setMessage("To ensure the best user experience, be sure that GPS or wireless networks are enabled in location settings.");
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
            this.mProgressbar.setVisibility(4);
        } else {
            resetFooterView();
            if (!this.isRetrievingData) {
                Location homeLocation = this.mLocationManager.getLastKnownLocation(this.mLocationProviderString);
                if (homeLocation == null) {
                    AlertDialog alertDialog2 = new AlertDialog.Builder(this).create();
                    alertDialog2.setTitle("Unable To Retrieve\nCurrent Location");
                    alertDialog2.setMessage("Using the last known location.");
                    alertDialog2.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog2.show();
                }
                if (homeLocation != null) {
                    try {
                        Toast.makeText(getBaseContext(), "Current Location Found", 0).show();
                        double homeLatitude = homeLocation.getLatitude();
                        double homeLongitude = homeLocation.getLongitude();
                        String localCity = findCityAddress(homeLatitude, homeLongitude);
                        if (localCity != null && localCity.length() > 0) {
                            int indexDelimiter = localCity.indexOf("~~");
                            if (indexDelimiter < 1) {
                                mDefaultHomeLocation = localCity;
                                mDefaultStreetLocation = null;
                            } else {
                                mDefaultHomeLocation = localCity.substring(indexDelimiter + 2);
                                mDefaultStreetLocation = localCity.substring(0, indexDelimiter);
                            }
                            mGeoPoint_DefaultHomeLocation = new GeoPoint((int) (1000000.0d * homeLatitude), (int) (1000000.0d * homeLongitude));
                            SharedPreferences.Editor editor = this.mShopLocalPrefs.edit();
                            editor.putString("SLHomeLocation", mDefaultHomeLocation);
                            editor.putString("SLHomeStreetLocation", mDefaultStreetLocation);
                            editor.putInt("SLHomeLocationLatitudeE6", mGeoPoint_DefaultHomeLocation.getLatitudeE6());
                            editor.putInt("SLHomeLocationLongitudeE6", mGeoPoint_DefaultHomeLocation.getLongitudeE6());
                            editor.commit();
                        }
                    } catch (Exception e) {
                        e.toString();
                        this.mProgressbar.setVisibility(4);
                    }
                }
                mSearchLocation = mDefaultHomeLocation;
                mGeoPoint_SearchLocation = new GeoPoint(mGeoPoint_DefaultHomeLocation.getLatitudeE6(), mGeoPoint_DefaultHomeLocation.getLongitudeE6());
                mSearchHomeLocation = mDefaultHomeLocation;
                mGeoPoint_SearchHomeLocation = new GeoPoint(mGeoPoint_DefaultHomeLocation.getLatitudeE6(), mGeoPoint_DefaultHomeLocation.getLongitudeE6());
                SharedPreferences.Editor editor2 = this.mShopLocalPrefs.edit();
                editor2.putString("SLSearchHomeLocation", mSearchHomeLocation);
                editor2.putInt("SLSearchHomeLocationLatitudeE6", mGeoPoint_SearchHomeLocation.getLatitudeE6());
                editor2.putInt("SLSearchHomeLocationLongitudeE6", mGeoPoint_SearchHomeLocation.getLongitudeE6());
                editor2.putString("SLSearchLocation", mSearchLocation);
                editor2.putInt("SLSearchLocationLatitudeE6", mGeoPoint_SearchLocation.getLatitudeE6());
                editor2.putInt("SLSearchLocationLongitudeE6", mGeoPoint_SearchLocation.getLongitudeE6());
                editor2.commit();
                mSearchLocationEditText.setText(mSearchHomeLocation);
                mSearchLocationTextView.setText(mSearchLocation);
                this.mProgressbar.setVisibility(4);
            } else {
                this.mProgressbar.setVisibility(4);
            }
            if (!mIsCurrentMode) {
                mIsCurrentMode = true;
                SharedPreferences.Editor editor3 = this.mShopLocalPrefs.edit();
                editor3.putBoolean("SLIsCurrentMode", mIsCurrentMode);
                editor3.commit();
                try {
                    this.mLocationManager.removeUpdates(this);
                    this.mLocationProviderString = getLocationProvider(this.mLocationManager);
                    if (mIsTrackingCurrentMode) {
                        this.mLocationManager.requestLocationUpdates(this.mLocationProviderString, (long) LOCATION_PROVIDER_TIME_SPAN_MILLS, 20.0f, this);
                    } else {
                        this.mLocationManager.requestLocationUpdates(this.mLocationProviderString, 0, 0.0f, this);
                    }
                } catch (Exception e2) {
                }
            } else if (!mIsTrackingCurrentMode) {
                try {
                    this.mLocationManager.removeUpdates(this);
                    this.mLocationProviderString = getLocationProvider(this.mLocationManager);
                    this.mLocationManager.requestLocationUpdates(this.mLocationProviderString, 0, 0.0f, this);
                } catch (Exception e3) {
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void changeSearchLocation() {
        mIsCurrentMode = false;
        SharedPreferences.Editor editor = this.mShopLocalPrefs.edit();
        editor.putBoolean("SLIsCurrentMode", mIsCurrentMode);
        editor.commit();
        if (this.isChange) {
            this.isChange = false;
            this.mMapChangeButton.setVisibility(0);
            mSearchLocationTextView.setVisibility(4);
            this.mHomeButton.setVisibility(4);
            this.mLocationListView.setVisibility(0);
            this.mLocationListView.setEnabled(false);
            this.mKeywordListView.setVisibility(4);
            this.mapFooterView.setVisibility(0);
            this.mainShortcutView.setVisibility(8);
            if (this.mIsPortrait) {
                RelativeLayout.LayoutParams lp1 = (RelativeLayout.LayoutParams) this.mapFooterView.getLayoutParams();
                lp1.addRule(10, -1);
                lp1.addRule(12, 0);
                lp1.addRule(11, 0);
                this.mapFooterView.setLayoutParams(lp1);
            }
            this.mMapChangeButton.setImageResource(R.drawable.map_cancel);
            mSearchLocationEditText.setVisibility(0);
            mSearchLocationEditText.requestFocus();
            mSearchLocationEditText.selectAll();
            this.mShopLocalDataHandler.post(this.mLocationThreadRetrieveUpdateTask);
            mSearchLocationEditText.requestFocus();
            ((InputMethodManager) getSystemService("input_method")).showSoftInput(mSearchLocationEditText, 2);
            return;
        }
        resetFooterView();
    }

    /* access modifiers changed from: private */
    public void resetFooterView() {
        this.isChange = true;
        this.mapFooterView.setVisibility(0);
        this.mainShortcutView.setVisibility(0);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        RelativeLayout.LayoutParams isvf = (RelativeLayout.LayoutParams) this.mainShortcutView.getLayoutParams();
        RelativeLayout.LayoutParams mshv = (RelativeLayout.LayoutParams) this.mapHeaderView.getLayoutParams();
        RelativeLayout.LayoutParams lpFooterview = (RelativeLayout.LayoutParams) this.mapFooterView.getLayoutParams();
        RelativeLayout.LayoutParams sket = (RelativeLayout.LayoutParams) mSearchKeyworkEditText.getLayoutParams();
        RelativeLayout.LayoutParams slet = (RelativeLayout.LayoutParams) mSearchLocationEditText.getLayoutParams();
        RelativeLayout.LayoutParams sltv = (RelativeLayout.LayoutParams) mSearchLocationTextView.getLayoutParams();
        if (this.mIsPortrait) {
            sket.width = (int) (186.0f * dm.density);
            slet.width = (int) (247.0f * dm.density);
            sltv.width = (int) (202.0f * dm.density);
            mshv.width = -1;
            lpFooterview.addRule(12, -1);
            lpFooterview.addRule(11, 0);
            lpFooterview.addRule(1, 0);
            lpFooterview.addRule(10, 0);
            isvf.addRule(2, R.id.footer_view);
        } else {
            int iScreenWidth = ((WindowManager) getSystemService("window")).getDefaultDisplay().getWidth();
            int widthOfView = ((int) (((double) (((((float) iScreenWidth) - (96.0f * dm.density)) - (44.0f * dm.density)) - (40.0f * dm.density))) - (4.7d * ((double) dm.density)))) / 2;
            if (iScreenWidth >= 800) {
                widthOfView -= (int) (30.0f * dm.density);
            }
            sket.width = widthOfView;
            slet.width = ((int) (46.7d * ((double) dm.density))) + widthOfView;
            sltv.width = widthOfView;
            mshv.width = -2;
            lpFooterview.addRule(10, -1);
            lpFooterview.addRule(1, R.id.header_view);
            lpFooterview.addRule(12, 0);
            lpFooterview.addRule(11, -1);
            isvf.addRule(2, 0);
        }
        this.mapHeaderView.setLayoutParams(mshv);
        mSearchKeyworkEditText.setLayoutParams(sket);
        mSearchLocationEditText.setLayoutParams(slet);
        mSearchLocationTextView.setLayoutParams(sltv);
        this.mapFooterView.setLayoutParams(lpFooterview);
        this.mainShortcutView.setLayoutParams(isvf);
        this.mMapChangeButton.setImageResource(R.drawable.map_change);
        mSearchLocationEditText.clearFocus();
        mSearchKeyworkEditText.clearFocus();
        hideKeyboard();
        this.mKeywordListView.setVisibility(4);
        mSearchLocationEditText.setVisibility(4);
        this.mLocationListView.setVisibility(4);
        mSearchLocationTextView.setVisibility(0);
        this.mHomeButton.setVisibility(0);
        rePositionShortcut();
    }

    private void rePositionShortcut() {
        Display display = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);
        int mScreenWidth = display.getWidth();
        int mScreenHeight = display.getHeight();
        RelativeLayout.LayoutParams shortcurRelativeLayout2 = (RelativeLayout.LayoutParams) this.shortcurLayout2.getLayoutParams();
        RelativeLayout.LayoutParams shortcurRelativeLayout3 = (RelativeLayout.LayoutParams) this.shortcurLayout3.getLayoutParams();
        RelativeLayout.LayoutParams shortcurRelativeLayout4 = (RelativeLayout.LayoutParams) this.shortcurLayout4.getLayoutParams();
        RelativeLayout.LayoutParams shortcurRelativeLayout5 = (RelativeLayout.LayoutParams) this.shortcurLayout5.getLayoutParams();
        RelativeLayout.LayoutParams shortcurRelativeLayout6 = (RelativeLayout.LayoutParams) this.shortcurLayout6.getLayoutParams();
        RelativeLayout.LayoutParams shortcurRelativeLayout7 = (RelativeLayout.LayoutParams) this.shortcurLayout7.getLayoutParams();
        RelativeLayout.LayoutParams shortcurRelativeLayout8 = (RelativeLayout.LayoutParams) this.shortcurLayout8.getLayoutParams();
        RelativeLayout.LayoutParams shortcurRelativeLayout9 = (RelativeLayout.LayoutParams) this.shortcurLayout9.getLayoutParams();
        RelativeLayout.LayoutParams shortcurRelativeLayout10 = (RelativeLayout.LayoutParams) this.shortcurLayout10.getLayoutParams();
        RelativeLayout.LayoutParams shortcurRelativeLayout11 = (RelativeLayout.LayoutParams) this.shortcurLayout11.getLayoutParams();
        if (this.mIsPortrait) {
            double tMargin = ((((double) mScreenHeight) - 126.0d) - ((double) (379.0f * dm.density))) / 3.0d;
            if (tMargin < 0.0d) {
                tMargin = 0.0d;
            }
            shortcurRelativeLayout2.addRule(10, -1);
            shortcurRelativeLayout2.addRule(14, -1);
            shortcurRelativeLayout2.leftMargin = 0;
            shortcurRelativeLayout2.addRule(1, 0);
            shortcurRelativeLayout3.addRule(10, -1);
            shortcurRelativeLayout3.addRule(11, -1);
            shortcurRelativeLayout3.leftMargin = 0;
            shortcurRelativeLayout3.addRule(1, 0);
            shortcurRelativeLayout4.addRule(10, 0);
            shortcurRelativeLayout4.addRule(9, -1);
            shortcurRelativeLayout4.addRule(11, 0);
            shortcurRelativeLayout4.topMargin = (int) tMargin;
            shortcurRelativeLayout4.addRule(3, R.id.shortcurLayout1);
            shortcurRelativeLayout5.addRule(14, -1);
            shortcurRelativeLayout5.addRule(6, R.id.shortcurLayout4);
            shortcurRelativeLayout5.addRule(15, 0);
            shortcurRelativeLayout5.addRule(9, 0);
            shortcurRelativeLayout6.addRule(5, 0);
            shortcurRelativeLayout6.addRule(15, 0);
            shortcurRelativeLayout6.addRule(6, R.id.shortcurLayout4);
            shortcurRelativeLayout6.addRule(11, -1);
            shortcurRelativeLayout7.addRule(5, 0);
            shortcurRelativeLayout7.addRule(6, 0);
            shortcurRelativeLayout7.addRule(9, -1);
            shortcurRelativeLayout7.topMargin = (int) tMargin;
            shortcurRelativeLayout7.addRule(3, R.id.shortcurLayout4);
            shortcurRelativeLayout8.addRule(14, -1);
            shortcurRelativeLayout8.addRule(6, R.id.shortcurLayout7);
            shortcurRelativeLayout8.addRule(15, 0);
            shortcurRelativeLayout8.addRule(11, 0);
            shortcurRelativeLayout9.addRule(9, 0);
            shortcurRelativeLayout9.addRule(12, 0);
            shortcurRelativeLayout9.addRule(6, R.id.shortcurLayout7);
            shortcurRelativeLayout9.addRule(11, -1);
            shortcurRelativeLayout10.addRule(5, 0);
            shortcurRelativeLayout10.addRule(9, -1);
            shortcurRelativeLayout10.addRule(12, -1);
            shortcurRelativeLayout11.addRule(5, 0);
            shortcurRelativeLayout11.addRule(14, -1);
            shortcurRelativeLayout11.addRule(12, -1);
        } else {
            double tMargin2 = (double) ((((float) mScreenWidth) - (290.0f * dm.density)) / 3.0f);
            if (tMargin2 < 0.0d) {
                tMargin2 = 0.0d;
            }
            shortcurRelativeLayout2.addRule(10, -1);
            shortcurRelativeLayout2.addRule(14, 0);
            shortcurRelativeLayout2.leftMargin = (int) tMargin2;
            shortcurRelativeLayout2.addRule(1, R.id.shortcurLayout1);
            shortcurRelativeLayout3.addRule(10, -1);
            shortcurRelativeLayout3.addRule(11, 0);
            shortcurRelativeLayout3.leftMargin = (int) tMargin2;
            shortcurRelativeLayout3.addRule(1, R.id.shortcurLayout2);
            shortcurRelativeLayout4.addRule(10, -1);
            shortcurRelativeLayout4.addRule(9, 0);
            shortcurRelativeLayout4.addRule(11, -1);
            shortcurRelativeLayout4.topMargin = 0;
            shortcurRelativeLayout4.addRule(3, 0);
            shortcurRelativeLayout5.addRule(14, 0);
            shortcurRelativeLayout5.addRule(6, 0);
            shortcurRelativeLayout5.addRule(15, -1);
            shortcurRelativeLayout5.addRule(9, -1);
            shortcurRelativeLayout6.addRule(5, R.id.shortcurLayout2);
            shortcurRelativeLayout6.addRule(15, -1);
            shortcurRelativeLayout6.addRule(6, 0);
            shortcurRelativeLayout6.addRule(11, 0);
            shortcurRelativeLayout7.addRule(5, R.id.shortcurLayout3);
            shortcurRelativeLayout7.addRule(15, -1);
            shortcurRelativeLayout7.addRule(9, 0);
            shortcurRelativeLayout7.topMargin = 0;
            shortcurRelativeLayout7.addRule(3, 0);
            shortcurRelativeLayout8.addRule(14, 0);
            shortcurRelativeLayout8.addRule(6, 0);
            shortcurRelativeLayout8.addRule(15, -1);
            shortcurRelativeLayout8.addRule(11, -1);
            shortcurRelativeLayout9.addRule(9, -1);
            shortcurRelativeLayout9.addRule(12, -1);
            shortcurRelativeLayout9.addRule(6, 0);
            shortcurRelativeLayout9.addRule(11, 0);
            shortcurRelativeLayout10.addRule(5, R.id.shortcurLayout2);
            shortcurRelativeLayout10.addRule(9, 0);
            shortcurRelativeLayout10.addRule(12, -1);
            shortcurRelativeLayout11.addRule(5, R.id.shortcurLayout3);
            shortcurRelativeLayout11.addRule(14, 0);
            shortcurRelativeLayout11.addRule(12, -1);
        }
        this.shortcurLayout2.setLayoutParams(shortcurRelativeLayout2);
        this.shortcurLayout3.setLayoutParams(shortcurRelativeLayout3);
        this.shortcurLayout4.setLayoutParams(shortcurRelativeLayout4);
        this.shortcurLayout5.setLayoutParams(shortcurRelativeLayout5);
        this.shortcurLayout6.setLayoutParams(shortcurRelativeLayout6);
        this.shortcurLayout7.setLayoutParams(shortcurRelativeLayout7);
        this.shortcurLayout8.setLayoutParams(shortcurRelativeLayout8);
        this.shortcurLayout9.setLayoutParams(shortcurRelativeLayout9);
        this.shortcurLayout10.setLayoutParams(shortcurRelativeLayout10);
        this.shortcurLayout11.setLayoutParams(shortcurRelativeLayout11);
        setRequestedOrientation(-1);
    }

    /* access modifiers changed from: private */
    public void hideKeyboard() {
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(mSearchKeyworkEditText.getWindowToken(), 0);
    }

    private void createStateDictionary() {
        STATE_DICTIONARY = new Hashtable<>();
        STATE_DICTIONARY.put("Alaska", "AK");
        STATE_DICTIONARY.put("Alabama", "AL");
        STATE_DICTIONARY.put("Arkansas", "AR");
        STATE_DICTIONARY.put("Arizona", "AZ");
        STATE_DICTIONARY.put("California", "CA");
        STATE_DICTIONARY.put("Colorado", "CO");
        STATE_DICTIONARY.put("Connecticut", "CT");
        STATE_DICTIONARY.put("District of Columbia Washington", "DC");
        STATE_DICTIONARY.put("Delaware", "DE");
        STATE_DICTIONARY.put("Florida", "FL");
        STATE_DICTIONARY.put("Georgia", "GA");
        STATE_DICTIONARY.put("Hawaii", "HI");
        STATE_DICTIONARY.put("Iowa", "IA");
        STATE_DICTIONARY.put("Idaho", "ID");
        STATE_DICTIONARY.put("Illinois", "IL");
        STATE_DICTIONARY.put("Indiana", "IN");
        STATE_DICTIONARY.put("Kansas", "KS");
        STATE_DICTIONARY.put("Kentucky", "KY");
        STATE_DICTIONARY.put("Louisiana", "LA");
        STATE_DICTIONARY.put("Massachusetts", "MA");
        STATE_DICTIONARY.put("Maine", "ME");
        STATE_DICTIONARY.put("Maryland", "MD");
        STATE_DICTIONARY.put("Michigan", "MI");
        STATE_DICTIONARY.put("Minnesota", "MN");
        STATE_DICTIONARY.put("Missouri", "MO");
        STATE_DICTIONARY.put("Montana", "MT");
        STATE_DICTIONARY.put("North Carolina", "NC");
        STATE_DICTIONARY.put("North Dakota", "ND");
        STATE_DICTIONARY.put("Nebraska", "NE");
        STATE_DICTIONARY.put("New Hampshire", "NH");
        STATE_DICTIONARY.put("New Jersey", "NJ");
        STATE_DICTIONARY.put("New Mexico", "NM");
        STATE_DICTIONARY.put("Nevada", "NV");
        STATE_DICTIONARY.put("New York", "NY");
        STATE_DICTIONARY.put("Ohio", "OH");
        STATE_DICTIONARY.put("Oklahoma", "OK");
        STATE_DICTIONARY.put("Oregon", "OR");
        STATE_DICTIONARY.put("Pennsylvania", "PA");
        STATE_DICTIONARY.put("Puerto Rico", "PR");
        STATE_DICTIONARY.put("Palau Koror", "PW");
        STATE_DICTIONARY.put("Rhode Island", "RI");
        STATE_DICTIONARY.put("South Carolina", "SC");
        STATE_DICTIONARY.put("South Dakota", "SD");
        STATE_DICTIONARY.put("Tennessee", "TN");
        STATE_DICTIONARY.put("Texas", "TX");
        STATE_DICTIONARY.put("Utah", "UT");
        STATE_DICTIONARY.put("Virgin Islands", "VI");
        STATE_DICTIONARY.put("Vermont", "VT");
        STATE_DICTIONARY.put("Virginia", "VA");
        STATE_DICTIONARY.put("Washington", "WA");
        STATE_DICTIONARY.put("Wisconsin", "WI");
        STATE_DICTIONARY.put("West Virginia", "WV");
        STATE_DICTIONARY.put("Wyoming", "WY");
        STATE_DICTIONARY.put("Alberta", "AB");
        STATE_DICTIONARY.put("British Columbia", "BC");
        STATE_DICTIONARY.put("Manitoba", "MB");
        STATE_DICTIONARY.put("New Brunswick", "NB");
        STATE_DICTIONARY.put("Newfoundland and Labrador", "NL");
        STATE_DICTIONARY.put("Northwest Territories", "NT");
        STATE_DICTIONARY.put("Nova Scotia", "NS");
        STATE_DICTIONARY.put("Nunavut", "NU");
        STATE_DICTIONARY.put("Ontario", "ON");
        STATE_DICTIONARY.put("Prince Edward Island", "PE");
        STATE_DICTIONARY.put("Quebec", "QC");
        STATE_DICTIONARY.put("Saskatchewan", "SK");
        STATE_DICTIONARY.put("Yukon", "YT");
    }

    private String findCityAddress(double latitude, double longitude) {
        List<Address> myList;
        List<Address> myList2;
        String stateAbbre;
        if (!checkInternetConnection()) {
            return null;
        }
        String cityAddress = ReverseGeocode.retrieveLocation(latitude, longitude);
        if (cityAddress != null) {
            return cityAddress;
        }
        new ArrayList();
        try {
            Geocoder myLocationGeoCoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            try {
                myList = myLocationGeoCoder.getFromLocation(latitude, longitude, 1);
            } catch (IOException e) {
                myList = ReverseGeocode.getFromLocation(latitude, longitude, 1);
            }
            for (int numbers = 0; numbers < 4; numbers++) {
                if (!myList2.isEmpty()) {
                    break;
                }
                try {
                    myList2 = myLocationGeoCoder.getFromLocation(latitude, longitude, 1);
                } catch (IOException e2) {
                    myList2 = ReverseGeocode.getFromLocation(latitude, longitude, 1);
                }
            }
            if (myList2.isEmpty()) {
                try {
                    JSONArray jsonCityArray = new JSONObject(convertStreamToString(new URL(String.format("http://%s/sys/pageserver.dll?gp=%s&pa=latlong&y=%d&x=%d&s=-20", McGillSearchServerURL, DomainDeviceID, Integer.valueOf((int) (1000000.0d * latitude)), Integer.valueOf((int) (1000000.0d * longitude)))).openConnection().getInputStream())).getJSONArray("city");
                    if (jsonCityArray.length() > 0) {
                        cityAddress = jsonCityArray.getString(0);
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
                return cityAddress;
            }
            Address firstAddrestt = myList2.get(0);
            String countryCode = firstAddrestt.getCountryCode();
            if (countryCode == null) {
                return null;
            }
            if (!countryCode.equalsIgnoreCase("US") && !countryCode.equalsIgnoreCase("CA")) {
                return null;
            }
            String streetAddress = null;
            if (firstAddrestt.getMaxAddressLineIndex() > 0) {
                streetAddress = firstAddrestt.getAddressLine(0);
            }
            cityAddress = firstAddrestt.getLocality();
            if (cityAddress == null) {
                return null;
            }
            if (streetAddress != null) {
                cityAddress = String.valueOf(streetAddress) + "~~" + cityAddress;
            }
            String statename = firstAddrestt.getAdminArea();
            if (statename != null) {
                if (statename.length() > 2 && (stateAbbre = STATE_DICTIONARY.get(statename)) != null) {
                    statename = stateAbbre;
                }
                cityAddress = String.valueOf(cityAddress) + ", " + statename;
            }
            return cityAddress;
        } catch (Exception e4) {
            e4.printStackTrace();
        }
    }

    private GeoLocation findCityGeoLocation(String locationName) {
        List<Address> myList;
        List<Address> myList2;
        String stateAbbre;
        GeoLocation cityGeoLocation = null;
        new ArrayList();
        try {
            Geocoder myLocationGeoCoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            try {
                myList = myLocationGeoCoder.getFromLocationName(locationName, 1);
            } catch (IOException e) {
                myList = ReverseGeocode.getFromLocation(locationName, 1);
            }
            for (int numbers = 0; numbers < 4; numbers++) {
                if (!myList2.isEmpty()) {
                    break;
                }
                try {
                    myList2 = myLocationGeoCoder.getFromLocationName(locationName, 1);
                } catch (IOException e2) {
                    myList2 = ReverseGeocode.getFromLocation(locationName, 1);
                }
            }
            if (myList2.isEmpty()) {
                return null;
            }
            Address firstAddrestt = myList2.get(0);
            String countryCode = firstAddrestt.getCountryCode();
            if (countryCode == null) {
                return null;
            }
            if (!countryCode.equalsIgnoreCase("US") && !countryCode.equalsIgnoreCase("CA")) {
                return null;
            }
            String cityAddress = firstAddrestt.getLocality();
            if (cityAddress == null) {
                return null;
            }
            if (!locationName.toLowerCase().contains(cityAddress.toLowerCase())) {
                return null;
            }
            String statename = firstAddrestt.getAdminArea();
            if (statename != null) {
                if (statename.length() > 2 && (stateAbbre = STATE_DICTIONARY.get(statename)) != null) {
                    statename = stateAbbre;
                }
                cityAddress = String.valueOf(cityAddress) + ", " + statename;
            }
            GeoLocation cityGeoLocation2 = new GeoLocation();
            try {
                cityGeoLocation2.setTitle(cityAddress);
                cityGeoLocation2.setLat(firstAddrestt.getLatitude());
                cityGeoLocation2.setLng(firstAddrestt.getLongitude());
                cityGeoLocation = cityGeoLocation2;
            } catch (Exception e3) {
                cityGeoLocation = cityGeoLocation2;
            }
            return cityGeoLocation;
        } catch (Exception e4) {
        }
    }

    private boolean isStateNameInside(String locationName) {
        Enumeration<String> e = STATE_DICTIONARY.keys();
        while (e.hasMoreElements()) {
            if (locationName.indexOf(e.nextElement().toLowerCase()) > -1) {
                return true;
            }
        }
        return false;
    }

    class GeoLocationAdapter extends BaseAdapter implements AdapterView.OnItemClickListener {
        private final LayoutInflater mInflater;
        private final List<GeoLocation> mItems;

        public GeoLocationAdapter(List<GeoLocation> items) {
            this.mItems = items;
            this.mInflater = (LayoutInflater) ShopLocalActivity.this.getSystemService("layout_inflater");
        }

        public int getCount() {
            if (this.mItems == null) {
                return 0;
            }
            return this.mItems.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            GeoLocation gl;
            View v = convertView;
            if (v == null) {
                v = this.mInflater.inflate((int) R.layout.location_list_row, (ViewGroup) null);
            }
            if (position < this.mItems.size() && (gl = this.mItems.get(position)) != null) {
                String inputstring = ShopLocalActivity.mSearchLocationEditText.getText().toString().toLowerCase();
                String inputstringCondensed = inputstring.replaceAll(" ", "").replaceAll(",", "");
                String lowercaseTitle = gl.getTitle().toLowerCase();
                if (lowercaseTitle.replaceAll(" ", "").replaceAll(",", "").startsWith(inputstringCondensed)) {
                    int startIndex = 0;
                    for (int j = 0; j < inputstringCondensed.length() && startIndex < lowercaseTitle.length(); j++) {
                        int oneChar = lowercaseTitle.charAt(startIndex);
                        while (true) {
                            if ((oneChar == 32 || oneChar == 44) && startIndex < lowercaseTitle.length()) {
                                startIndex++;
                                oneChar = lowercaseTitle.charAt(startIndex);
                            }
                        }
                        if (oneChar != inputstringCondensed.charAt(j)) {
                            break;
                        }
                        startIndex++;
                    }
                    int partIndex = 0;
                    for (int j2 = 0; j2 < inputstringCondensed.length() && partIndex < inputstring.length(); j2++) {
                        int oneChar2 = inputstring.charAt(partIndex);
                        while (true) {
                            if ((oneChar2 == 32 || oneChar2 == 44) && partIndex < inputstring.length()) {
                                partIndex++;
                                oneChar2 = inputstring.charAt(partIndex);
                            }
                        }
                        if (oneChar2 != inputstringCondensed.charAt(j2)) {
                            break;
                        }
                        partIndex++;
                    }
                    while (partIndex < inputstring.length() && startIndex < lowercaseTitle.length() && inputstring.charAt(partIndex) == lowercaseTitle.charAt(startIndex)) {
                        partIndex++;
                        startIndex++;
                    }
                    TextView tt = (TextView) v.findViewById(R.id.locationFirstEntryText);
                    if (tt != null) {
                        tt.setText(gl.getTitle().substring(0, startIndex));
                    }
                    TextView st = (TextView) v.findViewById(R.id.locationSecondEntryText);
                    if (st != null) {
                        st.setText(gl.getTitle().substring(startIndex));
                    }
                }
            }
            return v;
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            GeoLocation gl = this.mItems.get(position);
            ShopLocalActivity.mSearchLocation = gl.getTitle();
            double centerLatitude = gl.getLat();
            double centerLongitude = gl.getLng();
            boolean isSearchHomeChanged = false;
            if (ShopLocalActivity.this.calculateDistance(((double) ShopLocalActivity.mGeoPoint_SearchHomeLocation.getLatitudeE6()) / 1000000.0d, ((double) ShopLocalActivity.mGeoPoint_SearchHomeLocation.getLongitudeE6()) / 1000000.0d, centerLatitude, centerLongitude) > 100.0d) {
                isSearchHomeChanged = true;
            }
            if (isSearchHomeChanged) {
                ShopLocalActivity.mSearchHomeLocation = ShopLocalActivity.mSearchLocation;
                ShopLocalActivity.mGeoPoint_SearchHomeLocation = new GeoPoint((int) (1000000.0d * centerLatitude), (int) (1000000.0d * centerLongitude));
            }
            ShopLocalActivity.mGeoPoint_SearchLocation = new GeoPoint((int) (1000000.0d * centerLatitude), (int) (1000000.0d * centerLongitude));
            ShopLocalActivity.mSearchLocationEditText.setText(ShopLocalActivity.mSearchLocation);
            ShopLocalActivity.mSearchLocationTextView.setText(ShopLocalActivity.mSearchLocation);
            SharedPreferences.Editor editor = ShopLocalActivity.this.mShopLocalPrefs.edit();
            editor.putString("SLSearchHomeLocation", ShopLocalActivity.mSearchHomeLocation);
            editor.putInt("SLSearchHomeLocationLatitudeE6", ShopLocalActivity.mGeoPoint_SearchHomeLocation.getLatitudeE6());
            editor.putInt("SLSearchHomeLocationLongitudeE6", ShopLocalActivity.mGeoPoint_SearchHomeLocation.getLongitudeE6());
            editor.putString("SLSearchLocation", ShopLocalActivity.mSearchLocation);
            editor.putInt("SLSearchLocationLatitudeE6", ShopLocalActivity.mGeoPoint_SearchLocation.getLatitudeE6());
            editor.putInt("SLSearchLocationLongitudeE6", ShopLocalActivity.mGeoPoint_SearchLocation.getLongitudeE6());
            editor.commit();
            ShopLocalActivity.this.resetFooterView();
        }
    }

    class KeywordListAdapter extends BaseAdapter implements AdapterView.OnItemClickListener {
        private final LayoutInflater mInflater;
        private final ArrayList<String> mItems;

        public KeywordListAdapter(ArrayList<String> items) {
            this.mItems = items;
            this.mInflater = (LayoutInflater) ShopLocalActivity.this.getSystemService("layout_inflater");
        }

        public int getCount() {
            if (this.mItems == null) {
                return 0;
            }
            return this.mItems.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = this.mInflater.inflate((int) R.layout.keyword_list_row, (ViewGroup) null);
            }
            if (position < this.mItems.size()) {
                String title = this.mItems.get(position);
                TextView st = (TextView) v.findViewById(R.id.keywordSearchText);
                if (st != null) {
                    st.setText(title);
                }
            }
            return v;
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            String title = this.mItems.get(position);
            SharedPreferences.Editor editor = ShopLocalActivity.this.mShopLocalPrefs.edit();
            editor.putString("SLSearchKeyword", title);
            editor.commit();
            ShopLocalActivity.mSearchKeyworkEditText.setText(title);
            ShopLocalActivity.this.resetFooterView();
        }
    }

    /* access modifiers changed from: private */
    public void imprintSearch() {
        if (!checkInternetConnection()) {
            if (!this.internetConnectionAlertDialog.isShowing()) {
                this.internetConnectionAlertDialog.show();
            }
            this.mProgressbar.setVisibility(4);
            return;
        }
        resetFooterView();
        String searchKeyword = mSearchKeyworkEditText.getText().toString().trim();
        if (searchKeyword == null || searchKeyword.length() == 0) {
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("No Search Term Entered");
            alertDialog.setMessage("Please enter a keyword, business name or heading");
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
            return;
        }
        Intent intent = new Intent(this, ShopLocalMapListActivity.class);
        Bundle b = new Bundle();
        b.putString("keyword_search", searchKeyword);
        b.putString("location_default_home", mDefaultHomeLocation);
        b.putString("location_default_street_address", mDefaultStreetLocation);
        b.putInt("location_default_home_latE6", mGeoPoint_DefaultHomeLocation.getLatitudeE6());
        b.putInt("location_default_home_lonE6", mGeoPoint_DefaultHomeLocation.getLongitudeE6());
        b.putString("location_search_home", mSearchHomeLocation);
        b.putInt("location_search_home_latE6", mGeoPoint_SearchHomeLocation.getLatitudeE6());
        b.putInt("location_search_home_lonE6", mGeoPoint_SearchHomeLocation.getLongitudeE6());
        b.putString("location_search", mSearchLocation);
        b.putInt("location_search_latE6", mGeoPoint_SearchLocation.getLatitudeE6());
        b.putInt("location_search_lonE6", mGeoPoint_SearchLocation.getLongitudeE6());
        b.putBoolean("is_from_list_view", mIsFromListView);
        Location lastHomeLocation = null;
        try {
            lastHomeLocation = this.mLocationManager.getLastKnownLocation(this.mLocationProviderString);
        } catch (Exception e) {
        }
        if (lastHomeLocation == null) {
            b.putBoolean("is_current_mode", mIsCurrentMode);
        } else if (calculateDistance(((double) mGeoPoint_DefaultHomeLocation.getLatitudeE6()) / 1000000.0d, ((double) mGeoPoint_DefaultHomeLocation.getLongitudeE6()) / 1000000.0d, ((double) mGeoPoint_SearchLocation.getLatitudeE6()) / 1000000.0d, ((double) mGeoPoint_SearchLocation.getLongitudeE6()) / 1000000.0d) < 0.1d) {
            b.putBoolean("is_current_mode", true);
        } else {
            b.putBoolean("is_current_mode", mIsCurrentMode);
        }
        intent.putExtras(b);
        startActivity(intent);
    }

    /* access modifiers changed from: private */
    public double calculateDistance(double sourceLatitude, double sourceLongitude, double destLatitude, double destLongitude) {
        double dLat = destLatitude - sourceLatitude;
        double dLon = (destLongitude - sourceLongitude) * ((double) FloatMath.cos((float) ((3.141592653589793d * destLatitude) / 180.0d)));
        return ((double) FloatMath.sqrt((float) ((dLat * dLat) + (dLon * dLon)))) * 69.172d;
    }

    public boolean onTouchEvent(MotionEvent me) {
        return this.mGestureDetector.onTouchEvent(me);
    }

    public boolean onDown(MotionEvent e) {
        return false;
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        try {
            if (Math.abs(e1.getY() - e2.getY()) > 250.0f) {
                return false;
            }
            if (e1.getX() - e2.getX() <= 120.0f || Math.abs(velocityX) <= 200.0f) {
                if ((e2.getX() - e1.getX() <= 120.0f || Math.abs(velocityX) <= 200.0f) && ((e1.getY() - e2.getY() <= 120.0f || Math.abs(velocityX) <= 200.0f) && e2.getY() - e1.getY() > 120.0f)) {
                    Math.abs(velocityX);
                }
                return false;
            }
            imprintSearch();
            return true;
        } catch (Exception e) {
        }
    }

    public void onLongPress(MotionEvent e) {
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    public void onShowPress(MotionEvent e) {
    }

    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    public boolean onTouch(View v, MotionEvent event) {
        return this.mGestureDetector.onTouchEvent(event);
    }

    /* access modifiers changed from: private */
    public boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService("connectivity");
        if (cm.getActiveNetworkInfo() == null || !cm.getActiveNetworkInfo().isAvailable() || !cm.getActiveNetworkInfo().isConnected()) {
            return false;
        }
        return true;
    }
}
