package com.santabarbarayp.www;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ImprintWebsiteViewActivity extends Activity implements GestureDetector.OnGestureListener {
    private TextView accessoryTitleTextView;
    private GestureDetector mGestureDetector;
    ProgressBar mProgressbar;
    LayoutInflater mShopLocalInflater;
    private ImprintAccessory myImprintAccessory;
    /* access modifiers changed from: private */
    public WebView myWebsiteView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getResources().getConfiguration().orientation == 2) {
            getWindow().setFlags(1024, 1024);
        } else {
            getWindow().clearFlags(1024);
        }
        setContentView((int) R.layout.website_view);
        this.mShopLocalInflater = (LayoutInflater) getSystemService("layout_inflater");
        addContentView(this.mShopLocalInflater.inflate((int) R.layout.progressbar_spin_center, (ViewGroup) null), new RelativeLayout.LayoutParams(-1, -1));
        this.mProgressbar = (ProgressBar) findViewById(R.id.progressbar_large);
        this.myImprintAccessory = ImprintAccessory.fromBundle(getIntent().getExtras());
        this.mGestureDetector = new GestureDetector(this);
        String webUrl = this.myImprintAccessory.getAccssoryUrl();
        this.accessoryTitleTextView = (TextView) findViewById(R.id.websiteview_title);
        this.accessoryTitleTextView.setText(webUrl);
        setTitle("Online View");
        this.myWebsiteView = (WebView) findViewById(R.id.websiteview_webview);
        this.myWebsiteView.getSettings().setJavaScriptEnabled(true);
        this.myWebsiteView.getSettings().setBuiltInZoomControls(true);
        this.myWebsiteView.setWebViewClient(new AccessoryWebViewClient(this, null));
        if (!webUrl.startsWith("http")) {
            webUrl = String.format("http://%s", webUrl);
        }
        final String finalWebUrl = webUrl;
        new Thread() {
            public void run() {
                ImprintWebsiteViewActivity.this.myWebsiteView.loadUrl(finalWebUrl);
            }
        }.start();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        if (getResources().getConfiguration().orientation == 2) {
            getWindow().setFlags(1024, 1024);
        } else {
            getWindow().clearFlags(1024);
        }
        super.onConfigurationChanged(newConfig);
    }

    private class AccessoryWebViewClient extends WebViewClient {
        private AccessoryWebViewClient() {
        }

        /* synthetic */ AccessoryWebViewClient(ImprintWebsiteViewActivity imprintWebsiteViewActivity, AccessoryWebViewClient accessoryWebViewClient) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        public void onPageFinished(WebView view, String url) {
            ImprintWebsiteViewActivity.this.mProgressbar.setVisibility(4);
        }
    }

    public boolean onTouchEvent(MotionEvent me) {
        return this.mGestureDetector.onTouchEvent(me);
    }

    public boolean onDown(MotionEvent e) {
        return false;
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        try {
            if (Math.abs(e1.getY() - e2.getY()) > 250.0f) {
                return false;
            }
            if (e1.getX() - e2.getX() <= 120.0f || Math.abs(velocityX) <= 200.0f) {
                if (e2.getX() - e1.getX() > 120.0f && Math.abs(velocityX) > 200.0f) {
                    finish();
                    return true;
                } else if ((e1.getY() - e2.getY() <= 120.0f || Math.abs(velocityX) <= 200.0f) && e2.getY() - e1.getY() > 120.0f) {
                    Math.abs(velocityX);
                }
            }
            return false;
        } catch (Exception e) {
        }
    }

    public void onLongPress(MotionEvent e) {
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    public void onShowPress(MotionEvent e) {
    }

    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }
}
