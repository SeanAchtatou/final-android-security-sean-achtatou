package twitter4j.http;

import twitter4j.conf.Configuration;

public final class AuthorizationFactory {
    public static Authorization getInstance(Configuration conf) {
        return getInstance(conf, true);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: twitter4j.http.BasicAuthorization} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: twitter4j.http.BasicAuthorization} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: twitter4j.http.BasicAuthorization} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v0, resolved type: twitter4j.http.OAuthAuthorization} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v4, resolved type: twitter4j.http.BasicAuthorization} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static twitter4j.http.Authorization getInstance(twitter4j.conf.Configuration r9, boolean r10) {
        /*
            r2 = 0
            java.lang.String r3 = r9.getOAuthConsumerKey()
            java.lang.String r4 = r9.getOAuthConsumerSecret()
            if (r10 == 0) goto L_0x0030
            if (r3 == 0) goto L_0x0030
            if (r4 == 0) goto L_0x0030
            twitter4j.http.OAuthAuthorization r5 = new twitter4j.http.OAuthAuthorization
            r5.<init>(r9, r3, r4)
            java.lang.String r0 = r9.getOAuthAccessToken()
            java.lang.String r1 = r9.getOAuthAccessTokenSecret()
            if (r0 == 0) goto L_0x0028
            if (r1 == 0) goto L_0x0028
            twitter4j.http.AccessToken r8 = new twitter4j.http.AccessToken
            r8.<init>(r0, r1)
            r5.setOAuthAccessToken(r8)
        L_0x0028:
            r2 = r5
        L_0x0029:
            if (r2 != 0) goto L_0x002f
            twitter4j.http.NullAuthorization r2 = twitter4j.http.NullAuthorization.getInstance()
        L_0x002f:
            return r2
        L_0x0030:
            java.lang.String r7 = r9.getUser()
            java.lang.String r6 = r9.getPassword()
            if (r7 == 0) goto L_0x0029
            if (r6 == 0) goto L_0x0029
            twitter4j.http.BasicAuthorization r2 = new twitter4j.http.BasicAuthorization
            r2.<init>(r7, r6)
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: twitter4j.http.AuthorizationFactory.getInstance(twitter4j.conf.Configuration, boolean):twitter4j.http.Authorization");
    }

    public static Authorization getBasicAuthorizationInstance(String screenName, String password) {
        return new BasicAuthorization(screenName, password);
    }
}
