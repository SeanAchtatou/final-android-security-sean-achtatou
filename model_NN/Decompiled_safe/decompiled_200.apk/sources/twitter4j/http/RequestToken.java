package twitter4j.http;

import java.io.Serializable;
import twitter4j.TwitterException;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationContext;
import twitter4j.internal.http.HttpResponse;

public final class RequestToken extends OAuthToken implements Serializable {
    private static final long serialVersionUID = -8214365845469757952L;
    private final Configuration conf = ConfigurationContext.getInstance();
    private OAuthSupport oauth;

    public boolean equals(Object x0) {
        return super.equals(x0);
    }

    public String getParameter(String x0) {
        return super.getParameter(x0);
    }

    public String getToken() {
        return super.getToken();
    }

    public String getTokenSecret() {
        return super.getTokenSecret();
    }

    public int hashCode() {
        return super.hashCode();
    }

    public String toString() {
        return super.toString();
    }

    RequestToken(HttpResponse res, OAuthSupport oauth2) throws TwitterException {
        super(res);
        this.oauth = oauth2;
    }

    public RequestToken(String token, String tokenSecret) {
        super(token, tokenSecret);
    }

    RequestToken(String token, String tokenSecret, OAuthSupport oauth2) {
        super(token, tokenSecret);
        this.oauth = oauth2;
    }

    public AccessToken getAccessToken() throws TwitterException {
        return this.oauth.getOAuthAccessToken();
    }

    public AccessToken getAccessToken(String oauth_verifier) throws TwitterException {
        return this.oauth.getOAuthAccessToken(oauth_verifier);
    }

    public String getAuthorizationURL() {
        return new StringBuffer().append(this.conf.getOAuthAuthorizationURL()).append("?oauth_token=").append(getToken()).toString();
    }

    public String getAuthenticationURL() {
        return new StringBuffer().append(this.conf.getOAuthAuthenticationURL()).append("?oauth_token=").append(getToken()).toString();
    }
}
