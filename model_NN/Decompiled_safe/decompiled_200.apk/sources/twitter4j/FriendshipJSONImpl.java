package twitter4j;

import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.json.DataObjectFactoryUtil;
import twitter4j.internal.org.json.JSONArray;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.internal.util.ParseUtil;

class FriendshipJSONImpl implements Friendship {
    private static final long serialVersionUID = -7592213959077711096L;
    private boolean followedBy;
    private boolean following;
    private final int id;
    private final String name;
    private final String screenName;

    FriendshipJSONImpl(HttpResponse res) throws TwitterException {
        this(res.asJSONObject());
        DataObjectFactoryUtil.clearThreadLocalMap();
        DataObjectFactoryUtil.registerJSONObject(this, res.asJSONObject());
    }

    FriendshipJSONImpl(JSONObject json) throws TwitterException {
        this.following = false;
        this.followedBy = false;
        try {
            this.id = ParseUtil.getInt("id", json);
            this.name = json.getString("name");
            this.screenName = json.getString("screen_name");
            JSONArray connections = json.getJSONArray("connections");
            for (int i = 0; i < connections.length(); i++) {
                String connection = connections.getString(i);
                if ("following".equals(connection)) {
                    this.following = true;
                } else if ("followed_by".equals(connection)) {
                    this.followedBy = true;
                }
            }
        } catch (JSONException e) {
            JSONException jsone = e;
            throw new TwitterException(new StringBuffer().append(jsone.getMessage()).append(":").append(json.toString()).toString(), jsone);
        }
    }

    static ResponseList<Friendship> createFriendshipList(HttpResponse res) throws TwitterException {
        try {
            DataObjectFactoryUtil.clearThreadLocalMap();
            JSONArray list = res.asJSONArray();
            int size = list.length();
            ResponseList<Friendship> friendshipList = new ResponseListImpl<>(size, res);
            for (int i = 0; i < size; i++) {
                JSONObject json = list.getJSONObject(i);
                Friendship friendship = new FriendshipJSONImpl(json);
                DataObjectFactoryUtil.registerJSONObject(friendship, json);
                friendshipList.add(friendship);
            }
            DataObjectFactoryUtil.registerJSONObject(friendshipList, list);
            return friendshipList;
        } catch (JSONException e) {
            throw new TwitterException(e);
        } catch (TwitterException e2) {
            throw e2;
        }
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getScreenName() {
        return this.screenName;
    }

    public boolean isFollowing() {
        return this.following;
    }

    public boolean isFollowedBy() {
        return this.followedBy;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FriendshipJSONImpl that = (FriendshipJSONImpl) o;
        if (this.followedBy != that.followedBy) {
            return false;
        }
        if (this.following != that.following) {
            return false;
        }
        if (this.id != that.id) {
            return false;
        }
        if (!this.name.equals(that.name)) {
            return false;
        }
        return this.screenName.equals(that.screenName);
    }

    public int hashCode() {
        int i;
        int i2;
        int hashCode = ((((this.id * 31) + this.name.hashCode()) * 31) + this.screenName.hashCode()) * 31;
        if (this.following) {
            i = 1;
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 31;
        if (this.followedBy) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        return i3 + i2;
    }

    public String toString() {
        return new StringBuffer().append("ConnectionsJSONImpl{id=").append(this.id).append(", name='").append(this.name).append('\'').append(", screenName='").append(this.screenName).append('\'').append(", following=").append(this.following).append(", followedBy=").append(this.followedBy).append('}').toString();
    }
}
