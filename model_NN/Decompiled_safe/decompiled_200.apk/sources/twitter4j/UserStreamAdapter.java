package twitter4j;

public class UserStreamAdapter extends StatusAdapter implements UserStreamListener {
    public void onDeletionNotice(long directMessageId, int userId) {
    }

    public void onFriendList(int[] friendIds) {
    }

    public void onFavorite(User source, User target, Status favoritedStatus) {
    }

    public void onFollow(User source, User followedUser) {
    }

    public void onUnfavorite(User source, User target, Status unfavoritedStatus) {
    }

    public void onRetweet(User source, User target, Status retweetedStatus) {
    }

    public void onDirectMessage(DirectMessage directMessage) {
    }

    public void onUserListMemberAddition(User addedMember, User listOwner, UserList list) {
    }

    public void onUserListMemberDeletion(User deletedMember, User listOwner, UserList list) {
    }

    public void onUserListSubscription(User subscriber, User listOwner, UserList list) {
    }

    public void onUserListUnsubscription(User subscriber, User listOwner, UserList list) {
    }

    public void onUserListCreation(User listOwner, UserList list) {
    }

    public void onUserListUpdate(User listOwner, UserList list) {
    }

    public void onUserListDeletion(User listOwner, UserList list) {
    }

    public void onUserProfileUpdate(User updatedUser) {
    }

    public void onBlock(User source, User blockedUser) {
    }

    public void onUnblock(User source, User unblockedUser) {
    }

    public void onException(Exception ex) {
    }
}
