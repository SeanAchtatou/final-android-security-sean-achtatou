package twitter4j;

import java.util.Map;
import twitter4j.conf.Configuration;
import twitter4j.internal.http.HttpClientWrapperConfiguration;

/* compiled from: TwitterStream */
class StreamingReadTimeoutConfiguration implements HttpClientWrapperConfiguration {
    Configuration nestedConf;

    StreamingReadTimeoutConfiguration(Configuration httpConf) {
        this.nestedConf = httpConf;
    }

    public String getHttpProxyHost() {
        return this.nestedConf.getHttpProxyHost();
    }

    public int getHttpProxyPort() {
        return this.nestedConf.getHttpProxyPort();
    }

    public String getHttpProxyUser() {
        return this.nestedConf.getHttpProxyUser();
    }

    public String getHttpProxyPassword() {
        return this.nestedConf.getHttpProxyPassword();
    }

    public int getHttpConnectionTimeout() {
        return this.nestedConf.getHttpConnectionTimeout();
    }

    public int getHttpReadTimeout() {
        return this.nestedConf.getHttpStreamingReadTimeout();
    }

    public int getHttpRetryCount() {
        return this.nestedConf.getHttpRetryCount();
    }

    public int getHttpRetryIntervalSeconds() {
        return this.nestedConf.getHttpRetryIntervalSeconds();
    }

    public int getHttpMaxTotalConnections() {
        return this.nestedConf.getHttpMaxTotalConnections();
    }

    public int getHttpDefaultMaxPerRoute() {
        return this.nestedConf.getHttpDefaultMaxPerRoute();
    }

    public Map<String, String> getRequestHeaders() {
        return this.nestedConf.getRequestHeaders();
    }

    public boolean isPrettyDebugEnabled() {
        return this.nestedConf.isPrettyDebugEnabled();
    }
}
