package twitter4j.api;

import twitter4j.TwitterException;
import twitter4j.User;

public interface SpamReportingMethods {
    User reportSpam(int i) throws TwitterException;

    User reportSpam(String str) throws TwitterException;
}
