package twitter4j.api;

public interface FriendsFollowersMethodsAsync {
    void getFollowersIDs();

    void getFollowersIDs(int i);

    void getFollowersIDs(int i, long j);

    void getFollowersIDs(long j);

    void getFollowersIDs(String str);

    void getFollowersIDs(String str, long j);

    void getFriendsIDs();

    void getFriendsIDs(int i);

    void getFriendsIDs(int i, long j);

    void getFriendsIDs(long j);

    void getFriendsIDs(String str);

    void getFriendsIDs(String str, long j);
}
