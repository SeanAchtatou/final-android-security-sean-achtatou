package twitter4j.api;

public interface NotificationMethodsAsync {
    void disableNotification(int i);

    void disableNotification(String str);

    void enableNotification(int i);

    void enableNotification(String str);
}
