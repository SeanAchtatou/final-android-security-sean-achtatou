package twitter4j.api;

import twitter4j.Friendship;
import twitter4j.IDs;
import twitter4j.Relationship;
import twitter4j.ResponseList;
import twitter4j.TwitterException;
import twitter4j.User;

public interface FriendshipMethods {
    User createFriendship(int i) throws TwitterException;

    User createFriendship(int i, boolean z) throws TwitterException;

    User createFriendship(String str) throws TwitterException;

    User createFriendship(String str, boolean z) throws TwitterException;

    User destroyFriendship(int i) throws TwitterException;

    User destroyFriendship(String str) throws TwitterException;

    boolean existsFriendship(String str, String str2) throws TwitterException;

    IDs getIncomingFriendships(long j) throws TwitterException;

    IDs getOutgoingFriendships(long j) throws TwitterException;

    ResponseList<Friendship> lookupFriendships(int[] iArr) throws TwitterException;

    ResponseList<Friendship> lookupFriendships(String[] strArr) throws TwitterException;

    Relationship showFriendship(int i, int i2) throws TwitterException;

    Relationship showFriendship(String str, String str2) throws TwitterException;

    Relationship updateFriendship(int i, boolean z, boolean z2) throws TwitterException;

    Relationship updateFriendship(String str, boolean z, boolean z2) throws TwitterException;
}
