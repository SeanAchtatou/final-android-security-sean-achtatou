package twitter4j.api;

public interface LegalResourcesAsync {
    void getPrivacyPolicy();

    void getTermsOfService();
}
