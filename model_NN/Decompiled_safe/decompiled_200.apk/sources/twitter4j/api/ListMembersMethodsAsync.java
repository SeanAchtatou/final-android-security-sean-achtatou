package twitter4j.api;

public interface ListMembersMethodsAsync {
    void addUserListMember(int i, int i2);

    void addUserListMembers(int i, int[] iArr);

    void addUserListMembers(int i, String[] strArr);

    void checkUserListMembership(String str, int i, int i2);

    void deleteUserListMember(int i, int i2);

    void getUserListMembers(String str, int i, long j);
}
