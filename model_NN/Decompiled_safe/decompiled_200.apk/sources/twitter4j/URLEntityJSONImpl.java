package twitter4j;

import java.net.MalformedURLException;
import java.net.URL;
import twitter4j.internal.org.json.JSONArray;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;

final class URLEntityJSONImpl implements URLEntity {
    private static final long serialVersionUID = 1165188478018146676L;
    private String displayURL;
    private int end = -1;
    private URL expandedURL;
    private int start = -1;
    private URL url;

    URLEntityJSONImpl(JSONObject json) throws TwitterException {
        init(json);
    }

    private void init(JSONObject json) throws TwitterException {
        try {
            JSONArray indicesArray = json.getJSONArray("indices");
            this.start = indicesArray.getInt(0);
            this.end = indicesArray.getInt(1);
            try {
                this.url = new URL(json.getString("url"));
            } catch (MalformedURLException e) {
            }
            if (!json.isNull("expanded_url")) {
                try {
                    this.expandedURL = new URL(json.getString("expanded_url"));
                } catch (MalformedURLException e2) {
                }
            }
            if (!json.isNull("display_url")) {
                this.displayURL = json.getString("display_url");
            }
        } catch (JSONException e3) {
            throw new TwitterException(e3);
        }
    }

    public URL getURL() {
        return this.url;
    }

    public URL getExpandedURL() {
        return this.expandedURL;
    }

    public String getDisplayURL() {
        return this.displayURL;
    }

    public int getStart() {
        return this.start;
    }

    public int getEnd() {
        return this.end;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        URLEntityJSONImpl that = (URLEntityJSONImpl) o;
        if (this.end != that.end) {
            return false;
        }
        if (this.start != that.start) {
            return false;
        }
        if (this.displayURL == null ? that.displayURL != null : !this.displayURL.equals(that.displayURL)) {
            return false;
        }
        if (this.expandedURL == null ? that.expandedURL != null : !this.expandedURL.equals(that.expandedURL)) {
            return false;
        }
        return this.url == null ? that.url == null : this.url.equals(that.url);
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4 = ((this.start * 31) + this.end) * 31;
        if (this.url != null) {
            i = this.url.hashCode();
        } else {
            i = 0;
        }
        int i5 = (i4 + i) * 31;
        if (this.expandedURL != null) {
            i2 = this.expandedURL.hashCode();
        } else {
            i2 = 0;
        }
        int i6 = (i5 + i2) * 31;
        if (this.displayURL != null) {
            i3 = this.displayURL.hashCode();
        } else {
            i3 = 0;
        }
        return i6 + i3;
    }

    public String toString() {
        return new StringBuffer().append("URLEntityJSONImpl{start=").append(this.start).append(", end=").append(this.end).append(", url=").append(this.url).append(", expandedURL=").append(this.expandedURL).append(", displayURL=").append(this.displayURL).append('}').toString();
    }
}
