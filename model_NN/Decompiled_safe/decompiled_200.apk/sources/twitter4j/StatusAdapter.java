package twitter4j;

public class StatusAdapter implements StatusListener {
    public void onStatus(Status status) {
    }

    public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
    }

    public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
    }

    public void onScrubGeo(int userId, long upToStatusId) {
    }

    public void onException(Exception ex) {
    }
}
