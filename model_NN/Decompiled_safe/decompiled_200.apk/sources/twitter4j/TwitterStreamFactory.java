package twitter4j;

import java.io.Serializable;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationContext;
import twitter4j.http.AccessToken;
import twitter4j.http.Authorization;
import twitter4j.http.AuthorizationFactory;
import twitter4j.http.BasicAuthorization;
import twitter4j.http.OAuthAuthorization;

public final class TwitterStreamFactory implements Serializable {
    private static final long serialVersionUID = 8146074704915782233L;
    private final Configuration conf;
    private final StreamListener listener;

    public TwitterStreamFactory() {
        this(ConfigurationContext.getInstance());
    }

    public TwitterStreamFactory(Configuration conf2) {
        this.conf = conf2;
        this.listener = null;
    }

    public TwitterStreamFactory(StatusListener listener2) {
        this.conf = ConfigurationContext.getInstance();
        this.listener = listener2;
    }

    public TwitterStreamFactory(UserStreamListener listener2) {
        this.conf = ConfigurationContext.getInstance();
        this.listener = listener2;
    }

    public TwitterStreamFactory(String configTreePath) {
        this(configTreePath, (StatusListener) null);
    }

    public TwitterStreamFactory(String configTreePath, StatusListener listener2) {
        this(ConfigurationContext.getInstance(configTreePath), listener2);
    }

    public TwitterStreamFactory(String configTreePath, UserStreamListener listener2) {
        this(ConfigurationContext.getInstance(configTreePath), listener2);
    }

    public TwitterStreamFactory(Configuration conf2, StatusListener listener2) {
        if (conf2 == null) {
            throw new NullPointerException("configuration cannot be null");
        }
        this.conf = conf2;
        this.listener = listener2;
    }

    public TwitterStreamFactory(Configuration conf2, UserStreamListener listener2) {
        if (conf2 == null) {
            throw new NullPointerException("configuration cannot be null");
        }
        this.conf = conf2;
        this.listener = listener2;
    }

    public TwitterStream getInstance() {
        return getInstance(this.conf);
    }

    public TwitterStream getInstance(String screenName, String password) {
        return getInstance(this.conf, new BasicAuthorization(screenName, password));
    }

    public TwitterStream getInstance(AccessToken accessToken) {
        String consumerKey = this.conf.getOAuthConsumerKey();
        String consumerSecret = this.conf.getOAuthConsumerSecret();
        if (consumerKey == null && consumerSecret == null) {
            throw new IllegalStateException("Consumer key and Consumer secret not supplied.");
        }
        return getInstance(this.conf, new OAuthAuthorization(this.conf, consumerKey, consumerSecret, accessToken));
    }

    public TwitterStream getOAuthAuthorizedInstance(String consumerKey, String consumerSecret) {
        if (consumerKey == null && consumerSecret == null) {
            throw new IllegalStateException("Consumer key and Consumer secret not supplied.");
        }
        return getInstance(this.conf, new OAuthAuthorization(this.conf, consumerKey, consumerSecret));
    }

    public TwitterStream getOAuthAuthorizedInstance(AccessToken accessToken) {
        return getInstance(accessToken);
    }

    public TwitterStream getInstance(Authorization auth) {
        return getInstance(this.conf, auth);
    }

    private TwitterStream getInstance(Configuration conf2, Authorization auth) {
        return new TwitterStream(conf2, auth, this.listener);
    }

    private TwitterStream getInstance(Configuration conf2) {
        return new TwitterStream(conf2, AuthorizationFactory.getInstance(conf2, true), this.listener);
    }
}
