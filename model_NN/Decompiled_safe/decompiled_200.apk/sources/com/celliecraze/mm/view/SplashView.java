package com.celliecraze.mm.view;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.celliecraze.mm.activity.BubbleBusterSplashActivity;

public class SplashView extends View {
    private Context context;

    public SplashView(Context context2) {
        super(context2);
        this.context = context2;
    }

    public SplashView(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        this.context = context2;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        new LinearLayout(this.context).setOrientation(1);
        Rect rect = new Rect();
        ((BubbleBusterSplashActivity) this.context).getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        int statusHeight = rect.top;
        ((BubbleBusterSplashActivity) this.context).handleResize(w, h, statusHeight);
    }
}
