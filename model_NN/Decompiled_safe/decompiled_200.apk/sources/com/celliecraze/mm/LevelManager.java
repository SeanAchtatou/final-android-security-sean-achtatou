package com.celliecraze.mm;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.celliecraze.mm.activity.BubbleBusterOverviewActivity;
import com.celliecraze.mm.helper.BubbleBusterConstants;
import com.celliecraze.mm.highscores.DB;
import com.celliecraze.mm.highscores.HighScore;
import com.celliecraze.mm.highscores.HighscoreManager;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import java.lang.reflect.Array;
import java.util.Vector;

public class LevelManager {
    /* access modifiers changed from: private */
    public Context context;
    private int currentLevel;
    /* access modifiers changed from: private */
    public boolean custom;
    /* access modifiers changed from: private */
    public GameView gv;
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 300:
                    HighScore hs = (HighScore) msg.obj;
                    if (msg.arg1 == 1) {
                        LevelManager.showScoreDialog(LevelManager.this.context, true, hs, this, msg.arg2, LevelManager.this.gv, LevelManager.this.custom);
                        return;
                    } else {
                        LevelManager.showScoreDialog(LevelManager.this.context, false, hs, this, msg.arg2, LevelManager.this.gv, LevelManager.this.custom);
                        return;
                    }
                default:
                    return;
            }
        }
    };
    private Vector<byte[][]> levelList;

    public void saveState(Bundle map) {
        map.putInt("LevelManager-currentLevel", this.currentLevel);
    }

    public void restoreState(Bundle map) {
        this.currentLevel = map.getInt("LevelManager-currentLevel");
    }

    public LevelManager(byte[] levels, int startingLevel, Context context2, boolean custom2, GameView gv2) {
        int nextLevel;
        this.gv = gv2;
        this.context = context2;
        this.custom = custom2;
        String allLevels = new String(levels);
        this.currentLevel = startingLevel;
        this.levelList = new Vector<>();
        int nextLevel2 = allLevels.indexOf("\n\n");
        if (nextLevel2 == -1 && allLevels.trim().length() != 0) {
            nextLevel2 = allLevels.length();
        }
        while (nextLevel != -1) {
            this.levelList.addElement(getLevel(allLevels.substring(0, nextLevel).trim()));
            allLevels = allLevels.substring(nextLevel).trim();
            if (allLevels.length() == 0) {
                nextLevel = -1;
            } else {
                nextLevel = allLevels.indexOf("\n\n");
                if (nextLevel == -1) {
                    nextLevel = allLevels.length();
                }
            }
        }
        if (this.currentLevel >= this.levelList.size()) {
            this.currentLevel = 0;
        }
    }

    private byte[][] getLevel(String data) {
        byte[][] temp = (byte[][]) Array.newInstance(Byte.TYPE, 8, 12);
        for (int j = 0; j < 12; j++) {
            for (int i = 0; i < 8; i++) {
                temp[i][j] = -1;
            }
        }
        int tempX = 0;
        int tempY = 0;
        for (int i2 = 0; i2 < data.length(); i2++) {
            if (data.charAt(i2) >= '0' && data.charAt(i2) <= '7') {
                temp[tempX][tempY] = (byte) (data.charAt(i2) - '0');
                tempX++;
            } else if (data.charAt(i2) == '-') {
                temp[tempX][tempY] = -1;
                tempX++;
            }
            if (tempX == 8) {
                tempY++;
                if (tempY == 12) {
                    break;
                }
                tempX = tempY % 2;
            }
        }
        return temp;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public byte[][] getCurrentLevel() {
        GameView.bubblesFired = 0;
        HighscoreManager.startLevel();
        if (this.currentLevel < this.levelList.size()) {
            return this.levelList.elementAt(this.currentLevel);
        }
        return null;
    }

    public void goToNextLevel() {
        int realLevel = this.currentLevel - 1;
        HighScore hs = HighscoreManager.getHs();
        long lastHighScore = -1;
        try {
            lastHighScore = DB.getHighScore(this.context, realLevel).getScore();
        } catch (Exception e) {
        }
        Message msg = Message.obtain();
        msg.what = 300;
        if (lastHighScore < hs.getScore()) {
            msg.arg1 = 1;
        } else {
            msg.arg1 = 0;
        }
        msg.obj = hs;
        msg.arg2 = realLevel;
        this.handler.sendMessage(msg);
    }

    public void goToFirstLevel() {
        this.currentLevel = 0;
    }

    public int getLevelIndex() {
        return this.currentLevel;
    }

    public void setCurrentLevel(int currentLevel2) {
        this.currentLevel = currentLevel2;
    }

    /* JADX INFO: Multiple debug info for r27v2 com.scoreloop.client.android.core.model.User: [D('localUser' com.scoreloop.client.android.core.model.User), D('handler' android.os.Handler)] */
    /* access modifiers changed from: private */
    public static void showScoreDialog(Context mContext, boolean newHighScore, HighScore hs, Handler handler2, int currentLevel2, GameView gv2, boolean custom2) {
        Animation a;
        ImageView winPanelImage;
        TextView tv4;
        TextView tv3;
        TextView tv2;
        long timeBonus;
        Dialog myDialog;
        TextView tv;
        Animation hyperWinPanel;
        long timeBonus2;
        int levelPack = getLevelPack(currentLevel2);
        String userName = Session.getCurrentSession().getUser().getLogin();
        Typeface localTypeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/utb.ttf");
        if (FrozenBubble.fullScreenActive) {
            myDialog = new Dialog(mContext, 16973841);
            myDialog.setContentView(R.layout.dialog_score);
            winPanelImage = (ImageView) myDialog.findViewById(R.id.dialog_win_panel);
            winPanelImage.setImageResource(R.drawable.win_panel);
            hyperWinPanel = AnimationUtils.loadAnimation(mContext, R.anim.hyper_anim);
            winPanelImage.startAnimation(hyperWinPanel);
            a = AnimationUtils.loadAnimation(mContext, R.anim.text_anim);
            ((TextView) myDialog.findViewById(R.id.t_msg_new_hs)).setTypeface(localTypeface);
            ((TextView) myDialog.findViewById(R.id.t_msg_user_name)).setTypeface(localTypeface);
            ((TextView) myDialog.findViewById(R.id.t_msg_score_total)).setTypeface(localTypeface);
            ((TextView) myDialog.findViewById(R.id.t_msg_bubbles)).setTypeface(localTypeface);
            ((TextView) myDialog.findViewById(R.id.t_msg_time)).setTypeface(localTypeface);
            ((TextView) myDialog.findViewById(R.id.t_msg_score)).setTypeface(localTypeface);
            ((TextView) myDialog.findViewById(R.id.t_msg_time_bonus)).setTypeface(localTypeface);
            ((TextView) myDialog.findViewById(R.id.t_msg_levelpack_score)).setTypeface(localTypeface);
            if (!newHighScore) {
                myDialog.findViewById(R.id.t_msg_new_hs).setVisibility(8);
            } else {
                ((TextView) myDialog.findViewById(R.id.t_msg_new_hs)).startAnimation(a);
            }
            if (userName != null) {
                TextView tv5 = (TextView) myDialog.findViewById(R.id.t_msg_user_name);
                tv5.setText(mContext.getString(R.string.user_profile, userName));
                tv5.startAnimation(a);
            } else {
                myDialog.findViewById(R.id.t_msg_user_name).setVisibility(8);
            }
            tv = (TextView) myDialog.findViewById(R.id.t_msg_bubbles);
            tv.setText(mContext.getString(R.string.bubbles_fired, Integer.valueOf(hs.getBubbles())));
            tv.startAnimation(a);
            tv2 = (TextView) myDialog.findViewById(R.id.t_msg_time);
            tv2.setText(mContext.getString(R.string.time_played, getFormattedTime(hs.getTime(), true)));
            tv2.startAnimation(a);
            tv3 = (TextView) myDialog.findViewById(R.id.t_msg_score);
            tv3.setText(mContext.getString(R.string.score, Integer.valueOf(1040 - (hs.getBubbles() * 5))));
            tv3.startAnimation(a);
            if (hs.getTime() > 300) {
                timeBonus = 0;
            } else {
                timeBonus = hs.getScore() - ((long) (1040 - (hs.getBubbles() * 5)));
            }
            tv4 = (TextView) myDialog.findViewById(R.id.t_msg_time_bonus);
            tv4.setText(mContext.getString(R.string.time_bonus, Long.valueOf(timeBonus)));
            tv4.startAnimation(a);
            if (newHighScore) {
                TextView tv52 = (TextView) myDialog.findViewById(R.id.t_msg_score_total);
                tv52.setText(mContext.getString(R.string.level_high_score_total, Long.valueOf(hs.getScore())));
                tv52.startAnimation(a);
                long score = DB.getHighScore(mContext, currentLevel2).getScore();
                TextView tv6 = (TextView) myDialog.findViewById(R.id.t_msg_levelpack_score);
                tv6.setText(mContext.getString(R.string.score_level_pack, Integer.valueOf(levelPack + 1), Long.valueOf((DB.getLevelPackHighScore(mContext, levelPack) - score) + hs.getScore())));
                tv6.startAnimation(a);
            } else {
                TextView tv53 = (TextView) myDialog.findViewById(R.id.t_msg_score_total);
                tv53.setText(mContext.getString(R.string.level_score_total, Long.valueOf(hs.getScore())));
                tv53.startAnimation(a);
                TextView tv62 = (TextView) myDialog.findViewById(R.id.t_msg_levelpack_score);
                tv62.setText(mContext.getString(R.string.score_level_pack, Integer.valueOf(levelPack + 1), Long.valueOf(DB.getLevelPackHighScore(mContext, levelPack))));
                tv62.startAnimation(a);
            }
            if (custom2) {
                myDialog.findViewById(R.id.t_msg_levelpack_score).setVisibility(8);
            } else {
                DB.insertUpdateHighScore(mContext, userName, currentLevel2, hs);
                try {
                    ScoreloopManagerSingleton.get().onGamePlayEnded(Double.valueOf((double) DB.getLevelPackHighScore(mContext, levelPack)), Integer.valueOf(levelPack));
                } catch (Exception e) {
                }
            }
            final GameView gameView = gv2;
            final Context context2 = mContext;
            final Dialog dialog = myDialog;
            final ImageView imageView = winPanelImage;
            myDialog.findViewById(R.id.dialog_score).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    HighscoreManager.reset();
                    HighscoreManager.startLevel();
                    GameView.this.dispatchKeyEvent(new KeyEvent(0, 23));
                    GameView.this.dispatchKeyEvent(new KeyEvent(1, 23));
                    GameView.centerFired = false;
                    BubbleBusterConstants.vibrate(context2, 50);
                    dialog.dismiss();
                    imageView.setImageResource(0);
                    imageView.setImageDrawable(null);
                    if (dialog.getWindow() != null) {
                        dialog.getWindow().setCallback(null);
                    }
                }
            });
            final GameView gameView2 = gv2;
            final Context context3 = mContext;
            final ImageView imageView2 = winPanelImage;
            final Dialog dialog2 = myDialog;
            myDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent arg2) {
                    if (keyCode == 23 || keyCode == 21 || keyCode == 22 || keyCode == 19 || keyCode == 20) {
                        HighscoreManager.reset();
                        HighscoreManager.startLevel();
                        GameView.this.dispatchKeyEvent(new KeyEvent(0, 23));
                        GameView.this.dispatchKeyEvent(new KeyEvent(1, 23));
                        GameView.centerFired = false;
                        BubbleBusterConstants.vibrate(context3, 50);
                        dialog.dismiss();
                        imageView2.setImageResource(0);
                        imageView2.setImageDrawable(null);
                        if (dialog2.getWindow() != null) {
                            dialog2.getWindow().setCallback(null);
                        }
                    }
                    return false;
                }
            });
            myDialog.setCancelable(false);
            if (!((Activity) mContext).isFinishing()) {
                BubbleBusterConstants.vibrate(mContext, BubbleBusterConstants.longVibe);
                myDialog.show();
            }
        } else {
            myDialog = new Dialog(mContext, 16973840);
            myDialog.setContentView(R.layout.dialog_score);
            winPanelImage = (ImageView) myDialog.findViewById(R.id.dialog_win_panel);
            winPanelImage.setImageResource(R.drawable.win_panel);
            hyperWinPanel = AnimationUtils.loadAnimation(mContext, R.anim.hyper_anim);
            winPanelImage.startAnimation(hyperWinPanel);
            a = AnimationUtils.loadAnimation(mContext, R.anim.text_anim);
            ((TextView) myDialog.findViewById(R.id.t_msg_new_hs)).setTypeface(localTypeface);
            ((TextView) myDialog.findViewById(R.id.t_msg_user_name)).setTypeface(localTypeface);
            ((TextView) myDialog.findViewById(R.id.t_msg_score_total)).setTypeface(localTypeface);
            ((TextView) myDialog.findViewById(R.id.t_msg_bubbles)).setTypeface(localTypeface);
            ((TextView) myDialog.findViewById(R.id.t_msg_time)).setTypeface(localTypeface);
            ((TextView) myDialog.findViewById(R.id.t_msg_score)).setTypeface(localTypeface);
            ((TextView) myDialog.findViewById(R.id.t_msg_time_bonus)).setTypeface(localTypeface);
            ((TextView) myDialog.findViewById(R.id.t_msg_levelpack_score)).setTypeface(localTypeface);
            if (!newHighScore) {
                myDialog.findViewById(R.id.t_msg_new_hs).setVisibility(8);
            } else {
                ((TextView) myDialog.findViewById(R.id.t_msg_new_hs)).startAnimation(a);
            }
            if (userName != null) {
                TextView tv7 = (TextView) myDialog.findViewById(R.id.t_msg_user_name);
                tv7.setText(mContext.getString(R.string.user_profile, userName));
                tv7.startAnimation(a);
            } else {
                myDialog.findViewById(R.id.t_msg_user_name).setVisibility(8);
            }
            tv = (TextView) myDialog.findViewById(R.id.t_msg_bubbles);
            tv.setText(mContext.getString(R.string.bubbles_fired, Integer.valueOf(hs.getBubbles())));
            tv.startAnimation(a);
            tv2 = (TextView) myDialog.findViewById(R.id.t_msg_time);
            tv2.setText(mContext.getString(R.string.time_played, getFormattedTime(hs.getTime(), true)));
            tv2.startAnimation(a);
            tv3 = (TextView) myDialog.findViewById(R.id.t_msg_score);
            tv3.setText(mContext.getString(R.string.score, Integer.valueOf(1040 - (hs.getBubbles() * 5))));
            tv3.startAnimation(a);
            if (hs.getTime() > 300) {
                timeBonus2 = 0;
            } else {
                timeBonus2 = hs.getScore() - ((long) (1040 - (hs.getBubbles() * 5)));
            }
            tv4 = (TextView) myDialog.findViewById(R.id.t_msg_time_bonus);
            tv4.setText(mContext.getString(R.string.time_bonus, Long.valueOf(timeBonus)));
            tv4.startAnimation(a);
            if (newHighScore) {
                TextView tv54 = (TextView) myDialog.findViewById(R.id.t_msg_score_total);
                tv54.setText(mContext.getString(R.string.level_high_score_total, Long.valueOf(hs.getScore())));
                tv54.startAnimation(a);
                long score2 = DB.getHighScore(mContext, currentLevel2).getScore();
                TextView tv63 = (TextView) myDialog.findViewById(R.id.t_msg_levelpack_score);
                tv63.setText(mContext.getString(R.string.score_level_pack, Integer.valueOf(levelPack + 1), Long.valueOf((DB.getLevelPackHighScore(mContext, levelPack) - score2) + hs.getScore())));
                tv63.startAnimation(a);
            } else {
                TextView tv55 = (TextView) myDialog.findViewById(R.id.t_msg_score_total);
                tv55.setText(mContext.getString(R.string.level_score_total, Long.valueOf(hs.getScore())));
                tv55.startAnimation(a);
                TextView tv64 = (TextView) myDialog.findViewById(R.id.t_msg_levelpack_score);
                tv64.setText(mContext.getString(R.string.score_level_pack, Integer.valueOf(levelPack + 1), Long.valueOf(DB.getLevelPackHighScore(mContext, levelPack))));
                tv64.startAnimation(a);
            }
            if (custom2) {
                myDialog.findViewById(R.id.t_msg_levelpack_score).setVisibility(8);
            } else {
                DB.insertUpdateHighScore(mContext, userName, currentLevel2, hs);
                try {
                    ScoreloopManagerSingleton.get().onGamePlayEnded(Double.valueOf((double) DB.getLevelPackHighScore(mContext, levelPack)), Integer.valueOf(levelPack));
                } catch (Exception e2) {
                }
            }
            final GameView gameView3 = gv2;
            final Context context4 = mContext;
            final Dialog dialog3 = myDialog;
            final ImageView imageView3 = winPanelImage;
            myDialog.findViewById(R.id.dialog_score).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    HighscoreManager.reset();
                    HighscoreManager.startLevel();
                    GameView.this.dispatchKeyEvent(new KeyEvent(0, 23));
                    GameView.this.dispatchKeyEvent(new KeyEvent(1, 23));
                    GameView.centerFired = false;
                    BubbleBusterConstants.vibrate(context4, 50);
                    dialog3.dismiss();
                    imageView3.setImageResource(0);
                    imageView3.setImageDrawable(null);
                    if (dialog3.getWindow() != null) {
                        dialog3.getWindow().setCallback(null);
                    }
                }
            });
            final GameView gameView4 = gv2;
            final Context context5 = mContext;
            final ImageView imageView4 = winPanelImage;
            final Dialog dialog4 = myDialog;
            myDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent arg2) {
                    if (keyCode == 23 || keyCode == 21 || keyCode == 22 || keyCode == 19 || keyCode == 20) {
                        HighscoreManager.reset();
                        HighscoreManager.startLevel();
                        GameView.this.dispatchKeyEvent(new KeyEvent(0, 23));
                        GameView.this.dispatchKeyEvent(new KeyEvent(1, 23));
                        GameView.centerFired = false;
                        BubbleBusterConstants.vibrate(context5, 50);
                        dialog.dismiss();
                        imageView4.setImageResource(0);
                        imageView4.setImageDrawable(null);
                        if (dialog4.getWindow() != null) {
                            dialog4.getWindow().setCallback(null);
                        }
                    }
                    return false;
                }
            });
            myDialog.setCancelable(false);
            if (!((Activity) mContext).isFinishing()) {
                BubbleBusterConstants.vibrate(mContext, BubbleBusterConstants.longVibe);
                myDialog.show();
            }
        }
    }

    private void saveUnlockedToPreferences() {
        int maxLevel = FrozenBubble.prefs.getInt(BubbleBusterOverviewActivity.MAX_LEVEL, 1);
        int gameLevel = this.currentLevel + 1;
        if (maxLevel <= gameLevel) {
            maxLevel = gameLevel;
        }
        SharedPreferences.Editor edit = FrozenBubble.prefs.edit();
        edit.putInt(BubbleBusterOverviewActivity.MAX_LEVEL, maxLevel);
        edit.commit();
    }

    private static int getLevelPack(int currentLevel2) {
        return currentLevel2 / 100;
    }

    public static String getFormattedTime(long seconds, boolean showHours) {
        if (showHours) {
            return String.valueOf(formatTwoPlaces(((int) seconds) / 3600)) + ":" + formatTwoPlaces((((int) seconds) % 3600) / 60) + ":" + formatTwoPlaces(((int) seconds) % 60);
        }
        return String.valueOf(formatTwoPlaces(((int) seconds) / 60)) + ":" + formatTwoPlaces(((int) seconds) % 60);
    }

    public static String formatTwoPlaces(int number) {
        if (number < 10) {
            return "0" + number;
        }
        return new StringBuilder(String.valueOf(number)).toString();
    }

    public void saveNextLevel() {
        this.currentLevel++;
        if (this.currentLevel >= this.levelList.size()) {
            this.currentLevel = 0;
        }
        if (!this.custom) {
            SharedPreferences.Editor editor = FrozenBubble.prefs.edit();
            editor.putInt("level", this.currentLevel);
            editor.commit();
            saveUnlockedToPreferences();
            return;
        }
        SharedPreferences.Editor editor2 = FrozenBubble.prefs.edit();
        editor2.putInt("levelCustom", this.currentLevel);
        editor2.commit();
    }
}
