package com.celliecraze.mm.highscores;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.celliecraze.mm.helper.BubbleBusterConstants;

public class DB {
    private static final String DATABASE_NAME = "bubblebuster";
    private static final String TABLE_HIGHSCORES = "highscores";
    private static final String bubbles = "bubbles";
    private static final String id = "id";
    private static final String level = "level";
    private static final String player = "player";
    private static final String score = "score";
    private static final String time = "time";

    public static void insertUpdateHighScore(Context ctx, String player2, int level2, HighScore score2) {
        SQLiteDatabase myDB = null;
        try {
            myDB = ctx.openOrCreateDatabase(DATABASE_NAME, 0, null);
            myDB.execSQL("CREATE TABLE IF NOT EXISTS highscores (id INTEGER PRIMARY KEY, player TEXT, level INT, score INT, time INT, bubbles INT);");
            Cursor c = myDB.rawQuery("SELECT * FROM highscores WHERE level = '" + level2 + "'", null);
            if (c.getCount() == 0) {
                ContentValues values = new ContentValues();
                values.put("player", player2);
                values.put(level, Integer.valueOf(level2));
                values.put(score, Long.valueOf(score2.getScore()));
                values.put(time, Long.valueOf(score2.getTime()));
                values.put(bubbles, Integer.valueOf(score2.getBubbles()));
                myDB.insert(TABLE_HIGHSCORES, null, values);
            } else {
                ContentValues values2 = new ContentValues();
                values2.put("player", player2);
                values2.put(score, Long.valueOf(score2.getScore()));
                values2.put(time, Long.valueOf(score2.getTime()));
                values2.put(bubbles, Integer.valueOf(score2.getBubbles()));
                myDB.update(TABLE_HIGHSCORES, values2, "level = '" + level2 + "' AND " + score + " < " + score2.getScore(), null);
            }
            c.close();
            if (myDB != null) {
                myDB.close();
            }
        } catch (Exception e) {
            Log.w(BubbleBusterConstants.TAG, "DB Error", e);
            if (myDB != null) {
                myDB.close();
            }
        } catch (Throwable th) {
            if (myDB != null) {
                myDB.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long[] getScores(android.content.Context r9) {
        /*
            r5 = 0
            r6 = 600(0x258, float:8.41E-43)
            long[] r2 = new long[r6]
            java.lang.String r6 = "bubblebuster"
            r7 = 0
            r8 = 0
            android.database.sqlite.SQLiteDatabase r5 = r9.openOrCreateDatabase(r6, r7, r8)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r6 = "SELECT * FROM highscores ORDER BY level ASC"
            r7 = 0
            android.database.Cursor r0 = r5.rawQuery(r6, r7)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r6 = "score"
            int r4 = r0.getColumnIndex(r6)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r6 = "level"
            int r3 = r0.getColumnIndex(r6)     // Catch:{ Exception -> 0x0044 }
            r0.moveToFirst()     // Catch:{ Exception -> 0x0044 }
            if (r0 == 0) goto L_0x003b
            int r6 = r0.getCount()     // Catch:{ Exception -> 0x0044 }
            if (r6 <= 0) goto L_0x003b
        L_0x002b:
            int r6 = r0.getInt(r3)     // Catch:{ Exception -> 0x0044 }
            long r7 = r0.getLong(r4)     // Catch:{ Exception -> 0x0044 }
            r2[r6] = r7     // Catch:{ Exception -> 0x0044 }
            boolean r6 = r0.moveToNext()     // Catch:{ Exception -> 0x0044 }
            if (r6 != 0) goto L_0x002b
        L_0x003b:
            r0.close()     // Catch:{ Exception -> 0x0044 }
            if (r5 == 0) goto L_0x0043
            r5.close()
        L_0x0043:
            return r2
        L_0x0044:
            r6 = move-exception
            r1 = r6
            java.lang.String r6 = "MarbleMadness"
            java.lang.String r7 = "DB Error"
            android.util.Log.w(r6, r7, r1)     // Catch:{ all -> 0x0053 }
            if (r5 == 0) goto L_0x0043
            r5.close()
            goto L_0x0043
        L_0x0053:
            r6 = move-exception
            if (r5 == 0) goto L_0x0059
            r5.close()
        L_0x0059:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.celliecraze.mm.highscores.DB.getScores(android.content.Context):long[]");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x007c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.celliecraze.mm.highscores.HighScore getHighScore(android.content.Context r13, int r14) {
        /*
            r1 = 0
            r3 = 0
            r12 = 0
            com.celliecraze.mm.highscores.HighScore r0 = new com.celliecraze.mm.highscores.HighScore
            r4 = r1
            r0.<init>(r1, r3, r4)
            java.lang.String r1 = "bubblebuster"
            r2 = 0
            r3 = 0
            android.database.sqlite.SQLiteDatabase r12 = r13.openOrCreateDatabase(r1, r2, r3)     // Catch:{ Exception -> 0x006a }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x006a }
            java.lang.String r2 = "SELECT * FROM highscores WHERE level = "
            r1.<init>(r2)     // Catch:{ Exception -> 0x006a }
            java.lang.StringBuilder r1 = r1.append(r14)     // Catch:{ Exception -> 0x006a }
            java.lang.String r2 = " LIMIT 1"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x006a }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x006a }
            r2 = 0
            android.database.Cursor r6 = r12.rawQuery(r1, r2)     // Catch:{ Exception -> 0x006a }
            java.lang.String r1 = "score"
            int r10 = r6.getColumnIndex(r1)     // Catch:{ Exception -> 0x006a }
            java.lang.String r1 = "time"
            int r11 = r6.getColumnIndex(r1)     // Catch:{ Exception -> 0x006a }
            java.lang.String r1 = "bubbles"
            int r9 = r6.getColumnIndex(r1)     // Catch:{ Exception -> 0x006a }
            r6.moveToFirst()     // Catch:{ Exception -> 0x006a }
            if (r6 == 0) goto L_0x0061
            int r1 = r6.getCount()     // Catch:{ Exception -> 0x006a }
            if (r1 <= 0) goto L_0x0061
        L_0x0049:
            r8 = r0
            com.celliecraze.mm.highscores.HighScore r0 = new com.celliecraze.mm.highscores.HighScore     // Catch:{ Exception -> 0x0083, all -> 0x0080 }
            long r1 = r6.getLong(r10)     // Catch:{ Exception -> 0x0083, all -> 0x0080 }
            int r3 = r6.getInt(r9)     // Catch:{ Exception -> 0x0083, all -> 0x0080 }
            long r4 = r6.getLong(r11)     // Catch:{ Exception -> 0x0083, all -> 0x0080 }
            r0.<init>(r1, r3, r4)     // Catch:{ Exception -> 0x0083, all -> 0x0080 }
            boolean r1 = r6.moveToNext()     // Catch:{ Exception -> 0x006a }
            if (r1 != 0) goto L_0x0049
        L_0x0061:
            r6.close()     // Catch:{ Exception -> 0x006a }
            if (r12 == 0) goto L_0x0069
            r12.close()
        L_0x0069:
            return r0
        L_0x006a:
            r1 = move-exception
            r7 = r1
        L_0x006c:
            java.lang.String r1 = "MarbleMadness"
            java.lang.String r2 = "DB Error"
            android.util.Log.w(r1, r2, r7)     // Catch:{ all -> 0x0079 }
            if (r12 == 0) goto L_0x0069
            r12.close()
            goto L_0x0069
        L_0x0079:
            r1 = move-exception
        L_0x007a:
            if (r12 == 0) goto L_0x007f
            r12.close()
        L_0x007f:
            throw r1
        L_0x0080:
            r1 = move-exception
            r0 = r8
            goto L_0x007a
        L_0x0083:
            r1 = move-exception
            r7 = r1
            r0 = r8
            goto L_0x006c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.celliecraze.mm.highscores.DB.getHighScore(android.content.Context, int):com.celliecraze.mm.highscores.HighScore");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x005b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long getLevelPackHighScore(android.content.Context r9, int r10) {
        /*
            r5 = 0
            r2 = 0
            java.lang.String r6 = "bubblebuster"
            r7 = 0
            r8 = 0
            android.database.sqlite.SQLiteDatabase r5 = r9.openOrCreateDatabase(r6, r7, r8)     // Catch:{ Exception -> 0x005f }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x005f }
            java.lang.String r7 = "SELECT sum(score) AS score FROM highscores WHERE level >= "
            r6.<init>(r7)     // Catch:{ Exception -> 0x005f }
            int r7 = r10 * 100
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x005f }
            java.lang.String r7 = "   AND "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x005f }
            java.lang.String r7 = "level"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x005f }
            java.lang.String r7 = " < "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x005f }
            int r7 = r10 + 1
            int r7 = r7 * 100
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x005f }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x005f }
            r7 = 0
            android.database.Cursor r0 = r5.rawQuery(r6, r7)     // Catch:{ Exception -> 0x005f }
            java.lang.String r6 = "score"
            int r4 = r0.getColumnIndex(r6)     // Catch:{ Exception -> 0x005f }
            r0.moveToFirst()     // Catch:{ Exception -> 0x005f }
            if (r0 == 0) goto L_0x0056
            int r6 = r0.getCount()     // Catch:{ Exception -> 0x005f }
            if (r6 <= 0) goto L_0x0056
        L_0x004c:
            long r2 = r0.getLong(r4)     // Catch:{ Exception -> 0x005f }
            boolean r6 = r0.moveToNext()     // Catch:{ Exception -> 0x005f }
            if (r6 != 0) goto L_0x004c
        L_0x0056:
            r0.close()     // Catch:{ Exception -> 0x005f }
            if (r5 == 0) goto L_0x005e
            r5.close()
        L_0x005e:
            return r2
        L_0x005f:
            r6 = move-exception
            r1 = r6
            java.lang.String r6 = "MarbleMadness"
            java.lang.String r7 = "DB Error"
            android.util.Log.w(r6, r7, r1)     // Catch:{ all -> 0x006e }
            if (r5 == 0) goto L_0x005e
            r5.close()
            goto L_0x005e
        L_0x006e:
            r6 = move-exception
            if (r5 == 0) goto L_0x0074
            r5.close()
        L_0x0074:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.celliecraze.mm.highscores.DB.getLevelPackHighScore(android.content.Context, int):long");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0032  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long getAllLevelPackHighScores(android.content.Context r9) {
        /*
            r5 = 0
            r2 = 0
            java.lang.String r6 = "bubblebuster"
            r7 = 0
            r8 = 0
            android.database.sqlite.SQLiteDatabase r5 = r9.openOrCreateDatabase(r6, r7, r8)     // Catch:{ Exception -> 0x0036 }
            java.lang.String r6 = "SELECT sum(score) AS score FROM highscores"
            r7 = 0
            android.database.Cursor r0 = r5.rawQuery(r6, r7)     // Catch:{ Exception -> 0x0036 }
            java.lang.String r6 = "score"
            int r4 = r0.getColumnIndex(r6)     // Catch:{ Exception -> 0x0036 }
            r0.moveToFirst()     // Catch:{ Exception -> 0x0036 }
            if (r0 == 0) goto L_0x002d
            int r6 = r0.getCount()     // Catch:{ Exception -> 0x0036 }
            if (r6 <= 0) goto L_0x002d
        L_0x0023:
            long r2 = r0.getLong(r4)     // Catch:{ Exception -> 0x0036 }
            boolean r6 = r0.moveToNext()     // Catch:{ Exception -> 0x0036 }
            if (r6 != 0) goto L_0x0023
        L_0x002d:
            r0.close()     // Catch:{ Exception -> 0x0036 }
            if (r5 == 0) goto L_0x0035
            r5.close()
        L_0x0035:
            return r2
        L_0x0036:
            r6 = move-exception
            r1 = r6
            java.lang.String r6 = "DB"
            java.lang.String r7 = "DB Error"
            android.util.Log.w(r6, r7, r1)     // Catch:{ all -> 0x0045 }
            if (r5 == 0) goto L_0x0035
            r5.close()
            goto L_0x0035
        L_0x0045:
            r6 = move-exception
            if (r5 == 0) goto L_0x004b
            r5.close()
        L_0x004b:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.celliecraze.mm.highscores.DB.getAllLevelPackHighScores(android.content.Context):long");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x003f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getUserName(android.content.Context r8, int r9) {
        /*
            r3 = 0
            java.lang.String r4 = ""
            java.lang.String r5 = "bubblebuster"
            r6 = 0
            r7 = 0
            android.database.sqlite.SQLiteDatabase r3 = r8.openOrCreateDatabase(r5, r6, r7)     // Catch:{ Exception -> 0x0043 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0043 }
            java.lang.String r6 = "SELECT player FROM highscores WHERE level = "
            r5.<init>(r6)     // Catch:{ Exception -> 0x0043 }
            java.lang.StringBuilder r5 = r5.append(r9)     // Catch:{ Exception -> 0x0043 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0043 }
            r6 = 0
            android.database.Cursor r0 = r3.rawQuery(r5, r6)     // Catch:{ Exception -> 0x0043 }
            java.lang.String r5 = "player"
            int r2 = r0.getColumnIndex(r5)     // Catch:{ Exception -> 0x0043 }
            r0.moveToFirst()     // Catch:{ Exception -> 0x0043 }
            if (r0 == 0) goto L_0x003a
            int r5 = r0.getCount()     // Catch:{ Exception -> 0x0043 }
            if (r5 <= 0) goto L_0x003a
        L_0x0030:
            java.lang.String r4 = r0.getString(r2)     // Catch:{ Exception -> 0x0043 }
            boolean r5 = r0.moveToNext()     // Catch:{ Exception -> 0x0043 }
            if (r5 != 0) goto L_0x0030
        L_0x003a:
            r0.close()     // Catch:{ Exception -> 0x0043 }
            if (r3 == 0) goto L_0x0042
            r3.close()
        L_0x0042:
            return r4
        L_0x0043:
            r5 = move-exception
            r1 = r5
            java.lang.String r5 = "MarbleMadness"
            java.lang.String r6 = "DB Error"
            android.util.Log.w(r5, r6, r1)     // Catch:{ all -> 0x0052 }
            if (r3 == 0) goto L_0x0042
            r3.close()
            goto L_0x0042
        L_0x0052:
            r5 = move-exception
            if (r3 == 0) goto L_0x0058
            r3.close()
        L_0x0058:
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.celliecraze.mm.highscores.DB.getUserName(android.content.Context, int):java.lang.String");
    }

    public static void resetLevelHighScore(Context ctx, int level2) {
        SQLiteDatabase myDB = null;
        try {
            myDB = ctx.openOrCreateDatabase(DATABASE_NAME, 0, null);
            myDB.delete(TABLE_HIGHSCORES, "level = " + level2, null);
            if (myDB != null) {
                myDB.close();
            }
        } catch (Exception e) {
            Log.w(BubbleBusterConstants.TAG, "DB Error", e);
            if (myDB != null) {
                myDB.close();
            }
        } catch (Throwable th) {
            if (myDB != null) {
                myDB.close();
            }
            throw th;
        }
    }

    public static void resetLevelPackHighScore(Context ctx, int levelPack) {
        SQLiteDatabase myDB = null;
        try {
            myDB = ctx.openOrCreateDatabase(DATABASE_NAME, 0, null);
            myDB.delete(TABLE_HIGHSCORES, "level >= " + (levelPack * 100) + "   AND " + level + " < " + ((levelPack + 1) * 100), null);
            if (myDB != null) {
                myDB.close();
            }
        } catch (Exception e) {
            Log.w("DB", "DB Error", e);
            if (myDB != null) {
                myDB.close();
            }
        } catch (Throwable th) {
            if (myDB != null) {
                myDB.close();
            }
            throw th;
        }
    }

    public static void resetHighScore(Context ctx) {
        SQLiteDatabase myDB = null;
        try {
            myDB = ctx.openOrCreateDatabase(DATABASE_NAME, 0, null);
            myDB.delete(TABLE_HIGHSCORES, null, null);
            if (myDB != null) {
                myDB.close();
            }
        } catch (Exception e) {
            Log.w(BubbleBusterConstants.TAG, "DB Error", e);
            if (myDB != null) {
                myDB.close();
            }
        } catch (Throwable th) {
            if (myDB != null) {
                myDB.close();
            }
            throw th;
        }
    }
}
