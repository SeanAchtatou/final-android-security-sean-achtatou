package com.celliecraze.mm.highscores;

import com.celliecraze.mm.FrozenBubble;

public class HighscoreManager {
    private static boolean gameRunning = true;
    private static HighScore hs;
    private static int lastScore;
    private static String lastTime;
    private static long startTime;
    private static long totalTime;

    public static void reset() {
        lastScore = 1340;
        totalTime = 0;
        startTime = System.currentTimeMillis();
        hs = new HighScore(0, 0, 0);
    }

    public static void startLevel() {
        reset();
        gameRunning = true;
        lastTime = "00:00";
    }

    public static void endLevel(int nbBubbles) {
        long[] scoreTime = getScore(nbBubbles);
        hs = new HighScore(scoreTime[0], nbBubbles, scoreTime[1]);
        gameRunning = false;
    }

    public static HighScore getHs() {
        return hs;
    }

    public static int getActualScore(int nbBubbles) {
        if (!FrozenBubble.gamePausedActive && gameRunning) {
            totalTime += System.currentTimeMillis() - startTime;
            startTime = System.currentTimeMillis();
            lastScore = (int) getScore(nbBubbles)[0];
        }
        return lastScore;
    }

    public static String getTime(boolean showHours) {
        if (!FrozenBubble.gamePausedActive && gameRunning) {
            if (showHours) {
                long seconds = totalTime / 1000;
                lastTime = String.valueOf(formatTwoPlaces(((int) seconds) / 3600)) + ":" + formatTwoPlaces(((int) seconds) / 60) + ":" + formatTwoPlaces(((int) seconds) % 60);
            } else {
                long seconds2 = totalTime / 1000;
                lastTime = String.valueOf(formatTwoPlaces(((int) seconds2) / 60)) + ":" + formatTwoPlaces(((int) seconds2) % 60);
            }
        }
        return lastTime;
    }

    private static String formatTwoPlaces(int number) {
        if (number < 10) {
            return "0" + number;
        }
        return new StringBuilder(String.valueOf(number)).toString();
    }

    public static void pause(boolean pause) {
        if (pause) {
            totalTime += System.currentTimeMillis() - startTime;
        } else {
            startTime = System.currentTimeMillis();
        }
    }

    private static long[] getScore(int nbBubbles) {
        long[] scoreTime = new long[2];
        long score = (long) (1040 - (nbBubbles * 5));
        long time = totalTime / 1000;
        if (time == 0) {
            time = 1;
        }
        scoreTime[0] = score + (300 - time < 0 ? 0 : 300 - time);
        scoreTime[1] = time;
        return scoreTime;
    }
}
