package com.celliecraze.mm.activity;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import com.celliecraze.mm.MMApplication;
import com.celliecraze.mm.R;

public class TwitterAuthorizationActivity extends Activity {
    /* access modifiers changed from: private */
    public MMApplication app;
    /* access modifiers changed from: private */
    public WebView webView;
    private WebViewClient webViewClient = new WebViewClient() {
        public void onLoadResource(WebView view, String url) {
            Uri uri = Uri.parse(url);
            Log.d("tictacdroide", uri.getHost());
            if (!uri.getHost().equals("otweet.com")) {
                super.onLoadResource(view, url);
            } else if (uri.getQueryParameter("oauth_token") != null) {
                TwitterAuthorizationActivity.this.webView.setVisibility(4);
                TwitterAuthorizationActivity.this.app.authorizedTwitter();
                TwitterAuthorizationActivity.this.finish();
            } else {
                Toast.makeText(TwitterAuthorizationActivity.this.getApplicationContext(), "Error, try again...", 0).show();
            }
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.app = (MMApplication) getApplication();
        setContentView((int) R.layout.authorization_view);
        setUpViews();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.webView.loadUrl(this.app.beginTwitterAuthorization());
    }

    private void setUpViews() {
        this.webView = (WebView) findViewById(R.id.web_view);
        this.webView.setWebViewClient(this.webViewClient);
    }
}
