package com.celliecraze.mm.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.celliecraze.mm.R;

public class BubbleBusterHowToActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.how_to);
        ScrollView sView = (ScrollView) findViewById(R.id.how_to_scrollview);
        sView.setVerticalScrollBarEnabled(false);
        sView.setHorizontalScrollBarEnabled(false);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        Animation localAnimation1 = AnimationUtils.loadAnimation(this, R.anim.slide_left_to_right);
        Animation localAnimation2 = AnimationUtils.loadAnimation(this, R.anim.slide_top_to_bottom);
        ((ImageView) findViewById(R.id.background)).setAnimation(localAnimation1);
        ((TextView) findViewById(R.id.t_how_goal)).setAnimation(localAnimation2);
        ((TextView) findViewById(R.id.t_how_level)).setAnimation(localAnimation2);
        ((TextView) findViewById(R.id.t_how_score)).setAnimation(localAnimation2);
        ((TextView) findViewById(R.id.t_how_launcher)).setAnimation(localAnimation2);
        ((TextView) findViewById(R.id.t_how_current)).setAnimation(localAnimation2);
        ((TextView) findViewById(R.id.t_how_swap)).setAnimation(localAnimation2);
        ((TextView) findViewById(R.id.t_how_next)).setAnimation(localAnimation2);
        ((ImageView) findViewById(R.id.background)).getAnimation().startNow();
        ((TextView) findViewById(R.id.t_how_goal)).getAnimation().startNow();
        ((TextView) findViewById(R.id.t_how_level)).getAnimation().startNow();
        ((TextView) findViewById(R.id.t_how_score)).getAnimation().startNow();
        ((TextView) findViewById(R.id.t_how_launcher)).getAnimation().startNow();
        ((TextView) findViewById(R.id.t_how_current)).getAnimation().startNow();
        ((TextView) findViewById(R.id.t_how_swap)).getAnimation().startNow();
        ((TextView) findViewById(R.id.t_how_next)).getAnimation().startNow();
    }
}
