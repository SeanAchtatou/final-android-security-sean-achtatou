package com.celliecraze.mm.activity;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class BubbleBusterHelpActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WebView localWebView = new WebView(this);
        localWebView.setScrollBarStyle(1);
        localWebView.loadUrl("file:///android_asset/help/help.html");
        setContentView(localWebView);
    }
}
