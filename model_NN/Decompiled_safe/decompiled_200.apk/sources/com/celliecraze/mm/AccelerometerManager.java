package com.celliecraze.mm;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import java.util.List;

public class AccelerometerManager {
    /* access modifiers changed from: private */
    public static AccelerometerListener listener;
    private static boolean running = false;
    private static Sensor sensor;
    private static SensorEventListener sensorEventListener = new SensorEventListener() {
        private float avgX = 0.0f;
        private float avgY = 0.0f;
        private float avgZ = 0.0f;
        private float filter = 0.05f;

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        public void onSensorChanged(SensorEvent event) {
            this.avgX = (event.values[0] * this.filter) + (this.avgX * (1.0f - this.filter));
            this.avgY = (event.values[1] * this.filter) + (this.avgY * (1.0f - this.filter));
            this.avgZ = (event.values[2] * this.filter) + (this.avgZ * (1.0f - this.filter));
            AccelerometerManager.listener.onAccelerationChanged(this.avgX, this.avgY, this.avgZ);
        }
    };
    private static SensorManager sensorManager;
    private static Boolean supported;

    public static boolean isListening() {
        return running;
    }

    public static void stopListening() {
        running = false;
        try {
            if (sensorManager != null && sensorEventListener != null) {
                sensorManager.unregisterListener(sensorEventListener);
            }
        } catch (Exception e) {
        }
    }

    public static boolean isSupported(Context context) {
        if (supported == null) {
            sensorManager = (SensorManager) context.getSystemService("sensor");
            supported = new Boolean(sensorManager.getSensorList(1).size() > 0);
        }
        return supported.booleanValue();
    }

    public static void startListening(Context context, AccelerometerListener accelerometerListener) {
        sensorManager = (SensorManager) context.getSystemService("sensor");
        List<Sensor> sensors = sensorManager.getSensorList(1);
        if (sensors.size() > 0) {
            sensor = sensors.get(0);
            running = sensorManager.registerListener(sensorEventListener, sensor, 0);
            listener = accelerometerListener;
        }
    }
}
