package com.celliecraze.mm.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class MyTextView extends TextView {
    public MyTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public MyTextView(Context context) {
        super(context);
        init(context);
    }

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        Typeface tf = getTypeface();
        if (tf == null) {
            tf = Typeface.createFromAsset(context.getAssets(), "fonts/utb.ttf");
        }
        setTypeface(tf);
        setTextSize((float) ((int) ((16.0f * getResources().getDisplayMetrics().density) + 0.5f)));
    }
}
