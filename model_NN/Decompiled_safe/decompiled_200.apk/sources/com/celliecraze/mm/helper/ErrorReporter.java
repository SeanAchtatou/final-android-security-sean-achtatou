package com.celliecraze.mm.helper;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import com.celliecraze.mm.R;
import com.celliecraze.mm.activity.BubbleBusterAboutActivity;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread;
import java.util.Date;
import java.util.Random;

public class ErrorReporter implements Thread.UncaughtExceptionHandler {
    private static ErrorReporter S_mInstance;
    String AndroidVersion;
    String Board;
    String Brand;
    private Context CurContext;
    String Device;
    String Display;
    String FilePath;
    String FingerPrint;
    String Host;
    String ID;
    String Manufacturer;
    String Model;
    String PackageName;
    String PhoneModel;
    private Thread.UncaughtExceptionHandler PreviousHandler;
    String Product;
    String Tags;
    long Time;
    String Type;
    String User;
    String VersionName;

    public void Init(Context context) {
        this.PreviousHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
        RecoltInformations(context);
        this.CurContext = context;
    }

    public long getAvailableInternalMemorySize() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        return ((long) stat.getAvailableBlocks()) * ((long) stat.getBlockSize());
    }

    public long getTotalInternalMemorySize() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        return ((long) stat.getBlockCount()) * ((long) stat.getBlockSize());
    }

    /* access modifiers changed from: package-private */
    public void RecoltInformations(Context context) {
        try {
            PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            this.VersionName = pi.versionName;
            this.PackageName = pi.packageName;
            this.FilePath = context.getFilesDir().getAbsolutePath();
            this.PhoneModel = Build.MODEL;
            this.AndroidVersion = Build.VERSION.RELEASE;
            this.Board = Build.BOARD;
            this.Brand = Build.BRAND;
            this.Device = Build.DEVICE;
            this.Display = Build.DISPLAY;
            this.FingerPrint = Build.FINGERPRINT;
            this.Host = Build.HOST;
            this.ID = Build.ID;
            this.Model = Build.MODEL;
            this.Product = Build.PRODUCT;
            this.Tags = Build.TAGS;
            this.Time = Build.TIME;
            this.Type = Build.TYPE;
            this.User = Build.USER;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public String CreateInformationString() {
        return String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf("") + "Version : " + this.VersionName) + "\n") + "Package : " + this.PackageName) + "\n") + "FilePath : " + this.FilePath) + "\n") + "Phone Model : " + this.PhoneModel) + "\n") + "Android Version : " + this.AndroidVersion) + "\n") + "Board : " + this.Board) + "\n") + "Brand : " + this.Brand) + "\n") + "Device : " + this.Device) + "\n") + "Display : " + this.Display) + "\n") + "Finger Print : " + this.FingerPrint) + "\n") + "Host : " + this.Host) + "\n") + "ID : " + this.ID) + "\n") + "Model : " + this.Model) + "\n") + "Product : " + this.Product) + "\n") + "Tags : " + this.Tags) + "\n") + "Time : " + this.Time) + "\n") + "Type : " + this.Type) + "\n") + "User : " + this.User) + "\n") + "Total Internal memory : " + getTotalInternalMemorySize()) + "\n") + "Available Internal memory : " + getAvailableInternalMemorySize()) + "\n";
    }

    public void uncaughtException(Thread t, Throwable e) {
        String Report = String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf("") + "Error Report collected on : " + new Date().toString()) + "\n") + "\n") + "Informations :") + "\n") + "==============") + "\n") + "\n") + CreateInformationString()) + "\n\n") + "Stack : \n") + "======= \n";
        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        e.printStackTrace(printWriter);
        String Report2 = String.valueOf(String.valueOf(String.valueOf(String.valueOf(Report) + result.toString()) + "\n") + "Cause : \n") + "======= \n";
        for (Throwable cause = e.getCause(); cause != null; cause = cause.getCause()) {
            cause.printStackTrace(printWriter);
            Report2 = String.valueOf(Report2) + result.toString();
        }
        printWriter.close();
        SaveAsFile(String.valueOf(Report2) + "****  End of current Report ***");
        this.PreviousHandler.uncaughtException(t, e);
    }

    public static ErrorReporter getInstance() {
        if (S_mInstance == null) {
            S_mInstance = new ErrorReporter();
        }
        return S_mInstance;
    }

    private void SendErrorMail(Context _context, String ErrorContent) {
        Intent sendIntent = new Intent("android.intent.action.SEND");
        sendIntent.putExtra("android.intent.extra.EMAIL", new String[]{_context.getString(R.string.email)});
        sendIntent.putExtra("android.intent.extra.TEXT", String.valueOf(_context.getResources().getString(R.string.crashreport_note)) + "\n\n" + ErrorContent + "\n\n");
        sendIntent.putExtra("android.intent.extra.SUBJECT", String.valueOf(_context.getResources().getString(R.string.crashreport_subject)) + " " + _context.getString(R.string.app_name) + " " + BubbleBusterAboutActivity.getVersionName(_context));
        sendIntent.setType("message/rfc822");
        _context.startActivity(Intent.createChooser(sendIntent, "Title:"));
    }

    private void SaveAsFile(String ErrorContent) {
        try {
            FileOutputStream trace = this.CurContext.openFileOutput("stack-" + new Random().nextInt(99999) + ".stacktrace", 0);
            trace.write(ErrorContent.getBytes());
            trace.close();
        } catch (IOException e) {
        }
    }

    private String[] GetErrorFileList() {
        File dir = new File(String.valueOf(this.FilePath) + "/");
        dir.mkdir();
        return dir.list(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(".stacktrace");
            }
        });
    }

    public boolean bIsThereAnyErrorFile() {
        return GetErrorFileList().length > 0;
    }

    public void CheckErrorAndSendMail(Context _context) {
        try {
            if (bIsThereAnyErrorFile()) {
                String WholeErrorText = "";
                String[] ErrorFileList = GetErrorFileList();
                int length = ErrorFileList.length;
                int i = 0;
                int curIndex = 0;
                while (i < length) {
                    String curString = ErrorFileList[i];
                    int curIndex2 = curIndex + 1;
                    if (curIndex <= 5) {
                        WholeErrorText = String.valueOf(String.valueOf(WholeErrorText) + "New Trace collected :\n") + "=====================\n ";
                        BufferedReader bufferedReader = new BufferedReader(new FileReader(String.valueOf(this.FilePath) + "/" + curString));
                        while (true) {
                            String line = bufferedReader.readLine();
                            if (line == null) {
                                break;
                            }
                            WholeErrorText = String.valueOf(WholeErrorText) + line + "\n";
                        }
                        bufferedReader.close();
                    }
                    new File(String.valueOf(this.FilePath) + "/" + curString).delete();
                    i++;
                    curIndex = curIndex2;
                }
                SendErrorMail(_context, WholeErrorText);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteErrorFiles() {
        try {
            String[] ErrorFileList = GetErrorFileList();
            int length = ErrorFileList.length;
            for (int i = 0; i < length; i++) {
                new File(String.valueOf(this.FilePath) + "/" + ErrorFileList[i]).delete();
            }
        } catch (Exception e) {
        }
    }
}
