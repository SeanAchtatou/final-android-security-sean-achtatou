package com.celliecraze.mm;

import android.graphics.Bitmap;

class BmpWrap {
    public Bitmap bmp;
    public int id;
    public String name;

    BmpWrap(int id2, String name2) {
        this.id = id2;
        this.name = name2;
    }
}
