package com.scoreloop.client.android.ui.component.user;

import com.celliecraze.mm.R;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.StandardListItem;

public class UserFindMatchListItem extends StandardListItem<Void> {
    public UserFindMatchListItem(ComponentActivity activity) {
        super(activity, activity.getResources().getDrawable(R.drawable.sl_icon_recommend), activity.getResources().getString(R.string.sl_find_match), null, null);
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.sl_list_item_user;
    }

    public int getType() {
        return 28;
    }
}
