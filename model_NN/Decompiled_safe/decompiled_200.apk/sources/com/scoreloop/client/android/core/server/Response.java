package com.scoreloop.client.android.core.server;

import org.json.JSONArray;
import org.json.JSONObject;

public class Response {
    private final String a;
    private final Object b;
    private final int c;
    private final Integer d;
    private final boolean e;

    public Response(Object obj, String str, int i, Integer num) {
        this.b = obj;
        this.a = str;
        this.c = i;
        this.d = num;
        this.e = obj instanceof JSONArray;
    }

    public Object a() {
        return this.b;
    }

    public Integer b() {
        return this.d;
    }

    public boolean c() {
        return this.e;
    }

    public JSONArray d() {
        return (JSONArray) this.b;
    }

    public JSONObject e() {
        return (JSONObject) this.b;
    }

    public int f() {
        return this.c;
    }
}
