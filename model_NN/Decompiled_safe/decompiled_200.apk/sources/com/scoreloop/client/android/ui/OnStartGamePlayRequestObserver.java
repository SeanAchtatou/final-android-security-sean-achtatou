package com.scoreloop.client.android.ui;

public interface OnStartGamePlayRequestObserver {
    void onStartGamePlayRequest(Integer num);
}
