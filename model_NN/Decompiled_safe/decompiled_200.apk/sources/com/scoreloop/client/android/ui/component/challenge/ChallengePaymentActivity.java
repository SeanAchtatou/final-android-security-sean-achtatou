package com.scoreloop.client.android.ui.component.challenge;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.celliecraze.mm.R;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.framework.BaseDialog;
import com.scoreloop.client.android.ui.framework.NavigationIntent;
import com.scoreloop.client.android.ui.framework.OkCancelDialog;

public class ChallengePaymentActivity extends ComponentActivity implements BaseDialog.OnActionListener {
    protected NavigationIntent _navigationIntent;
    private WebView _webView;

    private class PaymentWebViewClient extends WebViewClient {
        private boolean _showsSpinner;

        private PaymentWebViewClient() {
        }

        /* synthetic */ PaymentWebViewClient(ChallengePaymentActivity challengePaymentActivity, PaymentWebViewClient paymentWebViewClient) {
            this();
        }

        public void onPageFinished(WebView view, String url) {
            ChallengePaymentActivity.this.getUserValues().setDirty(Constant.USER_BALANCE);
            if (this._showsSpinner) {
                ChallengePaymentActivity.this.hideSpinner();
                view.requestFocus();
                this._showsSpinner = false;
            }
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            if (!this._showsSpinner) {
                ChallengePaymentActivity.this.showSpinner();
                this._showsSpinner = true;
            }
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 7:
                OkCancelDialog dialog = new OkCancelDialog(this);
                dialog.setText(getResources().getString(R.string.sl_leave_payment));
                dialog.setOnActionListener(this);
                dialog.setOnDismissListener(this);
                return dialog;
            default:
                return super.onCreateDialog(id);
        }
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
            case 7:
                ((OkCancelDialog) dialog).setTarget(this._navigationIntent);
                break;
        }
        super.onPrepareDialog(id, dialog);
    }

    /* access modifiers changed from: protected */
    public boolean isNavigationAllowed(NavigationIntent navigationIntent) {
        this._navigationIntent = navigationIntent;
        showDialogSafe(7, true);
        return false;
    }

    /* Debug info: failed to restart local var, previous not found, register: 0 */
    public void onAction(BaseDialog dialog, int actionId) {
        if (actionId == 0) {
            dialog.dismiss();
            ((NavigationIntent) dialog.getTarget()).execute();
            return;
        }
        dialog.dismiss();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getActivityArguments().putValue(Constant.NAVIGATION_INTENT, this._navigationIntent);
    }

    public void onCreate(Bundle savedInstanceState) {
        this._navigationIntent = (NavigationIntent) getActivityArguments().getValue(Constant.NAVIGATION_INTENT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sl_challenge_payment, true);
        this._webView = (WebView) findViewById(R.id.sl_webview);
        this._webView.setWebViewClient(new PaymentWebViewClient(this, null));
        WebSettings settings = this._webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDefaultTextEncodingName("UTF-8");
        settings.setBuiltInZoomControls(true);
        this._webView.loadUrl(getSession().getPaymentUrl());
        this._webView.requestFocus();
    }
}
