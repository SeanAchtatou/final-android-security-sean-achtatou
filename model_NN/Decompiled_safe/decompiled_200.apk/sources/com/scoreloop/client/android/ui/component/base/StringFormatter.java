package com.scoreloop.client.android.ui.component.base;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.celliecraze.mm.R;
import com.scoreloop.client.android.core.addon.RSSItem;
import com.scoreloop.client.android.core.model.Money;
import com.scoreloop.client.android.core.model.Ranking;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.ScoreFormatter;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.framework.ValueStore;
import java.math.BigDecimal;
import java.util.List;

public class StringFormatter {
    private static final BigDecimal CENTS_FOR_UNIT = new BigDecimal(100);
    private static final int ROUND_TO_PERCENT = 5;

    public static String formatChallengesScore(Score score, Configuration configuration) {
        ScoreFormatter.ScoreFormatKey format = configuration.getChallengesScoreFormat();
        ScoreFormatter scoreFormatter = configuration.getScoreFormatter();
        if (format == null) {
            return formatScore(score, configuration);
        }
        return scoreFormatter.formatScore(score, format);
    }

    public static String formatLeaderboardsScore(Score score, Configuration configuration) {
        ScoreFormatter.ScoreFormatKey format = configuration.getLeaderboardScoreFormat();
        ScoreFormatter scoreFormatter = configuration.getScoreFormatter();
        if (format == null) {
            return formatScore(score, configuration);
        }
        return scoreFormatter.formatScore(score, format);
    }

    public static String formatMoney(Money money, Configuration configuration) {
        BigDecimal units = money.getAmount().divide(CENTS_FOR_UNIT);
        String currencyName = Money.getApplicationCurrencyNamePlural();
        if (units.equals(BigDecimal.ONE)) {
            currencyName = Money.getApplicationCurrencyNameSingular();
        }
        return String.format(configuration.getMoneyFormat(), units, currencyName);
    }

    public static String formatRanking(Context context, Ranking ranking, Configuration configuration) {
        int total;
        int roundedPercent;
        if (ranking == null || (total = ranking.getTotal().intValue()) == 0) {
            return "";
        }
        int percent = (ranking.getRank().intValue() * 100) / total;
        if (percent == 0) {
            roundedPercent = 1;
        } else if (percent == 100) {
            roundedPercent = 100;
        } else {
            roundedPercent = ((percent + 5) / 5) * 5;
        }
        return String.format(context.getString(R.string.sl_format_leaderboards_percent), Integer.valueOf(roundedPercent));
    }

    public static String formatScore(Score score, Configuration configuration) {
        String scoreResultFormat = configuration.getScoreResultFormat();
        if (scoreResultFormat == null) {
            return configuration.getScoreFormatter().formatScore(score, ScoreFormatter.ScoreFormatKey.ScoresOnlyFormat);
        }
        return String.format(scoreResultFormat, score.getResult());
    }

    public static String formatSocialNetworkPostScore(Score score, Configuration configuration) {
        ScoreFormatter.ScoreFormatKey format = configuration.getSocialNetworkPostScoreFormat();
        ScoreFormatter scoreFormatter = configuration.getScoreFormatter();
        if (format == null) {
            return formatScore(score, configuration);
        }
        return scoreFormatter.formatScore(score, format);
    }

    public static String getAchievementsSubTitle(Context context, ValueStore userValues, boolean extendedText) {
        Integer numAchieved = (Integer) userValues.getValue(Constant.NUMBER_ACHIEVEMENTS);
        Integer numTotal = (Integer) userValues.getValue(Constant.NUMBER_AWARDS);
        if (numAchieved == null || numTotal == null) {
            return "";
        }
        return String.format(context.getString(extendedText ? R.string.sl_format_achievements_extended : R.string.sl_format_achievements), numAchieved, numTotal);
    }

    public static String getBalanceSubTitle(Context context, ValueStore userValues, Configuration configuration) {
        Money balance = (Money) userValues.getValue(Constant.USER_BALANCE);
        if (balance == null) {
            return "";
        }
        return String.format(context.getString(R.string.sl_format_balance), formatMoney(balance, configuration));
    }

    public static String getBuddiesSubTitle(Context context, ValueStore userValues) {
        Integer count = (Integer) userValues.getValue(Constant.NUMBER_BUDDIES);
        if (count == null) {
            return "";
        }
        return String.format(context.getString(R.string.sl_format_number_friends), count);
    }

    public static String getChallengesSubTitle(Context context, ValueStore userValues) {
        Integer numWon = (Integer) userValues.getValue(Constant.NUMBER_CHALLENGES_WON);
        Integer numTotal = (Integer) userValues.getValue(Constant.NUMBER_CHALLENGES_PLAYED);
        if (numWon == null || numTotal == null) {
            return "";
        }
        return String.format(context.getString(R.string.sl_format_challenge_stats), numWon, numTotal);
    }

    public static String getGamesSubTitle(Context context, ValueStore userValues) {
        Integer count = (Integer) userValues.getValue(Constant.NUMBER_GAMES);
        if (count == null) {
            return "";
        }
        return String.format(context.getString(R.string.sl_format_number_games), count);
    }

    public static Drawable getNewsDrawable(Context context, ValueStore userValues, boolean large) {
        List<RSSItem> feed = (List) userValues.getValue(Constant.NEWS_FEED);
        Integer count = (Integer) userValues.getValue(Constant.NEWS_NUMBER_UNREAD_ITEMS);
        return context.getResources().getDrawable((feed == null || count == null || feed.size() == 0 || count.intValue() > 0) ? large ? R.drawable.sl_header_icon_news_closed : R.drawable.sl_icon_news_closed : large ? R.drawable.sl_header_icon_news_opened : R.drawable.sl_icon_news_opened);
    }

    public static String getNewsSubTitle(Context context, ValueStore userValues) {
        Integer count = (Integer) userValues.getValue(Constant.NEWS_NUMBER_UNREAD_ITEMS);
        if (count == null) {
            return "";
        }
        if (count.intValue() == 0) {
            List<RSSItem> feed = (List) userValues.getValue(Constant.NEWS_FEED);
            if (feed == null || feed.size() == 0) {
                return context.getString(R.string.sl_no_news);
            }
            return context.getString(R.string.sl_no_news_items);
        } else if (count.intValue() == 1) {
            return context.getString(R.string.sl_one_news_item);
        } else {
            return String.format(context.getString(R.string.sl_format_new_news_items), count);
        }
    }

    public static String getScoreTitle(Context context, Score score) {
        Integer rank = score.getRank();
        User user = score.getUser();
        if (user == null) {
            user = Session.getCurrentSession().getUser();
        }
        return String.format(context.getString(R.string.sl_format_score_title), rank, user.getDisplayName());
    }
}
