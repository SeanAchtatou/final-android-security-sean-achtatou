package com.scoreloop.client.android.ui.component.agent;

import com.celliecraze.mm.R;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerException;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.framework.BaseActivity;
import com.scoreloop.client.android.ui.framework.ValueStore;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ManageBuddiesTask implements RequestControllerObserver {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$scoreloop$client$android$ui$component$agent$ManageBuddiesTask$Mode;
    private final BaseActivity _activity;
    private final ManageBuddiesContinuation _continuation;
    private final UserController _controller;
    private int _count;
    private final Mode _mode;
    private final ValueStore _sessionUserValues;
    private final List<User> _users = new ArrayList();

    public interface ManageBuddiesContinuation {
        void withAddedOrRemovedBuddies(int i);
    }

    private enum Mode {
        ADD,
        REMOVE
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$scoreloop$client$android$ui$component$agent$ManageBuddiesTask$Mode() {
        int[] iArr = $SWITCH_TABLE$com$scoreloop$client$android$ui$component$agent$ManageBuddiesTask$Mode;
        if (iArr == null) {
            iArr = new int[Mode.values().length];
            try {
                iArr[Mode.ADD.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Mode.REMOVE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$com$scoreloop$client$android$ui$component$agent$ManageBuddiesTask$Mode = iArr;
        }
        return iArr;
    }

    public static void addBuddies(BaseActivity activity, List<User> users, ValueStore valueStore, ManageBuddiesContinuation continuation) {
        new ManageBuddiesTask(activity, Mode.ADD, users, valueStore, continuation);
    }

    public static void addBuddy(BaseActivity activity, User user, ValueStore valueStore, ManageBuddiesContinuation continuation) {
        new ManageBuddiesTask(activity, Mode.ADD, Collections.singletonList(user), valueStore, continuation);
    }

    public static void removeBuddy(BaseActivity activity, User user, ValueStore valueStore, ManageBuddiesContinuation continuation) {
        new ManageBuddiesTask(activity, Mode.REMOVE, Collections.singletonList(user), valueStore, continuation);
    }

    private ManageBuddiesTask(BaseActivity activity, Mode mode, List<User> users, ValueStore valueStore, ManageBuddiesContinuation continuation) {
        this._activity = activity;
        this._mode = mode;
        this._users.addAll(users);
        this._sessionUserValues = valueStore;
        this._continuation = continuation;
        this._controller = new UserController(this);
        processNextOrFinish();
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    private User popUser() {
        if (this._users == null) {
            return null;
        }
        if (this._users.isEmpty()) {
            return null;
        }
        return this._users.remove(0);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0055  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void processNextOrFinish() {
        /*
            r8 = this;
            com.scoreloop.client.android.core.model.Session r5 = com.scoreloop.client.android.core.model.Session.getCurrentSession()
            com.scoreloop.client.android.core.model.User r2 = r5.getUser()
            java.util.List r3 = r2.getBuddyUsers()
        L_0x000c:
            com.scoreloop.client.android.core.model.User r4 = r8.popUser()
            if (r4 == 0) goto L_0x0032
            boolean r5 = r2.equals(r4)
            if (r5 != 0) goto L_0x000c
            if (r3 == 0) goto L_0x0032
            com.scoreloop.client.android.ui.component.agent.ManageBuddiesTask$Mode r5 = r8._mode
            com.scoreloop.client.android.ui.component.agent.ManageBuddiesTask$Mode r6 = com.scoreloop.client.android.ui.component.agent.ManageBuddiesTask.Mode.ADD
            if (r5 != r6) goto L_0x0026
            boolean r5 = r3.contains(r4)
            if (r5 != 0) goto L_0x000c
        L_0x0026:
            com.scoreloop.client.android.ui.component.agent.ManageBuddiesTask$Mode r5 = r8._mode
            com.scoreloop.client.android.ui.component.agent.ManageBuddiesTask$Mode r6 = com.scoreloop.client.android.ui.component.agent.ManageBuddiesTask.Mode.REMOVE
            if (r5 != r6) goto L_0x0032
            boolean r5 = r3.contains(r4)
            if (r5 == 0) goto L_0x000c
        L_0x0032:
            if (r4 == 0) goto L_0x0055
            com.scoreloop.client.android.core.controller.UserController r5 = r8._controller
            r5.setUser(r4)
            int[] r5 = $SWITCH_TABLE$com$scoreloop$client$android$ui$component$agent$ManageBuddiesTask$Mode()
            com.scoreloop.client.android.ui.component.agent.ManageBuddiesTask$Mode r6 = r8._mode
            int r6 = r6.ordinal()
            r5 = r5[r6]
            switch(r5) {
                case 1: goto L_0x0049;
                case 2: goto L_0x004f;
                default: goto L_0x0048;
            }
        L_0x0048:
            return
        L_0x0049:
            com.scoreloop.client.android.core.controller.UserController r5 = r8._controller
            r5.addAsBuddy()
            goto L_0x0048
        L_0x004f:
            com.scoreloop.client.android.core.controller.UserController r5 = r8._controller
            r5.removeAsBuddy()
            goto L_0x0048
        L_0x0055:
            com.scoreloop.client.android.ui.framework.ValueStore r5 = r8._sessionUserValues
            java.lang.String r6 = "numberBuddies"
            java.lang.Object r1 = r5.getValue(r6)
            java.lang.Integer r1 = (java.lang.Integer) r1
            if (r1 == 0) goto L_0x007a
            com.scoreloop.client.android.ui.component.agent.ManageBuddiesTask$Mode r5 = r8._mode
            com.scoreloop.client.android.ui.component.agent.ManageBuddiesTask$Mode r6 = com.scoreloop.client.android.ui.component.agent.ManageBuddiesTask.Mode.ADD
            if (r5 != r6) goto L_0x008b
            int r5 = r1.intValue()
            int r6 = r8._count
            int r5 = r5 + r6
            r0 = r5
        L_0x006f:
            com.scoreloop.client.android.ui.framework.ValueStore r5 = r8._sessionUserValues
            java.lang.String r6 = "numberBuddies"
            java.lang.Integer r7 = java.lang.Integer.valueOf(r0)
            r5.putValue(r6, r7)
        L_0x007a:
            com.scoreloop.client.android.ui.framework.ValueStore r5 = r8._sessionUserValues
            r5.setAllDirty()
            com.scoreloop.client.android.ui.component.agent.ManageBuddiesTask$ManageBuddiesContinuation r5 = r8._continuation
            if (r5 == 0) goto L_0x0048
            com.scoreloop.client.android.ui.component.agent.ManageBuddiesTask$ManageBuddiesContinuation r5 = r8._continuation
            int r6 = r8._count
            r5.withAddedOrRemovedBuddies(r6)
            goto L_0x0048
        L_0x008b:
            int r5 = r1.intValue()
            int r6 = r8._count
            int r5 = r5 - r6
            r0 = r5
            goto L_0x006f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.scoreloop.client.android.ui.component.agent.ManageBuddiesTask.processNextOrFinish():void");
    }

    public void requestControllerDidFail(RequestController aRequestController, Exception anException) {
        if (anException instanceof RequestControllerException) {
            int code = ((RequestControllerException) anException).getErrorCode();
            switch ($SWITCH_TABLE$com$scoreloop$client$android$ui$component$agent$ManageBuddiesTask$Mode()[this._mode.ordinal()]) {
                case 1:
                    if (code == 40) {
                        this._activity.showToast(String.format(this._activity.getString(R.string.sl_format_friend_already_added), this._controller.getUser().getDisplayName()));
                        break;
                    }
                    break;
                case 2:
                    if (code == 41) {
                        this._activity.showToast(String.format(this._activity.getString(R.string.sl_format_friend_already_removed), this._controller.getUser().getDisplayName()));
                        break;
                    }
                    break;
            }
        }
        processNextOrFinish();
    }

    public void requestControllerDidReceiveResponse(RequestController aRequestController) {
        this._count++;
        processNextOrFinish();
    }
}
