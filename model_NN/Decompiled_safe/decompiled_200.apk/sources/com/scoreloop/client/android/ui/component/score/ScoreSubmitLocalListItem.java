package com.scoreloop.client.android.ui.component.score;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.celliecraze.mm.R;
import com.scoreloop.client.android.ui.framework.BaseListItem;

public class ScoreSubmitLocalListItem extends BaseListItem {
    public ScoreSubmitLocalListItem(Context context) {
        super(context, null, null);
    }

    public int getType() {
        return 22;
    }

    public View getView(View view, ViewGroup parent) {
        if (view == null) {
            view = getLayoutInflater().inflate((int) R.layout.sl_list_item_score_submit_local, (ViewGroup) null);
        }
        prepareView(view);
        return view;
    }

    public boolean isEnabled() {
        return true;
    }

    private void prepareView(final View view) {
        view.setEnabled(false);
        ((Button) view.findViewById(R.id.sl_submit_local_score_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                view.performClick();
            }
        });
    }
}
