package com.scoreloop.client.android.ui.component.news;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.celliecraze.mm.R;
import com.scoreloop.client.android.core.addon.RSSItem;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import com.scoreloop.client.android.ui.util.ImageDownloader;

public class NewsListItem extends BaseListItem {
    private final RSSItem _item;

    public NewsListItem(Context context, RSSItem item) {
        super(context, null, item.getTitle());
        this._item = item;
    }

    public RSSItem getItem() {
        return this._item;
    }

    public int getType() {
        return 15;
    }

    public View getView(View view, ViewGroup parent) {
        if (view == null) {
            view = getLayoutInflater().inflate((int) R.layout.sl_list_item_news, (ViewGroup) null);
        }
        ImageView icon = (ImageView) view.findViewById(R.id.sl_list_item_news_icon);
        int resId = this._item.hasPersistentReadFlag() ? R.drawable.sl_icon_news_opened : R.drawable.sl_icon_news_closed;
        icon.setImageResource(resId);
        String imageUrl = this._item.getImageUrlString();
        if (imageUrl != null) {
            ImageDownloader.downloadImage(imageUrl, getContext().getResources().getDrawable(resId), icon, null);
        }
        ((TextView) view.findViewById(R.id.sl_list_item_news_title)).setText(getTitle());
        ((TextView) view.findViewById(R.id.sl_list_item_news_description)).setText(this._item.getDescription());
        view.findViewById(R.id.sl_list_item_news_accessory).setVisibility(isEnabled() ? 0 : 4);
        return view;
    }

    public boolean isEnabled() {
        return this._item.getLinkUrlString() != null;
    }
}
