package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.PublishedFor__2_3_0;

public class ScoreSubmitException extends Exception {
    private static final long serialVersionUID = 1;

    @PublishedFor__2_3_0
    public ScoreSubmitException(String str, Throwable th) {
        super(str, th);
    }
}
