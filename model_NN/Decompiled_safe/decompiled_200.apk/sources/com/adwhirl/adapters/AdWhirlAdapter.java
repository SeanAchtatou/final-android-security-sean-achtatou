package com.adwhirl.adapters;

import android.util.Log;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.obj.Ration;
import com.adwhirl.util.AdWhirlUtil;
import com.scoreloop.client.android.ui.component.base.Constant;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;

public abstract class AdWhirlAdapter {
    protected static String googleAdSenseAppName;
    protected static String googleAdSenseChannel;
    protected static String googleAdSenseCompanyName;
    protected static String googleAdSenseExpandDirection;
    protected final WeakReference<AdWhirlLayout> adWhirlLayoutReference;
    protected Ration ration;

    public abstract void handle();

    public AdWhirlAdapter(AdWhirlLayout adWhirlLayout, Ration ration2) {
        this.adWhirlLayoutReference = new WeakReference<>(adWhirlLayout);
        this.ration = ration2;
    }

    private static AdWhirlAdapter getAdapter(AdWhirlLayout adWhirlLayout, Ration ration2) {
        try {
            switch (ration2.type) {
                case 1:
                    if (Class.forName("com.google.ads.AdView") != null) {
                        return getNetworkAdapter("com.adwhirl.adapters.GoogleAdMobAdsAdapter", adWhirlLayout, ration2);
                    }
                    return unknownAdNetwork(adWhirlLayout, ration2);
                case 2:
                case 3:
                case 4:
                case 5:
                case 7:
                case 10:
                case 11:
                case 13:
                case 15:
                case 19:
                case Constant.LIST_ITEM_TYPE_SCORE_HIGHLIGHTED /*21*/:
                case Constant.LIST_ITEM_TYPE_SCORE_SUBMIT_LOCAL /*22*/:
                default:
                    return unknownAdNetwork(adWhirlLayout, ration2);
                case 6:
                    if (Class.forName("com.millennialmedia.android.MMAdView") != null) {
                        return getNetworkAdapter("com.adwhirl.adapters.MillennialAdapter", adWhirlLayout, ration2);
                    }
                    return unknownAdNetwork(adWhirlLayout, ration2);
                case 8:
                    if (Class.forName("com.qwapi.adclient.android.view.QWAdView") != null) {
                        return getNetworkAdapter("com.adwhirl.adapters.QuattroAdapter", adWhirlLayout, ration2);
                    }
                    return unknownAdNetwork(adWhirlLayout, ration2);
                case 9:
                    return new CustomAdapter(adWhirlLayout, ration2);
                case 12:
                    return getNetworkAdapter("com.adwhirl.adapters.MdotMAdapter", adWhirlLayout, ration2);
                case 14:
                    if (Class.forName("com.google.ads.GoogleAdView") != null) {
                        return getNetworkAdapter("com.adwhirl.adapters.AdSenseAdapter", adWhirlLayout, ration2);
                    }
                    return unknownAdNetwork(adWhirlLayout, ration2);
                case 16:
                    return new GenericAdapter(adWhirlLayout, ration2);
                case 17:
                    return new EventAdapter(adWhirlLayout, ration2);
                case 18:
                    if (Class.forName("com.inmobi.androidsdk.impl.InMobiAdView") != null) {
                        return getNetworkAdapter("com.adwhirl.adapters.InMobiAdapter", adWhirlLayout, ration2);
                    }
                    return unknownAdNetwork(adWhirlLayout, ration2);
                case 20:
                    if (Class.forName("com.zestadz.android.ZestADZAdView") != null) {
                        return getNetworkAdapter("com.adwhirl.adapters.ZestAdzAdapter", adWhirlLayout, ration2);
                    }
                    return unknownAdNetwork(adWhirlLayout, ration2);
                case 23:
                    return getNetworkAdapter("com.adwhirl.adapters.OneRiotAdapter", adWhirlLayout, ration2);
            }
        } catch (ClassNotFoundException e) {
            return unknownAdNetwork(adWhirlLayout, ration2);
        } catch (VerifyError e2) {
            Log.e("AdWhirl", "Caught VerifyError", e2);
            return unknownAdNetwork(adWhirlLayout, ration2);
        }
    }

    private static AdWhirlAdapter getNetworkAdapter(String networkAdapter, AdWhirlLayout adWhirlLayout, Ration ration2) {
        try {
            return (AdWhirlAdapter) Class.forName(networkAdapter).getConstructor(AdWhirlLayout.class, Ration.class).newInstance(adWhirlLayout, ration2);
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | SecurityException | InvocationTargetException e) {
            return null;
        }
    }

    private static AdWhirlAdapter unknownAdNetwork(AdWhirlLayout adWhirlLayout, Ration ration2) {
        Log.w(AdWhirlUtil.ADWHIRL, "Unsupported ration type: " + ration2.type);
        return null;
    }

    public static void handle(AdWhirlLayout adWhirlLayout, Ration ration2) throws Throwable {
        AdWhirlAdapter adapter = getAdapter(adWhirlLayout, ration2);
        if (adapter != null) {
            Log.d(AdWhirlUtil.ADWHIRL, "Valid adapter, calling handle()");
            adapter.handle();
            return;
        }
        throw new Exception("Invalid adapter");
    }

    public static void setGoogleAdSenseCompanyName(String name) {
        googleAdSenseCompanyName = name;
    }

    public static void setGoogleAdSenseAppName(String name) {
        googleAdSenseAppName = name;
    }

    public static void setGoogleAdSenseChannel(String channel) {
        googleAdSenseChannel = channel;
    }

    public static void setGoogleAdSenseExpandDirection(String direction) {
        googleAdSenseExpandDirection = direction;
    }
}
