package org.anddev.andengine.opengl.texture.builder;

import java.util.ArrayList;
import org.anddev.andengine.opengl.texture.BuildableTexture;

public interface ITextureBuilder {

    public static class TextureSourcePackingException extends Exception {
        private static final long serialVersionUID = 4700734424214372671L;
    }

    void pack(BuildableTexture buildableTexture, ArrayList<BuildableTexture.TextureSourceWithWithLocationCallback> arrayList) throws TextureSourcePackingException;
}
