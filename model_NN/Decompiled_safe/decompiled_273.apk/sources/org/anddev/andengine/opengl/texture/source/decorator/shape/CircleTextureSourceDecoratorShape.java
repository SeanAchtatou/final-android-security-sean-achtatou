package org.anddev.andengine.opengl.texture.source.decorator.shape;

import android.graphics.Canvas;
import android.graphics.Paint;
import org.anddev.andengine.opengl.texture.source.decorator.BaseTextureSourceDecorator;

public class CircleTextureSourceDecoratorShape implements ITextureSourceDecoratorShape {
    private static CircleTextureSourceDecoratorShape sDefaultInstance;

    public static CircleTextureSourceDecoratorShape getDefaultInstance() {
        if (sDefaultInstance == null) {
            sDefaultInstance = new CircleTextureSourceDecoratorShape();
        }
        return sDefaultInstance;
    }

    public void onDecorateBitmap(Canvas pCanvas, Paint pPaint, BaseTextureSourceDecorator.TextureSourceDecoratorOptions pDecoratorOptions) {
        pCanvas.drawCircle(((((float) pCanvas.getWidth()) + pDecoratorOptions.getInsetLeft()) - pDecoratorOptions.getInsetRight()) / 2.0f, ((((float) pCanvas.getHeight()) + pDecoratorOptions.getInsetTop()) - pDecoratorOptions.getInsetBottom()) / 2.0f, Math.min(((((float) pCanvas.getWidth()) - pDecoratorOptions.getInsetLeft()) - pDecoratorOptions.getInsetRight()) / 2.0f, ((((float) pCanvas.getHeight()) - pDecoratorOptions.getInsetTop()) - pDecoratorOptions.getInsetBottom()) / 2.0f), pPaint);
    }
}
