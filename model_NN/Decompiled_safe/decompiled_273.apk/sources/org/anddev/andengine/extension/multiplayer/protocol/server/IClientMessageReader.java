package org.anddev.andengine.extension.multiplayer.protocol.server;

import java.io.DataInputStream;
import java.io.IOException;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.IMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.IClientMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.connection.ConnectionCloseClientMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.connection.ConnectionEstablishClientMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.connection.ConnectionPingClientMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.connection.ConnectionPongClientMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.connection.ConnectionAcceptedServerMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.connection.ConnectionPongServerMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.connection.ConnectionRefusedServerMessage;
import org.anddev.andengine.extension.multiplayer.protocol.server.connector.ClientConnector;
import org.anddev.andengine.extension.multiplayer.protocol.shared.Connection;
import org.anddev.andengine.extension.multiplayer.protocol.shared.Connector;
import org.anddev.andengine.extension.multiplayer.protocol.shared.IMessageHandler;
import org.anddev.andengine.extension.multiplayer.protocol.shared.IMessageReader;
import org.anddev.andengine.extension.multiplayer.protocol.shared.MessageReader;
import org.anddev.andengine.extension.multiplayer.protocol.util.constants.ClientMessageFlags;

public interface IClientMessageReader<C extends Connection> extends IMessageReader<C, ClientConnector<C>, IClientMessage> {
    void handleMessage(ClientConnector clientConnector, IClientMessage iClientMessage) throws IOException;

    IClientMessage readMessage(DataInputStream dataInputStream) throws IOException;

    void recycleMessage(IClientMessage iClientMessage);

    void registerMessage(short s, Class<? extends IClientMessage> cls);

    void registerMessage(short s, Class<? extends IClientMessage> cls, IMessageHandler<C, ClientConnector<C>, IClientMessage> iMessageHandler);

    void registerMessageHandler(short s, IMessageHandler<C, ClientConnector<C>, IClientMessage> iMessageHandler);

    public static class ClientMessageReader<C extends Connection> extends MessageReader<C, ClientConnector<C>, IClientMessage> implements ClientMessageFlags, IClientMessageReader<C> {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.anddev.andengine.extension.multiplayer.protocol.shared.MessageReader.handleMessage(org.anddev.andengine.extension.multiplayer.protocol.shared.Connector, org.anddev.andengine.extension.multiplayer.protocol.adt.message.IMessage):void
         arg types: [org.anddev.andengine.extension.multiplayer.protocol.server.connector.ClientConnector, org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.IClientMessage]
         candidates:
          org.anddev.andengine.extension.multiplayer.protocol.server.IClientMessageReader.ClientMessageReader.handleMessage(org.anddev.andengine.extension.multiplayer.protocol.server.connector.ClientConnector, org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.IClientMessage):void
          org.anddev.andengine.extension.multiplayer.protocol.server.IClientMessageReader.handleMessage(org.anddev.andengine.extension.multiplayer.protocol.server.connector.ClientConnector, org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.IClientMessage):void
          org.anddev.andengine.extension.multiplayer.protocol.shared.MessageReader.handleMessage(org.anddev.andengine.extension.multiplayer.protocol.shared.Connector, org.anddev.andengine.extension.multiplayer.protocol.adt.message.IMessage):void */
        public /* bridge */ /* synthetic */ void handleMessage(ClientConnector clientConnector, IClientMessage iClientMessage) throws IOException {
            handleMessage((Connector) clientConnector, (IMessage) iClientMessage);
        }

        public /* bridge */ /* synthetic */ IClientMessage readMessage(DataInputStream dataInputStream) throws IOException {
            return (IClientMessage) readMessage(dataInputStream);
        }

        public /* bridge */ /* synthetic */ void recycleMessage(IClientMessage iClientMessage) {
            recycleMessage((IMessage) iClientMessage);
        }

        public static class DefaultClientMessageReader<C extends Connection> extends ClientMessageReader<C> implements IClientMessageHandler<C> {
            public DefaultClientMessageReader() {
                registerMessage(Short.MIN_VALUE, ConnectionEstablishClientMessage.class);
                registerMessage(-32767, ConnectionCloseClientMessage.class);
                registerMessage(-32766, ConnectionPingClientMessage.class);
                registerMessage(-32765, ConnectionPongClientMessage.class);
                registerMessageHandler(Short.MIN_VALUE, this);
                registerMessageHandler(-32767, this);
                registerMessageHandler(-32766, this);
                registerMessageHandler(-32765, this);
            }

            public void onHandleMessage(ClientConnector<C> pClientConnector, IClientMessage pClientMessage) throws IOException {
                switch (pClientMessage.getFlag()) {
                    case Short.MIN_VALUE:
                        onHandleConnectionEstablishClientMessage(pClientConnector, (ConnectionEstablishClientMessage) pClientMessage);
                        return;
                    case -32767:
                        onHandleConnectionCloseClientMessage(pClientConnector, (ConnectionCloseClientMessage) pClientMessage);
                        return;
                    case -32766:
                        onHandleConnectionPingClientMessage(pClientConnector, (ConnectionPingClientMessage) pClientMessage);
                        return;
                    case -32765:
                        onHandleConnectionPongClientMessage(pClientConnector, (ConnectionPongClientMessage) pClientMessage);
                        return;
                    default:
                        return;
                }
            }

            /* access modifiers changed from: protected */
            public void onHandleConnectionEstablishClientMessage(ClientConnector<C> pClientConnector, ConnectionEstablishClientMessage pClientMessage) throws IOException {
                if (pClientMessage.getProtocolVersion() == 1) {
                    pClientConnector.sendServerMessage(new ConnectionAcceptedServerMessage());
                } else {
                    pClientConnector.sendServerMessage(new ConnectionRefusedServerMessage());
                }
            }

            /* access modifiers changed from: protected */
            public void onHandleConnectionPingClientMessage(ClientConnector<C> pClientConnector, ConnectionPingClientMessage pClientMessage) throws IOException {
                pClientConnector.sendServerMessage(new ConnectionPongServerMessage(pClientMessage));
            }

            /* access modifiers changed from: protected */
            public void onHandleConnectionPongClientMessage(ClientConnector<C> clientConnector, ConnectionPongClientMessage pClientMessage) {
            }

            /* access modifiers changed from: protected */
            public void onHandleConnectionCloseClientMessage(ClientConnector<C> pClientConnector, ConnectionCloseClientMessage pClientMessage) throws IOException {
                pClientConnector.getConnection().close();
            }
        }
    }
}
