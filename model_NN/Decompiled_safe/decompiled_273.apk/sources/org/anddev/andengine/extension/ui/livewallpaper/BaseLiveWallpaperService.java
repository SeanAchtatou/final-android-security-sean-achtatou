package org.anddev.andengine.extension.ui.livewallpaper;

import android.os.Bundle;
import android.os.SystemClock;
import android.service.wallpaper.WallpaperService;
import android.view.MotionEvent;
import net.rbgrn.opengl.GLWallpaperService;
import org.anddev.andengine.audio.music.MusicManager;
import org.anddev.andengine.audio.sound.SoundManager;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.opengl.view.GLSurfaceView;
import org.anddev.andengine.opengl.view.RenderSurfaceView;
import org.anddev.andengine.sensor.accelerometer.AccelerometerSensorOptions;
import org.anddev.andengine.sensor.accelerometer.IAccelerometerListener;
import org.anddev.andengine.sensor.location.ILocationListener;
import org.anddev.andengine.sensor.location.LocationSensorOptions;
import org.anddev.andengine.sensor.orientation.IOrientationListener;
import org.anddev.andengine.sensor.orientation.OrientationSensorOptions;
import org.anddev.andengine.ui.IGameInterface;

public abstract class BaseLiveWallpaperService extends GLWallpaperService implements IGameInterface {
    protected Engine mEngine;

    public void onCreate() {
        super.onCreate();
        this.mEngine = onLoadEngine();
        applyEngineOptions(this.mEngine.getEngineOptions());
        onLoadResources();
        this.mEngine.onLoadComplete(onLoadScene());
        onLoadComplete();
        this.mEngine.start();
    }

    public Engine getEngine() {
        return this.mEngine;
    }

    public SoundManager getSoundManager() {
        return this.mEngine.getSoundManager();
    }

    public MusicManager getMusicManager() {
        return this.mEngine.getMusicManager();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.mEngine.stop();
        onGamePaused();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.mEngine.start();
        onGameResumed();
    }

    public void onDestroy() {
        super.onDestroy();
        this.mEngine.interruptUpdateThread();
        onUnloadResources();
    }

    public WallpaperService.Engine onCreateEngine() {
        return new BaseWallpaperGLEngine();
    }

    public void onGamePaused() {
    }

    public void onGameResumed() {
    }

    public void onUnloadResources() {
        if (this.mEngine.getEngineOptions().needsMusic()) {
            getMusicManager().releaseAll();
        }
        if (this.mEngine.getEngineOptions().needsSound()) {
            getSoundManager().releaseAll();
        }
    }

    public void runOnUpdateThread(Runnable pRunnable) {
        this.mEngine.runOnUpdateThread(pRunnable);
    }

    /* access modifiers changed from: protected */
    public void onTap(int pX, int pY) {
        this.mEngine.onTouch(null, MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), 0, (float) pX, (float) pY, 0));
    }

    /* access modifiers changed from: protected */
    public void onDrop(int pX, int pY) {
    }

    /* access modifiers changed from: protected */
    public void applyEngineOptions(EngineOptions pEngineOptions) {
    }

    /* access modifiers changed from: protected */
    public boolean enableVibrator() {
        return this.mEngine.enableVibrator(this);
    }

    /* access modifiers changed from: protected */
    public boolean enableAccelerometerSensor(IAccelerometerListener pAccelerometerListener) {
        return this.mEngine.enableAccelerometerSensor(this, pAccelerometerListener);
    }

    /* access modifiers changed from: protected */
    public boolean enableAccelerometerSensor(IAccelerometerListener pAccelerometerListener, AccelerometerSensorOptions pAccelerometerSensorOptions) {
        return this.mEngine.enableAccelerometerSensor(this, pAccelerometerListener, pAccelerometerSensorOptions);
    }

    /* access modifiers changed from: protected */
    public boolean enableOrientationSensor(IOrientationListener pOrientationListener) {
        return this.mEngine.enableOrientationSensor(this, pOrientationListener);
    }

    /* access modifiers changed from: protected */
    public boolean enableOrientationSensor(IOrientationListener pOrientationListener, OrientationSensorOptions pOrientationSensorOptions) {
        return this.mEngine.enableOrientationSensor(this, pOrientationListener, pOrientationSensorOptions);
    }

    /* access modifiers changed from: protected */
    public boolean disableOrientationSensor() {
        return this.mEngine.disableOrientationSensor(this);
    }

    /* access modifiers changed from: protected */
    public void enableLocationSensor(ILocationListener pLocationListener, LocationSensorOptions pLocationSensorOptions) {
        this.mEngine.enableLocationSensor(this, pLocationListener, pLocationSensorOptions);
    }

    /* access modifiers changed from: protected */
    public void disableLocationSensor() {
        this.mEngine.disableLocationSensor(this);
    }

    protected class BaseWallpaperGLEngine extends GLWallpaperService.GLEngine {
        private GLSurfaceView.Renderer mRenderer;

        public BaseWallpaperGLEngine() {
            super();
            setEGLConfigChooser(false);
            this.mRenderer = new RenderSurfaceView.Renderer(BaseLiveWallpaperService.this.mEngine);
            setRenderer(this.mRenderer);
            setRenderMode(1);
        }

        public Bundle onCommand(String pAction, int pX, int pY, int pZ, Bundle pExtras, boolean pResultRequested) {
            if (pAction.equals("android.wallpaper.tap")) {
                BaseLiveWallpaperService.this.onTap(pX, pY);
            } else if (pAction.equals("android.home.drop")) {
                BaseLiveWallpaperService.this.onDrop(pX, pY);
            }
            return super.onCommand(pAction, pX, pY, pZ, pExtras, pResultRequested);
        }

        public void onResume() {
            super.onResume();
            BaseLiveWallpaperService.this.getEngine().onResume();
            BaseLiveWallpaperService.this.onResume();
        }

        public void onPause() {
            super.onPause();
            BaseLiveWallpaperService.this.getEngine().onPause();
            BaseLiveWallpaperService.this.onPause();
        }

        public void onDestroy() {
            super.onDestroy();
            this.mRenderer = null;
        }
    }
}
