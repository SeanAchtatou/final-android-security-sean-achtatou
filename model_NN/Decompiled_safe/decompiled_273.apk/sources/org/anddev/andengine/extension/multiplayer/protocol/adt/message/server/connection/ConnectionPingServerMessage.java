package org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.connection;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.ServerMessage;
import org.anddev.andengine.extension.multiplayer.protocol.util.constants.ServerMessageFlags;

public class ConnectionPingServerMessage extends ServerMessage {
    private long mTimestamp;

    @Deprecated
    public ConnectionPingServerMessage() {
    }

    public ConnectionPingServerMessage(long pTimestamp) {
        this.mTimestamp = pTimestamp;
    }

    public long getTimestamp() {
        return this.mTimestamp;
    }

    public short getFlag() {
        return ServerMessageFlags.FLAG_MESSAGE_SERVER_CONNECTION_PONG;
    }

    public void onReadTransmissionData(DataInputStream pDataInputStream) throws IOException {
        this.mTimestamp = pDataInputStream.readLong();
    }

    public void onWriteTransmissionData(DataOutputStream pDataOutputStream) throws IOException {
        pDataOutputStream.writeLong(this.mTimestamp);
    }

    /* access modifiers changed from: protected */
    public void onAppendTransmissionDataForToString(StringBuilder pStringBuilder) {
        pStringBuilder.append(", getTimestamp()=").append(this.mTimestamp);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ConnectionPingServerMessage other = (ConnectionPingServerMessage) obj;
        return getFlag() == other.getFlag() && getTimestamp() == other.getTimestamp();
    }
}
