package org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.connection;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.ClientMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.connection.ConnectionPingServerMessage;

public class ConnectionPongClientMessage extends ClientMessage {
    private long mOriginalPingTimestamp;

    @Deprecated
    public ConnectionPongClientMessage() {
    }

    public ConnectionPongClientMessage(ConnectionPingServerMessage pServerMessage) {
        this(pServerMessage.getTimestamp());
    }

    public ConnectionPongClientMessage(long pTimestamp) {
        this.mOriginalPingTimestamp = pTimestamp;
    }

    public long getOriginalPingTimestamp() {
        return this.mOriginalPingTimestamp;
    }

    public short getFlag() {
        return -32765;
    }

    public void onReadTransmissionData(DataInputStream pDataInputStream) throws IOException {
        this.mOriginalPingTimestamp = pDataInputStream.readLong();
    }

    public void onWriteTransmissionData(DataOutputStream pDataOutputStream) throws IOException {
        pDataOutputStream.writeLong(this.mOriginalPingTimestamp);
    }

    /* access modifiers changed from: protected */
    public void onAppendTransmissionDataForToString(StringBuilder pStringBuilder) {
        pStringBuilder.append(", getOriginalPingTimestamp()=").append(this.mOriginalPingTimestamp);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ConnectionPongClientMessage other = (ConnectionPongClientMessage) obj;
        return getFlag() == other.getFlag() && getOriginalPingTimestamp() == other.getOriginalPingTimestamp();
    }
}
