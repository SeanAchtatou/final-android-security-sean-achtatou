package org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.connection;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.ClientMessage;

public class ConnectionPingClientMessage extends ClientMessage {
    private long mTimestamp;

    @Deprecated
    public ConnectionPingClientMessage() {
    }

    public ConnectionPingClientMessage(long pTimestamp) {
        this.mTimestamp = pTimestamp;
    }

    public long getTimestamp() {
        return this.mTimestamp;
    }

    public short getFlag() {
        return -32766;
    }

    public void onReadTransmissionData(DataInputStream pDataInputStream) throws IOException {
        this.mTimestamp = pDataInputStream.readLong();
    }

    public void onWriteTransmissionData(DataOutputStream pDataOutputStream) throws IOException {
        pDataOutputStream.writeLong(this.mTimestamp);
    }

    /* access modifiers changed from: protected */
    public void onAppendTransmissionDataForToString(StringBuilder pStringBuilder) {
        pStringBuilder.append(", getTimestamp()=").append(this.mTimestamp);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ConnectionPingClientMessage other = (ConnectionPingClientMessage) obj;
        return getFlag() == other.getFlag() && getTimestamp() == other.getTimestamp();
    }
}
