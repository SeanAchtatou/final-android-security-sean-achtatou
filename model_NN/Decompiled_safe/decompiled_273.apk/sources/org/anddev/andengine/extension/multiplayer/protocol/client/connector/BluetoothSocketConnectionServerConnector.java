package org.anddev.andengine.extension.multiplayer.protocol.client.connector;

import java.io.IOException;
import org.anddev.andengine.extension.multiplayer.protocol.client.IServerMessageReader;
import org.anddev.andengine.extension.multiplayer.protocol.client.connector.ServerConnector;
import org.anddev.andengine.extension.multiplayer.protocol.exception.BluetoothException;
import org.anddev.andengine.extension.multiplayer.protocol.shared.BluetoothSocketConnection;
import org.anddev.andengine.extension.multiplayer.protocol.shared.Connector;
import org.anddev.andengine.extension.multiplayer.protocol.util.Bluetooth;
import org.anddev.andengine.util.Debug;

public class BluetoothSocketConnectionServerConnector extends ServerConnector<BluetoothSocketConnection> {

    public interface IBluetoothSocketConnectionServerConnectorListener extends ServerConnector.IServerConnectorListener<BluetoothSocketConnection> {
    }

    public BluetoothSocketConnectionServerConnector(BluetoothSocketConnection pBluetoothSocketConnection, IBluetoothSocketConnectionServerConnectorListener pBlutetoothSocketConnectionServerConnectorListener) throws IOException, BluetoothException {
        super(pBluetoothSocketConnection, pBlutetoothSocketConnectionServerConnectorListener);
        if (!Bluetooth.isSupportedByAndroidVersion()) {
            throw new BluetoothException();
        }
    }

    public BluetoothSocketConnectionServerConnector(BluetoothSocketConnection pBluetoothSocketConnection, IServerMessageReader<BluetoothSocketConnection> pServerMessageReader, IBluetoothSocketConnectionServerConnectorListener pBlutetoothSocketConnectionServerConnectorListener) throws IOException, BluetoothException {
        super(pBluetoothSocketConnection, pServerMessageReader, pBlutetoothSocketConnectionServerConnectorListener);
        if (!Bluetooth.isSupportedByAndroidVersion()) {
            throw new BluetoothException();
        }
    }

    public static class DefaultBluetoothConnectionSocketServerConnectorListener implements IBluetoothSocketConnectionServerConnectorListener {
        public /* bridge */ /* synthetic */ void onConnected(Connector connector) {
            onConnected((ServerConnector<BluetoothSocketConnection>) ((ServerConnector) connector));
        }

        public /* bridge */ /* synthetic */ void onDisconnected(Connector connector) {
            onDisconnected((ServerConnector<BluetoothSocketConnection>) ((ServerConnector) connector));
        }

        public void onConnected(ServerConnector<BluetoothSocketConnection> pServerConnector) {
            Debug.d("Accepted Server-Connection from: '" + pServerConnector.getConnection().getBluetoothSocket().getRemoteDevice().getAddress());
        }

        public void onDisconnected(ServerConnector<BluetoothSocketConnection> pServerConnector) {
            Debug.d("Closed Server-Connection from: '" + pServerConnector.getConnection().getBluetoothSocket().getRemoteDevice().getAddress());
        }
    }
}
