package org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.connection;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.connection.ConnectionPingClientMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.ServerMessage;
import org.anddev.andengine.extension.multiplayer.protocol.util.constants.ServerMessageFlags;

public class ConnectionPongServerMessage extends ServerMessage {
    private long mOriginalPingTimestamp;

    @Deprecated
    public ConnectionPongServerMessage() {
    }

    public ConnectionPongServerMessage(ConnectionPingClientMessage pClientMessage) {
        this(pClientMessage.getTimestamp());
    }

    public ConnectionPongServerMessage(long pTimestamp) {
        this.mOriginalPingTimestamp = pTimestamp;
    }

    public long getOriginalPingTimestamp() {
        return this.mOriginalPingTimestamp;
    }

    public short getFlag() {
        return ServerMessageFlags.FLAG_MESSAGE_SERVER_CONNECTION_PONG;
    }

    public void onReadTransmissionData(DataInputStream pDataInputStream) throws IOException {
        this.mOriginalPingTimestamp = pDataInputStream.readLong();
    }

    public void onWriteTransmissionData(DataOutputStream pDataOutputStream) throws IOException {
        pDataOutputStream.writeLong(this.mOriginalPingTimestamp);
    }

    /* access modifiers changed from: protected */
    public void onAppendTransmissionDataForToString(StringBuilder pStringBuilder) {
        pStringBuilder.append(", getOriginalPingTimestamp()=").append(this.mOriginalPingTimestamp);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ConnectionPongServerMessage other = (ConnectionPongServerMessage) obj;
        return getFlag() == other.getFlag() && getOriginalPingTimestamp() == other.getOriginalPingTimestamp();
    }
}
