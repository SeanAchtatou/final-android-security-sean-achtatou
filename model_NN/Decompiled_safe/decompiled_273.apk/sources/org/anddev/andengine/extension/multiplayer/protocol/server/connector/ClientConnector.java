package org.anddev.andengine.extension.multiplayer.protocol.server.connector;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.IClientMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.IServerMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.connection.ConnectionCloseServerMessage;
import org.anddev.andengine.extension.multiplayer.protocol.server.IClientMessageHandler;
import org.anddev.andengine.extension.multiplayer.protocol.server.IClientMessageReader;
import org.anddev.andengine.extension.multiplayer.protocol.shared.Connection;
import org.anddev.andengine.extension.multiplayer.protocol.shared.Connector;
import org.anddev.andengine.util.Debug;

public class ClientConnector<C extends Connection> extends Connector<C> {
    private final IClientMessageReader<C> mClientMessageReader;

    public interface IClientConnectorListener<T extends Connection> extends Connector.IConnectorListener<ClientConnector<T>> {
        void onConnected(ClientConnector<T> clientConnector);

        void onDisconnected(ClientConnector<T> clientConnector);
    }

    public ClientConnector(C pConnection) throws IOException {
        this(pConnection, new IClientMessageReader.ClientMessageReader.DefaultClientMessageReader());
    }

    public ClientConnector(C pConnection, IClientMessageReader<C> pClientMessageReader) throws IOException {
        super(pConnection);
        this.mClientMessageReader = pClientMessageReader;
    }

    public IClientMessageReader<C> getClientMessageReader() {
        return this.mClientMessageReader;
    }

    public IClientConnectorListener<C> getConnectorListener() {
        return (IClientConnectorListener) super.getConnectorListener();
    }

    public void setClientConnectorListener(IClientConnectorListener<C> pClientConnectorListener) {
        super.setConnectorListener(pClientConnectorListener);
    }

    public void onConnected(Connection pConnection) {
        getConnectorListener().onConnected(this);
    }

    public void onDisconnected(Connection pConnection) {
        getConnectorListener().onDisconnected(this);
        try {
            sendServerMessage(new ConnectionCloseServerMessage());
        } catch (Throwable th) {
            Debug.e(th);
        }
    }

    public void read(DataInputStream pDataInputStream) throws IOException {
        IClientMessage clientMessage = this.mClientMessageReader.readMessage(pDataInputStream);
        this.mClientMessageReader.handleMessage(this, clientMessage);
        this.mClientMessageReader.recycleMessage(clientMessage);
    }

    public void registerClientMessage(short pFlag, Class<? extends IClientMessage> pClientMessageClass) {
        this.mClientMessageReader.registerMessage(pFlag, pClientMessageClass);
    }

    public void registerClientMessage(short pFlag, Class<? extends IClientMessage> pClientMessageClass, IClientMessageHandler<C> pClientMessageHandler) {
        this.mClientMessageReader.registerMessage(pFlag, pClientMessageClass, pClientMessageHandler);
    }

    public void registerClientMessageHandler(short pFlag, IClientMessageHandler<C> pClientMessageHandler) {
        this.mClientMessageReader.registerMessageHandler(pFlag, pClientMessageHandler);
    }

    public void sendServerMessage(IServerMessage pServerMessage) throws IOException {
        DataOutputStream dataOutputStream = this.mConnection.getDataOutputStream();
        pServerMessage.transmit(dataOutputStream);
        dataOutputStream.flush();
    }
}
