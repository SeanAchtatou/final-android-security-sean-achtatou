package org.anddev.andengine.extension.multiplayer.protocol.shared;

import java.io.DataInputStream;
import java.io.IOException;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.IMessage;
import org.anddev.andengine.extension.multiplayer.protocol.shared.Connection;
import org.anddev.andengine.extension.multiplayer.protocol.shared.Connector;

public interface IMessageReader<C extends Connection, CC extends Connector<C>, M extends IMessage> {
    void handleMessage(Connector connector, IMessage iMessage) throws IOException;

    M readMessage(DataInputStream dataInputStream) throws IOException;

    void recycleMessage(IMessage iMessage);

    void registerMessage(short s, Class<? extends M> cls);

    void registerMessage(short s, Class<? extends M> cls, IMessageHandler<C, CC, M> iMessageHandler);

    void registerMessageHandler(short s, IMessageHandler<C, CC, M> iMessageHandler);
}
