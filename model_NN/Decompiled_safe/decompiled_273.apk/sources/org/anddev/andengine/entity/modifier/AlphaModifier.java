package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class AlphaModifier extends SingleValueSpanShapeModifier {
    public AlphaModifier(float pDuration, float pFromAlpha, float pToAlpha) {
        this(pDuration, pFromAlpha, pToAlpha, null, IEaseFunction.DEFAULT);
    }

    public AlphaModifier(float pDuration, float pFromAlpha, float pToAlpha, IEaseFunction pEaseFunction) {
        this(pDuration, pFromAlpha, pToAlpha, null, pEaseFunction);
    }

    public AlphaModifier(float pDuration, float pFromAlpha, float pToAlpha, IEntityModifier.IEntityModifierListener pEntityModifierListener) {
        super(pDuration, pFromAlpha, pToAlpha, pEntityModifierListener, IEaseFunction.DEFAULT);
    }

    public AlphaModifier(float pDuration, float pFromAlpha, float pToAlpha, IEntityModifier.IEntityModifierListener pEntityModifierListener, IEaseFunction pEaseFunction) {
        super(pDuration, pFromAlpha, pToAlpha, pEntityModifierListener, pEaseFunction);
    }

    protected AlphaModifier(AlphaModifier pAlphaModifier) {
        super(pAlphaModifier);
    }

    public AlphaModifier clone() {
        return new AlphaModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(IEntity pEntity, float pAlpha) {
        pEntity.setAlpha(pAlpha);
    }

    /* access modifiers changed from: protected */
    public void onSetValue(IEntity pEntity, float pPercentageDone, float pAlpha) {
        pEntity.setAlpha(pAlpha);
    }
}
