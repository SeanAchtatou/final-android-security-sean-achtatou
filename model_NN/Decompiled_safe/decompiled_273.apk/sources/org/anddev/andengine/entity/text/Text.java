package org.anddev.andengine.entity.text;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.shape.RectangularShape;
import org.anddev.andengine.opengl.buffer.BufferObjectManager;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.buffer.TextTextureBuffer;
import org.anddev.andengine.opengl.util.GLHelper;
import org.anddev.andengine.opengl.vertex.TextVertexBuffer;
import org.anddev.andengine.util.HorizontalAlign;
import org.anddev.andengine.util.StringUtils;

public class Text extends RectangularShape {
    protected final int mCharactersMaximum;
    private final Font mFont;
    private String[] mLines;
    private int mMaximumLineWidth;
    private String mText;
    private final TextTextureBuffer mTextTextureBuffer;
    protected final int mVertexCount;
    private int[] mWidths;

    public Text(float pX, float pY, Font pFont, String pText) {
        this(pX, pY, pFont, pText, HorizontalAlign.LEFT);
    }

    public Text(float pX, float pY, Font pFont, String pText, HorizontalAlign pHorizontalAlign) {
        this(pX, pY, pFont, pText, pHorizontalAlign, pText.length() - StringUtils.countOccurrences(pText, 10));
    }

    protected Text(float pX, float pY, Font pFont, String pText, HorizontalAlign pHorizontalAlign, int pCharactersMaximum) {
        super(pX, pY, 0.0f, 0.0f, new TextVertexBuffer(pCharactersMaximum, pHorizontalAlign, 35044));
        this.mCharactersMaximum = pCharactersMaximum;
        this.mVertexCount = this.mCharactersMaximum * 6;
        this.mTextTextureBuffer = new TextTextureBuffer(this.mVertexCount * 2, 35044);
        BufferObjectManager.getActiveInstance().loadBufferObject(this.mTextTextureBuffer);
        this.mFont = pFont;
        updateText(pText);
        initBlendFunction();
    }

    /* access modifiers changed from: protected */
    public void updateText(String pText) {
        this.mText = pText;
        Font font = this.mFont;
        this.mLines = StringUtils.split(this.mText, 10, this.mLines);
        String[] lines = this.mLines;
        int lineCount = lines.length;
        if (!(this.mWidths != null && this.mWidths.length == lineCount)) {
            this.mWidths = new int[lineCount];
        }
        int[] widths = this.mWidths;
        int maximumLineWidth = 0;
        for (int i = lineCount - 1; i >= 0; i--) {
            widths[i] = font.getStringWidth(lines[i]);
            maximumLineWidth = Math.max(maximumLineWidth, widths[i]);
        }
        this.mMaximumLineWidth = maximumLineWidth;
        this.mWidth = (float) this.mMaximumLineWidth;
        float width = this.mWidth;
        this.mBaseWidth = width;
        this.mHeight = (float) ((font.getLineHeight() * lineCount) + ((lineCount - 1) * font.getLineGap()));
        float height = this.mHeight;
        this.mBaseHeight = height;
        this.mRotationCenterX = width * 0.5f;
        this.mRotationCenterY = height * 0.5f;
        this.mScaleCenterX = this.mRotationCenterX;
        this.mScaleCenterY = this.mRotationCenterY;
        this.mTextTextureBuffer.update(font, lines);
        updateVertexBuffer();
    }

    public int getCharacterCount() {
        return this.mCharactersMaximum;
    }

    public TextVertexBuffer getVertexBuffer() {
        return (TextVertexBuffer) super.getVertexBuffer();
    }

    /* access modifiers changed from: protected */
    public void onInitDraw(GL10 pGL) {
        super.onInitDraw(pGL);
        GLHelper.enableTextures(pGL);
        GLHelper.enableTexCoordArray(pGL);
    }

    /* access modifiers changed from: protected */
    public void drawVertices(GL10 pGL, Camera pCamera) {
        pGL.glDrawArrays(4, 0, this.mVertexCount);
    }

    /* access modifiers changed from: protected */
    public void onUpdateVertexBuffer() {
        Font font = this.mFont;
        if (font != null) {
            getVertexBuffer().update(font, this.mMaximumLineWidth, this.mWidths, this.mLines);
        }
    }

    /* access modifiers changed from: protected */
    public void onApplyTransformations(GL10 pGL) {
        super.onApplyTransformations(pGL);
        applyTexture(pGL);
    }

    private void initBlendFunction() {
        if (this.mFont.getTexture().getTextureOptions().mPreMultipyAlpha) {
            setBlendFunction(1, 771);
        }
    }

    private void applyTexture(GL10 pGL) {
        if (GLHelper.EXTENSIONS_VERTEXBUFFEROBJECTS) {
            GL11 gl11 = (GL11) pGL;
            this.mTextTextureBuffer.selectOnHardware(gl11);
            GLHelper.bindTexture(pGL, this.mFont.getTexture().getHardwareTextureID());
            GLHelper.texCoordZeroPointer(gl11);
            return;
        }
        GLHelper.bindTexture(pGL, this.mFont.getTexture().getHardwareTextureID());
        GLHelper.texCoordPointer(pGL, this.mTextTextureBuffer.getFloatBuffer());
    }
}
