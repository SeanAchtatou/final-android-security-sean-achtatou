package org.anddev.andengine.util.pool;

import org.anddev.andengine.util.pool.PoolItem;

public abstract class Pool<T extends PoolItem> extends GenericPool<T> {
    public synchronized /* bridge */ /* synthetic */ void recyclePoolItem(Object obj) {
        recyclePoolItem((PoolItem) obj);
    }

    public Pool() {
    }

    public Pool(int pInitialSize) {
        super(pInitialSize);
    }

    public Pool(int pInitialSize, int pGrowth) {
        super(pInitialSize, pGrowth);
    }

    /* access modifiers changed from: protected */
    public T onHandleAllocatePoolItem() {
        T poolItem = (PoolItem) super.onHandleAllocatePoolItem();
        poolItem.mParent = this;
        return poolItem;
    }

    /* access modifiers changed from: protected */
    public void onHandleObtainItem(T pPoolItem) {
        pPoolItem.mRecycled = false;
        pPoolItem.onObtain();
    }

    /* access modifiers changed from: protected */
    public void onHandleRecycleItem(T pPoolItem) {
        pPoolItem.onRecycle();
        pPoolItem.mRecycled = true;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public synchronized void recyclePoolItem(T r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            org.anddev.andengine.util.pool.Pool<? extends org.anddev.andengine.util.pool.PoolItem> r0 = r3.mParent     // Catch:{ all -> 0x000d }
            if (r0 != 0) goto L_0x0010
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x000d }
            java.lang.String r1 = "PoolItem not assigned to a pool!"
            r0.<init>(r1)     // Catch:{ all -> 0x000d }
            throw r0     // Catch:{ all -> 0x000d }
        L_0x000d:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x0010:
            boolean r0 = r3.isFromPool(r2)     // Catch:{ all -> 0x000d }
            if (r0 != 0) goto L_0x001e
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x000d }
            java.lang.String r1 = "PoolItem from another pool!"
            r0.<init>(r1)     // Catch:{ all -> 0x000d }
            throw r0     // Catch:{ all -> 0x000d }
        L_0x001e:
            boolean r0 = r3.isRecycled()     // Catch:{ all -> 0x000d }
            if (r0 == 0) goto L_0x002c
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x000d }
            java.lang.String r1 = "PoolItem already recycled!"
            r0.<init>(r1)     // Catch:{ all -> 0x000d }
            throw r0     // Catch:{ all -> 0x000d }
        L_0x002c:
            super.recyclePoolItem(r3)     // Catch:{ all -> 0x000d }
            monitor-exit(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.util.pool.Pool.recyclePoolItem(org.anddev.andengine.util.pool.PoolItem):void");
    }

    public synchronized boolean ownsPoolItem(T pPoolItem) {
        return pPoolItem.mParent == this;
    }

    /* access modifiers changed from: package-private */
    public void recycle(PoolItem pPoolItem) {
        recyclePoolItem(pPoolItem);
    }
}
