package org.anddev.andengine.util.modifier.ease;

public class EaseQuadIn implements IEaseFunction {
    private static EaseQuadIn INSTANCE;

    private EaseQuadIn() {
    }

    public static EaseQuadIn getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseQuadIn();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        float pSecondsElapsed2 = pSecondsElapsed / pDuration;
        return (pMaxValue * pSecondsElapsed2 * pSecondsElapsed2) + pMinValue;
    }
}
