package org.anddev.andengine.util.constants;

public interface MIMETypes {
    public static final String GIF = "image/gif";
    public static final String JPEG = "image/jpeg";
    public static final String PNG = "image/png";
}
