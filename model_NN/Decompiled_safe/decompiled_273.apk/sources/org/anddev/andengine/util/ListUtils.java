package org.anddev.andengine.util;

import java.util.ArrayList;

public class ListUtils {
    public static <T> ArrayList<? extends T> toList(T pElement) {
        ArrayList<T> out = new ArrayList<>();
        out.add(pElement);
        return out;
    }
}
