package org.anddev.andengine.util.modifier.ease;

public class EaseQuintOut implements IEaseFunction {
    private static EaseQuintOut INSTANCE;

    private EaseQuintOut() {
    }

    public static EaseQuintOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseQuintOut();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        float pSecondsElapsed2 = (pSecondsElapsed / pDuration) - 1.0f;
        return (((pSecondsElapsed2 * pSecondsElapsed2 * pSecondsElapsed2 * pSecondsElapsed2 * pSecondsElapsed2) + 1.0f) * pMaxValue) + pMinValue;
    }
}
