package org.anddev.andengine.util.modifier.ease;

public class EaseBackIn implements IEaseFunction {
    private static EaseBackIn INSTANCE;

    private EaseBackIn() {
    }

    public static EaseBackIn getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseBackIn();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        float pSecondsElapsed2 = pSecondsElapsed / pDuration;
        return (pMaxValue * pSecondsElapsed2 * pSecondsElapsed2 * ((2.70158f * pSecondsElapsed2) - 1.70158f)) + pMinValue;
    }
}
