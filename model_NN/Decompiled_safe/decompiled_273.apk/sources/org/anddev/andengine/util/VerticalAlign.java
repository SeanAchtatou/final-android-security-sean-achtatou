package org.anddev.andengine.util;

public enum VerticalAlign {
    TOP,
    CENTER,
    BOTTOM
}
