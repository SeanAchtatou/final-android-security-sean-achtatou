package org.anddev.andengine.util.modifier;

import org.anddev.andengine.util.modifier.IModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public abstract class BaseSingleValueSpanModifier<T> extends BaseDurationModifier<T> {
    protected final IEaseFunction mEaseFunction;
    private final float mFromValue;
    private final float mValueSpan;

    /* access modifiers changed from: protected */
    public abstract void onSetInitialValue(Object obj, float f);

    /* access modifiers changed from: protected */
    public abstract void onSetValue(Object obj, float f, float f2);

    public BaseSingleValueSpanModifier(float pDuration, float pFromValue, float pToValue) {
        this(pDuration, pFromValue, pToValue, null, IEaseFunction.DEFAULT);
    }

    public BaseSingleValueSpanModifier(float pDuration, float pFromValue, float pToValue, IEaseFunction pEaseFunction) {
        this(pDuration, pFromValue, pToValue, null, pEaseFunction);
    }

    public BaseSingleValueSpanModifier(float pDuration, float pFromValue, float pToValue, IModifier.IModifierListener iModifierListener) {
        this(pDuration, pFromValue, pToValue, IEaseFunction.DEFAULT);
    }

    public BaseSingleValueSpanModifier(float pDuration, float pFromValue, float pToValue, IModifier.IModifierListener<T> pModifierListener, IEaseFunction pEaseFunction) {
        super(pDuration, pModifierListener);
        this.mFromValue = pFromValue;
        this.mValueSpan = pToValue - pFromValue;
        this.mEaseFunction = pEaseFunction;
    }

    protected BaseSingleValueSpanModifier(BaseSingleValueSpanModifier<T> pBaseSingleValueSpanModifier) {
        super(pBaseSingleValueSpanModifier);
        this.mFromValue = pBaseSingleValueSpanModifier.mFromValue;
        this.mValueSpan = pBaseSingleValueSpanModifier.mValueSpan;
        this.mEaseFunction = pBaseSingleValueSpanModifier.mEaseFunction;
    }

    /* access modifiers changed from: protected */
    public void onManagedInitialize(Object obj) {
        onSetInitialValue(obj, this.mFromValue);
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float pSecondsElapsed, T pItem) {
        float percentageDone = this.mEaseFunction.getPercentageDone(getTotalSecondsElapsed(), this.mDuration, 0.0f, 1.0f);
        onSetValue(pItem, percentageDone, this.mFromValue + (this.mValueSpan * percentageDone));
    }
}
