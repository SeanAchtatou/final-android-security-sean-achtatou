package com.google.gson.annotations;

public @interface Since {
    double value();
}
