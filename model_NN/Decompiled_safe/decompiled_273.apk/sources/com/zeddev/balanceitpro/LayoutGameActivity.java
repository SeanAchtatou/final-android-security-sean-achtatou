package com.zeddev.balanceitpro;

import com.google.ads.AdRequest;
import com.google.ads.AdView;
import org.anddev.andengine.opengl.view.RenderSurfaceView;
import org.anddev.andengine.ui.activity.BaseGameActivity;

public abstract class LayoutGameActivity extends BaseGameActivity {
    /* access modifiers changed from: protected */
    public abstract int getLayoutID();

    /* access modifiers changed from: protected */
    public abstract int getRenderSurfaceViewID();

    /* access modifiers changed from: protected */
    public void onSetContentView() {
        super.setContentView(getLayoutID());
        this.mRenderSurfaceView = (RenderSurfaceView) findViewById(getRenderSurfaceViewID());
        this.mRenderSurfaceView.setEGLConfigChooser(false);
        this.mRenderSurfaceView.setRenderer(this.mEngine);
        ((AdView) findViewById(R.id.adView)).loadAd(new AdRequest());
    }
}
