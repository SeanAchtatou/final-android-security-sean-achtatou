package com.zeddev.balanceitpro;

import android.graphics.Typeface;
import android.view.KeyEvent;
import android.widget.Toast;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import java.io.IOException;
import java.text.DecimalFormat;
import org.anddev.andengine.audio.sound.Sound;
import org.anddev.andengine.audio.sound.SoundFactory;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.SmoothCamera;
import org.anddev.andengine.engine.camera.hud.HUD;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.modifier.AlphaModifier;
import org.anddev.andengine.entity.modifier.ParallelEntityModifier;
import org.anddev.andengine.entity.modifier.ScaleModifier;
import org.anddev.andengine.entity.particle.ParticleSystem;
import org.anddev.andengine.entity.particle.emitter.CircleParticleEmitter;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.scene.menu.MenuScene;
import org.anddev.andengine.entity.scene.menu.animator.SlideMenuAnimator;
import org.anddev.andengine.entity.scene.menu.item.IMenuItem;
import org.anddev.andengine.entity.scene.menu.item.TextMenuItem;
import org.anddev.andengine.entity.scene.menu.item.decorator.ColorMenuItemDecorator;
import org.anddev.andengine.entity.shape.RectangularShape;
import org.anddev.andengine.entity.shape.Shape;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.entity.util.FPSCounter;
import org.anddev.andengine.entity.util.FPSLogger;
import org.anddev.andengine.extension.physics.box2d.PhysicsConnector;
import org.anddev.andengine.extension.physics.box2d.PhysicsFactory;
import org.anddev.andengine.extension.physics.box2d.PhysicsWorld;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.font.FontFactory;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureManager;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.sensor.accelerometer.AccelerometerData;
import org.anddev.andengine.sensor.accelerometer.IAccelerometerListener;
import org.anddev.andengine.util.Debug;

public class Balance extends LayoutGameActivity implements IAccelerometerListener, Scene.IOnSceneTouchListener, Scene.IOnAreaTouchListener, MenuScene.IOnMenuItemClickListener {
    private static final int CAMERA_HEIGHT = 480;
    private static final int CAMERA_WIDTH = 800;
    private static final int CENTX = 400;
    private static final int CENTY = 240;
    protected static final int LEV1 = 1;
    protected static final int LEV10 = 10;
    protected static final int LEV11 = 11;
    protected static final int LEV2 = 2;
    protected static final int LEV3 = 3;
    protected static final int LEV4 = 4;
    protected static final int LEV5 = 5;
    protected static final int LEV6 = 6;
    protected static final int LEV7 = 7;
    protected static final int LEV8 = 8;
    protected static final int LEV9 = 9;
    protected static final int MENU_QUIT = 1000;
    protected static final int MENU_RESET = 0;
    protected static final int MENU_SELECT = 1001;
    private Sprite back;
    private TextureRegion backregion;
    private Body ballbody;
    private Body ballbody2;
    private Sound bingsound;
    private Body body2;
    private PhysicsConnector[] boxconnectors;
    /* access modifiers changed from: private */
    public int boxcount = 0;
    private Body[] boxes;
    private Sprite[] boxpics;
    private TextureRegion boxregion1;
    private TextureRegion boxregion2;
    /* access modifiers changed from: private */
    public SmoothCamera camera;
    private RectangularShape centerRectangle;
    /* access modifiers changed from: private */
    public boolean died = false;
    private ChangeableText elapsedText;
    private PhysicsConnector facePhysicsConnector;
    private PhysicsConnector facePhysicsConnector2;
    private ChangeableText fpsText;
    private boolean gameover = false;
    /* access modifiers changed from: private */
    public Sprite girder;
    private Body girderbody;
    private PhysicsConnector girderonnector;
    private TextureRegion girderregion;
    /* access modifiers changed from: private */
    public float gravitymod = 0.3f;
    /* access modifiers changed from: private */
    public Sprite grower;
    /* access modifiers changed from: private */
    public boolean growing = false;
    private Highscore highscore;
    /* access modifiers changed from: private */
    public ChangeableText hightext;
    /* access modifiers changed from: private */
    public Rectangle hitline;
    private ChangeableText introtext;
    private Body latestbody;
    private Sprite latestshape = null;
    /* access modifiers changed from: private */
    public int levcounter = 0;
    /* access modifiers changed from: private */
    public int level = 0;
    private boolean levelstart = true;
    /* access modifiers changed from: private */
    public ChangeableText leveltext;
    private float levtime = 1000.0f;
    private TiledTextureRegion mBoxFaceTextureRegion;
    private TiledTextureRegion mBoxFaceTextureRegion2;
    private TiledTextureRegion mBoxFaceTextureRegion3;
    private TiledTextureRegion mCircleFaceTextureRegion;
    private TiledTextureRegion mCircleFaceTextureRegion2;
    private Sound mExplosionSound;
    private int mFaceCount = 0;
    private Font mFont;
    private Texture mFontTexture;
    private float mGravityX;
    private float mGravityY;
    private HUD mHud;
    /* access modifiers changed from: private */
    public MenuScene mMenuScene;
    private TextureRegion mParticleTextureRegion;
    private TextureRegion mParticleTextureRegion2;
    /* access modifiers changed from: private */
    public PhysicsWorld mPhysicsWorld;
    private Font mPlokFont;
    private Font mPlokFont2;
    private Texture mPlokFontTexture;
    private Texture mPlokFontTexture2;
    private MenuScene mSubMenuScene;
    private final Vector2 mTempVector = new Vector2();
    private Texture mTexture;
    private Texture mTexture10;
    private Texture mTexture11;
    private Texture mTexture12;
    private Texture mTexture13;
    private Texture mTexture2;
    private Texture mTexture3;
    private Texture mTexture4;
    private Texture mTexture5;
    private Texture mTexture6;
    private Texture mTexture7;
    private Texture mTexture8;
    private Texture mTexture9;
    private Texture mTexturex;
    private TiledTextureRegion mTriangleFaceTextureRegion;
    /* access modifiers changed from: private */
    public int menucount = 0;
    private int mode = 2;
    private float movex;
    private float movex2;
    private float movey;
    private float movey2;
    private boolean moving;
    private boolean moving2;
    private FixtureDef objectFixtureDef1;
    private FixtureDef objectFixtureDef2;
    private FixtureDef objectFixtureDef6;
    private Sound oversound;
    private CircleParticleEmitter particleEmitter;
    private CircleParticleEmitter particleEmitter2;
    private ParticleSystem particleSystem;
    private ParticleSystem particleSystem2;
    private Sprite pivot;
    private Body pivotbody;
    private PhysicsConnector pivotconnector;
    private TextureRegion pivotregion;
    /* access modifiers changed from: private */
    public boolean placeit = false;
    /* access modifiers changed from: private */
    public boolean playing = false;
    /* access modifiers changed from: private */
    public Sprite pro;
    private TextureRegion promoregion;
    /* access modifiers changed from: private */
    public int reloading = 0;
    private int reloadtime = 2;
    private TextureRegion rock1;
    /* access modifiers changed from: private */
    public float scale = 0.3f;
    /* access modifiers changed from: private */
    public Scene scene;
    private int score = 0;
    private ChangeableText score1text;
    private int score2 = 0;
    private ChangeableText score2text;
    private boolean scored = false;
    private Sound scoresound;
    private Sprite shield;
    private Rectangle shield1;
    private Sprite shield2;
    private TextureRegion shieldregion;
    private Texture shieldtex;
    private TextureRegion shooter1;
    private Sprite shooty;
    private TextureRegion shott;
    /* access modifiers changed from: private */
    public boolean showmenu = false;
    /* access modifiers changed from: private */
    public boolean startlevel = false;
    private int targtime = 0;
    /* access modifiers changed from: private */
    public float time = 10.0f;
    /* access modifiers changed from: private */
    public float touchedx = 0.0f;
    /* access modifiers changed from: private */
    public float touchedy = 0.0f;
    private Sprite track1 = null;
    private Sprite track2 = null;
    private Sprite track3 = null;
    private ChangeableText warningtext;
    private ChangeableText warntext;
    private boolean winner = false;
    private Sound winsound;

    public Engine onLoadEngine() {
        Toast.makeText(this, "Loading...", 1).show();
        this.camera = new SmoothCamera(0.0f, 0.0f, 800.0f, 480.0f, 0.0f, 0.0f, 2.0f);
        return new Engine(new EngineOptions(true, EngineOptions.ScreenOrientation.LANDSCAPE, new RatioResolutionPolicy(800.0f, 480.0f), this.camera).setNeedsSound(true));
    }

    public void onLoadResources() {
        this.mTexture = new Texture(64, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        TextureRegionFactory.setAssetBasePath("gfx/");
        this.mTexturex = new Texture(128, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mTexture = new Texture(32, 64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mTexture2 = new Texture(512, 128, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mTexture3 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mTexture4 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mTexture5 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mTexture6 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mTexture7 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mTexture8 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mTexture9 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mTexture10 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mTexture11 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mTexture12 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mTexture13 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.girderregion = TextureRegionFactory.createFromAsset(this.mTexture3, this, "girder1.png", 0, 0);
        this.boxregion1 = TextureRegionFactory.createFromAsset(this.mTexture4, this, "brick641.png", 0, 0);
        this.boxregion2 = TextureRegionFactory.createFromAsset(this.mTexture4, this, "brick642.png", 128, 0);
        this.pivotregion = TextureRegionFactory.createFromAsset(this.mTexture4, this, "crate642.png", 256, 0);
        this.backregion = TextureRegionFactory.createFromAsset(this.mTexture5, this, "sky1.png", 0, 0);
        this.promoregion = TextureRegionFactory.createFromAsset(this.mTexture6, this, "balancepromo.png", 0, 0);
        this.mPlokFontTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mPlokFontTexture2 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mPlokFont = FontFactory.createFromAsset(this.mPlokFontTexture, this, "font/Plok.ttf", 66.0f, true, -256);
        this.mPlokFont2 = FontFactory.createFromAsset(this.mPlokFontTexture2, this, "font/Plok.ttf", 40.0f, true, -1);
        this.mFontTexture = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mFont = new Font(this.mFontTexture, Typeface.create(Typeface.DEFAULT, 1), 28.0f, true, -65536);
        this.mEngine.getFontManager().loadFonts(this.mPlokFont, this.mPlokFont2, this.mFont);
        TextureManager textureManager = this.mEngine.getTextureManager();
        Texture[] textureArr = new Texture[17];
        textureArr[0] = this.mPlokFontTexture;
        textureArr[1] = this.mPlokFontTexture2;
        textureArr[2] = this.mFontTexture;
        textureArr[3] = this.mFontTexture;
        textureArr[4] = this.mTexture;
        textureArr[5] = this.mTexture2;
        textureArr[6] = this.mTexture3;
        textureArr[7] = this.mTexture4;
        textureArr[8] = this.mTexture5;
        textureArr[LEV9] = this.mTexture6;
        textureArr[LEV10] = this.mTexture7;
        textureArr[LEV11] = this.mTexture8;
        textureArr[12] = this.mTexture9;
        textureArr[13] = this.mTexture10;
        textureArr[14] = this.mTexture11;
        textureArr[15] = this.mTexture12;
        textureArr[16] = this.mTexture13;
        textureManager.loadTextures(textureArr);
        SoundFactory.setAssetBasePath("mfx/");
        try {
            this.mExplosionSound = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "dshot1.mp3");
            this.oversound = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "game_over.ogg");
            this.scoresound = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "bat_chirp.wav");
            this.bingsound = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "squelch1.ogg");
            this.winsound = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "pplclapping.mp3");
        } catch (IOException e) {
            Debug.e("Error", e);
        }
    }

    public Scene onLoadScene() {
        this.mEngine.registerUpdateHandler(new FPSCounter());
        this.mEngine.registerUpdateHandler(new FPSLogger());
        this.mPhysicsWorld = new PhysicsWorld(new Vector2(0.0f, 2.9419951f), false);
        this.scene = new Scene(3);
        this.scene.setOnSceneTouchListener(this);
        this.mMenuScene = createMenuScene();
        this.highscore = new Highscore(this);
        this.highscore.addScore("Localuser", 0);
        this.objectFixtureDef1 = PhysicsFactory.createFixtureDef(0.1f, 0.01f, 2.0f);
        this.objectFixtureDef2 = PhysicsFactory.createFixtureDef(0.6f, 0.2f, 1.0f);
        this.boxes = new Body[50];
        this.boxpics = new Sprite[50];
        this.boxconnectors = new PhysicsConnector[50];
        this.girder = new Sprite(80.0f, 300.0f, this.girderregion);
        this.girder.setScale(1.25f, 0.5f);
        this.girder.setPosition(155.0f, 300.0f);
        this.pivot = new Sprite(386.0f, 333.0f, this.pivotregion);
        this.pivot.setScale(0.5f);
        this.pivot.setPosition(366.0f, 333.0f);
        this.pivot.setColor(0.4f, 0.3f, 0.4f);
        this.grower = new Sprite(-1000.0f, 0.0f, this.boxregion2);
        this.grower.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        this.grower.setAlpha(0.3f);
        this.grower.setScaleCenter(32.0f, 32.0f);
        this.grower.setScale(0.2f);
        this.hitline = new Rectangle(-100.0f, 460.0f, 1200.0f, 3.0f);
        this.hitline.setColor(0.6f, 0.9f, 0.2f);
        this.girderbody = PhysicsFactory.createBoxBody(this.mPhysicsWorld, this.girder, BodyDef.BodyType.DynamicBody, this.objectFixtureDef1);
        this.girderonnector = new PhysicsConnector(this.girder, this.girderbody, true, true);
        this.mPhysicsWorld.registerPhysicsConnector(this.girderonnector);
        this.pivotbody = PhysicsFactory.createBoxBody(this.mPhysicsWorld, this.pivot, BodyDef.BodyType.StaticBody, this.objectFixtureDef1);
        this.pivotconnector = new PhysicsConnector(this.pivot, this.pivotbody, true, true);
        this.mPhysicsWorld.registerPhysicsConnector(this.pivotconnector);
        this.back = new Sprite(200.0f, 100.0f, this.backregion);
        this.back.setScale(1.8f, 1.8f);
        this.pro = new Sprite(300.0f, 50.0f, this.promoregion);
        this.pro.setScale(1.6f, 1.2f);
        this.pro.setPosition(-1000.0f, 0.0f);
        this.scene.getFirstChild().attachChild(this.back);
        this.scene.getLastChild().attachChild(this.hitline);
        this.scene.getLastChild().attachChild(this.girder);
        this.scene.getLastChild().attachChild(this.pivot);
        this.scene.getLastChild().attachChild(this.grower);
        this.scene.registerTouchArea(this.girder);
        this.warntext = new ChangeableText(300.0f, 190.0f, this.mPlokFont, " ", 60);
        this.hightext = new ChangeableText(-15.0f, 0.0f, this.mPlokFont2, "Time: ", 60);
        this.hightext.setScale(0.7f);
        this.leveltext = new ChangeableText(-15.0f, 30.0f, this.mPlokFont2, "Level: ", 60);
        this.leveltext.setScale(0.7f);
        this.warningtext = new ChangeableText(30.0f, 340.0f, this.mPlokFont, "Begin", 14);
        this.mHud = new HUD();
        this.camera.setHUD(this.mHud);
        this.mHud.getLastChild().attachChild(this.pro);
        this.mHud.getLastChild().attachChild(this.warntext);
        this.mHud.getLastChild().attachChild(this.hightext);
        this.mHud.getLastChild().attachChild(this.leveltext);
        this.scene.registerUpdateHandler(this.mPhysicsWorld);
        this.scene.setOnAreaTouchListener(this);
        this.scene.setBackground(new ColorBackground(0.6f, 0.7f, 0.9f));
        this.scene.registerUpdateHandler(new TimerHandler(0.05f, true, new ITimerCallback() {
            private boolean advance;
            private int gameovercount;

            public void onTimePassed(TimerHandler pTimerHandler) {
                Balance balance = Balance.this;
                balance.reloading = balance.reloading + 1;
                if (Balance.this.placeit && Balance.this.playing) {
                    if (Balance.this.boxcount < 49) {
                        Balance.this.addbox(Balance.this.grower.getX(), Balance.this.grower.getY());
                    }
                    Balance.this.placeit = false;
                    Balance.this.growing = false;
                    Balance.this.grower.setPosition(-1000.0f, 0.0f);
                    Balance.this.scale = 0.3f;
                    Balance.this.grower.setScale(Balance.this.scale);
                }
                if (Balance.this.growing && Balance.this.playing) {
                    if (Balance.this.scale < 3.0f) {
                        Balance balance2 = Balance.this;
                        balance2.scale = balance2.scale + 0.05f;
                    }
                    Balance.this.grower.setPosition(Balance.this.touchedx - 32.0f, Balance.this.touchedy - 32.0f);
                    Balance.this.grower.setScale(Balance.this.scale);
                }
                if (Balance.this.hitline.collidesWith(Balance.this.girder) && Balance.this.playing) {
                    Balance.this.died = true;
                    Balance.this.playing = false;
                    this.gameovercount = 0;
                }
                if (Balance.this.died) {
                    this.gameovercount++;
                    Balance.this.hitline.setColor(0.9f, 0.0f, 0.0f);
                    Balance.this.announce("Fail!");
                    if (this.gameovercount > 20) {
                        Balance.this.died = false;
                        this.gameovercount = 0;
                        Balance.this.level = 1;
                        Balance.this.showmenu = true;
                    }
                }
                if (Balance.this.startlevel) {
                    Balance.this.pro.setPosition(-1000.0f, 0.0f);
                    Balance balance3 = Balance.this;
                    balance3.levcounter = balance3.levcounter + 1;
                    Balance.this.announce("Level " + Balance.this.level);
                    Balance.this.leveltext.setText("Level: " + Balance.this.level);
                    Balance.this.placeit = false;
                    Balance.this.growing = false;
                    Balance.this.grower.setPosition(-1000.0f, 0.0f);
                    if (Balance.this.levcounter == 1) {
                        Balance.this.resetstuff();
                    }
                    if (Balance.this.levcounter > 33) {
                        Balance.this.resetgirder();
                        Balance.this.gravitymod = 0.1f + (((float) Balance.this.level) * 0.02f);
                        Balance.this.mPhysicsWorld.setGravity(new Vector2(0.0f, 9.80665f * Balance.this.gravitymod));
                        Balance.this.makerandomblock();
                        Balance.this.setstuff(Balance.this.level);
                        Balance.this.playing = true;
                        Balance.this.startlevel = false;
                        Balance.this.levcounter = 0;
                        Balance.this.scale = 0.3f;
                    }
                }
                if (Balance.this.showmenu) {
                    Balance balance4 = Balance.this;
                    balance4.menucount = balance4.menucount + 1;
                    if (Balance.this.menucount > 30) {
                        Balance.this.mMenuScene.back();
                        Balance.this.mMenuScene.reset();
                        Balance.this.scene.clearChildScene();
                        Balance.this.camera.reset();
                        Balance.this.pro.setPosition(310.0f, 20.0f);
                        Balance.this.scene.setChildScene(Balance.this.mMenuScene, false, true, true);
                        Balance.this.showmenu = false;
                        Balance.this.menucount = 0;
                    }
                }
                if (!this.advance && !Balance.this.died && Balance.this.playing) {
                    Balance balance5 = Balance.this;
                    balance5.time = balance5.time - 0.04f;
                    Balance.this.hightext.setText("Time: " + Balance.this.roundTwoDecimals((double) Balance.this.time));
                    if (Balance.this.time < 0.0f) {
                        Balance.this.time = 10.0f;
                        Balance.this.announce("Good!");
                        Balance.this.playing = false;
                        this.advance = true;
                        this.gameovercount = 0;
                    }
                }
                if (this.advance && !Balance.this.died) {
                    this.gameovercount++;
                    if (this.gameovercount > 70) {
                        Balance.this.levcounter = 0;
                        Balance balance6 = Balance.this;
                        balance6.level = balance6.level + 1;
                        Balance.this.startlevel = true;
                        this.gameovercount = 0;
                        this.advance = false;
                    }
                }
            }
        }));
        return this.scene;
    }

    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, Scene.ITouchArea pTouchArea, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        return false;
    }

    public void onLoadComplete() {
        this.showmenu = true;
    }

    public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
        if (this.mPhysicsWorld != null && pSceneTouchEvent.getY() < 360.0f) {
            if (pSceneTouchEvent.getAction() != 1) {
                this.growing = true;
                this.touchedx = pSceneTouchEvent.getX();
                this.touchedy = pSceneTouchEvent.getY();
                return true;
            } else if (pSceneTouchEvent.getAction() == 1) {
                this.placeit = true;
                return true;
            }
        }
        return false;
    }

    public void onAccelerometerChanged(AccelerometerData pAccelerometerData) {
    }

    /* access modifiers changed from: protected */
    public int getLayoutID() {
        return R.layout.xmllayoutexample;
    }

    /* access modifiers changed from: protected */
    public int getRenderSurfaceViewID() {
        return R.id.xmllayoutexample_rendersurfaceview;
    }

    /* access modifiers changed from: protected */
    public MenuScene createMenuScene() {
        MenuScene menuScene = new MenuScene(this.camera);
        IMenuItem resetMenuItem = new ColorMenuItemDecorator(new TextMenuItem(0, this.mPlokFont, "New Game"), 0.0f, 0.6f, 0.9f, 0.9f, 0.8f, 0.9f);
        resetMenuItem.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        menuScene.addMenuItem(resetMenuItem);
        IMenuItem lvl1 = new ColorMenuItemDecorator(new TextMenuItem(1, this.mPlokFont, "Level 1"), 0.0f, 0.6f, 0.9f, 0.9f, 0.8f, 0.9f);
        lvl1.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        IMenuItem quitMenuItem = new ColorMenuItemDecorator(new TextMenuItem(1000, this.mPlokFont, "Exit"), 0.0f, 0.6f, 0.9f, 0.9f, 0.8f, 0.9f);
        quitMenuItem.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        menuScene.addMenuItem(quitMenuItem);
        menuScene.buildAnimations();
        menuScene.setBackgroundEnabled(false);
        menuScene.setOnMenuItemClickListener(this);
        this.mSubMenuScene = new MenuScene(this.camera);
        this.mSubMenuScene.addMenuItem(lvl1);
        this.mSubMenuScene.setMenuAnimator(new SlideMenuAnimator());
        this.mSubMenuScene.buildAnimations();
        this.mSubMenuScene.setBackgroundEnabled(false);
        this.mSubMenuScene.setOnMenuItemClickListener(this);
        return menuScene;
    }

    public boolean onKeyDown(int pKeyCode, KeyEvent pEvent) {
        if ((pKeyCode != 82 && pKeyCode != 4) || pEvent.getAction() != 0) {
            return super.onKeyDown(pKeyCode, pEvent);
        }
        if (this.camera.getZoomFactor() >= 0.8f) {
            this.camera.reset();
            if (this.scene.hasChildScene()) {
                this.mMenuScene.back();
                this.pro.setPosition(-1000.0f, 0.0f);
            } else {
                this.showmenu = true;
            }
        }
        return true;
    }

    public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem, float pMenuItemLocalX, float pMenuItemLocalY) {
        switch (pMenuItem.getID()) {
            case 0:
                this.levelstart = true;
                this.scene.clearChildScene();
                this.mMenuScene.reset();
                this.level = 1;
                this.score = 0;
                this.score2 = 0;
                this.winner = false;
                this.levcounter = 0;
                this.startlevel = true;
                return true;
            case 1:
                this.level = 1;
                this.levelstart = true;
                this.scene.clearChildScene();
                this.mMenuScene.reset();
                return true;
            case 1000:
                finish();
                return true;
            case MENU_SELECT /*1001*/:
                pMenuScene.setChildSceneModal(this.mSubMenuScene);
                return true;
            default:
                return false;
        }
    }

    public void addbox(float x, float y) {
        this.boxpics[this.boxcount] = new Sprite(x, y, this.boxregion1);
        this.boxpics[this.boxcount].setScale(this.scale);
        this.scene.getLastChild().attachChild(this.boxpics[this.boxcount]);
        this.boxes[this.boxcount] = PhysicsFactory.createBoxBody(this.mPhysicsWorld, this.boxpics[this.boxcount], BodyDef.BodyType.DynamicBody, this.objectFixtureDef2);
        this.boxconnectors[this.boxcount] = new PhysicsConnector(this.boxpics[this.boxcount], this.boxes[this.boxcount], true, true);
        this.mPhysicsWorld.registerPhysicsConnector(this.boxconnectors[this.boxcount]);
        this.boxcount++;
    }

    public void resetstuff() {
        for (int j = 0; j <= 49; j++) {
            if (this.boxpics[j] != null) {
                this.mPhysicsWorld.unregisterPhysicsConnector(this.boxconnectors[j]);
                this.boxconnectors[j] = null;
                this.mPhysicsWorld.destroyBody(this.boxes[j]);
                this.boxes[j] = null;
                this.scene.getLastChild().detachChild(this.boxpics[j]);
                this.boxpics[j] = null;
            }
        }
        this.hitline.setColor(0.6f, 0.9f, 0.2f);
        this.mPhysicsWorld.unregisterPhysicsConnector(this.girderonnector);
        this.girder.setRotation(0.0f);
        this.girder.setPosition(-1000.0f, -1000.0f);
        this.girderbody.setAngularVelocity(0.0f);
        this.girderbody.setLinearVelocity(new Vector2(0.0f, 0.0f));
        this.mPhysicsWorld.destroyBody(this.girderbody);
        this.mPhysicsWorld.clearForces();
        this.mPhysicsWorld.reset();
        this.mPhysicsWorld.setGravity(new Vector2(0.0f, 0.0f));
        this.girderonnector.reset();
        this.boxcount = 0;
    }

    public void resetgirder() {
        this.girder.setRotation(0.0f);
        this.girder.setPosition(155.0f, 300.0f);
        this.mPhysicsWorld.clearForces();
        this.mPhysicsWorld.reset();
        this.mPhysicsWorld.reset();
        this.girderbody = PhysicsFactory.createBoxBody(this.mPhysicsWorld, this.girder, BodyDef.BodyType.DynamicBody, this.objectFixtureDef1);
        this.girderbody.setAngularVelocity(0.0f);
        this.girderbody.setLinearVelocity(new Vector2(0.0f, 0.0f));
        this.girderonnector = new PhysicsConnector(this.girder, this.girderbody, true, true);
        this.mPhysicsWorld.registerPhysicsConnector(this.girderonnector);
    }

    public void setstuff(int lev) {
        if (lev == 1) {
            this.time = 10.0f;
        } else if (lev == 2) {
            this.time = 15.0f;
        } else if (lev == 3) {
            this.time = 18.0f;
        } else if (lev == 4) {
            this.time = 22.0f;
        } else if (lev == 5) {
            this.time = 26.0f;
        } else if (lev == 6) {
            this.time = 30.0f;
        } else if (lev == 7) {
            this.time = 32.0f;
        } else if (lev == 8) {
            this.time = 35.0f;
        } else if (lev == LEV9) {
            this.time = 36.0f;
        } else if (lev == LEV10) {
            this.time = 40.0f;
        } else if (lev == LEV11) {
            this.time = 43.0f;
        } else if (lev == 12) {
            this.time = 44.0f;
        } else if (lev == 13) {
            this.time = 60.0f;
        } else if (lev == 14) {
            announce("You Win!");
            this.score += 500;
            this.highscore.addScore("Localuser", (long) this.score);
            this.showmenu = true;
        }
    }

    public double roundTwoDecimals(double d) {
        return Double.valueOf(new DecimalFormat("#.##").format(d)).doubleValue();
    }

    public void announce(String s) {
        this.warntext.setColor(0.8f, 0.4f, 0.2f);
        this.warntext.setText(s);
        this.warntext.registerEntityModifier(new ParallelEntityModifier(new AlphaModifier(2.4f, 1.0f, 0.0f), new ScaleModifier(2.2f, 1.1f, 2.2f)));
        this.warntext.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
    }

    public void makerandomblock() {
        int leftright;
        if (((int) Math.round(Math.random() * 20.0d)) < LEV10) {
            leftright = 1;
        } else {
            leftright = -1;
        }
        this.scale = 0.5f + (((float) this.level) * 0.1f);
        addbox((float) (368.0d + (((double) leftright) * (110.0d + (((double) this.level) * Math.random() * 12.0d)))), 295.0f - (this.scale * 64.0f));
    }
}
