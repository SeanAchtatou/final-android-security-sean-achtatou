package com.jcwp.Abeau;

import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Browser;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.Toast;
import com.vpview.util.AssetUtil;
import com.vpview.util.BookMarkUtil;
import com.vpview.util.FileMatch;
import com.vpview.util.FileUtil;
import com.vpview.util.Folder;
import com.vpview.util.GenUtil;
import com.vpview.util.XMEnDecrypt;
import com.wpview.yeexm.R;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class MainActivity extends TabActivity {
    public static TabHost tabHost = null;
    private int iNowTab = 0;
    private int iPreTab = 0;

    public void onCreate(Bundle savedInstanceState) {
        if (!Environment.getExternalStorageState().equalsIgnoreCase("mounted")) {
            new AlertDialog.Builder(this).setTitle(getResources().getText(R.string.txt_wifi_none_title)).setIcon((int) R.drawable.toast_warnning).setMessage(savedInstanceState.getString("nosdcard")).setPositiveButton("btn_ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    MainActivity.this.finish();
                }
            }).show();
        }
        super.onCreate(savedInstanceState);
        getWindow().addFlags(1024);
        requestWindowFeature(1);
        if (!new File(Folder.MYIMAGE).exists()) {
            try {
                FileUtil.createFolders(Folder.MYIMAGE);
            } catch (Exception e) {
                e.printStackTrace();
            }
            new BookMarkUtil(Folder.MYBOOKMARK, "bookmark").add("My saved|||/sdcard/WPViewernew/myimage/index.xml|||/sdcard/WPViewernew/myimage/index.jpg");
        }
        copyAssetWallpaper_index();
        copyAssetWallpaper();
        initTab();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.widget.FrameLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void initTab() {
        tabHost = getTabHost();
        TabWidget tabWidget = tabHost.getTabWidget();
        Resources resources = getResources();
        LayoutInflater.from(this).inflate((int) R.layout.main, (ViewGroup) tabHost.getTabContentView(), true);
        tabHost.addTab(tabHost.newTabSpec("MyFaorite").setIndicator(resources.getString(R.string.title_myalumb), resources.getDrawable(R.drawable.mywallpaper)).setContent(new Intent(this, ImageCategory.class)));
        tabHost.addTab(tabHost.newTabSpec("MyLib").setIndicator(resources.getString(R.string.title_morealumb), resources.getDrawable(R.drawable.mylibrary)).setContent(new Intent(this, ImageLibrary.class)));
        tabHost.addTab(tabHost.newTabSpec("MoreApps").setIndicator(resources.getString(R.string.title_bestapk), resources.getDrawable(R.drawable.more)).setContent(new Intent(this, MoreApk.class)));
    }

    private void copyAssetWallpaper() {
        String strAppName = getResources().getString(R.string.app_name).replace(" ", "");
        String strDestFolder = Folder.MYCOLLECTION + strAppName + "/";
        List<String> listFile = null;
        try {
            listFile = getSplitsFiles("index.xml");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (listFile == null) {
            GenUtil.systemPrintln("No packaged wallpapers");
            return;
        }
        if (!FileUtil.fileExist(strDestFolder)) {
            try {
                FileUtil.createFolders(strDestFolder);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            AssetUtil aUtil = new AssetUtil(this);
            aUtil.releaseAssetRes("index.xml", strDestFolder + "index.xml", true);
            GenUtil.systemPrintln("packaged wallpapers = " + listFile.size());
            aUtil.releaseAssetRes("index.jpg", strDestFolder + "index.jpg", true);
            for (int i = 0; i < listFile.size(); i++) {
                String strOrigFile = listFile.get(i);
                aUtil.releaseAssetRes(strOrigFile, strDestFolder + strOrigFile, true);
                String strOrigFile2 = strOrigFile.replace("sm_", "");
                Boolean releaseAssetResRet = aUtil.releaseAssetResRet(strOrigFile2, strDestFolder + strOrigFile2, true);
                GenUtil.systemPrintln("");
            }
        }
        new BookMarkUtil(Folder.MYBOOKMARK, "bookmark").add(strAppName + "|||" + strDestFolder + "index.xml" + "|||" + strDestFolder + "index.jpg");
    }

    private void copyAssetWallpaper_index() {
        if (!FileUtil.fileExist("/sdcard/WPViewernew/myimage/index.jpg")) {
            new AssetUtil(this).releaseAssetRes("index_saved.jpg", "/sdcard/WPViewernew/myimage/index.jpg", true);
        }
    }

    public List<String> getSplitsFiles(String strFileName) throws IOException {
        UnsupportedEncodingException e;
        List<String> lstFiles = null;
        InputStream finstream = getAssets().open(strFileName);
        if (finstream != null) {
            int iLen = finstream.available();
            byte[] btBuffer = new byte[iLen];
            if (finstream.read(btBuffer) == iLen) {
                try {
                    String strContent = new String(XMEnDecrypt.XMDecryptString(btBuffer), "utf-8");
                    try {
                        GenUtil.systemPrintln(strContent);
                        lstFiles = FileMatch.matchRootListNoencode(strContent, "Name");
                    } catch (UnsupportedEncodingException e2) {
                        e = e2;
                    }
                } catch (UnsupportedEncodingException e3) {
                    e = e3;
                    e.printStackTrace();
                    finstream.close();
                    return lstFiles;
                }
            }
            try {
                finstream.close();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
        }
        return lstFiles;
    }

    public boolean onCreateOptionsMenu(Menu paramMenu) {
        getMenuInflater().inflate(R.menu.menu, paramMenu);
        return super.onCreateOptionsMenu(paramMenu);
    }

    public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
        super.onOptionsItemSelected(paramMenuItem);
        switch (paramMenuItem.getItemId()) {
            case R.id.shortcut /*2131099685*/:
                addShortCut();
                return true;
            case R.id.share /*2131099686*/:
                AlertDialog.Builder localBuilder = new AlertDialog.Builder(this).setTitle((int) R.string.alertdialog_share);
                localBuilder.setItems((int) R.array.select_share_methods, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        switch (paramInt) {
                            case 0:
                                Intent intent1 = new Intent("android.intent.action.VIEW");
                                intent1.putExtra("sms_body", MainActivity.this.getResources().getString(R.string.txt_mailsubject) + " " + MainActivity.this.getResources().getString(R.string.txt_mailcontent));
                                intent1.setType("vnd.android-dir/mms-sms");
                                MainActivity.this.startActivity(intent1);
                                return;
                            case 1:
                                Intent intent2 = new Intent("android.intent.action.SEND");
                                intent2.setType("plain/text");
                                intent2.putExtra("android.intent.extra.SUBJECT", MainActivity.this.getResources().getString(R.string.txt_mailsubject));
                                intent2.putExtra("android.intent.extra.TEXT", MainActivity.this.getResources().getString(R.string.txt_mailcontent));
                                MainActivity.this.startActivity(Intent.createChooser(intent2, MainActivity.this.getResources().getString(R.string.txt_choice)));
                                return;
                            case R.styleable.com_admob_android_ads_AdView_secondaryTextColor /*2*/:
                                Browser.sendString(MainActivity.this, MainActivity.this.getResources().getString(R.string.txt_mailsubject) + MainActivity.this.getResources().getString(R.string.txt_mailcontent));
                                return;
                            default:
                                return;
                        }
                    }
                });
                localBuilder.create().show();
                return true;
            default:
                return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void addShortCut() {
        Intent shortcut = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        shortcut.putExtra("android.intent.extra.shortcut.NAME", getString(R.string.app_name));
        shortcut.putExtra("duplicate", false);
        shortcut.putExtra("android.intent.extra.shortcut.INTENT", new Intent("android.intent.action.MAIN").setComponent(new ComponentName(getPackageName(), "." + getLocalClassName())));
        shortcut.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(this, R.drawable.icon));
        sendBroadcast(shortcut);
        Toast.makeText(this, (int) R.string.tip_set_shortcut, 1000).show();
    }
}
