package com.jcwp.Abeau;

import com.vpview.util.FileUtil;
import com.vpview.util.Folder;
import com.vpview.util.GenUtil;
import com.vpview.util.XMEnDecrypt;
import com.wpview.yeexm.R;
import java.io.File;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class Protocals {
    private static Protocals mProtocals = null;
    public static final String strHomeRootURL = "http://www.iphone2020.com/wallpaperhd/";

    public static Protocals getInstance() {
        Protocals protocals;
        synchronized (Protocals.class) {
            if (mProtocals == null) {
                mProtocals = new Protocals();
            }
            protocals = mProtocals;
        }
        return protocals;
    }

    private HttpResponse executePost(String strURL) {
        try {
            if (strURL.length() <= 0) {
                return null;
            }
            BasicHttpParams bhParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(bhParams, 3000);
            HttpConnectionParams.setSoTimeout(bhParams, 6000);
            return new DefaultHttpClient(bhParams).execute(new HttpPost(strURL));
        } catch (Exception e) {
            return null;
        }
    }

    private HttpResponse executePost(String strURL, List list) {
        try {
            if (strURL.length() <= 0) {
                return null;
            }
            BasicHttpParams bhParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(bhParams, 3000);
            HttpConnectionParams.setSoTimeout(bhParams, 6000);
            DefaultHttpClient defaultClient = new DefaultHttpClient(bhParams);
            HttpPost httpPost = new HttpPost(strURL);
            httpPost.setEntity(new UrlEncodedFormEntity(list, "utf-8"));
            return defaultClient.execute(httpPost);
        } catch (Exception e) {
            return null;
        }
    }

    /* JADX WARN: Type inference failed for: r7v2, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getWebContent(java.lang.String r11) {
        /*
            r10 = this;
            r2 = 0
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x0037 }
            r4.<init>()     // Catch:{ Exception -> 0x0037 }
            java.net.URL r6 = new java.net.URL     // Catch:{ Exception -> 0x0037 }
            r6.<init>(r11)     // Catch:{ Exception -> 0x0037 }
            java.net.URLConnection r7 = r6.openConnection()     // Catch:{ Exception -> 0x0037 }
            r0 = r7
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0037 }
            r2 = r0
            r7 = 1
            r2.setDoOutput(r7)     // Catch:{ Exception -> 0x0037 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0037 }
            java.io.InputStreamReader r7 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0037 }
            java.io.InputStream r8 = r2.getInputStream()     // Catch:{ Exception -> 0x0037 }
            java.lang.String r9 = "gb2312"
            r7.<init>(r8, r9)     // Catch:{ Exception -> 0x0037 }
            r1.<init>(r7)     // Catch:{ Exception -> 0x0037 }
        L_0x0027:
            java.lang.String r5 = r1.readLine()     // Catch:{ Exception -> 0x0037 }
            if (r5 == 0) goto L_0x0040
            java.lang.StringBuffer r7 = r4.append(r5)     // Catch:{ Exception -> 0x0037 }
            java.lang.String r8 = "\r\n"
            r7.append(r8)     // Catch:{ Exception -> 0x0037 }
            goto L_0x0027
        L_0x0037:
            r7 = move-exception
            r3 = r7
            if (r2 == 0) goto L_0x003e
            r2.disconnect()
        L_0x003e:
            r7 = 0
        L_0x003f:
            return r7
        L_0x0040:
            r1.close()     // Catch:{ Exception -> 0x0037 }
            r2.disconnect()     // Catch:{ Exception -> 0x0037 }
            java.lang.String r7 = r4.toString()     // Catch:{ Exception -> 0x0037 }
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jcwp.Abeau.Protocals.getWebContent(java.lang.String):java.lang.String");
    }

    /* JADX WARN: Type inference failed for: r8v0, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String downWebContent(java.lang.String r25, java.lang.String r26) {
        /*
            r24 = this;
            r13 = 0
            r12 = 0
            r19 = 0
            r22 = 0
            java.lang.String r23 = "/"
            r0 = r26
            r1 = r23
            int r23 = r0.lastIndexOf(r1)
            r0 = r26
            r1 = r22
            r2 = r23
            java.lang.String r18 = r0.substring(r1, r2)
            com.vpview.util.FileUtil.createFolders(r18)     // Catch:{ Exception -> 0x0076 }
        L_0x001d:
            java.io.FileOutputStream r14 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x007b }
            r0 = r14
            r1 = r26
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x007b }
            r13 = r14
        L_0x0026:
            r7 = 0
            java.lang.StringBuffer r15 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x00d4 }
            r15.<init>()     // Catch:{ Exception -> 0x00d4 }
            java.net.URL r21 = new java.net.URL     // Catch:{ Exception -> 0x00d4 }
            r0 = r21
            r1 = r25
            r0.<init>(r1)     // Catch:{ Exception -> 0x00d4 }
            java.net.URLConnection r8 = r21.openConnection()     // Catch:{ Exception -> 0x00d4 }
            r0 = r8
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00d4 }
            r7 = r0
            r22 = 1
            r0 = r7
            r1 = r22
            r0.setDoOutput(r1)     // Catch:{ Exception -> 0x00d4 }
            int r8 = r7.getContentLength()     // Catch:{ Exception -> 0x00d4 }
            byte[] r12 = new byte[r8]     // Catch:{ Exception -> 0x00d4 }
            java.io.InputStream r17 = r7.getInputStream()     // Catch:{ Exception -> 0x00d4 }
            r22 = 1024(0x400, float:1.435E-42)
            r0 = r22
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x00d4 }
            r4 = r0
            r5 = -1
            r6 = 0
        L_0x0058:
            r0 = r17
            r1 = r4
            int r5 = r0.read(r1)     // Catch:{ Exception -> 0x00d4 }
            r22 = -1
            r0 = r5
            r1 = r22
            if (r0 == r1) goto L_0x00af
            r16 = 0
        L_0x0068:
            r0 = r16
            r1 = r5
            if (r0 >= r1) goto L_0x0082
            int r22 = r6 + r16
            byte r23 = r4[r16]     // Catch:{ Exception -> 0x00d4 }
            r12[r22] = r23     // Catch:{ Exception -> 0x00d4 }
            int r16 = r16 + 1
            goto L_0x0068
        L_0x0076:
            r11 = move-exception
            r11.printStackTrace()
            goto L_0x001d
        L_0x007b:
            r22 = move-exception
            r9 = r22
            r9.printStackTrace()
            goto L_0x0026
        L_0x0082:
            if (r13 == 0) goto L_0x00ce
            byte[] r22 = com.vpview.util.XMEnDecrypt.XMEncryptString(r4)     // Catch:{ Exception -> 0x00d4 }
            r23 = 0
            r0 = r13
            r1 = r22
            r2 = r23
            r3 = r5
            r0.write(r1, r2, r3)     // Catch:{ Exception -> 0x00d4 }
        L_0x0093:
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00d4 }
            r22.<init>()     // Catch:{ Exception -> 0x00d4 }
            java.lang.String r23 = "byteread ="
            java.lang.StringBuilder r22 = r22.append(r23)     // Catch:{ Exception -> 0x00d4 }
            r0 = r22
            r1 = r5
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ Exception -> 0x00d4 }
            java.lang.String r22 = r22.toString()     // Catch:{ Exception -> 0x00d4 }
            com.vpview.util.GenUtil.systemPrintln(r22)     // Catch:{ Exception -> 0x00d4 }
            int r6 = r6 + r5
            if (r6 != r8) goto L_0x0058
        L_0x00af:
            r13.flush()     // Catch:{ Exception -> 0x00d4 }
            r17.close()     // Catch:{ Exception -> 0x00d4 }
            r7.disconnect()     // Catch:{ Exception -> 0x00d4 }
            r21 = 0
            r0 = r12
            int r0 = r0.length     // Catch:{ Exception -> 0x00d4 }
            r22 = r0
            if (r22 <= 0) goto L_0x00ca
            java.lang.String r20 = new java.lang.String     // Catch:{ Exception -> 0x00d4 }
            r0 = r20
            r1 = r12
            r0.<init>(r1)     // Catch:{ Exception -> 0x00d4 }
            r19 = r20
        L_0x00ca:
            r12 = 0
            r22 = r19
        L_0x00cd:
            return r22
        L_0x00ce:
            java.lang.String r22 = "fsOutput is null"
            com.vpview.util.GenUtil.systemPrintln(r22)     // Catch:{ Exception -> 0x00d4 }
            goto L_0x0093
        L_0x00d4:
            r22 = move-exception
            r9 = r22
            if (r7 == 0) goto L_0x00dc
            r7.disconnect()
        L_0x00dc:
            r13.close()     // Catch:{ IOException -> 0x00e5 }
            com.vpview.util.FileUtil.delFile(r26)     // Catch:{ IOException -> 0x00e5 }
        L_0x00e2:
            r22 = 0
            goto L_0x00cd
        L_0x00e5:
            r22 = move-exception
            r10 = r22
            r10.printStackTrace()
            goto L_0x00e2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jcwp.Abeau.Protocals.downWebContent(java.lang.String, java.lang.String):java.lang.String");
    }

    /* JADX WARN: Type inference failed for: r8v0, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] getWebContent(java.lang.String r23, java.lang.String r24) {
        /*
            r22 = this;
            r13 = 0
            r12 = 0
            r20 = 0
            java.lang.String r21 = "/"
            r0 = r24
            r1 = r21
            int r21 = r0.lastIndexOf(r1)
            r0 = r24
            r1 = r20
            r2 = r21
            java.lang.String r18 = r0.substring(r1, r2)
            com.vpview.util.FileUtil.createFolders(r18)     // Catch:{ Exception -> 0x0074 }
        L_0x001b:
            java.io.FileOutputStream r14 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0079 }
            r0 = r14
            r1 = r24
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x0079 }
            r13 = r14
        L_0x0024:
            r7 = 0
            java.lang.StringBuffer r15 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x00bc }
            r15.<init>()     // Catch:{ Exception -> 0x00bc }
            java.net.URL r19 = new java.net.URL     // Catch:{ Exception -> 0x00bc }
            r0 = r19
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x00bc }
            java.net.URLConnection r8 = r19.openConnection()     // Catch:{ Exception -> 0x00bc }
            r0 = r8
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00bc }
            r7 = r0
            r20 = 1
            r0 = r7
            r1 = r20
            r0.setDoOutput(r1)     // Catch:{ Exception -> 0x00bc }
            int r8 = r7.getContentLength()     // Catch:{ Exception -> 0x00bc }
            byte[] r12 = new byte[r8]     // Catch:{ Exception -> 0x00bc }
            java.io.InputStream r17 = r7.getInputStream()     // Catch:{ Exception -> 0x00bc }
            r20 = 1024(0x400, float:1.435E-42)
            r0 = r20
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x00bc }
            r4 = r0
            r5 = -1
            r6 = 0
        L_0x0056:
            r0 = r17
            r1 = r4
            int r5 = r0.read(r1)     // Catch:{ Exception -> 0x00bc }
            r20 = -1
            r0 = r5
            r1 = r20
            if (r0 == r1) goto L_0x00a8
            r16 = 0
        L_0x0066:
            r0 = r16
            r1 = r5
            if (r0 >= r1) goto L_0x0080
            int r20 = r6 + r16
            byte r21 = r4[r16]     // Catch:{ Exception -> 0x00bc }
            r12[r20] = r21     // Catch:{ Exception -> 0x00bc }
            int r16 = r16 + 1
            goto L_0x0066
        L_0x0074:
            r11 = move-exception
            r11.printStackTrace()
            goto L_0x001b
        L_0x0079:
            r20 = move-exception
            r9 = r20
            r9.printStackTrace()
            goto L_0x0024
        L_0x0080:
            if (r13 == 0) goto L_0x00b6
            r20 = 0
            r0 = r13
            r1 = r4
            r2 = r20
            r3 = r5
            r0.write(r1, r2, r3)     // Catch:{ Exception -> 0x00bc }
        L_0x008c:
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bc }
            r20.<init>()     // Catch:{ Exception -> 0x00bc }
            java.lang.String r21 = "byteread ="
            java.lang.StringBuilder r20 = r20.append(r21)     // Catch:{ Exception -> 0x00bc }
            r0 = r20
            r1 = r5
            java.lang.StringBuilder r20 = r0.append(r1)     // Catch:{ Exception -> 0x00bc }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x00bc }
            com.vpview.util.GenUtil.systemPrintln(r20)     // Catch:{ Exception -> 0x00bc }
            int r6 = r6 + r5
            if (r6 != r8) goto L_0x0056
        L_0x00a8:
            r13.flush()     // Catch:{ Exception -> 0x00bc }
            r17.close()     // Catch:{ Exception -> 0x00bc }
            r7.disconnect()     // Catch:{ Exception -> 0x00bc }
            r19 = 0
            r20 = r12
        L_0x00b5:
            return r20
        L_0x00b6:
            java.lang.String r20 = "fsOutput is null"
            com.vpview.util.GenUtil.systemPrintln(r20)     // Catch:{ Exception -> 0x00bc }
            goto L_0x008c
        L_0x00bc:
            r20 = move-exception
            r9 = r20
            if (r7 == 0) goto L_0x00c4
            r7.disconnect()
        L_0x00c4:
            r13.close()     // Catch:{ IOException -> 0x00cd }
            com.vpview.util.FileUtil.delFile(r24)     // Catch:{ IOException -> 0x00cd }
        L_0x00ca:
            r20 = 0
            goto L_0x00b5
        L_0x00cd:
            r20 = move-exception
            r10 = r20
            r10.printStackTrace()
            goto L_0x00ca
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jcwp.Abeau.Protocals.getWebContent(java.lang.String, java.lang.String):byte[]");
    }

    public Matcher RegMatchSplit(String strContent, String strRegex) {
        return Pattern.compile(strRegex, 10).matcher(strContent);
    }

    public List getCategories(String strWebImgRootURL) {
        return getCategories(strWebImgRootURL, "http://www.iphone2020.com/wallpaperhd/", Folder.MYCACHE);
    }

    public List getCategories(String strWebImgRootURL, String strRootURL, String strRootFold) {
        String strLocal;
        String strType;
        if (strWebImgRootURL.indexOf(Folder.POPIMG) > -1) {
            strLocal = strWebImgRootURL;
            if (strWebImgRootURL.indexOf(Folder.MYIMAGE) > -1) {
                strType = "SAVEAS";
            } else {
                strType = "LOCALALBUM";
            }
        } else {
            strLocal = strRootFold + strWebImgRootURL.substring(strRootURL.length());
            strType = "WEB";
        }
        GenUtil.systemPrintln("getCategories--------- strWebImgRootURL = " + strWebImgRootURL);
        GenUtil.systemPrintln("getCategories--------- strLocal = " + strLocal);
        String strContent = "";
        byte[] bContent = null;
        if (FileUtil.fileExist(strLocal + "index.xml")) {
            bContent = FileUtil.readFile(strLocal + "index.xml");
        }
        if (bContent == null && strType.equals("WEB")) {
            try {
                FileUtil.createFolder(strLocal + "/");
            } catch (Exception e) {
                e.printStackTrace();
            }
            bContent = getWebContent(strWebImgRootURL + "index.xml", strLocal + "index.xml");
        }
        GenUtil.systemPrintln("bContent before = " + bContent);
        if (bContent != null) {
            byte[] btBuffers = XMEnDecrypt.XMDecryptString(bContent);
            try {
                strContent = new String(btBuffers, 0, btBuffers.length, "utf-8");
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
            GenUtil.systemPrintln("btBuffers = " + btBuffers.length);
        } else if (strType.equals("SAVEAS")) {
            GenUtil.systemPrintln("SAVEAS called ");
            strContent = "";
            File[] filelist = new File(Folder.MYIMAGE).listFiles();
            GenUtil.systemPrintln("filelist.length = " + filelist.length);
            for (int i = 0; i < filelist.length; i++) {
                String strFName = filelist[i].getName();
                GenUtil.systemPrintln("strFName = " + strFName);
                if (strFName.startsWith("sm_")) {
                    strContent = strContent + "<Name>" + strFName + "</Name><Type>File</Type><CoverImg></CoverImg><Category>F</Category>";
                }
            }
        }
        GenUtil.systemPrintln("strContent = " + strContent);
        AlbumReturn albumReturn = new AlbumReturn();
        albumReturn.parse(strContent, "<Name>(.*?)</Name><Type>(.*?)</Type><CoverImg>(.*?)</CoverImg><Category>(.*?)</Category>");
        return albumReturn.mlstAlbums;
    }

    private String getRelativePath(String strURL, String strRootURL) {
        return strURL.substring(strRootURL.length());
    }

    public class AlbumReturn implements Serializable {
        public int miTotal = 0;
        public List mlstAlbums = new ArrayList();

        public AlbumReturn() {
        }

        /* access modifiers changed from: private */
        public void parse(String strContent, String strRegex) {
            if (strContent != null && !strContent.equals("")) {
                this.mlstAlbums.clear();
                this.miTotal = 0;
                Matcher matcher = Protocals.this.RegMatchSplit(strContent, strRegex);
                while (matcher.find()) {
                    Album album = new Album();
                    int iGroupNum = matcher.groupCount();
                    for (int iIndex = 1; iIndex <= iGroupNum; iIndex++) {
                        switch (iIndex) {
                            case 1:
                                album.name = matcher.group(iIndex).replaceAll(" ", "%20");
                                GenUtil.systemPrintln("album.name = " + album.name);
                                break;
                            case R.styleable.com_admob_android_ads_AdView_secondaryTextColor /*2*/:
                                album.bIcon = matcher.group(iIndex).toString().equalsIgnoreCase("Fold");
                                break;
                            case R.styleable.com_admob_android_ads_AdView_keywords /*3*/:
                                album.image = matcher.group(iIndex);
                                break;
                            case R.styleable.com_admob_android_ads_AdView_refreshInterval /*4*/:
                                album.category = matcher.group(iIndex);
                                break;
                        }
                    }
                    this.mlstAlbums.add(album);
                    this.miTotal++;
                }
            }
        }
    }

    public class Album {
        public long addtime;
        public boolean bIcon;
        public String category;
        public String image;
        public String indexurl;
        public String mini;
        public String name;

        public Album() {
        }
    }
}
