package com.fengwallp.T4001;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;

public class BaseImgActivity extends Activity {
    static final String ALBUM_ID = "album_id";
    static final String ALBUM_NAME = "album_name";
    static final String ALBUM_NUM = "album_num";
    static final int DIALOG_LOADING = 1;
    static final int FALSE = 0;
    static final String IMAGE_POS = "image_pos";
    static final int MENU_ADD_FAVORITE = 9;
    static final int MENU_ITEM_REFRESH = 8;
    static final String PAGE_GOTO = "page_goto";
    static final String PAGE_INDEX = "page_index";
    static final String PAGE_TOTAL = "page_total";
    static final int RESULT_ALREADY_EXISTS = 3;
    static final int RQ_ADD_TO_FAVORITE = 1;
    static final int RQ_EXIT = 3;
    static final int RQ_GOTO = 2;
    static final int TRUE = 1;
    int iScreenHeight;
    int iScreenWidth;

    /* access modifiers changed from: package-private */
    public void calScreen() {
        Display display = getWindowManager().getDefaultDisplay();
        this.iScreenWidth = display.getWidth();
        this.iScreenWidth = display.getHeight();
    }

    /* access modifiers changed from: package-private */
    public void fullScreen() {
        getWindow().addFlags(1024);
        requestWindowFeature(1);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int iparam1, int iparam2, Intent paramIntent) {
        if (iparam1 == 3 && iparam2 == -1) {
            System.exit(0);
        }
        super.onActivityResult(iparam1, iparam2, paramIntent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        calScreen();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int iParm) {
        if (iParm == 1) {
            ProgressDialog pd = new ProgressDialog(this);
            pd.setIndeterminate(false);
            pd.setCancelable(false);
            pd.setMessage("loading...");
        }
        return super.onCreateDialog(iParm);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
