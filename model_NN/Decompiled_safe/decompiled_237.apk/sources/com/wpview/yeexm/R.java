package com.wpview.yeexm;

public final class R {

    public static final class array {
        public static final int select_share_methods = 2130968576;
    }

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class drawable {
        public static final int current = 2130837504;
        public static final int file_package = 2130837505;
        public static final int go = 2130837506;
        public static final int go_s = 2130837507;
        public static final int ic_menu_add = 2130837508;
        public static final int ic_menu_refresh = 2130837509;
        public static final int ic_menu_send = 2130837510;
        public static final int ic_menu_set_wallpaper = 2130837511;
        public static final int icon = 2130837512;
        public static final int more = 2130837513;
        public static final int more_s = 2130837514;
        public static final int mylibrary = 2130837515;
        public static final int mylibrary_s = 2130837516;
        public static final int mywallpaper = 2130837517;
        public static final int mywallpaper_s = 2130837518;
        public static final int next = 2130837519;
        public static final int next1 = 2130837520;
        public static final int next_press1 = 2130837521;
        public static final int next_s = 2130837522;
        public static final int no = 2130837523;
        public static final int prev = 2130837524;
        public static final int prev1 = 2130837525;
        public static final int prev_press1 = 2130837526;
        public static final int prev_s = 2130837527;
        public static final int toast_warnning = 2130837528;
        public static final int xml_btn_goto = 2130837529;
        public static final int xml_btn_next = 2130837530;
        public static final int xml_btn_next1 = 2130837531;
        public static final int xml_btn_prev = 2130837532;
        public static final int xml_btn_prev1 = 2130837533;
    }

    public static final class id {
        public static final int ad = 2131165196;
        public static final int album = 2131165184;
        public static final int btn_goto = 2131165189;
        public static final int cat_progressbar = 2131165193;
        public static final int category_list = 2131165192;
        public static final int deletealbum = 2131165211;
        public static final int grid = 2131165194;
        public static final int grid_img = 2131165195;
        public static final int hsv = 2131165197;
        public static final int image = 2131165198;
        public static final int img_progressbar = 2131165201;
        public static final int mylib_layer = 2131165203;
        public static final int next = 2131165191;
        public static final int next1 = 2131165200;
        public static final int page_info = 2131165190;
        public static final int prev = 2131165188;
        public static final int prev1 = 2131165199;
        public static final int progress_layout = 2131165187;
        public static final int refresh = 2131165207;
        public static final int save = 2131165209;
        public static final int savegrid = 2131165208;
        public static final int set_wallpaper = 2131165210;
        public static final int share = 2131165213;
        public static final int shortcut = 2131165212;
        public static final int top_layout = 2131165186;
        public static final int tv_txt_cat = 2131165185;
        public static final int txt_progress = 2131165202;
        public static final int web = 2131165205;
        public static final int webparent = 2131165204;
        public static final int wv_page = 2131165206;
    }

    public static final class layout {
        public static final int album_list = 2130903040;
        public static final int category = 2130903041;
        public static final int grid = 2130903042;
        public static final int grid_img = 2130903043;
        public static final int image = 2130903044;
        public static final int image_library = 2130903045;
        public static final int main = 2130903046;
        public static final int more_app = 2130903047;
    }

    public static final class menu {
        public static final int apk_menu = 2131099648;
        public static final int grid_menu = 2131099649;
        public static final int image_menu = 2131099650;
        public static final int local_menu = 2131099651;
        public static final int menu = 2131099652;
    }

    public static final class string {
        public static final int alertdialog_share = 2131034145;
        public static final int app_name = 2131034113;
        public static final int appurl = 2131034114;
        public static final int btn_cancel = 2131034119;
        public static final int btn_ok = 2131034127;
        public static final int btn_yes = 2131034118;
        public static final int hello = 2131034112;
        public static final int page_info_head = 2131034121;
        public static final int tip_loading = 2131034115;
        public static final int tip_set_shortcut = 2131034144;
        public static final int txt_album_save = 2131034136;
        public static final int txt_album_save_exist = 2131034137;
        public static final int txt_bookmark_title = 2131034124;
        public static final int txt_choice = 2131034148;
        public static final int txt_delbookmark_body = 2131034125;
        public static final int txt_deletealbum = 2131034122;
        public static final int txt_deletealbum_msg = 2131034123;
        public static final int txt_download_msg = 2131034128;
        public static final int txt_download_msg_cancel = 2131034130;
        public static final int txt_download_msg_error = 2131034129;
        public static final int txt_lastimg_body = 2131034141;
        public static final int txt_lastimg_title = 2131034140;
        public static final int txt_mailcontent = 2131034146;
        public static final int txt_mailsubject = 2131034147;
        public static final int txt_no_wifi_connection = 2131034132;
        public static final int txt_no_wifi_data = 2131034133;
        public static final int txt_quit_body = 2131034117;
        public static final int txt_quit_title = 2131034116;
        public static final int txt_refresh = 2131034120;
        public static final int txt_savecollection = 2131034134;
        public static final int txt_setWallpaper_body = 2131034139;
        public static final int txt_setWallpaper_title = 2131034138;
        public static final int txt_setwallpaper = 2131034135;
        public static final int txt_share = 2131034143;
        public static final int txt_shortcut = 2131034142;
        public static final int txt_wifi_none_title = 2131034131;
        public static final int webimgrooturlHtml = 2131034126;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {com.jcwp.Abeau.R.attr.adSize, com.jcwp.Abeau.R.attr.adUnitId, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
