package com.vpview.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BookMarkUtil {
    String strFilePath = "";
    String strKey = "";

    public BookMarkUtil(String strFilePath2, String strKey2) {
        this.strFilePath = strFilePath2;
        FileUtil.folderExistOrCreate(strFilePath2.substring(0, strFilePath2.lastIndexOf("/")));
        this.strKey = strKey2;
    }

    public Boolean add(String strContent) {
        GenUtil.systemPrintln("-----------strContent =" + strContent);
        if (keyExist(strContent).booleanValue()) {
            return false;
        }
        String strFileContent = getFileContentAll(this.strFilePath) + "<" + this.strKey + ">" + strContent + "</" + this.strKey + ">";
        GenUtil.systemPrintln("-----------strFileContent =" + strFileContent);
        GenUtil.systemPrintln("-------------strFilePath =" + this.strFilePath);
        buildXml(this.strFilePath, strFileContent);
        return true;
    }

    public Boolean add2Top(String strContent) {
        GenUtil.systemPrintln("-----------strContent =" + strContent);
        if (keyExist(strContent).booleanValue()) {
            return false;
        }
        String strFileContent = "<" + this.strKey + ">" + strContent + "</" + this.strKey + ">" + getFileContentAll(this.strFilePath);
        GenUtil.systemPrintln("-----------strFileContent =" + strFileContent);
        GenUtil.systemPrintln("-------------strFilePath =" + this.strFilePath);
        buildXml(this.strFilePath, strFileContent);
        return true;
    }

    public void delete(String strContent) {
        List<String> list = getElementList();
        String strFileContent = "";
        Boolean bDelete = false;
        for (int i = 0; i < list.size(); i++) {
            String strKeyTmp = list.get(i);
            if (!this.strKey.equals(strContent)) {
                strFileContent = strFileContent + "<" + this.strKey + ">" + strKeyTmp + "</" + this.strKey + ">";
            } else {
                bDelete = true;
            }
        }
        if (bDelete.booleanValue()) {
            buildXml(this.strFilePath, strFileContent);
        }
    }

    public void delete(int iPos) {
        List<String> list = getElementList();
        String strFileContent = "";
        Boolean bDelete = false;
        for (int i = 0; i < list.size(); i++) {
            String strKeyTmp = list.get(i);
            if (i != iPos) {
                strFileContent = strFileContent + "<" + this.strKey + ">" + strKeyTmp + "</" + this.strKey + ">";
            } else {
                bDelete = true;
            }
        }
        if (bDelete.booleanValue()) {
            buildXml(this.strFilePath, strFileContent);
        }
    }

    public List<String> getElementList() {
        GenUtil.systemPrintln("getElementList");
        return getElementListByName(this.strFilePath, this.strKey);
    }

    private Boolean keyExist(String strKey2) {
        GenUtil.systemPrintln("keyExist  strKey =" + strKey2);
        List<String> list = getElementList();
        GenUtil.systemPrintln("keyExist1  strKey =" + strKey2);
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals(strKey2)) {
                return true;
            }
        }
        return false;
    }

    private boolean buildXml(String xmlPath, String xml) {
        try {
            File myFilePath = new File(xmlPath.toString());
            if (!myFilePath.exists()) {
                myFilePath.createNewFile();
            }
            PrintWriter myFile = new PrintWriter(myFilePath, "UTF-8");
            myFile.println(xml);
            myFile.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private String getFileContentAll(String filePathAndName) {
        GenUtil.systemPrintln("getFileContentAll  filePathAndName =" + filePathAndName);
        if (!FileUtil.fileExist(filePathAndName)) {
            return "";
        }
        try {
            return getFileContent(filePathAndName);
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0055, code lost:
        r11 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0056, code lost:
        r0 = r1;
        r7 = r8;
        r5 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0063, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0064, code lost:
        r4 = r11;
        r0 = r1;
        r7 = r8;
        r5 = r6;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0055 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:8:0x001f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String getFileContent(java.lang.String r13) throws java.io.IOException {
        /*
            r12 = this;
            java.lang.StringBuffer r10 = new java.lang.StringBuffer
            java.lang.String r11 = ""
            r10.<init>(r11)
            r5 = 0
            r7 = 0
            r0 = 0
            java.lang.String r9 = ""
            java.io.FileInputStream r6 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0040 }
            r6.<init>(r13)     // Catch:{ IOException -> 0x0040 }
            java.io.InputStreamReader r8 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x005a, all -> 0x004e }
            java.lang.String r11 = "UTF-8"
            r8.<init>(r6, r11)     // Catch:{ IOException -> 0x005a, all -> 0x004e }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x005e, all -> 0x0051 }
            r1.<init>(r8)     // Catch:{ IOException -> 0x005e, all -> 0x0051 }
            java.lang.String r2 = ""
        L_0x001f:
            java.lang.String r2 = r1.readLine()     // Catch:{ IOException -> 0x0029, all -> 0x0055 }
            if (r2 == 0) goto L_0x0032
            r10.append(r2)     // Catch:{ IOException -> 0x0029, all -> 0x0055 }
            goto L_0x001f
        L_0x0029:
            r11 = move-exception
            r3 = r11
            java.lang.String r11 = r3.toString()     // Catch:{ IOException -> 0x0063, all -> 0x0055 }
            r10.append(r11)     // Catch:{ IOException -> 0x0063, all -> 0x0055 }
        L_0x0032:
            java.lang.String r9 = r10.toString()     // Catch:{ IOException -> 0x0063, all -> 0x0055 }
            r1.close()
            r8.close()
            r6.close()
            return r9
        L_0x0040:
            r11 = move-exception
            r4 = r11
        L_0x0042:
            throw r4     // Catch:{ all -> 0x0043 }
        L_0x0043:
            r11 = move-exception
        L_0x0044:
            r0.close()
            r7.close()
            r5.close()
            throw r11
        L_0x004e:
            r11 = move-exception
            r5 = r6
            goto L_0x0044
        L_0x0051:
            r11 = move-exception
            r7 = r8
            r5 = r6
            goto L_0x0044
        L_0x0055:
            r11 = move-exception
            r0 = r1
            r7 = r8
            r5 = r6
            goto L_0x0044
        L_0x005a:
            r11 = move-exception
            r4 = r11
            r5 = r6
            goto L_0x0042
        L_0x005e:
            r11 = move-exception
            r4 = r11
            r7 = r8
            r5 = r6
            goto L_0x0042
        L_0x0063:
            r11 = move-exception
            r4 = r11
            r0 = r1
            r7 = r8
            r5 = r6
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vpview.util.BookMarkUtil.getFileContent(java.lang.String):java.lang.String");
    }

    private List<String> getElementList(String content, String rootName) {
        List<String> list = new ArrayList<>();
        Matcher m = Pattern.compile("<" + rootName + ">(.*?)</" + rootName + ">").matcher(content);
        while (m.find()) {
            list.add(m.group(1).trim().replaceAll(" ", "%20"));
        }
        return list;
    }

    private List<String> getElementListByName(String fileName, String rootName) {
        List<String> list = new ArrayList<>();
        if (!FileUtil.fileExist(fileName)) {
            return list;
        }
        try {
            return getElementList(getFileContent(fileName), rootName);
        } catch (IOException e) {
            e.printStackTrace();
            FileUtil.delFile(fileName);
            return list;
        }
    }
}
