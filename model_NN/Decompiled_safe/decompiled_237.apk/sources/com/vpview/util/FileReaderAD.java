package com.vpview.util;

import android.app.Activity;
import android.content.res.AssetManager;
import java.io.BufferedReader;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class FileReaderAD {
    private static long lFileLength = -1;
    private static long[] lFileSizeStep = null;
    private Activity activity;
    private AssetManager assetManager;
    private boolean bEncoded = true;
    private boolean bFirst = false;
    private boolean bLast = false;
    private boolean bValid = false;
    private byte[] btPage = null;
    private String filePath = "";
    private long iCurrPosition = 0;
    private int iOnePage = 100;
    private int iStep = 0;
    private InputStream is = null;
    private InputStreamReader isr = null;
    private String strEncode = "";
    private String strFileName = "";

    public FileReaderAD(Activity activity2, String fileName, int iOnePage2, String encode, boolean bEnc) {
        this.activity = activity2;
        this.assetManager = this.activity.getAssets();
        this.strEncode = encode;
        this.strFileName = fileName;
        this.iOnePage = iOnePage2;
        this.btPage = new byte[iOnePage2];
        this.bFirst = true;
        this.iCurrPosition = 0;
        this.bValid = init();
        this.bEncoded = bEnc;
    }

    private boolean init() {
        GenUtil.printTime("before ");
        getAllIndex();
        GenUtil.printTime("after ");
        setIsr(0);
        return true;
    }

    public long getSize() {
        return lFileLength;
    }

    public String getNextPage() {
        String strPageOut;
        try {
            setIsr(this.iCurrPosition);
            int ipage = this.is.read(this.btPage);
            if (ipage < this.iOnePage) {
                byte[] btTmp = new byte[ipage];
                for (int i = 0; i < ipage; i++) {
                    btTmp[i] = this.btPage[i];
                }
                GenUtil.systemPrintln("want to print...");
                if ((btTmp[ipage - 2] & 224) == 224 && (btTmp[ipage - 1] & 128) == 128) {
                    btTmp[ipage - 2] = 0;
                    btTmp[ipage - 1] = 0;
                    ipage -= 2;
                    GenUtil.systemPrintln("ipage-2");
                } else if ((btTmp[ipage - 1] & 224) == 224) {
                    GenUtil.systemPrintln("utf8");
                    btTmp[ipage - 1] = 0;
                    ipage--;
                    GenUtil.systemPrintln("ipage-1");
                }
                if (this.bEncoded) {
                    strPageOut = new String(XMEnDecrypt.XMDecryptString(btTmp), 0, ipage, this.strEncode);
                } else {
                    strPageOut = new String(btTmp, this.strEncode);
                }
            } else if (this.bEncoded) {
                byte[] btBuffers = XMEnDecrypt.XMDecryptString(this.btPage);
                if ((btBuffers[this.iOnePage - 2] & 224) == 224 && (btBuffers[this.iOnePage - 1] & 128) == 128) {
                    btBuffers[this.iOnePage - 2] = 0;
                    btBuffers[this.iOnePage - 1] = 0;
                    ipage -= 2;
                    GenUtil.systemPrintln("ipage-2");
                }
                if ((btBuffers[this.iOnePage - 1] & 224) == 224) {
                    GenUtil.systemPrintln("utf8");
                    btBuffers[this.iOnePage - 1] = 0;
                    ipage--;
                    GenUtil.systemPrintln("ipage-1");
                }
                strPageOut = new String(btBuffers, 0, ipage, this.strEncode);
            } else {
                strPageOut = new String(this.btPage, this.strEncode);
            }
            GenUtil.systemPrintln("iCurrPosition = " + this.iCurrPosition);
            GenUtil.systemPrintln("ipage = " + ipage);
            GenUtil.systemPrintln("iStep = " + this.iStep);
            this.iCurrPosition += (long) ipage;
            GenUtil.systemPrintln("lFileSizeStep[iStep] = " + lFileSizeStep[this.iStep]);
            GenUtil.systemPrintln("iCurrPosition = " + this.iCurrPosition);
            GenUtil.printFile("\\sdcard\\log.txt", strPageOut);
            this.bFirst = false;
            this.bLast = false;
            if (this.iCurrPosition == ((long) ipage)) {
                this.bFirst = true;
            } else if (this.iCurrPosition >= lFileLength) {
                this.bLast = true;
            }
            if (this.iCurrPosition == lFileSizeStep[this.iStep]) {
                setIsr(this.iCurrPosition);
            }
            return strPageOut;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getPrePage() {
        long lPosition = this.iCurrPosition - ((long) this.iOnePage);
        GenUtil.systemPrintln("iCurrPosition = " + this.iCurrPosition);
        GenUtil.systemPrintln("iOnePage = " + this.iOnePage);
        GenUtil.systemPrintln("lPosition = " + lPosition);
        return getPage(lPosition);
    }

    public String getPage(long lPosition) {
        setIsr(lPosition);
        return getNextPage();
    }

    public int getPageStatus() {
        if (this.bLast) {
            return 1;
        }
        if (this.bFirst) {
            return 2;
        }
        return 0;
    }

    public boolean getStatus() {
        return this.bValid;
    }

    public long getCurrPosition() {
        return this.iCurrPosition;
    }

    private String readTextFile(String fileNameAsset, String encode) {
        String outString = "";
        try {
            InputStream inputStream = this.assetManager.open(fileNameAsset);
            StringBuilder sb = new StringBuilder();
            byte[] myByte = new byte[2000];
            while (inputStream.read(myByte) != -1) {
                myByte = XMEnDecrypt.XMDecryptString(myByte);
                sb.append(new String(myByte, encode));
            }
            outString = sb.toString();
            inputStream.close();
            return outString;
        } catch (IOException e) {
            GenUtil.print("startSocketMonitor", e.getMessage());
            return outString;
        }
    }

    private String readTextFileChar(String fileNameAsset, String encode) {
        IOException e;
        InputStreamReader isr2;
        String outString = "";
        try {
            InputStream inputStream = this.assetManager.open(fileNameAsset);
            if (encode.equals("")) {
                isr2 = new InputStreamReader(inputStream);
            } else {
                isr2 = new InputStreamReader(inputStream, encode);
            }
            BufferedReader bReader = new BufferedReader(isr2);
            try {
                CharArrayWriter outArrayWriter = new CharArrayWriter();
                char[] myChar = new char[2000];
                while (true) {
                    int len = bReader.read(myChar);
                    if (len == -1) {
                        break;
                    }
                    outArrayWriter.write(myChar, 0, len);
                }
                outString = String.valueOf(outArrayWriter.toCharArray());
                outArrayWriter.close();
                bReader.close();
            } catch (IOException e2) {
                e = e2;
                GenUtil.print("startSocketMonitor", e.getMessage());
                return outString;
            }
        } catch (IOException e3) {
            e = e3;
            GenUtil.print("startSocketMonitor", e.getMessage());
            return outString;
        }
        return outString;
    }

    public void getAllIndex() {
        if (lFileLength <= -1) {
            String strContent = readTextFile("index.txt", "utf-8");
            GenUtil.systemPrintln("strContent = " + strContent);
            List listFName = FileMatch.matchRootList(strContent, "fname");
            GenUtil.systemPrintln("listFName = " + listFName.size());
            lFileSizeStep = new long[listFName.size()];
            lFileLength = 0;
            for (int i = 0; i < listFName.size(); i++) {
                GenUtil.systemPrintln("i = " + i + " lFileSizeStep = " + lFileSizeStep[i]);
                lFileLength += getAssetFileLength(listFName.get(i), "utf-8");
                lFileSizeStep[i] = lFileLength;
                GenUtil.systemPrintln("i = " + i + " lFileSizeStep = " + lFileSizeStep[i]);
            }
        }
    }

    public String getMatch(String strInput, String strMatcher) {
        String preMatcher = "<" + strMatcher + ">";
        int iPre = strInput.indexOf(preMatcher) + preMatcher.length();
        int iPost = strInput.indexOf("</" + strMatcher + ">");
        if (iPre < 0 || iPost < 0) {
            return "";
        }
        return strInput.substring(iPre, iPost);
    }

    private boolean setIsr(long lPosition) {
        long lskip;
        if (lPosition < 0) {
            this.iCurrPosition = 0;
        } else {
            this.iCurrPosition = lPosition;
        }
        this.iStep = getStep(lPosition);
        String strFileNameTmp = this.strFileName + (this.iStep + 1) + ".txt";
        GenUtil.systemPrintln("iCurrPosition = " + this.iCurrPosition);
        GenUtil.systemPrintln("file being set: strFileNameTmp = " + strFileNameTmp);
        try {
            this.is = this.assetManager.open(strFileNameTmp);
            if (this.iStep == 0) {
                lskip = this.iCurrPosition;
            } else {
                lskip = this.iCurrPosition - lFileSizeStep[this.iStep - 1];
            }
            try {
                this.is.skip(lskip);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        } catch (IOException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    private int getStep(long lposition) {
        for (int i = 0; i < lFileSizeStep.length; i++) {
            if (lFileSizeStep[i] > lposition) {
                return i;
            }
        }
        return 0;
    }

    public long getAssetFileLength(String fName, String encoding) {
        try {
            try {
                return this.assetManager.open(fName).skip(10000000000L);
            } catch (IOException e) {
                e.printStackTrace();
                return -3;
            }
        } catch (IOException e2) {
            e2.printStackTrace();
            return -1;
        }
    }
}
