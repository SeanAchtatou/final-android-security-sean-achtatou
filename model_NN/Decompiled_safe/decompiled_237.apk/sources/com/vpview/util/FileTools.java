package com.vpview.util;

import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class FileTools {
    private static String strDefaultKey = "yeecaizhuazhua9618";
    private Cipher decryptCipher;
    private Cipher encryptCipher;

    public static String getFolderBySuffix(String suffix) {
        if ("txt".equalsIgnoreCase(suffix)) {
            return Folder.BOOK;
        }
        if (suffix.equalsIgnoreCase("jpg") || suffix.equalsIgnoreCase("gif") || suffix.equalsIgnoreCase("png") || suffix.equalsIgnoreCase("jpeg") || suffix.equalsIgnoreCase("bmp")) {
            return Folder.IMAGE;
        }
        if (suffix.equalsIgnoreCase("m4a") || suffix.equalsIgnoreCase("mp3") || suffix.equalsIgnoreCase("mid") || suffix.equalsIgnoreCase("xmf") || suffix.equalsIgnoreCase("ogg") || suffix.equalsIgnoreCase("wav")) {
            return Folder.AUDIO;
        }
        if (suffix.equalsIgnoreCase("mpeg") || suffix.equalsIgnoreCase("3gp") || suffix.equalsIgnoreCase("wmv") || suffix.equalsIgnoreCase("avi") || suffix.equalsIgnoreCase("mp4") || suffix.equalsIgnoreCase("flv") || suffix.equalsIgnoreCase("rm") || suffix.equalsIgnoreCase("rmvb") || suffix.equalsIgnoreCase("dat")) {
            return Folder.VIDEO;
        }
        if (suffix.equalsIgnoreCase("apk")) {
            return Folder.SOFT;
        }
        if (suffix.equalsIgnoreCase("ywz")) {
            return Folder.URL;
        }
        return Folder.OTHER;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v4, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String byteArr2HexStr(byte[] r6) throws java.lang.Exception {
        /*
            r5 = 16
            int r1 = r6.length
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            int r4 = r1 * 2
            r3.<init>(r4)
            r0 = 0
        L_0x000b:
            if (r0 >= r1) goto L_0x0025
            byte r2 = r6[r0]
        L_0x000f:
            if (r2 >= 0) goto L_0x0014
            int r2 = r2 + 256
            goto L_0x000f
        L_0x0014:
            if (r2 >= r5) goto L_0x001b
            java.lang.String r4 = "0"
            r3.append(r4)
        L_0x001b:
            java.lang.String r4 = java.lang.Integer.toString(r2, r5)
            r3.append(r4)
            int r0 = r0 + 1
            goto L_0x000b
        L_0x0025:
            java.lang.String r4 = r3.toString()
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vpview.util.FileTools.byteArr2HexStr(byte[]):java.lang.String");
    }

    public static byte[] hexStr2ByteArr(String strIn) throws Exception {
        byte[] arrB = strIn.getBytes();
        int iLen = arrB.length;
        byte[] arrOut = new byte[(iLen / 2)];
        for (int i = 0; i < iLen; i += 2) {
            arrOut[i / 2] = (byte) Integer.parseInt(new String(arrB, i, 2), 16);
        }
        return arrOut;
    }

    public FileTools() throws Exception {
        this(strDefaultKey);
    }

    public FileTools(String strKey) throws Exception {
        this.encryptCipher = null;
        this.decryptCipher = null;
        Key key = getKey(strKey.getBytes());
        this.encryptCipher = Cipher.getInstance("DES");
        this.encryptCipher.init(1, key);
        this.decryptCipher = Cipher.getInstance("DES");
        this.decryptCipher.init(2, key);
    }

    public byte[] encrypt(byte[] arrB) throws Exception {
        return this.encryptCipher.doFinal(arrB);
    }

    public String encrypt(String strIn) throws Exception {
        return byteArr2HexStr(encrypt(strIn.getBytes()));
    }

    public byte[] decrypt(byte[] arrB) throws Exception {
        return this.decryptCipher.doFinal(arrB);
    }

    public String decrypt(String strIn) throws Exception {
        return new String(decrypt(hexStr2ByteArr(strIn)));
    }

    private Key getKey(byte[] arrBTmp) throws Exception {
        byte[] arrB = new byte[8];
        int i = 0;
        while (i < arrBTmp.length && i < arrB.length) {
            arrB[i] = arrBTmp[i];
            i++;
        }
        return new SecretKeySpec(arrB, "DES");
    }
}
