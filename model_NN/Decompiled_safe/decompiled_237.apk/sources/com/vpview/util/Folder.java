package com.vpview.util;

public class Folder {
    public static final String AUDIO = "Audio";
    public static final String BOOK = "Book";
    public static final String IMAGE = "Image";
    public static final String MYBOOKMARK = "/sdcard/WPViewernew/mycollection/bookmark.xml";
    public static final String MYCACHE = "/sdcard/WPViewernew/cache/";
    public static final String MYCOLLECTION = "/sdcard/WPViewernew/mycollection/";
    public static final String MYIMAGE = "/sdcard/WPViewernew/myimage/";
    public static final String MYSAVED = "My saved";
    public static final String OTHER = "Other";
    public static final String POPIMG = "/sdcard/WPViewernew/";
    public static final String SOFT = "Soft";
    public static final String TMP = "Tmp";
    public static final String URL = "Url";
    public static final String USER = "user";
    public static final String VIDEO = "Video";
    public static final String WEB = "Web";
}
