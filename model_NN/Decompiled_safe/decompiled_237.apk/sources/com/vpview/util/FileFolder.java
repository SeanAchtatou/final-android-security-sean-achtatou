package com.vpview.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileFolder {
    File[] contents;
    File currentFile;
    List<String> listFolder = new ArrayList();
    File myDir;
    String path;

    public FileFolder(String path2) {
        this.path = path2;
        makelist(new File(path2));
    }

    public List<String> getFoldList() {
        return this.listFolder;
    }

    private void makelist(File fDir) {
        if (fDir.isDirectory()) {
            File[] filelist = fDir.listFiles();
            for (File f : filelist) {
                if (f.isDirectory()) {
                    if (!lastFold(f).booleanValue()) {
                        makelist(f);
                    } else {
                        this.listFolder.add(f.getAbsolutePath() + "/");
                        GenUtil.systemPrintln("f.getAbsolutePath() = " + f.getAbsolutePath());
                    }
                }
            }
        }
    }

    public Boolean lastFold(File fDir) {
        File[] filelist = fDir.listFiles();
        int iCount = 0;
        for (File f : filelist) {
            if (f.isDirectory()) {
                iCount++;
            }
        }
        if (iCount > 0) {
            return false;
        }
        return true;
    }
}
