package com.vpview.util;

import android.app.Activity;
import android.content.res.AssetManager;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class AssetUtil {
    private Activity activity;

    public AssetUtil(Activity activity2) {
        this.activity = activity2;
    }

    public String readTextFile(String fileNameAsset, String encode, boolean bEnc) {
        AssetManager assetManager = this.activity.getAssets();
        StringBuilder sb = new StringBuilder();
        try {
            InputStream inputStream = assetManager.open(fileNameAsset);
            byte[] btBuffer = new byte[2000];
            while (inputStream.read(btBuffer) != -1) {
                if (bEnc) {
                    sb.append(new String(XMEnDecrypt.XMDecryptString(btBuffer)));
                } else {
                    sb.append(new String(btBuffer));
                }
            }
            inputStream.close();
        } catch (IOException e) {
            GenUtil.print("startSocketMonitor", e.getMessage());
        }
        return sb.toString();
    }

    public List<String> getSplitsFilesInDir(String strDir) throws IOException {
        UnsupportedEncodingException e;
        List<String> lstFiles = null;
        InputStream finstream = this.activity.getAssets().open("index.txt");
        if (finstream != null) {
            int iLen = finstream.available();
            byte[] btBuffer = new byte[iLen];
            if (finstream.read(btBuffer) == iLen) {
                try {
                    String strContent = new String(XMEnDecrypt.XMDecryptString(btBuffer), "utf-8");
                    try {
                        GenUtil.systemPrintln(strContent);
                        lstFiles = FileMatch.matchRootListNoencode(strContent, "fname");
                    } catch (UnsupportedEncodingException e2) {
                        e = e2;
                    }
                } catch (UnsupportedEncodingException e3) {
                    e = e3;
                    e.printStackTrace();
                    finstream.close();
                    return lstFiles;
                }
            }
            try {
                finstream.close();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
        }
        return lstFiles;
    }

    /* JADX WARNING: Removed duplicated region for block: B:100:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003a A[SYNTHETIC, Splitter:B:19:0x003a] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x003f A[SYNTHETIC, Splitter:B:22:0x003f] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0044 A[SYNTHETIC, Splitter:B:25:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0049 A[SYNTHETIC, Splitter:B:28:0x0049] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x005c A[SYNTHETIC, Splitter:B:35:0x005c] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0061 A[SYNTHETIC, Splitter:B:38:0x0061] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0066 A[SYNTHETIC, Splitter:B:41:0x0066] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x006b A[SYNTHETIC, Splitter:B:44:0x006b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void releaseAssetRes(java.lang.String r13, java.lang.String r14, boolean r15) {
        /*
            r12 = this;
            r7 = 0
            r8 = 0
            r0 = 0
            r2 = 0
            android.app.Activity r10 = r12.activity     // Catch:{ IOException -> 0x00cc }
            android.content.res.AssetManager r10 = r10.getAssets()     // Catch:{ IOException -> 0x00cc }
            java.io.InputStream r7 = r10.open(r13)     // Catch:{ IOException -> 0x00cc }
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x00cc }
            r1.<init>(r7)     // Catch:{ IOException -> 0x00cc }
            java.io.FileOutputStream r9 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x00d0, all -> 0x00c5 }
            r9.<init>(r14)     // Catch:{ IOException -> 0x00d0, all -> 0x00c5 }
            java.io.BufferedOutputStream r3 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x00d5, all -> 0x00c8 }
            r3.<init>(r9)     // Catch:{ IOException -> 0x00d5, all -> 0x00c8 }
            r10 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r10]     // Catch:{ IOException -> 0x0030, all -> 0x0056 }
            r6 = 0
        L_0x0022:
            int r6 = r1.read(r4)     // Catch:{ IOException -> 0x0030, all -> 0x0056 }
            r10 = -1
            if (r6 == r10) goto L_0x006f
            if (r15 == 0) goto L_0x004d
            r10 = 0
            r3.write(r4, r10, r6)     // Catch:{ IOException -> 0x0030, all -> 0x0056 }
            goto L_0x0022
        L_0x0030:
            r10 = move-exception
            r5 = r10
            r2 = r3
            r0 = r1
            r8 = r9
        L_0x0035:
            r5.printStackTrace()     // Catch:{ all -> 0x00c3 }
            if (r0 == 0) goto L_0x003d
            r0.close()     // Catch:{ IOException -> 0x009b }
        L_0x003d:
            if (r7 == 0) goto L_0x0042
            r7.close()     // Catch:{ IOException -> 0x00a0 }
        L_0x0042:
            if (r2 == 0) goto L_0x0047
            r2.close()     // Catch:{ IOException -> 0x00a5 }
        L_0x0047:
            if (r8 == 0) goto L_0x004c
            r8.close()     // Catch:{ IOException -> 0x00aa }
        L_0x004c:
            return
        L_0x004d:
            byte[] r10 = com.vpview.util.XMEnDecrypt.XMDecryptString(r4)     // Catch:{ IOException -> 0x0030, all -> 0x0056 }
            r11 = 0
            r3.write(r10, r11, r6)     // Catch:{ IOException -> 0x0030, all -> 0x0056 }
            goto L_0x0022
        L_0x0056:
            r10 = move-exception
            r2 = r3
            r0 = r1
            r8 = r9
        L_0x005a:
            if (r0 == 0) goto L_0x005f
            r0.close()     // Catch:{ IOException -> 0x0087 }
        L_0x005f:
            if (r7 == 0) goto L_0x0064
            r7.close()     // Catch:{ IOException -> 0x008c }
        L_0x0064:
            if (r2 == 0) goto L_0x0069
            r2.close()     // Catch:{ IOException -> 0x0091 }
        L_0x0069:
            if (r8 == 0) goto L_0x006e
            r8.close()     // Catch:{ IOException -> 0x0096 }
        L_0x006e:
            throw r10
        L_0x006f:
            if (r1 == 0) goto L_0x0074
            r1.close()     // Catch:{ IOException -> 0x00af }
        L_0x0074:
            if (r7 == 0) goto L_0x0079
            r7.close()     // Catch:{ IOException -> 0x00b4 }
        L_0x0079:
            if (r3 == 0) goto L_0x007e
            r3.close()     // Catch:{ IOException -> 0x00b9 }
        L_0x007e:
            if (r9 == 0) goto L_0x0083
            r9.close()     // Catch:{ IOException -> 0x00be }
        L_0x0083:
            r2 = r3
            r0 = r1
            r8 = r9
            goto L_0x004c
        L_0x0087:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x005f
        L_0x008c:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x0064
        L_0x0091:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x0069
        L_0x0096:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x006e
        L_0x009b:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x003d
        L_0x00a0:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x0042
        L_0x00a5:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x0047
        L_0x00aa:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x004c
        L_0x00af:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x0074
        L_0x00b4:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x0079
        L_0x00b9:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x007e
        L_0x00be:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x0083
        L_0x00c3:
            r10 = move-exception
            goto L_0x005a
        L_0x00c5:
            r10 = move-exception
            r0 = r1
            goto L_0x005a
        L_0x00c8:
            r10 = move-exception
            r0 = r1
            r8 = r9
            goto L_0x005a
        L_0x00cc:
            r10 = move-exception
            r5 = r10
            goto L_0x0035
        L_0x00d0:
            r10 = move-exception
            r5 = r10
            r0 = r1
            goto L_0x0035
        L_0x00d5:
            r10 = move-exception
            r5 = r10
            r0 = r1
            r8 = r9
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vpview.util.AssetUtil.releaseAssetRes(java.lang.String, java.lang.String, boolean):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0044 A[SYNTHETIC, Splitter:B:20:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0049 A[SYNTHETIC, Splitter:B:23:0x0049] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x004e A[SYNTHETIC, Splitter:B:26:0x004e] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0053 A[SYNTHETIC, Splitter:B:29:0x0053] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0066 A[SYNTHETIC, Splitter:B:37:0x0066] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x006b A[SYNTHETIC, Splitter:B:40:0x006b] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0070 A[SYNTHETIC, Splitter:B:43:0x0070] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0075 A[SYNTHETIC, Splitter:B:46:0x0075] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Boolean releaseAssetResRet(java.lang.String r14, java.lang.String r15, boolean r16) {
        /*
            r13 = this;
            r11 = 1
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r11)
            r8 = 0
            r9 = 0
            r0 = 0
            r2 = 0
            android.app.Activity r11 = r13.activity     // Catch:{ IOException -> 0x00d6 }
            android.content.res.AssetManager r11 = r11.getAssets()     // Catch:{ IOException -> 0x00d6 }
            java.io.InputStream r8 = r11.open(r14)     // Catch:{ IOException -> 0x00d6 }
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x00d6 }
            r1.<init>(r8)     // Catch:{ IOException -> 0x00d6 }
            java.io.FileOutputStream r10 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x00da, all -> 0x00cf }
            r10.<init>(r15)     // Catch:{ IOException -> 0x00da, all -> 0x00cf }
            java.io.BufferedOutputStream r3 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x00df, all -> 0x00d2 }
            r3.<init>(r10)     // Catch:{ IOException -> 0x00df, all -> 0x00d2 }
            r11 = 1024(0x400, float:1.435E-42)
            byte[] r5 = new byte[r11]     // Catch:{ IOException -> 0x0035, all -> 0x0060 }
            r7 = 0
        L_0x0027:
            int r7 = r1.read(r5)     // Catch:{ IOException -> 0x0035, all -> 0x0060 }
            r11 = -1
            if (r7 == r11) goto L_0x0079
            if (r16 == 0) goto L_0x0057
            r11 = 0
            r3.write(r5, r11, r7)     // Catch:{ IOException -> 0x0035, all -> 0x0060 }
            goto L_0x0027
        L_0x0035:
            r11 = move-exception
            r6 = r11
            r2 = r3
            r0 = r1
            r9 = r10
        L_0x003a:
            r11 = 0
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r11)     // Catch:{ all -> 0x00cd }
            r6.printStackTrace()     // Catch:{ all -> 0x00cd }
            if (r0 == 0) goto L_0x0047
            r0.close()     // Catch:{ IOException -> 0x00a5 }
        L_0x0047:
            if (r8 == 0) goto L_0x004c
            r8.close()     // Catch:{ IOException -> 0x00aa }
        L_0x004c:
            if (r2 == 0) goto L_0x0051
            r2.close()     // Catch:{ IOException -> 0x00af }
        L_0x0051:
            if (r9 == 0) goto L_0x0056
            r9.close()     // Catch:{ IOException -> 0x00b4 }
        L_0x0056:
            return r4
        L_0x0057:
            byte[] r11 = com.vpview.util.XMEnDecrypt.XMDecryptString(r5)     // Catch:{ IOException -> 0x0035, all -> 0x0060 }
            r12 = 0
            r3.write(r11, r12, r7)     // Catch:{ IOException -> 0x0035, all -> 0x0060 }
            goto L_0x0027
        L_0x0060:
            r11 = move-exception
            r2 = r3
            r0 = r1
            r9 = r10
        L_0x0064:
            if (r0 == 0) goto L_0x0069
            r0.close()     // Catch:{ IOException -> 0x0091 }
        L_0x0069:
            if (r8 == 0) goto L_0x006e
            r8.close()     // Catch:{ IOException -> 0x0096 }
        L_0x006e:
            if (r2 == 0) goto L_0x0073
            r2.close()     // Catch:{ IOException -> 0x009b }
        L_0x0073:
            if (r9 == 0) goto L_0x0078
            r9.close()     // Catch:{ IOException -> 0x00a0 }
        L_0x0078:
            throw r11
        L_0x0079:
            if (r1 == 0) goto L_0x007e
            r1.close()     // Catch:{ IOException -> 0x00b9 }
        L_0x007e:
            if (r8 == 0) goto L_0x0083
            r8.close()     // Catch:{ IOException -> 0x00be }
        L_0x0083:
            if (r3 == 0) goto L_0x0088
            r3.close()     // Catch:{ IOException -> 0x00c3 }
        L_0x0088:
            if (r10 == 0) goto L_0x008d
            r10.close()     // Catch:{ IOException -> 0x00c8 }
        L_0x008d:
            r2 = r3
            r0 = r1
            r9 = r10
            goto L_0x0056
        L_0x0091:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x0069
        L_0x0096:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x006e
        L_0x009b:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x0073
        L_0x00a0:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x0078
        L_0x00a5:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x0047
        L_0x00aa:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x004c
        L_0x00af:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x0051
        L_0x00b4:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x0056
        L_0x00b9:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x007e
        L_0x00be:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x0083
        L_0x00c3:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x0088
        L_0x00c8:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x008d
        L_0x00cd:
            r11 = move-exception
            goto L_0x0064
        L_0x00cf:
            r11 = move-exception
            r0 = r1
            goto L_0x0064
        L_0x00d2:
            r11 = move-exception
            r0 = r1
            r9 = r10
            goto L_0x0064
        L_0x00d6:
            r11 = move-exception
            r6 = r11
            goto L_0x003a
        L_0x00da:
            r11 = move-exception
            r6 = r11
            r0 = r1
            goto L_0x003a
        L_0x00df:
            r11 = move-exception
            r6 = r11
            r0 = r1
            r9 = r10
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vpview.util.AssetUtil.releaseAssetResRet(java.lang.String, java.lang.String, boolean):java.lang.Boolean");
    }
}
