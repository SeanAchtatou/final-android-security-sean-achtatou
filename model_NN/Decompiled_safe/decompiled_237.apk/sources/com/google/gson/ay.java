package com.google.gson;

final class ay implements ExclusionStrategy {
    private final boolean a = true;

    ay() {
    }

    public final boolean shouldSkipClass(Class<?> cls) {
        return false;
    }

    public final boolean shouldSkipField(FieldAttributes f) {
        return this.a && f.a();
    }
}
