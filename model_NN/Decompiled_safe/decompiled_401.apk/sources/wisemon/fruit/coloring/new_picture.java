package wisemon.fruit.coloring;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;

public class new_picture extends Activity {
    private Gallery gallery;
    /* access modifiers changed from: private */
    public ImageView imgView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.new_picture);
        this.imgView = (ImageView) findViewById(R.id.GalleryImageView);
        this.imgView.setImageResource(MainColoring.Imgid[0].intValue());
        MainColoring.SetActiveImage(0);
        this.gallery = (Gallery) findViewById(R.id.imagegallery);
        this.gallery.setAdapter((SpinnerAdapter) new AddImgAdp(this));
        this.gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                if (position <= MainColoring.Imgid.length) {
                    new_picture.this.imgView.setImageResource(MainColoring.Imgid[position].intValue());
                    MainColoring.SetActiveImage(position);
                }
            }
        });
        this.imgView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.AlreadyTouched = false;
                new_picture.this.finish();
            }
        });
    }

    public class AddImgAdp extends BaseAdapter {
        int GalItemBg;
        private Context cont;

        public AddImgAdp(Context c) {
            this.cont = c;
            TypedArray typArray = new_picture.this.obtainStyledAttributes(R.styleable.GalleryTheme);
            this.GalItemBg = typArray.getResourceId(0, 0);
            typArray.recycle();
        }

        public int getCount() {
            return MainColoring.Imgid.length;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imgView = new ImageView(this.cont);
            imgView.setImageResource(MainColoring.Imgid[position].intValue());
            imgView.setLayoutParams(new Gallery.LayoutParams(80, 70));
            imgView.setScaleType(ImageView.ScaleType.FIT_XY);
            imgView.setBackgroundResource(this.GalItemBg);
            return imgView;
        }
    }
}
