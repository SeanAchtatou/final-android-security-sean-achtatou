package org.jsoup.select;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Element;

abstract class CombiningEvaluator extends Evaluator {
    final List<Evaluator> evaluators;

    CombiningEvaluator() {
        this.evaluators = new ArrayList();
    }

    CombiningEvaluator(Collection<Evaluator> evaluators2) {
        this();
        this.evaluators.addAll(evaluators2);
    }

    static final class And extends CombiningEvaluator {
        And(Collection<Evaluator> evaluators) {
            super(evaluators);
        }

        And(Evaluator... evaluators) {
            this(Arrays.asList(evaluators));
        }

        public boolean matches(Element root, Element node) {
            for (Evaluator s : this.evaluators) {
                if (!s.matches(root, node)) {
                    return false;
                }
            }
            return true;
        }

        public String toString() {
            return StringUtil.join(this.evaluators, " ");
        }
    }

    static final class Or extends CombiningEvaluator {
        Or(Collection<Evaluator> evaluators) {
            if (evaluators.size() > 1) {
                this.evaluators.add(new And(evaluators));
            } else {
                this.evaluators.addAll(evaluators);
            }
        }

        public void add(Evaluator e) {
            this.evaluators.add(e);
        }

        public boolean matches(Element root, Element node) {
            for (Evaluator s : this.evaluators) {
                if (s.matches(root, node)) {
                    return true;
                }
            }
            return false;
        }

        public String toString() {
            return String.format(":or%s", this.evaluators);
        }
    }
}
