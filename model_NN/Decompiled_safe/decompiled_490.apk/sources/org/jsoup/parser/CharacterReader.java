package org.jsoup.parser;

import org.jsoup.helper.Validate;

class CharacterReader {
    static final char EOF = '￿';
    private final String input;
    private final int length;
    private int mark = 0;
    private int pos = 0;

    CharacterReader(String input2) {
        Validate.notNull(input2);
        String input3 = input2.replaceAll("\r\n?", "\n");
        this.input = input3;
        this.length = input3.length();
    }

    /* access modifiers changed from: package-private */
    public int pos() {
        return this.pos;
    }

    /* access modifiers changed from: package-private */
    public boolean isEmpty() {
        return this.pos >= this.length;
    }

    /* access modifiers changed from: package-private */
    public char current() {
        return isEmpty() ? EOF : this.input.charAt(this.pos);
    }

    /* access modifiers changed from: package-private */
    public char consume() {
        char val = isEmpty() ? 65535 : this.input.charAt(this.pos);
        this.pos++;
        return val;
    }

    /* access modifiers changed from: package-private */
    public void unconsume() {
        this.pos--;
    }

    /* access modifiers changed from: package-private */
    public void advance() {
        this.pos++;
    }

    /* access modifiers changed from: package-private */
    public void mark() {
        this.mark = this.pos;
    }

    /* access modifiers changed from: package-private */
    public void rewindToMark() {
        this.pos = this.mark;
    }

    /* access modifiers changed from: package-private */
    public String consumeAsString() {
        String str = this.input;
        int i = this.pos;
        int i2 = this.pos;
        this.pos = i2 + 1;
        return str.substring(i, i2);
    }

    /* access modifiers changed from: package-private */
    public String consumeTo(char c) {
        int offset = this.input.indexOf(c, this.pos);
        if (offset == -1) {
            return consumeToEnd();
        }
        String consumed = this.input.substring(this.pos, offset);
        this.pos += consumed.length();
        return consumed;
    }

    /* access modifiers changed from: package-private */
    public String consumeTo(String seq) {
        int offset = this.input.indexOf(seq, this.pos);
        if (offset == -1) {
            return consumeToEnd();
        }
        String consumed = this.input.substring(this.pos, offset);
        this.pos += consumed.length();
        return consumed;
    }

    /* access modifiers changed from: package-private */
    public String consumeToAny(char... seq) {
        int start = this.pos;
        loop0:
        while (!isEmpty()) {
            char c = this.input.charAt(this.pos);
            for (char seek : seq) {
                if (seek == c) {
                    break loop0;
                }
            }
            this.pos++;
        }
        if (this.pos > start) {
            return this.input.substring(start, this.pos);
        }
        return "";
    }

    /* access modifiers changed from: package-private */
    public String consumeToEnd() {
        String data = this.input.substring(this.pos, this.input.length());
        this.pos = this.input.length();
        return data;
    }

    /* access modifiers changed from: package-private */
    public String consumeLetterSequence() {
        int start = this.pos;
        while (!isEmpty() && (((c = this.input.charAt(this.pos)) >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))) {
            this.pos++;
        }
        return this.input.substring(start, this.pos);
    }

    /* access modifiers changed from: package-private */
    public String consumeHexSequence() {
        int start = this.pos;
        while (!isEmpty() && (((c = this.input.charAt(this.pos)) >= '0' && c <= '9') || ((c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f')))) {
            this.pos++;
        }
        return this.input.substring(start, this.pos);
    }

    /* access modifiers changed from: package-private */
    public String consumeDigitSequence() {
        int start = this.pos;
        while (!isEmpty() && (c = this.input.charAt(this.pos)) >= '0' && c <= '9') {
            this.pos++;
        }
        return this.input.substring(start, this.pos);
    }

    /* access modifiers changed from: package-private */
    public boolean matches(char c) {
        return !isEmpty() && this.input.charAt(this.pos) == c;
    }

    /* access modifiers changed from: package-private */
    public boolean matches(String seq) {
        return this.input.startsWith(seq, this.pos);
    }

    /* access modifiers changed from: package-private */
    public boolean matchesIgnoreCase(String seq) {
        return this.input.regionMatches(true, this.pos, seq, 0, seq.length());
    }

    /* access modifiers changed from: package-private */
    public boolean matchesAny(char... seq) {
        if (isEmpty()) {
            return false;
        }
        char c = this.input.charAt(this.pos);
        for (char seek : seq) {
            if (seek == c) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean matchesLetter() {
        if (isEmpty()) {
            return false;
        }
        char c = this.input.charAt(this.pos);
        return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
    }

    /* access modifiers changed from: package-private */
    public boolean matchesDigit() {
        if (isEmpty()) {
            return false;
        }
        char c = this.input.charAt(this.pos);
        return c >= '0' && c <= '9';
    }

    /* access modifiers changed from: package-private */
    public boolean matchConsume(String seq) {
        if (!matches(seq)) {
            return false;
        }
        this.pos += seq.length();
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean matchConsumeIgnoreCase(String seq) {
        if (!matchesIgnoreCase(seq)) {
            return false;
        }
        this.pos += seq.length();
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean containsIgnoreCase(String seq) {
        return this.input.indexOf(seq.toLowerCase(), this.pos) > -1 || this.input.indexOf(seq.toUpperCase(), this.pos) > -1;
    }

    public String toString() {
        return this.input.substring(this.pos);
    }
}
