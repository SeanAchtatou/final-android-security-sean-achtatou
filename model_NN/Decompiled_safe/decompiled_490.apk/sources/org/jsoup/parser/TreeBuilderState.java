package org.jsoup.parser;

import com.adwhirl.util.AdWhirlUtil;
import com.google.ads.AdActivity;
import java.util.Iterator;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.DocumentType;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Token;

enum TreeBuilderState {
    Initial {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (TreeBuilderState.access$100(t)) {
                return true;
            }
            if (t.isComment()) {
                tb.insert(t.asComment());
            } else if (t.isDoctype()) {
                Token.Doctype d = t.asDoctype();
                tb.getDocument().appendChild(new DocumentType(d.getName(), d.getPublicIdentifier(), d.getSystemIdentifier(), tb.getBaseUri()));
                if (d.isForceQuirks()) {
                    tb.getDocument().quirksMode(Document.QuirksMode.quirks);
                }
                tb.transition(BeforeHtml);
            } else {
                tb.transition(BeforeHtml);
                return tb.process(t);
            }
            return true;
        }
    },
    BeforeHtml {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (t.isDoctype()) {
                tb.error(this);
                return false;
            }
            if (t.isComment()) {
                tb.insert(t.asComment());
            } else if (TreeBuilderState.access$100(t)) {
                return true;
            } else {
                if (!t.isStartTag() || !t.asStartTag().name().equals(AdActivity.HTML_PARAM)) {
                    if (t.isEndTag()) {
                        if (StringUtil.in(t.asEndTag().name(), "head", "body", AdActivity.HTML_PARAM, "br")) {
                            return anythingElse(t, tb);
                        }
                    }
                    if (!t.isEndTag()) {
                        return anythingElse(t, tb);
                    }
                    tb.error(this);
                    return false;
                }
                tb.insert(t.asStartTag());
                tb.transition(BeforeHead);
            }
            return true;
        }

        private boolean anythingElse(Token t, TreeBuilder tb) {
            tb.insert(AdActivity.HTML_PARAM);
            tb.transition(BeforeHead);
            return tb.process(t);
        }
    },
    BeforeHead {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (TreeBuilderState.access$100(t)) {
                return true;
            }
            if (t.isComment()) {
                tb.insert(t.asComment());
            } else if (t.isDoctype()) {
                tb.error(this);
                return false;
            } else if (t.isStartTag() && t.asStartTag().name().equals(AdActivity.HTML_PARAM)) {
                return InBody.process(t, tb);
            } else {
                if (!t.isStartTag() || !t.asStartTag().name().equals("head")) {
                    if (t.isEndTag()) {
                        if (StringUtil.in(t.asEndTag().name(), "head", "body", AdActivity.HTML_PARAM, "br")) {
                            tb.process(new Token.StartTag("head"));
                            return tb.process(t);
                        }
                    }
                    if (t.isEndTag()) {
                        tb.error(this);
                        return false;
                    }
                    tb.process(new Token.StartTag("head"));
                    return tb.process(t);
                }
                tb.setHeadElement(tb.insert(t.asStartTag()));
                tb.transition(InHead);
            }
            return true;
        }
    },
    InHead {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (TreeBuilderState.access$100(t)) {
                tb.insert(t.asCharacter());
                return true;
            }
            switch (AnonymousClass24.$SwitchMap$org$jsoup$parser$Token$TokenType[t.type.ordinal()]) {
                case 1:
                    tb.insert(t.asComment());
                    break;
                case 2:
                    tb.error(this);
                    return false;
                case 3:
                    Token.StartTag start = t.asStartTag();
                    String name = start.name();
                    if (name.equals(AdActivity.HTML_PARAM)) {
                        return InBody.process(t, tb);
                    }
                    if (StringUtil.in(name, "base", "basefont", "bgsound", "command", "link")) {
                        Element el = tb.insertEmpty(start);
                        if (name.equals("base") && el.hasAttr("href")) {
                            tb.setBaseUri(el);
                            break;
                        }
                    } else if (name.equals("meta")) {
                        tb.insertEmpty(start);
                        break;
                    } else if (name.equals("title")) {
                        TreeBuilderState.access$200(start, tb);
                        break;
                    } else {
                        if (StringUtil.in(name, "noframes", "style")) {
                            TreeBuilderState.access$300(start, tb);
                            break;
                        } else if (name.equals("noscript")) {
                            tb.insert(start);
                            tb.transition(InHeadNoscript);
                            break;
                        } else if (name.equals("script")) {
                            tb.insert(start);
                            tb.tokeniser.transition(TokeniserState.ScriptData);
                            tb.markInsertionMode();
                            tb.transition(Text);
                            break;
                        } else if (!name.equals("head")) {
                            return anythingElse(t, tb);
                        } else {
                            tb.error(this);
                            return false;
                        }
                    }
                case AdWhirlUtil.NETWORK_TYPE_MEDIALETS:
                    String name2 = t.asEndTag().name();
                    if (name2.equals("head")) {
                        tb.pop();
                        tb.transition(AfterHead);
                        break;
                    } else {
                        if (StringUtil.in(name2, "body", AdActivity.HTML_PARAM, "br")) {
                            return anythingElse(t, tb);
                        }
                        tb.error(this);
                        return false;
                    }
                default:
                    return anythingElse(t, tb);
            }
            return true;
        }

        private boolean anythingElse(Token t, TreeBuilder tb) {
            tb.process(new Token.EndTag("head"));
            return tb.process(t);
        }
    },
    InHeadNoscript {
        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0086, code lost:
            if (org.jsoup.helper.StringUtil.in(r8.asStartTag().name(), "basefont", "bgsound", "link", "meta", "noframes", "style") != false) goto L_0x0088;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x00c8, code lost:
            if (org.jsoup.helper.StringUtil.in(r8.asStartTag().name(), "head", "noscript") == false) goto L_0x00ca;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean process(org.jsoup.parser.Token r8, org.jsoup.parser.TreeBuilder r9) {
            /*
                r7 = this;
                r6 = 2
                r5 = 1
                r4 = 0
                boolean r0 = r8.isDoctype()
                if (r0 == 0) goto L_0x000e
                r9.error(r7)
            L_0x000c:
                r0 = r5
            L_0x000d:
                return r0
            L_0x000e:
                boolean r0 = r8.isStartTag()
                if (r0 == 0) goto L_0x002b
                org.jsoup.parser.Token$StartTag r0 = r8.asStartTag()
                java.lang.String r0 = r0.name()
                java.lang.String r1 = "html"
                boolean r0 = r0.equals(r1)
                if (r0 == 0) goto L_0x002b
                org.jsoup.parser.TreeBuilderState r0 = org.jsoup.parser.TreeBuilderState.AnonymousClass5.InBody
                boolean r0 = r9.process(r8, r0)
                goto L_0x000d
            L_0x002b:
                boolean r0 = r8.isEndTag()
                if (r0 == 0) goto L_0x004a
                org.jsoup.parser.Token$EndTag r0 = r8.asEndTag()
                java.lang.String r0 = r0.name()
                java.lang.String r1 = "noscript"
                boolean r0 = r0.equals(r1)
                if (r0 == 0) goto L_0x004a
                r9.pop()
                org.jsoup.parser.TreeBuilderState r0 = org.jsoup.parser.TreeBuilderState.AnonymousClass5.InHead
                r9.transition(r0)
                goto L_0x000c
            L_0x004a:
                boolean r0 = org.jsoup.parser.TreeBuilderState.access$100(r8)
                if (r0 != 0) goto L_0x0088
                boolean r0 = r8.isComment()
                if (r0 != 0) goto L_0x0088
                boolean r0 = r8.isStartTag()
                if (r0 == 0) goto L_0x0090
                org.jsoup.parser.Token$StartTag r0 = r8.asStartTag()
                java.lang.String r0 = r0.name()
                r1 = 6
                java.lang.String[] r1 = new java.lang.String[r1]
                java.lang.String r2 = "basefont"
                r1[r4] = r2
                java.lang.String r2 = "bgsound"
                r1[r5] = r2
                java.lang.String r2 = "link"
                r1[r6] = r2
                r2 = 3
                java.lang.String r3 = "meta"
                r1[r2] = r3
                r2 = 4
                java.lang.String r3 = "noframes"
                r1[r2] = r3
                r2 = 5
                java.lang.String r3 = "style"
                r1[r2] = r3
                boolean r0 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r0 == 0) goto L_0x0090
            L_0x0088:
                org.jsoup.parser.TreeBuilderState r0 = org.jsoup.parser.TreeBuilderState.AnonymousClass5.InHead
                boolean r0 = r9.process(r8, r0)
                goto L_0x000d
            L_0x0090:
                boolean r0 = r8.isEndTag()
                if (r0 == 0) goto L_0x00ac
                org.jsoup.parser.Token$EndTag r0 = r8.asEndTag()
                java.lang.String r0 = r0.name()
                java.lang.String r1 = "br"
                boolean r0 = r0.equals(r1)
                if (r0 == 0) goto L_0x00ac
                boolean r0 = r7.anythingElse(r8, r9)
                goto L_0x000d
            L_0x00ac:
                boolean r0 = r8.isStartTag()
                if (r0 == 0) goto L_0x00ca
                org.jsoup.parser.Token$StartTag r0 = r8.asStartTag()
                java.lang.String r0 = r0.name()
                java.lang.String[] r1 = new java.lang.String[r6]
                java.lang.String r2 = "head"
                r1[r4] = r2
                java.lang.String r2 = "noscript"
                r1[r5] = r2
                boolean r0 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r0 != 0) goto L_0x00d0
            L_0x00ca:
                boolean r0 = r8.isEndTag()
                if (r0 == 0) goto L_0x00d6
            L_0x00d0:
                r9.error(r7)
                r0 = r4
                goto L_0x000d
            L_0x00d6:
                boolean r0 = r7.anythingElse(r8, r9)
                goto L_0x000d
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.TreeBuilderState.AnonymousClass5.process(org.jsoup.parser.Token, org.jsoup.parser.TreeBuilder):boolean");
        }

        private boolean anythingElse(Token t, TreeBuilder tb) {
            tb.error(this);
            tb.process(new Token.EndTag("noscript"));
            return tb.process(t);
        }
    },
    AfterHead {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (TreeBuilderState.access$100(t)) {
                tb.insert(t.asCharacter());
            } else if (t.isComment()) {
                tb.insert(t.asComment());
            } else if (t.isDoctype()) {
                tb.error(this);
            } else if (t.isStartTag()) {
                Token.StartTag startTag = t.asStartTag();
                String name = startTag.name();
                if (name.equals(AdActivity.HTML_PARAM)) {
                    return tb.process(t, InBody);
                }
                if (name.equals("body")) {
                    tb.insert(startTag);
                    tb.framesetOk(false);
                    tb.transition(InBody);
                } else if (name.equals("frameset")) {
                    tb.insert(startTag);
                    tb.transition(InFrameset);
                } else {
                    if (StringUtil.in(name, "base", "basefont", "bgsound", "link", "meta", "noframes", "script", "style", "title")) {
                        tb.error(this);
                        Element head = tb.getHeadElement();
                        tb.push(head);
                        tb.process(t, InHead);
                        tb.removeFromStack(head);
                    } else if (name.equals("head")) {
                        tb.error(this);
                        return false;
                    } else {
                        anythingElse(t, tb);
                    }
                }
            } else if (t.isEndTag()) {
                if (StringUtil.in(t.asEndTag().name(), "body", AdActivity.HTML_PARAM)) {
                    anythingElse(t, tb);
                } else {
                    tb.error(this);
                    return false;
                }
            } else {
                anythingElse(t, tb);
            }
            return true;
        }

        private boolean anythingElse(Token t, TreeBuilder tb) {
            tb.process(new Token.StartTag("body"));
            tb.framesetOk(true);
            return tb.process(t);
        }
    },
    InBody {
        /* Debug info: failed to restart local var, previous not found, register: 42 */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.jsoup.nodes.Element.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Element
         arg types: [java.lang.String, java.lang.String]
         candidates:
          org.jsoup.nodes.Element.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Node
          org.jsoup.nodes.Node.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Node
          org.jsoup.nodes.Element.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Element */
        /* access modifiers changed from: package-private */
        /* JADX WARNING: Removed duplicated region for block: B:365:0x1107  */
        /* JADX WARNING: Removed duplicated region for block: B:372:0x114e A[LOOP:9: B:370:0x1148->B:372:0x114e, LOOP_END] */
        /* JADX WARNING: Removed duplicated region for block: B:379:0x119b  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean process(org.jsoup.parser.Token r43, org.jsoup.parser.TreeBuilder r44) {
            /*
                r42 = this;
                int[] r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass24.$SwitchMap$org$jsoup$parser$Token$TokenType
                r0 = r43
                org.jsoup.parser.Token$TokenType r0 = r0.type
                r39 = r0
                int r39 = r39.ordinal()
                r38 = r38[r39]
                switch(r38) {
                    case 1: goto L_0x0053;
                    case 2: goto L_0x005f;
                    case 3: goto L_0x0069;
                    case 4: goto L_0x0c0c;
                    case 5: goto L_0x0014;
                    default: goto L_0x0011;
                }
            L_0x0011:
                r38 = 1
            L_0x0013:
                return r38
            L_0x0014:
                org.jsoup.parser.Token$Character r9 = r43.asCharacter()
                java.lang.String r38 = r9.getData()
                java.lang.String r39 = org.jsoup.parser.TreeBuilderState.access$400()
                boolean r38 = r38.equals(r39)
                if (r38 == 0) goto L_0x0030
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x0030:
                boolean r38 = org.jsoup.parser.TreeBuilderState.access$100(r9)
                if (r38 == 0) goto L_0x0040
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r9
                r0.insert(r1)
                goto L_0x0011
            L_0x0040:
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r9
                r0.insert(r1)
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                goto L_0x0011
            L_0x0053:
                org.jsoup.parser.Token$Comment r38 = r43.asComment()
                r0 = r44
                r1 = r38
                r0.insert(r1)
                goto L_0x0011
            L_0x005f:
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x0069:
                org.jsoup.parser.Token$StartTag r36 = r43.asStartTag()
                java.lang.String r26 = r36.name()
                java.lang.String r38 = "html"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x00bb
                r0 = r44
                r1 = r42
                r0.error(r1)
                org.jsoup.helper.DescendableLinkedList r38 = r44.getStack()
                java.lang.Object r19 = r38.getFirst()
                org.jsoup.nodes.Element r19 = (org.jsoup.nodes.Element) r19
                org.jsoup.nodes.Attributes r38 = r36.getAttributes()
                java.util.Iterator r21 = r38.iterator()
            L_0x0096:
                boolean r38 = r21.hasNext()
                if (r38 == 0) goto L_0x0011
                java.lang.Object r7 = r21.next()
                org.jsoup.nodes.Attribute r7 = (org.jsoup.nodes.Attribute) r7
                java.lang.String r38 = r7.getKey()
                r0 = r19
                r1 = r38
                boolean r38 = r0.hasAttr(r1)
                if (r38 != 0) goto L_0x0096
                org.jsoup.nodes.Attributes r38 = r19.attributes()
                r0 = r38
                r1 = r7
                r0.put(r1)
                goto L_0x0096
            L_0x00bb:
                r38 = 10
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "base"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "basefont"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "bgsound"
                r38[r39] = r40
                r39 = 3
                java.lang.String r40 = "command"
                r38[r39] = r40
                r39 = 4
                java.lang.String r40 = "link"
                r38[r39] = r40
                r39 = 5
                java.lang.String r40 = "meta"
                r38[r39] = r40
                r39 = 6
                java.lang.String r40 = "noframes"
                r38[r39] = r40
                r39 = 7
                java.lang.String r40 = "script"
                r38[r39] = r40
                r39 = 8
                java.lang.String r40 = "style"
                r38[r39] = r40
                r39 = 9
                java.lang.String r40 = "title"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x0117
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.InHead
                r0 = r44
                r1 = r43
                r2 = r38
                boolean r38 = r0.process(r1, r2)
                goto L_0x0013
            L_0x0117:
                java.lang.String r38 = "body"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x01a3
                r0 = r44
                r1 = r42
                r0.error(r1)
                org.jsoup.helper.DescendableLinkedList r35 = r44.getStack()
                int r38 = r35.size()
                r39 = 1
                r0 = r38
                r1 = r39
                if (r0 == r1) goto L_0x015e
                int r38 = r35.size()
                r39 = 2
                r0 = r38
                r1 = r39
                if (r0 <= r1) goto L_0x0162
                r38 = 1
                r0 = r35
                r1 = r38
                java.lang.Object r42 = r0.get(r1)
                org.jsoup.nodes.Element r42 = (org.jsoup.nodes.Element) r42
                java.lang.String r38 = r42.nodeName()
                java.lang.String r39 = "body"
                boolean r38 = r38.equals(r39)
                if (r38 != 0) goto L_0x0162
            L_0x015e:
                r38 = 0
                goto L_0x0013
            L_0x0162:
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                r38 = 1
                r0 = r35
                r1 = r38
                java.lang.Object r8 = r0.get(r1)
                org.jsoup.nodes.Element r8 = (org.jsoup.nodes.Element) r8
                org.jsoup.nodes.Attributes r38 = r36.getAttributes()
                java.util.Iterator r21 = r38.iterator()
            L_0x017f:
                boolean r38 = r21.hasNext()
                if (r38 == 0) goto L_0x0011
                java.lang.Object r7 = r21.next()
                org.jsoup.nodes.Attribute r7 = (org.jsoup.nodes.Attribute) r7
                java.lang.String r38 = r7.getKey()
                r0 = r8
                r1 = r38
                boolean r38 = r0.hasAttr(r1)
                if (r38 != 0) goto L_0x017f
                org.jsoup.nodes.Attributes r38 = r8.attributes()
                r0 = r38
                r1 = r7
                r0.put(r1)
                goto L_0x017f
            L_0x01a3:
                java.lang.String r38 = "frameset"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x022f
                r0 = r44
                r1 = r42
                r0.error(r1)
                org.jsoup.helper.DescendableLinkedList r35 = r44.getStack()
                int r38 = r35.size()
                r39 = 1
                r0 = r38
                r1 = r39
                if (r0 == r1) goto L_0x01ea
                int r38 = r35.size()
                r39 = 2
                r0 = r38
                r1 = r39
                if (r0 <= r1) goto L_0x01ee
                r38 = 1
                r0 = r35
                r1 = r38
                java.lang.Object r42 = r0.get(r1)
                org.jsoup.nodes.Element r42 = (org.jsoup.nodes.Element) r42
                java.lang.String r38 = r42.nodeName()
                java.lang.String r39 = "body"
                boolean r38 = r38.equals(r39)
                if (r38 != 0) goto L_0x01ee
            L_0x01ea:
                r38 = 0
                goto L_0x0013
            L_0x01ee:
                boolean r38 = r44.framesetOk()
                if (r38 != 0) goto L_0x01f8
                r38 = 0
                goto L_0x0013
            L_0x01f8:
                r38 = 1
                r0 = r35
                r1 = r38
                java.lang.Object r32 = r0.get(r1)
                org.jsoup.nodes.Element r32 = (org.jsoup.nodes.Element) r32
                org.jsoup.nodes.Element r38 = r32.parent()
                if (r38 == 0) goto L_0x020d
                r32.remove()
            L_0x020d:
                int r38 = r35.size()
                r39 = 1
                r0 = r38
                r1 = r39
                if (r0 <= r1) goto L_0x021d
                r35.removeLast()
                goto L_0x020d
            L_0x021d:
                r0 = r44
                r1 = r36
                r0.insert(r1)
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.InFrameset
                r0 = r44
                r1 = r38
                r0.transition(r1)
                goto L_0x0011
            L_0x022f:
                r38 = 22
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "address"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "article"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "aside"
                r38[r39] = r40
                r39 = 3
                java.lang.String r40 = "blockquote"
                r38[r39] = r40
                r39 = 4
                java.lang.String r40 = "center"
                r38[r39] = r40
                r39 = 5
                java.lang.String r40 = "details"
                r38[r39] = r40
                r39 = 6
                java.lang.String r40 = "dir"
                r38[r39] = r40
                r39 = 7
                java.lang.String r40 = "div"
                r38[r39] = r40
                r39 = 8
                java.lang.String r40 = "dl"
                r38[r39] = r40
                r39 = 9
                java.lang.String r40 = "fieldset"
                r38[r39] = r40
                r39 = 10
                java.lang.String r40 = "figcaption"
                r38[r39] = r40
                r39 = 11
                java.lang.String r40 = "figure"
                r38[r39] = r40
                r39 = 12
                java.lang.String r40 = "footer"
                r38[r39] = r40
                r39 = 13
                java.lang.String r40 = "header"
                r38[r39] = r40
                r39 = 14
                java.lang.String r40 = "hgroup"
                r38[r39] = r40
                r39 = 15
                java.lang.String r40 = "menu"
                r38[r39] = r40
                r39 = 16
                java.lang.String r40 = "nav"
                r38[r39] = r40
                r39 = 17
                java.lang.String r40 = "ol"
                r38[r39] = r40
                r39 = 18
                java.lang.String r40 = "p"
                r38[r39] = r40
                r39 = 19
                java.lang.String r40 = "section"
                r38[r39] = r40
                r39 = 20
                java.lang.String r40 = "summary"
                r38[r39] = r40
                r39 = 21
                java.lang.String r40 = "ul"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x02e8
                java.lang.String r38 = "p"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inButtonScope(r1)
                if (r38 == 0) goto L_0x02df
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "p"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x02df:
                r0 = r44
                r1 = r36
                r0.insert(r1)
                goto L_0x0011
            L_0x02e8:
                r38 = 6
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "h1"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "h2"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "h3"
                r38[r39] = r40
                r39 = 3
                java.lang.String r40 = "h4"
                r38[r39] = r40
                r39 = 4
                java.lang.String r40 = "h5"
                r38[r39] = r40
                r39 = 5
                java.lang.String r40 = "h6"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x0385
                java.lang.String r38 = "p"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inButtonScope(r1)
                if (r38 == 0) goto L_0x0338
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "p"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x0338:
                org.jsoup.nodes.Element r38 = r44.currentElement()
                java.lang.String r38 = r38.nodeName()
                r39 = 6
                r0 = r39
                java.lang.String[] r0 = new java.lang.String[r0]
                r39 = r0
                r40 = 0
                java.lang.String r41 = "h1"
                r39[r40] = r41
                r40 = 1
                java.lang.String r41 = "h2"
                r39[r40] = r41
                r40 = 2
                java.lang.String r41 = "h3"
                r39[r40] = r41
                r40 = 3
                java.lang.String r41 = "h4"
                r39[r40] = r41
                r40 = 4
                java.lang.String r41 = "h5"
                r39[r40] = r41
                r40 = 5
                java.lang.String r41 = "h6"
                r39[r40] = r41
                boolean r38 = org.jsoup.helper.StringUtil.in(r38, r39)
                if (r38 == 0) goto L_0x037c
                r0 = r44
                r1 = r42
                r0.error(r1)
                r44.pop()
            L_0x037c:
                r0 = r44
                r1 = r36
                r0.insert(r1)
                goto L_0x0011
            L_0x0385:
                r38 = 2
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "pre"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "listing"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x03cf
                java.lang.String r38 = "p"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inButtonScope(r1)
                if (r38 == 0) goto L_0x03bd
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "p"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x03bd:
                r0 = r44
                r1 = r36
                r0.insert(r1)
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                goto L_0x0011
            L_0x03cf:
                java.lang.String r38 = "form"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0417
                org.jsoup.nodes.Element r38 = r44.getFormElement()
                if (r38 == 0) goto L_0x03ec
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x03ec:
                java.lang.String r38 = "p"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inButtonScope(r1)
                if (r38 == 0) goto L_0x0406
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "p"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x0406:
                r0 = r44
                r1 = r36
                org.jsoup.nodes.Element r16 = r0.insert(r1)
                r0 = r44
                r1 = r16
                r0.setFormElement(r1)
                goto L_0x0011
            L_0x0417:
                java.lang.String r38 = "li"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x04b1
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                org.jsoup.helper.DescendableLinkedList r35 = r44.getStack()
                int r38 = r35.size()
                r39 = 1
                int r20 = r38 - r39
            L_0x0438:
                if (r20 <= 0) goto L_0x045e
                r0 = r35
                r1 = r20
                java.lang.Object r14 = r0.get(r1)
                org.jsoup.nodes.Element r14 = (org.jsoup.nodes.Element) r14
                java.lang.String r38 = r14.nodeName()
                java.lang.String r39 = "li"
                boolean r38 = r38.equals(r39)
                if (r38 == 0) goto L_0x0481
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "li"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x045e:
                java.lang.String r38 = "p"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inButtonScope(r1)
                if (r38 == 0) goto L_0x0478
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "p"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x0478:
                r0 = r44
                r1 = r36
                r0.insert(r1)
                goto L_0x0011
            L_0x0481:
                r0 = r44
                r1 = r14
                boolean r38 = r0.isSpecial(r1)
                if (r38 == 0) goto L_0x04ae
                java.lang.String r38 = r14.nodeName()
                r39 = 3
                r0 = r39
                java.lang.String[] r0 = new java.lang.String[r0]
                r39 = r0
                r40 = 0
                java.lang.String r41 = "address"
                r39[r40] = r41
                r40 = 1
                java.lang.String r41 = "div"
                r39[r40] = r41
                r40 = 2
                java.lang.String r41 = "p"
                r39[r40] = r41
                boolean r38 = org.jsoup.helper.StringUtil.in(r38, r39)
                if (r38 == 0) goto L_0x045e
            L_0x04ae:
                int r20 = r20 + -1
                goto L_0x0438
            L_0x04b1:
                r38 = 2
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "dd"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "dt"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x0572
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                org.jsoup.helper.DescendableLinkedList r35 = r44.getStack()
                int r38 = r35.size()
                r39 = 1
                int r20 = r38 - r39
            L_0x04e4:
                if (r20 <= 0) goto L_0x051e
                r0 = r35
                r1 = r20
                java.lang.Object r14 = r0.get(r1)
                org.jsoup.nodes.Element r14 = (org.jsoup.nodes.Element) r14
                java.lang.String r38 = r14.nodeName()
                r39 = 2
                r0 = r39
                java.lang.String[] r0 = new java.lang.String[r0]
                r39 = r0
                r40 = 0
                java.lang.String r41 = "dd"
                r39[r40] = r41
                r40 = 1
                java.lang.String r41 = "dt"
                r39[r40] = r41
                boolean r38 = org.jsoup.helper.StringUtil.in(r38, r39)
                if (r38 == 0) goto L_0x0541
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = r14.nodeName()
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x051e:
                java.lang.String r38 = "p"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inButtonScope(r1)
                if (r38 == 0) goto L_0x0538
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "p"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x0538:
                r0 = r44
                r1 = r36
                r0.insert(r1)
                goto L_0x0011
            L_0x0541:
                r0 = r44
                r1 = r14
                boolean r38 = r0.isSpecial(r1)
                if (r38 == 0) goto L_0x056e
                java.lang.String r38 = r14.nodeName()
                r39 = 3
                r0 = r39
                java.lang.String[] r0 = new java.lang.String[r0]
                r39 = r0
                r40 = 0
                java.lang.String r41 = "address"
                r39[r40] = r41
                r40 = 1
                java.lang.String r41 = "div"
                r39[r40] = r41
                r40 = 2
                java.lang.String r41 = "p"
                r39[r40] = r41
                boolean r38 = org.jsoup.helper.StringUtil.in(r38, r39)
                if (r38 == 0) goto L_0x051e
            L_0x056e:
                int r20 = r20 + -1
                goto L_0x04e4
            L_0x0572:
                java.lang.String r38 = "plaintext"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x05ac
                java.lang.String r38 = "p"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inButtonScope(r1)
                if (r38 == 0) goto L_0x0598
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "p"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x0598:
                r0 = r44
                r1 = r36
                r0.insert(r1)
                r0 = r44
                org.jsoup.parser.Tokeniser r0 = r0.tokeniser
                r38 = r0
                org.jsoup.parser.TokeniserState r39 = org.jsoup.parser.TokeniserState.PLAINTEXT
                r38.transition(r39)
                goto L_0x0011
            L_0x05ac:
                java.lang.String r38 = "button"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x05f7
                java.lang.String r38 = "button"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inButtonScope(r1)
                if (r38 == 0) goto L_0x05e2
                r0 = r44
                r1 = r42
                r0.error(r1)
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "button"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
                r0 = r44
                r1 = r36
                r0.process(r1)
                goto L_0x0011
            L_0x05e2:
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r36
                r0.insert(r1)
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                goto L_0x0011
            L_0x05f7:
                java.lang.String r38 = "a"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0651
                java.lang.String r38 = "a"
                r0 = r44
                r1 = r38
                org.jsoup.nodes.Element r38 = r0.getActiveFormattingElement(r1)
                if (r38 == 0) goto L_0x063e
                r0 = r44
                r1 = r42
                r0.error(r1)
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "a"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
                java.lang.String r38 = "a"
                r0 = r44
                r1 = r38
                org.jsoup.nodes.Element r30 = r0.getFromStack(r1)
                if (r30 == 0) goto L_0x063e
                r0 = r44
                r1 = r30
                r0.removeFromActiveFormattingElements(r1)
                r0 = r44
                r1 = r30
                r0.removeFromStack(r1)
            L_0x063e:
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r36
                org.jsoup.nodes.Element r3 = r0.insert(r1)
                r0 = r44
                r1 = r3
                r0.pushActiveFormattingElements(r1)
                goto L_0x0011
            L_0x0651:
                r38 = 12
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "b"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "big"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "code"
                r38[r39] = r40
                r39 = 3
                java.lang.String r40 = "em"
                r38[r39] = r40
                r39 = 4
                java.lang.String r40 = "font"
                r38[r39] = r40
                r39 = 5
                java.lang.String r40 = "i"
                r38[r39] = r40
                r39 = 6
                java.lang.String r40 = "s"
                r38[r39] = r40
                r39 = 7
                java.lang.String r40 = "small"
                r38[r39] = r40
                r39 = 8
                java.lang.String r40 = "strike"
                r38[r39] = r40
                r39 = 9
                java.lang.String r40 = "strong"
                r38[r39] = r40
                r39 = 10
                java.lang.String r40 = "tt"
                r38[r39] = r40
                r39 = 11
                java.lang.String r40 = "u"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x06be
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r36
                org.jsoup.nodes.Element r14 = r0.insert(r1)
                r0 = r44
                r1 = r14
                r0.pushActiveFormattingElements(r1)
                goto L_0x0011
            L_0x06be:
                java.lang.String r38 = "nobr"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0701
                r44.reconstructFormattingElements()
                java.lang.String r38 = "nobr"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inScope(r1)
                if (r38 == 0) goto L_0x06f1
                r0 = r44
                r1 = r42
                r0.error(r1)
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "nobr"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
                r44.reconstructFormattingElements()
            L_0x06f1:
                r0 = r44
                r1 = r36
                org.jsoup.nodes.Element r14 = r0.insert(r1)
                r0 = r44
                r1 = r14
                r0.pushActiveFormattingElements(r1)
                goto L_0x0011
            L_0x0701:
                r38 = 3
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "applet"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "marquee"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "object"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x073d
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r36
                r0.insert(r1)
                r44.insertMarkerToFormattingElements()
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                goto L_0x0011
            L_0x073d:
                java.lang.String r38 = "table"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x078e
                org.jsoup.nodes.Document r38 = r44.getDocument()
                org.jsoup.nodes.Document$QuirksMode r38 = r38.quirksMode()
                org.jsoup.nodes.Document$QuirksMode r39 = org.jsoup.nodes.Document.QuirksMode.quirks
                r0 = r38
                r1 = r39
                if (r0 == r1) goto L_0x0773
                java.lang.String r38 = "p"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inButtonScope(r1)
                if (r38 == 0) goto L_0x0773
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "p"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x0773:
                r0 = r44
                r1 = r36
                r0.insert(r1)
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.InTable
                r0 = r44
                r1 = r38
                r0.transition(r1)
                goto L_0x0011
            L_0x078e:
                r38 = 6
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "area"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "br"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "embed"
                r38[r39] = r40
                r39 = 3
                java.lang.String r40 = "img"
                r38[r39] = r40
                r39 = 4
                java.lang.String r40 = "keygen"
                r38[r39] = r40
                r39 = 5
                java.lang.String r40 = "wbr"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x07d9
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r36
                r0.insertEmpty(r1)
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                goto L_0x0011
            L_0x07d9:
                java.lang.String r38 = "input"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x080c
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r36
                org.jsoup.nodes.Element r14 = r0.insertEmpty(r1)
                java.lang.String r38 = "type"
                r0 = r14
                r1 = r38
                java.lang.String r38 = r0.attr(r1)
                java.lang.String r39 = "hidden"
                boolean r38 = r38.equalsIgnoreCase(r39)
                if (r38 != 0) goto L_0x0011
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                goto L_0x0011
            L_0x080c:
                r38 = 3
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "param"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "source"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "track"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x0839
                r0 = r44
                r1 = r36
                r0.insertEmpty(r1)
                goto L_0x0011
            L_0x0839:
                java.lang.String r38 = "hr"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0871
                java.lang.String r38 = "p"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inButtonScope(r1)
                if (r38 == 0) goto L_0x085f
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "p"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x085f:
                r0 = r44
                r1 = r36
                r0.insertEmpty(r1)
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                goto L_0x0011
            L_0x0871:
                java.lang.String r38 = "image"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0890
                java.lang.String r38 = "img"
                r0 = r36
                r1 = r38
                r0.name(r1)
                r0 = r44
                r1 = r36
                boolean r38 = r0.process(r1)
                goto L_0x0013
            L_0x0890:
                java.lang.String r38 = "isindex"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x09cd
                r0 = r44
                r1 = r42
                r0.error(r1)
                org.jsoup.nodes.Element r38 = r44.getFormElement()
                if (r38 == 0) goto L_0x08ad
                r38 = 0
                goto L_0x0013
            L_0x08ad:
                r0 = r44
                org.jsoup.parser.Tokeniser r0 = r0.tokeniser
                r38 = r0
                r38.acknowledgeSelfClosingFlag()
                org.jsoup.parser.Token$StartTag r38 = new org.jsoup.parser.Token$StartTag
                java.lang.String r39 = "form"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
                r0 = r36
                org.jsoup.nodes.Attributes r0 = r0.attributes
                r38 = r0
                java.lang.String r39 = "action"
                boolean r38 = r38.hasKey(r39)
                if (r38 == 0) goto L_0x08ed
                org.jsoup.nodes.Element r16 = r44.getFormElement()
                java.lang.String r38 = "action"
                r0 = r36
                org.jsoup.nodes.Attributes r0 = r0.attributes
                r39 = r0
                java.lang.String r40 = "action"
                java.lang.String r39 = r39.get(r40)
                r0 = r16
                r1 = r38
                r2 = r39
                r0.attr(r1, r2)
            L_0x08ed:
                org.jsoup.parser.Token$StartTag r38 = new org.jsoup.parser.Token$StartTag
                java.lang.String r39 = "hr"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
                org.jsoup.parser.Token$StartTag r38 = new org.jsoup.parser.Token$StartTag
                java.lang.String r39 = "label"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
                r0 = r36
                org.jsoup.nodes.Attributes r0 = r0.attributes
                r38 = r0
                java.lang.String r39 = "prompt"
                boolean r38 = r38.hasKey(r39)
                if (r38 == 0) goto L_0x097b
                r0 = r36
                org.jsoup.nodes.Attributes r0 = r0.attributes
                r38 = r0
                java.lang.String r39 = "prompt"
                java.lang.String r38 = r38.get(r39)
                r29 = r38
            L_0x0925:
                org.jsoup.parser.Token$Character r38 = new org.jsoup.parser.Token$Character
                r0 = r38
                r1 = r29
                r0.<init>(r1)
                r0 = r44
                r1 = r38
                r0.process(r1)
                org.jsoup.nodes.Attributes r22 = new org.jsoup.nodes.Attributes
                r22.<init>()
                r0 = r36
                org.jsoup.nodes.Attributes r0 = r0.attributes
                r38 = r0
                java.util.Iterator r21 = r38.iterator()
            L_0x0944:
                boolean r38 = r21.hasNext()
                if (r38 == 0) goto L_0x0980
                java.lang.Object r6 = r21.next()
                org.jsoup.nodes.Attribute r6 = (org.jsoup.nodes.Attribute) r6
                java.lang.String r38 = r6.getKey()
                r39 = 3
                r0 = r39
                java.lang.String[] r0 = new java.lang.String[r0]
                r39 = r0
                r40 = 0
                java.lang.String r41 = "name"
                r39[r40] = r41
                r40 = 1
                java.lang.String r41 = "action"
                r39[r40] = r41
                r40 = 2
                java.lang.String r41 = "prompt"
                r39[r40] = r41
                boolean r38 = org.jsoup.helper.StringUtil.in(r38, r39)
                if (r38 != 0) goto L_0x0944
                r0 = r22
                r1 = r6
                r0.put(r1)
                goto L_0x0944
            L_0x097b:
                java.lang.String r38 = "This is a searchable index. Enter search keywords: "
                r29 = r38
                goto L_0x0925
            L_0x0980:
                java.lang.String r38 = "name"
                java.lang.String r39 = "isindex"
                r0 = r22
                r1 = r38
                r2 = r39
                r0.put(r1, r2)
                org.jsoup.parser.Token$StartTag r38 = new org.jsoup.parser.Token$StartTag
                java.lang.String r39 = "input"
                r0 = r38
                r1 = r39
                r2 = r22
                r0.<init>(r1, r2)
                r0 = r44
                r1 = r38
                r0.process(r1)
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "label"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
                org.jsoup.parser.Token$StartTag r38 = new org.jsoup.parser.Token$StartTag
                java.lang.String r39 = "hr"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "form"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
                goto L_0x0011
            L_0x09cd:
                java.lang.String r38 = "textarea"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0a02
                r0 = r44
                r1 = r36
                r0.insert(r1)
                r0 = r44
                org.jsoup.parser.Tokeniser r0 = r0.tokeniser
                r38 = r0
                org.jsoup.parser.TokeniserState r39 = org.jsoup.parser.TokeniserState.Rcdata
                r38.transition(r39)
                r44.markInsertionMode()
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.Text
                r0 = r44
                r1 = r38
                r0.transition(r1)
                goto L_0x0011
            L_0x0a02:
                java.lang.String r38 = "xmp"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0a3d
                java.lang.String r38 = "p"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inButtonScope(r1)
                if (r38 == 0) goto L_0x0a28
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "p"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x0a28:
                r44.reconstructFormattingElements()
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                r0 = r36
                r1 = r44
                org.jsoup.parser.TreeBuilderState.access$300(r0, r1)
                goto L_0x0011
            L_0x0a3d:
                java.lang.String r38 = "iframe"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0a5b
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                r0 = r36
                r1 = r44
                org.jsoup.parser.TreeBuilderState.access$300(r0, r1)
                goto L_0x0011
            L_0x0a5b:
                java.lang.String r38 = "noembed"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0a70
                r0 = r36
                r1 = r44
                org.jsoup.parser.TreeBuilderState.access$300(r0, r1)
                goto L_0x0011
            L_0x0a70:
                java.lang.String r38 = "select"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0ad1
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r36
                r0.insert(r1)
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                org.jsoup.parser.TreeBuilderState r37 = r44.state()
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.InTable
                boolean r38 = r37.equals(r38)
                if (r38 != 0) goto L_0x0abb
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.InCaption
                boolean r38 = r37.equals(r38)
                if (r38 != 0) goto L_0x0abb
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.InTableBody
                boolean r38 = r37.equals(r38)
                if (r38 != 0) goto L_0x0abb
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.InRow
                boolean r38 = r37.equals(r38)
                if (r38 != 0) goto L_0x0abb
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.InCell
                boolean r38 = r37.equals(r38)
                if (r38 == 0) goto L_0x0ac6
            L_0x0abb:
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.InSelectInTable
                r0 = r44
                r1 = r38
                r0.transition(r1)
                goto L_0x0011
            L_0x0ac6:
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.InSelect
                r0 = r44
                r1 = r38
                r0.transition(r1)
                goto L_0x0011
            L_0x0ad1:
                java.lang.String r38 = "optgroup"
                r39 = 1
                r0 = r39
                java.lang.String[] r0 = new java.lang.String[r0]
                r39 = r0
                r40 = 0
                java.lang.String r41 = "option"
                r39[r40] = r41
                boolean r38 = org.jsoup.helper.StringUtil.in(r38, r39)
                if (r38 == 0) goto L_0x0b11
                org.jsoup.nodes.Element r38 = r44.currentElement()
                java.lang.String r38 = r38.nodeName()
                java.lang.String r39 = "option"
                boolean r38 = r38.equals(r39)
                if (r38 == 0) goto L_0x0b05
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "option"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x0b05:
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r36
                r0.insert(r1)
                goto L_0x0011
            L_0x0b11:
                java.lang.String r38 = "rp"
                r39 = 1
                r0 = r39
                java.lang.String[] r0 = new java.lang.String[r0]
                r39 = r0
                r40 = 0
                java.lang.String r41 = "rt"
                r39[r40] = r41
                boolean r38 = org.jsoup.helper.StringUtil.in(r38, r39)
                if (r38 == 0) goto L_0x0b5f
                java.lang.String r38 = "ruby"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inScope(r1)
                if (r38 == 0) goto L_0x0011
                r44.generateImpliedEndTags()
                org.jsoup.nodes.Element r38 = r44.currentElement()
                java.lang.String r38 = r38.nodeName()
                java.lang.String r39 = "ruby"
                boolean r38 = r38.equals(r39)
                if (r38 != 0) goto L_0x0b56
                r0 = r44
                r1 = r42
                r0.error(r1)
                java.lang.String r38 = "ruby"
                r0 = r44
                r1 = r38
                r0.popStackToBefore(r1)
            L_0x0b56:
                r0 = r44
                r1 = r36
                r0.insert(r1)
                goto L_0x0011
            L_0x0b5f:
                java.lang.String r38 = "math"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0b80
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r36
                r0.insert(r1)
                r0 = r44
                org.jsoup.parser.Tokeniser r0 = r0.tokeniser
                r38 = r0
                r38.acknowledgeSelfClosingFlag()
                goto L_0x0011
            L_0x0b80:
                java.lang.String r38 = "svg"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0ba1
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r36
                r0.insert(r1)
                r0 = r44
                org.jsoup.parser.Tokeniser r0 = r0.tokeniser
                r38 = r0
                r38.acknowledgeSelfClosingFlag()
                goto L_0x0011
            L_0x0ba1:
                r38 = 11
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "caption"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "col"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "colgroup"
                r38[r39] = r40
                r39 = 3
                java.lang.String r40 = "frame"
                r38[r39] = r40
                r39 = 4
                java.lang.String r40 = "head"
                r38[r39] = r40
                r39 = 5
                java.lang.String r40 = "tbody"
                r38[r39] = r40
                r39 = 6
                java.lang.String r40 = "td"
                r38[r39] = r40
                r39 = 7
                java.lang.String r40 = "tfoot"
                r38[r39] = r40
                r39 = 8
                java.lang.String r40 = "th"
                r38[r39] = r40
                r39 = 9
                java.lang.String r40 = "thead"
                r38[r39] = r40
                r39 = 10
                java.lang.String r40 = "tr"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x0c00
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x0c00:
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r36
                r0.insert(r1)
                goto L_0x0011
            L_0x0c0c:
                org.jsoup.parser.Token$EndTag r15 = r43.asEndTag()
                java.lang.String r26 = r15.name()
                java.lang.String r38 = "body"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0c42
                java.lang.String r38 = "body"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inScope(r1)
                if (r38 != 0) goto L_0x0c37
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x0c37:
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.AfterBody
                r0 = r44
                r1 = r38
                r0.transition(r1)
                goto L_0x0011
            L_0x0c42:
                java.lang.String r38 = "html"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0c68
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "body"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                boolean r28 = r0.process(r1)
                if (r28 == 0) goto L_0x0011
                r0 = r44
                r1 = r15
                boolean r38 = r0.process(r1)
                goto L_0x0013
            L_0x0c68:
                r38 = 24
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "address"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "article"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "aside"
                r38[r39] = r40
                r39 = 3
                java.lang.String r40 = "blockquote"
                r38[r39] = r40
                r39 = 4
                java.lang.String r40 = "button"
                r38[r39] = r40
                r39 = 5
                java.lang.String r40 = "center"
                r38[r39] = r40
                r39 = 6
                java.lang.String r40 = "details"
                r38[r39] = r40
                r39 = 7
                java.lang.String r40 = "dir"
                r38[r39] = r40
                r39 = 8
                java.lang.String r40 = "div"
                r38[r39] = r40
                r39 = 9
                java.lang.String r40 = "dl"
                r38[r39] = r40
                r39 = 10
                java.lang.String r40 = "fieldset"
                r38[r39] = r40
                r39 = 11
                java.lang.String r40 = "figcaption"
                r38[r39] = r40
                r39 = 12
                java.lang.String r40 = "figure"
                r38[r39] = r40
                r39 = 13
                java.lang.String r40 = "footer"
                r38[r39] = r40
                r39 = 14
                java.lang.String r40 = "header"
                r38[r39] = r40
                r39 = 15
                java.lang.String r40 = "hgroup"
                r38[r39] = r40
                r39 = 16
                java.lang.String r40 = "listing"
                r38[r39] = r40
                r39 = 17
                java.lang.String r40 = "menu"
                r38[r39] = r40
                r39 = 18
                java.lang.String r40 = "nav"
                r38[r39] = r40
                r39 = 19
                java.lang.String r40 = "ol"
                r38[r39] = r40
                r39 = 20
                java.lang.String r40 = "pre"
                r38[r39] = r40
                r39 = 21
                java.lang.String r40 = "section"
                r38[r39] = r40
                r39 = 22
                java.lang.String r40 = "summary"
                r38[r39] = r40
                r39 = 23
                java.lang.String r40 = "ul"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x0d44
                r0 = r44
                r1 = r26
                boolean r38 = r0.inScope(r1)
                if (r38 != 0) goto L_0x0d1f
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x0d1f:
                r44.generateImpliedEndTags()
                org.jsoup.nodes.Element r38 = r44.currentElement()
                java.lang.String r38 = r38.nodeName()
                r0 = r38
                r1 = r26
                boolean r38 = r0.equals(r1)
                if (r38 != 0) goto L_0x0d3b
                r0 = r44
                r1 = r42
                r0.error(r1)
            L_0x0d3b:
                r0 = r44
                r1 = r26
                r0.popStackToClose(r1)
                goto L_0x0011
            L_0x0d44:
                java.lang.String r38 = "form"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0d98
                org.jsoup.nodes.Element r13 = r44.getFormElement()
                r38 = 0
                r0 = r44
                r1 = r38
                r0.setFormElement(r1)
                if (r13 == 0) goto L_0x0d69
                r0 = r44
                r1 = r26
                boolean r38 = r0.inScope(r1)
                if (r38 != 0) goto L_0x0d74
            L_0x0d69:
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x0d74:
                r44.generateImpliedEndTags()
                org.jsoup.nodes.Element r38 = r44.currentElement()
                java.lang.String r38 = r38.nodeName()
                r0 = r38
                r1 = r26
                boolean r38 = r0.equals(r1)
                if (r38 != 0) goto L_0x0d90
                r0 = r44
                r1 = r42
                r0.error(r1)
            L_0x0d90:
                r0 = r44
                r1 = r13
                r0.removeFromStack(r1)
                goto L_0x0011
            L_0x0d98:
                java.lang.String r38 = "p"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0df7
                r0 = r44
                r1 = r26
                boolean r38 = r0.inButtonScope(r1)
                if (r38 != 0) goto L_0x0dce
                r0 = r44
                r1 = r42
                r0.error(r1)
                org.jsoup.parser.Token$StartTag r38 = new org.jsoup.parser.Token$StartTag
                r0 = r38
                r1 = r26
                r0.<init>(r1)
                r0 = r44
                r1 = r38
                r0.process(r1)
                r0 = r44
                r1 = r15
                boolean r38 = r0.process(r1)
                goto L_0x0013
            L_0x0dce:
                r0 = r44
                r1 = r26
                r0.generateImpliedEndTags(r1)
                org.jsoup.nodes.Element r38 = r44.currentElement()
                java.lang.String r38 = r38.nodeName()
                r0 = r38
                r1 = r26
                boolean r38 = r0.equals(r1)
                if (r38 != 0) goto L_0x0dee
                r0 = r44
                r1 = r42
                r0.error(r1)
            L_0x0dee:
                r0 = r44
                r1 = r26
                r0.popStackToClose(r1)
                goto L_0x0011
            L_0x0df7:
                java.lang.String r38 = "li"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0e41
                r0 = r44
                r1 = r26
                boolean r38 = r0.inListItemScope(r1)
                if (r38 != 0) goto L_0x0e18
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x0e18:
                r0 = r44
                r1 = r26
                r0.generateImpliedEndTags(r1)
                org.jsoup.nodes.Element r38 = r44.currentElement()
                java.lang.String r38 = r38.nodeName()
                r0 = r38
                r1 = r26
                boolean r38 = r0.equals(r1)
                if (r38 != 0) goto L_0x0e38
                r0 = r44
                r1 = r42
                r0.error(r1)
            L_0x0e38:
                r0 = r44
                r1 = r26
                r0.popStackToClose(r1)
                goto L_0x0011
            L_0x0e41:
                r38 = 2
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "dd"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "dt"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x0e9d
                r0 = r44
                r1 = r26
                boolean r38 = r0.inScope(r1)
                if (r38 != 0) goto L_0x0e74
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x0e74:
                r0 = r44
                r1 = r26
                r0.generateImpliedEndTags(r1)
                org.jsoup.nodes.Element r38 = r44.currentElement()
                java.lang.String r38 = r38.nodeName()
                r0 = r38
                r1 = r26
                boolean r38 = r0.equals(r1)
                if (r38 != 0) goto L_0x0e94
                r0 = r44
                r1 = r42
                r0.error(r1)
            L_0x0e94:
                r0 = r44
                r1 = r26
                r0.popStackToClose(r1)
                goto L_0x0011
            L_0x0e9d:
                r38 = 6
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "h1"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "h2"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "h3"
                r38[r39] = r40
                r39 = 3
                java.lang.String r40 = "h4"
                r38[r39] = r40
                r39 = 4
                java.lang.String r40 = "h5"
                r38[r39] = r40
                r39 = 5
                java.lang.String r40 = "h6"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x0f69
                r38 = 6
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "h1"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "h2"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "h3"
                r38[r39] = r40
                r39 = 3
                java.lang.String r40 = "h4"
                r38[r39] = r40
                r39 = 4
                java.lang.String r40 = "h5"
                r38[r39] = r40
                r39 = 5
                java.lang.String r40 = "h6"
                r38[r39] = r40
                r0 = r44
                r1 = r38
                boolean r38 = r0.inScope(r1)
                if (r38 != 0) goto L_0x0f14
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x0f14:
                r0 = r44
                r1 = r26
                r0.generateImpliedEndTags(r1)
                org.jsoup.nodes.Element r38 = r44.currentElement()
                java.lang.String r38 = r38.nodeName()
                r0 = r38
                r1 = r26
                boolean r38 = r0.equals(r1)
                if (r38 != 0) goto L_0x0f34
                r0 = r44
                r1 = r42
                r0.error(r1)
            L_0x0f34:
                r38 = 6
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "h1"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "h2"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "h3"
                r38[r39] = r40
                r39 = 3
                java.lang.String r40 = "h4"
                r38[r39] = r40
                r39 = 4
                java.lang.String r40 = "h5"
                r38[r39] = r40
                r39 = 5
                java.lang.String r40 = "h6"
                r38[r39] = r40
                r0 = r44
                r1 = r38
                r0.popStackToClose(r1)
                goto L_0x0011
            L_0x0f69:
                java.lang.String r38 = "sarcasm"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0f7b
                boolean r38 = r42.anyOtherEndTag(r43, r44)
                goto L_0x0013
            L_0x0f7b:
                r38 = 14
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "a"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "b"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "big"
                r38[r39] = r40
                r39 = 3
                java.lang.String r40 = "code"
                r38[r39] = r40
                r39 = 4
                java.lang.String r40 = "em"
                r38[r39] = r40
                r39 = 5
                java.lang.String r40 = "font"
                r38[r39] = r40
                r39 = 6
                java.lang.String r40 = "i"
                r38[r39] = r40
                r39 = 7
                java.lang.String r40 = "nobr"
                r38[r39] = r40
                r39 = 8
                java.lang.String r40 = "s"
                r38[r39] = r40
                r39 = 9
                java.lang.String r40 = "small"
                r38[r39] = r40
                r39 = 10
                java.lang.String r40 = "strike"
                r38[r39] = r40
                r39 = 11
                java.lang.String r40 = "strong"
                r38[r39] = r40
                r39 = 12
                java.lang.String r40 = "tt"
                r38[r39] = r40
                r39 = 13
                java.lang.String r40 = "u"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x11cc
                r20 = 0
            L_0x0fe3:
                r38 = 8
                r0 = r20
                r1 = r38
                if (r0 >= r1) goto L_0x0011
                r0 = r44
                r1 = r26
                org.jsoup.nodes.Element r17 = r0.getActiveFormattingElement(r1)
                if (r17 != 0) goto L_0x0ffb
                boolean r38 = r42.anyOtherEndTag(r43, r44)
                goto L_0x0013
            L_0x0ffb:
                r0 = r44
                r1 = r17
                boolean r38 = r0.onStack(r1)
                if (r38 != 0) goto L_0x1017
                r0 = r44
                r1 = r42
                r0.error(r1)
                r0 = r44
                r1 = r17
                r0.removeFromActiveFormattingElements(r1)
                r38 = 1
                goto L_0x0013
            L_0x1017:
                java.lang.String r38 = r17.nodeName()
                r0 = r44
                r1 = r38
                boolean r38 = r0.inScope(r1)
                if (r38 != 0) goto L_0x1030
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x1030:
                org.jsoup.nodes.Element r38 = r44.currentElement()
                r0 = r38
                r1 = r17
                if (r0 == r1) goto L_0x1041
                r0 = r44
                r1 = r42
                r0.error(r1)
            L_0x1041:
                r18 = 0
                r12 = 0
                r33 = 0
                org.jsoup.helper.DescendableLinkedList r35 = r44.getStack()
                r34 = 0
            L_0x104c:
                int r38 = r35.size()
                r0 = r34
                r1 = r38
                if (r0 >= r1) goto L_0x1085
                r0 = r35
                r1 = r34
                java.lang.Object r14 = r0.get(r1)
                org.jsoup.nodes.Element r14 = (org.jsoup.nodes.Element) r14
                r0 = r14
                r1 = r17
                if (r0 != r1) goto L_0x1078
                r38 = 1
                int r38 = r34 - r38
                r0 = r35
                r1 = r38
                java.lang.Object r12 = r0.get(r1)
                org.jsoup.nodes.Element r12 = (org.jsoup.nodes.Element) r12
                r33 = 1
            L_0x1075:
                int r34 = r34 + 1
                goto L_0x104c
            L_0x1078:
                if (r33 == 0) goto L_0x1075
                r0 = r44
                r1 = r14
                boolean r38 = r0.isSpecial(r1)
                if (r38 == 0) goto L_0x1075
                r18 = r14
            L_0x1085:
                if (r18 != 0) goto L_0x109d
                java.lang.String r38 = r17.nodeName()
                r0 = r44
                r1 = r38
                r0.popStackToClose(r1)
                r0 = r44
                r1 = r17
                r0.removeFromActiveFormattingElements(r1)
                r38 = 1
                goto L_0x0013
            L_0x109d:
                r27 = r18
                r24 = r18
                r23 = 0
            L_0x10a3:
                r38 = 3
                r0 = r23
                r1 = r38
                if (r0 >= r1) goto L_0x10d7
                r0 = r44
                r1 = r27
                boolean r38 = r0.onStack(r1)
                if (r38 == 0) goto L_0x10bd
                r0 = r44
                r1 = r27
                org.jsoup.nodes.Element r27 = r0.aboveOnStack(r1)
            L_0x10bd:
                r0 = r44
                r1 = r27
                boolean r38 = r0.isInActiveFormattingElements(r1)
                if (r38 != 0) goto L_0x10d1
                r0 = r44
                r1 = r27
                r0.removeFromStack(r1)
            L_0x10ce:
                int r23 = r23 + 1
                goto L_0x10a3
            L_0x10d1:
                r0 = r27
                r1 = r17
                if (r0 != r1) goto L_0x1156
            L_0x10d7:
                java.lang.String r38 = r12.nodeName()
                r39 = 5
                r0 = r39
                java.lang.String[] r0 = new java.lang.String[r0]
                r39 = r0
                r40 = 0
                java.lang.String r41 = "table"
                r39[r40] = r41
                r40 = 1
                java.lang.String r41 = "tbody"
                r39[r40] = r41
                r40 = 2
                java.lang.String r41 = "tfoot"
                r39[r40] = r41
                r40 = 3
                java.lang.String r41 = "thead"
                r39[r40] = r41
                r40 = 4
                java.lang.String r41 = "tr"
                r39[r40] = r41
                boolean r38 = org.jsoup.helper.StringUtil.in(r38, r39)
                if (r38 == 0) goto L_0x119b
                org.jsoup.nodes.Element r38 = r24.parent()
                if (r38 == 0) goto L_0x1110
                r24.remove()
            L_0x1110:
                r0 = r44
                r1 = r24
                r0.insertInFosterParent(r1)
            L_0x1117:
                org.jsoup.nodes.Element r4 = new org.jsoup.nodes.Element
                org.jsoup.parser.Tag r38 = org.jsoup.parser.Tag.valueOf(r26)
                java.lang.String r39 = r44.getBaseUri()
                r0 = r4
                r1 = r38
                r2 = r39
                r0.<init>(r1, r2)
                java.util.List r38 = r18.childNodes()
                java.util.List r39 = r18.childNodes()
                int r39 = r39.size()
                r0 = r39
                org.jsoup.nodes.Node[] r0 = new org.jsoup.nodes.Node[r0]
                r39 = r0
                java.lang.Object[] r11 = r38.toArray(r39)
                org.jsoup.nodes.Node[] r11 = (org.jsoup.nodes.Node[]) r11
                r5 = r11
                r0 = r5
                int r0 = r0.length
                r25 = r0
                r21 = 0
            L_0x1148:
                r0 = r21
                r1 = r25
                if (r0 >= r1) goto L_0x11ac
                r10 = r5[r21]
                r4.appendChild(r10)
                int r21 = r21 + 1
                goto L_0x1148
            L_0x1156:
                org.jsoup.nodes.Element r31 = new org.jsoup.nodes.Element
                java.lang.String r38 = r27.nodeName()
                org.jsoup.parser.Tag r38 = org.jsoup.parser.Tag.valueOf(r38)
                java.lang.String r39 = r44.getBaseUri()
                r0 = r31
                r1 = r38
                r2 = r39
                r0.<init>(r1, r2)
                r0 = r44
                r1 = r27
                r2 = r31
                r0.replaceActiveFormattingElement(r1, r2)
                r0 = r44
                r1 = r27
                r2 = r31
                r0.replaceOnStack(r1, r2)
                r27 = r31
                r0 = r24
                r1 = r18
                if (r0 != r1) goto L_0x1187
            L_0x1187:
                org.jsoup.nodes.Element r38 = r24.parent()
                if (r38 == 0) goto L_0x1190
                r24.remove()
            L_0x1190:
                r0 = r27
                r1 = r24
                r0.appendChild(r1)
                r24 = r27
                goto L_0x10ce
            L_0x119b:
                org.jsoup.nodes.Element r38 = r24.parent()
                if (r38 == 0) goto L_0x11a4
                r24.remove()
            L_0x11a4:
                r0 = r12
                r1 = r24
                r0.appendChild(r1)
                goto L_0x1117
            L_0x11ac:
                r0 = r18
                r1 = r4
                r0.appendChild(r1)
                r0 = r44
                r1 = r17
                r0.removeFromActiveFormattingElements(r1)
                r0 = r44
                r1 = r17
                r0.removeFromStack(r1)
                r0 = r44
                r1 = r18
                r2 = r4
                r0.insertOnStackAfter(r1, r2)
                int r20 = r20 + 1
                goto L_0x0fe3
            L_0x11cc:
                r38 = 3
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "applet"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "marquee"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "object"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x1239
                java.lang.String r38 = "name"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inScope(r1)
                if (r38 != 0) goto L_0x0011
                r0 = r44
                r1 = r26
                boolean r38 = r0.inScope(r1)
                if (r38 != 0) goto L_0x1211
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x1211:
                r44.generateImpliedEndTags()
                org.jsoup.nodes.Element r38 = r44.currentElement()
                java.lang.String r38 = r38.nodeName()
                r0 = r38
                r1 = r26
                boolean r38 = r0.equals(r1)
                if (r38 != 0) goto L_0x122d
                r0 = r44
                r1 = r42
                r0.error(r1)
            L_0x122d:
                r0 = r44
                r1 = r26
                r0.popStackToClose(r1)
                r44.clearFormattingElementsToLastMarker()
                goto L_0x0011
            L_0x1239:
                java.lang.String r38 = "br"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x125e
                r0 = r44
                r1 = r42
                r0.error(r1)
                org.jsoup.parser.Token$StartTag r38 = new org.jsoup.parser.Token$StartTag
                java.lang.String r39 = "br"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
                r38 = 0
                goto L_0x0013
            L_0x125e:
                boolean r38 = r42.anyOtherEndTag(r43, r44)
                goto L_0x0013
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.TreeBuilderState.AnonymousClass7.process(org.jsoup.parser.Token, org.jsoup.parser.TreeBuilder):boolean");
        }

        /* access modifiers changed from: package-private */
        public boolean anyOtherEndTag(Token t, TreeBuilder tb) {
            Element node;
            String name = t.asEndTag().name();
            Iterator<Element> it = tb.getStack().descendingIterator();
            do {
                if (it.hasNext()) {
                    node = it.next();
                    if (node.nodeName().equals(name)) {
                        tb.generateImpliedEndTags(name);
                        if (!name.equals(tb.currentElement().nodeName())) {
                            tb.error(this);
                        }
                        tb.popStackToClose(name);
                    }
                }
                return true;
            } while (!tb.isSpecial(node));
            tb.error(this);
            return false;
        }
    },
    Text {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (t.isCharacter()) {
                tb.insert(t.asCharacter());
            } else if (t.isEOF()) {
                tb.error(this);
                tb.pop();
                tb.transition(tb.originalState());
                return tb.process(t);
            } else if (t.isEndTag()) {
                tb.pop();
                tb.transition(tb.originalState());
            }
            return true;
        }
    },
    InTable {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (t.isCharacter()) {
                tb.newPendingTableCharacters();
                tb.markInsertionMode();
                tb.transition(InTableText);
                return tb.process(t);
            }
            if (t.isComment()) {
                tb.insert(t.asComment());
            } else if (t.isDoctype()) {
                tb.error(this);
                return false;
            } else if (t.isStartTag()) {
                Token.StartTag startTag = t.asStartTag();
                String name = startTag.name();
                if (name.equals("caption")) {
                    tb.clearStackToTableContext();
                    tb.insertMarkerToFormattingElements();
                    tb.insert(startTag);
                    tb.transition(InCaption);
                } else if (name.equals("colgroup")) {
                    tb.clearStackToTableContext();
                    tb.insert(startTag);
                    tb.transition(InColumnGroup);
                } else if (name.equals("col")) {
                    tb.process(new Token.StartTag("colgroup"));
                    return tb.process(t);
                } else {
                    if (StringUtil.in(name, "tbody", "tfoot", "thead")) {
                        tb.clearStackToTableContext();
                        tb.insert(startTag);
                        tb.transition(InTableBody);
                    } else {
                        if (StringUtil.in(name, "td", "th", "tr")) {
                            tb.process(new Token.StartTag("tbody"));
                            return tb.process(t);
                        } else if (name.equals("table")) {
                            tb.error(this);
                            if (tb.process(new Token.EndTag("table"))) {
                                return tb.process(t);
                            }
                        } else {
                            if (StringUtil.in(name, "style", "script")) {
                                return tb.process(t, InHead);
                            }
                            if (name.equals("input")) {
                                if (!startTag.attributes.get("type").equalsIgnoreCase("hidden")) {
                                    return anythingElse(t, tb);
                                }
                                tb.insertEmpty(startTag);
                            } else if (!name.equals("form")) {
                                return anythingElse(t, tb);
                            } else {
                                tb.error(this);
                                if (tb.getFormElement() != null) {
                                    return false;
                                }
                                tb.setFormElement(tb.insertEmpty(startTag));
                            }
                        }
                    }
                }
            } else if (t.isEndTag()) {
                String name2 = t.asEndTag().name();
                if (!name2.equals("table")) {
                    if (!StringUtil.in(name2, "body", "caption", "col", "colgroup", AdActivity.HTML_PARAM, "tbody", "td", "tfoot", "th", "thead", "tr")) {
                        return anythingElse(t, tb);
                    }
                    tb.error(this);
                    return false;
                } else if (!tb.inTableScope(name2)) {
                    tb.error(this);
                    return false;
                } else {
                    tb.popStackToClose("table");
                    tb.resetInsertionMode();
                }
            } else if (t.isEOF()) {
                if (tb.currentElement().nodeName().equals(AdActivity.HTML_PARAM)) {
                    tb.error(this);
                }
                return true;
            }
            return anythingElse(t, tb);
        }

        /* access modifiers changed from: package-private */
        public boolean anythingElse(Token t, TreeBuilder tb) {
            tb.error(this);
            if (!StringUtil.in(tb.currentElement().nodeName(), "table", "tbody", "tfoot", "thead", "tr")) {
                return tb.process(t, InBody);
            }
            tb.setFosterInserts(true);
            boolean processed = tb.process(t, InBody);
            tb.setFosterInserts(false);
            return processed;
        }
    },
    InTableText {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            switch (AnonymousClass24.$SwitchMap$org$jsoup$parser$Token$TokenType[t.type.ordinal()]) {
                case AdWhirlUtil.NETWORK_TYPE_LIVERAIL:
                    Token.Character c = t.asCharacter();
                    if (c.getData().equals(TreeBuilderState.access$400())) {
                        tb.error(this);
                        return false;
                    }
                    tb.getPendingTableCharacters().add(c);
                    return true;
                default:
                    if (tb.getPendingTableCharacters().size() > 0) {
                        for (Token.Character character : tb.getPendingTableCharacters()) {
                            if (!TreeBuilderState.access$100(character)) {
                                tb.error(this);
                                if (StringUtil.in(tb.currentElement().nodeName(), "table", "tbody", "tfoot", "thead", "tr")) {
                                    tb.setFosterInserts(true);
                                    tb.process(character, InBody);
                                    tb.setFosterInserts(false);
                                } else {
                                    tb.process(character, InBody);
                                }
                            } else {
                                tb.insert(character);
                            }
                        }
                        tb.newPendingTableCharacters();
                    }
                    tb.transition(tb.originalState());
                    return tb.process(t);
            }
        }
    },
    InCaption {
        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0092, code lost:
            if (org.jsoup.helper.StringUtil.in(r13.asStartTag().name(), "caption", "col", "colgroup", "tbody", "td", "tfoot", "th", "thead", "tr") == false) goto L_0x0094;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean process(org.jsoup.parser.Token r13, org.jsoup.parser.TreeBuilder r14) {
            /*
                r12 = this;
                r11 = 4
                r10 = 3
                r9 = 2
                r8 = 1
                r7 = 0
                boolean r3 = r13.isEndTag()
                if (r3 == 0) goto L_0x0053
                org.jsoup.parser.Token$EndTag r3 = r13.asEndTag()
                java.lang.String r3 = r3.name()
                java.lang.String r4 = "caption"
                boolean r3 = r3.equals(r4)
                if (r3 == 0) goto L_0x0053
                org.jsoup.parser.Token$EndTag r0 = r13.asEndTag()
                java.lang.String r1 = r0.name()
                boolean r3 = r14.inTableScope(r1)
                if (r3 != 0) goto L_0x002e
                r14.error(r12)
                r3 = r7
            L_0x002d:
                return r3
            L_0x002e:
                r14.generateImpliedEndTags()
                org.jsoup.nodes.Element r3 = r14.currentElement()
                java.lang.String r3 = r3.nodeName()
                java.lang.String r4 = "caption"
                boolean r3 = r3.equals(r4)
                if (r3 != 0) goto L_0x0044
                r14.error(r12)
            L_0x0044:
                java.lang.String r3 = "caption"
                r14.popStackToClose(r3)
                r14.clearFormattingElementsToLastMarker()
                org.jsoup.parser.TreeBuilderState r3 = org.jsoup.parser.TreeBuilderState.AnonymousClass11.InTable
                r14.transition(r3)
            L_0x0051:
                r3 = r8
                goto L_0x002d
            L_0x0053:
                boolean r3 = r13.isStartTag()
                if (r3 == 0) goto L_0x0094
                org.jsoup.parser.Token$StartTag r3 = r13.asStartTag()
                java.lang.String r3 = r3.name()
                r4 = 9
                java.lang.String[] r4 = new java.lang.String[r4]
                java.lang.String r5 = "caption"
                r4[r7] = r5
                java.lang.String r5 = "col"
                r4[r8] = r5
                java.lang.String r5 = "colgroup"
                r4[r9] = r5
                java.lang.String r5 = "tbody"
                r4[r10] = r5
                java.lang.String r5 = "td"
                r4[r11] = r5
                r5 = 5
                java.lang.String r6 = "tfoot"
                r4[r5] = r6
                r5 = 6
                java.lang.String r6 = "th"
                r4[r5] = r6
                r5 = 7
                java.lang.String r6 = "thead"
                r4[r5] = r6
                r5 = 8
                java.lang.String r6 = "tr"
                r4[r5] = r6
                boolean r3 = org.jsoup.helper.StringUtil.in(r3, r4)
                if (r3 != 0) goto L_0x00aa
            L_0x0094:
                boolean r3 = r13.isEndTag()
                if (r3 == 0) goto L_0x00c0
                org.jsoup.parser.Token$EndTag r3 = r13.asEndTag()
                java.lang.String r3 = r3.name()
                java.lang.String r4 = "table"
                boolean r3 = r3.equals(r4)
                if (r3 == 0) goto L_0x00c0
            L_0x00aa:
                r14.error(r12)
                org.jsoup.parser.Token$EndTag r3 = new org.jsoup.parser.Token$EndTag
                java.lang.String r4 = "caption"
                r3.<init>(r4)
                boolean r2 = r14.process(r3)
                if (r2 == 0) goto L_0x0051
                boolean r3 = r14.process(r13)
                goto L_0x002d
            L_0x00c0:
                boolean r3 = r13.isEndTag()
                if (r3 == 0) goto L_0x010d
                org.jsoup.parser.Token$EndTag r3 = r13.asEndTag()
                java.lang.String r3 = r3.name()
                r4 = 10
                java.lang.String[] r4 = new java.lang.String[r4]
                java.lang.String r5 = "body"
                r4[r7] = r5
                java.lang.String r5 = "col"
                r4[r8] = r5
                java.lang.String r5 = "colgroup"
                r4[r9] = r5
                java.lang.String r5 = "html"
                r4[r10] = r5
                java.lang.String r5 = "tbody"
                r4[r11] = r5
                r5 = 5
                java.lang.String r6 = "td"
                r4[r5] = r6
                r5 = 6
                java.lang.String r6 = "tfoot"
                r4[r5] = r6
                r5 = 7
                java.lang.String r6 = "th"
                r4[r5] = r6
                r5 = 8
                java.lang.String r6 = "thead"
                r4[r5] = r6
                r5 = 9
                java.lang.String r6 = "tr"
                r4[r5] = r6
                boolean r3 = org.jsoup.helper.StringUtil.in(r3, r4)
                if (r3 == 0) goto L_0x010d
                r14.error(r12)
                r3 = r7
                goto L_0x002d
            L_0x010d:
                org.jsoup.parser.TreeBuilderState r3 = org.jsoup.parser.TreeBuilderState.AnonymousClass11.InBody
                boolean r3 = r14.process(r13, r3)
                goto L_0x002d
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.TreeBuilderState.AnonymousClass11.process(org.jsoup.parser.Token, org.jsoup.parser.TreeBuilder):boolean");
        }
    },
    InColumnGroup {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (TreeBuilderState.access$100(t)) {
                tb.insert(t.asCharacter());
                return true;
            }
            switch (AnonymousClass24.$SwitchMap$org$jsoup$parser$Token$TokenType[t.type.ordinal()]) {
                case 1:
                    tb.insert(t.asComment());
                    break;
                case 2:
                    tb.error(this);
                    break;
                case 3:
                    Token.StartTag startTag = t.asStartTag();
                    String name = startTag.name();
                    if (name.equals(AdActivity.HTML_PARAM)) {
                        return tb.process(t, InBody);
                    }
                    if (name.equals("col")) {
                        tb.insertEmpty(startTag);
                        break;
                    } else {
                        return anythingElse(t, tb);
                    }
                case AdWhirlUtil.NETWORK_TYPE_MEDIALETS:
                    if (t.asEndTag().name().equals("colgroup")) {
                        if (!tb.currentElement().nodeName().equals(AdActivity.HTML_PARAM)) {
                            tb.pop();
                            tb.transition(InTable);
                            break;
                        } else {
                            tb.error(this);
                            return false;
                        }
                    } else {
                        return anythingElse(t, tb);
                    }
                case AdWhirlUtil.NETWORK_TYPE_LIVERAIL:
                default:
                    return anythingElse(t, tb);
                case AdWhirlUtil.NETWORK_TYPE_MILLENNIAL:
                    if (tb.currentElement().nodeName().equals(AdActivity.HTML_PARAM)) {
                        return true;
                    }
                    return anythingElse(t, tb);
            }
            return true;
        }

        private boolean anythingElse(Token t, TreeBuilder tb) {
            if (tb.process(new Token.EndTag("colgroup"))) {
                return tb.process(t);
            }
            return true;
        }
    },
    InTableBody {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            switch (AnonymousClass24.$SwitchMap$org$jsoup$parser$Token$TokenType[t.type.ordinal()]) {
                case 3:
                    Token.StartTag startTag = t.asStartTag();
                    String name = startTag.name();
                    if (name.equals("tr")) {
                        tb.clearStackToTableBodyContext();
                        tb.insert(startTag);
                        tb.transition(InRow);
                        break;
                    } else {
                        if (StringUtil.in(name, "th", "td")) {
                            tb.error(this);
                            tb.process(new Token.StartTag("tr"));
                            return tb.process(startTag);
                        }
                        if (StringUtil.in(name, "caption", "col", "colgroup", "tbody", "tfoot", "thead")) {
                            return exitTableBody(t, tb);
                        }
                        return anythingElse(t, tb);
                    }
                case AdWhirlUtil.NETWORK_TYPE_MEDIALETS:
                    String name2 = t.asEndTag().name();
                    if (StringUtil.in(name2, "tbody", "tfoot", "thead")) {
                        if (tb.inTableScope(name2)) {
                            tb.clearStackToTableBodyContext();
                            tb.pop();
                            tb.transition(InTable);
                            break;
                        } else {
                            tb.error(this);
                            return false;
                        }
                    } else if (name2.equals("table")) {
                        return exitTableBody(t, tb);
                    } else {
                        if (!StringUtil.in(name2, "body", "caption", "col", "colgroup", AdActivity.HTML_PARAM, "td", "th", "tr")) {
                            return anythingElse(t, tb);
                        }
                        tb.error(this);
                        return false;
                    }
                default:
                    return anythingElse(t, tb);
            }
            return true;
        }

        private boolean exitTableBody(Token t, TreeBuilder tb) {
            if (tb.inTableScope("tbody") || tb.inTableScope("thead") || tb.inScope("tfoot")) {
                tb.clearStackToTableBodyContext();
                tb.process(new Token.EndTag(tb.currentElement().nodeName()));
                return tb.process(t);
            }
            tb.error(this);
            return false;
        }

        private boolean anythingElse(Token t, TreeBuilder tb) {
            return tb.process(t, InTable);
        }
    },
    InRow {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (t.isStartTag()) {
                Token.StartTag startTag = t.asStartTag();
                String name = startTag.name();
                if (StringUtil.in(name, "th", "td")) {
                    tb.clearStackToTableRowContext();
                    tb.insert(startTag);
                    tb.transition(InCell);
                    tb.insertMarkerToFormattingElements();
                } else {
                    if (StringUtil.in(name, "caption", "col", "colgroup", "tbody", "tfoot", "thead", "tr")) {
                        return handleMissingTr(t, tb);
                    }
                    return anythingElse(t, tb);
                }
            } else if (!t.isEndTag()) {
                return anythingElse(t, tb);
            } else {
                String name2 = t.asEndTag().name();
                if (name2.equals("tr")) {
                    if (!tb.inTableScope(name2)) {
                        tb.error(this);
                        return false;
                    }
                    tb.clearStackToTableRowContext();
                    tb.pop();
                    tb.transition(InTableBody);
                } else if (name2.equals("table")) {
                    return handleMissingTr(t, tb);
                } else {
                    if (!StringUtil.in(name2, "tbody", "tfoot", "thead")) {
                        if (!StringUtil.in(name2, "body", "caption", "col", "colgroup", AdActivity.HTML_PARAM, "td", "th")) {
                            return anythingElse(t, tb);
                        }
                        tb.error(this);
                        return false;
                    } else if (!tb.inTableScope(name2)) {
                        tb.error(this);
                        return false;
                    } else {
                        tb.process(new Token.EndTag("tr"));
                        return tb.process(t);
                    }
                }
            }
            return true;
        }

        private boolean anythingElse(Token t, TreeBuilder tb) {
            return tb.process(t, InTable);
        }

        private boolean handleMissingTr(Token t, TreeBuilder tb) {
            if (tb.process(new Token.EndTag("tr"))) {
                return tb.process(t);
            }
            return false;
        }
    },
    InCell {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (t.isEndTag()) {
                String name = t.asEndTag().name();
                if (!StringUtil.in(name, "td", "th")) {
                    if (StringUtil.in(name, "body", "caption", "col", "colgroup", AdActivity.HTML_PARAM)) {
                        tb.error(this);
                        return false;
                    }
                    if (!StringUtil.in(name, "table", "tbody", "tfoot", "thead", "tr")) {
                        return anythingElse(t, tb);
                    }
                    if (!tb.inTableScope(name)) {
                        tb.error(this);
                        return false;
                    }
                    closeCell(tb);
                    return tb.process(t);
                } else if (!tb.inTableScope(name)) {
                    tb.error(this);
                    tb.transition(InRow);
                    return false;
                } else {
                    tb.generateImpliedEndTags();
                    if (!tb.currentElement().nodeName().equals(name)) {
                        tb.error(this);
                    }
                    tb.popStackToClose(name);
                    tb.clearFormattingElementsToLastMarker();
                    tb.transition(InRow);
                    return true;
                }
            } else {
                if (t.isStartTag()) {
                    if (StringUtil.in(t.asStartTag().name(), "caption", "col", "colgroup", "tbody", "td", "tfoot", "th", "thead", "tr")) {
                        if (tb.inTableScope("td") || tb.inTableScope("th")) {
                            closeCell(tb);
                            return tb.process(t);
                        }
                        tb.error(this);
                        return false;
                    }
                }
                return anythingElse(t, tb);
            }
        }

        private boolean anythingElse(Token t, TreeBuilder tb) {
            return tb.process(t, InBody);
        }

        private void closeCell(TreeBuilder tb) {
            if (tb.inTableScope("td")) {
                tb.process(new Token.EndTag("td"));
            } else {
                tb.process(new Token.EndTag("th"));
            }
        }
    },
    InSelect {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            switch (AnonymousClass24.$SwitchMap$org$jsoup$parser$Token$TokenType[t.type.ordinal()]) {
                case 1:
                    tb.insert(t.asComment());
                    break;
                case 2:
                    tb.error(this);
                    return false;
                case 3:
                    Token.StartTag start = t.asStartTag();
                    String name = start.name();
                    if (name.equals(AdActivity.HTML_PARAM)) {
                        return tb.process(start, InBody);
                    }
                    if (name.equals("option")) {
                        tb.process(new Token.EndTag("option"));
                        tb.insert(start);
                        break;
                    } else if (name.equals("optgroup")) {
                        if (tb.currentElement().nodeName().equals("option")) {
                            tb.process(new Token.EndTag("option"));
                        } else if (tb.currentElement().nodeName().equals("optgroup")) {
                            tb.process(new Token.EndTag("optgroup"));
                        }
                        tb.insert(start);
                        break;
                    } else if (name.equals("select")) {
                        tb.error(this);
                        return tb.process(new Token.EndTag("select"));
                    } else {
                        if (StringUtil.in(name, "input", "keygen", "textarea")) {
                            tb.error(this);
                            if (!tb.inSelectScope("select")) {
                                return false;
                            }
                            tb.process(new Token.EndTag("select"));
                            return tb.process(start);
                        } else if (name.equals("script")) {
                            return tb.process(t, InHead);
                        } else {
                            return anythingElse(t, tb);
                        }
                    }
                case AdWhirlUtil.NETWORK_TYPE_MEDIALETS:
                    String name2 = t.asEndTag().name();
                    if (name2.equals("optgroup")) {
                        if (tb.currentElement().nodeName().equals("option") && tb.aboveOnStack(tb.currentElement()) != null && tb.aboveOnStack(tb.currentElement()).nodeName().equals("optgroup")) {
                            tb.process(new Token.EndTag("option"));
                        }
                        if (!tb.currentElement().nodeName().equals("optgroup")) {
                            tb.error(this);
                            break;
                        } else {
                            tb.pop();
                            break;
                        }
                    } else if (name2.equals("option")) {
                        if (!tb.currentElement().nodeName().equals("option")) {
                            tb.error(this);
                            break;
                        } else {
                            tb.pop();
                            break;
                        }
                    } else if (name2.equals("select")) {
                        if (tb.inSelectScope(name2)) {
                            tb.popStackToClose(name2);
                            tb.resetInsertionMode();
                            break;
                        } else {
                            tb.error(this);
                            return false;
                        }
                    } else {
                        return anythingElse(t, tb);
                    }
                case AdWhirlUtil.NETWORK_TYPE_LIVERAIL:
                    Token.Character c = t.asCharacter();
                    if (!c.getData().equals(TreeBuilderState.access$400())) {
                        tb.insert(c);
                        break;
                    } else {
                        tb.error(this);
                        return false;
                    }
                case AdWhirlUtil.NETWORK_TYPE_MILLENNIAL:
                    if (!tb.currentElement().nodeName().equals(AdActivity.HTML_PARAM)) {
                        tb.error(this);
                        break;
                    }
                    break;
                default:
                    return anythingElse(t, tb);
            }
            return true;
        }

        private boolean anythingElse(Token t, TreeBuilder tb) {
            tb.error(this);
            return false;
        }
    },
    InSelectInTable {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (t.isStartTag()) {
                if (StringUtil.in(t.asStartTag().name(), "caption", "table", "tbody", "tfoot", "thead", "tr", "td", "th")) {
                    tb.error(this);
                    tb.process(new Token.EndTag("select"));
                    return tb.process(t);
                }
            }
            if (t.isEndTag()) {
                if (StringUtil.in(t.asEndTag().name(), "caption", "table", "tbody", "tfoot", "thead", "tr", "td", "th")) {
                    tb.error(this);
                    if (!tb.inTableScope(t.asEndTag().name())) {
                        return false;
                    }
                    tb.process(new Token.EndTag("select"));
                    return tb.process(t);
                }
            }
            return tb.process(t, InSelect);
        }
    },
    AfterBody {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (TreeBuilderState.access$100(t)) {
                return tb.process(t, InBody);
            }
            if (t.isComment()) {
                tb.insert(t.asComment());
            } else if (t.isDoctype()) {
                tb.error(this);
                return false;
            } else if (t.isStartTag() && t.asStartTag().name().equals(AdActivity.HTML_PARAM)) {
                return tb.process(t, InBody);
            } else {
                if (!t.isEndTag() || !t.asEndTag().name().equals(AdActivity.HTML_PARAM)) {
                    if (!t.isEOF()) {
                        tb.error(this);
                        tb.transition(InBody);
                        return tb.process(t);
                    }
                } else if (tb.isFragmentParsing()) {
                    tb.error(this);
                    return false;
                } else {
                    tb.transition(AfterAfterBody);
                }
            }
            return true;
        }
    },
    InFrameset {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (TreeBuilderState.access$100(t)) {
                tb.insert(t.asCharacter());
            } else if (t.isComment()) {
                tb.insert(t.asComment());
            } else if (t.isDoctype()) {
                tb.error(this);
                return false;
            } else if (t.isStartTag()) {
                Token.StartTag start = t.asStartTag();
                String name = start.name();
                if (name.equals(AdActivity.HTML_PARAM)) {
                    return tb.process(start, InBody);
                }
                if (name.equals("frameset")) {
                    tb.insert(start);
                } else if (name.equals("frame")) {
                    tb.insertEmpty(start);
                } else if (name.equals("noframes")) {
                    return tb.process(start, InHead);
                } else {
                    tb.error(this);
                    return false;
                }
            } else if (!t.isEndTag() || !t.asEndTag().name().equals("frameset")) {
                if (!t.isEOF()) {
                    tb.error(this);
                    return false;
                } else if (!tb.currentElement().nodeName().equals(AdActivity.HTML_PARAM)) {
                    tb.error(this);
                    return true;
                }
            } else if (tb.currentElement().nodeName().equals(AdActivity.HTML_PARAM)) {
                tb.error(this);
                return false;
            } else {
                tb.pop();
                if (!tb.isFragmentParsing() && !tb.currentElement().nodeName().equals("frameset")) {
                    tb.transition(AfterFrameset);
                }
            }
            return true;
        }
    },
    AfterFrameset {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (TreeBuilderState.access$100(t)) {
                tb.insert(t.asCharacter());
            } else if (t.isComment()) {
                tb.insert(t.asComment());
            } else if (t.isDoctype()) {
                tb.error(this);
                return false;
            } else if (t.isStartTag() && t.asStartTag().name().equals(AdActivity.HTML_PARAM)) {
                return tb.process(t, InBody);
            } else {
                if (t.isEndTag() && t.asEndTag().name().equals(AdActivity.HTML_PARAM)) {
                    tb.transition(AfterAfterFrameset);
                } else if (t.isStartTag() && t.asStartTag().name().equals("noframes")) {
                    return tb.process(t, InHead);
                } else {
                    if (!t.isEOF()) {
                        tb.error(this);
                        return false;
                    }
                }
            }
            return true;
        }
    },
    AfterAfterBody {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (t.isComment()) {
                tb.insert(t.asComment());
            } else if (t.isDoctype() || TreeBuilderState.access$100(t) || (t.isStartTag() && t.asStartTag().name().equals(AdActivity.HTML_PARAM))) {
                return tb.process(t, InBody);
            } else {
                if (!t.isEOF()) {
                    tb.error(this);
                    tb.transition(InBody);
                    return tb.process(t);
                }
            }
            return true;
        }
    },
    AfterAfterFrameset {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (t.isComment()) {
                tb.insert(t.asComment());
            } else if (t.isDoctype() || TreeBuilderState.access$100(t) || (t.isStartTag() && t.asStartTag().name().equals(AdActivity.HTML_PARAM))) {
                return tb.process(t, InBody);
            } else {
                if (!t.isEOF()) {
                    if (t.isStartTag() && t.asStartTag().name().equals("nofrmes")) {
                        return tb.process(t, InHead);
                    }
                    tb.error(this);
                    tb.transition(InBody);
                    return tb.process(t);
                }
            }
            return true;
        }
    },
    ForeignContent {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            return true;
        }
    };
    
    private static String nullString = String.valueOf(0);

    /* access modifiers changed from: package-private */
    public abstract boolean process(Token token, TreeBuilder treeBuilder);

    /* renamed from: org.jsoup.parser.TreeBuilderState$24  reason: invalid class name */
    static /* synthetic */ class AnonymousClass24 {
        static final /* synthetic */ int[] $SwitchMap$org$jsoup$parser$Token$TokenType = new int[Token.TokenType.values().length];

        static {
            try {
                $SwitchMap$org$jsoup$parser$Token$TokenType[Token.TokenType.Comment.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$org$jsoup$parser$Token$TokenType[Token.TokenType.Doctype.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$org$jsoup$parser$Token$TokenType[Token.TokenType.StartTag.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$org$jsoup$parser$Token$TokenType[Token.TokenType.EndTag.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$org$jsoup$parser$Token$TokenType[Token.TokenType.Character.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$org$jsoup$parser$Token$TokenType[Token.TokenType.EOF.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
        }
    }

    private static boolean isWhitespace(Token t) {
        if (!t.isCharacter()) {
            return false;
        }
        String data = t.asCharacter().getData();
        for (int i = 0; i < data.length(); i++) {
            if (!Character.isWhitespace(data.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    private static void handleRcData(Token.StartTag startTag, TreeBuilder tb) {
        tb.insert(startTag);
        tb.tokeniser.transition(TokeniserState.Rcdata);
        tb.markInsertionMode();
        tb.transition(Text);
    }

    private static void handleRawtext(Token.StartTag startTag, TreeBuilder tb) {
        tb.insert(startTag);
        tb.tokeniser.transition(TokeniserState.Rawtext);
        tb.markInsertionMode();
        tb.transition(Text);
    }
}
