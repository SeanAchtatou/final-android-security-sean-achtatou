package org.jsoup.parser;

import java.util.ArrayList;
import java.util.List;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Entities;
import org.jsoup.parser.Token;

class Tokeniser {
    static final char replacementChar = '�';
    private StringBuilder charBuffer = new StringBuilder();
    Token.Comment commentPending;
    StringBuilder dataBuffer;
    Token.Doctype doctypePending;
    private Token emitPending;
    private List<ParseError> errors = new ArrayList();
    private boolean isEmitPending = false;
    private Token.StartTag lastStartTag;
    private CharacterReader reader;
    private boolean selfClosingFlagAcknowledged = true;
    private TokeniserState state = TokeniserState.Data;
    Token.Tag tagPending;
    private boolean trackErrors = true;

    Tokeniser(CharacterReader reader2) {
        this.reader = reader2;
    }

    /* access modifiers changed from: package-private */
    public Token read() {
        if (!this.selfClosingFlagAcknowledged) {
            error("Self closing flag not acknowledged");
            this.selfClosingFlagAcknowledged = true;
        }
        while (!this.isEmitPending) {
            this.state.read(this, this.reader);
        }
        if (this.charBuffer.length() > 0) {
            String str = this.charBuffer.toString();
            this.charBuffer.delete(0, this.charBuffer.length());
            return new Token.Character(str);
        }
        this.isEmitPending = false;
        return this.emitPending;
    }

    /* access modifiers changed from: package-private */
    public void emit(Token token) {
        Validate.isFalse(this.isEmitPending, "There is an unread token pending!");
        this.emitPending = token;
        this.isEmitPending = true;
        if (token.type == Token.TokenType.StartTag) {
            Token.StartTag startTag = (Token.StartTag) token;
            this.lastStartTag = startTag;
            if (startTag.selfClosing) {
                this.selfClosingFlagAcknowledged = false;
            }
        } else if (token.type == Token.TokenType.EndTag && ((Token.EndTag) token).attributes.size() > 0) {
            error("Attributes incorrectly present on end tag");
        }
    }

    /* access modifiers changed from: package-private */
    public void emit(String str) {
        this.charBuffer.append(str);
    }

    /* access modifiers changed from: package-private */
    public void emit(char c) {
        this.charBuffer.append(c);
    }

    /* access modifiers changed from: package-private */
    public TokeniserState getState() {
        return this.state;
    }

    /* access modifiers changed from: package-private */
    public void transition(TokeniserState state2) {
        this.state = state2;
    }

    /* access modifiers changed from: package-private */
    public void advanceTransition(TokeniserState state2) {
        this.reader.advance();
        this.state = state2;
    }

    /* access modifiers changed from: package-private */
    public void acknowledgeSelfClosingFlag() {
        this.selfClosingFlagAcknowledged = true;
    }

    /* access modifiers changed from: package-private */
    public Character consumeCharacterReference(Character additionalAllowedCharacter, boolean inAttribute) {
        if (this.reader.isEmpty()) {
            return null;
        }
        if (additionalAllowedCharacter != null && additionalAllowedCharacter.charValue() == this.reader.current()) {
            return null;
        }
        if (this.reader.matchesAny(9, 10, 12, '<', '&')) {
            return null;
        }
        this.reader.mark();
        if (this.reader.matchConsume("#")) {
            boolean isHexMode = this.reader.matchConsumeIgnoreCase("X");
            String numRef = isHexMode ? this.reader.consumeHexSequence() : this.reader.consumeDigitSequence();
            if (numRef.length() == 0) {
                characterReferenceError();
                this.reader.rewindToMark();
                return null;
            }
            if (!this.reader.matchConsume(";")) {
                characterReferenceError();
            }
            int charval = -1;
            try {
                charval = Integer.valueOf(numRef, isHexMode ? 16 : 10).intValue();
            } catch (NumberFormatException e) {
            }
            if (charval != -1 && ((charval < 55296 || charval > 57343) && charval <= 1114111)) {
                return Character.valueOf((char) charval);
            }
            characterReferenceError();
            return Character.valueOf(replacementChar);
        }
        String nameRef = this.reader.consumeLetterSequence();
        boolean looksLegit = this.reader.matches(';');
        boolean found = false;
        while (nameRef.length() > 0 && !found) {
            if (Entities.isNamedEntity(nameRef)) {
                found = true;
            } else {
                nameRef = nameRef.substring(0, nameRef.length() - 1);
                this.reader.unconsume();
            }
        }
        if (!found) {
            if (looksLegit) {
                characterReferenceError();
            }
            this.reader.rewindToMark();
            return null;
        } else if (!inAttribute || (!this.reader.matchesLetter() && !this.reader.matchesDigit() && !this.reader.matches('='))) {
            if (!this.reader.matchConsume(";")) {
                characterReferenceError();
            }
            return Entities.getCharacterByName(nameRef);
        } else {
            this.reader.rewindToMark();
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public Token.Tag createTagPending(boolean start) {
        this.tagPending = start ? new Token.StartTag() : new Token.EndTag();
        return this.tagPending;
    }

    /* access modifiers changed from: package-private */
    public void emitTagPending() {
        this.tagPending.finaliseTag();
        emit(this.tagPending);
    }

    /* access modifiers changed from: package-private */
    public void createCommentPending() {
        this.commentPending = new Token.Comment();
    }

    /* access modifiers changed from: package-private */
    public void emitCommentPending() {
        emit(this.commentPending);
    }

    /* access modifiers changed from: package-private */
    public void createDoctypePending() {
        this.doctypePending = new Token.Doctype();
    }

    /* access modifiers changed from: package-private */
    public void emitDoctypePending() {
        emit(this.doctypePending);
    }

    /* access modifiers changed from: package-private */
    public void createTempBuffer() {
        this.dataBuffer = new StringBuilder();
    }

    /* access modifiers changed from: package-private */
    public boolean isAppropriateEndTagToken() {
        return this.tagPending.tagName.equals(this.lastStartTag.tagName);
    }

    /* access modifiers changed from: package-private */
    public String appropriateEndTagName() {
        return this.lastStartTag.tagName;
    }

    /* access modifiers changed from: package-private */
    public boolean isTrackErrors() {
        return this.trackErrors;
    }

    /* access modifiers changed from: package-private */
    public void setTrackErrors(boolean trackErrors2) {
        this.trackErrors = trackErrors2;
    }

    /* access modifiers changed from: package-private */
    public void error(TokeniserState state2) {
        if (this.trackErrors) {
            this.errors.add(new ParseError("Unexpected character in input", this.reader.current(), state2, this.reader.pos()));
        }
    }

    /* access modifiers changed from: package-private */
    public void eofError(TokeniserState state2) {
        if (this.trackErrors) {
            this.errors.add(new ParseError("Unexpectedly reached end of file (EOF)", state2, this.reader.pos()));
        }
    }

    private void characterReferenceError() {
        if (this.trackErrors) {
            this.errors.add(new ParseError("Invalid character reference", this.reader.pos()));
        }
    }

    private void error(String errorMsg) {
        if (this.trackErrors) {
            this.errors.add(new ParseError(errorMsg, this.reader.pos()));
        }
    }

    /* access modifiers changed from: package-private */
    public boolean currentNodeInHtmlNS() {
        return true;
    }
}
