package org.jsoup.parser;

import java.util.List;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

public class Parser {
    public static Document parse(String html, String baseUri) {
        return new TreeBuilder().parse(html, baseUri);
    }

    public static List<Node> parseFragment(String fragmentHtml, Element context, String baseUri) {
        return new TreeBuilder().parseFragment(fragmentHtml, context, baseUri);
    }

    public static Document parseBodyFragment(String bodyHtml, String baseUri) {
        Document doc = Document.createShell(baseUri);
        Element body = doc.body();
        List<Node> nodeList = parseFragment(bodyHtml, body, baseUri);
        for (Node node : (Node[]) nodeList.toArray(new Node[nodeList.size()])) {
            body.appendChild(node);
        }
        return doc;
    }

    public static Document parseBodyFragmentRelaxed(String bodyHtml, String baseUri) {
        return parse(bodyHtml, baseUri);
    }
}
