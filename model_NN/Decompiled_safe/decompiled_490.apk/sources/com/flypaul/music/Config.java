package com.flypaul.music;

import android.os.Environment;
import java.io.File;

public class Config {
    public static final boolean AD_TEST = false;
    public static final int CATEGORIES_ALL = 1;
    public static final int CATEGORIES_DOWNLOAD = 2;
    public static final int CATEGORIES_FAVOURITE = 3;
    public static final String CATEGORIES_KEY = "CATEGORIES";
    public static final String CHART_BASE_URL = "http://music.sogou.com";
    public static final String DOWNLOAD_PATH;
    public static final String EN_UK = "/song/ukp_1.html?w=02013000";
    public static final String FILENAME_KEY = "filename";
    public static final String FILEPOSTION_KEY = "fileposition";
    public static final String FORCEPLAY_KEY = "force";
    public static final String LYRIC_SUFFIX = ".lrc";
    public static final String MOBILE_RING = "/rbt/rbt_rbt.html?w=02013000&dr=1";
    public static final String NEW_SONG_TOP_URL = "/song/newtop.html?w=02013000";
    public static final String RATE_URL = "market://details?id=com.flypaul.music";
    public static final String RINGTONG_PATH = (Environment.getExternalStorageDirectory() + "/media/audio/ringtone/");
    public static final String SDPATH;
    public static final String SEARCH_WORD_KEY = "SEARCH_WORD_KEY";
    public static final String SONG_TOP = "/song/chinesep_1.html?w=02013000&dr=1";
    public static final String[] SUPPORT_FORMAT = {"MP3"};
    public static final String USA_BILLBOARD = "/song/webbillboardp_1.html?w=02013000";
    public static final String USERAGENT = "Mozilla/5.0 (Windows NT 6.1; rv:2.0) Gecko/20100101 Firefox/4.0";
    public static final String WESTERN_TOP = "/song/enpop_1.html?w=02013000";

    static {
        StringBuilder stringbuilder = new StringBuilder();
        File file = Environment.getExternalStorageDirectory();
        SDPATH = stringbuilder.append(file).append("/").toString();
        DOWNLOAD_PATH = file + "/music/mp3/";
    }
}
