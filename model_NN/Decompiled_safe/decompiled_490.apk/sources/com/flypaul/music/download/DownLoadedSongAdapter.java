package com.flypaul.music.download;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.flypaul.music.R;
import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DownLoadedSongAdapter extends BaseAdapter {
    DecimalFormat m_decimalFormat = new DecimalFormat("0.00");
    LayoutInflater m_inflater;
    SimpleDateFormat m_simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
    public ArrayList<File> m_songFileList = new ArrayList<>();

    public final class ViewHolder {
        public TextView m_fileDate;
        public TextView m_fileName;
        public TextView m_fileSize;
        public TextView m_index;
        final DownLoadedSongAdapter this$0;

        public ViewHolder() {
            this.this$0 = DownLoadedSongAdapter.this;
        }
    }

    public DownLoadedSongAdapter(Context context) {
        this.m_inflater = LayoutInflater.from(context);
    }

    public void addFile(File[] afile, boolean flag) {
        if (afile != null) {
            this.m_songFileList.clear();
            for (File file : afile) {
                if (!file.isDirectory()) {
                    this.m_songFileList.add(file);
                }
            }
            if (flag) {
                notifyDataSetChanged();
            }
        }
    }

    public int getCount() {
        return this.m_songFileList.size();
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public File getItem(int i) {
        if (getCount() <= 0 || i < 0) {
            return null;
        }
        if (this.m_songFileList == null || this.m_songFileList.get(i) == null) {
            return null;
        }
        return this.m_songFileList.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewgroup) {
        ViewHolder viewholder;
        if (view == null) {
            viewholder = new ViewHolder();
            view = this.m_inflater.inflate((int) R.layout.download_song_item, (ViewGroup) null);
            viewholder.m_index = (TextView) view.findViewById(R.id.download_songinfo_index);
            viewholder.m_fileName = (TextView) view.findViewById(R.id.download_songinfo_displayname);
            viewholder.m_fileSize = (TextView) view.findViewById(R.id.download_songinfo_size);
            viewholder.m_fileDate = (TextView) view.findViewById(R.id.fileDate);
            view.setTag(viewholder);
        } else {
            viewholder = (ViewHolder) view.getTag();
        }
        viewholder.m_index.setText(Integer.toString(i + 1));
        viewholder.m_fileName.setText(getItem(i).getName());
        viewholder.m_fileSize.setText(String.valueOf(String.valueOf(this.m_decimalFormat.format(((double) getItem(i).length()) / 1048576.0d))) + "MB");
        viewholder.m_fileDate.setText(this.m_simpleDateFormat.format(new Date(getItem(i).lastModified())));
        return view;
    }
}
