package com.flypaul.music.download;

import android.os.Handler;
import android.os.Message;
import com.flypaul.music.utils.FileUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.net.URLConnection;

public class DownloadThread implements Runnable {
    private int cachSize = 4096;
    private boolean mCanceled = false;
    private int mDownloadFilePosition;
    private String mDownloadUrl;
    private int mFileSize;
    private String mFilename;
    private Handler mHandler;
    private String mName;
    private String mPath;
    private boolean mPaused = false;
    private int mPercent = 0;
    private State mState;

    public enum State {
        Waiting,
        Downloading,
        Paused,
        Canceled,
        Error,
        Finished
    }

    public DownloadThread(String name, String path, String fileName, String downloadUrl, Handler handler) {
        this.mPath = path;
        this.mFilename = fileName;
        this.mDownloadUrl = downloadUrl;
        this.mState = State.Waiting;
        this.mDownloadFilePosition = 0;
        this.mHandler = handler;
        this.mName = name;
    }

    public DownloadThread(String name, DownloadBean bean, Handler handler) {
        this.mPath = bean.path;
        this.mName = bean.threadName;
        this.mFilename = bean.filename;
        this.mFileSize = bean.fileSize;
        this.mDownloadUrl = bean.downloadUrl;
        this.mDownloadFilePosition = bean.downloadFilePosition;
        this.mPercent = bean.percent;
        this.mState = State.Waiting;
        this.mHandler = handler;
        this.mName = name;
    }

    public void run() {
        Message msg = this.mHandler.obtainMessage();
        msg.what = 1;
        msg.obj = getDownloadBean();
        this.mHandler.sendMessage(msg);
        this.mState = State.Downloading;
        if (this.mDownloadUrl == null) {
            setError("downloadUrl is null");
            return;
        }
        try {
            URLConnection conn = new URL(this.mDownloadUrl).openConnection();
            conn.setConnectTimeout(10000);
            conn.setReadTimeout(60000);
            if (this.mFileSize > 0 && this.mDownloadFilePosition > 0) {
                conn.setRequestProperty("Range", "bytes=" + this.mDownloadFilePosition + "-" + this.mFileSize);
            }
            conn.connect();
            InputStream is = conn.getInputStream();
            this.mFileSize = conn.getContentLength();
            if (this.mFileSize <= 0) {
                setError("file size <= 0");
                return;
            }
            if (is == null) {
                setError("input stream is null");
            }
            File file = FileUtil.createFile(this.mPath);
            if (!file.getName().equals(this.mFilename)) {
                this.mFilename = file.getName();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file, this.mFileSize > 0 && this.mDownloadFilePosition > 0);
            byte[] buf = new byte[this.cachSize];
            long preTimeLong = System.currentTimeMillis();
            while (!this.mCanceled && !this.mPaused) {
                int numread = is.read(buf);
                if (numread == -1) {
                    break;
                }
                fileOutputStream.write(buf, 0, numread);
                this.mDownloadFilePosition = this.mDownloadFilePosition + numread;
                int percent = Math.round(((0.0f + ((float) this.mDownloadFilePosition)) / ((float) this.mFileSize)) * 100.0f);
                if (percent > this.mPercent && System.currentTimeMillis() - preTimeLong > 1000) {
                    this.mPercent = percent;
                    Message msg2 = this.mHandler.obtainMessage();
                    msg2.what = 2;
                    msg2.arg1 = percent;
                    msg2.arg2 = this.mDownloadFilePosition;
                    msg2.obj = this.mName;
                    this.mHandler.sendMessage(msg2);
                    preTimeLong = System.currentTimeMillis();
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                }
            }
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
            if (this.mCanceled) {
                this.mState = State.Canceled;
                if (file.exists()) {
                    for (int i = 0; i < 3 && !file.delete(); i++) {
                    }
                }
                Message msg3 = this.mHandler.obtainMessage();
                msg3.what = 3;
                msg3.obj = getDownloadBean();
                this.mHandler.sendMessage(msg3);
            } else if (this.mPaused) {
                this.mState = State.Paused;
                Message msg4 = this.mHandler.obtainMessage();
                msg4.what = 4;
                msg4.obj = getDownloadBean();
                this.mHandler.sendMessage(msg4);
            } else {
                this.mState = State.Finished;
                Message msg5 = this.mHandler.obtainMessage();
                msg5.what = 5;
                msg5.obj = getDownloadBean();
                this.mHandler.sendMessage(msg5);
            }
        } catch (Exception e2) {
            Exception e3 = e2;
            if (this.mFileSize <= 0 || this.mDownloadFilePosition != this.mFileSize) {
                setError("download file throws exception " + e3.getClass() + ":" + e3.getMessage());
                System.out.println(e3.getStackTrace());
                e3.printStackTrace();
                return;
            }
            this.mState = State.Finished;
            Message msg6 = new Message();
            msg6.what = 5;
            msg6.obj = getDownloadBean();
            this.mHandler.sendMessage(msg6);
        }
    }

    public DownloadBean getDownloadBean() {
        DownloadBean bean = new DownloadBean();
        bean.threadName = this.mName;
        bean.path = this.mPath;
        bean.filename = this.mFilename;
        bean.fileSize = this.mFileSize;
        bean.downloadUrl = this.mDownloadUrl;
        bean.downloadFilePosition = this.mDownloadFilePosition;
        bean.state = this.mState;
        bean.percent = this.mPercent;
        return bean;
    }

    private void setError(String error) {
        this.mState = State.Error;
        Message msg = new Message();
        msg.what = 6;
        DownloadBean bean = getDownloadBean();
        bean.error = error;
        msg.obj = bean;
        this.mHandler.sendMessage(msg);
    }

    public void canceled() {
        this.mCanceled = true;
    }

    public void paused() {
        this.mPaused = true;
    }

    public boolean isCanceled() {
        return State.Canceled.equals(this.mState);
    }

    public boolean isPaused() {
        return State.Paused.equals(this.mState);
    }

    public class DownloadBean implements Serializable {
        private static final long serialVersionUID = 776609449252996561L;
        public int downloadFilePosition;
        public String downloadUrl;
        public String error;
        public int fileSize;
        public String filename;
        public String path;
        public int percent;
        public State state;
        public String threadName;

        public DownloadBean() {
        }

        public boolean equals(Object obj) {
            if (((DownloadBean) obj).threadName.equals(this.threadName)) {
                return true;
            }
            return false;
        }
    }
}
