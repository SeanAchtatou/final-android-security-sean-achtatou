package com.flypaul.music.download;

import com.flypaul.music.download.DownloadThread;

public interface DownloadListener {
    void onBeforeDownload(String str, DownloadThread.DownloadBean downloadBean);

    void onCanceled(String str, DownloadThread.DownloadBean downloadBean);

    void onError(String str, DownloadThread.DownloadBean downloadBean, String str2);

    void onFinished(String str, DownloadThread.DownloadBean downloadBean);

    void onPaused(String str, DownloadThread.DownloadBean downloadBean);

    void onUpdateProcess(String str, int i, int i2);
}
