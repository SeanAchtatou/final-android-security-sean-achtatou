package com.flypaul.music;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlManager;
import com.adwhirl.AdWhirlTargeting;
import com.flypaul.music.lyric.LyricView;
import com.flypaul.music.services.PlayService;
import java.io.File;

public class PlayerActivity extends Activity implements View.OnClickListener, MediaPlayer.OnPreparedListener, SeekBar.OnSeekBarChangeListener {
    /* access modifiers changed from: private */
    public static boolean hasThread = false;
    /* access modifiers changed from: private */
    public static boolean runFlag = false;
    /* access modifiers changed from: private */
    public TextView currentTime;
    private TextView durationTime;
    /* access modifiers changed from: private */
    public File file;
    private boolean isPlaying;
    /* access modifiers changed from: private */
    public LyricView lyricView;
    Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public PlayService mService;
    Runnable mUpdateCurrentText = new Runnable() {
        public void run() {
            PlayerActivity.this.currentTime.setText(PlayerActivity.this.getMinuteText(PlayerActivity.this.mService.getCurrentPosition()));
            PlayerActivity.this.seekBar.setProgress(PlayerActivity.this.mService.getCurrentPosition());
            if (PlayerActivity.this.mService.getCurrentPosition() < PlayerActivity.this.mService.getDuration()) {
                PlayerActivity.this.mHandler.postDelayed(PlayerActivity.this.mUpdateCurrentText, 1000);
            }
        }
    };
    Runnable mUpdateResults = new Runnable() {
        public void run() {
            PlayerActivity.this.lyricView.invalidate();
        }
    };
    private LinearLayout m_adArea;
    private Button playBut;
    private Button repeatBut;
    /* access modifiers changed from: private */
    public SeekBar seekBar;
    private PlayService.PlayType type = PlayService.PlayType.CycleAll;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.player);
        this.lyricView = (LyricView) findViewById(R.id.lyricView);
        this.playBut = (Button) findViewById(R.id.playControlBtn);
        this.playBut.setOnClickListener(this);
        findViewById(R.id.previousControlBtn).setOnClickListener(this);
        findViewById(R.id.nextControlBtn).setOnClickListener(this);
        this.repeatBut = (Button) findViewById(R.id.repeat_allBtn);
        this.repeatBut.setOnClickListener(this);
        setRepeatButtonBackgroud();
        this.currentTime = (TextView) findViewById(R.id.currentTimeTV);
        this.durationTime = (TextView) findViewById(R.id.durationTimeTV);
        this.seekBar = (SeekBar) findViewById(R.id.seekbar);
        this.seekBar.setOnSeekBarChangeListener(this);
        this.m_adArea = (LinearLayout) findViewById(R.id.ad_area_player);
        AdWhirlManager.setConfigExpireTimeout(300000);
        AdWhirlTargeting.setTestMode(false);
        AdWhirlLayout adWhirlLayout = new AdWhirlLayout(this, "0005fc1dfea141da88814828ccc82530");
        this.m_adArea.addView(adWhirlLayout, -1, -2);
        adWhirlLayout.setVisibility(0);
    }

    public void onStart() {
        super.onStart();
        prepareService();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    private void prepareService() {
        if (this.mService == null) {
            this.mService = ((MusicApplication) getApplication()).getPlayService();
            if (this.mService != null) {
                this.mService.setPlayType(this.type);
                this.mService.reset();
                this.mService.onPreparedListener(this);
                if (this.mService.getState() == PlayService.State.Playing || this.mService.getState() == PlayService.State.Paused) {
                    if (this.mService.getState() == PlayService.State.Playing) {
                        this.isPlaying = true;
                        this.playBut.setBackgroundResource(R.drawable.pause_button_default);
                    } else {
                        this.isPlaying = false;
                        this.playBut.setBackgroundResource(R.drawable.play_button_default);
                    }
                    prepare();
                }
            }
        }
    }

    public void onClick(View v) {
        if (this.mService == null) {
            prepareService();
        }
        if (this.mService != null) {
            if (v.getId() == R.id.playControlBtn) {
                if (this.isPlaying) {
                    v.setBackgroundResource(R.drawable.play_button_default);
                    this.mService.pause();
                    this.isPlaying = false;
                    return;
                }
                v.setBackgroundResource(R.drawable.pause_button_default);
                this.mService.play();
                this.isPlaying = true;
                this.durationTime.setText(getMinuteText(this.mService.getDuration()));
                this.mHandler.post(this.mUpdateCurrentText);
            } else if (v.getId() == R.id.previousControlBtn) {
                this.mService.rewind();
                this.playBut.setBackgroundResource(R.drawable.pause_button_default);
            } else if (v.getId() == R.id.nextControlBtn) {
                this.mService.skip();
                this.playBut.setBackgroundResource(R.drawable.pause_button_default);
            } else if (v.getId() == R.id.repeat_allBtn) {
                if (this.type == PlayService.PlayType.CycleAll) {
                    this.type = PlayService.PlayType.Random;
                } else if (this.type == PlayService.PlayType.Random) {
                    this.type = PlayService.PlayType.CycleOne;
                } else if (this.type == PlayService.PlayType.CycleOne) {
                    this.type = PlayService.PlayType.CycleAll;
                }
                this.mService.setPlayType(this.type);
                setRepeatButtonBackgroud();
            }
        }
    }

    private void setRepeatButtonBackgroud() {
        if (this.type == PlayService.PlayType.CycleAll) {
            this.repeatBut.setBackgroundResource(R.drawable.playmode_repeate_all_enable);
        } else if (this.type == PlayService.PlayType.CycleOne) {
            this.repeatBut.setBackgroundResource(R.drawable.playmode_repeate_single_enable);
        } else if (this.type == PlayService.PlayType.Random) {
            this.repeatBut.setBackgroundResource(R.drawable.playmode_repeate_random_hover);
        }
    }

    private void prepare() {
        this.currentTime.setText(getMinuteText(this.mService.getCurrentPosition()));
        this.durationTime.setText(getMinuteText(this.mService.getDuration()));
        this.seekBar.setMax(this.mService.getDuration());
        this.mHandler.post(this.mUpdateCurrentText);
        loadLyric();
    }

    private void loadLyric() {
        runFlag = false;
        while (hasThread) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }
        }
        String path = this.mService.getPlaySong().path;
        if (path.startsWith("http:") || path.startsWith("https:")) {
            this.lyricView.initLyric(this.mService.getPlaySong().title);
        } else {
            this.file = new File(path);
            this.lyricView.initLyric(this.file);
        }
        new Thread(new UIUpdateThread()).start();
    }

    /* access modifiers changed from: private */
    public String getMinuteText(int msec) {
        int minute = msec / 60000;
        int second = (msec % 60000) / 1000;
        if (second <= 9) {
            return String.valueOf(minute) + ":0" + second;
        }
        return String.valueOf(minute) + ":" + second;
    }

    class UIUpdateThread implements Runnable {
        long sleeptime = 100;

        UIUpdateThread() {
        }

        public void run() {
            PlayerActivity.runFlag = true;
            PlayerActivity.hasThread = true;
            while (PlayerActivity.runFlag) {
                if (this.sleeptime != -1) {
                    try {
                        Thread.sleep(this.sleeptime);
                        PlayerActivity.this.lyricView.updateIndex((long) PlayerActivity.this.mService.getCurrentPosition(), PlayerActivity.this.file);
                        PlayerActivity.this.mHandler.post(PlayerActivity.this.mUpdateResults);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    return;
                }
            }
            PlayerActivity.hasThread = false;
        }
    }

    public void onPrepared(MediaPlayer mp) {
        prepare();
    }

    public void onProgressChanged(SeekBar seekBar2, int progress, boolean fromUser) {
    }

    public void onStartTrackingTouch(SeekBar seekBar2) {
    }

    public void onStopTrackingTouch(SeekBar seekBar2) {
        if (this.mService == null) {
            prepareService();
        }
        if (this.mService != null) {
            this.mService.seekToPlay(seekBar2.getProgress());
        }
    }
}
