package com.flypaul.music.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;
import com.flypaul.music.PlayerActivity;
import com.flypaul.music.R;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class PlayService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {
    private final int NOTIFICATION_ID = 1;
    /* access modifiers changed from: private */
    public ArrayList<Item> itemL = new ArrayList<>();
    private String logTag = PlayService.class.getName();
    private final IBinder mBinder = new LocalBinder();
    private PlayHandler mHandler;
    private boolean mIsStreaming = false;
    private Notification mNotification = null;
    private NotificationManager mNotificationManager;
    private Item mPlaySong;
    private PlayType mPlayType = PlayType.CycleAll;
    private MediaPlayer mPlayer;
    private int mSongIndex = 0;
    private String mSongTitle;
    private State mState = State.Retrieving;
    private MediaPlayer.OnPreparedListener onPreparedListener;

    public enum PlayType {
        Once,
        CycleOne,
        CycleAll,
        Random
    }

    public enum State {
        Retrieving,
        Stopped,
        Preparing,
        Playing,
        Paused
    }

    public void onCreate() {
        super.onCreate();
        Log.i(this.logTag, "debug: Creating service");
        registerSDReceiver();
        refresh();
        this.mNotificationManager = (NotificationManager) getSystemService("notification");
        this.mHandler = new PlayHandler(this, null);
    }

    public void onDestroy() {
        super.onDestroy();
        this.mState = State.Stopped;
        relaxResources(true);
    }

    public IBinder onBind(Intent intente) {
        if (this.itemL == null || this.itemL.size() == 0) {
            this.itemL = getAllSong();
        }
        this.mState = State.Stopped;
        return this.mBinder;
    }

    public void onPreparedListener(MediaPlayer.OnPreparedListener listener) {
        this.onPreparedListener = listener;
    }

    public void reset() {
        this.onPreparedListener = null;
    }

    public ArrayList<Item> getAllSong() {
        ContentResolver mContentResolver = getContentResolver();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Log.i(this.logTag, "Querying media...");
        Log.i(this.logTag, "URI: " + uri.toString());
        Cursor cur = mContentResolver.query(uri, null, "mime_type=? and duration>=60000", new String[]{"audio/mpeg"}, "date_modified desc");
        Log.i(this.logTag, "Query finished. " + (cur == null ? "Returned NULL." : "Returned a cursor."));
        if (cur == null) {
            Log.e(this.logTag, "Failed to retrieve music: cursor is null :-(");
            return new ArrayList<>();
        } else if (!cur.moveToFirst()) {
            Log.e(this.logTag, "Failed to move cursor to first row (no query results).");
            return new ArrayList<>();
        } else {
            Log.i(this.logTag, "Listing...");
            ArrayList<Item> tmpl = new ArrayList<>();
            int titleColumn = cur.getColumnIndex("title");
            int idColumn = cur.getColumnIndex("_id");
            int pathColumn = cur.getColumnIndex("_data");
            do {
                tmpl.add(new Item((long) cur.getInt(idColumn), cur.getString(titleColumn), cur.getString(pathColumn)));
            } while (cur.moveToNext());
            return tmpl;
        }
    }

    public ArrayList<File> getAllSongFile() {
        ArrayList<Item> list = getAllSong();
        ArrayList<File> fl = new ArrayList<>();
        if (list.size() > 0) {
            Iterator<Item> it = list.iterator();
            while (it.hasNext()) {
                fl.add(new File(it.next().path));
            }
        }
        return fl;
    }

    public void setItemList(ArrayList<Item> list) {
        this.itemL = list;
    }

    private void refresh() {
        sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse("file://" + Environment.getExternalStorageDirectory().getAbsolutePath())));
    }

    private void registerSDReceiver() {
        IntentFilter intentfilter = new IntentFilter("android.intent.action.MEDIA_SCANNER_STARTED");
        intentfilter.addAction("android.intent.action.MEDIA_SCANNER_FINISHED");
        intentfilter.addAction("android.intent.action.MEDIA_SCANNER_STARTED");
        intentfilter.addAction("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        intentfilter.addDataScheme("file");
        registerReceiver(new RefreshReceiver(this, null), intentfilter);
    }

    public ArrayList<Item> getItemList() {
        return this.itemL;
    }

    public void setPlayType(PlayType playType) {
        this.mPlayType = playType;
    }

    public void seekToPlay(int msec) {
        if (this.mState == State.Playing || this.mState == State.Paused) {
            this.mPlayer.seekTo(msec);
        }
    }

    public int getDuration() {
        if (this.mState == State.Playing || this.mState == State.Paused) {
            return this.mPlayer.getDuration();
        }
        return 0;
    }

    public int getCurrentPosition() {
        if (this.mState == State.Playing || this.mState == State.Paused) {
            return this.mPlayer.getCurrentPosition();
        }
        return 0;
    }

    public State getState() {
        return this.mState;
    }

    public Item getPlaySong() {
        return this.mPlaySong;
    }

    public class LocalBinder extends Binder {
        public LocalBinder() {
        }

        public PlayService getService() {
            return PlayService.this;
        }
    }

    public void play() {
        if (this.mState == State.Stopped) {
            playSong();
        } else if (this.mState == State.Paused) {
            this.mState = State.Playing;
            setUpAsForeground(this.mSongTitle);
            configAndStartMediaPlayer();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flypaul.music.services.PlayService.playSong(com.flypaul.music.services.PlayService$Item, boolean):void
     arg types: [com.flypaul.music.services.PlayService$Item, int]
     candidates:
      com.flypaul.music.services.PlayService.playSong(java.lang.String, java.lang.String):void
      com.flypaul.music.services.PlayService.playSong(com.flypaul.music.services.PlayService$Item, boolean):void */
    public void play(Item item) {
        playSong(item, true);
    }

    public void play(String songName, String path) {
        playSong(songName, path);
    }

    public void play(String path) {
        playSong(path);
    }

    public void pause() {
        if (this.mState == State.Playing) {
            this.mState = State.Paused;
            this.mPlayer.pause();
            relaxResources(false);
            pauseNotification(this.mSongTitle);
        }
    }

    public void rewind() {
        if (this.mState == State.Playing || this.mState == State.Paused) {
            prevSong();
            playSong();
        }
    }

    public void skip() {
        if (this.mState == State.Playing || this.mState == State.Paused) {
            nextSong();
            playSong();
        }
    }

    public void stop() {
        if (this.mState == State.Playing || this.mState == State.Paused) {
            this.mState = State.Stopped;
            relaxResources(true);
        }
    }

    /* access modifiers changed from: package-private */
    public void createMediaPlayerIfNeeded() {
        if (this.mPlayer == null) {
            this.mPlayer = new MediaPlayer();
            this.mPlayer.setWakeMode(getApplicationContext(), 1);
            this.mPlayer.setOnPreparedListener(this);
            this.mPlayer.setOnCompletionListener(this);
            this.mPlayer.setOnErrorListener(this);
            return;
        }
        this.mPlayer.reset();
    }

    private void relaxResources(boolean releaseMediaPlayer) {
        if (releaseMediaPlayer && this.mPlayer != null) {
            this.mPlayer.reset();
            this.mPlayer.release();
            this.mPlayer = null;
        }
    }

    private void configAndStartMediaPlayer() {
        if (this.mPlayType.equals(PlayType.CycleOne)) {
            this.mPlayer.setLooping(true);
        } else {
            this.mPlayer.setLooping(false);
        }
        this.mPlayer.setVolume(1.0f, 1.0f);
        if (!this.mPlayer.isPlaying()) {
            this.mPlayer.start();
        }
    }

    private void nextSong() {
        if (this.itemL != null && this.itemL.size() != 0) {
            if (this.mPlayType.equals(PlayType.CycleAll)) {
                this.mSongIndex++;
                if (this.mSongIndex == this.itemL.size()) {
                    this.mSongIndex = 0;
                }
            } else if (this.mPlayType.equals(PlayType.Random)) {
                this.mSongIndex = new Random().nextInt(this.itemL.size());
            }
        }
    }

    private void prevSong() {
        if (this.itemL != null && this.itemL.size() != 0) {
            if (this.mPlayType.equals(PlayType.CycleAll)) {
                this.mSongIndex--;
                if (this.mSongIndex < 0) {
                    this.mSongIndex = this.itemL.size() - 1;
                }
            } else if (this.mPlayType.equals(PlayType.Random)) {
                this.mSongIndex = new Random().nextInt(this.itemL.size());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flypaul.music.services.PlayService.playSong(com.flypaul.music.services.PlayService$Item, boolean):void
     arg types: [com.flypaul.music.services.PlayService$Item, int]
     candidates:
      com.flypaul.music.services.PlayService.playSong(java.lang.String, java.lang.String):void
      com.flypaul.music.services.PlayService.playSong(com.flypaul.music.services.PlayService$Item, boolean):void */
    private void playSong(String songName, String manualUrl) {
        if (manualUrl != null) {
            this.mIsStreaming = manualUrl.startsWith("http:") || manualUrl.startsWith("https:");
            if (!this.mIsStreaming) {
                playSong(new Item(songName, manualUrl), true);
            } else {
                playSong(new Item(songName, manualUrl), false);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flypaul.music.services.PlayService.playSong(com.flypaul.music.services.PlayService$Item, boolean):void
     arg types: [com.flypaul.music.services.PlayService$Item, int]
     candidates:
      com.flypaul.music.services.PlayService.playSong(java.lang.String, java.lang.String):void
      com.flypaul.music.services.PlayService.playSong(com.flypaul.music.services.PlayService$Item, boolean):void */
    private void playSong(String manualUrl) {
        if (manualUrl != null) {
            this.mIsStreaming = manualUrl.startsWith("http:") || manualUrl.startsWith("https:");
            if (!this.mIsStreaming) {
                playSong(new Item(manualUrl), true);
            } else {
                playSong(new Item(manualUrl), false);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flypaul.music.services.PlayService.playSong(com.flypaul.music.services.PlayService$Item, boolean):void
     arg types: [com.flypaul.music.services.PlayService$Item, int]
     candidates:
      com.flypaul.music.services.PlayService.playSong(java.lang.String, java.lang.String):void
      com.flypaul.music.services.PlayService.playSong(com.flypaul.music.services.PlayService$Item, boolean):void */
    private void playSong() {
        this.mIsStreaming = false;
        Item item = getPlayItem();
        if (item == null) {
            say("No song to play :-(");
        } else {
            playSong(item, false);
        }
    }

    private void playSong(Item item, boolean isNeedAdd) {
        this.mPlaySong = item;
        if (isNeedAdd) {
            if (this.itemL == null) {
                this.itemL = new ArrayList<>();
            }
            if (this.itemL.contains(item)) {
                this.mSongIndex = this.itemL.indexOf(item);
            } else {
                this.itemL.add(item);
                this.mSongIndex = this.itemL.size() - 1;
            }
        }
        this.mSongTitle = item.title;
        this.mState = State.Stopped;
        relaxResources(false);
        try {
            createMediaPlayerIfNeeded();
            this.mPlayer.setAudioStreamType(3);
            if (this.mIsStreaming || !item.isUri) {
                this.mPlayer.setDataSource(item.path);
            } else {
                this.mPlayer.setDataSource(getApplicationContext(), item.getURI());
            }
            this.mState = State.Preparing;
            setUpAsForeground(this.mSongTitle);
            this.mPlayer.prepareAsync();
        } catch (Exception e) {
            Exception ex = e;
            Log.e("MusicService", "IOException playing next song: " + ex.getMessage());
            ex.printStackTrace();
            relaxResources(true);
            if (this.itemL != null && this.itemL.size() > this.mSongIndex + 1) {
                this.itemL.remove(this.mSongIndex);
            }
            nextSong();
            playSong();
        }
    }

    private Item getPlayItem() {
        if (this.itemL.size() <= 0) {
            return null;
        }
        if (this.itemL.size() < this.mSongIndex + 1) {
            if (this.mPlayType.equals(PlayType.Random)) {
                this.mSongIndex = new Random().nextInt(this.itemL.size());
            } else {
                this.mSongIndex = 0;
            }
        }
        Item tmp = this.itemL.get(this.mSongIndex);
        this.mSongTitle = tmp.title;
        return tmp;
    }

    private class RefreshReceiver extends BroadcastReceiver {
        private RefreshReceiver() {
        }

        /* synthetic */ RefreshReceiver(PlayService playService, RefreshReceiver refreshReceiver) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            String s = intent.getAction();
            if (!"android.intent.action.MEDIA_SCANNER_STARTED".equals(s)) {
                "android.intent.action.MEDIA_SCANNER_SCAN_FILE".equals(s);
            }
            if ("android.intent.action.MEDIA_SCANNER_FINISHED".equals(s)) {
                PlayService.this.itemL = PlayService.this.getAllSong();
            }
        }
    }

    private void updateNotification(String text) {
        PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(getApplicationContext(), PlayerActivity.class), 134217728);
        this.mNotification.flags = 32;
        this.mNotification.setLatestEventInfo(getApplicationContext(), "MusicPlayer", String.valueOf(text) + " (playing)", pi);
        this.mNotificationManager.notify(1, this.mNotification);
    }

    private void pauseNotification(String text) {
        this.mNotificationManager.cancel(1);
    }

    private void errorNotification(String text) {
        PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(getApplicationContext(), PlayerActivity.class), 134217728);
        this.mNotification.flags |= 16;
        this.mNotification.icon = R.drawable.btn_close_normal;
        this.mNotification.setLatestEventInfo(getApplicationContext(), "MusicPlayer", String.valueOf(text) + " (error)", pi);
        this.mNotificationManager.notify(1, this.mNotification);
    }

    private void setUpAsForeground(String text) {
        PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(getApplicationContext(), PlayerActivity.class), 134217728);
        this.mNotification = new Notification();
        this.mNotification.tickerText = text;
        this.mNotification.icon = R.drawable.play_button_default;
        this.mNotification.flags = 32;
        this.mNotification.setLatestEventInfo(getApplicationContext(), "MusicPlayer", String.valueOf(text) + "(loading)", pi);
        this.mNotificationManager.notify(1, this.mNotification);
    }

    public void onPrepared(MediaPlayer mp) {
        this.mState = State.Playing;
        updateNotification(this.mSongTitle);
        configAndStartMediaPlayer();
        if (this.onPreparedListener != null) {
            this.onPreparedListener.onPrepared(mp);
        }
    }

    public void onCompletion(MediaPlayer mp) {
        if (this.mPlayType.equals(PlayType.Once)) {
            mp.stop();
            return;
        }
        nextSong();
        playSong();
    }

    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.e(this.logTag, "Error: what=" + String.valueOf(what) + ", extra=" + String.valueOf(extra));
        errorNotification(this.mSongTitle);
        this.mState = State.Stopped;
        relaxResources(true);
        if (this.itemL != null && this.itemL.size() > this.mSongIndex + 1) {
            this.itemL.remove(this.mSongIndex);
        }
        nextSong();
        Message msg = this.mHandler.obtainMessage();
        msg.what = 1;
        msg.obj = String.valueOf(this.mSongTitle) + " can not be played.";
        this.mHandler.sendMessage(msg);
        return true;
    }

    /* access modifiers changed from: private */
    public void say(String message) {
        Toast.makeText(this, message, 0).show();
    }

    private class PlayHandler extends Handler {
        private PlayHandler() {
        }

        /* synthetic */ PlayHandler(PlayService playService, PlayHandler playHandler) {
            this();
        }

        public void handleMessage(Message msg) {
            PlayService.this.say(msg.obj.toString());
        }
    }

    public class Item {
        public long id;
        public boolean isUri = true;
        public String path;
        public String title;

        public Item(long id2, String title2, String path2) {
            this.id = id2;
            this.title = path2.substring(path2.lastIndexOf("/") + 1, path2.lastIndexOf("."));
            this.path = path2;
        }

        public Item(String path2) {
            this.path = path2;
            this.title = path2.substring(path2.lastIndexOf("/") + 1, path2.lastIndexOf("."));
        }

        public Item(String title2, String path2) {
            this.path = path2;
            this.title = title2;
        }

        public Uri getURI() {
            if (this.isUri) {
                return ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, this.id);
            }
            return null;
        }

        public boolean equals(Object obj) {
            if (((Item) obj).path.equals(this.path)) {
                return true;
            }
            return false;
        }
    }
}
