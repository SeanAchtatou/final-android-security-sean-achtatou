package com.flypaul.music.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Binder;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.adwhirl.util.AdWhirlUtil;
import com.flypaul.music.Config;
import com.flypaul.music.MainActivity;
import com.flypaul.music.R;
import com.flypaul.music.SongBean;
import com.flypaul.music.download.DownloadListener;
import com.flypaul.music.download.DownloadThread;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class DownloadService extends Service {
    private static int notificationId = 100;
    private Map<String, Integer> downloadMap = new HashMap();
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, DownloadThread.DownloadBean> downloadingList;
    private IBinder mBinder = new LocalBinder();
    private Handler mHandler;
    private NotificationManager mNotificationManager;
    private Map<Integer, Notification> notificationMap = new HashMap();
    private ExecutorService pool;
    /* access modifiers changed from: private */
    public volatile DownloadListener registListener;
    /* access modifiers changed from: private */
    public String tag = "DownloadService";
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, DownloadThread> threadMap;

    public void onCreate() {
        super.onCreate();
        this.pool = Executors.newCachedThreadPool();
        this.downloadingList = new ConcurrentHashMap<>();
        this.threadMap = new ConcurrentHashMap<>();
        this.mNotificationManager = (NotificationManager) getSystemService("notification");
        this.mHandler = new DownloadServiceHandler(this, null);
    }

    public void onDestroy() {
        super.onDestroy();
        for (String name : this.threadMap.keySet()) {
            this.threadMap.get(name).canceled();
        }
        shutdownAndAwaitTermination(this.pool);
    }

    private void shutdownAndAwaitTermination(ExecutorService pool2) {
        pool2.shutdown();
        try {
            if (!pool2.awaitTermination(20, TimeUnit.SECONDS)) {
                pool2.shutdownNow();
                if (!pool2.awaitTermination(20, TimeUnit.SECONDS)) {
                    Log.i(this.tag, "Pool did not terminate");
                }
            }
        } catch (InterruptedException e) {
            pool2.shutdownNow();
        }
    }

    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }

    public void registListener(DownloadListener listener) {
        this.registListener = listener;
    }

    public void unregistListener(DownloadListener listener) {
        this.registListener = null;
    }

    public ConcurrentHashMap<String, DownloadThread.DownloadBean> getDownloadingList() {
        return this.downloadingList;
    }

    public void downloadMusic(String name, SongBean song) {
        DownloadThread t = new DownloadThread(name, String.valueOf(Config.DOWNLOAD_PATH) + song.m_fileName, song.m_fileName, song.m_downloadUri, this.mHandler);
        this.threadMap.put(name, t);
        this.pool.execute(t);
        if (this.downloadingList.contains(t.getDownloadBean())) {
            this.downloadingList.remove(t.getDownloadBean());
        }
        this.downloadingList.put(name, t.getDownloadBean());
        Log.i(this.tag, "onStart,downloadingList size=" + this.downloadingList.size());
    }

    public void pauseMusic(String name) {
        DownloadThread t = this.threadMap.get(name);
        if (t != null) {
            t.paused();
        }
    }

    public void cancelMusic(String name) {
        DownloadThread t = this.threadMap.get(name);
        if (t != null) {
            t.canceled();
        }
    }

    public void resumMusic(String name, DownloadThread.DownloadBean bean) {
        if (bean.state.equals(DownloadThread.State.Paused)) {
            DownloadThread t = new DownloadThread(name, bean, this.mHandler);
            this.threadMap.put(name, t);
            this.pool.execute(t);
        }
    }

    /* access modifiers changed from: private */
    public void updateNotification(String name, int percent, int position) {
        Notification noti = this.notificationMap.get(this.downloadMap.get(name));
        if (noti != null) {
            noti.contentView.setTextViewText(R.id.downing_percent, String.valueOf(percent) + "%");
            noti.contentView.setProgressBar(R.id.downing_progressbar, 100, percent, false);
            this.mNotificationManager.notify(this.downloadMap.get(name).intValue(), noti);
        }
    }

    /* access modifiers changed from: private */
    public void cancelNotification(String name, String text) {
        int notid = this.downloadMap.get(name).intValue();
        if (notid != 0) {
            this.mNotificationManager.cancel(notid);
            this.notificationMap.remove(Integer.valueOf(notid));
        }
    }

    /* access modifiers changed from: private */
    public void errorNotification(String name, String text) {
        int notid = this.downloadMap.get(name).intValue();
        if (notid != 0) {
            this.mNotificationManager.cancel(notid);
            this.notificationMap.remove(Integer.valueOf(notid));
        }
    }

    /* access modifiers changed from: private */
    public void finishNotification(String name, String text) {
        int notid = this.downloadMap.get(name).intValue();
        if (notid != 0) {
            this.mNotificationManager.cancel(notid);
            this.notificationMap.remove(Integer.valueOf(notid));
        }
    }

    /* access modifiers changed from: private */
    public void refresh() {
        sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse("file://" + Environment.getExternalStorageDirectory().getAbsolutePath())));
    }

    /* access modifiers changed from: private */
    public void setUpAsForeground(String name, String text, int percent) {
        Intent downIntent = new Intent(getApplicationContext(), MainActivity.class);
        downIntent.putExtra("pageState", MainActivity.PageState.download);
        PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, downIntent, 134217728);
        if (this.downloadMap.get(name) == null) {
            Notification noti = new Notification(17301633, String.valueOf(text) + "(download)", 0);
            noti.tickerText = text;
            noti.flags |= 16;
            RemoteViews contentView = new RemoteViews(getPackageName(), (int) R.layout.notificaiton_downloading_item);
            contentView.setTextViewText(R.id.downing_name, text);
            contentView.setTextViewText(R.id.downing_percent, String.valueOf(percent) + "%");
            contentView.setProgressBar(R.id.downing_progressbar, 100, percent, false);
            noti.contentView = contentView;
            noti.contentIntent = pi;
            this.downloadMap.put(name, Integer.valueOf(notificationId));
            this.notificationMap.put(Integer.valueOf(notificationId), noti);
            notificationId++;
        }
        this.mNotificationManager.notify(this.downloadMap.get(name).intValue(), this.notificationMap.get(this.downloadMap.get(name)));
    }

    public class LocalBinder extends Binder {
        public LocalBinder() {
        }

        public DownloadService getService() {
            return DownloadService.this;
        }
    }

    private class DownloadServiceHandler extends Handler {
        private DownloadServiceHandler() {
        }

        /* synthetic */ DownloadServiceHandler(DownloadService downloadService, DownloadServiceHandler downloadServiceHandler) {
            this();
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Log.i(DownloadService.this.tag, "onBeforeDownload");
                    DownloadThread.DownloadBean bean = (DownloadThread.DownloadBean) msg.obj;
                    DownloadService.this.setUpAsForeground(bean.threadName, bean.filename, Math.round(((0.0f + ((float) bean.downloadFilePosition)) / ((float) bean.fileSize)) * 100.0f));
                    if (DownloadService.this.registListener != null) {
                        try {
                            DownloadService.this.registListener.onBeforeDownload(bean.threadName, bean);
                            return;
                        } catch (Exception e) {
                            return;
                        }
                    } else {
                        return;
                    }
                case 2:
                    DownloadService.this.updateNotification(msg.obj.toString(), msg.arg1, msg.arg2);
                    ((DownloadThread.DownloadBean) DownloadService.this.downloadingList.get(msg.obj.toString())).percent = msg.arg1;
                    ((DownloadThread.DownloadBean) DownloadService.this.downloadingList.get(msg.obj.toString())).downloadFilePosition = msg.arg2;
                    if (DownloadService.this.registListener != null) {
                        try {
                            DownloadService.this.registListener.onUpdateProcess(msg.obj.toString(), msg.arg1, msg.arg2);
                            return;
                        } catch (Exception e2) {
                            return;
                        }
                    } else {
                        return;
                    }
                case 3:
                    Log.i(DownloadService.this.tag, "onCanceled");
                    DownloadThread.DownloadBean bean2 = (DownloadThread.DownloadBean) msg.obj;
                    DownloadService.this.threadMap.remove(bean2.threadName);
                    DownloadService.this.downloadingList.remove(bean2.threadName);
                    DownloadService.this.cancelNotification(bean2.threadName, bean2.filename);
                    if (DownloadService.this.registListener != null) {
                        try {
                            DownloadService.this.registListener.onCanceled(bean2.threadName, bean2);
                        } catch (Exception e3) {
                        }
                    }
                    Toast.makeText(DownloadService.this, String.valueOf(bean2.filename) + " download has been canceled", 1).show();
                    return;
                case AdWhirlUtil.NETWORK_TYPE_MEDIALETS:
                    Log.i(DownloadService.this.tag, "onPaused");
                    DownloadThread.DownloadBean bean3 = (DownloadThread.DownloadBean) msg.obj;
                    DownloadService.this.threadMap.remove(bean3.threadName);
                    DownloadService.this.downloadingList.remove(bean3.threadName);
                    if (DownloadService.this.registListener != null) {
                        try {
                            DownloadService.this.registListener.onPaused(bean3.threadName, bean3);
                            return;
                        } catch (Exception e4) {
                            return;
                        }
                    } else {
                        return;
                    }
                case AdWhirlUtil.NETWORK_TYPE_LIVERAIL:
                    Log.i(DownloadService.this.tag, "onFinished");
                    DownloadThread.DownloadBean bean4 = (DownloadThread.DownloadBean) msg.obj;
                    DownloadService.this.threadMap.remove(bean4.threadName);
                    DownloadService.this.downloadingList.remove(bean4.threadName);
                    Log.i(DownloadService.this.tag, "onFinished,downloadingList size=" + DownloadService.this.downloadingList.size());
                    DownloadService.this.finishNotification(bean4.threadName, bean4.filename);
                    if (DownloadService.this.registListener != null) {
                        try {
                            DownloadService.this.registListener.onFinished(bean4.threadName, bean4);
                        } catch (Exception e5) {
                        }
                    }
                    Toast.makeText(DownloadService.this, String.valueOf(bean4.filename) + " has been downloaded success", 1).show();
                    DownloadService.this.refresh();
                    return;
                case AdWhirlUtil.NETWORK_TYPE_MILLENNIAL:
                    DownloadThread.DownloadBean bean5 = (DownloadThread.DownloadBean) msg.obj;
                    DownloadService.this.threadMap.remove(bean5.threadName);
                    DownloadService.this.downloadingList.remove(bean5.threadName);
                    Log.i(DownloadService.this.tag, "onError:" + bean5.threadName + ",error=" + bean5.error);
                    if (DownloadService.this.registListener != null) {
                        try {
                            DownloadService.this.registListener.onError(bean5.threadName, bean5, bean5.error);
                        } catch (Exception e6) {
                        }
                    }
                    DownloadService.this.errorNotification(bean5.threadName, bean5.filename);
                    Toast.makeText(DownloadService.this, String.valueOf(bean5.filename) + " download error", 1).show();
                    return;
                default:
                    return;
            }
        }
    }
}
