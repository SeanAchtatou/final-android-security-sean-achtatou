package com.flypaul.music;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;

public class SongBean implements Parcelable, Serializable {
    public static final Parcelable.Creator<SongBean> CREATOR = new Parcelable.Creator<SongBean>() {
        public SongBean createFromParcel(Parcel source) {
            SongBean bean = new SongBean();
            bean.m_status = source.readInt();
            bean.m_downloadKey = source.readInt();
            bean.m_downloadingProgress = source.readInt();
            bean.m_album = source.readString();
            bean.m_downloadLink = source.readString();
            bean.m_downloadUri = source.readString();
            bean.m_fileName = source.readString();
            bean.m_singer = source.readString();
            bean.m_size = source.readString();
            bean.m_songLyric = source.readString();
            bean.m_songLyricUri = source.readString();
            bean.m_songName = source.readString();
            return bean;
        }

        public SongBean[] newArray(int size) {
            return new SongBean[size];
        }
    };
    public static final int STATUS_CANCELDOWNLOADING = 2;
    public static final int STATUS_DOWNLOADFINISH = 3;
    public static final int STATUS_DOWNLOADING = 1;
    public static final int STATUS_NULL = 0;
    private static final long serialVersionUID = -3774164612585812724L;
    public String m_album;
    public int m_downloadKey;
    public String m_downloadLink;
    public String m_downloadUri;
    public int m_downloadingProgress;
    public String m_fileName;
    public String m_singer;
    public String m_size;
    public String m_songLyric;
    public String m_songLyricUri;
    public String m_songName;
    public volatile int m_status;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.m_status);
        dest.writeInt(this.m_downloadKey);
        dest.writeInt(this.m_downloadingProgress);
        dest.writeString(this.m_album);
        dest.writeString(this.m_downloadLink);
        dest.writeString(this.m_downloadUri);
        dest.writeString(this.m_fileName);
        dest.writeString(this.m_singer);
        dest.writeString(this.m_size);
        dest.writeString(this.m_songLyric);
        dest.writeString(this.m_songLyricUri);
        dest.writeString(this.m_songName);
    }
}
