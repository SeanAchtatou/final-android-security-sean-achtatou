package com.flypaul.music.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.flypaul.music.R;

public class CommonDialog extends Dialog {
    public Button m_cancel = ((Button) findViewById(R.id.dialog_cancel_btn_id));
    public ImageView m_dialog_frame_icon = ((ImageView) findViewById(R.id.dialog_frame_icon));
    public TextView m_dialog_frame_title = ((TextView) findViewById(R.id.dialog_frame_title));
    public TextView m_dialog_waiting_des = ((TextView) findViewById(R.id.dialog_waiting_des));
    public TextView m_downloadConfirm = ((TextView) findViewById(R.id.dialog_downloadConfirm));
    public View m_fetchLoadingURLLayout = findViewById(R.id.dialog_fetchLoadingURLLayout);
    public Button m_ok = ((Button) findViewById(R.id.dialog_ok_btn_id));

    public CommonDialog(Context context) {
        super(context, R.style.CommonDialogTheme);
        setContentView((int) R.layout.common_dialog);
    }
}
