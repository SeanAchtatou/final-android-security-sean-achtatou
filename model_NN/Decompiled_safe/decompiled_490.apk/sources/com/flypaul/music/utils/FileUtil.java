package com.flypaul.music.utils;

import com.flypaul.music.Config;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;

public class FileUtil {
    public static final String CHART_ENUK = "CHART_ENUK";
    public static final String CHART_SONG_TOP = "CHART_SONG_TOP";
    public static final String CHART_USABILL = "CHART_USABILL";
    public static final String CHART_WESTEN_TOP = "CHART_WESTEN_TOP";
    public static final String SEARCH_RESULT_NAME = "SEARCH_RESULT";
    public static String logTag = "FileUtil";

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0018 A[SYNTHETIC, Splitter:B:11:0x0018] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x002c A[SYNTHETIC, Splitter:B:23:0x002c] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x003c A[SYNTHETIC, Splitter:B:31:0x003c] */
    /* JADX WARNING: Removed duplicated region for block: B:47:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:48:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void writeObject(android.content.Context r7, java.lang.String r8, java.util.ArrayList r9) {
        /*
            r3 = 0
            java.io.ObjectOutputStream r4 = new java.io.ObjectOutputStream     // Catch:{ FileNotFoundException -> 0x0015, Exception -> 0x001e }
            r5 = 0
            java.io.FileOutputStream r5 = r7.openFileOutput(r8, r5)     // Catch:{ FileNotFoundException -> 0x0015, Exception -> 0x001e }
            r4.<init>(r5)     // Catch:{ FileNotFoundException -> 0x0015, Exception -> 0x001e }
            r4.writeObject(r9)     // Catch:{ FileNotFoundException -> 0x004c, Exception -> 0x0048, all -> 0x0045 }
            if (r4 == 0) goto L_0x004f
            r4.close()     // Catch:{ IOException -> 0x0040 }
            r3 = r4
        L_0x0014:
            return
        L_0x0015:
            r5 = move-exception
        L_0x0016:
            if (r3 == 0) goto L_0x0014
            r3.close()     // Catch:{ IOException -> 0x001c }
            goto L_0x0014
        L_0x001c:
            r5 = move-exception
            goto L_0x0014
        L_0x001e:
            r5 = move-exception
            r1 = r5
        L_0x0020:
            boolean r0 = r7.deleteFile(r8)     // Catch:{ all -> 0x0039 }
            r2 = 0
        L_0x0025:
            if (r0 != 0) goto L_0x002a
            r5 = 3
            if (r2 < r5) goto L_0x0032
        L_0x002a:
            if (r3 == 0) goto L_0x0014
            r3.close()     // Catch:{ IOException -> 0x0030 }
            goto L_0x0014
        L_0x0030:
            r5 = move-exception
            goto L_0x0014
        L_0x0032:
            boolean r0 = r7.deleteFile(r8)     // Catch:{ all -> 0x0039 }
            int r2 = r2 + 1
            goto L_0x0025
        L_0x0039:
            r5 = move-exception
        L_0x003a:
            if (r3 == 0) goto L_0x003f
            r3.close()     // Catch:{ IOException -> 0x0043 }
        L_0x003f:
            throw r5
        L_0x0040:
            r5 = move-exception
            r3 = r4
            goto L_0x0014
        L_0x0043:
            r6 = move-exception
            goto L_0x003f
        L_0x0045:
            r5 = move-exception
            r3 = r4
            goto L_0x003a
        L_0x0048:
            r5 = move-exception
            r1 = r5
            r3 = r4
            goto L_0x0020
        L_0x004c:
            r5 = move-exception
            r3 = r4
            goto L_0x0016
        L_0x004f:
            r3 = r4
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flypaul.music.utils.FileUtil.writeObject(android.content.Context, java.lang.String, java.util.ArrayList):void");
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v7, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.util.ArrayList} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x001d A[SYNTHETIC, Splitter:B:12:0x001d] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0026 A[SYNTHETIC, Splitter:B:17:0x0026] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.ArrayList readObject(android.content.Context r6, java.lang.String r7) {
        /*
            r1 = 0
            r3 = 0
            java.io.ObjectInputStream r2 = new java.io.ObjectInputStream     // Catch:{ Exception -> 0x001a, all -> 0x0023 }
            java.io.FileInputStream r4 = r6.openFileInput(r7)     // Catch:{ Exception -> 0x001a, all -> 0x0023 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x001a, all -> 0x0023 }
            java.lang.Object r4 = r2.readObject()     // Catch:{ Exception -> 0x0032, all -> 0x002f }
            r0 = r4
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ Exception -> 0x0032, all -> 0x002f }
            r3 = r0
            if (r2 == 0) goto L_0x0035
            r2.close()     // Catch:{ IOException -> 0x002a }
            r1 = r2
        L_0x0019:
            return r3
        L_0x001a:
            r4 = move-exception
        L_0x001b:
            if (r1 == 0) goto L_0x0019
            r1.close()     // Catch:{ IOException -> 0x0021 }
            goto L_0x0019
        L_0x0021:
            r4 = move-exception
            goto L_0x0019
        L_0x0023:
            r4 = move-exception
        L_0x0024:
            if (r1 == 0) goto L_0x0029
            r1.close()     // Catch:{ IOException -> 0x002d }
        L_0x0029:
            throw r4
        L_0x002a:
            r4 = move-exception
            r1 = r2
            goto L_0x0019
        L_0x002d:
            r5 = move-exception
            goto L_0x0029
        L_0x002f:
            r4 = move-exception
            r1 = r2
            goto L_0x0024
        L_0x0032:
            r4 = move-exception
            r1 = r2
            goto L_0x001b
        L_0x0035:
            r1 = r2
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flypaul.music.utils.FileUtil.readObject(android.content.Context, java.lang.String):java.util.ArrayList");
    }

    private static File createSDDir() {
        File file = new File(Config.DOWNLOAD_PATH);
        if (!file.exists()) {
            int i = 0;
            while (!file.mkdirs() && i < 3) {
                i++;
            }
        }
        return file;
    }

    public static File createDownloadFile(String s) throws IOException {
        createSDDir();
        String s2 = String.valueOf(Config.DOWNLOAD_PATH) + s;
        File file = new File(s2);
        int i = 1;
        while (file.exists()) {
            int index = s2.lastIndexOf(".");
            file = new File(s2.substring(0, index) + "_" + i + s2.substring(index));
            i++;
        }
        file.createNewFile();
        return file;
    }

    public static File createFile(String path) throws IOException {
        createSDDir();
        File file = new File(path);
        int i = 1;
        while (file.exists()) {
            int index = path.lastIndexOf(".");
            file = new File(path.substring(0, index) + "_" + i + path.substring(index));
            i++;
        }
        file.createNewFile();
        return file;
    }

    public static boolean renameFile(String path, String name) {
        if (path == null) {
            return false;
        }
        File file = new File(path);
        if (file == null || !file.exists()) {
            return false;
        }
        return file.renameTo(new File(String.valueOf(file.getParent()) + "/" + name + path.substring(path.lastIndexOf("."))));
    }

    public static boolean deleteFile(String path) {
        if (path == null) {
            return false;
        }
        File file = new File(path);
        if (!file.exists()) {
            return false;
        }
        boolean df = file.delete();
        int i = 0;
        while (!df && i < 3) {
            df = file.delete();
            i++;
        }
        return df;
    }

    public static boolean deleteFile(File f) {
        if (f == null || !f.exists()) {
            return false;
        }
        boolean df = f.delete();
        int i = 0;
        while (!df && i < 3) {
            df = f.delete();
            i++;
        }
        return df;
    }

    public static String getSDPATH() {
        return Config.SDPATH;
    }

    public static boolean isFileExist(String s) {
        return new File(String.valueOf(Config.SDPATH) + s).exists();
    }

    public static boolean isFileExistInDownloadPath(String filename) {
        return new File(Config.DOWNLOAD_PATH + filename).exists();
    }

    public static ArrayList<File> getAllSongFiles() {
        return new ArrayList<>();
    }

    public static ArrayList<File> getFileListInDownloadPath() {
        File[] arr = new File(Config.DOWNLOAD_PATH).listFiles(new FilenameFilter() {
            public boolean accept(File dir, String filename) {
                if (!filename.endsWith(Config.LYRIC_SUFFIX)) {
                    return true;
                }
                return false;
            }
        });
        ArrayList<File> l = new ArrayList<>();
        if (arr != null && arr.length > 0) {
            for (File f : arr) {
                l.add(f);
            }
        }
        return l;
    }
}
