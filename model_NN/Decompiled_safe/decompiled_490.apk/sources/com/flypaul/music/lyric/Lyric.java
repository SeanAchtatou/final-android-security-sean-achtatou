package com.flypaul.music.lyric;

import com.flypaul.music.Config;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lyric implements Serializable {
    private static Logger log = Logger.getLogger(Lyric.class.getName());
    private static final Pattern pattern = Pattern.compile("(?<=\\[).*?(?=\\])");
    private static final long serialVersionUID = 20071125;
    private String HOME = "/sdcard/UmilePlayer/music/";
    private int currentIndex;
    private long during = 2147483647L;
    private boolean enabled = true;
    private transient File file;
    private int height;
    private transient PlayListItem info;
    /* access modifiers changed from: private */
    public boolean initDone;
    private boolean isMoving;
    List<Sentence> list = new ArrayList();
    private int offset;
    private long tempTime;
    private long time;
    private int width;

    public Lyric(final PlayListItem info2) {
        this.offset = info2.getOffset();
        this.info = info2;
        this.file = info2.getLyricFile();
        if (this.file == null || !this.file.exists()) {
            new Thread() {
                public void run() {
                    Lyric.this.doInit(info2);
                    Lyric.this.initDone = true;
                }
            }.start();
            return;
        }
        init(this.file);
        this.initDone = true;
    }

    public Lyric(File file2, PlayListItem info2) {
        System.out.println(" Lyric file" + file2);
        this.offset = info2.getOffset();
        this.file = file2;
        this.info = info2;
        init(file2);
        this.initDone = true;
    }

    public Lyric(String lyric, PlayListItem info2) {
        this.offset = info2.getOffset();
        this.info = info2;
        init(lyric);
        this.initDone = true;
    }

    /* access modifiers changed from: private */
    public void doInit(PlayListItem info2) {
        init(info2);
        if (this.list.size() == 1) {
            Sentence temp = this.list.remove(0);
            if ("" != 0) {
                init("");
            }
            this.list.add(temp);
        }
    }

    private void saveLyric(String lyric, PlayListItem info2) {
        try {
            File dir = new File(this.HOME, "Lyrics" + File.separator);
            dir.mkdirs();
            this.file = new File(dir, String.valueOf(info2.getFormattedName()) + Config.LYRIC_SUFFIX);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.file), "GBK"));
            bw.write(lyric);
            bw.close();
            info2.setLyricFile(this.file);
        } catch (Exception e) {
        }
    }

    public void setEnabled(boolean b) {
        this.enabled = b;
    }

    public File getLyricFile() {
        return this.file;
    }

    public void adjustTime(int time2) {
        if (this.list.size() != 1) {
            this.offset += time2;
            this.info.setOffset(this.offset);
        }
    }

    private File getMathedLyricFile(File dir, PlayListItem info2) {
        for (File f : dir.listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                return pathname.getName().toLowerCase().endsWith(Config.LYRIC_SUFFIX);
            }
        })) {
            if (matchAll(info2, f) || matchSongName(info2, f)) {
                return f;
            }
        }
        return null;
    }

    private void init(PlayListItem info2) {
        File matched = null;
        File dir = new File(this.HOME, "Lyrics" + File.separator);
        if (!dir.exists()) {
            dir.mkdirs();
            matched = getMathedLyricFile(dir, info2);
        }
        if (matched == null || !matched.exists()) {
            init("");
            return;
        }
        info2.setLyricFile(matched);
        this.file = matched;
        init(matched);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, ?[OBJECT, ARRAY], java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private void init(File file2) {
        Exception ex;
        BufferedReader br = null;
        try {
            BufferedReader br2 = new BufferedReader(new InputStreamReader(new FileInputStream(file2), "UTF-8"));
            try {
                StringBuilder sb = new StringBuilder();
                while (true) {
                    String temp = br2.readLine();
                    if (temp == null) {
                        init(sb.toString());
                        try {
                            br2.close();
                            return;
                        } catch (Exception ex2) {
                            Logger.getLogger(Lyric.class.getName()).log(Level.SEVERE, (String) null, (Throwable) ex2);
                            return;
                        }
                    } else {
                        sb.append(temp).append("\n");
                    }
                }
            } catch (Exception e) {
                ex = e;
                br = br2;
            } catch (Throwable th) {
                th = th;
                br = br2;
                try {
                    br.close();
                } catch (Exception ex3) {
                    Logger.getLogger(Lyric.class.getName()).log(Level.SEVERE, (String) null, (Throwable) ex3);
                }
                throw th;
            }
        } catch (Exception e2) {
            ex = e2;
            try {
                Logger.getLogger(Lyric.class.getName()).log(Level.SEVERE, (String) null, (Throwable) ex);
                try {
                    br.close();
                } catch (Exception ex4) {
                    Logger.getLogger(Lyric.class.getName()).log(Level.SEVERE, (String) null, (Throwable) ex4);
                }
            } catch (Throwable th2) {
                th = th2;
                br.close();
                throw th;
            }
        }
    }

    private boolean matchAll(PlayListItem info2, File file2) {
        if (info2.getFormattedName().equals(file2.getName().substring(0, file2.getName().lastIndexOf(".")))) {
            return true;
        }
        return false;
    }

    private boolean matchSongName(PlayListItem info2, File file2) {
        String name = info2.getFormattedName();
        String rn = file2.getName().substring(0, file2.getName().lastIndexOf("."));
        if (name.equalsIgnoreCase(rn) || info2.getTitle().equalsIgnoreCase(rn)) {
            return true;
        }
        return false;
    }

    /* Debug info: failed to restart local var, previous not found, register: 18 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, ?[OBJECT, ARRAY], java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private void init(String content) {
        long length;
        if (content == null || content.trim().equals("")) {
            this.list.add(new Sentence(this.info.getFormattedName(), -2147483648L, 2147483647L));
            return;
        }
        try {
            BufferedReader br = new BufferedReader(new StringReader(content));
            while (true) {
                String temp = br.readLine();
                if (temp == null) {
                    break;
                }
                parseLine(temp.trim());
            }
            br.close();
            Collections.sort(this.list, new Comparator<Sentence>() {
                public int compare(Sentence o1, Sentence o2) {
                    return (int) (o1.getFromTime() - o2.getFromTime());
                }
            });
            if (this.list.size() == 0) {
                this.list.add(new Sentence(this.info.getFormattedName(), 0, 2147483647L));
                return;
            }
            this.list.add(0, new Sentence(this.info.getFormattedName(), 0, this.list.get(0).getFromTime()));
            int size = this.list.size();
            for (int i = 0; i < size; i++) {
                Sentence next = null;
                if (i + 1 < size) {
                    next = this.list.get(i + 1);
                }
                Sentence now = this.list.get(i);
                if (next != null) {
                    now.setToTime(next.getFromTime() - 1);
                }
            }
            if (this.list.size() == 1) {
                this.list.get(0).setToTime(2147483647L);
                return;
            }
            Sentence last = this.list.get(this.list.size() - 1);
            if (this.info == null) {
                length = 2147483647L;
            } else {
                length = (this.info.getLength() * 1000) + 1000;
            }
            last.setToTime(length);
        } catch (Exception e) {
            Logger.getLogger(Lyric.class.getName()).log(Level.SEVERE, (String) null, (Throwable) e);
        }
    }

    private int parseOffset(String str) {
        String[] ss = str.split("\\:");
        if (ss.length != 2) {
            return Integer.MAX_VALUE;
        }
        if (ss[0].equalsIgnoreCase("offset")) {
            return Integer.parseInt(ss[1]);
        }
        return Integer.MAX_VALUE;
    }

    private void parseLine(String line) {
        int i;
        if (!line.equals("")) {
            Matcher matcher = pattern.matcher(line);
            List<String> temp = new ArrayList<>();
            int lastIndex = -1;
            int lastLength = -1;
            while (matcher.find()) {
                String s = matcher.group();
                int index = line.indexOf("[" + s + "]");
                if (lastIndex != -1 && index - lastIndex > lastLength + 2) {
                    String content = line.substring(lastIndex + lastLength + 2, index);
                    for (String str : temp) {
                        long t = parseTime(str);
                        if (t != -1) {
                            System.out.println("content = " + content);
                            System.out.println("t = " + t);
                            this.list.add(new Sentence(content, t));
                        }
                    }
                    temp.clear();
                }
                temp.add(s);
                lastIndex = index;
                lastLength = s.length();
            }
            if (!temp.isEmpty()) {
                int length = lastLength + 2 + lastIndex;
                try {
                    if (length > line.length()) {
                        i = line.length();
                    } else {
                        i = length;
                    }
                    String content2 = line.substring(i);
                    if (!content2.equals("") || this.offset != 0) {
                        for (String s2 : temp) {
                            long t2 = parseTime(s2);
                            if (t2 != -1) {
                                this.list.add(new Sentence(content2, t2));
                                System.out.println("content = " + content2);
                                System.out.println("t = " + t2);
                            }
                        }
                        return;
                    }
                    for (String s3 : temp) {
                        int of = parseOffset(s3);
                        if (of != Integer.MAX_VALUE) {
                            this.offset = of;
                            this.info.setOffset(this.offset);
                            return;
                        }
                    }
                } catch (Exception e) {
                }
            }
        }
    }

    private long parseTime(String time2) {
        String[] ss = time2.split("\\:|\\.");
        if (ss.length < 2) {
            return -1;
        }
        if (ss.length == 2) {
            try {
                if (this.offset != 0 || !ss[0].equalsIgnoreCase("offset")) {
                    int min = Integer.parseInt(ss[0]);
                    int sec = Integer.parseInt(ss[1]);
                    if (min >= 0 && sec >= 0 && sec < 60) {
                        return ((long) ((min * 60) + sec)) * 1000;
                    }
                    throw new RuntimeException("number is illegal!");
                }
                this.offset = Integer.parseInt(ss[1]);
                this.info.setOffset(this.offset);
                return -1;
            } catch (Exception e) {
                return -1;
            }
        } else if (ss.length != 3) {
            return -1;
        } else {
            try {
                int min2 = Integer.parseInt(ss[0]);
                int sec2 = Integer.parseInt(ss[1]);
                int mm = Integer.parseInt(ss[2]);
                if (min2 >= 0 && sec2 >= 0 && sec2 < 60 && mm >= 0 && mm <= 99) {
                    return (((long) ((min2 * 60) + sec2)) * 1000) + ((long) (mm * 10));
                }
                throw new RuntimeException("number is illegal!");
            } catch (Exception e2) {
                return -1;
            }
        }
    }

    public void setHeight(int height2) {
        this.height = height2;
    }

    public void setWidth(int width2) {
        this.width = width2;
    }

    public void setTime(long time2) {
        if (!this.isMoving) {
            long j = ((long) this.offset) + time2;
            this.time = j;
            this.tempTime = j;
        }
    }

    public boolean isInitDone() {
        return this.initDone;
    }

    /* access modifiers changed from: package-private */
    public int getNowSentenceIndex(long t) {
        for (int i = 0; i < this.list.size(); i++) {
            if (this.list.get(i).isInTime(t)) {
                return i;
            }
        }
        return -1;
    }

    public boolean canMove() {
        return this.list.size() > 1 && this.enabled;
    }

    public long getTime() {
        return this.tempTime;
    }

    private void checkTempTime() {
        if (this.tempTime < 0) {
            this.tempTime = 0;
        } else if (this.tempTime > this.during) {
            this.tempTime = this.during;
        }
    }

    public void startMove() {
        this.isMoving = true;
    }

    public void stopMove() {
        this.isMoving = false;
    }
}
