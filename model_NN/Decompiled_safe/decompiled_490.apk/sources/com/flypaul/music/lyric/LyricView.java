package com.flypaul.music.lyric;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;
import com.flypaul.music.Config;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class LyricView extends TextView {
    private static final int DY = 30;
    private static float LYRIC_INTERVAL = 0.0f;
    private static Lyric mLyric;
    private long currentDunringTime;
    public int index = 0;
    private List<Sentence> list;
    private Paint mPaint;
    private Paint mPathPaint;
    public float mTouchHistoryY;
    private float mX;
    private int mY;
    private long m_currentTime;
    private float m_density;
    private int m_height;
    private String m_matchLrcFileName;
    private int m_sentenceIndex;
    private int m_width;
    private float middleY;
    private File playFile;
    private int status = 0;
    public String test = "test";

    public LyricView(Context context) {
        super(context);
        this.m_density = context.getResources().getDisplayMetrics().density;
        init();
    }

    public LyricView(Context context, AttributeSet attr) {
        super(context, attr);
        this.m_density = context.getResources().getDisplayMetrics().density;
        init();
    }

    public LyricView(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
        this.m_density = context.getResources().getDisplayMetrics().density;
        init();
    }

    private void init() {
        setFocusable(true);
        this.mPaint = new Paint();
        this.mPaint.setAntiAlias(true);
        this.mPaint.setTextSize(this.m_density * 15.0f);
        this.mPaint.setColor(-1);
        this.mPaint.setTypeface(Typeface.SERIF);
        this.mPathPaint = new Paint();
        this.mPathPaint.setAntiAlias(true);
        this.mPathPaint.setColor(-256);
        this.mPathPaint.setTextSize(this.m_density * 15.0f);
        this.mPathPaint.setTypeface(Typeface.SANS_SERIF);
        this.mPathPaint.setFakeBoldText(true);
    }

    public void initLyric(File paramFile) {
        if (paramFile != null) {
            this.m_sentenceIndex = 0;
            String str1 = paramFile.getName();
            String str3 = String.valueOf(String.valueOf(str1.substring(0, str1.lastIndexOf(".")))) + Config.LYRIC_SUFFIX;
            String str5 = String.valueOf(String.valueOf(Config.DOWNLOAD_PATH)) + str3;
            this.m_matchLrcFileName = str3;
            if (new File(str5).exists()) {
                mLyric = new Lyric(new File(str5), new PlayListItem(str1, paramFile.getPath(), 0, true));
                this.list = mLyric.list;
                this.status = 1;
                return;
            }
            this.status = 2;
        }
    }

    public void initLyric(String songName) {
        if (songName != null && !songName.equals("")) {
            this.m_sentenceIndex = 0;
            String str3 = String.valueOf(songName) + Config.LYRIC_SUFFIX;
            String str4 = String.valueOf(Config.DOWNLOAD_PATH);
            String str5 = String.valueOf(str4) + str3;
            this.m_matchLrcFileName = str3;
            if (new File(str5).exists()) {
                mLyric = new Lyric(new File(str5), new PlayListItem(String.valueOf(songName) + ".mp3", str4, 0, true));
                this.list = mLyric.list;
                this.status = 1;
                return;
            }
            this.status = 2;
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        float plus;
        super.onDraw(canvas);
        canvas.drawColor(251592703);
        Paint p = this.mPaint;
        Paint p2 = this.mPathPaint;
        p.setTextAlign(Paint.Align.CENTER);
        if (this.index != -1) {
            if (this.status == 2) {
                canvas.drawText(String.valueOf(this.m_matchLrcFileName) + " is not found", this.mX, this.middleY - 50.0f, this.mPaint);
                canvas.drawText("in the folder of " + Config.DOWNLOAD_PATH, this.mX, this.middleY, this.mPaint);
            } else if (mLyric != null && this.m_sentenceIndex != -1) {
                canvas.save();
                this.list = mLyric.list;
                long sentenceTime = this.list.get(this.m_sentenceIndex).getFromTime();
                if (this.currentDunringTime == 0) {
                    plus = 30.0f;
                } else {
                    plus = ((((float) this.m_currentTime) - ((float) sentenceTime)) / ((float) this.currentDunringTime)) * 30.0f;
                }
                canvas.translate(0.0f, -plus);
                p2.setTextAlign(Paint.Align.CENTER);
                canvas.drawText(this.list.get(this.m_sentenceIndex).getContent(), this.mX, this.middleY, p2);
                float tempY = this.middleY;
                for (int i = this.m_sentenceIndex - 1; i >= 0; i--) {
                    tempY -= 30.0f;
                    if (tempY < 0.0f) {
                        break;
                    }
                    canvas.drawText(this.list.get(i).getContent(), this.mX, tempY, p);
                }
                float tempY2 = this.middleY;
                int i2 = this.m_sentenceIndex + 1;
                while (i2 < this.list.size()) {
                    tempY2 += 30.0f;
                    if (tempY2 <= ((float) this.mY)) {
                        canvas.drawText(this.list.get(i2).getContent(), this.mX, tempY2, p);
                        i2++;
                    } else {
                        return;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int ow, int oh) {
        super.onSizeChanged(w, h, ow, oh);
        this.mX = ((float) w) * 0.5f;
        this.mY = h;
        this.middleY = ((float) h) * 0.5f;
    }

    public long updateIndex(long time) {
        this.m_sentenceIndex = mLyric.getNowSentenceIndex(time);
        if (this.m_sentenceIndex == -1) {
            return -1;
        }
        long during = this.list.get(this.m_sentenceIndex).getDuring();
        this.currentDunringTime = during;
        return during;
    }

    public long updateIndex(long time, File file) {
        if (this.status == 2) {
            return 0;
        }
        if (mLyric == null) {
            initLyric(file);
            return 0;
        }
        this.m_currentTime = time;
        this.m_sentenceIndex = mLyric.getNowSentenceIndex(time);
        if (this.m_sentenceIndex == -1) {
            return -1;
        }
        this.currentDunringTime = ((Sentence) ((ArrayList) mLyric.list).get(this.m_sentenceIndex)).getDuring();
        return this.currentDunringTime;
    }
}
