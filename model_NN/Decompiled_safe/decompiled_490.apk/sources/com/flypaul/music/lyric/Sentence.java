package com.flypaul.music.lyric;

import java.io.Serializable;

public class Sentence implements Serializable {
    private static final long DISAPPEAR_TIME = 1000;
    private static final long serialVersionUID = 20071125;
    private String content;
    private long fromTime;
    private long toTime;

    public Sentence(String content2, long fromTime2, long toTime2) {
        this.content = content2;
        this.fromTime = fromTime2;
        this.toTime = toTime2;
    }

    public Sentence(String content2, long fromTime2) {
        this(content2, fromTime2, 0);
    }

    public Sentence(String content2) {
        this(content2, 0, 0);
    }

    public long getFromTime() {
        return this.fromTime;
    }

    public void setFromTime(long fromTime2) {
        this.fromTime = fromTime2;
    }

    public long getToTime() {
        return this.toTime;
    }

    public void setToTime(long toTime2) {
        this.toTime = toTime2;
    }

    public boolean isInTime(long time) {
        return time >= this.fromTime && time <= this.toTime;
    }

    public String getContent() {
        return this.content;
    }

    public long getDuring() {
        return this.toTime - this.fromTime;
    }

    public String toString() {
        return "{" + this.fromTime + "(" + this.content + ")" + this.toTime + "}";
    }
}
