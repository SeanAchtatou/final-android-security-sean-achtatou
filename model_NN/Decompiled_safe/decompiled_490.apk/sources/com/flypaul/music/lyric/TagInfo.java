package com.flypaul.music.lyric;

import java.io.Serializable;
import java.util.Vector;

public interface TagInfo extends Serializable {
    String getAlbum();

    String getArtist();

    int getBitRate();

    int getChannels();

    Vector getComment();

    String getGenre();

    long getPlayTime();

    int getSamplingRate();

    String getTitle();

    String getTrack();

    String getType();

    String getYear();
}
