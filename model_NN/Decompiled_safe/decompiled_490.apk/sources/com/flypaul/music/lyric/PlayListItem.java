package com.flypaul.music.lyric;

import android.util.Log;
import java.io.File;
import java.io.Serializable;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class PlayListItem implements Serializable {
    protected static ExecutorService es = Executors.newSingleThreadExecutor();
    private static Logger log = Logger.getLogger(PlayListItem.class.getName());
    private static final long serialVersionUID = 20071213;
    private String album;
    private String artist;
    private String bitRate;
    private String channels;
    private String comment;
    protected String displayName = "";
    private String genre;
    protected boolean isFile = true;
    private boolean isRead;
    protected boolean isSelected = false;
    protected String location = "";
    private File lyricFile;
    protected String name = "";
    private int offset;
    private String sampled;
    protected long seconds = -1;
    protected TagInfo taginfo = null;
    private String title;
    private String track;
    private String year;

    protected PlayListItem() {
    }

    public boolean isInited() {
        return this.isRead;
    }

    public PlayListItem(String name2, String location2, long seconds2, boolean isFile2) {
        Log.v("PlayListItem", "PlayListItem");
        this.name = name2;
        this.seconds = seconds2;
        this.isFile = isFile2;
        this.location = location2;
    }

    public int getOffset() {
        return this.offset;
    }

    public void setOffset(int offset2) {
        this.offset = offset2;
    }

    public String getGenre() {
        return this.genre;
    }

    public void setGenre(String genre2) {
        this.genre = genre2;
    }

    public String getTrack() {
        return this.track;
    }

    public void setTrack(String track2) {
        this.track = track2;
    }

    public File getLyricFile() {
        System.out.println("lyricFile=" + this.lyricFile);
        return this.lyricFile;
    }

    public void setLyricFile(File lyricFile2) {
        this.lyricFile = lyricFile2;
    }

    public boolean isValid() {
        return getTagInfo() != null;
    }

    public void setDuration(long sec) {
        this.seconds = sec;
    }

    public void setBitRate(String s) {
        this.bitRate = s;
    }

    public void setSampled(String s) {
        this.sampled = s;
    }

    public String getBitRate() {
        int bit;
        if (this.bitRate == null && (bit = getBitrate()) > 0) {
            int bit2 = Math.round((float) (bit / 1000));
            if (bit2 > 999) {
                this.bitRate = String.valueOf(bit2 / 100) + "Hkbps";
            } else {
                this.bitRate = String.valueOf(String.valueOf(bit2)) + "kbps";
            }
        }
        return this.bitRate;
    }

    public String getSampled() {
        int sam;
        if (this.sampled == null && (sam = getSamplerate()) > 0) {
            this.sampled = String.valueOf(String.valueOf(Math.round((float) (sam / 1000)))) + "kHz";
        }
        return this.sampled;
    }

    public String getFormattedName() {
        return this.name;
    }

    public String getName() {
        return this.name;
    }

    public String getLocation() {
        return this.location;
    }

    public boolean isFile() {
        return this.isFile;
    }

    public void setFile(boolean b) {
        this.isFile = b;
    }

    public long getLength() {
        if (this.taginfo == null || this.taginfo.getPlayTime() <= 0) {
            return this.seconds;
        }
        return this.taginfo.getPlayTime();
    }

    public int getBitrate() {
        if (this.taginfo != null) {
            return this.taginfo.getBitRate();
        }
        return -1;
    }

    public void setAlbum(String album2) {
        this.album = album2;
    }

    public String getAlbum() {
        if (this.album != null) {
            return this.album;
        }
        TagInfo tagInfo = getTagInfo();
        this.taginfo = tagInfo;
        if (tagInfo == null) {
            return null;
        }
        this.album = this.taginfo.getAlbum();
        return this.album;
    }

    public String getChannelInfo() {
        return this.channels;
    }

    public void setChannels(String s) {
        this.channels = s;
    }

    public int getSamplerate() {
        if (this.taginfo != null) {
            return this.taginfo.getSamplingRate();
        }
        return -1;
    }

    public int getChannels() {
        if (this.taginfo != null) {
            return this.taginfo.getChannels();
        }
        return -1;
    }

    public String getComment() {
        Vector v;
        if (this.comment != null) {
            return this.comment;
        }
        TagInfo tagInfo = getTagInfo();
        this.taginfo = tagInfo;
        if (tagInfo == null || (v = this.taginfo.getComment()) == null) {
            return "";
        }
        this.comment = String.valueOf(v.get(0));
        return this.comment;
    }

    public void setComment(String comment2) {
        this.comment = comment2;
    }

    public String getYear() {
        if (this.year != null) {
            return this.year;
        }
        TagInfo tagInfo = getTagInfo();
        this.taginfo = tagInfo;
        if (tagInfo == null) {
            return "";
        }
        this.year = String.valueOf(this.taginfo.getYear());
        return this.year;
    }

    public void setYear(String year2) {
        this.year = year2;
    }

    public void setSelected(boolean mode) {
        this.isSelected = mode;
    }

    public boolean isSelected() {
        return this.isSelected;
    }

    public void setLocation(String l) {
        this.location = l;
    }

    public TagInfo getTagInfo() {
        return this.taginfo;
    }

    public String getFormat() {
        return String.valueOf(this.location.substring(this.location.lastIndexOf(".") + 1)) + " " + getSampled() + " " + getBitRate();
    }

    public String getType() {
        TagInfo tag = getTagInfo();
        if (tag != null) {
            return tag.getType();
        }
        return null;
    }

    public String getTitle() {
        TagInfo tag = getTagInfo();
        if (tag != null) {
            this.title = tag.getTitle() == null ? this.name : tag.getTitle().trim();
        } else if (this.title != null) {
            return this.title;
        } else {
            this.title = this.name;
        }
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getArtist() {
        String trim;
        TagInfo tagInfo = getTagInfo();
        this.taginfo = tagInfo;
        if (tagInfo != null) {
            if (this.taginfo.getArtist() == null) {
                trim = "";
            } else {
                trim = this.taginfo.getArtist().trim();
            }
            this.artist = trim;
        } else if (this.artist != null) {
            return this.artist;
        }
        return this.artist;
    }

    public void setArtist(String artist2) {
        this.artist = artist2;
    }

    public String toString() {
        TagInfo tagInfo = getTagInfo();
        return this.displayName;
    }
}
