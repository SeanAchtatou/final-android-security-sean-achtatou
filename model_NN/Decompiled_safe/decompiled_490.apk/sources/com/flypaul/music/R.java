package com.flypaul.music;

public final class R {

    public static final class array {
        public static final int categories = 2130968576;
        public static final int chartTypes = 2130968577;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int pure_white = 2131034113;
        public static final int tab_select_textcolor = 2131034114;
        public static final int tab_unselect_textcolor = 2131034115;
        public static final int white = 2131034112;
    }

    public static final class drawable {
        public static final int about_icon_default = 2130837504;
        public static final int about_icon_pressed = 2130837505;
        public static final int back_btn_chart = 2130837506;
        public static final int back_btn_default1 = 2130837507;
        public static final int back_btn_lib = 2130837508;
        public static final int back_btn_pressed1 = 2130837509;
        public static final int bar_rotate = 2130837510;
        public static final int blue_thatch_portrait = 2130837511;
        public static final int btn_bg_select = 2130837512;
        public static final int btn_close_bg = 2130837513;
        public static final int btn_close_normal = 2130837514;
        public static final int btn_close_pressed = 2130837515;
        public static final int btn_close_selected = 2130837516;
        public static final int btn_disabled = 2130837517;
        public static final int btn_normal = 2130837518;
        public static final int btn_pressed = 2130837519;
        public static final int btn_selected = 2130837520;
        public static final int chart_icon_default = 2130837521;
        public static final int chart_icon_pressed = 2130837522;
        public static final int common_about = 2130837523;
        public static final int common_new = 2130837524;
        public static final int context_menu_icon = 2130837525;
        public static final int control_bg_green = 2130837526;
        public static final int dialog_bg_bottom = 2130837527;
        public static final int dialog_bg_top = 2130837528;
        public static final int dialog_center = 2130837529;
        public static final int dialog_default_icon = 2130837530;
        public static final int down_icon_default = 2130837531;
        public static final int down_icon_pressed = 2130837532;
        public static final int download_arrow = 2130837533;
        public static final int download_arrow_pressed = 2130837534;
        public static final int download_btn = 2130837535;
        public static final int download_progress_running = 2130837536;
        public static final int downloading_dialog_bg = 2130837585;
        public static final int edittextbg = 2130837537;
        public static final int edittextbg_focus = 2130837538;
        public static final int edittextselect = 2130837539;
        public static final int gml_large = 2130837540;
        public static final int gml_large1 = 2130837541;
        public static final int halftransparent = 2130837584;
        public static final int home_btn = 2130837542;
        public static final int home_default = 2130837543;
        public static final int home_pressed = 2130837544;
        public static final int icon = 2130837545;
        public static final int library_icon_default = 2130837546;
        public static final int library_icon_pressed = 2130837547;
        public static final int menu_btn_chart = 2130837548;
        public static final int menu_btn_download = 2130837549;
        public static final int menu_btn_library = 2130837550;
        public static final int menu_btn_play = 2130837551;
        public static final int menu_btn_search = 2130837552;
        public static final int next_button_default = 2130837553;
        public static final int next_button_pressed = 2130837554;
        public static final int next_button_select = 2130837555;
        public static final int no_transparent = 2130837582;
        public static final int pause_button_default = 2130837556;
        public static final int play_button_default = 2130837557;
        public static final int play_icon_default = 2130837558;
        public static final int play_icon_pressed = 2130837559;
        public static final int playmode_repeate_all_enable = 2130837560;
        public static final int playmode_repeate_random_hover = 2130837561;
        public static final int playmode_repeate_single_enable = 2130837562;
        public static final int pre_button_default = 2130837563;
        public static final int pre_button_pressed = 2130837564;
        public static final int pre_button_select = 2130837565;
        public static final int refresh_btn = 2130837566;
        public static final int refresh_default = 2130837567;
        public static final int refresh_pressed = 2130837568;
        public static final int search_default = 2130837569;
        public static final int search_icon = 2130837570;
        public static final int search_icon_default = 2130837571;
        public static final int search_icon_pressed = 2130837572;
        public static final int search_pressed = 2130837573;
        public static final int searchloading_bg = 2130837574;
        public static final int seekbar_img_select = 2130837575;
        public static final int select = 2130837576;
        public static final int select_over = 2130837577;
        public static final int selectitem = 2130837578;
        public static final int thumb = 2130837579;
        public static final int thumb_1 = 2130837580;
        public static final int thumb_2 = 2130837581;
        public static final int transparent = 2130837583;
    }

    public static final class id {
        public static final int ad_area_main = 2131230783;
        public static final int ad_area_player = 2131230784;
        public static final int currentTimeTV = 2131230792;
        public static final int dialog_bottom_viewgroup = 2131230731;
        public static final int dialog_cancel_btn_id = 2131230733;
        public static final int dialog_downloadConfirm = 2131230728;
        public static final int dialog_fetchLoadingURLLayout = 2131230729;
        public static final int dialog_frame_icon = 2131230726;
        public static final int dialog_frame_root = 2131230724;
        public static final int dialog_frame_title = 2131230727;
        public static final int dialog_ok_btn_id = 2131230732;
        public static final int dialog_top_pane = 2131230725;
        public static final int dialog_waiting_des = 2131230730;
        public static final int downing_cancel_down = 2131230742;
        public static final int downing_name = 2131230739;
        public static final int downing_percent = 2131230741;
        public static final int downing_progressbar = 2131230740;
        public static final int download_songinfo_displayname = 2131230735;
        public static final int download_songinfo_index = 2131230734;
        public static final int download_songinfo_size = 2131230736;
        public static final int durationTimeTV = 2131230793;
        public static final int fileDate = 2131230737;
        public static final int item_album = 2131230799;
        public static final int item_downloadIcon = 2131230802;
        public static final int item_downloadIcon_chart = 2131230723;
        public static final int item_songAuthor = 2131230798;
        public static final int item_songAuthor_chart = 2131230721;
        public static final int item_songName = 2131230797;
        public static final int item_songName_chart = 2131230720;
        public static final int item_songSize = 2131230800;
        public static final int lib_item_des = 2131230744;
        public static final int lib_item_img = 2131230743;
        public static final int linearLayout1 = 2131230738;
        public static final int listarea = 2131230775;
        public static final int lyricView = 2131230785;
        public static final int mainLayout = 2131230746;
        public static final int moreLoadingLayout = 2131230796;
        public static final int moreSongBar = 2131230794;
        public static final int moreSongText = 2131230795;
        public static final int nextControlBtn = 2131230789;
        public static final int page_chart_back_icon = 2131230772;
        public static final int page_chart_chartLoadingLayout = 2131230777;
        public static final int page_chart_chartPlayURLLoadingLayout = 2131230778;
        public static final int page_chart_home_icon = 2131230771;
        public static final int page_chart_list = 2131230776;
        public static final int page_chart_refresh_icon = 2131230774;
        public static final int page_chart_title_text = 2131230773;
        public static final int page_download_home_icon = 2131230780;
        public static final int page_download_listempty = 2131230781;
        public static final int page_download_songlist = 2131230782;
        public static final int page_library_back_icon = 2131230765;
        public static final int page_library_home_icon = 2131230764;
        public static final int page_library_list = 2131230768;
        public static final int page_library_loadingLayout = 2131230769;
        public static final int page_library_refresh_icon = 2131230767;
        public static final int page_library_title_text = 2131230766;
        public static final int page_main_aboutLayout = 2131230753;
        public static final int page_main_about_btn = 2131230752;
        public static final int page_main_about_text = 2131230754;
        public static final int page_main_chartLayout = 2131230770;
        public static final int page_main_chart_btn = 2131230748;
        public static final int page_main_downloadLayout = 2131230779;
        public static final int page_main_download_btn = 2131230750;
        public static final int page_main_libraryLayout = 2131230763;
        public static final int page_main_library_btn = 2131230749;
        public static final int page_main_mainLayout = 2131230745;
        public static final int page_main_play_btn = 2131230751;
        public static final int page_main_searchLayout = 2131230755;
        public static final int page_main_search_btn = 2131230747;
        public static final int page_search_listarea = 2131230759;
        public static final int page_search_playURLLoadingLayout = 2131230762;
        public static final int page_search_searchBtn = 2131230758;
        public static final int page_search_searchInput = 2131230757;
        public static final int page_search_searchList = 2131230760;
        public static final int page_search_searchLoadingLayout = 2131230761;
        public static final int playControl = 2131230786;
        public static final int playControlBtn = 2131230787;
        public static final int previousControlBtn = 2131230788;
        public static final int repeat_allBtn = 2131230790;
        public static final int searchArea = 2131230756;
        public static final int seekbar = 2131230791;
        public static final int separator_line = 2131230801;
        public static final int separator_line_chart = 2131230722;
    }

    public static final class layout {
        public static final int chartlist = 2130903040;
        public static final int common_dialog = 2130903041;
        public static final int download_song_item = 2130903042;
        public static final int downloading_item = 2130903043;
        public static final int library_item = 2130903044;
        public static final int main = 2130903045;
        public static final int notificaiton_downloading_item = 2130903046;
        public static final int player = 2130903047;
        public static final int search_footer = 2130903048;
        public static final int songlist = 2130903049;
    }

    public static final class string {
        public static final int about_text = 2131099698;
        public static final int app_name = 2131099648;
        public static final int app_version = 2131099697;
        public static final int cancel_text = 2131099654;
        public static final int delete_file_confirm = 2131099677;
        public static final int delete_file_fail_toast = 2131099675;
        public static final int delete_file_title = 2131099676;
        public static final int delete_file_toast = 2131099674;
        public static final int diaglog_downloadConfirmtxt = 2131099691;
        public static final int diaglog_fetchSongLinkNote = 2131099690;
        public static final int dialog_confirm_string = 2131099663;
        public static final int dialog_download_title = 2131099661;
        public static final int dialog_fetchurl = 2131099664;
        public static final int dialog_play_title = 2131099662;
        public static final int download_list_text = 2131099665;
        public static final int download_lyric_toast = 2131099670;
        public static final int download_noSD_toast = 2131099672;
        public static final int download_no_item_text = 2131099666;
        public static final int download_repeat = 2131099667;
        public static final int download_song_toast = 2131099671;
        public static final int download_text = 2131099653;
        public static final int error_desc = 2131099651;
        public static final int error_text = 2131099650;
        public static final int exit_desc = 2131099696;
        public static final int fetch_url_error = 2131099673;
        public static final int get_download_url_error = 2131099652;
        public static final int get_more_song = 2131099692;
        public static final int loading_more_results = 2131099693;
        public static final int main_menu_about = 2131099660;
        public static final int main_menu_chart = 2131099655;
        public static final int main_menu_download = 2131099658;
        public static final int main_menu_library = 2131099657;
        public static final int main_menu_rate = 2131099659;
        public static final int main_menu_search = 2131099656;
        public static final int menu_delete = 2131099686;
        public static final int menu_play = 2131099683;
        public static final int menu_rename = 2131099685;
        public static final int menu_share = 2131099684;
        public static final int no_lyric_toast = 2131099669;
        public static final int ok_text = 2131099649;
        public static final int play_url_getting = 2131099688;
        public static final int prompt_text = 2131099695;
        public static final int rename_conflick = 2131099679;
        public static final int rename_file_title = 2131099678;
        public static final int search_hint = 2131099668;
        public static final int search_menu_lyric = 2131099682;
        public static final int search_menu_no_lyric = 2131099687;
        public static final int search_menu_song = 2131099681;
        public static final int search_menu_title = 2131099680;
        public static final int search_waiting = 2131099689;
        public static final int song_menu_title = 2131099694;
    }

    public static final class style {
        public static final int CommonDialogTheme = 2131165186;
        public static final int CustomDialogTheme = 2131165185;
        public static final int CustomProgressBarTheme = 2131165188;
        public static final int CustomSeekBarTheme = 2131165187;
        public static final int noTile = 2131165184;
    }
}
