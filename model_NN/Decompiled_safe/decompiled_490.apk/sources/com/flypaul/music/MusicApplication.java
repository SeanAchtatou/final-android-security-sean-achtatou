package com.flypaul.music;

import android.app.Application;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.flypaul.music.services.DownloadService;
import com.flypaul.music.services.PlayService;

public class MusicApplication extends Application {
    /* access modifiers changed from: private */
    public PlayService m_playservice;
    private final ServiceConnection m_playserviceConnection = new PlayServiceConncetion(this, null);
    /* access modifiers changed from: private */
    public DownloadService m_service;
    private final ServiceConnection m_serviceConnection = new DownloadServiceConncetion(this, null);

    public DownloadService getDownloadService() {
        return this.m_service;
    }

    public PlayService getPlayService() {
        return this.m_playservice;
    }

    public void onCreate() {
        super.onCreate();
        Intent intent = new Intent(this, DownloadService.class);
        startService(intent);
        bindService(intent, this.m_serviceConnection, 1);
        Intent pintent = new Intent(this, PlayService.class);
        startService(intent);
        bindService(pintent, this.m_playserviceConnection, 1);
    }

    public void onTerminate() {
        super.onTerminate();
    }

    private class DownloadServiceConncetion implements ServiceConnection {
        private DownloadServiceConncetion() {
        }

        /* synthetic */ DownloadServiceConncetion(MusicApplication musicApplication, DownloadServiceConncetion downloadServiceConncetion) {
            this();
        }

        public void onServiceConnected(ComponentName componentname, IBinder ibinder) {
            MusicApplication.this.m_service = ((DownloadService.LocalBinder) ibinder).getService();
        }

        public void onServiceDisconnected(ComponentName componentname) {
            MusicApplication.this.m_service = null;
        }
    }

    private class PlayServiceConncetion implements ServiceConnection {
        private PlayServiceConncetion() {
        }

        /* synthetic */ PlayServiceConncetion(MusicApplication musicApplication, PlayServiceConncetion playServiceConncetion) {
            this();
        }

        public void onServiceConnected(ComponentName componentname, IBinder ibinder) {
            MusicApplication.this.m_playservice = ((PlayService.LocalBinder) ibinder).getService();
        }

        public void onServiceDisconnected(ComponentName componentname) {
            MusicApplication.this.m_playservice = null;
        }
    }
}
