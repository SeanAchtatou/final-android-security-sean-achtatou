package com.flypaul.music;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlManager;
import com.adwhirl.AdWhirlTargeting;
import com.adwhirl.util.AdWhirlUtil;
import com.flypaul.music.download.DownLoadedSongAdapter;
import com.flypaul.music.download.DownloadListener;
import com.flypaul.music.download.DownloadThread;
import com.flypaul.music.services.DownloadService;
import com.flypaul.music.services.PlayService;
import com.flypaul.music.utils.CommonDialog;
import com.flypaul.music.utils.FileUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class MainActivity extends Activity implements AdapterView.OnItemClickListener {
    /* access modifiers changed from: private */
    public static int m_search_pageIndex = 1;
    /* access modifiers changed from: private */
    public String logTag = "MainActivity";
    private LinearLayout m_adArea;
    /* access modifiers changed from: private */
    public ArrayList<SongBean> m_chart_ENUKList;
    /* access modifiers changed from: private */
    public ArrayList<SongBean> m_chart_SongTopList;
    /* access modifiers changed from: private */
    public ArrayList<SongBean> m_chart_USABillList;
    /* access modifiers changed from: private */
    public ArrayList<SongBean> m_chart_WestenTopList;
    private ImageView m_chart_backBtn;
    private ListView m_chart_chartList;
    /* access modifiers changed from: private */
    public LinearLayout m_chart_chartLoading;
    /* access modifiers changed from: private */
    public int m_chart_chartPosition = 0;
    /* access modifiers changed from: private */
    public CommonDialog m_chart_commonDialog;
    /* access modifiers changed from: private */
    public ChartCommonHandler m_chart_handler;
    private ImageView m_chart_homeBtn;
    /* access modifiers changed from: private */
    public LinearLayout m_chart_playLoading;
    private ImageView m_chart_refreshBtn;
    /* access modifiers changed from: private */
    public SongBean m_chart_selectedSong;
    /* access modifiers changed from: private */
    public ChartSongAdapter m_chart_songAdapter;
    private TextView m_chart_titleView;
    private String[] m_chart_typeTitles;
    private ArrayList<SongBean> m_chartm_RingList;
    /* access modifiers changed from: private */
    public DownloadService m_downloadBoundService;
    private DownloadListener m_download_Listener = new DownloadListener() {
        public void onBeforeDownload(String name, DownloadThread.DownloadBean bean) {
        }

        public void onCanceled(String name, DownloadThread.DownloadBean bean) {
            Message msg = MainActivity.this.m_download_handler.obtainMessage();
            msg.what = 1;
            MainActivity.this.m_download_handler.sendMessage(msg);
        }

        public void onPaused(String name, DownloadThread.DownloadBean bean) {
        }

        public void onFinished(String name, DownloadThread.DownloadBean bean) {
            Message msg = MainActivity.this.m_download_handler.obtainMessage();
            msg.what = 1;
            MainActivity.this.m_download_handler.sendMessage(msg);
        }

        public void onUpdateProcess(String name, int percent, int position) {
            if (System.currentTimeMillis() - MainActivity.this.m_download_preUpdateTime > 3000) {
                MainActivity.this.m_download_preUpdateTime = System.currentTimeMillis();
                Message msg = MainActivity.this.m_download_handler.obtainMessage();
                msg.what = 1;
                MainActivity.this.m_download_handler.sendMessage(msg);
            }
        }

        public void onError(String name, DownloadThread.DownloadBean bean, String error) {
            Message msg = MainActivity.this.m_download_handler.obtainMessage();
            msg.what = 1;
            MainActivity.this.m_download_handler.sendMessage(msg);
        }
    };
    /* access modifiers changed from: private */
    public DownloadingAdapter m_download_adapter;
    /* access modifiers changed from: private */
    public Collection<DownloadThread.DownloadBean> m_download_downBeanList;
    /* access modifiers changed from: private */
    public TextView m_download_emptyText;
    /* access modifiers changed from: private */
    public DownloadCommonHandler m_download_handler;
    private ImageView m_download_homeBtn;
    /* access modifiers changed from: private */
    public long m_download_preUpdateTime = 0;
    /* access modifiers changed from: private */
    public ListView m_download_songList;
    private HashMap<PageState, LinearLayout> m_layoutMap;
    private LibraryRefreshReceiver m_libraryRefreshReceiver;
    private ArrayList<File> m_library_allSongList;
    private ImageView m_library_backBtn;
    private ArrayList<File> m_library_downloadedList;
    private ImageView m_library_homeBtn;
    /* access modifiers changed from: private */
    public LinearLayout m_library_libLoadingLayout;
    /* access modifiers changed from: private */
    public int m_library_libPosition;
    /* access modifiers changed from: private */
    public TextView m_library_libTitle;
    /* access modifiers changed from: private */
    public ListView m_library_list;
    private ImageView m_library_refreshBtn;
    private File m_library_selectedFileItem;
    private Button m_main_aboutBtn;
    private LinearLayout m_main_aboutLayout;
    private TextView m_main_aboutText;
    private MainPageBtnsClickHandler m_main_btnsClickHandler;
    private Button m_main_chartBtn;
    private LinearLayout m_main_chartLayout;
    private Button m_main_downloadBtn;
    private LinearLayout m_main_downloadLayout;
    private Button m_main_libBtn;
    private LinearLayout m_main_libraryLayout;
    private LinearLayout m_main_mainLayout;
    private Button m_main_playBtn;
    private Button m_main_searchBtn;
    private LinearLayout m_main_searchLayout;
    private PageState m_pageState;
    private PlayService m_playservice;
    private SharedPreferences m_search_Prefs;
    /* access modifiers changed from: private */
    public CommonDialog m_search_commonDialog;
    /* access modifiers changed from: private */
    public Handler m_search_handler;
    /* access modifiers changed from: private */
    public RelativeLayout m_search_moreSongBar;
    /* access modifiers changed from: private */
    public TextView m_search_moreSongText;
    /* access modifiers changed from: private */
    public LinearLayout m_search_more_loading;
    /* access modifiers changed from: private */
    public ArrayList<SongBean> m_search_newSearchResult;
    /* access modifiers changed from: private */
    public LinearLayout m_search_play_waiting;
    private Button m_search_searchBtn;
    /* access modifiers changed from: private */
    public EditText m_search_searchInput;
    /* access modifiers changed from: private */
    public String m_search_searchKeyWord = "";
    private ListView m_search_searchList;
    /* access modifiers changed from: private */
    public LinearLayout m_search_searchLoadingLayout;
    /* access modifiers changed from: private */
    public ArrayList<SongBean> m_search_searchResult;
    /* access modifiers changed from: private */
    public SongBean m_search_selectedSong;
    /* access modifiers changed from: private */
    public SearchSongAdapter m_search_songAdapter;

    public enum ChartTypes {
        WestenTop,
        USABillBoard,
        ENUK,
        SongTop
    }

    public enum PageState {
        main,
        about,
        search,
        chart,
        libray,
        download
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.m_pageState = PageState.main;
        this.m_layoutMap = new HashMap<>();
        this.m_downloadBoundService = getService();
        this.m_playservice = getPlayService();
        this.m_main_btnsClickHandler = new MainPageBtnsClickHandler();
        this.m_main_searchBtn = (Button) findViewById(R.id.page_main_search_btn);
        this.m_main_searchBtn.setOnClickListener(this.m_main_btnsClickHandler);
        this.m_main_downloadBtn = (Button) findViewById(R.id.page_main_download_btn);
        this.m_main_downloadBtn.setOnClickListener(this.m_main_btnsClickHandler);
        this.m_main_libBtn = (Button) findViewById(R.id.page_main_library_btn);
        this.m_main_libBtn.setOnClickListener(this.m_main_btnsClickHandler);
        this.m_main_chartBtn = (Button) findViewById(R.id.page_main_chart_btn);
        this.m_main_chartBtn.setOnClickListener(this.m_main_btnsClickHandler);
        this.m_main_playBtn = (Button) findViewById(R.id.page_main_play_btn);
        this.m_main_playBtn.setOnClickListener(this.m_main_btnsClickHandler);
        this.m_main_aboutBtn = (Button) findViewById(R.id.page_main_about_btn);
        this.m_main_aboutBtn.setOnClickListener(this.m_main_btnsClickHandler);
        this.m_main_aboutText = (TextView) findViewById(R.id.page_main_about_text);
        this.m_main_aboutText.setMovementMethod(LinkMovementMethod.getInstance());
        this.m_main_mainLayout = (LinearLayout) findViewById(R.id.page_main_mainLayout);
        this.m_main_aboutLayout = (LinearLayout) findViewById(R.id.page_main_aboutLayout);
        this.m_main_searchLayout = (LinearLayout) findViewById(R.id.page_main_searchLayout);
        this.m_main_downloadLayout = (LinearLayout) findViewById(R.id.page_main_downloadLayout);
        this.m_main_libraryLayout = (LinearLayout) findViewById(R.id.page_main_libraryLayout);
        this.m_main_chartLayout = (LinearLayout) findViewById(R.id.page_main_chartLayout);
        this.m_layoutMap.put(PageState.main, this.m_main_mainLayout);
        this.m_layoutMap.put(PageState.about, this.m_main_aboutLayout);
        this.m_layoutMap.put(PageState.search, this.m_main_searchLayout);
        this.m_layoutMap.put(PageState.download, this.m_main_downloadLayout);
        this.m_layoutMap.put(PageState.libray, this.m_main_libraryLayout);
        this.m_layoutMap.put(PageState.chart, this.m_main_chartLayout);
        this.m_search_searchBtn = (Button) findViewById(R.id.page_search_searchBtn);
        this.m_search_searchBtn.setOnClickListener(new SearchClickHandler());
        this.m_search_searchList = (ListView) findViewById(R.id.page_search_searchList);
        this.m_search_play_waiting = (LinearLayout) findViewById(R.id.page_search_playURLLoadingLayout);
        this.m_search_Prefs = getSharedPreferences(this.logTag, 0);
        String lastSearchKey = this.m_search_Prefs.getString(Config.SEARCH_WORD_KEY, "");
        this.m_search_searchInput = (EditText) findViewById(R.id.page_search_searchInput);
        this.m_search_searchInput.setText(lastSearchKey);
        this.m_search_searchKeyWord = this.m_search_searchInput.getText().toString().trim();
        this.m_search_searchLoadingLayout = (LinearLayout) findViewById(R.id.page_search_searchLoadingLayout);
        this.m_search_handler = new SearchCommonHandler(this);
        this.m_search_songAdapter = new SearchSongAdapter(this);
        this.m_search_searchResult = FileUtil.readObject(this, FileUtil.SEARCH_RESULT_NAME);
        if (this.m_search_searchResult == null) {
            this.m_search_searchResult = new ArrayList<>();
        }
        this.m_search_songAdapter.m_songBeanList = this.m_search_searchResult;
        this.m_search_searchList.setOnCreateContextMenuListener(new SearchContextMenu(this, null));
        this.m_search_searchList.setOnItemClickListener(this);
        this.m_search_searchList.addFooterView(new MoreSongFooterView(this));
        this.m_search_searchList.setAdapter((ListAdapter) this.m_search_songAdapter);
        if (this.m_search_searchResult.size() > 0) {
            searchShowMoreSongBar(true, false);
        }
        this.m_search_commonDialog = new CommonDialog(this);
        this.m_search_commonDialog.m_ok.setOnClickListener(new CommonDialogOKHandler(this));
        this.m_search_commonDialog.m_cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MainActivity.this.m_search_commonDialog.dismiss();
            }
        });
        this.m_download_handler = new DownloadCommonHandler(this);
        this.m_download_emptyText = (TextView) findViewById(R.id.page_download_listempty);
        this.m_download_songList = (ListView) findViewById(R.id.page_download_songlist);
        this.m_download_homeBtn = (ImageView) findViewById(R.id.page_download_home_icon);
        this.m_download_homeBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainActivity.this.updatePageState(PageState.main);
            }
        });
        this.m_libraryRefreshReceiver = new LibraryRefreshReceiver(this, null);
        this.m_library_list = (ListView) findViewById(R.id.page_library_list);
        this.m_library_list.setOnItemClickListener(this);
        this.m_library_libTitle = (TextView) findViewById(R.id.page_library_title_text);
        this.m_library_homeBtn = (ImageView) findViewById(R.id.page_library_home_icon);
        this.m_library_backBtn = (ImageView) findViewById(R.id.page_library_back_icon);
        this.m_library_refreshBtn = (ImageView) findViewById(R.id.page_library_refresh_icon);
        this.m_library_libLoadingLayout = (LinearLayout) findViewById(R.id.page_library_loadingLayout);
        this.m_library_allSongList = new ArrayList<>();
        this.m_library_downloadedList = new ArrayList<>();
        this.m_playservice = ((MusicApplication) getApplication()).getPlayService();
        this.m_library_homeBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainActivity.this.updatePageState(PageState.main);
            }
        });
        this.m_library_backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainActivity.this.m_library_libTitle.setText((int) R.string.main_menu_library);
                MainActivity.this.m_library_libPosition = 0;
                if (MainActivity.this.m_library_libLoadingLayout.getVisibility() == 0) {
                    MainActivity.this.m_library_libLoadingLayout.setVisibility(8);
                }
                MainActivity.this.libraryInitAllView();
                MainActivity.this.libraryUpdateBtnState();
            }
        });
        this.m_library_refreshBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MainActivity.this.m_library_libPosition == 2) {
                    MainActivity.this.queryDownloadFolder();
                } else if (MainActivity.this.m_library_libPosition == 1) {
                    MainActivity.this.libraryRefresh();
                }
            }
        });
        this.m_library_libPosition = 0;
        this.m_library_list.setOnCreateContextMenuListener(new LibraryCreateSongContextMenu());
        this.m_chart_chartList = (ListView) findViewById(R.id.page_chart_list);
        this.m_chart_titleView = (TextView) findViewById(R.id.page_chart_title_text);
        this.m_chart_chartLoading = (LinearLayout) findViewById(R.id.page_chart_chartLoadingLayout);
        this.m_chart_playLoading = (LinearLayout) findViewById(R.id.page_chart_chartPlayURLLoadingLayout);
        this.m_chart_chartPosition = 0;
        this.m_chart_handler = new ChartCommonHandler(this);
        this.m_chart_commonDialog = new CommonDialog(this);
        this.m_chart_commonDialog.m_ok.setOnClickListener(new ChartCommonDialogOKHandler(this));
        this.m_chart_commonDialog.m_cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MainActivity.this.m_chart_commonDialog.dismiss();
            }
        });
        this.m_chart_songAdapter = new ChartSongAdapter(this, null);
        this.m_chart_homeBtn = (ImageView) findViewById(R.id.page_chart_home_icon);
        this.m_chart_backBtn = (ImageView) findViewById(R.id.page_chart_back_icon);
        this.m_chart_refreshBtn = (ImageView) findViewById(R.id.page_chart_refresh_icon);
        this.m_chart_homeBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainActivity.this.updatePageState(PageState.main);
            }
        });
        this.m_chart_backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MainActivity.this.m_chart_chartPosition != 0) {
                    MainActivity.this.chartInitAllView();
                    if (MainActivity.this.m_chart_chartLoading.getVisibility() == 0) {
                        MainActivity.this.m_chart_chartLoading.setVisibility(8);
                    }
                    MainActivity.this.m_chart_chartPosition = 0;
                    MainActivity.this.chartUpdateBtnState();
                }
            }
        });
        this.m_chart_refreshBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainActivity.this.refreshChart();
            }
        });
        this.m_chart_typeTitles = getResources().getStringArray(R.array.chartTypes);
        this.m_chart_chartList.setOnCreateContextMenuListener(new ChartContextMenu());
        this.m_chart_chartList.setOnItemClickListener(this);
        Intent intent = getIntent();
        if (!(intent == null || intent.getExtras() == null)) {
            Log.i(this.logTag, intent.getExtras().toString());
            if (((PageState) intent.getExtras().get("pageState")) == PageState.download) {
                initDownloadPage();
            }
        }
        this.m_adArea = (LinearLayout) findViewById(R.id.ad_area_main);
        AdWhirlManager.setConfigExpireTimeout(300000);
        AdWhirlTargeting.setTestMode(false);
        AdWhirlLayout adWhirlLayout = new AdWhirlLayout(this, "0005fc1dfea141da88814828ccc82530");
        this.m_adArea.addView(adWhirlLayout, -1, -2);
        adWhirlLayout.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void updatePageState(PageState state) {
        if (this.m_layoutMap != null) {
            for (Map.Entry entry : this.m_layoutMap.entrySet()) {
                PageState key = (PageState) entry.getKey();
                LinearLayout val = (LinearLayout) entry.getValue();
                Log.i(this.logTag, "Key = " + key + ", " + val);
                if (key == state) {
                    if (val != null) {
                        val.setVisibility(0);
                    }
                    this.m_pageState = state;
                } else if (val != null) {
                    val.setVisibility(8);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public DownloadService getService() {
        if (this.m_downloadBoundService == null) {
            this.m_downloadBoundService = ((MusicApplication) getApplication()).getDownloadService();
        }
        return this.m_downloadBoundService;
    }

    /* access modifiers changed from: private */
    public PlayService getPlayService() {
        return ((MusicApplication) getApplication()).getPlayService();
    }

    /* access modifiers changed from: private */
    public void chartUpdateBtnState() {
        if (this.m_chart_chartPosition == 0) {
            if (this.m_chart_homeBtn != null) {
                this.m_chart_homeBtn.setVisibility(0);
            }
            if (this.m_chart_backBtn != null) {
                this.m_chart_backBtn.setVisibility(8);
            }
            if (this.m_chart_refreshBtn != null) {
                this.m_chart_refreshBtn.setVisibility(8);
                return;
            }
            return;
        }
        if (this.m_chart_refreshBtn != null) {
            this.m_chart_refreshBtn.setVisibility(0);
        }
        if (this.m_chart_backBtn != null) {
            this.m_chart_backBtn.setVisibility(0);
        }
        if (this.m_chart_homeBtn != null) {
            this.m_chart_homeBtn.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void searchGetMoreSongs() {
        m_search_pageIndex++;
        searchShowMoreSongBar(true, true);
        searchSearch(this.m_search_searchKeyWord);
    }

    private void searchDownloadLyric(SongBean songBean) {
        new Thread(new DownLoadLyricThread()).start();
    }

    private void searchShowMoreSongBar(Boolean flag, Boolean loadingFlag) {
        if (loadingFlag.booleanValue()) {
            this.m_search_moreSongText.setVisibility(8);
            this.m_search_more_loading.setVisibility(0);
        } else {
            this.m_search_moreSongText.setVisibility(0);
            this.m_search_more_loading.setVisibility(8);
        }
        if (flag.booleanValue()) {
            this.m_search_moreSongBar.setVisibility(0);
        } else {
            this.m_search_moreSongBar.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void searchSearch(String key) {
        SearchThread newSearch = new SearchThread();
        newSearch.keyWord = key;
        new Thread(newSearch).start();
    }

    /* access modifiers changed from: private */
    public void chartShowDownSongDialog() {
        this.m_chart_commonDialog.show();
        if (this.m_chart_selectedSong.m_downloadUri == null || this.m_chart_selectedSong.m_downloadUri.equals("")) {
            String s = getResources().getString(R.string.diaglog_fetchSongLinkNote);
            this.m_chart_commonDialog.m_dialog_frame_title.setText((int) R.string.dialog_download_title);
            this.m_chart_commonDialog.m_dialog_waiting_des.setText(s);
            this.m_chart_commonDialog.m_downloadConfirm.setVisibility(8);
            this.m_chart_commonDialog.m_fetchLoadingURLLayout.setVisibility(0);
            this.m_chart_commonDialog.m_ok.setEnabled(false);
            chartFetchDownLoadURI();
            return;
        }
        if (this.m_chart_selectedSong.m_fileName == null || this.m_chart_selectedSong.m_fileName.equals("")) {
            this.m_chart_selectedSong.m_fileName = String.valueOf(this.m_chart_selectedSong.m_songName) + this.m_chart_selectedSong.m_downloadUri.substring(this.m_chart_selectedSong.m_downloadUri.lastIndexOf("."));
        }
        this.m_chart_commonDialog.m_downloadConfirm.setVisibility(0);
        this.m_chart_commonDialog.m_dialog_frame_title.setText((int) R.string.dialog_download_title);
        chartSetDownLoadConfirmString(this.m_chart_selectedSong.m_fileName);
        this.m_chart_commonDialog.m_fetchLoadingURLLayout.setVisibility(8);
        this.m_chart_commonDialog.m_ok.setEnabled(true);
    }

    /* access modifiers changed from: private */
    public void chartSetDownLoadConfirmString(String filename) {
        String s1 = getResources().getString(R.string.diaglog_downloadConfirmtxt);
        String s3 = s1 + " " + filename + " to \"sdcard/music/mp3/\"";
        SpannableString spannablestring = new SpannableString(s3);
        ForegroundColorSpan foregroundcolorspan = new ForegroundColorSpan(-65536);
        Log.i(this.logTag, "Error : " + s1 + ", " + s3 + ", " + filename);
        spannablestring.setSpan(foregroundcolorspan, s1.length(), filename.length() + s1.length() + 1, 33);
        this.m_chart_commonDialog.m_downloadConfirm.setText(spannablestring);
    }

    /* access modifiers changed from: private */
    public void searchSetDownLoadConfirmString(String filename) {
        String s1 = getResources().getString(R.string.diaglog_downloadConfirmtxt);
        SpannableString spannablestring = new SpannableString(s1 + " " + filename + " to \"sdcard/music/mp3/\"");
        spannablestring.setSpan(new ForegroundColorSpan(-65536), s1.length(), filename.length() + s1.length() + 1, 33);
        this.m_search_commonDialog.m_downloadConfirm.setText(spannablestring);
    }

    private void searchFetchDownLoadURI() {
        new Thread(new DownLoadURLThread()).start();
    }

    /* access modifiers changed from: private */
    public void searchShowDownSongDialog() {
        this.m_search_commonDialog.show();
        if (this.m_search_selectedSong.m_downloadUri == null || this.m_search_selectedSong.m_downloadUri.equals("")) {
            this.m_search_commonDialog.m_dialog_waiting_des.setText(getResources().getString(R.string.diaglog_fetchSongLinkNote));
            this.m_search_commonDialog.m_downloadConfirm.setVisibility(8);
            this.m_search_commonDialog.m_fetchLoadingURLLayout.setVisibility(0);
            this.m_search_commonDialog.m_ok.setEnabled(false);
            searchFetchDownLoadURI();
            return;
        }
        this.m_search_commonDialog.m_downloadConfirm.setVisibility(0);
        searchSetDownLoadConfirmString(this.m_search_selectedSong.m_fileName);
        this.m_search_commonDialog.m_fetchLoadingURLLayout.setVisibility(8);
        this.m_search_commonDialog.m_ok.setEnabled(true);
    }

    private void SearchSetDownLoadConfirmString(String filename) {
        String s1 = getResources().getString(R.string.diaglog_downloadConfirmtxt);
        SpannableString spannablestring = new SpannableString(s1 + " " + filename + " to \"sdcard/music/mp3/\"");
        spannablestring.setSpan(new ForegroundColorSpan(-65536), s1.length(), filename.length() + s1.length() + 1, 33);
        this.m_search_commonDialog.m_downloadConfirm.setText(spannablestring);
    }

    private void chartFetchDownLoadURI() {
        new Thread(new ChartDownLoadURLThread()).start();
    }

    public void searchUpdateSongList() {
        if (this.m_search_newSearchResult != null) {
            if (this.m_search_searchResult == null || m_search_pageIndex == 1) {
                this.m_search_searchResult = new ArrayList<>();
            }
            if (this.m_search_songAdapter == null) {
                this.m_search_songAdapter = new SearchSongAdapter(this);
            }
            for (int i = 0; i < this.m_search_newSearchResult.size(); i++) {
                this.m_search_searchResult.add(this.m_search_newSearchResult.get(i));
            }
            this.m_search_songAdapter.m_songBeanList = this.m_search_searchResult;
            this.m_search_songAdapter.notifyDataSetChanged();
            searchShowMoreSongBar(true, false);
        }
    }

    public void onPause() {
        super.onPause();
        FileUtil.writeObject(this, FileUtil.SEARCH_RESULT_NAME, this.m_search_searchResult);
        if (this.m_downloadBoundService != null) {
            this.m_downloadBoundService.unregistListener(this.m_download_Listener);
        }
    }

    public void onResume() {
        super.onResume();
    }

    public boolean onContextItemSelected(MenuItem menuitem) {
        int itemId = menuitem.getItemId();
        if (this.m_pageState != PageState.search) {
            if (this.m_pageState != PageState.libray) {
                if (this.m_pageState == PageState.chart) {
                    switch (itemId) {
                        case 1:
                            chartShowDownSongDialog();
                            break;
                        case 2:
                            if (this.m_chart_selectedSong.m_downloadUri != null && !this.m_chart_selectedSong.m_downloadUri.equals("")) {
                                getPlayService().play(this.m_chart_selectedSong.m_songName, this.m_chart_selectedSong.m_downloadUri);
                                break;
                            } else {
                                if (this.m_chart_playLoading.getVisibility() == 8) {
                                    this.m_chart_playLoading.setVisibility(0);
                                }
                                chartFetchDownLoadURI();
                                break;
                            }
                            break;
                    }
                }
            } else {
                switch (itemId) {
                    case 1:
                        getPlayService().play(this.m_library_selectedFileItem.getAbsolutePath());
                        break;
                    case 2:
                        share(this.m_library_selectedFileItem);
                        break;
                    case 3:
                        rename(this.m_library_selectedFileItem);
                        break;
                    case AdWhirlUtil.NETWORK_TYPE_MEDIALETS:
                        delete(this.m_library_selectedFileItem);
                        break;
                }
            }
        } else {
            if (this.m_search_commonDialog == null) {
                this.m_search_commonDialog = new CommonDialog(this);
            }
            switch (itemId) {
                case 1:
                    searchShowDownSongDialog();
                    break;
                case 2:
                    if (this.m_search_selectedSong.m_songLyricUri != null && !this.m_search_selectedSong.m_songLyricUri.equals("")) {
                        SongBean songbean = this.m_search_selectedSong;
                        String s2 = String.valueOf(this.m_search_selectedSong.m_fileName);
                        Toast.makeText(this, String.format(getString(R.string.download_lyric_toast), s2, Config.DOWNLOAD_PATH), 0).show();
                        searchDownloadLyric(songbean);
                        break;
                    } else {
                        String s22 = String.valueOf(this.m_search_selectedSong.m_fileName);
                        Toast.makeText(this, String.format(getString(R.string.no_lyric_toast), s22), 0).show();
                        break;
                    }
                case 3:
                    if (this.m_search_selectedSong.m_downloadUri != null && !this.m_search_selectedSong.m_downloadUri.equals("")) {
                        getPlayService().play(this.m_search_selectedSong.m_songName, this.m_search_selectedSong.m_downloadUri);
                        break;
                    } else {
                        if (this.m_search_play_waiting.getVisibility() == 8) {
                            this.m_search_play_waiting.setVisibility(0);
                        }
                        searchFetchDownLoadURI();
                        break;
                    }
                    break;
            }
        }
        return super.onContextItemSelected(menuitem);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || this.m_pageState == PageState.main) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.m_pageState == PageState.search) {
            SharedPreferences.Editor ed = this.m_search_Prefs.edit();
            ed.putString(Config.SEARCH_WORD_KEY, this.m_search_searchKeyWord);
            ed.commit();
        }
        if (this.m_pageState == PageState.download && this.m_downloadBoundService != null) {
            this.m_downloadBoundService.unregistListener(this.m_download_Listener);
        }
        if (this.m_pageState == PageState.libray) {
            unregisterReceiver(this.m_libraryRefreshReceiver);
        }
        if (this.m_pageState == PageState.chart) {
            FileUtil.writeObject(this, FileUtil.CHART_WESTEN_TOP, this.m_chart_WestenTopList);
            FileUtil.writeObject(this, FileUtil.CHART_USABILL, this.m_chart_USABillList);
            FileUtil.writeObject(this, FileUtil.CHART_ENUK, this.m_chart_ENUKList);
            FileUtil.writeObject(this, FileUtil.CHART_SONG_TOP, this.m_chart_SongTopList);
        }
        updatePageState(PageState.main);
        return false;
    }

    public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
        if (this.m_pageState == PageState.search) {
            this.m_search_searchList.requestFocusFromTouch();
            if (position >= this.m_search_songAdapter.getCount()) {
                searchGetMoreSongs();
                return;
            }
            this.m_search_selectedSong = (SongBean) this.m_search_songAdapter.getItem(position);
            openContextMenu(v);
        } else if (this.m_pageState == PageState.libray) {
            String[] as = getResources().getStringArray(R.array.categories);
            if (this.m_library_libPosition == 0) {
                switch (position) {
                    case SongBean.STATUS_NULL:
                        this.m_library_libTitle.setText(as[0]);
                        libraryRefresh();
                        this.m_library_libPosition = 1;
                        libraryUpdateBtnState();
                        return;
                    case 1:
                        this.m_library_libTitle.setText(as[1]);
                        queryDownloadFolder();
                        this.m_library_libPosition = 2;
                        libraryUpdateBtnState();
                        return;
                    default:
                        return;
                }
            } else {
                if (this.m_library_libPosition == 2) {
                    this.m_library_selectedFileItem = this.m_library_downloadedList.get(position);
                } else if (this.m_library_libPosition == 1) {
                    this.m_library_selectedFileItem = this.m_library_allSongList.get(position);
                }
                openContextMenu(v);
            }
        } else if (this.m_pageState != PageState.chart) {
        } else {
            if (this.m_chart_chartPosition == 0) {
                switch (position) {
                    case SongBean.STATUS_NULL:
                        showChart(ChartTypes.WestenTop, this.m_chart_typeTitles[0], this.m_chart_WestenTopList);
                        this.m_chart_chartPosition = 1;
                        chartUpdateBtnState();
                        return;
                    case 1:
                        showChart(ChartTypes.USABillBoard, this.m_chart_typeTitles[1], this.m_chart_USABillList);
                        this.m_chart_chartPosition = 2;
                        chartUpdateBtnState();
                        return;
                    case 2:
                        showChart(ChartTypes.ENUK, this.m_chart_typeTitles[2], this.m_chart_ENUKList);
                        this.m_chart_chartPosition = 3;
                        chartUpdateBtnState();
                        return;
                    case 3:
                        showChart(ChartTypes.SongTop, this.m_chart_typeTitles[3], this.m_chart_SongTopList);
                        this.m_chart_chartPosition = 4;
                        chartUpdateBtnState();
                        return;
                    default:
                        return;
                }
            } else {
                this.m_chart_selectedSong = (SongBean) this.m_chart_songAdapter.getItem(position);
                Log.i(this.logTag, "show context menu");
                openContextMenu(v);
                chartUpdateBtnState();
            }
        }
    }

    private void chartSearch(String typeStr, ChartTypes newtop) {
        new Thread(new ChartSearchThread(typeStr, newtop)).start();
    }

    /* access modifiers changed from: private */
    public void refreshChart() {
        if (this.m_chart_chartLoading.getVisibility() == 8) {
        }
        this.m_chart_chartLoading.setVisibility(0);
        if (this.m_chart_chartPosition == 1) {
            chartSearch(Config.WESTERN_TOP, ChartTypes.WestenTop);
        } else if (this.m_chart_chartPosition == 2) {
            chartSearch(Config.USA_BILLBOARD, ChartTypes.USABillBoard);
        } else if (this.m_chart_chartPosition == 3) {
            chartSearch(Config.EN_UK, ChartTypes.ENUK);
        } else if (this.m_chart_chartPosition == 4) {
            chartSearch(Config.SONG_TOP, ChartTypes.SongTop);
        }
        chartUpdateBtnState();
    }

    private void showChart(ChartTypes type, String titleStr, ArrayList<SongBean> adapterList) {
        this.m_chart_titleView.setText(titleStr);
        this.m_chart_songAdapter.m_songBeanList = adapterList;
        this.m_chart_chartList.setAdapter((ListAdapter) this.m_chart_songAdapter);
        if (adapterList.size() <= 0) {
            this.m_chart_chartLoading.setVisibility(0);
        }
        if (type == ChartTypes.SongTop) {
            chartSearch(Config.SONG_TOP, ChartTypes.SongTop);
        } else if (type == ChartTypes.WestenTop) {
            chartSearch(Config.WESTERN_TOP, ChartTypes.WestenTop);
        } else if (type == ChartTypes.USABillBoard) {
            chartSearch(Config.USA_BILLBOARD, ChartTypes.USABillBoard);
        } else if (type == ChartTypes.ENUK) {
            chartSearch(Config.EN_UK, ChartTypes.ENUK);
        }
    }

    /* access modifiers changed from: private */
    public void libraryInitAllView() {
        ArrayList<HashMap<String, String>> arraylist = new ArrayList<>();
        String[] as = getResources().getStringArray(R.array.categories);
        for (String put : as) {
            HashMap<String, String> hashmap = new HashMap<>();
            hashmap.put("lib_item_des", put);
            arraylist.add(hashmap);
        }
        this.m_library_list.setAdapter((ListAdapter) new SimpleAdapter(this, arraylist, R.layout.library_item, new String[]{"lib_item_des"}, new int[]{R.id.lib_item_des}));
        if (this.m_library_list.getVisibility() == 8) {
            this.m_library_list.setVisibility(0);
        }
        this.m_library_libPosition = 0;
        libraryUpdateBtnState();
    }

    /* access modifiers changed from: private */
    public void libraryUpdateBtnState() {
        if (this.m_library_libPosition == 0) {
            this.m_library_homeBtn.setVisibility(0);
            this.m_library_backBtn.setVisibility(8);
            this.m_library_refreshBtn.setVisibility(8);
            return;
        }
        this.m_library_homeBtn.setVisibility(8);
        this.m_library_backBtn.setVisibility(0);
        this.m_library_refreshBtn.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void libraryQueryAllSong() {
        if (this.m_playservice == null) {
            this.m_playservice = getPlayService();
        }
        this.m_library_allSongList = this.m_playservice.getAllSongFile();
        DownLoadedSongAdapter downloadSongAdapter = new DownLoadedSongAdapter(this);
        downloadSongAdapter.m_songFileList = this.m_library_allSongList;
        this.m_library_list.setAdapter((ListAdapter) downloadSongAdapter);
    }

    /* access modifiers changed from: private */
    public void libraryRefresh() {
        if (this.m_library_libLoadingLayout.getVisibility() == 8) {
            this.m_library_libLoadingLayout.setVisibility(0);
        }
        this.m_library_list.setVisibility(8);
        sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse("file://" + Environment.getExternalStorageDirectory().getAbsolutePath())));
    }

    private void rename(File file) {
        EditText input = new EditText(this);
        input.setText(file.getName());
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.rename_file_title).setView(input);
        builder.setPositiveButton((int) R.string.ok_text, new LibraryRenameOKHandler(file, input));
        builder.setNegativeButton((int) R.string.cancel_text, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                dialoginterface.dismiss();
            }
        });
        builder.create().show();
    }

    private void delete(final File file) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.delete_file_title).setMessage((int) R.string.delete_file_confirm);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                if (FileUtil.deleteFile(file)) {
                    Toast.makeText(MainActivity.this, String.format(MainActivity.this.getString(R.string.delete_file_toast), file.getName()), 0).show();
                    if (MainActivity.this.m_library_libPosition == 1) {
                        MainActivity.this.libraryRefresh();
                    }
                } else {
                    Toast.makeText(MainActivity.this, String.format(MainActivity.this.getString(R.string.delete_file_fail_toast), file.getName()), 0).show();
                }
                if (MainActivity.this.m_library_libPosition == 2) {
                    MainActivity.this.queryDownloadFolder();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                dialoginterface.dismiss();
            }
        });
        builder.create().show();
    }

    private void share(File file) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("audio/*");
        intent.putExtra("android.intent.extra.SUBJECT", "share");
        intent.putExtra("android.intent.extra.STREAM", Uri.fromFile(file));
        intent.addFlags(268435456);
        startActivity(Intent.createChooser(intent, "share music with"));
    }

    /* access modifiers changed from: private */
    public void registSDReceiver() {
        IntentFilter intentfilter = new IntentFilter("android.intent.action.MEDIA_SCANNER_STARTED");
        intentfilter.addAction("android.intent.action.MEDIA_SCANNER_FINISHED");
        intentfilter.addAction("android.intent.action.MEDIA_SCANNER_STARTED");
        intentfilter.addAction("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        intentfilter.addDataScheme("file");
        registerReceiver(this.m_libraryRefreshReceiver, intentfilter);
    }

    /* access modifiers changed from: private */
    public void queryDownloadFolder() {
        this.m_library_downloadedList = FileUtil.getFileListInDownloadPath();
        DownLoadedSongAdapter downloadSongAdapter = new DownLoadedSongAdapter(this);
        downloadSongAdapter.m_songFileList = this.m_library_downloadedList;
        this.m_library_list.setAdapter((ListAdapter) downloadSongAdapter);
    }

    /* access modifiers changed from: private */
    public void chartInitAllView() {
        ArrayList<HashMap<String, String>> arraylist = new ArrayList<>();
        String[] as = getResources().getStringArray(R.array.chartTypes);
        for (String put : as) {
            HashMap<String, String> hashmap = new HashMap<>();
            hashmap.put("lib_item_des", put);
            arraylist.add(hashmap);
        }
        this.m_chart_chartList.setAdapter((ListAdapter) new SimpleAdapter(this, arraylist, R.layout.library_item, new String[]{"lib_item_des"}, new int[]{R.id.lib_item_des}));
        this.m_chart_titleView.setText((int) R.string.main_menu_chart);
        this.m_chart_chartPosition = 0;
        this.m_chart_WestenTopList = FileUtil.readObject(this, FileUtil.CHART_WESTEN_TOP);
        if (this.m_chart_WestenTopList == null) {
            this.m_chart_WestenTopList = new ArrayList<>();
        }
        this.m_chart_USABillList = FileUtil.readObject(this, FileUtil.CHART_USABILL);
        if (this.m_chart_USABillList == null) {
            this.m_chart_USABillList = new ArrayList<>();
        }
        this.m_chart_ENUKList = FileUtil.readObject(this, FileUtil.CHART_ENUK);
        if (this.m_chart_ENUKList == null) {
            this.m_chart_ENUKList = new ArrayList<>();
        }
        this.m_chart_SongTopList = FileUtil.readObject(this, FileUtil.CHART_SONG_TOP);
        if (this.m_chart_SongTopList == null) {
            this.m_chart_SongTopList = new ArrayList<>();
        }
        chartUpdateBtnState();
    }

    /* access modifiers changed from: private */
    public void initDownloadPage() {
        if (this.m_downloadBoundService != null) {
            this.m_downloadBoundService.registListener(this.m_download_Listener);
        }
        updatePageState(PageState.download);
        if (this.m_downloadBoundService == null) {
            this.m_downloadBoundService = getService();
        }
        if (this.m_downloadBoundService != null) {
            this.m_download_downBeanList = this.m_downloadBoundService.getDownloadingList().values();
        }
        if (this.m_download_downBeanList == null) {
            this.m_download_downBeanList = new ArrayList();
        }
        this.m_download_adapter = new DownloadingAdapter(this, this.m_download_downBeanList);
        this.m_download_songList.setAdapter((ListAdapter) this.m_download_adapter);
        Log.i(this.logTag, "Downloading Size : " + this.m_download_downBeanList.size());
        if (this.m_download_downBeanList.size() <= 0) {
            this.m_download_emptyText.setVisibility(0);
            this.m_download_songList.setVisibility(8);
            return;
        }
        this.m_download_emptyText.setVisibility(8);
        this.m_download_songList.setVisibility(0);
    }

    private class MainPageBtnsClickHandler implements View.OnClickListener {
        MainPageBtnsClickHandler() {
        }

        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.page_main_search_btn:
                    MainActivity.this.updatePageState(PageState.search);
                    return;
                case R.id.page_main_chart_btn:
                    MainActivity.this.updatePageState(PageState.chart);
                    MainActivity.this.chartInitAllView();
                    return;
                case R.id.page_main_library_btn:
                    MainActivity.this.updatePageState(PageState.libray);
                    MainActivity.this.registSDReceiver();
                    MainActivity.this.libraryInitAllView();
                    return;
                case R.id.page_main_download_btn:
                    MainActivity.this.initDownloadPage();
                    return;
                case R.id.page_main_play_btn:
                    MainActivity.this.startActivity(new Intent(MainActivity.this, PlayerActivity.class));
                    return;
                case R.id.page_main_about_btn:
                    MainActivity.this.updatePageState(PageState.about);
                    return;
                default:
                    return;
            }
        }
    }

    public final class SearchSongViewHolder {
        public View downBtn;
        public TextView song_album;
        public TextView song_author;
        public TextView song_name;
        public TextView song_size;

        public SearchSongViewHolder() {
        }
    }

    public class SearchSongAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        public ArrayList<SongBean> m_songBeanList;

        public SearchSongAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
            this.m_songBeanList = MainActivity.this.m_search_searchResult;
        }

        public int getCount() {
            return this.m_songBeanList.size();
        }

        public Object getItem(int position) {
            return this.m_songBeanList.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            SearchSongViewHolder holder;
            if (convertView == null) {
                holder = new SearchSongViewHolder();
                convertView = this.mInflater.inflate((int) R.layout.songlist, (ViewGroup) null);
                holder.song_name = (TextView) convertView.findViewById(R.id.item_songName);
                holder.song_author = (TextView) convertView.findViewById(R.id.item_songAuthor);
                holder.song_album = (TextView) convertView.findViewById(R.id.item_album);
                holder.song_size = (TextView) convertView.findViewById(R.id.item_songSize);
                holder.downBtn = convertView.findViewById(R.id.item_downloadIcon);
                convertView.setTag(holder);
            } else {
                holder = (SearchSongViewHolder) convertView.getTag();
            }
            holder.song_name.setText(((SongBean) MainActivity.this.m_search_searchResult.get(position)).m_songName);
            holder.song_author.setText(((SongBean) MainActivity.this.m_search_searchResult.get(position)).m_singer);
            holder.song_album.setText(((SongBean) MainActivity.this.m_search_searchResult.get(position)).m_album);
            holder.song_size.setText(((SongBean) MainActivity.this.m_search_searchResult.get(position)).m_size);
            holder.downBtn.setOnClickListener(new DownSongIconHandlerHandler(position));
            return convertView;
        }
    }

    private class DownSongIconHandlerHandler implements View.OnClickListener {
        private int position;

        public void onClick(View v) {
            MainActivity.this.m_search_selectedSong = (SongBean) MainActivity.this.m_search_songAdapter.getItem(this.position);
            MainActivity.this.searchShowDownSongDialog();
        }

        DownSongIconHandlerHandler(int pos) {
            this.position = pos;
        }
    }

    private class SearchContextMenu implements View.OnCreateContextMenuListener {
        private SearchContextMenu() {
        }

        /* synthetic */ SearchContextMenu(MainActivity mainActivity, SearchContextMenu searchContextMenu) {
            this();
        }

        public void onCreateContextMenu(ContextMenu contextmenu, View view, ContextMenu.ContextMenuInfo contextmenuinfo) {
            int i = ((AdapterView.AdapterContextMenuInfo) contextmenuinfo).position;
            if (i >= MainActivity.this.m_search_songAdapter.getCount()) {
                MainActivity.this.searchGetMoreSongs();
                return;
            }
            MainActivity.this.m_search_selectedSong = (SongBean) MainActivity.this.m_search_songAdapter.getItem(i);
            contextmenu.clear();
            contextmenu.clearHeader();
            contextmenu.setHeaderIcon((int) R.drawable.context_menu_icon);
            contextmenu.setHeaderTitle((int) R.string.song_menu_title);
            contextmenu.add(0, 1, 0, (int) R.string.search_menu_song);
            if (MainActivity.this.m_search_selectedSong.m_songLyricUri == null || MainActivity.this.m_search_selectedSong.m_songLyricUri.equals("")) {
                contextmenu.add(0, 2, 0, (int) R.string.search_menu_no_lyric).setEnabled(false);
            } else {
                contextmenu.add(0, 2, 0, (int) R.string.search_menu_lyric).setEnabled(true);
            }
            contextmenu.add(0, 3, 0, (int) R.string.menu_play);
        }
    }

    private class SearchCommonHandler extends Handler {
        public void handleMessage(Message msg) {
            Log.i(MainActivity.this.logTag, "Message returned " + msg.what);
            MainActivity.this.m_search_searchLoadingLayout.setVisibility(8);
            switch (msg.what) {
                case 1:
                    MainActivity.this.searchUpdateSongList();
                    return;
                case 2:
                    MainActivity.this.searchUpdateSongList();
                    return;
                case 3:
                    MainActivity.this.m_search_searchResult = new ArrayList();
                    return;
                case AdWhirlUtil.NETWORK_TYPE_MEDIALETS:
                case AdWhirlUtil.NETWORK_TYPE_MILLENNIAL:
                case AdWhirlUtil.NETWORK_TYPE_GREYSTRIP:
                case AdWhirlUtil.NETWORK_TYPE_QUATTRO:
                default:
                    return;
                case AdWhirlUtil.NETWORK_TYPE_LIVERAIL:
                    if (MainActivity.this.m_search_play_waiting.getVisibility() == 0) {
                        MainActivity.this.m_search_play_waiting.setVisibility(8);
                        MainActivity.this.getPlayService().play(MainActivity.this.m_search_selectedSong.m_songName, MainActivity.this.m_search_selectedSong.m_downloadUri);
                    }
                    if (MainActivity.this.m_search_commonDialog.isShowing() && MainActivity.this.m_search_selectedSong.m_downloadUri != null) {
                        MainActivity.this.m_search_commonDialog.m_downloadConfirm.setVisibility(0);
                        MainActivity.this.searchSetDownLoadConfirmString(MainActivity.this.m_search_selectedSong.m_fileName);
                        MainActivity.this.m_search_commonDialog.m_fetchLoadingURLLayout.setVisibility(8);
                        MainActivity.this.m_search_commonDialog.m_ok.setEnabled(true);
                        return;
                    }
                    return;
                case AdWhirlUtil.NETWORK_TYPE_CUSTOM:
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle((int) R.string.error_text).setMessage(String.format(MainActivity.this.getString(R.string.error_desc), msg.obj.toString()));
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialoginterface, int i) {
                            dialoginterface.dismiss();
                        }
                    });
                    return;
            }
        }

        SearchCommonHandler(Context context) {
        }
    }

    private class MoreSongFooterView extends LinearLayout {
        private Context context;
        private TextView textView;

        public MoreSongFooterView(Context context2) {
            super(context2);
            initialize(context2);
        }

        public MoreSongFooterView(Context context2, AttributeSet attrs) {
            super(context2, attrs);
            initialize(context2);
        }

        private void initialize(Context context2) {
            this.context = context2;
            View view = LayoutInflater.from(this.context).inflate((int) R.layout.search_footer, (ViewGroup) null);
            this.textView = (TextView) view.findViewById(R.id.moreSongText);
            this.textView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    MainActivity.this.searchGetMoreSongs();
                }
            });
            MainActivity.this.m_search_moreSongBar = (RelativeLayout) view.findViewById(R.id.moreSongBar);
            MainActivity.this.m_search_moreSongText = (TextView) view.findViewById(R.id.moreSongText);
            MainActivity.this.m_search_more_loading = (LinearLayout) view.findViewById(R.id.moreLoadingLayout);
            addView(view);
        }
    }

    private class ChartCommonDialogOKHandler implements View.OnClickListener {
        MainActivity parentActivity;

        public void onClick(View view) {
            MainActivity.this.m_chart_commonDialog.dismiss();
            if (Environment.getExternalStorageState().equals("mounted")) {
                Toast.makeText(MainActivity.this.getApplicationContext(), String.valueOf(MainActivity.this.m_chart_selectedSong.m_songName) + " will be downloaded", 0).show();
                this.parentActivity.m_downloadBoundService = MainActivity.this.getService();
                MainActivity.this.m_chart_selectedSong.m_fileName = String.valueOf(MainActivity.this.m_chart_selectedSong.m_songName) + MainActivity.this.m_chart_selectedSong.m_downloadUri.substring(MainActivity.this.m_chart_selectedSong.m_downloadUri.lastIndexOf("."));
                if (this.parentActivity.m_downloadBoundService != null) {
                    this.parentActivity.m_downloadBoundService.downloadMusic(String.valueOf(System.currentTimeMillis()), MainActivity.this.m_chart_selectedSong);
                    return;
                }
                return;
            }
            Toast.makeText(MainActivity.this.getApplicationContext(), (int) R.string.download_noSD_toast, 0).show();
        }

        ChartCommonDialogOKHandler(MainActivity activity) {
            this.parentActivity = activity;
        }
    }

    private class CommonDialogOKHandler implements View.OnClickListener {
        MainActivity parentActivity;

        public void onClick(View view) {
            MainActivity.this.m_search_commonDialog.dismiss();
            if (Environment.getExternalStorageState().equals("mounted")) {
                Context context = MainActivity.this.getApplicationContext();
                String s = String.valueOf(MainActivity.this.m_search_selectedSong.m_fileName);
                Toast.makeText(context, String.format(MainActivity.this.getString(R.string.download_song_toast), s), 0).show();
                this.parentActivity.m_downloadBoundService = MainActivity.this.getService();
                Log.i(MainActivity.this.logTag, "Service = " + this.parentActivity.m_downloadBoundService);
                if (this.parentActivity.m_downloadBoundService != null) {
                    this.parentActivity.m_downloadBoundService.downloadMusic(String.valueOf(System.currentTimeMillis()), MainActivity.this.m_search_selectedSong);
                    return;
                }
                return;
            }
            Toast.makeText(MainActivity.this.getApplicationContext(), MainActivity.this.getString(R.string.download_noSD_toast), 0).show();
        }

        CommonDialogOKHandler(MainActivity mainActivity) {
            this.parentActivity = mainActivity;
        }
    }

    private class SearchClickHandler implements View.OnClickListener {
        SearchClickHandler() {
        }

        public void onClick(View v) {
            MainActivity.m_search_pageIndex = 1;
            String inputKey = MainActivity.this.m_search_searchInput.getText().toString();
            MainActivity.this.findViewById(R.id.page_search_listarea).setVisibility(0);
            MainActivity.this.m_search_searchLoadingLayout.setVisibility(0);
            MainActivity.this.m_search_searchKeyWord = inputKey.trim();
            MainActivity.this.searchSearch(inputKey.trim());
            View view = MainActivity.this.getCurrentFocus();
            InputMethodManager imm = (InputMethodManager) MainActivity.this.getSystemService("input_method");
            if (imm != null && view != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    private class DownLoadLyricThread implements Runnable {
        private final SongBean songBean;

        public void run() {
            JSONObject jsonobject;
            HttpGet httpget = new HttpGet(this.songBean.m_songLyricUri);
            DefaultHttpClient defaulthttpclient = new DefaultHttpClient();
            httpget.setHeader("User-Agent", Config.USERAGENT);
            httpget.addHeader("Referer", "http://player.mbox.sogou.com/player");
            try {
                HttpResponse httpresponse = defaulthttpclient.execute(httpget);
                if (httpresponse.getStatusLine().getStatusCode() == 200 && (jsonobject = new JSONObject(EntityUtils.toString(httpresponse.getEntity(), "GBK")).getJSONObject("list")) != null) {
                    String s2 = jsonobject.getString("lrc");
                    File file = new File(String.valueOf(Config.DOWNLOAD_PATH) + this.songBean.m_songName + Config.LYRIC_SUFFIX);
                    if (file.exists()) {
                        file.delete();
                    }
                    file.createNewFile();
                    FileOutputStream fileoutputstream = new FileOutputStream(file);
                    PrintWriter printwriter = new PrintWriter(fileoutputstream);
                    printwriter.write(s2);
                    printwriter.flush();
                    fileoutputstream.close();
                    printwriter.close();
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            } catch (JSONException e3) {
                e3.printStackTrace();
            }
        }

        DownLoadLyricThread() {
            this.songBean = MainActivity.this.m_search_selectedSong;
        }
    }

    private class DownLoadURLThread implements Runnable {
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x00a3, code lost:
            r10 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x00a4, code lost:
            r10.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x00cd, code lost:
            r10 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x00ce, code lost:
            r10.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x00a3 A[ExcHandler: ClientProtocolException (r10v4 'e' org.apache.http.client.ClientProtocolException A[CUSTOM_DECLARE]), Splitter:B:1:0x0019] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r12 = this;
                com.flypaul.music.MainActivity r10 = com.flypaul.music.MainActivity.this
                com.flypaul.music.SongBean r10 = r10.m_search_selectedSong
                java.lang.String r9 = r10.m_downloadLink
                org.apache.http.client.methods.HttpGet r5 = new org.apache.http.client.methods.HttpGet
                r5.<init>(r9)
                org.apache.http.impl.client.DefaultHttpClient r1 = new org.apache.http.impl.client.DefaultHttpClient
                r1.<init>()
                java.lang.String r10 = "User-Agent"
                java.lang.String r11 = "Mozilla/5.0 (Windows NT 6.1; rv:2.0) Gecko/20100101 Firefox/4.0"
                r5.setHeader(r10, r11)
                org.apache.http.HttpResponse r6 = r1.execute(r5)     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                org.apache.http.StatusLine r10 = r6.getStatusLine()     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                int r0 = r10.getStatusCode()     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                r10 = 200(0xc8, float:2.8E-43)
                if (r0 == r10) goto L_0x002a
            L_0x0029:
                return
            L_0x002a:
                org.apache.http.HttpEntity r10 = r6.getEntity()     // Catch:{ ParseException -> 0x0080, IOException -> 0x00a9, ClientProtocolException -> 0x00a3 }
                java.lang.String r11 = "GBK"
                java.lang.String r10 = org.apache.http.util.EntityUtils.toString(r10, r11)     // Catch:{ ParseException -> 0x0080, IOException -> 0x00a9, ClientProtocolException -> 0x00a3 }
                org.jsoup.nodes.Document r10 = org.jsoup.Jsoup.parseBodyFragment(r10)     // Catch:{ ParseException -> 0x0080, IOException -> 0x00a9, ClientProtocolException -> 0x00a3 }
                java.lang.String r11 = "a"
                org.jsoup.select.Elements r10 = r10.getElementsByTag(r11)     // Catch:{ ParseException -> 0x0080, IOException -> 0x00a9, ClientProtocolException -> 0x00a3 }
                java.util.Iterator r7 = r10.iterator()     // Catch:{ ParseException -> 0x0080, IOException -> 0x00a9, ClientProtocolException -> 0x00a3 }
            L_0x0042:
                boolean r10 = r7.hasNext()     // Catch:{ ParseException -> 0x0080, IOException -> 0x00a9, ClientProtocolException -> 0x00a3 }
                if (r10 == 0) goto L_0x0029
                java.lang.Object r4 = r7.next()     // Catch:{ ParseException -> 0x0080, IOException -> 0x00a9, ClientProtocolException -> 0x00a3 }
                org.jsoup.nodes.Element r4 = (org.jsoup.nodes.Element) r4     // Catch:{ ParseException -> 0x0080, IOException -> 0x00a9, ClientProtocolException -> 0x00a3 }
                java.lang.String r10 = r4.text()     // Catch:{ ParseException -> 0x0080, IOException -> 0x00a9, ClientProtocolException -> 0x00a3 }
                java.lang.String r11 = "下"
                int r10 = r10.indexOf(r11)     // Catch:{ ParseException -> 0x0080, IOException -> 0x00a9, ClientProtocolException -> 0x00a3 }
                r11 = -1
                if (r10 == r11) goto L_0x0042
                com.flypaul.music.MainActivity r10 = com.flypaul.music.MainActivity.this     // Catch:{ ParseException -> 0x0080, IOException -> 0x00a9, ClientProtocolException -> 0x00a3 }
                com.flypaul.music.SongBean r10 = r10.m_search_selectedSong     // Catch:{ ParseException -> 0x0080, IOException -> 0x00a9, ClientProtocolException -> 0x00a3 }
                java.lang.String r11 = "href"
                java.lang.String r11 = r4.attr(r11)     // Catch:{ ParseException -> 0x0080, IOException -> 0x00a9, ClientProtocolException -> 0x00a3 }
                r10.m_downloadUri = r11     // Catch:{ ParseException -> 0x0080, IOException -> 0x00a9, ClientProtocolException -> 0x00a3 }
                com.flypaul.music.MainActivity r10 = com.flypaul.music.MainActivity.this     // Catch:{ ParseException -> 0x0080, IOException -> 0x00a9, ClientProtocolException -> 0x00a3 }
                android.os.Handler r10 = r10.m_search_handler     // Catch:{ ParseException -> 0x0080, IOException -> 0x00a9, ClientProtocolException -> 0x00a3 }
                android.os.Message r8 = r10.obtainMessage()     // Catch:{ ParseException -> 0x0080, IOException -> 0x00a9, ClientProtocolException -> 0x00a3 }
                r10 = 5
                r8.what = r10     // Catch:{ ParseException -> 0x0080, IOException -> 0x00a9, ClientProtocolException -> 0x00a3 }
                com.flypaul.music.MainActivity r10 = com.flypaul.music.MainActivity.this     // Catch:{ ParseException -> 0x0080, IOException -> 0x00a9, ClientProtocolException -> 0x00a3 }
                android.os.Handler r10 = r10.m_search_handler     // Catch:{ ParseException -> 0x0080, IOException -> 0x00a9, ClientProtocolException -> 0x00a3 }
                r10.sendMessage(r8)     // Catch:{ ParseException -> 0x0080, IOException -> 0x00a9, ClientProtocolException -> 0x00a3 }
                goto L_0x0042
            L_0x0080:
                r10 = move-exception
                r2 = r10
                com.flypaul.music.MainActivity r10 = com.flypaul.music.MainActivity.this     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                android.os.Handler r10 = r10.m_search_handler     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                android.os.Message r8 = r10.obtainMessage()     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                r10 = 9
                r8.what = r10     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                java.lang.String r10 = r2.getLocalizedMessage()     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                r8.obj = r10     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                com.flypaul.music.MainActivity r10 = com.flypaul.music.MainActivity.this     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                android.os.Handler r10 = r10.m_search_handler     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                r10.sendMessage(r8)     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                r2.printStackTrace()     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                goto L_0x0029
            L_0x00a3:
                r10 = move-exception
                r3 = r10
                r3.printStackTrace()
                goto L_0x0029
            L_0x00a9:
                r10 = move-exception
                r2 = r10
                com.flypaul.music.MainActivity r10 = com.flypaul.music.MainActivity.this     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                android.os.Handler r10 = r10.m_search_handler     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                android.os.Message r8 = r10.obtainMessage()     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                r10 = 9
                r8.what = r10     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                java.lang.String r10 = r2.getLocalizedMessage()     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                r8.obj = r10     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                com.flypaul.music.MainActivity r10 = com.flypaul.music.MainActivity.this     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                android.os.Handler r10 = r10.m_search_handler     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                r10.sendMessage(r8)     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                r2.printStackTrace()     // Catch:{ ClientProtocolException -> 0x00a3, IOException -> 0x00cd }
                goto L_0x0029
            L_0x00cd:
                r10 = move-exception
                r3 = r10
                r3.printStackTrace()
                goto L_0x0029
            */
            throw new UnsupportedOperationException("Method not decompiled: com.flypaul.music.MainActivity.DownLoadURLThread.run():void");
        }

        DownLoadURLThread() {
        }
    }

    private class ChartDownLoadURLThread implements Runnable {
        public void run() {
            String s = MainActivity.this.m_chart_selectedSong.m_downloadLink;
            Log.i(MainActivity.this.logTag, "Download link: " + s);
            try {
                String result = s.replaceAll("\\\\", "");
                Log.i(MainActivity.this.logTag, "Replaced after : " + result);
                HttpGet httpget = new HttpGet(result);
                DefaultHttpClient defaulthttpclient = new DefaultHttpClient();
                httpget.setHeader("User-Agent", Config.USERAGENT);
                HttpResponse httpresponse = defaulthttpclient.execute(httpget);
                if (httpresponse.getStatusLine().getStatusCode() != 200) {
                    Message message = MainActivity.this.m_chart_handler.obtainMessage();
                    message.what = 9;
                    MainActivity.this.m_chart_handler.sendMessage(message);
                    return;
                }
                try {
                    Iterator<Element> iterator = Jsoup.parseBodyFragment(EntityUtils.toString(httpresponse.getEntity(), "GBK")).getElementsByTag("a").iterator();
                    while (iterator.hasNext()) {
                        Element element = iterator.next();
                        if (element.text().indexOf("下") != -1) {
                            MainActivity.this.m_chart_selectedSong.m_downloadUri = element.attr("href");
                            Message message2 = MainActivity.this.m_chart_handler.obtainMessage();
                            message2.what = 5;
                            MainActivity.this.m_chart_handler.sendMessage(message2);
                            return;
                        }
                    }
                    if (0 == 0) {
                        Message message3 = MainActivity.this.m_chart_handler.obtainMessage();
                        message3.what = 9;
                        MainActivity.this.m_chart_handler.sendMessage(message3);
                    }
                } catch (Exception e) {
                    Exception e2 = e;
                    Message message4 = MainActivity.this.m_chart_handler.obtainMessage();
                    message4.what = 9;
                    message4.obj = e2.getLocalizedMessage();
                    MainActivity.this.m_chart_handler.sendMessage(message4);
                    e2.printStackTrace();
                }
            } catch (Exception e3) {
                Exception e1 = e3;
                Message message5 = MainActivity.this.m_chart_handler.obtainMessage();
                message5.what = 9;
                message5.obj = e1.getLocalizedMessage();
                MainActivity.this.m_chart_handler.sendMessage(message5);
                e1.printStackTrace();
            }
        }

        ChartDownLoadURLThread() {
        }
    }

    private class SearchThread implements Runnable {
        /* access modifiers changed from: private */
        public String keyWord = "";
        private int page = MainActivity.m_search_pageIndex;

        public void run() {
            sendSearchRequest();
        }

        public SearchThread() {
        }

        public void sendSearchRequest() {
            String songAlbum;
            ArrayList<SongBean> result = new ArrayList<>();
            StringBuilder sb = new StringBuilder("http://mp3.sogou.com/music.so?pf=mp3&query=");
            String key = this.keyWord;
            try {
                key = URLEncoder.encode(key, "GBK");
            } catch (UnsupportedEncodingException e) {
                UnsupportedEncodingException e2 = e;
                Message message = new Message();
                message.what = 9;
                MainActivity.this.m_search_handler.sendMessage(message);
                message.obj = e2.getLocalizedMessage();
                e2.printStackTrace();
            }
            String queryStr = sb.append(key).toString();
            if (this.page > 1) {
                queryStr = queryStr + ("&page=" + this.page);
            }
            HttpGet httpGet = new HttpGet(queryStr);
            httpGet.setHeader("User-Agent", Config.USERAGENT);
            try {
                HttpResponse httpresponse = new DefaultHttpClient().execute(httpGet);
                if (httpresponse.getStatusLine().getStatusCode() == 200) {
                    Matcher matcher = Pattern.compile("<table id=\"songlist\"(.+?)>(.+?)</table>", 34).matcher(EntityUtils.toString(httpresponse.getEntity(), "GBK"));
                    if (!matcher.find()) {
                        Message message2 = new Message();
                        message2.what = 2;
                        MainActivity.this.m_search_handler.sendMessage(message2);
                        return;
                    }
                    Document document = Jsoup.parseBodyFragment(matcher.group(0));
                    new StringBuffer().append("");
                    Elements elements = document.getElementsByTag("tr");
                    int listSize = elements.size();
                    if (listSize <= 1) {
                        Message message3 = new Message();
                        message3.what = 2;
                        MainActivity.this.m_search_handler.sendMessage(message3);
                    }
                    for (int i = 1; i < listSize; i++) {
                        Elements songAttr = elements.get(i).children();
                        if (songAttr.size() == 11) {
                            SongBean song = new SongBean();
                            String songName = songAttr.get(1).getElementsByTag("a").text();
                            song.m_songName = songName;
                            song.m_singer = songAttr.get(2).getElementsByTag("a").text();
                            song.m_size = songAttr.get(8).text();
                            String songAlbum2 = songAttr.get(3).getElementsByTag("a").text();
                            if (songAlbum2 == null || songAlbum2.equals("")) {
                                songAlbum = "Unknown";
                            } else {
                                songAlbum = "<<" + songAlbum2 + ">>";
                            }
                            song.m_album = songAlbum;
                            String song_link = songAttr.get(7).getElementsByTag("a").attr("href");
                            int j2 = song_link.indexOf("lyricId");
                            String lyricId = "";
                            if (j2 >= 0) {
                                lyricId = song_link.substring(j2);
                            }
                            if (lyricId != null && !lyricId.equals("")) {
                                song.m_songLyricUri = "http://mp3.sogou.com/api/lrc2?" + lyricId;
                            }
                            song.m_fileName = songName + "." + songAttr.get(9).text();
                            Matcher matcher_link = Pattern.compile("window.open\\('(.+?)'").matcher(songAttr.get(6).getElementsByTag("a").attr("onclick"));
                            if (matcher_link.find()) {
                                song.m_downloadLink = "http://mp3.sogou.com" + matcher_link.group(1);
                                Log.i(MainActivity.this.logTag, song.m_downloadLink);
                                result.add(song);
                            }
                        }
                    }
                    MainActivity.this.m_search_newSearchResult = result;
                    Message message4 = new Message();
                    message4.what = 1;
                    MainActivity.this.m_search_handler.sendMessage(message4);
                }
            } catch (ClientProtocolException e3) {
                ClientProtocolException e4 = e3;
                Message message5 = new Message();
                message5.what = 9;
                message5.obj = e4.getLocalizedMessage();
                MainActivity.this.m_search_handler.sendMessage(message5);
                e4.printStackTrace();
            } catch (IOException e5) {
                IOException e6 = e5;
                Message message6 = new Message();
                message6.what = 9;
                message6.obj = e6.getLocalizedMessage();
                MainActivity.this.m_search_handler.sendMessage(message6);
                e6.printStackTrace();
            } catch (Exception e7) {
                Exception e8 = e7;
                Message message7 = new Message();
                message7.what = 9;
                message7.obj = e8.getLocalizedMessage();
                MainActivity.this.m_search_handler.sendMessage(message7);
                e8.printStackTrace();
            }
        }
    }

    private class ChartSearchThread implements Runnable {
        private String chartTypeURL;
        private int page;
        private ChartTypes type;

        public void run() {
            sendSearchRequest();
        }

        public ChartSearchThread(String typeUrl, ChartTypes newtop) {
            this.chartTypeURL = typeUrl;
            this.type = newtop;
        }

        public void sendSearchRequest() {
            ArrayList<SongBean> result = new ArrayList<>();
            HttpGet httpGet = new HttpGet(Config.CHART_BASE_URL + this.chartTypeURL);
            httpGet.setHeader("User-Agent", Config.USERAGENT);
            try {
                HttpResponse httpresponse = new DefaultHttpClient().execute(httpGet);
                if (httpresponse.getStatusLine().getStatusCode() == 200) {
                    Matcher matcher = Pattern.compile("<table(.+?)id=\"songlist\"(.+?)>(.+?)</table>", 34).matcher(EntityUtils.toString(httpresponse.getEntity(), "GBK"));
                    while (matcher.find()) {
                        Document document = Jsoup.parseBodyFragment(matcher.group(0));
                        new StringBuffer().append("");
                        Elements elements = document.getElementsByTag("tr");
                        int listSize = elements.size();
                        if (listSize > 0) {
                            for (int i = 0; i < listSize; i++) {
                                Elements songAttr = elements.get(i).children();
                                if (songAttr.size() == 14) {
                                    SongBean song = new SongBean();
                                    String songName = songAttr.get(1).getElementsByTag("a").text();
                                    song.m_songName = songName;
                                    song.m_singer = songAttr.get(2).getElementsByTag("a").text();
                                    song.m_fileName = String.valueOf(songName) + ".mp3";
                                    String song_file_link = songAttr.get(5).getElementsByTag("a").attr("onclick");
                                    int index1 = song_file_link.indexOf(",");
                                    String subLink1 = "";
                                    if (index1 >= 1) {
                                        subLink1 = song_file_link.substring(13, index1 - 1);
                                    }
                                    Log.i(MainActivity.this.logTag, "Get Song Link for Name" + song.m_songName + " = " + subLink1);
                                    song.m_downloadLink = subLink1;
                                    result.add(song);
                                    SongBean songtwo = new SongBean();
                                    songtwo.m_songName = songAttr.get(8).getElementsByTag("a").text();
                                    songtwo.m_singer = songAttr.get(9).getElementsByTag("a").text();
                                    String song_file_link2 = songAttr.get(12).getElementsByTag("a").attr("onclick");
                                    int index = song_file_link2.indexOf(",");
                                    String subLink = "";
                                    if (index >= 1) {
                                        subLink = song_file_link2.substring(13, index - 1);
                                    }
                                    subLink.replaceAll("\\\\", "");
                                    Log.i(MainActivity.this.logTag, "Get Song Link for Name" + songtwo.m_songName + " = " + subLink);
                                    songtwo.m_downloadLink = subLink;
                                    result.add(songtwo);
                                }
                            }
                        }
                    }
                    if (this.type == ChartTypes.SongTop) {
                        MainActivity.this.m_chart_SongTopList.clear();
                        MainActivity.this.m_chart_SongTopList.addAll(result);
                    } else if (this.type == ChartTypes.WestenTop) {
                        MainActivity.this.m_chart_WestenTopList.clear();
                        MainActivity.this.m_chart_WestenTopList.addAll(result);
                    } else if (this.type == ChartTypes.USABillBoard) {
                        MainActivity.this.m_chart_USABillList.clear();
                        MainActivity.this.m_chart_USABillList.addAll(result);
                    } else if (this.type == ChartTypes.ENUK) {
                        MainActivity.this.m_chart_ENUKList.clear();
                        MainActivity.this.m_chart_ENUKList.addAll(result);
                    }
                    Message message = new Message();
                    message.what = 1;
                    message.obj = this.type;
                    MainActivity.this.m_chart_handler.sendMessage(message);
                }
            } catch (ClientProtocolException e) {
                ClientProtocolException e2 = e;
                Message message2 = new Message();
                message2.what = 9;
                message2.obj = e2.getLocalizedMessage();
                MainActivity.this.m_chart_handler.sendMessage(message2);
                e2.printStackTrace();
            } catch (Exception e3) {
                Exception e4 = e3;
                Message message3 = new Message();
                message3.what = 9;
                MainActivity.this.m_chart_handler.sendMessage(message3);
                message3.obj = e4.getLocalizedMessage();
                e4.printStackTrace();
            }
        }
    }

    private class DownloadCommonHandler extends Handler {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    MainActivity.this.m_download_downBeanList = MainActivity.this.m_downloadBoundService.getDownloadingList().values();
                    Log.i(MainActivity.this.logTag, "m_downBeanList size=" + MainActivity.this.m_download_downBeanList.size());
                    MainActivity.this.m_download_adapter.dataChange(MainActivity.this.m_download_downBeanList);
                    if (MainActivity.this.m_download_downBeanList.size() <= 0) {
                        MainActivity.this.m_download_emptyText.setVisibility(0);
                        MainActivity.this.m_download_songList.setVisibility(8);
                        return;
                    }
                    MainActivity.this.m_download_emptyText.setVisibility(8);
                    MainActivity.this.m_download_songList.setVisibility(0);
                    return;
                default:
                    return;
            }
        }

        DownloadCommonHandler(Context context) {
        }
    }

    public class DownloadingAdapter extends BaseAdapter {
        private List<DownloadThread.DownloadBean> m_downLoadingList = new ArrayList();
        private LayoutInflater m_inflater;

        public final class ViewHolder {
            public Button m_cancelBtn;
            public TextView m_downingPercent;
            public ProgressBar m_downingProgressbar;
            public TextView m_downingSongName;

            public ViewHolder() {
            }
        }

        public DownloadingAdapter(Context context, Collection<DownloadThread.DownloadBean> list) {
            this.m_downLoadingList.addAll(list);
            this.m_inflater = LayoutInflater.from(context);
        }

        public void dataChange(Collection<DownloadThread.DownloadBean> list) {
            this.m_downLoadingList.clear();
            this.m_downLoadingList.addAll(list);
            notifyDataSetChanged();
        }

        public int getCount() {
            return this.m_downLoadingList.size();
        }

        public DownloadThread.DownloadBean getItem(int i) {
            return this.m_downLoadingList.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int position, View view, ViewGroup viewgroup) {
            ViewHolder viewholder;
            Log.i(MainActivity.this.logTag, "File Name " + getItem(position).threadName + ", " + getItem(position).downloadFilePosition);
            if (view == null) {
                viewholder = new ViewHolder();
                view = this.m_inflater.inflate((int) R.layout.downloading_item, (ViewGroup) null);
                viewholder.m_downingPercent = (TextView) view.findViewById(R.id.downing_percent);
                viewholder.m_downingProgressbar = (ProgressBar) view.findViewById(R.id.downing_progressbar);
                viewholder.m_downingSongName = (TextView) view.findViewById(R.id.downing_name);
                viewholder.m_cancelBtn = (Button) view.findViewById(R.id.downing_cancel_down);
                view.setTag(viewholder);
            } else {
                viewholder = (ViewHolder) view.getTag();
            }
            int percent = getItem(position).percent;
            viewholder.m_downingPercent.setText(percent + "%");
            viewholder.m_downingProgressbar.setMax(100);
            viewholder.m_downingProgressbar.setProgress(percent);
            viewholder.m_downingSongName.setText(getItem(position).filename);
            viewholder.m_cancelBtn.setOnClickListener(new DownLoadingCancelHandler(position));
            return view;
        }

        private class DownLoadingCancelHandler implements View.OnClickListener {
            private int position;

            public void onClick(View view) {
                if (this.position < DownloadingAdapter.this.getCount()) {
                    DownloadingAdapter.this.getItem(this.position).state = DownloadThread.State.Canceled;
                    MainActivity.this.m_downloadBoundService.cancelMusic(DownloadingAdapter.this.getItem(this.position).threadName);
                }
            }

            DownLoadingCancelHandler(int pos) {
                this.position = pos;
            }
        }
    }

    private class LibraryRenameOKHandler implements DialogInterface.OnClickListener {
        private final File file;
        private final EditText input;

        public void onClick(DialogInterface dialoginterface, int i) {
            String s = this.input.getText().toString();
            if (!s.trim().equals("")) {
                File file1 = new File(this.file.getParent(), s);
                if (file1.exists()) {
                    Toast.makeText(MainActivity.this, (int) R.string.rename_conflick, 0).show();
                    return;
                }
                this.file.renameTo(file1);
                dialoginterface.dismiss();
                if (MainActivity.this.m_library_libPosition == 2) {
                    MainActivity.this.queryDownloadFolder();
                } else if (MainActivity.this.m_library_libPosition == 1) {
                    MainActivity.this.libraryRefresh();
                }
            }
        }

        LibraryRenameOKHandler(File file1, EditText edittext) {
            this.input = edittext;
            this.file = file1;
        }
    }

    public class LibraryCreateSongContextMenu implements View.OnCreateContextMenuListener {
        public void onCreateContextMenu(ContextMenu contextmenu, View view, ContextMenu.ContextMenuInfo contextmenuinfo) {
            if (MainActivity.this.m_library_libPosition != 0) {
                contextmenu.clear();
                contextmenu.clearHeader();
                contextmenu.setHeaderIcon((int) R.drawable.context_menu_icon);
                contextmenu.setHeaderTitle((int) R.string.song_menu_title);
                contextmenu.add(0, 1, 0, (int) R.string.menu_play);
                contextmenu.add(0, 2, 0, (int) R.string.menu_share);
                contextmenu.add(0, 3, 0, (int) R.string.menu_rename);
                contextmenu.add(0, 4, 0, (int) R.string.menu_delete);
            }
        }

        LibraryCreateSongContextMenu() {
        }
    }

    private class LibraryRefreshReceiver extends BroadcastReceiver {
        private LibraryRefreshReceiver() {
        }

        /* synthetic */ LibraryRefreshReceiver(MainActivity mainActivity, LibraryRefreshReceiver libraryRefreshReceiver) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            String s = intent.getAction();
            Log.i(MainActivity.this.logTag, "Refresh success, broadcast received." + s);
            if (!"android.intent.action.MEDIA_SCANNER_STARTED".equals(s)) {
                "android.intent.action.MEDIA_SCANNER_SCAN_FILE".equals(s);
            }
            if ("android.intent.action.MEDIA_SCANNER_FINISHED".equals(s)) {
                Log.i(MainActivity.this.logTag, "receive m_libPostion =" + MainActivity.this.m_library_libPosition);
                if (MainActivity.this.m_library_libPosition == 2) {
                    MainActivity.this.queryDownloadFolder();
                } else if (MainActivity.this.m_library_libPosition == 1) {
                    if (MainActivity.this.m_library_libLoadingLayout.getVisibility() == 0) {
                        MainActivity.this.m_library_libLoadingLayout.setVisibility(8);
                    }
                    MainActivity.this.m_library_list.setVisibility(0);
                    MainActivity.this.libraryQueryAllSong();
                }
            }
        }
    }

    private class ChartCommonHandler extends Handler {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    if (MainActivity.this.m_chart_chartPosition != 0) {
                        if (MainActivity.this.m_chart_chartLoading.getVisibility() == 0) {
                            MainActivity.this.m_chart_chartLoading.setVisibility(8);
                        }
                        MainActivity.this.m_chart_songAdapter.notifyDataSetChanged();
                        return;
                    }
                    return;
                case AdWhirlUtil.NETWORK_TYPE_LIVERAIL:
                    if (MainActivity.this.m_chart_playLoading.getVisibility() == 0) {
                        MainActivity.this.m_chart_playLoading.setVisibility(8);
                        MainActivity.this.getPlayService().play(MainActivity.this.m_chart_selectedSong.m_songName, MainActivity.this.m_chart_selectedSong.m_downloadUri);
                    }
                    if (MainActivity.this.m_chart_commonDialog.isShowing() && MainActivity.this.m_chart_selectedSong.m_downloadUri != null) {
                        MainActivity.this.m_chart_selectedSong.m_fileName = String.valueOf(MainActivity.this.m_chart_selectedSong.m_songName) + MainActivity.this.m_chart_selectedSong.m_downloadUri.substring(MainActivity.this.m_chart_selectedSong.m_downloadUri.lastIndexOf("."));
                        MainActivity.this.m_chart_commonDialog.m_dialog_frame_title.setText((int) R.string.dialog_download_title);
                        MainActivity.this.m_chart_commonDialog.m_downloadConfirm.setVisibility(0);
                        MainActivity.this.chartSetDownLoadConfirmString(MainActivity.this.m_chart_selectedSong.m_songName);
                        MainActivity.this.m_chart_commonDialog.m_fetchLoadingURLLayout.setVisibility(8);
                        MainActivity.this.m_chart_commonDialog.m_ok.setEnabled(true);
                        return;
                    }
                    return;
                case AdWhirlUtil.NETWORK_TYPE_CUSTOM:
                    String errorMsg = String.format(MainActivity.this.getString(R.string.get_download_url_error), MainActivity.this.m_chart_selectedSong.m_songName).toString();
                    SpannableString spannablestring = new SpannableString(errorMsg);
                    ForegroundColorSpan foregroundcolorspan = new ForegroundColorSpan(-65536);
                    int index = errorMsg.indexOf(MainActivity.this.m_chart_selectedSong.m_songName);
                    spannablestring.setSpan(foregroundcolorspan, index, MainActivity.this.m_chart_selectedSong.m_songName.length() + index, 33);
                    MainActivity.this.m_chart_commonDialog.m_downloadConfirm.setVisibility(0);
                    MainActivity.this.m_chart_commonDialog.m_downloadConfirm.setText(spannablestring);
                    MainActivity.this.m_chart_commonDialog.m_fetchLoadingURLLayout.setVisibility(8);
                    MainActivity.this.m_chart_commonDialog.m_ok.setEnabled(false);
                    MainActivity.this.m_chart_commonDialog.m_dialog_frame_title.setText((int) R.string.dialog_download_title);
                    if (MainActivity.this.m_chart_playLoading.getVisibility() == 0) {
                        MainActivity.this.m_chart_playLoading.setVisibility(8);
                        MainActivity.this.m_chart_commonDialog.m_dialog_frame_title.setText((int) R.string.dialog_play_title);
                    }
                    if (!MainActivity.this.m_chart_commonDialog.isShowing()) {
                        MainActivity.this.m_chart_commonDialog.show();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }

        ChartCommonHandler(Context context) {
        }
    }

    public class ChartContextMenu implements View.OnCreateContextMenuListener {
        public void onCreateContextMenu(ContextMenu contextmenu, View view, ContextMenu.ContextMenuInfo contextmenuinfo) {
            if (MainActivity.this.m_chart_chartPosition != 0) {
                contextmenu.clear();
                contextmenu.clearHeader();
                contextmenu.setHeaderIcon((int) R.drawable.context_menu_icon);
                contextmenu.setHeaderTitle((int) R.string.song_menu_title);
                contextmenu.add(0, 1, 0, (int) R.string.search_menu_song);
                contextmenu.add(0, 2, 0, (int) R.string.menu_play);
            }
        }

        ChartContextMenu() {
        }
    }

    public final class ChartSongViewHolder {
        public View downBtn;
        public TextView song_author;
        public TextView song_name;

        public ChartSongViewHolder() {
        }
    }

    public class ChartSongAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        public ArrayList<SongBean> m_songBeanList;

        public ChartSongAdapter(Context context, ArrayList<SongBean> list) {
            this.mInflater = LayoutInflater.from(context);
            this.m_songBeanList = list == null ? new ArrayList<>() : list;
        }

        public int getCount() {
            return this.m_songBeanList.size();
        }

        public void dataChange(List<SongBean> list) {
            this.m_songBeanList.clear();
            this.m_songBeanList.addAll(list);
            notifyDataSetChanged();
        }

        public Object getItem(int position) {
            return this.m_songBeanList.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ChartSongViewHolder holder;
            if (convertView == null) {
                holder = new ChartSongViewHolder();
                convertView = this.mInflater.inflate((int) R.layout.chartlist, (ViewGroup) null);
                holder.song_name = (TextView) convertView.findViewById(R.id.item_songName_chart);
                holder.song_author = (TextView) convertView.findViewById(R.id.item_songAuthor_chart);
                holder.downBtn = convertView.findViewById(R.id.item_downloadIcon_chart);
                convertView.setTag(holder);
            } else {
                holder = (ChartSongViewHolder) convertView.getTag();
            }
            holder.song_name.setText(this.m_songBeanList.get(position).m_songName);
            holder.song_author.setText(this.m_songBeanList.get(position).m_singer);
            holder.downBtn.setOnClickListener(new ChartDownSongIconHandlerHandler(position));
            return convertView;
        }

        public class ChartDownSongIconHandlerHandler implements View.OnClickListener {
            private int position;

            public void onClick(View v) {
                MainActivity.this.m_chart_selectedSong = (SongBean) MainActivity.this.m_chart_songAdapter.getItem(this.position);
                MainActivity.this.chartShowDownSongDialog();
            }

            ChartDownSongIconHandlerHandler(int pos) {
                this.position = pos;
            }
        }
    }
}
