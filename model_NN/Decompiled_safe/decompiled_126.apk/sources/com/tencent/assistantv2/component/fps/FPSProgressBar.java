package com.tencent.assistantv2.component.fps;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ProgressBar;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.by;

/* compiled from: ProGuard */
public class FPSProgressBar extends ProgressBar {

    /* renamed from: a  reason: collision with root package name */
    private static Bitmap f1991a;
    private static Bitmap b;
    private int c;
    private boolean d;
    /* access modifiers changed from: private */
    public int e;
    private float f;
    private boolean g;
    /* access modifiers changed from: private */
    public int h;
    private SIZE i;
    private Paint j;
    /* access modifiers changed from: private */
    public Handler k;

    /* compiled from: ProGuard */
    public enum SIZE {
        TINY,
        SMALL,
        MIDDLE
    }

    public synchronized void a(SIZE size) {
        this.i = size;
    }

    private void a() {
        try {
            if (f1991a == null || f1991a.isRecycled()) {
                f1991a = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.progress_moving_small);
            }
            if (b == null || b.isRecycled()) {
                b = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.progress_moving_middle);
            }
        } catch (OutOfMemoryError e2) {
            XLog.e("ljh", e2.toString());
        }
    }

    private void b() {
        a();
        this.j = new Paint(1);
        this.c = -by.a(AstApp.i(), 15.0f);
    }

    public FPSProgressBar(Context context) {
        this(context, null);
        b();
    }

    public FPSProgressBar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.c = 0;
        this.d = false;
        this.e = 1;
        this.f = 0.95f;
        this.g = false;
        this.i = SIZE.SMALL;
        this.k = new a(this);
        b();
    }

    public FPSProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = 0;
        this.d = false;
        this.e = 1;
        this.f = 0.95f;
        this.g = false;
        this.i = SIZE.SMALL;
        this.k = new a(this);
        b();
    }

    public void a(int i2) {
        setProgress(i2);
    }

    public void setProgress(int i2) {
        this.g = false;
        super.setProgress(i2);
    }

    public void b(int i2) {
        this.g = true;
        this.h = i2;
        float max = ((float) i2) / ((float) getMax());
        if (i2 == 0 || max >= this.f || i2 < super.getProgress()) {
            super.setProgress(i2);
        } else {
            this.e = ((i2 - getProgress()) / 6) + 1;
            this.k.sendEmptyMessage(2);
        }
        if (this.g && !this.k.hasMessages(1)) {
            invalidate();
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.g) {
            a();
            if (this.i == SIZE.TINY) {
                a(canvas);
            } else if (this.i == SIZE.SMALL) {
                b(canvas);
            } else if (this.i == SIZE.MIDDLE) {
                c(canvas);
            }
        }
    }

    private void a(Canvas canvas) {
        int a2;
        int i2 = 5;
        Bitmap bitmap = f1991a;
        if (bitmap != null && !bitmap.isRecycled()) {
            int progress = (int) ((((float) getProgress()) / ((float) getMax())) * ((float) getWidth()));
            canvas.clipRect(new Rect(0, 0, progress, getHeight()));
            if (progress > 5) {
                i2 = progress;
            }
            if (i2 <= by.a(AstApp.i(), 50.0f)) {
                a2 = by.a(AstApp.i(), 1.0f);
            } else {
                a2 = by.a(AstApp.i(), 2.0f);
            }
            if (a2 <= 0) {
                a2 = 1;
            }
            this.c = a2 + this.c;
            if (this.c > i2) {
                this.c = -by.a(AstApp.i(), 10.0f);
            }
            canvas.drawBitmap(bitmap, (float) this.c, 0.0f, this.j);
            canvas.save(31);
            if (this.g && !this.k.hasMessages(1)) {
                this.k.sendEmptyMessageDelayed(1, 15);
            }
        }
    }

    private void b(Canvas canvas) {
        int a2;
        int i2 = 5;
        Bitmap bitmap = f1991a;
        if (bitmap != null && !bitmap.isRecycled()) {
            int progress = (int) ((((float) getProgress()) / ((float) getMax())) * ((float) getWidth()));
            canvas.clipRect(new Rect(0, 0, progress, getHeight()));
            if (progress > 5) {
                i2 = progress;
            }
            if (i2 <= by.a(AstApp.i(), 10.0f)) {
                a2 = by.a(AstApp.i(), 1.0f);
            } else {
                a2 = by.a(AstApp.i(), 1.5f);
            }
            if (a2 <= 0) {
                a2 = 1;
            }
            this.c = a2 + this.c;
            if (this.c > i2 + 2) {
                this.c = -by.a(AstApp.i(), 7.0f);
            }
            canvas.drawBitmap(bitmap, (float) this.c, 0.0f, this.j);
            canvas.save(31);
            if (this.g && !this.k.hasMessages(1)) {
                this.k.sendEmptyMessageDelayed(1, 15);
            }
        }
    }

    private void c(Canvas canvas) {
        int progress;
        int a2;
        int i2 = 5;
        Bitmap bitmap = b;
        if (bitmap != null && !bitmap.isRecycled()) {
            if (getProgress() > 96) {
                progress = (int) (((double) ((((float) getProgress()) / ((float) getMax())) * ((float) getWidth()))) * 0.96d);
            } else {
                progress = (int) ((((float) getProgress()) / ((float) getMax())) * ((float) getWidth()));
            }
            if (progress > 5) {
                i2 = progress;
            }
            if (i2 <= by.a(AstApp.i(), 30.0f)) {
                a2 = by.a(AstApp.i(), 1.5f);
            } else {
                a2 = by.a(AstApp.i(), 4.0f);
            }
            if (a2 <= 0) {
                a2 = 1;
            }
            this.c = a2 + this.c;
            if (this.c > i2) {
                this.c = by.a(AstApp.i(), 1.0f);
            }
            canvas.clipRect(new Rect(0, 0, i2, getHeight()));
            canvas.drawBitmap(bitmap, (float) this.c, (float) (-by.a(AstApp.i(), 3.0f)), this.j);
            canvas.save(31);
            if (this.g && !this.k.hasMessages(1)) {
                this.k.sendEmptyMessageDelayed(1, 15);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        b();
        if (this.d) {
            this.k.sendEmptyMessageDelayed(1, 0);
        }
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.k.removeMessages(2);
        if (f1991a != null && !f1991a.isRecycled()) {
            f1991a.recycle();
            f1991a = null;
        }
        if (b != null && !b.isRecycled()) {
            b.recycle();
            b = null;
        }
        super.onDetachedFromWindow();
    }

    public void setVisibility(int i2) {
        if (getVisibility() != i2) {
            super.setVisibility(i2);
        }
    }
}
