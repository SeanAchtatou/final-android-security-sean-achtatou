package com.tencent.assistantv2.component;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.TotalTabLayout;
import com.tencent.assistant.utils.by;
import com.tencent.assistant.utils.r;
import com.tencent.pangu.component.search.c;

/* compiled from: ProGuard */
public class TabBarView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    float f1942a;
    boolean b;
    int c;
    boolean d;
    int e;
    int f;
    private final String g;
    private View[] h;
    /* access modifiers changed from: private */
    public String[] i;
    private int j;
    private int k;
    /* access modifiers changed from: private */
    public c l;
    private Paint m;
    private Paint n;
    private int o;

    public TabBarView(Context context) {
        this(context, null);
    }

    public TabBarView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public TabBarView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet);
        this.g = "TabBarView";
        this.j = -1;
        this.f1942a = 0.0f;
        this.b = true;
        this.c = 0;
        this.d = true;
        this.e = -1;
        this.f = -1;
        this.k = -1;
        this.l = null;
        a(context, attributeSet);
    }

    private void a(Context context, AttributeSet attributeSet) {
        this.m = new Paint();
        this.m.setAntiAlias(true);
        this.m.setStyle(Paint.Style.FILL);
        this.m.setColor(-16732673);
        this.m.setStrokeWidth((float) b());
        this.n = new Paint();
        this.n.setAntiAlias(true);
        this.n.setStyle(Paint.Style.FILL);
        this.n.setColor(-3750459);
        this.n.setStrokeWidth((float) c());
        setWillNotDraw(false);
        setOrientation(0);
    }

    public void a(int[] iArr) {
        String[] strArr = null;
        if (iArr != null) {
            String[] strArr2 = new String[iArr.length];
            for (int i2 = 0; i2 < iArr.length; i2++) {
                strArr2[i2] = getResources().getString(iArr[i2]);
            }
            strArr = strArr2;
        }
        a(strArr);
    }

    public void a(String[] strArr) {
        if (strArr == null || strArr.length == 0) {
            this.h = new View[1];
        } else {
            this.h = new View[strArr.length];
        }
        this.i = strArr;
        d();
    }

    private void d() {
        if (this.i != null && this.i.length > 0) {
            setWeightSum((float) this.i.length);
            for (int i2 = 0; i2 < this.i.length; i2++) {
                TextView textView = new TextView(getContext());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, by.a(getContext(), 38.0f));
                layoutParams.weight = 1.0f;
                textView.setText(this.i[i2]);
                textView.setGravity(17);
                textView.setTextSize(2, 16.0f);
                textView.setTextColor(getContext().getResources().getColor(R.color.second_tab_unselected_color));
                textView.setLayoutParams(layoutParams);
                textView.setBackgroundResource(R.drawable.v2_button_background_selector);
                addView(textView);
                this.h[i2] = textView;
            }
        }
    }

    public void a(int i2, boolean z) {
        if (this.k != i2) {
            this.k = i2;
            int i3 = 0;
            while (true) {
                int i4 = i3;
                if (i4 < this.h.length) {
                    TextView textView = (TextView) this.h[i4];
                    if (i4 == i2) {
                        textView.setTextColor(getContext().getResources().getColor(R.color.second_tab_selected_color));
                    } else {
                        textView.setTextColor(getContext().getResources().getColor(R.color.second_tab_unselected_color));
                    }
                    i3 = i4 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.TabBarView.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistantv2.component.TabBarView.a(android.content.Context, android.util.AttributeSet):void
      com.tencent.assistantv2.component.TabBarView.a(int, float):void
      com.tencent.assistantv2.component.TabBarView.a(int, boolean):void */
    public void a(int i2) {
        c(i2);
        a(i2, false);
        bf bfVar = new bf(this);
        for (int i3 = 0; i3 < this.i.length; i3++) {
            View b2 = b(i3);
            if (b2 != null) {
                b2.setId(i3 + TotalTabLayout.START_ID_INDEX);
                b2.setOnClickListener(bfVar);
            }
        }
    }

    public void b(int i2, boolean z) {
        View b2 = b(i2);
        if (b2 != null) {
            b2.setId(i2 + TotalTabLayout.START_ID_INDEX);
            a(i2, z);
            if (this.l != null) {
                this.l.a(b2, 0);
            }
        }
    }

    public void a(c cVar) {
        this.l = cVar;
    }

    public View b(int i2) {
        if (i2 >= 0 && this.h.length >= i2 + 1) {
            return this.h[i2];
        }
        return null;
    }

    public int a() {
        int b2 = r.b();
        return (this.i == null || this.i.length <= 0) ? b2 : b2 / this.i.length;
    }

    public int b() {
        return by.a(getContext(), 3.0f);
    }

    public int c() {
        return 1;
    }

    public void c(int i2) {
        b(i2, 0.0f);
    }

    private void b(int i2, float f2) {
        this.o = (int) ((((float) i2) + f2) * ((float) a()));
        invalidate();
    }

    public void a(int i2, float f2) {
        b(i2, f2);
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawLine(0.0f, (float) (getHeight() - c()), (float) getWidth(), (float) (getHeight() - c()), this.n);
        canvas.drawLine((float) this.o, (float) ((getHeight() - (b() / 2)) - c()), (float) (this.o + a()), (float) ((getHeight() - (b() / 2)) - c()), this.m);
    }
}
