package com.tencent.assistantv2.component.fps;

import android.os.Handler;
import android.os.Message;

/* compiled from: ProGuard */
class a extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FPSProgressBar f1996a;

    a(FPSProgressBar fPSProgressBar) {
        this.f1996a = fPSProgressBar;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                this.f1996a.invalidate();
                return;
            case 2:
                int progress = this.f1996a.getProgress() + this.f1996a.e;
                if (progress < this.f1996a.getMax() && progress <= this.f1996a.h) {
                    a.super.setProgress(progress);
                    this.f1996a.k.sendEmptyMessageDelayed(2, 50);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
