package com.tencent.assistantv2.adapter;

import com.tencent.assistantv2.manager.MainTabType;

/* compiled from: ProGuard */
/* synthetic */ class c {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f1895a = new int[MainTabType.values().length];

    static {
        try {
            f1895a[MainTabType.APP.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f1895a[MainTabType.DISCOVER.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f1895a[MainTabType.HOT.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f1895a[MainTabType.GAME.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f1895a[MainTabType.TREASURY.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f1895a[MainTabType.EBOOK.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f1895a[MainTabType.VIDEO.ordinal()] = 7;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f1895a[MainTabType.THEME.ordinal()] = 8;
        } catch (NoSuchFieldError e8) {
        }
        try {
            f1895a[MainTabType.WEBVIEW.ordinal()] = 9;
        } catch (NoSuchFieldError e9) {
        }
    }
}
