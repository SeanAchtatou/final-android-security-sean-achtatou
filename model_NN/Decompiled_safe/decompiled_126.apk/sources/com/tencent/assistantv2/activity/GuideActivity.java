package com.tencent.assistantv2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ExViewPager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.SplashActivity;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.component.txscrollview.TXTabBarLayout;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.module.an;
import com.tencent.assistant.protocol.jce.AppTagInfo;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse;
import com.tencent.assistant.protocol.jce.WelcomePageItem;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.r;
import com.tencent.assistantv2.adapter.a;
import com.tencent.assistantv2.component.UserTagAnimationView;
import com.tencent.assistantv2.component.bg;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.STPageInfo;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.socialcontact.login.j;
import com.tencent.pangu.activity.ShareBaseActivity;
import com.tencent.pangu.manager.b;
import com.tencent.pangu.module.au;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class GuideActivity extends ShareBaseActivity implements Handler.Callback, View.OnTouchListener, UIEventListener {
    private static String[][] aa = {new String[]{"309900001", "阅读"}, new String[]{"309900048", "听书"}, new String[]{"309900008", "故事汇"}, new String[]{"279900026", "娱乐先锋"}, new String[]{"289900001", "听音乐"}, new String[]{"359900010", "资讯在手"}, new String[]{"359900009", "新闻"}, new String[]{"329900001", "看视频"}, new String[]{"249900045", "通讯"}, new String[]{"249900067", "出行助手"}, new String[]{"249900043", "便捷生活"}, new String[]{"299900005", "照片"}, new String[]{"299900004", "手机拍大片"}, new String[]{"299900006", "壁纸"}, new String[]{"269900017", "交个朋友吧"}, new String[]{"149900001", "爱旅行"}, new String[]{"189900016", "投资理财"}, new String[]{"139900024", "健康"}, new String[]{"109900036", "折扣"}, new String[]{"109900027", "网购"}, new String[]{"109900044", "时尚潮人"}, new String[]{"349900019", "常用工具"}, new String[]{"349900001", "移动办公"}, new String[]{"129900014", "最爱美食"}, new String[]{"209900239", "动漫卡通"}, new String[]{"209900002", "塔防战争"}, new String[]{"209900051", "街机"}, new String[]{"209900011", "经营"}, new String[]{"209900102", "激情对战"}, new String[]{"209900108", "策略"}, new String[]{"209900047", "格斗"}, new String[]{"209900117", "消除"}, new String[]{"209900076", "酷跑"}, new String[]{"209900274", "小游戏"}, new String[]{"209900122", "酷炫3D"}, new String[]{"209900116", "角色扮演"}, new String[]{"209900072", "竞速狂飙"}, new String[]{"209900104", "闯关"}, new String[]{"209900070", "勇猛射击"}, new String[]{"209900081", "冒险家"}, new String[]{"209900046", "动作"}, new String[]{"209900068", "休闲"}};
    /* access modifiers changed from: private */
    public Button A;
    private TextView B;
    /* access modifiers changed from: private */
    public CheckBox C;
    /* access modifiers changed from: private */
    public CheckBox D;
    private View E;
    private View F;
    /* access modifiers changed from: private */
    public UserTagAnimationView G;
    private ArrayList<WelcomePageItem> H = new ArrayList<>();
    private boolean I = false;
    private Handler J;
    private final int K = TXTabBarLayout.TABITEM_TIPS_TEXT_ID;
    private final int L = 102;
    private final int M = 103;
    private final int N = 104;
    /* access modifiers changed from: private */
    public boolean O = false;
    /* access modifiers changed from: private */
    public boolean P = false;
    private int Q = 3;
    private int R = 50;
    private long S = 0;
    /* access modifiers changed from: private */
    public GetPhoneUserAppListResponse T;
    /* access modifiers changed from: private */
    public int U = 0;
    /* access modifiers changed from: private */
    public TextView V;
    /* access modifiers changed from: private */
    public boolean W = false;
    private String X = Constants.STR_EMPTY;
    private an Y = new an();
    /* access modifiers changed from: private */
    public ArrayList<AppTagInfo> Z = new ArrayList<>();
    private CompoundButton.OnCheckedChangeListener ab = new c(this);
    private OnTMAParamExClickListener ac = new d(this);
    g n = new g(this);
    au u;
    /* access modifiers changed from: private */
    public String v = "GuideActivity";
    private ExViewPager w;
    private a x;
    private List<View> y = new ArrayList();
    private LayoutInflater z;

    private void w() {
        int[] a2 = a(0, aa.length, 5);
        if (a2 != null) {
            for (int i : a2) {
                String[] strArr = aa[i];
                AppTagInfo appTagInfo = new AppTagInfo();
                appTagInfo.f1161a = strArr[0];
                appTagInfo.b = strArr[1];
                this.Z.add(appTagInfo);
            }
        }
    }

    public static int[] a(int i, int i2, int i3) {
        if (i3 > i2 - i || i2 < i) {
            return null;
        }
        int[] iArr = new int[i3];
        int i4 = 0;
        while (i4 < i3) {
            int random = ((int) (Math.random() * ((double) (i2 - i)))) + i;
            boolean z2 = true;
            int i5 = 0;
            while (true) {
                if (i5 >= i3) {
                    break;
                } else if (random == iArr[i5]) {
                    z2 = false;
                    break;
                } else {
                    i5++;
                }
            }
            if (z2) {
                iArr[i4] = random;
                i4++;
            }
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.S = System.currentTimeMillis();
        x();
        if (b.a().c()) {
            startActivity(new Intent(this, MainActivity.class));
            TemporaryThreadManager.get().start(new b(this));
            finish();
            overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
            return;
        }
        setContentView((int) R.layout.activity_guide);
        y();
        z();
        if (SplashActivity.f383a) {
            u();
        }
    }

    private void x() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            this.X = intent.getExtras().getString("from_activity");
        }
    }

    public String t() {
        if (this.X == null) {
            this.X = Constants.STR_EMPTY;
        }
        return this.X;
    }

    /* access modifiers changed from: package-private */
    public void u() {
        this.u = new au();
        this.u.register(this.n);
        this.u.a(false);
        this.u.a();
    }

    public int f() {
        return STConst.ST_PAGE_GUIDE_RESULT_HUANJI_NO;
    }

    private void y() {
        this.J = new Handler(this);
        this.Y.register(this.n);
        this.Y.a(1, Constants.STR_EMPTY);
        w();
    }

    public STPageInfo n() {
        this.p.f2060a = STConst.ST_PAGE_GUIDE_COVER;
        return this.p;
    }

    /* access modifiers changed from: protected */
    public boolean i() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean h() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_SHARE_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_SHARE_FAIL, this);
        this.J.sendEmptyMessageDelayed(103, 1500);
        if (this.W) {
            C();
        }
    }

    private void z() {
        this.z = LayoutInflater.from(this);
        A();
        this.w = (ExViewPager) findViewById(R.id.vPager);
        this.x = new a();
        this.x.a(this.y);
        this.w.setAdapter(this.x);
        this.w.setOnTouchListener(this);
        this.w.setOnPageChangeListener(new h(this));
    }

    private void A() {
        this.E = this.z.inflate((int) R.layout.guide_tags_pre_view, (ViewGroup) null);
        this.F = this.z.inflate((int) R.layout.guide_tags_view, (ViewGroup) null);
        this.G = (UserTagAnimationView) this.F.findViewById(R.id.user_tag_anim);
        this.G.a(this.Z);
        this.B = (TextView) this.F.findViewById(R.id.btn_skip);
        this.B.setOnClickListener(this.ac);
        this.B.setTag(R.id.tma_st_slot_tag, "002");
        this.y.add(this.E);
        this.y.add(this.F);
    }

    /* access modifiers changed from: private */
    public String B() {
        if (this.H.size() > 0) {
            return this.H.get(0).c;
        }
        return Constants.STR_EMPTY;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void C() {
        TemporaryThreadManager.get().start(new e(this));
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("action_key_from_guide", true);
        startActivity(intent);
        TemporaryThreadManager.get().start(new f(this));
        finish();
    }

    public void finish() {
        super.finish();
        if (SplashActivity.f383a) {
            if (this.u != null) {
                this.u.unregister(this.n);
            }
            if (this.T == null) {
                r.a(this, System.currentTimeMillis());
            }
        }
    }

    private void D() {
        this.J.sendEmptyMessageDelayed(TXTabBarLayout.TABITEM_TIPS_TEXT_ID, 10000);
    }

    private void F() {
        if (j.a().j()) {
            this.J.removeMessages(102);
            D();
            return;
        }
        XLog.i(this.v, "这小子还真跑进这个分支了");
        this.J.sendEmptyMessageDelayed(102, 200);
    }

    public void handleUIEvent(Message message) {
        boolean z2 = true;
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
                XLog.i(this.v, "login success" + message.arg2);
                if (message.arg2 == 1 && !this.P) {
                    z2 = false;
                }
                this.I = z2;
                D();
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_FAIL:
                XLog.i(this.v, "login fail");
                return;
            case EventDispatcherEnum.UI_EVENT_SHARE_SUCCESS:
                if (this.O) {
                    bg.a(getApplicationContext(), (int) R.string.share_success, 0);
                    C();
                    return;
                } else if ((message.obj instanceof Integer) && 4 == ((Integer) message.obj).intValue()) {
                    if (message.what == 1089) {
                        bg.a(getApplicationContext(), getString(R.string.share_success), 0);
                    }
                    C();
                    return;
                } else {
                    return;
                }
            case EventDispatcherEnum.UI_EVENT_SHARE_FAIL:
                if (this.O) {
                    bg.a(getApplicationContext(), (int) R.string.share_fail, 0);
                    C();
                    return;
                } else if ((message.obj instanceof Integer) && 4 == ((Integer) message.obj).intValue()) {
                    C();
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        XLog.i(this.v, ">>onStop>>");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        XLog.i(this.v, ">>onDestory>>");
        super.onDestroy();
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_FAIL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_SHARE_SUCCESS, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_SHARE_FAIL, this);
        if (this.J != null) {
            this.J.removeCallbacksAndMessages(null);
        }
        l.a((int) STConst.ST_PAGE_COMPETITIVE, CostTimeSTManager.TIMETYPE.CORRECTION_MINUS, System.currentTimeMillis() - this.S);
    }

    public boolean handleMessage(Message message) {
        switch (message.what) {
            case TXTabBarLayout.TABITEM_TIPS_TEXT_ID:
                if (this.I) {
                    try {
                        bg.a(getApplicationContext(), getString(R.string.guide_toast_fail), 0);
                        break;
                    } catch (Exception e) {
                        break;
                    }
                }
                break;
            case 102:
                F();
                break;
            case 103:
                if (this.w == null || this.w.getCurrentItem() == 0) {
                }
            case 104:
                if (this.w != null && this.w.getCurrentItem() == 0) {
                    this.w.smoothScrollTo(0, 0, 100);
                    break;
                }
        }
        return false;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (4 != i) {
            return true;
        }
        if (t().equals(SplashActivity.class.getSimpleName())) {
            C();
            l.a(new STInfoV2(STConst.ST_PAGE_GUIDE_RESULT_HUANJI_NO, "04_002", 0, STConst.ST_DEFAULT_SLOT, 200));
            return true;
        }
        finish();
        return true;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        this.J.removeMessages(103);
        this.J.removeMessages(104);
        return false;
    }

    /* access modifiers changed from: package-private */
    public void v() {
        l.a(new STInfoV2(STConst.ST_PAGE_GUIDE_RESULT_HUANJI_NO, STConst.ST_DEFAULT_SLOT, 0, STConst.ST_DEFAULT_SLOT, 100));
    }
}
