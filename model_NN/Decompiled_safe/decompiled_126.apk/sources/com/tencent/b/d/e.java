package com.tencent.b.d;

import android.content.Context;
import org.json.JSONObject;

public class e {

    /* renamed from: a  reason: collision with root package name */
    static g f2079a;
    private static JSONObject d = null;
    Integer b = null;
    String c = null;

    public e(Context context) {
        try {
            a(context);
            this.b = i.e(context.getApplicationContext());
            this.c = i.d(context);
        } catch (Throwable th) {
            k.a(th);
        }
    }

    static synchronized g a(Context context) {
        g gVar;
        synchronized (e.class) {
            if (f2079a == null) {
                f2079a = new g(context.getApplicationContext());
            }
            gVar = f2079a;
        }
        return gVar;
    }

    public void a(JSONObject jSONObject) {
        JSONObject jSONObject2 = new JSONObject();
        try {
            if (f2079a != null) {
                f2079a.a(jSONObject2);
            }
            k.a(jSONObject2, "cn", this.c);
            if (this.b != null) {
                jSONObject2.put("tn", this.b);
            }
            jSONObject.put("ev", jSONObject2);
            if (d != null && d.length() > 0) {
                jSONObject.put("eva", d);
            }
        } catch (Throwable th) {
            k.a(th);
        }
    }
}
