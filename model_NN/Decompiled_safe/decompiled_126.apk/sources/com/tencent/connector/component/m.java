package com.tencent.connector.component;

import android.view.View;
import com.qq.AppService.AppService;

/* compiled from: ProGuard */
class m implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ContentVersionLowTip f2409a;

    m(ContentVersionLowTip contentVersionLowTip) {
        this.f2409a = contentVersionLowTip;
    }

    public void onClick(View view) {
        if (this.f2409a.i == 0) {
            if (this.f2409a.b != null) {
                AppService.BusinessConnectionType Q = AppService.Q();
                if (Q == AppService.BusinessConnectionType.QQ) {
                    this.f2409a.b.i();
                } else if (Q == AppService.BusinessConnectionType.QRCODE) {
                    this.f2409a.b.a(this.f2409a.pcName, this.f2409a.qrcode);
                } else {
                    this.f2409a.b.b();
                }
            }
        } else if (this.f2409a.i == 1 && this.f2409a.b != null) {
            this.f2409a.a();
        }
    }
}
