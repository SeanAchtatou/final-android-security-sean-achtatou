package com.tencent.pangu.component;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class y extends PagerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private List<PopUpContentView> f3745a = new ArrayList();

    public int getCount() {
        return this.f3745a.size();
    }

    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        if (this.f3745a == null || this.f3745a.size() <= i) {
            return null;
        }
        viewGroup.addView(this.f3745a.get(i));
        return this.f3745a.get(i);
    }

    public void a(List<PopUpContentView> list) {
        this.f3745a.clear();
        this.f3745a.addAll(list);
        notifyDataSetChanged();
    }

    public long a() {
        long j = 0;
        Iterator<PopUpContentView> it = this.f3745a.iterator();
        while (true) {
            long j2 = j;
            if (!it.hasNext()) {
                return j2;
            }
            PopUpContentView next = it.next();
            if (next != null) {
                j = j2 + next.b();
            } else {
                j = j2;
            }
        }
    }

    public int b() {
        int i = 0;
        Iterator<PopUpContentView> it = this.f3745a.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            PopUpContentView next = it.next();
            if (next != null) {
                i = next.a() + i2;
            } else {
                i = i2;
            }
        }
    }

    public ArrayList<w> c() {
        ArrayList<w> arrayList = new ArrayList<>();
        for (PopUpContentView c : this.f3745a) {
            arrayList.addAll(c.c());
        }
        return arrayList;
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        if (this.f3745a != null && this.f3745a.size() > i) {
            viewGroup.removeView(this.f3745a.get(i));
        }
    }

    public void a(int i) {
        PopUpContentView popUpContentView;
        if (this.f3745a != null && this.f3745a.size() > i && (popUpContentView = this.f3745a.get(i)) != null) {
            popUpContentView.d();
            popUpContentView.e();
        }
    }
}
