package com.tencent.pangu.component;

import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.m;
import com.tencent.assistantv2.manager.a;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.game.activity.n;
import com.tencent.nucleus.manager.component.SwitchButton;

/* compiled from: ProGuard */
class ap extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RankNormalListView f3524a;

    ap(RankNormalListView rankNormalListView) {
        this.f3524a = rankNormalListView;
    }

    public void onTMAClick(View view) {
        boolean z = true;
        this.f3524a.F++;
        if (this.f3524a.F < 10) {
            int i = this.f3524a.F - 1;
            this.f3524a.E[i] = true;
            if (i > 0) {
                this.f3524a.E[i - 1] = false;
            }
            this.f3524a.B.postDelayed(new aq(this, i), 2000);
            if (this.f3524a.F == 5) {
                this.f3524a.A.setBackgroundResource(R.color.apk_list_bg);
            }
            if (this.f3524a.F > 5) {
                this.f3524a.D.setText(RankNormalListView.O[this.f3524a.F % 5]);
                this.f3524a.D.setVisibility(0);
                return;
            }
            SwitchButton switchButton = this.f3524a.B;
            if (this.f3524a.B.b()) {
                z = false;
            }
            switchButton.b(z);
            m.a().t(this.f3524a.B.b());
            a.a().b().a(this.f3524a.B.b());
            if (this.f3524a.B.b()) {
                Toast.makeText(this.f3524a.getContext(), (int) R.string.hide_installed_apps_in_list, 4).show();
            } else if (this.f3524a.G) {
                Toast.makeText(this.f3524a.getContext(), (int) R.string.hide_installed_apps_in_list_cancel_rank, 4).show();
            } else {
                Toast.makeText(this.f3524a.getContext(), (int) R.string.hide_installed_apps_in_list_cancel, 4).show();
            }
            this.f3524a.u.c();
            if (this.f3524a.u.getCount() > 0 && this.f3524a.f != null && !this.f3524a.f.g()) {
                this.f3524a.b_();
            }
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 sTInfoV2 = new STInfoV2(this.f3524a.y.G(), "08", this.f3524a.y.G(), "08", 200);
        if (!this.f3524a.G || !(this.f3524a.y instanceof n)) {
            sTInfoV2.slotId = com.tencent.assistantv2.st.page.a.a("08", 0);
        } else {
            sTInfoV2.slotId = com.tencent.assistantv2.st.page.a.a(((n) this.f3524a.y).M(), 0);
        }
        if (!a.a().b().c()) {
            sTInfoV2.status = "01";
        } else {
            sTInfoV2.status = "02";
        }
        sTInfoV2.actionId = 200;
        return sTInfoV2;
    }
}
