package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.c;
import com.tencent.assistant.protocol.jce.AppDetailExGift;
import com.tencent.assistant.protocol.jce.AppHotFriend;
import com.tencent.assistant.protocol.jce.AppTagInfo;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistant.protocol.jce.RelateNews;
import com.tencent.assistant.utils.an;
import com.tencent.assistant.utils.bo;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class AppDetailViewV5 extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private HorizonScrollPicViewer f3528a;
    private View b;
    /* access modifiers changed from: private */
    public ExpandableTextViewV5 c;
    /* access modifiers changed from: private */
    public ImageView d;
    private TXImageView e;
    private View f;
    private TextView g;
    /* access modifiers changed from: private */
    public SimpleAppModel h;
    private c i;
    /* access modifiers changed from: private */
    public Context j;
    private LayoutInflater k;
    private boolean l = false;
    private boolean m = true;
    private f n;
    private int[] o = {R.string.chinese, R.string.english, R.string.other};
    private FriendRankView p;
    private FriendTalkView q;
    private DetailTagView r;
    private DetailGameNewsView s;
    private ViewStub t;
    private ViewStub u;
    /* access modifiers changed from: private */
    public AppdetailGiftView v;
    private View.OnClickListener w = new i(this);

    /* compiled from: ProGuard */
    public enum APPDETAIL_MODE {
        JUST_OPEN,
        NEED_UPDATE,
        NOT_INSTALLED,
        FROM_OTHER_APPLICATION
    }

    public AppDetailViewV5(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.j = context;
    }

    public AppDetailViewV5(Context context) {
        super(context);
        this.j = context;
    }

    public static APPDETAIL_MODE a(c cVar, SimpleAppModel simpleAppModel) {
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(simpleAppModel.c);
        if (localApkInfo == null) {
            return APPDETAIL_MODE.NOT_INSTALLED;
        }
        if (localApkInfo.mVersionCode < cVar.f941a.a().a().get(0).c) {
            return APPDETAIL_MODE.NEED_UPDATE;
        }
        return APPDETAIL_MODE.JUST_OPEN;
    }

    public void b(c cVar, SimpleAppModel simpleAppModel) {
        if (cVar != null && simpleAppModel != null) {
            this.i = cVar;
            this.h = simpleAppModel;
            d();
            i();
            a(a((RelateNews) an.b(this.i.f941a.f1149a.f, RelateNews.class)));
        }
    }

    private ao a(RelateNews relateNews) {
        boolean z;
        boolean z2;
        if (relateNews == null || relateNews.d == null || relateNews.d.size() == 0) {
            return null;
        }
        ao aoVar = new ao();
        aoVar.e = relateNews.f;
        aoVar.f3573a = relateNews.f1457a;
        aoVar.b = relateNews.b;
        if (relateNews.e == 1) {
            z = true;
        } else {
            z = false;
        }
        aoVar.d = z;
        aoVar.f = relateNews.c;
        aoVar.g = relateNews.g;
        int i2 = 0;
        while (i2 < 3 && i2 < relateNews.d.size()) {
            aoVar.c[i2] = new ap();
            aoVar.c[i2].c = relateNews.d.get(i2).c;
            aoVar.c[i2].b = relateNews.d.get(i2).b;
            aoVar.c[i2].f3574a = relateNews.d.get(i2).e;
            ap apVar = aoVar.c[i2];
            if (relateNews.d.get(i2).d == 3) {
                z2 = true;
            } else {
                z2 = false;
            }
            apVar.d = z2;
            if (aoVar.c[i2].d) {
                aoVar.c[i2].e = relateNews.d.get(i2).f;
            } else {
                aoVar.c[i2].f = relateNews.d.get(i2).f;
            }
            aoVar.c[i2].g = relateNews.d.get(i2).g;
            i2++;
        }
        return aoVar;
    }

    private void d() {
        this.k = LayoutInflater.from(this.j);
        switch (l.f3599a[a(this.i, this.h).ordinal()]) {
            case 1:
                this.k.inflate((int) R.layout.app_detail_layout_v5_for_install, this);
                break;
            case 2:
                this.k.inflate((int) R.layout.app_detail_layout_v5_for_open, this);
                break;
            case 3:
                this.k.inflate((int) R.layout.app_detail_layout_v5_for_update, this);
                break;
            case 4:
                this.k.inflate((int) R.layout.app_detail_layout_v5_for_install, this);
                break;
        }
        e();
        f();
        g();
        h();
        l();
        m();
        n();
        o();
    }

    private void e() {
    }

    private void f() {
        this.f3528a = (HorizonScrollPicViewer) findViewById(R.id.my_view_pager);
        if (k()) {
            this.f3528a.setVisibility(0);
        } else {
            this.f3528a.setVisibility(8);
        }
    }

    private void g() {
        this.b = findViewById(R.id.description_detail_layout);
        this.b.setOnClickListener(this.w);
        this.g = (TextView) findViewById(R.id.update_time);
        this.c = (ExpandableTextViewV5) findViewById(R.id.description_txt);
        this.d = (ImageView) findViewById(R.id.expand_more_img);
        this.f = findViewById(R.id.gift_area_layout);
        this.e = (TXImageView) findViewById(R.id.gift_icon);
        if (this.h.T == null || this.h.T.b() == null || TextUtils.isEmpty(this.h.T.b().f1125a)) {
            this.f.setVisibility(8);
            return;
        }
        this.e.setVisibility(0);
        ((TextView) this.e.findViewById(R.id.my_gift_txt)).setText(this.h.T.a());
        if (!TextUtils.isEmpty(this.h.T.a())) {
            this.e.updateImageView(this.h.T.a(), R.drawable.user_task_icon_gift, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        }
        this.f.setOnClickListener(new h(this));
    }

    private void h() {
        this.u = (ViewStub) findViewById(R.id.detail_gift_stub);
    }

    private void i() {
        if (this.i != null) {
            String str = this.i.f941a.f1149a.b.get(0).l;
            if (k()) {
                this.f3528a.setVisibility(0);
                this.f3528a.a(this.i.f941a.f1149a.b.get(0));
            } else {
                this.f3528a.setVisibility(8);
            }
            String str2 = this.i.f941a.f1149a.f1144a.e;
            if (this.i.f941a.f1149a.b.get(0).r >= this.o.length) {
                int length = this.o.length - 1;
            }
            String c2 = bo.c(Long.valueOf(this.i.f941a.f1149a.b.get(0).i * 1000));
            String str3 = String.format(getResources().getString(R.string.update_time), c2) + "\n";
            if (this.g != null) {
                this.g.setText(str3);
            }
            if (a(this.i, this.h) != APPDETAIL_MODE.NEED_UPDATE) {
                String str4 = this.i.f941a.f1149a.b.get(0).j;
                if (TextUtils.isEmpty(str4)) {
                    str4 = this.i.f941a.f1149a.b.get(0).k;
                }
                String replaceAll = str4.replaceAll("[\\n]{3}", "\n\n");
                if (TextUtils.isEmpty(replaceAll)) {
                    replaceAll = getResources().getString(R.string.no_any_current_monent);
                }
                this.c.setText(replaceAll);
            } else {
                String str5 = this.i.f941a.f1149a.b.get(0).k;
                if (TextUtils.isEmpty(str5)) {
                    String str6 = this.i.f941a.f1149a.b.get(0).j;
                    if (TextUtils.isEmpty(str6)) {
                        this.c.setText(getResources().getString(R.string.no_any_current_monent));
                    } else {
                        this.c.setText(str6.replaceAll("[\\n]{3}", "\n\n"));
                    }
                } else {
                    String format = String.format(getResources().getString(R.string.update_info_description), str5);
                    if (!TextUtils.isEmpty(format)) {
                        format = format + "\n";
                    }
                    SpannableString spannableString = new SpannableString(format + this.i.f941a.f1149a.b.get(0).j);
                    spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.appdetail_update_info_title)), 0, 5, 33);
                    this.c.setText(spannableString);
                }
            }
            this.c.a(true);
            this.c.post(new j(this));
            j();
            if (this.m) {
                this.l = true;
                this.m = false;
            }
        }
    }

    private void j() {
        AppDetailExGift appDetailExGift;
        if (this.i.f941a.f1149a != null && (appDetailExGift = (AppDetailExGift) an.b(this.i.f941a.f1149a.e, AppDetailExGift.class)) != null && appDetailExGift.b != null && appDetailExGift.b.size() > 0 && appDetailExGift.c > 0) {
            if (this.v == null) {
                this.u.inflate();
                this.v = (AppdetailGiftView) findViewById(R.id.gift_about_area);
            }
            this.v.postDelayed(new k(this, appDetailExGift), 100);
        }
    }

    public void a() {
        if (this.f3528a != null) {
            this.f3528a.a();
        }
    }

    public void b() {
        if (this.f3528a != null) {
            this.f3528a.b();
        }
    }

    public void a(ba baVar) {
        this.f3528a.a(baVar);
    }

    public ac c() {
        return this.f3528a.c();
    }

    public void a(bb bbVar) {
        this.f3528a.a(bbVar);
    }

    private boolean k() {
        if ((!com.tencent.assistant.net.c.e() || com.tencent.assistant.net.c.l()) && m.a().p()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        if (this.l) {
            this.l = false;
        }
    }

    public void a(f fVar) {
        this.n = fVar;
    }

    private void l() {
        this.p = (FriendRankView) findViewById(R.id.friend_rank_view);
    }

    private void m() {
        this.q = (FriendTalkView) findViewById(R.id.my_friends_area);
    }

    public void a(ArrayList<CommentDetail> arrayList, ArrayList<CommentTagInfo> arrayList2) {
        this.q = (FriendTalkView) findViewById(R.id.my_friends_area);
        this.q.a("精彩评论", arrayList, arrayList2);
        if (this.q.getVisibility() == 8 && this.p.getVisibility() == 8) {
            findViewById(R.id.friends_show_area).setVisibility(8);
        } else {
            findViewById(R.id.friends_show_area).setVisibility(0);
        }
    }

    public void a(bh bhVar) {
        this.q.a(bhVar);
    }

    private void n() {
        this.r = (DetailTagView) findViewById(R.id.my_tag_area);
    }

    private void o() {
        this.t = (ViewStub) findViewById(R.id.detail_game_news_stub);
    }

    public void a(ao aoVar) {
        if (this.s == null) {
            this.t.inflate();
            this.s = (DetailGameNewsView) findViewById(R.id.game_about_area);
        }
        this.s.a(aoVar);
    }

    public void a(List<AppTagInfo> list, long j2, String str) {
        this.r.a("应用标签", list, j2, str);
    }

    public void a(String str, ArrayList<AppHotFriend> arrayList) {
        this.p.a(str, arrayList);
        if (this.q.getVisibility() == 8 && this.p.getVisibility() == 8) {
            findViewById(R.id.friends_show_area).setVisibility(8);
        } else {
            findViewById(R.id.friends_show_area).setVisibility(0);
        }
    }
}
