package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.b;
import com.tencent.assistant.module.k;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.utils.at;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.activity.AppDetailActivityV5;
import com.tencent.pangu.component.appdetail.process.s;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.module.a;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class CustomRelateAppViewV5 extends LinearLayout implements ak {

    /* renamed from: a  reason: collision with root package name */
    public View f3541a;
    public AstApp b;
    public int c = 0;
    public String d = Constants.STR_EMPTY;
    public String e = Constants.STR_EMPTY;
    /* access modifiers changed from: private */
    public Context f;
    private LinearLayout[] g = new LinearLayout[4];
    private TXImageView[] h = new TXImageView[4];
    private TextView[] i = new TextView[4];
    /* access modifiers changed from: private */
    public TextView[] j = new TextView[4];
    /* access modifiers changed from: private */
    public DwonloadButtonForAppDetail[] k = new DwonloadButtonForAppDetail[4];
    /* access modifiers changed from: private */
    public TXDwonloadProcessBar[] l = new TXDwonloadProcessBar[4];
    /* access modifiers changed from: private */
    public List<SimpleAppModel> m = new ArrayList();
    private long n = -100;
    private int o = 2000;
    private boolean p = false;
    private b q = new b();
    /* access modifiers changed from: private */
    public a r = new a();
    private View.OnClickListener s = new ag(this);
    private com.tencent.pangu.module.a.a t = new ah(this);

    public CustomRelateAppViewV5(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f = context;
        e();
    }

    public CustomRelateAppViewV5(Context context) {
        super(context);
        this.f = context;
        e();
    }

    private void e() {
        this.b = AstApp.i();
        View inflate = LayoutInflater.from(this.f).inflate((int) R.layout.appdetail_custom_relate_layout_v5, this);
        this.f3541a = this;
        this.g[0] = (LinearLayout) inflate.findViewById(R.id.ralate_app_layout1);
        this.g[1] = (LinearLayout) inflate.findViewById(R.id.ralate_app_layout2);
        this.g[2] = (LinearLayout) inflate.findViewById(R.id.ralate_app_layout3);
        this.g[3] = (LinearLayout) inflate.findViewById(R.id.ralate_app_layout4);
        for (int i2 = 0; i2 < this.g.length; i2++) {
            this.g[i2].setOnClickListener(this.s);
            this.g[i2].setBackgroundResource(R.drawable.v2_button_background_selector);
        }
        b();
        this.h[0] = (TXImageView) inflate.findViewById(R.id.soft_icon_img1);
        this.h[1] = (TXImageView) inflate.findViewById(R.id.soft_icon_img2);
        this.h[2] = (TXImageView) inflate.findViewById(R.id.soft_icon_img3);
        this.h[3] = (TXImageView) inflate.findViewById(R.id.soft_icon_img4);
        this.i[0] = (TextView) inflate.findViewById(R.id.soft_name_txt1);
        this.i[1] = (TextView) inflate.findViewById(R.id.soft_name_txt2);
        this.i[2] = (TextView) inflate.findViewById(R.id.soft_name_txt3);
        this.i[3] = (TextView) inflate.findViewById(R.id.soft_name_txt4);
        this.j[0] = (TextView) inflate.findViewById(R.id.apk_size_1);
        this.j[1] = (TextView) inflate.findViewById(R.id.apk_size_2);
        this.j[2] = (TextView) inflate.findViewById(R.id.apk_size_3);
        this.j[3] = (TextView) inflate.findViewById(R.id.apk_size_4);
        this.k[0] = (DwonloadButtonForAppDetail) inflate.findViewById(R.id.app_state_bar_1);
        this.k[1] = (DwonloadButtonForAppDetail) inflate.findViewById(R.id.app_state_bar_2);
        this.k[2] = (DwonloadButtonForAppDetail) inflate.findViewById(R.id.app_state_bar_3);
        this.k[3] = (DwonloadButtonForAppDetail) inflate.findViewById(R.id.app_state_bar_4);
        this.l[0] = (TXDwonloadProcessBar) inflate.findViewById(R.id.download_info1);
        this.l[1] = (TXDwonloadProcessBar) inflate.findViewById(R.id.download_info2);
        this.l[2] = (TXDwonloadProcessBar) inflate.findViewById(R.id.download_info3);
        this.l[3] = (TXDwonloadProcessBar) inflate.findViewById(R.id.download_info4);
    }

    public void b() {
        for (int i2 = 0; i2 < this.g.length; i2++) {
            this.g[i2].setTag(R.id.tma_st_slot_tag, Integer.valueOf(i2));
        }
    }

    public void a(List<SimpleAppModel> list) {
        if (list != null) {
            if (list.size() > 4) {
                list = list.subList(0, 4);
            }
            this.m.clear();
            this.m.addAll(list);
            this.r.register(this.t);
            f();
        }
    }

    private void f() {
        for (int i2 = 0; i2 < this.m.size(); i2++) {
            this.g[i2].setVisibility(0);
            SimpleAppModel simpleAppModel = this.m.get(i2);
            this.h[i2].updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.i[i2].setText(simpleAppModel.d);
            if (simpleAppModel.k > 0) {
                this.j[i2].setText(at.a(simpleAppModel.k));
            }
            new com.tencent.pangu.component.appdetail.process.a();
            this.k[i2].a(this.m.get(i2));
            this.l[i2].a(this.m.get(i2), this.j[i2]);
            if (s.a(this.m.get(i2))) {
                this.k[i2].b().setClickable(false);
            } else {
                this.k[i2].b().setOnClickListener(new af(this, i2));
            }
        }
        for (int size = this.m.size(); size < this.g.length; size++) {
            this.g[size].setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void a(SimpleAppModel simpleAppModel) {
        if (simpleAppModel != null && !this.p) {
            this.p = true;
            Intent intent = new Intent(this.f, AppDetailActivityV5.class);
            if (this.f instanceof BaseActivity) {
                intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.f).f());
            }
            intent.putExtra("simpleModeInfo", simpleAppModel);
            intent.putExtra("statInfo", STInfoBuilder.buildDownloadSTInfo(this.f, simpleAppModel));
            this.f.startActivity(intent);
            this.p = false;
        }
    }

    public void a() {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.m.size()) {
                l.a(STInfoBuilder.buildSTInfo(this.f, this.m.get(i3), a(i3), 100, null));
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(SimpleAppModel simpleAppModel, View view, int i2) {
        DownloadInfo downloadInfo;
        if (simpleAppModel != null) {
            if (simpleAppModel.d() || simpleAppModel.e() || simpleAppModel.h() || simpleAppModel.c() || simpleAppModel.e() || simpleAppModel.i()) {
                a(simpleAppModel);
                return;
            }
            DownloadInfo a2 = DownloadProxy.a().a(simpleAppModel);
            StatInfo buildDownloadSTInfo = STInfoBuilder.buildDownloadSTInfo(this.f, simpleAppModel);
            buildDownloadSTInfo.slotId = a(i2);
            buildDownloadSTInfo.recommendId = simpleAppModel.y;
            if (a2 != null && a2.needReCreateInfo(simpleAppModel)) {
                DownloadProxy.a().b(a2.downloadTicket);
                a2 = null;
            }
            if (a2 == null) {
                a2 = DownloadInfo.createDownloadInfo(simpleAppModel, buildDownloadSTInfo);
                if ((view instanceof com.tencent.assistant.manager.b) && simpleAppModel != null) {
                    com.tencent.assistant.manager.a.a().a(simpleAppModel.q(), (com.tencent.assistant.manager.b) view);
                    downloadInfo = a2;
                }
                downloadInfo = a2;
            } else {
                a2.updateDownloadInfoStatInfo(buildDownloadSTInfo);
                downloadInfo = a2;
            }
            switch (aj.f3569a[k.d(simpleAppModel).ordinal()]) {
                case 1:
                case 2:
                    com.tencent.pangu.download.a.a().a(downloadInfo);
                    com.tencent.assistant.utils.a.a((ImageView) this.f3541a.findViewWithTag(downloadInfo.downloadTicket));
                    return;
                case 3:
                case 4:
                    com.tencent.pangu.download.a.a().b(downloadInfo.downloadTicket);
                    return;
                case 5:
                    com.tencent.pangu.download.a.a().b(downloadInfo);
                    return;
                case 6:
                    com.tencent.pangu.download.a.a().d(downloadInfo);
                    return;
                case 7:
                    com.tencent.pangu.download.a.a().c(downloadInfo);
                    return;
                case 8:
                case 9:
                    com.tencent.pangu.download.a.a().a(downloadInfo);
                    return;
                case 10:
                    Toast.makeText(this.f, (int) R.string.unsupported, 0).show();
                    return;
                case 11:
                    Toast.makeText(this.f, (int) R.string.tips_slicent_install, 0).show();
                    return;
                case 12:
                    Toast.makeText(this.f, (int) R.string.tips_slicent_uninstall, 0).show();
                    return;
                default:
                    return;
            }
        }
    }

    public void c() {
        for (int i2 = 0; i2 < 4; i2++) {
            this.k[i2].d();
            this.l[i2].a();
        }
    }

    /* access modifiers changed from: package-private */
    public String a(int i2) {
        return com.tencent.assistantv2.st.page.a.a(Constants.VIA_REPORT_TYPE_MAKE_FRIEND, i2);
    }

    public void d() {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.m.size() && i3 < this.k.length) {
                this.k[i3].a(this.m.get(i3));
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }
}
