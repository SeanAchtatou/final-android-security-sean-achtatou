package com.tencent.pangu.component.appdetail;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class v extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3622a;
    final /* synthetic */ AppdetailGiftView b;

    v(AppdetailGiftView appdetailGiftView, int i) {
        this.b = appdetailGiftView;
        this.f3622a = i;
    }

    public void onTMAClick(View view) {
        this.b.a(this.b.c.size(), this.b.e);
    }

    public STInfoV2 getStInfo() {
        if (!(this.b.b instanceof AppDetailActivityV5)) {
            return null;
        }
        STInfoV2 t = ((AppDetailActivityV5) this.b.b).t();
        t.slotId = a.a(this.b.d == 1 ? Constants.VIA_ACT_TYPE_NINETEEN : "18", this.f3622a);
        t.actionId = 200;
        return t;
    }
}
