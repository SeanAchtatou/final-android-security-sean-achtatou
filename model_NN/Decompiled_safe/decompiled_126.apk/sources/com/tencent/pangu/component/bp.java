package com.tencent.pangu.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class bp extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ShareQzView f3658a;

    bp(ShareQzView shareQzView) {
        this.f3658a = shareQzView;
    }

    public void onTMAClick(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel /*2131166036*/:
                if (this.f3658a.k != null) {
                    this.f3658a.k.b();
                    return;
                }
                return;
            case R.id.btn_submit /*2131166037*/:
                if (this.f3658a.k != null) {
                    if (this.f3658a.i != null) {
                        this.f3658a.i.b = this.f3658a.f.getText().toString();
                    }
                    if (this.f3658a.j != null) {
                        this.f3658a.j.e = this.f3658a.f.getText().toString();
                    }
                    this.f3658a.k.a();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public STInfoV2 getStInfo() {
        if (!(this.f3658a.l instanceof AppDetailActivityV5)) {
            return null;
        }
        STInfoV2 t = ((AppDetailActivityV5) this.f3658a.l).t();
        t.actionId = 200;
        if (this.clickViewId == R.id.btn_cancel) {
            t.slotId = a.a(ShareQzView.m, "001");
            return t;
        } else if (this.clickViewId != R.id.btn_submit) {
            return t;
        } else {
            t.slotId = a.a(ShareQzView.m, "002");
            return t;
        }
    }
}
