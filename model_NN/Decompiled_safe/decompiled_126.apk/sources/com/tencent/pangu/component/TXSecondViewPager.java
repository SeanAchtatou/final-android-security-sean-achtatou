package com.tencent.pangu.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.tencent.assistantv2.component.TXViewPager;

/* compiled from: ProGuard */
public class TXSecondViewPager extends TXViewPager {

    /* renamed from: a  reason: collision with root package name */
    private boolean f3507a = true;

    public TXSecondViewPager(Context context) {
        super(context);
    }

    public TXSecondViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void a(boolean z) {
        this.f3507a = false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.f3507a) {
            return true;
        }
        return super.onTouchEvent(motionEvent);
    }
}
