package com.tencent.pangu.component.appdetail;

import android.text.TextUtils;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class af extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    int f3565a = this.b;
    final /* synthetic */ int b;
    final /* synthetic */ CustomRelateAppViewV5 c;
    private byte d = 0;

    af(CustomRelateAppViewV5 customRelateAppViewV5, int i) {
        this.c = customRelateAppViewV5;
        this.b = i;
    }

    public void onTMAClick(View view) {
        if (((SimpleAppModel) this.c.m.get(this.f3565a)).b <= 0 || TextUtils.isEmpty(((SimpleAppModel) this.c.m.get(this.f3565a)).i)) {
            this.c.l[this.f3565a].a((SimpleAppModel) this.c.m.get(this.f3565a));
            this.c.k[this.f3565a].a();
            this.c.k[this.f3565a].setTag(Integer.valueOf(this.c.r.a((SimpleAppModel) this.c.m.get(this.f3565a), this.d).b));
            return;
        }
        this.c.a((SimpleAppModel) this.c.m.get(this.f3565a), view, this.f3565a);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.c.f, (SimpleAppModel) this.c.m.get(this.f3565a), this.c.a(this.f3565a), 200, a.a(k.d((SimpleAppModel) this.c.m.get(this.f3565a)), (SimpleAppModel) this.c.m.get(this.f3565a)));
        buildSTInfo.actionId = a.a(k.d((SimpleAppModel) this.c.m.get(this.f3565a)));
        buildSTInfo.recommendId = ((SimpleAppModel) this.c.m.get(this.f3565a)).y;
        return buildSTInfo;
    }
}
