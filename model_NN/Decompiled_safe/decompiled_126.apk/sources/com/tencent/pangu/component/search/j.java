package com.tencent.pangu.component.search;

import com.tencent.pangu.component.search.ISearchResultPage;

/* compiled from: ProGuard */
public class j {

    /* renamed from: a  reason: collision with root package name */
    private ISearchResultPage.PageType f3706a = ISearchResultPage.PageType.NATIVE;
    private int b;
    private int c;
    private String d;

    public j a(ISearchResultPage.PageType pageType) {
        this.f3706a = pageType;
        return this;
    }

    public ISearchResultPage.PageType a() {
        return this.f3706a;
    }

    public j a(int i) {
        this.b = i;
        return this;
    }

    public int b() {
        return this.b;
    }

    public j b(int i) {
        this.c = i;
        return this;
    }

    public int c() {
        return this.c;
    }

    public j a(String str) {
        this.d = str;
        return this;
    }

    public String d() {
        return this.d;
    }
}
