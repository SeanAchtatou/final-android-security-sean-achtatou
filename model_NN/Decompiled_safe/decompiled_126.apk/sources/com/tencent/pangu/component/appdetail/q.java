package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.manager.t;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class q extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f3618a;
    final /* synthetic */ AppdetailFlagView b;

    q(AppdetailFlagView appdetailFlagView, Context context) {
        this.b = appdetailFlagView;
        this.f3618a = context;
    }

    public void onTMAClick(View view) {
        if (this.b.f3531a.getVisibility() == 8) {
            this.b.b();
            this.b.f3531a.setVisibility(0);
            try {
                this.b.b.setImageResource(R.drawable.icon_close);
            } catch (Throwable th) {
                t.a().b();
            }
        } else {
            this.b.f3531a.setVisibility(8);
            try {
                this.b.b.setImageResource(R.drawable.icon_open);
            } catch (Throwable th2) {
                t.a().b();
            }
        }
    }

    public STInfoV2 getStInfo() {
        if (!(this.f3618a instanceof AppDetailActivityV5)) {
            return null;
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b.getContext(), 200);
        buildSTInfo.slotId = a.a(this.b.f, this.b.g);
        if (this.b.f3531a.getVisibility() == 8) {
            buildSTInfo.status = "01";
            return buildSTInfo;
        }
        buildSTInfo.status = "02";
        return buildSTInfo;
    }
}
