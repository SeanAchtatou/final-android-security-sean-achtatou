package com.tencent.pangu.component;

import android.view.View;
import com.tencent.assistant.component.SecondNavigationTitleView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.SelfUpdateActivity;
import com.tencent.pangu.manager.SelfUpdateManager;

/* compiled from: ProGuard */
class bc extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SelfForceUpdateView f3648a;

    bc(SelfForceUpdateView selfForceUpdateView) {
        this.f3648a = selfForceUpdateView;
    }

    public void onTMAClick(View view) {
        this.f3648a.d();
    }

    public STInfoV2 getStInfo() {
        if (!(this.f3648a.getContext() instanceof SelfUpdateActivity)) {
            return null;
        }
        STInfoV2 t = ((SelfUpdateActivity) this.f3648a.getContext()).t();
        t.slotId = SecondNavigationTitleView.TMA_ST_NAVBAR_SEARCH_TAG;
        if (!SelfUpdateManager.a().a(this.f3648a.b.e)) {
            t.actionId = 900;
        } else {
            t.actionId = 305;
        }
        t.extraData = "02";
        return t;
    }
}
