package com.tencent.pangu.component;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.Random;

/* compiled from: ProGuard */
class j extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Drawable f3681a;
    final /* synthetic */ CommentDetail b;
    final /* synthetic */ int c;
    final /* synthetic */ int d;
    final /* synthetic */ View e;
    final /* synthetic */ Drawable f;
    final /* synthetic */ TextView g;
    final /* synthetic */ CommentDetailView h;

    j(CommentDetailView commentDetailView, Drawable drawable, CommentDetail commentDetail, int i, int i2, View view, Drawable drawable2, TextView textView) {
        this.h = commentDetailView;
        this.f3681a = drawable;
        this.b = commentDetail;
        this.c = i;
        this.d = i2;
        this.e = view;
        this.f = drawable2;
        this.g = textView;
    }

    public STInfoV2 getStInfo(View view) {
        return null;
    }

    public void onTMAClick(View view) {
        long j;
        long j2;
        TextView textView = (TextView) view;
        if (this.h.ag == 0) {
            ah.a().removeCallbacks(this.h.ah);
            ah.a().postDelayed(this.h.ah, 2000);
        } else if (this.h.ag >= 3) {
            Toast.makeText(this.h.d, this.h.d.getString(R.string.comment_praise_tofast), 0).show();
            ah.a().removeCallbacks(this.h.ah);
            ah.a().postDelayed(this.h.ah, 2000);
        }
        CommentDetailView.q(this.h);
        if (((Boolean) textView.getTag()).booleanValue()) {
            textView.setTag(false);
            textView.setCompoundDrawables(this.f3681a, null, null, null);
            long parseInt = ((long) Integer.parseInt(textView.getTag(R.id.comment_praise_count) + Constants.STR_EMPTY)) - 1;
            if (parseInt < 0) {
                j2 = 0;
            } else {
                j2 = parseInt;
            }
            textView.setText(bm.a(j2) + Constants.STR_EMPTY);
            textView.setTextColor(Color.parseColor("#a4a4a4"));
            textView.setTag(R.id.comment_praise_count, Long.valueOf(j2));
            this.h.F.a(this.b.h, this.h.r.f938a, (byte) 2, this.h.t, 0, Constants.STR_EMPTY, this.b.d, Constants.STR_EMPTY, this.h.r.d, this.h.r.c);
            l.a(new STInfoV2(STConst.ST_PAGE_APP_DETAIL_COMMENT, this.h.L + bm.a(this.c + this.d + 1), 0, STConst.ST_DEFAULT_SLOT, 200));
            CommentDetail commentDetail = (CommentDetail) this.e.getTag(R.id.comment_page_detail);
            if (commentDetail != null) {
                commentDetail.s = 0;
                commentDetail.r = j2;
                this.e.setTag(R.id.comment_page_detail, commentDetail);
                return;
            }
            return;
        }
        textView.setTag(true);
        textView.setCompoundDrawables(this.f, null, null, null);
        this.g.setText(this.h.I[new Random().nextInt(this.h.I.length)]);
        this.g.setVisibility(0);
        Animation loadAnimation = AnimationUtils.loadAnimation(this.h.d, R.anim.comment_praise_animation);
        loadAnimation.setAnimationListener(new n(this.h, this.g));
        this.g.startAnimation(loadAnimation);
        long parseInt2 = ((long) Integer.parseInt(textView.getTag(R.id.comment_praise_count) + Constants.STR_EMPTY)) + 1;
        if (parseInt2 < 0) {
            j = 0;
        } else {
            j = parseInt2;
        }
        textView.setText(j + Constants.STR_EMPTY);
        textView.setTextColor(Color.parseColor("#b68a46"));
        textView.setTag(R.id.comment_praise_count, Long.valueOf(j));
        this.h.F.a(this.b.h, this.h.r.f938a, (byte) 1, this.h.t, 0, Constants.STR_EMPTY, this.b.d, Constants.STR_EMPTY, this.h.r.d, this.h.r.c);
        l.a(new STInfoV2(STConst.ST_PAGE_APP_DETAIL_COMMENT, this.h.K + bm.a(this.c + this.d + 1), 0, STConst.ST_DEFAULT_SLOT, 200));
        CommentDetail commentDetail2 = (CommentDetail) this.e.getTag(R.id.comment_page_detail);
        if (commentDetail2 != null) {
            commentDetail2.s = 1;
            commentDetail2.r = j;
            this.e.setTag(R.id.comment_page_detail, commentDetail2);
        }
    }
}
