package com.tencent.pangu.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.ReportAppRequest;
import com.tencent.assistant.protocol.jce.ReportAppResponse;
import com.tencent.pangu.module.a.b;

/* compiled from: ProGuard */
public class e extends BaseEngine<b> {
    public int a(long j, long j2, byte[] bArr, String str) {
        ReportAppRequest reportAppRequest = new ReportAppRequest();
        reportAppRequest.f1462a = j;
        reportAppRequest.b = j2;
        reportAppRequest.c = bArr;
        reportAppRequest.d = str;
        return send(reportAppRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        ReportAppRequest reportAppRequest = (ReportAppRequest) jceStruct;
        int i2 = i;
        notifyDataChangedInMainThread(new f(this, i2, (ReportAppResponse) jceStruct2, reportAppRequest.f1462a, reportAppRequest.b));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new g(this, i, i2));
    }
}
