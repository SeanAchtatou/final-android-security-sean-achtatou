package com.tencent.pangu.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.GetAppTagInfoListRequest;
import com.tencent.assistant.protocol.jce.GetAppTagInfoListResponse;
import com.tencent.pangu.module.a.d;

/* compiled from: ProGuard */
public class s extends BaseEngine<d> {
    public int a(SimpleAppModel simpleAppModel) {
        return send(new GetAppTagInfoListRequest(simpleAppModel.f938a, simpleAppModel.c));
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new t(this, i, (GetAppTagInfoListResponse) jceStruct2));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        GetAppTagInfoListResponse getAppTagInfoListResponse = (GetAppTagInfoListResponse) jceStruct2;
        notifyDataChangedInMainThread(new u(this, i, i2));
    }
}
