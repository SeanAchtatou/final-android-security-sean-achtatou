package com.tencent.pangu.module.wisedownload;

import android.os.Message;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.notification.a;
import com.tencent.assistant.manager.notification.z;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.module.update.r;
import com.tencent.assistant.module.update.t;
import com.tencent.assistant.protocol.jce.AutoDownloadInfo;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bo;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.module.wisedownload.condition.ThresholdCondition;
import com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean;
import com.tencent.pangu.utils.installuninstall.p;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class j implements UIEventListener {
    private static j b = null;

    /* renamed from: a  reason: collision with root package name */
    private AstApp f3965a = AstApp.i();
    private ConcurrentHashMap<String, Integer> c = new ConcurrentHashMap<>();
    private ArrayList<String> d = new ArrayList<>();
    private final int e = 3;

    public static synchronized j a() {
        j jVar;
        synchronized (j.class) {
            if (b == null) {
                b = new j();
            }
            jVar = b;
        }
        return jVar;
    }

    private j() {
        e();
    }

    private void e() {
        b();
    }

    public void b() {
        this.f3965a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_START, this);
        this.f3965a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_PAUSE, this);
        this.f3965a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
        this.f3965a.k().addUIEventListener(1007, this);
        this.f3965a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
        this.f3965a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        this.f3965a.k().addUIEventListener(1027, this);
    }

    public void handleUIEvent(Message message) {
        DownloadInfo d2;
        DownloadInfo d3;
        DownloadInfo d4;
        DownloadInfo d5;
        DownloadInfo d6;
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING:
                if (message.obj instanceof String) {
                    String str = (String) message.obj;
                    if (TextUtils.isEmpty(str) || (d2 = DownloadProxy.a().d(str)) == null || !d2.isUiTypeWiseDownload() || DownloadInfo.getPercent(d2) % 5 == 0) {
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC:
                if (message.obj instanceof String) {
                    String str2 = (String) message.obj;
                    if (!TextUtils.isEmpty(str2) && (d6 = DownloadProxy.a().d(str2)) != null && d6.isUiTypeWiseDownload()) {
                        if (d6.uiType == SimpleDownloadInfo.UIType.WISE_BOOKING_DOWNLOAD && !p.a().b()) {
                            a.a().a(d6.downloadTicket);
                        } else if (d6.uiType == SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD) {
                            v.a();
                            if (!t.a().b(d6.uiType, d6.packageName, d6.versionCode) || !p.a().b()) {
                                z.a().a(d6.downloadTicket);
                            } else {
                                m.a().h(d6.downloadTicket);
                            }
                        } else if (d6.uiType == SimpleDownloadInfo.UIType.WISE_BOOKING_DOWNLOAD) {
                            m.a().g(d6.downloadTicket);
                        }
                    } else {
                        return;
                    }
                }
                i.a().o();
                return;
            case 1007:
                if (message.obj instanceof String) {
                    String str3 = (String) message.obj;
                    if (!TextUtils.isEmpty(str3) && (d5 = DownloadProxy.a().d(str3)) != null && d5.isUiTypeWiseDownload()) {
                        b(str3);
                        if (!b(d5)) {
                            return;
                        }
                        if (d5.response == null || d5.response.f3766a == 0) {
                            TemporaryThreadManager.get().start(new k(this, str3));
                            return;
                        } else {
                            i.a().p();
                            return;
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC:
                if (message.obj != null && (message.obj instanceof InstallUninstallTaskBean)) {
                    String str4 = ((InstallUninstallTaskBean) message.obj).downloadTicket;
                    if (!TextUtils.isEmpty(str4) && (d4 = DownloadProxy.a().d(str4)) != null && d4.isUiTypeWiseBookingDownload() && d4.isUiTypeWiseSubscribtionDownloadAutoInstall()) {
                        if (d4.uiType == SimpleDownloadInfo.UIType.WISE_BOOKING_DOWNLOAD) {
                            XLog.d("BookingDownload", "Download SUCC ticket " + str4);
                            a.a().a(d4.downloadTicket);
                            return;
                        } else if (d4.uiType == SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD) {
                            String ae = m.a().ae();
                            if (!TextUtils.isEmpty(ae) && ae.equals(d4.downloadTicket)) {
                                z.a().a(d4.downloadTicket);
                                return;
                            }
                            return;
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 1027:
                if (message.obj != null && (message.obj instanceof InstallUninstallTaskBean)) {
                    String str5 = ((InstallUninstallTaskBean) message.obj).downloadTicket;
                    if (!TextUtils.isEmpty(str5) && (d3 = DownloadProxy.a().d(str5)) != null && d3.isUiTypeWiseBookingDownload() && d3.isUiTypeWiseSubscribtionDownloadAutoInstall()) {
                        if (d3.uiType == SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD) {
                            String ae2 = m.a().ae();
                            if (!TextUtils.isEmpty(ae2) && ae2.equals(d3.downloadTicket)) {
                                z.a().a(d3.downloadTicket);
                                return;
                            }
                            return;
                        }
                        String ad = m.a().ad();
                        if (!TextUtils.isEmpty(ad) && ad.equals(d3.downloadTicket)) {
                            a.a().a(d3.downloadTicket);
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_START:
                a(message.arg1, message.arg2);
                return;
            case EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_PAUSE:
                a(message.arg1);
                return;
            default:
                return;
        }
    }

    private void b(String str) {
        if (!this.c.containsKey(str)) {
            this.c.put(str, 1);
        } else {
            this.c.put(str, Integer.valueOf(this.c.get(str).intValue() + 1));
        }
        this.d.add(str);
    }

    private boolean c(String str) {
        if (TextUtils.isEmpty(str) || !this.c.containsKey(str)) {
            return false;
        }
        int intValue = this.c.get(str).intValue();
        if (this.d.contains(str) || intValue >= 3) {
            return true;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.DownloadProxy.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.pangu.manager.DownloadProxy.b(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>):java.lang.String
      com.tencent.pangu.manager.DownloadProxy.b(java.lang.String, boolean):void */
    public void a(int i, int i2) {
        SimpleDownloadInfo.UIType uIType;
        boolean z;
        boolean z2;
        List<AutoDownloadInfo> i3;
        String str;
        switch (i) {
            case 1:
                uIType = SimpleDownloadInfo.UIType.WISE_SELF_UPDAET;
                break;
            case 2:
                uIType = SimpleDownloadInfo.UIType.WISE_APP_UPDATE;
                break;
            case 3:
                uIType = SimpleDownloadInfo.UIType.WISE_NEW_DOWNLOAD;
                break;
            case 4:
            default:
                return;
            case 5:
                uIType = SimpleDownloadInfo.UIType.WISE_BOOKING_DOWNLOAD;
                break;
            case 6:
                uIType = SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD;
                break;
        }
        if (!(ThresholdCondition.CONDITION_TRIGGER_ACTION.DOWNLOAD_FAIL.ordinal() == i2 || ThresholdCondition.CONDITION_TRIGGER_ACTION.DOWNLOAD_SUCC.ordinal() == i2)) {
            this.d.clear();
        }
        com.tencent.pangu.download.a.a();
        ArrayList<DownloadInfo> a2 = DownloadProxy.a().a(SimpleDownloadInfo.DownloadType.APK);
        if (a2 != null && a2.size() > 0) {
            Iterator<DownloadInfo> it = a2.iterator();
            while (true) {
                if (it.hasNext()) {
                    DownloadInfo next = it.next();
                    if (next != null && next.uiType == uIType && bo.b(next.createTime)) {
                        if (uIType == SimpleDownloadInfo.UIType.WISE_APP_UPDATE && (c(next) || d(next.packageName))) {
                            XLog.d("million", "WiseDownloadManager.startWifiAutoDownload, 应用" + next.packageName + "下载到一半由于被忽略或被卸载的原因，Wifi智能下载记录删除并中止它的下载");
                            DownloadProxy.a().b(next.downloadTicket, false);
                        } else if (a(next, uIType)) {
                            com.tencent.pangu.download.a.a().b(next, uIType);
                            m.b(i, ThresholdCondition.CONDITION_RESULT_CODE.OK_CONTINUE_DOWNLOAD, i2);
                            z = true;
                        }
                    }
                }
            }
        }
        z = false;
        if (!z) {
            switch (i) {
                case 1:
                    i3 = t.a().g();
                    break;
                case 2:
                    i3 = t.a().d();
                    break;
                case 3:
                    i3 = t.a().f();
                    break;
                case 4:
                default:
                    return;
                case 5:
                    i3 = t.a().h();
                    break;
                case 6:
                    i3 = t.a().i();
                    break;
            }
            Iterator<SimpleAppModel> it2 = k.e(i3).iterator();
            while (true) {
                if (it2.hasNext()) {
                    SimpleAppModel next2 = it2.next();
                    if (uIType != SimpleDownloadInfo.UIType.WISE_APP_UPDATE || !a(next2)) {
                        DownloadInfo a3 = DownloadProxy.a().a(next2);
                        if (a3 != null && a3.needReCreateInfo(next2)) {
                            DownloadProxy.a().b(a3.downloadTicket);
                            a3 = DownloadInfo.createDownloadInfo(next2, null);
                        }
                        if (a3 == null) {
                            DownloadInfo createDownloadInfo = DownloadInfo.createDownloadInfo(next2, (StatInfo) null, uIType);
                            if (a(createDownloadInfo, uIType)) {
                                com.tencent.pangu.download.a.a().a(createDownloadInfo, uIType);
                                m.b(i, ThresholdCondition.CONDITION_RESULT_CODE.OK_BEGIN_DOWNLOAD, i2);
                                z2 = true;
                            }
                        } else if (a3.uiType == uIType && a(a3, uIType)) {
                            com.tencent.pangu.download.a.a().b(a3, uIType);
                            m.b(i, ThresholdCondition.CONDITION_RESULT_CODE.OK_CONTINUE_DOWNLOAD, i2);
                            z2 = true;
                        }
                    } else {
                        StringBuilder append = new StringBuilder().append("WiseDownloadManager.startWifiAutoDownload, app:");
                        if (next2 == null) {
                            str = "null";
                        } else {
                            str = next2.c;
                        }
                        XLog.d("million", append.append(str).toString());
                    }
                }
            }
            if (!z2 && a2 != null && a2.size() > 0) {
                for (DownloadInfo next3 : a2) {
                    if (next3.uiType == uIType) {
                        if (uIType == SimpleDownloadInfo.UIType.WISE_APP_UPDATE && (c(next3) || d(next3.packageName))) {
                            XLog.d("million", "WiseDownloadManager.startWifiAutoDownload, 应用" + next3.packageName + "接着下今天以前的任务下载发现它是己忽略的或己卸载的原因，Wifi智能下载记录删除并中止它的下载");
                            DownloadProxy.a().b(next3.downloadTicket, false);
                        } else if (a(next3, uIType)) {
                            com.tencent.pangu.download.a.a().b(next3, uIType);
                            m.b(i, ThresholdCondition.CONDITION_RESULT_CODE.OK_CONTINUE_DOWNLOAD, i2);
                            return;
                        }
                    }
                }
                return;
            }
            return;
        }
        z2 = z;
        if (!z2) {
        }
    }

    public boolean a(DownloadInfo downloadInfo, SimpleDownloadInfo.UIType uIType) {
        if (downloadInfo != null && (uIType == SimpleDownloadInfo.UIType.WISE_SELF_UPDAET || uIType == SimpleDownloadInfo.UIType.WISE_BOOKING_DOWNLOAD || uIType == SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD || uIType == SimpleDownloadInfo.UIType.WISE_APP_UPDATE || uIType == SimpleDownloadInfo.UIType.WISE_NEW_DOWNLOAD)) {
            if (uIType == SimpleDownloadInfo.UIType.WISE_BOOKING_DOWNLOAD || uIType == SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD) {
                if (downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.SUCC || downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.INSTALLING || downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.INSTALLED || a(downloadInfo)) {
                    return false;
                }
                return true;
            } else if (!downloadInfo.isDownloaded() && !a(downloadInfo)) {
                if (uIType != SimpleDownloadInfo.UIType.WISE_NEW_DOWNLOAD || ApkResourceManager.getInstance().getLocalApkInfo(downloadInfo.packageName) == null) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    public boolean a(DownloadInfo downloadInfo) {
        if (downloadInfo == null || !downloadInfo.isUiTypeWiseDownload()) {
            return false;
        }
        return c(downloadInfo.downloadTicket);
    }

    private boolean b(DownloadInfo downloadInfo) {
        if (downloadInfo == null || !downloadInfo.isUiTypeWiseDownload() || DownloadProxy.a().b(downloadInfo.errorCode) || DownloadProxy.a().c(downloadInfo.errorCode) || DownloadProxy.a().d(downloadInfo.errorCode)) {
            return false;
        }
        return true;
    }

    private boolean c(DownloadInfo downloadInfo) {
        Set<r> e2 = com.tencent.assistant.module.update.j.b().e();
        if (e2 == null) {
            return false;
        }
        r rVar = new r();
        rVar.a(downloadInfo.packageName, downloadInfo.versionName, downloadInfo.versionCode, false);
        if (e2.contains(rVar)) {
            return true;
        }
        return false;
    }

    private boolean a(SimpleAppModel simpleAppModel) {
        Set<r> e2;
        if (simpleAppModel == null || (e2 = com.tencent.assistant.module.update.j.b().e()) == null) {
            return false;
        }
        r rVar = new r();
        rVar.a(simpleAppModel.c, simpleAppModel.f, simpleAppModel.g, false);
        if (e2.contains(rVar)) {
            return true;
        }
        return false;
    }

    private boolean d(String str) {
        if (ApkResourceManager.getInstance().getLocalApkInfo(str) == null) {
            return true;
        }
        return false;
    }

    public void a(int i) {
        for (DownloadInfo next : DownloadProxy.a().a(SimpleDownloadInfo.DownloadType.APK)) {
            if (next != null) {
                if (i == 1 && next.uiType == SimpleDownloadInfo.UIType.WISE_SELF_UPDAET) {
                    com.tencent.pangu.download.a.a().b(next.downloadTicket);
                } else if (i == 2 && next.uiType == SimpleDownloadInfo.UIType.WISE_APP_UPDATE) {
                    com.tencent.pangu.download.a.a().b(next.downloadTicket);
                } else if (i == 3 && next.uiType == SimpleDownloadInfo.UIType.WISE_NEW_DOWNLOAD) {
                    com.tencent.pangu.download.a.a().b(next.downloadTicket);
                } else if (i == 5 && next.uiType == SimpleDownloadInfo.UIType.WISE_BOOKING_DOWNLOAD) {
                    com.tencent.pangu.download.a.a().b(next.downloadTicket);
                } else if (i == 6 && next.uiType == SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD) {
                    com.tencent.pangu.download.a.a().b(next.downloadTicket);
                }
            }
        }
    }

    public static int c() {
        DownloadInfo downloadInfo;
        ArrayList<DownloadInfo> c2 = DownloadProxy.a().c();
        if (c2 == null || c2.isEmpty()) {
            return -1;
        }
        Iterator<DownloadInfo> it = c2.iterator();
        while (true) {
            if (!it.hasNext()) {
                downloadInfo = null;
                break;
            }
            downloadInfo = it.next();
            if (downloadInfo != null && downloadInfo.isUiTypeWiseDownload()) {
                if (downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.QUEUING || downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.DOWNLOADING) {
                    break;
                }
            }
        }
        if (downloadInfo == null) {
            return -1;
        }
        if (downloadInfo.uiType == SimpleDownloadInfo.UIType.WISE_SELF_UPDAET) {
            return 1;
        }
        if (downloadInfo.uiType == SimpleDownloadInfo.UIType.WISE_APP_UPDATE) {
            return 2;
        }
        if (downloadInfo.uiType == SimpleDownloadInfo.UIType.WISE_NEW_DOWNLOAD) {
            return 3;
        }
        if (downloadInfo.uiType == SimpleDownloadInfo.UIType.WISE_BOOKING_DOWNLOAD) {
            return 5;
        }
        return downloadInfo.uiType == SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD ? 6 : -1;
    }

    public boolean d() {
        ArrayList<DownloadInfo> d2 = DownloadProxy.a().d();
        if (d2 == null || d2.isEmpty()) {
            return false;
        }
        for (DownloadInfo next : d2) {
            if (next != null && next.isUiTypeWiseDownload()) {
                if (next.downloadState == SimpleDownloadInfo.DownloadState.QUEUING || next.downloadState == SimpleDownloadInfo.DownloadState.DOWNLOADING) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean a(String str) {
        boolean z = false;
        Iterator<SimpleAppModel> it = k.e(t.a().g()).iterator();
        while (true) {
            if (it.hasNext()) {
                if (it.next().c.equals(str)) {
                    z = true;
                    break;
                }
            } else {
                break;
            }
        }
        if (!z) {
            Iterator<SimpleAppModel> it2 = k.e(t.a().c()).iterator();
            while (it2.hasNext()) {
                if (it2.next().c.equals(str)) {
                    return true;
                }
            }
        }
        return z;
    }
}
