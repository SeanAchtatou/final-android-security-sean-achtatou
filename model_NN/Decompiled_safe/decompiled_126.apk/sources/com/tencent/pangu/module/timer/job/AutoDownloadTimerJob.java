package com.tencent.pangu.module.timer.job;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.module.timer.TimePointJob;
import com.tencent.assistant.module.update.z;
import com.tencent.assistant.protocol.jce.AutoDownloadCfg;
import com.tencent.assistant.protocol.jce.AutoDownloadPolicy;
import com.tencent.assistant.utils.XLog;
import com.tencent.pangu.module.wisedownload.i;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ProGuard */
public class AutoDownloadTimerJob implements TimePointJob {

    /* renamed from: a  reason: collision with root package name */
    private static AutoDownloadTimerJob f3951a;

    public static synchronized AutoDownloadTimerJob d() {
        AutoDownloadTimerJob autoDownloadTimerJob;
        synchronized (AutoDownloadTimerJob.class) {
            if (f3951a == null) {
                f3951a = new AutoDownloadTimerJob();
            }
            autoDownloadTimerJob = f3951a;
        }
        return autoDownloadTimerJob;
    }

    public int b() {
        return getClass().getSimpleName().hashCode();
    }

    public boolean a() {
        return true;
    }

    public void c() {
        i.a().n();
        a(true);
    }

    public void e() {
        a(false);
    }

    public void f() {
        Intent intent = new Intent("com.tencent.android.qqdownloader.action.SCHEDULE_JOB");
        intent.putExtra("com.tencent.android.qqdownloader.key.SCHEDULE_JOB", getClass().getName());
        try {
            ((AlarmManager) AstApp.i().getSystemService("alarm")).cancel(PendingIntent.getBroadcast(AstApp.i(), b(), intent, 268435456));
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public int[] g() {
        return null;
    }

    private int a(String str) {
        if (TextUtils.isEmpty(str) || str.length() != 9) {
            return 0;
        }
        if ('0' == str.charAt(0)) {
            return Integer.valueOf(str.substring(1, str.length())).intValue();
        }
        return Integer.valueOf(str).intValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.module.timer.job.AutoDownloadTimerJob.a(java.lang.String, boolean):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.pangu.module.timer.job.AutoDownloadTimerJob.a(boolean, long):long
      com.tencent.pangu.module.timer.job.AutoDownloadTimerJob.a(java.lang.String, boolean):long */
    private long a(boolean z, long j) {
        String str;
        boolean z2;
        Map<Integer, AutoDownloadPolicy> map;
        AutoDownloadPolicy autoDownloadPolicy;
        ArrayList<String> arrayList = null;
        AutoDownloadCfg j2 = com.tencent.assistant.manager.i.y().j();
        if (!(j2 == null || (map = j2.g) == null || (autoDownloadPolicy = map.get(2)) == null)) {
            arrayList = autoDownloadPolicy.a();
        }
        if (arrayList == null || arrayList.isEmpty()) {
            return 0;
        }
        long currentTimeMillis = System.currentTimeMillis();
        String str2 = (String) arrayList.get(0);
        Iterator it = arrayList.iterator();
        while (true) {
            if (!it.hasNext()) {
                str = str2;
                z2 = true;
                break;
            }
            str = (String) it.next();
            XLog.f("timeItem = " + str, "WiseDownloadTimePoints", true);
            if (currentTimeMillis < a(str, false)) {
                z2 = false;
                break;
            }
        }
        XLog.f("nextTime = " + str, "WiseDownloadTimePoints", true);
        return a(str, z2);
    }

    public long a(String str, boolean z) {
        int a2 = a(str);
        if (a2 == 0) {
            return 0;
        }
        Calendar c = z.c(a2);
        if (z) {
            c.set(6, c.get(6) + 1);
        }
        return c.getTimeInMillis();
    }

    private void a(boolean z) {
        long a2 = a(z, System.currentTimeMillis());
        if (a2 > 0) {
            try {
                Intent intent = new Intent("com.tencent.android.qqdownloader.action.SCHEDULE_JOB");
                intent.putExtra("com.tencent.android.qqdownloader.key.SCHEDULE_JOB", getClass().getName());
                ((AlarmManager) AstApp.i().getSystemService("alarm")).set(0, a2, PendingIntent.getBroadcast(AstApp.i(), b(), intent, 268435456));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
