package com.tencent.pangu.module.wisedownload;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class WiseDownloadService extends Service {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static i f3955a = null;

    public int onStartCommand(Intent intent, int i, int i2) {
        XLog.v("WiseDownloadService", "onStartCommand conditionControler == null: " + (f3955a == null));
        TemporaryThreadManager.get().start(new o(this));
        return 1;
    }

    public void onDestroy() {
        if (f3955a != null) {
            f3955a.b();
        }
        f3955a = null;
        super.onDestroy();
    }

    public void onLowMemory() {
        if (f3955a != null) {
            f3955a.b();
        }
        f3955a = null;
        super.onLowMemory();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
