package com.tencent.pangu.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.protocol.jce.CheckSelfUpdateResponse;
import com.tencent.pangu.manager.SelfUpdateManager;
import com.tencent.pangu.module.a.l;

/* compiled from: ProGuard */
class bd implements CallbackHelper.Caller<l> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CheckSelfUpdateResponse f3923a;
    final /* synthetic */ int b;
    final /* synthetic */ ax c;

    bd(ax axVar, CheckSelfUpdateResponse checkSelfUpdateResponse, int i) {
        this.c = axVar;
        this.f3923a = checkSelfUpdateResponse;
        this.b = i;
    }

    /* renamed from: a */
    public void call(l lVar) {
        SelfUpdateManager.SelfUpdateInfo a2 = this.c.a(this.f3923a);
        if (a2 != null) {
            SelfUpdateManager.a().a(a2);
            lVar.onCheckSelfUpdateFinish(this.b, 0, a2);
        }
    }
}
