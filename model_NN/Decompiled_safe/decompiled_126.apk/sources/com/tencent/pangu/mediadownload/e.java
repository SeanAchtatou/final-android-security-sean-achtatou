package com.tencent.pangu.mediadownload;

import android.os.Handler;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.table.n;
import com.tencent.assistant.event.EventDispatcher;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistantv2.st.l;
import com.tencent.downloadsdk.DownloadManager;
import com.tencent.downloadsdk.DownloadTask;
import com.tencent.downloadsdk.ac;
import com.tencent.downloadsdk.ae;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.download.k;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.model.AbstractDownloadInfo;
import com.tencent.pangu.model.a;
import com.tencent.pangu.model.d;
import com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class e {
    private static final Object e = new Object();
    private static e f;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Map<String, d> f3856a = new ConcurrentHashMap(5);
    /* access modifiers changed from: private */
    public n b = new n();
    /* access modifiers changed from: private */
    public EventDispatcher c = AstApp.i().j();
    /* access modifiers changed from: private */
    public InstallUninstallDialogManager d = new InstallUninstallDialogManager();
    private Handler g = k.a();
    private ae h = new j(this);

    private e() {
        this.d.a(true);
        try {
            List<d> a2 = this.b.a();
            ArrayList<ac> a3 = DownloadManager.a().a(100);
            for (d next : a2) {
                next.e();
                this.f3856a.put(next.m, next);
            }
            if (a3 != null) {
                Iterator<ac> it = a3.iterator();
                while (it.hasNext()) {
                    ac next2 = it.next();
                    d dVar = this.f3856a.get(next2.b);
                    if (dVar != null) {
                        if (dVar.u == null) {
                            dVar.u = new a();
                        }
                        dVar.u.b = next2.c;
                        dVar.u.f3881a = next2.d;
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public boolean a(d dVar) {
        this.g.post(new f(this, dVar));
        return true;
    }

    public boolean b(d dVar) {
        if (dVar == null || TextUtils.isEmpty(dVar.m)) {
            return false;
        }
        if (dVar.f() || dVar.d()) {
            this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_SUCC, dVar));
            d(dVar);
            return true;
        }
        String b2 = dVar.b();
        if (TextUtils.isEmpty(b2) || !b2.startsWith("/data/data/")) {
            ArrayList arrayList = new ArrayList(1);
            arrayList.add(dVar.l);
            DownloadTask downloadTask = new DownloadTask(100, dVar.m, 0, 0, dVar.b(), dVar.k, arrayList, null);
            ae a2 = l.a(dVar.m, 0, 0, (byte) dVar.a(), dVar.v, SimpleDownloadInfo.UIType.NORMAL, SimpleDownloadInfo.DownloadType.FILE);
            downloadTask.b = DownloadInfo.getPriority(SimpleDownloadInfo.DownloadType.FILE, SimpleDownloadInfo.UIType.NORMAL);
            dVar.q = downloadTask.d();
            downloadTask.a(this.h);
            downloadTask.a(a2);
            DownloadManager.a().a(downloadTask);
            boolean c2 = c(dVar);
            if (DownloadManager.a().c(dVar.a(), dVar.m) || DownloadProxy.a().h() < 2) {
                if (dVar.s != AbstractDownloadInfo.DownState.DOWNLOADING) {
                    dVar.s = AbstractDownloadInfo.DownState.DOWNLOADING;
                    this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_START, dVar));
                }
                this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOADING, dVar));
            } else {
                dVar.s = AbstractDownloadInfo.DownState.QUEUING;
                this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_QUEUING, dVar));
            }
            if (c2) {
                this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_ADD, dVar));
            }
            this.b.a(dVar);
            return true;
        }
        TemporaryThreadManager.get().start(new g(this));
        return false;
    }

    private boolean c(d dVar) {
        try {
            if (!this.f3856a.containsKey(dVar.m)) {
                this.f3856a.put(dVar.m, dVar);
                dVar.o = System.currentTimeMillis();
                return true;
            }
            if (this.f3856a.get(dVar.m) != dVar) {
                this.f3856a.put(dVar.m, dVar);
            }
            return false;
        } catch (NullPointerException e2) {
            return false;
        }
    }

    private void d(d dVar) {
        if (dVar != null && !TextUtils.isEmpty(dVar.m)) {
            this.f3856a.put(dVar.m, dVar);
            this.b.a(dVar);
        }
    }

    public void a(String str) {
        TemporaryThreadManager.get().start(new h(this, str));
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        d dVar;
        if (!TextUtils.isEmpty(str) && (dVar = this.f3856a.get(str)) != null) {
            DownloadManager.a().a(100, str);
            if (dVar.s == AbstractDownloadInfo.DownState.DOWNLOADING || dVar.s == AbstractDownloadInfo.DownState.QUEUING) {
                dVar.s = AbstractDownloadInfo.DownState.PAUSED;
                this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_PAUSE, dVar));
                this.b.a(dVar);
            }
        }
    }

    public boolean b(String str) {
        this.g.post(new i(this, str));
        return true;
    }

    public boolean a(String str, boolean z) {
        d remove;
        if (TextUtils.isEmpty(str) || (remove = this.f3856a.remove(str)) == null) {
            return false;
        }
        DownloadManager.a().b(100, str);
        this.b.a(str);
        if (z) {
            FileUtil.deleteFile(remove.r);
        }
        remove.s = AbstractDownloadInfo.DownState.DELETE;
        this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_DELETE, remove));
        return true;
    }

    public List<d> a() {
        return a(0);
    }

    private List<d> a(int i) {
        ArrayList arrayList = new ArrayList(this.f3856a.size());
        for (Map.Entry next : this.f3856a.entrySet()) {
            if (i == 0 || (i == 1 && (((d) next.getValue()).s == AbstractDownloadInfo.DownState.DOWNLOADING || ((d) next.getValue()).s == AbstractDownloadInfo.DownState.QUEUING))) {
                arrayList.add(next.getValue());
            }
        }
        return arrayList;
    }

    public List<d> b() {
        return a(1);
    }

    public static synchronized e c() {
        e eVar;
        synchronized (e.class) {
            if (f == null) {
                f = new e();
            }
            eVar = f;
        }
        return eVar;
    }

    public void d() {
        TemporaryThreadManager.get().start(new m(this));
    }

    public void e() {
        TemporaryThreadManager.get().start(new n(this));
    }

    public void f() {
        if (this.f3856a != null && this.f3856a.size() > 0) {
            for (Map.Entry<String, d> key : this.f3856a.entrySet()) {
                c((String) key.getKey());
            }
        }
    }

    public int g() {
        int i = 0;
        if (this.f3856a == null || this.f3856a.size() <= 0) {
            return 0;
        }
        Iterator<Map.Entry<String, d>> it = this.f3856a.entrySet().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            Map.Entry next = it.next();
            if (next.getValue() != null && (((d) next.getValue()).s == AbstractDownloadInfo.DownState.DOWNLOADING || ((d) next.getValue()).s == AbstractDownloadInfo.DownState.QUEUING)) {
                i2++;
            }
            i = i2;
        }
    }

    public d a(p pVar) {
        if (pVar == null || TextUtils.isEmpty(pVar.c())) {
            return null;
        }
        return this.f3856a.get(d.a(pVar.c(), pVar.b()));
    }
}
