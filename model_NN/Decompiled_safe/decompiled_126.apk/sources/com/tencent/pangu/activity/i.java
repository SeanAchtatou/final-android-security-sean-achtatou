package com.tencent.pangu.activity;

import android.text.TextUtils;
import com.tencent.android.qqdownloader.R;
import com.tencent.pangu.c.p;
import com.tencent.pangu.component.ShareAppDialog;
import com.tencent.pangu.component.appdetail.process.m;
import com.tencent.pangu.model.ShareAppModel;

/* compiled from: ProGuard */
class i implements m {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailActivityV5 f3383a;

    i(AppDetailActivityV5 appDetailActivityV5) {
        this.f3383a = appDetailActivityV5;
    }

    public void a(int i) {
        boolean z;
        p.a().a(this.f3383a.f());
        if (i == 5) {
            z = p.a().a(this.f3383a.f(), this.f3383a);
        } else {
            z = false;
        }
        if (!z && this.f3383a.ac.j() && 5 == i) {
            ShareAppModel unused = this.f3383a.a(this.f3383a.ae, this.f3383a.ac);
            if (TextUtils.isEmpty(this.f3383a.ad.l)) {
                this.f3383a.ad.l = this.f3383a.getString(R.string.share_specail_app_default_local_content);
            }
            ShareAppDialog a2 = p.a().a(this.f3383a, this.f3383a.ad, false, 0, R.string.dialog_share_to_friend);
            if (a2 != null) {
                a2.setOnCancelListener(this.f3383a.bn);
            }
        }
        if (1 == i || 3 == i) {
            a();
        } else if (2 == i || 4 == i) {
            b();
        }
    }

    public void a() {
        ShareAppModel unused = this.f3383a.a(this.f3383a.ae, this.f3383a.ac);
        if (TextUtils.isEmpty(this.f3383a.ad.l)) {
            this.f3383a.ad.l = this.f3383a.getString(R.string.share_bate_default_local_content);
        }
        ShareAppDialog a2 = p.a().a(this.f3383a, this.f3383a.ad, true, R.drawable.yyb_mascot_celebrate, R.string.share_get_ticket);
        if (a2 != null) {
            a2.setOnCancelListener(this.f3383a.bn);
        }
    }

    public void b() {
        ShareAppModel unused = this.f3383a.a(this.f3383a.ae, this.f3383a.ac);
        if (TextUtils.isEmpty(this.f3383a.ad.l)) {
            this.f3383a.ad.l = this.f3383a.getString(R.string.share_take_num_default_local_content);
        }
        ShareAppDialog a2 = p.a().a(this.f3383a, this.f3383a.ad, true, R.drawable.yyb_mascot_celebrate, R.string.share_get_num);
        if (a2 != null) {
            a2.setOnCancelListener(this.f3383a.bn);
            a2.setCanceledOnTouchOutside(true);
        }
    }
}
