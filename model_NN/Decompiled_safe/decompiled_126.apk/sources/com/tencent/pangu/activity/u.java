package com.tencent.pangu.activity;

import android.text.TextUtils;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.AppHotFriend;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistant.protocol.jce.GetAppTagInfoListResponse;
import com.tencent.assistant.protocol.jce.GetRecommendAppListResponse;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.component.appdetail.AppDetailViewV5;
import com.tencent.pangu.module.a.a;
import com.tencent.pangu.module.a.c;
import com.tencent.pangu.module.a.d;
import com.tencent.pangu.module.a.e;
import com.tencent.pangu.module.a.i;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
class u implements a, c, d, e, i {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailActivityV5 f3395a;

    private u(AppDetailActivityV5 appDetailActivityV5) {
        this.f3395a = appDetailActivityV5;
    }

    /* synthetic */ u(AppDetailActivityV5 appDetailActivityV5, a aVar) {
        this(appDetailActivityV5);
    }

    public void a(int i, int i2, com.tencent.assistant.model.c cVar, int i3) {
        this.f3395a.a(i, i2, cVar, i3);
    }

    public void a(int i, int i2, boolean z, List<SimpleAppModel> list, boolean z2, byte[] bArr) {
        if ((this.f3395a.aV & 2) == 0) {
            this.f3395a.a(i, i2, z, list, z2, bArr);
        }
    }

    public void a(int i, int i2, String str, GetRecommendAppListResponse getRecommendAppListResponse) {
        if (this.f3395a.w[1] == i && (this.f3395a.aV & 1) == 0) {
            this.f3395a.a(i2, getRecommendAppListResponse);
        }
    }

    public void a(int i, int i2, String str, ArrayList<AppHotFriend> arrayList) {
        if (i2 != 0 || i != this.f3395a.w[7]) {
            return;
        }
        if (this.f3395a.aW) {
            this.f3395a.I.a(str, arrayList);
            return;
        }
        HashMap hashMap = new HashMap();
        this.f3395a.v[4] = new ViewInvalidateMessage(4);
        hashMap.put("hasNext", str);
        hashMap.put("pageContext", arrayList);
        this.f3395a.v[4].params = hashMap;
    }

    public void a(int i, int i2, ArrayList<CommentDetail> arrayList, ArrayList<CommentTagInfo> arrayList2, ArrayList<CommentTagInfo> arrayList3) {
        if (i2 == 0 && i == this.f3395a.w[7]) {
            this.f3395a.I.b().a(this.f3395a.bc);
            this.f3395a.y.clear();
            this.f3395a.y.addAll(arrayList3);
            if (this.f3395a.aW) {
                this.f3395a.I.b().a(arrayList, arrayList2);
                return;
            }
            this.f3395a.v[5] = new ViewInvalidateMessage(5);
            HashMap hashMap = new HashMap();
            hashMap.put("selectedCommentList", arrayList);
            hashMap.put("goodTagList", arrayList2);
            this.f3395a.v[5].params = hashMap;
        }
    }

    public void a(int i, int i2, GetAppTagInfoListResponse getAppTagInfoListResponse) {
        if (i2 == 0 && i == this.f3395a.w[6] && getAppTagInfoListResponse != null) {
            if (this.f3395a.ao) {
                getAppTagInfoListResponse.c.clear();
            }
            if (this.f3395a.aW) {
                this.f3395a.I.b().a(getAppTagInfoListResponse.b, this.f3395a.ac.f938a, this.f3395a.ac.c);
                if (getAppTagInfoListResponse.c.size() >= 2) {
                    this.f3395a.J.a(getAppTagInfoListResponse.e, getAppTagInfoListResponse.c, this.f3395a.aV);
                    return;
                }
                return;
            }
            this.f3395a.v[6] = new ViewInvalidateMessage(6);
            HashMap hashMap = new HashMap();
            hashMap.put("hasNext", getAppTagInfoListResponse.b);
            hashMap.put("pageContext", getAppTagInfoListResponse.c);
            hashMap.put("pageText", getAppTagInfoListResponse.e);
            this.f3395a.v[6].params = hashMap;
        }
    }

    public void a(int i, int i2, int i3, int i4) {
        String str;
        if (i2 != 0 || i != this.f3395a.w[7] || i3 != 0) {
            return;
        }
        if (!this.f3395a.aW) {
            this.f3395a.v[7] = new ViewInvalidateMessage(7);
            this.f3395a.v[7].params = Integer.valueOf(i4);
        } else if (AppDetailActivityV5.a(this.f3395a.ae) && TextUtils.isEmpty(this.f3395a.ae.f941a.f1149a.f1144a.v)) {
            if (AppDetailViewV5.a(this.f3395a.ae, this.f3395a.ac) == AppDetailViewV5.APPDETAIL_MODE.NEED_UPDATE && this.f3395a.ae.f941a.f1149a.b.get(0).F > 10000) {
                BigDecimal scale = new BigDecimal(((double) this.f3395a.ae.f941a.f1149a.b.get(0).F) / 10000.0d).setScale(1, 4);
                str = String.format(this.f3395a.getResources().getString(R.string.appdetail_friends_update), scale);
            } else if (i4 > 0) {
                str = String.format(this.f3395a.getResources().getString(R.string.appdetail_friends_using), Integer.valueOf(i4));
            } else {
                str = Constants.STR_EMPTY;
            }
            this.f3395a.G.a(str);
        }
    }
}
