package com.tencent.pangu.activity;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class bo extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopUpNecessaryAcitivity f3339a;

    bo(PopUpNecessaryAcitivity popUpNecessaryAcitivity) {
        this.f3339a = popUpNecessaryAcitivity;
    }

    public STInfoV2 getStInfo(View view) {
        return new STInfoV2(STConst.ST_PAGE_POP_UP_NECESSRAY_CONTENT_VEIW + this.f3339a.E, "04_001", 2000, STConst.ST_DEFAULT_SLOT, 200);
    }

    public void onTMAClick(View view) {
        this.f3339a.y();
    }
}
