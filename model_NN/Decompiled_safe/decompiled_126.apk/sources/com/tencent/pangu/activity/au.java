package com.tencent.pangu.activity;

import android.content.Intent;
import android.view.View;
import com.tencent.assistantv2.activity.MainActivity;

/* compiled from: ProGuard */
class au implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ExternalCallActivity f3320a;

    au(ExternalCallActivity externalCallActivity) {
        this.f3320a = externalCallActivity;
    }

    public void onClick(View view) {
        this.f3320a.n.startActivity(new Intent(this.f3320a.n, MainActivity.class));
        if (view.equals(this.f3320a.E)) {
            this.f3320a.a((String) null, 200, 1);
        } else {
            this.f3320a.a((String) null, 200, 2);
        }
    }
}
