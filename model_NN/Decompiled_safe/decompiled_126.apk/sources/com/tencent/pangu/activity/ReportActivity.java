package com.tencent.pangu.activity;

import android.os.Bundle;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.pangu.module.a.b;
import com.tencent.pangu.module.e;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class ReportActivity extends BaseActivity implements b {
    /* access modifiers changed from: private */
    public TextView A;
    /* access modifiers changed from: private */
    public TextView B;
    /* access modifiers changed from: private */
    public TextView C;
    /* access modifiers changed from: private */
    public EditText D;
    /* access modifiers changed from: private */
    public long E;
    /* access modifiers changed from: private */
    public long F;
    /* access modifiers changed from: private */
    public ScrollView G;
    /* access modifiers changed from: private */
    public e H = new e();
    private View.OnClickListener I = new bt(this);
    TextWatcher n = new bs(this);
    /* access modifiers changed from: private */
    public TextView u;
    /* access modifiers changed from: private */
    public TextView v;
    /* access modifiers changed from: private */
    public TextView w;
    /* access modifiers changed from: private */
    public TextView x;
    /* access modifiers changed from: private */
    public TextView y;
    /* access modifiers changed from: private */
    public TextView z;

    public int f() {
        return STConst.ST_PAGE_APP_DETAIL_MORE_REPORT;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.report_view);
        this.H.register(this);
        t();
        u();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.H.unregister(this);
        super.onDestroy();
    }

    private void t() {
        this.E = getIntent().getLongExtra("appid", 0);
        this.F = getIntent().getLongExtra("apkid", 0);
    }

    private void u() {
        SecondNavigationTitleViewV5 secondNavigationTitleViewV5 = (SecondNavigationTitleViewV5) findViewById(R.id.header);
        secondNavigationTitleViewV5.a(this);
        secondNavigationTitleViewV5.d();
        secondNavigationTitleViewV5.b(getString(R.string.inform));
        this.G = (ScrollView) findViewById(R.id.scroller);
        this.G.getViewTreeObserver().addOnGlobalLayoutListener(new bq(this));
        this.u = (TextView) findViewById(R.id.submit);
        this.u.setOnClickListener(this.I);
        this.v = (TextView) findViewById(R.id.checkbox_downlaod);
        this.w = (TextView) findViewById(R.id.checkbox_install);
        this.x = (TextView) findViewById(R.id.checkbox_version);
        this.y = (TextView) findViewById(R.id.checkbox_plugin);
        this.z = (TextView) findViewById(R.id.checkbox_fee);
        this.A = (TextView) findViewById(R.id.checkbox_virus);
        this.B = (TextView) findViewById(R.id.checkbox_right);
        this.C = (TextView) findViewById(R.id.checkbox_private);
        this.v.setOnClickListener(this.I);
        this.w.setOnClickListener(this.I);
        this.x.setOnClickListener(this.I);
        this.z.setOnClickListener(this.I);
        this.y.setOnClickListener(this.I);
        this.A.setOnClickListener(this.I);
        this.B.setOnClickListener(this.I);
        this.C.setOnClickListener(this.I);
        this.D = (EditText) findViewById(R.id.content);
        this.D.addTextChangedListener(this.n);
    }

    /* access modifiers changed from: private */
    public boolean v() {
        return this.v.isSelected() || this.w.isSelected() || this.x.isSelected() || this.z.isSelected() || this.y.isSelected() || this.A.isSelected() || this.B.isSelected() || this.C.isSelected() || this.D.getText().toString().length() != 0;
    }

    /* access modifiers changed from: private */
    public void w() {
        if (this.z.isSelected() || this.y.isSelected() || this.A.isSelected() || this.C.isSelected()) {
            this.w.setEnabled(false);
            this.v.setEnabled(false);
            this.w.setSelected(false);
            this.v.setSelected(false);
            this.u.setEnabled(true);
            return;
        }
        this.w.setEnabled(true);
        this.v.setEnabled(true);
    }

    /* access modifiers changed from: private */
    public byte[] x() {
        ArrayList arrayList = new ArrayList();
        if (this.v.isSelected()) {
            arrayList.add((byte) 1);
        }
        if (this.w.isSelected()) {
            arrayList.add((byte) 2);
        }
        if (this.x.isSelected()) {
            arrayList.add((byte) 5);
        }
        if (this.y.isSelected()) {
            arrayList.add((byte) 8);
        }
        if (this.z.isSelected()) {
            arrayList.add((byte) 6);
        }
        if (this.x.isSelected()) {
            arrayList.add((byte) 5);
        }
        if (this.B.isSelected()) {
            arrayList.add((byte) 9);
        }
        if (this.C.isSelected()) {
            arrayList.add((byte) 10);
        }
        byte[] bArr = new byte[arrayList.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= arrayList.size()) {
                return bArr;
            }
            bArr[i2] = ((Byte) arrayList.get(i2)).byteValue();
            i = i2 + 1;
        }
    }

    public void a(int i, int i2, long j, long j2) {
        if (i2 == 0 && this.E == j && this.F == j2) {
            Toast.makeText(this, getString(R.string.report_success), 0).show();
        } else if (i2 == 1) {
            Toast.makeText(this, getString(R.string.repeat_report), 0).show();
        } else {
            Toast.makeText(this, getString(R.string.report_fail), 0).show();
        }
    }
}
