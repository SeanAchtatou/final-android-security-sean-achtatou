package com.tencent.pangu.activity;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.pangu.component.search.SearchResultTabPagesBase;

/* compiled from: ProGuard */
class ch implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchResultTabPagesBase.TabType f3359a;
    final /* synthetic */ String b;
    final /* synthetic */ SimpleAppModel c;
    final /* synthetic */ SearchActivity d;

    ch(SearchActivity searchActivity, SearchResultTabPagesBase.TabType tabType, String str, SimpleAppModel simpleAppModel) {
        this.d = searchActivity;
        this.f3359a = tabType;
        this.b = str;
        this.c = simpleAppModel;
    }

    public void run() {
        try {
            if (this.f3359a != SearchResultTabPagesBase.TabType.NONE) {
                this.d.z.a(this.f3359a, this.b, this.d.N, this.c);
            } else {
                this.d.z.a(this.b, this.d.N, this.c);
            }
        } catch (Throwable th) {
        }
    }
}
