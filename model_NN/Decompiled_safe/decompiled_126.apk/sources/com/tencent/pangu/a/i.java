package com.tencent.pangu.a;

import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class i implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BaseActivity f3295a;
    final /* synthetic */ h b;

    i(h hVar, BaseActivity baseActivity) {
        this.b = hVar;
        this.f3295a = baseActivity;
    }

    public void onClick(View view) {
        this.b.c();
        this.b.e.setVisibility(8);
        this.b.f.setVisibility(8);
        l.a(new STInfoV2(this.f3295a.f(), "13_001", this.f3295a.m(), STConst.ST_DEFAULT_SLOT, 200));
    }
}
