package com.tencent.pangu.manager;

import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.h;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;

/* compiled from: ProGuard */
class aq implements h {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ak f3799a;

    aq(ak akVar) {
        this.f3799a = akVar;
    }

    public void onGetAppInfoFail(int i, int i2) {
    }

    public void onGetAppInfoSuccess(int i, int i2, AppSimpleDetail appSimpleDetail) {
        DownloadInfo downloadInfo = null;
        SimpleAppModel a2 = k.a(appSimpleDetail);
        DownloadInfo a3 = DownloadProxy.a().a(a2);
        StatInfo statInfo = new StatInfo(a2.b, STConst.ST_PAGE_APP_DETAIL, 0, null, 0);
        if (a3 == null || !a3.needReCreateInfo(a2)) {
            downloadInfo = a3;
        } else {
            DownloadProxy.a().b(a3.downloadTicket);
        }
        if (downloadInfo == null) {
            DownloadInfo createDownloadInfo = DownloadInfo.createDownloadInfo(a2, statInfo);
            DownloadProxy.a().d(createDownloadInfo);
            SimpleDownloadInfo.DownloadState c = ak.c();
            if (SimpleDownloadInfo.DownloadState.DOWNLOADING == c || SimpleDownloadInfo.DownloadState.QUEUING == c || SimpleDownloadInfo.DownloadState.INSTALLING == c) {
                this.f3799a.a((int) R.string.qube_wait_qube_install, createDownloadInfo);
            } else {
                this.f3799a.a(createDownloadInfo, true, 2);
            }
        }
    }
}
