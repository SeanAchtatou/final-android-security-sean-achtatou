package com.tencent.pangu.manager;

import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.protocol.jce.AdviceApp;
import com.tencent.assistant.st.STConst;

/* compiled from: ProGuard */
class aw implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.DialogInfo f3805a;
    final /* synthetic */ bj b;
    final /* synthetic */ au c;

    aw(au auVar, AppConst.DialogInfo dialogInfo, bj bjVar) {
        this.c = auVar;
        this.f3805a = dialogInfo;
        this.b = bjVar;
    }

    public void run() {
        try {
            if (AstApp.m() != null) {
                if (this.f3805a instanceof AppConst.TwoBtnDialogInfo) {
                    DialogUtils.show2BtnDialog((AppConst.TwoBtnDialogInfo) this.f3805a);
                }
                int f = AstApp.m().f();
                AdviceApp b2 = this.b.b();
                this.c.a(this.f3805a.pageId, f, STConst.ST_DEFAULT_SLOT, 100, b2 != null ? b2.i() : null);
                au.a("rec_pop_window_exp", this.b.e);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
