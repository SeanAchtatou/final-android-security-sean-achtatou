package com.tencent.pangu.manager;

import android.text.TextUtils;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;

/* compiled from: ProGuard */
class v {

    /* renamed from: a  reason: collision with root package name */
    DownloadInfo f3845a;
    SimpleDownloadInfo.DownloadState b;
    final /* synthetic */ DownloadProxy c;

    public v(DownloadProxy downloadProxy, DownloadInfo downloadInfo, SimpleDownloadInfo.DownloadState downloadState) {
        this.c = downloadProxy;
        this.f3845a = downloadInfo;
        this.b = downloadState;
    }

    public boolean a(v vVar) {
        return b(this) && b(vVar) && this.f3845a.packageName.equals(vVar.f3845a.packageName);
    }

    private boolean b(v vVar) {
        if (vVar == null || vVar.f3845a == null || !TextUtils.isEmpty(vVar.f3845a.packageName)) {
            return false;
        }
        return true;
    }
}
