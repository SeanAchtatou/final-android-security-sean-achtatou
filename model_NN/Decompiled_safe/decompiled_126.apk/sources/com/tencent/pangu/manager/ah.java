package com.tencent.pangu.manager;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class ah extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Dialog f3791a;
    final /* synthetic */ Activity b;
    final /* synthetic */ ac c;

    ah(ac acVar, Dialog dialog, Activity activity) {
        this.c = acVar;
        this.f3791a = dialog;
        this.b = activity;
    }

    public void onTMAClick(View view) {
        this.f3791a.dismiss();
        this.c.f();
    }

    public STInfoV2 getStInfo() {
        STInfoV2 a2 = this.c.c(this.b);
        a2.slotId = "03_001";
        a2.actionId = 305;
        return a2;
    }
}
