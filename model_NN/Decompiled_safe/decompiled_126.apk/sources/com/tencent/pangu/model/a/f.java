package com.tencent.pangu.model.a;

import android.text.TextUtils;
import com.tencent.assistant.protocol.jce.AdvancedHotWord;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/* compiled from: ProGuard */
public class f extends d {

    /* renamed from: a  reason: collision with root package name */
    public int f3885a = -1;
    public String b;
    public String c;
    public String d;
    public List<AdvancedHotWord> e = new ArrayList();
    private final String f = "SearchHotwordsModel";
    private final int g = 9;
    private final int h = -1;

    public void a() {
        if (f() > 0) {
            this.f3885a = new Random().nextInt(f()) - 1;
        } else {
            this.f3885a = -1;
        }
    }

    public void a(AdvancedHotWord advancedHotWord) {
        if (advancedHotWord != null) {
            if (this.e == null) {
                this.e = new ArrayList();
            }
            this.e.add(advancedHotWord);
        }
    }

    public int a(String str) {
        int i = 0;
        if (TextUtils.isEmpty(str) || this.e == null || this.e.size() <= 0) {
            return 0;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.e);
        Iterator<AdvancedHotWord> it = this.e.iterator();
        while (true) {
            int i2 = i;
            if (it.hasNext()) {
                AdvancedHotWord next = it.next();
                if (!(next == null || next.d == null || next.d.f == null || !str.equals(next.d.f.f))) {
                    arrayList.remove(next);
                    i2++;
                }
                i = i2;
            } else {
                this.e = arrayList;
                return i2;
            }
        }
    }

    public List<AdvancedHotWord> c() {
        if (f() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        this.f3885a = (this.f3885a + 1) % f();
        int i = this.f3885a * 9;
        for (int i2 = 0; i2 < 9; i2++) {
            arrayList.add(this.e.get(i + i2));
        }
        XLog.d("SearchHotwordsModel", "getNextPage:page_index:" + this.f3885a);
        return arrayList;
    }

    public int d() {
        return this.f3885a;
    }

    public int e() {
        return 9;
    }

    public void a(d dVar) {
        if (dVar != null) {
            this.f3885a = -1;
            if (dVar instanceof f) {
                f fVar = (f) dVar;
                if (fVar != null && fVar.e != null) {
                    if (this.e == null) {
                        this.e = fVar.e;
                    } else {
                        this.e.addAll(fVar.e);
                    }
                }
            } else {
                XLog.d("SearchHotwordsModel", "model is not SearchHotwordsModel instance!");
            }
        }
    }

    public int b() {
        if (this.e == null) {
            return 0;
        }
        return this.e.size();
    }

    public int f() {
        int size = this.e == null ? 0 : this.e.size() / 9;
        if (size > 3) {
            return 3;
        }
        return size;
    }
}
