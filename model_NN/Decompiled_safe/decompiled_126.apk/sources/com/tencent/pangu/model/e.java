package com.tencent.pangu.model;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
final class e implements Parcelable.Creator<ShareAppModel> {
    e() {
    }

    /* renamed from: a */
    public ShareAppModel createFromParcel(Parcel parcel) {
        ShareAppModel shareAppModel = new ShareAppModel();
        shareAppModel.f3879a = parcel.readString();
        shareAppModel.b = parcel.readString();
        shareAppModel.c = parcel.readBundle();
        shareAppModel.d = parcel.readInt();
        shareAppModel.e = parcel.readLong();
        shareAppModel.f = parcel.readString();
        shareAppModel.g = parcel.readString();
        shareAppModel.h = parcel.readDouble();
        shareAppModel.i = parcel.readLong();
        shareAppModel.j = parcel.readLong();
        shareAppModel.k = parcel.readString();
        shareAppModel.l = parcel.readString();
        shareAppModel.m = parcel.readString();
        return shareAppModel;
    }

    /* renamed from: a */
    public ShareAppModel[] newArray(int i) {
        return null;
    }
}
