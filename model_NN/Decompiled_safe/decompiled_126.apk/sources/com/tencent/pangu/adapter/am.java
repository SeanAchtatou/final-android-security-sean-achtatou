package com.tencent.pangu.adapter;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class am extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f3415a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ al c;

    am(al alVar, SimpleAppModel simpleAppModel, STInfoV2 sTInfoV2) {
        this.c = alVar;
        this.f3415a = simpleAppModel;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.c.e, AppDetailActivityV5.class);
        intent.putExtra("simpleModeInfo", this.f3415a);
        this.c.e.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (this.b != null) {
            this.b.actionId = 200;
            this.b.status = "01";
        }
        return this.b;
    }
}
