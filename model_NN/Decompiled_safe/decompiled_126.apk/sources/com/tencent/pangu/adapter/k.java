package com.tencent.pangu.adapter;

import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.GetRecommendAppListResponse;
import com.tencent.assistant.protocol.jce.RecommendAppInfo;
import com.tencent.assistant.protocol.jce.SmartCardHotWords;
import com.tencent.pangu.module.a.i;
import java.util.ArrayList;

/* compiled from: ProGuard */
class k implements i {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfoMultiAdapter f3452a;

    k(DownloadInfoMultiAdapter downloadInfoMultiAdapter) {
        this.f3452a = downloadInfoMultiAdapter;
    }

    public void a(int i, int i2, String str, GetRecommendAppListResponse getRecommendAppListResponse) {
        if (this.f3452a.u != null) {
            ArrayList<RecommendAppInfo> arrayList = getRecommendAppListResponse.b;
            if (arrayList != null && !arrayList.isEmpty()) {
                ArrayList b = this.f3452a.b(this.f3452a.a(arrayList));
                if (b.size() >= 3) {
                    this.f3452a.u.a().a(2, b, getRecommendAppListResponse.d);
                    this.f3452a.u.a().setVisibility(0);
                }
            }
            if (getRecommendAppListResponse.b() != null) {
                SmartCardHotWords b2 = getRecommendAppListResponse.b();
                if (b2 == null || b2.a() == null || b2.a().size() < 5) {
                    this.f3452a.u.a(8);
                    return;
                }
                this.f3452a.u.a(b2.f1508a, this.f3452a.m.getResources().getString(R.string.hotwords_card_more_text), b2.c, b2.a());
                this.f3452a.u.a(0);
            }
        }
    }
}
