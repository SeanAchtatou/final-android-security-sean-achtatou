package com.tencent.beacon.f;

import android.content.Context;

/* compiled from: ProGuard */
public abstract class f {

    /* renamed from: a  reason: collision with root package name */
    private static f f2151a = null;

    public abstract byte[] a(String str, byte[] bArr, e eVar, b bVar);

    public static synchronized f a(Context context) {
        f fVar;
        synchronized (f.class) {
            if (f2151a == null && context != null) {
                f2151a = new g(context);
            }
            fVar = f2151a;
        }
        return fVar;
    }
}
