package com.tencent.beacon.a;

/* compiled from: ProGuard */
public abstract class d {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f2098a = true;
    private static d b;

    public abstract void a(int i, Runnable runnable, long j, long j2);

    public abstract void a(int i, boolean z);

    public abstract void a(Runnable runnable);

    public abstract void a(Runnable runnable, long j);

    public static synchronized d a() {
        d dVar;
        synchronized (d.class) {
            if (b == null) {
                b = new e();
            }
            dVar = b;
        }
        return dVar;
    }
}
