package com.tencent.wcs.agent;

import com.qq.taf.jce.JceInputStream;
import com.qq.util.k;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.wcs.agent.config.a;
import com.tencent.wcs.b.c;
import com.tencent.wcs.jce.RecvPacketSeqList;
import com.tencent.wcs.jce.TransDataMessage;
import com.tencent.wcs.proxy.b.b;
import com.tencent.wcs.proxy.b.t;
import com.tencent.wcs.proxy.e.d;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.concurrent.PriorityBlockingQueue;

/* compiled from: ProGuard */
public class l {

    /* renamed from: a  reason: collision with root package name */
    private Socket f4057a;
    private InputStream b;
    private OutputStream c;
    private Runnable d;
    private Runnable e;
    private volatile boolean f = false;
    /* access modifiers changed from: private */
    public volatile boolean g = false;
    private final Object h = new Object();
    private final Object i = new Object();
    private final Object j = new Object();
    /* access modifiers changed from: private */
    public final a k;
    private final com.tencent.wcs.proxy.l l;
    /* access modifiers changed from: private */
    public final int m;
    /* access modifiers changed from: private */
    public final int n;
    private final String o;
    private final String p;
    /* access modifiers changed from: private */
    public final int q;
    private PriorityBlockingQueue<TransDataMessage> r;
    private volatile int s;
    private volatile int t;
    /* access modifiers changed from: private */
    public p u;
    private volatile r v;

    static /* synthetic */ int h(l lVar) {
        int i2 = lVar.t;
        lVar.t = i2 + 1;
        return i2;
    }

    public l(int i2, int i3, String str, String str2, com.tencent.wcs.proxy.l lVar, a aVar) {
        this.m = i2;
        this.n = i3;
        this.o = str;
        this.p = str2;
        this.r = new PriorityBlockingQueue<>(c.e + 10, new m(this));
        this.l = lVar;
        this.k = aVar;
        this.v = new r(this);
        this.q = this.k.a(i2);
    }

    public void a() {
        if (!this.f) {
            synchronized (this.j) {
                try {
                    InetSocketAddress inetSocketAddress = new InetSocketAddress("127.0.0.1", this.q);
                    this.f4057a = new Socket();
                    this.f4057a.setTcpNoDelay(true);
                    this.f4057a.setKeepAlive(false);
                    this.f4057a.connect(inetSocketAddress, EventDispatcherEnum.CACHE_EVENT_START);
                    if (this.f4057a != null) {
                        try {
                            synchronized (this.i) {
                                this.b = this.f4057a.getInputStream();
                            }
                            synchronized (this.h) {
                                this.c = this.f4057a.getOutputStream();
                            }
                        } catch (IOException e2) {
                            e2.printStackTrace();
                            return;
                        }
                    }
                    this.s = 0;
                    this.t = 0;
                    this.f = true;
                    this.e = new n(this);
                    this.d = new o(this);
                    b.a().a(this.e);
                    b.a().a(this.d);
                } catch (UnknownHostException e3) {
                    e3.printStackTrace();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            }
        }
    }

    public void b() {
        if (this.f) {
            this.f = false;
            if (this.v != null) {
                this.v.h();
            }
            synchronized (this.j) {
                synchronized (this.i) {
                    try {
                        if (this.b != null) {
                            this.b.close();
                            this.b = null;
                        }
                    } catch (IOException e2) {
                    }
                }
                synchronized (this.h) {
                    try {
                        if (this.c != null) {
                            this.c.close();
                            this.c = null;
                        }
                    } catch (IOException e3) {
                    }
                }
                try {
                    if (this.f4057a != null) {
                        this.f4057a.close();
                        this.f4057a = null;
                    }
                } catch (IOException e4) {
                }
                if (this.e != null) {
                    this.e = null;
                }
                if (this.d != null) {
                    this.d = null;
                }
                if (this.r != null) {
                    if (!this.r.isEmpty()) {
                        this.r.clear();
                    }
                    this.r = null;
                }
                this.g = false;
            }
            this.s = 0;
            this.t = 0;
        }
    }

    public void a(p pVar) {
        synchronized (this.j) {
            this.u = pVar;
        }
    }

    public boolean c() {
        return this.f;
    }

    public void a(TransDataMessage transDataMessage) {
        if (c() && this.r != null) {
            this.r.put(transDataMessage);
            if (this.v != null) {
                this.v.a();
            }
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        while (this.f) {
            if (this.r != null) {
                TransDataMessage peek = this.r.peek();
                if (peek != null && peek.f() == this.s) {
                    if (this.v != null) {
                        this.v.e();
                    }
                    TransDataMessage poll = this.r.poll();
                    if (poll == null) {
                        continue;
                    } else {
                        int g2 = poll.g();
                        if (g2 == 1) {
                            this.s++;
                            t.b().b((Object) poll);
                        } else if (g2 == 0) {
                            synchronized (this.h) {
                                if (!(this.c == null || poll.d() == null)) {
                                    this.c.write(poll.d());
                                    this.c.flush();
                                }
                            }
                            if (!(this.v == null || poll.d() == null)) {
                                this.v.g();
                                this.v.a(this.s, poll.d().length);
                            }
                            this.s++;
                            t.b().b((Object) poll);
                        } else if (g2 == 5) {
                            if (this.v != null) {
                                this.v.f();
                            }
                            t.b().b((Object) poll);
                        } else if (g2 == 2) {
                            if (c.f4064a) {
                                com.tencent.wcs.c.b.a("AgentSessionOriginal server calling close channelId " + this.m + " messageId " + this.n + " port " + this.q + " remote " + this.k.a() + ", close it now");
                            }
                            this.g = true;
                            if (this.u != null) {
                                this.u.a(this.n);
                            }
                            t.b().b((Object) poll);
                            return;
                        }
                    }
                } else if (peek == null || peek.f() >= this.s) {
                    if (!(peek == null || this.v == null || !this.v.c())) {
                        if (this.v.d()) {
                            com.tencent.wcs.c.b.a("AgentSessionOriginal miss at " + this.s + ", for too many times, " + " channelId " + this.m + " messageId " + this.n + " port " + this.q + " remote " + this.k.a() + ", close it now");
                            synchronized (this) {
                                int i2 = this.t;
                                this.t = i2 + 1;
                                a(null, i2, 2);
                            }
                            if (this.u != null) {
                                this.u.a(this.n);
                                return;
                            }
                            return;
                        }
                        if (c.f4064a) {
                            com.tencent.wcs.c.b.a("AgentSessionOriginal expect " + this.s + ", but got " + peek.f() + " channelId " + this.m + " messageId " + this.n + " port " + this.q + " type " + peek.g() + " remote " + this.k.a() + ", after count overflow");
                        }
                        a(this.s, this.r);
                    }
                    if (this.v != null && this.v.b()) {
                        if (this.r != null) {
                            peek = this.r.peek();
                        }
                        if (peek == null || peek.f() != this.s) {
                            a(this.s, this.r);
                        }
                        if (c.f4064a) {
                            if (peek != null) {
                                com.tencent.wcs.c.b.a("AgentSessionOriginal expect " + this.s + ", but got " + peek.f() + " channelId " + this.m + " messageId " + this.n + " port " + this.q + " type " + peek.g() + " remote " + this.k.a() + ", after time delay");
                            } else {
                                com.tencent.wcs.c.b.a("AgentSessionOriginal expect " + this.s + ", but got null " + " channel id " + this.m + " messageId " + this.n + " port " + this.q + " remote " + this.k.a() + ", after time delay");
                            }
                        }
                    }
                } else {
                    t.b().b((Object) this.r.poll());
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        int read;
        if (this.f) {
            byte[] bArr = new byte[49152];
            while (this.b != null && (read = this.b.read(bArr)) != -1) {
                byte[] bArr2 = new byte[read];
                System.arraycopy(bArr, 0, bArr2, 0, read);
                a(bArr2, this.t, 0);
                if (this.v != null) {
                    this.v.a(this.t, bArr2);
                }
                this.t++;
            }
            if (!this.g) {
                if (c.f4064a) {
                    com.tencent.wcs.c.b.a("AgentSessionOriginal read input closed channelId " + this.m + " messageId " + this.n + " port " + this.q + " remote " + this.k.a() + ", close it now");
                }
                synchronized (this) {
                    int i2 = this.t;
                    this.t = i2 + 1;
                    a(null, i2, 2);
                }
                if (this.u != null) {
                    this.u.a(this.n);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        if (this.f) {
            byte[] bArr = new byte[49152];
            boolean z = false;
            do {
                int i2 = 0;
                while (true) {
                    if (i2 >= 49152) {
                        break;
                    }
                    int length = bArr.length - i2;
                    if (this.b == null) {
                        z = true;
                        break;
                    }
                    int read = this.b.read(bArr, i2, length);
                    if (read > 0) {
                        i2 += read;
                    }
                    if (read == -1) {
                        z = true;
                        break;
                    }
                }
                byte[] bArr2 = new byte[i2];
                System.arraycopy(bArr, 0, bArr2, 0, i2);
                a(bArr2, this.t, 0);
                if (this.v != null) {
                    this.v.a(this.t, bArr2);
                }
                this.t++;
            } while (!z);
            if (!this.g) {
                synchronized (this) {
                    int i3 = this.t;
                    this.t = i3 + 1;
                    a(null, i3, 2);
                }
                if (this.u != null) {
                    this.u.a(this.n);
                }
            }
        }
    }

    public void a(byte[] bArr, int i2, int i3) {
        TransDataMessage b2 = b(bArr, i2, i3);
        if (this.l != null) {
            if (c.f4064a) {
                com.tencent.wcs.c.b.a("AgentSessionOriginal echo data type " + i3 + " , at seq " + b2.f() + " channel id " + b2.a() + " message id " + b2.e() + " size " + (b2.d() != null ? b2.d().length : 0) + " remote " + this.k.a());
            }
            this.l.a(this.k.c(), b2);
        }
    }

    private TransDataMessage b(byte[] bArr, int i2, int i3) {
        TransDataMessage transDataMessage = (TransDataMessage) t.b().c((Object[]) new Void[0]);
        transDataMessage.f4106a = this.m;
        transDataMessage.e = this.n;
        transDataMessage.b = this.p;
        transDataMessage.c = this.o;
        if (bArr == null || bArr.length <= c.h) {
            transDataMessage.d = bArr;
            transDataMessage.i = 0;
        } else {
            transDataMessage.d = d.a(bArr);
            transDataMessage.i = 1;
        }
        if (transDataMessage.d != null) {
            transDataMessage.h = transDataMessage.d.length;
        } else {
            transDataMessage.h = 0;
        }
        if (!(bArr == null || this.q == 14087)) {
            try {
                transDataMessage.d = k.a("TencentMolo&&##%%!!!1234".getBytes("UTF-8"), transDataMessage.d);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        transDataMessage.f = i2;
        transDataMessage.g = i3;
        return transDataMessage;
    }

    private void a(int i2, PriorityBlockingQueue<TransDataMessage> priorityBlockingQueue) {
        if (this.v != null && priorityBlockingQueue != null) {
            Iterator<TransDataMessage> it = priorityBlockingQueue.iterator();
            ArrayList arrayList = new ArrayList();
            while (it.hasNext()) {
                arrayList.add(Integer.valueOf(it.next().f()));
            }
            Collections.sort(arrayList);
            this.v.a(i2, arrayList);
        }
    }

    public int d() {
        return this.q;
    }

    public void a(int i2) {
        if (this.v != null) {
            this.v.a(i2);
        }
    }

    public void a(int i2, byte[] bArr) {
        if (this.v != null) {
            ArrayList<Integer> arrayList = null;
            if (bArr != null) {
                JceInputStream jceInputStream = new JceInputStream(bArr);
                RecvPacketSeqList recvPacketSeqList = new RecvPacketSeqList();
                recvPacketSeqList.readFrom(jceInputStream);
                arrayList = recvPacketSeqList.a();
            }
            if (arrayList == null) {
                arrayList = new ArrayList<>();
            }
            this.v.a(i2, this.t, arrayList);
        }
    }
}
