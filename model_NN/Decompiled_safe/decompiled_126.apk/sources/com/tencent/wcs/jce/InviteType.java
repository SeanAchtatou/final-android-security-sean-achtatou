package com.tencent.wcs.jce;

import java.io.Serializable;

/* compiled from: ProGuard */
public final class InviteType implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final InviteType f4086a = new InviteType(0, 0, "INVITE_TYPE_LAN");
    public static final InviteType b = new InviteType(1, 1, "INVITE_TYPE_WIFI");
    public static final InviteType c = new InviteType(2, 2, "INVITE_TYPE_3G");
    public static final InviteType d = new InviteType(3, 3, "INVITE_TYPE_2G");
    static final /* synthetic */ boolean e = (!InviteType.class.desiredAssertionStatus());
    private static InviteType[] f = new InviteType[4];
    private int g;
    private String h = new String();

    public String toString() {
        return this.h;
    }

    private InviteType(int i, int i2, String str) {
        this.h = str;
        this.g = i2;
        f[i] = this;
    }
}
