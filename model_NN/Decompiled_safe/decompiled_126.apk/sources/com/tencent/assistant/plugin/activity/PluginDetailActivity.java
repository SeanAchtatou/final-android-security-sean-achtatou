package com.tencent.assistant.plugin.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.module.k;
import com.tencent.assistant.plugin.PluginDownloadInfo;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.PluginStartEntry;
import com.tencent.assistant.plugin.component.PluginDownStateButton;
import com.tencent.assistant.plugin.mgr.c;
import com.tencent.assistant.plugin.mgr.h;
import com.tencent.assistant.plugin.mgr.i;
import com.tencent.assistant.plugin.proxy.PluginProxyActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;

/* compiled from: ProGuard */
public class PluginDetailActivity extends BaseActivity {
    private PluginStartEntry n;
    private TXImageView u;
    private TextView v;
    private PluginDownStateButton w;
    private SecondNavigationTitleViewV5 x;
    private TextView y;
    private boolean z = true;

    public int f() {
        return STConst.ST_PAGE_PLUGIN;
    }

    public void onCreate(Bundle bundle) {
        PluginInfo.PluginEntry pluginEntry;
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_plugin_detail);
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra("plugin_start_entry")) {
            this.n = (PluginStartEntry) intent.getSerializableExtra("plugin_start_entry");
        }
        u();
        if (this.n != null) {
            PluginInfo a2 = i.b().a(this.n.getPackageName());
            if (a2 != null && (a2.getVersion() >= this.n.getVersionCode() || h.a(this.n.getPackageName()) >= 0)) {
                PluginInfo.PluginEntry pluginEntryByStartActivity = a2.getPluginEntryByStartActivity(this.n.getStartActivity());
                if (pluginEntryByStartActivity == null) {
                    pluginEntry = new PluginInfo.PluginEntry(a2, Constants.STR_EMPTY, null, this.n.getStartActivity());
                } else {
                    pluginEntry = pluginEntryByStartActivity;
                }
                if (pluginEntry != null) {
                    try {
                        PluginProxyActivity.a(this, pluginEntry.getHostPlugInfo().getPackageName(), pluginEntry.getHostPlugInfo().getVersion(), pluginEntry.getStartActivity(), pluginEntry.getHostPlugInfo().getInProcess(), null, pluginEntry.getHostPlugInfo().getLaunchApplication());
                    } catch (Throwable th) {
                        th.printStackTrace();
                    }
                    String str = this.n.getPluginId() + "|" + pluginEntry.getHostPlugInfo().getPackageName() + "|" + pluginEntry.getStartActivity() + "|" + 1;
                    STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, 100);
                    if (buildSTInfo != null) {
                        buildSTInfo.extraData = str;
                        l.a(buildSTInfo);
                    }
                    finish();
                    return;
                }
            }
            PluginDownloadInfo a3 = c.a().a(this.n.getPluginId());
            if (a3 == null) {
                a3 = c.a().a(this.n.getPackageName());
            }
            if (a3 == null) {
                Toast.makeText(this, (int) R.string.plugin_not_exist, 0).show();
                finish();
                return;
            }
            this.x.b(a3.name);
            this.x.d();
            this.u.updateImageView(a3.imgUrl, R.drawable.icon_default_pic_empty, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.v.setText(a3.desc);
            if (a2 == null || a2.getVersion() == this.n.getVersionCode()) {
                this.y.setVisibility(8);
                this.w.setUpdateState(false);
            } else {
                this.y.setText(String.format(getResources().getString(R.string.plugin_update_tips), a3.name));
                this.y.setVisibility(0);
                this.w.setUpdateState(true);
            }
            this.w.setDownloadInfo(a3, this.n);
            return;
        }
        finish();
    }

    private DownloadInfo t() {
        PluginDownloadInfo pluginDownloadInfo;
        PluginDownloadInfo a2 = c.a().a(this.n.getPluginId());
        if (a2 == null) {
            pluginDownloadInfo = c.a().a(this.n.getPackageName());
        } else {
            pluginDownloadInfo = a2;
        }
        if (pluginDownloadInfo == null) {
            return null;
        }
        DownloadInfo b = c.a().b(pluginDownloadInfo);
        if (b == null) {
            return c.a().a(pluginDownloadInfo, SimpleDownloadInfo.UIType.PLUGIN_PREDOWNLOAD);
        }
        return b;
    }

    private void u() {
        this.u = (TXImageView) findViewById(R.id.plugin_pic);
        this.v = (TextView) findViewById(R.id.plugin_desc);
        this.w = (PluginDownStateButton) findViewById(R.id.plugin_down_btn);
        this.w.setHost(this);
        this.x = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.x.d(false);
        this.x.a(this);
        this.y = (TextView) findViewById(R.id.plugin_update_tips);
    }

    private void a(PluginDownloadInfo pluginDownloadInfo, PluginStartEntry pluginStartEntry) {
        if (i.b().a(pluginStartEntry.getPackageName(), pluginStartEntry.getVersionCode()) == null) {
            DownloadInfo t = t();
            if (k.a(t, pluginDownloadInfo, (PluginInfo) null) == AppConst.AppState.DOWNLOADED) {
                TemporaryThreadManager.get().start(new a(this, t, pluginDownloadInfo));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.w != null) {
            this.w.onPause();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.w != null) {
            this.w.onResume();
        }
        if (this.z) {
            PluginDownloadInfo a2 = c.a().a(this.n.getPluginId());
            if (a2 == null) {
                a2 = c.a().a(this.n.getPackageName());
            }
            a(a2, this.n);
            this.z = false;
        }
    }
}
