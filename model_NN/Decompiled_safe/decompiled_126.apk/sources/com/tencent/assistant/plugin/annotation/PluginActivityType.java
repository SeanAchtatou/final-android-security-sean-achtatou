package com.tencent.assistant.plugin.annotation;

/* compiled from: ProGuard */
public abstract class PluginActivityType {
    public static final String TYPE_DIM_CENTER_VIEW = "dim_center_view";
    public static final String TYPE_NOT_DIM_DIALOG = "not_dim_dialog";
    public static final String TYPE_TRANSLUCENT = "translucent";
}
