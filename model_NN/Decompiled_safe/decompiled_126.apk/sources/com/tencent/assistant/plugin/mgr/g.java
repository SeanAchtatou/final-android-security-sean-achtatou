package com.tencent.assistant.plugin.mgr;

import com.tencent.assistant.plugin.GetPluginListEngine;
import com.tencent.assistant.plugin.PluginDownloadInfo;
import java.util.List;

/* compiled from: ProGuard */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f1101a;

    g(c cVar) {
        this.f1101a = cVar;
    }

    public void run() {
        List<PluginDownloadInfo> list = GetPluginListEngine.getInstance().getList();
        if (list == null || list.size() == 0) {
            GetPluginListEngine.getInstance().refreshData(0);
        } else {
            this.f1101a.b(list);
        }
    }
}
