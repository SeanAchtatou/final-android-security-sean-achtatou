package com.tencent.assistant.module.a;

import android.os.Handler;
import android.os.Message;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class e extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f952a;

    e(d dVar) {
        this.f952a = dVar;
    }

    public void handleMessage(Message message) {
        TemporaryThreadManager.get().start((g) message.obj);
    }
}
