package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;

/* compiled from: ProGuard */
class n implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CallbackHelper.Caller f999a;
    final /* synthetic */ BaseEngine b;

    n(BaseEngine baseEngine, CallbackHelper.Caller caller) {
        this.b = baseEngine;
        this.f999a = caller;
    }

    public void run() {
        this.b.notifyDataChanged(this.f999a);
    }
}
