package com.tencent.assistant.module.nac;

import com.tencent.assistant.protocol.jce.IPDataAddress;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class c {

    /* renamed from: a  reason: collision with root package name */
    public short f1004a;
    public String b;
    public int c;
    public String d;
    public ArrayList<IPDataAddress> e;
    public ArrayList<IPDataAddress> f;
    final /* synthetic */ b g;

    public c(b bVar, short s, String str, int i, String str2, ArrayList<IPDataAddress> arrayList, ArrayList<IPDataAddress> arrayList2) {
        this.g = bVar;
        this.f1004a = s;
        this.b = str;
        this.c = i;
        this.d = str2;
        this.e = arrayList;
        this.f = arrayList2;
    }
}
