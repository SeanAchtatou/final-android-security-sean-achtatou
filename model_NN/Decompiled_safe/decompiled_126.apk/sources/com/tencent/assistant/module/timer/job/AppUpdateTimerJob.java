package com.tencent.assistant.module.timer.job;

import com.tencent.assistant.m;
import com.tencent.assistant.module.a.a.a;
import com.tencent.assistant.module.timer.BaseTimePointJob;
import com.tencent.assistant.module.update.AppUpdateConst;
import com.tencent.assistant.module.update.aa;
import com.tencent.assistant.module.update.b;
import com.tencent.assistant.module.update.t;
import com.tencent.assistant.module.update.z;
import java.util.Calendar;

/* compiled from: ProGuard */
public class AppUpdateTimerJob extends BaseTimePointJob {
    private static AppUpdateTimerJob c;

    /* renamed from: a  reason: collision with root package name */
    private int[] f1015a;
    private int[] b;

    public static synchronized AppUpdateTimerJob h() {
        AppUpdateTimerJob appUpdateTimerJob;
        synchronized (AppUpdateTimerJob.class) {
            if (c == null) {
                c = new AppUpdateTimerJob();
            }
            appUpdateTimerJob = c;
        }
        return appUpdateTimerJob;
    }

    public void d() {
        if (j()) {
            new a().run();
            b.a().a(AppUpdateConst.RequestLaunchType.TYPE_TIMER);
            t.a().b();
        }
    }

    private void i() {
        aa a2 = z.a((byte) 1);
        if (a2 != null) {
            this.f1015a = a2.b;
            this.b = a2.f1027a;
        }
    }

    public int[] g() {
        if (this.f1015a == null) {
            i();
        }
        return this.f1015a;
    }

    private int[] a(int i) {
        if (this.b == null) {
            i();
        }
        if (this.b == null || this.b.length == 0) {
            return null;
        }
        int[] iArr = new int[2];
        int i2 = 0;
        while (true) {
            if (i2 >= this.b.length) {
                break;
            }
            int i3 = this.b[i2];
            int a2 = z.a(i3);
            int b2 = z.b(i3);
            if (i >= a2 && i < b2) {
                iArr[0] = a2;
                iArr[1] = b2;
                break;
            }
            i2++;
        }
        return iArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    private boolean j() {
        long a2 = m.a().a("app_update_refresh_suc_time", 0L);
        int[] a3 = a(Calendar.getInstance().get(11));
        Calendar instance = Calendar.getInstance();
        if (a3 != null) {
            instance.set(11, a3[0]);
        }
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar instance2 = Calendar.getInstance();
        if (a3 != null) {
            instance2.set(11, a3[1]);
        }
        instance2.set(12, 59);
        instance2.set(13, 0);
        instance2.set(14, 0);
        if (a2 < instance.getTimeInMillis() || a2 > instance2.getTimeInMillis()) {
            return true;
        }
        return false;
    }
}
