package com.tencent.assistant.module.update;

import android.text.TextUtils;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.protocol.jce.AutoDownloadInfo;
import com.tencent.assistant.protocol.jce.AutoDownloadUserInfo;
import com.tencent.assistant.protocol.jce.GetAutoDownloadResponse;
import com.tencent.pangu.module.wisedownload.u;
import com.tencent.pangu.module.wisedownload.v;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class s {

    /* renamed from: a  reason: collision with root package name */
    public AutoDownloadUserInfo f1045a = null;
    public ArrayList<AutoDownloadInfo> b = null;
    public ArrayList<AutoDownloadInfo> c = null;
    public ArrayList<AutoDownloadInfo> d = null;
    public ArrayList<AutoDownloadInfo> e = null;
    public ArrayList<AutoDownloadInfo> f = null;
    private GetAutoDownloadResponse g = null;
    private ArrayList<AutoDownloadInfo> h = null;

    public s() {
        l();
    }

    private void l() {
        this.g = i.y().k();
        List<AutoDownloadInfo> l = i.y().l();
        if (l != null) {
            this.h = new ArrayList<>(l);
        }
        b(this.g);
    }

    public void a(GetAutoDownloadResponse getAutoDownloadResponse) {
        if (getAutoDownloadResponse != null) {
            b(getAutoDownloadResponse);
            u.d();
            com.tencent.pangu.module.wisedownload.s.k();
            v.b();
            i.y().a(getAutoDownloadResponse);
            if (this.f1045a != null) {
                m.a().a(AppConst.WISE_DOWNLOAD_SWITCH_TYPE.UPDATE, this.f1045a.f1171a);
                m.a().a(AppConst.WISE_DOWNLOAD_SWITCH_TYPE.NEW_DOWNLOAD, this.f1045a.c);
            }
        }
    }

    private synchronized void b(GetAutoDownloadResponse getAutoDownloadResponse) {
        if (getAutoDownloadResponse != null) {
            AutoDownloadUserInfo a2 = getAutoDownloadResponse.a();
            if (a2 != null) {
                this.f1045a = new AutoDownloadUserInfo(a2.f1171a, a2.b, a2.c);
            }
            if (getAutoDownloadResponse.d() != null) {
                this.d = new ArrayList<>(getAutoDownloadResponse.d());
            }
            if (getAutoDownloadResponse.e() != null) {
                this.e = new ArrayList<>(getAutoDownloadResponse.e());
            }
            if (getAutoDownloadResponse.f() != null) {
                this.f = new ArrayList<>(getAutoDownloadResponse.f());
            }
            if (getAutoDownloadResponse.b() != null) {
                this.b = a(getAutoDownloadResponse.b());
            }
            if (getAutoDownloadResponse.c() != null) {
                c(getAutoDownloadResponse);
            }
            if (this.h != null) {
                this.h.clear();
            }
            i.y().m();
        }
    }

    private ArrayList<AutoDownloadInfo> a(ArrayList<AutoDownloadInfo> arrayList) {
        LocalApkInfo localApkInfo;
        if (arrayList == null || arrayList.isEmpty()) {
            return arrayList;
        }
        ArrayList<AutoDownloadInfo> arrayList2 = new ArrayList<>();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= arrayList.size()) {
                return arrayList2;
            }
            AutoDownloadInfo autoDownloadInfo = arrayList.get(i2);
            if (autoDownloadInfo != null && !TextUtils.isEmpty(autoDownloadInfo.f1166a) && (localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(autoDownloadInfo.f1166a)) != null && localApkInfo.mVersionCode < autoDownloadInfo.d) {
                arrayList2.add(autoDownloadInfo);
            }
            i = i2 + 1;
        }
    }

    private void c(GetAutoDownloadResponse getAutoDownloadResponse) {
        ArrayList<AutoDownloadInfo> c2;
        if (getAutoDownloadResponse != null && (c2 = getAutoDownloadResponse.c()) != null && !c2.isEmpty()) {
            if (this.c == null) {
                this.c = new ArrayList<>();
            }
            this.c.clear();
            for (AutoDownloadInfo next : c2) {
                if (ApkResourceManager.getInstance().getLocalApkInfo(next.f1166a) == null) {
                    this.c.add(next);
                }
            }
            getAutoDownloadResponse.a(new ArrayList(this.c));
        }
    }

    public void a() {
        if (this.g != null) {
            this.g.a(this.c);
        }
        i.y().a(this.g);
    }

    public void a(List<AutoDownloadInfo> list) {
        boolean z;
        if (list != null && !list.isEmpty()) {
            if (this.h == null) {
                this.h = new ArrayList<>();
            }
            if (this.h.isEmpty()) {
                this.h.addAll(list);
                i.y().b(this.h);
                return;
            }
            ArrayList arrayList = new ArrayList();
            for (AutoDownloadInfo next : list) {
                Iterator<AutoDownloadInfo> it = this.h.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        z = false;
                        break;
                    }
                    AutoDownloadInfo next2 = it.next();
                    if (next.f1166a.endsWith(next2.f1166a) && next.d == next2.d) {
                        z = true;
                        break;
                    }
                }
                if (!z) {
                    arrayList.add(next);
                }
            }
            if (!arrayList.isEmpty()) {
                this.h.addAll(arrayList);
            }
            i.y().b(this.h);
        }
    }

    public ArrayList<AutoDownloadInfo> b() {
        return this.h;
    }

    public List<AutoDownloadInfo> c() {
        return a(this.b);
    }

    public List<AutoDownloadInfo> d() {
        return this.b;
    }

    public List<String> e() {
        return this.f1045a != null ? this.f1045a.b : new ArrayList();
    }

    public List<AutoDownloadInfo> f() {
        c(this.g);
        return this.c;
    }

    public List<AutoDownloadInfo> g() {
        return this.c;
    }

    public List<AutoDownloadInfo> h() {
        return (this.d == null || this.d.isEmpty()) ? new ArrayList() : this.d;
    }

    public List<AutoDownloadInfo> i() {
        return (this.e == null || this.e.isEmpty()) ? new ArrayList() : this.e;
    }

    public List<AutoDownloadInfo> j() {
        return (this.f == null || this.f.isEmpty()) ? new ArrayList() : this.f;
    }

    public List<AutoDownloadInfo> k() {
        return this.h;
    }

    public void a(AutoDownloadInfo autoDownloadInfo) {
        if (autoDownloadInfo != null && this.b != null && !this.b.isEmpty() && this.b.remove(autoDownloadInfo) && this.g != null) {
            this.g.c = new ArrayList<>(this.b);
            i.y().a(this.g);
        }
    }

    public void b(AutoDownloadInfo autoDownloadInfo) {
        if (autoDownloadInfo != null && this.c != null && !this.c.isEmpty() && this.c.remove(autoDownloadInfo) && this.g != null) {
            this.g.d = new ArrayList<>(this.c);
            i.y().a(this.g);
        }
    }

    public void c(AutoDownloadInfo autoDownloadInfo) {
        if (autoDownloadInfo != null && this.d != null && !this.d.isEmpty() && this.d.remove(autoDownloadInfo) && this.g != null) {
            this.g.e = new ArrayList<>(this.d);
            i.y().a(this.g);
        }
    }

    public void d(AutoDownloadInfo autoDownloadInfo) {
        if (autoDownloadInfo != null && this.f != null && !this.f.isEmpty() && this.f.remove(autoDownloadInfo) && this.g != null) {
            this.g.g = new ArrayList<>(this.f);
            i.y().a(this.g);
        }
    }
}
