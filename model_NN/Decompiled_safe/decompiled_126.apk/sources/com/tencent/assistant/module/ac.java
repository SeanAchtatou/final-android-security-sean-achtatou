package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.db.table.e;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.g;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.manager.notification.a.d;
import com.tencent.assistant.module.update.z;
import com.tencent.assistant.protocol.jce.AutoDownloadCfg;
import com.tencent.assistant.protocol.jce.AutoDownloadPushCfg;
import com.tencent.assistant.protocol.jce.AutoStartCfg;
import com.tencent.assistant.protocol.jce.CSProtocolCfg;
import com.tencent.assistant.protocol.jce.CommonCfg;
import com.tencent.assistant.protocol.jce.DownloadButtonSpecailInfoList;
import com.tencent.assistant.protocol.jce.DownloadCfg;
import com.tencent.assistant.protocol.jce.DownloadCheckCfg;
import com.tencent.assistant.protocol.jce.ExternalCallYYBCfg;
import com.tencent.assistant.protocol.jce.FloatWindowCfg;
import com.tencent.assistant.protocol.jce.GetSettingRequest;
import com.tencent.assistant.protocol.jce.GetSettingResponse;
import com.tencent.assistant.protocol.jce.InTimePushCfg;
import com.tencent.assistant.protocol.jce.InstallPopupOrderCfg;
import com.tencent.assistant.protocol.jce.NLRSettingsCfg;
import com.tencent.assistant.protocol.jce.NpcListCfg;
import com.tencent.assistant.protocol.jce.PNGSettingsCfg60;
import com.tencent.assistant.protocol.jce.SearchWebCfg;
import com.tencent.assistant.protocol.jce.SettingCfg;
import com.tencent.assistant.protocol.jce.SmartCardCfgList;
import com.tencent.assistant.protocol.jce.StatCfg;
import com.tencent.assistant.protocol.jce.TempRootCfg;
import com.tencent.assistant.protocol.jce.TimerCfg;
import com.tencent.assistant.protocol.jce.UpdateCfg;
import com.tencent.assistant.protocol.jce.UserTaskCfg;
import com.tencent.assistant.protocol.jce.WebviewCfg;
import com.tencent.assistant.smartcard.c.p;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.an;
import com.tencent.assistant.utils.bh;
import com.tencent.pangu.manager.f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public class ac extends p {
    private static ac b;

    /* renamed from: a  reason: collision with root package name */
    public final byte f957a = 30;

    private ac() {
    }

    public static synchronized ac a() {
        ac acVar;
        synchronized (ac.class) {
            if (b == null) {
                b = new ac();
            }
            acVar = b;
        }
        return acVar;
    }

    public int b() {
        return send(new GetSettingRequest());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        PNGSettingsCfg60 pNGSettingsCfg60;
        XLog.d("linmg", getClass().getSimpleName() + " onRequestSuccessed....");
        GetSettingResponse getSettingResponse = (GetSettingResponse) jceStruct2;
        HashMap hashMap = new HashMap();
        ArrayList<SettingCfg> arrayList = getSettingResponse.b;
        Map<String, String> a2 = getSettingResponse.a();
        if (a2 != null) {
            g.a().a(a2, hashMap);
        }
        for (SettingCfg next : arrayList) {
            XLog.v("cfg", "cfg.type : " + ((int) next.f1492a));
            if (next.f1492a == 1) {
                TimerCfg timerCfg = (TimerCfg) an.b(next.b, TimerCfg.class);
                if (timerCfg != null) {
                    if (timerCfg.f1592a == 1) {
                        g.a().a(timerCfg, hashMap);
                    } else if (timerCfg.f1592a == 2) {
                        g.a().d(timerCfg, hashMap);
                    } else if (timerCfg.f1592a == 3) {
                        g.a().e(timerCfg, hashMap);
                    } else if (timerCfg.f1592a == 4) {
                        g.a().f(timerCfg, hashMap);
                    } else if (timerCfg.f1592a == 5) {
                        g.a().b(timerCfg, hashMap);
                    } else if (timerCfg.f1592a == 30) {
                        g.a().c(timerCfg, hashMap);
                    }
                }
            } else if (next.f1492a == 2) {
                g.a().a((StatCfg) an.b(next.b, StatCfg.class), hashMap);
            } else if (next.f1492a == 3) {
                g.a().a((DownloadCfg) an.b(next.b, DownloadCfg.class), hashMap);
            } else if (next.f1492a == 4) {
                z.a(next.c, (UpdateCfg) an.b(next.b, UpdateCfg.class));
            } else if (next.f1492a == 5) {
                g.a().a((WebviewCfg) an.b(next.b, WebviewCfg.class), hashMap);
            } else if (next.f1492a == 6) {
                AutoDownloadCfg autoDownloadCfg = (AutoDownloadCfg) an.b(next.b, AutoDownloadCfg.class);
                if (autoDownloadCfg != null) {
                    i.y().a(autoDownloadCfg);
                    com.tencent.pangu.module.wisedownload.i.a().e();
                    g.a().a(autoDownloadCfg.a(), hashMap);
                    g.a().b(autoDownloadCfg.b(), hashMap);
                }
            } else if (next.f1492a == 7) {
                SmartCardCfgList smartCardCfgList = (SmartCardCfgList) an.b(next.b, SmartCardCfgList.class);
                if (smartCardCfgList != null) {
                    p.a().a(smartCardCfgList);
                }
            } else if (next.f1492a == 8) {
                g.a().a(next.b, hashMap);
            } else if (next.f1492a == 9) {
                CommonCfg commonCfg = (CommonCfg) an.b(next.b, CommonCfg.class);
                if (commonCfg != null) {
                    g.a().a(commonCfg, hashMap);
                }
            } else if (next.f1492a == 10) {
                UserTaskCfg userTaskCfg = (UserTaskCfg) an.b(next.b, UserTaskCfg.class);
                if (userTaskCfg != null) {
                    XLog.i("ig", "<<UserTask=" + userTaskCfg.toString());
                    i.y().a(userTaskCfg);
                }
            } else if (next.f1492a == 12) {
                CSProtocolCfg cSProtocolCfg = (CSProtocolCfg) an.b(next.b, CSProtocolCfg.class);
                if (cSProtocolCfg != null) {
                    if (cSProtocolCfg.f1181a != m.a().a("auth_protocol_update_period", 0)) {
                        m.a().b("auth_protocol_update_period", Integer.valueOf(cSProtocolCfg.f1181a));
                    }
                    if (cSProtocolCfg.b != m.a().a("auth_protocol_event_cause_period", 0)) {
                        m.a().b("auth_protocol_event_cause_period", Integer.valueOf(cSProtocolCfg.b));
                    }
                    if (cSProtocolCfg.c != m.a().a("auth_protocol_fail_nottry_period", 0)) {
                        m.a().b("auth_protocol_fail_nottry_period", Integer.valueOf(cSProtocolCfg.c));
                    }
                    if (cSProtocolCfg.d != m.a().a("auth_protocol_log_st", false)) {
                        m.a().b("auth_protocol_log_st", Boolean.valueOf(cSProtocolCfg.d));
                    }
                }
            } else if (next.f1492a == 11) {
                XLog.d("donaldxu", "get autostart setting");
                new e().b((AutoStartCfg) an.b(next.b, AutoStartCfg.class));
            } else if (next.f1492a == 13) {
                NpcListCfg npcListCfg = (NpcListCfg) an.b(next.b, NpcListCfg.class);
                if (npcListCfg != null) {
                    i.y().a(npcListCfg);
                }
            } else if (next.f1492a == 15) {
                DownloadCheckCfg downloadCheckCfg = (DownloadCheckCfg) an.b(next.b, DownloadCheckCfg.class);
                if (downloadCheckCfg != null) {
                    g.a().a(downloadCheckCfg, hashMap);
                }
            } else if (next.f1492a == 16) {
                g.a().b(next.b, hashMap);
            } else if (next.f1492a == 17) {
                f.a().a((SearchWebCfg) an.b(next.b, SearchWebCfg.class));
            } else if (next.f1492a == 18) {
                g.a().a(next.b);
            } else if (next.f1492a == 24) {
                DownloadButtonSpecailInfoList downloadButtonSpecailInfoList = (DownloadButtonSpecailInfoList) an.b(next.b, DownloadButtonSpecailInfoList.class);
                if (downloadButtonSpecailInfoList != null) {
                    bh.a().a(true, downloadButtonSpecailInfoList);
                }
            } else if (next.f1492a == 19) {
                d.a().a((InTimePushCfg) an.b(next.b, InTimePushCfg.class));
            } else if (next.f1492a == 20) {
                g.a().a((AutoDownloadPushCfg) an.b(next.b, AutoDownloadPushCfg.class));
            } else if (next.f1492a == 21) {
                XLog.i("BackgroundScan", "<settings> get background scan settings success !");
                g.a().b(next.b);
            } else if (next.f1492a == 23) {
                XLog.i("TempRoot", "<settings> get temp root settings success !");
                TempRootCfg tempRootCfg = (TempRootCfg) an.b(next.b, TempRootCfg.class);
                if (tempRootCfg != null) {
                    g.a().a(tempRootCfg);
                }
            } else if (next.f1492a == 26) {
                XLog.i("apkAutoOpenConfig", "<settings> get apk auto open config settings success !");
                g.a().c(next.b);
            } else if (next.f1492a == 25) {
                XLog.d("ExternalCallYYBCfg", "<settings> get ExternalCallYYBCfg settings success !");
                ExternalCallYYBCfg externalCallYYBCfg = (ExternalCallYYBCfg) an.b(next.b, ExternalCallYYBCfg.class);
                if (externalCallYYBCfg != null) {
                    g.a().a(externalCallYYBCfg);
                }
            } else if (next.f1492a == 27) {
                XLog.i("floatingwindow", "<settings> get float window settings success !");
                FloatWindowCfg floatWindowCfg = (FloatWindowCfg) an.b(next.b, FloatWindowCfg.class);
                if (floatWindowCfg != null) {
                    g.a().a(floatWindowCfg);
                }
            } else if (next.f1492a == 50) {
                InstallPopupOrderCfg installPopupOrderCfg = (InstallPopupOrderCfg) an.b(next.b, InstallPopupOrderCfg.class);
                if (installPopupOrderCfg != null) {
                    XLog.i("AppInstallRemind", "<<appInstallTaskCfg=" + installPopupOrderCfg.toString());
                    i.y().a(installPopupOrderCfg);
                }
            } else if (next.f1492a == 80) {
                NLRSettingsCfg nLRSettingsCfg = (NLRSettingsCfg) an.b(next.b, NLRSettingsCfg.class);
                if (nLRSettingsCfg != null) {
                    g.a().a(nLRSettingsCfg);
                }
            } else if (next.f1492a == 60 && (pNGSettingsCfg60 = (PNGSettingsCfg60) an.b(next.b, PNGSettingsCfg60.class)) != null) {
                g.a().a(pNGSettingsCfg60);
            }
        }
        g.a().a(hashMap);
        m.a().c(getSettingResponse.c - System.currentTimeMillis());
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.d("linmg", getClass().getSimpleName() + " onRequestFailed...." + i2);
    }
}
