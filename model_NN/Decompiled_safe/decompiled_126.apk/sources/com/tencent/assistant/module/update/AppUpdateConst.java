package com.tencent.assistant.module.update;

/* compiled from: ProGuard */
public class AppUpdateConst {

    /* compiled from: ProGuard */
    public enum RequestLaunchType {
        TYPE_DEFAULT,
        TYPE_STARTUP,
        TYPE_TIMER,
        TYPE_APP_INSTALLED,
        TYPE_APP_UNINSTALL,
        TYPE_APP_REPLACED,
        TYPE_INTIME_UPDATE_PUSH,
        TYPE_ASSISTANT_RETRY
    }
}
