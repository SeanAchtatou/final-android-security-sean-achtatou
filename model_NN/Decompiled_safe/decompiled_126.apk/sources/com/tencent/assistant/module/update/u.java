package com.tencent.assistant.module.update;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.TemporaryThreadManager;
import java.util.List;

/* compiled from: ProGuard */
class u extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ t f1047a;

    u(t tVar) {
        this.f1047a = tVar;
    }

    public void onLoadInstalledApkSuccess(List<LocalApkInfo> list) {
        this.f1047a.l();
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        if (localApkInfo != null && 1 == i && this.f1047a.c != null) {
            TemporaryThreadManager.get().start(new v(this, localApkInfo));
        }
    }
}
