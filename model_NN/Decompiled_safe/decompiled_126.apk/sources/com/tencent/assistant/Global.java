package com.tencent.assistant;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.os.Build;
import android.os.Message;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.protocol.jce.Terminal;
import com.tencent.assistant.protocol.jce.TerminalExtra;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.f;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ax;
import com.tencent.assistant.utils.bc;
import com.tencent.assistant.utils.p;
import com.tencent.assistant.utils.t;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.manager.root.e;
import com.tencent.open.utils.ServerSetting;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/* compiled from: ProGuard */
public class Global {
    public static AppStatus APP_STATUS = AppStatus.OFFICIAL;
    public static final boolean ASSISTANT_DEBUG = false;
    public static String BUILD_NO = "1068";
    public static int DIVIDING_POS = 9;
    public static final String[] H5_SERVER_ENVIRONMENT_NAMES = {"正式环境", "预发布环境", "dev环境", "test环境"};
    public static final String HTTP_USER_AGENT = "TMA/5.4.1";
    public static final boolean LAUNCH_OPT_LOG = false;
    public static final String QUA_HEADER = "TMA";
    public static final String QUA_HEADER_LITE = "TMAL";

    /* renamed from: QUA＿HEADER＿FULL  reason: contains not printable characters */
    public static final String f1QUAHEADERFULL = "TMAF";
    public static final String RELEASE_DATE = "2015-07-06";
    public static final boolean RESEARCH_TOOL_DEBUG = false;
    public static String[] SERVER_ENVIRONMENT_NAME = {"正式环境", "测试环境", "预发布", "预发布ma", "预发布mc", " 预发布mm", "sdk正式环境", "sdk测试环境", "OEM set正式环境", "webserver", "内容dev", "内容test", "游戏dev", "游戏test", "基础dev", "基础test", "能力dev", "能力test", "商业化dev", "商业化test", "生态test"};
    public static final boolean START_DEBUG = false;
    public static final boolean WISE_DEBUG = false;

    /* renamed from: a  reason: collision with root package name */
    private static String f379a;
    private static String b = null;
    private static String c;
    private static String d;
    private static Terminal e;
    /* access modifiers changed from: private */
    public static String f = Constants.STR_EMPTY;
    private static String g;
    private static String h = Constants.STR_EMPTY;
    private static short i = 0;
    private static volatile boolean j = false;
    private static TerminalExtra k = null;
    private static AppStatus l = APP_STATUS;
    private static byte[] m = new byte[0];
    private static String n = Constants.STR_EMPTY;
    private static String[] o = {"ma.3g.qq.com", "madev2.kf0309.3g.qq.com", "mastage2.kf0309.3g.qq.com", "mastage4.kf0309.3g.qq.com", "mastage5.kf0309.3g.qq.com", "mastage3.kf0309.3g.qq.com", "masdk.3g.qq.com", "masdkdev.kf0309.3g.qq.com", "maoem.3g.qq.com", "maweb.3g.qq.com", "maclouddev.kf0309.3g.qq.com", "macloud.kf0309.3g.qq.com", "gftdev.kf0309.3g.qq.com", "gft.kf0309.3g.qq.com", "pngdev.kf0309.3g.qq.com", "png.kf0309.3g.qq.com", "nucleardev.kf0309.3g.qq.com", "nuclear.kf0309.3g.qq.com", "fbidev.kf0309.3g.qq.com", "fbi.kf0309.3g.qq.com", "rft.kf0309.3g.qq.com"};
    private static final String[] p = {ServerSetting.KEY_HOST_QZS_QQ, "pre-qzs.yybv.qq.com", "dev-qzs.yybv.qq.com", "test-qzs.yybv.qq.com"};

    /* compiled from: ProGuard */
    enum AppStatus {
        DEV,
        GRAY,
        OFFICIAL
    }

    public static void init() {
        synchronized (m) {
            if (!j) {
                long currentTimeMillis = System.currentTimeMillis();
                if (b.a().f()) {
                    l = AppStatus.DEV;
                }
                t.a(AstApp.i());
                k();
                i.a();
                g();
                i();
                h();
                d();
                c();
                j = true;
                XLog.d("Start", "init global cost " + (System.currentTimeMillis() - currentTimeMillis));
            }
        }
    }

    public static synchronized boolean hasInit() {
        boolean z;
        synchronized (Global.class) {
            z = j;
        }
        return z;
    }

    public static boolean isDev() {
        return APP_STATUS == AppStatus.DEV || l == AppStatus.DEV;
    }

    public static boolean isOfficial() {
        return APP_STATUS == AppStatus.OFFICIAL;
    }

    public static boolean isGray() {
        return APP_STATUS == AppStatus.GRAY;
    }

    private static String b() {
        return i.c();
    }

    public static boolean isLite() {
        return QUA_HEADER_LITE.equals(b());
    }

    public static String getServerAddress() {
        int c2 = b.a().c();
        XLog.d("Config", "***************** global getServerAddress =" + c2);
        if (c2 < 0) {
            c2 = m.a().c();
        }
        return (!isDev() || c2 < 0 || c2 >= o.length) ? "ma.3g.qq.com" : o[c2];
    }

    public static String getServerAddressName() {
        int c2 = m.a().c();
        return (c2 < 0 || c2 >= SERVER_ENVIRONMENT_NAME.length) ? "选择环境" : SERVER_ENVIRONMENT_NAME[c2];
    }

    public static String getQUA() {
        if (f379a == null) {
            d();
        }
        return f379a;
    }

    public static String getQUAForBeacon() {
        if (b == null) {
            d();
            b = f379a.substring(0, f379a.indexOf("&"));
        }
        return b;
    }

    public static final String getPhoneGuid() {
        if (TextUtils.isEmpty(f) || f.equals("NA")) {
            f = Constants.STR_EMPTY;
        }
        return f;
    }

    public static final String getPhoneGuidAndGen() {
        if (TextUtils.isEmpty(f) || f.equals("NA")) {
            g();
        }
        return f;
    }

    public static final String getMoloDeviceId() {
        return i.b();
    }

    public static final void setPhoneGuid(String str) {
        if (!TextUtils.isEmpty(str)) {
            if (str.equals("NA")) {
                if (f != null && !TextUtils.isEmpty(f)) {
                    a(Constants.STR_EMPTY);
                    TemporaryThreadManager.get().start(new f());
                }
            } else if (TextUtils.isEmpty(f) || !f.equals(str)) {
                a(str);
                TemporaryThreadManager.get().start(new g());
            }
        }
    }

    private static void a(String str) {
        f = str;
        Message message = new Message();
        message.what = EventDispatcherEnum.UI_EVENT_GUID_CHANGED;
        AstApp.i().j().dispatchMessage(message);
    }

    private static void c() {
        k = new TerminalExtra();
        k.f1582a = c.a();
        k.b = c.e();
        k.c = c.b();
        k.d = c.c();
        k.e = c.d();
        String[] f2 = c.f();
        k.f = f2[0];
        k.g = f2[1];
    }

    public static final Terminal getNotNullTerminal() {
        return getPhoneTerminal();
    }

    public static final Terminal getPhoneTerminal() {
        return e;
    }

    public static TerminalExtra getTerminalExtra() {
        return k;
    }

    public static String getClientIp() {
        return h;
    }

    public static void setClientIp(String str) {
        h = str;
    }

    private static void d() {
        String str;
        ax axVar = new ax();
        switch (h.f759a[APP_STATUS.ordinal()]) {
            case 1:
                str = "DEV";
                break;
            case 2:
                str = "P";
                break;
            case 3:
                str = "F";
                break;
            default:
                str = "DEV";
                break;
        }
        axVar.a(b());
        axVar.d(getBuildNo());
        axVar.e(str);
        axVar.f(getChannelId());
        axVar.g(e());
        axVar.b(t.c);
        axVar.a(t.b);
        axVar.c(l().ordinal());
        axVar.b(f());
        axVar.c(getAppVersion());
        axVar.d(bc.a(false));
        axVar.a(e.a().c());
        f379a = axVar.a();
        XLog.i("Config", "qua:" + f379a);
    }

    public static String getBuildNo() {
        if (BUILD_NO == null || BUILD_NO.contains("BuildNo")) {
            return "0000";
        }
        if (BUILD_NO.length() != 4) {
            if (BUILD_NO.length() <= 0) {
                BUILD_NO = "0000";
            } else if (BUILD_NO.length() == 1) {
                BUILD_NO = "000" + BUILD_NO;
            } else if (BUILD_NO.length() == 2) {
                BUILD_NO = STConst.ST_STATUS_DEFAULT + BUILD_NO;
            } else if (BUILD_NO.length() == 3) {
                BUILD_NO = "0" + BUILD_NO;
            } else {
                BUILD_NO = BUILD_NO.substring(BUILD_NO.length() - 4, BUILD_NO.length());
            }
        }
        return BUILD_NO;
    }

    public static String getAppVersionName() {
        if (TextUtils.isEmpty(g)) {
            i();
        }
        return g;
    }

    public static String getAppVersion() {
        String e2 = b.a().e();
        XLog.d("Config", "***************** global getAppVersion =" + e2);
        if (TextUtils.isEmpty(e2)) {
            return m.a().e();
        }
        return e2;
    }

    public static int getAppVersionCode() {
        try {
            return AstApp.i().getPackageManager().getPackageInfo(AstApp.i().getPackageName(), 0).versionCode;
        } catch (Exception e2) {
            return 0;
        }
    }

    public static String getAppVersionName4RQD() {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            PackageInfo packageInfo = AstApp.i().getPackageManager().getPackageInfo(AstApp.i().getPackageName(), 0);
            stringBuffer.append(packageInfo.versionName);
            stringBuffer.append("_");
            stringBuffer.append(packageInfo.versionCode);
        } catch (PackageManager.NameNotFoundException | Exception e2) {
        }
        return stringBuffer.toString();
    }

    public static String getAppVersionName4Crash() {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            stringBuffer.append(AstApp.i().getPackageManager().getPackageInfo(AstApp.i().getPackageName(), 0).versionName);
            stringBuffer.append(".");
            stringBuffer.append(getBuildNo());
        } catch (PackageManager.NameNotFoundException | Exception e2) {
        }
        String d2 = i.d();
        if (!TextUtils.isEmpty(d2)) {
            stringBuffer.append(".");
            stringBuffer.append(d2);
        }
        return stringBuffer.toString();
    }

    public static String getAppReleaseDate() {
        return RELEASE_DATE;
    }

    public static String getChannelId() {
        if (TextUtils.isEmpty(c)) {
            k();
        }
        return c;
    }

    public static String getRealChannelId() {
        if (TextUtils.isEmpty(d)) {
            j();
        }
        return d;
    }

    private static String e() {
        StringBuffer stringBuffer = new StringBuffer();
        String str = Build.VERSION.RELEASE;
        if (TextUtils.isEmpty(str)) {
            stringBuffer.append("NA");
        } else {
            stringBuffer.append(str);
        }
        stringBuffer.append("_");
        stringBuffer.append(Build.VERSION.SDK_INT);
        return stringBuffer.toString();
    }

    private static StringBuffer b(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        if (TextUtils.isEmpty(str)) {
            stringBuffer.append("NA");
            return stringBuffer;
        }
        char[] charArray = str.toCharArray();
        for (int i2 = 0; i2 < charArray.length; i2++) {
            if (!(charArray[i2] <= ' ' || charArray[i2] == '/' || charArray[i2] == '_' || charArray[i2] == '&' || charArray[i2] == '|' || charArray[i2] == '-')) {
                stringBuffer.append(charArray[i2]);
            }
        }
        return stringBuffer;
    }

    private static String f() {
        StringBuffer stringBuffer = new StringBuffer();
        String str = Constants.STR_EMPTY;
        try {
            str = Build.BRAND;
        } catch (Exception e2) {
        }
        stringBuffer.append(b(str));
        stringBuffer.append("_");
        stringBuffer.append(b(Build.MODEL));
        stringBuffer.append("_");
        stringBuffer.append(b(Build.MANUFACTURER));
        stringBuffer.append("_");
        stringBuffer.append(b(Build.PRODUCT));
        return stringBuffer.toString();
    }

    private static void g() {
        String str = null;
        try {
            str = FileUtil.read(AstApp.i().getFilesDir().getAbsolutePath() + "/" + ".pid");
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (TextUtils.isEmpty(str) || str.equals("NA")) {
            f = Constants.STR_EMPTY;
        } else {
            f = str;
        }
    }

    private static void h() {
        String l2 = t.l();
        String m2 = t.m();
        e = new Terminal();
        e.c = l2;
        e.d = m2;
        String[] strArr = new String[2];
        String[] j2 = t.j();
        if (j2 == null || !t.a(j2, 0)) {
            e.f1581a = t.g();
        } else {
            e.f1581a = j2[0];
        }
        if (j2 != null && t.a(j2, 1)) {
            e.g = j2[1];
        }
        String[] strArr2 = new String[2];
        String[] i2 = t.i();
        if (i2 == null || !t.a(i2, 0)) {
            e.e = t.h();
        } else {
            e.e = i2[0];
        }
        if (i2 != null && t.a(i2, 1)) {
            e.f = i2[1];
        }
        e.b = t.k();
        e.h = f.e();
        t.a(l2, m2);
    }

    private static void i() {
        String packageName = AstApp.i().getPackageName();
        try {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(AstApp.i().getPackageManager().getPackageInfo(packageName, 0).versionName);
            stringBuffer.append(" build");
            stringBuffer.append(getBuildNo());
            g = stringBuffer.toString();
        } catch (PackageManager.NameNotFoundException e2) {
            g = "NA";
        }
    }

    private static void j() {
        BufferedReader bufferedReader;
        InputStream inputStream;
        String stringBuffer;
        InputStream inputStream2 = null;
        AssetManager assets = AstApp.i().getAssets();
        StringBuffer stringBuffer2 = new StringBuffer();
        try {
            inputStream = assets.open("channel.ini");
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine != null) {
                        String[] split = readLine.split("=");
                        if (split == null || split.length < 2) {
                            stringBuffer2.append("NA");
                        } else {
                            stringBuffer2.append(split[1]);
                        }
                    }
                    p.a(bufferedReader);
                    p.a(inputStream);
                } catch (IOException e2) {
                    e = e2;
                    inputStream2 = inputStream;
                } catch (Throwable th) {
                    th = th;
                    p.a(bufferedReader);
                    p.a(inputStream);
                    throw th;
                }
            } catch (IOException e3) {
                e = e3;
                bufferedReader = null;
                inputStream2 = inputStream;
                try {
                    e.printStackTrace();
                    stringBuffer2.append("NA");
                    p.a(bufferedReader);
                    p.a(inputStream2);
                    stringBuffer = stringBuffer2.toString();
                    if (!"0".equals(stringBuffer)) {
                    }
                    d = "NA";
                } catch (Throwable th2) {
                    th = th2;
                    inputStream = inputStream2;
                    p.a(bufferedReader);
                    p.a(inputStream);
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                bufferedReader = null;
                p.a(bufferedReader);
                p.a(inputStream);
                throw th;
            }
        } catch (IOException e4) {
            e = e4;
            bufferedReader = null;
        } catch (Throwable th4) {
            th = th4;
            bufferedReader = null;
            inputStream = null;
            p.a(bufferedReader);
            p.a(inputStream);
            throw th;
        }
        stringBuffer = stringBuffer2.toString();
        if (!"0".equals(stringBuffer) || "NA".equals(stringBuffer)) {
            d = "NA";
        } else {
            d = stringBuffer;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00d8 A[SYNTHETIC, Splitter:B:56:0x00d8] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00dd A[SYNTHETIC, Splitter:B:59:0x00dd] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void k() {
        /*
            r2 = 0
            com.tencent.assistant.b r0 = com.tencent.assistant.b.a()
            java.lang.String r0 = r0.d()
            java.lang.String r1 = "Config"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "***************** global getChannelId ="
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            com.tencent.assistant.utils.XLog.d(r1, r3)
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 != 0) goto L_0x003a
            java.lang.String r1 = "NA"
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x003a
            java.lang.String r1 = "0"
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x003a
            com.tencent.assistant.Global.c = r0
        L_0x0039:
            return
        L_0x003a:
            com.tencent.assistant.m r0 = com.tencent.assistant.m.a()
            java.lang.String r0 = r0.b()
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 != 0) goto L_0x005b
            java.lang.String r1 = "NA"
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x005b
            java.lang.String r1 = "0"
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x005b
            com.tencent.assistant.Global.c = r0
            goto L_0x0039
        L_0x005b:
            com.qq.AppService.AstApp r0 = com.qq.AppService.AstApp.i()
            android.content.res.AssetManager r0 = r0.getAssets()
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            r4.<init>()
            java.lang.String r1 = "channel.ini"
            java.io.InputStream r3 = r0.open(r1)     // Catch:{ IOException -> 0x010a, all -> 0x00d3 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x010d, all -> 0x0102 }
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x010d, all -> 0x0102 }
            r0.<init>(r3)     // Catch:{ IOException -> 0x010d, all -> 0x0102 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x010d, all -> 0x0102 }
            java.lang.String r0 = r1.readLine()     // Catch:{ IOException -> 0x00b9, all -> 0x0105 }
            if (r0 == 0) goto L_0x008f
            java.lang.String r2 = "="
            java.lang.String[] r0 = r0.split(r2)     // Catch:{ IOException -> 0x00b9, all -> 0x0105 }
            if (r0 == 0) goto L_0x008a
            int r2 = r0.length     // Catch:{ IOException -> 0x00b9, all -> 0x0105 }
            r5 = 2
            if (r2 >= r5) goto L_0x00b2
        L_0x008a:
            java.lang.String r0 = "NA"
            r4.append(r0)     // Catch:{ IOException -> 0x00b9, all -> 0x0105 }
        L_0x008f:
            if (r1 == 0) goto L_0x0094
            r1.close()     // Catch:{ Exception -> 0x00fb }
        L_0x0094:
            if (r3 == 0) goto L_0x0099
            r3.close()     // Catch:{ Exception -> 0x0100 }
        L_0x0099:
            java.lang.String r0 = r4.toString()
            java.lang.String r1 = "0"
            boolean r1 = r1.equals(r0)
            if (r1 != 0) goto L_0x00ad
            java.lang.String r1 = "NA"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x00e1
        L_0x00ad:
            java.lang.String r0 = "NA"
            com.tencent.assistant.Global.c = r0
            goto L_0x0039
        L_0x00b2:
            r2 = 1
            r0 = r0[r2]     // Catch:{ IOException -> 0x00b9, all -> 0x0105 }
            r4.append(r0)     // Catch:{ IOException -> 0x00b9, all -> 0x0105 }
            goto L_0x008f
        L_0x00b9:
            r0 = move-exception
            r2 = r3
        L_0x00bb:
            r0.printStackTrace()     // Catch:{ all -> 0x0107 }
            java.lang.String r0 = "NA"
            r4.append(r0)     // Catch:{ all -> 0x0107 }
            if (r1 == 0) goto L_0x00c8
            r1.close()     // Catch:{ Exception -> 0x00f6 }
        L_0x00c8:
            if (r2 == 0) goto L_0x0099
            r2.close()     // Catch:{ Exception -> 0x00ce }
            goto L_0x0099
        L_0x00ce:
            r0 = move-exception
        L_0x00cf:
            r0.printStackTrace()
            goto L_0x0099
        L_0x00d3:
            r0 = move-exception
            r1 = r2
            r3 = r2
        L_0x00d6:
            if (r1 == 0) goto L_0x00db
            r1.close()     // Catch:{ Exception -> 0x00ec }
        L_0x00db:
            if (r3 == 0) goto L_0x00e0
            r3.close()     // Catch:{ Exception -> 0x00f1 }
        L_0x00e0:
            throw r0
        L_0x00e1:
            com.tencent.assistant.Global.c = r0
            com.tencent.assistant.m r1 = com.tencent.assistant.m.a()
            r1.a(r0)
            goto L_0x0039
        L_0x00ec:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00db
        L_0x00f1:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00e0
        L_0x00f6:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00c8
        L_0x00fb:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0094
        L_0x0100:
            r0 = move-exception
            goto L_0x00cf
        L_0x0102:
            r0 = move-exception
            r1 = r2
            goto L_0x00d6
        L_0x0105:
            r0 = move-exception
            goto L_0x00d6
        L_0x0107:
            r0 = move-exception
            r3 = r2
            goto L_0x00d6
        L_0x010a:
            r0 = move-exception
            r1 = r2
            goto L_0x00bb
        L_0x010d:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x00bb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.Global.k():void");
    }

    private static AppConst.ROOT_STATUS l() {
        return m.a().n();
    }

    public static short getAreacode() {
        return i;
    }

    public static void setAreacode(short s) {
        i = s;
    }

    public static String getUUIDFromManifest() {
        String string;
        AstApp i2 = AstApp.i();
        try {
            ApplicationInfo applicationInfo = i2.getPackageManager().getApplicationInfo(i2.getPackageName(), 128);
            return (applicationInfo == null || applicationInfo.metaData == null || (string = applicationInfo.metaData.getString("com.tencent.rdm.uuid")) == null) ? Constants.STR_EMPTY : string;
        } catch (Exception e2) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0031 A[SYNTHETIC, Splitter:B:17:0x0031] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x003f A[SYNTHETIC, Splitter:B:24:0x003f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getUUID4Crash() {
        /*
            r0 = 0
            com.qq.AppService.AstApp r1 = com.qq.AppService.AstApp.i()
            android.content.pm.ApplicationInfo r1 = r1.getApplicationInfo()
            java.lang.String r1 = r1.sourceDir
            java.util.zip.ZipFile r2 = new java.util.zip.ZipFile     // Catch:{ IOException -> 0x002a, all -> 0x003a }
            r2.<init>(r1)     // Catch:{ IOException -> 0x002a, all -> 0x003a }
            java.lang.String r1 = "classes.dex"
            java.util.zip.ZipEntry r1 = r2.getEntry(r1)     // Catch:{ IOException -> 0x004c }
            if (r1 == 0) goto L_0x0024
            long r3 = r1.getCrc()     // Catch:{ IOException -> 0x004c }
            java.lang.String r1 = java.lang.Long.toHexString(r3)     // Catch:{ IOException -> 0x004c }
            java.lang.String r0 = r1.toUpperCase()     // Catch:{ IOException -> 0x004c }
        L_0x0024:
            if (r2 == 0) goto L_0x0029
            r2.close()     // Catch:{ IOException -> 0x0048 }
        L_0x0029:
            return r0
        L_0x002a:
            r1 = move-exception
            r2 = r0
        L_0x002c:
            r1.printStackTrace()     // Catch:{ all -> 0x004a }
            if (r2 == 0) goto L_0x0029
            r2.close()     // Catch:{ IOException -> 0x0035 }
            goto L_0x0029
        L_0x0035:
            r1 = move-exception
        L_0x0036:
            r1.printStackTrace()
            goto L_0x0029
        L_0x003a:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x003d:
            if (r2 == 0) goto L_0x0042
            r2.close()     // Catch:{ IOException -> 0x0043 }
        L_0x0042:
            throw r0
        L_0x0043:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0042
        L_0x0048:
            r1 = move-exception
            goto L_0x0036
        L_0x004a:
            r0 = move-exception
            goto L_0x003d
        L_0x004c:
            r1 = move-exception
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.Global.getUUID4Crash():java.lang.String");
    }

    public static synchronized String getStartLogFileName() {
        synchronized (Global.class) {
        }
        return "Start.txt";
    }

    public static String getH5ServerAddress() {
        XLog.d("Config", "***************** global getH5ServerAddress =" + -1);
        int d2 = m.a().d();
        return (!isDev() || d2 < 0 || d2 >= p.length) ? ServerSetting.KEY_HOST_QZS_QQ : p[d2];
    }
}
