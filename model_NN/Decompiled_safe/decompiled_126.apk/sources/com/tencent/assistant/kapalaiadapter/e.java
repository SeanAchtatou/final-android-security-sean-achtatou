package com.tencent.assistant.kapalaiadapter;

import com.tencent.assistant.kapalaiadapter.IMoblieModelConfig;
import com.tencent.tmsecurelite.optimize.ISystemOptimize;

/* compiled from: ProGuard */
public class e implements IMoblieModelConfig {

    /* renamed from: a  reason: collision with root package name */
    private static e f791a;

    private e() {
    }

    public static e a() {
        if (f791a == null) {
            synchronized (e.class) {
                if (f791a == null) {
                    f791a = new e();
                }
            }
        }
        return f791a;
    }

    public void a(String str) {
        try {
            switch (f.f792a[IMoblieModelConfig.SAMSUNG.valueOf(r(str)).ordinal()]) {
                case 1:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 3;
                    return;
                case 2:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 3;
                    return;
                case 3:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 3;
                    return;
                case 4:
                    d.d = false;
                    d.f = false;
                    d.e[0] = 0;
                    d.e[1] = 0;
                    d.g[0] = 0;
                    d.g[1] = 0;
                    return;
                case 5:
                    d.d = false;
                    d.f = false;
                    d.e[0] = 0;
                    d.e[1] = 0;
                    d.g[0] = 8;
                    d.g[1] = 0;
                    return;
                case 6:
                    d.d = false;
                    d.f = false;
                    d.e[0] = 0;
                    d.e[1] = 0;
                    d.g[0] = 8;
                    d.g[1] = 0;
                    return;
                case 7:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 5;
                    return;
                case 8:
                    d.d = false;
                    d.f = false;
                    d.e[0] = 0;
                    d.e[1] = 0;
                    d.g[0] = 8;
                    d.g[1] = 0;
                    return;
                case 9:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 0;
                    return;
                case 10:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 0;
                    return;
                case 11:
                    d.d = false;
                    d.f = false;
                    d.e[0] = 0;
                    d.e[1] = 0;
                    d.g[0] = 8;
                    d.g[1] = 0;
                    return;
                case 12:
                    d.d = false;
                    d.f = false;
                    d.e[0] = 0;
                    d.e[1] = 0;
                    d.g[0] = 8;
                    d.g[1] = 0;
                    return;
                case 13:
                    d.d = false;
                    d.f = false;
                    d.e[0] = 0;
                    d.e[1] = 0;
                    d.g[0] = 8;
                    d.g[1] = 0;
                    return;
                case 14:
                    d.d = false;
                    d.f = false;
                    d.e[0] = 0;
                    d.e[1] = 0;
                    d.g[0] = 8;
                    d.g[1] = 0;
                    return;
                case 15:
                    d.d = false;
                    d.f = false;
                    d.e[0] = 0;
                    d.e[1] = 0;
                    d.g[0] = 8;
                    d.g[1] = 0;
                    return;
                case 16:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 0;
                    return;
                case 17:
                    d.d = false;
                    d.f = false;
                    d.e[0] = 0;
                    d.e[1] = 0;
                    d.g[0] = 8;
                    d.g[1] = 0;
                    return;
                case 18:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 0;
                    return;
                case 19:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 8;
                    return;
                case 20:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 0;
                    return;
                case 21:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 0;
                    return;
                case ISystemOptimize.T_killTaskAsync /*22*/:
                    d.d = false;
                    d.f = false;
                    d.e[0] = 0;
                    d.e[1] = 0;
                    d.g[0] = 8;
                    d.g[1] = 0;
                    return;
                case 23:
                    d.d = false;
                    d.f = false;
                    d.e[0] = 0;
                    d.e[1] = 0;
                    d.g[0] = 8;
                    d.g[1] = 0;
                    return;
                case ISystemOptimize.T_checkVersionAsync /*24*/:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 3;
                    return;
                case ISystemOptimize.T_hasRootAsync /*25*/:
                    d.f = false;
                    d.g[0] = 8;
                    d.g[1] = 0;
                    return;
                case ISystemOptimize.T_askForRootAsync /*26*/:
                    d.f = false;
                    d.g[0] = 8;
                    d.g[1] = 0;
                    return;
                case ISystemOptimize.T_getMemoryUsageAsync /*27*/:
                    d.f = false;
                    d.g[0] = 8;
                    d.g[1] = 0;
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
        }
    }

    public void b(String str) {
        try {
            switch (f.b[IMoblieModelConfig.HTC.valueOf(r(str)).ordinal()]) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    d.b = false;
                    return;
                case 6:
                    d.b = false;
                    d.d = false;
                    d.f = false;
                    d.e[0] = 0;
                    d.e[1] = 7;
                    d.g[0] = 7;
                    d.g[1] = 7;
                    d.i[1] = 0;
                    return;
                case 7:
                    d.f = false;
                    d.g[0] = 7;
                    d.g[1] = 7;
                    d.i[1] = 0;
                    return;
                case 8:
                    d.b = false;
                    d.f = false;
                    d.g[0] = 8;
                    d.g[1] = 7;
                    d.i[1] = 0;
                    return;
                case 9:
                    d.f = false;
                    d.g[0] = 8;
                    d.g[1] = 7;
                    d.i[1] = 0;
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
        }
    }

    public void c(String str) {
        try {
            IMoblieModelConfig.MOTOROLA.valueOf(r(str));
        } catch (Exception e) {
        }
    }

    public void d(String str) {
        try {
            switch (f.c[IMoblieModelConfig.HUAWEI.valueOf(r(str)).ordinal()]) {
                case 1:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 0;
                    return;
                case 2:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 3;
                    return;
                case 3:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 3;
                    return;
                case 4:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 3;
                    return;
                case 5:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 6:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 7:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 3;
                    return;
                case 8:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 9:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 5;
                    return;
                case 10:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 5;
                    return;
                case 11:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 3;
                    return;
                case 12:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 5;
                    return;
                case 13:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 14:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 3;
                    return;
                case 15:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 5;
                    return;
                case 16:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
        }
    }

    public void e(String str) {
        try {
            switch (f.d[IMoblieModelConfig.ZTE.valueOf(r(str)).ordinal()]) {
                case 1:
                    d.b = false;
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
        }
    }

    public void f(String str) {
        try {
            IMoblieModelConfig.HISENSE.valueOf(r(str));
        } catch (Exception e) {
        }
    }

    public void g(String str) {
        try {
            switch (f.e[IMoblieModelConfig.XIAOMI.valueOf(r(str)).ordinal()]) {
                case 1:
                    d.f790a = false;
                    return;
                case 2:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 3:
                    d.d = false;
                    d.e[0] = 3;
                    d.e[1] = 3;
                    return;
                case 4:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
        }
    }

    public void h(String str) {
        try {
            IMoblieModelConfig.MEIZU.valueOf(r(str));
        } catch (Exception e) {
        }
    }

    public void i(String str) {
        try {
            switch (f.f[IMoblieModelConfig.ALPS.valueOf(r(str)).ordinal()]) {
                case 1:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 2:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 3:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
        }
    }

    public void j(String str) {
        try {
            IMoblieModelConfig.K_TOUCH.valueOf(r(str));
        } catch (Exception e) {
        }
    }

    public void k(String str) {
        try {
            switch (f.g[IMoblieModelConfig.COOLPAD.valueOf(r(str)).ordinal()]) {
                case 1:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 2:
                    d.d = false;
                    d.e[0] = 2;
                    d.e[1] = 2;
                    return;
                case 3:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 4:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 5:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 6:
                    d.d = false;
                    d.e[0] = 2;
                    d.e[1] = 2;
                    return;
                case 7:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 8:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 9:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 6;
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
        }
    }

    public void l(String str) {
        try {
            switch (f.h[IMoblieModelConfig.LENOVO.valueOf(r(str)).ordinal()]) {
                case 1:
                    d.d = false;
                    d.e[0] = 2;
                    d.e[1] = 2;
                    return;
                case 2:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 3:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 4:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 5:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 6:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 7:
                    d.d = false;
                    d.e[0] = 2;
                    d.e[1] = 2;
                    return;
                case 8:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 9:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 10:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 11:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 12:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
        }
    }

    public void m(String str) {
        try {
            switch (f.i[IMoblieModelConfig.BBK.valueOf(r(str)).ordinal()]) {
                case 1:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 2:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 3:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 4:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 5:
                    d.c = false;
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 6:
                    d.c = false;
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 7:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 8:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 9:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 10:
                    d.c = false;
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 11:
                    d.c = false;
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 12:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 13:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
        }
    }

    public void n(String str) {
        try {
            switch (f.j[IMoblieModelConfig.SONY.valueOf(r(str)).ordinal()]) {
                case 1:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 2:
                case 3:
                case 4:
                    d.b = false;
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
        }
    }

    public void o(String str) {
        try {
            switch (f.k[IMoblieModelConfig.OPPO.valueOf(r(str)).ordinal()]) {
                case 1:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 2:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 3:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 4:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 0;
                    return;
                case 5:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 0;
                    return;
                case 6:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 7:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 8:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 9:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 0;
                    return;
                case 10:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
        }
    }

    public void p(String str) {
        try {
            switch (f.l[IMoblieModelConfig.GIONEE.valueOf(r(str)).ordinal()]) {
                case 1:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                case 2:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 1;
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
        }
    }

    public void q(String str) {
        try {
            switch (f.m[IMoblieModelConfig.YULONG.valueOf(r(str)).ordinal()]) {
                case 1:
                case 2:
                case 3:
                case 4:
                    d.c = false;
                    return;
                case 5:
                    d.d = false;
                    d.e[0] = 0;
                    d.e[1] = 6;
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
        }
    }

    private String r(String str) {
        return new StringBuffer("_").append(str.replaceAll("[-\\s]", "_").toUpperCase()).toString();
    }
}
