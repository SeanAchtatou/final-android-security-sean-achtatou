package com.tencent.assistant.activity;

import android.os.Build;
import com.tencent.assistant.Global;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.t;
import com.tencent.beacon.event.a;
import java.util.HashMap;

/* compiled from: ProGuard */
class bm implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ InstalledAppManagerActivity f424a;

    bm(InstalledAppManagerActivity installedAppManagerActivity) {
        this.f424a = installedAppManagerActivity;
    }

    public void run() {
        for (LocalApkInfo localApkInfo : this.f424a.v) {
            HashMap hashMap = new HashMap();
            hashMap.put("B1", localApkInfo.mPackageName);
            hashMap.put("B2", localApkInfo.mAppName);
            hashMap.put("B3", Build.MANUFACTURER);
            hashMap.put("B4", Global.getPhoneGuidAndGen());
            hashMap.put("B5", Global.getQUAForBeacon());
            hashMap.put("B6", t.g());
            a.a("PreInstallAppList", true, -1, -1, hashMap, false);
        }
    }
}
