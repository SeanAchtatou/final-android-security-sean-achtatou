package com.tencent.assistant.activity;

import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.e;
import com.tencent.nucleus.manager.spaceclean.SpaceScanManager;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.manager.ak;
import com.tencent.pangu.utils.installuninstall.p;

/* compiled from: ProGuard */
class a extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkMgrActivity f385a;

    a(ApkMgrActivity apkMgrActivity) {
        this.f385a = apkMgrActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.localres.ab.a(com.tencent.assistant.localres.model.LocalApkInfo, boolean):void
     arg types: [com.tencent.assistant.localres.model.LocalApkInfo, int]
     candidates:
      com.tencent.assistant.localres.ab.a(com.tencent.assistant.localres.ab, java.util.List):void
      com.tencent.assistant.localres.ab.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, java.lang.String):void
      com.tencent.assistant.localres.ab.a(boolean, int):void
      com.tencent.assistant.localres.ab.a(java.lang.String, int):void
      com.tencent.assistant.localres.ab.a(com.tencent.assistant.localres.model.LocalApkInfo, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.download.a.a(com.tencent.pangu.download.DownloadInfo, boolean):boolean
     arg types: [com.tencent.pangu.download.DownloadInfo, int]
     candidates:
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.a, java.util.List):java.util.List
      com.tencent.pangu.download.a.a(java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>, boolean):void
      com.tencent.pangu.download.a.a(long, long):boolean
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.a, boolean):boolean
      com.tencent.pangu.download.a.a(java.lang.String, int):boolean
      com.tencent.pangu.download.a.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, com.tencent.assistantv2.st.model.StatInfo):int
      com.tencent.pangu.download.a.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo):void
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.DownloadInfo, com.tencent.pangu.download.SimpleDownloadInfo$UIType):void
      com.tencent.pangu.download.a.a(java.lang.String, com.tencent.cloud.a.f):void
      com.tencent.pangu.download.a.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.download.a.a(java.lang.String, boolean):boolean
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.DownloadInfo, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, boolean):boolean
     arg types: [com.tencent.assistant.activity.ApkMgrActivity, int]
     candidates:
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long):long
      com.tencent.assistant.activity.ApkMgrActivity.a(long, int):void
      com.tencent.assistant.activity.ApkMgrActivity.a(long, boolean):void
      com.tencent.assistant.activity.ApkMgrActivity.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, boolean):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, boolean):void
     arg types: [com.tencent.assistant.activity.ApkMgrActivity, long, int]
     candidates:
      com.tencent.assistant.activity.ApkMgrActivity.a(int, long, boolean):void
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, int):void
      com.tencent.assistant.activity.BaseActivity.a(com.tencent.assistantv2.st.page.STPageInfo, java.lang.String, java.lang.String):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, boolean, boolean):android.support.v4.app.z
      android.support.v4.app.FragmentActivity.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.ApkMgrActivity.b(com.tencent.assistant.activity.ApkMgrActivity, boolean):boolean
     arg types: [com.tencent.assistant.activity.ApkMgrActivity, int]
     candidates:
      com.tencent.assistant.activity.ApkMgrActivity.b(com.tencent.assistant.activity.ApkMgrActivity, long):long
      com.tencent.assistant.activity.ApkMgrActivity.b(com.tencent.assistant.activity.ApkMgrActivity, boolean):boolean */
    public void handleMessage(Message message) {
        switch (message.what) {
            case 110001:
                if (message.obj != null && (message.obj instanceof LocalApkInfo)) {
                    LocalApkInfo localApkInfo = (LocalApkInfo) message.obj;
                    if (!e.d(localApkInfo.mLocalFilePath)) {
                        Toast.makeText(this.f385a.u, (int) R.string.apkmgr_apk_not_exist, 0).show();
                        this.f385a.B.a(localApkInfo, false);
                        this.f385a.a(this.f385a.B.f(), false, false, 2, true);
                        return;
                    }
                    localApkInfo.mApkState = 2;
                    String ddownloadTicket = ApkResourceManager.getInstance().getDdownloadTicket(localApkInfo);
                    DownloadInfo d = DownloadProxy.a().d(ddownloadTicket);
                    if (d == null) {
                        d = new DownloadInfo();
                        d.packageName = localApkInfo != null ? localApkInfo.mPackageName : null;
                        d.versionCode = localApkInfo != null ? localApkInfo.mVersionCode : 0;
                        d.scene = STConst.ST_PAGE_APK_MANAGER;
                    }
                    if (ak.a().c(localApkInfo.mPackageName)) {
                        com.tencent.pangu.download.a.a().a(d, false);
                        return;
                    } else {
                        p.a().a(ddownloadTicket, localApkInfo.mPackageName, localApkInfo.mAppName, localApkInfo.mLocalFilePath, localApkInfo.mVersionCode, localApkInfo.signature, ddownloadTicket, localApkInfo.occupySize, false, d);
                        return;
                    }
                } else {
                    return;
                }
            case 110002:
                TemporaryThreadManager.get().start(new b(this, (LocalApkInfo) message.obj));
                return;
            case 110003:
                this.f385a.x.a(this.f385a.B.d(), true, false);
                return;
            case 110004:
            case 110005:
                this.f385a.B.a((LocalApkInfo) message.obj);
                return;
            case 110006:
            case 110008:
            default:
                return;
            case 110007:
                this.f385a.B.c();
                return;
            case 110009:
                boolean unused = this.f385a.N = true;
                long unused2 = this.f385a.Q = ((Long) message.obj).longValue();
                if (this.f385a.N) {
                    boolean unused3 = this.f385a.N = false;
                    this.f385a.a(this.f385a.Q, true);
                    return;
                }
                return;
            case 110010:
                boolean unused4 = this.f385a.P = true;
                return;
            case 110011:
                boolean unused5 = this.f385a.O = true;
                SpaceScanManager.a().h();
                return;
        }
    }
}
