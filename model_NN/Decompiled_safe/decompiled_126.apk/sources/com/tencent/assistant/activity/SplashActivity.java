package com.tencent.assistant.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.DownloadProgressButton;
import com.tencent.assistant.component.SecondNavigationTitleView;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.SplashManager;
import com.tencent.assistant.model.k;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.f;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.activity.GuideActivity;
import com.tencent.assistantv2.st.business.LaunchSpeedSTManager;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.link.b;
import com.tencent.pangu.manager.x;
import com.tencent.pangu.utils.d;

/* compiled from: ProGuard */
public class SplashActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f383a = false;
    private final int b = 3;
    private boolean c = false;
    private k d = null;
    private boolean e = false;
    private SplashScreenReceiver f = null;
    private ImageView g = null;
    /* access modifiers changed from: private */
    public Button h;
    private ImageView i;
    /* access modifiers changed from: private */
    public cz j;
    private int k;

    public int a() {
        if (this.d == null || this.d.r() != 1) {
            return STConst.ST_PAGE_SPLASH;
        }
        return STConst.ST_PAGE_SPLASH_URL;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        boolean z = true;
        super.onCreate(bundle);
        LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.Splash_onCreate_Begin);
        AstApp.i().a();
        Bundle a2 = d.a(getIntent());
        if (a2 != null) {
            String string = a2.getString("appCustom");
            if (!TextUtils.isEmpty(string)) {
                b.a(this, string);
                finish();
                return;
            }
        }
        x.a().b();
        AstApp.i().e();
        f.a((byte) 1);
        if (m.a().a("key_has_loaded_5_2", false)) {
            z = false;
        }
        f383a = z;
        if (z) {
            TemporaryThreadManager.get().start(new ct(this));
            Intent intent = new Intent(this, GuideActivity.class);
            intent.putExtra("from_activity", SplashActivity.class.getSimpleName());
            startActivity(intent);
            finish();
            return;
        }
        b();
        LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.Splash_onCreate_End);
    }

    /* compiled from: ProGuard */
    public class SplashScreenReceiver extends BroadcastReceiver {
        public SplashScreenReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            SplashActivity.this.c();
        }
    }

    public void a(int i2) {
        if (this.d != null && this.d.r() == 1) {
            l.a(new STInfoV2(a(), SecondNavigationTitleView.TMA_ST_NAVBAR_SEARCH_TAG, i2, STConst.ST_DEFAULT_SLOT, 100));
        }
        l.a(new STInfoV2(a(), STConst.ST_DEFAULT_SLOT, i2, STConst.ST_DEFAULT_SLOT, 100));
    }

    @SuppressLint({"NewApi"})
    private void b() {
        int i2;
        SplashManager a2 = SplashManager.a();
        this.d = a2.b();
        if (this.d != null) {
            XLog.d("splashInfo", "当前闪屏信息: " + this.d);
            try {
                View inflate = getLayoutInflater().inflate((int) R.layout.splash_for_operation, (ViewGroup) null);
                setContentView(inflate);
                this.g = (ImageView) inflate.findViewById(R.id.splash_img);
                this.h = (Button) findViewById(R.id.btn_jump);
                this.i = (ImageView) findViewById(R.id.btn_join);
                Bitmap c2 = a2.c();
                if (c2 == null || c2.isRecycled()) {
                    c();
                    return;
                }
                Bitmap d2 = a2.d();
                if (this.d.r() != 1 || (d2 != null && !d2.isRecycled())) {
                    this.g.setImageBitmap(c2);
                    getWindow().setFlags(1024, 1024);
                    this.c = true;
                    this.f = new SplashScreenReceiver();
                    IntentFilter intentFilter = new IntentFilter();
                    intentFilter.addAction("android.intent.action.SCREEN_OFF");
                    AstApp.i().registerReceiver(this.f, intentFilter);
                    if (this.d.r() == 0) {
                        this.g.setOnClickListener(new cu(this));
                    }
                    this.k = getIntent().getIntExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, 2000);
                    a(this.k);
                    TemporaryThreadManager.get().start(new cv(this));
                    int f2 = this.d.f() * 1000;
                    if (f2 <= 0) {
                        i2 = 3000;
                    } else {
                        i2 = f2;
                    }
                    this.j = new cz(this, (long) i2, 1000);
                    this.j.start();
                    if (this.d.r() == 1 && a2.d() != null) {
                        String t = this.d.t();
                        if (TextUtils.isEmpty(t)) {
                            c();
                            return;
                        }
                        this.h.setVisibility(0);
                        this.h.setText(String.format(AstApp.i().getResources().getString(R.string.jump), Integer.valueOf(i2 / 1000)));
                        this.h.setOnClickListener(new cw(this));
                        int q = this.d.q();
                        int p = this.d.p();
                        int n = this.d.n();
                        int o = this.d.o();
                        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.i.getLayoutParams();
                        if (q < 0 || q > by.b()) {
                            q = by.a(AstApp.i(), 50.0f);
                        }
                        if (p < 0 || p > by.c()) {
                            p = by.a(AstApp.i(), 30.0f);
                        }
                        layoutParams.width = q;
                        layoutParams.height = p;
                        XLog.d("splashInfo", "btnWidth = " + q + ", btnHeight = " + p);
                        if (n < 0 || n > by.b()) {
                            XLog.d("splashInfo", "btnMarginLeft error");
                            layoutParams.addRule(14, -1);
                        } else {
                            layoutParams.leftMargin = n;
                        }
                        if (o < 0 || o > by.c()) {
                            XLog.d("splashInfo", "btnMarginBottom error");
                            by.a(AstApp.i(), 30.0f);
                        } else {
                            layoutParams.bottomMargin = o;
                        }
                        this.i.setLayoutParams(layoutParams);
                        this.i.setVisibility(0);
                        try {
                            BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), a2.d());
                            if (Build.VERSION.SDK_INT < 16) {
                                this.i.setBackgroundDrawable(bitmapDrawable);
                            } else {
                                this.i.setBackground(bitmapDrawable);
                            }
                        } catch (Exception e2) {
                            XLog.e("splashInfo", e2.toString());
                            e2.printStackTrace();
                            this.i.setVisibility(8);
                            c();
                        }
                        this.i.setOnClickListener(new cx(this, t));
                        l.a(new STInfoV2(a(), DownloadProgressButton.TMA_ST_NAVBAR_DOWNLOAD_TAG, this.k, STConst.ST_DEFAULT_SLOT, 100));
                        return;
                    }
                    return;
                }
                c();
            } catch (Throwable th) {
                c();
            }
        } else {
            XLog.d("splashInfo", "currentSplashInfo is null . ");
            if (!this.c) {
                c();
                LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.START_UP_TYPE.main);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.e = true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.e) {
            c();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        d();
        super.onDestroy();
        if (this.f != null) {
            try {
                AstApp.i().unregisterReceiver(this.f);
            } catch (Exception e2) {
            }
        }
        if (this.j != null) {
            this.j.cancel();
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || !this.c) {
            return false;
        }
        c();
        return true;
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.j != null) {
            this.j.cancel();
        }
        int i2 = 2000;
        if (this.c) {
            if (this.d.g() > 0) {
                SplashManager.a().a(this.d);
            }
            this.c = false;
            i2 = a();
        }
        Intent intent = new Intent();
        intent.setClassName(AstApp.i().getPackageName(), "com.tencent.assistantv2.activity.MainActivity");
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, Integer.valueOf(i2));
        intent.addFlags(67108864);
        try {
            startActivity(intent);
        } catch (Throwable th) {
            th.printStackTrace();
        }
        if (!AstApp.r()) {
            TemporaryThreadManager.get().start(new cy(this));
        }
        finish();
    }

    private void d() {
        try {
            if (this.g != null && this.g.getDrawable() != null) {
                Drawable drawable = this.g.getDrawable();
                drawable.setCallback(null);
                if (drawable instanceof BitmapDrawable) {
                    Bitmap bitmap = ((BitmapDrawable) this.g.getDrawable()).getBitmap();
                    this.g.setImageDrawable(null);
                    this.g.setBackgroundDrawable(null);
                    this.g.setImageBitmap(null);
                    if (bitmap != null && !bitmap.isRecycled()) {
                        bitmap.recycle();
                    }
                }
            }
        } catch (Throwable th) {
        }
    }
}
