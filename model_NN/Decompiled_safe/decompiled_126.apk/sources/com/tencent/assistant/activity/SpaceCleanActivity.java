package com.tencent.assistant.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.view.KeyEvent;
import android.view.ViewStub;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.component.FooterView;
import com.tencent.assistant.component.NormalErrorPage;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.plugin.PluginStartEntry;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.e;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.manager.MobileManagerInstallActivity;
import com.tencent.nucleus.manager.a;
import com.tencent.nucleus.manager.component.ScaningProgressView;
import com.tencent.nucleus.manager.component.TxManagerCommContainView;
import com.tencent.nucleus.manager.main.AssistantTabActivity;
import com.tencent.nucleus.manager.spaceclean.RubbishResultListView;
import com.tencent.nucleus.manager.spaceclean.SpaceScanManager;
import com.tencent.nucleus.manager.spaceclean.SubRubbishInfo;
import com.tencent.nucleus.manager.spaceclean.ad;
import com.tencent.nucleus.manager.spaceclean.n;
import com.tencent.tmsecurelite.commom.b;
import com.tencent.tmsecurelite.optimize.f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;

/* compiled from: ProGuard */
public class SpaceCleanActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public FooterView A = null;
    /* access modifiers changed from: private */
    public TxManagerCommContainView B;
    /* access modifiers changed from: private */
    public ScaningProgressView C;
    private RubbishResultListView D;
    private ViewStub E = null;
    private NormalErrorPage F = null;
    /* access modifiers changed from: private */
    public Map<Integer, ArrayList<n>> G = new HashMap();
    /* access modifiers changed from: private */
    public int H = 0;
    /* access modifiers changed from: private */
    public volatile long I = 0;
    /* access modifiers changed from: private */
    public long J = 0;
    private long K = 0;
    /* access modifiers changed from: private */
    public long L = 0;
    /* access modifiers changed from: private */
    public boolean M = true;
    /* access modifiers changed from: private */
    public boolean N = false;
    /* access modifiers changed from: private */
    public boolean O = false;
    /* access modifiers changed from: private */
    public boolean P = false;
    /* access modifiers changed from: private */
    public PluginStartEntry Q = null;
    /* access modifiers changed from: private */
    public long R = 0;
    /* access modifiers changed from: private */
    public long S = 0;
    /* access modifiers changed from: private */
    public long T = 0;
    private long U = 0;
    /* access modifiers changed from: private */
    public long V = 0;
    private boolean W = false;
    private int X = 0;
    /* access modifiers changed from: private */
    public Handler Y = new ck(this);
    /* access modifiers changed from: private */
    public ad Z = new co(this);
    private a aa = new cq(this);
    /* access modifiers changed from: private */
    public f ab = new cr(this);
    /* access modifiers changed from: private */
    public b ac = new ce(this);
    private final int n = 1;
    private final int u = -1;
    private final int v = -2;
    /* access modifiers changed from: private */
    public Context w;
    private boolean x = false;
    /* access modifiers changed from: private */
    public boolean y = false;
    private SecondNavigationTitleViewV5 z;

    static /* synthetic */ long h(SpaceCleanActivity spaceCleanActivity, long j) {
        long j2 = spaceCleanActivity.I + j;
        spaceCleanActivity.I = j2;
        return j2;
    }

    static /* synthetic */ long i(SpaceCleanActivity spaceCleanActivity, long j) {
        long j2 = spaceCleanActivity.J + j;
        spaceCleanActivity.J = j2;
        return j2;
    }

    static /* synthetic */ int w(SpaceCleanActivity spaceCleanActivity) {
        int i = spaceCleanActivity.H;
        spaceCleanActivity.H = i + 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            setContentView((int) R.layout.activity_rubbish_clear);
            this.w = this;
            try {
                x();
                v();
                t();
                y();
                u();
            } catch (Throwable th) {
                this.W = true;
                finish();
                t.a().b();
            }
        } catch (Throwable th2) {
            this.W = true;
            finish();
            t.a().b();
        }
    }

    private void u() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", com.tencent.assistant.utils.t.g());
        XLog.d("beacon", "beacon report >> expose_spaceclean. " + hashMap.toString());
        com.tencent.beacon.event.a.a("expose_spaceclean", true, -1, -1, hashMap, true);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.W) {
            super.onResume();
            return;
        }
        if (this.z != null) {
            this.z.l();
        }
        super.onResume();
        this.M = true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.W) {
            super.onPause();
            return;
        }
        this.M = false;
        if (this.z != null) {
            this.z.m();
        }
        super.onPause();
    }

    private void v() {
        SpaceScanManager.a().a(this.aa);
    }

    private void w() {
        SpaceScanManager.a().b(this.aa);
    }

    private void x() {
        this.z = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.z.a(this);
        this.z.b(getString(R.string.rubbish_clear_title));
        this.z.d();
        this.z.c(new cd(this));
        this.B = (TxManagerCommContainView) findViewById(R.id.contentView);
        this.C = new ScaningProgressView(this.w);
        this.D = new RubbishResultListView(this.w);
        this.D.a(this.Y);
        this.B.a(this.C);
        this.B.b(this.D);
        this.A = new FooterView(this.w);
        this.B.a(this.A, new RelativeLayout.LayoutParams(-1, -2));
        this.A.updateContent(getString(R.string.rubbish_clear_one_key_delete));
        this.A.setFooterViewEnable(false);
        this.A.setOnFooterViewClickListener(new cj(this));
        this.E = (ViewStub) findViewById(R.id.error_stub);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    public void t() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.x = extras.getBoolean(com.tencent.assistant.a.a.O);
            if (this.s) {
                m.a().b("key_space_clean_last_push_clicked", (Object) true);
            }
            this.Q = (PluginStartEntry) extras.getSerializable("dock_plugin");
        }
    }

    /* access modifiers changed from: private */
    public void y() {
        XLog.d("miles", "initData called...");
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME);
        boolean z2 = false;
        if (localApkInfo == null) {
            z2 = e.a(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME);
        }
        if (localApkInfo == null && !z2) {
            b(Constants.STR_EMPTY);
        } else if (!SpaceScanManager.a().e()) {
            b(Constants.STR_EMPTY);
        } else {
            this.Y.sendEmptyMessage(1);
        }
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        Intent intent = new Intent(this, MobileManagerInstallActivity.class);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
        intent.putExtra("toast_message", str);
        startActivityForResult(intent, 1000);
    }

    /* access modifiers changed from: private */
    public void z() {
        this.y = true;
        this.U = System.currentTimeMillis();
        this.Y.sendEmptyMessageDelayed(30, 180000);
        TemporaryThreadManager.get().start(new cm(this));
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", ((this.L / 1024) / 1024) + Constants.STR_EMPTY);
        hashMap.put("B2", this.L + Constants.STR_EMPTY);
        hashMap.put("B3", str);
        hashMap.put("B4", Global.getPhoneGuidAndGen());
        hashMap.put("B5", Global.getQUAForBeacon());
        hashMap.put("B6", String.valueOf(this.X));
        XLog.d("beacon", "beacon report >> RubbishCleanTimeout. " + hashMap.toString());
        com.tencent.beacon.event.a.a("RubbishCleanTimeout", true, -1, -1, hashMap, true);
    }

    /* access modifiers changed from: private */
    public void A() {
        TemporaryThreadManager.get().start(new cn(this));
    }

    /* access modifiers changed from: private */
    public boolean a(PackageManager packageManager, String str) {
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, 0);
            if ((applicationInfo.flags & 1) == 0 || (applicationInfo.flags & 128) != 0) {
                return true;
            }
            return false;
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: private */
    public long a(n nVar) {
        long j = 0;
        if (this.G == null || this.G.size() <= 0) {
            return 0;
        }
        ArrayList arrayList = this.G.get(1);
        ArrayList arrayList2 = this.G.get(0);
        if (arrayList != null && arrayList.size() > 0) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                n nVar2 = (n) it.next();
                if (nVar != null && nVar.e.equals(nVar2.e)) {
                    nVar2.c = nVar.c;
                    nVar2.a(nVar.f);
                }
                j += nVar2.c;
            }
        }
        if (arrayList2 == null || arrayList2.size() <= 0) {
            return j;
        }
        Iterator it2 = arrayList2.iterator();
        long j2 = j;
        while (it2.hasNext()) {
            n nVar3 = (n) it2.next();
            if (nVar != null && nVar.b.equals(nVar3.b)) {
                nVar3.c = nVar.c;
                nVar3.a(nVar.f);
            }
            j2 += nVar3.c;
        }
        return j2;
    }

    /* access modifiers changed from: private */
    public ArrayList<String> B() {
        this.L = 0;
        this.X = 0;
        ArrayList<String> arrayList = new ArrayList<>();
        if (this.G != null && this.G.size() > 0) {
            ArrayList arrayList2 = this.G.get(1);
            ArrayList arrayList3 = this.G.get(0);
            if (arrayList2 != null && arrayList2.size() > 0) {
                Iterator it = arrayList2.iterator();
                while (it.hasNext()) {
                    Iterator<SubRubbishInfo> it2 = ((n) it.next()).f.iterator();
                    while (it2.hasNext()) {
                        SubRubbishInfo next = it2.next();
                        if (next.d) {
                            arrayList.addAll(next.f);
                            this.L += next.c;
                        }
                    }
                }
            }
            if (arrayList3 != null && arrayList3.size() > 0) {
                Iterator it3 = arrayList3.iterator();
                while (it3.hasNext()) {
                    Iterator<SubRubbishInfo> it4 = ((n) it3.next()).f.iterator();
                    while (it4.hasNext()) {
                        SubRubbishInfo next2 = it4.next();
                        if (next2.d) {
                            arrayList.addAll(next2.f);
                            this.L += next2.c;
                        }
                    }
                }
            }
        }
        this.X = arrayList.size();
        return arrayList;
    }

    /* access modifiers changed from: private */
    public void a(long j, boolean z2) {
        if (z2) {
            d(at.c(j));
        } else {
            b(1);
        }
    }

    private void d(String str) {
        STInfoV2 C2 = C();
        String string = getString(R.string.apkmgr_clear_success);
        String format = String.format(getString(R.string.rubbish_clear_delete_tips), str);
        String string2 = getString(R.string.rubbish_clear_delete_guide);
        SpannableString spannableString = new SpannableString(string2);
        spannableString.setSpan(new cs(this, new cp(this)), 0, string2.length(), 33);
        this.B.a(string, new SpannableString(format), spannableString);
        float c = (float) com.tencent.assistant.utils.t.c();
        float d = (float) com.tencent.assistant.utils.t.d();
        if (!(d == 0.0f || c == 0.0f)) {
            float f = c / d;
            XLog.d("miles", "SpaceCleanActivity >> internal avaliable memory percent is " + f);
            if (f < 0.2f) {
                this.B.a(R.drawable.icon_space_clean_xiaobao, getString(R.string.rubbish_skip_to_app_uninstall_tip), 20, 20, 4);
                this.B.a(this, InstalledAppManagerActivity.class, (PluginStartEntry) null);
                C2.slotId = "03_001";
                l.a(C2);
                e(getString(R.string.soft_admin));
                return;
            }
        }
        if (this.N && this.Q != null) {
            this.B.a(R.drawable.icon_space_clean_xiaobao, getString(R.string.rubbish_skip_to_accelerate_plugin_tip), 20, 20, 3);
            this.B.a(this, this.Q);
            C2.slotId = "03_001";
            l.a(C2);
            e(getString(R.string.mobile_accelerate_title));
        }
    }

    private void e(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", com.tencent.assistant.utils.t.g());
        hashMap.put("B4", getString(R.string.rubbish_clear_title));
        hashMap.put("B5", str);
        XLog.d("beacon", "beacon report >> featureLinkExp. " + hashMap.toString());
        com.tencent.beacon.event.a.a("featureLinkExp", true, -1, -1, hashMap, true);
    }

    private STInfoV2 C() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, 100);
        buildSTInfo.scene = STConst.ST_PAGE_RUBBISH_CLEAR_FINISH_STEWARD;
        com.tencent.assistantv2.st.b.b.getInstance().exposure(buildSTInfo);
        return buildSTInfo;
    }

    private void b(int i) {
        if (this.F == null) {
            this.E.inflate();
            this.F = (NormalErrorPage) findViewById(R.id.error);
        }
        this.F.setErrorType(i);
        if (i == 1) {
            this.F.setErrorHint(getResources().getString(R.string.rubbish_clear_empty_tips));
            this.F.setErrorImage(R.drawable.emptypage_pic_02);
            this.F.setErrorHintTextColor(getResources().getColor(R.color.common_listiteminfo));
            this.F.setErrorHintTextSize(getResources().getDimension(R.dimen.appadmin_empty_page_text_size));
            this.F.setErrorTextVisibility(8);
            this.F.setErrorHintVisibility(0);
            this.F.setFreshButtonVisibility(8);
        }
        this.B.setVisibility(8);
        this.A.setVisibility(8);
        this.F.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void D() {
        long j = 0;
        HashMap hashMap = new HashMap();
        hashMap.put("B1", String.valueOf(this.T - this.S));
        hashMap.put("B2", ((this.I / 1024) / 1024) + Constants.STR_EMPTY);
        hashMap.put("B3", this.I + Constants.STR_EMPTY);
        hashMap.put("B4", Global.getPhoneGuidAndGen());
        hashMap.put("B5", Global.getQUAForBeacon());
        hashMap.put("B6", com.tencent.assistant.utils.t.g());
        if (this.R != 0) {
            j = this.T - this.R;
        }
        hashMap.put("B7", String.valueOf(j));
        com.tencent.beacon.event.a.a("SpaceCleanScan", true, j, -1, hashMap, true);
        XLog.d("beacon", "beacon report >> event: SpaceCleanScan, totalTime : " + (this.T - this.R) + ", params : " + hashMap.toString());
    }

    /* access modifiers changed from: private */
    public void a(String str, boolean z2, int i) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", ((this.L / 1024) / 1024) + Constants.STR_EMPTY);
        hashMap.put("B2", this.L + Constants.STR_EMPTY);
        hashMap.put("B3", Global.getPhoneGuidAndGen());
        hashMap.put("B4", Global.getQUAForBeacon());
        hashMap.put("B5", com.tencent.assistant.utils.t.g());
        hashMap.put("B6", String.valueOf(this.V - this.U));
        hashMap.put("B7", String.valueOf(i));
        hashMap.put("B8", String.valueOf(this.X));
        hashMap.put("B9", String.valueOf(z2));
        com.tencent.beacon.event.a.a(str, z2, this.V - this.U, -1, hashMap, true);
        XLog.d("beacon", "beacon report >> event: " + str + ", totalTime : " + (this.V - this.U) + ", params : " + hashMap.toString());
    }

    /* access modifiers changed from: private */
    public void a(boolean z2, String str, long j, String str2, JSONArray jSONArray) {
        ArrayList arrayList;
        n nVar;
        n nVar2;
        SubRubbishInfo subRubbishInfo;
        ArrayList arrayList2 = this.G.get(1);
        if (arrayList2 == null) {
            ArrayList arrayList3 = new ArrayList();
            this.G.put(1, arrayList3);
            arrayList = arrayList3;
        } else {
            arrayList = arrayList2;
        }
        Iterator it = arrayList.iterator();
        while (true) {
            if (!it.hasNext()) {
                nVar = null;
                break;
            }
            nVar = (n) it.next();
            if (str.equals(nVar.e)) {
                break;
            }
        }
        if (nVar == null) {
            n nVar3 = new n();
            nVar3.f3037a = 1;
            nVar3.e = str;
            arrayList.add(nVar3);
            nVar2 = nVar3;
        } else {
            nVar2 = nVar;
        }
        Iterator<SubRubbishInfo> it2 = nVar2.f.iterator();
        while (true) {
            if (!it2.hasNext()) {
                subRubbishInfo = null;
                break;
            }
            subRubbishInfo = it2.next();
            if (str2.equals(subRubbishInfo.b) && z2 == subRubbishInfo.e) {
                break;
            }
        }
        if (subRubbishInfo == null) {
            nVar2.a(new SubRubbishInfo(str2, j, z2, jSONArray));
        } else {
            subRubbishInfo.c += j;
            subRubbishInfo.a(jSONArray);
        }
        nVar2.d += j;
        if (z2) {
            nVar2.c += j;
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z2, String str, long j, String str2, JSONArray jSONArray) {
        ArrayList arrayList;
        n nVar;
        n nVar2;
        SubRubbishInfo subRubbishInfo;
        ArrayList arrayList2 = this.G.get(0);
        if (arrayList2 == null) {
            ArrayList arrayList3 = new ArrayList();
            this.G.put(0, arrayList3);
            arrayList = arrayList3;
        } else {
            arrayList = arrayList2;
        }
        Iterator it = arrayList.iterator();
        while (true) {
            if (!it.hasNext()) {
                nVar = null;
                break;
            }
            nVar = (n) it.next();
            if (str.equals(nVar.b)) {
                break;
            }
        }
        if (nVar == null) {
            n nVar3 = new n();
            nVar3.f3037a = 0;
            nVar3.b = str;
            arrayList.add(nVar3);
            nVar2 = nVar3;
        } else {
            nVar2 = nVar;
        }
        Iterator<SubRubbishInfo> it2 = nVar2.f.iterator();
        while (true) {
            if (!it2.hasNext()) {
                subRubbishInfo = null;
                break;
            }
            subRubbishInfo = it2.next();
            if (str2.equals(subRubbishInfo.b)) {
                break;
            }
        }
        if (subRubbishInfo == null) {
            nVar2.a(new SubRubbishInfo(str2, j, z2, jSONArray));
        } else {
            subRubbishInfo.c += j;
            subRubbishInfo.a(jSONArray);
        }
        nVar2.d += j;
        if (z2) {
            nVar2.c += j;
        }
    }

    /* access modifiers changed from: private */
    public void a(long j, boolean z2, int i) {
        if (z2) {
            this.C.a(j);
        } else if (i == 1) {
            this.C.b(j);
            this.C.c();
            this.Y.post(new cf(this, i));
        } else if (i == 2) {
            this.C.a(j);
            this.Y.postDelayed(new ch(this), 1000);
        } else {
            this.C.b(j);
            this.Y.post(new ci(this));
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        if (z2) {
            this.D.setVisibility(8);
        } else if (this.G != null && !this.G.isEmpty()) {
            this.D.a(this.G);
            this.D.setVisibility(0);
            if (this.F != null) {
                this.F.setVisibility(8);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(long j) {
        String c = at.c(j);
        String string = getString(R.string.rubbish_clear_one_key_delete);
        if (j > 0) {
            this.A.setFooterViewEnable(true);
            this.A.updateContent(string, " " + String.format(getString(R.string.rubbish_clear_one_key_delete_extra), c));
            return;
        }
        this.A.setFooterViewEnable(false);
        this.A.updateContent(string);
    }

    public int f() {
        return STConst.ST_PAGE_GARBAGE_INSTALL_STEWARD;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.W) {
            super.onDestroy();
            return;
        }
        w();
        com.tencent.assistant.utils.b.a();
        if (this.x && this.y) {
            m.a().b("key_space_clean_has_run_clean", (Object) true);
        }
        if (this.Y != null) {
            this.Y.removeMessages(30);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        XLog.d("miles", "onActivityResult called. requestCode = " + i + ", resultCode = " + i2);
        if (i != 1000) {
            return;
        }
        if (i2 == 0) {
            finish();
        } else if (i2 == -1) {
            y();
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (4 != i) {
            return super.onKeyDown(i, keyEvent);
        }
        E();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void E() {
        if (!this.s) {
            finish();
            return;
        }
        Intent intent = new Intent(this, AssistantTabActivity.class);
        intent.setFlags(67108864);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
        intent.putExtra(com.tencent.assistant.a.a.G, true);
        startActivity(intent);
        this.s = false;
        finish();
        XLog.i("miles", "SpaceCleanActivity >> key back finish");
    }
}
