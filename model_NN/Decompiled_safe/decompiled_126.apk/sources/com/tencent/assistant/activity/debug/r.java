package com.tencent.assistant.activity.debug;

import android.view.View;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class r implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DActivity f517a;

    r(DActivity dActivity) {
        this.f517a = dActivity;
    }

    public void onClick(View view) {
        TemporaryThreadManager.get().start(new s(this));
    }
}
