package com.tencent.assistant.activity;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.connector.ipc.a;

/* compiled from: ProGuard */
class bv implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PhotoBackupNewActivity f433a;

    bv(PhotoBackupNewActivity photoBackupNewActivity) {
        this.f433a = photoBackupNewActivity;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel /*2131165503*/:
                this.f433a.d(5);
                return;
            case R.id.ok /*2131165504*/:
                this.f433a.d(5);
                a.a().i();
                return;
            case R.id.no_backup /*2131165785*/:
                this.f433a.d(2);
                this.f433a.finish();
                return;
            case R.id.connect_pc /*2131165786*/:
                this.f433a.d(2);
                this.f433a.v();
                return;
            case R.id.cancel_backup /*2131165796*/:
                this.f433a.d(1);
                if (!a.a().d()) {
                    this.f433a.finish();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
