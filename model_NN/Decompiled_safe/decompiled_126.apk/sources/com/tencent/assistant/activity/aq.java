package com.tencent.assistant.activity;

import android.view.KeyEvent;
import android.view.View;

/* compiled from: ProGuard */
class aq implements View.OnKeyListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BaseActivity f402a;

    aq(BaseActivity baseActivity) {
        this.f402a = baseActivity;
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if ((i != 82 && i != 4) || this.f402a.w == null || !this.f402a.w.isShowing()) {
            return false;
        }
        this.f402a.w.dismiss();
        return false;
    }
}
