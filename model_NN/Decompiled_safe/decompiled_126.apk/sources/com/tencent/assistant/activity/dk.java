package com.tencent.assistant.activity;

import android.os.Message;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class dk extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StartScanActivity f531a;

    private dk(StartScanActivity startScanActivity) {
        this.f531a = startScanActivity;
    }

    /* synthetic */ dk(StartScanActivity startScanActivity, da daVar) {
        this(startScanActivity);
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        if (localApkInfo != null) {
            Message obtainMessage = this.f531a.ac.obtainMessage();
            obtainMessage.obj = localApkInfo;
            obtainMessage.arg1 = i;
            switch (i) {
                case 1:
                    obtainMessage.what = 1;
                    this.f531a.ac.removeMessages(1);
                    break;
                case 2:
                    obtainMessage.what = 2;
                    this.f531a.ac.removeMessages(2);
                    break;
            }
            this.f531a.ac.sendMessage(obtainMessage);
        }
    }
}
