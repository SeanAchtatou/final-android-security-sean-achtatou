package com.tencent.assistant.activity.debug;

import android.view.View;
import android.widget.AdapterView;
import com.tencent.assistant.Global;
import com.tencent.assistant.m;

/* compiled from: ProGuard */
class az implements AdapterView.OnItemSelectedListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ServerAdressSettingActivity f499a;

    az(ServerAdressSettingActivity serverAdressSettingActivity) {
        this.f499a = serverAdressSettingActivity;
    }

    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
        if (i != this.f499a.h) {
            m.a().a(this.f499a.d = this.f499a.g + i);
            this.f499a.f472a.setSelection(this.f499a.g);
            this.f499a.e.setText("当前：" + Global.getServerAddressName());
            this.f499a.d();
        }
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }
}
