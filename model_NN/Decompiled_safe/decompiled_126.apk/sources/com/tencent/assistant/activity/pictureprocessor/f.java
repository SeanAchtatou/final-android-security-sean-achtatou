package com.tencent.assistant.activity.pictureprocessor;

import android.view.animation.Animation;

/* compiled from: ProGuard */
class f implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PicView f554a;

    f(PicView picView) {
        this.f554a = picView;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.pictureprocessor.PicView.a(com.tencent.assistant.activity.pictureprocessor.PicView, boolean):boolean
     arg types: [com.tencent.assistant.activity.pictureprocessor.PicView, int]
     candidates:
      com.tencent.assistant.activity.pictureprocessor.PicView.a(com.tencent.assistant.activity.pictureprocessor.PicView, com.tencent.assistant.thumbnailCache.o):void
      com.tencent.assistant.activity.pictureprocessor.PicView.a(java.lang.String, com.tencent.assistant.activity.pictureprocessor.g):void
      com.tencent.assistant.activity.pictureprocessor.PicView.a(com.tencent.assistant.activity.pictureprocessor.PicView, boolean):boolean */
    public void onAnimationEnd(Animation animation) {
        boolean unused = this.f554a.o = false;
        if (this.f554a.m != null) {
            this.f554a.k.obtainMessage(0, this.f554a.m).sendToTarget();
        } else if (this.f554a.n) {
            this.f554a.k.sendEmptyMessageDelayed(1, 0);
        }
    }
}
