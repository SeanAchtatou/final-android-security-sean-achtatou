package com.tencent.assistant.activity;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAClickListener;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class df extends OnTMAClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StartScanActivity f526a;

    df(StartScanActivity startScanActivity) {
        this.f526a = startScanActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.StartScanActivity.b(com.tencent.assistant.activity.StartScanActivity, java.lang.Boolean):java.lang.Boolean
     arg types: [com.tencent.assistant.activity.StartScanActivity, int]
     candidates:
      com.tencent.assistant.activity.StartScanActivity.b(com.tencent.assistant.activity.StartScanActivity, int):int
      com.tencent.assistant.activity.StartScanActivity.b(com.tencent.assistant.activity.StartScanActivity, java.lang.Boolean):java.lang.Boolean */
    public void onTMAClick(View view) {
        this.f526a.B.setVisibility(0);
        this.f526a.y.j();
        this.f526a.D.setVisibility(8);
        this.f526a.I.b();
        Boolean unused = this.f526a.T = (Boolean) true;
        TemporaryThreadManager.get().start(new dg(this));
    }
}
