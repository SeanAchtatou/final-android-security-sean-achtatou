package com.tencent.assistant.smartcard.component;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class y implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchSmartCardBaseItem f1742a;

    y(SearchSmartCardBaseItem searchSmartCardBaseItem) {
        this.f1742a = searchSmartCardBaseItem;
    }

    public void onClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f1742a.b(0));
        b.b(this.f1742a.f1693a, this.f1742a.d.o, bundle);
        this.f1742a.a("03_001", 200, this.f1742a.d.s, -1);
    }
}
