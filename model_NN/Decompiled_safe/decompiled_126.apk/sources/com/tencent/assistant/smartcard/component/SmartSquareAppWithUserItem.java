package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.d;
import com.tencent.assistant.smartcard.d.y;
import com.tencent.assistant.utils.at;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.j;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.component.appdetail.TXDwonloadProcessBar;
import com.tencent.pangu.component.appdetail.process.s;

/* compiled from: ProGuard */
public class SmartSquareAppWithUserItem extends RelativeLayout {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f1699a;
    private LayoutInflater b;
    private TXAppIconView c;
    private TextView d;
    private TextView e;
    private TextView f;
    private TXImageView g;
    private DownloadButton h;
    /* access modifiers changed from: private */
    public y i;
    private TXDwonloadProcessBar j;

    public SmartSquareAppWithUserItem(Context context) {
        this(context, null);
    }

    public SmartSquareAppWithUserItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f1699a = context;
        this.b = (LayoutInflater) this.f1699a.getSystemService("layout_inflater");
        a();
    }

    private void a() {
        this.b.inflate((int) R.layout.smartcard_square_item_app_with_user, this);
        this.c = (TXAppIconView) findViewById(R.id.icon);
        this.d = (TextView) findViewById(R.id.name);
        this.e = (TextView) findViewById(R.id.size);
        this.f = (TextView) findViewById(R.id.item_title);
        this.g = (TXImageView) findViewById(R.id.item_title_pic);
        this.h = (DownloadButton) findViewById(R.id.downloadButton);
        this.j = (TXDwonloadProcessBar) findViewById(R.id.progress);
    }

    public void a(y yVar, STInfoV2 sTInfoV2) {
        this.i = yVar;
        if (this.i != null) {
            if (this.i.f1768a != null) {
                this.c.updateImageView(this.i.f1768a.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            }
            this.d.setText(this.i.f1768a.d);
            this.e.setText(at.a(this.i.f1768a.k));
            this.f.setText(this.i.a());
            if (!TextUtils.isEmpty(this.i.c)) {
                this.g.setVisibility(0);
                this.g.updateImageView(this.i.c, R.drawable.card_avatar, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            } else {
                this.g.setVisibility(8);
            }
            this.h.a(this.i.f1768a);
            this.j.a(this.i.f1768a, new View[]{this.e});
            if (s.a(this.i.f1768a)) {
                this.h.setClickable(false);
            } else {
                this.h.setClickable(true);
                this.h.a(sTInfoV2, (j) null, (d) null, this.h, this.j);
            }
            setOnClickListener(new al(this, sTInfoV2));
        }
    }
}
