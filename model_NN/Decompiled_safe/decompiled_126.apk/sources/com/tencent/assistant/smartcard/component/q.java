package com.tencent.assistant.smartcard.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class q extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f1734a;
    final /* synthetic */ NormalSmartcardCpaAdvertiseItem b;

    q(NormalSmartcardCpaAdvertiseItem normalSmartcardCpaAdvertiseItem, SimpleAppModel simpleAppModel) {
        this.b = normalSmartcardCpaAdvertiseItem;
        this.f1734a = simpleAppModel;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.b.f1693a, AppDetailActivityV5.class);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.b.c(this.b.a(this.b.f1693a)));
        intent.putExtra("simpleModeInfo", this.f1734a);
        this.b.f1693a.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 a2 = this.b.a(STConst.ST_DEFAULT_SLOT, 200);
        if (a2 != null) {
            a2.updateStatusToDetail(this.f1734a);
        }
        return a2;
    }
}
