package com.tencent.assistant.smartcard.d;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.SmartCardTemplate3;
import com.tencent.assistant.protocol.jce.Template3App;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class j extends a {
    public k e;
    public List<i> f;
    public int g;
    public int h;
    public String i;

    public boolean a(byte b, JceStruct jceStruct) {
        if (!(jceStruct instanceof SmartCardTemplate3)) {
            return false;
        }
        SmartCardTemplate3 smartCardTemplate3 = (SmartCardTemplate3) jceStruct;
        this.e = new k();
        this.e.a(smartCardTemplate3.f1524a);
        this.f = new ArrayList();
        Iterator<Template3App> it = smartCardTemplate3.b.iterator();
        while (it.hasNext()) {
            i iVar = new i();
            iVar.a(it.next());
            this.f.add(iVar);
        }
        this.j = b;
        this.k = smartCardTemplate3.c;
        this.g = smartCardTemplate3.d;
        this.h = smartCardTemplate3.e;
        this.i = smartCardTemplate3.f;
        if (smartCardTemplate3.f1524a != null) {
            this.o = smartCardTemplate3.f1524a.d;
        }
        return true;
    }

    public List<SimpleAppModel> a() {
        if (this.f == null || this.f.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (i next : this.f) {
            if (next.f1755a != null) {
                arrayList.add(next.f1755a);
            }
        }
        return arrayList;
    }

    public x c() {
        if (this.g <= 0 || this.h <= 0) {
            return null;
        }
        x xVar = new x();
        xVar.f = this.k;
        xVar.e = this.j;
        xVar.f1767a = this.g;
        xVar.b = this.h;
        return xVar;
    }

    public void a(List<Long> list) {
        if (list != null && list.size() != 0 && this.f != null && this.f.size() != 0) {
            ArrayList arrayList = new ArrayList();
            for (i next : this.f) {
                if (list.contains(Long.valueOf(next.f1755a.f938a))) {
                    arrayList.add(next);
                }
            }
            this.f.removeAll(arrayList);
        }
    }

    public void b() {
        if (this.f != null && this.f.size() > 0) {
            Iterator<i> it = this.f.iterator();
            while (it.hasNext()) {
                i next = it.next();
                if (!(next.f1755a == null || ApkResourceManager.getInstance().getInstalledApkInfo(next.f1755a.c) == null)) {
                    it.remove();
                }
            }
        }
    }
}
