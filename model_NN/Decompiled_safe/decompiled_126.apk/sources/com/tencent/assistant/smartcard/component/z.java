package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.cloud.model.SimpleEbookModel;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class z extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchSmartCardEbookRelateItem f1743a;
    private Context b;
    private int c;
    private SimpleEbookModel d;

    public z(SearchSmartCardEbookRelateItem searchSmartCardEbookRelateItem, Context context, int i, SimpleEbookModel simpleEbookModel) {
        this.f1743a = searchSmartCardEbookRelateItem;
        this.b = context;
        this.c = i;
        this.d = simpleEbookModel;
    }

    public void onTMAClick(View view) {
        if (this.d != null) {
            b.a(this.b, this.d.l);
        }
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f1743a.getContext(), 200);
        buildSTInfo.slotId = a.a(Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE, this.c);
        if (this.f1743a.p != null) {
            buildSTInfo.searchId = this.f1743a.p.c();
            buildSTInfo.extraData = this.f1743a.p.d() + ";" + this.d.f2318a;
        }
        buildSTInfo.status = "0" + (this.c + 2);
        return buildSTInfo;
    }
}
