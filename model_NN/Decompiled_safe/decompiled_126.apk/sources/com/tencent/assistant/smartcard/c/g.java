package com.tencent.assistant.smartcard.c;

import com.tencent.assistant.smartcard.d.l;
import com.tencent.assistant.smartcard.d.n;
import java.util.List;

/* compiled from: ProGuard */
public class g extends z {
    public boolean a(n nVar, List<Long> list) {
        if (nVar == null || !(nVar instanceof l)) {
            return false;
        }
        l lVar = (l) nVar;
        if (!((Boolean) b(nVar).first).booleanValue()) {
            return false;
        }
        if (lVar.b() < 5) {
            return false;
        }
        return true;
    }
}
