package com.tencent.assistant.smartcard.d;

import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.SmartCardRecommandation;
import com.tencent.assistant.protocol.jce.SmartCardRecommandedItem;
import com.tencent.assistant.protocol.jce.SmartCardRecommandedPerson;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class p extends n {

    /* renamed from: a  reason: collision with root package name */
    public long f1759a;
    public int b = 0;
    public List<q> c;
    public int d;
    public int e;

    public boolean a(int i, SmartCardRecommandation smartCardRecommandation) {
        if (smartCardRecommandation == null) {
            return false;
        }
        this.j = i;
        this.l = smartCardRecommandation.f1516a;
        this.f1759a = smartCardRecommandation.c;
        this.n = smartCardRecommandation.d;
        this.p = smartCardRecommandation.e;
        if (this.c == null) {
            this.c = new ArrayList();
        } else {
            this.c.clear();
        }
        if (smartCardRecommandation.b == null || smartCardRecommandation.b.size() <= 0) {
            return false;
        }
        Iterator<SmartCardRecommandedItem> it = smartCardRecommandation.b.iterator();
        while (it.hasNext()) {
            SmartCardRecommandedItem next = it.next();
            q qVar = new q();
            qVar.b = next.b;
            qVar.f1760a = k.a(next.f1517a);
            qVar.c = b(next.c);
            this.c.add(qVar);
        }
        return true;
    }

    public List<o> b(List<SmartCardRecommandedPerson> list) {
        ArrayList arrayList = new ArrayList();
        if (list != null && list.size() > 0) {
            for (SmartCardRecommandedPerson a2 : list) {
                o oVar = new o();
                oVar.a(a2);
                arrayList.add(oVar);
            }
        }
        return arrayList;
    }

    public List<Long> d() {
        ArrayList arrayList = new ArrayList();
        if (this.c != null && this.c.size() > 0) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.d || i2 >= this.c.size()) {
                    break;
                }
                arrayList.add(Long.valueOf(this.c.get(i2).f1760a.f938a));
                i = i2 + 1;
            }
        }
        return arrayList;
    }

    public void a(List<Long> list) {
        if (list != null && list.size() != 0 && this.c != null && this.c.size() != 0) {
            ArrayList arrayList = new ArrayList();
            for (q next : this.c) {
                if (list.contains(Long.valueOf(next.f1760a.f938a))) {
                    arrayList.add(next);
                }
            }
            this.c.removeAll(arrayList);
        }
    }
}
