package com.tencent.assistant.smartcard.a;

import android.content.Context;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.protocol.jce.SmartCardPicTemplate;
import com.tencent.assistant.smartcard.c.c;
import com.tencent.assistant.smartcard.c.z;
import com.tencent.assistant.smartcard.component.NormalSmartcardBaseItem;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistant.smartcard.d.g;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.view.GameSmartCardRegisterTemplateItem;

/* compiled from: ProGuard */
public class a extends c {
    public Class<? extends JceStruct> a() {
        return SmartCardPicTemplate.class;
    }

    public com.tencent.assistant.smartcard.d.a b() {
        return new g();
    }

    /* access modifiers changed from: protected */
    public z c() {
        return new com.tencent.assistant.smartcard.e.a();
    }

    public NormalSmartcardBaseItem a(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        return new GameSmartCardRegisterTemplateItem(context, nVar, asVar, iViewInvalidater);
    }
}
