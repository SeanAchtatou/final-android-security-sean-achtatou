package com.tencent.assistant.protocol.scu;

import com.tencent.assistant.protocol.a.e;
import com.tencent.assistant.protocol.jce.StatCSChannelData;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: ProGuard */
public class AppCertificateState {

    /* renamed from: a  reason: collision with root package name */
    private CertificateState f1621a = CertificateState.STATE_NONE;
    private ReferenceQueue<f> b = new ReferenceQueue<>();
    private ConcurrentLinkedQueue<WeakReference<f>> c = new ConcurrentLinkedQueue<>();

    /* compiled from: ProGuard */
    enum CertificateState {
        STATE_NONE,
        STATE_SUCCESS,
        STATE_FAIL,
        STATE_TIMEOUT
    }

    public void a(f fVar) {
        if (fVar != null) {
            while (true) {
                WeakReference weakReference = (WeakReference) this.b.poll();
                if (weakReference == null) {
                    break;
                }
                this.c.remove(weakReference);
            }
            Iterator<WeakReference<f>> it = this.c.iterator();
            while (it.hasNext()) {
                if (fVar == ((f) it.next().get())) {
                    return;
                }
            }
            this.c.add(new WeakReference(fVar));
        }
    }

    public boolean a() {
        return this.f1621a == CertificateState.STATE_SUCCESS;
    }

    public void a(CertificateState certificateState) {
        synchronized (this.f1621a) {
            CertificateState certificateState2 = this.f1621a;
            switch (c.f1626a[certificateState.ordinal()]) {
                case 1:
                    e();
                    break;
                case 2:
                    f();
                    break;
                case 3:
                    g();
                    break;
                case 4:
                    h();
                    break;
            }
        }
    }

    public void a(int i) {
        b(i);
    }

    private void e() {
        if (this.f1621a == CertificateState.STATE_FAIL || this.f1621a == CertificateState.STATE_TIMEOUT) {
            CertificateState certificateState = this.f1621a;
            this.f1621a = CertificateState.STATE_NONE;
            if (e.a().n()) {
                a(certificateState, this.f1621a);
            }
        }
    }

    private void f() {
        if (this.f1621a != CertificateState.STATE_SUCCESS) {
            CertificateState certificateState = this.f1621a;
            this.f1621a = CertificateState.STATE_SUCCESS;
            if (e.a().n()) {
                a(certificateState, this.f1621a);
            }
            i();
        }
    }

    private void g() {
        if (this.f1621a != CertificateState.STATE_FAIL) {
            CertificateState certificateState = this.f1621a;
            this.f1621a = CertificateState.STATE_FAIL;
            if (e.a().n()) {
                a(certificateState, this.f1621a);
            }
            j();
        }
    }

    private void h() {
        if (this.f1621a == CertificateState.STATE_SUCCESS) {
            CertificateState certificateState = this.f1621a;
            this.f1621a = CertificateState.STATE_TIMEOUT;
            if (e.a().n()) {
                a(certificateState, this.f1621a);
            }
            k();
        }
    }

    public void b() {
        a(CertificateState.STATE_SUCCESS);
    }

    public void c() {
        a(CertificateState.STATE_FAIL);
    }

    public void d() {
        a(CertificateState.STATE_TIMEOUT);
    }

    private void i() {
        if (e.a().n()) {
            a((byte) 1);
        }
        Iterator<WeakReference<f>> it = this.c.iterator();
        while (it.hasNext()) {
            f fVar = (f) it.next().get();
            if (fVar != null) {
                fVar.f();
            }
        }
    }

    private void j() {
        if (e.a().n()) {
            a((byte) 2);
        }
        Iterator<WeakReference<f>> it = this.c.iterator();
        while (it.hasNext()) {
            f fVar = (f) it.next().get();
            if (fVar != null) {
                fVar.e();
            }
        }
    }

    private void k() {
        if (e.a().n()) {
            a((byte) 3);
        }
        Iterator<WeakReference<f>> it = this.c.iterator();
        while (it.hasNext()) {
            f fVar = (f) it.next().get();
            if (fVar != null) {
                fVar.g();
            }
        }
    }

    private void b(int i) {
        if (e.a().n()) {
            a((byte) 4);
        }
        Iterator<WeakReference<f>> it = this.c.iterator();
        while (it.hasNext()) {
            f fVar = (f) it.next().get();
            if (fVar != null) {
                fVar.a(i);
            }
        }
    }

    private void a(CertificateState certificateState, CertificateState certificateState2) {
        StatCSChannelData statCSChannelData = new StatCSChannelData();
        statCSChannelData.b = 2;
        statCSChannelData.c = 3;
        statCSChannelData.d = 5;
        StringBuilder sb = new StringBuilder();
        sb.append("oldState:").append(certificateState).append(";");
        sb.append("newState:").append(certificateState2).append(";");
        statCSChannelData.g = sb.toString();
        e.a().a(statCSChannelData);
    }

    private void a(byte b2) {
        StatCSChannelData statCSChannelData = new StatCSChannelData();
        statCSChannelData.b = 2;
        statCSChannelData.c = 3;
        statCSChannelData.d = 6;
        StringBuilder sb = new StringBuilder();
        sb.append("NotifyType:").append((int) b2).append(";");
        statCSChannelData.g = sb.toString();
        e.a().a(statCSChannelData);
    }
}
