package com.tencent.assistant.d;

import android.content.Context;

/* compiled from: ProGuard */
final class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f738a;
    final /* synthetic */ long b;

    k(Context context, long j) {
        this.f738a = context;
        this.b = j;
    }

    public void run() {
        boolean unused = j.f737a = true;
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        j.c(this.f738a, this.b);
        boolean unused2 = j.f737a = false;
    }
}
