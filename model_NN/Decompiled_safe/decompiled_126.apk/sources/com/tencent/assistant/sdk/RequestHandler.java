package com.tencent.assistant.sdk;

import android.content.Context;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.text.TextUtils;
import com.tencent.assistant.sdk.a.d;
import com.tencent.assistant.utils.XLog;
import com.tencent.connect.common.Constants;
import com.tencent.d.a.a;
import com.tencent.d.a.e;
import java.util.concurrent.PriorityBlockingQueue;

/* compiled from: ProGuard */
public class RequestHandler {
    private static RequestHandler c;

    /* renamed from: a  reason: collision with root package name */
    private RemoteCallbackList<d> f1639a = new RemoteCallbackList<>();
    private RequestQueue b = new RequestQueue();

    private RequestHandler() {
        Thread thread = new Thread(this.b, "Thread_RequestHandler");
        thread.setDaemon(true);
        thread.start();
    }

    public static synchronized RequestHandler a() {
        RequestHandler requestHandler;
        synchronized (RequestHandler.class) {
            if (c == null) {
                c = new RequestHandler();
            }
            requestHandler = c;
        }
        return requestHandler;
    }

    public int a(byte b2, String str, String str2, String str3, d dVar) {
        byte[] a2;
        if (!a(b2, str2) || dVar == null || (a2 = new e().a(a.a(str3, 0), str2.getBytes())) == null) {
            return 2;
        }
        try {
            String str4 = new String(a2, "utf-8");
            if (TextUtils.isEmpty(str2) || str4 == null) {
                return 2;
            }
            if (System.currentTimeMillis() - Long.parseLong(str4) >= 60000 || !this.f1639a.register(dVar, str2)) {
                return 2;
            }
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return 2;
        }
    }

    private boolean a(byte b2, String str) {
        return b2 != 1 || str == null || !str.startsWith("__plugin_ipc_");
    }

    public int a(d dVar) {
        if (dVar == null || !this.f1639a.unregister(dVar)) {
            return 2;
        }
        return 1;
    }

    public byte[] a(byte b2, Context context, String str, String str2, byte[] bArr) {
        p a2;
        if (!a(b2, str2) || (a2 = q.a(context, bArr, Constants.STR_EMPTY)) == null) {
            return null;
        }
        SDKIPCBroadcaster.a().a(str2, a2);
        return a2.c();
    }

    public void b(byte b2, Context context, String str, String str2, byte[] bArr) {
        if (!TextUtils.isEmpty(str2) && bArr != null && a(b2, str2)) {
            this.b.a(context, str2, bArr, str);
        }
    }

    public synchronized void a(String str, byte[] bArr) {
        d broadcastItem;
        if (!TextUtils.isEmpty(str) && bArr != null) {
            int beginBroadcast = this.f1639a.beginBroadcast();
            while (beginBroadcast > 0) {
                int i = beginBroadcast - 1;
                try {
                    String str2 = (String) this.f1639a.getBroadcastCookie(i);
                    if (!TextUtils.isEmpty(str2) && str2.equals(str) && (broadcastItem = this.f1639a.getBroadcastItem(i)) != null) {
                        broadcastItem.a(bArr);
                    }
                    beginBroadcast = i;
                } catch (RemoteException e) {
                    beginBroadcast = i;
                } catch (Exception e2) {
                    beginBroadcast = i;
                }
            }
            this.f1639a.finishBroadcast();
        }
    }

    /* access modifiers changed from: private */
    public void a(d dVar) {
        XLog.i("IPC", " handleAsync ");
        if (dVar != null && dVar.d != null) {
            XLog.i("IPC", "request / data  = " + dVar + " / " + dVar.d);
            XLog.i("IPC", " create resolver ");
            p a2 = q.a(dVar.f1648a, dVar.d, dVar.c);
            if (a2 != null) {
                SDKIPCBroadcaster.a().a(dVar.b, a2);
            }
        }
    }

    /* compiled from: ProGuard */
    class RequestQueue extends PriorityBlockingQueue<d> implements Runnable {
        private Object b = new Object();

        public RequestQueue() {
            super(10, new e());
        }

        public void run() {
            while (true) {
                synchronized (this.b) {
                    while (size() == 0) {
                        try {
                            this.b.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    try {
                        RequestHandler.this.a((d) take());
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }

        public void a(Context context, String str, byte[] bArr, String str2) {
            synchronized (this.b) {
                super.put(new d(context, str, bArr, str2));
                this.b.notify();
            }
        }
    }
}
