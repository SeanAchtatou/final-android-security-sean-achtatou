package com.tencent.assistant.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.model.OnlinePCListItemModel;

/* compiled from: ProGuard */
class ae implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PCListLinearLayout f626a;

    ae(PCListLinearLayout pCListLinearLayout) {
        this.f626a = pCListLinearLayout;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.pc1 /*2131165787*/:
                if (this.f626a.objList.size() > 0) {
                    OnlinePCListItemModel onlinePCListItemModel = (OnlinePCListItemModel) this.f626a.objList.get(0);
                    this.f626a.printInfo(onlinePCListItemModel);
                    this.f626a.wantToConnectNewPC(onlinePCListItemModel);
                    return;
                }
                this.f626a.wantToConnectNewPC(null);
                return;
            case R.id.pc2 /*2131165788*/:
                if (this.f626a.objList.size() > 1) {
                    OnlinePCListItemModel onlinePCListItemModel2 = (OnlinePCListItemModel) this.f626a.objList.get(1);
                    this.f626a.printInfo(onlinePCListItemModel2);
                    this.f626a.wantToConnectNewPC(onlinePCListItemModel2);
                    return;
                }
                this.f626a.wantToConnectNewPC(null);
                return;
            case R.id.pc3 /*2131165789*/:
                if (this.f626a.objList.size() > 2) {
                    OnlinePCListItemModel onlinePCListItemModel3 = (OnlinePCListItemModel) this.f626a.objList.get(2);
                    this.f626a.printInfo(onlinePCListItemModel3);
                    this.f626a.wantToConnectNewPC(onlinePCListItemModel3);
                    return;
                }
                this.f626a.wantToConnectNewPC(null);
                return;
            case R.id.pc4 /*2131165790*/:
                this.f626a.wantToConnectNewPC(null);
                return;
            default:
                return;
        }
    }
}
