package com.tencent.assistant.component.dialog;

import android.app.Dialog;
import android.view.View;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class d implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.OneBtnDialogInfo f650a;
    final /* synthetic */ Dialog b;

    d(AppConst.OneBtnDialogInfo oneBtnDialogInfo, Dialog dialog) {
        this.f650a = oneBtnDialogInfo;
        this.b = dialog;
    }

    public void onClick(View view) {
        if (this.f650a != null) {
            this.f650a.onBtnClick();
            this.b.dismiss();
        }
    }
}
