package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.utils.at;

/* compiled from: ProGuard */
public class LocalPkgSizeTextView extends TextView {
    private ApkResourceManager apkManager = ApkResourceManager.getInstance();
    private s callback;
    /* access modifiers changed from: private */
    public ViewInvalidateMessageHandler invalidateHandler = new r(this);
    /* access modifiers changed from: private */
    public IViewInvalidater invalidater;
    /* access modifiers changed from: private */
    public String mPkgName;

    public LocalPkgSizeTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init(context);
    }

    public LocalPkgSizeTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    public LocalPkgSizeTextView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        this.callback = new s(this, null);
        this.apkManager.registerApkResCallback(this.callback);
    }

    public void updateTextView(String str, long j) {
        if (j > 0) {
            updateText(str, j);
            return;
        }
        long pkgSize = this.apkManager.getPkgSize(str);
        if (pkgSize > 0) {
            updateText(str, pkgSize);
            return;
        }
        setText(getResources().getString(R.string.calculating));
        this.mPkgName = str;
    }

    /* access modifiers changed from: private */
    public void updateText(String str, long j) {
        setText(at.a(j, 1));
        this.mPkgName = null;
        requestLayout();
    }

    public void setInvalidater(IViewInvalidater iViewInvalidater) {
        this.invalidater = iViewInvalidater;
    }
}
