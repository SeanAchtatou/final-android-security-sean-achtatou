package com.tencent.assistant.component.txscrollview;

import android.view.View;
import com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RankRefreshGetMoreListView f704a;

    b(RankRefreshGetMoreListView rankRefreshGetMoreListView) {
        this.f704a = rankRefreshGetMoreListView;
    }

    public void onClick(View view) {
        if (this.f704a.k != null && this.f704a.c == TXRefreshScrollViewBase.RefreshState.RESET) {
            this.f704a.k.onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState.ScrollState_FromEnd);
        }
    }
}
