package com.tencent.assistant.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class aj implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SecondNavigationTitleView f631a;

    aj(SecondNavigationTitleView secondNavigationTitleView) {
        this.f631a = secondNavigationTitleView;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_layout /*2131166428*/:
                if (this.f631a.hostActivity != null) {
                    this.f631a.hostActivity.finish();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
