package com.tencent.assistant;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
public class k {
    public static boolean a(int i) {
        switch (i) {
            case 0:
                return m.a().p();
            case 1:
                return m.a().d(AppConst.WISE_DOWNLOAD_SWITCH_TYPE.UPDATE);
            case 2:
                return m.a().d(AppConst.WISE_DOWNLOAD_SWITCH_TYPE.NEW_DOWNLOAD);
            case 3:
                return m.a().j();
            case 4:
                return m.a().k();
            case 5:
                return m.a().l();
            case 6:
                return m.a().U();
            case 7:
            case 8:
            case 9:
            case 10:
            default:
                return false;
            case 11:
                return m.a().q();
            case 12:
                return m.a().r();
            case 13:
                return m.a().s();
            case 14:
                return m.a().t();
            case 15:
                return m.a().u();
            case 16:
                return m.a().o();
        }
    }

    public static void a(int i, boolean z) {
        TemporaryThreadManager.get().start(new l(i, z));
    }
}
