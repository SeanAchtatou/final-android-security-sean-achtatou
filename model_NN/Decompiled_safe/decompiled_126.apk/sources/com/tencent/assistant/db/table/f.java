package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.AstDbHelper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.nucleus.manager.backgroundscan.BackgroundScan;

/* compiled from: ProGuard */
public class f implements IBaseTable {

    /* renamed from: a  reason: collision with root package name */
    private static f f750a = null;

    public static synchronized f a() {
        f fVar;
        synchronized (f.class) {
            if (f750a == null) {
                f750a = new f();
            }
            fVar = f750a;
        }
        return fVar;
    }

    /* access modifiers changed from: protected */
    public BackgroundScan a(Cursor cursor) {
        BackgroundScan backgroundScan = new BackgroundScan();
        backgroundScan.f2818a = Byte.valueOf(cursor.getString(cursor.getColumnIndex("scan_type"))).byteValue();
        backgroundScan.b = cursor.getLong(cursor.getColumnIndex("time_gap"));
        backgroundScan.c = cursor.getDouble(cursor.getColumnIndex("weight"));
        backgroundScan.d = cursor.getLong(cursor.getColumnIndex("last_push_time"));
        backgroundScan.e = cursor.getLong(cursor.getColumnIndex("scan_result"));
        return backgroundScan;
    }

    public synchronized boolean a(BackgroundScan backgroundScan) {
        ContentValues contentValues;
        contentValues = new ContentValues();
        contentValues.put("scan_type", Byte.valueOf(backgroundScan.f2818a));
        contentValues.put("time_gap", Long.valueOf(backgroundScan.b));
        contentValues.put("weight", Double.valueOf(backgroundScan.c));
        contentValues.put("last_push_time", Long.valueOf(backgroundScan.d));
        contentValues.put("scan_result", Long.valueOf(backgroundScan.e));
        return getHelper().getWritableDatabaseWrapper().insert("background_scan", null, contentValues) > 0;
    }

    public synchronized boolean b(BackgroundScan backgroundScan) {
        boolean z = true;
        synchronized (this) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("scan_type", Byte.valueOf(backgroundScan.f2818a));
            contentValues.put("time_gap", Long.valueOf(backgroundScan.b));
            contentValues.put("weight", Double.valueOf(backgroundScan.c));
            contentValues.put("last_push_time", Long.valueOf(backgroundScan.d));
            contentValues.put("scan_result", Long.valueOf(backgroundScan.e));
            if (getHelper().getWritableDatabaseWrapper().update("background_scan", contentValues, "scan_type=?", new String[]{String.valueOf((int) backgroundScan.f2818a)}) <= 0) {
                z = false;
            }
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0049 A[SYNTHETIC, Splitter:B:31:0x0049] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List<com.tencent.nucleus.manager.backgroundscan.BackgroundScan> b() {
        /*
            r10 = this;
            r8 = 0
            monitor-enter(r10)
            java.util.ArrayList r9 = new java.util.ArrayList     // Catch:{ all -> 0x0042 }
            r9.<init>()     // Catch:{ all -> 0x0042 }
            com.tencent.assistant.db.helper.SqliteHelper r0 = r10.getHelper()     // Catch:{ all -> 0x0042 }
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()     // Catch:{ all -> 0x0042 }
            java.lang.String r1 = "background_scan"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x0037, all -> 0x0045 }
            if (r1 == 0) goto L_0x0030
            boolean r0 = r1.moveToFirst()     // Catch:{ Throwable -> 0x004f }
            if (r0 == 0) goto L_0x0030
        L_0x0023:
            com.tencent.nucleus.manager.backgroundscan.BackgroundScan r0 = r10.a(r1)     // Catch:{ Throwable -> 0x004f }
            r9.add(r0)     // Catch:{ Throwable -> 0x004f }
            boolean r0 = r1.moveToNext()     // Catch:{ Throwable -> 0x004f }
            if (r0 != 0) goto L_0x0023
        L_0x0030:
            if (r1 == 0) goto L_0x0035
            r1.close()     // Catch:{ all -> 0x0042 }
        L_0x0035:
            monitor-exit(r10)
            return r9
        L_0x0037:
            r0 = move-exception
            r1 = r8
        L_0x0039:
            r0.printStackTrace()     // Catch:{ all -> 0x004d }
            if (r1 == 0) goto L_0x0035
            r1.close()     // Catch:{ all -> 0x0042 }
            goto L_0x0035
        L_0x0042:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        L_0x0045:
            r0 = move-exception
            r1 = r8
        L_0x0047:
            if (r1 == 0) goto L_0x004c
            r1.close()     // Catch:{ all -> 0x0042 }
        L_0x004c:
            throw r0     // Catch:{ all -> 0x0042 }
        L_0x004d:
            r0 = move-exception
            goto L_0x0047
        L_0x004f:
            r0 = move-exception
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.f.b():java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0036 A[SYNTHETIC, Splitter:B:19:0x0036] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.tencent.nucleus.manager.backgroundscan.BackgroundScan a(byte r10) {
        /*
            r9 = this;
            r8 = 0
            monitor-enter(r9)
            com.tencent.assistant.db.helper.SqliteHelper r0 = r9.getHelper()     // Catch:{ all -> 0x003a }
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()     // Catch:{ all -> 0x003a }
            java.lang.String r1 = "background_scan"
            r2 = 0
            java.lang.String r3 = "scan_type=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x003d, all -> 0x0033 }
            r5 = 0
            java.lang.String r6 = java.lang.String.valueOf(r10)     // Catch:{ Exception -> 0x003d, all -> 0x0033 }
            r4[r5] = r6     // Catch:{ Exception -> 0x003d, all -> 0x0033 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x003d, all -> 0x0033 }
            if (r1 == 0) goto L_0x0046
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x004f, all -> 0x004c }
            if (r0 == 0) goto L_0x0046
            com.tencent.nucleus.manager.backgroundscan.BackgroundScan r0 = r9.a(r1)     // Catch:{ Exception -> 0x004f, all -> 0x004c }
            if (r1 == 0) goto L_0x0031
            r1.close()     // Catch:{ all -> 0x003a }
        L_0x0031:
            monitor-exit(r9)
            return r0
        L_0x0033:
            r0 = move-exception
        L_0x0034:
            if (r8 == 0) goto L_0x0039
            r8.close()     // Catch:{ all -> 0x003a }
        L_0x0039:
            throw r0     // Catch:{ all -> 0x003a }
        L_0x003a:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        L_0x003d:
            r0 = move-exception
            r0 = r8
        L_0x003f:
            if (r0 == 0) goto L_0x0044
            r0.close()     // Catch:{ all -> 0x003a }
        L_0x0044:
            r0 = r8
            goto L_0x0031
        L_0x0046:
            if (r1 == 0) goto L_0x0044
            r1.close()     // Catch:{ all -> 0x003a }
            goto L_0x0044
        L_0x004c:
            r0 = move-exception
            r8 = r1
            goto L_0x0034
        L_0x004f:
            r0 = move-exception
            r0 = r1
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.f.a(byte):com.tencent.nucleus.manager.backgroundscan.BackgroundScan");
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "background_scan";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists background_scan(_id INTEGER PRIMARY KEY AUTOINCREMENT,scan_type INTEGER,time_gap INTEGER,weight TEXT,last_push_time INTEGER,scan_result INTEGER);";
    }

    public SqliteHelper getHelper() {
        return AstDbHelper.get(AstApp.i());
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public String[] getAlterSQL(int i, int i2) {
        return null;
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }
}
