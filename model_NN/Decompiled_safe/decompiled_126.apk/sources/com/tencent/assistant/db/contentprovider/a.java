package com.tencent.assistant.db.contentprovider;

import android.net.Uri;
import android.text.TextUtils;

/* compiled from: ProGuard */
class a {

    /* renamed from: a  reason: collision with root package name */
    public String f744a;
    public String b;
    public String[] c;

    a(Uri uri, String str, String[] strArr) {
        if (uri.getPathSegments().size() > 0) {
            this.f744a = uri.getPathSegments().get(0);
        }
        if (!TextUtils.isEmpty(str)) {
            this.b = str;
            this.c = strArr;
        }
    }
}
