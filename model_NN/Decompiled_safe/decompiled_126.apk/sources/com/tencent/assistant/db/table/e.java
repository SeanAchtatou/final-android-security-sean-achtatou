package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.db.helper.StatDbHelper;
import com.tencent.assistant.protocol.jce.AutoStartAppInfo;
import com.tencent.assistant.protocol.jce.AutoStartCfg;
import com.tencent.assistant.utils.XLog;
import com.tencent.open.SocialConstants;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class e implements IBaseTable {
    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "st_autostart_config";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists st_autostart_config (_id INTEGER PRIMARY KEY AUTOINCREMENT,packagename TEXT,type INTEGER,suggestwords TEXT);";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i != 3 || i2 != 4) {
            return null;
        }
        return new String[]{"CREATE TABLE if not exists st_autostart_config (_id INTEGER PRIMARY KEY AUTOINCREMENT,packagename TEXT,type INTEGER,suggestwords TEXT);"};
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return StatDbHelper.get(AstApp.i());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public ArrayList<ContentValues> a(AutoStartCfg autoStartCfg) {
        if (autoStartCfg == null) {
            return null;
        }
        ArrayList<ContentValues> arrayList = new ArrayList<>();
        if (autoStartCfg.whiteList != null && autoStartCfg.whiteList.size() > 0) {
            Iterator<AutoStartAppInfo> it = autoStartCfg.whiteList.iterator();
            while (it.hasNext()) {
                AutoStartAppInfo next = it.next();
                ContentValues contentValues = new ContentValues();
                contentValues.put("packagename", next.packageName);
                contentValues.put(SocialConstants.PARAM_TYPE, (Integer) 1);
                contentValues.put("suggestwords", next.suggestWords);
                arrayList.add(contentValues);
            }
        }
        if (autoStartCfg.blackList != null && autoStartCfg.blackList.size() > 0) {
            Iterator<AutoStartAppInfo> it2 = autoStartCfg.blackList.iterator();
            while (it2.hasNext()) {
                AutoStartAppInfo next2 = it2.next();
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("packagename", next2.packageName);
                contentValues2.put(SocialConstants.PARAM_TYPE, (Integer) 2);
                contentValues2.put("suggestwords", next2.suggestWords);
                arrayList.add(contentValues2);
            }
        }
        return arrayList;
    }

    public synchronized void b(AutoStartCfg autoStartCfg) {
        if (autoStartCfg != null) {
            ArrayList<ContentValues> a2 = a(autoStartCfg);
            if (a2 != null && !a2.isEmpty()) {
                SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
                writableDatabaseWrapper.beginTransaction();
                try {
                    Iterator<ContentValues> it = a2.iterator();
                    while (it.hasNext()) {
                        ContentValues next = it.next();
                        if (writableDatabaseWrapper.update("st_autostart_config", next, "packagename = ?", new String[]{next.getAsString("packagename")}) == 0) {
                            writableDatabaseWrapper.insert("st_autostart_config", "empty", next);
                        }
                    }
                    writableDatabaseWrapper.setTransactionSuccessful();
                    writableDatabaseWrapper.endTransaction();
                } catch (Exception e) {
                    XLog.d("AutoStartConfigTable", "save error");
                    e.printStackTrace();
                    writableDatabaseWrapper.endTransaction();
                } catch (Throwable th) {
                    writableDatabaseWrapper.endTransaction();
                    throw th;
                }
            }
        }
        return;
    }

    public AutoStartCfg a() {
        Cursor cursor = null;
        try {
            Cursor query = getHelper().getReadableDatabaseWrapper().query("st_autostart_config", null, null, null, null, null, null);
            try {
                AutoStartCfg autoStartCfg = new AutoStartCfg();
                autoStartCfg.whiteList = new ArrayList<>();
                autoStartCfg.blackList = new ArrayList<>();
                int columnIndexOrThrow = query.getColumnIndexOrThrow("packagename");
                int columnIndexOrThrow2 = query.getColumnIndexOrThrow(SocialConstants.PARAM_TYPE);
                int columnIndexOrThrow3 = query.getColumnIndexOrThrow("suggestwords");
                if (query == null || !query.moveToFirst()) {
                    if (query != null && !query.isClosed()) {
                        query.close();
                    }
                    return null;
                }
                do {
                    AutoStartAppInfo autoStartAppInfo = new AutoStartAppInfo();
                    autoStartAppInfo.packageName = query.getString(columnIndexOrThrow);
                    autoStartAppInfo.suggestWords = query.getString(columnIndexOrThrow3);
                    int i = query.getInt(columnIndexOrThrow2);
                    if (i == 1) {
                        autoStartCfg.whiteList.add(autoStartAppInfo);
                    } else if (i == 2) {
                        autoStartCfg.blackList.add(autoStartAppInfo);
                    }
                } while (query.moveToNext());
                if (query == null || query.isClosed()) {
                    return autoStartCfg;
                }
                query.close();
                return autoStartCfg;
            } catch (Throwable th) {
                th = th;
                cursor = query;
            }
        } catch (Throwable th2) {
            th = th2;
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        throw th;
    }
}
