package com.tencent.assistant.manager;

import android.os.Message;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class a implements UIEventListener {
    private static a c;

    /* renamed from: a  reason: collision with root package name */
    protected ReferenceQueue<b> f854a;
    private AstApp b = AstApp.i();
    private ConcurrentHashMap<String, ArrayList<WeakReference<b>>> d = null;

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (c == null) {
                c = new a();
            }
            aVar = c;
        }
        return aVar;
    }

    private a() {
        this.b.k().addUIEventListener(1002, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, this);
        this.b.k().addUIEventListener(1007, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        this.b.k().addUIEventListener(1010, this);
        this.b.k().addUIEventListener(1013, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_DELETE, this);
        this.b.k().addUIEventListener(1001, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        this.b.k().addUIEventListener(1027, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APK_DELETE, this);
        this.d = new ConcurrentHashMap<>();
        this.f854a = new ReferenceQueue<>();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r8, com.tencent.assistant.manager.b r9) {
        /*
            r7 = this;
            if (r9 == 0) goto L_0x0008
            boolean r0 = android.text.TextUtils.isEmpty(r8)
            if (r0 == 0) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.util.ArrayList<java.lang.ref.WeakReference<com.tencent.assistant.manager.b>>> r3 = r7.d
            monitor-enter(r3)
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.util.ArrayList<java.lang.ref.WeakReference<com.tencent.assistant.manager.b>>> r0 = r7.d     // Catch:{ all -> 0x0050 }
            java.util.Set r0 = r0.keySet()     // Catch:{ all -> 0x0050 }
            java.util.Iterator r4 = r0.iterator()     // Catch:{ all -> 0x0050 }
        L_0x0016:
            java.lang.ref.ReferenceQueue<com.tencent.assistant.manager.b> r0 = r7.f854a     // Catch:{ all -> 0x0050 }
            java.lang.ref.Reference r5 = r0.poll()     // Catch:{ all -> 0x0050 }
            if (r5 == 0) goto L_0x005f
        L_0x001e:
            boolean r0 = r4.hasNext()     // Catch:{ all -> 0x0050 }
            if (r0 == 0) goto L_0x0016
            java.lang.Object r0 = r4.next()     // Catch:{ all -> 0x0050 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0050 }
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.util.ArrayList<java.lang.ref.WeakReference<com.tencent.assistant.manager.b>>> r1 = r7.d     // Catch:{ all -> 0x0050 }
            java.lang.Object r1 = r1.get(r0)     // Catch:{ all -> 0x0050 }
            java.util.ArrayList r1 = (java.util.ArrayList) r1     // Catch:{ all -> 0x0050 }
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x0050 }
            r2.<init>()     // Catch:{ all -> 0x0050 }
            r2.addAll(r1)     // Catch:{ all -> 0x0050 }
            java.util.Iterator r6 = r2.iterator()     // Catch:{ all -> 0x0050 }
        L_0x003e:
            boolean r2 = r6.hasNext()     // Catch:{ all -> 0x0050 }
            if (r2 == 0) goto L_0x0053
            java.lang.Object r2 = r6.next()     // Catch:{ all -> 0x0050 }
            java.lang.ref.WeakReference r2 = (java.lang.ref.WeakReference) r2     // Catch:{ all -> 0x0050 }
            if (r2 != r5) goto L_0x003e
            r1.remove(r2)     // Catch:{ all -> 0x0050 }
            goto L_0x003e
        L_0x0050:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0050 }
            throw r0
        L_0x0053:
            int r1 = r1.size()     // Catch:{ all -> 0x0050 }
            if (r1 != 0) goto L_0x001e
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.util.ArrayList<java.lang.ref.WeakReference<com.tencent.assistant.manager.b>>> r1 = r7.d     // Catch:{ all -> 0x0050 }
            r1.remove(r0)     // Catch:{ all -> 0x0050 }
            goto L_0x001e
        L_0x005f:
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.util.ArrayList<java.lang.ref.WeakReference<com.tencent.assistant.manager.b>>> r0 = r7.d     // Catch:{ all -> 0x0050 }
            boolean r0 = r0.containsKey(r8)     // Catch:{ all -> 0x0050 }
            if (r0 == 0) goto L_0x00ae
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.util.ArrayList<java.lang.ref.WeakReference<com.tencent.assistant.manager.b>>> r0 = r7.d     // Catch:{ all -> 0x0050 }
            java.lang.Object r0 = r0.get(r8)     // Catch:{ all -> 0x0050 }
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ all -> 0x0050 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x0050 }
        L_0x0073:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x0050 }
            if (r0 == 0) goto L_0x0089
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x0050 }
            java.lang.ref.WeakReference r0 = (java.lang.ref.WeakReference) r0     // Catch:{ all -> 0x0050 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0050 }
            com.tencent.assistant.manager.b r0 = (com.tencent.assistant.manager.b) r0     // Catch:{ all -> 0x0050 }
            if (r0 != r9) goto L_0x0073
            monitor-exit(r3)     // Catch:{ all -> 0x0050 }
            goto L_0x0008
        L_0x0089:
            java.lang.ref.WeakReference r1 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x0050 }
            java.lang.ref.ReferenceQueue<com.tencent.assistant.manager.b> r0 = r7.f854a     // Catch:{ all -> 0x0050 }
            r1.<init>(r9, r0)     // Catch:{ all -> 0x0050 }
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.util.ArrayList<java.lang.ref.WeakReference<com.tencent.assistant.manager.b>>> r0 = r7.d     // Catch:{ all -> 0x0050 }
            java.lang.Object r0 = r0.get(r8)     // Catch:{ all -> 0x0050 }
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ all -> 0x0050 }
            if (r0 == 0) goto L_0x00a0
            r0.add(r1)     // Catch:{ all -> 0x0050 }
        L_0x009d:
            monitor-exit(r3)     // Catch:{ all -> 0x0050 }
            goto L_0x0008
        L_0x00a0:
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0050 }
            r0.<init>()     // Catch:{ all -> 0x0050 }
            r0.add(r1)     // Catch:{ all -> 0x0050 }
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.util.ArrayList<java.lang.ref.WeakReference<com.tencent.assistant.manager.b>>> r1 = r7.d     // Catch:{ all -> 0x0050 }
            r1.put(r8, r0)     // Catch:{ all -> 0x0050 }
            goto L_0x009d
        L_0x00ae:
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x0050 }
            java.lang.ref.ReferenceQueue<com.tencent.assistant.manager.b> r1 = r7.f854a     // Catch:{ all -> 0x0050 }
            r0.<init>(r9, r1)     // Catch:{ all -> 0x0050 }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x0050 }
            r1.<init>()     // Catch:{ all -> 0x0050 }
            r1.add(r0)     // Catch:{ all -> 0x0050 }
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.util.ArrayList<java.lang.ref.WeakReference<com.tencent.assistant.manager.b>>> r0 = r7.d     // Catch:{ all -> 0x0050 }
            r0.put(r8, r1)     // Catch:{ all -> 0x0050 }
            goto L_0x009d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.manager.a.a(java.lang.String, com.tencent.assistant.manager.b):void");
    }

    public void handleUIEvent(Message message) {
        String str;
        InstallUninstallTaskBean installUninstallTaskBean;
        if (message.obj instanceof String) {
            str = (String) message.obj;
            installUninstallTaskBean = null;
        } else if (message.obj instanceof InstallUninstallTaskBean) {
            installUninstallTaskBean = (InstallUninstallTaskBean) message.obj;
            str = installUninstallTaskBean.downloadTicket;
        } else {
            str = Constants.STR_EMPTY;
            installUninstallTaskBean = null;
        }
        if (!TextUtils.isEmpty(str)) {
            switch (message.what) {
                case 1001:
                case EventDispatcherEnum.UI_EVENT_APK_DELETE:
                    a(str, AppConst.AppState.DOWNLOAD);
                    return;
                case 1002:
                case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING:
                    a(str, AppConst.AppState.DOWNLOADING);
                    return;
                case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE:
                    a(str, AppConst.AppState.PAUSED);
                    return;
                case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC:
                    a(str, AppConst.AppState.DOWNLOADED);
                    return;
                case 1007:
                    a(str, AppConst.AppState.FAIL);
                    return;
                case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING:
                    a(str, AppConst.AppState.QUEUING);
                    return;
                case 1010:
                    a(str, AppConst.AppState.DOWNLOADING);
                    return;
                case EventDispatcherEnum.UI_EVENT_APP_UNINSTALL:
                    a(str, AppConst.AppState.ILLEGAL);
                    return;
                case 1013:
                    a(str, AppConst.AppState.INSTALLED);
                    return;
                case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_DELETE:
                    a(str, AppConst.AppState.ILLEGAL);
                    return;
                case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START:
                    a(str, AppConst.AppState.INSTALLING, installUninstallTaskBean);
                    return;
                case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC:
                    a(str, AppConst.AppState.INSTALLED, installUninstallTaskBean);
                    return;
                case 1027:
                    a(str, AppConst.AppState.ILLEGAL, installUninstallTaskBean);
                    return;
                case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START:
                    a(str, AppConst.AppState.UNINSTALLING);
                    return;
                case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC:
                case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL:
                    a(str, AppConst.AppState.ILLEGAL);
                    return;
                default:
                    return;
            }
        }
    }

    private void a(String str, AppConst.AppState appState, InstallUninstallTaskBean installUninstallTaskBean) {
        ArrayList arrayList;
        if (!a(DownloadProxy.a().d(str), appState, installUninstallTaskBean) && this.d != null && this.d.size() > 0 && (arrayList = this.d.get(str)) != null) {
            ArrayList arrayList2 = new ArrayList();
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                arrayList2.add(((WeakReference) it.next()).get());
            }
            Iterator it2 = arrayList2.iterator();
            while (it2.hasNext()) {
                b bVar = (b) it2.next();
                if (bVar != null) {
                    bVar.onAppStateChange(str, appState);
                }
            }
        }
    }

    private void a(String str, AppConst.AppState appState) {
        a(str, appState, (InstallUninstallTaskBean) null);
    }

    private boolean a(DownloadInfo downloadInfo, AppConst.AppState appState, InstallUninstallTaskBean installUninstallTaskBean) {
        if (downloadInfo == null || !downloadInfo.isUiTypeWiseDownload()) {
            return false;
        }
        if (appState == AppConst.AppState.QUEUING || appState == AppConst.AppState.DOWNLOADING || appState == AppConst.AppState.PAUSED || appState == AppConst.AppState.FAIL) {
            return true;
        }
        return false;
    }
}
