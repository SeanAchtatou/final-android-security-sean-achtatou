package com.tencent.assistant.manager.notification;

import android.app.Notification;
import android.content.Context;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;

/* compiled from: ProGuard */
public class r {

    /* renamed from: a  reason: collision with root package name */
    protected Integer f893a = null;
    protected float b = 14.0f;
    protected Integer c = null;
    protected float d = 16.0f;
    protected final String e = "SearchForText";
    protected final String f = "SearchForTitle";
    protected DisplayMetrics g = new DisplayMetrics();
    Object h = new Object();
    private Handler i = null;

    public r(Context context, boolean z) {
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(this.g);
        a(AstApp.i(), z);
    }

    public Integer a() {
        return Integer.valueOf(this.f893a != null ? this.f893a.intValue() : 0);
    }

    public float b() {
        return this.b;
    }

    public Integer c() {
        return Integer.valueOf(this.c != null ? this.c.intValue() : 0);
    }

    public float d() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public boolean a(ViewGroup viewGroup) {
        int childCount = viewGroup.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            if (viewGroup.getChildAt(i2) instanceof TextView) {
                TextView textView = (TextView) viewGroup.getChildAt(i2);
                if ("SearchForTitle".equals(textView.getText().toString())) {
                    this.c = Integer.valueOf(textView.getTextColors().getDefaultColor());
                    this.d = textView.getTextSize();
                    this.d /= this.g.scaledDensity;
                    return true;
                }
            } else if ((viewGroup.getChildAt(i2) instanceof ViewGroup) && a((ViewGroup) viewGroup.getChildAt(i2))) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean b(ViewGroup viewGroup) {
        int childCount = viewGroup.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            if (viewGroup.getChildAt(i2) instanceof TextView) {
                TextView textView = (TextView) viewGroup.getChildAt(i2);
                if ("SearchForText".equals(textView.getText().toString())) {
                    this.f893a = Integer.valueOf(textView.getTextColors().getDefaultColor());
                    this.b = textView.getTextSize();
                    this.b /= this.g.scaledDensity;
                    return true;
                }
            } else if ((viewGroup.getChildAt(i2) instanceof ViewGroup) && b((ViewGroup) viewGroup.getChildAt(i2))) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void a(Context context, boolean z) {
        if (this.f893a == null || this.c == null) {
            if (z) {
                try {
                    Notification notification = new Notification();
                    notification.setLatestEventInfo(AstApp.i(), "SearchForTitle", "SearchForText", null);
                    ViewGroup viewGroup = (ViewGroup) notification.contentView.apply(context, null);
                    a(viewGroup);
                    b(viewGroup);
                } catch (Throwable th) {
                }
            } else {
                e();
            }
            this.i = null;
        }
    }

    /* access modifiers changed from: protected */
    public void e() {
        if (this.i == null) {
            this.i = f();
        }
        this.i.sendEmptyMessage(EventDispatcherEnum.UI_EVENT_NOTIFICATION_STYLE_NEED_HELP_TO_MAIN_THREAD);
        try {
            synchronized (this.h) {
                this.h.wait();
            }
        } catch (InterruptedException e2) {
        }
    }

    private Handler f() {
        return new s(this, AstApp.i().getMainLooper());
    }
}
