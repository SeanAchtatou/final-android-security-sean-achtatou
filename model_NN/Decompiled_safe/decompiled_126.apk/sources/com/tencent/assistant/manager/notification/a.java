package com.tencent.assistant.manager.notification;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.widget.RemoteViews;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.m;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.r;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import java.util.Calendar;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static a f863a;
    private int[] b = {10, 23};

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (f863a == null) {
                f863a = new a();
            }
            aVar = f863a;
        }
        return aVar;
    }

    public synchronized void b() {
        boolean z = false;
        synchronized (this) {
            XLog.d("BookingDownload", "showCahcedNotification");
            String ad = m.a().ad();
            if (!TextUtils.isEmpty(ad)) {
                z = true;
            }
            if (!z) {
                XLog.d("BookingDownload", "has no cache ticket!");
            } else {
                Calendar instance = Calendar.getInstance();
                instance.set(11, this.b[0]);
                instance.set(12, 2);
                instance.set(13, 0);
                instance.set(14, 0);
                if (Math.abs(System.currentTimeMillis() - instance.getTimeInMillis()) < 120000 || !z) {
                    XLog.d("BookingDownload", "Will not do any scan push!");
                } else {
                    XLog.d("BookingDownload", "show cache notification !");
                    XLog.d("BookingDownload", "showCahcedNotification cachedTicket = " + ad);
                    if (!a(ad)) {
                        m.a().g(Constants.STR_EMPTY);
                    }
                }
            }
        }
    }

    public synchronized boolean a(String str) {
        boolean z = false;
        synchronized (this) {
            XLog.d("BookingDownload", "showCahcedNotification cachedTicket = " + str);
            if (!TextUtils.isEmpty(str)) {
                DownloadInfo c = DownloadProxy.a().c(str);
                if (c != null && (c.downloadState == SimpleDownloadInfo.DownloadState.SUCC || c.downloadState == SimpleDownloadInfo.DownloadState.INSTALLED)) {
                    if (d()) {
                        XLog.d("BookingDownload", "Time OK");
                        a(c);
                    } else {
                        m.a().g(str);
                        XLog.d("BookingDownload", "Time Noet OK cacheTicket = " + str);
                        a(c());
                    }
                    z = true;
                }
            }
        }
        return z;
    }

    private void a(DownloadInfo downloadInfo) {
        if (downloadInfo != null) {
            d dVar = new d(this, downloadInfo.iconUrl, 1);
            dVar.a(new b(this, downloadInfo));
            dVar.a();
        }
    }

    private long c() {
        boolean z = true;
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(System.currentTimeMillis());
        int i = instance.get(11);
        if (i < this.b[1]) {
            if (i < this.b[0]) {
                z = false;
            } else {
                z = false;
            }
        }
        Calendar instance2 = Calendar.getInstance();
        instance2.set(11, this.b[0]);
        instance2.set(12, 2);
        instance2.set(13, 0);
        instance2.set(14, 0);
        if (!z) {
            return instance2.getTimeInMillis();
        }
        return instance2.getTimeInMillis() + TesDownloadConfig.TES_CONFIG_CHECK_PERIOD;
    }

    private boolean d() {
        long currentTimeMillis = System.currentTimeMillis();
        Calendar instance = Calendar.getInstance();
        instance.set(11, this.b[0] - 1);
        instance.set(12, 58);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar instance2 = Calendar.getInstance();
        instance2.set(11, this.b[1]);
        instance2.set(12, 2);
        instance2.set(13, 0);
        instance2.set(14, 0);
        if (currentTimeMillis < instance.getTimeInMillis() || currentTimeMillis > instance2.getTimeInMillis()) {
            return false;
        }
        return true;
    }

    public Notification a(String str, Bitmap bitmap) {
        DownloadInfo c = DownloadProxy.a().c(str);
        if (c == null) {
            return null;
        }
        return y.a(AstApp.i(), R.drawable.logo32, a(c, bitmap), b(c), System.currentTimeMillis(), e(c), null, true, false);
    }

    private RemoteViews a(DownloadInfo downloadInfo, Bitmap bitmap) {
        if (downloadInfo == null) {
            return null;
        }
        RemoteViews remoteViews = new RemoteViews(AstApp.i().getPackageName(), (int) R.layout.notification_card_bookingdownload);
        r rVar = new r(AstApp.i(), true);
        Integer c = rVar.c();
        if (c != null) {
            remoteViews.setTextColor(R.id.title, c.intValue());
        }
        Integer a2 = rVar.a();
        if (a2 != null) {
            remoteViews.setTextColor(R.id.content, a2.intValue());
        }
        if (bitmap == null || bitmap.isRecycled()) {
            remoteViews.setImageViewResource(R.id.big_icon, R.drawable.logo72);
        } else {
            remoteViews.setImageViewBitmap(R.id.big_icon, bitmap);
        }
        remoteViews.setFloat(R.id.title, "setTextSize", rVar.d());
        remoteViews.setFloat(R.id.content, "setTextSize", rVar.b());
        remoteViews.setTextViewText(R.id.title, b(downloadInfo));
        remoteViews.setTextViewText(R.id.content, d(downloadInfo));
        remoteViews.setTextViewText(R.id.rightBtn, c(downloadInfo));
        if (r.d() < 20) {
            return remoteViews;
        }
        remoteViews.setInt(R.id.root, "setBackgroundResource", R.color.notification_bg_50);
        return remoteViews;
    }

    private String b(DownloadInfo downloadInfo) {
        if (downloadInfo != null) {
            if (downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.SUCC) {
                return AstApp.i().getString(R.string.bookingdownload_notification_title_downloaded, new Object[]{downloadInfo.name});
            } else if (downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.INSTALLED) {
                return AstApp.i().getString(R.string.bookingdownload_notification_title_installed, new Object[]{downloadInfo.name});
            }
        }
        return Constants.STR_EMPTY;
    }

    private String c(DownloadInfo downloadInfo) {
        if (downloadInfo != null) {
            if (downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.SUCC) {
                return AstApp.i().getString(R.string.bookingdownload_notification_btn_downloaded);
            }
            if (downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.INSTALLED) {
                return AstApp.i().getString(R.string.bookingdownload_notification_btn_installed);
            }
        }
        return Constants.STR_EMPTY;
    }

    private String d(DownloadInfo downloadInfo) {
        if (downloadInfo != null) {
            if (downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.SUCC) {
                return AstApp.i().getString(R.string.bookingdownload_notification_content_downloaded);
            }
            if (downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.INSTALLED) {
                return AstApp.i().getString(R.string.bookingdownload_notification_content_installed);
            }
        }
        return Constants.STR_EMPTY;
    }

    private PendingIntent e(DownloadInfo downloadInfo) {
        Intent intent = new Intent(AstApp.i(), NotificationService.class);
        intent.putExtra("notification_id", 120);
        intent.putExtra("notification_push_extra", downloadInfo.downloadTicket);
        return PendingIntent.getService(AstApp.i(), 120, intent, 268435456);
    }

    public void a(long j) {
        m.a().i(j);
    }
}
