package com.tencent.assistant.manager.notification.a;

import android.graphics.Bitmap;
import android.widget.RemoteViews;
import com.tencent.assistant.manager.notification.a.a.e;

/* compiled from: ProGuard */
class m implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RemoteViews f873a;
    final /* synthetic */ int[] b;
    final /* synthetic */ int c;
    final /* synthetic */ l d;

    m(l lVar, RemoteViews remoteViews, int[] iArr, int i) {
        this.d = lVar;
        this.f873a = remoteViews;
        this.b = iArr;
        this.c = i;
    }

    public void a(Bitmap bitmap) {
        if (bitmap != null && !bitmap.isRecycled()) {
            this.f873a.setViewVisibility(this.b[this.c], 0);
            this.f873a.setImageViewBitmap(this.b[this.c], bitmap);
        }
    }
}
