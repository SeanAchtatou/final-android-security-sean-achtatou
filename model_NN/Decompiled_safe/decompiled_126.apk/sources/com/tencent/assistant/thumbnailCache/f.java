package com.tencent.assistant.thumbnailCache;

import android.content.ContentUris;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import com.qq.AppService.AstApp;
import com.tencent.assistant.Global;
import com.tencent.assistant.component.TouchAnalizer;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.am;
import com.tencent.assistant.utils.aw;
import com.tencent.assistant.utils.n;
import com.tencent.assistant.utils.q;
import com.tencent.assistant.utils.t;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;

/* compiled from: ProGuard */
public class f {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final c[] f1793a = {new c("resuming", 0, 100, 0), new c("icon", 80, 600, 0), new c("middle", 10, 200, 0), new c("iconlocal", 80, 0, 0), new c("middlelocal", 20, 0, 0), new c("iconbyapk", 80, 600, 0), new c("middlebyorig", 10, 200, 0), new c("middlebyvideo", 10, 200, 0), new c("smallbymp3", 40, TouchAnalizer.CLICK_AREA, 0), new c("tjdata", 0, 100, 0), new c("round", 30, STConstAction.ACTION_HIT_OPEN, 128)};
    private static final Uri f = Uri.parse("content://media/external/audio/albumart");
    private final int b = 4;
    private ExecutorService c;
    /* access modifiers changed from: private */
    public b[] d = new b[11];
    private j e = new j();
    /* access modifiers changed from: private */
    public n g = n.a();

    public f() {
        float b2 = t.b();
        for (int i = 0; i < 11; i++) {
            this.d[i] = new b(f1793a[i].f1790a, f1793a[i].f1790a, (int) (((float) f1793a[i].b) * b2), (int) (((float) f1793a[i].c) * b2), f1793a[i].d, false, this.g);
        }
        a();
        this.c = Executors.newFixedThreadPool(4, new q("thumbailDownloader"));
    }

    public Bitmap a(String str, int i) {
        return this.d[o.c(i)].a(str);
    }

    public void a() {
        TemporaryThreadManager.get().start(new g(this));
    }

    public void b() {
        TemporaryThreadManager.get().start(new h(this));
    }

    public int c() {
        return 4;
    }

    public Future<o> a(o oVar) {
        Callable<o> b2 = b(oVar);
        if (b2 != null) {
            return this.c.submit(b2);
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void a(o oVar, int i) {
        switch (i) {
            case 0:
                oVar.b(i);
                return;
            case 1:
            default:
                return;
            case 2:
                oVar.b(i);
                return;
        }
    }

    private Callable<o> b(o oVar) {
        return new i(this, oVar);
    }

    /* access modifiers changed from: private */
    public byte[] a(String str, String str2, int i) {
        DefaultHttpClient a2 = this.e.a();
        HttpGet httpGet = new HttpGet(str2);
        httpGet.addHeader("Accept-Language", "zh-cn");
        httpGet.addHeader("User-Agent", Global.HTTP_USER_AGENT);
        httpGet.addHeader("Range", "bytes=" + this.d[0].c(str) + "-");
        try {
            return a(str, i, a2.execute(httpGet));
        } catch (ClientProtocolException e2) {
            return new byte[]{105};
        } catch (IOException e3) {
            if (e3 instanceof ConnectTimeoutException) {
                return new byte[]{102};
            } else if (e3 instanceof SocketTimeoutException) {
                return new byte[]{103};
            } else {
                return new byte[]{104};
            }
        } catch (Exception e4) {
            e4.printStackTrace();
            return new byte[]{106};
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:155:0x0246, code lost:
        r1 = null;
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x024b, code lost:
        r1 = r3;
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x014b, code lost:
        r0 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:166:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0107 A[SYNTHETIC, Splitter:B:46:0x0107] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x010c A[SYNTHETIC, Splitter:B:49:0x010c] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x014b A[ExcHandler: all (th java.lang.Throwable), Splitter:B:18:0x00aa] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0153 A[SYNTHETIC, Splitter:B:75:0x0153] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0158 A[SYNTHETIC, Splitter:B:78:0x0158] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x015d A[SYNTHETIC, Splitter:B:81:0x015d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] a(java.lang.String r15, int r16, org.apache.http.HttpResponse r17) {
        /*
            r14 = this;
            if (r17 != 0) goto L_0x000b
            r0 = 1
            byte[] r0 = new byte[r0]
            r1 = 0
            r2 = 111(0x6f, float:1.56E-43)
            r0[r1] = r2
        L_0x000a:
            return r0
        L_0x000b:
            r3 = 0
            r1 = 0
            r2 = 0
            org.apache.http.StatusLine r0 = r17.getStatusLine()
            int r0 = r0.getStatusCode()
            com.tencent.assistant.thumbnailCache.b[] r4 = r14.d
            r5 = 0
            r4 = r4[r5]
            long r5 = r4.c(r15)
            r4 = 200(0xc8, float:2.8E-43)
            if (r4 == r0) goto L_0x0027
            r4 = 206(0xce, float:2.89E-43)
            if (r4 != r0) goto L_0x01ec
        L_0x0027:
            org.apache.http.Header[] r4 = r17.getAllHeaders()
            int r7 = r4.length
            r0 = 0
            r13 = r0
            r0 = r1
            r1 = r3
            r3 = r13
        L_0x0031:
            if (r3 >= r7) goto L_0x0099
            r8 = r4[r3]
            java.lang.String r9 = "Content-Range"
            java.lang.String r10 = r8.getName()
            boolean r9 = r9.equals(r10)
            if (r9 == 0) goto L_0x007e
            r1 = 1
            java.lang.String r0 = r8.getValue()
            java.lang.String r9 = "bytes "
            int r0 = r0.indexOf(r9)
            int r0 = r0 + 6
            java.lang.String r9 = r8.getValue()
            java.lang.String r10 = "-"
            int r9 = r9.indexOf(r10)
            java.lang.String r10 = r8.getValue()
            java.lang.String r0 = r10.substring(r0, r9)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            int r0 = r0.intValue()
            long r9 = (long) r0
            int r9 = (r9 > r5 ? 1 : (r9 == r5 ? 0 : -1))
            if (r9 == 0) goto L_0x007e
            com.tencent.assistant.thumbnailCache.b[] r0 = r14.d
            r1 = 0
            r0 = r0[r1]
            r0.f(r15)
            r0 = 1
            byte[] r0 = new byte[r0]
            r1 = 0
            r2 = 112(0x70, float:1.57E-43)
            r0[r1] = r2
            goto L_0x000a
        L_0x007e:
            java.lang.String r9 = "Content-Length"
            java.lang.String r10 = r8.getName()
            boolean r9 = r9.equals(r10)
            if (r9 == 0) goto L_0x0096
            java.lang.String r2 = r8.getValue()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            int r2 = r2.intValue()
        L_0x0096:
            int r3 = r3 + 1
            goto L_0x0031
        L_0x0099:
            r4 = 0
            r3 = 0
            com.tencent.assistant.utils.n r7 = r14.g
            r8 = 1024(0x400, float:1.435E-42)
            byte[] r9 = r7.a(r8)
            com.tencent.assistant.utils.aw r10 = new com.tencent.assistant.utils.aw
            com.tencent.assistant.utils.n r7 = r14.g
            r10.<init>(r7)
            org.apache.http.HttpEntity r3 = r17.getEntity()     // Catch:{ Exception -> 0x0245, all -> 0x014b }
            java.io.InputStream r4 = r3.getContent()     // Catch:{ Exception -> 0x024a, all -> 0x014b }
            r7 = 0
            r11 = 0
            int r11 = (r11 > r5 ? 1 : (r11 == r5 ? 0 : -1))
            if (r11 == 0) goto L_0x024f
            if (r1 == 0) goto L_0x0161
            r1 = 0
            com.tencent.assistant.thumbnailCache.b[] r7 = r14.d     // Catch:{ all -> 0x0144 }
            r8 = 0
            r7 = r7[r8]     // Catch:{ all -> 0x0144 }
            byte[] r1 = r7.d(r15)     // Catch:{ all -> 0x0144 }
            if (r1 == 0) goto L_0x0117
            r7 = 0
            int r8 = r1.length     // Catch:{ all -> 0x0144 }
            r10.write(r1, r7, r8)     // Catch:{ all -> 0x0144 }
            com.tencent.assistant.utils.n r7 = r14.g     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            r7.a(r1)     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
        L_0x00d2:
            r1 = -1
            r7 = 0
            int r8 = r9.length     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            int r7 = r4.read(r9, r7, r8)     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            if (r1 == r7) goto L_0x0189
            long r11 = (long) r7     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            long r5 = r5 + r11
            r1 = 0
            r10.write(r9, r1, r7)     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            goto L_0x00d2
        L_0x00e2:
            r0 = move-exception
            r1 = r3
            r2 = r4
        L_0x00e5:
            if (r10 == 0) goto L_0x00f3
            com.tencent.assistant.thumbnailCache.b[] r0 = r14.d     // Catch:{ all -> 0x0240 }
            r3 = 0
            r0 = r0[r3]     // Catch:{ all -> 0x0240 }
            byte[] r3 = r10.toByteArray()     // Catch:{ all -> 0x0240 }
            r0.a(r15, r3)     // Catch:{ all -> 0x0240 }
        L_0x00f3:
            r0 = 1
            byte[] r0 = new byte[r0]     // Catch:{ all -> 0x0240 }
            r3 = 0
            r4 = 113(0x71, float:1.58E-43)
            r0[r3] = r4     // Catch:{ all -> 0x0240 }
            com.tencent.assistant.utils.n r3 = r14.g
            r3.a(r9)
            if (r2 == 0) goto L_0x0105
            r2.close()     // Catch:{ IOException -> 0x0208 }
        L_0x0105:
            if (r10 == 0) goto L_0x010a
            r10.close()     // Catch:{ IOException -> 0x020e }
        L_0x010a:
            if (r1 == 0) goto L_0x000a
            r1.consumeContent()     // Catch:{ IOException -> 0x0111 }
            goto L_0x000a
        L_0x0111:
            r1 = move-exception
        L_0x0112:
            r1.printStackTrace()
            goto L_0x000a
        L_0x0117:
            com.tencent.assistant.thumbnailCache.b[] r0 = r14.d     // Catch:{ all -> 0x0144 }
            r2 = 0
            r0 = r0[r2]     // Catch:{ all -> 0x0144 }
            r0.f(r15)     // Catch:{ all -> 0x0144 }
            r0 = 1
            byte[] r0 = new byte[r0]     // Catch:{ all -> 0x0144 }
            r2 = 0
            r5 = 112(0x70, float:1.57E-43)
            r0[r2] = r5     // Catch:{ all -> 0x0144 }
            com.tencent.assistant.utils.n r2 = r14.g     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            r2.a(r1)     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            com.tencent.assistant.utils.n r1 = r14.g
            r1.a(r9)
            if (r4 == 0) goto L_0x0136
            r4.close()     // Catch:{ IOException -> 0x0228 }
        L_0x0136:
            if (r10 == 0) goto L_0x013b
            r10.close()     // Catch:{ IOException -> 0x022e }
        L_0x013b:
            if (r3 == 0) goto L_0x000a
            r3.consumeContent()     // Catch:{ IOException -> 0x0142 }
            goto L_0x000a
        L_0x0142:
            r1 = move-exception
            goto L_0x0112
        L_0x0144:
            r0 = move-exception
            com.tencent.assistant.utils.n r2 = r14.g     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            r2.a(r1)     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            throw r0     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
        L_0x014b:
            r0 = move-exception
        L_0x014c:
            com.tencent.assistant.utils.n r1 = r14.g
            r1.a(r9)
            if (r4 == 0) goto L_0x0156
            r4.close()     // Catch:{ IOException -> 0x01f6 }
        L_0x0156:
            if (r10 == 0) goto L_0x015b
            r10.close()     // Catch:{ IOException -> 0x01fc }
        L_0x015b:
            if (r3 == 0) goto L_0x0160
            r3.consumeContent()     // Catch:{ IOException -> 0x0202 }
        L_0x0160:
            throw r0
        L_0x0161:
            com.tencent.assistant.thumbnailCache.b[] r0 = r14.d     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            r1 = 0
            r0 = r0[r1]     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            r0.f(r15)     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            r0 = 1
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            r1 = 0
            r2 = 112(0x70, float:1.57E-43)
            r0[r1] = r2     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            com.tencent.assistant.utils.n r1 = r14.g
            r1.a(r9)
            if (r4 == 0) goto L_0x017b
            r4.close()     // Catch:{ IOException -> 0x0234 }
        L_0x017b:
            if (r10 == 0) goto L_0x0180
            r10.close()     // Catch:{ IOException -> 0x023a }
        L_0x0180:
            if (r3 == 0) goto L_0x000a
            r3.consumeContent()     // Catch:{ IOException -> 0x0187 }
            goto L_0x000a
        L_0x0187:
            r1 = move-exception
            goto L_0x0112
        L_0x0189:
            r10.flush()     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            int r1 = r10.size()     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            int r0 = r0 + r2
            if (r1 != r0) goto L_0x01bf
            byte[] r0 = r10.toByteArray()     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            com.tencent.assistant.thumbnailCache.b[] r1 = r14.d     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            r1 = r1[r16]     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            r1.a(r15, r0)     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            com.tencent.assistant.thumbnailCache.b[] r1 = r14.d     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            r2 = 0
            r1 = r1[r2]     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            r1.f(r15)     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            com.tencent.assistant.utils.n r1 = r14.g
            r1.a(r9)
            if (r4 == 0) goto L_0x01b0
            r4.close()     // Catch:{ IOException -> 0x0214 }
        L_0x01b0:
            if (r10 == 0) goto L_0x01b5
            r10.close()     // Catch:{ IOException -> 0x0219 }
        L_0x01b5:
            if (r3 == 0) goto L_0x000a
            r3.consumeContent()     // Catch:{ IOException -> 0x01bc }
            goto L_0x000a
        L_0x01bc:
            r1 = move-exception
            goto L_0x0112
        L_0x01bf:
            com.tencent.assistant.thumbnailCache.b[] r0 = r14.d     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            r1 = 0
            r0 = r0[r1]     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            byte[] r1 = r10.toByteArray()     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            r0.a(r15, r1)     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            r0 = 1
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            r1 = 0
            r2 = 112(0x70, float:1.57E-43)
            r0[r1] = r2     // Catch:{ Exception -> 0x00e2, all -> 0x014b }
            com.tencent.assistant.utils.n r1 = r14.g
            r1.a(r9)
            if (r4 == 0) goto L_0x01dd
            r4.close()     // Catch:{ IOException -> 0x021e }
        L_0x01dd:
            if (r10 == 0) goto L_0x01e2
            r10.close()     // Catch:{ IOException -> 0x0223 }
        L_0x01e2:
            if (r3 == 0) goto L_0x000a
            r3.consumeContent()     // Catch:{ IOException -> 0x01e9 }
            goto L_0x000a
        L_0x01e9:
            r1 = move-exception
            goto L_0x0112
        L_0x01ec:
            r0 = 1
            byte[] r0 = new byte[r0]
            r1 = 0
            r2 = 112(0x70, float:1.57E-43)
            r0[r1] = r2
            goto L_0x000a
        L_0x01f6:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0156
        L_0x01fc:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x015b
        L_0x0202:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0160
        L_0x0208:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0105
        L_0x020e:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x010a
        L_0x0214:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x01b0
        L_0x0219:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x01b5
        L_0x021e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x01dd
        L_0x0223:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x01e2
        L_0x0228:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0136
        L_0x022e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x013b
        L_0x0234:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x017b
        L_0x023a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0180
        L_0x0240:
            r0 = move-exception
            r3 = r1
            r4 = r2
            goto L_0x014c
        L_0x0245:
            r0 = move-exception
            r1 = r3
            r2 = r4
            goto L_0x00e5
        L_0x024a:
            r0 = move-exception
            r1 = r3
            r2 = r4
            goto L_0x00e5
        L_0x024f:
            r5 = r7
            goto L_0x00d2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.thumbnailCache.f.a(java.lang.String, int, org.apache.http.HttpResponse):byte[]");
    }

    /* access modifiers changed from: private */
    public Bitmap c(o oVar) {
        switch (oVar.d()) {
            case 3:
                return b(oVar.c(), oVar.d());
            case 4:
                return c(oVar.c(), oVar.d());
            case 5:
                return d(oVar.c(), oVar.d());
            case 6:
                return a(oVar.c(), oVar.d(), 120, 120);
            case 7:
                return a(oVar.c(), oVar.d(), 150, 150, 1);
            case 8:
                return e(oVar.c(), oVar.d());
            default:
                return null;
        }
    }

    private Bitmap b(String str, int i) {
        Bitmap bitmap = null;
        try {
            PackageManager packageManager = AstApp.i().getPackageManager();
            if (!(packageManager == null || (bitmap = am.a(packageManager.getApplicationIcon(str))) == null)) {
                this.d[i].a(str, bitmap);
            }
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
        }
        return bitmap;
    }

    private Bitmap c(String str, int i) {
        return am.a(this.d[i].e(str));
    }

    private Bitmap d(String str, int i) {
        PackageInfo packageInfo;
        try {
            packageInfo = AstApp.i().getPackageManager().getPackageArchiveInfo(str, 1);
        } catch (Exception e2) {
            packageInfo = null;
        }
        if (packageInfo != null) {
            ApplicationInfo applicationInfo = packageInfo.applicationInfo;
            if (Build.VERSION.SDK_INT >= 8) {
                applicationInfo.sourceDir = str;
                applicationInfo.publicSourceDir = str;
            }
            Drawable loadIcon = applicationInfo.loadIcon(AstApp.i().getPackageManager());
            if (loadIcon != null) {
                if (loadIcon instanceof BitmapDrawable) {
                    return ((BitmapDrawable) loadIcon).getBitmap();
                }
                if (loadIcon instanceof StateListDrawable) {
                    return ((BitmapDrawable) loadIcon.getCurrent()).getBitmap();
                }
            }
        }
        return null;
    }

    private Bitmap a(String str, int i, int i2, int i3) {
        int i4 = 1;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(str, options);
        int i5 = options.outHeight;
        int i6 = options.outWidth / i2;
        int i7 = i5 / i3;
        if (i6 >= i7) {
            i6 = i7;
        }
        if (i6 > 0) {
            i4 = i6;
        }
        options.inSampleSize = i4;
        options.inJustDecodeBounds = false;
        return ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(str, options), i2, i3, 2);
    }

    private Bitmap a(String str, int i, int i2, int i3, int i4) {
        return ThumbnailUtils.extractThumbnail(ThumbnailUtils.createVideoThumbnail(str, 1), i2, i3, 2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x009e A[SYNTHETIC, Splitter:B:36:0x009e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.Bitmap e(java.lang.String r11, int r12) {
        /*
            r10 = this;
            r6 = 0
            r0 = 0
            r3 = 0
            java.lang.String r1 = "|"
            int r1 = r11.indexOf(r1)
            r2 = -1
            if (r2 == r1) goto L_0x0031
            java.lang.String r2 = r11.substring(r3, r1)
            long r2 = java.lang.Long.parseLong(r2)
            int r1 = r1 + 1
            int r4 = r11.length()
            java.lang.String r1 = r11.substring(r1, r4)
            long r4 = java.lang.Long.parseLong(r1)
            int r1 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r1 >= 0) goto L_0x0032
            int r1 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r1 < 0) goto L_0x0031
            r0 = -1
            android.graphics.Bitmap r0 = a(r2, r0)
        L_0x0031:
            return r0
        L_0x0032:
            com.qq.AppService.AstApp r1 = com.qq.AppService.AstApp.i()
            android.content.ContentResolver r1 = r1.getContentResolver()
            android.net.Uri r6 = com.tencent.assistant.thumbnailCache.f.f
            android.net.Uri r6 = android.content.ContentUris.withAppendedId(r6, r4)
            if (r6 == 0) goto L_0x0031
            java.io.InputStream r0 = r1.openInputStream(r6)     // Catch:{ FileNotFoundException -> 0x007b, all -> 0x0098 }
            android.graphics.BitmapFactory$Options r7 = new android.graphics.BitmapFactory$Options     // Catch:{ FileNotFoundException -> 0x00ae, all -> 0x00a7 }
            r7.<init>()     // Catch:{ FileNotFoundException -> 0x00ae, all -> 0x00a7 }
            r8 = 1
            r7.inSampleSize = r8     // Catch:{ FileNotFoundException -> 0x00ae, all -> 0x00a7 }
            r8 = 1
            r7.inJustDecodeBounds = r8     // Catch:{ FileNotFoundException -> 0x00ae, all -> 0x00a7 }
            r8 = 0
            android.graphics.BitmapFactory.decodeStream(r0, r8, r7)     // Catch:{ FileNotFoundException -> 0x00ae, all -> 0x00a7 }
            r8 = 30
            int r8 = a(r7, r8)     // Catch:{ FileNotFoundException -> 0x00ae, all -> 0x00a7 }
            r7.inSampleSize = r8     // Catch:{ FileNotFoundException -> 0x00ae, all -> 0x00a7 }
            r8 = 0
            r7.inJustDecodeBounds = r8     // Catch:{ FileNotFoundException -> 0x00ae, all -> 0x00a7 }
            r8 = 0
            r7.inDither = r8     // Catch:{ FileNotFoundException -> 0x00ae, all -> 0x00a7 }
            android.graphics.Bitmap$Config r8 = android.graphics.Bitmap.Config.ARGB_8888     // Catch:{ FileNotFoundException -> 0x00ae, all -> 0x00a7 }
            r7.inPreferredConfig = r8     // Catch:{ FileNotFoundException -> 0x00ae, all -> 0x00a7 }
            java.io.InputStream r1 = r1.openInputStream(r6)     // Catch:{ FileNotFoundException -> 0x00ae, all -> 0x00a7 }
            r0 = 0
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r1, r0, r7)     // Catch:{ FileNotFoundException -> 0x00b1 }
            if (r1 == 0) goto L_0x0031
            r1.close()     // Catch:{ IOException -> 0x0076 }
            goto L_0x0031
        L_0x0076:
            r1 = move-exception
        L_0x0077:
            r1.printStackTrace()
            goto L_0x0031
        L_0x007b:
            r1 = move-exception
            r1 = r0
        L_0x007d:
            android.graphics.Bitmap r0 = a(r2, r4)     // Catch:{ all -> 0x00ac }
            if (r0 == 0) goto L_0x0090
            android.graphics.Bitmap$Config r2 = r0.getConfig()     // Catch:{ all -> 0x00ac }
            if (r2 != 0) goto L_0x0090
            android.graphics.Bitmap$Config r2 = android.graphics.Bitmap.Config.RGB_565     // Catch:{ all -> 0x00ac }
            r3 = 0
            android.graphics.Bitmap r0 = r0.copy(r2, r3)     // Catch:{ all -> 0x00ac }
        L_0x0090:
            if (r1 == 0) goto L_0x0031
            r1.close()     // Catch:{ IOException -> 0x0096 }
            goto L_0x0031
        L_0x0096:
            r1 = move-exception
            goto L_0x0077
        L_0x0098:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x009c:
            if (r1 == 0) goto L_0x00a1
            r1.close()     // Catch:{ IOException -> 0x00a2 }
        L_0x00a1:
            throw r0
        L_0x00a2:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00a1
        L_0x00a7:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x009c
        L_0x00ac:
            r0 = move-exception
            goto L_0x009c
        L_0x00ae:
            r1 = move-exception
            r1 = r0
            goto L_0x007d
        L_0x00b1:
            r0 = move-exception
            goto L_0x007d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.thumbnailCache.f.e(java.lang.String, int):android.graphics.Bitmap");
    }

    private static Bitmap a(long j, long j2) {
        FileDescriptor fileDescriptor;
        if (j2 < 0 && j < 0) {
            return null;
        }
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            if (j2 < 0) {
                ParcelFileDescriptor openFileDescriptor = AstApp.i().getContentResolver().openFileDescriptor(Uri.parse("content://media/external/audio/media/" + j + "/albumart"), "r");
                if (openFileDescriptor != null) {
                    fileDescriptor = openFileDescriptor.getFileDescriptor();
                }
                fileDescriptor = null;
            } else {
                ParcelFileDescriptor openFileDescriptor2 = AstApp.i().getContentResolver().openFileDescriptor(ContentUris.withAppendedId(f, j2), "r");
                if (openFileDescriptor2 != null) {
                    fileDescriptor = openFileDescriptor2.getFileDescriptor();
                }
                fileDescriptor = null;
            }
            options.inSampleSize = 1;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
            options.inSampleSize = STConstAction.ACTION_HIT_OPEN;
            options.inJustDecodeBounds = false;
            options.inDither = false;
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            return BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
        } catch (FileNotFoundException e2) {
            return null;
        }
    }

    private static int a(BitmapFactory.Options options, int i) {
        int i2 = options.outWidth;
        int i3 = options.outHeight;
        int max = Math.max(i2 / i, i3 / i);
        if (max == 0) {
            return 1;
        }
        if (max > 1 && i2 > i && i2 / max < i) {
            max--;
        }
        if (max <= 1 || i3 <= i || i3 / max >= i) {
            return max;
        }
        return max - 1;
    }

    public void a(APN apn) {
        this.e.b();
    }

    public void d() {
        this.d[10].a();
        this.d[2].a();
        this.d[4].a();
        this.d[6].a();
        this.d[7].a();
    }

    public byte[] a(Bitmap bitmap) {
        aw awVar = new aw(this.g);
        byte[] bArr = null;
        if (awVar != null) {
            try {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, awVar);
                awVar.flush();
                bArr = awVar.a();
                if (awVar != null) {
                    try {
                        awVar.close();
                    } catch (IOException e2) {
                        e = e2;
                        e.printStackTrace();
                        return bArr;
                    }
                }
            } catch (IOException e3) {
                e3.printStackTrace();
                if (awVar != null) {
                    try {
                        awVar.close();
                    } catch (IOException e4) {
                        e = e4;
                    }
                }
            } catch (Throwable th) {
                if (awVar != null) {
                    try {
                        awVar.close();
                    } catch (IOException e5) {
                        e5.printStackTrace();
                    }
                }
                throw th;
            }
        }
        return bArr;
    }
}
