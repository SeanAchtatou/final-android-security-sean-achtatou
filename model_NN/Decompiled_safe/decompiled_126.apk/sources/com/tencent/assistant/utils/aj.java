package com.tencent.assistant.utils;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.EntranceBlock;
import com.tencent.assistant.protocol.jce.EntranceTemplate;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.component.homeEntry.HomeEntryCell1;
import com.tencent.pangu.component.homeEntry.HomeEntryCell2;
import com.tencent.pangu.component.homeEntry.HomeEntryCellBase;
import com.tencent.pangu.component.homeEntry.HomeEntryTemplateView;
import com.tencent.pangu.manager.x;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class aj {
    private static HomeEntryCellBase a(Context context, EntranceBlock entranceBlock) {
        if (entranceBlock.f1228a == 1) {
            return new HomeEntryCell1(context, entranceBlock);
        }
        if (entranceBlock.f1228a == 2) {
            return new HomeEntryCell2(context, entranceBlock);
        }
        return null;
    }

    public static HomeEntryTemplateView a(Context context, HomeEntryTemplateView homeEntryTemplateView) {
        if (homeEntryTemplateView == null || a(homeEntryTemplateView)) {
            XLog.v("home_entry", "factory--getHomeTemplateView--create new view ");
            return a(context);
        }
        XLog.v("home_entry", "factory--getHomeTemplateView--use cur view ");
        return homeEntryTemplateView;
    }

    public static HomeEntryTemplateView a(Context context) {
        EntranceTemplate j = x.a().j();
        long k = x.a().k();
        if (j == null || j.f == null || j.f.isEmpty()) {
            XLog.v("home_entry", "factory--createHomeTemplateView--data is null ");
            return null;
        }
        EntranceTemplate entranceTemplate = new EntranceTemplate(j.f1229a, j.b, j.c, j.d, j.e, j.f);
        try {
            HomeEntryTemplateView homeEntryTemplateView = new HomeEntryTemplateView(context, entranceTemplate, k);
            int size = entranceTemplate.f.size();
            int i = 0;
            int i2 = 0;
            while (i < size) {
                ArrayList arrayList = entranceTemplate.f.get(i);
                int size2 = arrayList.size();
                LinearLayout linearLayout = new LinearLayout(context);
                if (size2 == 0) {
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
                    if (layoutParams instanceof LinearLayout.LayoutParams) {
                        layoutParams.height = by.a(context, 90.0f);
                    }
                    linearLayout.setLayoutParams(layoutParams);
                    linearLayout.setVisibility(4);
                    linearLayout.setClickable(false);
                } else {
                    linearLayout.setOrientation(0);
                    linearLayout.setGravity(17);
                    linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                    for (int i3 = 0; i3 < size2; i3++) {
                        HomeEntryCellBase a2 = a(context, (EntranceBlock) arrayList.get(i3));
                        if (a2 != null) {
                            if (!(((EntranceBlock) arrayList.get(i3)).f1228a != 2 || i3 == 0 || ((EntranceBlock) arrayList.get(i3 - 1)).f1228a == 2)) {
                                linearLayout.addView(a(context, 1));
                            }
                            linearLayout.addView(a2);
                            if (((EntranceBlock) arrayList.get(i3)).f1228a == 2 && i3 != size2 - 1) {
                                linearLayout.addView(a(context, 1));
                            }
                            XLog.v("home_entry", "factory--createHomeTemplateView--rank = " + size + "--collum= " + size2 + "--cell type= " + ((int) ((EntranceBlock) arrayList.get(i3)).f1228a));
                        }
                    }
                }
                homeEntryTemplateView.a(linearLayout);
                if (i < size - 1 && ((int) entranceTemplate.f1229a) != 1) {
                    homeEntryTemplateView.a(a(context, 2));
                }
                i++;
                i2 = size2;
            }
            XLog.v("home_entry", "factory--createHomeTemplateView--total rank = " + size + "--total collum= " + i2);
            return homeEntryTemplateView;
        } catch (Exception e) {
            return null;
        }
    }

    public static View a(Context context, int i) {
        LinearLayout.LayoutParams layoutParams;
        ImageView imageView = new ImageView(context);
        if (i == 1) {
            layoutParams = new LinearLayout.LayoutParams(-2, -1);
            layoutParams.width = by.a(context, 0.5f);
        } else if (i == 2) {
            layoutParams = new LinearLayout.LayoutParams(-1, -2);
            layoutParams.height = by.a(context, 0.5f);
        } else {
            layoutParams = new LinearLayout.LayoutParams(-1, -2);
            layoutParams.height = by.a(context, 0.5f);
        }
        if (layoutParams instanceof LinearLayout.LayoutParams) {
        }
        imageView.setLayoutParams(layoutParams);
        imageView.setBackgroundColor(context.getResources().getColor(R.color.home_entry_dividing_line_color));
        return imageView;
    }

    public static boolean a(HomeEntryTemplateView homeEntryTemplateView) {
        boolean z;
        boolean z2 = false;
        if (homeEntryTemplateView == null || homeEntryTemplateView.f3672a == null) {
            return true;
        }
        StringBuilder append = new StringBuilder().append("factory--isTemplateViewOutOfData--curView is null= ").append(homeEntryTemplateView == null).append("--curView.mSourceData is null= ");
        if (homeEntryTemplateView.f3672a == null) {
            z = true;
        } else {
            z = false;
        }
        XLog.v("home_entry", append.append(z).toString());
        EntranceTemplate j = x.a().j();
        long k = x.a().k();
        if (j == null) {
            XLog.v("home_entry", "factory--isTemplateViewOutOfData--homeEntryData == null");
            return true;
        }
        XLog.v("home_entry", "factory--isTemplateViewOutOfData--compare outOfData--newId= " + j.f1229a + "--oldId= " + homeEntryTemplateView.f3672a.f1229a + "--newRevision= " + k + "--oldRevision= " + homeEntryTemplateView.b);
        if (!(j.f1229a == homeEntryTemplateView.f3672a.f1229a && k == homeEntryTemplateView.b)) {
            z2 = true;
        }
        return z2;
    }

    public static void a(String str, STInfoV2 sTInfoV2) {
        if (!TextUtils.isEmpty(str) && sTInfoV2 != null) {
            int indexOf = str.indexOf("@");
            int length = str.length();
            if (indexOf == -1 || indexOf >= length) {
                sTInfoV2.slotId = str;
                return;
            }
            sTInfoV2.slotId = str.substring(0, indexOf);
            sTInfoV2.pushInfo = str.substring(indexOf + 1, length);
        }
    }
}
