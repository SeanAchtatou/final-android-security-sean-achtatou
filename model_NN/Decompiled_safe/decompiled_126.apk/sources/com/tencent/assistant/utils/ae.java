package com.tencent.assistant.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import com.qq.AppService.AstApp;
import com.tencent.assistant.m;

/* compiled from: ProGuard */
final class ae implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Dialog f1815a;
    final /* synthetic */ Activity b;

    ae(Dialog dialog, Activity activity) {
        this.f1815a = dialog;
        this.b = activity;
    }

    public void onClick(View view) {
        this.f1815a.dismiss();
        if (m.a().y()) {
            boolean unused = FunctionUtils.e(this.b);
            this.b.finish();
            return;
        }
        this.b.finish();
        AstApp.i().f();
    }
}
