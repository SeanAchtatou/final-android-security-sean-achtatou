package com.tencent.assistant.utils;

import android.content.Intent;
import com.qq.AppService.AstApp;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.activity.SingleBrowserActivity;

/* compiled from: ProGuard */
final class ab implements Runnable {
    ab() {
    }

    public void run() {
        BaseActivity m = AstApp.m();
        if (m != null && AstApp.i().l()) {
            Intent intent = new Intent(m, SingleBrowserActivity.class);
            intent.putExtra("com.tencent.assistant.BROWSER_URL", "http://maweb.3g.qq.com/welcome.html");
            m.startActivity(intent);
        }
    }
}
