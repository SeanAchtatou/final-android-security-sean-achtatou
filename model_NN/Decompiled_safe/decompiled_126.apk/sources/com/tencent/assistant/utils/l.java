package com.tencent.assistant.utils;

import android.os.PowerManager;
import com.qq.AppService.AstApp;
import com.tencent.assistant.m;

/* compiled from: ProGuard */
public class l {
    public static boolean a() {
        try {
            PowerManager powerManager = (PowerManager) AstApp.i().getSystemService("power");
            return powerManager != null && powerManager.isScreenOn();
        } catch (Exception e) {
            return true;
        }
    }

    public static void a(int i) {
        m.a().b("key_battery_level", Integer.valueOf(i));
    }

    public static void a(boolean z) {
        m.a().b("key_battery_is_charging", Boolean.valueOf(z));
    }
}
