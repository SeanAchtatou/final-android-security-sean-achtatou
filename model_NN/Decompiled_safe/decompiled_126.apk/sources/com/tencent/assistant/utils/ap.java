package com.tencent.assistant.utils;

import android.util.Printer;

/* compiled from: ProGuard */
public class ap implements Printer {

    /* renamed from: a  reason: collision with root package name */
    private final int f1822a;
    private final String b;

    public ap(int i, String str) {
        this.f1822a = i;
        this.b = str;
    }

    public void println(String str) {
        switch (this.f1822a) {
            case 2:
                XLog.d(this.b, str);
                return;
            case 3:
                XLog.d(this.b, str);
                return;
            case 4:
                XLog.i(this.b, str);
                return;
            case 5:
                XLog.w(this.b, str);
                return;
            default:
                XLog.e(this.b, str);
                return;
        }
    }
}
