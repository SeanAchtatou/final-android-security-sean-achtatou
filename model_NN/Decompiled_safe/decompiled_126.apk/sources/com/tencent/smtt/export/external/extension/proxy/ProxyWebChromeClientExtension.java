package com.tencent.smtt.export.external.extension.proxy;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import com.tencent.smtt.export.external.extension.interfaces.IX5WebChromeClientExtension;
import com.tencent.smtt.export.external.extension.interfaces.IX5WebViewExtension;
import com.tencent.smtt.export.external.interfaces.IX5WebViewBase;
import com.tencent.smtt.export.external.interfaces.JsResult;
import java.util.HashMap;

public class ProxyWebChromeClientExtension implements IX5WebChromeClientExtension {
    protected IX5WebChromeClientExtension mWebChromeClient;

    public IX5WebChromeClientExtension getmWebChromeClient() {
        return this.mWebChromeClient;
    }

    public void setWebChromeClientExtend(IX5WebChromeClientExtension webChromeClient) {
        this.mWebChromeClient = webChromeClient;
    }

    public View getVideoLoadingProgressView() {
        if (this.mWebChromeClient != null) {
            return this.mWebChromeClient.getVideoLoadingProgressView();
        }
        return null;
    }

    public void onBackforwardFinished(int arg1) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onBackforwardFinished(arg1);
        }
    }

    public void onHitTestResultForPluginFinished(IX5WebViewExtension webView, IX5WebViewBase.HitTestResult hitTestResult, Bundle bundle) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onHitTestResultForPluginFinished(webView, hitTestResult, bundle);
        }
    }

    public void onHitTestResultFinished(IX5WebViewExtension webView, IX5WebViewBase.HitTestResult hitTestResult) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onHitTestResultFinished(webView, hitTestResult);
        }
    }

    public boolean onAddFavorite(IX5WebViewExtension view, String href, String title, JsResult result) {
        if (this.mWebChromeClient != null) {
            return this.mWebChromeClient.onAddFavorite(view, href, title, result);
        }
        return false;
    }

    public void onPrepareX5ReadPageDataFinished(IX5WebViewExtension webView, HashMap<String, String> obj) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onPrepareX5ReadPageDataFinished(webView, obj);
        }
    }

    public void onPromptScaleSaved(IX5WebViewExtension view) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onPromptScaleSaved(view);
        }
    }

    public void onPromptNotScalable(IX5WebViewExtension view) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onPromptNotScalable(view);
        }
    }

    public boolean onSavePassword(String schemePlusHost, String username, String password, boolean isReplace, Message resumeMsg) {
        if (this.mWebChromeClient != null) {
            return this.mWebChromeClient.onSavePassword(schemePlusHost, username, password, isReplace, resumeMsg);
        }
        return false;
    }

    public void onX5ReadModeAvailableChecked(HashMap<String, String> available) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onX5ReadModeAvailableChecked(available);
        }
    }

    public Object getX5WebChromeClientInstance() {
        return this.mWebChromeClient;
    }

    public void addFlashView(View view, ViewGroup.LayoutParams params) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.addFlashView(view, params);
        }
    }

    public void requestFullScreenFlash() {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.requestFullScreenFlash();
        }
    }

    public void exitFullScreenFlash() {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.exitFullScreenFlash();
        }
    }

    public void h5videoRequestFullScreen(String playerId) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.h5videoRequestFullScreen(playerId);
        }
    }

    public void h5videoExitFullScreen(String playerId) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.h5videoExitFullScreen(playerId);
        }
    }

    public void acquireWakeLock() {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.acquireWakeLock();
        }
    }

    public void releaseWakeLock() {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.releaseWakeLock();
        }
    }

    public Context getApplicationContex() {
        if (this.mWebChromeClient != null) {
            return this.mWebChromeClient.getApplicationContex();
        }
        return null;
    }

    public void onAllMetaDataFinished(IX5WebViewExtension view, HashMap<String, String> hmMeta) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onAllMetaDataFinished(view, hmMeta);
        }
    }

    public boolean onPageNotResponding(Runnable rndWait) {
        if (this.mWebChromeClient != null) {
            return this.mWebChromeClient.onPageNotResponding(rndWait);
        }
        return false;
    }

    public Object onMiscCallBack(String name, Bundle bunlde) {
        if (this.mWebChromeClient != null) {
            return this.mWebChromeClient.onMiscCallBack(name, bunlde);
        }
        return null;
    }
}
