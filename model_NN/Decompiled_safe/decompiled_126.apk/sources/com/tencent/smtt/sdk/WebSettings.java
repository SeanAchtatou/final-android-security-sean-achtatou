package com.tencent.smtt.sdk;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.webkit.WebSettings;
import com.tencent.smtt.export.external.interfaces.IX5WebSettings;
import com.tencent.smtt.utils.ReflectionUtils;

public class WebSettings {
    public static final int LOAD_CACHE_ELSE_NETWORK = 1;
    public static final int LOAD_CACHE_ONLY = 3;
    public static final int LOAD_DEFAULT = -1;
    public static final int LOAD_NORMAL = 0;
    public static final int LOAD_NO_CACHE = 2;
    private static final String LOGTAG = "WebSettings";
    private boolean isUseX5;
    private android.webkit.WebSettings mSystemWebSettings;
    private IX5WebSettings mWebSettingsImpl;

    public enum LayoutAlgorithm {
        NORMAL,
        SINGLE_COLUMN,
        NARROW_COLUMNS
    }

    public enum PluginState {
        ON,
        ON_DEMAND,
        OFF
    }

    public enum RenderPriority {
        NORMAL,
        HIGH,
        LOW
    }

    public enum TextSize {
        SMALLEST(50),
        SMALLER(75),
        NORMAL(100),
        LARGER(125),
        LARGEST(150);
        
        int value;

        private TextSize(int size) {
            this.value = size;
        }
    }

    public enum ZoomDensity {
        FAR(150),
        MEDIUM(100),
        CLOSE(75);
        
        int value;

        private ZoomDensity(int size) {
            this.value = size;
        }
    }

    WebSettings(IX5WebSettings setting) {
        this.mWebSettingsImpl = null;
        this.mSystemWebSettings = null;
        this.isUseX5 = false;
        this.mWebSettingsImpl = setting;
        this.mSystemWebSettings = null;
        this.isUseX5 = true;
    }

    WebSettings(android.webkit.WebSettings setting) {
        this.mWebSettingsImpl = null;
        this.mSystemWebSettings = null;
        this.isUseX5 = false;
        this.mWebSettingsImpl = null;
        this.mSystemWebSettings = setting;
        this.isUseX5 = false;
    }

    public void setNavDump(boolean enabled) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setNavDump(enabled);
            return;
        }
        ReflectionUtils.invokeInstance(this.mSystemWebSettings, "setNavDump", new Class[]{Boolean.TYPE}, Boolean.valueOf(enabled));
    }

    public boolean getNavDump() {
        if (this.isUseX5) {
            return this.mWebSettingsImpl.getNavDump();
        }
        Object ret = ReflectionUtils.invokeInstance(this.mSystemWebSettings, "getNavDump");
        if (ret == null) {
            return false;
        }
        return ((Boolean) ret).booleanValue();
    }

    public void setSupportZoom(boolean support) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setSupportZoom(support);
        } else {
            this.mSystemWebSettings.setSupportZoom(support);
        }
    }

    public boolean supportZoom() {
        if (this.isUseX5) {
            return this.mWebSettingsImpl.supportZoom();
        }
        return this.mSystemWebSettings.supportZoom();
    }

    @TargetApi(3)
    public void setBuiltInZoomControls(boolean enabled) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setBuiltInZoomControls(enabled);
        } else {
            this.mSystemWebSettings.setBuiltInZoomControls(enabled);
        }
    }

    @TargetApi(3)
    public boolean getBuiltInZoomControls() {
        if (this.isUseX5) {
            return this.mWebSettingsImpl.getBuiltInZoomControls();
        }
        return this.mSystemWebSettings.getBuiltInZoomControls();
    }

    @TargetApi(11)
    public void setDisplayZoomControls(boolean enabled) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setDisplayZoomControls(enabled);
        } else if (Build.VERSION.SDK_INT >= 11) {
            ReflectionUtils.invokeInstance(this.mSystemWebSettings, "setDisplayZoomControls", new Class[]{Boolean.TYPE}, Boolean.valueOf(enabled));
        }
    }

    @TargetApi(11)
    public boolean getDisplayZoomControls() {
        Object ret;
        if (this.isUseX5) {
            return this.mWebSettingsImpl.getDisplayZoomControls();
        }
        if (Build.VERSION.SDK_INT < 11 || (ret = ReflectionUtils.invokeInstance(this.mSystemWebSettings, "getDisplayZoomControls")) == null) {
            return false;
        }
        return ((Boolean) ret).booleanValue();
    }

    @TargetApi(3)
    public void setAllowFileAccess(boolean allow) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setAllowFileAccess(allow);
        } else {
            this.mSystemWebSettings.setAllowFileAccess(allow);
        }
    }

    @TargetApi(3)
    public boolean getAllowFileAccess() {
        if (this.isUseX5) {
            return this.mWebSettingsImpl.getAllowFileAccess();
        }
        return this.mSystemWebSettings.getAllowFileAccess();
    }

    @TargetApi(11)
    public void setAllowContentAccess(boolean allow) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setAllowContentAccess(allow);
        } else if (Build.VERSION.SDK_INT >= 11) {
            ReflectionUtils.invokeInstance(this.mSystemWebSettings, "setAllowContentAccess", new Class[]{Boolean.TYPE}, Boolean.valueOf(allow));
        }
    }

    @TargetApi(11)
    public boolean getAllowContentAccess() {
        Object ret;
        if (this.isUseX5) {
            return this.mWebSettingsImpl.getAllowContentAccess();
        }
        if (Build.VERSION.SDK_INT < 11 || (ret = ReflectionUtils.invokeInstance(this.mSystemWebSettings, "getAllowContentAccess")) == null) {
            return false;
        }
        return ((Boolean) ret).booleanValue();
    }

    @TargetApi(7)
    public void setLoadWithOverviewMode(boolean overview) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setLoadWithOverviewMode(overview);
        } else {
            this.mSystemWebSettings.setLoadWithOverviewMode(overview);
        }
    }

    @TargetApi(7)
    public boolean getLoadWithOverviewMode() {
        if (this.isUseX5) {
            return this.mWebSettingsImpl.getLoadWithOverviewMode();
        }
        return this.mSystemWebSettings.getLoadWithOverviewMode();
    }

    @TargetApi(11)
    @Deprecated
    public void setEnableSmoothTransition(boolean enable) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setEnableSmoothTransition(enable);
        } else if (Build.VERSION.SDK_INT >= 11) {
            ReflectionUtils.invokeInstance(this.mSystemWebSettings, "setEnableSmoothTransition", new Class[]{Boolean.TYPE}, Boolean.valueOf(enable));
        }
    }

    @Deprecated
    public boolean enableSmoothTransition() {
        Object ret;
        if (this.isUseX5) {
            return this.mWebSettingsImpl.enableSmoothTransition();
        }
        if (Build.VERSION.SDK_INT < 11 || (ret = ReflectionUtils.invokeInstance(this.mSystemWebSettings, "enableSmoothTransition")) == null) {
            return false;
        }
        return ((Boolean) ret).booleanValue();
    }

    @Deprecated
    public void setUseWebViewBackgroundForOverscrollBackground(boolean view) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setUseWebViewBackgroundForOverscrollBackground(view);
            return;
        }
        ReflectionUtils.invokeInstance(this.mSystemWebSettings, "setUseWebViewBackgroundForOverscrollBackground", new Class[]{Boolean.TYPE}, Boolean.valueOf(view));
    }

    @Deprecated
    public boolean getUseWebViewBackgroundForOverscrollBackground() {
        if (this.isUseX5) {
            return this.mWebSettingsImpl.getUseWebViewBackgroundForOverscrollBackground();
        }
        Object ret = ReflectionUtils.invokeInstance(this.mSystemWebSettings, "getUseWebViewBackgroundForOverscrollBackground");
        if (ret == null) {
            return false;
        }
        return ((Boolean) ret).booleanValue();
    }

    public void setSaveFormData(boolean save) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setSaveFormData(save);
        } else {
            this.mSystemWebSettings.setSaveFormData(save);
        }
    }

    public boolean getSaveFormData() {
        if (this.isUseX5) {
            return this.mWebSettingsImpl.getSaveFormData();
        }
        return this.mSystemWebSettings.getSaveFormData();
    }

    public void setSavePassword(boolean save) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setSavePassword(save);
        } else {
            this.mSystemWebSettings.setSavePassword(save);
        }
    }

    public boolean getSavePassword() {
        if (this.isUseX5) {
            return this.mWebSettingsImpl.getSavePassword();
        }
        return this.mSystemWebSettings.getSavePassword();
    }

    @TargetApi(14)
    public synchronized void setTextZoom(int textZoom) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setTextZoom(textZoom);
        } else if (Build.VERSION.SDK_INT >= 14) {
            ReflectionUtils.invokeInstance(this.mSystemWebSettings, "setTextZoom", new Class[]{Integer.TYPE}, Integer.valueOf(textZoom));
        }
    }

    @TargetApi(14)
    public synchronized int getTextZoom() {
        Object ret;
        int i = 0;
        synchronized (this) {
            if (this.isUseX5) {
                i = this.mWebSettingsImpl.getTextZoom();
            } else if (Build.VERSION.SDK_INT >= 14 && (ret = ReflectionUtils.invokeInstance(this.mSystemWebSettings, "getTextZoom")) != null) {
                i = ((Integer) ret).intValue();
            }
        }
        return i;
    }

    public void setTextSize(TextSize t) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setTextSize(IX5WebSettings.TextSize.valueOf(t.name()));
        } else {
            this.mSystemWebSettings.setTextSize(WebSettings.TextSize.valueOf(t.name()));
        }
    }

    public TextSize getTextSize() {
        if (this.isUseX5) {
            return TextSize.valueOf(this.mWebSettingsImpl.getTextSize().name());
        }
        return TextSize.valueOf(this.mSystemWebSettings.getTextSize().name());
    }

    @TargetApi(7)
    public void setDefaultZoom(ZoomDensity zoom) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setDefaultZoom(IX5WebSettings.ZoomDensity.valueOf(zoom.name()));
        } else {
            this.mSystemWebSettings.setDefaultZoom(WebSettings.ZoomDensity.valueOf(zoom.name()));
        }
    }

    @TargetApi(7)
    public ZoomDensity getDefaultZoom() {
        if (this.isUseX5) {
            return ZoomDensity.valueOf(this.mWebSettingsImpl.getDefaultZoom().name());
        }
        return ZoomDensity.valueOf(this.mSystemWebSettings.getDefaultZoom().name());
    }

    public void setLightTouchEnabled(boolean enabled) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setLightTouchEnabled(enabled);
        } else {
            this.mSystemWebSettings.setLightTouchEnabled(enabled);
        }
    }

    public boolean getLightTouchEnabled() {
        if (this.isUseX5) {
            return this.mWebSettingsImpl.getLightTouchEnabled();
        }
        return this.mSystemWebSettings.getLightTouchEnabled();
    }

    public void setUserAgent(String ua) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setUserAgent(ua);
        } else {
            this.mSystemWebSettings.setUserAgentString(ua);
        }
    }

    @TargetApi(3)
    public String getUserAgentString() {
        if (this.isUseX5 && this.mWebSettingsImpl != null) {
            return this.mWebSettingsImpl.getUserAgentString();
        }
        if (this.isUseX5 || this.mSystemWebSettings == null) {
            return null;
        }
        return this.mSystemWebSettings.getUserAgentString();
    }

    @TargetApi(3)
    public void setUserAgentString(String ua) {
        if (this.isUseX5 && this.mWebSettingsImpl != null) {
            this.mWebSettingsImpl.setUserAgentString(ua);
        } else if (!this.isUseX5 && this.mSystemWebSettings != null) {
            this.mSystemWebSettings.setUserAgentString(ua);
        }
    }

    public void setUseWideViewPort(boolean use) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setUseWideViewPort(use);
        } else {
            this.mSystemWebSettings.setUseWideViewPort(use);
        }
    }

    public synchronized boolean getUseWideViewPort() {
        boolean useWideViewPort;
        if (this.isUseX5) {
            useWideViewPort = this.mWebSettingsImpl.getUseWideViewPort();
        } else {
            useWideViewPort = this.mSystemWebSettings.getUseWideViewPort();
        }
        return useWideViewPort;
    }

    public void setSupportMultipleWindows(boolean support) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setSupportMultipleWindows(support);
        } else {
            this.mSystemWebSettings.setSupportMultipleWindows(support);
        }
    }

    public synchronized boolean supportMultipleWindows() {
        boolean supportMultipleWindows;
        if (this.isUseX5) {
            supportMultipleWindows = this.mWebSettingsImpl.supportMultipleWindows();
        } else {
            supportMultipleWindows = this.mSystemWebSettings.supportMultipleWindows();
        }
        return supportMultipleWindows;
    }

    public void setLayoutAlgorithm(LayoutAlgorithm l) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setLayoutAlgorithm(IX5WebSettings.LayoutAlgorithm.valueOf(l.name()));
        } else {
            this.mSystemWebSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.valueOf(l.name()));
        }
    }

    public synchronized LayoutAlgorithm getLayoutAlgorithm() {
        LayoutAlgorithm valueOf;
        if (this.isUseX5) {
            valueOf = LayoutAlgorithm.valueOf(this.mWebSettingsImpl.getLayoutAlgorithm().name());
        } else {
            valueOf = LayoutAlgorithm.valueOf(this.mSystemWebSettings.getLayoutAlgorithm().name());
        }
        return valueOf;
    }

    public synchronized void setStandardFontFamily(String font) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setStandardFontFamily(font);
        } else {
            this.mSystemWebSettings.setStandardFontFamily(font);
        }
    }

    public synchronized String getStandardFontFamily() {
        String standardFontFamily;
        if (this.isUseX5) {
            standardFontFamily = this.mWebSettingsImpl.getStandardFontFamily();
        } else {
            standardFontFamily = this.mSystemWebSettings.getStandardFontFamily();
        }
        return standardFontFamily;
    }

    public synchronized void setFixedFontFamily(String font) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setFixedFontFamily(font);
        } else {
            this.mSystemWebSettings.setFixedFontFamily(font);
        }
    }

    public synchronized String getFixedFontFamily() {
        String fixedFontFamily;
        if (this.isUseX5) {
            fixedFontFamily = this.mWebSettingsImpl.getFixedFontFamily();
        } else {
            fixedFontFamily = this.mSystemWebSettings.getFixedFontFamily();
        }
        return fixedFontFamily;
    }

    public synchronized void setSansSerifFontFamily(String font) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setSansSerifFontFamily(font);
        } else {
            this.mSystemWebSettings.setSansSerifFontFamily(font);
        }
    }

    public synchronized String getSansSerifFontFamily() {
        String sansSerifFontFamily;
        if (this.isUseX5) {
            sansSerifFontFamily = this.mWebSettingsImpl.getSansSerifFontFamily();
        } else {
            sansSerifFontFamily = this.mSystemWebSettings.getSansSerifFontFamily();
        }
        return sansSerifFontFamily;
    }

    public synchronized void setSerifFontFamily(String font) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setSerifFontFamily(font);
        } else {
            this.mSystemWebSettings.setSerifFontFamily(font);
        }
    }

    public synchronized String getSerifFontFamily() {
        String serifFontFamily;
        if (this.isUseX5) {
            serifFontFamily = this.mWebSettingsImpl.getSerifFontFamily();
        } else {
            serifFontFamily = this.mSystemWebSettings.getSerifFontFamily();
        }
        return serifFontFamily;
    }

    public synchronized void setCursiveFontFamily(String font) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setCursiveFontFamily(font);
        } else {
            this.mSystemWebSettings.setCursiveFontFamily(font);
        }
    }

    public synchronized String getCursiveFontFamily() {
        String cursiveFontFamily;
        if (this.isUseX5) {
            cursiveFontFamily = this.mWebSettingsImpl.getCursiveFontFamily();
        } else {
            cursiveFontFamily = this.mSystemWebSettings.getCursiveFontFamily();
        }
        return cursiveFontFamily;
    }

    public synchronized void setFantasyFontFamily(String font) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setFantasyFontFamily(font);
        } else {
            this.mSystemWebSettings.setFantasyFontFamily(font);
        }
    }

    public synchronized String getFantasyFontFamily() {
        String fantasyFontFamily;
        if (this.isUseX5) {
            fantasyFontFamily = this.mWebSettingsImpl.getFantasyFontFamily();
        } else {
            fantasyFontFamily = this.mSystemWebSettings.getFantasyFontFamily();
        }
        return fantasyFontFamily;
    }

    public synchronized void setMinimumFontSize(int size) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setMinimumFontSize(size);
        } else {
            this.mSystemWebSettings.setMinimumFontSize(size);
        }
    }

    public synchronized int getMinimumFontSize() {
        int minimumFontSize;
        if (this.isUseX5) {
            minimumFontSize = this.mWebSettingsImpl.getMinimumFontSize();
        } else {
            minimumFontSize = this.mSystemWebSettings.getMinimumFontSize();
        }
        return minimumFontSize;
    }

    public synchronized void setMinimumLogicalFontSize(int size) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setMinimumLogicalFontSize(size);
        } else {
            this.mSystemWebSettings.setMinimumLogicalFontSize(size);
        }
    }

    public synchronized int getMinimumLogicalFontSize() {
        int minimumLogicalFontSize;
        if (this.isUseX5) {
            minimumLogicalFontSize = this.mWebSettingsImpl.getMinimumLogicalFontSize();
        } else {
            minimumLogicalFontSize = this.mSystemWebSettings.getMinimumLogicalFontSize();
        }
        return minimumLogicalFontSize;
    }

    public synchronized void setDefaultFontSize(int size) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setDefaultFontSize(size);
        } else {
            this.mSystemWebSettings.setDefaultFontSize(size);
        }
    }

    public synchronized int getDefaultFontSize() {
        int defaultFontSize;
        if (this.isUseX5) {
            defaultFontSize = this.mWebSettingsImpl.getDefaultFontSize();
        } else {
            defaultFontSize = this.mSystemWebSettings.getDefaultFontSize();
        }
        return defaultFontSize;
    }

    public synchronized void setDefaultFixedFontSize(int size) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setDefaultFixedFontSize(size);
        } else {
            this.mSystemWebSettings.setDefaultFixedFontSize(size);
        }
    }

    public synchronized int getDefaultFixedFontSize() {
        int defaultFixedFontSize;
        if (this.isUseX5) {
            defaultFixedFontSize = this.mWebSettingsImpl.getDefaultFixedFontSize();
        } else {
            defaultFixedFontSize = this.mSystemWebSettings.getDefaultFixedFontSize();
        }
        return defaultFixedFontSize;
    }

    public void setLoadsImagesAutomatically(boolean flag) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setLoadsImagesAutomatically(flag);
        } else {
            this.mSystemWebSettings.setLoadsImagesAutomatically(flag);
        }
    }

    public synchronized boolean getLoadsImagesAutomatically() {
        boolean loadsImagesAutomatically;
        if (this.isUseX5) {
            loadsImagesAutomatically = this.mWebSettingsImpl.getLoadsImagesAutomatically();
        } else {
            loadsImagesAutomatically = this.mSystemWebSettings.getLoadsImagesAutomatically();
        }
        return loadsImagesAutomatically;
    }

    public void setBlockNetworkImage(boolean flag) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setBlockNetworkImage(flag);
        } else {
            this.mSystemWebSettings.setBlockNetworkImage(flag);
        }
    }

    public synchronized boolean getBlockNetworkImage() {
        boolean blockNetworkImage;
        if (this.isUseX5) {
            blockNetworkImage = this.mWebSettingsImpl.getBlockNetworkImage();
        } else {
            blockNetworkImage = this.mSystemWebSettings.getBlockNetworkImage();
        }
        return blockNetworkImage;
    }

    @TargetApi(8)
    public synchronized void setBlockNetworkLoads(boolean flag) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setBlockNetworkLoads(flag);
        } else if (Build.VERSION.SDK_INT >= 8) {
            this.mSystemWebSettings.setBlockNetworkLoads(flag);
        }
    }

    @TargetApi(8)
    public synchronized boolean getBlockNetworkLoads() {
        boolean z;
        if (this.isUseX5) {
            z = this.mWebSettingsImpl.getBlockNetworkLoads();
        } else if (Build.VERSION.SDK_INT >= 8) {
            z = this.mSystemWebSettings.getBlockNetworkLoads();
        } else {
            z = false;
        }
        return z;
    }

    @Deprecated
    public void setJavaScriptEnabled(boolean flag) {
        try {
            if (this.isUseX5) {
                this.mWebSettingsImpl.setJavaScriptEnabled(flag);
            } else {
                this.mSystemWebSettings.setJavaScriptEnabled(flag);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @TargetApi(16)
    public void setAllowUniversalAccessFromFileURLs(boolean isAllow) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setAllowUniversalAccessFromFileURLs(isAllow);
            return;
        }
        ReflectionUtils.invokeInstance(this.mSystemWebSettings, "setAllowUniversalAccessFromFileURLs", new Class[]{Boolean.TYPE}, Boolean.valueOf(isAllow));
    }

    @TargetApi(16)
    public void setAllowFileAccessFromFileURLs(boolean isAllow) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setAllowFileAccessFromFileURLs(isAllow);
            return;
        }
        ReflectionUtils.invokeInstance(this.mSystemWebSettings, "setAllowFileAccessFromFileURLs", new Class[]{Boolean.TYPE}, Boolean.valueOf(isAllow));
    }

    @Deprecated
    public void setPluginsEnabled(boolean flag) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setPluginsEnabled(flag);
            return;
        }
        ReflectionUtils.invokeInstance(this.mSystemWebSettings, "setPluginsEnabled", new Class[]{Boolean.TYPE}, Boolean.valueOf(flag));
    }

    @TargetApi(8)
    @Deprecated
    public synchronized void setPluginState(PluginState state) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setPluginState(IX5WebSettings.PluginState.valueOf(state.name()));
        } else if (Build.VERSION.SDK_INT >= 8) {
            WebSettings.PluginState ps = WebSettings.PluginState.valueOf(state.name());
            ReflectionUtils.invokeInstance(this.mSystemWebSettings, "setPluginState", new Class[]{WebSettings.PluginState.class}, ps);
        }
    }

    @Deprecated
    public synchronized void setPluginsPath(String pluginsPath) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setPluginsPath(pluginsPath);
        } else {
            ReflectionUtils.invokeInstance(this.mSystemWebSettings, "setPluginsPath", new Class[]{String.class}, pluginsPath);
        }
    }

    @TargetApi(5)
    @Deprecated
    public void setDatabasePath(String databasePath) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setDatabasePath(databasePath);
            return;
        }
        ReflectionUtils.invokeInstance(this.mSystemWebSettings, "setDatabasePath", new Class[]{String.class}, databasePath);
    }

    @TargetApi(5)
    public void setGeolocationDatabasePath(String databasePath) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setGeolocationDatabasePath(databasePath);
        } else {
            this.mSystemWebSettings.setGeolocationDatabasePath(databasePath);
        }
    }

    @TargetApi(7)
    public void setAppCacheEnabled(boolean flag) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setAppCacheEnabled(flag);
        } else {
            this.mSystemWebSettings.setAppCacheEnabled(flag);
        }
    }

    @TargetApi(7)
    public void setAppCachePath(String appCachePath) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setAppCachePath(appCachePath);
        } else {
            this.mSystemWebSettings.setAppCachePath(appCachePath);
        }
    }

    @TargetApi(7)
    public void setAppCacheMaxSize(long appCacheMaxSize) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setAppCacheMaxSize(appCacheMaxSize);
        } else {
            this.mSystemWebSettings.setAppCacheMaxSize(appCacheMaxSize);
        }
    }

    @TargetApi(5)
    public void setDatabaseEnabled(boolean flag) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setDatabaseEnabled(flag);
        } else {
            this.mSystemWebSettings.setDatabaseEnabled(flag);
        }
    }

    @TargetApi(7)
    public void setDomStorageEnabled(boolean flag) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setDomStorageEnabled(flag);
        } else {
            this.mSystemWebSettings.setDomStorageEnabled(flag);
        }
    }

    @TargetApi(7)
    public synchronized boolean getDomStorageEnabled() {
        boolean domStorageEnabled;
        if (this.isUseX5) {
            domStorageEnabled = this.mWebSettingsImpl.getDomStorageEnabled();
        } else {
            domStorageEnabled = this.mSystemWebSettings.getDomStorageEnabled();
        }
        return domStorageEnabled;
    }

    @TargetApi(5)
    public synchronized String getDatabasePath() {
        String databasePath;
        if (this.isUseX5) {
            databasePath = this.mWebSettingsImpl.getDatabasePath();
        } else {
            databasePath = this.mSystemWebSettings.getDatabasePath();
        }
        return databasePath;
    }

    @TargetApi(5)
    public synchronized boolean getDatabaseEnabled() {
        boolean databaseEnabled;
        if (this.isUseX5) {
            databaseEnabled = this.mWebSettingsImpl.getDatabaseEnabled();
        } else {
            databaseEnabled = this.mSystemWebSettings.getDatabaseEnabled();
        }
        return databaseEnabled;
    }

    @TargetApi(5)
    public void setGeolocationEnabled(boolean flag) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setGeolocationEnabled(flag);
        } else {
            this.mSystemWebSettings.setGeolocationEnabled(flag);
        }
    }

    public synchronized boolean getJavaScriptEnabled() {
        boolean javaScriptEnabled;
        if (this.isUseX5) {
            javaScriptEnabled = this.mWebSettingsImpl.getJavaScriptEnabled();
        } else {
            javaScriptEnabled = this.mSystemWebSettings.getJavaScriptEnabled();
        }
        return javaScriptEnabled;
    }

    @TargetApi(8)
    @Deprecated
    public synchronized boolean getPluginsEnabled() {
        boolean z = false;
        synchronized (this) {
            if (this.isUseX5) {
                z = this.mWebSettingsImpl.getPluginsEnabled();
            } else if (Build.VERSION.SDK_INT <= 17) {
                Object ret = ReflectionUtils.invokeInstance(this.mSystemWebSettings, "getPluginsEnabled");
                if (ret != null) {
                    z = ((Boolean) ret).booleanValue();
                }
            } else if (Build.VERSION.SDK_INT == 18 && WebSettings.PluginState.ON == this.mSystemWebSettings.getPluginState()) {
                z = true;
            }
        }
        return z;
    }

    @TargetApi(8)
    @Deprecated
    public synchronized PluginState getPluginState() {
        Object ret;
        PluginState pluginState = null;
        synchronized (this) {
            if (this.isUseX5) {
                pluginState = PluginState.valueOf(this.mWebSettingsImpl.getPluginState().name());
            } else if (Build.VERSION.SDK_INT >= 8 && (ret = ReflectionUtils.invokeInstance(this.mSystemWebSettings, "getPluginState")) != null) {
                pluginState = PluginState.valueOf(((WebSettings.PluginState) ret).name());
            }
        }
        return pluginState;
    }

    @Deprecated
    public synchronized String getPluginsPath() {
        String str;
        if (this.isUseX5) {
            str = this.mWebSettingsImpl.getPluginsPath();
        } else if (Build.VERSION.SDK_INT <= 17) {
            Object ret = ReflectionUtils.invokeInstance(this.mSystemWebSettings, "getPluginsPath");
            str = ret == null ? null : (String) ret;
        } else {
            str = null;
        }
        return str;
    }

    public synchronized void setJavaScriptCanOpenWindowsAutomatically(boolean flag) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setJavaScriptCanOpenWindowsAutomatically(flag);
        } else {
            this.mSystemWebSettings.setJavaScriptCanOpenWindowsAutomatically(flag);
        }
    }

    public synchronized boolean getJavaScriptCanOpenWindowsAutomatically() {
        boolean javaScriptCanOpenWindowsAutomatically;
        if (this.isUseX5) {
            javaScriptCanOpenWindowsAutomatically = this.mWebSettingsImpl.getJavaScriptCanOpenWindowsAutomatically();
        } else {
            javaScriptCanOpenWindowsAutomatically = this.mSystemWebSettings.getJavaScriptCanOpenWindowsAutomatically();
        }
        return javaScriptCanOpenWindowsAutomatically;
    }

    public synchronized void setDefaultTextEncodingName(String encoding) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setDefaultTextEncodingName(encoding);
        } else {
            this.mSystemWebSettings.setDefaultTextEncodingName(encoding);
        }
    }

    public synchronized String getDefaultTextEncodingName() {
        String defaultTextEncodingName;
        if (this.isUseX5) {
            defaultTextEncodingName = this.mWebSettingsImpl.getDefaultTextEncodingName();
        } else {
            defaultTextEncodingName = this.mSystemWebSettings.getDefaultTextEncodingName();
        }
        return defaultTextEncodingName;
    }

    @TargetApi(17)
    public static String getDefaultUserAgent(Context context) {
        if ((SDKEngine.getInstance(false) != null && SDKEngine.getInstance(false).isX5Core()) || Build.VERSION.SDK_INT < 17) {
            return null;
        }
        Object ret = ReflectionUtils.invokeStatic(android.webkit.WebSettings.class, "getDefaultUserAgent", new Class[]{Context.class}, context);
        return ret == null ? null : (String) ret;
    }

    @TargetApi(17)
    public boolean getMediaPlaybackRequiresUserGesture() {
        Object ret;
        if (this.isUseX5) {
            return this.mWebSettingsImpl.getMediaPlaybackRequiresUserGesture();
        }
        if (Build.VERSION.SDK_INT < 17 || (ret = ReflectionUtils.invokeInstance(this.mSystemWebSettings, "getMediaPlaybackRequiresUserGesture")) == null) {
            return false;
        }
        return ((Boolean) ret).booleanValue();
    }

    @TargetApi(17)
    public void setMediaPlaybackRequiresUserGesture(boolean require) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setMediaPlaybackRequiresUserGesture(require);
        } else if (Build.VERSION.SDK_INT >= 17) {
            ReflectionUtils.invokeInstance(this.mSystemWebSettings, "setMediaPlaybackRequiresUserGesture", new Class[]{Boolean.TYPE}, Boolean.valueOf(require));
        }
    }

    public void setNeedInitialFocus(boolean flag) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setNeedInitialFocus(flag);
        } else {
            this.mSystemWebSettings.setNeedInitialFocus(flag);
        }
    }

    public void setRenderPriority(RenderPriority priority) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setRenderPriority(IX5WebSettings.RenderPriority.valueOf(priority.name()));
        } else {
            this.mSystemWebSettings.setRenderPriority(WebSettings.RenderPriority.valueOf(priority.name()));
        }
    }

    public void setCacheMode(int mode) {
        if (this.isUseX5) {
            this.mWebSettingsImpl.setCacheMode(mode);
        } else {
            this.mSystemWebSettings.setCacheMode(mode);
        }
    }

    public int getCacheMode() {
        if (this.isUseX5) {
            return this.mWebSettingsImpl.getCacheMode();
        }
        return this.mSystemWebSettings.getCacheMode();
    }
}
