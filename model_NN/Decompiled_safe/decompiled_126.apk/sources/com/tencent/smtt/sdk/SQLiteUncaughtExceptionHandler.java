package com.tencent.smtt.sdk;

import android.database.sqlite.SQLiteException;
import java.lang.Thread;

public class SQLiteUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
    public void uncaughtException(Thread thread, Throwable ex) {
        if (!(ex instanceof SQLiteException)) {
            throw new RuntimeException(ex);
        }
    }
}
