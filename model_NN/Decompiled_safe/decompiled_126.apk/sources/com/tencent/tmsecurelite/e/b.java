package com.tencent.tmsecurelite.e;

import android.os.IBinder;
import android.os.Parcel;

/* compiled from: ProGuard */
public final class b implements a {

    /* renamed from: a  reason: collision with root package name */
    private IBinder f4036a;

    public b(IBinder iBinder) {
        this.f4036a = iBinder;
    }

    public IBinder asBinder() {
        return this.f4036a;
    }

    public void a(String str) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IRootService");
            obtain.writeString(str);
            this.f4036a.transact(1, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }
}
