package com.tencent.tmsecurelite.a;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* compiled from: ProGuard */
public abstract class f extends Binder implements o {
    public f() {
        attachInterface(this, "com.tencent.tmsecurelite.IEncryptDataChangeListener");
    }

    public static o a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.tencent.tmsecurelite.IEncryptDataChangeListener");
        if (queryLocalInterface == null || !(queryLocalInterface instanceof o)) {
            return new e(iBinder);
        }
        return (o) queryLocalInterface;
    }

    /* access modifiers changed from: protected */
    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        switch (i) {
            case 1:
                a(parcel.readInt());
                parcel2.writeNoException();
                return true;
            default:
                return true;
        }
    }
}
