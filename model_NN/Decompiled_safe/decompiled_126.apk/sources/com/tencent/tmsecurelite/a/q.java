package com.tencent.tmsecurelite.a;

import android.os.IInterface;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.commom.b;
import java.util.ArrayList;

/* compiled from: ProGuard */
public interface q extends IInterface {
    @Deprecated
    int a();

    @Deprecated
    int a(String str, String str2, int i);

    @Deprecated
    DataEntity a(String str);

    @Deprecated
    ArrayList<String> a(int i, int i2);

    @Deprecated
    void a(int i);

    void a(int i, int i2, b bVar);

    void a(int i, r rVar);

    void a(int i, b bVar);

    @Deprecated
    void a(m mVar, ArrayList<String> arrayList);

    @Deprecated
    void a(n nVar, ArrayList<String> arrayList);

    @Deprecated
    void a(p pVar, ArrayList<String> arrayList);

    void a(b bVar);

    void a(String str, b bVar);

    void a(String str, String str2, int i, b bVar);

    @Deprecated
    void a(boolean z);

    void a(boolean z, b bVar);

    @Deprecated
    boolean a(o oVar);

    int b(m mVar, ArrayList<String> arrayList);

    int b(n nVar, ArrayList<String> arrayList);

    int b(p pVar, ArrayList<String> arrayList);

    @Deprecated
    int b(String str, String str2, int i);

    @Deprecated
    ArrayList<DataEntity> b(int i);

    void b(int i, b bVar);

    void b(b bVar);

    void b(String str, String str2, int i, b bVar);

    @Deprecated
    boolean b();

    @Deprecated
    boolean b(o oVar);

    @Deprecated
    int c(int i);

    int c(o oVar);

    void c(int i, b bVar);

    void c(b bVar);

    @Deprecated
    boolean c();

    int d(o oVar);

    void d(b bVar);

    @Deprecated
    boolean d();
}
