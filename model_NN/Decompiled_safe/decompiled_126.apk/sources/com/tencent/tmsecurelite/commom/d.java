package com.tencent.tmsecurelite.commom;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import org.json.JSONException;

/* compiled from: ProGuard */
public abstract class d extends Binder implements b {
    public d() {
        attachInterface(this, "com.tencent.tmsecurelite.ITmsCallback");
    }

    public IBinder asBinder() {
        return null;
    }

    public static b a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.tencent.tmsecurelite.ITmsCallback");
        if (queryLocalInterface == null || !(queryLocalInterface instanceof b)) {
            return new c(iBinder);
        }
        return (b) queryLocalInterface;
    }

    /* access modifiers changed from: protected */
    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        DataEntity dataEntity;
        switch (i) {
            case 1:
                int readInt = parcel.readInt();
                try {
                    dataEntity = new DataEntity(parcel);
                } catch (JSONException e) {
                    e.printStackTrace();
                    dataEntity = null;
                }
                a(readInt, dataEntity);
                parcel2.writeNoException();
                return true;
            case 2:
                a(parcel.readInt(), DataEntity.readFromParcel(parcel));
                parcel2.writeNoException();
                return true;
            default:
                return true;
        }
    }
}
