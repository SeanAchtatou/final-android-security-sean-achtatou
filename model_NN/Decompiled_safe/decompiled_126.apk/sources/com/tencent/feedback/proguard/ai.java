package com.tencent.feedback.proguard;

import android.os.Process;
import java.nio.ByteBuffer;

/* compiled from: ProGuard */
public final class ai {
    public static boolean a(int i, int i2) {
        return 1 == i2;
    }

    public static boolean a(Object obj, Object obj2) {
        return obj.equals(obj2);
    }

    public static byte[] a(ByteBuffer byteBuffer) {
        byte[] bArr = new byte[byteBuffer.position()];
        System.arraycopy(byteBuffer.array(), 0, bArr, 0, bArr.length);
        return bArr;
    }

    static {
        byte[] bArr = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70};
        byte[] bArr2 = new byte[Process.PROC_COMBINE];
        byte[] bArr3 = new byte[Process.PROC_COMBINE];
        for (int i = 0; i < 256; i++) {
            bArr2[i] = bArr[i >>> 4];
            bArr3[i] = bArr[i & 15];
        }
    }
}
