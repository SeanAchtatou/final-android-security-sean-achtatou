package com.tencent.feedback.proguard;

import com.tencent.connect.common.Constants;

public final class D extends C0008j {
    private static byte[] i;

    /* renamed from: a  reason: collision with root package name */
    public byte f2567a = 0;
    public int b = 0;
    public byte[] c = null;
    public String d = Constants.STR_EMPTY;
    public byte e = 0;
    public byte f = 0;
    public long g = 0;
    private String h = Constants.STR_EMPTY;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long */
    public final void a(ag agVar) {
        this.f2567a = agVar.a(this.f2567a, 0, true);
        this.b = agVar.a(this.b, 1, true);
        if (i == null) {
            byte[] bArr = new byte[1];
            i = bArr;
            bArr[0] = 0;
        }
        byte[] bArr2 = i;
        this.c = agVar.c(2, true);
        this.d = agVar.b(3, true);
        this.e = agVar.a(this.e, 4, false);
        this.f = agVar.a(this.f, 5, false);
        this.g = agVar.a(this.g, 6, false);
        this.h = agVar.b(7, false);
    }

    public final void a(ah ahVar) {
        ahVar.a(this.f2567a, 0);
        ahVar.a(this.b, 1);
        ahVar.a(this.c, 2);
        ahVar.a(this.d, 3);
        ahVar.a(this.e, 4);
        ahVar.a(this.f, 5);
        ahVar.a(this.g, 6);
        if (this.h != null) {
            ahVar.a(this.h, 7);
        }
    }

    public final void a(StringBuilder sb, int i2) {
    }
}
