package com.tencent.feedback.proguard;

import android.content.Context;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.st.STConstAction;
import com.tencent.feedback.b.h;
import com.tencent.feedback.b.i;
import com.tencent.feedback.common.b;
import com.tencent.feedback.common.c;
import com.tencent.feedback.common.e;
import com.tencent.feedback.common.g;
import com.tencent.feedback.common.j;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
public final class ar implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private static long f2594a = 0;
    private Context b = null;

    public ar(Context context) {
        this.b = context;
    }

    private static synchronized long a() {
        long j;
        synchronized (ar.class) {
            j = f2594a;
        }
        return j;
    }

    private long b() {
        ba a2 = ac.a(this.b, 300);
        if (!(a2 == null || a2.b() != 300 || a2.c() == null)) {
            try {
                h c = ao.a(this.b).c();
                if (c != null) {
                    c.a(300, a2.c(), false);
                    g.b("rqdp{  common strategy setted by history}", new Object[0]);
                }
                return a2.d();
            } catch (Throwable th) {
                if (!g.a(th)) {
                    th.printStackTrace();
                }
            }
        }
        return -1;
    }

    private void a(long j, long j2) {
        B b2;
        B b3 = null;
        int i = 0;
        i a2 = ao.a();
        aw a3 = aw.a(this.b, c.a(), a2);
        boolean b4 = a3.b();
        if (!b4) {
            a3.a(60000L);
        }
        if (new Date().getTime() < j + j2) {
            g.a("lastUpdate:%d ,return not query", Long.valueOf(j));
            return;
        }
        i iVar = a2;
        int i2 = 0;
        while (iVar == null) {
            int i3 = i2 + 1;
            if (i3 >= 5) {
                break;
            }
            g.b("rqdp{  wait uphandler:} %d", 200);
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                if (!g.a(e)) {
                    e.printStackTrace();
                }
            }
            iVar = ao.a();
            i2 = i3;
        }
        if (iVar == null || !j.b(this.b)) {
            g.a("rqdp{  no uphandler or offline ,not query!!}", new Object[0]);
            return;
        }
        if (!b4) {
            try {
                if (a3.b(b.i(this.b)) <= 0) {
                    g.b("rqdp{  wait lanch record:} %d", Integer.valueOf((int) STConstAction.ACTION_HIT_OPEN));
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e2) {
                        if (!g.a(e2)) {
                            e2.printStackTrace();
                        }
                    }
                }
            } catch (Exception e3) {
                if (!g.a(e3)) {
                    e3.printStackTrace();
                }
            }
        }
        Y a4 = a.a(this.b, a3, (byte) 2);
        if (a4 != null) {
            HashMap hashMap = new HashMap();
            hashMap.put(Integer.valueOf((int) NormalErrorRecommendPage.ERROR_TYPE_SEARCH_RESULT_EMPTY), a4.a());
            b2 = new B();
            b2.f2565a = hashMap;
            Object[] objArr = new Object[1];
            if (a4.f != null) {
                i = a4.f.size();
            }
            objArr[0] = Integer.valueOf(i);
            g.b("rqdp{ common query add uin} %d", objArr);
        } else {
            b2 = null;
        }
        b3 = b2;
        iVar.a(new as(this.b, EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLIST_FAIL, 200, b3));
    }

    private void c() {
        an[] d;
        g.a("rqdp{  AppFirstRun } %s", e.a(this.b).l());
        g.a("rqdp{  clear ao count} %d", Integer.valueOf(aj.a(this.b, null, -1, Long.MAX_VALUE, -1, -1)));
        ao a2 = ao.a(this.b);
        synchronized (a2) {
            d = a2.d();
            ao.a(this.b).a(true);
        }
        if (d != null) {
            for (an f : d) {
                f.f();
            }
        }
    }

    public final void run() {
        an[] d;
        an[] d2;
        boolean z;
        long b2 = b();
        ao a2 = ao.a(this.b);
        at b3 = a2.b();
        long c = b3 == null ? -1 : (long) (b3.c() * 3600 * 1000);
        if (a2.e() == 0) {
            g.a("rqdp{  onlaunch}", new Object[0]);
            a2.a(1);
            String a3 = b.a(this.b, e.a(this.b).c());
            if (a3 == null || a3.trim().length() == 0) {
                g.d("not found apk %s", a3);
                z = false;
            } else {
                File file = new File(a3);
                if (!file.exists() || !file.canRead()) {
                    g.d("apk not exist or read %s", a3);
                    z = false;
                } else {
                    long lastModified = file.lastModified();
                    long length = file.length();
                    String B = e.a(this.b).B();
                    List<al> a4 = aj.a(this.b, a3, 0, 10);
                    al alVar = null;
                    if (a4 != null && a4.size() > 0) {
                        alVar = a4.get(0);
                    }
                    e a5 = e.a(this.b);
                    if (alVar == null) {
                        ArrayList arrayList = new ArrayList();
                        al alVar2 = new al();
                        alVar2.a(0);
                        alVar2.a(a3);
                        alVar2.b(a5.m());
                        alVar2.c(B);
                        alVar2.b(lastModified);
                        alVar2.c(length);
                        arrayList.add(alVar2);
                        aj.c(this.b, arrayList);
                        g.a("new app %s ", alVar2.d());
                        z = true;
                    } else if (a5.n()) {
                        if (a5.m().equals(alVar.d())) {
                            z = false;
                        } else {
                            aj.d(this.b, a4);
                            ArrayList arrayList2 = new ArrayList();
                            al alVar3 = new al();
                            alVar3.a(0);
                            alVar3.a(a3);
                            alVar3.b(a5.m());
                            alVar3.c(B);
                            alVar3.b(lastModified);
                            alVar3.c(length);
                            arrayList2.add(alVar3);
                            aj.c(this.b, arrayList2);
                            g.a("new app %s ", alVar3.d());
                            z = true;
                        }
                    } else if (alVar.d() == null || !B.equals(alVar.f()) || lastModified != alVar.b() || length != alVar.c()) {
                        String m = e.a(this.b).m();
                        if (m == null) {
                            g.d("rqdp{  apkid get error!return false}", new Object[0]);
                            z = false;
                        } else {
                            z = m.equals(alVar.d());
                            aj.d(this.b, a4);
                            ArrayList arrayList3 = new ArrayList();
                            al alVar4 = new al();
                            alVar4.a(0);
                            alVar4.a(a3);
                            alVar4.b(m);
                            alVar4.c(B);
                            alVar4.b(lastModified);
                            alVar4.c(length);
                            arrayList3.add(alVar4);
                            aj.c(this.b, arrayList3);
                            g.a("is new :%b %s ", Boolean.valueOf(z), alVar4.toString());
                        }
                    } else {
                        e.a(this.b).d(alVar.d());
                        z = false;
                    }
                }
            }
            if (z) {
                c();
            } else {
                long a6 = a();
                if (a6 > 0) {
                    g.b("rqdp{  delay:} %d", Long.valueOf(a6));
                    try {
                        Thread.sleep(a6);
                    } catch (InterruptedException e) {
                        if (!g.a(e)) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        g.b("rqdp{  on Query Start}", new Object[0]);
        synchronized (a2) {
            d = a2.d();
            a2.a(2);
        }
        if (d != null) {
            g.b("rqdp{  notify Query Start}", new Object[0]);
            for (an d3 : d) {
                d3.d();
            }
        }
        a(b2, c);
        g.b("rqdp{  on query end!}", new Object[0]);
        synchronized (a2) {
            d2 = a2.d();
            a2.a(3);
        }
        if (d2 != null) {
            g.b("rqdp{  notify Query end}", new Object[0]);
            for (an e2 : d2) {
                e2.e();
            }
        }
        if (c <= 0) {
            g.c("periodLimit %d return", Long.valueOf(c));
        } else if (c > 0) {
            c.a().a(this, c);
            g.b("rqdp{  next time} %d", Long.valueOf(c));
            ao.a(this.b).a(4);
        } else {
            g.b("rqdp{  stop loop query}", new Object[0]);
        }
    }
}
