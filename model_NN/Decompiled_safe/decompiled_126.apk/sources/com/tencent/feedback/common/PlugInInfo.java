package com.tencent.feedback.common;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;

/* compiled from: ProGuard */
public class PlugInInfo implements Parcelable, Serializable {
    public static final Parcelable.Creator<PlugInInfo> CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    public final String f2540a;
    public final String b;
    public final String c;

    public PlugInInfo(String str, String str2, String str3) {
        this.f2540a = str;
        this.b = str2;
        this.c = str3;
    }

    public String toString() {
        return "plid:" + this.f2540a + " plV:" + this.b + " plUUID:" + this.c;
    }

    public PlugInInfo(Parcel parcel) {
        this.f2540a = parcel.readString();
        this.b = parcel.readString();
        this.c = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f2540a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
    }
}
