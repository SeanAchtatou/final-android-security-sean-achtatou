package com.tencent.feedback.eup;

import android.content.Context;
import android.os.Process;
import com.tencent.connect.common.Constants;
import com.tencent.feedback.common.b;
import com.tencent.feedback.common.g;
import com.tencent.feedback.proguard.ac;
import com.tencent.open.SocialConstants;
import java.io.File;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public final class j implements Thread.UncaughtExceptionHandler {
    private static j c = null;

    /* renamed from: a  reason: collision with root package name */
    private Thread.UncaughtExceptionHandler f2557a = null;
    private Context b = null;

    public static synchronized j a(Context context) {
        j jVar;
        synchronized (j.class) {
            if (c == null && context != null) {
                c = new j(context);
            }
            jVar = c;
        }
        return jVar;
    }

    private j(Context context) {
        Context applicationContext;
        if (!(context == null || (applicationContext = context.getApplicationContext()) == null)) {
            context = applicationContext;
        }
        this.b = context;
    }

    public final synchronized void a() {
        g.a("rqdp{ eup regist}", new Object[0]);
        Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        if (defaultUncaughtExceptionHandler != this) {
            this.f2557a = defaultUncaughtExceptionHandler;
            Thread.setDefaultUncaughtExceptionHandler(this);
        }
    }

    public final synchronized void b() {
        g.a("rqdp{ eup unregister}", new Object[0]);
        if (Thread.getDefaultUncaughtExceptionHandler() == this) {
            Thread.setDefaultUncaughtExceptionHandler(this.f2557a);
            this.f2557a = null;
        }
    }

    private synchronized void a(Thread thread, Throwable th) {
        if (this.f2557a != null) {
            g.b("rqdp{ sys crhandle!}", new Object[0]);
            this.f2557a.uncaughtException(thread, th);
        } else {
            g.b("rqdp{ kill!}", new Object[0]);
            Process.killProcess(Process.myPid());
            System.exit(1);
        }
    }

    private static void c() {
        h hVar = new h();
        hVar.setName("ImmediateEUP");
        hVar.start();
        try {
            hVar.join(3000);
        } catch (InterruptedException e) {
            if (!g.a(e)) {
                e.printStackTrace();
            }
        }
    }

    private int a(List<e> list, int i, boolean z) {
        Context context = this.b;
        if (list == null || i <= 0) {
            return 0;
        }
        ArrayList arrayList = new ArrayList();
        Collections.sort(list, new i());
        Iterator<e> it = list.iterator();
        while (it.hasNext() && i > arrayList.size()) {
            e next = it.next();
            if (next.b() && !z) {
                break;
            }
            arrayList.add(next);
            it.remove();
        }
        if (arrayList.size() > 0) {
            return g.a(context, arrayList);
        }
        return 0;
    }

    public final boolean a(e eVar, d dVar) {
        e eVar2;
        int size;
        e eVar3;
        boolean z;
        if (eVar == null && dVar == null) {
            g.c("handler exception data params error", new Object[0]);
            return false;
        }
        if (dVar.e()) {
            Context context = this.b;
            if (eVar == null) {
                eVar3 = null;
            } else {
                String c2 = ac.c((((int) eVar.S()) + "\n" + eVar.e() + "\n" + eVar.h()).getBytes());
                if (c2 == null) {
                    g.c("rqdp{  md5 error!}", new Object[0]);
                    eVar3 = null;
                } else {
                    eVar.g(c2);
                    eVar.a(true);
                    eVar.b(1);
                    eVar.a(0);
                    List<e> a2 = g.a(context, 1, SocialConstants.PARAM_APP_DESC, -1, c2, -1, -1, -1, -1, -1, -1, null);
                    if (a2 == null || a2.size() <= 0) {
                        g.b("rqdp{  new one ,no merged!}", new Object[0]);
                        eVar3 = null;
                    } else {
                        eVar3 = a2.get(0);
                        if (eVar3.n() == null || !eVar3.n().contains(new StringBuilder().append(eVar.i()).toString())) {
                            eVar3.b(eVar3.o() + 1);
                            if (eVar3.n() == null) {
                                eVar3.f(Constants.STR_EMPTY);
                            }
                            eVar3.f(eVar3.n() + eVar.i() + "\n");
                            g.b("rqdp{  EUPDAO.insertOrUpdateEUP() start}", new Object[0]);
                            if (context == null || eVar3 == null) {
                                g.c("rqdp{  context == null || bean == null,pls check}", new Object[0]);
                                z = false;
                            } else {
                                ArrayList arrayList = new ArrayList();
                                arrayList.add(eVar3);
                                z = g.b(context, arrayList);
                            }
                            if (z) {
                                g.a("rqdp{  eupMeg update success} %b , c:%d , at:%s up:%d", Boolean.valueOf(z), Integer.valueOf(eVar3.o()), eVar3.n(), Integer.valueOf(eVar3.l()));
                                if (eVar.r() != null) {
                                    File file = new File(eVar.r());
                                    if (file.exists() && file.isFile()) {
                                        file.delete();
                                    }
                                }
                            }
                        } else {
                            g.b("rqdp{ already merged} %d", Long.valueOf(eVar.i()));
                        }
                    }
                }
            }
            if (eVar3 != null) {
                g.a("merge success return", new Object[0]);
                if (!eVar3.y() && eVar3.o() >= 2) {
                    g.a("rqdp{ may be crash too frequent! do immediate upload in merge!}", new Object[0]);
                    c();
                }
                return true;
            }
        }
        int a3 = dVar.a();
        List<e> a4 = g.a(this.b, a3 + 1, "asc", -1, null, -1, -1, -1, -1, -1, -1, null);
        if (a4 == null || a4.size() <= 0 || (size = (a4.size() - a3) + 1) <= 0 || a(a4, size, eVar.b()) >= size) {
            if (a4 != null && a4.size() > 1) {
                e eVar4 = a4.get(0);
                Iterator<e> it = a4.iterator();
                while (true) {
                    eVar2 = eVar4;
                    if (!it.hasNext()) {
                        break;
                    }
                    eVar4 = it.next();
                    if (eVar2.i() >= eVar4.i() || !eVar4.b()) {
                        eVar4 = eVar2;
                    }
                }
                if (eVar2.b() && eVar.i() - eVar2.i() < 60000) {
                    g.c("rqdp{ may be crash too frequent! do immediate upload in not merge!}", new Object[0]);
                    c();
                }
            }
            g.a(this.b, eVar, dVar);
            if (!b.e(this.b) || "main".equals(Thread.currentThread().getName())) {
                eVar.a((byte[]) null);
            } else {
                g.b("save log", new Object[0]);
                eVar.a(ac.a(dVar.g(), dVar.f()));
            }
            boolean a5 = g.a(this.b, eVar);
            g.a("store new eup pn:%s, isMe:%b , isNa:%b , res:%b ", eVar.s(), Boolean.valueOf(eVar.c()), Boolean.valueOf(eVar.b()), Boolean.valueOf(a5));
            return a5;
        }
        g.c("can't add more eup!", new Object[0]);
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c6 A[Catch:{ Throwable -> 0x019c }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0129  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0132  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0199  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x01cc  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(java.lang.String r26, java.lang.Throwable r27, java.lang.String r28, byte[] r29, boolean r30) {
        /*
            r25 = this;
            r0 = r25
            android.content.Context r1 = r0.b
            java.lang.String r15 = com.tencent.feedback.common.b.i(r1)
            java.lang.String r5 = ""
            java.lang.String r4 = ""
            if (r27 == 0) goto L_0x013d
            java.lang.String r19 = r27.getMessage()
        L_0x0012:
            if (r27 == 0) goto L_0x0141
            java.lang.Class r1 = r27.getClass()
            java.lang.String r3 = r1.getName()
        L_0x001c:
            com.tencent.feedback.eup.l r1 = com.tencent.feedback.eup.l.l()
            if (r1 != 0) goto L_0x0145
            java.lang.String r1 = "rqdp{  instance == null}"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.c(r1, r2)
            r1 = 0
        L_0x002b:
            java.util.Date r2 = new java.util.Date
            r2.<init>()
            long r7 = r2.getTime()
            com.tencent.feedback.eup.d r2 = com.tencent.feedback.eup.c.a()     // Catch:{ Throwable -> 0x014b }
            r0 = r27
            java.lang.String r5 = com.tencent.feedback.eup.g.a(r0, r2)     // Catch:{ Throwable -> 0x014b }
        L_0x003e:
            if (r5 == 0) goto L_0x0053
            java.lang.String r2 = "\n"
            boolean r2 = r5.contains(r2)
            if (r2 == 0) goto L_0x0053
            r2 = 0
            java.lang.String r4 = "\n"
            int r4 = r5.indexOf(r4)
            java.lang.String r4 = r5.substring(r2, r4)
        L_0x0053:
            java.lang.String r2 = "rqdp{ stack:}%s"
            r6 = 1
            java.lang.Object[] r6 = new java.lang.Object[r6]
            r9 = 0
            r6[r9] = r5
            com.tencent.feedback.common.g.b(r2, r6)
            if (r30 == 0) goto L_0x0193
            if (r1 == 0) goto L_0x0193
            java.lang.String r2 = "get crash extra..."
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]
            com.tencent.feedback.common.g.b(r2, r6)
            if (r1 == 0) goto L_0x007b
            java.lang.String r2 = "your crmsg"
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x015f }
            com.tencent.feedback.common.g.a(r2, r6)     // Catch:{ Throwable -> 0x015f }
            r2 = 0
            r6 = -10000(0xffffffffffffd8f0, float:NaN)
            java.lang.String r28 = r1.b(r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x015f }
        L_0x007b:
            if (r1 == 0) goto L_0x0193
            java.lang.String r2 = "your crdata"
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x017a }
            com.tencent.feedback.common.g.a(r2, r6)     // Catch:{ Throwable -> 0x017a }
            r2 = 0
            r6 = -10000(0xffffffffffffd8f0, float:NaN)
            byte[] r29 = r1.a(r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x017a }
            r24 = r29
            r23 = r28
        L_0x0090:
            r0 = r25
            android.content.Context r2 = r0.b
            com.tencent.feedback.common.e r2 = com.tencent.feedback.common.e.a(r2)
            r0 = r25
            android.content.Context r9 = r0.b
            java.lang.String r10 = r2.h()
            java.lang.String r11 = r2.p()
            long r12 = r2.k()
            java.util.Map r14 = r2.y()
            r16 = r26
            r17 = r4
            r18 = r3
            r20 = r5
            r21 = r7
            com.tencent.feedback.eup.e r13 = com.tencent.feedback.eup.g.a(r9, r10, r11, r12, r14, r15, r16, r17, r18, r19, r20, r21, r23, r24)
            if (r30 == 0) goto L_0x0199
            r2 = 0
        L_0x00bd:
            r13.a(r2)
            java.util.Map r2 = com.tencent.feedback.proguard.ac.b()     // Catch:{ Throwable -> 0x019c }
            if (r2 == 0) goto L_0x00e2
            java.util.Map r6 = r13.F()     // Catch:{ Throwable -> 0x019c }
            r6.putAll(r2)     // Catch:{ Throwable -> 0x019c }
            if (r26 == 0) goto L_0x00e2
            java.lang.String r2 = r26.trim()     // Catch:{ Throwable -> 0x019c }
            int r2 = r2.length()     // Catch:{ Throwable -> 0x019c }
            if (r2 <= 0) goto L_0x00e2
            java.util.Map r2 = r13.F()     // Catch:{ Throwable -> 0x019c }
            r0 = r26
            r2.remove(r0)     // Catch:{ Throwable -> 0x019c }
        L_0x00e2:
            r12 = 1
            if (r30 == 0) goto L_0x01c9
            if (r1 == 0) goto L_0x01c9
            java.lang.String r2 = "your ask2save"
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x01b0 }
            com.tencent.feedback.common.g.a(r2, r6)     // Catch:{ Throwable -> 0x01b0 }
            if (r19 == 0) goto L_0x0114
            java.lang.String r2 = r19.trim()     // Catch:{ Throwable -> 0x01b0 }
            int r2 = r2.length()     // Catch:{ Throwable -> 0x01b0 }
            if (r2 <= 0) goto L_0x0114
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01b0 }
            r2.<init>()     // Catch:{ Throwable -> 0x01b0 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x01b0 }
            java.lang.String r3 = ":"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x01b0 }
            r0 = r19
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ Throwable -> 0x01b0 }
            java.lang.String r3 = r2.toString()     // Catch:{ Throwable -> 0x01b0 }
        L_0x0114:
            r2 = 0
            r6 = -10000(0xffffffffffffd8f0, float:NaN)
            java.lang.String r9 = r13.m()     // Catch:{ Throwable -> 0x01b0 }
            java.lang.String r10 = r13.G()     // Catch:{ Throwable -> 0x01b0 }
            java.lang.String r11 = r13.x()     // Catch:{ Throwable -> 0x01b0 }
            boolean r1 = r1.a(r2, r3, r4, r5, r6, r7, r9, r10, r11)     // Catch:{ Throwable -> 0x01b0 }
        L_0x0127:
            if (r30 == 0) goto L_0x0130
            r0 = r25
            android.content.Context r2 = r0.b
            com.tencent.feedback.eup.BuglyBroadcastRecevier.a(r2, r13)
        L_0x0130:
            if (r1 == 0) goto L_0x01cc
            com.tencent.feedback.eup.d r1 = com.tencent.feedback.eup.c.a()
            r0 = r25
            boolean r1 = r0.a(r13, r1)
        L_0x013c:
            return r1
        L_0x013d:
            java.lang.String r19 = ""
            goto L_0x0012
        L_0x0141:
            java.lang.String r3 = ""
            goto L_0x001c
        L_0x0145:
            com.tencent.feedback.eup.b r1 = r1.q()
            goto L_0x002b
        L_0x014b:
            r2 = move-exception
            java.lang.String r6 = "create stack from throw fail!"
            r9 = 0
            java.lang.Object[] r9 = new java.lang.Object[r9]
            com.tencent.feedback.common.g.d(r6, r9)
            boolean r6 = com.tencent.feedback.common.g.a(r2)
            if (r6 != 0) goto L_0x003e
            r2.printStackTrace()
            goto L_0x003e
        L_0x015f:
            r2 = move-exception
            java.lang.String r6 = "rqdp{ get extra msg error} %s"
            r9 = 1
            java.lang.Object[] r9 = new java.lang.Object[r9]
            r10 = 0
            java.lang.String r11 = r2.toString()
            r9[r10] = r11
            com.tencent.feedback.common.g.d(r6, r9)
            boolean r6 = com.tencent.feedback.common.g.a(r2)
            if (r6 != 0) goto L_0x007b
            r2.printStackTrace()
            goto L_0x007b
        L_0x017a:
            r2 = move-exception
            java.lang.String r6 = "rqdp{ get extra msg error} %s"
            r9 = 1
            java.lang.Object[] r9 = new java.lang.Object[r9]
            r10 = 0
            java.lang.String r11 = r2.toString()
            r9[r10] = r11
            com.tencent.feedback.common.g.d(r6, r9)
            boolean r6 = com.tencent.feedback.common.g.a(r2)
            if (r6 != 0) goto L_0x0193
            r2.printStackTrace()
        L_0x0193:
            r24 = r29
            r23 = r28
            goto L_0x0090
        L_0x0199:
            r2 = 1
            goto L_0x00bd
        L_0x019c:
            r2 = move-exception
            java.lang.String r6 = "get all threads stack fail"
            r9 = 0
            java.lang.Object[] r9 = new java.lang.Object[r9]
            com.tencent.feedback.common.g.d(r6, r9)
            boolean r6 = com.tencent.feedback.common.g.a(r2)
            if (r6 != 0) goto L_0x00e2
            r2.printStackTrace()
            goto L_0x00e2
        L_0x01b0:
            r1 = move-exception
            java.lang.String r2 = "rqdp{ get extra msg error} %s"
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r4 = 0
            java.lang.String r5 = r1.toString()
            r3[r4] = r5
            com.tencent.feedback.common.g.d(r2, r3)
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x01c9
            r1.printStackTrace()
        L_0x01c9:
            r1 = r12
            goto L_0x0127
        L_0x01cc:
            java.lang.String r1 = "not to save"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.c(r1, r2)
            r1 = 0
            goto L_0x013c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.eup.j.a(java.lang.String, java.lang.Throwable, java.lang.String, byte[], boolean):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0058 A[SYNTHETIC, Splitter:B:25:0x0058] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void uncaughtException(java.lang.Thread r9, java.lang.Throwable r10) {
        /*
            r8 = this;
            r0 = 0
            r6 = 1
            monitor-enter(r8)
            if (r10 == 0) goto L_0x000e
            boolean r1 = com.tencent.feedback.common.g.a(r10)     // Catch:{ all -> 0x007d }
            if (r1 != 0) goto L_0x000e
            r10.printStackTrace()     // Catch:{ all -> 0x007d }
        L_0x000e:
            com.tencent.feedback.eup.l r1 = com.tencent.feedback.eup.l.l()     // Catch:{ all -> 0x007d }
            if (r1 != 0) goto L_0x005d
            java.lang.String r1 = "rqdp{  instance == null}"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x007d }
            com.tencent.feedback.common.g.c(r1, r2)     // Catch:{ all -> 0x007d }
            r7 = r0
        L_0x001d:
            if (r7 == 0) goto L_0x002b
            java.lang.String r0 = "your crhandler start"
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Throwable -> 0x0063 }
            com.tencent.feedback.common.g.a(r0, r1)     // Catch:{ Throwable -> 0x0063 }
            r0 = 0
            r7.a(r0)     // Catch:{ Throwable -> 0x0063 }
        L_0x002b:
            if (r9 != 0) goto L_0x0080
            java.lang.String r1 = ""
        L_0x002f:
            r3 = 0
            r4 = 0
            r5 = 1
            r0 = r8
            r2 = r10
            boolean r0 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x007d }
            java.lang.String r1 = "rqdp{ handle eup result} %b"
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x007d }
            r3 = 0
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x007d }
            r2[r3] = r0     // Catch:{ all -> 0x007d }
            com.tencent.feedback.common.g.b(r1, r2)     // Catch:{ all -> 0x007d }
            if (r7 == 0) goto L_0x009e
            java.lang.String r0 = "your crhandler end"
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Throwable -> 0x0085 }
            com.tencent.feedback.common.g.a(r0, r1)     // Catch:{ Throwable -> 0x0085 }
            r0 = 0
            boolean r0 = r7.b(r0)     // Catch:{ Throwable -> 0x0085 }
        L_0x0056:
            if (r0 == 0) goto L_0x005b
            r8.a(r9, r10)     // Catch:{ all -> 0x007d }
        L_0x005b:
            monitor-exit(r8)
            return
        L_0x005d:
            com.tencent.feedback.eup.b r0 = r1.q()     // Catch:{ all -> 0x007d }
            r7 = r0
            goto L_0x001d
        L_0x0063:
            r0 = move-exception
            java.lang.String r1 = "rqdp{ handle start error} %s"
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x007d }
            r3 = 0
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x007d }
            r2[r3] = r4     // Catch:{ all -> 0x007d }
            com.tencent.feedback.common.g.d(r1, r2)     // Catch:{ all -> 0x007d }
            boolean r1 = com.tencent.feedback.common.g.a(r0)     // Catch:{ all -> 0x007d }
            if (r1 != 0) goto L_0x002b
            r0.printStackTrace()     // Catch:{ all -> 0x007d }
            goto L_0x002b
        L_0x007d:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        L_0x0080:
            java.lang.String r1 = r9.getName()     // Catch:{ all -> 0x007d }
            goto L_0x002f
        L_0x0085:
            r0 = move-exception
            java.lang.String r1 = "rqdp{ your crash handle end error} %s"
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x007d }
            r3 = 0
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x007d }
            r2[r3] = r4     // Catch:{ all -> 0x007d }
            com.tencent.feedback.common.g.d(r1, r2)     // Catch:{ all -> 0x007d }
            boolean r1 = com.tencent.feedback.common.g.a(r0)     // Catch:{ all -> 0x007d }
            if (r1 != 0) goto L_0x009e
            r0.printStackTrace()     // Catch:{ all -> 0x007d }
        L_0x009e:
            r0 = r6
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.eup.j.uncaughtException(java.lang.Thread, java.lang.Throwable):void");
    }
}
