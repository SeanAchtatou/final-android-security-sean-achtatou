package com.tencent.downloadsdk;

import com.tencent.connect.common.Constants;

public class ao {

    /* renamed from: a  reason: collision with root package name */
    public int f2464a = 0;
    public String b = Constants.STR_EMPTY;
    public String c = Constants.STR_EMPTY;
    public long d = 0;

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0044 A[SYNTHETIC, Splitter:B:23:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0049 A[SYNTHETIC, Splitter:B:26:0x0049] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x005c A[SYNTHETIC, Splitter:B:35:0x005c] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0061 A[SYNTHETIC, Splitter:B:38:0x0061] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.tencent.downloadsdk.ao a(byte[] r7) {
        /*
            r2 = 0
            com.tencent.downloadsdk.ao r4 = new com.tencent.downloadsdk.ao
            r4.<init>()
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x003d, all -> 0x0057 }
            r3.<init>(r7)     // Catch:{ Exception -> 0x003d, all -> 0x0057 }
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ Exception -> 0x0077, all -> 0x006f }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0077, all -> 0x006f }
            int r0 = r1.readInt()     // Catch:{ Exception -> 0x007b, all -> 0x0072 }
            r4.f2464a = r0     // Catch:{ Exception -> 0x007b, all -> 0x0072 }
            java.lang.String r0 = r1.readUTF()     // Catch:{ Exception -> 0x007b, all -> 0x0072 }
            r4.b = r0     // Catch:{ Exception -> 0x007b, all -> 0x0072 }
            java.lang.String r0 = r1.readUTF()     // Catch:{ Exception -> 0x007b, all -> 0x0072 }
            r4.c = r0     // Catch:{ Exception -> 0x007b, all -> 0x0072 }
            long r5 = r1.readLong()     // Catch:{ Exception -> 0x007b, all -> 0x0072 }
            r4.d = r5     // Catch:{ Exception -> 0x007b, all -> 0x0072 }
            if (r3 == 0) goto L_0x002d
            r3.close()     // Catch:{ IOException -> 0x0033 }
        L_0x002d:
            if (r1 == 0) goto L_0x0032
            r1.close()     // Catch:{ IOException -> 0x0038 }
        L_0x0032:
            return r4
        L_0x0033:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x002d
        L_0x0038:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0032
        L_0x003d:
            r0 = move-exception
            r1 = r2
        L_0x003f:
            r0.printStackTrace()     // Catch:{ all -> 0x0074 }
            if (r2 == 0) goto L_0x0047
            r2.close()     // Catch:{ IOException -> 0x0052 }
        L_0x0047:
            if (r1 == 0) goto L_0x0032
            r1.close()     // Catch:{ IOException -> 0x004d }
            goto L_0x0032
        L_0x004d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0032
        L_0x0052:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0047
        L_0x0057:
            r0 = move-exception
            r1 = r2
            r3 = r2
        L_0x005a:
            if (r3 == 0) goto L_0x005f
            r3.close()     // Catch:{ IOException -> 0x0065 }
        L_0x005f:
            if (r1 == 0) goto L_0x0064
            r1.close()     // Catch:{ IOException -> 0x006a }
        L_0x0064:
            throw r0
        L_0x0065:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x005f
        L_0x006a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0064
        L_0x006f:
            r0 = move-exception
            r1 = r2
            goto L_0x005a
        L_0x0072:
            r0 = move-exception
            goto L_0x005a
        L_0x0074:
            r0 = move-exception
            r3 = r2
            goto L_0x005a
        L_0x0077:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x003f
        L_0x007b:
            r0 = move-exception
            r2 = r3
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.downloadsdk.ao.a(byte[]):com.tencent.downloadsdk.ao");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002d A[SYNTHETIC, Splitter:B:14:0x002d] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0032 A[SYNTHETIC, Splitter:B:17:0x0032] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x005f A[SYNTHETIC, Splitter:B:39:0x005f] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0064 A[SYNTHETIC, Splitter:B:42:0x0064] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(java.util.List<com.tencent.downloadsdk.ao> r6) {
        /*
            r1 = 0
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0077, all -> 0x005a }
            r3.<init>()     // Catch:{ Exception -> 0x0077, all -> 0x005a }
            java.io.DataOutputStream r2 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x007b, all -> 0x0072 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x007b, all -> 0x0072 }
            java.util.Iterator r4 = r6.iterator()     // Catch:{ Exception -> 0x0027 }
        L_0x000f:
            boolean r0 = r4.hasNext()     // Catch:{ Exception -> 0x0027 }
            if (r0 == 0) goto L_0x0037
            java.lang.Object r0 = r4.next()     // Catch:{ Exception -> 0x0027 }
            com.tencent.downloadsdk.ao r0 = (com.tencent.downloadsdk.ao) r0     // Catch:{ Exception -> 0x0027 }
            byte[] r0 = r0.a()     // Catch:{ Exception -> 0x0027 }
            int r5 = r0.length     // Catch:{ Exception -> 0x0027 }
            r2.writeInt(r5)     // Catch:{ Exception -> 0x0027 }
            r2.write(r0)     // Catch:{ Exception -> 0x0027 }
            goto L_0x000f
        L_0x0027:
            r0 = move-exception
        L_0x0028:
            r0.printStackTrace()     // Catch:{ all -> 0x0075 }
            if (r3 == 0) goto L_0x0030
            r3.close()     // Catch:{ IOException -> 0x0050 }
        L_0x0030:
            if (r2 == 0) goto L_0x0035
            r2.close()     // Catch:{ IOException -> 0x0055 }
        L_0x0035:
            r0 = r1
        L_0x0036:
            return r0
        L_0x0037:
            byte[] r0 = r3.toByteArray()     // Catch:{ Exception -> 0x0027 }
            if (r3 == 0) goto L_0x0040
            r3.close()     // Catch:{ IOException -> 0x004b }
        L_0x0040:
            if (r2 == 0) goto L_0x0036
            r2.close()     // Catch:{ IOException -> 0x0046 }
            goto L_0x0036
        L_0x0046:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0036
        L_0x004b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0040
        L_0x0050:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0030
        L_0x0055:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0035
        L_0x005a:
            r0 = move-exception
            r2 = r1
            r3 = r1
        L_0x005d:
            if (r3 == 0) goto L_0x0062
            r3.close()     // Catch:{ IOException -> 0x0068 }
        L_0x0062:
            if (r2 == 0) goto L_0x0067
            r2.close()     // Catch:{ IOException -> 0x006d }
        L_0x0067:
            throw r0
        L_0x0068:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0062
        L_0x006d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0067
        L_0x0072:
            r0 = move-exception
            r2 = r1
            goto L_0x005d
        L_0x0075:
            r0 = move-exception
            goto L_0x005d
        L_0x0077:
            r0 = move-exception
            r2 = r1
            r3 = r1
            goto L_0x0028
        L_0x007b:
            r0 = move-exception
            r2 = r1
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.downloadsdk.ao.a(java.util.List):byte[]");
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x003a A[SYNTHETIC, Splitter:B:24:0x003a] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x003f A[SYNTHETIC, Splitter:B:27:0x003f] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0049 A[SYNTHETIC, Splitter:B:33:0x0049] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x004e A[SYNTHETIC, Splitter:B:36:0x004e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<com.tencent.downloadsdk.ao> b(byte[] r6) {
        /*
            r2 = 0
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            if (r6 == 0) goto L_0x0032
            int r0 = r6.length
            if (r0 <= 0) goto L_0x0032
            r5 = 0
            java.io.ByteArrayInputStream r1 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x0033, all -> 0x0045 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x0033, all -> 0x0045 }
            java.io.DataInputStream r3 = new java.io.DataInputStream     // Catch:{ Exception -> 0x0061 }
            r3.<init>(r1)     // Catch:{ Exception -> 0x0061 }
        L_0x0016:
            int r0 = r3.readInt()     // Catch:{ Exception -> 0x0027, all -> 0x005e }
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x0027, all -> 0x005e }
            r3.readFully(r0)     // Catch:{ Exception -> 0x0027, all -> 0x005e }
            com.tencent.downloadsdk.ao r0 = a(r0)     // Catch:{ Exception -> 0x0027, all -> 0x005e }
            r4.add(r0)     // Catch:{ Exception -> 0x0027, all -> 0x005e }
            goto L_0x0016
        L_0x0027:
            r0 = move-exception
            if (r1 == 0) goto L_0x002d
            r1.close()     // Catch:{ IOException -> 0x0052 }
        L_0x002d:
            if (r3 == 0) goto L_0x0032
            r3.close()     // Catch:{ IOException -> 0x0054 }
        L_0x0032:
            return r4
        L_0x0033:
            r0 = move-exception
            r1 = r2
        L_0x0035:
            r0.printStackTrace()     // Catch:{ all -> 0x005c }
            if (r1 == 0) goto L_0x003d
            r1.close()     // Catch:{ IOException -> 0x0056 }
        L_0x003d:
            if (r2 == 0) goto L_0x0032
            r5.close()     // Catch:{ IOException -> 0x0043 }
            goto L_0x0032
        L_0x0043:
            r0 = move-exception
            goto L_0x0032
        L_0x0045:
            r0 = move-exception
            r1 = r2
        L_0x0047:
            if (r1 == 0) goto L_0x004c
            r1.close()     // Catch:{ IOException -> 0x0058 }
        L_0x004c:
            if (r2 == 0) goto L_0x0051
            r2.close()     // Catch:{ IOException -> 0x005a }
        L_0x0051:
            throw r0
        L_0x0052:
            r0 = move-exception
            goto L_0x002d
        L_0x0054:
            r0 = move-exception
            goto L_0x0032
        L_0x0056:
            r0 = move-exception
            goto L_0x003d
        L_0x0058:
            r1 = move-exception
            goto L_0x004c
        L_0x005a:
            r1 = move-exception
            goto L_0x0051
        L_0x005c:
            r0 = move-exception
            goto L_0x0047
        L_0x005e:
            r0 = move-exception
            r2 = r3
            goto L_0x0047
        L_0x0061:
            r0 = move-exception
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.downloadsdk.ao.b(byte[]):java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0040 A[SYNTHETIC, Splitter:B:23:0x0040] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0045 A[SYNTHETIC, Splitter:B:26:0x0045] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0059 A[SYNTHETIC, Splitter:B:35:0x0059] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x005e A[SYNTHETIC, Splitter:B:38:0x005e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] a() {
        /*
            r6 = this;
            r0 = 0
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0038, all -> 0x0053 }
            r3.<init>()     // Catch:{ Exception -> 0x0038, all -> 0x0053 }
            java.io.DataOutputStream r2 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x0072, all -> 0x006c }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0072, all -> 0x006c }
            int r1 = r6.f2464a     // Catch:{ Exception -> 0x0075 }
            r2.writeInt(r1)     // Catch:{ Exception -> 0x0075 }
            java.lang.String r1 = r6.b     // Catch:{ Exception -> 0x0075 }
            r2.writeUTF(r1)     // Catch:{ Exception -> 0x0075 }
            java.lang.String r1 = r6.c     // Catch:{ Exception -> 0x0075 }
            r2.writeUTF(r1)     // Catch:{ Exception -> 0x0075 }
            long r4 = r6.d     // Catch:{ Exception -> 0x0075 }
            r2.writeLong(r4)     // Catch:{ Exception -> 0x0075 }
            byte[] r0 = r3.toByteArray()     // Catch:{ Exception -> 0x0075 }
            if (r3 == 0) goto L_0x0028
            r3.close()     // Catch:{ IOException -> 0x002e }
        L_0x0028:
            if (r2 == 0) goto L_0x002d
            r2.close()     // Catch:{ IOException -> 0x0033 }
        L_0x002d:
            return r0
        L_0x002e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0028
        L_0x0033:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x002d
        L_0x0038:
            r1 = move-exception
            r2 = r0
            r3 = r0
        L_0x003b:
            r1.printStackTrace()     // Catch:{ all -> 0x0070 }
            if (r3 == 0) goto L_0x0043
            r3.close()     // Catch:{ IOException -> 0x004e }
        L_0x0043:
            if (r2 == 0) goto L_0x002d
            r2.close()     // Catch:{ IOException -> 0x0049 }
            goto L_0x002d
        L_0x0049:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x002d
        L_0x004e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0043
        L_0x0053:
            r1 = move-exception
            r2 = r0
            r3 = r0
            r0 = r1
        L_0x0057:
            if (r3 == 0) goto L_0x005c
            r3.close()     // Catch:{ IOException -> 0x0062 }
        L_0x005c:
            if (r2 == 0) goto L_0x0061
            r2.close()     // Catch:{ IOException -> 0x0067 }
        L_0x0061:
            throw r0
        L_0x0062:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x005c
        L_0x0067:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0061
        L_0x006c:
            r1 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x0057
        L_0x0070:
            r0 = move-exception
            goto L_0x0057
        L_0x0072:
            r1 = move-exception
            r2 = r0
            goto L_0x003b
        L_0x0075:
            r1 = move-exception
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.downloadsdk.ao.a():byte[]");
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ao)) {
            return false;
        }
        ao aoVar = (ao) obj;
        return aoVar.f2464a == this.f2464a && aoVar.b.equals(this.b);
    }

    public int hashCode() {
        return ((Integer.valueOf(this.f2464a).hashCode() + 629) * 37) + this.b.hashCode();
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.f2464a);
        stringBuffer.append("|");
        stringBuffer.append(this.b);
        stringBuffer.append("|");
        stringBuffer.append(this.c);
        stringBuffer.append("|");
        stringBuffer.append(this.d);
        return stringBuffer.toString();
    }
}
