package com.tencent.downloadsdk;

public class i {

    /* renamed from: a  reason: collision with root package name */
    public int f2475a;
    public byte[] b;

    private i(int i, byte[] bArr) {
        this.f2475a = i;
        this.b = bArr;
    }

    static i a(int i) {
        return new i(i, null);
    }

    static i a(int i, byte[] bArr) {
        return new i(i, bArr);
    }
}
