package com.tencent.downloadsdk;

import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import com.tencent.downloadsdk.a.c;
import com.tencent.downloadsdk.b.b;
import com.tencent.downloadsdk.utils.f;
import com.tencent.downloadsdk.utils.i;
import java.util.HashMap;

class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f2471a;

    d(c cVar) {
        this.f2471a = cVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.downloadsdk.c.a(com.tencent.downloadsdk.c, boolean):boolean
     arg types: [com.tencent.downloadsdk.c, int]
     candidates:
      com.tencent.downloadsdk.c.a(com.tencent.downloadsdk.c, int):int
      com.tencent.downloadsdk.c.a(com.tencent.downloadsdk.network.a, java.lang.String):com.tencent.downloadsdk.h
      com.tencent.downloadsdk.c.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.downloadsdk.c.a(com.tencent.downloadsdk.c, boolean):boolean */
    public void run() {
        if (!this.f2471a.x && this.f2471a.o != null && this.f2471a.B.size() <= 0) {
            if (this.f2471a.y == null || !this.f2471a.y.f2457a) {
                f.d("DownloadScheduler", "checkIfDownloadComplete ok id:" + this.f2471a.q + " mErrCode: " + this.f2471a.v);
                if (this.f2471a.o != null) {
                    String str = Constants.STR_EMPTY;
                    String str2 = Constants.STR_EMPTY;
                    if (this.f2471a.I != null) {
                        str = this.f2471a.I.m + Constants.STR_EMPTY;
                        str2 = this.f2471a.I.l + Constants.STR_EMPTY;
                    }
                    try {
                        this.f2471a.o.a(("2,reportKey=" + this.f2471a.h + ",mErrCode=" + this.f2471a.v + ",mResult=" + str + ",taskResult=" + str2 + ",detect=" + this.f2471a.c + "+" + this.f2471a.d + "+" + this.f2471a.g + "+" + this.f2471a.e + "+" + this.f2471a.f) + this.f2471a.i);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (this.f2471a.v == 0) {
                    if (this.f2471a.y == null || this.f2471a.y.c() != this.f2471a.y.b() || this.f2471a.y.b() == 0) {
                        HashMap hashMap = new HashMap();
                        hashMap.put("B110", this.f2471a.h);
                        hashMap.put("B120", this.f2471a.v + Constants.STR_EMPTY);
                        if (this.f2471a.y == null) {
                            hashMap.put("B121", "mDownloadWriteFile is null");
                        } else if (this.f2471a.y.c() != this.f2471a.y.b()) {
                            hashMap.put("B121", "SavedLength :" + this.f2471a.y.c() + ",TotalLength:" + this.f2471a.y.b());
                        }
                        a.a("TMDownloadSDK", true, 0, 0, hashMap, true);
                    } else {
                        this.f2471a.z.a(this.f2471a.j.get());
                        b.a(this.f2471a.h, this.f2471a.I, this.f2471a.p.b, this.f2471a.n, this.f2471a.f2470a, this.f2471a.f2470a, this.f2471a.z.d, this.f2471a.z.b, this.f2471a.z.c, null, this.f2471a.v, this.f2471a.v);
                        if (this.f2471a.o != null) {
                            try {
                                this.f2471a.o.b();
                            } catch (Exception e2) {
                                e2.printStackTrace();
                            }
                        }
                        if (this.f2471a.u != null) {
                            this.f2471a.u.shutdownNow();
                        }
                    }
                } else if (this.f2471a.v == 2 || this.f2471a.G) {
                    this.f2471a.z.a(this.f2471a.j.get());
                    b.a(this.f2471a.h, this.f2471a.I, this.f2471a.p.b, this.f2471a.n, this.f2471a.f2470a, this.f2471a.f2470a, this.f2471a.z.d, this.f2471a.z.b, this.f2471a.z.c, null, 2, 2);
                    this.f2471a.z.a(this.f2471a.j.get());
                    if (this.f2471a.o != null) {
                        try {
                            this.f2471a.o.c();
                        } catch (Exception e3) {
                            e3.printStackTrace();
                        }
                    }
                    if (this.f2471a.u != null) {
                        this.f2471a.u.shutdownNow();
                    }
                } else if (this.f2471a.v == 1 || this.f2471a.F) {
                    b.a(this.f2471a.h, this.f2471a.I, this.f2471a.p.b, this.f2471a.n, this.f2471a.f2470a, this.f2471a.f2470a, this.f2471a.z.d, this.f2471a.z.b, this.f2471a.z.c, null, 1, 2);
                    this.f2471a.z.a(this.f2471a.j.get());
                    if (this.f2471a.o != null) {
                        try {
                            this.f2471a.o.d();
                        } catch (Exception e4) {
                            e4.printStackTrace();
                        }
                    }
                    if (this.f2471a.u != null) {
                        this.f2471a.u.shutdownNow();
                    }
                    if (this.f2471a.y != null) {
                        this.f2471a.y.b(this.f2471a.q);
                    }
                    i.a(this.f2471a.n + ".yyb");
                    i.a(this.f2471a.n);
                } else if (this.f2471a.v < 0) {
                    this.f2471a.z.a(this.f2471a.j.get());
                    b.a(this.f2471a.h, this.f2471a.I, this.f2471a.p.b, this.f2471a.n, this.f2471a.f2470a, this.f2471a.f2470a, this.f2471a.z.d, this.f2471a.z.b, this.f2471a.z.c, this.f2471a.w, this.f2471a.v, 1);
                    if (this.f2471a.o != null) {
                        try {
                            this.f2471a.o.a(this.f2471a.v, this.f2471a.w);
                        } catch (Exception e5) {
                            e5.printStackTrace();
                        }
                    }
                    if (this.f2471a.u != null) {
                        this.f2471a.u.shutdownNow();
                    }
                    if (this.f2471a.v == -19 || this.f2471a.v == -32) {
                        if (this.f2471a.y != null) {
                            this.f2471a.y.b(this.f2471a.q);
                        }
                        i.a(this.f2471a.n + ".yyb");
                        i.a(this.f2471a.n);
                    }
                } else {
                    HashMap hashMap2 = new HashMap();
                    hashMap2.put("B110", this.f2471a.h);
                    hashMap2.put("B120", this.f2471a.v + Constants.STR_EMPTY);
                    a.a("TMDownloadSDK", true, 0, 0, hashMap2, true);
                }
                f.b("DownloadScheduler", "aveSpeed: " + c.b(this.f2471a.z.d()));
                f.b("DownloadScheduler", "costTime: " + this.f2471a.z.d);
                f.b("DownloadScheduler", "mIsDownloadFinished = true");
                boolean unused = this.f2471a.x = true;
            }
        }
    }
}
