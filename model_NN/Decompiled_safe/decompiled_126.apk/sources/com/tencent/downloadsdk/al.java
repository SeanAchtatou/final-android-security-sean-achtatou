package com.tencent.downloadsdk;

import java.util.Comparator;

class al implements Comparator<an> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ak f2461a;

    al(ak akVar) {
        this.f2461a = akVar;
    }

    /* renamed from: a */
    public int compare(an anVar, an anVar2) {
        if (anVar.h != anVar2.h) {
            return anVar.h ? 1 : -1;
        }
        int b = (int) (anVar2.b() - anVar.b());
        if (anVar.h) {
            return b;
        }
        if (anVar.a() == anVar2.a()) {
            return 0;
        }
        return !anVar.a() ? -1 : 1;
    }
}
