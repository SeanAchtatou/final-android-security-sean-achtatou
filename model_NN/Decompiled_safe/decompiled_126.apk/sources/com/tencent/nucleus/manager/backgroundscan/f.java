package com.tencent.nucleus.manager.backgroundscan;

import com.tencent.assistant.manager.notification.v;
import com.tencent.assistant.protocol.jce.PushInfo;
import com.tencent.assistant.utils.XLog;
import java.util.List;

/* compiled from: ProGuard */
class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ byte f2827a;
    final /* synthetic */ e b;

    f(e eVar, byte b2) {
        this.b = eVar;
        this.f2827a = b2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.notification.v.a(int, com.tencent.assistant.protocol.jce.PushInfo, byte[], boolean):void
     arg types: [int, com.tencent.assistant.protocol.jce.PushInfo, ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.assistant.manager.notification.v.a(int, android.app.Notification, int, boolean):void
      com.tencent.assistant.manager.notification.v.a(int, com.tencent.assistant.protocol.jce.PushInfo, byte[], boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.backgroundscan.d.a(com.tencent.nucleus.manager.backgroundscan.d, boolean):boolean
     arg types: [com.tencent.nucleus.manager.backgroundscan.d, int]
     candidates:
      com.tencent.nucleus.manager.backgroundscan.d.a(byte, com.tencent.assistant.protocol.jce.ContextItem):java.lang.String
      com.tencent.nucleus.manager.backgroundscan.d.a(byte, com.tencent.nucleus.manager.backgroundscan.BackgroundScan):java.lang.String
      com.tencent.nucleus.manager.backgroundscan.d.a(com.tencent.nucleus.manager.backgroundscan.d, byte):void
      com.tencent.nucleus.manager.backgroundscan.d.a(byte, int):void
      com.tencent.nucleus.manager.backgroundscan.d.a(com.tencent.nucleus.manager.backgroundscan.d, boolean):boolean */
    public void run() {
        XLog.d("BackgroundScan", "<push> send Notification");
        PushInfo a2 = this.b.b.a(this.f2827a);
        if (a2 != null) {
            XLog.d("BackgroundScan", "<push> send Notification successful notifyUpdateStatusBar, type = " + ((int) this.f2827a));
            v.a().a(119, a2, (byte[]) null, false);
            this.b.b.b(this.f2827a);
            List<BackgroundScan> b2 = com.tencent.assistant.db.table.f.a().b();
            if (b2.size() > 0) {
                for (BackgroundScan next : b2) {
                    next.e = -2;
                    if (this.f2827a == next.f2818a) {
                        next.d = System.currentTimeMillis();
                    }
                    com.tencent.assistant.db.table.f.a().b(next);
                }
            }
            this.b.b.f();
            o.a().a("b_new_scan_push_exp", this.f2827a, this.b.b.f, this.b.b.g);
        }
        boolean unused = this.b.b.b = false;
    }
}
