package com.tencent.nucleus.manager.main;

import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import com.tencent.android.qqdownloader.R;
import com.tencent.nucleus.manager.component.t;

/* compiled from: ProGuard */
class z implements t {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AssistantTabActivity f2968a;

    z(AssistantTabActivity assistantTabActivity) {
        this.f2968a = assistantTabActivity;
    }

    public void a() {
        if (this.f2968a.G != null) {
            this.f2968a.G.setVisibility(this.f2968a.F.b() == 0 ? 4 : 0);
        }
        this.f2968a.w.b(true);
        this.f2968a.w.b();
    }

    public void b() {
        if (this.f2968a.G != null) {
            this.f2968a.G.setVisibility(0);
            if (this.f2968a.F.b() == 0) {
                this.f2968a.G.startAnimation(AnimationUtils.loadAnimation(this.f2968a.v, R.anim.mgr_fade_in));
            } else {
                this.f2968a.b(this.f2968a.d(this.f2968a.aE));
            }
        }
        this.f2968a.w.smoothScrollTo(0, 0);
        this.f2968a.w.c();
        this.f2968a.w.b(false);
    }

    public void a(float f, Transformation transformation) {
        this.f2968a.w.smoothScrollTo(0, 0);
    }
}
