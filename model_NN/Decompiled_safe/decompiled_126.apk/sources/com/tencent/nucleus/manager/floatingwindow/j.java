package com.tencent.nucleus.manager.floatingwindow;

import android.view.animation.Animation;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class j implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FloatWindowSmallView f2906a;

    j(FloatWindowSmallView floatWindowSmallView) {
        this.f2906a = floatWindowSmallView;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        XLog.d("floatingwindow", "onAnimationEnd.. isAnimationFinish = true. isAccelerateFinish = " + this.f2906a.s);
        boolean unused = this.f2906a.u = true;
        this.f2906a.g();
        this.f2906a.e.setVisibility(8);
        this.f2906a.d.setVisibility(8);
        this.f2906a.setVisibility(0);
        try {
            this.f2906a.c.updateViewLayout(this.f2906a, this.f2906a.g);
            if (!this.f2906a.s) {
                this.f2906a.d.setVisibility(4);
            } else {
                this.f2906a.i();
            }
        } catch (Throwable th) {
            th.printStackTrace();
            XLog.d("floatingwindow", "FloatWindowSmallView >> <onAnimationEnd> updateViewLayout exception");
        }
    }
}
