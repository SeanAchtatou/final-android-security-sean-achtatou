package com.tencent.nucleus.manager.spaceclean;

import android.widget.TextView;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
class t extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TextView f3043a;
    final /* synthetic */ SubRubbishInfo b;
    final /* synthetic */ RubbishItemView c;

    t(RubbishItemView rubbishItemView, TextView textView, SubRubbishInfo subRubbishInfo) {
        this.c = rubbishItemView;
        this.f3043a = textView;
        this.b = subRubbishInfo;
    }

    public void onRightBtnClick() {
        this.c.b(this.f3043a, this.b);
    }

    public void onLeftBtnClick() {
    }

    public void onCancell() {
    }
}
