package com.tencent.nucleus.manager.spaceclean;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class s extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SubRubbishInfo f3042a;
    final /* synthetic */ v b;
    final /* synthetic */ RubbishItemView c;

    s(RubbishItemView rubbishItemView, SubRubbishInfo subRubbishInfo, v vVar) {
        this.c = rubbishItemView;
        this.f3042a = subRubbishInfo;
        this.b = vVar;
    }

    public void onTMAClick(View view) {
        if (!RubbishItemView.b) {
            if (!RubbishItemView.f3007a || this.f3042a.d || this.f3042a.e) {
                this.c.b(this.b.e, this.f3042a);
                return;
            }
            RubbishItemView.f3007a = false;
            this.c.a(this.b.e, this.f3042a);
        }
    }

    public STInfoV2 getStInfo() {
        this.c.o.actionId = 200;
        if (this.f3042a.d) {
            this.c.o.status = "02";
        } else {
            this.c.o.status = "01";
        }
        return this.c.o;
    }
}
