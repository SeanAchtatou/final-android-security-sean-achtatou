package com.tencent.nucleus.manager.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.ApkMgrActivity;
import com.tencent.assistant.activity.SpaceCleanActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity;

/* compiled from: ProGuard */
class ai implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TxManagerCommContainView f2855a;

    ai(TxManagerCommContainView txManagerCommContainView) {
        this.f2855a = txManagerCommContainView;
    }

    public void onClick(View view) {
        String str;
        String str2;
        if (this.f2855a.u != null) {
            String str3 = Constants.STR_EMPTY;
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f2855a.u, 200);
            if ((!(this.f2855a.u instanceof SpaceCleanActivity) || this.f2855a.w == null) && (!(this.f2855a.u instanceof BigFileCleanActivity) || this.f2855a.w == null)) {
                this.f2855a.i();
                if (this.f2855a.u instanceof SpaceCleanActivity) {
                    buildSTInfo.scene = STConst.ST_PAGE_RUBBISH_CLEAR_FINISH_STEWARD;
                    str3 = this.f2855a.f2845a.getString(R.string.rubbish_clear_title);
                } else if (this.f2855a.u instanceof ApkMgrActivity) {
                    buildSTInfo.scene = STConst.ST_PAGE_APK_MANAGER_EMPTY;
                    str3 = this.f2855a.f2845a.getString(R.string.apkmgr_title);
                } else if (this.f2855a.u instanceof BigFileCleanActivity) {
                    str3 = this.f2855a.f2845a.getString(R.string.space_clean_big_file_title);
                    buildSTInfo.scene = STConst.ST_PAGE_BIG_FILE_CLEAN_FINISH_STEWARD;
                }
                if (this.f2855a.v.getName().equals(SpaceCleanActivity.class.getName())) {
                    buildSTInfo.slotId = "03_001";
                    str = str3;
                    str2 = this.f2855a.f2845a.getString(R.string.rubbish_clear_title);
                } else {
                    buildSTInfo.slotId = "04_001";
                    str = str3;
                    str2 = this.f2855a.f2845a.getString(R.string.soft_admin);
                }
            } else {
                str = this.f2855a.f2845a.getString(R.string.rubbish_clear_title);
                str2 = this.f2855a.f2845a.getString(R.string.mobile_accelerate_title);
                this.f2855a.j();
                buildSTInfo.scene = STConst.ST_PAGE_RUBBISH_CLEAR_FINISH_STEWARD;
                buildSTInfo.slotId = "03_001";
            }
            l.a(buildSTInfo);
            this.f2855a.a(str, str2);
        }
    }
}
