package com.tencent.nucleus.manager.floatingwindow;

import android.view.animation.AlphaAnimation;
import com.tencent.assistant.utils.ar;

/* compiled from: ProGuard */
class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FloatWindowBigView f2902a;

    f(FloatWindowBigView floatWindowBigView) {
        this.f2902a = floatWindowBigView;
    }

    public void run() {
        this.f2902a.s.clearAnimation();
        this.f2902a.r.clearAnimation();
        this.f2902a.q.clearAnimation();
        this.f2902a.s.setVisibility(8);
        this.f2902a.r.setVisibility(8);
        this.f2902a.q.setVisibility(4);
        int unused = this.f2902a.x = (int) (ar.d() * 100.0f);
        this.f2902a.w.b((double) this.f2902a.x);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(400);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setAnimationListener(new g(this));
        this.f2902a.q.startAnimation(alphaAnimation);
    }
}
