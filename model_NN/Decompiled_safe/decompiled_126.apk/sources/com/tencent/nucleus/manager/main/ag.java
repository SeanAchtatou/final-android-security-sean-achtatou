package com.tencent.nucleus.manager.main;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.Global;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.model.QuickEntranceNotify;
import com.tencent.assistant.module.ak;
import com.tencent.assistant.module.p;
import com.tencent.assistant.protocol.jce.GetManageInfoListRequest;
import com.tencent.assistant.protocol.jce.GetManageInfoListResponse;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: ProGuard */
public class ag extends p implements ak {
    private static ag d;

    /* renamed from: a  reason: collision with root package name */
    protected ReferenceQueue<ai> f2936a;
    protected ConcurrentLinkedQueue<WeakReference<ai>> b;
    private String c = "GetManageIconEngine";

    public static synchronized ag a() {
        ag agVar;
        synchronized (ag.class) {
            if (d == null) {
                d = new ag();
            }
            agVar = d;
        }
        return agVar;
    }

    public ag() {
        com.tencent.assistant.module.ag.b().a(this);
        this.f2936a = new ReferenceQueue<>();
        this.b = new ConcurrentLinkedQueue<>();
    }

    public void a(ai aiVar) {
        if (aiVar != null) {
            while (true) {
                Reference<? extends ai> poll = this.f2936a.poll();
                if (poll == null) {
                    break;
                }
                this.b.remove(poll);
            }
            Iterator<WeakReference<ai>> it = this.b.iterator();
            while (it.hasNext()) {
                if (((ai) it.next().get()) == aiVar) {
                    return;
                }
            }
            this.b.add(new WeakReference(aiVar, this.f2936a));
        }
    }

    public void b(ai aiVar) {
        if (aiVar != null) {
            Iterator<WeakReference<ai>> it = this.b.iterator();
            while (it.hasNext()) {
                WeakReference next = it.next();
                if (((ai) next.get()) == aiVar) {
                    this.b.remove(next);
                    return;
                }
            }
        }
    }

    public void b() {
        TemporaryThreadManager.get().start(new ah(this));
    }

    /* access modifiers changed from: private */
    public boolean c() {
        GetManageInfoListResponse e = i.y().e();
        if (e == null) {
            return false;
        }
        long a2 = m.a().a((byte) 17);
        long j = e.c;
        if (m.a().a("key_g_m_i_appver", -1) == Global.getAppVersionCode() && a2 != -1 && a2 <= j) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public int d() {
        GetManageInfoListRequest getManageInfoListRequest = new GetManageInfoListRequest();
        int appVersionCode = Global.getAppVersionCode();
        getManageInfoListRequest.f1298a = appVersionCode;
        XLog.d("jeson", "sendRequest baoVersion = " + appVersionCode);
        return send(getManageInfoListRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        if (jceStruct2 != null) {
            GetManageInfoListResponse getManageInfoListResponse = (GetManageInfoListResponse) jceStruct2;
            if (getManageInfoListResponse.f1299a == 0) {
                m.a().b("key_g_m_i_appver", Integer.valueOf(Global.getAppVersionCode()));
                i.y().a(getManageInfoListResponse);
                a(getManageInfoListResponse.c);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.e("jeson", "GetManageInfoListRequest onRequestFailed ; errorCode=" + i2);
    }

    public void onLocalDataHasUpdate() {
        GetManageInfoListResponse e = i.y().e();
        if (e != null) {
            long a2 = m.a().a((byte) 17);
            long j = e.c;
            if (a2 == -1 || a2 > j) {
                d();
                return;
            }
            return;
        }
        d();
    }

    public void onPromptHasNewNotify(ArrayList<QuickEntranceNotify> arrayList) {
    }

    /* access modifiers changed from: protected */
    public void a(long j) {
        Iterator<WeakReference<ai>> it = this.b.iterator();
        while (it.hasNext()) {
            ai aiVar = (ai) it.next().get();
            if (aiVar != null) {
                aiVar.a(j);
            }
        }
    }
}
