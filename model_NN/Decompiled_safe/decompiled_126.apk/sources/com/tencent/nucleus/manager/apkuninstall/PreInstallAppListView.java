package com.tencent.nucleus.manager.apkuninstall;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;
import android.view.ViewStub;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.component.FooterView;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorPage;
import com.tencent.assistant.component.SideBar;
import com.tencent.assistant.component.txscrollview.TXRefreshListView;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.t;
import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.manager.root.e;
import com.tencent.pangu.manager.au;
import com.tencent.pangu.utils.installuninstall.p;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* compiled from: ProGuard */
public class PreInstallAppListView extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f2780a = false;
    /* access modifiers changed from: private */
    public Context b;
    /* access modifiers changed from: private */
    public TXRefreshListView c;
    private SideBar d;
    /* access modifiers changed from: private */
    public FooterView e;
    private TextView f = null;
    private LinearLayout g;
    private LoadingView h;
    private ViewStub i = null;
    private NormalErrorPage j = null;
    private Button k;
    /* access modifiers changed from: private */
    public PreInstalledAppListAdapter l;
    private e m;
    /* access modifiers changed from: private */
    public boolean n = false;
    /* access modifiers changed from: private */
    public boolean o = false;
    /* access modifiers changed from: private */
    public List<LocalApkInfo> p = Collections.synchronizedList(new ArrayList());
    /* access modifiers changed from: private */
    public Set<String> q = new HashSet();
    private int r;

    public PreInstallAppListView(Context context) {
        super(context);
        this.b = context;
        g();
    }

    public PreInstallAppListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = context;
        g();
    }

    private void g() {
        this.m = new e(this);
        LayoutInflater.from(this.b).inflate((int) R.layout.installed_applist_layout, this);
        this.c = (TXRefreshListView) findViewById(R.id.user_applist);
        this.c.setOnScrollListener(this.m);
        this.c.setDivider(null);
        this.d = (SideBar) findViewById(R.id.sidrbar);
        this.e = (FooterView) findViewById(R.id.foot_view);
        this.g = (LinearLayout) findViewById(R.id.ll_get_root);
        this.k = (Button) findViewById(R.id.bt_get_root);
        this.k.setOnClickListener(new a(this));
        this.d.setTextView(this.f);
        this.d.setOnTouchingLetterChangedListener(new b(this));
        this.e.updateContent(this.b.getString(R.string.app_admin_one_key_uninstall));
        this.e.setFooterViewEnable(false);
        this.e.setOnFooterViewClickListener(new d(this));
        this.h = (LoadingView) findViewById(R.id.loading);
        this.i = (ViewStub) findViewById(R.id.error_stub);
        this.l = new PreInstalledAppListAdapter(this.b);
        this.l.a(this.m);
        this.c.setAdapter(this.l);
        this.h.setVisibility(0);
        this.c.setVisibility(8);
        this.d.setVisibility(8);
        this.g.setVisibility(8);
        this.e.setVisibility(8);
    }

    private boolean h() {
        return e.a().c() || m.a().n() == AppConst.ROOT_STATUS.ROOTED;
    }

    public void a(List<LocalApkInfo> list, int i2) {
        this.r = i2;
        if (list == null || list.isEmpty()) {
            i();
            return;
        }
        if (h()) {
            this.g.setVisibility(8);
            if (i2 == 2) {
                this.d.setVisibility(0);
            }
        } else {
            this.g.setVisibility(0);
        }
        this.h.setVisibility(8);
        this.c.setVisibility(0);
        this.e.setVisibility(0);
        this.l.a(list, i2);
        if (this.j != null) {
            this.j.setVisibility(8);
        }
    }

    private void i() {
        if (this.j == null) {
            this.i.inflate();
            this.j = (NormalErrorPage) findViewById(R.id.error);
        }
        this.j.setErrorType(1);
        this.j.setErrorHint(getResources().getString(R.string.no_pre_apps));
        this.j.setErrorImage(R.drawable.emptypage_pic_03);
        this.j.setErrorHintTextColor(getResources().getColor(R.color.common_listiteminfo));
        this.j.setErrorHintTextSize(getResources().getDimension(R.dimen.appadmin_empty_page_text_size));
        this.j.setErrorTextVisibility(8);
        this.j.setErrorHintVisibility(0);
        this.j.setFreshButtonVisibility(8);
        this.j.setVisibility(0);
        this.h.setVisibility(8);
        this.g.setVisibility(8);
        this.c.setVisibility(8);
        this.d.setVisibility(8);
        this.e.setVisibility(8);
    }

    public void a() {
        this.d.setVisibility(8);
        this.l.a(true);
    }

    public void b() {
        if (h()) {
            this.d.setVisibility(0);
            this.l.a(false);
        }
    }

    public void a(TextView textView) {
        this.f = textView;
        if (this.d != null) {
            this.d.setTextView(this.f);
        }
    }

    public void c() {
        if (this.l != null) {
            this.l.notifyDataSetChanged();
        }
    }

    public void a(int i2) {
        this.c.setSelection(i2);
    }

    public List<LocalApkInfo> d() {
        return this.p;
    }

    public void a(LocalApkInfo localApkInfo) {
        boolean contains = this.p.contains(localApkInfo);
        if (localApkInfo.mIsSelect && !contains) {
            this.p.add(localApkInfo);
        } else if (!localApkInfo.mIsSelect && contains) {
            this.p.remove(localApkInfo);
        }
        j();
    }

    public void a(Handler handler) {
        if (this.l != null) {
            this.l.a(handler);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(int, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(android.widget.TextView, long):void
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(com.tencent.nucleus.manager.apkuninstall.i, com.tencent.assistant.localres.model.LocalApkInfo):void
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter, boolean):boolean
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, int):void
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(boolean, boolean):void */
    public boolean a(LocalApkInfo localApkInfo, String str, boolean z) {
        if (z) {
            this.p.remove(localApkInfo);
            this.q.remove(localApkInfo.mPackageName);
        } else {
            this.q.add(str);
        }
        LocalApkInfo e2 = e();
        if (e2 == null || !this.o) {
            this.n = false;
            this.o = false;
            if (this.l != null) {
                this.l.a(this.n, false);
            }
            j();
        } else {
            this.n = true;
            if (this.l != null) {
                this.l.a(this.n, true);
            }
            j();
            b(e2);
        }
        return this.n;
    }

    /* access modifiers changed from: private */
    public void b(LocalApkInfo localApkInfo) {
        if (!p.a().a(localApkInfo.mPackageName)) {
            au.a().c(localApkInfo.mAppName);
            au.a().d(localApkInfo.mPackageName);
            p.a().a(this.b, String.valueOf(localApkInfo.mAppid), localApkInfo.mPackageName, (long) localApkInfo.mVersionCode, localApkInfo.mAppName, localApkInfo.mLocalFilePath, false, false);
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        long j2;
        if (this.n) {
            this.e.updateContent(this.b.getString(R.string.uninstalling));
            this.e.setFooterViewEnable(false);
            return;
        }
        int size = this.p.size();
        long j3 = 0;
        Iterator<LocalApkInfo> it = this.p.iterator();
        while (true) {
            j2 = j3;
            if (!it.hasNext()) {
                break;
            }
            j3 = j2 + it.next().occupySize;
        }
        if (this.e != null) {
            String c2 = at.c(j2);
            String string = this.b.getString(R.string.app_admin_one_key_uninstall);
            if (size > 0) {
                this.e.setFooterViewEnable(true);
                this.e.updateContent(string, " " + String.format(this.b.getString(R.string.apkmgr_delete_format), Integer.valueOf(size), c2));
                return;
            }
            this.e.setFooterViewEnable(false);
            this.e.updateContent(string);
        }
    }

    public LocalApkInfo e() {
        if (this.p.size() > 0) {
            for (LocalApkInfo next : this.p) {
                if (!this.q.contains(next.mPackageName)) {
                    return next;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", i2 + Constants.STR_EMPTY);
        hashMap.put("B2", this.r + Constants.STR_EMPTY);
        hashMap.put("B3", Global.getPhoneGuidAndGen());
        hashMap.put("B4", Global.getQUAForBeacon());
        hashMap.put("B5", t.g());
        a.a("batchUninstallPreApp", true, -1, -1, hashMap, true);
        XLog.d("beacon", "beacon report >> event: batchUninstallPreApp, params : " + hashMap.toString());
    }

    /* access modifiers changed from: private */
    public float k() {
        try {
            Field declaredField = AbsListView.class.getDeclaredField("mVelocityTracker");
            declaredField.setAccessible(true);
            try {
                VelocityTracker velocityTracker = (VelocityTracker) declaredField.get(this.c.getListView());
                velocityTracker.computeCurrentVelocity(1000);
                return velocityTracker.getYVelocity();
            } catch (IllegalArgumentException e2) {
                e2.printStackTrace();
                return 0.0f;
            } catch (IllegalAccessException e3) {
                e3.printStackTrace();
                return 0.0f;
            } catch (Exception e4) {
                e4.printStackTrace();
                return 0.0f;
            }
        } catch (NoSuchFieldException e5) {
            e5.printStackTrace();
            return 0.0f;
        }
    }

    /* access modifiers changed from: private */
    public boolean l() {
        return Math.abs(k()) < ((float) (ViewConfiguration.get(getContext()).getScaledMinimumFlingVelocity() * 50));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(int, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(android.widget.TextView, long):void
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(com.tencent.nucleus.manager.apkuninstall.i, com.tencent.assistant.localres.model.LocalApkInfo):void
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter, boolean):boolean
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, int):void
      com.tencent.nucleus.manager.apkuninstall.PreInstalledAppListAdapter.a(boolean, boolean):void */
    public void f() {
        if (h()) {
            f2780a = true;
        } else {
            f2780a = false;
        }
        if (f2780a && this.g != null) {
            this.g.setVisibility(8);
        } else if (this.g != null) {
            this.g.setVisibility(0);
        }
        this.n = false;
        if (this.l != null) {
            this.l.a(this.n, true);
        }
        j();
    }
}
