package com.tencent.nucleus.manager.spaceclean;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.manager.spaceclean.SubRubbishInfo;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ProGuard */
public class RubbishItemView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f3007a = true;
    public static boolean b = false;
    private View c;
    private View d;
    private RelativeLayout e;
    private TXImageView f;
    private TextView g;
    private TextView h;
    /* access modifiers changed from: private */
    public ImageView i;
    /* access modifiers changed from: private */
    public LinearLayout j;
    /* access modifiers changed from: private */
    public Context k;
    private LayoutInflater l;
    private Handler m;
    /* access modifiers changed from: private */
    public n n;
    /* access modifiers changed from: private */
    public STInfoV2 o;
    /* access modifiers changed from: private */
    public boolean p;
    private int q;
    private Map<String, Bitmap> r;
    private View.OnClickListener s;

    public RubbishItemView(Context context) {
        this(context, null, null, null, false, -1.0f);
    }

    public RubbishItemView(Context context, n nVar, STInfoV2 sTInfoV2) {
        this(context, null, nVar, sTInfoV2, false, -1.0f);
    }

    public RubbishItemView(Context context, n nVar, STInfoV2 sTInfoV2, boolean z, float f2) {
        this(context, null, nVar, sTInfoV2, z, f2);
    }

    public RubbishItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, null, null, false, -1.0f);
    }

    public RubbishItemView(Context context, AttributeSet attributeSet, n nVar, STInfoV2 sTInfoV2, boolean z, float f2) {
        super(context, attributeSet);
        this.p = false;
        this.r = new HashMap();
        this.s = new u(this);
        this.k = context;
        this.l = (LayoutInflater) this.k.getSystemService("layout_inflater");
        this.n = nVar;
        this.o = sTInfoV2;
        this.p = z;
        this.q = Environment.getExternalStorageDirectory().getAbsolutePath().length();
        a();
        a(nVar, sTInfoV2, f2 == -1.0f ? this.k.getResources().getDimension(R.dimen.common_rubbish_detail_item_height) : f2);
    }

    public void a(Handler handler) {
        this.m = handler;
    }

    public void a(boolean z) {
        this.p = z;
    }

    private void a() {
        this.c = this.l.inflate((int) R.layout.space_clean_rubbish_item, this);
        this.d = this.c.findViewById(R.id.top_margin);
        this.e = (RelativeLayout) this.c.findViewById(R.id.rl_rubbish_overview);
        this.f = (TXImageView) this.c.findViewById(R.id.rubbish_icon_img);
        this.g = (TextView) this.c.findViewById(R.id.rubbish_name);
        this.h = (TextView) this.c.findViewById(R.id.rubbish_size);
        this.i = (ImageView) this.c.findViewById(R.id.iv_expand);
        this.j = (LinearLayout) this.c.findViewById(R.id.ll_rubbish_detail);
        this.i.setOnClickListener(this.s);
        this.e.setOnClickListener(this.s);
    }

    public void a(int i2) {
        if (this.d != null) {
            this.d.setVisibility(i2);
        }
    }

    public void a(n nVar, STInfoV2 sTInfoV2, float f2) {
        v vVar;
        if (nVar != null) {
            int i2 = R.drawable.pic_defaule;
            this.n = nVar;
            this.o = sTInfoV2;
            if (nVar.f3037a == 1) {
                try {
                    this.g.setText(this.k.getPackageManager().getApplicationInfo(nVar.e, 0).loadLabel(this.k.getPackageManager()));
                } catch (Exception e2) {
                    this.g.setText(this.k.getResources().getString(R.string.unknown));
                    e2.printStackTrace();
                }
            } else {
                i2 = a(nVar.b);
                this.g.setText(nVar.b);
            }
            this.f.updateImageView(nVar.e, i2, TXImageView.TXImageViewType.INSTALL_APK_ICON);
            this.h.setText(this.k.getString(R.string.rubbish_clear_selected_size, at.c(nVar.d), at.c(nVar.c)));
            int a2 = by.a(this.k, 0.7f);
            Iterator<SubRubbishInfo> it = nVar.f.iterator();
            int i3 = 0;
            while (it.hasNext()) {
                SubRubbishInfo next = it.next();
                int i4 = i3 + 1;
                View childAt = this.j.getChildAt(i3);
                int i5 = i4 + 1;
                View childAt2 = this.j.getChildAt(i4);
                if (childAt == null || childAt2 == null || childAt2.getTag() == null) {
                    View inflate = this.l.inflate((int) R.layout.space_clean_sub_rubbish_item, (ViewGroup) null);
                    v vVar2 = new v(this, null);
                    vVar2.f3045a = (ImageView) inflate.findViewById(R.id.iv_thumbnail);
                    vVar2.b = (TextView) inflate.findViewById(R.id.rubbish_name);
                    vVar2.c = (TextView) inflate.findViewById(R.id.rubbish_from);
                    vVar2.d = (TextView) inflate.findViewById(R.id.rubbish_size);
                    vVar2.e = (TextView) inflate.findViewById(R.id.iv_check);
                    inflate.setTag(vVar2);
                    View view = new View(this.k);
                    view.setBackgroundColor(this.k.getResources().getColor(R.color.appadmin_list_item_divide_line));
                    this.j.addView(view, new LinearLayout.LayoutParams(-1, a2));
                    this.j.addView(inflate, new LinearLayout.LayoutParams(-1, (int) f2));
                    vVar = vVar2;
                    childAt2 = inflate;
                } else {
                    vVar = (v) childAt2.getTag();
                    childAt.setVisibility(0);
                    childAt2.setVisibility(0);
                }
                vVar.f3045a.setTag(next.f.get(0));
                a(vVar, next, childAt2);
                i3 = i5;
            }
            int childCount = this.j.getChildCount();
            if (i3 < childCount) {
                while (i3 < childCount) {
                    View childAt3 = this.j.getChildAt(i3);
                    if (childAt3 != null) {
                        childAt3.setVisibility(8);
                    }
                    i3++;
                }
            }
            if (this.n.g) {
                this.j.setVisibility(0);
                this.i.setImageResource(R.drawable.icon_close);
                return;
            }
            this.j.setVisibility(8);
            this.i.setImageResource(R.drawable.icon_open);
        }
    }

    private void a(v vVar, SubRubbishInfo subRubbishInfo, View view) {
        if (subRubbishInfo.f3011a == SubRubbishInfo.RubbishType.VIDEO_FILE) {
            if (subRubbishInfo.f == null || this.r.get(subRubbishInfo.f.get(0)) == null) {
                vVar.f3045a.setImageDrawable(this.k.getResources().getDrawable(R.drawable.common_videopic));
                TemporaryThreadManager.get().start(new p(this, subRubbishInfo, vVar));
            } else {
                vVar.f3045a.setImageBitmap(this.r.get(subRubbishInfo.f.get(0)));
            }
            vVar.f3045a.setVisibility(0);
        } else {
            vVar.f3045a.setVisibility(8);
        }
        vVar.b.setText(subRubbishInfo.b);
        if (this.p) {
            vVar.c.setVisibility(0);
            String str = subRubbishInfo.f != null ? subRubbishInfo.f.get(0) : Constants.STR_EMPTY;
            String substring = str.substring(this.q, str.lastIndexOf("/"));
            if (TextUtils.isEmpty(substring)) {
                substring = "/sdcard";
            } else {
                String[] split = substring.split("/");
                if (split.length > 2) {
                    substring = File.separator + split[1] + File.separator + split[2];
                }
            }
            vVar.c.setText(this.k.getResources().getString(R.string.space_clean_rubbish_from, substring));
        } else {
            vVar.c.setVisibility(8);
        }
        vVar.d.setText(at.c(subRubbishInfo.c));
        vVar.e.setSelected(subRubbishInfo.d);
        vVar.e.setOnClickListener(new r(this, subRubbishInfo, vVar));
        view.setOnClickListener(new s(this, subRubbishInfo, vVar));
    }

    /* access modifiers changed from: private */
    public Bitmap a(String str, int i2, int i3, int i4) {
        if (str == null) {
            return null;
        }
        if (this.r.get(str) != null) {
            return this.r.get(str);
        }
        Bitmap extractThumbnail = ThumbnailUtils.extractThumbnail(ThumbnailUtils.createVideoThumbnail(str, i4), i2, i3, 2);
        this.r.put(str, extractThumbnail);
        return extractThumbnail;
    }

    private int a(String str) {
        if ("安装包".equals(str)) {
            return R.drawable.rubbish_icon_apk;
        }
        if ("垃圾文件".equals(str)) {
            return R.drawable.rubbish_icon_file;
        }
        if ("视频".equals(str)) {
            return R.drawable.common_icon_video;
        }
        if ("音乐".equals(str)) {
            return R.drawable.common_icon_music;
        }
        if ("文档".equals(str)) {
            return R.drawable.common_icon_doc;
        }
        if ("压缩文件".equals(str)) {
            return R.drawable.common_icon_zip;
        }
        if ("其他文件".equals(str)) {
            return R.drawable.common_icon_film;
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public void a(TextView textView, SubRubbishInfo subRubbishInfo) {
        t tVar = new t(this, textView, subRubbishInfo);
        tVar.blockCaller = true;
        tVar.titleRes = this.k.getString(R.string.rubbish_clear_tips_dialog_title);
        tVar.lBtnTxtRes = this.k.getString(R.string.cancel);
        tVar.rBtnTxtRes = this.k.getString(R.string.app_admin_uninstall_dialog_tips_confirm);
        tVar.contentRes = this.k.getString(R.string.rubbish_clear_tips_dialog_content);
        DialogUtils.show2BtnDialog(tVar);
    }

    /* access modifiers changed from: private */
    public void b(TextView textView, SubRubbishInfo subRubbishInfo) {
        textView.setSelected(!textView.isSelected());
        subRubbishInfo.d = textView.isSelected();
        if (subRubbishInfo.d) {
            this.n.c += subRubbishInfo.c;
        } else {
            this.n.c -= subRubbishInfo.c;
            if (this.n.c < 0) {
                this.n.c = 0;
            }
        }
        this.h.setText(this.k.getString(R.string.rubbish_clear_selected_size, at.c(this.n.d), at.c(this.n.c)));
        if (this.m != null) {
            Message obtain = Message.obtain(this.m, 26);
            obtain.obj = this.n;
            obtain.sendToTarget();
        }
    }
}
