package com.tencent.nucleus.manager.spaceclean;

import java.util.Comparator;

/* compiled from: ProGuard */
class o implements Comparator<SubRubbishInfo> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ n f3038a;

    o(n nVar) {
        this.f3038a = nVar;
    }

    /* renamed from: a */
    public int compare(SubRubbishInfo subRubbishInfo, SubRubbishInfo subRubbishInfo2) {
        if (subRubbishInfo == null || subRubbishInfo2 == null) {
            return 0;
        }
        long j = subRubbishInfo2.c - subRubbishInfo.c;
        if (j > 0) {
            return 1;
        }
        if (j != 0) {
            return -1;
        }
        return 0;
    }
}
