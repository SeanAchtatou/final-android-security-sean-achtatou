package com.tencent.nucleus.manager.spaceclean;

import android.os.Handler;
import android.os.Message;
import com.tencent.assistant.utils.XLog;
import com.tencent.tmsecurelite.optimize.ISystemOptimize;

/* compiled from: ProGuard */
class h extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BigFileCleanActivity f3031a;

    h(BigFileCleanActivity bigFileCleanActivity) {
        this.f3031a = bigFileCleanActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity.a(com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, long, boolean, int):void
     arg types: [com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, long, int, int]
     candidates:
      com.tencent.assistant.activity.BaseActivity.a(android.widget.LinearLayout, int, int, int):void
      com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity.a(com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, long, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity.a(com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, boolean):void
     arg types: [com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, int]
     candidates:
      com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity.a(com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, int):int
      com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity.a(com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, long):long
      com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity.a(com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, com.tencent.nucleus.manager.spaceclean.n):long
      com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity.a(long, boolean):void
      com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity.a(android.content.pm.PackageManager, java.lang.String):boolean
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity.a(com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity.a(com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, long, boolean):void
     arg types: [com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, int, int]
     candidates:
      com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity.a(long, boolean, int):void
      com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity.a(com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, android.content.pm.PackageManager, java.lang.String):boolean
      com.tencent.assistant.activity.BaseActivity.a(com.tencent.assistantv2.st.page.STPageInfo, java.lang.String, java.lang.String):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, boolean, boolean):android.support.v4.app.z
      android.support.v4.app.FragmentActivity.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity.a(com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity.a(com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, long, boolean, int):void
     arg types: [com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, int, int, int]
     candidates:
      com.tencent.assistant.activity.BaseActivity.a(android.widget.LinearLayout, int, int, int):void
      com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity.a(com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, long, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity.b(com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, boolean):boolean
     arg types: [com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, int]
     candidates:
      com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity.b(com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, long):long
      com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity.b(com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity.a(com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, long, boolean):void
     arg types: [com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, long, int]
     candidates:
      com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity.a(long, boolean, int):void
      com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity.a(com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, android.content.pm.PackageManager, java.lang.String):boolean
      com.tencent.assistant.activity.BaseActivity.a(com.tencent.assistantv2.st.page.STPageInfo, java.lang.String, java.lang.String):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, boolean, boolean):android.support.v4.app.z
      android.support.v4.app.FragmentActivity.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity.a(com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity, long, boolean):void */
    public void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case ISystemOptimize.T_checkVersionAsync /*24*/:
                this.f3031a.a(((Long) message.obj).longValue(), true, 1);
                this.f3031a.b(true);
                this.f3031a.a(0);
                return;
            case ISystemOptimize.T_hasRootAsync /*25*/:
                long longValue = ((Long) message.obj).longValue();
                XLog.d("miles", "BigFileCleanActivity >> MSG_RUBBISH_SCAN_FINISHED reveived. sizeTotal = " + longValue);
                if (longValue > 0) {
                    this.f3031a.a(longValue, false, 1);
                    this.f3031a.b(false);
                    this.f3031a.a(0);
                    return;
                }
                this.f3031a.a(0L, false);
                return;
            case ISystemOptimize.T_askForRootAsync /*26*/:
                this.f3031a.a(this.f3031a.a((n) message.obj));
                return;
            case ISystemOptimize.T_getMemoryUsageAsync /*27*/:
                this.f3031a.a(0L, false, 2);
                this.f3031a.P.sendEmptyMessageDelayed(28, 1000);
                return;
            case ISystemOptimize.T_isMemoryReachWarningAsync /*28*/:
                XLog.d("miles", "BigFileCleanActivity >> MSG_RUBBISH_DELETE_ALL_FINISHED recived.");
                boolean unused = this.f3031a.I = true;
                if (this.f3031a.H && this.f3031a.I) {
                    this.f3031a.a(this.f3031a.F, true);
                }
                RubbishItemView.b = false;
                return;
            case ISystemOptimize.T_getSysRubbishAsync /*29*/:
                XLog.d("miles", "BigFileCleanActivity >> MSG_ACCELERATE_SCAN_FINISHED recived.");
                if (this.f3031a.H && this.f3031a.I) {
                    this.f3031a.a(this.f3031a.F, true);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
