package com.tencent.nucleus.manager.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: ProGuard */
public class BubbleAnimationView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    boolean f2840a = false;
    private TextView b = null;
    private TextView c = null;
    private Animation d;
    private List<Double> e = Collections.synchronizedList(new ArrayList());
    private int f = 0;
    private DecimalFormat g = new DecimalFormat("0.0");

    public BubbleAnimationView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public void a(Context context) {
        LayoutInflater.from(context).inflate((int) R.layout.bubble_animview_layout, this);
        this.b = (TextView) findViewById(R.id.bubble_txt);
        this.c = (TextView) findViewById(R.id.bubble_size_unit);
        this.d = AnimationUtils.loadAnimation(getContext(), R.anim.manager_bubble_animation);
        this.d.setFillEnabled(true);
        this.d.setFillBefore(false);
    }
}
