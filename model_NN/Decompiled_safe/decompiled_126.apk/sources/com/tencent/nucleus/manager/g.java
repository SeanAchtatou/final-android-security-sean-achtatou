package com.tencent.nucleus.manager;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.module.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class g extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f2923a;

    g(f fVar) {
        this.f2923a = fVar;
    }

    public void onTMAClick(View view) {
        this.f2923a.f2883a.v();
    }

    public STInfoV2 getStInfo() {
        return STInfoBuilder.buildSTInfo(this.f2923a.f2883a, this.f2923a.f2883a.H, "03_001", a.a(k.d(this.f2923a.f2883a.H)), a.a(k.d(this.f2923a.f2883a.H), this.f2923a.f2883a.H));
    }
}
