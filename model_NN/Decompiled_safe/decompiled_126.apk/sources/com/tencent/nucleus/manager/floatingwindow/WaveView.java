package com.tencent.nucleus.manager.floatingwindow;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.android.qqdownloader.b;

/* compiled from: ProGuard */
public class WaveView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private int f2893a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private Wave h;
    private Solid i;
    private final int j = -1;
    private final int k = -1;
    private final int l = 80;

    public WaveView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setOrientation(1);
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, b.u, R.attr.waveViewStyle, 0);
        this.f2893a = obtainStyledAttributes.getColor(0, -1);
        this.b = obtainStyledAttributes.getColor(1, -1);
        this.c = obtainStyledAttributes.getInt(2, 80);
        this.d = obtainStyledAttributes.getInt(4, 2);
        this.e = obtainStyledAttributes.getInt(3, 1);
        this.f = obtainStyledAttributes.getInt(5, 2);
        obtainStyledAttributes.recycle();
        this.h = new Wave(context, null);
        this.h.a(this.e, this.d, this.f);
        this.h.a(this.f2893a);
        this.h.b(this.b);
        this.h.c();
        this.i = new Solid(context, null);
        this.i.a(this.h.a());
        this.i.b(this.h.b());
        addView(this.h);
        addView(this.i);
        a(this.c);
    }

    public void a(int i2) {
        if (i2 > 100) {
            i2 = 100;
        }
        this.c = i2;
        a();
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            a();
        }
    }

    private void a() {
        this.g = (int) (((float) getHeight()) * (1.0f - (((float) this.c) / 100.0f)));
        ViewGroup.LayoutParams layoutParams = this.h.getLayoutParams();
        if (layoutParams != null) {
            ((LinearLayout.LayoutParams) layoutParams).topMargin = this.g;
        }
        this.h.setLayoutParams(layoutParams);
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f2894a = this.c;
        return savedState;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        a(savedState.f2894a);
    }

    /* compiled from: ProGuard */
    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new ae();

        /* renamed from: a  reason: collision with root package name */
        int f2894a;

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.f2894a = parcel.readInt();
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f2894a);
        }
    }
}
