package com.tencent.nucleus.socialcontact.comment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.RatingView;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.r;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.socialcontact.login.j;
import com.tencent.pangu.activity.AppDetailActivityV5;
import com.tencent.pangu.activity.v;
import com.tencent.pangu.c.e;
import com.tencent.pangu.component.appdetail.CommentDetailTabView;
import com.tencent.pangu.manager.ak;
import com.tencent.pangu.manager.at;
import com.tencent.pangu.model.ShareAppModel;

/* compiled from: ProGuard */
public class PopViewDialog extends Dialog {
    /* access modifiers changed from: private */
    public boolean A;
    /* access modifiers changed from: private */
    public boolean B;
    /* access modifiers changed from: private */
    public ShareAppModel C;
    /* access modifiers changed from: private */
    public e D;
    private long E = -1;
    /* access modifiers changed from: private */
    public int F = -1;
    /* access modifiers changed from: private */
    public int G = -1;
    /* access modifiers changed from: private */
    public String H;
    /* access modifiers changed from: private */
    public String I;
    private boolean J = true;
    /* access modifiers changed from: private */
    public boolean K;
    private CommentDetailTabView L;
    private View.OnClickListener M = new bn(this);
    private v N;

    /* renamed from: a  reason: collision with root package name */
    public boolean f3081a = false;
    /* access modifiers changed from: private */
    public Context b;
    /* access modifiers changed from: private */
    public RatingView c;
    /* access modifiers changed from: private */
    public EditText d;
    /* access modifiers changed from: private */
    public TextView e;
    /* access modifiers changed from: private */
    public TextView f;
    /* access modifiers changed from: private */
    public TextView g;
    private TextView h;
    private TextView i;
    /* access modifiers changed from: private */
    public ScrollView j;
    /* access modifiers changed from: private */
    public bo k;
    /* access modifiers changed from: private */
    public CommentDetail l;
    /* access modifiers changed from: private */
    public g m = null;
    private bc n;
    private Window o = null;
    /* access modifiers changed from: private */
    public long p = 0;
    /* access modifiers changed from: private */
    public long q = 0;
    /* access modifiers changed from: private */
    public String r;
    /* access modifiers changed from: private */
    public int s = 0;
    /* access modifiers changed from: private */
    public String t;
    /* access modifiers changed from: private */
    public ImageView u;
    private TextView v;
    /* access modifiers changed from: private */
    public ImageView w;
    private ImageView x;
    private ImageView y;
    /* access modifiers changed from: private */
    public j z;

    public PopViewDialog(Context context, boolean z2, DialogInterface.OnCancelListener onCancelListener) {
        super(context, z2, onCancelListener);
        this.b = context;
    }

    public PopViewDialog(Context context, int i2, String str, CommentDetailTabView commentDetailTabView) {
        super(context, i2);
        this.b = context;
        this.l = new CommentDetail();
        this.m = commentDetailTabView.g();
        this.n = commentDetailTabView.h();
        this.I = str;
        this.L = commentDetailTabView;
        setCanceledOnTouchOutside(false);
    }

    public PopViewDialog(Context context) {
        super(context);
        this.b = context;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.comment_detail_popview);
        this.z = j.a();
        a(0, 0, r.b() - 50, r.c());
        this.e = (TextView) findViewById(R.id.over_text_tips);
        this.c = (RatingView) findViewById(R.id.comment_score);
        this.f = (TextView) findViewById(R.id.starCount);
        this.g = (TextView) findViewById(R.id.errorTips);
        this.c.setEnable(true);
        this.c.setOnRatingBarChangeListener(new bf(this));
        if (this.F > 0) {
            this.c.setRating((float) this.F);
        }
        this.d = (EditText) findViewById(R.id.input_more_advice);
        this.d.setLongClickable(false);
        if (r.d() >= 11) {
            this.d.setCustomSelectionActionModeCallback(new bg(this));
            this.d.setTextIsSelectable(false);
        }
        this.d.setOnFocusChangeListener(new bh(this));
        this.d.addTextChangedListener(new bi(this));
        if (!TextUtils.isEmpty(this.H)) {
            this.d.setText(this.H);
        }
        this.h = (TextView) findViewById(R.id.cancel);
        this.i = (TextView) findViewById(R.id.submit);
        this.h.setOnClickListener(this.M);
        this.i.setOnClickListener(this.M);
        this.j = (ScrollView) findViewById(R.id.scroller_view);
        b();
        this.v = (TextView) findViewById(R.id.share_txt);
        this.u = (ImageView) findViewById(R.id.share_btn);
        this.y = (ImageView) findViewById(R.id.share_img);
        this.w = (ImageView) findViewById(R.id.share_btn2);
        this.x = (ImageView) findViewById(R.id.share_img2);
        this.u.setOnClickListener(new bj(this));
        this.w.setOnClickListener(new bk(this));
    }

    public void a() {
        if (!this.z.j()) {
            this.y.setVisibility(0);
            this.y.setImageResource(R.drawable.common_pinglun_icon_kongjian);
            this.u.setVisibility(0);
            if (this.z.v()) {
                this.x.setVisibility(0);
                this.x.setImageResource(R.drawable.common_pinglun_icon_pengyouquan);
                this.w.setVisibility(0);
            } else {
                this.x.setVisibility(8);
                this.w.setVisibility(8);
            }
        } else if (this.z.k()) {
            this.y.setVisibility(0);
            this.y.setImageResource(R.drawable.common_pinglun_icon_kongjian);
            this.u.setVisibility(0);
            this.x.setVisibility(8);
            this.w.setVisibility(8);
        } else if (this.z.v()) {
            this.x.setVisibility(0);
            this.x.setImageResource(R.drawable.common_pinglun_icon_pengyouquan);
            this.w.setVisibility(0);
            this.y.setVisibility(8);
            this.u.setVisibility(8);
        }
        this.u.setSelected(false);
        this.w.setSelected(false);
        this.g.setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        a();
        this.A = false;
        this.B = false;
    }

    public void b() {
        if (this.j != null) {
            this.j.getViewTreeObserver().addOnGlobalLayoutListener(new bl(this));
        }
    }

    public void a(int i2, int i3, int i4, int i5) {
        this.o = getWindow();
        WindowManager.LayoutParams attributes = this.o.getAttributes();
        attributes.x = i2;
        attributes.y = i3;
        attributes.width = i4;
        this.o.setAttributes(attributes);
    }

    public EditText c() {
        return this.d;
    }

    public void a(boolean z2, long j2, long j3, long j4, String str, int i2, int i3, bo boVar, String str2, boolean z3) {
        this.p = j2;
        this.q = j3;
        this.K = z2;
        this.r = str2;
        if (boVar != null) {
            this.k = boVar;
        }
        int i4 = 0;
        if (!ak.a().c(this.I) || !ak.a().d(this.I)) {
            LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(this.I);
            if (localApkInfo != null) {
                i4 = localApkInfo.mVersionCode;
            }
        } else {
            at a2 = ak.a().a(this.I);
            if (a2 != null) {
                i4 = a2.c;
            }
        }
        if (i2 <= 0 || i4 <= 0 || i4 != i3) {
            this.G = -1;
            if (this.d != null) {
                this.d.setText(Constants.STR_EMPTY);
            }
            this.H = Constants.STR_EMPTY;
            if (this.c != null) {
                this.c.setRating(0.0f);
            }
            this.E = -1;
            this.F = -1;
        } else {
            this.G = i3;
            if (this.d != null) {
                this.d.setText(str);
            }
            this.H = str;
            if (this.c != null) {
                this.c.setRating((float) i2);
            }
            this.E = j4;
            this.F = i2;
        }
        b();
        if (this.z == null) {
            this.z = j.a();
        }
    }

    public void d() {
        if (this.L.i()) {
            if (this.N != null) {
                this.N.a(true);
            }
            this.L.a((CommentTagInfo) null);
            this.f3081a = true;
        } else if (this.z.j()) {
            show();
        } else {
            j();
            l.a(new STInfoV2(STConst.ST_PAGE_COMMENT_DIALOG_NOT_LOGIN, "03_002", STConst.ST_PAGE_APP_DETAIL_COMMENT, STConst.ST_DEFAULT_SLOT, 200));
        }
    }

    public void a(int i2, String str) {
        this.s = i2;
        this.t = str;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            f();
        }
        return super.onTouchEvent(motionEvent);
    }

    /* access modifiers changed from: private */
    public void f() {
        View currentFocus;
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService("input_method");
        if (inputMethodManager != null && inputMethodManager.isActive() && (currentFocus = getCurrentFocus()) != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }

    public void dismiss() {
        this.f.setText((int) R.string.comment_pop_star_text);
        this.f.setTextColor(this.b.getResources().getColor(R.color.appadmin_risk_tips));
        this.d.clearFocus();
        this.g.setVisibility(8);
        super.dismiss();
    }

    public void a(v vVar) {
        this.N = vVar;
    }

    private int g() {
        if (this.z.j()) {
            return STConst.ST_PAGE_COMMENT_DIALOG_LOGIN;
        }
        return STConst.ST_PAGE_COMMENT_DIALOG_NOT_LOGIN;
    }

    /* access modifiers changed from: private */
    public String h() {
        if (this.z.j()) {
            return "04";
        }
        return "03";
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        l.a(i());
    }

    /* access modifiers changed from: private */
    public STInfoV2 i() {
        int i2 = 2000;
        if (!(this.b instanceof AppDetailActivityV5)) {
            return new STInfoV2(g(), STConst.ST_DEFAULT_SLOT, 2000, STConst.ST_DEFAULT_SLOT, 100);
        }
        STInfoV2 t2 = ((AppDetailActivityV5) this.b).t();
        if (t2 != null) {
            i2 = t2.scene;
        }
        return new STInfoV2(g(), STConst.ST_DEFAULT_SLOT, i2, a.b(t2 != null ? t2.slotId : STConst.ST_DEFAULT_SLOT, STConst.ST_STATUS_DEFAULT), 100);
    }

    public void onDetachedFromWindow() {
        this.J = true;
        super.onDetachedFromWindow();
    }

    public void a(ShareAppModel shareAppModel) {
        if (shareAppModel != null) {
            this.C = shareAppModel;
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConst.KEY_LOGIN_TYPE, 2);
        bundle.putInt(AppConst.KEY_FROM_TYPE, 19);
        j.a().a(AppConst.IdentityType.MOBILEQ, bundle);
    }

    public void a(e eVar) {
        this.D = eVar;
    }

    public void a(String str) {
        this.I = str;
    }

    public void e() {
        if (this.N != null) {
            this.N.a(true);
        }
        this.L.a((CommentTagInfo) null);
        this.f3081a = true;
    }
}
