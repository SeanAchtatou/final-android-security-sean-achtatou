package com.tencent.nucleus.socialcontact.comment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.RatingView;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.component.txscrollview.TXTabBarLayout;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.bo;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.socialcontact.login.j;

/* compiled from: ProGuard */
public class CommentReplyListActivity extends BaseActivity implements ITXRefreshListViewListener, UIEventListener {
    public static String n = "com.tencent.android.qqdownloader.key.COMMENT_DETAIL";
    public static String u = "com.tencent.android.qqdownloader.key.COMMENT_APPID";
    public static String v = "com.tencent.android.qqdownloader.key.COMMENT_APKID";
    public static String w = "com.tencent.android.qqdownloader.key.COMMENT_PKGNAME";
    public static String x = "com.tencent.android.qqdownloader.key.APPDETAIL_VERSIONCODE";
    public static String y = "com.tencent.android.qqdownloader.key.COMMENTLIST_COMMENT_DETAIL";
    public j A = new aa(this);
    /* access modifiers changed from: private */
    public LoadingView B;
    /* access modifiers changed from: private */
    public NormalErrorRecommendPage C;
    private SecondNavigationTitleViewV5 D;
    private ar E;
    /* access modifiers changed from: private */
    public TXCommentGetMoreListView F;
    /* access modifiers changed from: private */
    public az G;
    /* access modifiers changed from: private */
    public CommentReplyListAdapter H = null;
    /* access modifiers changed from: private */
    public d I;
    /* access modifiers changed from: private */
    public v J;
    /* access modifiers changed from: private */
    public CommentDetail K;
    /* access modifiers changed from: private */
    public long L;
    /* access modifiers changed from: private */
    public long M;
    private int N;
    /* access modifiers changed from: private */
    public String O;
    /* access modifiers changed from: private */
    public int P;
    /* access modifiers changed from: private */
    public String[] Q = {"+1", "好", "顶", "赞", "朕觉OK", "不明觉厉", "吊炸天"};
    /* access modifiers changed from: private */
    public TextView R = null;
    /* access modifiers changed from: private */
    public CommentReplyListFooterView S;
    /* access modifiers changed from: private */
    public EditText T;
    /* access modifiers changed from: private */
    public j U;
    /* access modifiers changed from: private */
    public int V = 0;
    /* access modifiers changed from: private */
    public View W;
    /* access modifiers changed from: private */
    public String X = "01_";
    /* access modifiers changed from: private */
    public String Y = "02_";
    /* access modifiers changed from: private */
    public String Z = "03_";
    /* access modifiers changed from: private */
    public String aa = "04_";
    private View.OnClickListener ab = new ab(this);
    /* access modifiers changed from: private */
    public long ac = 0;
    /* access modifiers changed from: private */
    public boolean ad = false;
    private y ae = new ac(this);
    private View.OnClickListener af = new ag(this);
    /* access modifiers changed from: private */
    public Runnable ag = new ah(this);
    aq z = new af(this);

    static /* synthetic */ int r(CommentReplyListActivity commentReplyListActivity) {
        int i = commentReplyListActivity.P;
        commentReplyListActivity.P = i + 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().setSoftInputMode(3);
        setContentView((int) R.layout.comment_replylist_activity);
        v();
        w();
        this.U = j.a();
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, this);
    }

    private void v() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.K = (CommentDetail) extras.getSerializable(n);
            this.L = extras.getLong(u);
            this.M = extras.getLong(v);
            this.O = extras.getString(w);
            this.N = extras.getInt(x);
        }
        if (this.K == null || this.K.h <= 0) {
            finish();
        }
    }

    private void w() {
        this.B = (LoadingView) findViewById(R.id.loading_view);
        this.B.setVisibility(0);
        this.C = (NormalErrorRecommendPage) findViewById(R.id.error_page);
        this.C.setButtonClickListener(this.af);
        this.D = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.D.b(getResources().getString(R.string.comment_replypage_title));
        this.D.a(this);
        this.D.d();
        this.D.j();
        this.E = new ar();
        this.G = new az();
        this.I = new d();
        this.J = new v();
        this.F = (TXCommentGetMoreListView) findViewById(R.id.reply_listview);
        this.F.setVisibility(8);
        this.H = new CommentReplyListAdapter(this, this.F.getListView(), this.K.h, null);
        this.H.a(this.z);
        this.W = x();
        this.S = (CommentReplyListFooterView) findViewById(R.id.reply_button_layout);
        this.S.a(this.ab);
        this.T = this.S.b();
        this.F.addHeaderView(this.W);
        this.F.setAdapter(this.H);
        this.F.setDivider(null);
        this.F.setSelector(getResources().getDrawable(R.drawable.transparent_selector));
        this.F.setCacheColorHint(17170445);
        this.F.setRefreshListViewListener(this);
        this.F.getListView().setVerticalFadingEdgeEnabled(false);
        this.F.setVerticalScrollBarEnabled(false);
        this.F.getListView().setOnTouchListener(new z(this));
        this.F.addClickLoadMore();
        if (Build.VERSION.SDK_INT >= 9) {
            this.F.getListView().setOverScrollMode(2);
        }
        t();
    }

    public void t() {
        if (this.H.getCount() > 0) {
            u();
            this.H.notifyDataSetChanged();
            return;
        }
        this.E.a(this.L, this.M, this.K.h);
    }

    public void u() {
        this.F.setVisibility(0);
        this.B.setVisibility(8);
        this.C.setVisibility(8);
    }

    public int f() {
        return STConst.ST_PAGE_COMMENT_REPLY;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.E.unregister(this.ae);
        this.I.unregister(this.A);
        this.G.unregister(this.ae);
        this.J.unregister(this.ae);
        this.D.m();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.E.register(this.ae);
        this.I.register(this.A);
        this.G.register(this.ae);
        this.J.register(this.ae);
        this.H.notifyDataSetChanged();
        this.D.l();
        this.S.b(this.U.j());
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_FAIL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, this);
        this.E = null;
        this.G = null;
        this.I = null;
        this.H = null;
        this.F = null;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            finish();
        }
        return super.onKeyDown(i, keyEvent);
    }

    private View x() {
        long j;
        aj ajVar = new aj(this, null);
        View inflate = LayoutInflater.from(this).inflate((int) R.layout.comment_reply_header, (ViewGroup) null);
        TXAppIconView tXAppIconView = (TXAppIconView) inflate.findViewById(R.id.picture);
        tXAppIconView.updateImageView(this.K.i, R.drawable.common_icon_useravarta_default, TXImageView.TXImageViewType.ROUND_IMAGE);
        ajVar.f3092a = tXAppIconView;
        TextView textView = (TextView) inflate.findViewById(R.id.nick_name);
        ajVar.b = textView;
        TextView textView2 = (TextView) inflate.findViewById(R.id.version);
        ajVar.d = textView2;
        TextView textView3 = (TextView) inflate.findViewById(R.id.myorfriends);
        if (this.K.l) {
            textView.setText((int) R.string.comment_nick_my_comment);
            textView.setTextColor(getResources().getColor(R.color.comment_nickname_highlight_text));
            textView3.setVisibility(8);
        } else if (this.K.o == 3) {
            textView.setText((int) R.string.comment_nick_kaifazhe);
            textView.setTextColor(getResources().getColor(R.color.comment_nickname_kaifazhe_text));
            textView3.setVisibility(8);
            ((ImageView) inflate.findViewById(R.id.kaifaimg)).setVisibility(0);
        } else if (TextUtils.isEmpty(this.K.c)) {
            textView.setText((int) R.string.comment_default_nick_name);
            textView3.setVisibility(8);
        } else if (!this.K.c.startsWith("(好友)")) {
            textView.setText(this.K.c);
            textView3.setVisibility(8);
        } else {
            String substring = this.K.c.substring(4);
            if (substring.length() > 8) {
                substring = substring.substring(0, 8) + "...";
            }
            if (TextUtils.isEmpty(substring)) {
                textView.setText((int) R.string.comment_default_nick_name);
                textView3.setVisibility(8);
            } else {
                textView.setText(substring);
                textView.setTextColor(Color.parseColor("#ff7a0c"));
                textView3.setVisibility(0);
            }
        }
        ajVar.c = textView3;
        if (this.K.d > 0) {
            textView2.setVisibility(0);
            if (this.N <= this.K.d) {
                textView2.setText(getString(R.string.comment_detail_current_version));
            } else {
                textView2.setText(this.K.u);
            }
        }
        ajVar.d = textView2;
        RatingView ratingView = (RatingView) inflate.findViewById(R.id.comment_score);
        ratingView.setRating((float) this.K.b);
        ajVar.e = ratingView;
        TextView textView4 = (TextView) inflate.findViewById(R.id.time);
        if (this.K.m <= 0) {
            j = this.K.f1202a;
        } else {
            j = this.K.m;
        }
        textView4.setText(bo.g(j * 1000));
        ajVar.g = textView4;
        TextView textView5 = (TextView) inflate.findViewById(R.id.content);
        textView5.setText(this.K.g);
        ajVar.f = textView5;
        this.R = (TextView) inflate.findViewById(R.id.likecount);
        this.R.setText(bm.a(this.K.b()) + Constants.STR_EMPTY);
        this.R.setTag(R.id.comment_praise_count, Long.valueOf(this.K.b()));
        TextView textView6 = (TextView) inflate.findViewById(R.id.likeanimation);
        Drawable drawable = getResources().getDrawable(R.drawable.pinglun_icon_zan);
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
        Drawable drawable2 = getResources().getDrawable(R.drawable.pinglun_icon_yizan);
        drawable2.setBounds(0, 0, drawable2.getMinimumWidth(), drawable2.getMinimumHeight());
        this.R.setCompoundDrawablePadding(by.a(this, 6.0f));
        if (this.K.c() == 1) {
            this.R.setCompoundDrawables(drawable2, null, null, null);
            this.R.setTextColor(Color.parseColor("#b68a46"));
            this.R.setTag(true);
        } else {
            this.R.setCompoundDrawables(drawable, null, null, null);
            this.R.setTextColor(Color.parseColor("#a4a4a4"));
            this.R.setTag(false);
        }
        this.R.setOnClickListener(new ad(this, drawable, drawable2, textView6));
        ajVar.h = this.R;
        TextView textView7 = (TextView) inflate.findViewById(R.id.replycount);
        textView7.setText(bm.a(this.K.v) + Constants.STR_EMPTY);
        textView7.setTextColor(Color.parseColor("#a4a4a4"));
        textView7.setOnClickListener(new ae(this));
        ajVar.i = textView7;
        l.a(new STInfoV2(STConst.ST_PAGE_COMMENT_REPLY, this.Z + "001", 0, STConst.ST_DEFAULT_SLOT, 100));
        inflate.setTag(ajVar);
        return inflate;
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
                this.S.b(true);
                y();
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_FAIL:
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void b(int i) {
        this.B.setVisibility(8);
        if (i != 100 || this.W == null) {
            this.F.setVisibility(8);
        } else {
            this.F.setVisibility(0);
        }
        this.C.setVisibility(0);
        this.C.setErrorType(i);
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        this.E.a(10);
    }

    /* access modifiers changed from: private */
    public void y() {
        ah.a().postDelayed(new ai(this), 300);
    }

    /* access modifiers changed from: private */
    public void z() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
        if (inputMethodManager != null && inputMethodManager.isActive() && this.T != null) {
            inputMethodManager.hideSoftInputFromWindow(this.T.getWindowToken(), 0);
        }
    }

    /* access modifiers changed from: private */
    public void A() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
        if (inputMethodManager != null && this.T != null) {
            inputMethodManager.showSoftInput(this.T, 2);
        }
    }

    /* access modifiers changed from: private */
    public void B() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConst.KEY_LOGIN_TYPE, 2);
        j.a().a(AppConst.IdentityType.MOBILEQ, bundle);
    }

    public void finish() {
        if (this.K != null) {
            Intent intent = new Intent();
            intent.putExtra(y, this.K);
            setResult(TXTabBarLayout.TABITEM_TIPS_TEXT_ID, intent);
        }
        z();
        super.finish();
    }
}
