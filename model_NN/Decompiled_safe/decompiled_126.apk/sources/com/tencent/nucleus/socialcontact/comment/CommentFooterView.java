package com.tencent.nucleus.socialcontact.comment;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
public class CommentFooterView extends TXLoadingLayoutBase {
    private TextView c;
    private LinearLayout d;
    private CharSequence e;
    private CharSequence f;
    private CharSequence g;
    private CharSequence h;
    private ProgressBar i;
    private int j = 0;

    public CommentFooterView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public CommentFooterView(Context context, TXScrollViewBase.ScrollDirection scrollDirection, TXScrollViewBase.ScrollMode scrollMode) {
        super(context, scrollDirection, scrollMode);
        a(context);
    }

    private void a(Context context) {
        this.e = context.getString(R.string.refresh_list_loading_refreshing_from_end);
        this.h = context.getString(R.string.is_finished);
        this.f = context.getString(R.string.comment_detail_more_text);
        this.g = context.getString(R.string.is_next_page_error);
        View inflate = LayoutInflater.from(context).inflate((int) R.layout.comment_detail_footer_layout, this);
        this.c = (TextView) inflate.findViewById(R.id.comment_detail_footer_text);
        this.d = (LinearLayout) inflate.findViewById(R.id.layout_id);
        this.i = (ProgressBar) inflate.findViewById(R.id.foot_loading_bar);
        reset();
    }

    public void reset() {
        this.c.setText(this.e);
    }

    public void pullToRefresh() {
        this.c.setText(this.e);
    }

    public void releaseToRefresh() {
        this.c.setText(this.e);
    }

    public void refreshing() {
        this.c.setText(this.e);
    }

    public void loadFinish(String str) {
        this.c.setText(this.h);
    }

    public void onPull(int i2) {
    }

    public void hideAllSubViews() {
    }

    public void showAllSubViews() {
    }

    public void setWidth(int i2) {
        getLayoutParams().width = i2;
        requestLayout();
    }

    public void setHeight(int i2) {
        getLayoutParams().height = i2;
        requestLayout();
    }

    public int getContentSize() {
        return this.d.getHeight();
    }

    public int getTriggerSize() {
        return getResources().getDimensionPixelSize(R.dimen.tx_pull_to_refresh_trigger_size);
    }

    public void a(int i2) {
        this.j = i2;
        switch (i2) {
            case 1:
                setClickable(true);
                setEnabled(true);
                this.i.setVisibility(8);
                this.c.setText(this.f);
                this.c.setVisibility(8);
                break;
            case 2:
                setClickable(false);
                setEnabled(false);
                this.i.setVisibility(0);
                this.c.setText(this.e);
                this.c.setVisibility(0);
                break;
            case 3:
                setClickable(false);
                setEnabled(false);
                this.i.setVisibility(8);
                this.c.setVisibility(8);
                break;
            case 4:
                setClickable(true);
                setEnabled(true);
                this.i.setVisibility(8);
                this.c.setText(this.g);
                this.c.setVisibility(0);
                break;
        }
        requestLayout();
    }

    public int a() {
        return this.j;
    }

    public void refreshSuc() {
    }

    public void refreshFail(String str) {
    }

    public void loadSuc() {
    }

    public void loadFail() {
    }
}
