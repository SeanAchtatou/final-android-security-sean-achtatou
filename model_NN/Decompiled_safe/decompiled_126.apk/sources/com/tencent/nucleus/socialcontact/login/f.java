package com.tencent.nucleus.socialcontact.login;

import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.login.model.MoblieQIdentityInfo;
import com.tencent.assistant.manager.webview.js.l;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.beacon.event.a;
import java.util.HashMap;

/* compiled from: ProGuard */
public abstract class f {

    /* renamed from: a  reason: collision with root package name */
    protected Bundle f3160a;
    private String b = "LoginBaseEngine";

    public abstract void d();

    public abstract AppConst.LoginEgnineType e();

    public abstract void g();

    /* access modifiers changed from: protected */
    public void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            j.a().a(new q(str, false));
        }
    }

    public void a(Bundle bundle) {
        this.f3160a = bundle;
        if (this.f3160a != null) {
            this.f3160a.putInt(AppConst.KEY_LOGIN_ANIM, 0);
            if (AstApp.m() != null) {
                this.f3160a.putInt("login_scene", AstApp.m().f());
                this.f3160a.putInt("login_source_scene", AstApp.m().m());
                return;
            }
            this.f3160a.putInt("login_scene", 0);
            this.f3160a.putInt("login_source_scene", 0);
        }
    }

    public Bundle a() {
        return this.f3160a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.socialcontact.login.f.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.tencent.nucleus.socialcontact.login.f.a(com.tencent.assistant.login.model.MoblieQIdentityInfo, boolean):void
      com.tencent.nucleus.socialcontact.login.f.a(boolean, java.lang.String):void */
    public void a(String str, long j, byte[] bArr, byte[] bArr2, byte[] bArr3, String str2, boolean z) {
        XLog.d(this.b, "onLoginSuccess");
        if (!(bArr == null || bArr2 == null || bArr3 == null)) {
            j.a().a((e) new MoblieQIdentityInfo(str, bArr, bArr3, bArr2, str2, j));
            a(z);
        }
        if (!z) {
            b(STConstAction.ACTION_HIT_LOGIN_SUCCESS_QQ);
            a(true, "success");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.socialcontact.login.f.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.tencent.nucleus.socialcontact.login.f.a(com.tencent.assistant.login.model.MoblieQIdentityInfo, boolean):void
      com.tencent.nucleus.socialcontact.login.f.a(boolean, java.lang.String):void */
    public void a(MoblieQIdentityInfo moblieQIdentityInfo, boolean z) {
        XLog.d(this.b, "onLoginSuccess");
        if (moblieQIdentityInfo != null) {
            if (!z) {
                j.a().a((e) moblieQIdentityInfo);
            } else {
                j.a().a(moblieQIdentityInfo);
            }
            a(z);
        }
        if (!z) {
            b(STConstAction.ACTION_HIT_LOGIN_SUCCESS_QQ);
            a(true, "success");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.socialcontact.login.f.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.tencent.nucleus.socialcontact.login.f.a(com.tencent.assistant.login.model.MoblieQIdentityInfo, boolean):void
      com.tencent.nucleus.socialcontact.login.f.a(boolean, java.lang.String):void */
    public void b() {
        XLog.d(this.b, "onLoginFail");
        j.a().f();
        a((int) EventDispatcherEnum.UI_EVENT_LOGIN_FAIL);
        a(false, "fail");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.socialcontact.login.f.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.tencent.nucleus.socialcontact.login.f.a(com.tencent.assistant.login.model.MoblieQIdentityInfo, boolean):void
      com.tencent.nucleus.socialcontact.login.f.a(boolean, java.lang.String):void */
    public void c() {
        XLog.d(this.b, "onLoginCancel");
        j.a().f();
        a((int) EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL);
        a(false, "cancel");
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        Message obtainMessage = AstApp.i().j().obtainMessage(i);
        obtainMessage.arg1 = e().ordinal();
        if (this.f3160a != null) {
            obtainMessage.obj = this.f3160a;
        }
        AstApp.i().j().sendMessage(obtainMessage);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        Message obtainMessage = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS);
        obtainMessage.arg1 = e().ordinal();
        obtainMessage.arg2 = z ? 1 : 2;
        if (this.f3160a != null) {
            obtainMessage.obj = this.f3160a;
        }
        AstApp.i().j().sendMessage(obtainMessage);
        l.a(AstApp.i(), "http://www.qq.com", "ALL");
    }

    public void f() {
    }

    private void b(int i) {
        int i2;
        int i3 = 2000;
        if (this.f3160a != null) {
            int i4 = this.f3160a.getInt("login_scene");
            if (i4 == 0) {
                i4 = 2000;
            }
            int i5 = this.f3160a.getInt("login_source_scene");
            if (i5 != 0) {
                i3 = i5;
            }
            i2 = i3;
            i3 = i4;
        } else {
            i2 = 2000;
        }
        STInfoV2 sTInfoV2 = new STInfoV2(i3, STConst.ST_DEFAULT_SLOT, i2, STConst.ST_DEFAULT_SLOT, i);
        if (this.f3160a != null && this.f3160a.containsKey(AppConst.KEY_FROM_TYPE)) {
            sTInfoV2.extraData = String.valueOf(this.f3160a.getInt(AppConst.KEY_FROM_TYPE));
        }
        com.tencent.assistantv2.st.l.a(sTInfoV2);
    }

    private void a(boolean z, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", str);
        hashMap.put("B2", Global.getPhoneGuidAndGen());
        hashMap.put("B3", Global.getQUAForBeacon());
        hashMap.put("B4", t.g());
        if (this instanceof h) {
            a.a("Login_QQ", z, -1, -1, hashMap, true);
            XLog.d("beacon", "beacon report >> event: Login_QQ, params : " + hashMap.toString());
        } else if (this instanceof s) {
            a.a("Login_WX", z, -1, -1, hashMap, true);
            XLog.d("beacon", "beacon report >> event: Login_WX, params : " + hashMap.toString());
        }
    }
}
