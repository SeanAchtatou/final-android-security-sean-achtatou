package com.tencent.nucleus.socialcontact.tagpage;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.pictureprocessor.ShowPictureActivity;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.thumbnailCache.o;
import com.tencent.assistant.thumbnailCache.p;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class TagPageThumbnailView extends LinearLayout implements View.OnClickListener, TXImageView.ITXImageViewListener, p {

    /* renamed from: a  reason: collision with root package name */
    private int f3175a = 2;
    private Context b = null;
    private LayoutInflater c = null;
    private TXImageView d = null;
    private TXImageView e = null;
    private ArrayList<String> f = new ArrayList<>();
    private ArrayList<String> g = new ArrayList<>();
    private ArrayList<String> h = new ArrayList<>();
    private boolean i = true;
    private int j = 0;

    public TagPageThumbnailView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = context;
        a();
    }

    public TagPageThumbnailView(Context context) {
        super(context);
        this.b = context;
        a();
    }

    private void a() {
        this.c = (LayoutInflater) this.b.getSystemService("layout_inflater");
        View inflate = this.c.inflate((int) R.layout.tag_page_thumbnail_view, this);
        this.d = (TXImageView) inflate.findViewById(R.id.tag_page_thumbnail_1);
        this.e = (TXImageView) inflate.findViewById(R.id.tag_page_thumbnail_2);
        this.d.setListener(this);
        this.e.setListener(this);
        this.d.setOnClickListener(this);
        this.e.setOnClickListener(this);
        if (by.b() >= 720) {
            this.i = true;
        } else {
            this.i = false;
        }
    }

    public void a(SimpleAppModel simpleAppModel, int i2) {
        if (simpleAppModel != null) {
            this.j = i2;
            this.f.clear();
            this.f.addAll(simpleAppModel.ao);
            this.g.clear();
            this.g.addAll(simpleAppModel.an);
            this.h.clear();
            this.h.addAll(simpleAppModel.ap);
            b();
        }
    }

    private void b() {
        if (this.i) {
            if (this.g.size() >= this.f3175a) {
                this.d.updateImageView(this.g.get(0), R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
                this.e.updateImageView(this.g.get(1), R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
            }
        } else if (this.f.size() >= this.f3175a) {
            this.d.updateImageView(this.f.get(0), R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
            this.e.updateImageView(this.f.get(1), R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
        }
    }

    public void onTXImageViewLoadImageFinish(TXImageView tXImageView, Bitmap bitmap) {
        XLog.i("TagPageActivity", "### onTXImageViewLoadImageFinish ### " + tXImageView.getId());
    }

    public void thumbnailRequestCompleted(o oVar) {
        XLog.i("TagPageActivity", "*** thumbnailRequestCompleted *** ");
    }

    public void thumbnailRequestFailed(o oVar) {
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.b, ShowPictureActivity.class);
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        int[] iArr2 = {iArr[0], iArr[1], view.getWidth(), view.getHeight()};
        switch (view.getId()) {
            case R.id.tag_page_thumbnail_1 /*2131166595*/:
                intent.putStringArrayListExtra("picUrls", this.h);
                if (this.i) {
                    intent.putStringArrayListExtra("thumbnails", this.g);
                } else {
                    intent.putStringArrayListExtra("thumbnails", this.f);
                }
                intent.putExtra("startPos", 0);
                intent.putExtra("imagePos", iArr2);
                this.b.startActivity(intent);
                a(a(this.j) + "_08", Constants.STR_EMPTY, 200);
                return;
            case R.id.tag_page_thumbnail_2 /*2131166596*/:
                intent.putStringArrayListExtra("picUrls", this.h);
                if (this.i) {
                    intent.putStringArrayListExtra("thumbnails", this.g);
                } else {
                    intent.putStringArrayListExtra("thumbnails", this.f);
                }
                intent.putExtra("startPos", 1);
                intent.putExtra("imagePos", iArr2);
                this.b.startActivity(intent);
                a(a(this.j) + "_08", Constants.STR_EMPTY, 200);
                return;
            default:
                return;
        }
    }

    public void a(String str, String str2, int i2) {
        XLog.i("TagPageThumbnailView", "[logReport] ---> actionId = " + i2 + ", slotId = " + str + ", extraData = " + str2);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b, i2);
        buildSTInfo.slotId = str;
        buildSTInfo.extraData = str2;
        l.a(buildSTInfo);
    }

    private String a(int i2) {
        return "07_" + bm.a(i2 + 1);
    }
}
