package com.tencent.nucleus.socialcontact.comment;

import android.view.MotionEvent;
import android.view.View;

/* compiled from: ProGuard */
class z implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentReplyListActivity f3141a;

    z(CommentReplyListActivity commentReplyListActivity) {
        this.f3141a = commentReplyListActivity;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() != 0) {
            return false;
        }
        this.f3141a.z();
        if (this.f3141a.T == null) {
            return false;
        }
        this.f3141a.T.clearFocus();
        return false;
    }
}
