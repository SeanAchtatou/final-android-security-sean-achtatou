package com.tencent.nucleus.socialcontact.tagpage;

import android.media.MediaPlayer;
import android.text.TextUtils;
import android.view.SurfaceHolder;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.aq;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class al implements SurfaceHolder.Callback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TagPageCardAdapter f3186a;
    private int b = 0;

    public al(TagPageCardAdapter tagPageCardAdapter, int i) {
        this.f3186a = tagPageCardAdapter;
        this.b = i;
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        SimpleAppModel simpleAppModel;
        TPVideoDownInfo tPVideoDownInfo;
        XLog.i("TagPageCradAdapter", "*** surfaceCreated ***");
        SurfaceHolder unused = this.f3186a.w = surfaceHolder;
        if (this.f3186a.l == null) {
            MediaPlayer unused2 = this.f3186a.l = new MediaPlayer();
        }
        if (this.b >= 0 && (simpleAppModel = (SimpleAppModel) this.f3186a.getItem(this.b)) != null) {
            String unused3 = this.f3186a.y = Constants.STR_EMPTY;
            if (!TextUtils.isEmpty(simpleAppModel.aD)) {
                String unused4 = this.f3186a.y = aq.b(simpleAppModel.aD);
                if (!TextUtils.isEmpty(this.f3186a.y)) {
                    TPVideoDownInfo b2 = l.c().b(this.f3186a.y);
                    if (b2 == null) {
                        TPVideoDownInfo tPVideoDownInfo2 = new TPVideoDownInfo();
                        tPVideoDownInfo2.b = simpleAppModel.aD;
                        tPVideoDownInfo2.f3171a = this.f3186a.y;
                        tPVideoDownInfo = tPVideoDownInfo2;
                    } else {
                        tPVideoDownInfo = b2;
                    }
                    tPVideoDownInfo.j = this.b;
                    XLog.i("TagPageCradAdapter", "[surfaceCreated] ---> startDownload video : " + tPVideoDownInfo);
                    l.c().a(tPVideoDownInfo);
                }
            }
        }
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        XLog.i("TagPageCradAdapter", "*** surfaceChanged ***");
        if (this.f3186a.m != null) {
            this.f3186a.m.s.b();
            ah.a().postDelayed(new am(this), (long) this.f3186a.k());
        }
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        XLog.i("TagPageCradAdapter", "### surfaceDestroyed ###");
    }
}
