package com.tencent.nucleus.socialcontact.usercenter;

import android.content.SharedPreferences;
import android.text.format.Time;
import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.protocol.jce.GetUserActivityResponse;
import com.tencent.assistant.protocol.jce.UserActivityItem;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class j {

    /* renamed from: a  reason: collision with root package name */
    private static HashMap<String, ArrayList<Object>> f3233a = new HashMap<>();
    private static long b = 0;

    public static synchronized void a(List<? extends JceStruct> list, boolean z) {
        ArrayList arrayList;
        UserActivityItem userActivityItem;
        synchronized (j.class) {
            if (list != null) {
                if (list.size() >= 1) {
                    XLog.i("UserCenterUtils", "[parseResponseEx] : size = " + list.size());
                    if (z) {
                        a(list);
                        XLog.i("UserCenterUtils", "[parseResponseEx] ---> parse network jce data");
                    } else {
                        XLog.i("UserCenterUtils", "[parseResponseEx] ---> parse local jce data");
                    }
                    SharedPreferences.Editor edit = l().edit();
                    f3233a.clear();
                    boolean i = i();
                    Iterator<? extends JceStruct> it = list.iterator();
                    while (true) {
                        if (it.hasNext()) {
                            JceStruct jceStruct = (JceStruct) it.next();
                            if (jceStruct instanceof GetUserActivityResponse) {
                                XLog.i("UserCenterUtils", "[GetUserActivityResponse] : *********** begin ***********");
                                GetUserActivityResponse getUserActivityResponse = (GetUserActivityResponse) jceStruct;
                                ArrayList<UserActivityItem> a2 = getUserActivityResponse.a();
                                if (a2 == null) {
                                    XLog.i("UserCenterUtils", "itemsList is null");
                                    break;
                                }
                                if (l().getInt("max_prompt_num", 0) != getUserActivityResponse.b) {
                                    edit.putInt("max_prompt_num", getUserActivityResponse.b);
                                }
                                Iterator<UserActivityItem> it2 = a2.iterator();
                                while (it2.hasNext()) {
                                    UserActivityItem next = it2.next();
                                    if (next != null && next.j > 0 && next.i) {
                                        String str = "uc_list_group_" + next.j;
                                        if (f3233a.containsKey(str)) {
                                            ArrayList arrayList2 = f3233a.get(str);
                                            if (arrayList2 != null) {
                                                arrayList2.add(a(next, arrayList2.size() + 1));
                                                arrayList = arrayList2;
                                            } else {
                                                arrayList = arrayList2;
                                            }
                                        } else {
                                            ArrayList arrayList3 = new ArrayList();
                                            arrayList3.add(a(next, arrayList3.size() + 1));
                                            arrayList = arrayList3;
                                        }
                                        f3233a.put(str, arrayList);
                                        if (arrayList != null) {
                                            userActivityItem = i.y().a(a("uc_list_jce_", next.j, arrayList.size()));
                                        } else {
                                            userActivityItem = null;
                                        }
                                        if (userActivityItem == null) {
                                            XLog.i("UserCenterUtils", "oldItem is null ");
                                        }
                                        if ((!z || !i || !a(userActivityItem, next)) && (z || !i)) {
                                            if (!(0 == next.f && 0 == next.g)) {
                                                a(false);
                                                if (arrayList != null) {
                                                    edit.putBoolean(a("p_card_dot_", next.j, arrayList.size()), true);
                                                }
                                            }
                                            if (arrayList != null) {
                                                if (next.k != l().getInt(a("p_card_num_", next.j, arrayList.size()), 0)) {
                                                    edit.putInt(a("p_card_num_", next.j, arrayList.size()), next.k);
                                                }
                                                i.y().a(a("uc_list_jce_", next.j, arrayList.size()), next);
                                            }
                                            j();
                                        }
                                    }
                                }
                                XLog.i("UserCenterUtils", "[GetUserActivityResponse] : *********** end ***********");
                            } else {
                                XLog.i("UserCenterUtils", "[parseResponseEx] : other data handler");
                            }
                        } else {
                            edit.commit();
                            if (z) {
                                AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_GET_USER_ACTIVITY_SUCCESS));
                            }
                        }
                    }
                }
            }
        }
    }

    private static void a(List<? extends JceStruct> list) {
        if (list != null && list.size() > 0) {
            XLog.i("UserCenterUtils", "[saveAllJceData] ---> isSucc = " + i.y().c(list));
        }
    }

    public static List<? extends JceStruct> a() {
        return i.y().a(GetUserActivityResponse.class);
    }

    private static boolean i() {
        if (l().contains("uc_sp_flag")) {
            XLog.i("UserCenterUtils", "[isExistSpFlag] ---> uc_sp_flag is exist");
            return true;
        }
        XLog.i("UserCenterUtils", "[isExistSpFlag] ---> uc_sp_flag is not exist");
        return false;
    }

    private static void j() {
        SharedPreferences.Editor edit = l().edit();
        edit.putInt("uc_sp_flag", 0);
        edit.commit();
    }

    private static a a(JceStruct jceStruct, int i) {
        a aVar = new a();
        if (jceStruct != null && (jceStruct instanceof UserActivityItem)) {
            UserActivityItem userActivityItem = (UserActivityItem) jceStruct;
            aVar.l = userActivityItem.h;
            aVar.h = userActivityItem.b;
            aVar.e = userActivityItem.j;
            aVar.i = userActivityItem.c;
            aVar.j = userActivityItem.d;
            aVar.b = userActivityItem.i;
            aVar.c = i;
        }
        return aVar;
    }

    private static boolean a(UserActivityItem userActivityItem, UserActivityItem userActivityItem2) {
        boolean z;
        if (userActivityItem2 != null) {
            z = userActivityItem2.equals(userActivityItem);
        } else if (userActivityItem == null) {
            z = true;
        } else {
            z = false;
        }
        XLog.i("UserCenterUtils", "[isSaveUserActivityItemData] ---> data " + (z ? "### not changed ###" : "### has changed ###"));
        return z;
    }

    public static void a(int i, int i2) {
        UserActivityItem a2;
        if (i > 0 && i2 > 0 && l().getBoolean("p_card_dot_" + i + "_" + i2, false) && (a2 = i.y().a("uc_list_jce_" + i + "_" + i2)) != null && System.currentTimeMillis() > a2.e) {
            SharedPreferences.Editor edit = l().edit();
            edit.putBoolean("p_card_dot_" + i + "_" + i2, false);
            edit.commit();
        }
    }

    public static void b(int i, int i2) {
        if (i > 0 && i2 > 0 && l().getInt("p_card_num_" + i + "_" + i2, 0) != 0) {
            SharedPreferences.Editor edit = l().edit();
            edit.putInt("p_card_num_" + i + "_" + i2, 0);
            a(i, i2);
            edit.commit();
        }
    }

    public static boolean c(int i, int i2) {
        UserActivityItem a2;
        if (i <= 0 || i2 <= 0 || !l().getBoolean("p_card_dot_" + i + "_" + i2, false) || (a2 = i.y().a("uc_list_jce_" + i + "_" + i2)) == null) {
            return false;
        }
        if (0 == a2.f && 0 == a2.g) {
            return false;
        }
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis <= a2.f || currentTimeMillis >= a2.g) {
            return false;
        }
        return true;
    }

    public static boolean b() {
        boolean z;
        boolean z2;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        if (f3233a != null) {
            int size = f3233a.size();
            for (int i = 0; i < size; i++) {
                ArrayList arrayList3 = f3233a.get("uc_list_group_" + (i + i));
                if (arrayList3 != null) {
                    int size2 = arrayList3.size();
                    for (int i2 = 0; i2 < size2; i2++) {
                        Object obj = arrayList3.get(i);
                        if (obj instanceof a) {
                            a aVar = (a) obj;
                            arrayList.add(Boolean.valueOf(c(aVar.e, aVar.c)));
                            arrayList2.add(Boolean.valueOf(d(aVar.e, aVar.c) > 0));
                        }
                    }
                }
            }
        }
        if (l().getBoolean("p_u_mr", false) || !arrayList.contains(true)) {
            z = false;
        } else {
            z = true;
        }
        if (!arrayList2.contains(true) || k()) {
            z2 = false;
        } else {
            z2 = true;
        }
        XLog.i("UserCenterUtils", "condition1 = " + z + ", condition2 = " + z2);
        if (z || z2) {
            return true;
        }
        return false;
    }

    public static boolean c() {
        return l().getBoolean("first_open_assistant", true);
    }

    public static void d() {
        if (l().getBoolean("first_open_assistant", true)) {
            l().edit().putBoolean("first_open_assistant", false).commit();
        }
    }

    public static void a(boolean z) {
        SharedPreferences.Editor edit = l().edit();
        edit.putBoolean("p_u_mr", z);
        if (z) {
            edit.putLong("p_n_mr", System.currentTimeMillis());
        }
        edit.commit();
    }

    public static int d(int i, int i2) {
        if (i <= 0 || i2 <= 0) {
            return 0;
        }
        return l().getInt("p_card_num_" + i + "_" + i2, 0);
    }

    public static void e() {
        a aVar;
        if (f3233a != null) {
            SharedPreferences.Editor edit = l().edit();
            int size = f3233a.size();
            for (int i = 0; i < size; i++) {
                ArrayList arrayList = f3233a.get("uc_list_group_" + (i + i));
                if (arrayList != null) {
                    int size2 = arrayList.size();
                    for (int i2 = 0; i2 < size2; i2++) {
                        Object obj = arrayList.get(i2);
                        if ((obj instanceof a) && (aVar = (a) obj) != null) {
                            String a2 = a("p_card_num_", aVar.e, aVar.c);
                            if (l().contains(a2)) {
                                edit.putInt(a2, 0);
                            }
                        }
                    }
                }
            }
            edit.commit();
        }
    }

    public static String a(String str, int i, int i2) {
        return str + i + "_" + i2;
    }

    public static int f() {
        return l().getInt("max_prompt_num", 2);
    }

    private static boolean k() {
        long j = l().getLong("p_n_mr", 0);
        long currentTimeMillis = System.currentTimeMillis();
        Time time = new Time();
        time.set(j);
        String str = time.year + "_" + time.month + "_" + time.monthDay;
        time.set(currentTimeMillis);
        return str.equals(time.year + "_" + time.month + "_" + time.monthDay);
    }

    private static SharedPreferences l() {
        return AstApp.i().getSharedPreferences("usercenter", 0);
    }

    public static HashMap<String, ArrayList<Object>> g() {
        return f3233a;
    }

    private static boolean m() {
        long currentTimeMillis = System.currentTimeMillis();
        if (Math.abs(currentTimeMillis - b) < 300000) {
            XLog.i("UserCenterUtils", "time space : " + Math.abs(currentTimeMillis - b));
            return false;
        }
        b = currentTimeMillis;
        return true;
    }

    public static void h() {
        if (m()) {
            XLog.i("UserCenterUtils", "[requestUserCenterData]");
            new h().a();
        }
    }
}
