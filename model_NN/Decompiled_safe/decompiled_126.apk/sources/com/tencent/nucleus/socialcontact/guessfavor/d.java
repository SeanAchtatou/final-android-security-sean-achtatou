package com.tencent.nucleus.socialcontact.guessfavor;

import android.os.Bundle;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.nucleus.socialcontact.login.j;

/* compiled from: ProGuard */
class d extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GuessFavorActivity f3146a;

    d(GuessFavorActivity guessFavorActivity) {
        this.f3146a = guessFavorActivity;
    }

    public void onTMAClick(View view) {
        switch (view.getId()) {
            case R.id.tv_login /*2131165440*/:
                Bundle bundle = new Bundle();
                bundle.putInt(AppConst.KEY_LOGIN_TYPE, 2);
                bundle.putInt(AppConst.KEY_FROM_TYPE, 9);
                j.a().a(AppConst.IdentityType.MOBILEQ, bundle);
                return;
            case R.id.btn_change /*2131165441*/:
                if (this.f3146a.x != null) {
                    this.f3146a.w();
                    this.f3146a.x.a();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public STInfoV2 getStInfo(View view) {
        String str = STConst.ST_DEFAULT_SLOT;
        if (view.getTag(R.id.tma_st_slot_tag) instanceof String) {
            str = (String) view.getTag(R.id.tma_st_slot_tag);
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3146a.n, 200);
        buildSTInfo.slotId = str;
        return buildSTInfo;
    }
}
