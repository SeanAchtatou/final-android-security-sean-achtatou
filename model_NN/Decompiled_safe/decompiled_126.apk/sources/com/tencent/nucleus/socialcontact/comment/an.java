package com.tencent.nucleus.socialcontact.comment;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.protocol.jce.ReplyDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class an extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ReplyDetail f3096a;
    final /* synthetic */ CommentReplyListAdapter b;

    an(CommentReplyListAdapter commentReplyListAdapter, ReplyDetail replyDetail) {
        this.b = commentReplyListAdapter;
        this.f3096a = replyDetail;
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b.f3079a, 200);
        buildSTInfo.scene = STConst.ST_PAGE_COMMENT_REPLY;
        String str = STConst.ST_DEFAULT_SLOT;
        if (view.getTag(R.id.tma_st_slot_tag) instanceof String) {
            str = this.b.k + ((String) view.getTag(R.id.tma_st_slot_tag));
        }
        buildSTInfo.slotId = str;
        return buildSTInfo;
    }

    public void onTMAClick(View view) {
        this.b.h.a(this.f3096a.f1459a, this.f3096a.l);
    }
}
