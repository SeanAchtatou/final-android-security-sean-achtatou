package com.tencent.nucleus.socialcontact.tagpage;

import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcher;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.t;
import com.tencent.downloadsdk.DownloadManager;
import com.tencent.downloadsdk.DownloadTask;
import com.tencent.downloadsdk.ac;
import com.tencent.downloadsdk.ae;
import com.tencent.nucleus.socialcontact.tagpage.TPVideoDownInfo;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class l {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f3206a = new Object();
    private static l b = null;
    private static boolean c = false;
    /* access modifiers changed from: private */
    public Map<String, TPVideoDownInfo> d = new ConcurrentHashMap(5);
    /* access modifiers changed from: private */
    public k e = new k();
    /* access modifiers changed from: private */
    public EventDispatcher f = AstApp.i().j();
    /* access modifiers changed from: private */
    public InstallUninstallDialogManager g = new InstallUninstallDialogManager();
    private ae h = new p(this);

    private l() {
        this.g.a(true);
        try {
            List<TPVideoDownInfo> a2 = this.e.a();
            ArrayList<ac> a3 = DownloadManager.a().a(9);
            for (TPVideoDownInfo next : a2) {
                next.d();
                this.d.put(next.f3171a, next);
            }
            if (a3 != null) {
                Iterator<ac> it = a3.iterator();
                while (it.hasNext()) {
                    ac next2 = it.next();
                    TPVideoDownInfo tPVideoDownInfo = this.d.get(next2.b);
                    if (tPVideoDownInfo != null) {
                        if (tPVideoDownInfo.k == null) {
                            tPVideoDownInfo.k = new j();
                        }
                        tPVideoDownInfo.k.b = next2.c;
                        tPVideoDownInfo.k.f3205a = next2.d;
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void a() {
        if (!c) {
            c = true;
            TemporaryThreadManager.get().start(new m(this));
        }
    }

    public long b() {
        long[] s = t.s();
        XLog.i("TPVideoManager", "SDCard 可用 : " + (((double) s[1]) / 1048576.0d) + " MB");
        XLog.i("TPVideoManager", "SDCard 总共: " + (((double) s[0]) / 1048576.0d) + " MB");
        return (long) (((double) s[1]) / 1048576.0d);
    }

    public static l c() {
        l lVar;
        synchronized (f3206a) {
            if (b == null) {
                b = new l();
            }
            lVar = b;
        }
        return lVar;
    }

    public boolean a(TPVideoDownInfo tPVideoDownInfo) {
        if (tPVideoDownInfo == null || TextUtils.isEmpty(tPVideoDownInfo.f3171a)) {
            return false;
        }
        if (tPVideoDownInfo.e() || tPVideoDownInfo.c()) {
            this.f.sendMessage(this.f.obtainMessage(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_SUCC, tPVideoDownInfo));
            b(tPVideoDownInfo);
            XLog.i("TPVideoManager", "[startDownload] ---> UI_EVENT_TP_VIDEO_DOWNLOAD_SUCC ");
            return true;
        }
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(tPVideoDownInfo.b);
        DownloadTask downloadTask = new DownloadTask(tPVideoDownInfo.b(), tPVideoDownInfo.f3171a, 0, 0, tPVideoDownInfo.f(), tPVideoDownInfo.f3171a, arrayList, null);
        ae a2 = com.tencent.assistantv2.st.l.a(tPVideoDownInfo.f3171a, 0, 0, (byte) tPVideoDownInfo.b(), tPVideoDownInfo.l, SimpleDownloadInfo.UIType.NORMAL, SimpleDownloadInfo.DownloadType.VIDEO);
        downloadTask.b = DownloadInfo.getPriority(SimpleDownloadInfo.DownloadType.VIDEO, SimpleDownloadInfo.UIType.NORMAL);
        tPVideoDownInfo.c = downloadTask.d();
        downloadTask.a(this.h);
        downloadTask.a(a2);
        DownloadManager.a().a(downloadTask);
        boolean c2 = c(tPVideoDownInfo);
        if (DownloadManager.a().c(9, tPVideoDownInfo.f3171a) || DownloadProxy.a().h() < 2) {
            if (tPVideoDownInfo.g != TPVideoDownInfo.DownState.DOWNLOADING) {
                tPVideoDownInfo.g = TPVideoDownInfo.DownState.DOWNLOADING;
                this.f.sendMessage(this.f.obtainMessage(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_START, tPVideoDownInfo));
            }
            this.f.sendMessage(this.f.obtainMessage(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOADING, tPVideoDownInfo));
        } else {
            tPVideoDownInfo.g = TPVideoDownInfo.DownState.QUEUING;
            this.f.sendMessage(this.f.obtainMessage(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_QUEUING, tPVideoDownInfo));
        }
        if (c2) {
            this.f.sendMessage(this.f.obtainMessage(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_ADD, tPVideoDownInfo));
        }
        XLog.i("TPVideoManager", "[startDownload] ---> res = " + this.e.a(tPVideoDownInfo));
        return true;
    }

    private void b(TPVideoDownInfo tPVideoDownInfo) {
        if (tPVideoDownInfo != null && !TextUtils.isEmpty(tPVideoDownInfo.f3171a)) {
            this.d.put(tPVideoDownInfo.f3171a, tPVideoDownInfo);
            this.e.a(tPVideoDownInfo);
        }
    }

    private boolean c(TPVideoDownInfo tPVideoDownInfo) {
        try {
            if (!this.d.containsKey(tPVideoDownInfo.f3171a)) {
                this.d.put(tPVideoDownInfo.f3171a, tPVideoDownInfo);
                tPVideoDownInfo.e = System.currentTimeMillis();
                return true;
            }
            if (this.d.get(tPVideoDownInfo.f3171a) != tPVideoDownInfo) {
                this.d.put(tPVideoDownInfo.f3171a, tPVideoDownInfo);
            }
            return false;
        } catch (NullPointerException e2) {
            return false;
        }
    }

    public void a(String str) {
        TemporaryThreadManager.get().start(new o(this, str));
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        TPVideoDownInfo tPVideoDownInfo;
        if (!TextUtils.isEmpty(str) && (tPVideoDownInfo = this.d.get(str)) != null) {
            DownloadManager.a().a(9, str);
            if (tPVideoDownInfo.g == TPVideoDownInfo.DownState.DOWNLOADING || tPVideoDownInfo.g == TPVideoDownInfo.DownState.QUEUING) {
                tPVideoDownInfo.g = TPVideoDownInfo.DownState.PAUSED;
                this.f.sendMessage(this.f.obtainMessage(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_PAUSE, tPVideoDownInfo));
                this.e.a(tPVideoDownInfo);
            }
        }
    }

    public boolean a(String str, boolean z) {
        TPVideoDownInfo remove;
        if (TextUtils.isEmpty(str) || (remove = this.d.remove(str)) == null) {
            return false;
        }
        DownloadManager.a().b(9, str);
        this.e.a(str);
        if (z) {
            FileUtil.deleteFile(remove.c);
        }
        remove.g = TPVideoDownInfo.DownState.DELETE;
        this.f.sendMessage(this.f.obtainMessage(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_DELETE, remove));
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.socialcontact.tagpage.l.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.nucleus.socialcontact.tagpage.l.a(com.tencent.nucleus.socialcontact.tagpage.l, java.lang.String):void
      com.tencent.nucleus.socialcontact.tagpage.l.a(java.lang.String, boolean):boolean */
    public TPVideoDownInfo b(String str) {
        TPVideoDownInfo tPVideoDownInfo = this.d.get(str);
        if (tPVideoDownInfo == null) {
            return tPVideoDownInfo;
        }
        File file = new File(tPVideoDownInfo.c);
        if (TPVideoDownInfo.DownState.SUCC != tPVideoDownInfo.g || file.exists()) {
            return tPVideoDownInfo;
        }
        a(str, true);
        return null;
    }

    public void d() {
        if (this.d != null && this.d.size() > 0) {
            for (Map.Entry<String, TPVideoDownInfo> key : this.d.entrySet()) {
                c((String) key.getKey());
            }
        }
    }
}
