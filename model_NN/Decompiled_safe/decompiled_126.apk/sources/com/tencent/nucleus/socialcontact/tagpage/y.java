package com.tencent.nucleus.socialcontact.tagpage;

import android.view.animation.Animation;

/* compiled from: ProGuard */
class y implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TagPageCardAdapter f3218a;

    y(TagPageCardAdapter tagPageCardAdapter) {
        this.f3218a = tagPageCardAdapter;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter.b(com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter, boolean):boolean
     arg types: [com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter, int]
     candidates:
      com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter.b(com.tencent.nucleus.socialcontact.tagpage.ai, int):void
      com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter.b(com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter, boolean):boolean */
    public void onAnimationStart(Animation animation) {
        boolean unused = this.f3218a.z = true;
    }

    public void onAnimationRepeat(Animation animation) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter.b(com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter, boolean):boolean
     arg types: [com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter, int]
     candidates:
      com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter.b(com.tencent.nucleus.socialcontact.tagpage.ai, int):void
      com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter.b(com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter, boolean):boolean */
    public void onAnimationEnd(Animation animation) {
        this.f3218a.m.v.setVisibility(8);
        boolean unused = this.f3218a.z = false;
    }
}
