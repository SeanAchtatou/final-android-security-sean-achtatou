package com.tencent.nucleus.socialcontact.tagpage;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.ah;

/* compiled from: ProGuard */
public class LoadingView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private TextView f3167a = null;
    /* access modifiers changed from: private */
    public RoundProgressBar b = null;
    /* access modifiers changed from: private */
    public int c = 0;
    private boolean d = false;
    /* access modifiers changed from: private */
    public Handler e = null;
    /* access modifiers changed from: private */
    public Runnable f = new f(this);
    /* access modifiers changed from: private */
    public Runnable g = new g(this);

    static /* synthetic */ int a(LoadingView loadingView, int i) {
        int i2 = loadingView.c + i;
        loadingView.c = i2;
        return i2;
    }

    public LoadingView(Context context) {
        super(context);
        a(context);
    }

    public LoadingView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public LoadingView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context);
    }

    private void a(Context context) {
        LayoutInflater.from(context).inflate((int) R.layout.tag_page_loading_layout, this);
        this.f3167a = (TextView) findViewById(R.id.loading_info);
        this.b = (RoundProgressBar) findViewById(R.id.progress_bar);
        this.d = false;
        this.e = ah.a("video_loading_thread");
    }

    public void a(boolean z) {
        if (this.f3167a != null) {
            this.f3167a.setVisibility(z ? 0 : 8);
        }
    }

    public void a() {
        this.d = true;
        if (this.e != null) {
            this.e.postDelayed(this.f, 50);
        }
    }

    public void b() {
        if (this.c < 100 && this.d) {
            this.d = false;
            if (this.e != null) {
                this.e.postDelayed(this.g, 10);
            }
        }
    }

    public void c() {
        this.c = 0;
    }
}
