package com.tencent.game.smartcard.c;

import android.util.Pair;
import com.tencent.assistant.smartcard.c.z;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.w;
import java.util.List;

/* compiled from: ProGuard */
public class a extends z {
    public boolean a(n nVar, List<Long> list) {
        Pair<Boolean, w> b = b(nVar);
        if (nVar instanceof com.tencent.game.smartcard.b.a) {
            if (!((com.tencent.game.smartcard.b.a) nVar).h() && ((com.tencent.game.smartcard.b.a) nVar).e != null && ((com.tencent.game.smartcard.b.a) nVar).e.size() == 0) {
                return false;
            }
            if (((com.tencent.game.smartcard.b.a) nVar).e == null || ((com.tencent.game.smartcard.b.a) nVar).e.size() < 3) {
                return false;
            }
        }
        if (!((Boolean) b.first).booleanValue()) {
            return false;
        }
        return true;
    }
}
