package com.tencent.game.component;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.SimpleAppInfo;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.STPageInfo;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public abstract class GameNormalSmartcardBaseItem extends IAppCard {

    /* renamed from: a  reason: collision with root package name */
    protected List<SimpleAppInfo> f2653a = new ArrayList();
    HorizontalCardType b = HorizontalCardType.MY_PLAYING_GAME;

    /* compiled from: ProGuard */
    public enum HorizontalCardType {
        MY_PLAYING_GAME,
        FRIEND_PLAYING_GAME,
        COMPETITIVE_GAME
    }

    public GameNormalSmartcardBaseItem(Context context) {
        super(context);
    }

    public GameNormalSmartcardBaseItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public GameNormalSmartcardBaseItem(Context context, List<SimpleAppInfo> list, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
        this.f2653a = list;
        a();
    }

    public GameNormalSmartcardBaseItem(Context context, List<SimpleAppInfo> list, n nVar, as asVar, IViewInvalidater iViewInvalidater, HorizontalCardType horizontalCardType) {
        super(context, nVar, asVar, iViewInvalidater);
        this.f2653a = list;
        this.b = horizontalCardType;
        a();
    }

    /* access modifiers changed from: protected */
    public SimpleAppModel b() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void c() {
        if (this.e != null) {
            if (this.f != null) {
                this.f.a(this.e.j, this.e.k);
            }
            SimpleAppModel b2 = b();
            long j = -1;
            if (b2 != null) {
                j = b2.f938a;
            }
            if (this.e != null) {
                a(STConst.ST_DEFAULT_SLOT, 100, this.e.s, j);
            }
        }
    }

    /* access modifiers changed from: protected */
    public int a(int i) {
        int i2 = 0;
        switch (i) {
            case STConst.ST_PAGE_COMPETITIVE:
                i2 = 1;
                break;
            case 200501:
                i2 = 3;
                break;
            case STConst.ST_PAGE_GAME_POPULAR:
                i2 = 2;
                break;
        }
        if (this.e == null) {
            return STConst.ST_PAGE_SMARTCARD;
        }
        return (i2 * 100) + 20290000 + this.e.j;
    }

    /* access modifiers changed from: protected */
    public String b(int i) {
        if (this.e == null) {
            return Constants.STR_EMPTY + i;
        }
        return this.e.k + "||" + this.e.j + "|" + i;
    }

    /* access modifiers changed from: protected */
    public void a(String str, int i, byte[] bArr, long j) {
        STInfoV2 b2 = b(str, i, bArr, j);
        if (b2 != null) {
            l.a(b2);
        }
    }

    /* access modifiers changed from: protected */
    public STInfoV2 b(String str, int i, byte[] bArr, long j) {
        STInfoV2 a2 = a(str, i);
        if (a2 != null) {
            if (this.e != null) {
                a2.pushInfo = b(0);
            }
            if (j > 0) {
                a2.appId = j;
            }
            if (a2.recommendId == null || a2.recommendId.length == 0) {
                a2.recommendId = bArr;
            }
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public STInfoV2 a(String str, int i) {
        STPageInfo sTPageInfo;
        String str2;
        if (this.c instanceof BaseActivity) {
            sTPageInfo = ((BaseActivity) this.c).n();
        } else {
            sTPageInfo = null;
        }
        if (sTPageInfo == null) {
            return null;
        }
        String str3 = sTPageInfo.b;
        if (this.e.u >= 0) {
            str2 = (this.e.u < 9 ? "0" : Constants.STR_EMPTY) + String.valueOf(this.e.u + 1);
        } else {
            str2 = str3;
        }
        STInfoV2 sTInfoV2 = new STInfoV2(a(sTPageInfo.f2060a), str, sTPageInfo.f2060a, a.b(str2, STConst.ST_STATUS_DEFAULT), i);
        sTInfoV2.pushInfo = b(0);
        sTInfoV2.updateWithSimpleAppModel(b());
        return sTInfoV2;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.g) {
            this.g = true;
            c();
        }
    }
}
