package com.tencent.game.component;

import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
class h extends ViewInvalidateMessageHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameCategoryListView f2663a;

    h(GameCategoryListView gameCategoryListView) {
        this.f2663a = gameCategoryListView;
    }

    public void handleMessage(ViewInvalidateMessage viewInvalidateMessage) {
        List list;
        List list2;
        List list3;
        List list4;
        List list5 = null;
        if (viewInvalidateMessage.what == 1) {
            int i = viewInvalidateMessage.arg2;
            int i2 = viewInvalidateMessage.arg1;
            if (viewInvalidateMessage.params != null) {
                Map map = (Map) viewInvalidateMessage.params;
                if (map.containsKey(0)) {
                    list3 = (List) map.get(0);
                } else {
                    list3 = null;
                }
                if (map.containsKey(1)) {
                    list4 = (List) map.get(1);
                } else {
                    list4 = null;
                }
                if (map.containsKey(2)) {
                    list5 = (List) map.get(2);
                    List list6 = list4;
                    list2 = list3;
                    list = list6;
                } else {
                    List list7 = list4;
                    list2 = list3;
                    list = list7;
                }
            } else {
                list = null;
                list2 = null;
            }
            this.f2663a.a(i, i2, list2, list, list5);
            return;
        }
        this.f2663a.w.a();
        if (this.f2663a.y != null) {
            this.f2663a.y.notifyDataSetChanged();
        }
    }
}
