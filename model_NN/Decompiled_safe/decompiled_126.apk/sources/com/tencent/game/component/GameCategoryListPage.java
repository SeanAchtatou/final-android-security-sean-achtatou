package com.tencent.game.component;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.game.d.j;

/* compiled from: ProGuard */
public class GameCategoryListPage extends RelativeLayout implements i {

    /* renamed from: a  reason: collision with root package name */
    private Context f2652a;
    private GameCategoryListView b;
    private LoadingView c;
    private NormalErrorRecommendPage d;
    private View.OnClickListener e = new f(this);

    public GameCategoryListPage(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context);
        this.f2652a = context;
        this.b.a(this);
    }

    public GameCategoryListPage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
        this.f2652a = context;
        this.b.a(this);
    }

    public GameCategoryListPage(Context context) {
        super(context);
        a(context);
        this.f2652a = context;
        this.b.a(this);
    }

    private void a(Context context) {
        LayoutInflater.from(context).inflate((int) R.layout.game_applist_component_category_view, this);
        this.b = (GameCategoryListView) findViewById(R.id.applist);
        this.b.setVisibility(8);
        this.b.setDivider(null);
        this.b.setSelector(new ColorDrawable(0));
        this.b.setCacheColorHint(17170445);
        this.c = (LoadingView) findViewById(R.id.loading_view);
        this.c.setVisibility(0);
        this.d = (NormalErrorRecommendPage) findViewById(R.id.error_page);
        this.d.setButtonClickListener(this.e);
    }

    public void a(ViewPageScrollListener viewPageScrollListener) {
        this.b.a(viewPageScrollListener);
    }

    public void a(BaseAdapter baseAdapter) {
        this.b.setAdapter(baseAdapter);
    }

    public void a(int i) {
        this.b.setVisibility(8);
        this.d.setVisibility(0);
        this.c.setVisibility(8);
        this.d.setErrorType(i);
    }

    public void a() {
        this.b.setVisibility(0);
        this.d.setVisibility(8);
        this.c.setVisibility(8);
    }

    public void b() {
        this.c.setVisibility(0);
        this.b.setVisibility(8);
        this.d.setVisibility(8);
    }

    public void c() {
        this.b.r();
    }

    public void d() {
        this.b.q();
    }

    public void a(long j) {
        if (this.b != null) {
            this.b.a(j);
        }
    }

    public void a(j jVar) {
        if (this.b != null) {
            this.b.a(jVar);
        }
    }
}
