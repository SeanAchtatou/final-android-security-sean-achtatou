package com.tencent.game.d;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.protocol.jce.GftAppGiftInfo;
import com.tencent.assistant.protocol.jce.GftMydesktopOtherAppInfo;
import com.tencent.game.d.a.b;
import java.util.List;

/* compiled from: ProGuard */
class u implements CallbackHelper.Caller<b> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f2712a;
    final /* synthetic */ int b;
    final /* synthetic */ List c;
    final /* synthetic */ GftAppGiftInfo d;
    final /* synthetic */ GftMydesktopOtherAppInfo e;
    final /* synthetic */ p f;

    u(p pVar, int i, int i2, List list, GftAppGiftInfo gftAppGiftInfo, GftMydesktopOtherAppInfo gftMydesktopOtherAppInfo) {
        this.f = pVar;
        this.f2712a = i;
        this.b = i2;
        this.c = list;
        this.d = gftAppGiftInfo;
        this.e = gftMydesktopOtherAppInfo;
    }

    /* renamed from: a */
    public void call(b bVar) {
        bVar.a(this.f2712a, this.b, this.c, this.d, this.e);
    }
}
