package com.tencent.game.activity;

import android.view.View;
import com.tencent.assistant.protocol.jce.TagGroup;

/* compiled from: ProGuard */
class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameCategoryDetailActivity f2615a;

    b(GameCategoryDetailActivity gameCategoryDetailActivity) {
        this.f2615a = gameCategoryDetailActivity;
    }

    public void onClick(View view) {
        TagGroup tagGroup = (TagGroup) view.getTag();
        if (tagGroup != null && this.f2615a.C != null) {
            this.f2615a.C.k();
            this.f2615a.C.d(tagGroup.b());
        }
    }
}
