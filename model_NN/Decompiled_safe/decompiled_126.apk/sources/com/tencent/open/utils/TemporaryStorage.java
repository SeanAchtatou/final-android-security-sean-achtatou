package com.tencent.open.utils;

import java.util.HashMap;

/* compiled from: ProGuard */
public class TemporaryStorage {

    /* renamed from: a  reason: collision with root package name */
    private static HashMap<String, Object> f3280a = new HashMap<>();

    public static Object set(String str, Object obj) {
        return f3280a.put(str, obj);
    }

    public static Object get(String str) {
        return f3280a.remove(str);
    }

    public static void remove(String str) {
        f3280a.remove(str);
    }
}
