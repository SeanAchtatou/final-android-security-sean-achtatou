package com.tencent.open.a;

import android.os.Environment;

/* compiled from: ProGuard */
public final class j {
    public static boolean a() {
        String externalStorageState = Environment.getExternalStorageState();
        return "mounted".equals(externalStorageState) || "mounted_ro".equals(externalStorageState);
    }

    public static k b() {
        if (!a()) {
            return null;
        }
        return k.b(Environment.getExternalStorageDirectory());
    }
}
