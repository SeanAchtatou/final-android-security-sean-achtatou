package com.tencent.open.a;

import java.io.File;

/* compiled from: ProGuard */
public final class i {
    public static boolean a(File file) {
        if (file == null) {
            return false;
        }
        if (file.isFile()) {
            if (file.delete()) {
                return true;
            }
            file.deleteOnExit();
            return false;
        } else if (!file.isDirectory()) {
            return false;
        } else {
            for (File a2 : file.listFiles()) {
                a(a2);
            }
            return file.delete();
        }
    }
}
