package com.tencent.open.b;

import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.os.SystemClock;
import com.tencent.connect.common.Constants;
import com.tencent.open.a.n;
import com.tencent.open.utils.Global;
import com.tencent.open.utils.OpenConfig;

/* compiled from: ProGuard */
class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ long f3257a;
    final /* synthetic */ String b;
    final /* synthetic */ String c;
    final /* synthetic */ int d;
    final /* synthetic */ long e;
    final /* synthetic */ long f;
    final /* synthetic */ boolean g;
    final /* synthetic */ g h;

    j(g gVar, long j, String str, String str2, int i, long j2, long j3, boolean z) {
        this.h = gVar;
        this.f3257a = j;
        this.b = str;
        this.c = str2;
        this.d = i;
        this.e = j2;
        this.f = j3;
        this.g = z;
    }

    public void run() {
        int i = 1;
        try {
            long elapsedRealtime = SystemClock.elapsedRealtime() - this.f3257a;
            Bundle bundle = new Bundle();
            String a2 = a.a(Global.getContext());
            bundle.putString("apn", a2);
            bundle.putString("appid", "1000067");
            bundle.putString("commandid", this.b);
            bundle.putString("detail", this.c);
            StringBuilder sb = new StringBuilder();
            sb.append("network=").append(a2).append('&');
            sb.append("sdcard=").append(Environment.getExternalStorageState().equals("mounted") ? 1 : 0).append('&');
            sb.append("wifi=").append(a.e(Global.getContext()));
            bundle.putString("deviceInfo", sb.toString());
            int a3 = 100 / this.h.a(this.d);
            if (a3 > 0) {
                if (a3 > 100) {
                    i = 100;
                } else {
                    i = a3;
                }
            }
            bundle.putString("frequency", i + Constants.STR_EMPTY);
            bundle.putString("reqSize", this.e + Constants.STR_EMPTY);
            bundle.putString("resultCode", this.d + Constants.STR_EMPTY);
            bundle.putString("rspSize", this.f + Constants.STR_EMPTY);
            bundle.putString("timeCost", elapsedRealtime + Constants.STR_EMPTY);
            bundle.putString("uin", Constants.DEFAULT_UIN);
            this.h.d.add(new b(bundle));
            int size = this.h.d.size();
            int i2 = OpenConfig.getInstance(Global.getContext(), null).getInt("Agent_ReportTimeInterval");
            if (i2 == 0) {
                i2 = 10000;
            }
            if (this.h.a("report_cgi", size) || this.g) {
                this.h.b();
                this.h.g.removeMessages(1000);
            } else if (!this.h.g.hasMessages(1000)) {
                Message obtain = Message.obtain();
                obtain.what = 1000;
                this.h.g.sendMessageDelayed(obtain, (long) i2);
            }
        } catch (Exception e2) {
            n.b(g.f3254a, "--> reportCGI, exception in sub thread.", e2);
        }
    }
}
