package com.tencent.cloud.smartcard.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.protocol.jce.ContentAggregationComplexItem;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class b extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ContentAggregationComplexItem f2326a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ ComplexContentItemView c;

    b(ComplexContentItemView complexContentItemView, ContentAggregationComplexItem contentAggregationComplexItem, STInfoV2 sTInfoV2) {
        this.c = complexContentItemView;
        this.f2326a = contentAggregationComplexItem;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        com.tencent.pangu.link.b.a(this.c.getContext(), this.f2326a.f);
    }

    public STInfoV2 getStInfo() {
        if (this.b == null) {
            return null;
        }
        this.b.status = STConst.ST_STATUS_DEFAULT;
        this.b.actionId = 200;
        return this.b;
    }
}
