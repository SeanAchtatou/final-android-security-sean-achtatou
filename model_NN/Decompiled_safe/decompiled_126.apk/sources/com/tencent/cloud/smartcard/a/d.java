package com.tencent.cloud.smartcard.a;

import android.content.Context;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.protocol.jce.SmartCardContentAggregationSimple;
import com.tencent.assistant.smartcard.c.c;
import com.tencent.assistant.smartcard.c.z;
import com.tencent.assistant.smartcard.component.NormalSmartcardBaseItem;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistant.smartcard.d.a;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.cloud.smartcard.b.e;
import com.tencent.cloud.smartcard.view.NormalSmartCardSimpleCollectionView;

/* compiled from: ProGuard */
public class d extends c {
    public Class<? extends JceStruct> a() {
        return SmartCardContentAggregationSimple.class;
    }

    public a b() {
        return new e();
    }

    /* access modifiers changed from: protected */
    public z c() {
        return new com.tencent.assistant.smartcard.c.d();
    }

    public NormalSmartcardBaseItem a(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        return new NormalSmartCardSimpleCollectionView(context, nVar, asVar, iViewInvalidater);
    }
}
