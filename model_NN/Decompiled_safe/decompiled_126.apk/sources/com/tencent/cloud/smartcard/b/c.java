package com.tencent.cloud.smartcard.b;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.SmartCardTag;
import com.tencent.assistant.protocol.jce.SmartCardTitle;
import com.tencent.assistant.smartcard.d.a;
import com.tencent.assistant.smartcard.d.x;
import java.util.List;

/* compiled from: ProGuard */
public class c extends a {
    private SmartCardTag e;

    public boolean a(byte b, JceStruct jceStruct) {
        if (b != 37 || !(jceStruct instanceof SmartCardTag)) {
            return false;
        }
        this.e = (SmartCardTag) jceStruct;
        this.j = b;
        this.k = this.e.f;
        SmartCardTitle a2 = this.e.a();
        if (a2 != null) {
            this.l = a2.b;
            this.p = a2.c;
            this.o = a2.d;
        }
        return true;
    }

    public List<SimpleAppModel> a() {
        return null;
    }

    public SmartCardTag h() {
        return this.e;
    }

    public x c() {
        x xVar = new x();
        xVar.e = this.j;
        xVar.f = this.k;
        xVar.f1767a = this.e.c;
        xVar.b = this.e.d;
        return xVar;
    }
}
