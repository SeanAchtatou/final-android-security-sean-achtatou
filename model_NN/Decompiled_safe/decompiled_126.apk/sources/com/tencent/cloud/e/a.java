package com.tencent.cloud.e;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ListView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.g;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.CardItem;
import com.tencent.assistant.protocol.jce.LbsData;
import com.tencent.cloud.adapter.b;
import com.tencent.cloud.b.r;
import com.tencent.cloud.updaterec.f;
import java.util.HashMap;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static HashMap<String, CardItem> f2317a = new HashMap<>();

    public static f a(Object obj) {
        if (obj == null || !(obj instanceof b)) {
            return null;
        }
        return ((b) obj).n;
    }

    public static boolean a(Context context, SimpleAppModel simpleAppModel, ListView listView, View view, boolean z, LbsData lbsData) {
        f fVar;
        if (listView == null) {
            return false;
        }
        boolean z2 = z && g.a().a("oneMoreApp", 0) == 1;
        if (view != null) {
            fVar = a(view.getTag());
        } else {
            fVar = null;
        }
        boolean z3 = fVar != null ? fVar.e == 2 : true;
        if (!z2 || z3 || view == null || fVar == null || simpleAppModel == null) {
            return false;
        }
        fVar.f2337a.setTag(R.id.one_more_list, Integer.valueOf(r.a().a(2, simpleAppModel.f938a, lbsData, simpleAppModel.c)));
        a(context, fVar);
        return true;
    }

    public static void a(Context context, f fVar, CardItem cardItem) {
        fVar.f2337a.a(cardItem, fVar);
        fVar.f2337a.setVisibility(0);
        fVar.b.setVisibility(0);
        if (!TextUtils.isEmpty(cardItem.b) && cardItem.f1183a == 0) {
            fVar.d.setText(cardItem.b);
            fVar.d.setVisibility(0);
        } else if (cardItem.f1183a != 0) {
            fVar.d.setVisibility(8);
        }
        fVar.e = 2;
    }

    public static void a(f fVar) {
        fVar.f2337a.setTag(null);
        fVar.b.setVisibility(8);
        fVar.e = 0;
        fVar.c = null;
    }

    public static void a(Context context, f fVar) {
        fVar.e = 1;
    }

    public static CardItem a(String str) {
        return f2317a.get(str);
    }

    public static void a(String str, CardItem cardItem) {
        f2317a.put(str, cardItem);
    }

    public static void a() {
        f2317a.clear();
    }

    public static boolean a(long j) {
        for (CardItem cardItem : f2317a.values()) {
            if (cardItem.f.f1495a == j) {
                return true;
            }
        }
        return false;
    }
}
