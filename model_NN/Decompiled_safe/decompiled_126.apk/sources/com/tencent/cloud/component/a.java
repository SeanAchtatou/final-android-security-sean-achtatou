package com.tencent.cloud.component;

import android.content.Context;
import android.graphics.Canvas;
import android.widget.LinearLayout;

/* compiled from: ProGuard */
class a extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppRankTabBarView f2272a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    a(AppRankTabBarView appRankTabBarView, Context context) {
        super(context);
        this.f2272a = appRankTabBarView;
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawLine(0.0f, (float) (getHeight() - this.f2272a.c()), (float) getWidth(), (float) (getHeight() - this.f2272a.c()), this.f2272a.n);
        canvas.drawLine((float) this.f2272a.o, (float) ((getHeight() - (this.f2272a.b() / 2)) - this.f2272a.c()), (float) (this.f2272a.o + this.f2272a.a()), (float) ((getHeight() - (this.f2272a.b() / 2)) - this.f2272a.c()), this.f2272a.m);
    }
}
