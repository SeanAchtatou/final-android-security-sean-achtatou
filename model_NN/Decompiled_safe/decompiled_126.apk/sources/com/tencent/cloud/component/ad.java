package com.tencent.cloud.component;

import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
/* synthetic */ class ad {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f2276a = new int[AppConst.AppState.values().length];

    static {
        try {
            f2276a[AppConst.AppState.DOWNLOAD.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f2276a[AppConst.AppState.UPDATE.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f2276a[AppConst.AppState.ILLEGAL.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f2276a[AppConst.AppState.FAIL.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f2276a[AppConst.AppState.PAUSED.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f2276a[AppConst.AppState.DOWNLOADED.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
    }
}
