package com.tencent.cloud.activity.debug;

import android.view.View;
import android.widget.Toast;
import com.tencent.assistant.plugin.QReaderClient;
import com.tencent.assistant.plugin.SimpleLoginInfo;
import com.tencent.assistant.plugin.mgr.k;

/* compiled from: ProGuard */
class h implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ QReaderPluginDebugActivity f2188a;

    h(QReaderPluginDebugActivity qReaderPluginDebugActivity) {
        this.f2188a = qReaderPluginDebugActivity;
    }

    public void onClick(View view) {
        SimpleLoginInfo userLoginInfo = QReaderClient.getInstance().getUserLoginInfo();
        if (userLoginInfo != null) {
            k.a().a(this.f2188a, userLoginInfo.uin, 3);
        } else {
            Toast.makeText(this.f2188a, "没有登录qq无法充值", 0).show();
        }
    }
}
