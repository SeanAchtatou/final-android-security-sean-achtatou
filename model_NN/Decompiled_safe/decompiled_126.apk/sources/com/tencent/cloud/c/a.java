package com.tencent.cloud.c;

import com.tencent.assistant.localres.y;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.be;
import com.tencent.assistant.smartcard.b.d;
import com.tencent.assistant.smartcard.c.p;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.cloud.d.a.c;
import com.tencent.cloud.d.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class a extends y<c> {

    /* renamed from: a  reason: collision with root package name */
    private be f2255a = new be();
    /* access modifiers changed from: private */
    public long b = 0;
    private b c = b.a();
    /* access modifiers changed from: private */
    public d d = new d();
    private d e = new d(this, null);
    /* access modifiers changed from: private */
    public volatile boolean f = false;
    /* access modifiers changed from: private */
    public volatile boolean g = false;
    /* access modifiers changed from: private */
    public c h;
    private int i;
    private List<Long> j = new ArrayList();

    public a() {
        this.c.register(this.e);
        this.f2255a.register(this.e);
    }

    public void a() {
        this.f = false;
        this.g = false;
        this.c.d();
        this.f2255a.a(1);
    }

    public int b() {
        return this.c.e();
    }

    public void c() {
        a();
    }

    public com.tencent.assistant.model.b d() {
        com.tencent.assistant.model.b b2 = this.c.b();
        b2.c(a(b2.c(), 0));
        b2.b((List<SimpleAppModel>) null);
        return b2;
    }

    public void e() {
        if (this.b > 0 && System.currentTimeMillis() - this.b > m.a().ai()) {
            c();
        }
    }

    public void a(c cVar) {
        if (this.f && this.g && cVar != null) {
            int i2 = this.i;
            if (cVar.e) {
                i2 = 0;
            }
            a(new b(this, cVar, a(cVar.g, i2)));
        }
    }

    public synchronized List<com.tencent.pangu.model.b> a(List<SimpleAppModel> list, int i2) {
        ArrayList arrayList;
        if (list != null) {
            if (list.size() > 0) {
                if (i2 == 0) {
                    this.j.clear();
                }
                ArrayList arrayList2 = new ArrayList();
                for (Long next : this.j) {
                    for (SimpleAppModel next2 : list) {
                        if (next2.f938a == next.longValue()) {
                            arrayList2.add(next2);
                        }
                    }
                }
                ArrayList arrayList3 = new ArrayList(list);
                arrayList3.removeAll(arrayList2);
                ArrayList arrayList4 = new ArrayList();
                int i3 = 0;
                while (i3 < arrayList3.size()) {
                    com.tencent.pangu.model.b bVar = new com.tencent.pangu.model.b();
                    List<n> a2 = this.d.a(i2);
                    if (a2 != null && a2.size() > 0) {
                        bVar.g = p.a().a(a2, this.j, i2);
                        if (bVar.g != null) {
                            p.a().a(bVar.g);
                            bVar.b = 2;
                            List<Long> d2 = bVar.g.d();
                            if (d2 != null) {
                                this.j.addAll(d2);
                            }
                        }
                    }
                    if (bVar.g == null) {
                        do {
                            int i4 = i3;
                            SimpleAppModel simpleAppModel = (SimpleAppModel) arrayList3.get(i4);
                            if (!this.j.contains(Long.valueOf(simpleAppModel.f938a))) {
                                bVar.c = simpleAppModel;
                                bVar.b = 1;
                                this.j.add(Long.valueOf(simpleAppModel.f938a));
                            }
                            i3 = i4 + 1;
                            if (bVar.c != null) {
                                break;
                            }
                        } while (i3 < arrayList3.size());
                    }
                    i2++;
                    arrayList4.add(bVar);
                }
                if (arrayList4 != null) {
                    this.i += arrayList4.size();
                }
                arrayList = arrayList4;
            }
        }
        arrayList = null;
        return arrayList;
    }
}
