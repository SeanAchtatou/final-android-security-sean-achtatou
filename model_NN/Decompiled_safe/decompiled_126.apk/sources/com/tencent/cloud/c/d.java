package com.tencent.cloud.c;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.n;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.protocol.jce.GetSmartCardsResponse;
import com.tencent.assistant.protocol.jce.SmartCardWrapper;
import com.tencent.cloud.d.a.a;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
class d implements n, a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f2258a;

    private d(a aVar) {
        this.f2258a = aVar;
    }

    /* synthetic */ d(a aVar, b bVar) {
        this(aVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.cloud.c.a.a(com.tencent.cloud.c.a, boolean):boolean
     arg types: [com.tencent.cloud.c.a, int]
     candidates:
      com.tencent.cloud.c.a.a(com.tencent.cloud.c.a, long):long
      com.tencent.cloud.c.a.a(com.tencent.cloud.c.a, com.tencent.cloud.c.c):com.tencent.cloud.c.c
      com.tencent.cloud.c.a.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, int):java.util.List<com.tencent.pangu.model.b>
      com.tencent.cloud.c.a.a(com.tencent.cloud.c.a, boolean):boolean */
    public void a(int i, int i2, List<SmartCardWrapper> list, int i3, JceStruct jceStruct) {
        if (jceStruct != null) {
            GetSmartCardsResponse getSmartCardsResponse = (GetSmartCardsResponse) jceStruct;
            synchronized (this.f2258a) {
                this.f2258a.d.a(getSmartCardsResponse.b);
            }
        }
        boolean unused = this.f2258a.f = true;
        if (this.f2258a.g) {
            this.f2258a.a(this.f2258a.h);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.cloud.c.a.a(com.tencent.cloud.c.a, boolean):boolean
     arg types: [com.tencent.cloud.c.a, int]
     candidates:
      com.tencent.cloud.c.a.a(com.tencent.cloud.c.a, long):long
      com.tencent.cloud.c.a.a(com.tencent.cloud.c.a, com.tencent.cloud.c.c):com.tencent.cloud.c.c
      com.tencent.cloud.c.a.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, int):java.util.List<com.tencent.pangu.model.b>
      com.tencent.cloud.c.a.a(com.tencent.cloud.c.a, boolean):boolean */
    public void a(int i, int i2) {
        boolean unused = this.f2258a.f = true;
        if (this.f2258a.g) {
            this.f2258a.a(this.f2258a.h);
        }
    }

    public void a(int i, int i2, boolean z, byte[] bArr, boolean z2, ArrayList<ColorCardItem> arrayList, List<SimpleAppModel> list) {
        long unused = this.f2258a.b = System.currentTimeMillis();
        c cVar = new c(this.f2258a, null);
        cVar.f2257a = i;
        cVar.b = i2;
        cVar.c = z;
        cVar.d = bArr;
        cVar.e = z2;
        cVar.f = arrayList;
        cVar.g = list;
        boolean unused2 = this.f2258a.g = true;
        if (!this.f2258a.f) {
            c unused3 = this.f2258a.h = cVar;
        } else {
            this.f2258a.a(cVar);
        }
    }
}
