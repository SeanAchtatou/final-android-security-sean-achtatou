package com.tencent.cloud.d;

import com.tencent.assistant.manager.i;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.GetRecommendTabPageResponse;
import java.util.ArrayList;

/* compiled from: ProGuard */
class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ i f2308a;

    j(i iVar) {
        this.f2308a = iVar;
    }

    public void run() {
        boolean z = true;
        GetRecommendTabPageResponse b = i.y().b(this.f2308a.c);
        if (b == null || b.e != this.f2308a.f2307a.b || b.a() == null || b.a().size() <= 0) {
            int unused = this.f2308a.h = this.f2308a.f2307a.a(this.f2308a.h, this.f2308a.c);
            return;
        }
        i iVar = this.f2308a;
        long j = b.e;
        ArrayList<SimpleAppModel> b2 = k.b(b.a());
        if (b.f != 1) {
            z = false;
        }
        iVar.a(j, b2, z, b.c());
    }
}
