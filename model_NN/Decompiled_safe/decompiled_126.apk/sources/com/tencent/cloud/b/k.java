package com.tencent.cloud.b;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.a;
import com.tencent.assistant.protocol.jce.CftGetAppListResponse;
import java.util.ArrayList;

/* compiled from: ProGuard */
class k implements CallbackHelper.Caller<a> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f2243a;
    final /* synthetic */ boolean b;
    final /* synthetic */ ArrayList c;
    final /* synthetic */ CftGetAppListResponse d;
    final /* synthetic */ g e;

    k(g gVar, int i, boolean z, ArrayList arrayList, CftGetAppListResponse cftGetAppListResponse) {
        this.e = gVar;
        this.f2243a = i;
        this.b = z;
        this.c = arrayList;
        this.d = cftGetAppListResponse;
    }

    /* renamed from: a */
    public void call(a aVar) {
        aVar.a(this.f2243a, 0, this.b, this.c, this.d.f);
    }
}
