package com.tencent.cloud.b;

import com.tencent.assistant.model.AppGroupInfo;
import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.cloud.b.a.b;
import java.util.List;

/* compiled from: ProGuard */
class p implements CallbackHelper.Caller<b> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f2248a;
    final /* synthetic */ boolean b;
    final /* synthetic */ List c;
    final /* synthetic */ AppGroupInfo d;
    final /* synthetic */ o e;

    p(o oVar, int i, boolean z, List list, AppGroupInfo appGroupInfo) {
        this.e = oVar;
        this.f2248a = i;
        this.b = z;
        this.c = list;
        this.d = appGroupInfo;
    }

    /* renamed from: a */
    public void call(b bVar) {
        bVar.a(this.f2248a, 0, this.b, this.c, this.d);
    }
}
