package com.tencent.cloud.b;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.model.QuickEntranceNotify;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.ag;
import com.tencent.assistant.module.ak;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.protocol.jce.AppCategory;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.protocol.jce.GetAppCategoryRequest;
import com.tencent.assistant.protocol.jce.GetAppCategoryResponse;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class a extends BaseEngine<com.tencent.cloud.b.a.a> implements ak, NetworkMonitor.ConnectivityChangeListener {
    private static a b = null;

    /* renamed from: a  reason: collision with root package name */
    private final String f2233a = "CategoryEngine";
    private long c = -1;
    /* access modifiers changed from: private */
    public List<ColorCardItem> d = new ArrayList();
    /* access modifiers changed from: private */
    public List<ColorCardItem> e = new ArrayList();
    /* access modifiers changed from: private */
    public List<AppCategory> f = new ArrayList();
    /* access modifiers changed from: private */
    public List<AppCategory> g = new ArrayList();
    /* access modifiers changed from: private */
    public List<AppCategory> h = new ArrayList();
    /* access modifiers changed from: private */
    public List<AppCategory> i = new ArrayList();
    private int j = -1;

    private a() {
        ag.b().a(this);
    }

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (b == null) {
                b = new a();
            }
            aVar = b;
        }
        return aVar;
    }

    public List<AppCategory> a(long j2) {
        if (j2 == -1) {
            return this.f;
        }
        return this.g;
    }

    public List<AppCategory> b(long j2) {
        if (j2 == -1) {
            return this.h;
        }
        return this.i;
    }

    public List<ColorCardItem> c(long j2) {
        if (j2 == -1) {
            return this.d;
        }
        return this.e;
    }

    public long b() {
        return this.c;
    }

    public void c() {
        XLog.d("CategoryEngine", "loadData:start");
        if (this.h.size() <= 0 || this.i.size() <= 0) {
            XLog.d("CategoryEngine", "loadData:load from server");
            TemporaryThreadManager.get().start(new c(this));
            return;
        }
        XLog.d("CategoryEngine", "loadData:load from local");
        notifyDataChangedInMainThread(new b(this));
    }

    public int d() {
        if (this.j > 0) {
            cancel(this.j);
        }
        this.j = send(new GetAppCategoryRequest());
        return this.j;
    }

    private List<com.tencent.assistant.module.a> a(List<AppCategory> list) {
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        ArrayList<com.tencent.assistant.module.a> arrayList2 = new ArrayList<>();
        for (AppCategory next : list) {
            com.tencent.assistant.module.a aVar = new com.tencent.assistant.module.a(next);
            arrayList2.add(aVar);
            hashMap.put(Long.valueOf(next.f1140a), aVar);
        }
        for (com.tencent.assistant.module.a aVar2 : arrayList2) {
            if (aVar2.c.e == 0) {
                arrayList.add(aVar2);
            } else {
                com.tencent.assistant.module.a aVar3 = (com.tencent.assistant.module.a) hashMap.get(Long.valueOf(aVar2.c.e));
                if (aVar3 != null) {
                    aVar3.a(aVar2);
                }
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        XLog.d("CategoryEngine", "(loadLoaclCache)");
        GetAppCategoryResponse b2 = i.y().b();
        if (b2 != null && b2.c != m.a().a((byte) 4)) {
            return false;
        }
        XLog.d("CategoryEngine", "(loadLoaclCache) start");
        if (b2 == null || b2.b == null || b2.b.size() <= 0) {
            return false;
        }
        XLog.d("CategoryEngine", "(loadLoaclCache) color 1");
        ArrayList<ColorCardItem> b3 = b2.b();
        this.d.clear();
        if (b3 != null && b3.size() > 0) {
            this.d.addAll(b3);
        }
        ArrayList<ColorCardItem> c2 = b2.c();
        this.e.clear();
        if (c2 != null && c2.size() > 0) {
            this.e.addAll(c2);
        }
        XLog.d("CategoryEngine", "(loadLoaclCache) common 1");
        ArrayList<AppCategory> arrayList = b2.e;
        if (arrayList != null && arrayList.size() > 0) {
            List<AppCategory> b4 = b(arrayList, -1);
            List<AppCategory> b5 = b(arrayList, -2);
            if (b4 != null && b4.size() > 0) {
                this.f.clear();
                this.f.addAll(b4);
            }
            if (b5 != null && b5.size() > 0) {
                this.g.clear();
                this.g.addAll(b5);
            }
        }
        XLog.d("CategoryEngine", "(loadLoaclCache) normal 1");
        ArrayList<AppCategory> arrayList2 = b2.b;
        XLog.d("CategoryEngine", "(loadLoaclCache) normal 1:" + arrayList2.size());
        if (arrayList2 == null || arrayList2.size() <= 0) {
            return false;
        }
        XLog.d("CategoryEngine", "(loadLoaclCache) normal 2:");
        List<com.tencent.assistant.module.a> a2 = a(arrayList2);
        if (a2 == null) {
            return false;
        }
        XLog.d("CategoryEngine", "(loadLoaclCache) normal 3:");
        this.c = b2.c;
        List<AppCategory> a3 = a(a2, -1);
        List<AppCategory> a4 = a(a2, -2);
        if (a3 != null && a3.size() > 0) {
            this.h.clear();
            this.h.addAll(a3);
        }
        if (a4 != null && a4.size() > 0) {
            this.i.clear();
            this.i.addAll(a4);
        }
        notifyDataChangedInMainThread(new d(this));
        return true;
    }

    private List<AppCategory> a(List<com.tencent.assistant.module.a> list, long j2) {
        if (list == null || list.size() == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (com.tencent.assistant.module.a a2 : list) {
            com.tencent.assistant.module.a a3 = a(a2, j2);
            if (a3 != null) {
                Iterator<com.tencent.assistant.module.a> it = a3.f949a.iterator();
                while (it.hasNext()) {
                    arrayList.add(it.next().c);
                }
                return arrayList;
            }
        }
        return null;
    }

    private List<AppCategory> b(List<AppCategory> list, long j2) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (AppCategory next : list) {
            if (next != null && next.e == j2) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    private com.tencent.assistant.module.a a(com.tencent.assistant.module.a aVar, long j2) {
        if (aVar.c.a() == j2) {
            return aVar;
        }
        if (aVar.f949a != null && !aVar.f949a.isEmpty()) {
            Iterator<com.tencent.assistant.module.a> it = aVar.f949a.iterator();
            while (it.hasNext()) {
                com.tencent.assistant.module.a a2 = a(it.next(), j2);
                if (a2 != null) {
                    return a2;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        List<com.tencent.assistant.module.a> a2;
        if (jceStruct2 != null) {
            GetAppCategoryResponse getAppCategoryResponse = (GetAppCategoryResponse) jceStruct2;
            ArrayList<ColorCardItem> b2 = getAppCategoryResponse.b();
            this.d.clear();
            if (b2 != null && b2.size() > 0) {
                this.d.addAll(b2);
            }
            ArrayList<ColorCardItem> c2 = getAppCategoryResponse.c();
            this.e.clear();
            if (c2 != null && c2.size() > 0) {
                this.e.addAll(c2);
            }
            ArrayList<AppCategory> arrayList = getAppCategoryResponse.e;
            if (arrayList != null && arrayList.size() > 0) {
                List<AppCategory> b3 = b(arrayList, -1);
                List<AppCategory> b4 = b(arrayList, -2);
                if (b3 != null && b3.size() > 0) {
                    this.f.clear();
                    this.f.addAll(b3);
                }
                if (b4 != null && b4.size() > 0) {
                    this.g.clear();
                    this.g.addAll(b4);
                }
            }
            ArrayList<AppCategory> a3 = getAppCategoryResponse.a();
            if (a3 != null && a3.size() > 0 && (a2 = a(a3)) != null) {
                this.c = getAppCategoryResponse.c;
                List<AppCategory> a4 = a(a2, -1);
                List<AppCategory> a5 = a(a2, -2);
                if (a4 != null && a4.size() > 0) {
                    this.h.clear();
                    this.h.addAll(a4);
                }
                if (a5 != null && a5.size() > 0) {
                    this.i.clear();
                    this.i.addAll(a5);
                }
                notifyDataChangedInMainThread(new e(this, i2));
                i.y().a(getAppCategoryResponse);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new f(this, i2, i3));
    }

    public void onLocalDataHasUpdate() {
        if (this.c != m.a().a((byte) 4)) {
            d();
        }
    }

    public void onPromptHasNewNotify(ArrayList<QuickEntranceNotify> arrayList) {
    }

    public void onConnected(APN apn) {
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
    }
}
