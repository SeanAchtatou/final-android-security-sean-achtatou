package com.tencent.cloud.adapter;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class e extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f2214a;
    final /* synthetic */ b b;
    final /* synthetic */ STInfoV2 c;
    final /* synthetic */ AppUpdateIgnoreListAdapter d;

    e(AppUpdateIgnoreListAdapter appUpdateIgnoreListAdapter, SimpleAppModel simpleAppModel, b bVar, STInfoV2 sTInfoV2) {
        this.d = appUpdateIgnoreListAdapter;
        this.f2214a = simpleAppModel;
        this.b = bVar;
        this.c = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.d.g.clear();
        if (this.d.l == null || !this.d.l.equals(this.f2214a.c)) {
            String unused = this.d.l = this.f2214a.c;
            this.d.notifyDataSetChanged();
            return;
        }
        String unused2 = this.d.l = (String) null;
        this.b.j.setVisibility(0);
        this.b.k.setVisibility(8);
        this.d.g.delete(this.f2214a.c.hashCode());
        this.b.m.setImageDrawable(this.d.d.getResources().getDrawable(R.drawable.icon_open));
    }

    public STInfoV2 getStInfo() {
        if (this.c != null) {
            this.c.actionId = 200;
            this.c.status = "02";
        }
        return this.c;
    }
}
