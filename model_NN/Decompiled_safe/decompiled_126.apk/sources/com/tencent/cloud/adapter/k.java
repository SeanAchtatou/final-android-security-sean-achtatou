package com.tencent.cloud.adapter;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class k extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f2220a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ g c;

    k(g gVar, SimpleAppModel simpleAppModel, STInfoV2 sTInfoV2) {
        this.c = gVar;
        this.f2220a = simpleAppModel;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.c.c(this.f2220a);
    }

    public STInfoV2 getStInfo() {
        this.b.actionId = 200;
        this.b.status = "01";
        return this.b;
    }
}
