package com.tencent.cloud.adapter;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class l extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f2221a;
    final /* synthetic */ int b;
    final /* synthetic */ int c;
    final /* synthetic */ STInfoV2 d;
    final /* synthetic */ g e;

    l(g gVar, SimpleAppModel simpleAppModel, int i, int i2, STInfoV2 sTInfoV2) {
        this.e = gVar;
        this.f2221a = simpleAppModel;
        this.b = i;
        this.c = i2;
        this.d = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.e.a(this.f2221a, this.b, this.c);
    }

    public STInfoV2 getStInfo() {
        this.d.actionId = 200;
        this.d.status = "03";
        return this.d;
    }
}
