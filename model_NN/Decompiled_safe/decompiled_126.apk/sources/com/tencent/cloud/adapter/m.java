package com.tencent.cloud.adapter;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class m extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f2222a;
    final /* synthetic */ b b;
    final /* synthetic */ STInfoV2 c;
    final /* synthetic */ g d;

    m(g gVar, SimpleAppModel simpleAppModel, b bVar, STInfoV2 sTInfoV2) {
        this.d = gVar;
        this.f2222a = simpleAppModel;
        this.b = bVar;
        this.c = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.d.p.clear();
        if (this.d.q == null || !this.d.q.equals(this.f2222a.c)) {
            String unused = this.d.q = this.f2222a.c;
            this.d.notifyDataSetChanged();
            return;
        }
        String unused2 = this.d.q = (String) null;
        this.b.j.setVisibility(0);
        this.b.k.setVisibility(8);
        this.d.p.delete(this.f2222a.c.hashCode());
        this.b.m.setImageResource(R.drawable.icon_open);
    }

    public STInfoV2 getStInfo() {
        this.c.actionId = 200;
        this.c.status = "02";
        return this.c;
    }
}
