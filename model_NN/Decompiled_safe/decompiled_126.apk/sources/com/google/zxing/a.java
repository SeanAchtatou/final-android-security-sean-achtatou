package com.google.zxing;

import java.util.Hashtable;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    public static final a f114a = new a("QR_CODE");
    public static final a b = new a("DATA_MATRIX");
    public static final a c = new a("UPC_E");
    public static final a d = new a("UPC_A");
    public static final a e = new a("EAN_8");
    public static final a f = new a("EAN_13");
    public static final a g = new a("UPC_EAN_EXTENSION");
    public static final a h = new a("CODE_128");
    public static final a i = new a("CODE_39");
    public static final a j = new a("CODE_93");
    public static final a k = new a("CODABAR");
    public static final a l = new a("ITF");
    public static final a m = new a("RSS14");
    public static final a n = new a("PDF417");
    public static final a o = new a("RSS_EXPANDED");
    private static final Hashtable p = new Hashtable();
    private final String q;

    private a(String str) {
        this.q = str;
        p.put(str, this);
    }

    public String toString() {
        return this.q;
    }
}
