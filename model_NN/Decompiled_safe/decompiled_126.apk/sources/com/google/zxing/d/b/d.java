package com.google.zxing.d.b;

import com.google.zxing.j;

public final class d extends j {

    /* renamed from: a  reason: collision with root package name */
    private final float f188a;
    private int b = 1;

    d(float f, float f2, float f3) {
        super(f, f2);
        this.f188a = f3;
    }

    /* access modifiers changed from: package-private */
    public boolean a(float f, float f2, float f3) {
        if (Math.abs(f2 - b()) > f || Math.abs(f3 - a()) > f) {
            return false;
        }
        float abs = Math.abs(f - this.f188a);
        return abs <= 1.0f || abs / this.f188a <= 1.0f;
    }

    public float c() {
        return this.f188a;
    }

    /* access modifiers changed from: package-private */
    public int d() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        this.b++;
    }
}
