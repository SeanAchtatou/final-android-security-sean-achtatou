package com.google.zxing.common;

import java.util.Hashtable;

public final class d extends j {

    /* renamed from: a  reason: collision with root package name */
    private static Hashtable f160a;
    private static Hashtable b;
    private final String c;

    private d(int i, String str) {
        super(i);
        this.c = str;
    }

    public static d a(int i) {
        if (f160a == null) {
            b();
        }
        if (i >= 0 && i < 900) {
            return (d) f160a.get(new Integer(i));
        }
        throw new IllegalArgumentException(new StringBuffer().append("Bad ECI value: ").append(i).toString());
    }

    private static void a(int i, String str) {
        d dVar = new d(i, str);
        f160a.put(new Integer(i), dVar);
        b.put(str, dVar);
    }

    private static void a(int i, String[] strArr) {
        d dVar = new d(i, strArr[0]);
        f160a.put(new Integer(i), dVar);
        for (String put : strArr) {
            b.put(put, dVar);
        }
    }

    private static void b() {
        f160a = new Hashtable(29);
        b = new Hashtable(29);
        a(0, "Cp437");
        a(1, new String[]{"ISO8859_1", "ISO-8859-1"});
        a(2, "Cp437");
        a(3, new String[]{"ISO8859_1", "ISO-8859-1"});
        a(4, "ISO8859_2");
        a(5, "ISO8859_3");
        a(6, "ISO8859_4");
        a(7, "ISO8859_5");
        a(8, "ISO8859_6");
        a(9, "ISO8859_7");
        a(10, "ISO8859_8");
        a(11, "ISO8859_9");
        a(12, "ISO8859_10");
        a(13, "ISO8859_11");
        a(15, "ISO8859_13");
        a(16, "ISO8859_14");
        a(17, "ISO8859_15");
        a(18, "ISO8859_16");
        a(20, new String[]{"SJIS", "Shift_JIS"});
    }

    public String a() {
        return this.c;
    }
}
