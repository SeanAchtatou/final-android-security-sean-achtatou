package com.google.zxing.b.a.a.a;

final class n extends q {
    private final char b;

    n(int i, char c) {
        super(i);
        this.b = c;
    }

    /* access modifiers changed from: package-private */
    public char a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.b == '$';
    }
}
