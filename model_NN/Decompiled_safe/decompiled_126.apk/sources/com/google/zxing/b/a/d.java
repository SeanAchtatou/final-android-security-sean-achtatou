package com.google.zxing.b.a;

final class d extends b {

    /* renamed from: a  reason: collision with root package name */
    private final c f137a;
    private int b;

    d(int i, int i2, c cVar) {
        super(i, i2);
        this.f137a = cVar;
    }

    /* access modifiers changed from: package-private */
    public c c() {
        return this.f137a;
    }

    /* access modifiers changed from: package-private */
    public int d() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        this.b++;
    }
}
