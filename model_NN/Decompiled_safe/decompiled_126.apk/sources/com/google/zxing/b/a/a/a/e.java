package com.google.zxing.b.a.a.a;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.a;

final class e extends i {
    private final String c;
    private final String d;

    e(a aVar, String str, String str2) {
        super(aVar);
        this.c = str2;
        this.d = str;
    }

    private void c(StringBuffer stringBuffer, int i) {
        int a2 = this.b.a(i, 16);
        if (a2 != 38400) {
            stringBuffer.append('(');
            stringBuffer.append(this.c);
            stringBuffer.append(')');
            int i2 = a2 % 32;
            int i3 = a2 / 32;
            int i4 = (i3 % 12) + 1;
            int i5 = i3 / 12;
            if (i5 / 10 == 0) {
                stringBuffer.append('0');
            }
            stringBuffer.append(i5);
            if (i4 / 10 == 0) {
                stringBuffer.append('0');
            }
            stringBuffer.append(i4);
            if (i2 / 10 == 0) {
                stringBuffer.append('0');
            }
            stringBuffer.append(i2);
        }
    }

    /* access modifiers changed from: protected */
    public int a(int i) {
        return i % 100000;
    }

    public String a() {
        if (this.f128a.b != 84) {
            throw NotFoundException.a();
        }
        StringBuffer stringBuffer = new StringBuffer();
        b(stringBuffer, 8);
        b(stringBuffer, 48, 20);
        c(stringBuffer, 68);
        return stringBuffer.toString();
    }

    /* access modifiers changed from: protected */
    public void a(StringBuffer stringBuffer, int i) {
        stringBuffer.append('(');
        stringBuffer.append(this.d);
        stringBuffer.append(i / 100000);
        stringBuffer.append(')');
    }
}
