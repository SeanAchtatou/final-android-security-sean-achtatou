package com.google.zxing.b.a.a.a;

final class p extends q {
    private final int b;
    private final int c;

    p(int i, int i2, int i3) {
        super(i);
        this.b = i2;
        this.c = i3;
        if (this.b < 0 || this.b > 10) {
            throw new IllegalArgumentException(new StringBuffer().append("Invalid firstDigit: ").append(i2).toString());
        } else if (this.c < 0 || this.c > 10) {
            throw new IllegalArgumentException(new StringBuffer().append("Invalid secondDigit: ").append(i3).toString());
        }
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.b == 10;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.c == 10;
    }
}
