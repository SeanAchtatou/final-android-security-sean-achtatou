package com.qq.util;

import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class z {
    public static String a(byte[] bArr) {
        if (bArr == null) {
            return Constants.STR_EMPTY;
        }
        String str = Constants.STR_EMPTY;
        for (int i = 0; i < bArr.length; i++) {
            str = (str + Integer.toHexString((bArr[i] >> 4) & 15)) + Integer.toHexString(bArr[i] & 15);
        }
        return str;
    }
}
