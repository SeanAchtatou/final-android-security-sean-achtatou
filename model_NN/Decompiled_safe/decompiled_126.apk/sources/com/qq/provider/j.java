package com.qq.provider;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;
import com.qq.AppService.r;
import com.qq.d.b.a;
import com.qq.d.b.b;
import com.qq.d.b.d;
import com.qq.d.b.e;
import com.qq.g.c;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class j {

    /* renamed from: a  reason: collision with root package name */
    public static final String[] f342a = {"mimetype", "raw_contact_id", "_id", "data1", "data2", "data3", "data4", "data5", "data6", "data15"};
    private static j b = null;
    private boolean c = false;
    private String d = null;
    private String e = null;

    private j() {
    }

    public void a(Context context) {
        if (!this.c) {
            this.c = true;
            a ad = ad(context, new c());
            if (ad != null) {
                this.d = ad.b;
                this.e = ad.f271a;
                return;
            }
            AccountManager accountManager = AccountManager.get(context);
            if (accountManager != null) {
                Account[] accounts = accountManager.getAccounts();
                if (accounts == null || accounts.length <= 0) {
                    this.d = null;
                    this.e = null;
                    return;
                }
                this.d = accounts[0].name;
                this.e = accounts[0].type;
                return;
            }
            this.d = null;
            this.e = null;
        }
    }

    private a ad(Context context, c cVar) {
        Cursor cursor;
        Cursor cursor2;
        Cursor cursor3;
        boolean z;
        if (com.qq.util.j.c < 14) {
            try {
                cursor = context.getContentResolver().query(ContactsContract.RawContacts.CONTENT_URI, new String[]{"account_type", "account_name", "_id"}, " 1=1) group by account_type order by _id --(", null, null);
            } catch (Throwable th) {
                cursor = null;
            }
        } else {
            cursor = null;
        }
        if (cursor != null || com.qq.util.j.c >= 16) {
            cursor2 = cursor;
        } else {
            try {
                cursor2 = context.getContentResolver().query(ContactsContract.RawContacts.CONTENT_URI, new String[]{"account_type", "distinct account_name", "_id"}, null, null, "_id");
            } catch (Throwable th2) {
                cursor2 = cursor;
            }
        }
        if (cursor2 == null) {
            cursor3 = context.getContentResolver().query(ContactsContract.RawContacts.CONTENT_URI, new String[]{"account_type", "account_name", "_id"}, null, null, null);
        } else {
            cursor3 = cursor2;
        }
        if (cursor3 == null) {
            cVar.a(8);
            return null;
        }
        a aVar = new a();
        boolean moveToFirst = cursor3.moveToFirst();
        while (true) {
            if (!moveToFirst) {
                z = false;
                break;
            }
            String string = cursor3.getString(0);
            if (string == null) {
                aVar.b = null;
                aVar.f271a = null;
                moveToFirst = cursor3.moveToNext();
            } else if (string.equals("DeviceOnly")) {
                aVar.b = cursor3.getString(1);
                aVar.f271a = string;
                z = true;
            } else if (string.equals("com.sonyericsson.localcontacts")) {
                aVar.b = cursor3.getString(1);
                aVar.f271a = string;
                z = true;
            } else if (string.equals("com.local.contacts")) {
                aVar.b = cursor3.getString(1);
                aVar.f271a = string;
                z = true;
            } else {
                aVar.b = cursor3.getString(1);
                aVar.f271a = string;
                z = false;
            }
        }
        cursor3.close();
        if (!z) {
        }
        return aVar;
    }

    public static j a() {
        if (b != null) {
            return b;
        }
        b = new j();
        return b;
    }

    private int a(String str) {
        if (str == null) {
            return -1;
        }
        if (str.charAt(4) == '0') {
            return 0;
        }
        if (str.charAt(4) == '1') {
            return 1;
        }
        if (str.charAt(4) == '2') {
            return 2;
        }
        if (str.charAt(4) == '3') {
            return 3;
        }
        if (str.charAt(4) == '4') {
            return 4;
        }
        if (str.charAt(4) == '5') {
            return 5;
        }
        if (str.charAt(4) == '6') {
            return 6;
        }
        if (str.charAt(4) == '7') {
            return 7;
        }
        if (str.charAt(4) == '8') {
            return 8;
        }
        return -1;
    }

    private String a(int i) {
        if (i < 0 || i > 8) {
            return "pre:-1";
        }
        return "pre:" + i;
    }

    private void a(b bVar, ArrayList<byte[]> arrayList) {
        arrayList.add(r.a(bVar.f272a));
        arrayList.add(r.a(bVar.b));
        arrayList.add(r.a(bVar.c));
        arrayList.add(r.a(bVar.d));
        arrayList.add(r.a(bVar.e));
        arrayList.add(r.a(bVar.f));
        arrayList.add(r.a(bVar.g));
        arrayList.add(r.a(bVar.h));
        arrayList.add(r.a(bVar.i));
        arrayList.add(r.a(bVar.j));
        arrayList.add(r.a(bVar.l));
        arrayList.add(r.a(bVar.k));
        arrayList.add(r.a(bVar.m));
        arrayList.add(r.a(bVar.n));
        arrayList.add(r.a(bVar.p));
        arrayList.add(r.a(bVar.q));
        arrayList.add(r.a(bVar.o));
        if (bVar.t.size() < 0) {
            bVar.r = 0;
        } else {
            bVar.r = bVar.t.size();
        }
        arrayList.add(r.a(bVar.r));
        for (int i = 0; i < bVar.r; i++) {
            arrayList.add(r.a(bVar.t.get(i).f275a));
            arrayList.add(r.a(bVar.t.get(i).b));
            arrayList.add(r.a(bVar.t.get(i).c));
            arrayList.add(r.a(bVar.t.get(i).d));
        }
        if (bVar.w.size() < 0) {
            bVar.u = 0;
        } else {
            bVar.u = bVar.w.size();
        }
        arrayList.add(r.a(bVar.u));
        for (int i2 = 0; i2 < bVar.u; i2++) {
            arrayList.add(r.a(bVar.w.get(i2).f273a));
            arrayList.add(r.a(bVar.w.get(i2).b));
            arrayList.add(r.a(bVar.w.get(i2).c));
            arrayList.add(r.a(bVar.w.get(i2).d));
            arrayList.add(r.a(bVar.w.get(i2).e));
            arrayList.add(r.a(bVar.w.get(i2).f));
        }
        if (bVar.z.size() < 0) {
            bVar.u = 0;
        } else {
            bVar.x = bVar.z.size();
        }
        arrayList.add(r.a(bVar.x));
        for (int i3 = 0; i3 < bVar.x; i3++) {
            arrayList.add(r.a(bVar.z.get(i3).f274a));
            arrayList.add(r.a(bVar.z.get(i3).b));
            arrayList.add(r.a(bVar.z.get(i3).c));
            arrayList.add(r.a(bVar.z.get(i3).d));
            arrayList.add(r.a(bVar.z.get(i3).e));
        }
    }

    public void a(Context context, c cVar) {
        a ad = ad(context, cVar);
        ArrayList arrayList = new ArrayList();
        if (ad == null) {
            arrayList.add(r.a("com.qq.Contacts"));
            arrayList.add(r.a("com.qq.Contacts"));
        } else {
            arrayList.add(r.a(ad.f271a));
            arrayList.add(r.a(ad.b));
        }
        cVar.a(cVar.b());
        cVar.a(0);
    }

    public void b(Context context, c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        String j2 = cVar.j();
        if (!r.b(j) || !r.b(j2)) {
            this.d = j;
            this.e = j2;
            this.c = true;
            return;
        }
        this.d = null;
        this.e = null;
        this.c = true;
        cVar.a(0);
    }

    public void c(Context context, c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        String j = cVar.j();
        if (h <= 0 || r.b(j)) {
            cVar.a(1);
            return;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("raw_contact_id", Integer.valueOf(h));
        contentValues.put("data1", j);
        contentValues.put("mimetype", "vnd.android.cursor.item/note");
        if (context.getContentResolver().insert(ContactsContract.Data.CONTENT_URI, contentValues) != null) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }

    public void d(Context context, c cVar) {
        Cursor cursor;
        int i;
        boolean z;
        int i2;
        Account[] accounts;
        Cursor cursor2 = null;
        AccountManager accountManager = AccountManager.get(context);
        ArrayList arrayList = new ArrayList();
        if (!(accountManager == null || (accounts = accountManager.getAccounts()) == null)) {
            for (Account add : accounts) {
                arrayList.add(add);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        int size = arrayList.size();
        arrayList2.add(r.a(size));
        for (int i3 = 0; i3 < size; i3++) {
            Account account = (Account) arrayList.get(i3);
            arrayList2.add(r.a(account.name));
            arrayList2.add(r.a(account.type));
        }
        if (com.qq.util.j.c < 14) {
            try {
                cursor2 = context.getContentResolver().query(ContactsContract.Groups.CONTENT_URI, new String[]{"account_name", "account_type"}, "1) GROUP BY (account_name", null, null);
            } catch (Throwable th) {
            }
        }
        if (cursor2 == null) {
            try {
                cursor = context.getContentResolver().query(ContactsContract.Groups.CONTENT_URI, new String[]{"account_name", "account_type"}, null, null, null);
            } catch (Throwable th2) {
                cursor = cursor2;
            }
        } else {
            cursor = cursor2;
        }
        if (cursor != null) {
            i = size;
            for (boolean moveToFirst = cursor.moveToFirst(); moveToFirst; moveToFirst = cursor.moveToNext()) {
                String string = cursor.getString(0);
                String string2 = cursor.getString(1);
                int i4 = 0;
                while (true) {
                    if (i4 >= size) {
                        z = false;
                        break;
                    }
                    Account account2 = (Account) arrayList.get(i4);
                    if (account2.name.equals(string) && account2.type.equals(string2)) {
                        z = true;
                        break;
                    }
                    i4++;
                }
                if (!z) {
                    arrayList2.add(r.a(string));
                    arrayList2.add(r.a(string2));
                    i2 = i + 1;
                } else {
                    i2 = i;
                }
                i = i2;
            }
            cursor.close();
        } else {
            i = size;
        }
        arrayList2.set(0, r.a(i));
        cVar.a(arrayList2);
        cVar.a(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, boolean):void}
     arg types: [android.net.Uri, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, int):void}
      ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, boolean):void} */
    public void e(Context context, c cVar) {
        int i;
        if (cVar.e() < 11) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        String j2 = cVar.j();
        String j3 = cVar.j();
        String j4 = cVar.j();
        String j5 = cVar.j();
        int h = cVar.h();
        String j6 = cVar.j();
        String j7 = cVar.j();
        ContentValues contentValues = new ContentValues();
        if (h > 0) {
            i = 1;
        } else {
            i = 0;
        }
        contentValues.put("starred", Integer.valueOf(i));
        contentValues.put("version", (Integer) 1);
        contentValues.put("deleted", (Integer) 0);
        contentValues.put("aggregation_mode", (Integer) 2);
        if (!r.b(j7)) {
            contentValues.put("account_name", j7);
        } else {
            contentValues.putNull("account_name");
        }
        if (!r.b(j6)) {
            contentValues.put("account_type", j6);
        } else {
            contentValues.putNull("account_type");
        }
        Uri insert = context.getContentResolver().insert(ContactsContract.RawContacts.CONTENT_URI, contentValues);
        try {
            int parseInt = Integer.parseInt(insert.getLastPathSegment());
            contentValues.clear();
            int h2 = cVar.h();
            int h3 = cVar.h();
            int h4 = cVar.h();
            int i2 = 0;
            if (!r.b(j5)) {
                i2 = 1;
            }
            if (!r.b(j2)) {
                i2++;
            }
            if (!r.b(j) || !r.b(j3) || !r.b(j4)) {
                i2++;
            }
            ContentValues[] contentValuesArr = new ContentValues[(i2 + h2 + h3 + h4)];
            int i3 = 0;
            if (!r.b(j5)) {
                contentValuesArr[0] = new ContentValues();
                contentValuesArr[0].put("mimetype", "vnd.android.cursor.item/note");
                contentValuesArr[0].put("raw_contact_id", Integer.valueOf(parseInt));
                contentValuesArr[0].put("data1", j5);
                i3 = 1;
            }
            if (!r.b(j2)) {
                contentValuesArr[i3] = new ContentValues();
                contentValuesArr[i3].put("mimetype", "vnd.android.cursor.item/nickname");
                contentValuesArr[i3].put("raw_contact_id", Integer.valueOf(parseInt));
                contentValuesArr[i3].put("data1", j2);
                i3++;
            }
            if (!r.b(j) || !r.b(j3) || !r.b(j4)) {
                contentValuesArr[i3] = new ContentValues();
                contentValuesArr[i3].put("mimetype", "vnd.android.cursor.item/name");
                contentValuesArr[i3].put("raw_contact_id", Integer.valueOf(parseInt));
                contentValuesArr[i3].put("data1", j);
                contentValuesArr[i3].put("data2", j);
                i3++;
            }
            int i4 = i3;
            for (int i5 = 0; i5 < h2; i5++) {
                contentValuesArr[i4] = new ContentValues();
                int h5 = cVar.h();
                String j8 = cVar.j();
                String j9 = cVar.j();
                contentValuesArr[i4].put("raw_contact_id", Integer.valueOf(parseInt));
                contentValuesArr[i4].put("mimetype", "vnd.android.cursor.item/phone_v2");
                if (!r.b(j9)) {
                    contentValuesArr[i4].put("data1", j9);
                }
                contentValuesArr[i4].put("data2", Integer.valueOf(h5));
                if (h5 == 0 && !r.b(j8)) {
                    contentValuesArr[i4].put("data3", j8);
                }
                i4++;
            }
            for (int i6 = 0; i6 < h3; i6++) {
                int h6 = cVar.h();
                int h7 = cVar.h();
                String j10 = cVar.j();
                String j11 = cVar.j();
                String j12 = cVar.j();
                contentValuesArr[i4] = new ContentValues();
                if (h6 == 2) {
                    contentValuesArr[i4].put("mimetype", "vnd.android.cursor.item/postal-address_v2");
                    contentValuesArr[i4].put("raw_contact_id", Integer.valueOf(parseInt));
                    contentValuesArr[i4].put("data2", Integer.valueOf(h7));
                    if (h7 == 0 && !r.b(j10)) {
                        contentValuesArr[i4].put("data3", j10);
                    }
                    if (!r.b(j12)) {
                        contentValuesArr[i4].put("data1", j12);
                    }
                }
                if (h6 == 1) {
                    contentValuesArr[i4].put("mimetype", "vnd.android.cursor.item/email_v2");
                    contentValuesArr[i4].put("raw_contact_id", Integer.valueOf(parseInt));
                    contentValuesArr[i4].put("data2", Integer.valueOf(h7));
                    if (h7 == 0 && !r.b(j10)) {
                        contentValuesArr[i4].put("data3", j10);
                    }
                    if (!r.b(j12)) {
                        contentValuesArr[i4].put("data1", j12);
                    }
                }
                if (h6 == 3) {
                    contentValuesArr[i4].put("mimetype", "vnd.android.cursor.item/im");
                    contentValuesArr[i4].put("raw_contact_id", Integer.valueOf(parseInt));
                    contentValuesArr[i4].put("data2", Integer.valueOf(h7));
                    if (h7 == 0 && !r.b(j10)) {
                        contentValuesArr[i4].put("data3", j10);
                    }
                    int a2 = a(j11);
                    contentValuesArr[i4].put("data5", Integer.valueOf(a2));
                    if (a2 == -1 && !r.b(j10)) {
                        contentValuesArr[i4].put("data6", j10);
                    }
                    if (!r.b(j12)) {
                        contentValuesArr[i4].put("data1", j12);
                    }
                }
                i4++;
            }
            for (int i7 = 0; i7 < h4; i7++) {
                int h8 = cVar.h();
                String j13 = cVar.j();
                String j14 = cVar.j();
                String j15 = cVar.j();
                contentValuesArr[i4] = new ContentValues();
                contentValuesArr[i4].put("mimetype", "vnd.android.cursor.item/organization");
                contentValuesArr[i4].put("raw_contact_id", Integer.valueOf(parseInt));
                contentValuesArr[i4].put("data2", Integer.valueOf(h8));
                if (h8 == 0 && !r.b(j13)) {
                    contentValuesArr[i4].put("data3", j13);
                }
                if (!r.b(j14)) {
                    contentValuesArr[i4].put("data1", j14);
                }
                if (!r.b(j15)) {
                    contentValuesArr[i4].put("data4", j15);
                }
                i4++;
            }
            try {
                context.getContentResolver().bulkInsert(ContactsContract.Data.CONTENT_URI, contentValuesArr);
                context.getContentResolver().notifyChange(insert, (ContentObserver) null, false);
                for (int i8 = 0; i8 < contentValuesArr.length; i8++) {
                    contentValuesArr[i8].clear();
                    contentValuesArr[i8] = null;
                }
                ArrayList arrayList = new ArrayList();
                arrayList.add(r.a(parseInt));
                cVar.a(arrayList);
                cVar.a(0);
            } catch (Exception e2) {
                e2.printStackTrace();
                cVar.a(1);
            }
        } catch (Exception e3) {
            e3.printStackTrace();
            cVar.a(8);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, boolean):void}
     arg types: [android.net.Uri, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, int):void}
      ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, boolean):void} */
    public void f(Context context, c cVar) {
        int i;
        if (cVar.e() < 9) {
            cVar.a(1);
            return;
        }
        a(context);
        String j = cVar.j();
        String j2 = cVar.j();
        String j3 = cVar.j();
        String j4 = cVar.j();
        String j5 = cVar.j();
        int h = cVar.h();
        ContentValues contentValues = new ContentValues();
        if (h > 0) {
            i = 1;
        } else {
            i = 0;
        }
        contentValues.put("starred", Integer.valueOf(i));
        contentValues.put("version", (Integer) 1);
        contentValues.put("deleted", (Integer) 0);
        contentValues.put("aggregation_mode", (Integer) 3);
        if (!r.b(this.d)) {
            contentValues.put("account_name", this.d);
        }
        if (!r.b(this.e)) {
            contentValues.put("account_type", this.e);
        }
        Uri insert = context.getContentResolver().insert(ContactsContract.RawContacts.CONTENT_URI, contentValues);
        try {
            int parseInt = Integer.parseInt(insert.getLastPathSegment());
            contentValues.clear();
            int h2 = cVar.h();
            int h3 = cVar.h();
            int h4 = cVar.h();
            int i2 = 0;
            if (!r.b(j5)) {
                i2 = 1;
            }
            if (!r.b(j2)) {
                i2++;
            }
            if (!r.b(j) || !r.b(j3) || !r.b(j4)) {
                i2++;
            }
            ContentValues[] contentValuesArr = new ContentValues[(i2 + h2 + h3 + h4)];
            int i3 = 0;
            if (!r.b(j5)) {
                contentValuesArr[0] = new ContentValues();
                contentValuesArr[0].put("mimetype", "vnd.android.cursor.item/note");
                contentValuesArr[0].put("raw_contact_id", Integer.valueOf(parseInt));
                contentValuesArr[0].put("data1", j5);
                i3 = 1;
            }
            if (!r.b(j2)) {
                contentValuesArr[i3] = new ContentValues();
                contentValuesArr[i3].put("mimetype", "vnd.android.cursor.item/nickname");
                contentValuesArr[i3].put("raw_contact_id", Integer.valueOf(parseInt));
                contentValuesArr[i3].put("data1", j2);
                i3++;
            }
            if (!r.b(j) || !r.b(j3) || !r.b(j4)) {
                contentValuesArr[i3] = new ContentValues();
                contentValuesArr[i3].put("mimetype", "vnd.android.cursor.item/name");
                contentValuesArr[i3].put("raw_contact_id", Integer.valueOf(parseInt));
                contentValuesArr[i3].put("data1", j);
                contentValuesArr[i3].put("data2", j);
                i3++;
            }
            int i4 = i3;
            for (int i5 = 0; i5 < h2; i5++) {
                contentValuesArr[i4] = new ContentValues();
                int h5 = cVar.h();
                String j6 = cVar.j();
                String j7 = cVar.j();
                contentValuesArr[i4].put("raw_contact_id", Integer.valueOf(parseInt));
                contentValuesArr[i4].put("mimetype", "vnd.android.cursor.item/phone_v2");
                if (!r.b(j7)) {
                    contentValuesArr[i4].put("data1", j7);
                }
                contentValuesArr[i4].put("data2", Integer.valueOf(h5));
                if (h5 == 0 && !r.b(j6)) {
                    contentValuesArr[i4].put("data3", j6);
                }
                i4++;
            }
            for (int i6 = 0; i6 < h3; i6++) {
                int h6 = cVar.h();
                int h7 = cVar.h();
                String j8 = cVar.j();
                String j9 = cVar.j();
                String j10 = cVar.j();
                contentValuesArr[i4] = new ContentValues();
                if (h6 == 2) {
                    contentValuesArr[i4].put("mimetype", "vnd.android.cursor.item/postal-address_v2");
                    contentValuesArr[i4].put("raw_contact_id", Integer.valueOf(parseInt));
                    contentValuesArr[i4].put("data2", Integer.valueOf(h7));
                    if (h7 == 0 && !r.b(j8)) {
                        contentValuesArr[i4].put("data3", j8);
                    }
                    if (!r.b(j10)) {
                        contentValuesArr[i4].put("data1", j10);
                    }
                }
                if (h6 == 1) {
                    contentValuesArr[i4].put("mimetype", "vnd.android.cursor.item/email_v2");
                    contentValuesArr[i4].put("raw_contact_id", Integer.valueOf(parseInt));
                    contentValuesArr[i4].put("data2", Integer.valueOf(h7));
                    if (h7 == 0 && !r.b(j8)) {
                        contentValuesArr[i4].put("data3", j8);
                    }
                    if (!r.b(j10)) {
                        contentValuesArr[i4].put("data1", j10);
                    }
                }
                if (h6 == 3) {
                    contentValuesArr[i4].put("mimetype", "vnd.android.cursor.item/im");
                    contentValuesArr[i4].put("raw_contact_id", Integer.valueOf(parseInt));
                    contentValuesArr[i4].put("data2", Integer.valueOf(h7));
                    if (h7 == 0 && !r.b(j8)) {
                        contentValuesArr[i4].put("data3", j8);
                    }
                    int a2 = a(j9);
                    contentValuesArr[i4].put("data5", Integer.valueOf(a2));
                    if (a2 == -1 && !r.b(j8)) {
                        contentValuesArr[i4].put("data6", j8);
                    }
                    if (!r.b(j10)) {
                        contentValuesArr[i4].put("data1", j10);
                    }
                }
                i4++;
            }
            for (int i7 = 0; i7 < h4; i7++) {
                int h8 = cVar.h();
                String j11 = cVar.j();
                String j12 = cVar.j();
                String j13 = cVar.j();
                contentValuesArr[i4] = new ContentValues();
                contentValuesArr[i4].put("mimetype", "vnd.android.cursor.item/organization");
                contentValuesArr[i4].put("raw_contact_id", Integer.valueOf(parseInt));
                contentValuesArr[i4].put("data2", Integer.valueOf(h8));
                if (h8 == 0 && !r.b(j11)) {
                    contentValuesArr[i4].put("data3", j11);
                }
                if (!r.b(j12)) {
                    contentValuesArr[i4].put("data1", j12);
                }
                if (!r.b(j13)) {
                    contentValuesArr[i4].put("data4", j13);
                }
                i4++;
            }
            try {
                context.getContentResolver().bulkInsert(ContactsContract.Data.CONTENT_URI, contentValuesArr);
                context.getContentResolver().notifyChange(insert, (ContentObserver) null, false);
                for (int i8 = 0; i8 < contentValuesArr.length; i8++) {
                    contentValuesArr[i8].clear();
                    contentValuesArr[i8] = null;
                }
                ArrayList arrayList = new ArrayList();
                arrayList.add(r.a(parseInt));
                cVar.a(arrayList);
                cVar.a(0);
            } catch (Exception e2) {
                e2.printStackTrace();
                cVar.a(1);
            }
        } catch (Exception e3) {
            e3.printStackTrace();
            cVar.a(8);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void g(Context context, c cVar) {
        int i;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (h <= 0) {
            cVar.a(1);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(h));
        ContentValues contentValues = new ContentValues();
        for (int i2 = 0; i2 < h; i2++) {
            String j = cVar.j();
            cVar.g();
            String j2 = cVar.j();
            String j3 = cVar.j();
            String j4 = cVar.j();
            int h2 = cVar.h();
            String j5 = cVar.j();
            String j6 = cVar.j();
            if (h2 > 0) {
                contentValues.put("starred", (Integer) 1);
            }
            contentValues.put("deleted", (Integer) 0);
            contentValues.put("aggregation_mode", (Integer) 2);
            if (!r.b(j6)) {
                contentValues.put("account_name", j6);
            } else {
                contentValues.putNull("account_name");
            }
            if (!r.b(j5)) {
                contentValues.put("account_type", j5);
            } else {
                contentValues.putNull("account_type");
            }
            try {
                i = Integer.parseInt(context.getContentResolver().insert(ContactsContract.RawContacts.CONTENT_URI, contentValues).getLastPathSegment());
            } catch (Exception e2) {
                i = 0;
            }
            arrayList.add(r.a(i));
            contentValues.clear();
            int h3 = cVar.h();
            int h4 = cVar.h();
            int h5 = cVar.h();
            int i3 = 0;
            if (!r.b(j4)) {
                i3 = 1;
            }
            if (!r.b(j) || !r.b(j2) || !r.b(j3)) {
                i3++;
            }
            int i4 = i3 + h3 + h4 + h5;
            int i5 = 0;
            ContentValues[] contentValuesArr = new ContentValues[i4];
            if (!r.b(j4)) {
                contentValuesArr[0] = new ContentValues();
                contentValuesArr[0].put("mimetype", "vnd.android.cursor.item/note");
                contentValuesArr[0].put("raw_contact_id", Integer.valueOf(i));
                contentValuesArr[0].put("data1", j4);
                i5 = 1;
            }
            if (!r.b(j)) {
                contentValuesArr[i5] = new ContentValues();
                contentValuesArr[i5].put("mimetype", "vnd.android.cursor.item/name");
                contentValuesArr[i5].put("raw_contact_id", Integer.valueOf(i));
                contentValuesArr[i5].put("data1", j);
                contentValuesArr[i5].put("data2", j);
                i5++;
            }
            int i6 = i5;
            for (int i7 = 0; i7 < h3; i7++) {
                contentValuesArr[i6] = new ContentValues();
                int h6 = cVar.h();
                String j7 = cVar.j();
                String j8 = cVar.j();
                contentValuesArr[i6].put("raw_contact_id", Integer.valueOf(i));
                contentValuesArr[i6].put("mimetype", "vnd.android.cursor.item/phone_v2");
                if (!r.b(j8)) {
                    contentValuesArr[i6].put("data1", j8);
                }
                contentValuesArr[i6].put("data2", Integer.valueOf(h6));
                if (h6 == 0 && !r.b(j7)) {
                    contentValuesArr[i6].put("data3", j7);
                }
                i6++;
            }
            for (int i8 = 0; i8 < h4; i8++) {
                int h7 = cVar.h();
                int h8 = cVar.h();
                String j9 = cVar.j();
                String j10 = cVar.j();
                String j11 = cVar.j();
                contentValuesArr[i6] = new ContentValues();
                if (h7 == 2) {
                    contentValuesArr[i6].put("mimetype", "vnd.android.cursor.item/postal-address_v2");
                    contentValuesArr[i6].put("raw_contact_id", Integer.valueOf(i));
                    contentValuesArr[i6].put("data2", Integer.valueOf(h8));
                    if (h8 == 0 && !r.b(j9)) {
                        contentValuesArr[i6].put("data3", j9);
                    }
                    if (!r.b(j11)) {
                        contentValuesArr[i6].put("data1", j11);
                    }
                }
                if (h7 == 1) {
                    contentValuesArr[i6].put("mimetype", "vnd.android.cursor.item/email_v2");
                    contentValuesArr[i6].put("raw_contact_id", Integer.valueOf(i));
                    contentValuesArr[i6].put("data2", Integer.valueOf(h8));
                    if (h8 == 0 && !r.b(j9)) {
                        contentValuesArr[i6].put("data3", j9);
                    }
                    if (!r.b(j11)) {
                        contentValuesArr[i6].put("data1", j11);
                    }
                }
                if (h7 == 3) {
                    contentValuesArr[i6].put("mimetype", "vnd.android.cursor.item/im");
                    contentValuesArr[i6].put("raw_contact_id", Integer.valueOf(i));
                    contentValuesArr[i6].put("data2", Integer.valueOf(h8));
                    if (h8 == 0 && !r.b(j9)) {
                        contentValuesArr[i6].put("data3", j9);
                    }
                    int a2 = a(j10);
                    contentValuesArr[i6].put("data5", Integer.valueOf(a2));
                    if (a2 == -1 && !r.b(j9)) {
                        contentValuesArr[i6].put("data6", j9);
                    }
                    if (!r.b(j11)) {
                        contentValuesArr[i6].put("data1", j11);
                    }
                }
                i6++;
            }
            for (int i9 = 0; i9 < h5; i9++) {
                int h9 = cVar.h();
                String j12 = cVar.j();
                String j13 = cVar.j();
                String j14 = cVar.j();
                contentValuesArr[i6] = new ContentValues();
                contentValuesArr[i6].put("mimetype", "vnd.android.cursor.item/organization");
                contentValuesArr[i6].put("raw_contact_id", Integer.valueOf(i));
                contentValuesArr[i6].put("data2", Integer.valueOf(h9));
                if (h9 == 0 && !r.b(j12)) {
                    contentValuesArr[i6].put("data3", j12);
                }
                if (!r.b(j13)) {
                    contentValuesArr[i6].put("data1", j13);
                }
                if (!r.b(j14)) {
                    contentValuesArr[i6].put("data4", j14);
                }
                i6++;
            }
            if (i > 0) {
                try {
                    context.getContentResolver().bulkInsert(ContactsContract.Data.CONTENT_URI, contentValuesArr);
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            }
        }
        cVar.a(arrayList);
        cVar.a(0);
        System.gc();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void h(Context context, c cVar) {
        int i;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (h <= 0) {
            cVar.a(1);
            return;
        }
        a(context);
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(h));
        ContentValues contentValues = new ContentValues();
        for (int i2 = 0; i2 < h; i2++) {
            String j = cVar.j();
            cVar.g();
            String j2 = cVar.j();
            String j3 = cVar.j();
            String j4 = cVar.j();
            if (cVar.h() > 0) {
                contentValues.put("starred", (Integer) 1);
            }
            contentValues.put("deleted", (Integer) 0);
            contentValues.put("aggregation_mode", (Integer) 2);
            if (!r.b(this.d)) {
                contentValues.put("account_name", this.d);
            }
            if (!r.b(this.e)) {
                contentValues.put("account_type", this.e);
            }
            try {
                i = Integer.parseInt(context.getContentResolver().insert(ContactsContract.RawContacts.CONTENT_URI, contentValues).getLastPathSegment());
            } catch (Exception e2) {
                i = 0;
            }
            arrayList.add(r.a(i));
            contentValues.clear();
            int h2 = cVar.h();
            int h3 = cVar.h();
            int h4 = cVar.h();
            int i3 = 0;
            if (!r.b(j4)) {
                i3 = 1;
            }
            if (!r.b(j) || !r.b(j2) || !r.b(j3)) {
                i3++;
            }
            int i4 = i3 + h2 + h3 + h4;
            int i5 = 0;
            ContentValues[] contentValuesArr = new ContentValues[i4];
            if (!r.b(j4)) {
                contentValuesArr[0] = new ContentValues();
                contentValuesArr[0].put("mimetype", "vnd.android.cursor.item/note");
                contentValuesArr[0].put("raw_contact_id", Integer.valueOf(i));
                contentValuesArr[0].put("data1", j4);
                i5 = 1;
            }
            if (!r.b(j)) {
                contentValuesArr[i5] = new ContentValues();
                contentValuesArr[i5].put("mimetype", "vnd.android.cursor.item/name");
                contentValuesArr[i5].put("raw_contact_id", Integer.valueOf(i));
                contentValuesArr[i5].put("data1", j);
                if (j2 != null) {
                    j = j2;
                }
                contentValuesArr[i5].put("data3", j);
                i5++;
            }
            int i6 = i5;
            for (int i7 = 0; i7 < h2; i7++) {
                contentValuesArr[i6] = new ContentValues();
                int h5 = cVar.h();
                String j5 = cVar.j();
                String j6 = cVar.j();
                contentValuesArr[i6].put("raw_contact_id", Integer.valueOf(i));
                contentValuesArr[i6].put("mimetype", "vnd.android.cursor.item/phone_v2");
                if (!r.b(j6)) {
                    contentValuesArr[i6].put("data1", j6);
                }
                contentValuesArr[i6].put("data2", Integer.valueOf(h5));
                if (h5 == 0 && !r.b(j5)) {
                    contentValuesArr[i6].put("data3", j5);
                }
                i6++;
            }
            for (int i8 = 0; i8 < h3; i8++) {
                int h6 = cVar.h();
                int h7 = cVar.h();
                String j7 = cVar.j();
                String j8 = cVar.j();
                String j9 = cVar.j();
                contentValuesArr[i6] = new ContentValues();
                if (h6 == 2) {
                    contentValuesArr[i6].put("mimetype", "vnd.android.cursor.item/postal-address_v2");
                    contentValuesArr[i6].put("raw_contact_id", Integer.valueOf(i));
                    contentValuesArr[i6].put("data2", Integer.valueOf(h7));
                    if (h7 == 0 && !r.b(j7)) {
                        contentValuesArr[i6].put("data3", j7);
                    }
                    if (!r.b(j9)) {
                        contentValuesArr[i6].put("data1", j9);
                    }
                }
                if (h6 == 1) {
                    contentValuesArr[i6].put("mimetype", "vnd.android.cursor.item/email_v2");
                    contentValuesArr[i6].put("raw_contact_id", Integer.valueOf(i));
                    contentValuesArr[i6].put("data2", Integer.valueOf(h7));
                    if (h7 == 0 && !r.b(j7)) {
                        contentValuesArr[i6].put("data3", j7);
                    }
                    if (!r.b(j9)) {
                        contentValuesArr[i6].put("data1", j9);
                    }
                }
                if (h6 == 3) {
                    contentValuesArr[i6].put("mimetype", "vnd.android.cursor.item/im");
                    contentValuesArr[i6].put("raw_contact_id", Integer.valueOf(i));
                    contentValuesArr[i6].put("data2", Integer.valueOf(h7));
                    if (h7 == 0 && !r.b(j7)) {
                        contentValuesArr[i6].put("data3", j7);
                    }
                    int a2 = a(j8);
                    contentValuesArr[i6].put("data5", Integer.valueOf(a2));
                    if (a2 == -1 && !r.b(j7)) {
                        contentValuesArr[i6].put("data6", j7);
                    }
                    if (!r.b(j9)) {
                        contentValuesArr[i6].put("data1", j9);
                    }
                }
                i6++;
            }
            for (int i9 = 0; i9 < h4; i9++) {
                int h8 = cVar.h();
                String j10 = cVar.j();
                String j11 = cVar.j();
                String j12 = cVar.j();
                contentValuesArr[i6] = new ContentValues();
                contentValuesArr[i6].put("mimetype", "vnd.android.cursor.item/organization");
                contentValuesArr[i6].put("raw_contact_id", Integer.valueOf(i));
                contentValuesArr[i6].put("data2", Integer.valueOf(h8));
                if (h8 == 0 && !r.b(j10)) {
                    contentValuesArr[i6].put("data3", j10);
                }
                if (!r.b(j11)) {
                    contentValuesArr[i6].put("data1", j11);
                }
                if (!r.b(j12)) {
                    contentValuesArr[i6].put("data4", j12);
                }
                i6++;
            }
            if (i > 0) {
                try {
                    context.getContentResolver().bulkInsert(ContactsContract.Data.CONTENT_URI, contentValuesArr);
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            }
        }
        cVar.a(arrayList);
        cVar.a(0);
        System.gc();
    }

    public void i(Context context, c cVar) {
        int i;
        int i2;
        String str;
        String str2;
        int i3 = 1;
        if (cVar.e() < 6) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        String j2 = cVar.j();
        String j3 = cVar.j();
        String j4 = cVar.j();
        String j5 = cVar.j();
        int h = cVar.h();
        ContentValues contentValues = new ContentValues();
        if (h > 0) {
            i = 1;
        } else {
            i = 0;
        }
        contentValues.put("starred", Integer.valueOf(i));
        try {
            int parseInt = Integer.parseInt(context.getContentResolver().insert(ContactsContract.RawContacts.CONTENT_URI, contentValues).getLastPathSegment());
            if (!r.b(j5)) {
                i2 = 1;
            } else {
                i2 = 0;
            }
            if (!r.b(j2)) {
                i2++;
            }
            if (!r.b(j) || !r.b(j3) || !r.b(j4)) {
                i2++;
            }
            ContentValues[] contentValuesArr = new ContentValues[i2];
            if (!r.b(j5)) {
                contentValuesArr[0] = new ContentValues();
                contentValuesArr[0].put("mimetype", "vnd.android.cursor.item/note");
                contentValuesArr[0].put("raw_contact_id", Integer.valueOf(parseInt));
                contentValuesArr[0].put("data1", j5);
            } else {
                i3 = 0;
            }
            if (!r.b(j2)) {
                contentValuesArr[i3] = new ContentValues();
                contentValuesArr[i3].put("mimetype", "vnd.android.cursor.item/nickname");
                contentValuesArr[i3].put("raw_contact_id", Integer.valueOf(parseInt));
                contentValuesArr[i3].put("data1", j2);
                i3++;
            }
            if (!r.b(j) || !r.b(j3) || !r.b(j4)) {
                contentValuesArr[i3] = new ContentValues();
                contentValuesArr[i3].put("mimetype", "vnd.android.cursor.item/name");
                contentValuesArr[i3].put("raw_contact_id", Integer.valueOf(parseInt));
                if (!r.b(j3) || !r.b(j4)) {
                    if (!r.b(j3)) {
                        str = Constants.STR_EMPTY;
                    } else {
                        str = j3;
                    }
                    if (!r.b(j4)) {
                        str2 = str + j4;
                    } else {
                        str2 = str;
                    }
                    contentValuesArr[i3].put("data1", str2);
                    contentValuesArr[i3].put("data2", str);
                    if (!r.b(j4)) {
                        contentValuesArr[i3].put("data3", j4);
                    }
                } else {
                    contentValuesArr[i3].put("data1", j);
                    contentValuesArr[i3].put("data2", j);
                }
            }
            context.getContentResolver().bulkInsert(ContactsContract.Data.CONTENT_URI, contentValuesArr);
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(parseInt));
            cVar.a(arrayList);
            cVar.a(0);
        } catch (Exception e2) {
            e2.printStackTrace();
            cVar.a(8);
        }
    }

    public void j(Context context, c cVar) {
        int i;
        if (cVar.e() < 4) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        String j = cVar.j();
        String j2 = cVar.j();
        if (r.b(j2) || h < 0) {
            cVar.a(1);
            return;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("raw_contact_id", Integer.valueOf(h));
        contentValues.put("data2", Integer.valueOf(h2));
        if (h2 == 0 && !r.b(j)) {
            contentValues.put("data3", j);
        }
        contentValues.put("data1", j2);
        contentValues.put("mimetype", "vnd.android.cursor.item/phone_v2");
        Uri insert = context.getContentResolver().insert(ContactsContract.Data.CONTENT_URI, contentValues);
        if (insert == null) {
            cVar.a(1);
            return;
        }
        try {
            i = Integer.parseInt(insert.getLastPathSegment());
        } catch (Exception e2) {
            i = 0;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(i));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void k(Context context, c cVar) {
        int i;
        if (cVar.e() < 6) {
            cVar.a(0);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        int h3 = cVar.h();
        String j = cVar.j();
        String j2 = cVar.j();
        String j3 = cVar.j();
        ContentValues contentValues = new ContentValues();
        if (h2 == 1) {
            contentValues.put("mimetype", "vnd.android.cursor.item/email_v2");
            contentValues.put("raw_contact_id", Integer.valueOf(h));
            contentValues.put("data2", Integer.valueOf(h3));
            if (h3 == 0 && !r.b(j)) {
                contentValues.put("data3", j);
            }
            if (!r.b(j3)) {
                contentValues.put("data1", j3);
            }
        }
        if (h2 == 2) {
            contentValues.put("mimetype", "vnd.android.cursor.item/postal-address_v2");
            contentValues.put("raw_contact_id", Integer.valueOf(h));
            contentValues.put("data2", Integer.valueOf(h3));
            if (h3 == 0 && !r.b(j)) {
                contentValues.put("data3", j);
            }
            if (!r.b(j3)) {
                contentValues.put("data1", j3);
            }
        }
        if (h2 == 3) {
            contentValues.put("mimetype", "vnd.android.cursor.item/im");
            contentValues.put("raw_contact_id", Integer.valueOf(h));
            contentValues.put("data2", Integer.valueOf(h3));
            if (h3 == 0 && !r.b(j)) {
                contentValues.put("data3", j);
            }
            if (!r.b(j3)) {
                contentValues.put("data1", j3);
            }
            int a2 = a(j2);
            contentValues.put("data5", Integer.valueOf(a2));
            if (a2 == -1 && !r.b(j)) {
                contentValues.put("data6", j);
            }
        }
        Uri insert = context.getContentResolver().insert(ContactsContract.Data.CONTENT_URI, contentValues);
        if (insert == null) {
            cVar.a(1);
            return;
        }
        try {
            i = Integer.parseInt(insert.getLastPathSegment());
        } catch (Exception e2) {
            i = 0;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(i));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void l(Context context, c cVar) {
        int i;
        if (cVar.e() < 5) {
            cVar.a(0);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        String j = cVar.j();
        String j2 = cVar.j();
        String j3 = cVar.j();
        ContentValues contentValues = new ContentValues();
        contentValues.put("mimetype", "vnd.android.cursor.item/organization");
        contentValues.put("raw_contact_id", Integer.valueOf(h));
        contentValues.put("data2", Integer.valueOf(h2));
        if (h2 == 0 && !r.b(j)) {
            contentValues.put("data3", j);
        }
        if (!r.b(j2)) {
            contentValues.put("data1", j2);
        }
        if (!r.b(j3)) {
            contentValues.put("data4", j3);
        }
        Uri insert = context.getContentResolver().insert(ContactsContract.Data.CONTENT_URI, contentValues);
        if (insert == null) {
            cVar.a(1);
            return;
        }
        try {
            i = Integer.parseInt(insert.getLastPathSegment());
        } catch (Exception e2) {
            i = 0;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(i));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void m(Context context, c cVar) {
        byte[] blob;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        ContentResolver contentResolver = context.getContentResolver();
        int h = cVar.h();
        b bVar = new b();
        Cursor query = contentResolver.query(Uri.withAppendedPath(ContactsContract.RawContacts.CONTENT_URI, Constants.STR_EMPTY + h), null, "deleted = 0", null, null);
        if (query == null || !query.moveToFirst()) {
            if (query != null) {
                query.close();
            }
            cVar.a(1);
            return;
        }
        bVar.f272a = h;
        bVar.g = query.getInt(query.getColumnIndex("starred"));
        if (bVar.g > 0) {
            bVar.g = 1;
        } else {
            bVar.g = 0;
        }
        String string = query.getString(query.getColumnIndex("custom_ringtone"));
        if (string == null) {
            bVar.h = 0;
        } else {
            Uri parse = Uri.parse(string);
            if (parse == null) {
                bVar.h = 0;
            }
            String lastPathSegment = parse.getLastPathSegment();
            if (lastPathSegment == null) {
                bVar.h = 0;
            } else {
                try {
                    bVar.h = Integer.parseInt(lastPathSegment);
                } catch (Exception e2) {
                    Log.d("parse ringtone uri failure", parse.toString());
                    bVar.h = 0;
                }
                if (bVar.h != 0 && string.contains("/external/")) {
                    bVar.i = 1;
                }
            }
        }
        bVar.m = query.getInt(query.getColumnIndex("times_contacted"));
        bVar.n = query.getInt(query.getColumnIndex("last_time_contacted"));
        bVar.p = query.getString(query.getColumnIndex("account_name"));
        bVar.q = query.getString(query.getColumnIndex("account_type"));
        query.close();
        Cursor query2 = contentResolver.query(ContactsContract.Data.CONTENT_URI, null, "raw_contact_id = " + h, null, null);
        if (query2 == null) {
            cVar.a(1);
            return;
        }
        boolean moveToFirst = query2.moveToFirst();
        while (moveToFirst) {
            String string2 = query2.getString(query2.getColumnIndex("mimetype"));
            if (string2 == null) {
                moveToFirst = query2.moveToNext();
            } else {
                if (string2.equals("vnd.android.cursor.item/nickname")) {
                    bVar.c = query2.getString(query2.getColumnIndex("data1"));
                }
                if (string2.equals("vnd.android.cursor.item/name")) {
                    bVar.b = query2.getString(query2.getColumnIndex("data1"));
                    bVar.d = query2.getString(query2.getColumnIndex("data2"));
                    bVar.e = query2.getString(query2.getColumnIndex("data3"));
                }
                if (string2.equals("vnd.android.cursor.item/note")) {
                    bVar.f = query2.getString(query2.getColumnIndex("data1"));
                }
                if (string2.equals("vnd.android.cursor.item/photo") && (blob = query2.getBlob(query2.getColumnIndex("data15"))) != null) {
                    bVar.o = blob.length;
                }
                if (string2.equals("vnd.android.cursor.item/phone_v2")) {
                    e eVar = new e();
                    eVar.f275a = query2.getInt(query2.getColumnIndex("_id"));
                    if (query2.getInt(query2.getColumnIndex("is_primary")) > 0) {
                        bVar.j = eVar.f275a;
                    }
                    eVar.b = query2.getInt(query2.getColumnIndex("data2"));
                    eVar.c = query2.getString(query2.getColumnIndex("data3"));
                    eVar.d = query2.getString(query2.getColumnIndex("data1"));
                    bVar.t.add(eVar);
                }
                if (string2.equals("vnd.android.cursor.item/postal-address_v2")) {
                    com.qq.d.b.c cVar2 = new com.qq.d.b.c();
                    cVar2.f273a = query2.getInt(query2.getColumnIndex("_id"));
                    if (query2.getInt(query2.getColumnIndex("is_primary")) > 0) {
                        bVar.l = cVar2.f273a;
                    }
                    cVar2.c = query2.getInt(query2.getColumnIndex("data2"));
                    cVar2.d = query2.getString(query2.getColumnIndex("data3"));
                    cVar2.b = 2;
                    cVar2.f = query2.getString(query2.getColumnIndex("data1"));
                    bVar.w.add(cVar2);
                }
                if (string2.equals("vnd.android.cursor.item/email_v2")) {
                    com.qq.d.b.c cVar3 = new com.qq.d.b.c();
                    cVar3.f273a = query2.getInt(query2.getColumnIndex("_id"));
                    if (query2.getInt(query2.getColumnIndex("is_primary")) > 0) {
                        bVar.l = cVar3.f273a;
                    }
                    cVar3.c = query2.getInt(query2.getColumnIndex("data2"));
                    cVar3.d = query2.getString(query2.getColumnIndex("data3"));
                    cVar3.b = 1;
                    cVar3.f = query2.getString(query2.getColumnIndex("data1"));
                    bVar.w.add(cVar3);
                }
                if (string2.equals("vnd.android.cursor.item/im")) {
                    com.qq.d.b.c cVar4 = new com.qq.d.b.c();
                    cVar4.f273a = query2.getInt(query2.getColumnIndex("_id"));
                    if (query2.getInt(query2.getColumnIndex("is_primary")) > 0) {
                        bVar.l = cVar4.f273a;
                    }
                    cVar4.c = query2.getInt(query2.getColumnIndex("data2"));
                    cVar4.d = query2.getString(query2.getColumnIndex("data3"));
                    cVar4.b = 3;
                    cVar4.e = a(query2.getInt(query2.getColumnIndex("data5")));
                    cVar4.f = query2.getString(query2.getColumnIndex("data1"));
                    bVar.w.add(cVar4);
                }
                if (string2.equals("vnd.android.cursor.item/organization")) {
                    d dVar = new d();
                    dVar.f274a = query2.getInt(query2.getColumnIndex("_id"));
                    if (query2.getInt(query2.getColumnIndex("is_primary")) > 0) {
                        bVar.k = dVar.f274a;
                    }
                    dVar.b = query2.getInt(query2.getColumnIndex("data2"));
                    dVar.c = query2.getString(query2.getColumnIndex("data3"));
                    dVar.d = query2.getString(query2.getColumnIndex("data1"));
                    dVar.e = query2.getString(query2.getColumnIndex("data4"));
                    bVar.z.add(dVar);
                }
                moveToFirst = query2.moveToNext();
            }
        }
        query2.close();
        ArrayList arrayList = new ArrayList();
        a(bVar, arrayList);
        cVar.a(arrayList);
        cVar.a(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, boolean):void}
     arg types: [android.net.Uri, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, int):void}
      ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, boolean):void} */
    public void n(Context context, c cVar) {
        if (cVar.e() < 12) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        String j = cVar.j();
        String j2 = cVar.j();
        String j3 = cVar.j();
        String j4 = cVar.j();
        String j5 = cVar.j();
        int h2 = cVar.h();
        int h3 = cVar.h();
        int h4 = cVar.h();
        if (h <= 0) {
            cVar.a(1);
            return;
        }
        Uri withAppendedPath = Uri.withAppendedPath(ContactsContract.RawContacts.CONTENT_URI, Constants.STR_EMPTY + h);
        ContentValues contentValues = new ContentValues();
        contentValues.put("starred", Integer.valueOf(h2));
        contentValues.put("times_contacted", Integer.valueOf(h3));
        contentValues.put("last_time_contacted", Integer.valueOf(h4));
        if (context.getContentResolver().update(withAppendedPath, contentValues, null, null) < 1) {
            cVar.a(1);
            return;
        }
        context.getContentResolver().delete(ContactsContract.Data.CONTENT_URI, "raw_contact_id = " + h + " and " + "mimetype" + " <> '" + "vnd.android.cursor.item/photo" + "'", null);
        int h5 = cVar.h();
        int h6 = cVar.h();
        int h7 = cVar.h();
        int i = 0;
        if (!r.b(j5)) {
            i = 1;
        }
        if (!r.b(j2)) {
            i++;
        }
        if (!r.b(j)) {
            i++;
        }
        ContentValues[] contentValuesArr = new ContentValues[(i + h5 + h6 + h7)];
        int i2 = 0;
        if (!r.b(j5)) {
            contentValuesArr[0] = new ContentValues();
            contentValuesArr[0].put("mimetype", "vnd.android.cursor.item/note");
            contentValuesArr[0].put("raw_contact_id", Integer.valueOf(h));
            contentValuesArr[0].put("data1", j5);
            i2 = 1;
        }
        if (!r.b(j2)) {
            contentValuesArr[i2] = new ContentValues();
            contentValuesArr[i2].put("mimetype", "vnd.android.cursor.item/nickname");
            contentValuesArr[i2].put("raw_contact_id", Integer.valueOf(h));
            contentValuesArr[i2].put("data1", j2);
            i2++;
        }
        if (!r.b(j)) {
            contentValuesArr[i2] = new ContentValues();
            contentValuesArr[i2].put("mimetype", "vnd.android.cursor.item/name");
            contentValuesArr[i2].put("raw_contact_id", Integer.valueOf(h));
            if (!r.b(j)) {
                contentValuesArr[i2].put("data1", j);
            }
            if (!r.b(j3)) {
                contentValuesArr[i2].put("data2", j3);
                if (!r.b(j4)) {
                    contentValuesArr[i2].put("data3", j4);
                }
            } else {
                contentValuesArr[i2].put("data2", j);
            }
            i2++;
        }
        int i3 = i2;
        for (int i4 = 0; i4 < h5; i4++) {
            contentValuesArr[i3] = new ContentValues();
            cVar.g();
            int h8 = cVar.h();
            String j6 = cVar.j();
            String j7 = cVar.j();
            contentValuesArr[i3].put("raw_contact_id", Integer.valueOf(h));
            contentValuesArr[i3].put("mimetype", "vnd.android.cursor.item/phone_v2");
            if (!r.b(j7)) {
                contentValuesArr[i3].put("data1", j7);
            }
            contentValuesArr[i3].put("data2", Integer.valueOf(h8));
            if (h8 == 0 && !r.b(j6)) {
                contentValuesArr[i3].put("data3", j6);
            }
            i3++;
        }
        for (int i5 = 0; i5 < h6; i5++) {
            cVar.g();
            int h9 = cVar.h();
            int h10 = cVar.h();
            String j8 = cVar.j();
            String j9 = cVar.j();
            String j10 = cVar.j();
            contentValuesArr[i3] = new ContentValues();
            if (h9 == 2) {
                contentValuesArr[i3].put("mimetype", "vnd.android.cursor.item/postal-address_v2");
                contentValuesArr[i3].put("raw_contact_id", Integer.valueOf(h));
                contentValuesArr[i3].put("data2", Integer.valueOf(h10));
                if (h10 == 0 && !r.b(j8)) {
                    contentValuesArr[i3].put("data3", j8);
                }
                if (!r.b(j10)) {
                    contentValuesArr[i3].put("data1", j10);
                }
            }
            if (h9 == 1) {
                contentValuesArr[i3].put("mimetype", "vnd.android.cursor.item/email_v2");
                contentValuesArr[i3].put("raw_contact_id", Integer.valueOf(h));
                contentValuesArr[i3].put("data2", Integer.valueOf(h10));
                if (h10 == 0 && !r.b(j8)) {
                    contentValuesArr[i3].put("data3", j8);
                }
                if (!r.b(j10)) {
                    contentValuesArr[i3].put("data1", j10);
                }
            }
            if (h9 == 3) {
                contentValuesArr[i3].put("mimetype", "vnd.android.cursor.item/im");
                contentValuesArr[i3].put("raw_contact_id", Integer.valueOf(h));
                contentValuesArr[i3].put("data2", Integer.valueOf(h10));
                if (h10 == 0 && !r.b(j8)) {
                    contentValuesArr[i3].put("data3", j8);
                }
                int a2 = a(j9);
                contentValuesArr[i3].put("data5", Integer.valueOf(a2));
                if (a2 == -1 && !r.b(j8)) {
                    contentValuesArr[i3].put("data6", j8);
                }
                if (!r.b(j10)) {
                    contentValuesArr[i3].put("data1", j10);
                }
            }
            i3++;
        }
        for (int i6 = 0; i6 < h7; i6++) {
            cVar.g();
            int h11 = cVar.h();
            String j11 = cVar.j();
            String j12 = cVar.j();
            String j13 = cVar.j();
            contentValuesArr[i3] = new ContentValues();
            contentValuesArr[i3].put("mimetype", "vnd.android.cursor.item/organization");
            contentValuesArr[i3].put("raw_contact_id", Integer.valueOf(h));
            contentValuesArr[i3].put("data2", Integer.valueOf(h11));
            if (h11 == 0 && !r.b(j11)) {
                contentValuesArr[i3].put("data3", j11);
            }
            if (!r.b(j12)) {
                contentValuesArr[i3].put("data1", j12);
            }
            if (!r.b(j13)) {
                contentValuesArr[i3].put("data4", j13);
            }
            i3++;
        }
        try {
            context.getContentResolver().bulkInsert(ContactsContract.Data.CONTENT_URI, contentValuesArr);
            context.getContentResolver().notifyChange(withAppendedPath, (ContentObserver) null, false);
            cVar.a(0);
        } catch (Exception e2) {
            e2.printStackTrace();
            cVar.a(1);
        }
    }

    public void o(Context context, c cVar) {
        if (cVar.e() < 14) {
            cVar.a(0);
            return;
        }
        int h = cVar.h();
        String j = cVar.j();
        String j2 = cVar.j();
        String j3 = cVar.j();
        String j4 = cVar.j();
        String j5 = cVar.j();
        int e2 = cVar.e();
        int h2 = cVar.h();
        int h3 = cVar.h();
        cVar.g();
        cVar.g();
        cVar.g();
        int h4 = cVar.h();
        int h5 = cVar.h();
        ContentValues contentValues = new ContentValues();
        contentValues.put("starred", Integer.valueOf(e2));
        Uri uri = null;
        if (h2 > 0) {
            if (h3 > 0) {
                uri = Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h2);
            } else {
                uri = Uri.withAppendedPath(MediaStore.Audio.Media.INTERNAL_CONTENT_URI, Constants.STR_EMPTY + h2);
            }
        }
        if (uri != null) {
            contentValues.put("custom_ringtone", uri.toString());
        } else {
            contentValues.putNull("custom_ringtone");
        }
        contentValues.put("times_contacted", Integer.valueOf(h4));
        contentValues.put("last_time_contacted", Integer.valueOf(h5));
        if (context.getContentResolver().update(Uri.withAppendedPath(ContactsContract.RawContacts.CONTENT_URI, Constants.STR_EMPTY + h), contentValues, null, null) <= 0) {
            cVar.a(8);
            return;
        }
        context.getContentResolver().delete(ContactsContract.Data.CONTENT_URI, "raw_contact_id = " + h + " and ( " + "mimetype" + " = '" + "vnd.android.cursor.item/nickname" + "' or " + "mimetype" + " = '" + "vnd.android.cursor.item/note" + "' or " + "mimetype" + " = '" + "vnd.android.cursor.item/name" + "'  " + " )", null);
        int i = 0;
        if (!r.b(j2)) {
            i = 1;
        }
        if (!r.b(j5)) {
            i++;
        }
        if (!r.b(j) || !r.b(j3) || !r.b(j4)) {
            i++;
        }
        ContentValues[] contentValuesArr = new ContentValues[i];
        int i2 = 0;
        if (!r.b(j2)) {
            contentValuesArr[0] = new ContentValues();
            contentValuesArr[0].put("mimetype", "vnd.android.cursor.item/nickname");
            contentValuesArr[0].put("raw_contact_id", Integer.valueOf(h));
            contentValuesArr[0].put("data1", j2);
            i2 = 1;
        }
        if (!r.b(j5)) {
            contentValuesArr[i2] = new ContentValues();
            contentValuesArr[i2].put("mimetype", "vnd.android.cursor.item/note");
            contentValuesArr[i2].put("raw_contact_id", Integer.valueOf(h));
            contentValuesArr[i2].put("data1", j5);
            i2++;
        }
        if (!r.b(j) || !r.b(j3) || !r.b(j4)) {
            contentValuesArr[i2] = new ContentValues();
            contentValuesArr[i2].put("mimetype", "vnd.android.cursor.item/name");
            contentValuesArr[i2].put("raw_contact_id", Integer.valueOf(h));
            if (!r.b(j3) || !r.b(j4)) {
                if (r.b(j3)) {
                    j3 = Constants.STR_EMPTY;
                }
                contentValuesArr[i2].put("data2", j3);
                if (r.b(j4)) {
                    contentValuesArr[i2].put("data1", j3);
                } else {
                    contentValuesArr[i2].put("data1", j3 + j4);
                    contentValuesArr[i2].put("data3", j4);
                }
            } else {
                contentValuesArr[i2].put("data1", j);
                contentValuesArr[i2].put("data2", j3);
            }
            int i3 = i2 + 1;
        }
        context.getContentResolver().bulkInsert(ContactsContract.Data.CONTENT_URI, contentValuesArr);
        cVar.a((ArrayList<byte[]>) null);
        cVar.a(0);
    }

    public void p(Context context, c cVar) {
        if (cVar.e() < 6) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        int h3 = cVar.h();
        String j = cVar.j();
        String j2 = cVar.j();
        String j3 = cVar.j();
        ContentValues contentValues = new ContentValues();
        if (h2 == 2) {
            contentValues.put("mimetype", "vnd.android.cursor.item/postal-address_v2");
            contentValues.put("data2", Integer.valueOf(h3));
            if (h3 != 0 || r.b(j)) {
                contentValues.putNull("data3");
            } else {
                contentValues.put("data3", j);
            }
            if (r.b(j3)) {
                contentValues.putNull("data1");
            } else {
                contentValues.put("data1", j3);
            }
        }
        if (h2 == 1) {
            contentValues.put("mimetype", "vnd.android.cursor.item/email_v2");
            contentValues.put("data2", Integer.valueOf(h3));
            if (h3 != 0 || r.b(j)) {
                contentValues.putNull("data3");
            } else {
                contentValues.put("data3", j);
            }
            if (r.b(j3)) {
                contentValues.putNull("data1");
            } else {
                contentValues.put("data1", j3);
            }
        }
        if (h2 == 3) {
            contentValues.put("mimetype", "vnd.android.cursor.item/im");
            contentValues.put("data2", Integer.valueOf(h3));
            if (h3 != 0 || r.b(j)) {
                contentValues.putNull("data3");
            } else {
                contentValues.put("data3", j);
            }
            if (r.b(j3)) {
                contentValues.putNull("data1");
            } else {
                contentValues.put("data1", j3);
            }
            int a2 = a(j2);
            contentValues.put("data5", Integer.valueOf(a2));
            if (a2 != -1 || r.b(j)) {
                contentValues.putNull("data6");
            } else {
                contentValues.put("data6", j);
            }
        }
        if (context.getContentResolver().insert(Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, Constants.STR_EMPTY + h), contentValues) == null) {
            cVar.a(8);
        }
    }

    public void q(Context context, c cVar) {
        if (cVar.e() < 4) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        String j = cVar.j();
        String j2 = cVar.j();
        ContentValues contentValues = new ContentValues();
        contentValues.put("data2", Integer.valueOf(h2));
        if (h2 != 0 || r.b(j)) {
            contentValues.putNull("data3");
        } else {
            contentValues.put("data3", j);
        }
        if (!r.b(j2)) {
            contentValues.put("data1", j2);
        } else {
            contentValues.putNull("data1");
        }
        int update = context.getContentResolver().update(Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, Constants.STR_EMPTY + h), contentValues, null, null);
        cVar.a((ArrayList<byte[]>) null);
        if (update > 0) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }

    public void r(Context context, c cVar) {
        if (cVar.e() < 5) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        String j = cVar.j();
        String j2 = cVar.j();
        String j3 = cVar.j();
        ContentValues contentValues = new ContentValues();
        contentValues.put("data2", Integer.valueOf(h2));
        if (h2 != 0 || r.b(j)) {
            contentValues.putNull("data3");
        } else {
            contentValues.put("data3", j);
        }
        if (r.b(j2)) {
            contentValues.putNull("data1");
        } else {
            contentValues.put("data1", j2);
        }
        if (r.b(j3)) {
            contentValues.putNull("data4");
        } else {
            contentValues.put("data4", j3);
        }
        int update = context.getContentResolver().update(Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, Constants.STR_EMPTY + h), contentValues, null, null);
        cVar.a((ArrayList<byte[]>) null);
        if (update > 0) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }

    public void s(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        if (context.getContentResolver().delete(Uri.withAppendedPath(ContactsContract.RawContacts.CONTENT_URI, Constants.STR_EMPTY + cVar.h()), null, null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }

    public void t(Context context, c cVar) {
        Uri uri = ContactsContract.RawContacts.CONTENT_URI;
        context.getContentResolver().delete(ContactsContract.Groups.CONTENT_URI, null, null);
        try {
            context.getContentResolver().delete(uri, "deleted = 0", null);
        } catch (Exception e2) {
            cVar.a(8);
        }
    }

    public void u(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        if (context.getContentResolver().delete(Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, Constants.STR_EMPTY + cVar.h()), null, null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }

    public void v(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        if (context.getContentResolver().delete(Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, Constants.STR_EMPTY + cVar.h()), null, null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }

    public void w(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        if (context.getContentResolver().delete(Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, Constants.STR_EMPTY + cVar.h()), null, null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }

    public void x(Context context, c cVar) {
        ArrayList arrayList = new ArrayList();
        Cursor query = context.getContentResolver().query(ContactsContract.RawContacts.CONTENT_URI, null, "deleted = 0", null, "_id");
        if (query == null) {
            cVar.a(8);
            return;
        }
        arrayList.add(r.a(query.getCount()));
        query.close();
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void y(Context context, c cVar) {
        Cursor query = context.getContentResolver().query(ContactsContract.RawContacts.CONTENT_URI, null, "deleted = 0", null, "_id");
        ArrayList arrayList = new ArrayList();
        if (query == null) {
            cVar.a(8);
            return;
        }
        arrayList.add(r.a(query.getCount()));
        for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
            arrayList.add(r.a(query.getInt(query.getColumnIndex("_id"))));
        }
        query.close();
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void z(Context context, c cVar) {
        int i;
        Uri parse;
        String lastPathSegment;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        if (h2 <= 0) {
            cVar.a(1);
            return;
        }
        Cursor query = context.getContentResolver().query(ContactsContract.RawContacts.CONTENT_URI, null, "_id > " + h + " and " + "deleted" + " = 0  and " + "starred" + "  = 1", null, "_id");
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        boolean moveToFirst = query.moveToFirst();
        if (count <= h2) {
            h2 = count;
        }
        int[] iArr = new int[h2];
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(1));
        arrayList.add(r.a(h2));
        int i2 = 0;
        boolean z = moveToFirst;
        while (i2 < h2 && z) {
            int i3 = query.getInt(query.getColumnIndex("_id"));
            int i4 = query.getInt(query.getColumnIndex("starred"));
            iArr[i2] = i3;
            String string = query.getString(query.getColumnIndex("custom_ringtone"));
            int i5 = 0;
            int i6 = 0;
            if (!(string == null || (parse = Uri.parse(string)) == null || (lastPathSegment = parse.getLastPathSegment()) == null)) {
                try {
                    i5 = Integer.parseInt(lastPathSegment);
                } catch (Exception e2) {
                    i5 = 0;
                }
                if (i5 > 0 && string.contains("/external/")) {
                    i6 = 1;
                }
            }
            int i7 = query.getInt(query.getColumnIndex("times_contacted"));
            int i8 = query.getInt(query.getColumnIndex("last_time_contacted"));
            arrayList.add(r.a(i3));
            arrayList.add(r.a(i4));
            arrayList.add(r.a(i5));
            arrayList.add(r.a(i6));
            arrayList.add(r.a(i7));
            arrayList.add(r.a(i8));
            String string2 = query.getString(query.getColumnIndex("account_name"));
            String string3 = query.getString(query.getColumnIndex("account_type"));
            byte[] a2 = r.a(string2);
            byte[] a3 = r.a(string3);
            arrayList.add(a2);
            arrayList.add(a3);
            i2++;
            z = query.moveToNext();
        }
        query.close();
        if (h2 >= 1) {
            i = iArr[h2 - 1];
        } else {
            i = 0;
        }
        int size = arrayList.size();
        arrayList.add(r.a(0));
        Cursor query2 = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, f342a, "raw_contact_id > " + h + " and " + "raw_contact_id" + " <= " + i, null, "raw_contact_id");
        if (query2 == null) {
            cVar.a(8);
            return;
        }
        boolean moveToFirst2 = query2.moveToFirst();
        int i9 = 0;
        while (moveToFirst2) {
            String string4 = query2.getString(query2.getColumnIndex("mimetype"));
            if (string4 == null) {
                moveToFirst2 = query2.moveToNext();
            } else {
                int i10 = query2.getInt(query2.getColumnIndex("raw_contact_id"));
                int i11 = 0;
                while (true) {
                    if (i11 >= h2) {
                        i11 = -1;
                        break;
                    } else if (iArr[i11] == i10) {
                        break;
                    } else {
                        i11++;
                    }
                }
                if (i11 == -1) {
                    moveToFirst2 = query2.moveToNext();
                } else {
                    if (string4.equals("vnd.android.cursor.item/nickname")) {
                        byte[] a4 = r.a(query2.getString(query2.getColumnIndex("data1")));
                        if (r.b(a4, r.hr)) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            arrayList.add(r.a(i11));
                            arrayList.add(r.a(0));
                            arrayList.add(a4);
                            i9++;
                        }
                    } else if (string4.equals("vnd.android.cursor.item/name")) {
                        byte[] a5 = r.a(query2.getString(query2.getColumnIndex("data1")));
                        byte[] a6 = r.a(query2.getString(query2.getColumnIndex("data2")));
                        byte[] a7 = r.a(query2.getString(query2.getColumnIndex("data3")));
                        if (r.b(a5, r.hr)) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            arrayList.add(r.a(i11));
                            arrayList.add(r.a(1));
                            arrayList.add(a5);
                            arrayList.add(a6);
                            arrayList.add(a7);
                            i9++;
                        }
                    } else if (string4.equals("vnd.android.cursor.item/note")) {
                        byte[] a8 = r.a(query2.getString(query2.getColumnIndex("data1")));
                        if (r.b(a8, r.hr)) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            arrayList.add(r.a(i11));
                            arrayList.add(r.a(2));
                            arrayList.add(a8);
                            i9++;
                        }
                    } else if (string4.equals("vnd.android.cursor.item/photo")) {
                        byte[] blob = query2.getBlob(query2.getColumnIndex("data15"));
                        if (blob == null) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            arrayList.add(r.a(i11));
                            arrayList.add(r.a(3));
                            arrayList.add(r.a(blob.length));
                            i9++;
                        }
                    } else if (string4.equals("vnd.android.cursor.item/phone_v2")) {
                        int i12 = query2.getInt(query2.getColumnIndex("_id"));
                        byte[] a9 = r.a(query2.getString(query2.getColumnIndex("data3")));
                        int i13 = query2.getInt(query2.getColumnIndex("data2"));
                        byte[] a10 = r.a(query2.getString(query2.getColumnIndex("data1")));
                        if (r.b(a10, r.hr)) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            arrayList.add(r.a(i11));
                            arrayList.add(r.a(4));
                            arrayList.add(r.a(i12));
                            arrayList.add(r.a(i13));
                            arrayList.add(a9);
                            arrayList.add(a10);
                            i9++;
                        }
                    } else if (string4.equals("vnd.android.cursor.item/postal-address_v2")) {
                        int i14 = query2.getInt(query2.getColumnIndex("_id"));
                        int i15 = query2.getInt(query2.getColumnIndex("data2"));
                        byte[] a11 = r.a(query2.getString(query2.getColumnIndex("data3")));
                        byte[] a12 = r.a(query2.getString(query2.getColumnIndex("data1")));
                        if (r.b(a12, r.hr)) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            arrayList.add(r.a(i11));
                            arrayList.add(r.a(5));
                            arrayList.add(r.a(i14));
                            arrayList.add(r.a(i15));
                            arrayList.add(r.a(2));
                            arrayList.add(a11);
                            arrayList.add(a12);
                            arrayList.add(r.a(0));
                            i9++;
                        }
                    } else if (string4.equals("vnd.android.cursor.item/email_v2")) {
                        int i16 = query2.getInt(query2.getColumnIndex("_id"));
                        int i17 = query2.getInt(query2.getColumnIndex("data2"));
                        byte[] a13 = r.a(query2.getString(query2.getColumnIndex("data3")));
                        byte[] a14 = r.a(query2.getString(query2.getColumnIndex("data1")));
                        if (r.b(a14, r.hr)) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            arrayList.add(r.a(i11));
                            arrayList.add(r.a(5));
                            arrayList.add(r.a(i16));
                            arrayList.add(r.a(i17));
                            arrayList.add(r.a(1));
                            arrayList.add(a13);
                            arrayList.add(a14);
                            arrayList.add(r.a(0));
                            i9++;
                        }
                    } else if (string4.equals("vnd.android.cursor.item/im")) {
                        int i18 = query2.getInt(query2.getColumnIndex("_id"));
                        int i19 = query2.getInt(query2.getColumnIndex("data2"));
                        byte[] a15 = r.a(query2.getString(query2.getColumnIndex("data6")));
                        byte[] a16 = r.a(query2.getString(query2.getColumnIndex("data1")));
                        int i20 = query2.getInt(query2.getColumnIndex("data5"));
                        if (r.b(a16, r.hr)) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            arrayList.add(r.a(i11));
                            arrayList.add(r.a(5));
                            arrayList.add(r.a(i18));
                            arrayList.add(r.a(i19));
                            arrayList.add(r.a(3));
                            arrayList.add(a15);
                            arrayList.add(a16);
                            arrayList.add(r.a(i20));
                            i9++;
                        }
                    } else if (string4.equals("vnd.android.cursor.item/organization")) {
                        int i21 = query2.getInt(query2.getColumnIndex("_id"));
                        int i22 = query2.getInt(query2.getColumnIndex("data2"));
                        byte[] a17 = r.a(query2.getString(query2.getColumnIndex("data3")));
                        byte[] a18 = r.a(query2.getString(query2.getColumnIndex("data1")));
                        byte[] a19 = r.a(query2.getString(query2.getColumnIndex("data4")));
                        if (!r.b(a19, r.hr) || !r.b(a18, r.hr)) {
                            arrayList.add(r.a(i11));
                            arrayList.add(r.a(6));
                            arrayList.add(r.a(i21));
                            arrayList.add(r.a(i22));
                            arrayList.add(a18);
                            arrayList.add(a19);
                            arrayList.add(a17);
                            i9++;
                        } else {
                            moveToFirst2 = query2.moveToNext();
                        }
                    }
                    moveToFirst2 = query2.moveToNext();
                }
            }
        }
        query2.close();
        arrayList.set(size, r.a(i9));
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void A(Context context, c cVar) {
        int i;
        Uri parse;
        String lastPathSegment;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        if (h2 <= 0) {
            cVar.a(1);
            return;
        }
        Cursor query = context.getContentResolver().query(ContactsContract.RawContacts.CONTENT_URI, null, "_id > " + h + " and " + "deleted" + " = " + 0, null, "_id");
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        boolean moveToFirst = query.moveToFirst();
        if (count <= h2) {
            h2 = count;
        }
        int[] iArr = new int[h2];
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(1));
        arrayList.add(r.a(h2));
        int i2 = 0;
        boolean z = moveToFirst;
        while (i2 < h2 && z) {
            int i3 = query.getInt(query.getColumnIndex("_id"));
            int i4 = query.getInt(query.getColumnIndex("starred"));
            iArr[i2] = i3;
            String string = query.getString(query.getColumnIndex("custom_ringtone"));
            int i5 = 0;
            int i6 = 0;
            if (!(string == null || (parse = Uri.parse(string)) == null || (lastPathSegment = parse.getLastPathSegment()) == null)) {
                try {
                    i5 = Integer.parseInt(lastPathSegment);
                } catch (Exception e2) {
                    i5 = 0;
                }
                if (i5 > 0 && string.contains("/external/")) {
                    i6 = 1;
                }
            }
            int i7 = query.getInt(query.getColumnIndex("times_contacted"));
            int i8 = query.getInt(query.getColumnIndex("last_time_contacted"));
            arrayList.add(r.a(i3));
            arrayList.add(r.a(i4));
            arrayList.add(r.a(i5));
            arrayList.add(r.a(i6));
            arrayList.add(r.a(i7));
            arrayList.add(r.a(i8));
            String string2 = query.getString(query.getColumnIndex("account_name"));
            String string3 = query.getString(query.getColumnIndex("account_type"));
            byte[] a2 = r.a(string2);
            byte[] a3 = r.a(string3);
            arrayList.add(a2);
            arrayList.add(a3);
            i2++;
            z = query.moveToNext();
        }
        query.close();
        if (h2 >= 1) {
            i = iArr[h2 - 1];
        } else {
            i = 0;
        }
        int size = arrayList.size();
        arrayList.add(r.a(0));
        Cursor query2 = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, f342a, "raw_contact_id > " + h + " and " + "raw_contact_id" + " <= " + i, null, "raw_contact_id");
        if (query2 == null) {
            cVar.a(8);
            return;
        }
        boolean moveToFirst2 = query2.moveToFirst();
        int i9 = 0;
        while (moveToFirst2) {
            String string4 = query2.getString(query2.getColumnIndex("mimetype"));
            if (string4 == null) {
                moveToFirst2 = query2.moveToNext();
            } else {
                int i10 = query2.getInt(query2.getColumnIndex("raw_contact_id"));
                int i11 = 0;
                while (true) {
                    if (i11 >= h2) {
                        i11 = -1;
                        break;
                    } else if (iArr[i11] == i10) {
                        break;
                    } else {
                        i11++;
                    }
                }
                if (i11 == -1) {
                    moveToFirst2 = query2.moveToNext();
                } else {
                    if (string4.equals("vnd.android.cursor.item/nickname")) {
                        byte[] a4 = r.a(query2.getString(query2.getColumnIndex("data1")));
                        if (r.b(a4, r.hr)) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            arrayList.add(r.a(i11));
                            arrayList.add(r.a(0));
                            arrayList.add(a4);
                            i9++;
                        }
                    } else if (string4.equals("vnd.android.cursor.item/name")) {
                        byte[] a5 = r.a(query2.getString(query2.getColumnIndex("data1")));
                        byte[] a6 = r.a(query2.getString(query2.getColumnIndex("data2")));
                        byte[] a7 = r.a(query2.getString(query2.getColumnIndex("data3")));
                        if (r.b(a5, r.hr)) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            arrayList.add(r.a(i11));
                            arrayList.add(r.a(1));
                            arrayList.add(a5);
                            arrayList.add(a6);
                            arrayList.add(a7);
                            i9++;
                        }
                    } else if (string4.equals("vnd.android.cursor.item/note")) {
                        byte[] a8 = r.a(query2.getString(query2.getColumnIndex("data1")));
                        if (r.b(a8, r.hr)) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            arrayList.add(r.a(i11));
                            arrayList.add(r.a(2));
                            arrayList.add(a8);
                            i9++;
                        }
                    } else if (string4.equals("vnd.android.cursor.item/photo")) {
                        byte[] blob = query2.getBlob(query2.getColumnIndex("data15"));
                        if (blob == null) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            arrayList.add(r.a(i11));
                            arrayList.add(r.a(3));
                            arrayList.add(r.a(blob.length));
                            i9++;
                        }
                    } else if (string4.equals("vnd.android.cursor.item/phone_v2")) {
                        int i12 = query2.getInt(query2.getColumnIndex("_id"));
                        byte[] a9 = r.a(query2.getString(query2.getColumnIndex("data3")));
                        int i13 = query2.getInt(query2.getColumnIndex("data2"));
                        byte[] a10 = r.a(query2.getString(query2.getColumnIndex("data1")));
                        if (r.b(a10, r.hr)) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            arrayList.add(r.a(i11));
                            arrayList.add(r.a(4));
                            arrayList.add(r.a(i12));
                            arrayList.add(r.a(i13));
                            arrayList.add(a9);
                            arrayList.add(a10);
                            i9++;
                        }
                    } else if (string4.equals("vnd.android.cursor.item/postal-address_v2")) {
                        int i14 = query2.getInt(query2.getColumnIndex("_id"));
                        int i15 = query2.getInt(query2.getColumnIndex("data2"));
                        byte[] a11 = r.a(query2.getString(query2.getColumnIndex("data3")));
                        byte[] a12 = r.a(query2.getString(query2.getColumnIndex("data1")));
                        if (r.b(a12, r.hr)) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            arrayList.add(r.a(i11));
                            arrayList.add(r.a(5));
                            arrayList.add(r.a(i14));
                            arrayList.add(r.a(i15));
                            arrayList.add(r.a(2));
                            arrayList.add(a11);
                            arrayList.add(a12);
                            arrayList.add(r.a(0));
                            i9++;
                        }
                    } else if (string4.equals("vnd.android.cursor.item/email_v2")) {
                        int i16 = query2.getInt(query2.getColumnIndex("_id"));
                        int i17 = query2.getInt(query2.getColumnIndex("data2"));
                        byte[] a13 = r.a(query2.getString(query2.getColumnIndex("data3")));
                        byte[] a14 = r.a(query2.getString(query2.getColumnIndex("data1")));
                        if (r.b(a14, r.hr)) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            arrayList.add(r.a(i11));
                            arrayList.add(r.a(5));
                            arrayList.add(r.a(i16));
                            arrayList.add(r.a(i17));
                            arrayList.add(r.a(1));
                            arrayList.add(a13);
                            arrayList.add(a14);
                            arrayList.add(r.a(0));
                            i9++;
                        }
                    } else if (string4.equals("vnd.android.cursor.item/im")) {
                        int i18 = query2.getInt(query2.getColumnIndex("_id"));
                        int i19 = query2.getInt(query2.getColumnIndex("data2"));
                        byte[] a15 = r.a(query2.getString(query2.getColumnIndex("data6")));
                        byte[] a16 = r.a(query2.getString(query2.getColumnIndex("data1")));
                        int i20 = query2.getInt(query2.getColumnIndex("data5"));
                        if (r.b(a16, r.hr)) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            arrayList.add(r.a(i11));
                            arrayList.add(r.a(5));
                            arrayList.add(r.a(i18));
                            arrayList.add(r.a(i19));
                            arrayList.add(r.a(3));
                            arrayList.add(a15);
                            arrayList.add(a16);
                            arrayList.add(r.a(i20));
                            i9++;
                        }
                    } else if (string4.equals("vnd.android.cursor.item/organization")) {
                        int i21 = query2.getInt(query2.getColumnIndex("_id"));
                        int i22 = query2.getInt(query2.getColumnIndex("data2"));
                        byte[] a17 = r.a(query2.getString(query2.getColumnIndex("data3")));
                        byte[] a18 = r.a(query2.getString(query2.getColumnIndex("data1")));
                        byte[] a19 = r.a(query2.getString(query2.getColumnIndex("data4")));
                        if (!r.b(a19, r.hr) || !r.b(a18, r.hr)) {
                            arrayList.add(r.a(i11));
                            arrayList.add(r.a(6));
                            arrayList.add(r.a(i21));
                            arrayList.add(r.a(i22));
                            arrayList.add(a18);
                            arrayList.add(a19);
                            arrayList.add(a17);
                            i9++;
                        } else {
                            moveToFirst2 = query2.moveToNext();
                        }
                    }
                    moveToFirst2 = query2.moveToNext();
                }
            }
        }
        query2.close();
        arrayList.set(size, r.a(i9));
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void B(Context context, c cVar) {
        int i;
        int i2;
        int i3;
        Uri parse;
        String lastPathSegment;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        if (h2 <= 0) {
            cVar.a(1);
            return;
        }
        Cursor query = context.getContentResolver().query(ContactsContract.RawContacts.CONTENT_URI, null, "_id > " + h + " and " + "deleted" + " = " + 0, null, "_id");
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        boolean moveToFirst = query.moveToFirst();
        if (count <= h2) {
            h2 = count;
        }
        int[] iArr = new int[h2];
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(1));
        arrayList.add(r.a(h2));
        int i4 = 0;
        boolean z = moveToFirst;
        while (i4 < h2 && z) {
            int i5 = query.getInt(query.getColumnIndex("_id"));
            int i6 = query.getInt(query.getColumnIndex("starred"));
            iArr[i4] = i5;
            String string = query.getString(query.getColumnIndex("custom_ringtone"));
            int i7 = 0;
            int i8 = 0;
            if (!(string == null || (parse = Uri.parse(string)) == null || (lastPathSegment = parse.getLastPathSegment()) == null)) {
                try {
                    i7 = Integer.parseInt(lastPathSegment);
                } catch (Exception e2) {
                    i7 = 0;
                }
                if (i7 > 0 && string.contains("/external/")) {
                    i8 = 1;
                }
            }
            int i9 = query.getInt(query.getColumnIndex("times_contacted"));
            int i10 = query.getInt(query.getColumnIndex("last_time_contacted"));
            arrayList.add(r.a(i5));
            arrayList.add(r.a(i6));
            arrayList.add(r.a(i7));
            arrayList.add(r.a(i8));
            arrayList.add(r.a(i9));
            arrayList.add(r.a(i10));
            byte[] blob = query.getBlob(query.getColumnIndex("account_name"));
            byte[] blob2 = query.getBlob(query.getColumnIndex("account_type"));
            if (blob == null) {
                blob = r.hr;
            }
            if (blob2 == null) {
                blob2 = r.hr;
            }
            arrayList.add(blob);
            arrayList.add(blob2);
            i4++;
            z = query.moveToNext();
        }
        query.close();
        if (h2 >= 1) {
            i = iArr[h2 - 1];
        } else {
            i = 0;
        }
        int size = arrayList.size();
        arrayList.add(r.a(0));
        Cursor query2 = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, f342a, "raw_contact_id > " + h + " and " + "raw_contact_id" + " <= " + i, null, "raw_contact_id");
        if (query2 == null) {
            cVar.a(8);
            return;
        }
        boolean moveToFirst2 = query2.moveToFirst();
        int i11 = 0;
        while (moveToFirst2) {
            String string2 = query2.getString(query2.getColumnIndex("mimetype"));
            if (string2 == null) {
                moveToFirst2 = query2.moveToNext();
            } else {
                int i12 = query2.getInt(query2.getColumnIndex("raw_contact_id"));
                int i13 = 0;
                while (true) {
                    if (i13 >= h2) {
                        i2 = -1;
                        break;
                    } else if (iArr[i13] == i12) {
                        i2 = i13;
                        break;
                    } else {
                        i13++;
                    }
                }
                if (i2 == -1) {
                    moveToFirst2 = query2.moveToNext();
                } else {
                    if (string2.equals("vnd.android.cursor.item/nickname")) {
                        byte[] blob3 = query2.getBlob(query2.getColumnIndex("data1"));
                        if (blob3 == null) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            arrayList.add(r.a(i2));
                            arrayList.add(r.a(0));
                            arrayList.add(blob3);
                            i3 = i11 + 1;
                        }
                    } else if (string2.equals("vnd.android.cursor.item/name")) {
                        byte[] blob4 = query2.getBlob(query2.getColumnIndex("data1"));
                        byte[] blob5 = query2.getBlob(query2.getColumnIndex("data2"));
                        byte[] blob6 = query2.getBlob(query2.getColumnIndex("data3"));
                        if (blob4 == null) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            if (blob5 == null) {
                                blob5 = r.hr;
                            }
                            if (blob6 == null) {
                                blob6 = r.hr;
                            }
                            arrayList.add(r.a(i2));
                            arrayList.add(r.a(1));
                            arrayList.add(blob4);
                            arrayList.add(blob5);
                            arrayList.add(blob6);
                            i3 = i11 + 1;
                        }
                    } else if (string2.equals("vnd.android.cursor.item/note")) {
                        byte[] blob7 = query2.getBlob(query2.getColumnIndex("data1"));
                        if (blob7 == null) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            arrayList.add(r.a(i2));
                            arrayList.add(r.a(2));
                            arrayList.add(blob7);
                            i3 = i11 + 1;
                        }
                    } else if (string2.equals("vnd.android.cursor.item/photo")) {
                        byte[] blob8 = query2.getBlob(query2.getColumnIndex("data15"));
                        if (blob8 == null) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            arrayList.add(r.a(i2));
                            arrayList.add(r.a(3));
                            arrayList.add(r.a(blob8.length));
                            i3 = i11 + 1;
                        }
                    } else if (string2.equals("vnd.android.cursor.item/phone_v2")) {
                        int i14 = query2.getInt(query2.getColumnIndex("_id"));
                        byte[] blob9 = query2.getBlob(query2.getColumnIndex("data3"));
                        int i15 = query2.getInt(query2.getColumnIndex("data2"));
                        byte[] blob10 = query2.getBlob(query2.getColumnIndex("data1"));
                        if (blob10 == null) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            if (blob9 == null) {
                                blob9 = r.hr;
                            }
                            arrayList.add(r.a(i2));
                            arrayList.add(r.a(4));
                            arrayList.add(r.a(i14));
                            arrayList.add(r.a(i15));
                            arrayList.add(blob9);
                            arrayList.add(blob10);
                            i3 = i11 + 1;
                        }
                    } else if (string2.equals("vnd.android.cursor.item/postal-address_v2")) {
                        int i16 = query2.getInt(query2.getColumnIndex("_id"));
                        int i17 = query2.getInt(query2.getColumnIndex("data2"));
                        byte[] blob11 = query2.getBlob(query2.getColumnIndex("data3"));
                        byte[] blob12 = query2.getBlob(query2.getColumnIndex("data1"));
                        if (blob12 == null) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            if (blob11 == null) {
                                blob11 = r.hr;
                            }
                            arrayList.add(r.a(i2));
                            arrayList.add(r.a(5));
                            arrayList.add(r.a(i16));
                            arrayList.add(r.a(i17));
                            arrayList.add(r.a(2));
                            arrayList.add(blob11);
                            arrayList.add(blob12);
                            arrayList.add(r.a(0));
                            i3 = i11 + 1;
                        }
                    } else if (string2.equals("vnd.android.cursor.item/email_v2")) {
                        int i18 = query2.getInt(query2.getColumnIndex("_id"));
                        int i19 = query2.getInt(query2.getColumnIndex("data2"));
                        byte[] blob13 = query2.getBlob(query2.getColumnIndex("data3"));
                        byte[] blob14 = query2.getBlob(query2.getColumnIndex("data1"));
                        if (blob14 == null) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            if (blob13 == null) {
                                blob13 = r.hr;
                            }
                            arrayList.add(r.a(i2));
                            arrayList.add(r.a(5));
                            arrayList.add(r.a(i18));
                            arrayList.add(r.a(i19));
                            arrayList.add(r.a(1));
                            arrayList.add(blob13);
                            arrayList.add(blob14);
                            arrayList.add(r.a(0));
                            i3 = i11 + 1;
                        }
                    } else if (string2.equals("vnd.android.cursor.item/im")) {
                        int i20 = query2.getInt(query2.getColumnIndex("_id"));
                        int i21 = query2.getInt(query2.getColumnIndex("data2"));
                        byte[] blob15 = query2.getBlob(query2.getColumnIndex("data6"));
                        byte[] blob16 = query2.getBlob(query2.getColumnIndex("data1"));
                        int i22 = query2.getInt(query2.getColumnIndex("data5"));
                        if (blob16 == null) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            if (blob15 == null) {
                                blob15 = r.hr;
                            }
                            arrayList.add(r.a(i2));
                            arrayList.add(r.a(5));
                            arrayList.add(r.a(i20));
                            arrayList.add(r.a(i21));
                            arrayList.add(r.a(3));
                            arrayList.add(blob15);
                            arrayList.add(blob16);
                            arrayList.add(r.a(i22));
                            i3 = i11 + 1;
                        }
                    } else if (string2.equals("vnd.android.cursor.item/organization")) {
                        int i23 = query2.getInt(query2.getColumnIndex("_id"));
                        int i24 = query2.getInt(query2.getColumnIndex("data2"));
                        byte[] blob17 = query2.getBlob(query2.getColumnIndex("data3"));
                        byte[] blob18 = query2.getBlob(query2.getColumnIndex("data1"));
                        byte[] blob19 = query2.getBlob(query2.getColumnIndex("data4"));
                        if (blob19 == null && blob18 == null) {
                            moveToFirst2 = query2.moveToNext();
                        } else {
                            if (blob17 == null) {
                                blob17 = r.hr;
                            }
                            if (blob18 == null) {
                                blob18 = r.hr;
                            }
                            if (blob19 == null) {
                                blob19 = r.hr;
                            }
                            arrayList.add(r.a(i2));
                            arrayList.add(r.a(6));
                            arrayList.add(r.a(i23));
                            arrayList.add(r.a(i24));
                            arrayList.add(blob18);
                            arrayList.add(blob19);
                            arrayList.add(blob17);
                            i3 = i11 + 1;
                        }
                    } else {
                        i3 = i11;
                    }
                    i11 = i3;
                    moveToFirst2 = query2.moveToNext();
                }
            }
        }
        query2.close();
        arrayList.set(size, r.a(i11));
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void C(Context context, c cVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(-1));
        arrayList.add(r.a(65555));
        cVar.a(arrayList);
        B(context, cVar);
    }

    public void D(Context context, c cVar) {
        byte[] bArr = null;
        ArrayList arrayList = new ArrayList();
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        Cursor query = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, "raw_contact_id = " + cVar.h() + " and " + "mimetype" + " = '" + "vnd.android.cursor.item/photo" + "'", null, null);
        if (query == null) {
            cVar.a(8);
        } else if (query.moveToFirst()) {
            try {
                bArr = query.getBlob(query.getColumnIndex("data15"));
            } catch (Exception e2) {
            }
            if (bArr != null) {
                arrayList.add(bArr);
                query.close();
                cVar.a(0);
                cVar.a(arrayList);
                return;
            }
            cVar.a(7);
            query.close();
        } else {
            cVar.a(1);
            query.close();
        }
    }

    public void E(Context context, c cVar) {
        int i;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (h < 0) {
            i = 0;
        } else {
            i = h;
        }
        Cursor query = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, "raw_contact_id = " + i + " and " + "mimetype" + " = '" + "vnd.android.cursor.item/photo" + "'", null, null);
        if (query == null) {
            cVar.a(8);
        } else if (!query.moveToFirst()) {
            query.close();
            cVar.a(0);
        } else {
            int i2 = query.getInt(query.getColumnIndex("_id"));
            query.close();
            ContentValues contentValues = new ContentValues();
            contentValues.putNull("data15");
            if (context.getContentResolver().update(Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, Constants.STR_EMPTY + i2), contentValues, null, null) > 0) {
                cVar.a(0);
            } else {
                cVar.a(8);
            }
        }
    }

    public void F(Context context, c cVar) {
        boolean z;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        a(context);
        int h = cVar.h();
        Uri withAppendedPath = Uri.withAppendedPath(ContactsContract.RawContacts.CONTENT_URI, Constants.STR_EMPTY + h);
        Cursor query = context.getContentResolver().query(withAppendedPath, null, null, null, null);
        if (query == null) {
            cVar.a(8);
        } else if (!query.moveToFirst()) {
            query.close();
            cVar.a(1);
        } else {
            String string = query.getString(query.getColumnIndex("account_name"));
            String string2 = query.getString(query.getColumnIndex("account_type"));
            query.close();
            if (string != null || this.d == null) {
                z = false;
            } else {
                z = true;
            }
            if (string2 == null && this.e != null) {
                z = true;
            }
            if (z && Build.BRAND.equals("moto_chn")) {
                ContentValues contentValues = new ContentValues();
                if (!r.b(this.d)) {
                    contentValues.put("account_name", this.d);
                }
                if (!r.b(this.e)) {
                    contentValues.put("account_type", this.e);
                }
                context.getContentResolver().update(withAppendedPath, contentValues, null, null);
            }
            byte[] k = cVar.k();
            ContentValues contentValues2 = new ContentValues();
            if (k == null) {
                contentValues2.putNull("data15");
            } else {
                contentValues2.put("data15", k);
            }
            Cursor query2 = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, "raw_contact_id = " + h + " and " + "mimetype" + " = '" + "vnd.android.cursor.item/photo" + "'", null, null);
            if (query2 == null) {
                cVar.a(8);
            } else if (!query2.moveToFirst()) {
                query2.close();
                contentValues2.put("raw_contact_id", Integer.valueOf(h));
                contentValues2.put("mimetype", "vnd.android.cursor.item/photo");
                context.getContentResolver().insert(ContactsContract.Data.CONTENT_URI, contentValues2);
            } else {
                int i = query2.getInt(query2.getColumnIndex("_id"));
                query2.close();
                if (context.getContentResolver().update(Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, Constants.STR_EMPTY + i), contentValues2, null, null) > 0) {
                    cVar.a(0);
                } else {
                    cVar.a(1);
                }
            }
        }
    }

    public void G(Context context, c cVar) {
        boolean z;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        if (cVar.e() != h2 + 2) {
            cVar.a(1);
            return;
        }
        Cursor query = context.getContentResolver().query(Uri.withAppendedPath(ContactsContract.RawContacts.CONTENT_URI, Constants.STR_EMPTY + h), null, "deleted = 0", null, null);
        if (query == null) {
            cVar.a(8);
        } else if (!query.moveToFirst()) {
            query.close();
            cVar.a(1);
        } else {
            query.close();
            if (h2 > 0) {
                int[] iArr = new int[h2];
                for (int i = 0; i < h2; i++) {
                    iArr[i] = cVar.h();
                }
                for (int i2 = 0; i2 < h2; i2++) {
                    if (iArr[i2] > 0) {
                        for (int i3 = i2 + 1; i3 < h2 - 1; i3++) {
                            if (iArr[i2] == iArr[i3]) {
                                iArr[i3] = 0;
                            }
                        }
                    }
                }
                ArrayList arrayList = new ArrayList();
                Cursor query2 = context.getContentResolver().query(ContactsContract.Groups.CONTENT_URI, null, "deleted = 0", null, "_id");
                if (query2 == null) {
                    cVar.a(8);
                    return;
                }
                for (boolean moveToFirst = query2.moveToFirst(); moveToFirst; moveToFirst = query2.moveToNext()) {
                    arrayList.add(Integer.valueOf(query2.getInt(query2.getColumnIndex("_id"))));
                }
                query2.close();
                for (int i4 = 0; i4 < iArr.length; i4++) {
                    if (iArr[i4] > 0) {
                        int i5 = 0;
                        while (true) {
                            if (i5 >= arrayList.size()) {
                                z = false;
                                break;
                            } else if (iArr[i4] == ((Integer) arrayList.get(i5)).intValue()) {
                                z = true;
                                break;
                            } else {
                                i5++;
                            }
                        }
                        if (!z) {
                            iArr[i4] = 0;
                        }
                    }
                }
                Cursor query3 = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, "mimetype = 'vnd.android.cursor.item/group_membership' and raw_contact_id = " + h, null, null);
                if (query3 == null) {
                    cVar.a(8);
                    return;
                }
                for (boolean moveToFirst2 = query3.moveToFirst(); moveToFirst2; moveToFirst2 = query3.moveToNext()) {
                    int i6 = query3.getInt(query3.getColumnIndex("data1"));
                    for (int i7 = 0; i7 < iArr.length; i7++) {
                        if (i6 == iArr[i7]) {
                            iArr[i7] = 0;
                        }
                    }
                }
                query3.close();
                ArrayList arrayList2 = new ArrayList();
                for (int i8 = 0; i8 < iArr.length; i8++) {
                    if (iArr[i8] > 0) {
                        arrayList2.add(Integer.valueOf(iArr[i8]));
                    }
                }
                ContentValues[] contentValuesArr = new ContentValues[arrayList2.size()];
                for (int i9 = 0; i9 < contentValuesArr.length; i9++) {
                    contentValuesArr[i9] = new ContentValues();
                    contentValuesArr[i9].put("mimetype", "vnd.android.cursor.item/group_membership");
                    contentValuesArr[i9].put("raw_contact_id", Integer.valueOf(h));
                    contentValuesArr[i9].put("data1", Integer.valueOf(((Integer) arrayList2.get(i9)).intValue()));
                }
                if (contentValuesArr.length > 0) {
                    context.getContentResolver().bulkInsert(ContactsContract.Data.CONTENT_URI, contentValuesArr);
                }
                cVar.a(0);
                cVar.a((ArrayList<byte[]>) null);
                return;
            }
            cVar.a(0);
        }
    }

    public void H(Context context, c cVar) {
        ArrayList arrayList = new ArrayList();
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        Cursor query = context.getContentResolver().query(Uri.withAppendedPath(ContactsContract.Groups.CONTENT_URI, Constants.STR_EMPTY + h), null, "deleted = 0", null, null);
        if (query == null) {
            cVar.a(7);
        } else if (query.moveToFirst()) {
            arrayList.add(r.a(h));
            arrayList.add(r.a(query.getString(query.getColumnIndex("title"))));
            arrayList.add(r.a(query.getString(query.getColumnIndex("notes"))));
            arrayList.add(r.a(query.getString(query.getColumnIndex("system_id"))));
            arrayList.add(r.a("account_name"));
            arrayList.add(r.a("account_type"));
            Cursor query2 = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, "data1=" + h + " and " + "mimetype" + " = '" + "vnd.android.cursor.item/group_membership" + "'", null, null);
            if (query2 == null) {
                if (query != null) {
                    query.close();
                }
                cVar.a(8);
                return;
            }
            if (query2.getCount() < 0) {
            }
            for (boolean moveToFirst = query2.moveToFirst(); moveToFirst; moveToFirst = query2.moveToNext()) {
                arrayList.add(r.a(query2.getInt(query2.getColumnIndex("raw_contact_id"))));
            }
            query2.close();
            if (query != null) {
                query.close();
            }
            cVar.a(arrayList);
            cVar.a(0);
        } else {
            query.close();
            cVar.a(7);
        }
    }

    public void I(Context context, c cVar) {
        int i;
        boolean z;
        ArrayList arrayList = new ArrayList();
        Cursor query = context.getContentResolver().query(ContactsContract.Groups.CONTENT_URI, null, "deleted = 0", null, "_id");
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        boolean moveToFirst = query.moveToFirst();
        if (count < 0) {
            count = 0;
        }
        arrayList.add(r.a(1));
        arrayList.add(r.a(count));
        int i2 = 0;
        int[] iArr = new int[count];
        for (int i3 = 0; i3 < iArr.length; i3++) {
            iArr[i3] = 0;
        }
        int i4 = 0;
        boolean z2 = moveToFirst;
        while (i4 < count && z2) {
            int i5 = query.getInt(query.getColumnIndex("_id"));
            String string = query.getString(query.getColumnIndex("title"));
            byte[] blob = query.getBlob(query.getColumnIndex("notes"));
            byte[] a2 = r.a(query.getString(query.getColumnIndex("system_id")));
            if (string == null || !string.equals("Starred in Android")) {
                byte[] a3 = r.a(query.getString(query.getColumnIndex("account_name")));
                byte[] a4 = r.a(query.getString(query.getColumnIndex("account_type")));
                if (blob == null) {
                    blob = r.hr;
                }
                if (a2 == null) {
                    a2 = r.hr;
                }
                if (a3 == null) {
                    a3 = r.hr;
                }
                if (a4 == null) {
                    a4 = r.hr;
                }
                arrayList.add(r.a(i5));
                arrayList.add(r.a(string));
                arrayList.add(blob);
                arrayList.add(a2);
                arrayList.add(a3);
                arrayList.add(a4);
                boolean moveToNext = query.moveToNext();
                iArr[i2] = i5;
                i = i2 + 1;
                z = moveToNext;
            } else {
                int i6 = i2;
                z = query.moveToNext();
                i = i6;
            }
            i4++;
            int i7 = i;
            z2 = z;
            i2 = i7;
        }
        arrayList.set(1, r.a(i2));
        query.close();
        Cursor query2 = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{"data1", "raw_contact_id"}, "mimetype = 'vnd.android.cursor.item/group_membership'", null, "data1");
        if (query2 == null) {
            cVar.a(8);
            return;
        }
        int count2 = query2.getCount();
        int size = arrayList.size();
        arrayList.add(r.a(count2));
        boolean moveToFirst2 = query2.moveToFirst();
        int i8 = 0;
        while (moveToFirst2) {
            int i9 = query2.getInt(query2.getColumnIndex("data1"));
            int i10 = 0;
            while (true) {
                if (i10 >= iArr.length) {
                    i10 = -1;
                    break;
                } else if (i9 == iArr[i10]) {
                    break;
                } else {
                    i10++;
                }
            }
            if (i10 == -1) {
                moveToFirst2 = query2.moveToNext();
            } else {
                int i11 = query2.getInt(query2.getColumnIndex("raw_contact_id"));
                arrayList.add(r.a(i10));
                arrayList.add(r.a(i11));
                moveToFirst2 = query2.moveToNext();
                i8++;
            }
        }
        query2.close();
        arrayList.set(size, r.a(i8));
        cVar.a(arrayList);
        cVar.a(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void J(Context context, c cVar) {
        if (cVar.f() < 4) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        String j2 = cVar.j();
        String j3 = cVar.j();
        String j4 = cVar.j();
        ContentValues contentValues = new ContentValues();
        if (!r.b(j)) {
            contentValues.put("title", j);
        }
        if (!r.b(j2)) {
            contentValues.put("notes", j2);
        }
        contentValues.put("group_visible", (Integer) 1);
        contentValues.put("should_sync", (Integer) 0);
        if (!com.tencent.assistant.kapalaiadapter.d.b) {
            String[] e2 = com.tencent.assistant.kapalaiadapter.a.a().e(context);
            j4 = e2[0];
            j3 = e2[1];
        }
        a(context);
        if (!r.b(j4)) {
            contentValues.put("account_name", j4);
        }
        if (!r.b(j3)) {
            contentValues.put("account_type", j3);
        }
        Uri insert = context.getContentResolver().insert(ContactsContract.Groups.CONTENT_URI, contentValues);
        if (insert == null) {
            cVar.a(8);
            return;
        }
        int intValue = Integer.valueOf(insert.getLastPathSegment()).intValue();
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(intValue));
        cVar.a(0);
        cVar.a(arrayList);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void K(Context context, c cVar) {
        ArrayList arrayList = new ArrayList();
        if (cVar.f() < 2) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        String j2 = cVar.j();
        ContentValues contentValues = new ContentValues();
        if (!r.b(j)) {
            contentValues.put("title", j);
        }
        if (!r.b(j2)) {
            contentValues.put("notes", j2);
        }
        contentValues.put("group_visible", (Integer) 1);
        contentValues.put("should_sync", (Integer) 0);
        a(context);
        if (!r.b(this.d)) {
            contentValues.put("account_name", this.d);
        }
        if (!r.b(this.e)) {
            contentValues.put("account_type", this.e);
        }
        Uri insert = context.getContentResolver().insert(ContactsContract.Groups.CONTENT_URI, contentValues);
        if (insert == null) {
            cVar.a(8);
            return;
        }
        arrayList.add(r.a(Integer.valueOf(insert.getLastPathSegment()).intValue()));
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void L(Context context, c cVar) {
        Uri withAppendedId;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (h < 1) {
            cVar.a(1);
            return;
        }
        context.getContentResolver().delete(ContactsContract.Data.CONTENT_URI, "mimetype = 'vnd.android.cursor.item/group_membership' and data1 = " + h, null);
        if (Build.MODEL.equals("MT917")) {
            withAppendedId = ContentUris.withAppendedId(ContactsContract.Groups.CONTENT_URI, (long) h).buildUpon().appendQueryParameter("caller_is_syncadapter", "true").build();
        } else if (!com.tencent.assistant.kapalaiadapter.d.c) {
            withAppendedId = com.tencent.assistant.kapalaiadapter.a.a().a((long) h);
        } else {
            withAppendedId = ContentUris.withAppendedId(ContactsContract.Groups.CONTENT_URI, (long) h);
        }
        if (withAppendedId == null) {
            cVar.a(1);
        } else if (context.getContentResolver().delete(withAppendedId, null, null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }

    public void M(Context context, c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        Uri withAppendedPath = Uri.withAppendedPath(ContactsContract.RawContacts.CONTENT_URI, Constants.STR_EMPTY + h);
        Cursor query = context.getContentResolver().query(withAppendedPath, new String[]{"_id"}, null, null, null);
        if (query == null) {
            cVar.a(8);
        } else if (query.getCount() < 1) {
            query.close();
            cVar.a(1);
        } else {
            query.close();
            Uri withAppendedPath2 = Uri.withAppendedPath(ContactsContract.Groups.CONTENT_URI, Constants.STR_EMPTY + h2);
            Cursor query2 = context.getContentResolver().query(withAppendedPath2, new String[]{"_id"}, null, null, null);
            if (query2 == null) {
                cVar.a(8);
            } else if (query2.getCount() < 1) {
                query2.close();
                cVar.a(1);
            } else {
                query2.close();
                Cursor query3 = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, f342a, "mimetype = 'vnd.android.cursor.item/group_membership' and raw_contact_id = " + h + " and " + "data1" + " = " + h2, null, null);
                if (query3 == null) {
                    cVar.a(8);
                } else if (query3.moveToFirst()) {
                    query3.close();
                    cVar.a(0);
                } else {
                    query3.close();
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("raw_contact_id", Integer.valueOf(h));
                    contentValues.put("mimetype", "vnd.android.cursor.item/group_membership");
                    contentValues.put("data1", Integer.valueOf(h2));
                    context.getContentResolver().insert(ContactsContract.Data.CONTENT_URI, contentValues);
                    cVar.a(0);
                }
            }
        }
    }

    public void N(Context context, c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        Uri withAppendedPath = Uri.withAppendedPath(ContactsContract.RawContacts.CONTENT_URI, Constants.STR_EMPTY + h);
        Cursor query = context.getContentResolver().query(withAppendedPath, new String[]{"_id"}, null, null, null);
        if (query == null) {
            cVar.a(8);
        } else if (query.getCount() < 1) {
            query.close();
            cVar.a(1);
        } else {
            query.close();
            Uri withAppendedPath2 = Uri.withAppendedPath(ContactsContract.Groups.CONTENT_URI, Constants.STR_EMPTY + h2);
            Cursor query2 = context.getContentResolver().query(withAppendedPath2, new String[]{"_id"}, null, null, null);
            if (query2 == null) {
                cVar.a(8);
            } else if (query2.getCount() < 1) {
                query2.close();
                cVar.a(1);
            } else {
                query2.close();
                Cursor query3 = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, f342a, "mimetype = 'vnd.android.cursor.item/group_membership' and raw_contact_id = " + h + " and " + "data1" + " = " + h2, null, null);
                if (query3 == null) {
                    cVar.a(8);
                } else if (!query3.moveToFirst()) {
                    query3.close();
                    cVar.a(0);
                } else {
                    int i = query3.getInt(query3.getColumnIndex("_id"));
                    query3.close();
                    if (context.getContentResolver().delete(Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, Constants.STR_EMPTY + i), null, null) > 0) {
                        cVar.a(0);
                    } else {
                        cVar.a(8);
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void O(Context context, c cVar) {
        int i;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        Uri withAppendedPath = Uri.withAppendedPath(ContactsContract.RawContacts.CONTENT_URI, Constants.STR_EMPTY + cVar.h());
        Cursor query = context.getContentResolver().query(withAppendedPath, new String[]{"_id", "starred"}, null, null, null);
        if (query == null) {
            cVar.a(8);
        } else if (!query.moveToFirst()) {
            query.close();
            cVar.a(1);
        } else {
            int i2 = query.getInt(query.getColumnIndex("starred"));
            query.close();
            ContentValues contentValues = new ContentValues();
            contentValues.put("starred", (Integer) 1);
            if (i2 != 1) {
                i = context.getContentResolver().update(withAppendedPath, contentValues, null, null);
                context.getContentResolver().notifyChange(withAppendedPath, null);
            } else {
                i = 1;
            }
            if (i == 1) {
                cVar.a(0);
            } else {
                cVar.a(8);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void P(Context context, c cVar) {
        int i;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        Uri withAppendedPath = Uri.withAppendedPath(ContactsContract.RawContacts.CONTENT_URI, Constants.STR_EMPTY + cVar.h());
        Cursor query = context.getContentResolver().query(withAppendedPath, new String[]{"_id", "starred"}, null, null, null);
        if (query == null) {
            cVar.a(8);
        } else if (!query.moveToFirst()) {
            query.close();
            cVar.a(1);
        } else {
            int i2 = query.getInt(query.getColumnIndex("starred"));
            query.close();
            ContentValues contentValues = new ContentValues();
            contentValues.put("starred", (Integer) 0);
            if (i2 != 0) {
                i = context.getContentResolver().update(withAppendedPath, contentValues, null, null);
                context.getContentResolver().notifyChange(withAppendedPath, null);
            } else {
                i = 1;
            }
            if (i == 1) {
                cVar.a(0);
            } else {
                cVar.a(8);
            }
        }
    }

    public void Q(Context context, c cVar) {
        if (cVar.e() < 3) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        String j = cVar.j();
        String j2 = cVar.j();
        ContentValues contentValues = new ContentValues();
        Uri withAppendedPath = Uri.withAppendedPath(ContactsContract.Groups.CONTENT_URI, Constants.STR_EMPTY + h);
        if (!r.b(j)) {
            contentValues.put("title", j);
        } else {
            contentValues.putNull("title");
        }
        if (!r.b(j2)) {
            contentValues.put("notes", j2);
        } else {
            contentValues.putNull("notes");
        }
        if (context.getContentResolver().update(withAppendedPath, contentValues, null, null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(8);
        }
    }

    public void R(Context context, c cVar) {
        String str;
        int i;
        String str2 = null;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        Uri withAppendedPath = Uri.withAppendedPath(ContactsContract.RawContacts.CONTENT_URI, Constants.STR_EMPTY + cVar.h());
        ArrayList arrayList = new ArrayList();
        Cursor query = context.getContentResolver().query(withAppendedPath, new String[]{"_id"}, null, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        if (query.moveToFirst()) {
            str = query.getString(query.getColumnIndex("custom_ringtone"));
        } else {
            str = null;
        }
        query.close();
        if (str == null || !str.startsWith("content:")) {
            arrayList.add(r.a(0));
            arrayList.add(r.a(0));
            cVar.a(0);
            cVar.a(arrayList);
            return;
        }
        Cursor query2 = context.getContentResolver().query(Uri.parse(str), null, null, null, null);
        if (query2 == null) {
            cVar.a(8);
            return;
        }
        if (query2.moveToFirst()) {
            str2 = query2.getString(query2.getColumnIndex("_data"));
        }
        query2.close();
        if (str2 == null) {
            cVar.a(7);
            cVar.a(arrayList);
            return;
        }
        int parseInt = Integer.parseInt(Uri.parse(str2).getLastPathSegment());
        if (str2.contains("/external/")) {
            i = 1;
        } else {
            i = 0;
        }
        arrayList.add(r.a(parseInt));
        arrayList.add(r.a(i));
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void S(Context context, c cVar) {
        Uri withAppendedPath;
        int i;
        if (cVar.e() < 3) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        if (cVar.h() > 0) {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h2);
        } else {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Audio.Media.INTERNAL_CONTENT_URI, Constants.STR_EMPTY + h2);
        }
        Uri withAppendedPath2 = Uri.withAppendedPath(ContactsContract.RawContacts.CONTENT_URI, Constants.STR_EMPTY + h);
        ContentValues contentValues = new ContentValues();
        contentValues.put("custom_ringtone", withAppendedPath.toString());
        int update = context.getContentResolver().update(withAppendedPath2, contentValues, null, null);
        if (update > 0) {
            Cursor query = context.getContentResolver().query(withAppendedPath2, null, null, null, null);
            if (query != null) {
                if (query.moveToFirst()) {
                    i = query.getInt(query.getColumnIndex("contact_id"));
                } else {
                    i = 0;
                }
                query.close();
            } else {
                i = 0;
            }
            if (h > 0) {
                context.getContentResolver().update(Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, Constants.STR_EMPTY + i), contentValues, null, null);
            }
        }
        if (update > 0) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }

    public void T(Context context, c cVar) {
        int i;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        Uri withAppendedPath = Uri.withAppendedPath(ContactsContract.RawContacts.CONTENT_URI, Constants.STR_EMPTY + h);
        ContentValues contentValues = new ContentValues();
        contentValues.putNull("custom_ringtone");
        if (context.getContentResolver().update(withAppendedPath, contentValues, null, null) > 0) {
            Cursor query = context.getContentResolver().query(withAppendedPath, null, null, null, null);
            if (query != null) {
                if (query.moveToFirst()) {
                    i = query.getInt(query.getColumnIndex("contact_id"));
                } else {
                    i = 0;
                }
                query.close();
            } else {
                i = 0;
            }
            if (h > 0) {
                context.getContentResolver().update(Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, Constants.STR_EMPTY + i), contentValues, null, null);
            }
            cVar.a(0);
            return;
        }
        cVar.a(1);
    }

    public void U(Context context, c cVar) {
        Cursor query = context.getContentResolver().query(ContactsContract.Groups.CONTENT_URI, null, "deleted = 0", null, "_id");
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(count));
        boolean moveToFirst = query.moveToFirst();
        int i = 0;
        int i2 = 0;
        while (i < count && moveToFirst) {
            arrayList.add(r.a(query.getInt(query.getColumnIndex("_id"))));
            byte[] blob = query.getBlob(query.getColumnIndex("title"));
            if (blob == null) {
                blob = r.hr;
            }
            arrayList.add(blob);
            byte[] blob2 = query.getBlob(query.getColumnIndex("notes"));
            if (blob2 == null) {
                blob2 = r.hr;
            }
            arrayList.add(blob2);
            String string = query.getString(query.getColumnIndex("account_type"));
            String string2 = query.getString(query.getColumnIndex("account_name"));
            arrayList.add(r.a(string));
            arrayList.add(r.a(string2));
            i++;
            moveToFirst = query.moveToNext();
            i2++;
        }
        query.close();
        arrayList.set(0, r.a(i2));
        cVar.a(arrayList);
    }

    public void V(Context context, c cVar) {
        Cursor query = context.getContentResolver().query(ContactsContract.Groups.CONTENT_URI, null, "deleted = 0", null, "_id");
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(count));
        boolean moveToFirst = query.moveToFirst();
        int i = 0;
        int i2 = 0;
        while (i < count && moveToFirst) {
            arrayList.add(r.a(query.getInt(query.getColumnIndex("_id"))));
            byte[] blob = query.getBlob(query.getColumnIndex("title"));
            if (blob == null) {
                blob = r.hr;
            }
            arrayList.add(blob);
            byte[] blob2 = query.getBlob(query.getColumnIndex("notes"));
            if (blob2 == null) {
                blob2 = r.hr;
            }
            arrayList.add(blob2);
            i++;
            moveToFirst = query.moveToNext();
            i2++;
        }
        query.close();
        arrayList.set(0, r.a(i2));
        cVar.a(arrayList);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void W(Context context, c cVar) {
        int i;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        ContentValues contentValues = new ContentValues();
        contentValues.put("group_visible", (Integer) 1);
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(0));
        int i2 = 0;
        for (int i3 = 0; i3 < h; i3++) {
            String j = cVar.j();
            String j2 = cVar.j();
            String j3 = cVar.j();
            String j4 = cVar.j();
            if (r.b(j)) {
                j = Constants.STR_EMPTY;
            }
            contentValues.put("title", j);
            if (!r.b(j2)) {
                contentValues.put("notes", j2);
            }
            contentValues.put("group_visible", (Integer) 1);
            contentValues.put("should_sync", (Integer) 0);
            if (!r.b(j4)) {
                contentValues.put("account_name", j4);
            } else {
                contentValues.putNull("account_name");
            }
            if (!r.b(j3)) {
                contentValues.put("account_type", j3);
            } else {
                contentValues.putNull("account_type");
            }
            try {
                i = Integer.parseInt(context.getContentResolver().insert(ContactsContract.Groups.CONTENT_URI, contentValues).getLastPathSegment());
            } catch (Exception e2) {
                i = 0;
            }
            arrayList.add(r.a(i));
            i2++;
        }
        arrayList.set(0, r.a(i2));
        cVar.a(arrayList);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void X(Context context, c cVar) {
        int i;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        ContentValues contentValues = new ContentValues();
        contentValues.put("group_visible", (Integer) 1);
        a(context);
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(0));
        int i2 = 0;
        for (int i3 = 0; i3 < h; i3++) {
            String j = cVar.j();
            String j2 = cVar.j();
            if (r.b(j)) {
                j = Constants.STR_EMPTY;
            }
            contentValues.put("title", j);
            if (!r.b(j2)) {
                contentValues.put("notes", j2);
            }
            contentValues.put("group_visible", (Integer) 1);
            contentValues.put("should_sync", (Integer) 0);
            if (!r.b(this.d)) {
                contentValues.put("account_name", this.d);
            }
            if (!r.b(this.e)) {
                contentValues.put("account_type", this.e);
            }
            try {
                i = Integer.parseInt(context.getContentResolver().insert(ContactsContract.Groups.CONTENT_URI, contentValues).getLastPathSegment());
            } catch (Exception e2) {
                i = 0;
            }
            arrayList.add(r.a(i));
            i2++;
        }
        arrayList.set(0, r.a(i2));
        cVar.a(arrayList);
    }

    public void Y(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        for (int i = 0; i < h; i++) {
            int h2 = cVar.h();
            int h3 = cVar.h();
            if (h3 > 0) {
                ContentValues[] contentValuesArr = new ContentValues[h3];
                for (int i2 = 0; i2 < h3; i2++) {
                    int h4 = cVar.h();
                    contentValuesArr[i2] = new ContentValues();
                    contentValuesArr[i2].put("data1", Integer.valueOf(h2));
                    contentValuesArr[i2].put("mimetype", "vnd.android.cursor.item/group_membership");
                    contentValuesArr[i2].put("raw_contact_id", Integer.valueOf(h4));
                }
                context.getContentResolver().bulkInsert(ContactsContract.Data.CONTENT_URI, contentValuesArr);
            }
        }
        cVar.a(0);
    }

    public void Z(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(8);
            return;
        }
        int h = cVar.h();
        int i = 0;
        while (i < h) {
            try {
                aa(context, cVar);
                i++;
            } catch (Exception e2) {
                cVar.a(8);
                return;
            }
        }
        cVar.a(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void aa(Context context, c cVar) {
        Uri uri;
        Uri uri2;
        int i;
        int i2;
        int i3 = 1;
        Uri uri3 = null;
        int h = cVar.h();
        int h2 = cVar.h();
        int i4 = h % 4;
        int i5 = h / 4;
        if (i5 == 0) {
            if (i4 == 3) {
                context.getContentResolver().delete(ContactsContract.RawContacts.CONTENT_URI, null, null);
            } else if (i4 == 1) {
                String j = cVar.j();
                String j2 = cVar.j();
                int h3 = cVar.h();
                int h4 = cVar.h();
                int h5 = cVar.h();
                if (h4 > 0) {
                    if (h5 > 0) {
                        uri3 = Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h4);
                    } else {
                        uri3 = Uri.withAppendedPath(MediaStore.Audio.Media.INTERNAL_CONTENT_URI, Constants.STR_EMPTY + h4);
                    }
                }
                ContentValues contentValues = new ContentValues();
                contentValues.put("starred", Integer.valueOf(h3));
                if (uri3 != null) {
                    contentValues.put("custom_ringtone", uri3.toString());
                }
                contentValues.put("deleted", (Integer) 0);
                contentValues.put("aggregation_mode", (Integer) 2);
                Uri insert = context.getContentResolver().insert(ContactsContract.RawContacts.CONTENT_URI, contentValues);
                contentValues.clear();
                try {
                    int parseInt = Integer.parseInt(insert.getLastPathSegment());
                    if (!r.b(j)) {
                        i2 = 1;
                    } else {
                        i2 = 0;
                    }
                    if (!r.b(j2)) {
                        i2++;
                    }
                    ContentValues[] contentValuesArr = new ContentValues[i2];
                    if (!r.b(j)) {
                        contentValuesArr[0] = new ContentValues();
                        contentValuesArr[0].put("mimetype", "vnd.android.cursor.item/name");
                        contentValuesArr[0].put("raw_contact_id", Integer.valueOf(parseInt));
                        contentValuesArr[0].put("data1", j);
                    } else {
                        i3 = 0;
                    }
                    if (!r.b(j2)) {
                        contentValuesArr[i3] = new ContentValues();
                        contentValuesArr[i3].put("mimetype", "vnd.android.cursor.item/note");
                        contentValuesArr[i3].put("raw_contact_id", Integer.valueOf(parseInt));
                        contentValuesArr[i3].put("data1", j2);
                        int i6 = i3 + 1;
                    }
                    context.getContentResolver().bulkInsert(ContactsContract.Data.CONTENT_URI, contentValuesArr);
                } catch (Exception e2) {
                }
            } else if (i4 == 2) {
                String j3 = cVar.j();
                String j4 = cVar.j();
                int h6 = cVar.h();
                int h7 = cVar.h();
                int h8 = cVar.h();
                if (h7 <= 0) {
                    uri2 = null;
                } else if (h8 > 0) {
                    uri2 = Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h7);
                } else {
                    uri2 = Uri.withAppendedPath(MediaStore.Audio.Media.INTERNAL_CONTENT_URI, Constants.STR_EMPTY + h7);
                }
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("starred", Integer.valueOf(h6));
                if (uri2 != null) {
                    contentValues2.put("custom_ringtone", uri2.toString());
                } else {
                    contentValues2.putNull("custom_ringtone");
                }
                contentValues2.put("deleted", (Integer) 0);
                contentValues2.put("aggregation_mode", (Integer) 2);
                context.getContentResolver().update(Uri.withAppendedPath(ContactsContract.RawContacts.CONTENT_URI, Constants.STR_EMPTY + h2), contentValues2, null, null);
                context.getContentResolver().delete(ContactsContract.Data.CONTENT_URI, "raw_contact_id = " + h2 + " and (" + "mimetype" + " = '" + "vnd.android.cursor.item/name" + " ' or" + "mimetype" + " = '" + "vnd.android.cursor.item/note" + "'" + ")", null);
                if (!r.b(j3)) {
                    i = 1;
                } else {
                    i = 0;
                }
                if (!r.b(j4)) {
                    i++;
                }
                ContentValues[] contentValuesArr2 = new ContentValues[i];
                if (!r.b(j3)) {
                    contentValuesArr2[0] = new ContentValues();
                    contentValuesArr2[0].put("mimetype", "vnd.android.cursor.item/name");
                    contentValuesArr2[0].put("raw_contact_id", Integer.valueOf(h2));
                    contentValuesArr2[0].put("data1", j3);
                } else {
                    i3 = 0;
                }
                if (!r.b(j4)) {
                    contentValuesArr2[i3] = new ContentValues();
                    contentValuesArr2[i3].put("mimetype", "vnd.android.cursor.item/note");
                    contentValuesArr2[i3].put("raw_contact_id", Integer.valueOf(h2));
                    contentValuesArr2[i3].put("data1", j4);
                    int i7 = i3 + 1;
                }
                context.getContentResolver().bulkInsert(ContactsContract.Data.CONTENT_URI, contentValuesArr2);
            }
        } else if (i5 == 1) {
            if (i4 == 3) {
                context.getContentResolver().delete(ContactsContract.Data.CONTENT_URI, "raw_contact_id = " + h2 + " and " + "mimetype" + " = " + "vnd.android.cursor.item/photo", null);
            } else if (i4 == 1) {
                byte[] k = cVar.k();
                ContentValues contentValues3 = new ContentValues();
                contentValues3.put("mimetype", "vnd.android.cursor.item/photo");
                contentValues3.put("raw_contact_id", Constants.STR_EMPTY + h2);
                contentValues3.put("data15", k);
                context.getContentResolver().insert(ContactsContract.Data.CONTENT_URI, contentValues3);
            } else if (i4 == 2) {
                byte[] k2 = cVar.k();
                ContentValues contentValues4 = new ContentValues();
                contentValues4.put("mimetype", "vnd.android.cursor.item/photo");
                contentValues4.put("raw_contact_id", Constants.STR_EMPTY + h2);
                contentValues4.put("data15", k2);
                Cursor query = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, "raw_contact_id = " + h2 + " and " + "mimetype" + " = " + "vnd.android.cursor.item/photo", null, null);
                if (query != null) {
                    if (query.moveToFirst()) {
                        uri = Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, Constants.STR_EMPTY + query.getInt(query.getColumnIndex("_id")));
                    } else {
                        uri = null;
                    }
                    query.close();
                    if (uri != null) {
                        context.getContentResolver().update(uri, contentValues4, null, null);
                    } else {
                        context.getContentResolver().insert(ContactsContract.Data.CONTENT_URI, contentValues4);
                    }
                }
            }
        } else if (i5 == 2) {
            int h9 = cVar.h();
            if (i4 == 3) {
                context.getContentResolver().delete(Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, Constants.STR_EMPTY + h9), null, null);
            } else if (i4 == 1) {
                int h10 = cVar.h();
                String j5 = cVar.j();
                String j6 = cVar.j();
                ContentValues contentValues5 = new ContentValues();
                contentValues5.put("raw_contact_id", Integer.valueOf(h2));
                contentValues5.put("data2", Integer.valueOf(h10));
                contentValues5.put("mimetype", "vnd.android.cursor.item/phone_v2");
                contentValues5.put("raw_contact_id", Integer.valueOf(h2));
                if (!r.b(j5)) {
                    contentValues5.put("data3", j5);
                }
                if (!r.b(j6)) {
                    contentValues5.put("data1", j6);
                }
                context.getContentResolver().insert(ContactsContract.Data.CONTENT_URI, contentValues5);
            } else if (i4 == 2) {
                int h11 = cVar.h();
                String j7 = cVar.j();
                String j8 = cVar.j();
                ContentValues contentValues6 = new ContentValues();
                contentValues6.put("raw_contact_id", Integer.valueOf(h2));
                contentValues6.put("data2", Integer.valueOf(h11));
                contentValues6.put("mimetype", "vnd.android.cursor.item/phone_v2");
                if (!r.b(j7)) {
                    contentValues6.put("data3", j7);
                } else {
                    contentValues6.putNull("data3");
                }
                if (!r.b(j8)) {
                    contentValues6.put("data1", j8);
                } else {
                    contentValues6.putNull("data1");
                }
                context.getContentResolver().update(Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, Constants.STR_EMPTY + h9), contentValues6, null, null);
            }
        } else if (i5 == 3) {
            int h12 = cVar.h();
            if (i4 == 3) {
                context.getContentResolver().delete(Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, Constants.STR_EMPTY + h12), null, null);
            } else if (i4 == 1) {
                int h13 = cVar.h();
                int h14 = cVar.h();
                String j9 = cVar.j();
                String j10 = cVar.j();
                String j11 = cVar.j();
                ContentValues contentValues7 = new ContentValues();
                contentValues7.put("raw_contact_id", Integer.valueOf(h2));
                if (h13 == 2) {
                    contentValues7.put("mimetype", "vnd.android.cursor.item/postal-address_v2");
                } else if (h13 == 1) {
                    contentValues7.put("mimetype", "vnd.android.cursor.item/email_v2");
                } else if (h13 == 3) {
                    contentValues7.put("mimetype", "vnd.android.cursor.item/im");
                }
                contentValues7.put("data2", Integer.valueOf(h14));
                if (h14 == 0 && !r.b(j9)) {
                    contentValues7.put("data3", j9);
                }
                int a2 = a(j10);
                if (h13 == 3) {
                    contentValues7.put("data5", Integer.valueOf(a2));
                }
                if (h13 == 3 && a2 == -1 && !r.b(j9)) {
                    contentValues7.put("data6", j9);
                }
                if (!r.b(j11)) {
                    contentValues7.put("data1", j10);
                }
                context.getContentResolver().insert(ContactsContract.Data.CONTENT_URI, contentValues7);
            } else if (i4 == 2) {
                int h15 = cVar.h();
                int h16 = cVar.h();
                String j12 = cVar.j();
                String j13 = cVar.j();
                String j14 = cVar.j();
                ContentValues contentValues8 = new ContentValues();
                contentValues8.put("raw_contact_id", Integer.valueOf(h2));
                if (h15 == 2) {
                    contentValues8.put("mimetype", "vnd.android.cursor.item/postal-address_v2");
                } else if (h15 == 1) {
                    contentValues8.put("mimetype", "vnd.android.cursor.item/email_v2");
                } else if (h15 == 3) {
                    contentValues8.put("mimetype", "vnd.android.cursor.item/im");
                }
                contentValues8.put("data2", Integer.valueOf(h16));
                if (h16 == 0 && !r.b(j12)) {
                    contentValues8.put("data3", j12);
                }
                int a3 = a(j13);
                if (h15 == 3) {
                    contentValues8.put("data5", Integer.valueOf(a3));
                }
                if (h15 == 3 && a3 == -1 && !r.b(j12)) {
                    contentValues8.put("data6", j12);
                }
                if (!r.b(j14)) {
                    contentValues8.put("data1", j13);
                }
                context.getContentResolver().update(Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, Constants.STR_EMPTY + h2), contentValues8, null, null);
            }
        } else if (i5 == 4) {
            int h17 = cVar.h();
            if (i4 == 3) {
                context.getContentResolver().delete(Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, Constants.STR_EMPTY + h17), null, null);
            } else if (i4 == 1) {
                int h18 = cVar.h();
                String j15 = cVar.j();
                String j16 = cVar.j();
                String j17 = cVar.j();
                ContentValues contentValues9 = new ContentValues();
                contentValues9.put("raw_contact_id", Integer.valueOf(h2));
                contentValues9.put("mimetype", "vnd.android.cursor.item/organization");
                contentValues9.put("data2", Integer.valueOf(h18));
                if (h18 == 0 && !r.b(j15)) {
                    contentValues9.put("data3", j15);
                }
                if (!r.b(j16)) {
                    contentValues9.put("data1", j16);
                }
                if (!r.b(j17)) {
                    contentValues9.put("data4", j17);
                }
                context.getContentResolver().insert(ContactsContract.Data.CONTENT_URI, contentValues9);
            } else if (i4 == 2) {
                int h19 = cVar.h();
                String j18 = cVar.j();
                String j19 = cVar.j();
                String j20 = cVar.j();
                ContentValues contentValues10 = new ContentValues();
                contentValues10.put("raw_contact_id", Integer.valueOf(h2));
                contentValues10.put("mimetype", "vnd.android.cursor.item/organization");
                contentValues10.put("data2", Integer.valueOf(h19));
                if (h19 != 0 || r.b(j18)) {
                    contentValues10.putNull("data3");
                } else {
                    contentValues10.put("data3", j18);
                }
                if (!r.b(j19)) {
                    contentValues10.put("data1", j19);
                } else {
                    contentValues10.putNull("data1");
                }
                if (!r.b(j20)) {
                    contentValues10.put("data4", j20);
                } else {
                    contentValues10.putNull("data4");
                }
                context.getContentResolver().insert(Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, Constants.STR_EMPTY + h17), contentValues10);
            }
        }
    }

    public void ab(Context context, c cVar) {
        if (cVar.e() < 3) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        String j = cVar.j();
        String j2 = cVar.j();
        ContentValues contentValues = new ContentValues();
        if (r.b(j)) {
            contentValues.putNull("account_type");
        } else {
            contentValues.put("account_type", j);
        }
        if (r.b(j2)) {
            contentValues.putNull("account_name");
        } else {
            contentValues.put("account_name", j2);
        }
        if (context.getContentResolver().update(Uri.withAppendedPath(ContactsContract.Groups.CONTENT_URI, Constants.STR_EMPTY + h), contentValues, null, null) <= 0) {
            cVar.a(1);
        }
    }

    public void ac(Context context, c cVar) {
        if (cVar.e() < 3) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        String j = cVar.j();
        String j2 = cVar.j();
        ContentValues contentValues = new ContentValues();
        if (r.b(j)) {
            contentValues.putNull("account_type");
        } else {
            contentValues.put("account_type", j);
        }
        if (r.b(j2)) {
            contentValues.putNull("account_name");
        } else {
            contentValues.put("account_name", j2);
        }
        if (context.getContentResolver().update(Uri.withAppendedPath(ContactsContract.RawContacts.CONTENT_URI, Constants.STR_EMPTY + h), contentValues, null, null) <= 0) {
            cVar.a(1);
        }
    }

    public static int b(Context context) {
        Cursor query = context.getContentResolver().query(ContactsContract.RawContacts.CONTENT_URI, com.qq.util.j.b, "deleted =  0", null, null);
        if (query == null) {
            return -1;
        }
        int count = query.getCount();
        query.close();
        return count;
    }
}
