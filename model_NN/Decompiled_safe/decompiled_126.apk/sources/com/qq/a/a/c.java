package com.qq.a.a;

import android.media.MediaScannerConnection;
import android.net.Uri;
import com.qq.provider.h;

/* compiled from: ProGuard */
class c implements MediaScannerConnection.MediaScannerConnectionClient {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f257a;
    private int b = 0;

    c(b bVar) {
        this.f257a = bVar;
    }

    public void onMediaScannerConnected() {
        if (this.f257a.d != null) {
            try {
                this.f257a.b.scanFile(this.f257a.d, this.f257a.e);
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        if (this.f257a.f != null) {
            try {
                for (String scanFile : this.f257a.f) {
                    this.f257a.b.scanFile(scanFile, this.f257a.e);
                }
            } catch (Throwable th2) {
                th2.printStackTrace();
            }
        }
        String unused = this.f257a.d = (String) null;
        String unused2 = this.f257a.e = null;
        String[] unused3 = this.f257a.f = (String[]) null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.a.a.b.a(com.qq.a.a.b, boolean):boolean
     arg types: [com.qq.a.a.b, int]
     candidates:
      com.qq.a.a.b.a(com.qq.a.a.b, android.net.Uri):android.net.Uri
      com.qq.a.a.b.a(com.qq.a.a.b, java.lang.String):java.lang.String
      com.qq.a.a.b.a(com.qq.a.a.b, java.lang.String[]):java.lang.String[]
      com.qq.a.a.b.a(java.lang.String, java.lang.String):void
      com.qq.a.a.b.a(java.lang.String[], java.lang.String):void
      com.qq.a.a.b.a(com.qq.a.a.b, boolean):boolean */
    public void onScanCompleted(String str, Uri uri) {
        int i;
        int i2 = 0;
        if (this.f257a.d != null) {
            this.f257a.b.disconnect();
        } else if (this.f257a.f != null && this.f257a.f.length > 0) {
            this.b++;
            if (this.b == this.f257a.f.length) {
                this.f257a.b.disconnect();
            }
        }
        this.f257a.f256a = str;
        Uri unused = this.f257a.g = uri;
        boolean unused2 = this.f257a.h = true;
        if (h.b) {
            try {
                i = Integer.parseInt(this.f257a.g.getLastPathSegment());
            } catch (Throwable th) {
                i = 0;
            }
            String uri2 = this.f257a.g.toString();
            if (i > 0) {
                if (uri2.contains("image")) {
                    i2 = 1;
                } else if (uri2.contains("video")) {
                    i2 = 3;
                } else if (uri2.contains("audio")) {
                    i2 = 2;
                }
                if (i2 > 0) {
                    h.b(i2, i);
                }
            }
        }
    }
}
