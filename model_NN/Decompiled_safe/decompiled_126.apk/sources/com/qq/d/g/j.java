package com.qq.d.g;

import android.content.Context;
import android.os.Build;
import android.os.SystemProperties;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class j {
    public static String a(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), "android_id");
    }

    public static String a() {
        return "MIUI-" + b();
    }

    public static String b() {
        return Build.VERSION.INCREMENTAL;
    }

    public static String c() {
        return Build.VERSION.RELEASE;
    }

    public static String d() {
        String str = SystemProperties.get("ro.product.mod_device");
        if (TextUtils.isEmpty(str)) {
            return Build.DEVICE;
        }
        return str;
    }

    public static String b(Context context) {
        if (b.c || !c(context)) {
            return "X";
        }
        return "S";
    }

    public static boolean c(Context context) {
        boolean z;
        if (Settings.System.getInt(context.getContentResolver(), "update_stable_version_only", 0) != 1) {
            z = false;
        } else {
            z = true;
        }
        if (z) {
            return true;
        }
        return false;
    }

    public static String d(Context context) {
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        if (TextUtils.isEmpty(deviceId)) {
            return SystemProperties.get("ro.ril.miui.imei", Constants.STR_EMPTY);
        }
        return deviceId;
    }
}
