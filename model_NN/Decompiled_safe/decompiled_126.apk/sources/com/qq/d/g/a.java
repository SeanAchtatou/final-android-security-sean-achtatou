package com.qq.d.g;

import android.os.SystemProperties;
import android.text.TextUtils;
import com.tencent.connect.common.Constants;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: ProGuard */
public class a extends r {
    public boolean a(Map<String, String> map, boolean z, s sVar) {
        String str;
        String str2;
        boolean z2;
        boolean z3;
        String str3 = SystemProperties.get("ro.baidu.build.version.release");
        String str4 = SystemProperties.get("ro.product.manufacturer");
        String str5 = SystemProperties.get("ro.version.type");
        String str6 = SystemProperties.get("ro.build.version.incremental");
        HashMap hashMap = new HashMap();
        hashMap.put("1", "alpha");
        hashMap.put("2", "beta");
        hashMap.put("3", "release");
        hashMap.put("s", "release");
        hashMap.put("r", "beta");
        if (TextUtils.isEmpty(str5)) {
            str = Constants.STR_EMPTY;
        } else {
            str = (String) hashMap.get(str5);
        }
        if (!TextUtils.isEmpty(str3) || (!TextUtils.isEmpty(str4) && str4.toLowerCase().equals("baidu"))) {
            Matcher matcher = Pattern.compile("(\\w*)_(\\w*)_([\\d\\.]+)").matcher(str6);
            if (matcher.find()) {
                if (TextUtils.isEmpty(str)) {
                    str = (String) hashMap.get(matcher.group(2).toLowerCase());
                }
                String[] split = matcher.group(3).split("\\.");
                if (split != null && split.length >= 3) {
                    String str7 = split[2];
                }
                str2 = str6;
                z2 = true;
            } else {
                str2 = str6;
                z2 = true;
            }
        } else {
            str2 = str6;
            z2 = false;
        }
        if (!z && z2) {
            a(map, "rombrand", "baiduyun");
            a(map, "romversion", str2);
            a(map, "rombranch", str);
        }
        if (this.b == null) {
            return z2;
        }
        r rVar = this.b;
        if (z || z2) {
            z3 = true;
        } else {
            z3 = false;
        }
        boolean a2 = rVar.a(map, z3, sVar);
        if (z2 || a2) {
            return true;
        }
        return false;
    }
}
