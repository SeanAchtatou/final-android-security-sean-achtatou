package com.qq.AppService;

import com.qq.AppService.AppService;
import com.qq.l.p;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.plugin.LoginStateCallback;
import com.tencent.assistant.plugin.PluginIPCClient;
import com.tencent.assistant.plugin.UserLoginInfo;
import com.tencent.assistant.plugin.UserStateInfo;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class h implements LoginStateCallback {

    /* renamed from: a  reason: collision with root package name */
    public String f241a;
    public String b;
    public boolean c = false;
    public final Object d = new Object();
    final /* synthetic */ AppService e;

    public h(AppService appService) {
        this.e = appService;
    }

    public void a(String str, String str2) {
        this.f241a = str;
        this.b = str2;
        this.c = false;
    }

    public void gotUserInfo(UserLoginInfo userLoginInfo) {
        XLog.d("Longer", "QueryLoginInfoCallback.gotUserInfo() user = " + this.f241a + ",  cmd = " + this.b + ", get user info = " + userLoginInfo);
        if (!PluginIPCClient.LOGIN_ADDTION_INFO_GET_USER_INFO.equals(this.b)) {
            if (PluginIPCClient.LOGIN_ADDTION_INFO_LOGIN.equals(this.b)) {
            }
        } else if ("activity".equals(this.f241a)) {
            int state = userLoginInfo.getState();
            if (state != 2 && state != 3) {
                UserLoginInfo unused = this.e.x = null;
                AppService.k = null;
            } else if (this.e.x == null) {
                this.e.S();
                this.e.a(userLoginInfo);
            } else if (this.e.x.getUin() != userLoginInfo.getUin()) {
                this.e.b(this.e.x);
                this.e.a(userLoginInfo);
                this.e.S();
            } else {
                this.e.S();
                this.e.x.setA2(userLoginInfo.getA2());
                this.e.x.setNickName(userLoginInfo.getNickName());
                this.e.x.setPic(userLoginInfo.getPic());
                this.e.x.setState(userLoginInfo.getState());
                this.e.x.setUin(userLoginInfo.getUin());
            }
            AstApp.i().j().sendEmptyMessage(EventDispatcherEnum.CONNECTION_EVENT_QUERY_LOGIN_INFO);
        } else if ("service".equals(this.f241a)) {
            int state2 = userLoginInfo.getState();
            if (state2 == 2 || state2 == 3) {
                this.e.a(userLoginInfo);
            } else {
                this.e.b(this.e.x);
            }
        }
    }

    public void gotUserStateChange(UserStateInfo userStateInfo) {
        XLog.d("Longer", "QueryLoginInfoCallback.gotUserStateChange() user = " + this.f241a + ", cmd = " + this.b + ", get state info = " + userStateInfo);
        if (userStateInfo.getStateChangeType() == 4) {
            this.e.b(this.e.x);
            if (this.e.q == AppService.BusinessConnectionType.QQ) {
                if (AppService.u()) {
                    AppService.j();
                }
                if (AppService.q()) {
                    AppService.i();
                }
            }
        }
        UserLoginInfo userLoginInfo = userStateInfo.getUserLoginInfo();
        if (userLoginInfo != null) {
            int stateChangeType = userStateInfo.getStateChangeType();
            if (stateChangeType == 2 || stateChangeType == 3) {
                p.m().c(601, 1, -1);
                if (this.e.x == null) {
                    this.c = true;
                    this.e.a(userLoginInfo);
                } else if (!(this.e.x == null || this.e.x.getUin() == userLoginInfo.getUin())) {
                    this.e.b(this.e.x);
                    this.c = true;
                    this.e.a(userLoginInfo);
                }
            } else if (stateChangeType == 0 || stateChangeType == 1) {
                this.e.b(userLoginInfo);
                if (stateChangeType == 0) {
                    p.m().c(601, 3, -1);
                } else if (stateChangeType == 1) {
                    p.m().c(601, 2, -1);
                }
            }
            if ("activity".equals(this.f241a)) {
                synchronized (this.d) {
                    while (this.c) {
                        try {
                            this.d.wait(5000);
                        } catch (InterruptedException e2) {
                            e2.printStackTrace();
                        }
                    }
                }
                XLog.d("Longer", "sendMsg to UI thread");
                this.c = false;
                AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.CONNECTION_EVENT_LOGIN_QQ_RESULT, userStateInfo));
            }
        }
    }
}
