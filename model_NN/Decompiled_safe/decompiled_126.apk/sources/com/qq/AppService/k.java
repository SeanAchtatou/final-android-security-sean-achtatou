package com.qq.AppService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class k extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AstApp f244a;

    k(AstApp astApp) {
        this.f244a = astApp;
    }

    public void onReceive(Context context, Intent intent) {
        String action;
        if (intent != null && (action = intent.getAction()) != null && action.equals("com.tencent.android.qqdownloader.action.QUERY_CONNECT_STATE")) {
            int h = this.f244a.h();
            Intent intent2 = new Intent("com.tencent.android.qqdownloader.action.CONNECT_PC_STATE");
            intent2.putExtra("pc_state_result", h);
            intent2.putExtra("pc_connect_name", TextUtils.isEmpty(AppService.h) ? Constants.STR_EMPTY : AppService.h);
            context.sendBroadcast(intent2);
        }
    }
}
