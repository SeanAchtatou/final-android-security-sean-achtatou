package com.qq.AppService;

import android.app.Notification;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.tencent.tmsecurelite.optimize.ISystemOptimize;

/* compiled from: ProGuard */
public abstract class x extends Binder implements w {
    public x() {
        attachInterface(this, "com.qq.AppService.IMoloService");
    }

    public static w a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.qq.AppService.IMoloService");
        if (queryLocalInterface == null || !(queryLocalInterface instanceof w)) {
            return new y(iBinder);
        }
        return (w) queryLocalInterface;
    }

    public IBinder asBinder() {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        ParcelObject parcelObject;
        Notification notification;
        Notification notification2;
        int i3 = 0;
        switch (i) {
            case 1:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                boolean a2 = a();
                parcel2.writeNoException();
                parcel2.writeInt(a2 ? 1 : 0);
                return true;
            case 2:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                boolean b = b();
                parcel2.writeNoException();
                if (b) {
                    i3 = 1;
                }
                parcel2.writeInt(i3);
                return true;
            case 3:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                boolean c = c();
                parcel2.writeNoException();
                if (c) {
                    i3 = 1;
                }
                parcel2.writeInt(i3);
                return true;
            case 4:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                int d = d();
                parcel2.writeNoException();
                parcel2.writeInt(d);
                return true;
            case 5:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                int a3 = a(parcel.readInt());
                parcel2.writeNoException();
                parcel2.writeInt(a3);
                return true;
            case 6:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                boolean a4 = a(parcel.readString());
                parcel2.writeNoException();
                if (a4) {
                    i3 = 1;
                }
                parcel2.writeInt(i3);
                return true;
            case 7:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                a(aa.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 8:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                e();
                parcel2.writeNoException();
                return true;
            case 9:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                int f = f();
                parcel2.writeNoException();
                parcel2.writeInt(f);
                return true;
            case 10:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                g();
                parcel2.writeNoException();
                return true;
            case 11:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                int h = h();
                parcel2.writeNoException();
                parcel2.writeInt(h);
                return true;
            case 12:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                i();
                parcel2.writeNoException();
                return true;
            case 13:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                j();
                parcel2.writeNoException();
                return true;
            case 14:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                k();
                parcel2.writeNoException();
                return true;
            case 15:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                l();
                parcel2.writeNoException();
                return true;
            case 16:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                int m = m();
                parcel2.writeNoException();
                parcel2.writeInt(m);
                return true;
            case 17:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                String n = n();
                parcel2.writeNoException();
                parcel2.writeString(n);
                return true;
            case 18:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                String o = o();
                parcel2.writeNoException();
                parcel2.writeString(o);
                return true;
            case 19:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                String p = p();
                parcel2.writeNoException();
                parcel2.writeString(p);
                return true;
            case 20:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                a(parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 21:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                b(parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case ISystemOptimize.T_killTaskAsync /*22*/:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                if (parcel.readInt() != 0) {
                    notification = (Notification) Notification.CREATOR.createFromParcel(parcel);
                } else {
                    notification = null;
                }
                if (parcel.readInt() != 0) {
                    notification2 = (Notification) Notification.CREATOR.createFromParcel(parcel);
                } else {
                    notification2 = null;
                }
                a(notification, notification2);
                parcel2.writeNoException();
                return true;
            case 23:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                Notification q = q();
                parcel2.writeNoException();
                if (q != null) {
                    parcel2.writeInt(1);
                    q.writeToParcel(parcel2, 1);
                    return true;
                }
                parcel2.writeInt(0);
                return true;
            case ISystemOptimize.T_checkVersionAsync /*24*/:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                Notification r = r();
                parcel2.writeNoException();
                if (r != null) {
                    parcel2.writeInt(1);
                    r.writeToParcel(parcel2, 1);
                    return true;
                }
                parcel2.writeInt(0);
                return true;
            case ISystemOptimize.T_hasRootAsync /*25*/:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                boolean s = s();
                parcel2.writeNoException();
                if (s) {
                    i3 = 1;
                }
                parcel2.writeInt(i3);
                return true;
            case ISystemOptimize.T_askForRootAsync /*26*/:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                boolean t = t();
                parcel2.writeNoException();
                if (t) {
                    i3 = 1;
                }
                parcel2.writeInt(i3);
                return true;
            case ISystemOptimize.T_getMemoryUsageAsync /*27*/:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                boolean u = u();
                parcel2.writeNoException();
                if (u) {
                    i3 = 1;
                }
                parcel2.writeInt(i3);
                return true;
            case ISystemOptimize.T_isMemoryReachWarningAsync /*28*/:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                boolean v = v();
                parcel2.writeNoException();
                if (v) {
                    i3 = 1;
                }
                parcel2.writeInt(i3);
                return true;
            case ISystemOptimize.T_getSysRubbishAsync /*29*/:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                boolean w = w();
                parcel2.writeNoException();
                if (w) {
                    i3 = 1;
                }
                parcel2.writeInt(i3);
                return true;
            case 30:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                b(parcel.readString());
                parcel2.writeNoException();
                return true;
            case 31:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                x();
                parcel2.writeNoException();
                return true;
            case 32:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                String y = y();
                parcel2.writeNoException();
                parcel2.writeString(y);
                return true;
            case ISystemOptimize.T_cancelScanRubbish /*33*/:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                int z = z();
                parcel2.writeNoException();
                parcel2.writeInt(z);
                return true;
            case ISystemOptimize.T_cleanRubbishAsync /*34*/:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                long a5 = a(parcel.createIntArray());
                parcel2.writeNoException();
                parcel2.writeLong(a5);
                return true;
            case 35:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                int readInt = parcel.readInt();
                String readString = parcel.readString();
                if (parcel.readInt() != 0) {
                    parcelObject = ParcelObject.CREATOR.createFromParcel(parcel);
                } else {
                    parcelObject = null;
                }
                a(readInt, readString, parcelObject);
                parcel2.writeNoException();
                if (parcelObject != null) {
                    parcel2.writeInt(1);
                    parcelObject.writeToParcel(parcel2, 1);
                    return true;
                }
                parcel2.writeInt(0);
                return true;
            case 36:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                int c2 = c(parcel.readString());
                parcel2.writeNoException();
                parcel2.writeInt(c2);
                return true;
            case 37:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                String A = A();
                parcel2.writeNoException();
                parcel2.writeString(A);
                return true;
            case 38:
                parcel.enforceInterface("com.qq.AppService.IMoloService");
                String B = B();
                parcel2.writeNoException();
                parcel2.writeString(B);
                return true;
            case 1598968902:
                parcel2.writeString("com.qq.AppService.IMoloService");
                return true;
            default:
                return super.onTransact(i, parcel, parcel2, i2);
        }
    }
}
