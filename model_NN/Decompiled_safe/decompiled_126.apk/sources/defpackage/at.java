package defpackage;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;

/* renamed from: at  reason: default package */
public final class at {
    public static String a() {
        return Build.MODEL;
    }

    public static String a(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
    }

    public static String b() {
        return Build.PRODUCT;
    }

    public static String b(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
    }

    public static int c() {
        return Integer.parseInt(Build.VERSION.SDK);
    }

    public static String c(Context context) {
        WifiInfo connectionInfo = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo();
        if (connectionInfo != null) {
            return connectionInfo.getMacAddress();
        }
        return null;
    }

    public static String d() {
        return "android_id";
    }

    public static String d(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getSimSerialNumber();
    }
}
