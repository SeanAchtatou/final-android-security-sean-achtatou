package android.content.a;

import java.io.IOException;

/* compiled from: ProGuard */
class c {
    c() {
    }

    public static final void a(d dVar, int i) {
        int b = dVar.b();
        if (b != i) {
            throw new IOException("Expected chunk of type 0x" + Integer.toHexString(i) + ", read 0x" + Integer.toHexString(b) + ".");
        }
    }
}
