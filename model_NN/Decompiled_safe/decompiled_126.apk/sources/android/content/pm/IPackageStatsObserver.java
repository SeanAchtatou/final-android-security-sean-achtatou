package android.content.pm;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* compiled from: ProGuard */
public interface IPackageStatsObserver extends IInterface {
    void onGetStatsCompleted(PackageStats packageStats, boolean z);

    /* compiled from: ProGuard */
    public abstract class Stub extends Binder implements IPackageStatsObserver {
        public Stub() {
            attachInterface(this, "android.content.pm.IPackageStatsObserver");
        }

        public static IPackageStatsObserver asInterface(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("android.content.pm.IPackageStatsObserver");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IPackageStatsObserver)) {
                return new d(iBinder);
            }
            return (IPackageStatsObserver) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            PackageStats packageStats;
            switch (i) {
                case 1:
                    parcel.enforceInterface("android.content.pm.IPackageStatsObserver");
                    if (parcel.readInt() != 0) {
                        packageStats = (PackageStats) PackageStats.CREATOR.createFromParcel(parcel);
                    } else {
                        packageStats = null;
                    }
                    onGetStatsCompleted(packageStats, parcel.readInt() != 0);
                    return true;
                case 1598968902:
                    parcel2.writeString("android.content.pm.IPackageStatsObserver");
                    return true;
                default:
                    return super.onTransact(i, parcel, parcel2, i2);
            }
        }
    }
}
