package android.os;

import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.util.Log;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class Process {
    public static final String ANDROID_SHARED_MEDIA = "com.android.process.media";
    public static final int BLUETOOTH_UID = 1002;
    public static final int DRM_UID = 1019;
    public static final int FIRST_APPLICATION_UID = 10000;
    public static final int FIRST_ISOLATED_UID = 99000;
    public static final int FIRST_SHARED_APPLICATION_GID = 50000;
    public static final String GOOGLE_SHARED_APP_CONTENT = "com.google.process.content";
    public static final int LAST_APPLICATION_UID = 19999;
    public static final int LAST_ISOLATED_UID = 99999;
    public static final int LAST_SHARED_APPLICATION_GID = 59999;
    public static final int LOG_UID = 1007;
    public static final int MEDIA_RW_GID = 1023;
    public static final int MEDIA_UID = 1013;
    public static final int NFC_UID = 1027;
    public static final int PACKAGE_INFO_GID = 1032;
    public static final int PHONE_UID = 1001;
    public static final int PROC_COMBINE = 256;
    public static final int PROC_OUT_FLOAT = 16384;
    public static final int PROC_OUT_LONG = 8192;
    public static final int PROC_OUT_STRING = 4096;
    public static final int PROC_PARENS = 512;
    public static final int PROC_QUOTES = 1024;
    public static final int PROC_SPACE_TERM = 32;
    public static final int PROC_TAB_TERM = 9;
    public static final int PROC_TERM_MASK = 255;
    public static final int PROC_ZERO_TERM = 0;
    public static final int SCHED_BATCH = 3;
    public static final int SCHED_FIFO = 1;
    public static final int SCHED_IDLE = 5;
    public static final int SCHED_OTHER = 0;
    public static final int SCHED_RR = 2;
    public static final int SHELL_UID = 2000;
    public static final int SIGNAL_KILL = 9;
    public static final int SIGNAL_QUIT = 3;
    public static final int SIGNAL_USR1 = 10;
    public static final int SYSTEM_UID = 1000;
    public static final int THREAD_GROUP_AUDIO_APP = 3;
    public static final int THREAD_GROUP_AUDIO_SYS = 4;
    public static final int THREAD_GROUP_BG_NONINTERACTIVE = 0;
    public static final int THREAD_GROUP_DEFAULT = -1;
    public static final int THREAD_GROUP_SYSTEM = 2;
    public static final int THREAD_PRIORITY_AUDIO = -16;
    public static final int THREAD_PRIORITY_BACKGROUND = 10;
    public static final int THREAD_PRIORITY_DEFAULT = 0;
    public static final int THREAD_PRIORITY_DISPLAY = -4;
    public static final int THREAD_PRIORITY_FOREGROUND = -2;
    public static final int THREAD_PRIORITY_LESS_FAVORABLE = 1;
    public static final int THREAD_PRIORITY_LOWEST = 19;
    public static final int THREAD_PRIORITY_MORE_FAVORABLE = -1;
    public static final int THREAD_PRIORITY_URGENT_AUDIO = -19;
    public static final int THREAD_PRIORITY_URGENT_DISPLAY = -8;
    public static final int VPN_UID = 1016;
    public static final int WIFI_UID = 1010;

    /* renamed from: a  reason: collision with root package name */
    static LocalSocket f34a;
    static DataInputStream b;
    static BufferedWriter c;
    static boolean d;

    public static final native long getElapsedCpuTime();

    public static final native long getFreeMemory();

    public static final native int getGidForName(String str);

    public static final native int[] getPids(String str, int[] iArr);

    public static final native int[] getPidsForCommands(String[] strArr);

    public static final native int getProcessGroup(int i);

    public static final native long getPss(int i);

    public static final native int getThreadPriority(int i);

    public static final native long getTotalMemory();

    public static final native int getUidForName(String str);

    public static final native boolean parseProcLine(byte[] bArr, int i, int i2, int[] iArr, String[] strArr, long[] jArr, float[] fArr);

    public static final native boolean readProcFile(String str, int[] iArr, String[] strArr, long[] jArr, float[] fArr);

    public static final native void readProcLines(String str, String[] strArr, long[] jArr);

    public static final native void sendSignal(int i, int i2);

    public static final native void sendSignalQuiet(int i, int i2);

    public static final native void setArgV0(String str);

    public static final native void setCanSelfBackground(boolean z);

    public static final native int setGid(int i);

    public static final native boolean setOomAdj(int i, int i2);

    public static final native void setProcessGroup(int i, int i2);

    public static final native boolean setSwappiness(int i, boolean z);

    public static final native void setThreadGroup(int i, int i2);

    public static final native void setThreadPriority(int i);

    public static final native void setThreadPriority(int i, int i2);

    public static final native void setThreadScheduler(int i, int i2, int i3);

    public static final native int setUid(int i);

    public static final a start(String str, String str2, int i, int i2, int[] iArr, int i3, int i4, int i5, String str3, String[] strArr) {
        try {
            return a(str, str2, i, i2, iArr, i3, i4, i5, str3, strArr);
        } catch (ZygoteStartFailedEx e) {
            Log.e("Process", "Starting VM process through Zygote failed");
            throw new RuntimeException("Starting VM process through Zygote failed", e);
        }
    }

    private static void a() {
        int i;
        int i2 = 0;
        if (d) {
            i = 0;
        } else {
            i = 10;
        }
        while (f34a == null && i2 < i + 1) {
            if (i2 > 0) {
                try {
                    Log.i("Zygote", "Zygote not up yet, sleeping...");
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                }
            }
            try {
                f34a = new LocalSocket();
                f34a.connect(new LocalSocketAddress("zygote", LocalSocketAddress.Namespace.RESERVED));
                b = new DataInputStream(f34a.getInputStream());
                c = new BufferedWriter(new OutputStreamWriter(f34a.getOutputStream()), PROC_COMBINE);
                Log.i("Zygote", "Process: zygote socket opened");
                d = false;
                break;
            } catch (IOException e2) {
                if (f34a != null) {
                    try {
                        f34a.close();
                    } catch (IOException e3) {
                        Log.e("Process", "I/O exception on close after exception", e3);
                    }
                }
                f34a = null;
                i2++;
            }
        }
        if (f34a == null) {
            d = true;
            throw new ZygoteStartFailedEx("connect failed");
        }
    }

    private static a a(ArrayList<String> arrayList) {
        a();
        try {
            c.write(Integer.toString(arrayList.size()));
            c.newLine();
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                String str = arrayList.get(i);
                if (str.indexOf(10) >= 0) {
                    throw new ZygoteStartFailedEx("embedded newlines not allowed");
                }
                c.write(str);
                c.newLine();
            }
            c.flush();
            a aVar = new a();
            aVar.f35a = b.readInt();
            if (aVar.f35a < 0) {
                throw new ZygoteStartFailedEx("fork() failed");
            }
            aVar.b = b.readBoolean();
            return aVar;
        } catch (IOException e) {
            try {
                if (f34a != null) {
                    f34a.close();
                }
            } catch (IOException e2) {
                Log.e("Process", "I/O exception on routine close", e2);
            }
            f34a = null;
            throw new ZygoteStartFailedEx(e);
        }
    }

    private static a a(String str, String str2, int i, int i2, int[] iArr, int i3, int i4, int i5, String str3, String[] strArr) {
        a a2;
        synchronized (Process.class) {
            a2 = a(new ArrayList());
        }
        return a2;
    }

    public static final int myPid() {
        return 0;
    }

    public static final int myPpid() {
        return 0;
    }

    public static final int myTid() {
        return 0;
    }

    public static final int myUid() {
        return 0;
    }

    public static final UserHandle myUserHandle() {
        return new UserHandle(null);
    }

    public static final boolean isIsolated() {
        return true;
    }

    public static final int getUidForPid(int i) {
        long[] jArr = {-1};
        readProcLines("/proc/" + i + "/status", new String[]{"Uid:"}, jArr);
        return (int) jArr[0];
    }

    public static final int getParentPid(int i) {
        long[] jArr = {-1};
        readProcLines("/proc/" + i + "/status", new String[]{"PPid:"}, jArr);
        return (int) jArr[0];
    }

    public static final int getThreadGroupLeader(int i) {
        long[] jArr = {-1};
        readProcLines("/proc/" + i + "/status", new String[]{"Tgid:"}, jArr);
        return (int) jArr[0];
    }

    @Deprecated
    public static final boolean supportsProcesses() {
        return true;
    }

    public static final void killProcess(int i) {
        sendSignal(i, 9);
    }

    public static final void killProcessQuiet(int i) {
        sendSignalQuiet(i, 9);
    }
}
