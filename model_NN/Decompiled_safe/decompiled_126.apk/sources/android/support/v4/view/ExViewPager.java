package android.support.v4.view;

import android.content.Context;
import android.util.AttributeSet;

/* compiled from: ProGuard */
public class ExViewPager extends ViewPager {
    public ExViewPager(Context context) {
        super(context);
    }

    public ExViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void smoothScrollTo(int i, int i2, int i3) {
        super.smoothScrollTo(i, i2, i3);
    }
}
