package android.support.v4.view;

import android.content.Context;
import android.os.Handler;
import android.view.GestureDetector;
import android.view.MotionEvent;

/* compiled from: ProGuard */
class v implements s {

    /* renamed from: a  reason: collision with root package name */
    private final GestureDetector f99a;

    public v(Context context, GestureDetector.OnGestureListener onGestureListener, Handler handler) {
        this.f99a = new GestureDetector(context, onGestureListener, handler);
    }

    public boolean a() {
        return this.f99a.isLongpressEnabled();
    }

    public boolean a(MotionEvent motionEvent) {
        return this.f99a.onTouchEvent(motionEvent);
    }

    public void a(boolean z) {
        this.f99a.setIsLongpressEnabled(z);
    }

    public void a(GestureDetector.OnDoubleTapListener onDoubleTapListener) {
        this.f99a.setOnDoubleTapListener(onDoubleTapListener);
    }
}
