package android.support.v4.view.a;

import android.os.Build;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final c f76a;
    private final Object b;

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            f76a = new d();
        } else if (Build.VERSION.SDK_INT >= 14) {
            f76a = new b();
        } else {
            f76a = new e();
        }
    }

    public a(Object obj) {
        this.b = obj;
    }

    public Object a() {
        return this.b;
    }

    public void a(int i) {
        f76a.a(this.b, i);
    }

    public void a(boolean z) {
        f76a.a(this.b, z);
    }

    public void a(CharSequence charSequence) {
        f76a.a(this.b, charSequence);
    }

    public int hashCode() {
        if (this.b == null) {
            return 0;
        }
        return this.b.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        a aVar = (a) obj;
        if (this.b == null) {
            if (aVar.b != null) {
                return false;
            }
            return true;
        } else if (!this.b.equals(aVar.b)) {
            return false;
        } else {
            return true;
        }
    }
}
