package android.support.v4.b;

/* compiled from: ProGuard */
public class c<E> {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f68a = new Object();
    private boolean b;
    private int[] c;
    private Object[] d;
    private int e;

    public c() {
        this(10);
    }

    public c(int i) {
        this.b = false;
        int d2 = d(i);
        this.c = new int[d2];
        this.d = new Object[d2];
        this.e = 0;
    }

    private void c() {
        int i = this.e;
        int[] iArr = this.c;
        Object[] objArr = this.d;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            Object obj = objArr[i3];
            if (obj != f68a) {
                if (i3 != i2) {
                    iArr[i2] = iArr[i3];
                    objArr[i2] = obj;
                }
                i2++;
            }
        }
        this.b = false;
        this.e = i2;
    }

    public int a() {
        if (this.b) {
            c();
        }
        return this.e;
    }

    public int a(int i) {
        if (this.b) {
            c();
        }
        return this.c[i];
    }

    public E b(int i) {
        if (this.b) {
            c();
        }
        return this.d[i];
    }

    public void b() {
        int i = this.e;
        Object[] objArr = this.d;
        for (int i2 = 0; i2 < i; i2++) {
            objArr[i2] = null;
        }
        this.e = 0;
        this.b = false;
    }

    static int c(int i) {
        for (int i2 = 4; i2 < 32; i2++) {
            if (i <= (1 << i2) - 12) {
                return (1 << i2) - 12;
            }
        }
        return i;
    }

    static int d(int i) {
        return c(i * 4) / 4;
    }
}
