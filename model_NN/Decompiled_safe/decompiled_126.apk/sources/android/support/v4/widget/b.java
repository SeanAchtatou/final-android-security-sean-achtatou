package android.support.v4.widget;

import android.database.Cursor;
import android.widget.Filter;

/* compiled from: ProGuard */
class b extends Filter {

    /* renamed from: a  reason: collision with root package name */
    c f102a;

    b(c cVar) {
        this.f102a = cVar;
    }

    public CharSequence convertResultToString(Object obj) {
        return this.f102a.c((Cursor) obj);
    }

    /* access modifiers changed from: protected */
    public Filter.FilterResults performFiltering(CharSequence charSequence) {
        Cursor a2 = this.f102a.a(charSequence);
        Filter.FilterResults filterResults = new Filter.FilterResults();
        if (a2 != null) {
            filterResults.count = a2.getCount();
            filterResults.values = a2;
        } else {
            filterResults.count = 0;
            filterResults.values = null;
        }
        return filterResults;
    }

    /* access modifiers changed from: protected */
    public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
        Cursor a2 = this.f102a.a();
        if (filterResults.values != null && filterResults.values != a2) {
            this.f102a.a((Cursor) filterResults.values);
        }
    }
}
