package android.support.v4.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.widget.RemoteViews;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class ag {

    /* renamed from: a  reason: collision with root package name */
    Context f51a;
    CharSequence b;
    CharSequence c;
    PendingIntent d;
    PendingIntent e;
    RemoteViews f;
    Bitmap g;
    CharSequence h;
    int i;
    int j;
    boolean k;
    an l;
    CharSequence m;
    int n;
    int o;
    boolean p;
    ArrayList<ad> q = new ArrayList<>();
    Notification r = new Notification();

    public ag(Context context) {
        this.f51a = context;
        this.r.when = System.currentTimeMillis();
        this.r.audioStreamType = -1;
        this.j = 0;
    }

    public ag a(long j2) {
        this.r.when = j2;
        return this;
    }

    public ag a(int i2) {
        this.r.icon = i2;
        return this;
    }

    public ag a(CharSequence charSequence) {
        this.b = charSequence;
        return this;
    }

    public ag b(CharSequence charSequence) {
        this.c = charSequence;
        return this;
    }

    public ag c(CharSequence charSequence) {
        this.h = charSequence;
        return this;
    }

    public ag a(RemoteViews remoteViews) {
        this.r.contentView = remoteViews;
        return this;
    }

    public ag a(PendingIntent pendingIntent) {
        this.d = pendingIntent;
        return this;
    }

    public ag b(PendingIntent pendingIntent) {
        this.r.deleteIntent = pendingIntent;
        return this;
    }

    public ag d(CharSequence charSequence) {
        this.r.tickerText = charSequence;
        return this;
    }

    public ag a(boolean z) {
        a(2, z);
        return this;
    }

    public ag b(boolean z) {
        a(16, z);
        return this;
    }

    private void a(int i2, boolean z) {
        if (z) {
            this.r.flags |= i2;
            return;
        }
        this.r.flags &= i2 ^ -1;
    }

    public Notification a() {
        return ac.f47a.a(this);
    }
}
