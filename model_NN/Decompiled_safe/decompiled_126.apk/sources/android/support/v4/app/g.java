package android.support.v4.app;

import android.os.Handler;
import android.os.Message;

/* compiled from: ProGuard */
class g extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FragmentActivity f57a;

    g(FragmentActivity fragmentActivity) {
        this.f57a = fragmentActivity;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                if (this.f57a.f) {
                    this.f57a.a(false);
                    return;
                }
                return;
            case 2:
                this.f57a.a();
                this.f57a.b.e();
                return;
            default:
                super.handleMessage(message);
                return;
        }
    }
}
