package com.millennialmedia.android;

import com.millennialmedia.android.MMAdViewSDK;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;

class HttpHeadRequest {
    protected String locationString;
    protected String response;
    protected URI uri;

    HttpHeadRequest() {
    }

    /* access modifiers changed from: package-private */
    public String sendRequest(String urlString) {
        try {
            URL connectURL = new URL(urlString);
            HttpURLConnection.setFollowRedirects(false);
            HttpURLConnection conn = (HttpURLConnection) connectURL.openConnection();
            conn.connect();
            MMAdViewSDK.Log.v("Response Code:" + conn.getResponseCode());
            MMAdViewSDK.Log.v("Response Message:" + conn.getResponseMessage());
            MMAdViewSDK.Log.v("Location Header:" + conn.getHeaderField("Location"));
            this.response = conn.getHeaderField("Location");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.response;
    }
}
