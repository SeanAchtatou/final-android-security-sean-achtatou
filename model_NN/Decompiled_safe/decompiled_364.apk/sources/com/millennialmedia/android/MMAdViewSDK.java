package com.millennialmedia.android;

import android.net.ConnectivityManager;

public final class MMAdViewSDK {
    static final String BASEURL = "http://androidsdk.ads.mp.mydas.mobi/getAd.php5?";
    public static final String DEFAULT_APID = "28911";
    public static final int DEFAULT_VIEWID = 1897808289;
    static final String ERR_BROKENREF = "The reference to the ad view was broken.";
    static final String ERR_NOACTIVITY = "The ad view does not have a parent activity.";
    static final int HTML = 2;
    static final int IMAGE = 4;
    static final int PACKAGE = 3;
    static final String PREFS_NAME = "MillennialMediaSettings";
    static final String PRIVATE_CACHE_DIR = "millennialmedia";
    public static final String SDKLOG = "MillennialMediaAdSDK";
    public static final String SDKVER = "4.2.6-11.07.12.a";
    static final int VIDEO = 1;
    static ConnectivityManager connectivityManager = null;
    public static int logLevel;
    public static boolean privateLogging;

    static class Log {
        Log() {
        }

        static void d(String message) {
            if (MMAdViewSDK.logLevel > 0) {
                android.util.Log.i(MMAdViewSDK.SDKLOG, "Diagnostic - " + message);
            }
        }

        static void v(String message) {
            if (MMAdViewSDK.logLevel > 1) {
                android.util.Log.i(MMAdViewSDK.SDKLOG, "Verbose - " + message);
            }
        }

        static void p(String message) {
            if (MMAdViewSDK.privateLogging) {
                android.util.Log.i(MMAdViewSDK.SDKLOG, "Private - " + message);
            }
        }
    }
}
