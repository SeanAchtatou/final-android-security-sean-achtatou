package com.mopub.mobileads;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.util.AttributeSet;
import android.util.Log;
import android.webkit.WebViewDatabase;
import android.widget.FrameLayout;
import java.util.HashMap;
import org.apache.http.HttpResponse;

public class MoPubView extends FrameLayout {
    public static String AD_HANDLER = "/m/ad";
    public static String HOST = "ads.mopub.com";
    private Activity mActivity;
    protected AdView mAdView;
    protected BaseAdapter mAdapter;
    private Context mContext;
    /* access modifiers changed from: private */
    public boolean mIsInForeground;
    private OnAdClickedListener mOnAdClickedListener;
    private OnAdClosedListener mOnAdClosedListener;
    private OnAdFailedListener mOnAdFailedListener;
    private OnAdLoadedListener mOnAdLoadedListener;
    private OnAdWillLoadListener mOnAdWillLoadListener;
    private BroadcastReceiver mScreenStateReceiver;

    public interface OnAdClickedListener {
        void OnAdClicked(MoPubView moPubView);
    }

    public interface OnAdClosedListener {
        void OnAdClosed(MoPubView moPubView);
    }

    public interface OnAdFailedListener {
        void OnAdFailed(MoPubView moPubView);
    }

    public interface OnAdLoadedListener {
        void OnAdLoaded(MoPubView moPubView);
    }

    public interface OnAdWillLoadListener {
        void OnAdWillLoad(MoPubView moPubView, String str);
    }

    public MoPubView(Context context) {
        this(context, null);
    }

    public MoPubView(Context context, AttributeSet attrs) {
        super(context, attrs);
        boolean z;
        this.mContext = context;
        if (getVisibility() == 0) {
            z = true;
        } else {
            z = false;
        }
        this.mIsInForeground = z;
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        if (WebViewDatabase.getInstance(context) == null) {
            Log.e("MoPub", "Disabling MoPub. Local cache file is inaccessible so MoPub will fail if we try to create a WebView. Details of this Android bug found at:http://code.google.com/p/android/issues/detail?id=10789");
            return;
        }
        this.mAdView = new AdView(context, this);
        registerScreenStateBroadcastReceiver();
        this.mActivity = (Activity) context;
    }

    private void registerScreenStateBroadcastReceiver() {
        if (this.mAdView != null) {
            this.mScreenStateReceiver = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    if (intent.getAction().equals("android.intent.action.SCREEN_OFF")) {
                        if (MoPubView.this.mIsInForeground) {
                            Log.d("MoPub", "Screen sleep with ad in foreground, disable refresh");
                            MoPubView.this.mAdView.setAutorefreshEnabled(false);
                            return;
                        }
                        Log.d("MoPub", "Screen sleep but ad in background; refresh should already be disabled");
                    } else if (!intent.getAction().equals("android.intent.action.USER_PRESENT")) {
                    } else {
                        if (MoPubView.this.mIsInForeground) {
                            Log.d("MoPub", "Screen wake / ad in foreground, enable refresh");
                            MoPubView.this.mAdView.setAutorefreshEnabled(true);
                            return;
                        }
                        Log.d("MoPub", "Screen wake but ad in background; don't enable refresh");
                    }
                }
            };
            IntentFilter filter = new IntentFilter("android.intent.action.SCREEN_OFF");
            filter.addAction("android.intent.action.USER_PRESENT");
            this.mContext.registerReceiver(this.mScreenStateReceiver, filter);
        }
    }

    private void unregisterScreenStateBroadcastReceiver() {
        this.mContext.unregisterReceiver(this.mScreenStateReceiver);
    }

    public void loadAd() {
        if (this.mAdView != null) {
            this.mAdView.loadAd();
        }
    }

    public void destroy() {
        unregisterScreenStateBroadcastReceiver();
        if (this.mAdView != null) {
            this.mAdView.cleanup();
            this.mAdView = null;
        }
    }

    /* access modifiers changed from: protected */
    public void loadFailUrl() {
        if (this.mAdView != null) {
            this.mAdView.loadFailUrl();
        }
    }

    /* access modifiers changed from: protected */
    public void loadNativeSDK(HashMap<String, String> paramsHash) {
        if (this.mAdapter != null) {
            this.mAdapter.invalidate();
        }
        String type = paramsHash.get("X-Adtype");
        this.mAdapter = BaseAdapter.getAdapterForType(this, type, paramsHash);
        if (this.mAdapter != null) {
            Log.i("MoPub", "Loading native adapter for type: " + type);
            this.mAdapter.loadAd();
            return;
        }
        Log.i("MoPub", "Couldn't load native adapter. Trying next ad...");
        loadFailUrl();
    }

    /* access modifiers changed from: protected */
    public void registerClick() {
        if (this.mAdView != null) {
            this.mAdView.registerClick();
            adClicked();
        }
    }

    /* access modifiers changed from: protected */
    public void loadHtmlString(String html) {
        if (this.mAdView != null) {
            this.mAdView.loadResponseString(html);
        }
    }

    /* access modifiers changed from: protected */
    public void trackNativeImpression() {
        Log.d("MoPub", "Tracking impression for native adapter.");
        if (this.mAdView != null) {
            this.mAdView.trackImpression();
        }
    }

    public void setAdUnitId(String adUnitId) {
        if (this.mAdView != null) {
            this.mAdView.setAdUnitId(adUnitId);
        }
    }

    public void setKeywords(String keywords) {
        if (this.mAdView != null) {
            this.mAdView.setKeywords(keywords);
        }
    }

    public String getKeywords() {
        if (this.mAdView == null) {
            return null;
        }
        return this.mAdView.getKeywords();
    }

    public void setLocation(Location location) {
        if (this.mAdView != null) {
            this.mAdView.setLocation(location);
        }
    }

    public Location getLocation() {
        if (this.mAdView == null) {
            return null;
        }
        return this.mAdView.getLocation();
    }

    public void setTimeout(int milliseconds) {
        if (this.mAdView != null) {
            this.mAdView.setTimeout(milliseconds);
        }
    }

    public int getAdWidth() {
        if (this.mAdView == null) {
            return 0;
        }
        return this.mAdView.getAdWidth();
    }

    public int getAdHeight() {
        if (this.mAdView == null) {
            return 0;
        }
        return this.mAdView.getAdHeight();
    }

    public HttpResponse getResponse() {
        if (this.mAdView == null) {
            return null;
        }
        return this.mAdView.getResponse();
    }

    public String getResponseString() {
        if (this.mAdView == null) {
            return null;
        }
        return this.mAdView.getResponseString();
    }

    public Activity getActivity() {
        return this.mActivity;
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int visibility) {
        if (this.mAdView != null) {
            if (visibility == 0) {
                Log.d("MoPub", "Ad Unit (" + this.mAdView.getAdUnitId() + ") going visible: enabling refresh");
                this.mIsInForeground = true;
                this.mAdView.setAutorefreshEnabled(true);
                return;
            }
            Log.d("MoPub", "Ad Unit (" + this.mAdView.getAdUnitId() + ") going invisible: disabling refresh");
            this.mIsInForeground = false;
            this.mAdView.setAutorefreshEnabled(false);
        }
    }

    /* access modifiers changed from: protected */
    public void adWillLoad(String url) {
        Log.d("MoPub", "adWillLoad: " + url);
        if (this.mOnAdWillLoadListener != null) {
            this.mOnAdWillLoadListener.OnAdWillLoad(this, url);
        }
    }

    /* access modifiers changed from: protected */
    public void adLoaded() {
        Log.d("MoPub", "adLoaded");
        if (this.mOnAdLoadedListener != null) {
            this.mOnAdLoadedListener.OnAdLoaded(this);
        }
    }

    /* access modifiers changed from: protected */
    public void adFailed() {
        if (this.mOnAdFailedListener != null) {
            this.mOnAdFailedListener.OnAdFailed(this);
        }
    }

    /* access modifiers changed from: protected */
    public void adClosed() {
        if (this.mOnAdClosedListener != null) {
            this.mOnAdClosedListener.OnAdClosed(this);
        }
    }

    /* access modifiers changed from: protected */
    public void adClicked() {
        if (this.mOnAdClickedListener != null) {
            this.mOnAdClickedListener.OnAdClicked(this);
        }
    }

    /* access modifiers changed from: protected */
    public void nativeAdLoaded() {
        if (this.mAdView != null) {
            this.mAdView.scheduleRefreshTimerIfEnabled();
        }
        adLoaded();
    }

    public void setOnAdWillLoadListener(OnAdWillLoadListener listener) {
        this.mOnAdWillLoadListener = listener;
    }

    public void setOnAdLoadedListener(OnAdLoadedListener listener) {
        this.mOnAdLoadedListener = listener;
    }

    public void setOnAdFailedListener(OnAdFailedListener listener) {
        this.mOnAdFailedListener = listener;
    }

    public void setOnAdClosedListener(OnAdClosedListener listener) {
        this.mOnAdClosedListener = listener;
    }

    public void setOnAdClickedListener(OnAdClickedListener listener) {
        this.mOnAdClickedListener = listener;
    }
}
