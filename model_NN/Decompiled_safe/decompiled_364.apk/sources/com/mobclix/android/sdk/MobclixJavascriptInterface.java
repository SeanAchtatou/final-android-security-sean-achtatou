package com.mobclix.android.sdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import android.view.animation.Animation;
import android.webkit.WebView;
import com.millennialmedia.android.MMAdView;
import com.mobclix.android.sdk.Mobclix;
import com.mobclix.android.sdk.MobclixBrowserActivity;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.lang.ref.SoftReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class MobclixJavascriptInterface implements LocationListener {
    private static String TAG = "MobclixJavascriptInterface";
    private final int MAX_SONGS = 75;
    private final int PERMISSION_CALENDAR_READ = 0;
    private final int PERMISSION_CALENDAR_WRITE = 1;
    private final int PERMISSION_CONTACTS_READ = 2;
    private final int PERMISSION_CONTACTS_WRITE = 3;
    private final int TYPE_COMPASS = 1338;
    private final int TYPE_SHAKE = 1337;
    private String adDidDisplayCallbackFunctionName = null;
    private String adDidReturnFromHiddenCallbackFunctionName = null;
    private String adFinishedResizingCallbackFunctionName = null;
    private String adWillBecomeHiddenCallbackFunctionName = null;
    private String adWillContractCallbackFunctionName = null;
    private String adWillTerminateCallbackFunctionName = null;
    private boolean autoplay = true;
    private Camera camera = null;
    private String contactCallbackFunctionName = "";
    private String contactErrorCallbackFunctionName = "";
    private Mobclix controller = Mobclix.getInstance();
    int duration = 500;
    boolean expanded = false;
    MobclixBrowserActivity.MobclixFullScreen expanderActivity;
    private String flashMode = "null";
    private boolean flashlightOn = false;
    boolean fullScreenAdView = false;
    private String gpsDataCallbackFunctionName = "";
    private float gpsDistanceFilter = 0.0f;
    private boolean gpsListenerAdded = false;
    private boolean gps_enabled = false;
    private LocationManager locationManager = null;
    private final MobclixContacts mMobclixContacts = MobclixContacts.getInstance();
    private boolean network_enabled = false;
    private boolean paused = false;
    /* access modifiers changed from: private */
    public String photoCallbackFunctionName = "";
    private String photoErrorCallbackFunctionName = "";
    int photoHeight;
    Thread photoSendingThread;
    String photoServerUrl;
    int photoWidth;
    private boolean requireUserInteraction = false;
    private HashMap<Integer, Integer> sensorIntervals = new HashMap<>();
    private HashMap<Integer, SensorEventListener> sensorListeners = new HashMap<>();
    private SensorManager sensorMgr;
    private List<Integer> sensors = new ArrayList();
    private String uniqueKey = null;
    boolean userHasInteracted = false;
    /* access modifiers changed from: private */
    public HashMap<Integer, Integer> userPermissions = new HashMap<>();
    /* access modifiers changed from: private */
    public MobclixWebView webview;
    Activity webviewActivity;

    MobclixJavascriptInterface(MobclixWebView w, boolean fullscreen) {
        this.webview = w;
        this.webviewActivity = (Activity) this.webview.getWorkingContext();
        this.fullScreenAdView = fullscreen;
        try {
            this.requireUserInteraction = this.webview.parentCreative.parentAdView.richMediaRequiresUserInteraction();
            this.autoplay = this.webview.parentCreative.parentAdView.allowAutoplay();
        } catch (Exception e) {
        }
        if (fullscreen) {
            this.autoplay = true;
            this.requireUserInteraction = false;
        }
        try {
            this.sensorMgr = (SensorManager) this.webviewActivity.getSystemService("sensor");
            for (Sensor s : this.sensorMgr.getSensorList(-1)) {
                this.sensors.add(Integer.valueOf(s.getType()));
            }
            this.userPermissions.put(0, -1);
            this.userPermissions.put(1, -1);
            this.userPermissions.put(2, -1);
            this.userPermissions.put(3, -1);
            checkLocationProviders();
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void checkLocationProviders() {
        this.locationManager = (LocationManager) this.webviewActivity.getSystemService("location");
        PackageManager package_manager = this.webview.getContext().getPackageManager();
        String app_name = this.webview.getContext().getPackageName();
        if (package_manager.checkPermission("android.permission.ACCESS_FINE_LOCATION", app_name) != 0) {
            this.gps_enabled = false;
        } else if (this.controller.location.isProviderSupported("gps")) {
            this.gps_enabled = this.locationManager.isProviderEnabled("gps");
        } else {
            this.gps_enabled = false;
        }
        if (package_manager.checkPermission("android.permission.ACCESS_COARSE_LOCATION", app_name) != 0) {
            this.network_enabled = false;
        } else if (this.controller.location.isProviderSupported("network")) {
            this.network_enabled = this.locationManager.isProviderEnabled("network");
        } else {
            this.network_enabled = false;
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void reset() {
        pauseListeners();
        this.expanded = false;
        try {
            this.requireUserInteraction = this.webview.parentCreative.parentAdView.richMediaRequiresUserInteraction();
            this.autoplay = this.webview.parentCreative.parentAdView.allowAutoplay();
        } catch (Exception e) {
        }
        if (this.fullScreenAdView) {
            this.autoplay = true;
            this.requireUserInteraction = false;
        }
        this.userHasInteracted = false;
        this.uniqueKey = null;
        this.camera = null;
        this.flashMode = "null";
        this.flashlightOn = false;
        this.userPermissions.put(0, -1);
        this.userPermissions.put(1, -1);
        this.userPermissions.put(2, -1);
        this.userPermissions.put(3, -1);
        this.photoServerUrl = null;
        this.photoSendingThread = null;
        this.photoCallbackFunctionName = "";
        this.photoErrorCallbackFunctionName = "";
        this.contactCallbackFunctionName = "";
        this.contactErrorCallbackFunctionName = "";
        this.sensorListeners = new HashMap<>();
        this.sensorIntervals = new HashMap<>();
        this.locationManager = null;
        this.gpsDistanceFilter = 0.0f;
        this.gpsDataCallbackFunctionName = "";
        this.gpsListenerAdded = false;
        try {
            checkLocationProviders();
        } catch (Exception e2) {
            this.gps_enabled = false;
            this.network_enabled = false;
        }
        this.paused = false;
        this.adDidDisplayCallbackFunctionName = null;
        this.adFinishedResizingCallbackFunctionName = null;
        this.adWillContractCallbackFunctionName = null;
        this.adWillTerminateCallbackFunctionName = null;
        this.adWillBecomeHiddenCallbackFunctionName = null;
        this.adDidReturnFromHiddenCallbackFunctionName = null;
        return;
    }

    /* access modifiers changed from: package-private */
    public synchronized void pauseListeners() {
        if (!this.paused) {
            for (Map.Entry<Integer, SensorEventListener> entry : this.sensorListeners.entrySet()) {
                try {
                    this.sensorMgr.unregisterListener((SensorEventListener) entry.getValue());
                } catch (Exception e) {
                }
            }
            try {
                this.locationManager.removeUpdates(this);
            } catch (Exception e2) {
            }
            try {
                deviceFlashlightPause(true);
            } catch (Exception e3) {
            }
            this.paused = true;
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void resumeListeners() {
        if (this.paused) {
            for (Map.Entry<Integer, SensorEventListener> entry : this.sensorListeners.entrySet()) {
                try {
                    this.sensorMgr.unregisterListener((SensorEventListener) entry.getValue());
                } catch (Exception e) {
                }
                try {
                    if (((Integer) entry.getKey()).intValue() == 1337) {
                        this.sensorMgr.registerListener((SensorEventListener) entry.getValue(), this.sensorMgr.getDefaultSensor(1), this.sensorIntervals.get(entry.getKey()).intValue());
                    } else {
                        this.sensorMgr.registerListener((SensorEventListener) entry.getValue(), this.sensorMgr.getDefaultSensor(((Integer) entry.getKey()).intValue()), this.sensorIntervals.get(entry.getKey()).intValue());
                    }
                } catch (Exception e2) {
                }
            }
            try {
                if (this.gpsListenerAdded) {
                    if (this.gps_enabled) {
                        this.locationManager.requestLocationUpdates("gps", 0, this.gpsDistanceFilter, this);
                    }
                    if (this.network_enabled) {
                        this.locationManager.requestLocationUpdates("network", 0, this.gpsDistanceFilter, this);
                    }
                }
            } catch (Exception e3) {
            }
            try {
                if (this.flashlightOn) {
                    deviceFlashlightResume();
                }
            } catch (Exception e4) {
            }
            this.paused = false;
        }
    }

    /* access modifiers changed from: package-private */
    public String callback(String callbackFunctionName, String value, boolean addQuotes) {
        StringBuilder js = new StringBuilder("javascript:");
        js.append(callbackFunctionName);
        if (addQuotes) {
            js.append("(\"");
        } else {
            js.append("(");
        }
        js.append(value);
        if (addQuotes) {
            js.append("\");");
        } else {
            js.append(");");
        }
        return js.toString();
    }

    /* access modifiers changed from: package-private */
    public void checkUserPermission(int permission, DialogInterface.OnClickListener positiveButtonListener) throws Exception {
        try {
            Context currentActivity = this.webviewActivity;
            if (this.expanderActivity != null) {
                currentActivity = this.expanderActivity.getContext();
            }
            String message = "";
            if (permission == 0) {
                message = "Do you want to allow this ad to access your calendar?";
            } else if (permission == 1) {
                message = "Do you want to allow this ad to add a calendar event?";
            } else if (permission == 2) {
                message = "Do you want to allow this ad to access your contacts?";
            } else if (permission == 3) {
                message = "Do you want to allow this ad to add a contact?";
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(currentActivity);
            builder.setMessage(message).setCancelable(false).setPositiveButton("Yes", positiveButtonListener).setNegativeButton("No", new Mobclix.ObjectOnClickListener(Integer.valueOf(permission)) {
                public void onClick(DialogInterface dialog, int id) {
                    MobclixJavascriptInterface.this.userPermissions.put((Integer) this.obj1, 0);
                }
            });
            builder.create().show();
        } catch (Exception e) {
            Exception e2 = e;
            this.userPermissions.put(Integer.valueOf(permission), 0);
            throw new Exception(e2.toString());
        }
    }

    public synchronized void domReady() {
        injectPermissionsKey();
    }

    /* access modifiers changed from: package-private */
    public synchronized void injectPermissionsKey() {
        this.uniqueKey = UUID.randomUUID().toString();
        StringBuilder s = new StringBuilder("javascript:window.addEventListener('click', function(){mAdViewPermissionsKey='");
        s.append(this.uniqueKey).append("';}, true);");
        this.webview.loadUrl(s.toString());
    }

    /* access modifiers changed from: package-private */
    public synchronized void checkPermissionsForUserInteraction(String callback, String errorCallbackFunctionName) {
        StringBuilder s = new StringBuilder("javascript: try { window.MOBCLIX.checkPermissionsForUserInteractionResponse(mAdViewPermissionsKey, \"");
        s.append(callback).append("\"); } catch(e) { ").append(errorCallbackFunctionName).append("('User hasnt interacted with the ad yet.'); }");
        Log.v(TAG, s.toString());
        this.webview.loadUrl(s.toString());
    }

    public synchronized void checkPermissionsForUserInteractionResponse(String key, String callback) {
        if (key.equals(this.uniqueKey)) {
            this.userHasInteracted = true;
            if (callback != null && !callback.equals("null")) {
                StringBuilder s = new StringBuilder("javascript:");
                s.append(callback).append(";");
                this.webview.loadUrl(s.toString());
            }
        }
    }

    public synchronized boolean sensorStart(int type, float updateInterval, String callbackFunctionName, String successCallbackFunctionName, String errorCallbackFunctionName) {
        int interval;
        boolean z;
        if (((double) updateInterval) > 0.5d) {
            interval = 2;
        } else if (((double) updateInterval) > 0.2d) {
            interval = 3;
        } else if (((double) updateInterval) > 0.1d) {
            interval = 1;
        } else {
            interval = 0;
        }
        this.sensorIntervals.put(Integer.valueOf(type), Integer.valueOf(interval));
        try {
            if (!this.sensors.contains(Integer.valueOf(type))) {
                throw new Exception("Sensor not supported.");
            }
            if (this.sensorListeners.containsKey(Integer.valueOf(type))) {
                try {
                    this.sensorMgr.unregisterListener(this.sensorListeners.get(Integer.valueOf(type)));
                    this.sensorListeners.remove(Integer.valueOf(type));
                } catch (Exception e) {
                }
            }
            this.sensorListeners.put(Integer.valueOf(type), new MobclixSensorEventListener(this.webview, type, callbackFunctionName));
            if (this.sensorMgr.registerListener(this.sensorListeners.get(Integer.valueOf(type)), this.sensorMgr.getDefaultSensor(type), interval)) {
                if (successCallbackFunctionName != null) {
                    this.webview.loadUrl(callback(successCallbackFunctionName, "", false));
                }
                z = true;
            } else {
                this.sensorListeners.remove(Integer.valueOf(type));
                throw new Exception("Error registering listener.");
            }
        } catch (Exception e2) {
            Exception e3 = e2;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e3.toString()), true));
            }
            z = false;
        }
        return z;
    }

    public synchronized void sensorStop(int type, String successCallbackFunctionName, String errorCallbackFunctionName) {
        try {
            if (this.sensorListeners.containsKey(Integer.valueOf(type))) {
                this.sensorMgr.unregisterListener(this.sensorListeners.get(Integer.valueOf(type)));
                this.sensorListeners.remove(Integer.valueOf(type));
                if (successCallbackFunctionName != null) {
                    this.webview.loadUrl(callback(successCallbackFunctionName, "", false));
                }
            }
        } catch (Exception e) {
            throw e;
        } catch (Exception e2) {
            this.webview.loadUrl(callback(successCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
        }
        return;
    }

    public synchronized boolean accelerometerStart(float updateInterval, String callbackFunctionName, String successCallbackFunctionName, String errorCallbackFunctionName) {
        return sensorStart(1, updateInterval, callbackFunctionName, successCallbackFunctionName, errorCallbackFunctionName);
    }

    public synchronized void accelerometerStop(String successCallbackFunctionName, String errorCallbackFunctionName) {
        sensorStop(1, successCallbackFunctionName, errorCallbackFunctionName);
    }

    public synchronized boolean gyroscopeStart(float updateInterval, String callbackFunctionName, String successCallbackFunctionName, String errorCallbackFunctionName) {
        return sensorStart(4, updateInterval, callbackFunctionName, successCallbackFunctionName, errorCallbackFunctionName);
    }

    public synchronized void gyroscopeStop(String successCallbackFunctionName, String errorCallbackFunctionName) {
        sensorStop(4, successCallbackFunctionName, errorCallbackFunctionName);
    }

    public synchronized boolean deviceMotionStart(float updateInterval, String callbackFunctionName, String successCallbackFunctionName, String errorCallbackFunctionName) {
        return sensorStart(3, updateInterval, callbackFunctionName, successCallbackFunctionName, errorCallbackFunctionName);
    }

    public synchronized void deviceMotionStop(String successCallbackFunctionName, String errorCallbackFunctionName) {
        sensorStop(3, successCallbackFunctionName, errorCallbackFunctionName);
    }

    public synchronized boolean magnetometerStart(String callbackFunctionName, String successCallbackFunctionName, String errorCallbackFunctionName) {
        boolean z;
        this.sensorIntervals.put(1338, 2);
        try {
            if (!this.sensors.contains(3)) {
                throw new Exception("Sensor not supported.");
            }
            if (this.sensorListeners.containsKey(1338)) {
                try {
                    this.sensorMgr.unregisterListener(this.sensorListeners.get(1338));
                    this.sensorListeners.remove(1338);
                } catch (Exception e) {
                }
            }
            this.sensorListeners.put(1338, new MobclixSensorEventListener(this.webview, 1338, callbackFunctionName));
            if (this.sensorMgr.registerListener(this.sensorListeners.get(1338), this.sensorMgr.getDefaultSensor(3), 2)) {
                if (successCallbackFunctionName != null) {
                    this.webview.loadUrl(callback(successCallbackFunctionName, "", false));
                }
                z = true;
            } else {
                this.sensorListeners.remove(1338);
                throw new Exception("Error registering listener.");
            }
        } catch (Exception e2) {
            Exception e3 = e2;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e3.toString()), true));
            }
            z = false;
        }
        return z;
    }

    public synchronized void magnetometerStop(String successCallbackFunctionName, String errorCallbackFunctionName) {
        sensorStop(1338, successCallbackFunctionName, errorCallbackFunctionName);
    }

    public synchronized boolean gpsStart(float distanceFilter, String callbackFunctionName, String successCallbackFunctionName, String errorCallbackFunctionName) {
        boolean z;
        try {
            if (this.gps_enabled || this.network_enabled) {
                try {
                    this.locationManager.removeUpdates(this);
                } catch (Exception e) {
                }
                this.gpsDistanceFilter = distanceFilter;
                if (this.gps_enabled) {
                    this.locationManager.requestLocationUpdates("gps", 0, this.gpsDistanceFilter, this);
                    this.gpsDataCallbackFunctionName = callbackFunctionName;
                    this.gpsListenerAdded = true;
                }
                if (this.network_enabled) {
                    this.locationManager.requestLocationUpdates("network", 0, this.gpsDistanceFilter, this);
                    this.gpsDataCallbackFunctionName = callbackFunctionName;
                    this.gpsListenerAdded = true;
                }
                if (this.gpsListenerAdded) {
                    if (successCallbackFunctionName != null) {
                        this.webview.loadUrl(callback(successCallbackFunctionName, "", false));
                    }
                    z = true;
                } else {
                    throw new Exception("Sensor not supported.");
                }
            } else {
                throw new Exception("Sensor not supported.");
            }
        } catch (Exception e2) {
            Exception e3 = e2;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e3.toString()), true));
            }
            z = false;
        }
        return z;
    }

    public synchronized void gpsStop(String successCallbackFunctionName, String errorCallbackFunctionName) {
        try {
            this.locationManager.removeUpdates(this);
        } catch (Exception e) {
            Exception e2 = e;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
            }
        }
        this.gpsListenerAdded = false;
        if (successCallbackFunctionName != null) {
            this.webview.loadUrl(callback(successCallbackFunctionName, "", false));
        }
    }

    public void onLocationChanged(Location location) {
        Location net_loc = null;
        Location gps_loc = null;
        if (this.gps_enabled) {
            gps_loc = this.locationManager.getLastKnownLocation("gps");
        }
        if (this.network_enabled) {
            net_loc = this.locationManager.getLastKnownLocation("network");
        }
        if (gps_loc == null || net_loc == null) {
            if (gps_loc != null) {
                gotLocation(gps_loc);
            } else if (net_loc != null) {
                gotLocation(net_loc);
            }
        } else if (gps_loc.getTime() > net_loc.getTime()) {
            gotLocation(gps_loc);
        } else {
            gotLocation(net_loc);
        }
    }

    public void gotLocation(Location location) {
        try {
            if (!this.gpsDataCallbackFunctionName.equals("")) {
                JSONObject data = new JSONObject();
                data.put("altitude", location.getAltitude());
                JSONObject coordinate = new JSONObject();
                coordinate.put(MMAdView.KEY_LATITUDE, location.getLatitude());
                coordinate.put(MMAdView.KEY_LONGITUDE, location.getLongitude());
                data.put("coordinate", coordinate);
                data.put("course", (double) location.getBearing());
                data.put("speed", (double) location.getSpeed());
                data.put("timestamp", location.getTime());
                data.put("horizontalAccuracy", (double) location.getAccuracy());
                data.put("verticalAccuracy", (double) location.getAccuracy());
                this.webview.loadUrl(callback(this.gpsDataCallbackFunctionName, "eval('(" + data.toString() + ")')", false));
            }
        } catch (JSONException e) {
        }
    }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public synchronized boolean startListeningForShake(String callbackFunctionName, String successCallbackFunctionName, String errorCallbackFunctionName) {
        boolean z;
        this.sensorIntervals.put(1337, 2);
        try {
            if (!this.sensors.contains(1)) {
                throw new Exception("Sensor not supported.");
            }
            if (this.sensorListeners.containsKey(1337)) {
                try {
                    this.sensorMgr.unregisterListener(this.sensorListeners.get(1337));
                    this.sensorListeners.remove(1337);
                } catch (Exception e) {
                }
            }
            this.sensorListeners.put(1337, new MobclixShakeEventListener(this.webview, callbackFunctionName));
            if (this.sensorMgr.registerListener(this.sensorListeners.get(1337), this.sensorMgr.getDefaultSensor(1), 2)) {
                if (successCallbackFunctionName != null) {
                    this.webview.loadUrl(callback(successCallbackFunctionName, "", false));
                }
                z = true;
            } else {
                this.sensorListeners.remove(1337);
                throw new Exception("Error registering listener.");
            }
        } catch (Exception e2) {
            Exception e3 = e2;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e3.toString()), true));
            }
            z = false;
        }
        return z;
    }

    public synchronized void stopListeningForShake(String successCallbackFunctionName, String errorCallbackFunctionName) {
        sensorStop(1337, successCallbackFunctionName, errorCallbackFunctionName);
    }

    class MobclixSensorEventListener implements SensorEventListener {
        String callbackFunctionName;
        int type;
        WebView webview;

        MobclixSensorEventListener(WebView w, int t, String function) {
            this.webview = w;
            this.type = t;
            this.callbackFunctionName = function;
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        public void onSensorChanged(SensorEvent event) {
            try {
                if (event.sensor.getType() == this.type) {
                    StringBuilder js = new StringBuilder(new StringBuilder().append(event.values[0]).toString());
                    if (event.values.length > 1) {
                        js.append(",");
                        js.append(event.values[1]);
                        if (event.values.length > 2) {
                            js.append(",");
                            js.append(event.values[2]);
                        }
                    }
                    this.webview.loadUrl(MobclixJavascriptInterface.this.callback(this.callbackFunctionName, js.toString(), false));
                }
                if (this.type == 1338) {
                    this.webview.loadUrl(MobclixJavascriptInterface.this.callback(this.callbackFunctionName, new StringBuilder().append(event.values[0]).toString(), false));
                }
            } catch (Exception e) {
            }
        }
    }

    class MobclixShakeEventListener implements SensorEventListener {
        String callbackFunctionName;
        private double m_totalForcePrev = 0.0d;
        WebView webview;

        MobclixShakeEventListener(WebView w, String function) {
            this.webview = w;
            this.callbackFunctionName = function;
        }

        public void onAccuracyChanged(Sensor sensor, int arg0) {
        }

        public void onSensorChanged(SensorEvent event) {
            if (event.sensor.getType() == 1) {
                double totalForce = Math.sqrt(0.0d + Math.pow((double) (event.values[0] / 9.80665f), 2.0d) + Math.pow((double) (event.values[1] / 9.80665f), 2.0d) + Math.pow((double) (event.values[2] / 9.80665f), 2.0d));
                if (totalForce < 1.399999976158142d && this.m_totalForcePrev > 1.399999976158142d) {
                    this.webview.loadUrl(MobclixJavascriptInterface.this.callback(this.callbackFunctionName, "", false));
                }
                this.m_totalForcePrev = totalForce;
            }
        }
    }

    public synchronized void cameraTakePhoto(int width, int height, String successCallbackFunctionName, String errorCallbackFunctionName) {
        try {
            if (!this.webview.displayed) {
                throw new Exception("Ad not yet displayed.");
            } else if (this.autoplay || this.userHasInteracted) {
                Intent mIntent = new Intent();
                String packageName = this.webviewActivity.getPackageName();
                mIntent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "camera");
                this.photoWidth = width;
                this.photoHeight = height;
                this.photoCallbackFunctionName = successCallbackFunctionName;
                this.photoErrorCallbackFunctionName = errorCallbackFunctionName;
                this.controller.cameraWebview = new SoftReference<>(this.webview);
                Context a = this.webviewActivity;
                if (this.expanderActivity != null) {
                    a = this.expanderActivity.getContext();
                    this.expanderActivity.wasAdActivity = true;
                }
                a.startActivity(mIntent);
            } else {
                StringBuilder s = new StringBuilder("window.MOBCLIX.cameraTakePhoto(");
                s.append(width).append(",").append(height).append(",'").append(successCallbackFunctionName).append("','").append(errorCallbackFunctionName).append("');");
                checkPermissionsForUserInteraction(s.toString(), errorCallbackFunctionName);
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void photoTaken(String image) {
        if (this.photoCallbackFunctionName != null) {
            this.webview.loadUrl(callback(this.photoCallbackFunctionName, image, true));
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void photoCanceled(String e) {
        if (this.photoErrorCallbackFunctionName != null) {
            this.webview.loadUrl(callback(this.photoErrorCallbackFunctionName, e, true));
        }
    }

    public synchronized void deviceAccelerometerAvailable(String callbackFunctionName) {
        String b;
        if (this.sensors.contains(1)) {
            b = "true";
        } else {
            b = "false";
        }
        if (callbackFunctionName != null) {
            this.webview.loadUrl(callback(callbackFunctionName, b, false));
        }
    }

    public synchronized void deviceGyroscopeAvailable(String callbackFunctionName) {
        String b;
        if (this.sensors.contains(4)) {
            b = "true";
        } else {
            b = "false";
        }
        if (callbackFunctionName != null) {
            this.webview.loadUrl(callback(callbackFunctionName, b, false));
        }
    }

    public synchronized void deviceDeviceMotionAvailable(String callbackFunctionName) {
        String b;
        if (this.sensors.contains(3)) {
            b = "true";
        } else {
            b = "false";
        }
        if (callbackFunctionName != null) {
            this.webview.loadUrl(callback(callbackFunctionName, b, false));
        }
    }

    public synchronized void deviceGPSAvailable(String callbackFunctionName) {
        String b;
        if (this.gps_enabled || this.network_enabled) {
            b = "true";
        } else {
            b = "false";
        }
        if (callbackFunctionName != null) {
            this.webview.loadUrl(callback(callbackFunctionName, b, false));
        }
    }

    public synchronized void deviceMagnetometerAvailable(String callbackFunctionName) {
        String b;
        if (this.sensors.contains(3)) {
            b = "true";
        } else {
            b = "false";
        }
        if (callbackFunctionName != null) {
            this.webview.loadUrl(callback(callbackFunctionName, b, false));
        }
    }

    public synchronized void deviceCameraAvailable(String callbackFunctionName) {
        String b = "false";
        if (this.controller.permissions.containsKey("android.permission.CAMERA")) {
            b = "true";
        }
        if (!MobclixUtility.isSdPresent()) {
            b = "false";
        }
        if (callbackFunctionName != null) {
            this.webview.loadUrl(callback(callbackFunctionName, b, false));
        }
    }

    public synchronized void deviceFlashlightAvailable(String callbackFunctionName) {
        deviceCameraAvailable(callbackFunctionName);
    }

    public synchronized void deviceCalendarReadAvailable(String callbackFunctionName) {
        String b;
        if (this.controller.permissions.containsKey("android.permission.READ_CALENDAR")) {
            b = "true";
        } else {
            b = "false";
        }
        if (callbackFunctionName != null) {
            this.webview.loadUrl(callback(callbackFunctionName, b, false));
        }
    }

    public synchronized void deviceCalendarWriteAvailable(String callbackFunctionName) {
        String b;
        if (this.controller.permissions.containsKey("android.permission.WRITE_CALENDAR")) {
            b = "true";
        } else {
            b = "false";
        }
        if (callbackFunctionName != null) {
            this.webview.loadUrl(callback(callbackFunctionName, b, false));
        }
    }

    public synchronized void deviceContactsWriteAvailable(String callbackFunctionName) {
        String b;
        if (this.controller.permissions.containsKey("android.permission.WRITE_CONTACTS")) {
            b = "true";
        } else {
            b = "false";
        }
        if (callbackFunctionName != null) {
            this.webview.loadUrl(callback(callbackFunctionName, b, false));
        }
    }

    public synchronized void deviceVibrateAvailable(String callbackFunctionName) {
        String b;
        if (this.controller.permissions.containsKey("android.permission.VIBRATE")) {
            b = "true";
        } else {
            b = "false";
        }
        if (callbackFunctionName != null) {
            this.webview.loadUrl(callback(callbackFunctionName, b, false));
        }
    }

    public synchronized void deviceEmailAvailable(String callbackFunctionName) {
        if (callbackFunctionName != null) {
            this.webview.loadUrl(callback(callbackFunctionName, "true", false));
        }
    }

    public synchronized void deviceSMSAvailable(String callbackFunctionName) {
        if (callbackFunctionName != null) {
            this.webview.loadUrl(callback(callbackFunctionName, "true", false));
        }
    }

    public synchronized void deviceShakeListeningAvailable(String callbackFunctionName) {
        deviceAccelerometerAvailable(callbackFunctionName);
    }

    public synchronized void deviceVibrate(String successCallbackFunctionName, String errorCallbackFunctionName) {
        try {
            if (!this.webview.displayed) {
                throw new Exception("Ad not yet displayed.");
            } else if (!this.autoplay && !this.userHasInteracted) {
                StringBuilder s = new StringBuilder("window.MOBCLIX.deviceVibrate('");
                s.append(successCallbackFunctionName).append("','").append(errorCallbackFunctionName).append("');");
                checkPermissionsForUserInteraction(s.toString(), errorCallbackFunctionName);
            } else if (!this.controller.permissions.containsKey("android.permission.VIBRATE")) {
                throw new Exception("No permission to vibrate.");
            } else {
                ((Vibrator) this.webview.getWorkingContext().getSystemService("vibrator")).vibrate(500);
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0011, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void deviceAlert(java.lang.String r12, java.lang.String r13, java.lang.String r14, java.lang.String r15) {
        /*
            r11 = this;
            r10 = 2
            r9 = 1
            monitor-enter(r11)
            com.mobclix.android.sdk.MobclixWebView r6 = r11.webview     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            boolean r6 = r6.displayed     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            if (r6 != 0) goto L_0x0015
            java.lang.Exception r6 = new java.lang.Exception     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            java.lang.String r7 = "Ad not yet displayed."
            r6.<init>(r7)     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            throw r6     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
        L_0x0011:
            r6 = move-exception
            r4 = r6
        L_0x0013:
            monitor-exit(r11)
            return
        L_0x0015:
            boolean r6 = r11.autoplay     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            if (r6 != 0) goto L_0x0058
            boolean r6 = r11.userHasInteracted     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            if (r6 != 0) goto L_0x0058
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            java.lang.String r6 = "window.MOBCLIX.deviceAlert('"
            r5.<init>(r6)     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            java.lang.StringBuilder r6 = r5.append(r12)     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            java.lang.String r7 = "','"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            java.lang.StringBuilder r6 = r6.append(r13)     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            java.lang.String r7 = "','"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            java.lang.StringBuilder r6 = r6.append(r14)     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            java.lang.String r7 = "','"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            java.lang.StringBuilder r6 = r6.append(r15)     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            java.lang.String r7 = "');"
            r6.append(r7)     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            java.lang.String r6 = r5.toString()     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            java.lang.String r7 = "error"
            r11.checkPermissionsForUserInteraction(r6, r7)     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            goto L_0x0013
        L_0x0055:
            r6 = move-exception
            monitor-exit(r11)
            throw r6
        L_0x0058:
            android.app.Activity r3 = r11.webviewActivity     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            com.mobclix.android.sdk.MobclixBrowserActivity$MobclixFullScreen r6 = r11.expanderActivity     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            if (r6 == 0) goto L_0x0064
            com.mobclix.android.sdk.MobclixBrowserActivity$MobclixFullScreen r6 = r11.expanderActivity     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            android.content.Context r3 = r6.getContext()     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
        L_0x0064:
            if (r15 == 0) goto L_0x0013
            if (r12 == 0) goto L_0x0013
            if (r13 == 0) goto L_0x0013
            java.lang.String r6 = "\\|"
            java.lang.String[] r2 = r15.split(r6)     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            android.app.AlertDialog$Builder r6 = r1.setMessage(r13)     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            android.app.AlertDialog$Builder r6 = r6.setTitle(r12)     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            r7 = 0
            r6.setCancelable(r7)     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            int r6 = r2.length     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            if (r6 <= 0) goto L_0x0092
            r6 = 0
            r6 = r2[r6]     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            com.mobclix.android.sdk.MobclixJavascriptInterface$2 r7 = new com.mobclix.android.sdk.MobclixJavascriptInterface$2     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            r8 = 0
            r8 = r2[r8]     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            r7.<init>(r14, r8)     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            r1.setPositiveButton(r6, r7)     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
        L_0x0092:
            int r6 = r2.length     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            if (r6 <= r9) goto L_0x00a3
            r6 = 1
            r6 = r2[r6]     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            com.mobclix.android.sdk.MobclixJavascriptInterface$3 r7 = new com.mobclix.android.sdk.MobclixJavascriptInterface$3     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            r8 = 1
            r8 = r2[r8]     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            r7.<init>(r14, r8)     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            r1.setNeutralButton(r6, r7)     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
        L_0x00a3:
            int r6 = r2.length     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            if (r6 <= r10) goto L_0x00b4
            r6 = 2
            r6 = r2[r6]     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            com.mobclix.android.sdk.MobclixJavascriptInterface$4 r7 = new com.mobclix.android.sdk.MobclixJavascriptInterface$4     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            r8 = 2
            r8 = r2[r8]     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            r7.<init>(r14, r8)     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            r1.setNegativeButton(r6, r7)     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
        L_0x00b4:
            android.app.AlertDialog r0 = r1.create()     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            r0.show()     // Catch:{ Exception -> 0x0011, all -> 0x0055 }
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixJavascriptInterface.deviceAlert(java.lang.String, java.lang.String, java.lang.String, java.lang.String):void");
    }

    public synchronized void deviceBatteryState(String callbackFunctionName) {
        String value;
        if (this.controller.getBatteryLevel() == -1) {
            value = "'unavailable'";
        } else {
            value = Float.toString(((float) this.controller.getBatteryLevel()) / 100.0f);
        }
        if (callbackFunctionName != null) {
            this.webview.loadUrl(callback(callbackFunctionName, value, false));
        }
    }

    public synchronized void deviceUniqueId(String callbackFunctionName) {
        if (callbackFunctionName != null) {
            this.webview.loadUrl(callback(callbackFunctionName, this.controller.getDeviceId(), true));
        }
    }

    public synchronized void deviceModel(String callbackFunctionName) {
        if (callbackFunctionName != null) {
            this.webview.loadUrl(callback(callbackFunctionName, this.controller.getDeviceModel(), true));
        }
    }

    public synchronized void deviceSystemVersion(String callbackFunctionName) {
        if (callbackFunctionName != null) {
            this.webview.loadUrl(callback(callbackFunctionName, this.controller.getAndroidVersion(), true));
        }
    }

    public synchronized void deviceHardwareModel(String callbackFunctionName) {
        if (callbackFunctionName != null) {
            this.webview.loadUrl(callback(callbackFunctionName, this.controller.getDeviceHardwareModel(), true));
        }
    }

    public synchronized void deviceConnectionType(String callbackFunctionName, String errorCallbackFunctionName) {
        if (callbackFunctionName != null) {
            if (!this.controller.permissions.containsKey("android.permission.ACCESS_NETWORK_STATE") && errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, "App does not have the ACCESS_NETWORK_STATE permission.", true));
            } else if (this.controller.getConnectionType().equals("null") && errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, "Error getting connection type.", true));
            } else if (callbackFunctionName != null) {
                this.webview.loadUrl(callback(callbackFunctionName, this.controller.getConnectionType(), true));
            }
        }
    }

    public synchronized void deviceLanguage(String callbackFunctionName) {
        if (callbackFunctionName != null) {
            this.webview.loadUrl(callback(callbackFunctionName, this.controller.getLanguage(), true));
        }
    }

    public synchronized void deviceLocale(String callbackFunctionName) {
        if (callbackFunctionName != null) {
            this.webview.loadUrl(callback(callbackFunctionName, this.controller.getLocale(), true));
        }
    }

    public synchronized void deviceScalingFactor(String callbackFunctionName) {
        if (callbackFunctionName != null) {
            this.webview.loadUrl(callback(callbackFunctionName, Float.toString(this.controller.getContext().getResources().getDisplayMetrics().density), false));
        }
    }

    public synchronized void devicePlatform(String callbackFunctionName) {
        if (callbackFunctionName != null) {
            this.webview.loadUrl(callback(callbackFunctionName, this.controller.getPlatform(), true));
        }
    }

    public synchronized void deviceScreenSize(String callbackFunctionName) {
        if (callbackFunctionName != null) {
            Activity a = this.webviewActivity;
            if (this.expanderActivity != null) {
                a = (Activity) this.expanderActivity.getContext();
            }
            DisplayMetrics dm = new DisplayMetrics();
            a.getWindowManager().getDefaultDisplay().getMetrics(dm);
            int screenWidth = dm.widthPixels;
            int screenHeight = dm.heightPixels;
            StringBuffer value = new StringBuffer();
            value.append(screenWidth).append(",").append(screenHeight);
            this.webview.loadUrl(callback(callbackFunctionName, value.toString(), false));
        }
    }

    public synchronized void deviceScreenDensity(String callbackFunctionName) {
        if (callbackFunctionName != null) {
            this.webview.loadUrl(callback(callbackFunctionName, Float.toString(this.controller.getContext().getResources().getDisplayMetrics().xdpi), false));
        }
    }

    public synchronized void deviceCurrentOrientation(String callbackFunctionName) {
        int orientation;
        if (callbackFunctionName != null) {
            Activity a = this.webviewActivity;
            if (this.expanderActivity != null) {
                a = (Activity) this.expanderActivity.getContext();
            }
            a.getWindowManager().getDefaultDisplay().getMetrics(new DisplayMetrics());
            if (a.getResources().getConfiguration().orientation == 2) {
                orientation = 0;
            } else {
                orientation = 1;
            }
            if (orientation == 0) {
                this.webview.loadUrl(callback(callbackFunctionName, "landscape", true));
            } else {
                this.webview.loadUrl(callback(callbackFunctionName, "portrait", true));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void deviceFlashlightResume() throws Exception {
        if (!this.controller.permissions.containsKey("android.permission.CAMERA")) {
            throw new Exception("Application does not have the CAMERA permission.");
        }
        if (this.camera != null) {
            this.camera.release();
        }
        this.camera = Camera.open();
        Camera.Parameters p = this.camera.getParameters();
        if (this.flashMode.equals("null")) {
            try {
                this.flashMode = (String) Camera.Parameters.class.getMethod("getFlashMode", new Class[0]).invoke(p, new Object[0]);
            } catch (Exception e) {
            }
        }
        try {
            Camera.Parameters.class.getMethod("setFlashMode", String.class).invoke(p, "torch");
        } catch (Exception e2) {
        }
        this.camera.setParameters(p);
        this.flashlightOn = true;
    }

    public synchronized void deviceFlashlightOn(String callbackFunctionName, String errorCallbackFunctionName) {
        try {
            if (!this.webview.displayed) {
                throw new Exception("Ad not yet displayed.");
            } else if (this.autoplay || this.userHasInteracted) {
                deviceFlashlightResume();
                if (callbackFunctionName != null) {
                    this.webview.loadUrl(callback(callbackFunctionName, "", false));
                }
            } else {
                StringBuilder s = new StringBuilder("window.MOBCLIX.deviceFlashlightOn('");
                s.append(callbackFunctionName).append("','").append(errorCallbackFunctionName).append("');");
                checkPermissionsForUserInteraction(s.toString(), errorCallbackFunctionName);
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void deviceFlashlightPause(boolean pausing) throws Exception {
        if (!this.controller.permissions.containsKey("android.permission.CAMERA")) {
            throw new Exception("Application does not have the CAMERA permission.");
        } else if (this.camera == null) {
            throw new Exception("Flashlight isn't on.");
        } else {
            Camera.Parameters p = this.camera.getParameters();
            String mode = "auto";
            if (!this.flashMode.equals("null")) {
                mode = this.flashMode;
            }
            try {
                Camera.Parameters.class.getMethod("setFlashMode", String.class).invoke(p, mode);
            } catch (Exception e) {
            }
            this.camera.setParameters(p);
            this.camera.release();
            this.camera = null;
            if (!pausing) {
                this.flashlightOn = false;
            }
        }
    }

    public synchronized void deviceFlashlightOff(String callbackFunctionName, String errorCallbackFunctionName) {
        try {
            if (!this.webview.displayed) {
                throw new Exception("Ad not yet displayed.");
            } else if (this.autoplay || this.userHasInteracted) {
                deviceFlashlightPause(false);
                if (callbackFunctionName != null) {
                    this.webview.loadUrl(callback(callbackFunctionName, "", false));
                }
            } else {
                StringBuilder s = new StringBuilder("window.MOBCLIX.deviceFlashlightOff('");
                s.append(callbackFunctionName).append("','").append(errorCallbackFunctionName).append("');");
                checkPermissionsForUserInteraction(s.toString(), errorCallbackFunctionName);
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
            }
        }
    }

    public synchronized void calendarGetCalendars(String callbackFunctionName, String errorCallbackFunctionName) {
        Uri calendars;
        try {
            if (!this.webview.displayed) {
                throw new Exception("Ad not yet displayed.");
            } else if (!this.autoplay && this.requireUserInteraction && !this.userHasInteracted) {
                StringBuilder sb = new StringBuilder("window.MOBCLIX.calendarGetCalendars('");
                sb.append(callbackFunctionName).append("','").append(errorCallbackFunctionName).append("');");
                checkPermissionsForUserInteraction(sb.toString(), errorCallbackFunctionName);
            } else if (!this.controller.permissions.containsKey("android.permission.READ_CALENDAR")) {
                throw new Exception("Application does not have the READ_CALENDAR permission.");
            } else if (this.userPermissions.get(0).intValue() == 0) {
                throw new Exception("User declined access to calendar.");
            } else if (this.userPermissions.get(0).intValue() == -1) {
                checkUserPermission(0, new Mobclix.ObjectOnClickListener(callbackFunctionName, errorCallbackFunctionName) {
                    public void onClick(DialogInterface dialog, int id) {
                        MobclixJavascriptInterface.this.userPermissions.put(0, 1);
                        MobclixJavascriptInterface.this.calendarGetCalendars((String) this.obj1, (String) this.obj2);
                    }
                });
            } else {
                String[] projection = {"_id", "name"};
                if (Integer.parseInt(Build.VERSION.SDK) >= 8) {
                    calendars = Uri.parse("content://com.android.calendar/calendars");
                } else {
                    calendars = Uri.parse("content://calendar/calendars");
                }
                Cursor managedCursor = this.webviewActivity.managedQuery(calendars, projection, "selected=1", null, null);
                if (managedCursor.moveToFirst()) {
                    StringBuffer js = new StringBuffer("['");
                    int nameColumn = managedCursor.getColumnIndex("name");
                    int columnIndex = managedCursor.getColumnIndex("_id");
                    int count = 0;
                    do {
                        if (count != 0) {
                            js.append("','");
                        }
                        String calName = managedCursor.getString(nameColumn);
                        if (calName != null) {
                            calName = MobclixUtility.JSONescape(calName);
                        }
                        js.append(calName);
                        count++;
                    } while (managedCursor.moveToNext());
                    js.append("']");
                    if (callbackFunctionName != null) {
                        this.webview.loadUrl(callback(callbackFunctionName, js.toString(), false));
                    }
                }
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
            }
        }
    }

    public synchronized void calendarAddEvent(String eventObject, String successCallbackFunctionName, String errorCallbackFunctionName) {
        Uri calendars;
        String calId;
        Uri eventsUri;
        try {
            if (!this.webview.displayed) {
                throw new Exception("Ad not yet displayed.");
            } else if (!this.autoplay && this.requireUserInteraction && !this.userHasInteracted) {
                StringBuilder sb = new StringBuilder("window.MOBCLIX.calendarAddEvent('");
                sb.append(eventObject).append("','").append(successCallbackFunctionName).append("','").append(errorCallbackFunctionName).append("');");
                checkPermissionsForUserInteraction(sb.toString(), errorCallbackFunctionName);
            } else if (!this.controller.permissions.containsKey("android.permission.WRITE_CALENDAR")) {
                throw new Exception("Application does not have the WRITE_CALENDAR permission.");
            } else if (this.userPermissions.get(1).intValue() == 0) {
                throw new Exception("User declined access to calendar.");
            } else if (this.userPermissions.get(1).intValue() == -1) {
                checkUserPermission(1, new Mobclix.ObjectOnClickListener(eventObject, successCallbackFunctionName, errorCallbackFunctionName) {
                    public void onClick(DialogInterface dialog, int id) {
                        MobclixJavascriptInterface.this.userPermissions.put(1, 1);
                        MobclixJavascriptInterface.this.calendarAddEvent((String) this.obj1, (String) this.obj2, (String) this.obj3);
                    }
                });
            } else {
                JSONObject jSONObject = new JSONObject(eventObject);
                String calendarId = null;
                String[] projection = {"_id", "name"};
                if (Integer.parseInt(Build.VERSION.SDK) >= 8) {
                    calendars = Uri.parse("content://com.android.calendar/calendars");
                } else {
                    calendars = Uri.parse("content://calendar/calendars");
                }
                Cursor managedCursor = this.webviewActivity.managedQuery(calendars, projection, "selected=1", null, null);
                if (managedCursor.moveToFirst()) {
                    int nameColumn = managedCursor.getColumnIndex("name");
                    int idColumn = managedCursor.getColumnIndex("_id");
                    while (true) {
                        String string = managedCursor.getString(nameColumn);
                        calId = managedCursor.getString(idColumn);
                        if (jSONObject.has("calendar") && !calId.equals(URLDecoder.decode(jSONObject.getString("calendar")))) {
                            if (!managedCursor.moveToNext()) {
                                break;
                            }
                        } else {
                            calendarId = calId;
                        }
                    }
                    calendarId = calId;
                    if (calendarId == null) {
                        throw new Exception("Calendar not found.");
                    }
                    ContentValues cv = new ContentValues();
                    cv.put("calendar_id", calendarId);
                    if (jSONObject.has("hasAlarm") && jSONObject.getBoolean("hasAlarm")) {
                        cv.put("hasAlarm", (Integer) 1);
                    }
                    if (jSONObject.has("alarms") && jSONObject.getJSONArray("alarms").length() > 0) {
                        cv.put("hasAlarm", (Integer) 1);
                    }
                    if (jSONObject.has("allDay") && jSONObject.getBoolean("allDay")) {
                        cv.put("allDay", (Integer) 1);
                    }
                    if (!jSONObject.has("startDate")) {
                        throw new Exception("Start date not provided.");
                    } else if (!jSONObject.has("endDate")) {
                        throw new Exception("End date not provided.");
                    } else {
                        String dateTime = jSONObject.getString("startDate");
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mmZ");
                        Date startDate = simpleDateFormat.parse(dateTime);
                        cv.put("dtstart", Long.valueOf(startDate.getTime()));
                        cv.put("dtend", Long.valueOf(simpleDateFormat.parse(jSONObject.getString("endDate")).getTime()));
                        if (jSONObject.has("location")) {
                            cv.put("eventLocation", jSONObject.getString("location"));
                        }
                        if (jSONObject.has("description")) {
                            cv.put("description", jSONObject.getString("description"));
                        }
                        if (jSONObject.has("eventTitle")) {
                            cv.put("title", jSONObject.getString("eventTitle"));
                        }
                        if (jSONObject.has("status")) {
                            if (jSONObject.getString("status").equalsIgnoreCase("tentative")) {
                                cv.put("eventStatus", (Integer) null);
                            }
                            if (jSONObject.getString("status").equalsIgnoreCase("confirmed")) {
                                cv.put("eventStatus", (Integer) 1);
                            }
                            if (jSONObject.getString("status").equalsIgnoreCase("canceled")) {
                                cv.put("eventStatus", (Integer) 2);
                            }
                        }
                        if (jSONObject.has("visibility")) {
                            if (jSONObject.getString("visibility").equalsIgnoreCase("default")) {
                                cv.put("visibility", (Integer) null);
                            }
                            if (jSONObject.getString("visibility").equalsIgnoreCase("confidential")) {
                                cv.put("visibility", (Integer) 1);
                            }
                            if (jSONObject.getString("visibility").equalsIgnoreCase("private")) {
                                cv.put("visibility", (Integer) 2);
                            }
                            if (jSONObject.getString("visibility").equalsIgnoreCase("public")) {
                                cv.put("visibility", (Integer) 3);
                            }
                        }
                        if (jSONObject.has("transparency")) {
                            if (jSONObject.getString("transparency").equalsIgnoreCase("opaque")) {
                                cv.put("status", (Integer) null);
                            }
                            if (jSONObject.getString("transparency").equalsIgnoreCase("transparent")) {
                                cv.put("status", (Integer) 1);
                            }
                        }
                        if (Integer.parseInt(Build.VERSION.SDK) >= 8) {
                            eventsUri = Uri.parse("content://com.android.calendar/events");
                        } else {
                            eventsUri = Uri.parse("content://calendar/events");
                        }
                        ContentResolver cr = this.webviewActivity.getContentResolver();
                        Uri newEvent = cr.insert(eventsUri, cv);
                        if (newEvent != null && jSONObject.has("alarms")) {
                            JSONArray alarms = jSONObject.getJSONArray("alarms");
                            for (int i = 0; i < alarms.length(); i++) {
                                int diffInMinute = (int) ((startDate.getTime() - simpleDateFormat.parse(alarms.getString(i)).getTime()) / 60000);
                                long id = Long.parseLong(newEvent.getLastPathSegment());
                                ContentValues values = new ContentValues();
                                values.put("event_id", Long.valueOf(id));
                                values.put("method", (Integer) 1);
                                values.put("minutes", Integer.valueOf(diffInMinute));
                                if (Integer.parseInt(Build.VERSION.SDK) >= 8) {
                                    cr.insert(Uri.parse("content://com.android.calendar/reminders"), values);
                                } else {
                                    cr.insert(Uri.parse("content://calendar/reminders"), values);
                                }
                            }
                        }
                        if (successCallbackFunctionName != null) {
                            this.webview.loadUrl(callback(successCallbackFunctionName, "", false));
                        }
                    }
                } else {
                    throw new Exception("No calendars found.");
                }
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
            }
        }
    }

    public synchronized void calendarQueryEvents(String eventObject, String eventsCallbackFunctionName, String errorCallbackFunctionName) {
        Uri calendarUri;
        Uri.Builder builder;
        try {
            if (!this.webview.displayed) {
                throw new Exception("Ad not yet displayed.");
            } else if (!this.autoplay && this.requireUserInteraction && !this.userHasInteracted) {
                StringBuilder sb = new StringBuilder("window.MOBCLIX.calendarQueryEvents('");
                sb.append(eventObject).append("','").append(eventsCallbackFunctionName).append("','").append(errorCallbackFunctionName).append("');");
                checkPermissionsForUserInteraction(sb.toString(), errorCallbackFunctionName);
            } else if (!this.controller.permissions.containsKey("android.permission.READ_CALENDAR")) {
                throw new Exception("Application does not have the READ_CALENDAR permission.");
            } else if (this.userPermissions.get(0).intValue() == 0) {
                throw new Exception("User declined access to calendar.");
            } else if (this.userPermissions.get(0).intValue() == -1) {
                checkUserPermission(0, new Mobclix.ObjectOnClickListener(eventObject, eventsCallbackFunctionName, errorCallbackFunctionName) {
                    public void onClick(DialogInterface dialog, int id) {
                        MobclixJavascriptInterface.this.userPermissions.put(0, 1);
                        MobclixJavascriptInterface.this.calendarQueryEvents((String) this.obj1, (String) this.obj2, (String) this.obj3);
                    }
                });
            } else {
                JSONObject jSONObject = new JSONObject(eventObject);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mmZ");
                long startDate = 0;
                long endDate = 0;
                String calendar = null;
                if (jSONObject.has("startDate")) {
                    startDate = simpleDateFormat.parse(jSONObject.getString("startDate")).getTime();
                }
                if (jSONObject.has("endDate")) {
                    endDate = simpleDateFormat.parse(jSONObject.getString("endDate")).getTime();
                }
                if (jSONObject.has("calendar")) {
                    calendar = jSONObject.getString("calendar");
                }
                if (startDate == 0 && endDate == 0 && calendar == null) {
                    throw new Exception("No query parameters supplied.");
                }
                HashMap<String, String> calendars = new HashMap<>();
                String[] projection = {"_id", "name"};
                if (Integer.parseInt(Build.VERSION.SDK) >= 8) {
                    calendarUri = Uri.parse("content://com.android.calendar/calendars");
                } else {
                    calendarUri = Uri.parse("content://calendar/calendars");
                }
                Cursor managedCursor = this.webviewActivity.managedQuery(calendarUri, projection, "selected=1", null, null);
                if (managedCursor.moveToFirst()) {
                    int nameColumn = managedCursor.getColumnIndex("name");
                    int idColumn = managedCursor.getColumnIndex("_id");
                    do {
                        String calName = managedCursor.getString(nameColumn);
                        if (calName == null) {
                            calName = "null";
                        }
                        String calId = managedCursor.getString(idColumn);
                        if (calendar == null) {
                            calendars.put(calId, calName);
                        } else if (calName.equals(calendar)) {
                            calendars.put(calId, calName);
                        }
                    } while (managedCursor.moveToNext());
                    if (calendars.isEmpty()) {
                        throw new Exception("Calendar not found.");
                    }
                    JSONArray events = new JSONArray();
                    for (String id : calendars.keySet()) {
                        if (Integer.parseInt(Build.VERSION.SDK) >= 8) {
                            builder = Uri.parse("content://com.android.calendar/instances/when").buildUpon();
                        } else {
                            builder = Uri.parse("content://calendar/instances/when").buildUpon();
                        }
                        long now = new Date().getTime();
                        if (startDate != 0) {
                            ContentUris.appendId(builder, startDate);
                        } else {
                            ContentUris.appendId(builder, now - 864000000000L);
                        }
                        if (endDate != 0) {
                            ContentUris.appendId(builder, endDate);
                        } else {
                            ContentUris.appendId(builder, 864000000000L + now);
                        }
                        Cursor eventCursor = this.webviewActivity.getContentResolver().query(builder.build(), new String[]{"hasAlarm", "allDay", "begin", "end", "eventLocation", "description", "title", "eventStatus", "visibility", "transparency"}, "Calendars._id=" + id, null, "startDay ASC, startMinute ASC");
                        while (eventCursor.moveToNext()) {
                            JSONObject e = new JSONObject();
                            e.put("calendar", calendars.get(id));
                            if (eventCursor.getString(0) != null) {
                                e.put("hasAlarm", Integer.parseInt(eventCursor.getString(0)));
                            }
                            if (eventCursor.getString(1) != null) {
                                e.put("allDay", Integer.parseInt(eventCursor.getString(1)));
                            }
                            e.put("startDate", simpleDateFormat.format(new Date(eventCursor.getLong(2))));
                            e.put("endDate", simpleDateFormat.format(new Date(eventCursor.getLong(3))));
                            if (eventCursor.getString(4) != null) {
                                e.put("location", eventCursor.getString(4));
                            }
                            if (eventCursor.getString(5) != null) {
                                e.put("description", eventCursor.getString(5));
                            }
                            if (eventCursor.getString(6) != null) {
                                e.put("eventTitle", eventCursor.getString(6));
                            }
                            e.put("status", eventCursor.getInt(7));
                            e.put("visibility", eventCursor.getInt(8));
                            e.put("transparency", eventCursor.getInt(9));
                            events.put(e);
                        }
                    }
                    if (eventsCallbackFunctionName != null) {
                        StringBuilder sb2 = new StringBuilder("eval('(");
                        sb2.append(MobclixUtility.JSONescape(events.toString()));
                        sb2.append(")')");
                        this.webview.loadUrl(callback(eventsCallbackFunctionName, sb2.toString(), false));
                    }
                } else {
                    throw new Exception("No calendars found.");
                }
            }
        } catch (Exception e2) {
            Exception e3 = e2;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e3.toString()), true));
            }
        }
    }

    public synchronized void contactsAddContact(String contactObject, String successCallbackFunctionName, String errorCallbackFunctionName) {
        boolean useLegacyContactsAPI = false;
        try {
            if (!this.webview.displayed) {
                throw new Exception("Ad not yet displayed.");
            } else if (this.autoplay || !this.requireUserInteraction || this.userHasInteracted) {
                int sdkVersion = Integer.parseInt(Build.VERSION.SDK);
                if (this.controller.permissions.containsKey("android.permission.WRITE_CONTACTS") || sdkVersion >= 5) {
                    if (!this.controller.permissions.containsKey("android.permission.READ_CONTACTS") || !this.controller.permissions.containsKey("android.permission.GET_ACCOUNTS")) {
                        useLegacyContactsAPI = true;
                    }
                    if (this.userPermissions.get(3).intValue() == 0) {
                        throw new Exception("User declined access to contacts.");
                    } else if (this.userPermissions.get(3).intValue() == -1) {
                        checkUserPermission(3, new Mobclix.ObjectOnClickListener(contactObject, successCallbackFunctionName, errorCallbackFunctionName) {
                            public void onClick(DialogInterface dialog, int id) {
                                MobclixJavascriptInterface.this.userPermissions.put(3, 1);
                                MobclixJavascriptInterface.this.contactsAddContact((String) this.obj1, (String) this.obj2, (String) this.obj3);
                            }
                        });
                    } else {
                        Activity currentActivity = this.webviewActivity;
                        if (this.expanderActivity != null) {
                            currentActivity = (Activity) this.expanderActivity.getContext();
                        }
                        JSONObject contact = new JSONObject(contactObject);
                        if (sdkVersion < 5) {
                            this.mMobclixContacts.addContact(contact, currentActivity);
                        } else if (!useLegacyContactsAPI) {
                            this.mMobclixContacts.addContact(contact, currentActivity);
                        } else {
                            Intent mIntent = new Intent();
                            String packageName = this.webviewActivity.getPackageName();
                            this.controller.cameraWebview = new SoftReference<>(this.webview);
                            this.contactCallbackFunctionName = successCallbackFunctionName;
                            this.contactErrorCallbackFunctionName = errorCallbackFunctionName;
                            mIntent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "addContact").putExtra(String.valueOf(packageName) + ".data", contactObject);
                            Activity a = this.webviewActivity;
                            if (this.expanderActivity != null) {
                                this.expanderActivity.wasAdActivity = true;
                            }
                            a.startActivity(mIntent);
                        }
                        if (successCallbackFunctionName != null) {
                            this.webview.loadUrl(callback(successCallbackFunctionName, "", false));
                        }
                    }
                } else {
                    throw new Exception("Application does not have the WRITE_CONTACTS permission.");
                }
            } else {
                StringBuilder s = new StringBuilder("window.MOBCLIX.contactsAddContact('");
                s.append(contactObject).append("','").append(successCallbackFunctionName).append("','").append(errorCallbackFunctionName).append("');");
                checkPermissionsForUserInteraction(s.toString(), errorCallbackFunctionName);
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
            }
        }
    }

    public synchronized void contactsGetContact(String successCallbackFunctionName, String errorCallbackFunctionName) {
        try {
            if (!this.webview.displayed) {
                throw new Exception("Ad not yet displayed.");
            } else if (!this.autoplay && this.requireUserInteraction && !this.userHasInteracted) {
                StringBuilder s = new StringBuilder("window.MOBCLIX.contactsGetContact('");
                s.append(successCallbackFunctionName).append("','").append(errorCallbackFunctionName).append("');");
                checkPermissionsForUserInteraction(s.toString(), errorCallbackFunctionName);
            } else if (!this.controller.permissions.containsKey("android.permission.READ_CONTACTS")) {
                throw new Exception("Application does not have the READ_CONTACTS permission.");
            } else if (this.userPermissions.get(2).intValue() == 0) {
                throw new Exception("User declined access to contacts.");
            } else if (this.userPermissions.get(2).intValue() == -1) {
                checkUserPermission(2, new Mobclix.ObjectOnClickListener(successCallbackFunctionName, errorCallbackFunctionName) {
                    public void onClick(DialogInterface dialog, int id) {
                        MobclixJavascriptInterface.this.userPermissions.put(2, 1);
                        MobclixJavascriptInterface.this.contactsGetContact((String) this.obj1, (String) this.obj2);
                    }
                });
            } else {
                Intent mIntent = new Intent();
                String packageName = this.webviewActivity.getPackageName();
                mIntent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "contact");
                this.controller.cameraWebview = new SoftReference<>(this.webview);
                this.contactCallbackFunctionName = successCallbackFunctionName;
                this.contactErrorCallbackFunctionName = errorCallbackFunctionName;
                Activity a = this.webviewActivity;
                if (this.expanderActivity != null) {
                    this.expanderActivity.wasAdActivity = true;
                }
                a.startActivity(mIntent);
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void contactAdded() {
        if (this.contactCallbackFunctionName != null) {
            this.webview.loadUrl(callback(this.contactCallbackFunctionName, "", false));
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void contactPicked(Uri contactUri) {
        Activity a = this.webviewActivity;
        if (this.expanderActivity != null) {
            this.expanderActivity.wasAdActivity = true;
        }
        JSONObject contact = this.mMobclixContacts.loadContact(a.getContentResolver(), contactUri);
        if (contact == null && this.contactErrorCallbackFunctionName != null) {
            this.webview.loadUrl(callback(this.contactErrorCallbackFunctionName, "Error getting contact.", true));
        } else if (this.contactCallbackFunctionName != null) {
            this.webview.loadUrl(callback(this.contactCallbackFunctionName, "eval('(" + MobclixUtility.JSONescape(contact.toString()) + ")')", false));
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void contactCanceled(String e) {
        if (this.contactErrorCallbackFunctionName != null) {
            this.webview.loadUrl(callback(this.contactErrorCallbackFunctionName, e, true));
        }
    }

    public synchronized void mediaQuerySongs(String songObject, String successCallbackFunctionName, String errorCallbackFunctionName) {
        String composer;
        String albumArtist;
        String artist;
        String albumTitle;
        String title;
        try {
            if (!this.webview.displayed) {
                throw new Exception("Ad not yet displayed.");
            } else if (this.autoplay || !this.requireUserInteraction || this.userHasInteracted) {
                Activity currentActivity = this.webviewActivity;
                if (this.expanderActivity != null) {
                    currentActivity = (Activity) this.expanderActivity.getContext();
                }
                JSONObject jSONObject = new JSONObject(songObject);
                HashMap<String, String> genreIdMap = new HashMap<>();
                HashMap<String, String> genreNameMap = new HashMap<>();
                Cursor c = currentActivity.managedQuery(MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI, new String[]{"_id", "name"}, null, null, null);
                c.moveToFirst();
                while (!c.isAfterLast()) {
                    genreIdMap.put(c.getString(0), c.getString(1));
                    genreNameMap.put(c.getString(1).toLowerCase(), c.getString(0).toLowerCase());
                    c.moveToNext();
                }
                c.close();
                StringBuilder sb = new StringBuilder("is_music");
                sb.append(" != 0");
                if (jSONObject.has("title") && (title = jSONObject.getString("title")) != null && !title.equals("")) {
                    sb.append(" AND ").append("title").append(" = '").append(title).append("'");
                }
                if (jSONObject.has("albumTitle") && (albumTitle = jSONObject.getString("albumTitle")) != null && !albumTitle.equals("")) {
                    sb.append(" AND ").append("album").append(" = '").append(albumTitle).append("'");
                }
                if (jSONObject.has("artist") && (artist = jSONObject.getString("artist")) != null && !artist.equals("")) {
                    sb.append(" AND ").append("artist").append(" = '").append(artist).append("'");
                }
                if (jSONObject.has("albumArtist") && (albumArtist = jSONObject.getString("albumArtist")) != null && !albumArtist.equals("")) {
                    sb.append(" AND ").append("artist").append(" = '").append(albumArtist).append("'");
                }
                if (jSONObject.has("composer") && (composer = jSONObject.getString("composer")) != null && !composer.equals("")) {
                    sb.append(" AND ").append("composer").append(" = '").append(composer).append("'");
                }
                String[] projection = {"_id", "_data", "artist", "title", "album", "composer"};
                JSONArray songs = new JSONArray();
                String genre = null;
                if (jSONObject.has("genre")) {
                    genre = jSONObject.getString("genre");
                    if (genre.equals("")) {
                        genre = null;
                    }
                }
                if (genre == null) {
                    int count = 0;
                    StringBuilder sb2 = new StringBuilder("");
                    for (String genreId : genreIdMap.keySet()) {
                        Cursor c2 = currentActivity.managedQuery(makeGenreUri(genreId), projection, String.valueOf(sb.toString()) + " AND " + "_id" + " NOT IN (-1" + sb2.toString() + ")", null, null);
                        while (c2.moveToNext() && count <= 75) {
                            songs.put(songCursorToJSON(c2, (String) genreIdMap.get(genreId)));
                            sb2.append(",").append(c2.getString(c2.getColumnIndexOrThrow("_id")));
                            count++;
                        }
                        c2.close();
                    }
                    Cursor c3 = currentActivity.managedQuery(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, projection, String.valueOf(sb.toString()) + " AND " + "_id" + " NOT IN (-1" + sb2.toString() + ")", null, null);
                    while (c3.moveToNext() && count <= 75) {
                        songs.put(songCursorToJSON(c3, null));
                        count++;
                    }
                    c3.close();
                } else if (genreNameMap.containsKey(genre.toLowerCase())) {
                    int count2 = 0;
                    String genreId2 = (String) genreNameMap.get(genre.toLowerCase());
                    Cursor c4 = currentActivity.managedQuery(makeGenreUri(genreId2), projection, sb.toString(), null, null);
                    while (c4.moveToNext() && count2 <= 75) {
                        songs.put(songCursorToJSON(c4, (String) genreIdMap.get(genreId2)));
                        count2++;
                    }
                    c4.close();
                }
                if (successCallbackFunctionName != null) {
                    StringBuilder sb3 = new StringBuilder("eval('(");
                    sb3.append(MobclixUtility.JSONescape(songs.toString()));
                    sb3.append(")')");
                    this.webview.loadUrl(callback(successCallbackFunctionName, sb3.toString(), false));
                }
            } else {
                StringBuilder sb4 = new StringBuilder("window.MOBCLIX.mediaQuerySongs('");
                sb4.append(songObject).append("','").append(successCallbackFunctionName).append("','").append(errorCallbackFunctionName).append("');");
                checkPermissionsForUserInteraction(sb4.toString(), errorCallbackFunctionName);
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
            }
        }
    }

    private JSONObject songCursorToJSON(Cursor c, String genre) throws Exception {
        JSONObject s = new JSONObject();
        s.put("title", c.getString(c.getColumnIndexOrThrow("title")));
        s.put("albumTitle", c.getString(c.getColumnIndexOrThrow("album")));
        s.put("artist", c.getString(c.getColumnIndexOrThrow("artist")));
        s.put("albumArtist", "");
        s.put("composer", c.getString(c.getColumnIndexOrThrow("composer")));
        if (genre != null) {
            s.put("genre", genre);
        }
        return s;
    }

    private Uri makeGenreUri(String genreId) {
        return Uri.parse(MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI.toString() + "/" + genreId + "/" + "members");
    }

    public synchronized void mediaGetImage(int width, int height, String successCallbackFunctionName, String errorCallbackFunctionName) {
        try {
            if (!this.webview.displayed) {
                throw new Exception("Ad not yet displayed.");
            } else if (this.autoplay || !this.requireUserInteraction || this.userHasInteracted) {
                Intent mIntent = new Intent();
                String packageName = this.webviewActivity.getPackageName();
                mIntent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "gallery");
                this.photoWidth = width;
                this.photoHeight = height;
                this.photoCallbackFunctionName = successCallbackFunctionName;
                this.photoErrorCallbackFunctionName = errorCallbackFunctionName;
                this.controller.cameraWebview = new SoftReference<>(this.webview);
                Activity a = this.webviewActivity;
                if (this.expanderActivity != null) {
                    this.expanderActivity.wasAdActivity = true;
                }
                a.startActivity(mIntent);
            } else {
                StringBuilder s = new StringBuilder("window.MOBCLIX.mediaGetImage(");
                s.append(width).append(",").append(height).append(",'").append(successCallbackFunctionName).append("','").append(errorCallbackFunctionName).append("');");
                checkPermissionsForUserInteraction(s.toString(), errorCallbackFunctionName);
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
            }
        }
    }

    public synchronized void mediaSendImageToServer(String serverUrl, String successCallbackFunctionName, String errorCallbackFunctionName) {
        try {
            if (!this.webview.displayed) {
                throw new Exception("Ad not yet displayed.");
            } else if (!this.autoplay && this.requireUserInteraction && !this.userHasInteracted) {
                StringBuilder s = new StringBuilder("window.MOBCLIX.mediaSendImageToServer('");
                s.append(serverUrl).append("','").append(successCallbackFunctionName).append("','").append(errorCallbackFunctionName).append("');");
                checkPermissionsForUserInteraction(s.toString(), errorCallbackFunctionName);
            } else if (this.photoSendingThread != null) {
                throw new Exception("Image already being sent.");
            } else {
                Intent mIntent = new Intent();
                String packageName = this.webviewActivity.getPackageName();
                mIntent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "sendToServer");
                this.photoServerUrl = serverUrl;
                this.photoCallbackFunctionName = successCallbackFunctionName;
                this.photoErrorCallbackFunctionName = errorCallbackFunctionName;
                this.controller.cameraWebview = new SoftReference<>(this.webview);
                Activity a = this.webviewActivity;
                if (this.expanderActivity != null) {
                    this.expanderActivity.wasAdActivity = true;
                }
                a.startActivity(mIntent);
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void sendImageToServer(String path) {
        if (this.photoSendingThread == null) {
            this.photoSendingThread = new Thread(new SendImageToServerThread(this.photoServerUrl, path));
            this.photoSendingThread.run();
        } else if (this.photoErrorCallbackFunctionName != null) {
            this.webview.loadUrl(callback(this.photoErrorCallbackFunctionName, "Image already being sent.", true));
        }
    }

    class SendImageToServerThread implements Runnable {
        private String imagePath;
        private String serverUrl;

        SendImageToServerThread(String p, String u) {
            this.serverUrl = u;
            this.imagePath = p;
        }

        public void run() {
            try {
                Bitmap pic = BitmapFactory.decodeFile(new File(this.imagePath).getAbsolutePath());
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                pic.compress(Bitmap.CompressFormat.JPEG, 50, bos);
                HttpURLConnection connection = (HttpURLConnection) new URL(this.serverUrl).openConnection();
                connection.setRequestMethod("POST");
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.connect();
                DataOutputStream out = new DataOutputStream(connection.getOutputStream());
                bos.writeTo(out);
                out.flush();
                out.close();
                bos.close();
                System.gc();
                if (MobclixJavascriptInterface.this.photoCallbackFunctionName != null) {
                    MobclixJavascriptInterface.this.webview.loadUrl(MobclixJavascriptInterface.this.callback(MobclixJavascriptInterface.this.photoCallbackFunctionName, "", false));
                }
            } catch (Exception e) {
                Exception e2 = e;
                if (!(Mobclix.getInstance().cameraWebview == null || Mobclix.getInstance().cameraWebview.get() == null)) {
                    Mobclix.getInstance().cameraWebview.get().getJavascriptInterface().photoCanceled(e2.toString());
                    Mobclix.getInstance().cameraWebview.get().getJavascriptInterface().photoCanceled("Error processing photo.");
                }
            }
            MobclixJavascriptInterface.this.photoSendingThread = null;
        }
    }

    private class ExpanderRunnable implements Runnable, Animation.AnimationListener {
        int duration;
        int fHeight;
        int fLeftMargin;
        int fTopMargin;
        int fWidth;

        ExpanderRunnable(int fTopMargin2, int fLeftMargin2, int fWidth2, int fHeight2, int duration2) {
            this.fTopMargin = fTopMargin2;
            this.fLeftMargin = fLeftMargin2;
            this.fWidth = fWidth2;
            this.fHeight = fHeight2;
            this.duration = duration2;
        }

        public void run() {
            ((MobclixBrowserActivity.MobclixExpander) MobclixJavascriptInterface.this.expanderActivity).expand(this.fLeftMargin, this.fTopMargin, this.fWidth, this.fHeight, this.duration, this);
        }

        public void onAnimationEnd(Animation animation) {
            MobclixJavascriptInterface.this.adFinishedResizing();
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    public synchronized void displayResizeTo(int fLeftMargin, int fTopMargin, int fWidth, int fHeight, float duration2, String successCallbackFunctionName, String errorCallbackFunctionName) {
        try {
            if (!this.webview.displayed) {
                throw new Exception("Ad not yet displayed.");
            } else if (!this.autoplay && !this.userHasInteracted) {
                StringBuilder sb = new StringBuilder("window.MOBCLIX.displayResizeTo(");
                sb.append(fLeftMargin).append(",").append(fTopMargin).append(",").append(fWidth).append(",").append(fHeight).append(",").append(duration2).append(",'").append(successCallbackFunctionName).append("','").append(errorCallbackFunctionName).append("');");
                checkPermissionsForUserInteraction(sb.toString(), errorCallbackFunctionName);
            } else if (this.fullScreenAdView) {
                throw new Exception("FullScreenAdView cannot resize.");
            } else {
                if (duration2 > 3.0f) {
                    duration2 = 3.0f;
                }
                if (duration2 < 0.0f) {
                    duration2 = 0.1f;
                }
                float duration3 = duration2 * 1000.0f;
                if (!this.expanded) {
                    Rect rectgle = new Rect();
                    Window window = this.webviewActivity.getWindow();
                    window.getDecorView().getWindowVisibleDisplayFrame(rectgle);
                    int statusBarHeight = rectgle.top;
                    int top = window.findViewById(16908290).getTop() - statusBarHeight;
                    this.webviewActivity.getWindowManager().getDefaultDisplay().getMetrics(new DisplayMetrics());
                    int[] coords = new int[2];
                    this.webview.getLocationInWindow(coords);
                    int topMargin = coords[1];
                    int leftMargin = coords[0];
                    Mobclix.getInstance().webview = new SoftReference<>(this.webview);
                    JSONObject data = new JSONObject();
                    data.put("statusBarHeight", statusBarHeight);
                    data.put("topMargin", topMargin);
                    data.put("leftMargin", leftMargin);
                    data.put("fTopMargin", fTopMargin);
                    data.put("fLeftMargin", fLeftMargin);
                    data.put("fWidth", fWidth);
                    data.put("fHeight", fHeight);
                    data.put("duration", (double) duration3);
                    Intent mIntent = new Intent();
                    String packageName = this.webview.getContext().getPackageName();
                    mIntent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "expander").putExtra(String.valueOf(packageName) + ".data", data.toString());
                    this.webview.getWorkingContext().startActivity(mIntent);
                    this.expanded = true;
                    if (successCallbackFunctionName != null) {
                        this.webview.loadUrl(callback(successCallbackFunctionName, "", false));
                    }
                } else if (this.expanderActivity == null) {
                    throw new Exception("Error resizing advertisement.");
                } else {
                    ((Activity) this.expanderActivity.getContext()).runOnUiThread(new ExpanderRunnable(fTopMargin, fLeftMargin, fWidth, fHeight, (int) duration3));
                    if (successCallbackFunctionName != null) {
                        this.webview.loadUrl(callback(successCallbackFunctionName, "", false));
                    }
                }
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
            }
        }
    }

    public synchronized void displayExpandToFullScreen(float duration2, String successCallbackFunctionName, String errorCallbackFunctionName) {
        DisplayMetrics dm = new DisplayMetrics();
        this.webviewActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        displayResizeTo(0, 0, dm.widthPixels, dm.heightPixels, duration2, successCallbackFunctionName, errorCallbackFunctionName);
    }

    public synchronized void displayContractAd(float animationDuration, String successCallbackFunctionName, String errorCallbackFunctionName) {
        try {
            if (!this.webview.displayed) {
                throw new Exception("Ad not yet displayed.");
            }
            this.duration = (int) (1000.0f * animationDuration);
            if (!this.expanded || this.expanderActivity == null) {
                throw new Exception("Error contracting ad.");
            }
            ((Activity) this.expanderActivity.getContext()).runOnUiThread(new Runnable() {
                public void run() {
                    MobclixJavascriptInterface.this.expanderActivity.exit(MobclixJavascriptInterface.this.duration);
                }
            });
            this.expanded = false;
            if (successCallbackFunctionName != null) {
                this.webview.loadUrl(callback(successCallbackFunctionName, "", false));
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
            }
        }
    }

    public synchronized void displayOpenInBrowser(String url, String successCallbackFunctionName, String errorCallbackFunctionName) {
        try {
            Uri uri = Uri.parse(url);
            if (!this.webview.displayed) {
                throw new Exception("Ad not yet displayed.");
            } else if (this.autoplay || this.userHasInteracted) {
                if (this.expanderActivity != null) {
                    this.expanderActivity.wasAdActivity = true;
                }
                if (uri.getScheme().equals("sms")) {
                    String[] smsUrl = url.split(":");
                    String tmp = String.valueOf(smsUrl[0]) + "://";
                    for (int i = 1; i < smsUrl.length; i++) {
                        tmp = String.valueOf(tmp) + smsUrl[i];
                    }
                    String body = Uri.parse(tmp).getQueryParameter("body");
                    Intent mIntent = new Intent("android.intent.action.VIEW", Uri.parse(url.split("\\?")[0]));
                    mIntent.putExtra("sms_body", body);
                    this.webviewActivity.startActivity(mIntent);
                } else {
                    this.webviewActivity.startActivity(new Intent("android.intent.action.VIEW", uri));
                }
                if (successCallbackFunctionName != null) {
                    this.webview.loadUrl(callback(successCallbackFunctionName, "", false));
                }
            } else {
                StringBuilder s = new StringBuilder("window.MOBCLIX.displayOpenInBrowser('");
                s.append(url).append("','").append(successCallbackFunctionName).append("','").append(errorCallbackFunctionName).append("');");
                checkPermissionsForUserInteraction(s.toString(), errorCallbackFunctionName);
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
            }
        }
    }

    public synchronized void setAdDidDisplayCallback(String adDidDisplayCallbackFunctionName2, String successCallbackFunctionName, String errorCallbackFunctionName) {
        try {
            this.adDidDisplayCallbackFunctionName = adDidDisplayCallbackFunctionName2;
            if (successCallbackFunctionName != null) {
                this.webview.loadUrl(callback(successCallbackFunctionName, "", false));
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void adDidDisplay() {
        if (this.adDidDisplayCallbackFunctionName != null) {
            this.webview.loadUrl(callback(this.adDidDisplayCallbackFunctionName, "", false));
        }
    }

    public synchronized void setAdFinishedResizingCallback(String adFinishedResizingCallbackFunctionName2, String successCallbackFunctionName, String errorCallbackFunctionName) {
        try {
            this.adFinishedResizingCallbackFunctionName = adFinishedResizingCallbackFunctionName2;
            if (successCallbackFunctionName != null) {
                this.webview.loadUrl(callback(successCallbackFunctionName, "", false));
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void adFinishedResizing() {
        if (this.adFinishedResizingCallbackFunctionName != null) {
            this.webview.loadUrl(callback(this.adFinishedResizingCallbackFunctionName, "", false));
        }
    }

    public synchronized void setAdWillContractCallback(String adWillContractCallbackFunctionName2, String successCallbackFunctionName, String errorCallbackFunctionName) {
        try {
            this.adWillContractCallbackFunctionName = adWillContractCallbackFunctionName2;
            if (successCallbackFunctionName != null) {
                this.webview.loadUrl(callback(successCallbackFunctionName, "", false));
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void adWillContract() {
        if (this.adWillContractCallbackFunctionName != null) {
            this.webview.loadUrl(callback(this.adWillContractCallbackFunctionName, "", false));
        }
    }

    public synchronized void setAdWillTerminateCallback(String adWillTerminateCallbackFunctionName2, String successCallbackFunctionName, String errorCallbackFunctionName) {
        try {
            this.adWillTerminateCallbackFunctionName = adWillTerminateCallbackFunctionName2;
            if (successCallbackFunctionName != null) {
                this.webview.loadUrl(callback(successCallbackFunctionName, "", false));
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void adWillTerminate() {
        if (this.adWillTerminateCallbackFunctionName != null) {
            this.webview.loadUrl(callback(this.adWillTerminateCallbackFunctionName, "", false));
        }
    }

    public synchronized void setAdWillBecomeHiddenCallback(String adWillBecomeHiddenCallbackFunctionName2, String successCallbackFunctionName, String errorCallbackFunctionName) {
        try {
            this.adWillBecomeHiddenCallbackFunctionName = adWillBecomeHiddenCallbackFunctionName2;
            if (successCallbackFunctionName != null) {
                this.webview.loadUrl(callback(successCallbackFunctionName, "", false));
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void adWillBecomeHidden() {
        if (this.adWillBecomeHiddenCallbackFunctionName != null) {
            this.webview.loadUrl(callback(this.adWillBecomeHiddenCallbackFunctionName, "", false));
        }
    }

    public synchronized void setAdDidReturnFromHiddenCallback(String adDidReturnFromHiddenCallbackFunctionName2, String successCallbackFunctionName, String errorCallbackFunctionName) {
        try {
            this.adDidReturnFromHiddenCallbackFunctionName = adDidReturnFromHiddenCallbackFunctionName2;
            if (successCallbackFunctionName != null) {
                this.webview.loadUrl(callback(successCallbackFunctionName, "", false));
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void adDidReturnFromHidden() {
        if (this.adDidReturnFromHiddenCallbackFunctionName != null) {
            this.webview.loadUrl(callback(this.adDidReturnFromHiddenCallbackFunctionName, "", false));
        }
    }

    public synchronized void setAllDisplayCallbacks(String adFinishedResizingCallbackFunctionName2, String adWillContractCallbackFunctionName2, String adWillTerminateCallbackFunctionName2, String adWillBecomeHiddenCallbackFunctionName2, String adDidReturnFromHiddenCallbackFunctionName2, String successCallbackFunctionName, String errorCallbackFunctionName) {
        try {
            this.adFinishedResizingCallbackFunctionName = adFinishedResizingCallbackFunctionName2;
            this.adWillContractCallbackFunctionName = adWillContractCallbackFunctionName2;
            this.adWillTerminateCallbackFunctionName = adWillTerminateCallbackFunctionName2;
            this.adWillBecomeHiddenCallbackFunctionName = adWillBecomeHiddenCallbackFunctionName2;
            this.adDidReturnFromHiddenCallbackFunctionName = adDidReturnFromHiddenCallbackFunctionName2;
            if (successCallbackFunctionName != null) {
                this.webview.loadUrl(callback(successCallbackFunctionName, "", false));
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (errorCallbackFunctionName != null) {
                this.webview.loadUrl(callback(errorCallbackFunctionName, MobclixUtility.JSONescape(e2.toString()), true));
            }
        }
    }
}
