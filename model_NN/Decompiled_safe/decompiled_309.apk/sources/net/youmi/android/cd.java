package net.youmi.android;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import java.util.Date;
import java.util.Properties;
import org.json.JSONObject;

class cd {
    static String a = "E2FDAA28C7344D2F9FAA4A0FEC1296AA";
    static String b = "startTime";
    static String c = "lastActiveTime";

    cd() {
    }

    static Properties a(Context context) {
        return aa.a(context, a);
    }

    static void a(Activity activity) {
        try {
            if (!e.i(activity)) {
                int taskId = activity.getTaskId();
                if (e.a(taskId)) {
                    f(activity);
                    return;
                }
                e.b(taskId);
                d(activity);
                e(activity);
            }
        } catch (Exception e) {
        }
    }

    static by b(Activity activity) {
        try {
            Properties a2 = a((Context) activity);
            if (a2 != null) {
                by byVar = new by();
                byVar.a = aa.a(a2, b, 0);
                byVar.b = aa.a(a2, c, 0);
                return byVar;
            }
        } catch (Exception e) {
        }
        return new by();
    }

    private static String c(Activity activity) {
        String str;
        String str2;
        String str3;
        try {
            StringBuilder sb = new StringBuilder(512);
            by b2 = b(activity);
            Location a2 = m.a(activity);
            if (a2 != null) {
                try {
                    str2 = new StringBuilder(String.valueOf(a2.getLongitude())).toString();
                    try {
                        str = new StringBuilder(String.valueOf(a2.getLatitude())).toString();
                    } catch (Exception e) {
                        str3 = str2;
                    }
                } catch (Exception e2) {
                    str3 = "";
                }
            } else {
                str = "";
                str2 = "";
            }
            sb.append("lbt=");
            sb.append(b2.a());
            t.a(sb, "let", new StringBuilder(String.valueOf(b2.b())).toString());
            t.a(sb, "lon", str2);
            t.a(sb, "lat", str);
            t.a(sb, "scn", e.d());
            Date date = new Date(System.currentTimeMillis());
            t.a(sb, activity, date);
            t.a(sb, "sig", as.c(sb.toString(), String.valueOf(n.c()) + t.a().substring(0, (date.getSeconds() % 16) + 1)));
            String sb2 = sb.toString();
            int a3 = ag.a(Integer.MAX_VALUE);
            return String.valueOf(ar.a(sb2, a3)) + "&k=" + a3;
            str2 = str3;
            str = "";
            sb.append("lbt=");
            sb.append(b2.a());
            t.a(sb, "let", new StringBuilder(String.valueOf(b2.b())).toString());
            t.a(sb, "lon", str2);
            t.a(sb, "lat", str);
            t.a(sb, "scn", e.d());
            Date date2 = new Date(System.currentTimeMillis());
            t.a(sb, activity, date2);
            t.a(sb, "sig", as.c(sb.toString(), String.valueOf(n.c()) + t.a().substring(0, (date2.getSeconds() % 16) + 1)));
            String sb22 = sb.toString();
            int a32 = ag.a(Integer.MAX_VALUE);
            return String.valueOf(ar.a(sb22, a32)) + "&k=" + a32;
        } catch (Exception e3) {
            return "";
        }
    }

    private static void d(Activity activity) {
        try {
            int a2 = bd.a(bd.a(cg.d(activity, t.a(cr.a(), c(activity))), "result", (JSONObject) null), "code", -999);
            if (a2 == -999) {
                g.b("Unable to connect to the server, please check your network configuration!");
            } else {
                g.a(activity, a2, 0);
            }
        } catch (Exception e) {
        }
    }

    private static boolean e(Activity activity) {
        try {
            int currentTimeMillis = (int) (System.currentTimeMillis() / 1000);
            Properties properties = new Properties();
            properties.setProperty(b, new StringBuilder(String.valueOf(currentTimeMillis)).toString());
            properties.setProperty(c, new StringBuilder(String.valueOf(currentTimeMillis)).toString());
            return aa.a(activity, properties, a);
        } catch (Exception e) {
            return false;
        }
    }

    private static boolean f(Activity activity) {
        try {
            by b2 = b(activity);
            Properties properties = new Properties();
            long currentTimeMillis = System.currentTimeMillis() / 1000;
            if (b2.a() != 0) {
                properties.setProperty(b, new StringBuilder(String.valueOf(b2.a())).toString());
                properties.setProperty(c, new StringBuilder(String.valueOf(currentTimeMillis)).toString());
            } else {
                properties.setProperty(b, new StringBuilder(String.valueOf(currentTimeMillis)).toString());
                properties.setProperty(c, new StringBuilder(String.valueOf(currentTimeMillis)).toString());
            }
            return aa.a(activity, properties, a);
        } catch (Exception e) {
            return false;
        }
    }
}
