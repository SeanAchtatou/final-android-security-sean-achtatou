package net.youmi.android;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;

class bl {
    bl() {
    }

    static boolean a(Activity activity, WebView webView, String str) {
        if (activity == null) {
            return false;
        }
        if (str == null) {
            return false;
        }
        try {
            String trim = str.trim();
            if (trim.length() == 0) {
                return false;
            }
            if (trim.indexOf("youmi://") == 0) {
                if (a(activity, trim)) {
                    return true;
                }
                if (b(activity, trim)) {
                    return true;
                }
                if (c(activity, trim)) {
                    return true;
                }
                if (d(activity, trim)) {
                    return true;
                }
                if (b(activity, webView, trim)) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
        }
    }

    private static boolean a(Activity activity, String str) {
        String substring;
        try {
            if (str.indexOf("youmi://tel/") == 0 && (substring = str.substring("youmi://tel/".length())) != null) {
                if (ad.e(activity, substring)) {
                    try {
                        a.b(activity, aj.a().f(), substring, 0);
                    } catch (Exception e) {
                    }
                }
                return true;
            }
        } catch (Exception e2) {
        }
        return false;
    }

    private static boolean b(Activity activity, WebView webView, String str) {
        String str2;
        String str3;
        try {
            if (str.indexOf("youmi://wap/") == 0) {
                String substring = str.substring("youmi://wap/".length());
                int indexOf = substring.indexOf("/");
                if (indexOf > -1) {
                    String substring2 = substring.substring(0, indexOf);
                    try {
                        if (substring.length() > indexOf + 1) {
                            str3 = substring.substring(indexOf + 1);
                            if (str3 != null) {
                                try {
                                    str3 = v.a(str3);
                                    str2 = substring2;
                                } catch (Exception e) {
                                    str2 = substring2;
                                    webView.loadUrl(str3);
                                    try {
                                        a.b(activity, aj.a().f(), str2, 3);
                                    } catch (Exception e2) {
                                    }
                                    return true;
                                }
                            } else {
                                str2 = substring2;
                            }
                        } else {
                            str3 = null;
                            str2 = substring2;
                        }
                    } catch (Exception e3) {
                        str3 = null;
                        str2 = substring2;
                        webView.loadUrl(str3);
                        a.b(activity, aj.a().f(), str2, 3);
                        return true;
                    }
                } else {
                    str2 = substring;
                    str3 = null;
                }
                if (!(str2 == null || str3 == null)) {
                    webView.loadUrl(str3);
                    a.b(activity, aj.a().f(), str2, 3);
                }
                return true;
            }
        } catch (Exception e4) {
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x003c A[SYNTHETIC, Splitter:B:16:0x003c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean b(android.app.Activity r8, java.lang.String r9) {
        /*
            r7 = 1
            r6 = 0
            java.lang.String r0 = "youmi://sms/"
            java.lang.String r0 = "youmi://sms/"
            int r0 = r9.indexOf(r0)     // Catch:{ Exception -> 0x0057 }
            if (r0 != 0) goto L_0x0058
            java.lang.String r0 = "youmi://sms/"
            int r0 = r0.length()     // Catch:{ Exception -> 0x0057 }
            java.lang.String r0 = r9.substring(r0)     // Catch:{ Exception -> 0x0057 }
            java.lang.String r1 = "/"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0057 }
            r2 = 0
            r3 = -1
            if (r1 <= r3) goto L_0x0054
            r3 = 0
            java.lang.String r3 = r0.substring(r3, r1)     // Catch:{ Exception -> 0x0057 }
            int r4 = r0.length()     // Catch:{ Exception -> 0x0050 }
            int r5 = r1 + 1
            if (r4 <= r5) goto L_0x0060
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0050 }
            if (r0 == 0) goto L_0x005e
            java.lang.String r0 = net.youmi.android.v.a(r0)     // Catch:{ Exception -> 0x005c }
            r1 = r3
        L_0x003a:
            if (r1 == 0) goto L_0x0058
            boolean r0 = net.youmi.android.ad.a(r8, r1, r0)     // Catch:{ Exception -> 0x0057 }
            if (r0 == 0) goto L_0x004e
            net.youmi.android.ax r0 = net.youmi.android.aj.a()     // Catch:{ Exception -> 0x005a }
            int r0 = r0.f()     // Catch:{ Exception -> 0x005a }
            r2 = 1
            net.youmi.android.a.b(r8, r0, r1, r2)     // Catch:{ Exception -> 0x005a }
        L_0x004e:
            r0 = r7
        L_0x004f:
            return r0
        L_0x0050:
            r0 = move-exception
            r0 = r2
        L_0x0052:
            r1 = r3
            goto L_0x003a
        L_0x0054:
            r1 = r0
            r0 = r2
            goto L_0x003a
        L_0x0057:
            r0 = move-exception
        L_0x0058:
            r0 = r6
            goto L_0x004f
        L_0x005a:
            r0 = move-exception
            goto L_0x004e
        L_0x005c:
            r1 = move-exception
            goto L_0x0052
        L_0x005e:
            r1 = r3
            goto L_0x003a
        L_0x0060:
            r0 = r2
            r1 = r3
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.bl.b(android.app.Activity, java.lang.String):boolean");
    }

    private static boolean c(Activity activity, String str) {
        String str2;
        String str3;
        String str4;
        String str5;
        try {
            if (str.indexOf("youmi://geo/") == 0) {
                String substring = str.substring("youmi://geo/".length());
                int indexOf = substring.indexOf("/");
                if (indexOf > -1) {
                    String substring2 = substring.substring(0, indexOf);
                    try {
                        if (substring.length() > indexOf + 1) {
                            str2 = substring.substring(indexOf + 1);
                            str3 = substring2;
                        } else {
                            str2 = null;
                            str3 = substring2;
                        }
                    } catch (Exception e) {
                        str2 = null;
                        str3 = substring2;
                    }
                } else {
                    str2 = null;
                    str3 = null;
                }
                if (str2 != null) {
                    int indexOf2 = str2.indexOf("/");
                    if (indexOf2 > -1) {
                        String substring3 = str2.substring(0, indexOf2);
                        if (str2.length() > indexOf2 + 1) {
                            str4 = str2.substring(indexOf2 + 1);
                            str5 = substring3;
                        } else {
                            str4 = null;
                            str5 = substring3;
                        }
                        if (!(str3 == null || str5 == null || str4 == null)) {
                            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("geo:" + str4 + "," + str5));
                            intent.addFlags(268435456);
                            activity.startActivity(intent);
                            a.b(activity, aj.a().f(), str3, 3);
                        }
                        return true;
                    }
                }
                str4 = null;
                str5 = null;
                Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse("geo:" + str4 + "," + str5));
                intent2.addFlags(268435456);
                activity.startActivity(intent2);
                try {
                    a.b(activity, aj.a().f(), str3, 3);
                } catch (Exception e2) {
                }
                return true;
            }
        } catch (Exception e3) {
        }
        return false;
    }

    private static boolean d(Activity activity, String str) {
        String str2;
        String str3;
        try {
            if (str.indexOf("youmi://dl/") == 0) {
                String substring = str.substring("youmi://dl/".length());
                int indexOf = substring.indexOf("/");
                if (indexOf > -1) {
                    String substring2 = substring.substring(0, indexOf);
                    try {
                        if (substring.length() > indexOf + 1) {
                            str3 = substring.substring(indexOf + 1);
                            if (str3 != null) {
                                try {
                                    str3 = v.a(str3);
                                    str2 = substring2;
                                } catch (Exception e) {
                                    str2 = substring2;
                                    ad.b(activity, str3);
                                    try {
                                        a.b(activity, aj.a().f(), str2, 3);
                                    } catch (Exception e2) {
                                    }
                                    return true;
                                }
                            } else {
                                str2 = substring2;
                            }
                        } else {
                            str3 = null;
                            str2 = substring2;
                        }
                    } catch (Exception e3) {
                        str3 = null;
                        str2 = substring2;
                        ad.b(activity, str3);
                        a.b(activity, aj.a().f(), str2, 3);
                        return true;
                    }
                } else {
                    str2 = substring;
                    str3 = null;
                }
                if (!(str2 == null || str3 == null)) {
                    ad.b(activity, str3);
                    a.b(activity, aj.a().f(), str2, 3);
                }
                return true;
            }
        } catch (Exception e4) {
        }
        return false;
    }
}
