package net.youmi.android;

import android.graphics.Bitmap;

class ax {
    private int a = 1;
    private int b = 3;
    private String c;
    private int d;
    private boolean e = false;
    private boolean f = false;
    private String g;
    private String h;
    private Bitmap i;
    private au j;
    private z k;
    private Bitmap l;
    private String m;

    ax() {
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        if (i2 == 1 || i2 == 2 || i2 == 3) {
            this.a = i2;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Bitmap bitmap) {
        this.l = bitmap;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.m = str;
    }

    /* access modifiers changed from: package-private */
    public void a(z zVar) {
        this.k = zVar;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:48:0x0082=Splitter:B:48:0x0082, B:60:0x00aa=Splitter:B:60:0x00aa, B:73:0x00da=Splitter:B:73:0x00da} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(android.app.Activity r4, int r5, java.lang.String r6, java.lang.String r7, java.lang.String r8, java.lang.String r9, int r10, int r11) {
        /*
            r3 = this;
            if (r5 > 0) goto L_0x0004
            r0 = 0
        L_0x0003:
            return r0
        L_0x0004:
            if (r6 != 0) goto L_0x0008
            r0 = 0
            goto L_0x0003
        L_0x0008:
            java.lang.String r0 = r6.trim()     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r1 = ""
            boolean r1 = r0.equals(r1)     // Catch:{ Exception -> 0x00e4 }
            if (r1 == 0) goto L_0x0016
            r0 = 0
            goto L_0x0003
        L_0x0016:
            r3.d = r5     // Catch:{ Exception -> 0x00e4 }
            r3.c = r0     // Catch:{ Exception -> 0x00e4 }
            if (r9 != 0) goto L_0x0035
            java.lang.String r0 = ""
        L_0x001e:
            if (r7 != 0) goto L_0x003a
            java.lang.String r1 = ""
        L_0x0022:
            r3.g = r1     // Catch:{ Exception -> 0x00e4 }
            if (r8 != 0) goto L_0x003f
            java.lang.String r2 = ""
        L_0x0028:
            r3.h = r2     // Catch:{ Exception -> 0x00e4 }
            r2 = 1
            if (r11 == r2) goto L_0x0044
            r2 = 3
            if (r11 == r2) goto L_0x0044
            r2 = 2
            if (r11 == r2) goto L_0x0044
            r0 = 0
            goto L_0x0003
        L_0x0035:
            java.lang.String r0 = r9.trim()     // Catch:{ Exception -> 0x00e4 }
            goto L_0x001e
        L_0x003a:
            java.lang.String r1 = r7.trim()     // Catch:{ Exception -> 0x00e4 }
            goto L_0x0022
        L_0x003f:
            java.lang.String r2 = r8.trim()     // Catch:{ Exception -> 0x00e4 }
            goto L_0x0028
        L_0x0044:
            r3.a = r11     // Catch:{ Exception -> 0x00e4 }
            r2 = 4
            if (r10 == r2) goto L_0x0054
            r2 = 3
            if (r10 == r2) goto L_0x0054
            r2 = 2
            if (r10 == r2) goto L_0x0054
            r2 = 1
            if (r10 == r2) goto L_0x0054
            r0 = 0
            goto L_0x0003
        L_0x0054:
            r3.b = r10     // Catch:{ Exception -> 0x00e4 }
            switch(r10) {
                case 1: goto L_0x005b;
                case 2: goto L_0x0065;
                case 3: goto L_0x008c;
                case 4: goto L_0x00b7;
                default: goto L_0x0059;
            }     // Catch:{ Exception -> 0x00e4 }
        L_0x0059:
            r0 = 0
            goto L_0x0003
        L_0x005b:
            java.lang.String r0 = ""
            boolean r0 = r1.equals(r0)     // Catch:{ Exception -> 0x00e4 }
            if (r0 == 0) goto L_0x00b1
            r0 = 0
            goto L_0x0003
        L_0x0065:
            java.lang.String r1 = ""
            boolean r1 = r0.equals(r1)     // Catch:{ Exception -> 0x00e4 }
            if (r1 == 0) goto L_0x006f
            r0 = 0
            goto L_0x0003
        L_0x006f:
            net.youmi.android.cx r1 = new net.youmi.android.cx     // Catch:{ Exception -> 0x00ec }
            r1.<init>(r4)     // Catch:{ Exception -> 0x00ec }
            net.youmi.android.ay r2 = net.youmi.android.af.b     // Catch:{ Exception -> 0x00ec }
            boolean r0 = r1.a(r0, r2)     // Catch:{ Exception -> 0x00ec }
            if (r0 == 0) goto L_0x0089
            android.graphics.Bitmap r0 = r1.b()     // Catch:{ Exception -> 0x00ec }
            r3.i = r0     // Catch:{ Exception -> 0x00ec }
        L_0x0082:
            android.graphics.Bitmap r0 = r3.i     // Catch:{ Exception -> 0x00e4 }
            if (r0 != 0) goto L_0x00b1
            r0 = 0
            goto L_0x0003
        L_0x0089:
            r0 = 0
            goto L_0x0003
        L_0x008c:
            java.lang.String r2 = ""
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x00e4 }
            if (r1 == 0) goto L_0x0097
            r0 = 0
            goto L_0x0003
        L_0x0097:
            net.youmi.android.cx r1 = new net.youmi.android.cx     // Catch:{ Exception -> 0x00ea }
            r1.<init>(r4)     // Catch:{ Exception -> 0x00ea }
            net.youmi.android.ay r2 = net.youmi.android.af.a     // Catch:{ Exception -> 0x00ea }
            boolean r0 = r1.a(r0, r2)     // Catch:{ Exception -> 0x00ea }
            if (r0 == 0) goto L_0x00b4
            android.graphics.Bitmap r0 = r1.b()     // Catch:{ Exception -> 0x00ea }
            r3.i = r0     // Catch:{ Exception -> 0x00ea }
        L_0x00aa:
            android.graphics.Bitmap r0 = r3.i     // Catch:{ Exception -> 0x00e4 }
            if (r0 != 0) goto L_0x00b1
            r0 = 1
            r3.b = r0     // Catch:{ Exception -> 0x00e4 }
        L_0x00b1:
            r0 = 1
            goto L_0x0003
        L_0x00b4:
            r0 = 0
            goto L_0x0003
        L_0x00b7:
            java.lang.String r1 = ""
            boolean r1 = r0.equals(r1)     // Catch:{ Exception -> 0x00e4 }
            if (r1 == 0) goto L_0x00c2
            r0 = 0
            goto L_0x0003
        L_0x00c2:
            net.youmi.android.cx r1 = new net.youmi.android.cx     // Catch:{ Exception -> 0x00e8 }
            r1.<init>(r4)     // Catch:{ Exception -> 0x00e8 }
            net.youmi.android.ay r2 = net.youmi.android.af.d     // Catch:{ Exception -> 0x00e8 }
            boolean r0 = r1.a(r0, r2)     // Catch:{ Exception -> 0x00e8 }
            if (r0 == 0) goto L_0x00e1
            net.youmi.android.au r0 = new net.youmi.android.au     // Catch:{ Exception -> 0x00e8 }
            byte[] r1 = r1.c()     // Catch:{ Exception -> 0x00e8 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x00e8 }
            r3.j = r0     // Catch:{ Exception -> 0x00e8 }
        L_0x00da:
            net.youmi.android.au r0 = r3.j     // Catch:{ Exception -> 0x00e4 }
            if (r0 != 0) goto L_0x00b1
            r0 = 0
            goto L_0x0003
        L_0x00e1:
            r0 = 0
            goto L_0x0003
        L_0x00e4:
            r0 = move-exception
            r0 = 0
            goto L_0x0003
        L_0x00e8:
            r0 = move-exception
            goto L_0x00da
        L_0x00ea:
            r0 = move-exception
            goto L_0x00aa
        L_0x00ec:
            r0 = move-exception
            goto L_0x0082
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.ax.a(android.app.Activity, int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, int):boolean");
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public Bitmap d() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public String e() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public int f() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public String g() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public z h() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public boolean i() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public void j() {
        this.e = true;
    }

    /* access modifiers changed from: package-private */
    public void k() {
        this.f = true;
    }

    /* access modifiers changed from: package-private */
    public au l() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public Bitmap m() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public String n() {
        return this.m;
    }
}
