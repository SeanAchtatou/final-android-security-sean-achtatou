package net.youmi.android;

import android.app.Activity;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

class y extends RelativeLayout implements ac {
    ak a;
    int b = -1;
    Activity c;
    bs d;
    ImageView e;
    final /* synthetic */ bb f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public y(bb bbVar, Activity activity, ak akVar, int i) {
        super(activity);
        this.f = bbVar;
        this.a = akVar;
        this.b = i;
        this.c = activity;
        f();
    }

    private void f() {
        if (this.d == null) {
            this.d = new bs(this.f, this.c, this.b, this.a);
            this.d.setVisibility(0);
        }
        if (this.e == null) {
            this.e = new ImageView(getContext());
        }
        this.d.setId(1003);
        this.e.setId(1004);
        RelativeLayout.LayoutParams b2 = ap.b();
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, this.a.d());
        int f2 = this.a.f();
        layoutParams.addRule(11);
        b2.addRule(9);
        b2.addRule(15);
        b2.setMargins(f2, 0, 30, 0);
        addView(this.e, layoutParams);
        addView(this.d, b2);
    }

    public void a() {
        setVisibility(8);
    }

    public void a(Animation animation) {
        try {
            startAnimation(animation);
        } catch (Exception e2) {
        }
    }

    public boolean a(ax axVar) {
        if (axVar == null) {
            return false;
        }
        try {
            return this.d.a(axVar);
        } catch (Exception e2) {
            return false;
        }
    }

    public void b() {
        removeAllViews();
    }

    public void c() {
        this.d.c();
    }

    public void d() {
        setVisibility(0);
    }

    public void e() {
        this.d.e();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        try {
            new cx(this.c).a(new ae(this), af.a, this.f.n.g());
        } catch (Exception e2) {
        }
    }
}
