package net.youmi.android;

import android.app.Activity;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.TextView;

class bs extends FrameLayout implements ac {
    TextView a;
    TextView b;
    TextView c;
    Animation d;
    Animation e;
    final /* synthetic */ bb f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bs(bb bbVar, Activity activity, int i, ak akVar) {
        super(activity);
        this.f = bbVar;
        this.a = new TextView(activity);
        this.a.setMaxLines(2);
        this.a.setGravity(16);
        this.a.setTextColor(i);
        this.b = new TextView(activity);
        this.b.setMaxLines(2);
        this.b.setGravity(16);
        this.b.setTextColor(i);
        this.a.setVisibility(8);
        this.b.setVisibility(8);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        addView(this.a, layoutParams);
        addView(this.b, layoutParams);
        this.d = r.a(akVar);
        this.e = r.b(akVar);
    }

    public void a() {
        setVisibility(8);
    }

    public void a(Animation animation) {
        try {
            a(animation);
        } catch (Exception e2) {
        }
    }

    public boolean a(ax axVar) {
        try {
            TextView b2 = b();
            if (b2 == null) {
                return false;
            }
            b2.setText(axVar.c());
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public TextView b() {
        return this.a == this.c ? this.b : this.a;
    }

    public void c() {
        TextView b2 = b();
        if (b2 != null) {
            if (this.c != null) {
                this.c.setVisibility(8);
            }
            b2.setVisibility(0);
            this.c = b2;
        }
    }

    public void d() {
        setVisibility(0);
    }

    public void e() {
        TextView b2 = b();
        if (b2 != null) {
            if (this.c != null) {
                this.c.startAnimation(this.e);
                this.c.setVisibility(8);
            }
            b2.setVisibility(0);
            b2.startAnimation(this.d);
            this.c = b2;
        }
    }
}
