package net.youmi.android;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import java.util.Locale;
import java.util.Properties;

class e {
    private static int a = -1;
    private static String b = "yuJtmxbnRzbmWJnK";
    private static String c;
    private static String d;
    private static String e;
    private static String f = "";
    private static String g = "";
    private static String h;
    private static String i;

    e() {
    }

    static String a() {
        Locale locale = Locale.getDefault();
        return String.format("%s-%s", locale.getLanguage(), locale.getCountry());
    }

    static void a(Context context) {
        try {
            i = Settings.Secure.getString(context.getContentResolver(), "android_id");
        } catch (Exception e2) {
        }
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager != null) {
                try {
                    String subscriberId = telephonyManager.getSubscriberId();
                    if (subscriberId != null) {
                        d = subscriberId;
                    }
                } catch (Exception e3) {
                }
                try {
                    String deviceId = telephonyManager.getDeviceId();
                    if (deviceId != null) {
                        c = deviceId;
                        if (c != null) {
                            if (c.indexOf(" ") > -1) {
                                c.replace(" ", "");
                            }
                            if (c.indexOf("-") > -1) {
                                c = c.replace("-", "");
                            }
                            if (c.indexOf("\n") > -1) {
                                c = c.replace("\n", "");
                            }
                            int indexOf = c.indexOf("MEID:");
                            if (indexOf > -1) {
                                c = c.substring("MEID:".length() + indexOf);
                            }
                            c = c.trim();
                        }
                    }
                } catch (Exception e4) {
                }
                try {
                    String networkOperatorName = telephonyManager.getNetworkOperatorName();
                    if (networkOperatorName == null) {
                        e = "";
                    } else {
                        e = networkOperatorName;
                    }
                } catch (Exception e5) {
                }
            }
        } catch (Exception e6) {
        }
        if (c != null || d != null) {
            h = "";
        } else if (i == null) {
            h = "";
        } else {
            h = i;
        }
    }

    static boolean a(int i2) {
        return a == i2;
    }

    static String b() {
        return Build.MODEL;
    }

    static String b(Context context) {
        if (i == null) {
            a(context);
        }
        return i;
    }

    static void b(int i2) {
        a = i2;
    }

    static String c() {
        return "android " + Build.VERSION.RELEASE;
    }

    static String c(Context context) {
        try {
            String b2 = b(context);
            if (b2 != null) {
                String trim = b2.trim();
                if (!trim.equals("")) {
                    return as.a(trim);
                }
            }
            String str = null;
            Properties a2 = aa.a(context, "DD5E8CD46CF94B22BAAD68AB06710752");
            if (a2.containsKey("46C02DF8DF4C4C18A578C63449C7F64D")) {
                str = (String) a2.get("46C02DF8DF4C4C18A578C63449C7F64D");
            }
            if (str == null && (str = as.a(String.valueOf(f()) + b() + ag.a(Integer.MAX_VALUE) + System.currentTimeMillis() + c() + b)) != null) {
                try {
                    a2.put("46C02DF8DF4C4C18A578C63449C7F64D", str);
                    aa.a(context, a2, "DD5E8CD46CF94B22BAAD68AB06710752");
                } catch (Exception e2) {
                }
            }
            return str == null ? as.a(Build.MODEL) : str;
        } catch (Exception e3) {
            return "";
        }
    }

    static String d() {
        return g;
    }

    static String d(Context context) {
        String g2 = g(context);
        String h2 = h(context);
        b(context);
        String f2 = f(context);
        return (!g2.equals("") || !h2.equals("") || !f2.equals("")) ? as.a(String.valueOf(g2) + h2 + f2 + b) : c(context);
    }

    static String e(Context context) {
        if (e == null) {
            a(context);
        }
        return e == null ? "" : e;
    }

    /* access modifiers changed from: private */
    public static String f() {
        return Build.BRAND;
    }

    static String f(Context context) {
        if (h == null) {
            a(context);
        }
        return h;
    }

    static String g(Context context) {
        if (c == null) {
            a(context);
        }
        return c;
    }

    static String h(Context context) {
        if (d == null) {
            a(context);
        }
        return d;
    }

    static boolean i(Context context) {
        if (n.a()) {
            return true;
        }
        return j(context);
    }

    static boolean j(Context context) {
        try {
            String g2 = g(context);
            String b2 = b(context);
            String lowerCase = Build.MODEL.trim().toLowerCase();
            if (g2 != null && g2.equals("000000000000000")) {
                if (b2 == null) {
                    return true;
                }
                if (b2.equals("")) {
                    return true;
                }
            }
            if (lowerCase != null) {
                if (lowerCase.equals("sdk")) {
                    return true;
                }
                if (lowerCase.equals("google_sdk")) {
                    return true;
                }
            }
            return false;
        } catch (Exception e2) {
            return true;
        }
    }
}
