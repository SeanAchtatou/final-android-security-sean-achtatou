package net.youmi.android;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;

class bt implements Runnable {
    final /* synthetic */ cx a;
    private final /* synthetic */ bp b;
    private final /* synthetic */ ay c;
    private final /* synthetic */ String d;

    bt(cx cxVar, bp bpVar, ay ayVar, String str) {
        this.a = cxVar;
        this.b = bpVar;
        this.c = ayVar;
        this.d = str;
    }

    public void run() {
        boolean z;
        try {
            this.a.a(this.b, 0, "连接中");
            this.a.a(this.b, 10);
            InputStream inputStream = null;
            try {
                if (this.c != null) {
                    inputStream = this.c.c(this.d);
                }
            } catch (Exception e) {
            }
            if (inputStream != null) {
                this.a.a(this.b, 50);
                z = true;
            } else {
                this.a.a(this.b, 10);
                HttpURLConnection a2 = cg.a(this.a.b, this.d);
                if (a2 == null) {
                    g.b("Unable to connect to the server, please check your network configuration.");
                    this.a.a(this.b, 1, this.d);
                    return;
                }
                try {
                    a2.setRequestMethod("GET");
                    a2.setDoInput(true);
                    cg.a(a2);
                    a2.connect();
                    this.a.a(a2);
                    inputStream = a2.getInputStream();
                    this.a.a(this.b, 50);
                    z = false;
                } catch (Exception e2) {
                    g.b("Unable to connect to the server, please check your network configuration..");
                    this.a.a(this.b, 1, this.d);
                    return;
                }
            }
            if (inputStream != null) {
                int available = inputStream.available();
                if (available > 0) {
                    byte[] bArr = new byte[512];
                    int length = available / bArr.length;
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    while (true) {
                        int read = inputStream.read(bArr, 0, bArr.length);
                        if (read <= 0) {
                            break;
                        }
                        byteArrayOutputStream.write(bArr, 0, read);
                        this.a.a(this.b, length);
                    }
                    this.a.c = byteArrayOutputStream.toByteArray();
                    try {
                        inputStream.close();
                    } catch (Exception e3) {
                    }
                    try {
                        byteArrayOutputStream.close();
                    } catch (Exception e4) {
                    }
                    if (this.c != null && !z) {
                        this.c.a(this.d, this.a.c);
                    }
                    this.a.a(this.b);
                    return;
                }
                this.a.a(this.b, 1, this.d);
                return;
            }
            this.a.a(this.b, 1, this.d);
        } catch (Exception e5) {
            this.a.a(this.b, 1, e5.getMessage());
        }
    }
}
