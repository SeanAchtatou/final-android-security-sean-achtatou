package net.youmi.android;

import java.util.ArrayList;

class ai {
    private StringBuilder a = new StringBuilder(2048);
    private String b = "";
    private String c;
    private String d = "utf-8";

    ai() {
    }

    static String a(int i) {
        StringBuilder sb = new StringBuilder((i * 6) + 16);
        for (int i2 = 0; i2 < i; i2++) {
            sb.append("&nbsp;");
        }
        return sb.toString();
    }

    static String a(String str, String str2) {
        return String.valueOf(str) + a(1) + b("点此下载", str2);
    }

    static String a(av avVar) {
        if (avVar == null) {
            return "";
        }
        if (avVar.a() == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder(256);
        int c2 = avVar.c();
        if (c2 == 0) {
            return avVar.a();
        }
        sb.append(avVar.a());
        sb.append(a(1));
        if (c2 == 1 || c2 == 3) {
            sb.append(b("电话", "youmi://tel/" + avVar.a()));
        }
        sb.append(a(3));
        if (c2 == 2 || c2 == 3) {
            String str = "youmi://sms/" + avVar.a();
            if (avVar.b() != null) {
                str = String.valueOf(String.valueOf(str) + "/") + avVar.b();
            }
            sb.append(b("短信", str));
        }
        return sb.toString();
    }

    static String a(be beVar) {
        if (beVar == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder(256);
        String d2 = beVar.d();
        sb.append("youmi://geo/");
        sb.append(beVar.a());
        sb.append("/");
        sb.append(beVar.c());
        sb.append("/");
        sb.append(beVar.b());
        return b(d2, sb.toString());
    }

    static String a(cj cjVar) {
        if (cjVar == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder(256);
        sb.append(cjVar.d());
        sb.append("[");
        sb.append(cjVar.c());
        sb.append("]");
        String sb2 = sb.toString();
        sb.delete(0, sb.length());
        sb.append("youmi://dl/");
        sb.append(cjVar.a());
        sb.append("/");
        sb.append(as.b(cjVar.b()));
        return a(sb2, sb.toString());
    }

    static String a(co coVar) {
        if (coVar == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder(256);
        sb.append("youmi://wap/");
        sb.append(coVar.c());
        sb.append("/");
        sb.append(as.b(coVar.a()));
        return b(coVar.b(), sb.toString());
    }

    static String b(String str, String str2) {
        StringBuilder sb = new StringBuilder(256);
        sb.append("<a href=\"");
        sb.append(str2);
        sb.append("\">");
        sb.append(str);
        sb.append("</a>");
        return sb.toString();
    }

    static String c(String str) {
        return "<img src=\"" + str + "\"/>";
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.a.append("<br/>");
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.a.append("<p>");
        this.a.append(str);
        this.a.append("</p>");
    }

    /* access modifiers changed from: package-private */
    public void a(String str, al alVar) {
        this.a.append("<p ");
        this.a.append(alVar.a());
        this.a.append(">");
        this.a.append(str);
        this.a.append("</p>");
    }

    /* access modifiers changed from: package-private */
    public void a(ArrayList arrayList, al alVar) {
        if (arrayList != null && arrayList.size() > 0) {
            a("【联系地址】", alVar);
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < arrayList.size()) {
                    a(a((be) arrayList.get(i2)));
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ArrayList arrayList, aw awVar, al alVar) {
        boolean z;
        if (arrayList != null || awVar != null) {
            StringBuilder sb = new StringBuilder(256);
            if (arrayList == null || arrayList.size() <= 0) {
                z = false;
            } else {
                for (int i = 0; i < arrayList.size(); i++) {
                    sb.append("<p>").append(a((av) arrayList.get(i))).append("</p>");
                }
                z = true;
            }
            if (awVar != null) {
                if (awVar.d() != null) {
                    sb.append("<p>").append("传真:").append(a(1)).append(awVar.d()).append("</p>");
                    z = true;
                }
                if (awVar.c() != null) {
                    sb.append("<p>").append("Email:").append(a(1)).append(awVar.c()).append("</p>");
                    z = true;
                }
                if (awVar.a() != null) {
                    sb.append("<p>").append("MSN:").append(a(1)).append(awVar.a()).append("</p>");
                    z = true;
                }
                if (awVar.b() != null) {
                    sb.append("<p>").append("QQ:").append(a(1)).append(awVar.b()).append("</p>");
                    z = true;
                }
            }
            if (z) {
                a("【联系方式】", alVar);
            }
            a(sb.toString());
        }
    }

    /* access modifiers changed from: package-private */
    public String b() {
        StringBuilder sb = new StringBuilder(3072);
        sb.append("<html>");
        sb.append("<head>");
        sb.append("<title>");
        sb.append(this.b);
        sb.append("</title>");
        sb.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />");
        sb.append("<meta http-equiv=\"Content-Language\" content=\"zh-cn\" />");
        sb.append("</head>");
        sb.append("<body>");
        if (this.c != null) {
            sb.append("<h2>");
            sb.append(this.c);
            sb.append("</h2>");
        }
        sb.append(this.a.toString());
        sb.append("</body>");
        sb.append("</html>");
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        this.b = str;
    }

    /* access modifiers changed from: package-private */
    public void b(ArrayList arrayList, al alVar) {
        if (arrayList != null && arrayList.size() > 0) {
            a("【下载地址】", alVar);
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < arrayList.size()) {
                    a(a((cj) arrayList.get(i2)));
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void c(ArrayList arrayList, al alVar) {
        if (arrayList != null && arrayList.size() > 0) {
            a("【关于】", alVar);
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < arrayList.size()) {
                    a(a((co) arrayList.get(i2)));
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void d(String str) {
        this.c = str;
    }
}
