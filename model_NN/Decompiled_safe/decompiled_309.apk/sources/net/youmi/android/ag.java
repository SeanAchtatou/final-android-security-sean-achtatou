package net.youmi.android;

import com.guohead.sdk.util.GuoheAdUtil;
import java.util.Random;

class ag {
    private static final Random a = new Random(System.currentTimeMillis());

    ag() {
    }

    static final int a(int i) {
        return a.nextInt(i);
    }

    static final void a(StringBuilder sb) {
        a(sb, a(16));
    }

    static final void a(StringBuilder sb, int i) {
        switch (i) {
            case GuoheAdUtil.NETWORK_TYPE_GUOHEAD:
                sb.append('a');
                return;
            case GuoheAdUtil.NETWORK_TYPE_MOBCLIX:
                sb.append('b');
                return;
            case GuoheAdUtil.NETWORK_TYPE_MDOTM:
                sb.append('c');
                return;
            case GuoheAdUtil.NETWORK_TYPE_4THSCREEN:
                sb.append('d');
                return;
            case GuoheAdUtil.NETWORK_TYPE_ADSENSE:
                sb.append('e');
                return;
            case GuoheAdUtil.NETWORK_TYPE_DOUBLECLICK:
                sb.append('f');
                return;
            default:
                sb.append(i);
                return;
        }
    }
}
