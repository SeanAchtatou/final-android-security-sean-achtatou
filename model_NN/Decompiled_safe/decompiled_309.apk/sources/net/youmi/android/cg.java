package net.youmi.android;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import org.json.JSONObject;

class cg {
    private static String a;

    cg() {
    }

    static int a(Context context) {
        return a(b(context));
    }

    static int a(String str) {
        if (str == null) {
            return -1;
        }
        String trim = str.toLowerCase().trim();
        if (trim.length() == 0) {
            return -1;
        }
        if (trim.equals("wifi")) {
            return 0;
        }
        if (trim.equals("cmnet")) {
            return 1;
        }
        if (trim.equals("cmwap")) {
            return 2;
        }
        if (trim.equals("uninet")) {
            return 3;
        }
        if (trim.equals("uniwap")) {
            return 4;
        }
        if (trim.equals("3gnet")) {
            return 5;
        }
        if (trim.equals("3gwap")) {
            return 6;
        }
        if (trim.equals("ctnet")) {
            return 7;
        }
        if (trim.equals("ctwap")) {
            return 8;
        }
        return trim.equals("internet") ? 9 : -1;
    }

    static HttpURLConnection a(Context context, String str) {
        URL url;
        if (context.checkCallingOrSelfPermission("android.permission.INTERNET") == -1) {
            g.b("Cannot request an ad without Internet permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.INTERNET\" />");
            return null;
        }
        try {
            url = new URL(str);
        } catch (Exception e) {
            url = null;
        }
        if (url == null) {
            return null;
        }
        return a(url, a(context));
    }

    private static HttpURLConnection a(URL url, int i) {
        if (url == null) {
            return null;
        }
        if (i == 2) {
            try {
                return (HttpURLConnection) url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.172", 80)));
            } catch (Exception e) {
            }
        }
        if (i == 8) {
            try {
                return (HttpURLConnection) url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.200", 80)));
            } catch (Exception e2) {
            }
        }
        try {
            return (HttpURLConnection) url.openConnection();
        } catch (Exception e3) {
            return null;
        }
    }

    static void a(HttpURLConnection httpURLConnection) {
        if (a == null) {
            a = "Mozilla/5.0(Linux;U;" + e.c() + ";" + e.a() + ";" + e.b() + ") AppleWebkit/533.1(KHTML,like Gecko) Version/4.0 Mobile Safari/533.1";
        }
        httpURLConnection.setRequestProperty("User-Agent", a);
    }

    static InputStream b(Context context, String str) {
        HttpURLConnection a2 = a(context, str);
        if (a2 == null) {
            return null;
        }
        try {
            a2.setRequestMethod("GET");
            a2.setDoInput(true);
            a(a2);
            a2.connect();
            return a2.getInputStream();
        } catch (Exception e) {
            return null;
        }
    }

    static String b(Context context) {
        try {
            if (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == -1) {
                return "";
            }
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null) {
                g.b("can not connect to network,please check the network configuration");
                return "";
            } else if (!activeNetworkInfo.isAvailable()) {
                g.b("can not connect to network,please check the network configuration");
                return "";
            } else if (activeNetworkInfo.getType() != 0) {
                return "wifi";
            } else {
                String extraInfo = activeNetworkInfo.getExtraInfo();
                return extraInfo != null ? extraInfo.trim().toLowerCase() : "";
            }
        } catch (Exception e) {
            return "";
        }
    }

    static String c(Context context, String str) {
        try {
            InputStream b = b(context, str);
            if (b == null) {
                return null;
            }
            byte[] bArr = new byte[512];
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            while (true) {
                int read = b.read(bArr);
                if (read <= 0) {
                    try {
                        break;
                    } catch (Exception e) {
                    }
                } else {
                    byteArrayOutputStream.write(bArr, 0, read);
                }
            }
            b.close();
            String byteArrayOutputStream2 = byteArrayOutputStream.toString("utf-8");
            try {
                byteArrayOutputStream.close();
                return byteArrayOutputStream2;
            } catch (Exception e2) {
                return byteArrayOutputStream2;
            }
        } catch (Exception e3) {
            return null;
        }
    }

    static JSONObject d(Context context, String str) {
        try {
            String c = c(context, str);
            if (c == null) {
                return null;
            }
            return new JSONObject(c);
        } catch (Exception e) {
            return null;
        }
    }
}
