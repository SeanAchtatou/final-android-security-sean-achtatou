package net.youmi.android;

import android.graphics.Bitmap;

class bf implements p {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ ct a;

    bf(ct ctVar) {
        this.a = ctVar;
    }

    public void a(cu cuVar) {
        try {
            if (this.a.a != null) {
                Bitmap b = this.a.c;
                this.a.c = this.a.a.e();
                if (this.a.c != null) {
                    this.a.getHandler().post(new ab(this));
                    if (b != null) {
                        try {
                            if (!b.isRecycled()) {
                                b.recycle();
                            }
                        } catch (Exception e) {
                        }
                    }
                }
            } else {
                cuVar.a();
            }
        } catch (Exception e2) {
        }
    }

    public long b(cu cuVar) {
        try {
            if (this.a.a != null) {
                return (long) this.a.a.f();
            }
            cuVar.a();
            return 100;
        } catch (Exception e) {
        }
    }

    public void c(cu cuVar) {
    }
}
