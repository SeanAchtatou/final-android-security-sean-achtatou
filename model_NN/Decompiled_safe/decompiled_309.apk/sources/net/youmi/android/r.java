package net.youmi.android;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;

class r {
    private static Animation a;
    private static Animation b;
    private static Animation c;
    private static Animation d;

    r() {
    }

    private static Animation a(int i) {
        AnimationSet animationSet = new AnimationSet(true);
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) (-i), 0.0f);
        translateAnimation.setDuration(300);
        animationSet.addAnimation(translateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(300);
        animationSet.addAnimation(alphaAnimation);
        return animationSet;
    }

    static Animation a(ak akVar) {
        if (c == null) {
            c = a(akVar.d());
        }
        return c;
    }

    private static Animation b(int i) {
        AnimationSet animationSet = new AnimationSet(true);
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) i);
        translateAnimation.setDuration(300);
        animationSet.addAnimation(translateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(300);
        animationSet.addAnimation(alphaAnimation);
        return animationSet;
    }

    static Animation b(ak akVar) {
        if (d == null) {
            d = b(akVar.d());
        }
        return d;
    }

    private static Animation c(int i) {
        AnimationSet animationSet = new AnimationSet(true);
        RotateAnimation rotateAnimation = new RotateAnimation(0.0f, 360.0f, (float) (i / 2), (float) (i / 2));
        rotateAnimation.setDuration(300);
        animationSet.addAnimation(rotateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(300);
        animationSet.addAnimation(alphaAnimation);
        return animationSet;
    }

    static Animation c(ak akVar) {
        if (a == null) {
            a = c(akVar.e());
        }
        return a;
    }

    private static Animation d(int i) {
        AnimationSet animationSet = new AnimationSet(true);
        RotateAnimation rotateAnimation = new RotateAnimation(0.0f, -360.0f, (float) (i / 2), (float) (i / 2));
        rotateAnimation.setDuration(300);
        animationSet.addAnimation(rotateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(300);
        animationSet.addAnimation(alphaAnimation);
        return animationSet;
    }

    static Animation d(ak akVar) {
        if (b == null) {
            b = d(akVar.e());
        }
        return b;
    }
}
