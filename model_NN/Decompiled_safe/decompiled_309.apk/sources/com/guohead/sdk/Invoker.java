package com.guohead.sdk;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.yb.sim.R;

public class Invoker extends Activity {
    LinearLayout layout;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        RelativeLayout.LayoutParams GuoheAdLayoutParams = new RelativeLayout.LayoutParams(-1, -2);
        GuoheAdLayout adLayout = new GuoheAdLayout(this);
        adLayout.setListener(new GuoheAdStateListener() {
            public void onFail() {
            }

            public void onClick() {
            }

            public void onReceiveAd() {
            }

            public void onRefreshAd() {
            }
        });
        ((LinearLayout) findViewById(R.id.COLUMN_1)).addView(adLayout, GuoheAdLayoutParams);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        GuoheAdManager.finish(this);
    }
}
