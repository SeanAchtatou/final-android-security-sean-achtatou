package com.guohead.sdk.adapters;

import android.graphics.Color;
import cn.domob.android.ads.DomobAdListener;
import cn.domob.android.ads.DomobAdManager;
import cn.domob.android.ads.DomobAdView;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Extra;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.Logger;

public class DomobAdapter extends GuoheAdAdapter implements DomobAdListener {
    static int counter = 0;
    /* access modifiers changed from: private */
    public DomobAdView adView;

    public DomobAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    public void handle() {
        try {
            DomobAdManager.setPublisherId(this.ration.key);
            this.adView = new DomobAdView(this.guoheAdLayout.activity);
            this.adView.setAdListener(this);
            Extra extra = this.guoheAdLayout.extra;
            int bgColor = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
            int fgColor = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
            this.adView.setRequestInterval(900);
            this.adView.setBackgroundColor(bgColor);
            this.adView.setPrimaryTextColor(fgColor);
            this.adView.setKeywords(this.ration.key2);
            boolean debug = false;
            try {
                debug = Boolean.parseBoolean(this.ration.key2);
            } catch (Exception e) {
            }
            DomobAdManager.setIsTestMode(debug);
            this.adView.requestFreshAd();
        } catch (IllegalArgumentException e2) {
            this.guoheAdLayout.rollover();
        }
    }

    public void finish() {
        Logger.i(getClass().getSimpleName() + "==> finish ");
        Logger.addStatus("Domob finish");
    }

    public void onFailedToReceiveFreshAd(DomobAdView arg0) {
        Logger.d("==========> onFailedToReceiveAd");
        notifyOnFail();
        this.adView.setAdListener((DomobAdListener) null);
        clearAdStateListener();
        this.guoheAdLayout.activity.runOnUiThread(new Runnable() {
            public void run() {
                DomobAdapter.this.guoheAdLayout.removeView(DomobAdapter.this.adView);
                DomobAdapter.this.guoheAdLayout.rollover();
            }
        });
        Logger.addStatus("Domob receive ad failed----");
    }

    public void onReceivedFreshAd(DomobAdView arg0) {
        Logger.d("========> Domob success");
        this.adView.setAdListener((DomobAdListener) null);
        clearAdStateListener();
        this.guoheAdLayout.activity.runOnUiThread(new Runnable() {
            public void run() {
                DomobAdapter.this.guoheAdLayout.removeView(DomobAdapter.this.adView);
                DomobAdapter.this.adView.setVisibility(0);
                DomobAdapter.this.guoheAdLayout.guoheAdManager.resetRollover();
                DomobAdapter.this.guoheAdLayout.nextView = DomobAdapter.this.adView;
                DomobAdapter.this.guoheAdLayout.handler.post(DomobAdapter.this.guoheAdLayout.viewRunnable);
                DomobAdapter.this.guoheAdLayout.rotateThreadedDelayed();
            }
        });
        Logger.addStatus("Domob receive ad suc++++");
    }

    public void refreshAd() {
        this.guoheAdLayout.handler.post(this.guoheAdLayout.adRunnable);
        Logger.addStatus("Domob refresh");
    }
}
