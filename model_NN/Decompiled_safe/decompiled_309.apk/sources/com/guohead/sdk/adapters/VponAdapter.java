package com.guohead.sdk.adapters;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.GuoheAdUtil;
import com.guohead.sdk.util.Logger;
import com.vpon.adon.android.AdListener;
import com.vpon.adon.android.AdOnPlatform;
import com.vpon.adon.android.AdView;

public class VponAdapter extends GuoheAdAdapter implements AdListener {
    private AdView adView;

    public VponAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [com.vpon.adon.android.AdView, android.widget.RelativeLayout$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public void handle() {
        try {
            this.adView = new AdView(this.guoheAdLayout.activity);
            this.adView.setLicenseKey(this.ration.key, AdOnPlatform.CN, true);
            this.adView.setAdListener(this);
            this.guoheAdLayout.addView((View) this.adView, (ViewGroup.LayoutParams) new RelativeLayout.LayoutParams(-2, -2));
        } catch (IllegalArgumentException e) {
            this.guoheAdLayout.rollover();
        }
    }

    public void onFailedToRecevieAd(AdView arg0) {
        Log.i(GuoheAdUtil.GUOHEAD, "Vpon: Failed to receive ad");
        AdView adView2 = arg0;
        adView2.setAdListener((AdListener) null);
        this.guoheAdLayout.removeView(adView2);
        this.guoheAdLayout.rolloverThreaded();
        Logger.addStatus("vpon receive ad failed----");
    }

    public void onRecevieAd(AdView arg0) {
        Log.i(GuoheAdUtil.GUOHEAD, "Vpon successful++++++++++++++");
        AdView adView2 = arg0;
        adView2.setAdListener((AdListener) null);
        this.guoheAdLayout.removeView(adView2);
        adView2.setVisibility(0);
        this.guoheAdLayout.guoheAdManager.resetRollover();
        this.guoheAdLayout.nextView = adView2;
        this.guoheAdLayout.handler.post(this.guoheAdLayout.viewRunnable);
        this.guoheAdLayout.rotateThreadedDelayed();
        Logger.addStatus("vpon receive ad suc++++");
    }

    public void finish() {
        Logger.addStatus("vpon finish");
    }

    public void refreshAd() {
        this.guoheAdLayout.handler.post(this.guoheAdLayout.adRunnable);
        Logger.addStatus("refresh");
    }
}
