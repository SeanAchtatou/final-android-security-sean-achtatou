package com.guohead.sdk.adapters;

import android.graphics.Color;
import android.view.ViewGroup;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Extra;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.Logger;
import net.youmi.android.AdListener;
import net.youmi.android.AdManager;
import net.youmi.android.AdView;

public class YouMiAdapter extends GuoheAdAdapter implements AdListener {
    static int counter = 0;
    /* access modifiers changed from: private */
    public AdView adView;

    public YouMiAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    public void handle() {
        counter++;
        Extra extra = this.guoheAdLayout.extra;
        int bgColor = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
        int fgColor = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
        Logger.i(bgColor + ", " + fgColor);
        AdManager.init(this.ration.key, this.ration.key2, AdView.DEFAULT_BACKGROUND_TRANS, false, 1.0d);
        this.adView = new AdView(this.guoheAdLayout.activity, bgColor, fgColor, extra.bgAlpha);
        this.adView.setAdListener(this);
        this.guoheAdLayout.addView(this.adView, new ViewGroup.LayoutParams(-2, -2));
    }

    public void onConnectFailed(AdView arg0) {
        Logger.d("==========> YouMi failure");
        this.adView.setAdListener(null);
        this.guoheAdLayout.activity.runOnUiThread(new Runnable() {
            public void run() {
                YouMiAdapter.this.guoheAdLayout.removeView(YouMiAdapter.this.adView);
                YouMiAdapter.this.guoheAdLayout.rollover();
            }
        });
        Logger.addStatus("youmi receive ad failed----");
    }

    public void onReceiveAd(AdView arg0) {
        Logger.d("========> YouMi success");
        this.adView.setAdListener(null);
        this.guoheAdLayout.activity.runOnUiThread(new Runnable() {
            public void run() {
                YouMiAdapter.this.guoheAdLayout.guoheAdManager.resetRollover();
                YouMiAdapter.this.guoheAdLayout.nextView = YouMiAdapter.this.adView;
                YouMiAdapter.this.guoheAdLayout.activeRation = YouMiAdapter.this.ration;
                YouMiAdapter.this.guoheAdLayout.countImpressionThreaded();
                YouMiAdapter.this.guoheAdLayout.rotateThreadedDelayed();
            }
        });
        Logger.addStatus("youmi receive ad suc++++");
    }

    public void finish() {
        Logger.i(getClass().getSimpleName() + "==> finish ");
        Logger.addStatus("youmi finish");
    }

    public void refreshAd() {
        this.guoheAdLayout.handler.post(this.guoheAdLayout.adRunnable);
        Logger.addStatus("youmi refresh ");
    }
}
