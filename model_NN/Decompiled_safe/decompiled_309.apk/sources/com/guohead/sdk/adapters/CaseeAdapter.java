package com.guohead.sdk.adapters;

import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import com.casee.adsdk.CaseeAdView;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Extra;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.Logger;

public class CaseeAdapter extends GuoheAdAdapter implements CaseeAdView.AdListener {
    public CaseeAdView cav;
    public int refreshTime;

    public CaseeAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [com.casee.adsdk.CaseeAdView, android.view.ViewGroup$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public void handle() {
        Extra extra = this.guoheAdLayout.extra;
        this.cav = new CaseeAdView(this.guoheAdLayout.activity, this.ration.key, false, 800000, Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue), Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue), false);
        this.cav.setListener(this);
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(-1, -2);
        this.cav.setListener(this);
        this.guoheAdLayout.addView((View) this.cav, params);
    }

    public void onFailedToReceiveAd(final CaseeAdView adView) {
        Logger.d("=======> CaseeAd failure");
        adView.setListener((CaseeAdView.AdListener) null);
        this.guoheAdLayout.activity.runOnUiThread(new Runnable() {
            public void run() {
                CaseeAdapter.this.guoheAdLayout.removeView(adView);
                CaseeAdapter.this.guoheAdLayout.rollover();
            }
        });
        Logger.addStatus("casee receive ad failed----");
    }

    public void onFailedToReceiveRefreshAd(CaseeAdView arg0) {
    }

    public void onReceiveAd(final CaseeAdView adView) {
        Logger.out("CASEE onReceiveAd");
        Logger.d("======> CaseeAd success");
        this.guoheAdLayout.activity.runOnUiThread(new Runnable() {
            public void run() {
                CaseeAdapter.this.guoheAdLayout.removeView(adView);
                adView.setVisibility(0);
                CaseeAdapter.this.guoheAdLayout.guoheAdManager.resetRollover();
                CaseeAdapter.this.guoheAdLayout.nextView = adView;
                CaseeAdapter.this.guoheAdLayout.handler.post(CaseeAdapter.this.guoheAdLayout.viewRunnable);
                CaseeAdapter.this.guoheAdLayout.rotateThreadedDelayed();
            }
        });
        Logger.addStatus("casee receive ad suc++++");
    }

    public void onReceiveRefreshAd(CaseeAdView arg0) {
        Logger.out("CASEE onReceiveRefreshAd!");
    }

    public void finish() {
        Logger.i(getClass().getSimpleName() + "==> finish ");
        if (this.cav != null) {
            this.cav.onUnshown();
        }
        Logger.addStatus("casee finish");
    }

    public void refreshAd() {
        this.guoheAdLayout.handler.post(this.guoheAdLayout.adRunnable);
        Logger.addStatus("casee refresh");
    }
}
