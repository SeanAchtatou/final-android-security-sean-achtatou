package com.guohead.sdk.adapters;

import android.view.View;
import android.view.ViewGroup;
import com.adwo.adsdk.AdListener;
import com.adwo.adsdk.AdwoAdView;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.GuoheAdManager;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.Logger;

public class AdWoAdapter extends GuoheAdAdapter implements AdListener {
    /* access modifiers changed from: private */
    public AdwoAdView adView;
    public GuoheAdManager guoheAdManager;

    public AdWoAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [com.adwo.adsdk.AdwoAdView, android.view.ViewGroup$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public void handle() {
        this.adView = new AdwoAdView(this.guoheAdLayout.activity, this.ration.key, 4194432, 16711680, Boolean.parseBoolean(this.ration.key2), 120);
        this.adView.setListener(this);
        this.guoheAdLayout.addView((View) this.adView, new ViewGroup.LayoutParams(-2, -2));
    }

    public void finish() {
        if (this.adView != null) {
            this.adView.finalize();
        }
        Logger.i(getClass().getSimpleName() + "==> finish ");
        Logger.addStatus("adwo finish");
    }

    public void refreshAd() {
        this.guoheAdLayout.handler.post(this.guoheAdLayout.viewRunnable);
        Logger.addStatus("adwo refresh");
    }

    public void onFailedToReceiveAd(AdwoAdView arg0) {
        Logger.d("==========> onFailedToReceiveAd");
        notifyOnFail();
        this.adView.setListener((AdListener) null);
        clearAdStateListener();
        this.guoheAdLayout.activity.runOnUiThread(new Runnable() {
            public void run() {
                AdWoAdapter.this.guoheAdLayout.removeView(AdWoAdapter.this.adView);
                AdWoAdapter.this.guoheAdLayout.rollover();
            }
        });
        Logger.addStatus("adwo receive ad failed----");
    }

    public void onFailedToReceiveRefreshedAd(AdwoAdView arg0) {
    }

    public void onReceiveAd(AdwoAdView arg0) {
        Logger.d("========> Adwo success");
        this.adView.setListener((AdListener) null);
        clearAdStateListener();
        this.guoheAdLayout.activity.runOnUiThread(new Runnable() {
            public void run() {
                AdWoAdapter.this.guoheAdLayout.removeView(AdWoAdapter.this.adView);
                AdWoAdapter.this.adView.setVisibility(0);
                AdWoAdapter.this.guoheAdLayout.guoheAdManager.resetRollover();
                AdWoAdapter.this.guoheAdLayout.nextView = AdWoAdapter.this.adView;
                AdWoAdapter.this.guoheAdLayout.handler.post(AdWoAdapter.this.guoheAdLayout.viewRunnable);
                AdWoAdapter.this.guoheAdLayout.rotateThreadedDelayed();
            }
        });
        Logger.addStatus("adwo receive ad suc++++");
    }
}
