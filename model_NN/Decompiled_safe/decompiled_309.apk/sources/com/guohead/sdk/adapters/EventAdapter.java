package com.guohead.sdk.adapters;

import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.Logger;

public class EventAdapter extends GuoheAdAdapter {
    public EventAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    public void handle() {
        Logger.d("Event notification request initiated");
        if (this.guoheAdLayout.guoheAdInterface != null) {
            String key = this.ration.key;
            if (key == null) {
                Logger.w("Event key is null");
            }
            int methodIndex = key.indexOf("|;|");
            if (methodIndex < 0) {
                Logger.w("Event key separator not found");
            }
            try {
                this.guoheAdLayout.guoheAdInterface.getClass().getMethod(key.substring(methodIndex + 3), null).invoke(this.guoheAdLayout.guoheAdInterface, null);
            } catch (Exception e) {
                Logger.e("Caught exception in handle() at EventAdapter" + e);
            }
        } else {
            Logger.w("Event notification would be sent, but no interface is listening");
        }
        this.guoheAdLayout.guoheAdManager.resetRollover();
        this.guoheAdLayout.rotateThreadedDelayed();
    }

    public void finish() {
    }

    public void refreshAd() {
        this.guoheAdLayout.rotateThreadedNow();
    }
}
