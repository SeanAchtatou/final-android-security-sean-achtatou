package com.guohead.sdk.adapters;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.GuoheAdManager;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.Logger;
import com.millennialmedia.android.MMAdView;
import java.util.Hashtable;

public class MillennialAdapter extends GuoheAdAdapter implements MMAdView.MMAdListener {
    public MMAdView adView;
    public GuoheAdManager guoheAdManager;

    public MillennialAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [com.millennialmedia.android.MMAdView, android.view.ViewGroup$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public void handle() {
        Hashtable<String, String> map = new Hashtable<>();
        if (!TextUtils.isEmpty(this.ration.key2)) {
            map.put("height", "53");
            map.put("width", "320");
            map.put("keywords", this.ration.key2);
        }
        this.adView = new MMAdView(this.guoheAdLayout.activity, this.ration.key, "MMBannerAdTop", 900, map);
        this.adView.setListener(this);
        this.adView.setId(1897808289);
        this.adView.callForAd();
        this.adView.setHorizontalScrollBarEnabled(false);
        this.adView.setVerticalScrollBarEnabled(false);
        this.guoheAdLayout.addView((View) this.adView, new ViewGroup.LayoutParams(-2, -2));
    }

    public void MMAdReturned(final MMAdView adView2) {
        Logger.addStatus("Millennial receive ad suc++++");
        Logger.d("Millennial success");
        adView2.setListener((MMAdView.MMAdListener) null);
        this.guoheAdLayout.activity.runOnUiThread(new Runnable() {
            public void run() {
                MillennialAdapter.this.guoheAdLayout.guoheAdManager.resetRollover();
                MillennialAdapter.this.guoheAdLayout.nextView = adView2;
                MillennialAdapter.this.guoheAdLayout.activeRation = MillennialAdapter.this.ration;
                MillennialAdapter.this.guoheAdLayout.handler.post(MillennialAdapter.this.guoheAdLayout.viewRunnable);
                MillennialAdapter.this.guoheAdLayout.rotateThreadedDelayed();
            }
        });
    }

    public void MMAdFailed(MMAdView adView2) {
        Logger.d("Millennial failure");
        adView2.setListener((MMAdView.MMAdListener) null);
        this.guoheAdLayout.removeView(adView2);
        this.guoheAdLayout.rollover();
        Logger.addStatus("Millennial receive ad failed----");
    }

    public void MMAdClickedToNewBrowser(MMAdView adview) {
        Logger.d("Millennial Ad clicked, new browser launched");
    }

    public void MMAdClickedToOverlay(MMAdView adview) {
        Logger.d("Millennial Ad Clicked to overlay");
    }

    public void MMAdOverlayLaunched(MMAdView adview) {
        Logger.d("Millennial Ad Overlay Launched");
    }

    public void finish() {
        this.adView.halt();
        Logger.addStatus("Millennial finish");
    }

    public void refreshAd() {
        this.guoheAdLayout.handler.post(this.guoheAdLayout.viewRunnable);
        Logger.addStatus("Millennial refresh");
    }

    public void MMAdRequestIsCaching(MMAdView arg0) {
    }
}
