package com.guohead.sdk.adapters;

import android.content.Context;
import android.util.Log;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.GuoheAdStateListener;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.GuoheAdUtil;

public abstract class GuoheAdAdapter {
    static GuoheAdAdapter adapter;
    protected final GuoheAdLayout guoheAdLayout;
    private GuoheAdStateListener guoheAdStateListener;
    protected Ration ration;

    public abstract void finish();

    public abstract void handle();

    public abstract void refreshAd();

    public GuoheAdAdapter(GuoheAdLayout guoheAdLayout2, Ration ration2) {
        this.guoheAdLayout = guoheAdLayout2;
        this.guoheAdStateListener = guoheAdLayout2;
        this.ration = ration2;
    }

    private static GuoheAdAdapter getAdapter(GuoheAdLayout guoheAdLayout2, Ration ration2) {
        try {
            switch (ration2.type) {
                case 1:
                    if (Class.forName("com.google.ads.AdView") != null) {
                        return getNetworkAdapter("com.guohead.sdk.adapters.AdMobAdapter", guoheAdLayout2, ration2);
                    }
                    return unknownAdNetwork(guoheAdLayout2, ration2);
                case 6:
                    if (Class.forName("com.millennialmedia.android.MMAdView") != null) {
                        return getNetworkAdapter("com.guohead.sdk.adapters.MillennialAdapter", guoheAdLayout2, ration2);
                    }
                    return unknownAdNetwork(guoheAdLayout2, ration2);
                case GuoheAdUtil.NETWORK_TYPE_QUATTRO /*8*/:
                    if (Class.forName("com.qwapi.adclient.android.view.QWAdView") != null) {
                        return getNetworkAdapter("com.guohead.sdk.adapters.QuattroAdapter", guoheAdLayout2, ration2);
                    }
                    return unknownAdNetwork(guoheAdLayout2, ration2);
                case GuoheAdUtil.NETWORK_TYPE_CUSTOM /*9*/:
                    return new CustomAdapter(guoheAdLayout2, ration2);
                case GuoheAdUtil.NETWORK_TYPE_VPON /*90*/:
                    if (Class.forName("com.vpon.adon.android.AdView") != null) {
                        return getNetworkAdapter("com.guohead.sdk.adapters.VponAdapter", guoheAdLayout2, ration2);
                    }
                    return unknownAdNetwork(guoheAdLayout2, ration2);
                case GuoheAdUtil.NETWORK_TYPE_ADTOUCH /*91*/:
                    if (Class.forName("com.energysource.szj.embeded.AdView") != null) {
                        return getNetworkAdapter("com.guohead.sdk.adapters.AdTouchAdapter", guoheAdLayout2, ration2);
                    }
                    return unknownAdNetwork(guoheAdLayout2, ration2);
                case GuoheAdUtil.NETWORK_TYPE_DOMOB /*92*/:
                    if (Class.forName("cn.domob.android.ads.DomobAdView") != null) {
                        return getNetworkAdapter("com.guohead.sdk.adapters.DomobAdapter", guoheAdLayout2, ration2);
                    }
                    return unknownAdNetwork(guoheAdLayout2, ration2);
                case GuoheAdUtil.NETWORK_TYPE_WIYUN /*93*/:
                    if (Class.forName("com.wiyun.ad.AdView") != null) {
                        return getNetworkAdapter("com.guohead.sdk.adapters.WiyunAdapter", guoheAdLayout2, ration2);
                    }
                    return unknownAdNetwork(guoheAdLayout2, ration2);
                case GuoheAdUtil.NETWORK_TYPE_MADHOUSE /*94*/:
                    if (Class.forName("com.madhouse.android.ads.AdView") != null) {
                        return getNetworkAdapter("com.guohead.sdk.adapters.MadHouseAdapter", guoheAdLayout2, ration2);
                    }
                    return unknownAdNetwork(guoheAdLayout2, ration2);
                case GuoheAdUtil.NETWORK_TYPE_YOUMI /*95*/:
                    if (Class.forName("net.youmi.android.AdView") != null) {
                        return getNetworkAdapter("com.guohead.sdk.adapters.YouMiAdapter", guoheAdLayout2, ration2);
                    }
                    return unknownAdNetwork(guoheAdLayout2, ration2);
                case GuoheAdUtil.NETWORK_TYPE_WOOBOO /*96*/:
                    if (Class.forName("com.wooboo.adlib_android.WoobooAdView") != null) {
                        return getNetworkAdapter("com.guohead.sdk.adapters.WooBooAdapter", guoheAdLayout2, ration2);
                    }
                    return unknownAdNetwork(guoheAdLayout2, ration2);
                case GuoheAdUtil.NETWORK_TYPE_ADCHINA /*97*/:
                    if (Class.forName("com.adchina.android.ads.views.AdView") != null) {
                        return getNetworkAdapter("com.guohead.sdk.adapters.AdChinaAdapter", guoheAdLayout2, ration2);
                    }
                    return unknownAdNetwork(guoheAdLayout2, ration2);
                case GuoheAdUtil.NETWORK_TYPE_CASEE /*98*/:
                    if (Class.forName("com.casee.adsdk.CaseeAdView") != null) {
                        return getNetworkAdapter("com.guohead.sdk.adapters.CaseeAdapter", guoheAdLayout2, ration2);
                    }
                    return unknownAdNetwork(guoheAdLayout2, ration2);
                case GuoheAdUtil.NETWORK_TYPE_ADWO /*100*/:
                    if (Class.forName("com.adwo.adsdk.AdwoAdView") != null) {
                        return getNetworkAdapter("com.guohead.sdk.adapters.AdWoAdapter", guoheAdLayout2, ration2);
                    }
                    return unknownAdNetwork(guoheAdLayout2, ration2);
                default:
                    return unknownAdNetwork(guoheAdLayout2, ration2);
            }
        } catch (ClassNotFoundException e) {
            return unknownAdNetwork(guoheAdLayout2, ration2);
        } catch (VerifyError e2) {
            Log.e("GuoheAd", "YYY - Caught VerifyError", e2);
            return unknownAdNetwork(guoheAdLayout2, ration2);
        }
    }

    private static GuoheAdAdapter getNetworkAdapter(String networkAdapter, GuoheAdLayout guoheAdLayout2, Ration ration2) {
        try {
            return (GuoheAdAdapter) Class.forName(networkAdapter).getConstructor(GuoheAdLayout.class, Ration.class).newInstance(guoheAdLayout2, ration2);
        } catch (Exception e) {
            Log.e("GuoheAdAdapter", "ERROR: " + e.getMessage());
            return null;
        }
    }

    private static GuoheAdAdapter unknownAdNetwork(GuoheAdLayout guoheAdLayout2, Ration ration2) {
        Log.w(GuoheAdUtil.GUOHEAD, "Unsupported ration type: " + ration2.type);
        return null;
    }

    public static void finish(Context context) {
        Log.i(GuoheAdUtil.GUOHEAD, "================> finish execute.");
        if (adapter != null) {
            adapter.guoheAdStateListener = null;
        }
        adapter.finish();
    }

    public static void handle(GuoheAdLayout guoheAdLayout2, Ration ration2) throws Throwable {
        if (adapter != null) {
            adapter.finish();
        }
        adapter = getAdapter(guoheAdLayout2, ration2);
        if (adapter != null) {
            Log.d(GuoheAdUtil.GUOHEAD, "Valid adapter, calling handle()");
            adapter.handle();
            return;
        }
        throw new Exception("Invalid adapter");
    }

    /* access modifiers changed from: protected */
    public void notifyOnFail() {
        if (this.guoheAdStateListener != null) {
            this.guoheAdStateListener.onFail();
        }
    }

    /* access modifiers changed from: protected */
    public void notifyOnClick() {
        if (this.guoheAdStateListener != null) {
            this.guoheAdStateListener.onClick();
        }
    }

    /* access modifiers changed from: protected */
    public void clearAdStateListener() {
        this.guoheAdStateListener = null;
    }

    public static void refreshAd(Context context) {
        Log.i(GuoheAdUtil.GUOHEAD, "Manual Refresh AD");
        adapter.refreshAd();
    }
}
