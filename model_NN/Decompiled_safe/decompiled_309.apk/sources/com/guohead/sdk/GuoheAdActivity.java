package com.guohead.sdk;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

public class GuoheAdActivity extends Activity {
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        String Link = getIntent().getExtras().getString("link");
        ScrollView scrollView = new ScrollView(this);
        scrollView.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        RelativeLayout view = new RelativeLayout(this);
        view.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        ProgressBar progressbar = new ProgressBar(this);
        WebView webView = new WebView(this);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.addJavascriptInterface(new Object() {
            public void closeWindow() {
                GuoheAdActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        GuoheAdActivity.this.finish();
                    }
                });
            }
        }, "guohead");
        webView.loadUrl(Link);
        RelativeLayout.LayoutParams webViewParams = new RelativeLayout.LayoutParams(-1, -1);
        RelativeLayout.LayoutParams progressBarParams = new RelativeLayout.LayoutParams(-2, -2);
        progressBarParams.addRule(14);
        progressBarParams.addRule(13);
        view.addView(progressbar, progressBarParams);
        view.addView(webView, webViewParams);
        scrollView.addView(view, webViewParams);
        setContentView(scrollView);
    }
}
