package com.guohead.sdk.obj;

public class Ration implements Comparable<Ration> {
    public String appVersion = "v1.0";
    public String appid = "";
    public String key = "";
    public String key2 = "";
    public String key3 = "";
    public String name = "";
    public String nid = "";
    public int priority = 0;
    public String sdkVersion = "";
    public int type = 0;
    public int weight = 0;

    public int compareTo(Ration another) {
        int otherPriority = another.priority;
        if (this.priority < otherPriority) {
            return -1;
        }
        if (this.priority > otherPriority) {
            return 1;
        }
        return 0;
    }
}
