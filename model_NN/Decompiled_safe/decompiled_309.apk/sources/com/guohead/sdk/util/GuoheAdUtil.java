package com.guohead.sdk.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import com.guohead.sdk.LocationProvider;
import com.guohead.sdk.obj.Extra;
import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

public class GuoheAdUtil {
    public static final int CUSTOM_TYPE_BANNER = 1;
    public static final int CUSTOM_TYPE_BANNER_INAPP = 3;
    public static final int CUSTOM_TYPE_CHOICE = 5;
    public static final int CUSTOM_TYPE_ICON = 2;
    public static final int CUSTOM_TYPE_ICON_INAPP = 4;
    public static final String GUOHEAD = "Guohead SDK";
    public static String Gender = "UnknowGender";
    public static final int NETWORK_TYPE_4THSCREEN = 13;
    public static final int NETWORK_TYPE_ADCHINA = 97;
    public static final int NETWORK_TYPE_ADMOB = 1;
    public static final int NETWORK_TYPE_ADSENSE = 14;
    public static final int NETWORK_TYPE_ADTOUCH = 91;
    public static final int NETWORK_TYPE_ADWO = 100;
    public static final int NETWORK_TYPE_CASEE = 98;
    public static final int NETWORK_TYPE_CUSTOM = 9;
    public static final int NETWORK_TYPE_DOMOB = 92;
    public static final int NETWORK_TYPE_DOUBLECLICK = 15;
    public static final int NETWORK_TYPE_EVENT = 17;
    public static final int NETWORK_TYPE_GENERIC = 16;
    public static final int NETWORK_TYPE_GREYSTRIP = 7;
    public static final int NETWORK_TYPE_GUOHEAD = 10;
    public static final int NETWORK_TYPE_JUMPTAP = 2;
    public static final int NETWORK_TYPE_LIVERAIL = 5;
    public static final int NETWORK_TYPE_MADHOUSE = 94;
    public static final int NETWORK_TYPE_MDOTM = 12;
    public static final int NETWORK_TYPE_MEDIALETS = 4;
    public static final int NETWORK_TYPE_MILLENNIAL = 6;
    public static final int NETWORK_TYPE_MOBCLIX = 11;
    public static final int NETWORK_TYPE_QUATTRO = 8;
    public static final int NETWORK_TYPE_VIDEOEGG = 3;
    public static final int NETWORK_TYPE_VPON = 90;
    public static final int NETWORK_TYPE_WIYUN = 93;
    public static final int NETWORK_TYPE_WOOBOO = 96;
    public static final int NETWORK_TYPE_YOUMI = 95;
    public static String SDK = "Unknow SDK";
    public static final int URL_TYPE_CLICK = 3;
    public static final int URL_TYPE_CONFIG = 1;
    public static final int URL_TYPE_CUSTOM = 5;
    public static final int URL_TYPE_CUSTOM_CHOICE_NO = 7;
    public static final int URL_TYPE_CUSTOM_CHOICE_YES = 6;
    public static final int URL_TYPE_FAIL = 4;
    public static final int URL_TYPE_IMPRESSION = 2;
    public static String appKey = "00000000000000000000000000000000";
    public static String deviceIDHash = "00000000000000000000000000000000";
    public static Extra extra = null;
    public static String filePath = "/sdcard/.GuoheAd";
    public static final String ghVersion = "v1.6.2";
    public static String locale = "zh_CN";
    public static String locationString = ",";
    public static String model = "Unknow Device";
    public static String network = "UnknowModel";

    public static String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 15;
            int two_halfs = 0;
            while (true) {
                if (halfbyte < 0 || halfbyte > 9) {
                    buf.append((char) ((halfbyte - 10) + 97));
                } else {
                    buf.append((char) (halfbyte + 48));
                }
                halfbyte = data[i] & 15;
                int two_halfs2 = two_halfs + 1;
                if (two_halfs >= 1) {
                    break;
                }
                two_halfs = two_halfs2;
            }
        }
        return buf.toString();
    }

    public static void setAppKey(String Key) {
        appKey = Key;
        Logger.addStatus("GHKEY:" + Key);
    }

    public static String getAppKey(Context context) {
        try {
            ApplicationInfo appInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (appInfo.metaData.containsKey("GH_APPKEY")) {
                appKey = appInfo.metaData.getString("GH_APPKEY");
                if (appKey == null || appKey.length() != 32) {
                    appKey = "00000000000000000000000000000000";
                    Logger.e("Guohead Key's length does not equals 32 characters");
                }
                return appKey;
            }
            throw new RuntimeException("GH_APPKEY not found,please set the meta-data GH_APPKEY in AndroidManifest.xml");
        } catch (Exception e) {
            Logger.v(e.toString());
        }
    }

    public static String getAppVersion(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "1.0";
        }
    }

    public static String getNetworkAccessMode(Context context) {
        try {
            if (context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) != 0) {
                return "Unknown";
            }
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null) {
                return "Unknown";
            }
            NetworkInfo info = connectivityManager.getActiveNetworkInfo();
            String typeName = info.getTypeName().toLowerCase();
            if (!typeName.equals("wifi")) {
                typeName = info.getExtraInfo().toLowerCase();
            }
            if (typeName != null) {
                return typeName;
            }
            return "Unknown";
        } catch (Exception e) {
            Logger.v("getNetworkAccessMode Error");
            return "Unknown";
        }
    }

    public static void init(Context context) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            StringBuffer deviceIDString = new StringBuffer("android_id");
            deviceIDString.append("GuoheAd");
            deviceIDHash = convertToHex(md.digest(deviceIDString.toString().getBytes()));
            model = Build.MODEL;
            SDK = Build.VERSION.RELEASE;
            appKey = getAppKey(context);
            locale = Locale.getDefault().toString();
            network = getNetworkAccessMode(context);
            locationString = getLocation(context);
            mkGHDir();
        } catch (NoSuchAlgorithmException e) {
            Logger.e(e.toString());
        }
    }

    public static String getLocation(Context context) {
        return new BASE64Encoder().encode(("GHLocationString:" + new LocationProvider(context).getLocation()).getBytes());
    }

    private static void mkGHDir() {
        File myFilePath = new File(filePath);
        try {
            if (!new File("/sdcard/").exists()) {
                Logger.e("SD card doesn't exist");
            } else if (!myFilePath.isDirectory()) {
                myFilePath.mkdir();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
