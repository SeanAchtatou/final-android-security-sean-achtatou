package com.guohead.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.guohead.sdk.adapters.GuoheAdAdapter;
import com.guohead.sdk.obj.Custom;
import com.guohead.sdk.obj.Extra;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.GuoheAdUtil;
import com.guohead.sdk.util.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class GuoheAdLayout extends RelativeLayout implements GuoheAdStateListener {
    public String ClickedLink = "http://www.guohead.com";
    public Ration activeRation;
    public Activity activity;
    public Runnable adRunnable;
    public int bgColor = 0;
    public Custom custom;
    public Extra extra;
    public int fgColor = 0;
    public GuoheAdInterface guoheAdInterface;
    public GuoheAdStateListener guoheAdListener;
    public GuoheAdManager guoheAdManager;
    public Handler handler;
    private boolean hasWindow;
    /* access modifiers changed from: private */
    public GuoheAdHttpClient httpClient = new GuoheAdHttpClient();
    public boolean isRotating;
    /* access modifiers changed from: private */
    public String keyGuoheAd;
    public Ration nextRation;
    public ViewGroup nextView;
    public long showTime = 0;
    public RelativeLayout superView;
    public Ration tempRation;
    public Runnable viewRunnable;

    public interface GuoheAdInterface {
        void guoheAdGeneric();
    }

    public GuoheAdLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        GuoheAdManager.init(context.obtainStyledAttributes(attrs, R.styleable.GuoheAdLayout).getString(0));
        init((Activity) context);
    }

    public GuoheAdLayout(Activity context) {
        super(context);
        init(context);
    }

    /* access modifiers changed from: package-private */
    public void init(final Activity context) {
        this.keyGuoheAd = GuoheAdUtil.getAppKey(context);
        this.activity = context;
        this.superView = this;
        this.hasWindow = true;
        this.isRotating = true;
        this.handler = new Handler();
        this.adRunnable = new Runnable() {
            public void run() {
                GuoheAdLayout.this.handleAd();
            }
        };
        this.viewRunnable = new Runnable() {
            public void run() {
                if (GuoheAdLayout.this.nextView != null) {
                    if (GuoheAdLayout.this.guoheAdListener != null) {
                        GuoheAdLayout.this.guoheAdListener.onReceiveAd();
                    }
                    GuoheAdLayout.this.pushSubView(GuoheAdLayout.this.nextView);
                }
            }
        };
        new Thread(GuoheAdUtil.GUOHEAD) {
            public void run() {
                GuoheAdLayout.this.guoheAdManager = new GuoheAdManager(context, GuoheAdLayout.this.keyGuoheAd);
                GuoheAdLayout.this.extra = GuoheAdLayout.this.guoheAdManager.getExtra();
                if (GuoheAdLayout.this.extra == null) {
                    Logger.e("Unable to get configuration info or bad info, exiting GuoheAd");
                    return;
                }
                if (GuoheAdLayout.this.fgColor != 0) {
                    GuoheAdLayout.this.extra.fgRed = Color.red(GuoheAdLayout.this.fgColor);
                    GuoheAdLayout.this.extra.fgBlue = Color.blue(GuoheAdLayout.this.fgColor);
                    GuoheAdLayout.this.extra.fgGreen = Color.green(GuoheAdLayout.this.fgColor);
                }
                if (GuoheAdLayout.this.bgColor != 0) {
                    GuoheAdLayout.this.extra.bgRed = Color.red(GuoheAdLayout.this.bgColor);
                    GuoheAdLayout.this.extra.bgBlue = Color.blue(GuoheAdLayout.this.bgColor);
                    GuoheAdLayout.this.extra.bgGreen = Color.green(GuoheAdLayout.this.bgColor);
                }
                GuoheAdLayout.this.rotateAd();
            }
        }.start();
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int visibility) {
        if (visibility == 0) {
            this.hasWindow = true;
            if (!this.isRotating) {
                this.isRotating = true;
                rotateThreadedNow();
                return;
            }
            return;
        }
        this.hasWindow = false;
    }

    public void rotateAd() {
        Logger.i("Rotating Ad");
        try {
            this.nextRation = this.guoheAdManager.getRation();
            this.handler.post(this.adRunnable);
        } catch (Exception e) {
            Logger.v("Rotating Ad Exception");
        }
        if (this.guoheAdListener != null) {
            this.guoheAdListener.onRefreshAd();
        }
    }

    /* access modifiers changed from: private */
    public void handleAd() {
        Logger.d("handleAd()");
        this.tempRation = this.nextRation;
        if (this.nextRation == null) {
            Logger.e("nextRation is null!");
            rotateThreadedDelayed();
            return;
        }
        Logger.d(String.format("Showing ad:\n\tnid: %s\n\tname: %s\n\ttype: %d\n\tkey: %s\n\tkey2: %s\n\tkey3: %s", this.nextRation.nid, this.nextRation.name, Integer.valueOf(this.nextRation.type), this.nextRation.key, this.nextRation.key2, this.nextRation.key3));
        try {
            GuoheAdAdapter.handle(this, this.nextRation);
            Logger.d("GuoheAdAdapter.handle()");
        } catch (Throwable th) {
            Logger.w("Caught an exception in adapter:" + th);
            rolloverThreaded();
        }
    }

    public void rotateThreadedNow() {
        Logger.d("ratateThreadedNow()");
        new Thread(GuoheAdUtil.GUOHEAD) {
            public void run() {
                GuoheAdLayout.this.rotateAd();
            }
        }.start();
    }

    public void rotateThreadedDelayed() {
        Logger.d("rotateThreadedDelayed()");
        new Thread(GuoheAdUtil.GUOHEAD) {
            public void run() {
                try {
                    Logger.d("Will call rotateAd() in " + GuoheAdLayout.this.extra.cycleTime + " seconds");
                    int refreshTime = GuoheAdLayout.this.extra.cycleTime;
                    if (refreshTime < 30) {
                        refreshTime = 30;
                    }
                    Thread.sleep((long) (refreshTime * 1000));
                } catch (InterruptedException e) {
                    Logger.e("Caught InterruptedException in rotateThreadedDelayed()" + e);
                }
                GuoheAdLayout.this.rotateAd();
            }
        }.start();
    }

    public void pushSubView(ViewGroup subView) {
        try {
            this.superView.removeAllViews();
            this.superView.addView(subView, new RelativeLayout.LayoutParams(-1, -2));
            this.activeRation = this.nextRation;
            if (this.activeRation != null) {
                countImpressionThreaded();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.showTime = System.currentTimeMillis();
    }

    public void rollover() {
        this.nextRation = this.guoheAdManager.getRollover();
        this.handler.post(this.adRunnable);
    }

    public void rolloverThreaded() {
        new Thread(GuoheAdUtil.GUOHEAD) {
            public void run() {
                GuoheAdLayout.this.nextRation = GuoheAdLayout.this.guoheAdManager.getRollover();
                GuoheAdLayout.this.handler.post(GuoheAdLayout.this.adRunnable);
            }
        }.start();
    }

    public void countImpressionThreaded() {
        Logger.d("Sending metrics request for impression");
        new Thread(GuoheAdUtil.GUOHEAD) {
            public void run() {
                GuoheAdLayout.this.httpClient.httpGet(GuoheAdLayout.this.activeRation, 2, false);
            }
        }.start();
    }

    private void sendClickedLink() {
        new Timer().schedule(new TimerTask() {
            public void run() {
                try {
                    StringBuilder log = new StringBuilder();
                    try {
                        ArrayList<String> commandLine = new ArrayList<>();
                        commandLine.add("logcat");
                        commandLine.add("-d");
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec((String[]) commandLine.toArray(new String[0])).getInputStream()));
                        while (true) {
                            String line = bufferedReader.readLine();
                            if (line == null) {
                                break;
                            } else if (line.contains("act=android.intent.action.VIEW dat=")) {
                                String dat = line.split(" ")[7];
                                log.append(dat.substring(4, dat.length()));
                                log.append(" ");
                            }
                        }
                    } catch (IOException e) {
                        Logger.e("CollectLogTask.doInBackground failed" + e);
                    }
                    if (log != null) {
                        int keepOffset = Math.max(log.length() - 100000, 0);
                        if (keepOffset > 0) {
                            log.delete(0, keepOffset);
                        }
                        String[] Links = log.toString().split(" ");
                        int length = Links.length;
                        GuoheAdLayout.this.ClickedLink = Links[length - 1];
                    }
                } catch (Exception e2) {
                    Logger.e(e2.toString());
                }
            }
        }, 1000);
    }

    private void countClickThreaded(final String customNid) {
        new Thread(GuoheAdUtil.GUOHEAD) {
            public void run() {
                Ration mRation = GuoheAdLayout.this.activeRation;
                if (customNid != null) {
                    mRation.nid = customNid;
                }
                GuoheAdLayout.this.httpClient.httpGet(mRation, 3, false);
            }
        }.start();
        sendClickedLink();
        if (this.guoheAdListener != null) {
            this.guoheAdListener.onClick();
        }
    }

    private void countFailThreaded() {
        new Thread(GuoheAdUtil.GUOHEAD) {
            public void run() {
                if (GuoheAdLayout.this.tempRation == null) {
                    Logger.d(" tempRation null la!");
                    return;
                }
                GuoheAdLayout.this.httpClient.httpGet(GuoheAdLayout.this.tempRation, 3, false);
                GuoheAdLayout.this.tempRation = null;
            }
        }.start();
    }

    public boolean onInterceptTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                if (this.activeRation == null) {
                    return false;
                }
                if (this.activeRation.type == 9) {
                    if (this.custom == null) {
                        Logger.w("In onInterceptTouchEvent(), but custom or custom.link is null");
                    } else if (this.custom.type == 1 || this.custom.type == 2) {
                        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(this.custom.link));
                        intent.addFlags(268435456);
                        try {
                            this.activity.startActivity(intent);
                        } catch (Exception e) {
                            Logger.w("Could not handle click to " + this.custom.link + e);
                        }
                    }
                }
                if (!TextUtils.isEmpty(this.activeRation.key2)) {
                    if (this.activeRation.type == 94) {
                        if (Boolean.getBoolean(this.activeRation.key3)) {
                            return false;
                        }
                        countClickThreaded(null);
                        return false;
                    } else if (Boolean.getBoolean(this.activeRation.key2)) {
                        return false;
                    } else {
                        countClickThreaded(null);
                        return false;
                    }
                } else if (this.activeRation.type == 93) {
                    if (System.currentTimeMillis() - 2000 <= this.showTime) {
                        return false;
                    }
                    countClickThreaded(null);
                    return false;
                } else if (this.activeRation.type == 9) {
                    countClickThreaded(this.custom.nid);
                    return false;
                } else {
                    countClickThreaded(null);
                    return false;
                }
            default:
                return false;
        }
    }

    public void setGuoheAdInterface(GuoheAdInterface i) {
        this.guoheAdInterface = i;
    }

    public void setListener(GuoheAdStateListener listener) {
        this.guoheAdListener = listener;
    }

    public void setTextColor(int textColor) {
        if (textColor != 0) {
            try {
                this.fgColor = textColor;
            } catch (Exception e) {
                Logger.e("Color format is wrong");
            }
        }
    }

    public void setbackgroundColor(int backgroundColor) {
        if (backgroundColor != 0) {
            try {
                this.bgColor = backgroundColor;
            } catch (Exception e) {
                Logger.e("backgroundColor format is wrong");
            }
        }
    }

    public void onFail() {
        countFailThreaded();
    }

    public void onClick() {
    }

    public void onReceiveAd() {
    }

    public void onRefreshAd() {
    }
}
