package com.yb.sim;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class SIMHelp {
    public static List<User> SimQueryAll(Activity activity) {
        List<User> ls = new ArrayList<>();
        Cursor cursor = activity.getContentResolver().query(Uri.parse("content://icc/adn"), null, null, null, null);
        if (cursor != null) {
            Log.d("SimQueryAll", ">>>>>>" + cursor.getCount());
            while (cursor.moveToNext()) {
                User u = new User();
                String id = cursor.getString(cursor.getColumnIndex("_id"));
                String name = cursor.getString(cursor.getColumnIndex("name"));
                String phoneNumber = cursor.getString(cursor.getColumnIndex("number"));
                u.setId(id);
                u.setName(name);
                u.setTel(phoneNumber);
                ls.add(u);
                Log.d("SimQueryAll", ">>>>>> " + id + "  " + name + "  " + phoneNumber);
            }
        }
        return ls;
    }

    public static void SimInsert(Activity activity, String name, String tel) {
        Uri uri = Uri.parse("content://icc/adn");
        ContentValues values = new ContentValues();
        values.put("tag", name);
        values.put("number", tel);
        Log.d("1023", ">>>>>>new sim contact uri, " + activity.getContentResolver().insert(uri, values).toString());
    }

    public static void SimUpdate(Activity activity, String oldname, String oldtel, String newname, String newtel) {
        Uri uri = Uri.parse("content://icc/adn");
        ContentValues values = new ContentValues();
        values.put("tag", oldname);
        values.put("number", oldtel);
        values.put("newTag", newname);
        values.put("newNumber", newtel);
        activity.getContentResolver().update(uri, values, null, null);
    }

    public static void SimDelete(Activity activity, String _name, String _tel) {
        Uri uri = Uri.parse("content://icc/adn");
        Cursor cursor = activity.getContentResolver().query(uri, null, null, null, null);
        Log.d("1023", ">>>>>> " + cursor.getCount());
        while (cursor.moveToNext()) {
            String name = cursor.getString(cursor.getColumnIndex("name"));
            String phoneNumber = cursor.getString(cursor.getColumnIndex("number"));
            if (name.equals(_name) && phoneNumber.equals(_tel)) {
                activity.getContentResolver().delete(uri, String.valueOf("tag='" + name + "'") + " AND number='" + phoneNumber + "'", null);
                return;
            }
        }
    }

    public static String toSIM(String alphaTag) {
        byte[] byteTagTemp = null;
        if (alphaTag.getBytes().length == alphaTag.length()) {
            return alphaTag;
        }
        try {
            byte[] byteTag = alphaTag.getBytes("utf-16BE");
            byteTagTemp = new byte[(byteTag.length + 1)];
            for (int i = 0; i < byteTag.length; i++) {
                byteTagTemp[i + 1] = (byte) (byteTag[i] & 255);
            }
            for (int j = byteTag.length + 1; j < byteTagTemp.length; j++) {
                byteTagTemp[j] = -1;
            }
            byteTagTemp[0] = Byte.MIN_VALUE;
        } catch (UnsupportedEncodingException e) {
            Log.e("AdnRecord", "alphaTag convert byte exception");
        }
        String d = new String(byteTagTemp);
        Log.i("yb", String.valueOf(alphaTag) + "----------------" + d);
        return d;
    }
}
