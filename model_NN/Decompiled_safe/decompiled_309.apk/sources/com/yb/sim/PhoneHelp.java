package com.yb.sim;

import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public class PhoneHelp {
    public static List<User> phoneQueryAll(Activity activity) {
        List<User> ls = new ArrayList<>();
        Cursor cursor = activity.getContentResolver().query(Contacts.Phones.CONTENT_URI, null, null, null, null);
        if (cursor != null) {
            Log.d("SimQueryAll", ">>>>>>" + cursor.getCount());
            while (cursor.moveToNext()) {
                User u = new User();
                String id = cursor.getString(cursor.getColumnIndex("_id"));
                String name = cursor.getString(cursor.getColumnIndex("name"));
                String phoneNumber = cursor.getString(cursor.getColumnIndex("number"));
                u.setId(id);
                u.setName(name);
                u.setTel(phoneNumber);
                ls.add(u);
                Log.d("SimQueryAll", ">>>>>> " + id + "  " + name + "  " + phoneNumber);
            }
        }
        return ls;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static void phoneInsert(Activity activity, String name, String tel) {
        ContentValues values = new ContentValues();
        long rawContactId = ContentUris.parseId(activity.getContentResolver().insert(ContactsContract.RawContacts.CONTENT_URI, values));
        values.clear();
        values.put("raw_contact_id", Long.valueOf(rawContactId));
        values.put("mimetype", "vnd.android.cursor.item/name");
        values.put("data2", name);
        values.put("data3", "");
        activity.getContentResolver().insert(ContactsContract.Data.CONTENT_URI, values);
        values.clear();
        values.put("raw_contact_id", Long.valueOf(rawContactId));
        values.put("mimetype", "vnd.android.cursor.item/phone_v2");
        values.put("data2", (Integer) 2);
        values.put("data1", tel);
        activity.getContentResolver().insert(ContactsContract.Data.CONTENT_URI, values);
    }

    public static void phoneUpdate(Activity activity, String oldname, String oldtel, String newname, String newtel) {
        Uri uri = Contacts.Phones.CONTENT_URI;
        ContentValues values = new ContentValues();
        values.put("tag", oldname);
        values.put("number", oldtel);
        values.put("newTag", newname);
        values.put("newNumber", newtel);
        activity.getContentResolver().update(uri, values, null, null);
    }

    public static void phoneDelete(Activity activity, String _name, String _tel) {
        Uri uri = Contacts.Phones.CONTENT_URI;
        Cursor cursor = activity.getContentResolver().query(uri, null, null, null, null);
        Log.d("1023", ">>>>>> " + cursor.getCount());
        while (cursor.moveToNext()) {
            String name = cursor.getString(cursor.getColumnIndex("name"));
            String phoneNumber = cursor.getString(cursor.getColumnIndex("number"));
            if (name.equals(_name) && phoneNumber.equals(_tel)) {
                activity.getContentResolver().delete(uri, String.valueOf("tag='" + name + "'") + " AND number='" + phoneNumber + "'", null);
                return;
            }
        }
    }
}
