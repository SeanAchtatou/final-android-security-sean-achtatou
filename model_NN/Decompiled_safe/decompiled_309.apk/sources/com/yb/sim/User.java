package com.yb.sim;

public class User {
    private String desc;
    private String id;
    private String name;
    private String tel;

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc2) {
        this.desc = desc2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getTel() {
        return this.tel;
    }

    public void setTel(String tel2) {
        this.tel = tel2;
    }
}
