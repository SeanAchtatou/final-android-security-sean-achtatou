package com.yb.sim;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.GuoheAdManager;
import com.guohead.sdk.GuoheAdStateListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends Activity {
    private GuoheAdLayout adLayout;
    /* access modifiers changed from: private */
    public SimpleAdapter adapter = null;
    /* access modifiers changed from: private */
    public Button addBtn;
    /* access modifiers changed from: private */
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            String regMsg = msg.getData().getString("regMsg");
            if (regMsg.equals("0")) {
                MainActivity.this.addBtn.setVisibility(8);
                MainActivity.this.toSimBtn.setVisibility(8);
                MainActivity.this.nullTxt.setText("");
                MainActivity.this.adapter = new SimpleAdapter(MainActivity.this, MainActivity.this.list, R.layout.item_320x480, new String[]{"COLUMN_1", "COLUMN_2"}, new int[]{R.id.COLUMN_1, R.id.COLUMN_2});
                MainActivity.this.listview.setAdapter((ListAdapter) MainActivity.this.adapter);
            } else if (regMsg.equals("1")) {
                MainActivity.this.addBtn.setVisibility(8);
                MainActivity.this.toSimBtn.setVisibility(8);
                MainActivity.this.nullTxt.setText("");
                SimpleAdapter la = (SimpleAdapter) MainActivity.this.listview.getAdapter();
                if (la != null) {
                    la.notifyDataSetChanged();
                } else {
                    MainActivity.this.adapter = new SimpleAdapter(MainActivity.this, MainActivity.this.list, R.layout.item_320x480, new String[]{"COLUMN_1", "COLUMN_2"}, new int[]{R.id.COLUMN_1, R.id.COLUMN_2});
                    MainActivity.this.listview.setAdapter((ListAdapter) MainActivity.this.adapter);
                }
            } else if (regMsg.equals("all data")) {
                MainActivity.this.showMsgDialog(R.string.success);
            } else if (regMsg.equals("no data")) {
                MainActivity.this.showMsgDialog(R.string.failed);
            } else {
                MainActivity.this.nullTxt.setText(MainActivity.this.getString(R.string.searchNull));
                if (MainActivity.this.simType.equals("SIM")) {
                    MainActivity.this.addBtn.setVisibility(0);
                    MainActivity.this.toSimBtn.setVisibility(0);
                }
                SimpleAdapter la2 = (SimpleAdapter) MainActivity.this.listview.getAdapter();
                if (la2 != null) {
                    la2.notifyDataSetChanged();
                }
            }
            MainActivity.this.searchprogressBar.setVisibility(8);
            Thread.currentThread().interrupt();
            super.handleMessage(msg);
        }
    };
    /* access modifiers changed from: private */
    public List<Map<String, Object>> list = new ArrayList();
    /* access modifiers changed from: private */
    public List listrs = null;
    /* access modifiers changed from: private */
    public List<User> lists = null;
    /* access modifiers changed from: private */
    public ListView listview = null;
    private ArrayAdapter<String> myTypeAdapter;
    /* access modifiers changed from: private */
    public EditText name_txt;
    /* access modifiers changed from: private */
    public TextView nullTxt = null;
    private AdapterView.OnItemClickListener onItemClick = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
            Log.i("yb", "a1------" + arg2 + "-----");
        }
    };
    private AdapterView.OnItemLongClickListener onItemLongClick = new AdapterView.OnItemLongClickListener() {
        public boolean onItemLongClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
            Log.i("yb", "a2------" + arg2 + "-----");
            MainActivity.this.user = (User) MainActivity.this.lists.get(arg2);
            return false;
        }
    };
    private ProgressDialog progress = null;
    /* access modifiers changed from: private */
    public ProgressBar searchprogressBar = null;
    String simType = "SIM";
    /* access modifiers changed from: private */
    public EditText tel_txt;
    /* access modifiers changed from: private */
    public Button toSimBtn;
    /* access modifiers changed from: private */
    public User user = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        GuoheAdManager.init("e7f154dc5048bc5281d59f5adfd36ea9");
        setContentView((int) R.layout.main);
        RelativeLayout.LayoutParams GuoheAdLayoutParams = new RelativeLayout.LayoutParams(320, -2);
        this.adLayout = new GuoheAdLayout(this);
        this.adLayout.setListener(new GuoheAdStateListener() {
            public void onFail() {
                Log.v("GuoheAd", "OnFail");
            }

            public void onClick() {
                Log.v("GuoheAd", "OnClick");
            }

            public void onReceiveAd() {
                Log.v("GuoheAd", "OnReceiveAd");
            }

            public void onRefreshAd() {
                Log.v("GuoheAd", "OnRefreshAd");
            }
        });
        ((LinearLayout) findViewById(R.id.adLayout)).addView(this.adLayout, GuoheAdLayoutParams);
        checkSIM();
        if (!this.simType.equals("SIM")) {
            showMsgDialog(String.valueOf(getString(R.string.simType)) + this.simType);
        }
        this.listview = (ListView) findViewById(R.id.my_list);
        this.listview.setOnItemClickListener(this.onItemClick);
        this.listview.setOnItemLongClickListener(this.onItemLongClick);
        registerForContextMenu(this.listview);
        this.nullTxt = (TextView) findViewById(R.id.mynullTxt);
        this.searchprogressBar = (ProgressBar) findViewById(R.id.myprogressBar);
        this.searchprogressBar.setVisibility(0);
        this.addBtn = (Button) findViewById(R.id.addBtn);
        this.addBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainActivity.this.showDialogByDetail(MainActivity.this.getString(R.string.add), -1);
            }
        });
        this.toSimBtn = (Button) findViewById(R.id.toSimBtn);
        this.toSimBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainActivity.this.searchprogressBar.setVisibility(0);
                new Thread() {
                    public void run() {
                        String msg;
                        List<User> users = PhoneHelp.phoneQueryAll(MainActivity.this);
                        if (users != null || users.size() > 0) {
                            for (User u : users) {
                                SIMHelp.SimInsert(MainActivity.this, u.getName(), u.getTel());
                            }
                            msg = "all data";
                        } else {
                            msg = "no data";
                        }
                        Message message = MainActivity.this.handler.obtainMessage();
                        Bundle data = new Bundle();
                        data.putString("regMsg", msg);
                        message.setData(data);
                        MainActivity.this.handler.sendMessage(message);
                    }
                }.start();
            }
        });
        new Thread() {
            public void run() {
                MainActivity.this.listrs = MainActivity.this.getListForSimpleAdapter();
                String msg = "";
                if (MainActivity.this.listrs != null && MainActivity.this.listrs.size() > 0) {
                    msg = "0";
                }
                Message message = MainActivity.this.handler.obtainMessage();
                Bundle data = new Bundle();
                data.putString("regMsg", msg);
                message.setData(data);
                MainActivity.this.handler.sendMessage(message);
            }
        }.start();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void refreshList() {
        this.list.clear();
        this.searchprogressBar.setVisibility(0);
        new Thread() {
            public void run() {
                MainActivity.this.listrs = MainActivity.this.getListForSimpleAdapter();
                String msg = "";
                if (MainActivity.this.listrs != null && MainActivity.this.listrs.size() > 0) {
                    msg = "1";
                }
                Message message = MainActivity.this.handler.obtainMessage();
                Bundle data = new Bundle();
                data.putString("regMsg", msg);
                message.setData(data);
                MainActivity.this.handler.sendMessage(message);
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public List<Map<String, Object>> getListForSimpleAdapter() {
        String format = new SimpleDateFormat("yyyy-MM-dd").format(new Date(System.currentTimeMillis()));
        this.lists = SIMHelp.SimQueryAll(this);
        for (User u : this.lists) {
            Map<String, Object> map = new HashMap<>();
            map.put("COLUMN_1", u.getName());
            map.put("COLUMN_2", u.getTel());
            this.list.add(map);
        }
        return this.list;
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.setHeaderTitle((int) R.string.menu);
        menu.add(0, 0, 0, (int) R.string.call);
        menu.add(0, 1, 0, (int) R.string.send);
        if (this.simType.equals("SIM")) {
            menu.add(0, 2, 0, (int) R.string.edit);
            menu.add(0, 3, 0, (int) R.string.del);
            menu.add(0, 4, 0, (int) R.string.add);
            menu.add(0, 5, 0, (int) R.string.toPhone);
            menu.add(0, 6, 0, (int) R.string.toSim);
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        Log.i("yb", "b------" + item.getItemId() + "-----");
        switch (item.getItemId()) {
            case 0:
                startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel://" + this.user.getTel())));
                break;
            case 1:
                startActivity(new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + this.user.getTel())));
                break;
            case 2:
                showDialogByDetail(getString(R.string.edit), 0);
                break;
            case 3:
                showDialogByDel(this.user);
                break;
            case 4:
                showDialogByDetail(getString(R.string.add), -1);
                break;
            case 5:
                PhoneHelp.phoneInsert(this, this.user.getName(), this.user.getTel());
                showMsgDialog((int) R.string.success);
                break;
            case 6:
                this.searchprogressBar.setVisibility(0);
                new Thread() {
                    public void run() {
                        String msg;
                        List<User> users = PhoneHelp.phoneQueryAll(MainActivity.this);
                        if (users != null || users.size() > 0) {
                            for (User u : users) {
                                SIMHelp.SimInsert(MainActivity.this, u.getName(), u.getTel());
                            }
                            msg = "all data";
                        } else {
                            msg = "no data";
                        }
                        Message message = MainActivity.this.handler.obtainMessage();
                        Bundle data = new Bundle();
                        data.putString("regMsg", msg);
                        message.setData(data);
                        MainActivity.this.handler.sendMessage(message);
                    }
                }.start();
                break;
        }
        return super.onContextItemSelected(item);
    }

    /* access modifiers changed from: private */
    public void showDialogByDetail(String title, int state) {
        View view = LayoutInflater.from(this).inflate((int) R.layout.alert_dialog_info, (ViewGroup) null);
        this.name_txt = (EditText) view.findViewById(R.id.name_edit);
        this.tel_txt = (EditText) view.findViewById(R.id.tel_edit);
        if (state != -1) {
            this.name_txt.setText(this.user.getName());
            this.tel_txt.setText(this.user.getTel());
            new AlertDialog.Builder(this).setTitle(title).setView(view).setPositiveButton((int) R.string.save, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    try {
                        SIMHelp.SimUpdate(MainActivity.this, MainActivity.this.user.getName(), MainActivity.this.user.getTel(), MainActivity.this.name_txt.getText().toString(), MainActivity.this.tel_txt.getText().toString());
                        MainActivity.this.showMsgDialog(R.string.success);
                    } catch (Exception e) {
                        MainActivity.this.showMsgDialog(R.string.failed);
                        e.printStackTrace();
                    }
                }
            }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            }).show();
            return;
        }
        new AlertDialog.Builder(this).setTitle(title).setView(view).setPositiveButton((int) R.string.save, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                try {
                    SIMHelp.SimInsert(MainActivity.this, MainActivity.this.name_txt.getText().toString(), MainActivity.this.tel_txt.getText().toString());
                    MainActivity.this.showMsgDialog(R.string.success);
                } catch (Exception e) {
                    MainActivity.this.showMsgDialog(R.string.failed);
                    e.printStackTrace();
                }
            }
        }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }

    private void showDialogByDel(User u) {
        new AlertDialog.Builder(this).setTitle((int) R.string.alert).setMessage(String.valueOf(getString(R.string.delUser)) + "（" + u.getName() + "）").setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        }).setNeutralButton((int) R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                try {
                    SIMHelp.SimDelete(MainActivity.this, MainActivity.this.user.getName(), MainActivity.this.user.getTel());
                    MainActivity.this.showMsgDialog(R.string.success);
                } catch (Exception e) {
                    MainActivity.this.showMsgDialog(R.string.failed);
                    e.printStackTrace();
                }
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void showMsgDialog(int msg) {
        new AlertDialog.Builder(this).setTitle((int) R.string.alert).setMessage(msg).setNeutralButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                MainActivity.this.refreshList();
            }
        }).show();
    }

    private void showMsgDialog(String msg) {
        new AlertDialog.Builder(this).setTitle((int) R.string.alert).setMessage(msg).setNeutralButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        }).show();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void checkSIM() {
        int type = ((TelephonyManager) getSystemService("phone")).getNetworkType();
        if (type == 3) {
            this.simType = "USIM";
        } else if (type == 1) {
            this.simType = "SIM";
        } else if (type == 2) {
            this.simType = "SIM";
        }
        Log.i("yb", "---------------" + this.simType);
    }
}
