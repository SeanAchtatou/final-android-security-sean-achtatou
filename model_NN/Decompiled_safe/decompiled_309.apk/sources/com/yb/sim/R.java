package com.yb.sim;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int call = 2130837504;
        public static final int icon = 2130837505;
        public static final int line = 2130837506;
        public static final int photo = 2130837507;
    }

    public static final class id {
        public static final int COLUMN_1 = 2131034114;
        public static final int COLUMN_2 = 2131034115;
        public static final int adLayout = 2131034121;
        public static final int addBtn = 2131034119;
        public static final int my_list = 2131034116;
        public static final int mynullTxt = 2131034118;
        public static final int myprogressBar = 2131034117;
        public static final int name_edit = 2131034112;
        public static final int tel_edit = 2131034113;
        public static final int toSimBtn = 2131034120;
    }

    public static final class layout {
        public static final int alert_dialog_info = 2130903040;
        public static final int item_320x480 = 2130903041;
        public static final int main = 2130903042;
    }

    public static final class string {
        public static final int add = 2130968589;
        public static final int alert = 2130968584;
        public static final int app_name = 2130968577;
        public static final int call = 2130968585;
        public static final int cancel = 2130968582;
        public static final int del = 2130968588;
        public static final int delUser = 2130968594;
        public static final int edit = 2130968587;
        public static final int failed = 2130968593;
        public static final int menu = 2130968583;
        public static final int no = 2130968579;
        public static final int ok = 2130968580;
        public static final int save = 2130968581;
        public static final int searchNull = 2130968576;
        public static final int send = 2130968586;
        public static final int simType = 2130968595;
        public static final int success = 2130968592;
        public static final int toPhone = 2130968590;
        public static final int toSim = 2130968591;
        public static final int yes = 2130968578;
    }
}
