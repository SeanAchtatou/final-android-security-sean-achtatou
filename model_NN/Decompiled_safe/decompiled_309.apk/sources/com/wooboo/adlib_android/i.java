package com.wooboo.adlib_android;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class i extends SQLiteOpenHelper {
    private static i a = null;

    protected static i a(Context context) {
        if (a == null) {
            a = new i(context, "woobooad.db", null, 1);
        }
        return a;
    }

    private i(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 1);
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS woobooad( ID integer primary key,imgName varchar,downloadTime date,imgData blob)");
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS woobooad");
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS woobooad( ID integer primary key,imgName varchar,downloadTime date,imgData blob)");
    }

    /* access modifiers changed from: protected */
    public final void a(String str, byte[] bArr) {
        try {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("imgName", str);
            contentValues.put("downloadTime", new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
            contentValues.put("imgData", bArr);
            writableDatabase.insert("woobooad", "ID", contentValues);
            writableDatabase.close();
        } catch (Exception e) {
        }
        try {
            Cursor query = getReadableDatabase().query("woobooad", null, null, null, null, null, null);
            if (query.getCount() > 20) {
                query.moveToFirst();
                getReadableDatabase().execSQL("delete from woobooad where ID = " + query.getString(0));
            }
            query.close();
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a(String str) {
        boolean z;
        Cursor query = getReadableDatabase().query("woobooad", null, "imgName=?", new String[]{str}, null, null, null);
        if (query == null || query.getCount() == 0) {
            z = false;
        } else {
            z = true;
        }
        try {
            query.close();
        } catch (Exception e) {
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public final byte[] b(String str) {
        byte[] bArr = null;
        Cursor rawQuery = getReadableDatabase().rawQuery("select imgName,imgData from woobooad where imgName = " + str, new String[0]);
        if (rawQuery.moveToFirst()) {
            bArr = rawQuery.getBlob(1);
        }
        rawQuery.close();
        return bArr;
    }
}
