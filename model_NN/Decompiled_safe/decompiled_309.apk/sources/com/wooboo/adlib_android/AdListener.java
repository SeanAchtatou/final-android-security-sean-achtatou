package com.wooboo.adlib_android;

public interface AdListener {
    void onFailedToReceiveAd(WoobooAdView woobooAdView);

    @Deprecated
    void onNewAd();

    void onReceiveAd(WoobooAdView woobooAdView);
}
