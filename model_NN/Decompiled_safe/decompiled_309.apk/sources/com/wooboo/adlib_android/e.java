package com.wooboo.adlib_android;

import java.util.HashMap;
import java.util.Properties;

public final class e {
    private static HashMap<String, String> a = new HashMap<>();
    private static e b = null;

    static {
        new Properties();
    }

    private e() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x0067 A[SYNTHETIC, Splitter:B:37:0x0067] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x006c A[Catch:{ IOException -> 0x0075 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:20:0x0035=Splitter:B:20:0x0035, B:41:0x006f=Splitter:B:41:0x006f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static synchronized com.wooboo.adlib_android.e a(android.content.Context r7) {
        /*
            r4 = 0
            java.lang.Class<com.wooboo.adlib_android.e> r0 = com.wooboo.adlib_android.e.class
            monitor-enter(r0)
            com.wooboo.adlib_android.e r1 = com.wooboo.adlib_android.e.b     // Catch:{ all -> 0x0070 }
            if (r1 == 0) goto L_0x000c
            com.wooboo.adlib_android.e r1 = com.wooboo.adlib_android.e.b     // Catch:{ all -> 0x0070 }
        L_0x000a:
            monitor-exit(r0)
            return r1
        L_0x000c:
            android.content.res.AssetManager r1 = r7.getAssets()     // Catch:{ IOException -> 0x0085, all -> 0x0062 }
            java.lang.String r2 = "wooboo_ua.properties"
            java.io.InputStream r1 = r1.open(r2)     // Catch:{ IOException -> 0x0085, all -> 0x0062 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0089, all -> 0x0077 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0089, all -> 0x0077 }
            r3.<init>(r1)     // Catch:{ IOException -> 0x0089, all -> 0x0077 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x0089, all -> 0x0077 }
        L_0x0020:
            java.lang.String r3 = r2.readLine()     // Catch:{ IOException -> 0x004d, all -> 0x007d }
            if (r3 != 0) goto L_0x0038
            com.wooboo.adlib_android.e r3 = new com.wooboo.adlib_android.e     // Catch:{ IOException -> 0x004d, all -> 0x007d }
            r3.<init>()     // Catch:{ IOException -> 0x004d, all -> 0x007d }
            com.wooboo.adlib_android.e.b = r3     // Catch:{ IOException -> 0x004d, all -> 0x007d }
            if (r1 == 0) goto L_0x0032
            r1.close()     // Catch:{ IOException -> 0x0073 }
        L_0x0032:
            r2.close()     // Catch:{ IOException -> 0x0073 }
        L_0x0035:
            com.wooboo.adlib_android.e r1 = com.wooboo.adlib_android.e.b     // Catch:{ all -> 0x0070 }
            goto L_0x000a
        L_0x0038:
            java.util.StringTokenizer r4 = new java.util.StringTokenizer     // Catch:{ IOException -> 0x004d, all -> 0x007d }
            java.lang.String r5 = "="
            r4.<init>(r3, r5)     // Catch:{ IOException -> 0x004d, all -> 0x007d }
            java.util.HashMap<java.lang.String, java.lang.String> r3 = com.wooboo.adlib_android.e.a     // Catch:{ IOException -> 0x004d, all -> 0x007d }
            java.lang.String r5 = r4.nextToken()     // Catch:{ IOException -> 0x004d, all -> 0x007d }
            java.lang.String r4 = r4.nextToken()     // Catch:{ IOException -> 0x004d, all -> 0x007d }
            r3.put(r5, r4)     // Catch:{ IOException -> 0x004d, all -> 0x007d }
            goto L_0x0020
        L_0x004d:
            r3 = move-exception
            r6 = r3
            r3 = r2
            r2 = r1
            r1 = r6
        L_0x0052:
            r1.printStackTrace()     // Catch:{ all -> 0x0083 }
            if (r2 == 0) goto L_0x005a
            r2.close()     // Catch:{ IOException -> 0x0060 }
        L_0x005a:
            if (r3 == 0) goto L_0x0035
            r3.close()     // Catch:{ IOException -> 0x0060 }
            goto L_0x0035
        L_0x0060:
            r1 = move-exception
            goto L_0x0035
        L_0x0062:
            r1 = move-exception
            r2 = r4
            r3 = r4
        L_0x0065:
            if (r2 == 0) goto L_0x006a
            r2.close()     // Catch:{ IOException -> 0x0075 }
        L_0x006a:
            if (r3 == 0) goto L_0x006f
            r3.close()     // Catch:{ IOException -> 0x0075 }
        L_0x006f:
            throw r1     // Catch:{ all -> 0x0070 }
        L_0x0070:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x0073:
            r1 = move-exception
            goto L_0x0035
        L_0x0075:
            r2 = move-exception
            goto L_0x006f
        L_0x0077:
            r2 = move-exception
            r3 = r4
            r6 = r1
            r1 = r2
            r2 = r6
            goto L_0x0065
        L_0x007d:
            r3 = move-exception
            r6 = r3
            r3 = r2
            r2 = r1
            r1 = r6
            goto L_0x0065
        L_0x0083:
            r1 = move-exception
            goto L_0x0065
        L_0x0085:
            r1 = move-exception
            r2 = r4
            r3 = r4
            goto L_0x0052
        L_0x0089:
            r2 = move-exception
            r3 = r4
            r6 = r1
            r1 = r2
            r2 = r6
            goto L_0x0052
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wooboo.adlib_android.e.a(android.content.Context):com.wooboo.adlib_android.e");
    }

    public static int a(String str) {
        String upperCase = str.toUpperCase();
        for (String next : a.keySet()) {
            if (upperCase.indexOf(next) != -1) {
                return Integer.parseInt(a.get(next));
            }
        }
        return -1;
    }
}
