package com.wooboo.adlib_android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public final class ImpressionAdView extends PopupWindow {
    /* access modifiers changed from: private */
    public static ImpressionAdView a;
    /* access modifiers changed from: private */
    public static Handler b = null;
    /* access modifiers changed from: private */
    public static Context c;
    /* access modifiers changed from: private */
    public static int d = 0;
    /* access modifiers changed from: private */
    public static int e = 0;
    private static int f;
    /* access modifiers changed from: private */
    public static View g;
    private static int h;
    private static Timer i = null;
    /* access modifiers changed from: private */
    public static ImageButton j;
    private static Handler k = new Handler();
    /* access modifiers changed from: private */
    public static RelativeLayout l;
    /* access modifiers changed from: private */
    public static Bitmap m = null;
    private static double n = 0.0d;
    private static int o;
    private static int p;
    private static int q = 0;
    private static int r = 0;

    protected static int getAdWidth() {
        return o;
    }

    protected static void setAdWidth(int i2) {
        o = i2;
    }

    protected static int getAdHeight() {
        return p;
    }

    protected static void setAdHeight(int i2) {
        p = i2;
    }

    protected static double getDen() {
        return n;
    }

    protected static void setDen(double d2) {
        n = d2;
    }

    protected static int getBtnWidth() {
        return q;
    }

    protected static void setBtnWidth(int i2) {
        q = i2;
    }

    protected static int getBtnHeight() {
        return r;
    }

    protected static void setBtnHeight(int i2) {
        r = i2;
    }

    public static void show(Context context, String str, View view, int i2, int i3, int i4, boolean z, int i5) {
        int i6;
        if (view == null) {
            d.c("The parent view that you add is null,please check whether the parent view is initialized or is a real view.");
        }
        d.d(str);
        new DisplayMetrics();
        setDen((double) context.getApplicationContext().getResources().getDisplayMetrics().density);
        ImpressionAdView impressionAdView = new ImpressionAdView(context);
        a = impressionAdView;
        impressionAdView.setAnimationStyle(16973826);
        c = context;
        e = i2;
        d = i3;
        d.c(context);
        d.a(z);
        f = -16777216 | i4;
        g = view;
        j();
        if (i5 <= 0) {
            i6 = 0;
        } else if (i5 < 20) {
            d.c("AdView.setRequestInterval(" + i5 + ") seconds must be >= " + 20);
            i6 = i5;
        } else {
            if (i5 > 600) {
                d.c("AdView.setRequestInterval(" + i5 + ") seconds must be <= " + 600);
            }
            i6 = i5;
        }
        int i7 = i6 * 1000;
        h = i7;
        if (i7 == 0) {
            a(false);
        } else {
            a(true);
        }
        Log.i("Wooboo SDK", "Version 1.1");
        d.a(d.a(context));
        try {
            if (m == null) {
                m = BitmapFactory.decodeStream(c.getAssets().open("wooboo_btn.png"));
            }
        } catch (IOException e2) {
        }
        setBtnWidth((int) (n * ((double) m.getWidth())));
        setBtnHeight((int) (n * ((double) m.getHeight())));
        setAdHeight((int) (n * 48.0d));
        setAdWidth((int) (n * 320.0d));
        d.a();
        d.e(d.e(context));
        d.b(d.d(context));
        d.a(context.getPackageName());
        e.a(context);
        d.b(e.a(Build.MODEL));
    }

    public static void show(Context context, View view, int i2, int i3, int i4, boolean z, int i5) {
        show(context, d.b(context), view, i2, i3, i4, z, i5);
    }

    private static void a(boolean z) {
        synchronized (a) {
            if (z) {
                if (h > 0) {
                    if (i == null) {
                        Timer timer = new Timer();
                        i = timer;
                        timer.schedule(new TimerTask() {
                            public final void run() {
                                if (ImpressionAdView.g.isShown() && !ImpressionAdView.a.isShowing()) {
                                    ImpressionAdView.j();
                                }
                            }
                        }, (long) h, (long) h);
                    }
                }
            }
            if ((!z || h == 0) && i != null) {
                i.cancel();
                i = null;
            }
        }
    }

    /* access modifiers changed from: private */
    public static void j() {
        if (b == null) {
            b = new Handler();
        }
        new Thread() {
            public final void run() {
                try {
                    c f = d.f(ImpressionAdView.c);
                    if (f != null) {
                        synchronized (this) {
                            final a aVar = new a(f, ImpressionAdView.c, true, ImpressionAdView.getAdWidth(), ImpressionAdView.getAdHeight(), ImpressionAdView.getDen());
                            aVar.a(ImpressionAdView.getTextColor());
                            aVar.setVisibility(0);
                            ImpressionAdView.b.post(new Runnable(this) {
                                public final void run() {
                                    ImpressionAdView.close();
                                    aVar.d();
                                    aVar.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
                                    ImpressionAdView.l = new RelativeLayout(ImpressionAdView.c);
                                    ImpressionAdView.l.addView(aVar);
                                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ImpressionAdView.getBtnWidth(), ImpressionAdView.getBtnHeight());
                                    ImpressionAdView.j = new ImageButton(ImpressionAdView.c);
                                    layoutParams.addRule(11);
                                    ImpressionAdView.j.setId(2);
                                    ImpressionAdView.j.setLayoutParams(layoutParams);
                                    try {
                                        ImpressionAdView.j.setBackgroundDrawable(new BitmapDrawable(ImpressionAdView.m));
                                    } catch (Exception e) {
                                    }
                                    ImpressionAdView.j.setOnClickListener(new View.OnClickListener(this) {
                                        public final void onClick(View view) {
                                            ImpressionAdView.close();
                                        }
                                    });
                                    ImpressionAdView.l.addView(ImpressionAdView.j, layoutParams);
                                    try {
                                        ImpressionAdView.a.setContentView(ImpressionAdView.l);
                                        ImpressionAdView.a.showAtLocation(ImpressionAdView.g, 0, ImpressionAdView.e, ImpressionAdView.d);
                                        ImpressionAdView.a.update(ImpressionAdView.e, ImpressionAdView.d, ImpressionAdView.getAdWidth(), ImpressionAdView.getAdHeight());
                                    } catch (Exception e2) {
                                        Log.e("Wooboo SDK", "Can not display an impressionAdView,please check params.");
                                    }
                                }
                            });
                        }
                    }
                } catch (Exception e) {
                    Log.e("Wooboo SDK", "Unhandled exception requesting a fresh ad.", e);
                }
            }
        }.start();
    }

    public final void onWindowFocusChanged(boolean z) {
        a(z);
    }

    protected static int getTextColor() {
        return f;
    }

    public static void close() {
        if (a != null) {
            k.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        if (ImpressionAdView.a != null) {
                            ImpressionAdView.a.dismiss();
                        }
                    } catch (Exception e) {
                    }
                }
            }, 8000);
            if (a.isShowing()) {
                try {
                    a.dismiss();
                    if (l != null) {
                        l.removeAllViews();
                        l = null;
                    }
                } catch (Exception e2) {
                    a = null;
                }
            }
        }
    }

    private ImpressionAdView() {
    }

    private ImpressionAdView(Context context) {
        super(context);
    }
}
