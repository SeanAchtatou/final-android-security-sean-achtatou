package com.wooboo.adlib_android;

import android.content.Context;
import android.util.Log;
import java.util.ArrayList;

final class c {
    /* access modifiers changed from: private */
    public static int a = 5000;
    /* access modifiers changed from: private */
    public Context b;
    private String c = null;
    /* access modifiers changed from: private */
    public String d = null;
    private String e = null;
    /* access modifiers changed from: private */
    public byte f;
    /* access modifiers changed from: private */
    public a g;

    interface a {
        void a();

        void b();
    }

    public static c a(Context context, byte[] bArr) {
        c cVar = new c();
        cVar.b = context;
        ArrayList<Object> a2 = h.a(bArr);
        if (a2 == null) {
            return null;
        }
        cVar.c = (String) a2.get(0);
        cVar.c = cVar.c;
        cVar.e = (String) a2.get(1);
        cVar.d = (String) a2.get(2);
        cVar.f = ((Byte) a2.get(3)).byteValue();
        if (cVar.c.length() == 0 && cVar.e.length() == 0) {
            return null;
        }
        Log.d("Wooboo SDK", "Get an ad from Wooboo servers.");
        return cVar;
    }

    private c() {
    }

    public final void a() {
        if (this.d != null) {
            new Thread() {
                /* JADX WARNING: Removed duplicated region for block: B:13:0x0053 A[SYNTHETIC, Splitter:B:13:0x0053] */
                /* JADX WARNING: Removed duplicated region for block: B:164:? A[RETURN, SYNTHETIC] */
                /* JADX WARNING: Removed duplicated region for block: B:16:0x0059  */
                /* JADX WARNING: Removed duplicated region for block: B:24:0x0079  */
                /* JADX WARNING: Removed duplicated region for block: B:47:0x012f A[SYNTHETIC, Splitter:B:47:0x012f] */
                /* JADX WARNING: Removed duplicated region for block: B:79:0x0196 A[SYNTHETIC, Splitter:B:79:0x0196] */
                /* JADX WARNING: Removed duplicated region for block: B:82:0x019b A[Catch:{ Exception -> 0x019f }] */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final void run() {
                    /*
                        r13 = this;
                        r10 = 0
                        r9 = 1
                        r7 = 0
                        java.lang.String r11 = "|"
                        java.lang.String r8 = "Wooboo SDK"
                        com.wooboo.adlib_android.c r0 = com.wooboo.adlib_android.c.this
                        byte r1 = r0.f
                        com.wooboo.adlib_android.c r0 = com.wooboo.adlib_android.c.this     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        com.wooboo.adlib_android.c$a r0 = r0.g     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        if (r0 == 0) goto L_0x001e
                        com.wooboo.adlib_android.c r0 = com.wooboo.adlib_android.c.this     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        com.wooboo.adlib_android.c$a r0 = r0.g     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        r0.a()     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                    L_0x001e:
                        java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        com.wooboo.adlib_android.c r2 = com.wooboo.adlib_android.c.this     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        java.lang.String r2 = r2.d     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        r0.<init>(r2)     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        int r2 = com.wooboo.adlib_android.d.e     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        int r3 = com.wooboo.adlib_android.d.a     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        if (r2 != r3) goto L_0x0083
                        java.net.URLConnection r0 = r0.openConnection()     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        r2 = 1
                        java.net.HttpURLConnection.setFollowRedirects(r2)     // Catch:{ MalformedURLException -> 0x00b8, IOException -> 0x0106 }
                        int r2 = com.wooboo.adlib_android.c.a     // Catch:{ MalformedURLException -> 0x00b8, IOException -> 0x0106 }
                        r0.setConnectTimeout(r2)     // Catch:{ MalformedURLException -> 0x00b8, IOException -> 0x0106 }
                        int r2 = com.wooboo.adlib_android.c.a     // Catch:{ MalformedURLException -> 0x00b8, IOException -> 0x0106 }
                        r0.setReadTimeout(r2)     // Catch:{ MalformedURLException -> 0x00b8, IOException -> 0x0106 }
                    L_0x0047:
                        r0.connect()     // Catch:{ MalformedURLException -> 0x00b8, IOException -> 0x0106 }
                        r0.getResponseCode()     // Catch:{ MalformedURLException -> 0x00b8, IOException -> 0x0106 }
                        java.net.URL r2 = r0.getURL()     // Catch:{ MalformedURLException -> 0x00b8, IOException -> 0x0106 }
                    L_0x0051:
                        if (r1 != r9) goto L_0x012f
                        java.lang.String r0 = r2.toString()     // Catch:{ NullPointerException -> 0x0124 }
                    L_0x0057:
                        if (r0 == 0) goto L_0x0071
                        android.content.Intent r2 = new android.content.Intent
                        r2.<init>()
                        switch(r1) {
                            case 1: goto L_0x01cd;
                            case 2: goto L_0x01cd;
                            case 3: goto L_0x01e5;
                            case 4: goto L_0x01e5;
                            case 5: goto L_0x01e5;
                            case 6: goto L_0x01e5;
                            case 7: goto L_0x01e5;
                            case 8: goto L_0x0201;
                            case 9: goto L_0x022a;
                            case 10: goto L_0x0288;
                            case 11: goto L_0x01e5;
                            case 12: goto L_0x02a1;
                            case 13: goto L_0x02e5;
                            default: goto L_0x0061;
                        }
                    L_0x0061:
                        r1 = r0
                        r0 = r2
                    L_0x0063:
                        r2 = 268435456(0x10000000, float:2.5243549E-29)
                        r0.addFlags(r2)
                        com.wooboo.adlib_android.c r2 = com.wooboo.adlib_android.c.this     // Catch:{ Exception -> 0x032d }
                        android.content.Context r2 = r2.b     // Catch:{ Exception -> 0x032d }
                        r2.startActivity(r0)     // Catch:{ Exception -> 0x032d }
                    L_0x0071:
                        com.wooboo.adlib_android.c r0 = com.wooboo.adlib_android.c.this
                        com.wooboo.adlib_android.c$a r0 = r0.g
                        if (r0 == 0) goto L_0x0082
                        com.wooboo.adlib_android.c r0 = com.wooboo.adlib_android.c.this
                        com.wooboo.adlib_android.c$a r0 = r0.g
                        r0.b()
                    L_0x0082:
                        return
                    L_0x0083:
                        int r2 = com.wooboo.adlib_android.d.e     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        int r3 = com.wooboo.adlib_android.d.b     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        if (r2 == r3) goto L_0x008f
                        int r2 = com.wooboo.adlib_android.d.e     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        int r3 = com.wooboo.adlib_android.d.c     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        if (r2 != r3) goto L_0x00d6
                    L_0x008f:
                        java.net.Proxy r2 = new java.net.Proxy     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        java.net.Proxy$Type r3 = java.net.Proxy.Type.HTTP     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        java.net.InetSocketAddress r4 = new java.net.InetSocketAddress     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        java.lang.String r5 = "10.0.0.172"
                        r6 = 80
                        r4.<init>(r5, r6)     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        r2.<init>(r3, r4)     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        java.net.URLConnection r0 = r0.openConnection(r2)     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        r2 = 1
                        java.net.HttpURLConnection.setFollowRedirects(r2)     // Catch:{ MalformedURLException -> 0x00b8, IOException -> 0x0106 }
                        int r2 = com.wooboo.adlib_android.c.a     // Catch:{ MalformedURLException -> 0x00b8, IOException -> 0x0106 }
                        r0.setConnectTimeout(r2)     // Catch:{ MalformedURLException -> 0x00b8, IOException -> 0x0106 }
                        int r2 = com.wooboo.adlib_android.c.a     // Catch:{ MalformedURLException -> 0x00b8, IOException -> 0x0106 }
                        r0.setReadTimeout(r2)     // Catch:{ MalformedURLException -> 0x00b8, IOException -> 0x0106 }
                        goto L_0x0047
                    L_0x00b8:
                        r2 = move-exception
                    L_0x00b9:
                        java.lang.String r2 = "Wooboo SDK"
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder
                        java.lang.String r3 = "Malformed click URL.  Will try to follow anyway."
                        r2.<init>(r3)
                        com.wooboo.adlib_android.c r3 = com.wooboo.adlib_android.c.this
                        java.lang.String r3 = r3.d
                        java.lang.StringBuilder r2 = r2.append(r3)
                        java.lang.String r2 = r2.toString()
                        android.util.Log.w(r8, r2)
                        r2 = r7
                        goto L_0x0051
                    L_0x00d6:
                        int r2 = com.wooboo.adlib_android.d.e     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        int r3 = com.wooboo.adlib_android.d.d     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        if (r2 != r3) goto L_0x036c
                        java.net.Proxy r2 = new java.net.Proxy     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        java.net.Proxy$Type r3 = java.net.Proxy.Type.HTTP     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        java.net.InetSocketAddress r4 = new java.net.InetSocketAddress     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        java.lang.String r5 = "10.0.0.200"
                        r6 = 80
                        r4.<init>(r5, r6)     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        r2.<init>(r3, r4)     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        java.net.URLConnection r0 = r0.openConnection(r2)     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x035a, IOException -> 0x0356 }
                        r2 = 1
                        java.net.HttpURLConnection.setFollowRedirects(r2)     // Catch:{ MalformedURLException -> 0x00b8, IOException -> 0x0106 }
                        int r2 = com.wooboo.adlib_android.c.a     // Catch:{ MalformedURLException -> 0x00b8, IOException -> 0x0106 }
                        r0.setConnectTimeout(r2)     // Catch:{ MalformedURLException -> 0x00b8, IOException -> 0x0106 }
                        int r2 = com.wooboo.adlib_android.c.a     // Catch:{ MalformedURLException -> 0x00b8, IOException -> 0x0106 }
                        r0.setReadTimeout(r2)     // Catch:{ MalformedURLException -> 0x00b8, IOException -> 0x0106 }
                        goto L_0x0047
                    L_0x0106:
                        r2 = move-exception
                    L_0x0107:
                        java.lang.String r2 = "Wooboo SDK"
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder
                        java.lang.String r3 = "Could not determine final click destination URL.  Will try to follow anyway.  "
                        r2.<init>(r3)
                        com.wooboo.adlib_android.c r3 = com.wooboo.adlib_android.c.this
                        java.lang.String r3 = r3.d
                        java.lang.StringBuilder r2 = r2.append(r3)
                        java.lang.String r2 = r2.toString()
                        android.util.Log.w(r8, r2)
                        r2 = r7
                        goto L_0x0051
                    L_0x0124:
                        r0 = move-exception
                        java.lang.String r0 = "Wooboo SDK"
                        java.lang.String r0 = "Could not get ad click url from wooboo server."
                        android.util.Log.e(r8, r0)
                        r0 = r7
                        goto L_0x0057
                    L_0x012f:
                        java.io.InputStream r2 = r0.getInputStream()     // Catch:{ IOException -> 0x0156, all -> 0x0192 }
                        if (r2 == 0) goto L_0x0369
                        int r3 = r2.available()     // Catch:{ IOException -> 0x0353 }
                        byte[] r4 = new byte[r3]     // Catch:{ IOException -> 0x0353 }
                        r5 = r10
                    L_0x013c:
                        if (r5 < r3) goto L_0x0150
                        java.lang.String r3 = new java.lang.String     // Catch:{ IOException -> 0x0353 }
                        r3.<init>(r4)     // Catch:{ IOException -> 0x0353 }
                    L_0x0143:
                        if (r2 == 0) goto L_0x0148
                        r2.close()     // Catch:{ Exception -> 0x01b5 }
                    L_0x0148:
                        if (r0 == 0) goto L_0x01ca
                        r0.disconnect()     // Catch:{ Exception -> 0x01b5 }
                        r0 = r3
                        goto L_0x0057
                    L_0x0150:
                        r2.read(r4)     // Catch:{ IOException -> 0x0353 }
                        int r5 = r5 + 1
                        goto L_0x013c
                    L_0x0156:
                        r2 = move-exception
                        r2 = r7
                    L_0x0158:
                        java.lang.String r3 = "Wooboo SDK"
                        java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0350 }
                        java.lang.String r5 = "Connection off "
                        r4.<init>(r5)     // Catch:{ all -> 0x0350 }
                        r5 = 0
                        java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0350 }
                        java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0350 }
                        android.util.Log.e(r3, r4)     // Catch:{ all -> 0x0350 }
                        if (r2 == 0) goto L_0x0172
                        r2.close()     // Catch:{ Exception -> 0x017a }
                    L_0x0172:
                        if (r0 == 0) goto L_0x0366
                        r0.disconnect()     // Catch:{ Exception -> 0x017a }
                        r0 = r7
                        goto L_0x0057
                    L_0x017a:
                        r0 = move-exception
                        java.lang.String r0 = "Wooboo SDK"
                        java.lang.StringBuilder r0 = new java.lang.StringBuilder
                        java.lang.String r2 = "Could not close stream"
                        r0.<init>(r2)
                        java.lang.StringBuilder r0 = r0.append(r7)
                        java.lang.String r0 = r0.toString()
                        android.util.Log.e(r8, r0)
                        r0 = r7
                        goto L_0x0057
                    L_0x0192:
                        r1 = move-exception
                        r2 = r7
                    L_0x0194:
                        if (r2 == 0) goto L_0x0199
                        r2.close()     // Catch:{ Exception -> 0x019f }
                    L_0x0199:
                        if (r0 == 0) goto L_0x019e
                        r0.disconnect()     // Catch:{ Exception -> 0x019f }
                    L_0x019e:
                        throw r1
                    L_0x019f:
                        r0 = move-exception
                        java.lang.String r0 = "Wooboo SDK"
                        java.lang.StringBuilder r0 = new java.lang.StringBuilder
                        java.lang.String r2 = "Could not close stream"
                        r0.<init>(r2)
                        java.lang.StringBuilder r0 = r0.append(r7)
                        java.lang.String r0 = r0.toString()
                        android.util.Log.e(r8, r0)
                        goto L_0x019e
                    L_0x01b5:
                        r0 = move-exception
                        java.lang.String r0 = "Wooboo SDK"
                        java.lang.StringBuilder r0 = new java.lang.StringBuilder
                        java.lang.String r2 = "Could not close stream"
                        r0.<init>(r2)
                        java.lang.StringBuilder r0 = r0.append(r3)
                        java.lang.String r0 = r0.toString()
                        android.util.Log.e(r8, r0)
                    L_0x01ca:
                        r0 = r3
                        goto L_0x0057
                    L_0x01cd:
                        java.lang.String r1 = "android.intent.action.VIEW"
                        r2.setAction(r1)     // Catch:{ NullPointerException -> 0x01dd }
                        android.net.Uri r1 = android.net.Uri.parse(r0)     // Catch:{ NullPointerException -> 0x01dd }
                        r2.setData(r1)     // Catch:{ NullPointerException -> 0x01dd }
                        r1 = r0
                        r0 = r2
                        goto L_0x0063
                    L_0x01dd:
                        r1 = move-exception
                        r1.printStackTrace()
                        r1 = r0
                        r0 = r2
                        goto L_0x0063
                    L_0x01e5:
                        java.lang.String r1 = r0.trim()     // Catch:{ NullPointerException -> 0x01f9 }
                        java.lang.String r3 = "android.intent.action.VIEW"
                        r2.setAction(r3)     // Catch:{ NullPointerException -> 0x01f9 }
                        android.net.Uri r1 = android.net.Uri.parse(r1)     // Catch:{ NullPointerException -> 0x01f9 }
                        r2.setData(r1)     // Catch:{ NullPointerException -> 0x01f9 }
                        r1 = r0
                        r0 = r2
                        goto L_0x0063
                    L_0x01f9:
                        r1 = move-exception
                        r1.printStackTrace()
                        r1 = r0
                        r0 = r2
                        goto L_0x0063
                    L_0x0201:
                        java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0220 }
                        java.lang.String r3 = "tel:"
                        r1.<init>(r3)     // Catch:{ Exception -> 0x0220 }
                        java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ Exception -> 0x0220 }
                        java.lang.String r0 = r1.toString()     // Catch:{ Exception -> 0x0220 }
                        java.lang.String r1 = "android.intent.action.DIAL"
                        r2.setAction(r1)     // Catch:{ Exception -> 0x034a }
                        android.net.Uri r1 = android.net.Uri.parse(r0)     // Catch:{ Exception -> 0x034a }
                        r2.setData(r1)     // Catch:{ Exception -> 0x034a }
                        r1 = r0
                        r0 = r2
                        goto L_0x0063
                    L_0x0220:
                        r1 = move-exception
                        r12 = r1
                        r1 = r0
                        r0 = r12
                    L_0x0224:
                        r0.printStackTrace()
                        r0 = r2
                        goto L_0x0063
                    L_0x022a:
                        java.lang.String r1 = "|"
                        boolean r1 = r0.contains(r11)
                        if (r1 == 0) goto L_0x027b
                        java.lang.String r1 = "|"
                        int r1 = r0.indexOf(r11)
                        java.lang.String r3 = r0.substring(r10, r1)
                        int r1 = r1 + 1
                        java.lang.String r1 = r0.substring(r1)
                        r12 = r3
                        r3 = r1
                        r1 = r12
                    L_0x0245:
                        java.lang.String r4 = "android.intent.action.VIEW"
                        r2.setAction(r4)     // Catch:{ Exception -> 0x0280 }
                        java.lang.String r4 = "com.google.android.apps.maps"
                        java.lang.String r5 = "com.google.android.maps.MapsActivity"
                        r2.setClassName(r4, r5)     // Catch:{ Exception -> 0x0280 }
                        java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0280 }
                        java.lang.String r5 = "http://maps.google.com/maps?q="
                        r4.<init>(r5)     // Catch:{ Exception -> 0x0280 }
                        java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ Exception -> 0x0280 }
                        java.lang.String r4 = "("
                        java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0280 }
                        java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0280 }
                        java.lang.String r3 = ")&z=22"
                        java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0280 }
                        java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0280 }
                        android.net.Uri r1 = android.net.Uri.parse(r1)     // Catch:{ Exception -> 0x0280 }
                        r2.setData(r1)     // Catch:{ Exception -> 0x0280 }
                        r1 = r0
                        r0 = r2
                        goto L_0x0063
                    L_0x027b:
                        java.lang.String r1 = "I am here"
                        r3 = r1
                        r1 = r0
                        goto L_0x0245
                    L_0x0280:
                        r1 = move-exception
                        r1.printStackTrace()
                        r1 = r0
                        r0 = r2
                        goto L_0x0063
                    L_0x0288:
                        android.content.Intent r1 = new android.content.Intent     // Catch:{ Exception -> 0x0299 }
                        java.lang.String r3 = "android.intent.action.WEB_SEARCH"
                        r1.<init>(r3)     // Catch:{ Exception -> 0x0299 }
                        java.lang.String r2 = "query"
                        r1.putExtra(r2, r0)     // Catch:{ Exception -> 0x0344 }
                        r12 = r1
                        r1 = r0
                        r0 = r12
                        goto L_0x0063
                    L_0x0299:
                        r1 = move-exception
                    L_0x029a:
                        r1.printStackTrace()
                        r1 = r0
                        r0 = r2
                        goto L_0x0063
                    L_0x02a1:
                        java.lang.String r1 = "|"
                        boolean r1 = r0.contains(r11)
                        if (r1 == 0) goto L_0x0362
                        java.lang.String r1 = "|"
                        int r1 = r0.indexOf(r11)
                        java.lang.String r3 = r0.substring(r10, r1)
                        int r1 = r1 + 1
                        java.lang.String r1 = r0.substring(r1)
                    L_0x02b9:
                        java.lang.String r4 = "android.intent.action.SENDTO"
                        r2.setAction(r4)     // Catch:{ Exception -> 0x02dd }
                        java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02dd }
                        java.lang.String r5 = "smsto:"
                        r4.<init>(r5)     // Catch:{ Exception -> 0x02dd }
                        java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x02dd }
                        java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x02dd }
                        android.net.Uri r3 = android.net.Uri.parse(r3)     // Catch:{ Exception -> 0x02dd }
                        r2.setData(r3)     // Catch:{ Exception -> 0x02dd }
                        java.lang.String r3 = "sms_body"
                        r2.putExtra(r3, r1)     // Catch:{ Exception -> 0x02dd }
                        r1 = r0
                        r0 = r2
                        goto L_0x0063
                    L_0x02dd:
                        r1 = move-exception
                        r1.printStackTrace()
                        r1 = r0
                        r0 = r2
                        goto L_0x0063
                    L_0x02e5:
                        java.lang.String r1 = "android.intent.action.SEND"
                        r2.setAction(r1)     // Catch:{ Exception -> 0x0327 }
                        java.lang.String r1 = "|"
                        boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x0327 }
                        if (r1 == 0) goto L_0x035e
                        java.lang.String[] r1 = com.wooboo.adlib_android.c.a(r0)     // Catch:{ Exception -> 0x0327 }
                        r3 = 0
                        r3 = r1[r3]     // Catch:{ Exception -> 0x0327 }
                        r4 = 1
                        r4 = r1[r4]     // Catch:{ Exception -> 0x0327 }
                        r5 = 2
                        r1 = r1[r5]     // Catch:{ Exception -> 0x0327 }
                        r12 = r4
                        r4 = r1
                        r1 = r12
                    L_0x0302:
                        android.net.Uri r5 = android.net.Uri.parse(r3)     // Catch:{ Exception -> 0x0327 }
                        r2.setData(r5)     // Catch:{ Exception -> 0x0327 }
                        r5 = 1
                        java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ Exception -> 0x0327 }
                        r6 = 0
                        r5[r6] = r3     // Catch:{ Exception -> 0x0327 }
                        java.lang.String r3 = "android.intent.extra.EMAIL"
                        r2.putExtra(r3, r5)     // Catch:{ Exception -> 0x0327 }
                        java.lang.String r3 = "android.intent.extra.TEXT"
                        r2.putExtra(r3, r4)     // Catch:{ Exception -> 0x0327 }
                        java.lang.String r3 = "android.intent.extra.SUBJECT"
                        r2.putExtra(r3, r1)     // Catch:{ Exception -> 0x0327 }
                        java.lang.String r1 = "message/rfc882"
                        r2.setType(r1)     // Catch:{ Exception -> 0x0327 }
                        r1 = r0
                        r0 = r2
                        goto L_0x0063
                    L_0x0327:
                        r1 = move-exception
                        r1.printStackTrace()
                        goto L_0x0061
                    L_0x032d:
                        r0 = move-exception
                        java.lang.String r2 = "Wooboo SDK"
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder
                        java.lang.String r3 = "Could not intent to "
                        r2.<init>(r3)
                        java.lang.StringBuilder r1 = r2.append(r1)
                        java.lang.String r1 = r1.toString()
                        android.util.Log.e(r8, r1, r0)
                        goto L_0x0071
                    L_0x0344:
                        r2 = move-exception
                        r12 = r2
                        r2 = r1
                        r1 = r12
                        goto L_0x029a
                    L_0x034a:
                        r1 = move-exception
                        r12 = r1
                        r1 = r0
                        r0 = r12
                        goto L_0x0224
                    L_0x0350:
                        r1 = move-exception
                        goto L_0x0194
                    L_0x0353:
                        r3 = move-exception
                        goto L_0x0158
                    L_0x0356:
                        r0 = move-exception
                        r0 = r7
                        goto L_0x0107
                    L_0x035a:
                        r0 = move-exception
                        r0 = r7
                        goto L_0x00b9
                    L_0x035e:
                        r1 = r7
                        r3 = r7
                        r4 = r7
                        goto L_0x0302
                    L_0x0362:
                        r1 = r7
                        r3 = r7
                        goto L_0x02b9
                    L_0x0366:
                        r0 = r7
                        goto L_0x0057
                    L_0x0369:
                        r3 = r7
                        goto L_0x0143
                    L_0x036c:
                        r0 = r7
                        goto L_0x0047
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.wooboo.adlib_android.c.AnonymousClass1.run():void");
                }
            }.start();
            ImpressionAdView.close();
        }
    }

    static /* synthetic */ String[] a(String str) {
        int indexOf = str.indexOf("|");
        String substring = str.substring(indexOf + 1);
        int indexOf2 = substring.indexOf("|");
        return new String[]{str.substring(0, indexOf), substring.substring(0, indexOf2), substring.substring(indexOf2 + 1)};
    }

    public final void a(a aVar) {
        this.g = aVar;
    }

    public final String b() {
        return this.c;
    }

    public final String c() {
        return this.e;
    }

    public final String toString() {
        return this.c;
    }

    public final boolean equals(Object obj) {
        if (obj instanceof c) {
            return toString().equals(((c) obj).toString());
        }
        return false;
    }

    public final int hashCode() {
        return toString().hashCode();
    }
}
