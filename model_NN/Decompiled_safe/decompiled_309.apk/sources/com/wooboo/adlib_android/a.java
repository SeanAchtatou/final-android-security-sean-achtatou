package com.wooboo.adlib_android;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.wooboo.adlib_android.c;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import net.youmi.android.AdView;

final class a extends RelativeLayout implements Animation.AnimationListener, c.a {
    private static final Typeface a = Typeface.create(Typeface.SANS_SERIF, 1);
    private static final Typeface b = Typeface.create(Typeface.SANS_SERIF, 0);
    private int c = AdView.DEFAULT_BACKGROUND_COLOR;
    private int d = -1;
    private Drawable e;
    private Drawable f;
    private Drawable g;
    private Drawable h;
    /* access modifiers changed from: private */
    public c i;
    private TextView j;
    private TextView k;
    /* access modifiers changed from: private */
    public f l = null;
    private ImageView m = null;
    /* access modifiers changed from: private */
    public ProgressBar n;
    /* access modifiers changed from: private */
    public boolean o;
    private boolean p;
    private i q;

    private synchronized byte[] a(String str) {
        byte[] bArr;
        int i2 = 0;
        synchronized (this) {
            String lowerCase = str.substring(str.lastIndexOf("/") + 1, str.lastIndexOf(".")).toLowerCase();
            try {
                try {
                    HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
                    httpURLConnection.setDoInput(true);
                    httpURLConnection.connect();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    int contentLength = httpURLConnection.getContentLength();
                    if (contentLength != -1) {
                        bArr = new byte[contentLength];
                        byte[] bArr2 = new byte[512];
                        while (true) {
                            int read = inputStream.read(bArr2);
                            if (read <= 0) {
                                break;
                            }
                            System.arraycopy(bArr2, 0, bArr, i2, read);
                            i2 += read;
                        }
                        this.q.a(lowerCase, bArr);
                    } else {
                        bArr = null;
                    }
                } catch (IOException e2) {
                    bArr = null;
                }
            } catch (MalformedURLException e3) {
                bArr = null;
            }
        }
        return bArr;
    }

    public final void c() {
        g();
        if (!(this.l == null || this.l.getBackground() == null)) {
            this.l.getBackground().setCallback(null);
            this.l.setBackgroundDrawable(null);
        }
        if (this.m != null && this.m.getBackground() != null) {
            this.m.getBackground().setCallback(null);
            this.m.setBackgroundDrawable(null);
        }
    }

    private void g() {
        if (this.e != null) {
            ((BitmapDrawable) this.e).getBitmap().recycle();
        }
        if (this.f != null) {
            ((BitmapDrawable) this.f).getBitmap().recycle();
        }
        if (this.g != null) {
            ((BitmapDrawable) this.g).getBitmap().recycle();
        }
        this.e = null;
        this.g = null;
        this.f = null;
        if (this.h != null) {
            ((BitmapDrawable) this.h).getBitmap().recycle();
        }
        this.h = null;
    }

    public final void d() {
        if (this.l != null) {
            this.l.a();
        }
    }

    public final void e() {
        if (this.l != null) {
            this.l.b();
            if (this.l.getBackground() != null) {
                this.l.getBackground().setCallback(null);
                this.l.setBackgroundDrawable(null);
            }
            this.l = null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x00e6  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0184  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x01c5  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x020d A[SYNTHETIC, Splitter:B:62:0x020d] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0222 A[SYNTHETIC, Splitter:B:71:0x0222] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x024a  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x029e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public a(com.wooboo.adlib_android.c r11, android.content.Context r12, boolean r13, int r14, int r15, double r16) {
        /*
            r10 = this;
            r10.<init>(r12)
            r0 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r10.c = r0
            r0 = -1
            r10.d = r0
            r0 = 0
            r10.l = r0
            r0 = 0
            r10.m = r0
            com.wooboo.adlib_android.i r0 = r10.q
            if (r0 != 0) goto L_0x001a
            com.wooboo.adlib_android.i r0 = com.wooboo.adlib_android.i.a(r12)
            r10.q = r0
        L_0x001a:
            r10.p = r13
            r10.i = r11
            r11.a(r10)
            r0 = 0
            r10.e = r0
            r0 = 0
            r10.g = r0
            r0 = 0
            r10.f = r0
            r0 = 0
            r10.n = r0
            r0 = 0
            r10.o = r0
            if (r11 == 0) goto L_0x01ca
            r0 = 1
            r10.setFocusable(r0)
            r0 = 1
            r10.setClickable(r0)
            r0 = 0
            java.lang.String r1 = r11.b()
            int r1 = r1.length()
            java.lang.String r2 = r11.c()     // Catch:{ Exception -> 0x02bb, all -> 0x02ad }
            if (r2 == 0) goto L_0x00dd
            r3 = 0
            int r4 = r2.length()     // Catch:{ Exception -> 0x02bb, all -> 0x02ad }
            if (r4 == 0) goto L_0x01e5
            java.lang.String r4 = "/"
            int r4 = r2.lastIndexOf(r4)     // Catch:{ Exception -> 0x02bb, all -> 0x02ad }
            int r4 = r4 + 1
            java.lang.String r5 = "."
            int r5 = r2.lastIndexOf(r5)     // Catch:{ Exception -> 0x02bb, all -> 0x02ad }
            java.lang.String r4 = r2.substring(r4, r5)     // Catch:{ Exception -> 0x02bb, all -> 0x02ad }
            java.lang.String r4 = r4.toLowerCase()     // Catch:{ Exception -> 0x02bb, all -> 0x02ad }
            com.wooboo.adlib_android.i r5 = r10.q     // Catch:{ Exception -> 0x02bb, all -> 0x02ad }
            boolean r5 = r5.a(r4)     // Catch:{ Exception -> 0x02bb, all -> 0x02ad }
            if (r5 == 0) goto L_0x01d4
            com.wooboo.adlib_android.i r3 = r10.q     // Catch:{ Exception -> 0x02bb, all -> 0x02ad }
            byte[] r3 = r3.b(r4)     // Catch:{ Exception -> 0x02bb, all -> 0x02ad }
            java.io.ByteArrayInputStream r4 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x02bb, all -> 0x02ad }
            r4.<init>(r3)     // Catch:{ Exception -> 0x02bb, all -> 0x02ad }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r4)     // Catch:{ Exception -> 0x02be, all -> 0x02b3 }
            r3 = r4
        L_0x007e:
            if (r0 != 0) goto L_0x0092
            android.content.Context r0 = r10.getContext()     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            android.content.res.AssetManager r0 = r0.getAssets()     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            java.lang.String r4 = "wooboo_logo.png"
            java.io.InputStream r0 = r0.open(r4)     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ Exception -> 0x0209, all -> 0x021e }
        L_0x0092:
            int r4 = r0.getWidth()     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            int r5 = r0.getHeight()     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            double r6 = (double) r4     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            double r6 = r6 * r16
            int r4 = (int) r6     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            double r5 = (double) r5     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            double r5 = r5 * r16
            int r5 = (int) r5     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            android.widget.RelativeLayout$LayoutParams r6 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            r6.<init>(r4, r5)     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            if (r1 > 0) goto L_0x01fc
            r4 = 0
            r5 = 0
            r7 = 0
            r8 = 0
            r6.setMargins(r4, r5, r7, r8)     // Catch:{ Exception -> 0x0209, all -> 0x021e }
        L_0x00b0:
            r4 = 9
            r6.addRule(r4)     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            java.lang.String r4 = ".gif"
            boolean r4 = r2.contains(r4)     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            if (r4 != 0) goto L_0x00c5
            java.lang.String r4 = ".GIF"
            boolean r2 = r2.contains(r4)     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            if (r2 == 0) goto L_0x0226
        L_0x00c5:
            com.wooboo.adlib_android.f r0 = new com.wooboo.adlib_android.f     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            r0.<init>(r12, r3)     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            r10.l = r0     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            com.wooboo.adlib_android.f r0 = r10.l     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            r0.setLayoutParams(r6)     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            com.wooboo.adlib_android.f r0 = r10.l     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            r2 = 1
            r0.setId(r2)     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            com.wooboo.adlib_android.f r0 = r10.l     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            r10.addView(r0)     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            r0 = r3
        L_0x00dd:
            if (r0 == 0) goto L_0x00e2
            r0.close()     // Catch:{ IOException -> 0x02aa }
        L_0x00e2:
            r0 = 15
            if (r1 <= r0) goto L_0x024a
            android.widget.TextView r0 = new android.widget.TextView
            r0.<init>(r12)
            r10.j = r0
            android.widget.TextView r0 = r10.j
            java.lang.String r2 = r11.b()
            r0.setText(r2)
            android.widget.TextView r0 = r10.j
            android.graphics.Typeface r2 = com.wooboo.adlib_android.a.a
            r0.setTypeface(r2)
            android.widget.TextView r0 = r10.j
            int r2 = r10.d
            r0.setTextColor(r2)
            android.widget.TextView r0 = r10.j
            r2 = 1096810496(0x41600000, float:14.0)
            r0.setTextSize(r2)
            android.widget.TextView r0 = r10.j
            android.text.TextUtils$TruncateAt r2 = android.text.TextUtils.TruncateAt.END
            r0.setEllipsize(r2)
            android.widget.TextView r0 = r10.j
            r2 = 1
            r0.setSingleLine(r2)
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            r2 = -1
            r3 = -1
            r0.<init>(r2, r3)
            android.widget.ImageView r2 = r10.m
            if (r2 == 0) goto L_0x0128
            r2 = 1
            r3 = 1
            r0.addRule(r2, r3)
        L_0x0128:
            r2 = 6
            r3 = 6
            r4 = 0
            r5 = 0
            r0.setMargins(r2, r3, r4, r5)
            android.widget.TextView r2 = r10.j
            r2.setLayoutParams(r0)
            android.widget.TextView r0 = r10.j
            r2 = 2
            r0.setId(r2)
            android.widget.TextView r0 = r10.j
            r10.addView(r0)
        L_0x013f:
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            r2 = 44
            r3 = 44
            r0.<init>(r2, r3)
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r0.setMargins(r2, r3, r4, r5)
            android.widget.ProgressBar r2 = new android.widget.ProgressBar
            r2.<init>(r12)
            r10.n = r2
            android.widget.ProgressBar r2 = r10.n
            r3 = 1
            r2.setIndeterminate(r3)
            android.widget.ProgressBar r2 = r10.n
            r2.setLayoutParams(r0)
            android.widget.ProgressBar r0 = r10.n
            r2 = 4
            r0.setVisibility(r2)
            android.widget.ProgressBar r0 = r10.n
            r10.addView(r0)
            android.widget.TextView r0 = new android.widget.TextView
            r0.<init>(r12)
            r10.k = r0
            android.widget.TextView r0 = r10.k
            r2 = 5
            r0.setGravity(r2)
            android.widget.TextView r0 = r10.k
            android.graphics.Typeface r2 = com.wooboo.adlib_android.a.b
            r0.setTypeface(r2)
            boolean r0 = r10.p
            if (r0 == 0) goto L_0x029e
            android.widget.TextView r0 = r10.k
            int r2 = com.wooboo.adlib_android.ImpressionAdView.getTextColor()
            r0.setTextColor(r2)
        L_0x018d:
            android.widget.TextView r0 = r10.k
            r2 = 1093664768(0x41300000, float:11.0)
            r0.setTextSize(r2)
            android.widget.TextView r0 = r10.k
            java.lang.String r2 = "哇棒传媒"
            r0.setText(r2)
            android.widget.TextView r0 = r10.k
            r2 = 3
            r0.setId(r2)
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            r2 = -2
            r3 = -2
            r0.<init>(r2, r3)
            r2 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r2 = r2 * r16
            int r2 = (int) r2
            int r2 = r2 + 2
            r3 = 0
            int r2 = r2 * 6
            int r2 = r15 - r2
            r4 = 6
            r5 = 6
            r0.setMargins(r3, r2, r4, r5)
            r2 = 11
            r0.addRule(r2)
            android.widget.TextView r2 = r10.k
            r2.setLayoutParams(r0)
            if (r1 <= 0) goto L_0x01ca
            android.widget.TextView r0 = r10.k
            r10.addView(r0)
        L_0x01ca:
            r0 = -1
            r10.a(r0)
            r0 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r10.setBackgroundColor(r0)
            return
        L_0x01d4:
            byte[] r4 = r10.a(r2)     // Catch:{ Exception -> 0x02bb, all -> 0x02ad }
            if (r4 == 0) goto L_0x02c6
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x02bb, all -> 0x02ad }
            r3.<init>(r4)     // Catch:{ Exception -> 0x02bb, all -> 0x02ad }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r3)     // Catch:{ Exception -> 0x02c2, all -> 0x02b7 }
            goto L_0x007e
        L_0x01e5:
            android.content.Context r3 = r10.getContext()     // Catch:{ Exception -> 0x02bb, all -> 0x02ad }
            android.content.res.AssetManager r3 = r3.getAssets()     // Catch:{ Exception -> 0x02bb, all -> 0x02ad }
            java.lang.String r4 = "wooboo_logo.png"
            java.io.InputStream r3 = r3.open(r4)     // Catch:{ Exception -> 0x02bb, all -> 0x02ad }
            android.graphics.Bitmap r3 = android.graphics.BitmapFactory.decodeStream(r3)     // Catch:{ Exception -> 0x02bb, all -> 0x02ad }
            r9 = r3
            r3 = r0
            r0 = r9
            goto L_0x007e
        L_0x01fc:
            boolean r4 = r10.p     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            if (r4 == 0) goto L_0x0215
            r4 = 1
            r5 = 1
            r7 = 0
            r8 = 2
            r6.setMargins(r4, r5, r7, r8)     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            goto L_0x00b0
        L_0x0209:
            r0 = move-exception
            r0 = r3
        L_0x020b:
            if (r0 == 0) goto L_0x00e2
            r0.close()     // Catch:{ IOException -> 0x0212 }
            goto L_0x00e2
        L_0x0212:
            r0 = move-exception
            goto L_0x00e2
        L_0x0215:
            r4 = 4
            r5 = 4
            r7 = 0
            r8 = 4
            r6.setMargins(r4, r5, r7, r8)     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            goto L_0x00b0
        L_0x021e:
            r0 = move-exception
            r1 = r3
        L_0x0220:
            if (r1 == 0) goto L_0x0225
            r1.close()     // Catch:{ IOException -> 0x02a7 }
        L_0x0225:
            throw r0
        L_0x0226:
            android.widget.ImageView r2 = new android.widget.ImageView     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            r2.<init>(r12)     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            r10.m = r2     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            android.widget.ImageView r2 = r10.m     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            r2.setLayoutParams(r6)     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            android.widget.ImageView r2 = r10.m     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            android.graphics.drawable.BitmapDrawable r4 = new android.graphics.drawable.BitmapDrawable     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            r4.<init>(r0)     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            r2.setBackgroundDrawable(r4)     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            android.widget.ImageView r0 = r10.m     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            r2 = 1
            r0.setId(r2)     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            android.widget.ImageView r0 = r10.m     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            r10.addView(r0)     // Catch:{ Exception -> 0x0209, all -> 0x021e }
            r0 = r3
            goto L_0x00dd
        L_0x024a:
            android.widget.TextView r0 = new android.widget.TextView
            r0.<init>(r12)
            r10.j = r0
            android.widget.TextView r0 = r10.j
            java.lang.String r2 = r11.b()
            r0.setText(r2)
            android.widget.TextView r0 = r10.j
            android.graphics.Typeface r2 = com.wooboo.adlib_android.a.a
            r0.setTypeface(r2)
            android.widget.TextView r0 = r10.j
            int r2 = r10.d
            r0.setTextColor(r2)
            android.widget.TextView r0 = r10.j
            r2 = 1098907648(0x41800000, float:16.0)
            r0.setTextSize(r2)
            android.widget.TextView r0 = r10.j
            r2 = 1
            r0.setSingleLine(r2)
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            r2 = -2
            r3 = -2
            r0.<init>(r2, r3)
            android.widget.ImageView r2 = r10.m
            if (r2 == 0) goto L_0x0285
            r2 = 1
            r3 = 1
            r0.addRule(r2, r3)
        L_0x0285:
            r2 = 6
            r3 = 6
            r4 = 0
            r5 = 0
            r0.setMargins(r2, r3, r4, r5)
            android.widget.TextView r2 = r10.j
            r2.setLayoutParams(r0)
            android.widget.TextView r0 = r10.j
            r2 = 2
            r0.setId(r2)
            android.widget.TextView r0 = r10.j
            r10.addView(r0)
            goto L_0x013f
        L_0x029e:
            android.widget.TextView r0 = r10.k
            int r2 = r10.d
            r0.setTextColor(r2)
            goto L_0x018d
        L_0x02a7:
            r1 = move-exception
            goto L_0x0225
        L_0x02aa:
            r0 = move-exception
            goto L_0x00e2
        L_0x02ad:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0220
        L_0x02b3:
            r0 = move-exception
            r1 = r4
            goto L_0x0220
        L_0x02b7:
            r0 = move-exception
            r1 = r3
            goto L_0x0220
        L_0x02bb:
            r2 = move-exception
            goto L_0x020b
        L_0x02be:
            r0 = move-exception
            r0 = r4
            goto L_0x020b
        L_0x02c2:
            r0 = move-exception
            r0 = r3
            goto L_0x020b
        L_0x02c6:
            r9 = r3
            r3 = r0
            r0 = r9
            goto L_0x007e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wooboo.adlib_android.a.<init>(com.wooboo.adlib_android.c, android.content.Context, boolean, int, int, double):void");
    }

    public final void a(int i2) {
        this.d = -16777216 | i2;
        this.j.setTextColor(this.d);
        if (!this.p) {
            this.k.setTextColor(this.d);
        }
        postInvalidate();
    }

    public final void setBackgroundColor(int i2) {
        this.c = -16777216 | i2;
    }

    /* access modifiers changed from: protected */
    public final c f() {
        return this.i;
    }

    /* access modifiers changed from: protected */
    public final void onSizeChanged(int i2, int i3, int i4, int i5) {
        int i6;
        super.onSizeChanged(i2, i3, i4, i5);
        if (!this.p) {
            i6 = this.k.getVisibility();
        } else {
            i6 = 0;
        }
        if (i6 == 0) {
            Typeface typeface = this.j.getTypeface();
            String b2 = this.i.b();
            if (b2 != null) {
                Paint paint = new Paint();
                paint.setTypeface(typeface);
                paint.setTextSize(this.j.getTextSize());
                if (paint.measureText(b2) > ((float) i2)) {
                    i6 = 8;
                }
            }
        }
        this.k.setVisibility(0);
        if (!this.p) {
            this.k.setVisibility(i6);
        }
        if (i2 != 0 && i3 != 0 && !this.p) {
            Rect rect = new Rect(0, 0, i2, i3);
            g();
            this.e = a(rect, -1, this.c);
            this.g = a(rect, -1147097, -19456);
            this.f = a(rect, -1, this.c, true);
            setBackgroundDrawable(this.e);
        }
    }

    private Drawable a(Rect rect, int i2, int i3) {
        return a(rect, i2, i3, false);
    }

    private Drawable a(Rect rect, int i2, int i3, boolean z) {
        Bitmap createBitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        if (!this.p) {
            Paint paint = new Paint();
            paint.setColor(i2);
            paint.setAntiAlias(true);
            canvas.drawRect(rect, paint);
            GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.argb(127, Color.red(i3), Color.green(i3), Color.blue(i3)), i3});
            int height = ((int) (((double) rect.height()) * 0.4375d)) + rect.top;
            gradientDrawable.setBounds(rect.left, rect.top, rect.right, height);
            gradientDrawable.draw(canvas);
            Rect rect2 = new Rect(rect.left, height, rect.right, rect.bottom);
            Paint paint2 = new Paint();
            paint2.setColor(i3);
            canvas.drawRect(rect2, paint2);
        }
        if (z) {
            Paint paint3 = new Paint();
            paint3.setAntiAlias(true);
            paint3.setColor(-1147097);
            paint3.setStyle(Paint.Style.STROKE);
            paint3.setStrokeWidth(3.0f);
            paint3.setPathEffect(new CornerPathEffect(3.0f));
            Path path = new Path();
            path.addRoundRect(new RectF(rect), 3.0f, 3.0f, Path.Direction.CW);
            canvas.drawPath(path, paint3);
        }
        return new BitmapDrawable(createBitmap);
    }

    public final void b() {
        post(new Thread() {
            public final void run() {
                try {
                    a.this.n.setVisibility(4);
                    a.this.l.setVisibility(0);
                    a.this.o = false;
                } catch (Exception e) {
                }
            }
        });
    }

    public final void a() {
        post(new Thread() {
            public final void run() {
                try {
                    a.this.n.setVisibility(0);
                } catch (Exception e) {
                }
            }
        });
    }

    public final boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 66 || i2 == 23) {
            setPressed(true);
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public final boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (i2 == 66 || i2 == 23) {
            h();
        }
        setPressed(false);
        return super.onKeyUp(i2, keyEvent);
    }

    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            setPressed(true);
        } else if (action == 2) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            int left = getLeft();
            int top = getTop();
            int right = getRight();
            int bottom = getBottom();
            if (x < ((float) left) || x > ((float) right) || y < ((float) top) || y > ((float) bottom)) {
                setPressed(false);
            } else {
                setPressed(true);
            }
        } else if (action == 1) {
            if (isPressed()) {
                h();
            }
            setPressed(false);
        } else if (action == 3) {
            setPressed(false);
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public final boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            setPressed(true);
        } else if (motionEvent.getAction() == 1) {
            if (hasFocus()) {
                h();
            }
            setPressed(false);
        }
        return super.onTrackballEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public final void onFocusChanged(boolean z, int i2, Rect rect) {
        super.onFocusChanged(z, i2, rect);
        if (this.p) {
            return;
        }
        if (z) {
            setBackgroundDrawable(this.f);
        } else {
            setBackgroundDrawable(this.e);
        }
    }

    public final void setPressed(boolean z) {
        Drawable drawable;
        if ((!z || !this.o) && isPressed() != z) {
            int i2 = this.d;
            if (z) {
                this.h = getBackground();
                drawable = this.g;
                i2 = -16777216;
            } else {
                drawable = this.h;
            }
            setBackgroundDrawable(drawable);
            if (this.j != null) {
                this.j.setTextColor(i2);
            }
            if (this.k != null) {
                this.k.setTextColor(i2);
            }
            super.setPressed(z);
            invalidate();
        }
    }

    private void h() {
        float f2;
        float f3;
        if (this.i != null && isPressed()) {
            setPressed(false);
            if (!this.o) {
                this.o = true;
                if (this.l == null && this.m == null) {
                    this.i.a();
                    this.o = false;
                    return;
                }
                AnimationSet animationSet = new AnimationSet(true);
                float f4 = 20.0f;
                float f5 = 20.0f;
                if (this.l != null) {
                    f4 = ((float) this.l.getWidth()) / 2.0f;
                    f5 = ((float) this.l.getHeight()) / 2.0f;
                    this.l.b();
                }
                float f6 = f5;
                float f7 = f4;
                float f8 = f6;
                if (this.m != null) {
                    f2 = ((float) this.m.getHeight()) / 2.0f;
                    f3 = ((float) this.m.getWidth()) / 2.0f;
                } else {
                    f2 = f8;
                    f3 = f7;
                }
                ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.2f, 1.0f, 1.2f, f3, f2);
                scaleAnimation.setDuration(200);
                animationSet.addAnimation(scaleAnimation);
                ScaleAnimation scaleAnimation2 = new ScaleAnimation(1.2f, 0.001f, 1.2f, 0.001f, f3, f2);
                scaleAnimation2.setDuration(299);
                scaleAnimation2.setStartOffset(200);
                scaleAnimation2.setAnimationListener(this);
                animationSet.addAnimation(scaleAnimation2);
                postDelayed(new Thread() {
                    public final void run() {
                        a.this.i.a();
                        a.this.o = false;
                    }
                }, 500);
                if (this.l != null) {
                    this.l.startAnimation(animationSet);
                }
                if (this.m != null) {
                    this.m.startAnimation(animationSet);
                }
            }
        }
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }

    public final void onAnimationEnd(Animation animation) {
    }
}
