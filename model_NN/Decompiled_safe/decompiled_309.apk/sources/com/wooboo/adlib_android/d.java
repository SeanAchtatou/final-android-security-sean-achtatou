package com.wooboo.adlib_android;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public final class d {
    protected static int a = 1;
    protected static int b = 2;
    protected static int c = 3;
    protected static int d = 4;
    protected static volatile int e = 1;
    private static TelephonyManager f;
    private static boolean g;
    private static String h;
    private static String i;
    private static int j;
    private static String k;
    private static String l = null;
    private static Context m = null;
    private static volatile boolean n = false;
    private static String o = null;
    private static String p = null;
    /* access modifiers changed from: private */
    public static Location q;
    /* access modifiers changed from: private */
    public static long r;
    private static int s = 3000;
    private static int t = -2;
    private static String u = null;
    private static int v = 0;

    protected static int a(Context context) {
        int i2;
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo == null) {
                return 1;
            }
            i2 = applicationInfo.metaData.getInt("Market_ID", 1);
            try {
                Log.d("Wooboo SDK", "The Market_ID is set to " + i2);
                return i2;
            } catch (Exception e2) {
            }
        } catch (Exception e3) {
            i2 = 1;
            Log.e("Wooboo SDK", "Could not read Market_ID meta-data from AndroidManifest.xml.");
            return i2;
        }
    }

    private static Location g(Context context) {
        String str;
        final LocationManager locationManager;
        boolean z;
        if (context != null && (q == null || System.currentTimeMillis() > r + 900000)) {
            synchronized (context) {
                if (q == null || System.currentTimeMillis() > r + 900000) {
                    r = System.currentTimeMillis();
                    if (context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                        locationManager = (LocationManager) context.getSystemService("location");
                        if (locationManager != null) {
                            Criteria criteria = new Criteria();
                            criteria.setAccuracy(2);
                            criteria.setCostAllowed(false);
                            str = locationManager.getBestProvider(criteria, true);
                            z = true;
                        } else {
                            str = null;
                            z = true;
                        }
                    } else {
                        str = null;
                        locationManager = null;
                        z = false;
                    }
                    if (str == null && context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
                        locationManager = (LocationManager) context.getSystemService("location");
                        if (locationManager != null) {
                            Criteria criteria2 = new Criteria();
                            criteria2.setAccuracy(1);
                            criteria2.setCostAllowed(false);
                            str = locationManager.getBestProvider(criteria2, true);
                            z = true;
                        } else {
                            z = true;
                        }
                    }
                    if (z && str != null) {
                        locationManager.requestLocationUpdates(str, 0, 0.0f, new LocationListener() {
                            public final void onLocationChanged(Location location) {
                                d.q = location;
                                d.r = System.currentTimeMillis();
                                locationManager.removeUpdates(this);
                            }

                            public final void onProviderDisabled(String str) {
                            }

                            public final void onProviderEnabled(String str) {
                            }

                            public final void onStatusChanged(String str, int i, Bundle bundle) {
                            }
                        }, context.getMainLooper());
                    }
                }
            }
        }
        return q;
    }

    protected static String b(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                return applicationInfo.metaData.getString("Wooboo_PID");
            }
            return null;
        } catch (Exception e2) {
            Log.e("Wooboo SDK", "Could not read Wooboo_PID meta-data from AndroidManifest.xml.");
            return null;
        }
    }

    protected static void a(String str) {
        p = str;
    }

    protected static void b(String str) {
        o = str;
    }

    protected static void a(int i2) {
        j = i2;
    }

    protected static void c(Context context) {
        String str;
        m = context;
        f = (TelephonyManager) context.getSystemService("phone");
        if (l == null) {
            if (f != null) {
                str = f.getSimSerialNumber();
                k = str;
            } else {
                str = null;
            }
            l = str;
        }
        if (l != null && u == null) {
            u = g(l);
        }
    }

    protected static void a() {
        try {
            AnonymousClass2 r0 = new Thread() {
                public final void run() {
                    d.b();
                }
            };
            if (!n) {
                n = true;
                r0.start();
            }
        } catch (Exception e2) {
        }
    }

    private static boolean f(String str) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setDoInput(true);
            httpURLConnection.setConnectTimeout(5000);
            httpURLConnection.setReadTimeout(5000);
            httpURLConnection.connect();
            InputStream inputStream = httpURLConnection.getInputStream();
            if (inputStream != null) {
                inputStream.close();
            }
            httpURLConnection.disconnect();
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    static /* synthetic */ void b() {
        if (!f("http://www.baidu.com/") && !f("http://162.105.131.113/") && !f("http://www.wooboo.com.cn")) {
            if (v == 5) {
                e = b;
                return;
            } else if (v == 11) {
                e = c;
                return;
            } else if (v == 12) {
                e = d;
                return;
            }
        }
        e = a;
    }

    protected static void c(String str) {
        Log.e("Wooboo SDK", str);
        throw new IllegalArgumentException(str);
    }

    protected static String d(Context context) {
        String language = context.getResources().getConfiguration().locale.getLanguage();
        if (language.contains("en")) {
            return "2";
        }
        if (language.contains("zh")) {
            return "0";
        }
        if (language.contains("ko")) {
            return "5";
        }
        if (language.contains("fr")) {
            return "3";
        }
        if (language.contains("es")) {
            return "8";
        }
        if (language.contains("de")) {
            return "6";
        }
        if (language.contains("it")) {
            return "7";
        }
        if (language.contains("ja")) {
            return "4";
        }
        if (language.contains("ru")) {
            return "9";
        }
        return "2";
    }

    protected static void d(String str) {
        if (str == null || str.length() != 32) {
            c("CONFIGURATION ERROR:  Incorrect Wooboo publisher ID.  Should 32 [a-z,0-9] characters:  " + h);
        }
        Log.d("Wooboo SDK", "Your Wooboo_PID is " + str);
        h = str;
    }

    protected static void a(boolean z) {
        g = z;
    }

    protected static void e(String str) {
        i = str;
    }

    protected static String e(Context context) {
        String string = Settings.System.getString(context.getContentResolver(), "android_id");
        if (string == null && f != null) {
            string = f.getDeviceId();
        }
        if (string == null) {
            return "00000000";
        }
        return string;
    }

    /* JADX WARNING: Removed duplicated region for block: B:70:0x0194 A[SYNTHETIC, Splitter:B:70:0x0194] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0199 A[Catch:{ Exception -> 0x026f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static com.wooboo.adlib_android.c f(android.content.Context r11) {
        /*
            r9 = -1
            r8 = 0
            java.lang.String r0 = "Wooboo SDK"
            java.lang.String r0 = "Could not get ad from Wooboo servers."
            java.lang.String r0 = "Content-Length"
            java.lang.String r0 = "android.permission.INTERNET"
            int r0 = r11.checkCallingOrSelfPermission(r0)
            if (r0 != r9) goto L_0x0015
            java.lang.String r0 = "Cannot request an ad without Internet permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.INTERNET\" />"
            c(r0)
        L_0x0015:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "pit"
            java.lang.StringBuilder r1 = r0.append(r1)
            java.lang.String r2 = "="
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "1"
            r1.append(r2)
            java.lang.String r1 = "4"
            java.lang.String r2 = "ifm"
            a(r0, r2, r1)
            int r1 = com.wooboo.adlib_android.d.t
            r2 = -2
            if (r1 != r2) goto L_0x0037
        L_0x0037:
            java.lang.String r1 = "mt"
            java.lang.String r2 = android.os.Build.MODEL
            a(r0, r1, r2)
            int r1 = com.wooboo.adlib_android.d.t
            if (r1 == r9) goto L_0x0056
            java.lang.String r1 = "mi"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            int r3 = com.wooboo.adlib_android.d.t
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            a(r0, r1, r2)
        L_0x0056:
            java.lang.String r1 = "bs"
            java.lang.String r2 = "7"
            a(r0, r1, r2)
            java.lang.String r1 = "pid"
            java.lang.String r2 = com.wooboo.adlib_android.d.h
            a(r0, r1, r2)
            java.lang.String r1 = "csdk"
            java.lang.String r2 = android.os.Build.VERSION.RELEASE
            a(r0, r1, r2)
            java.lang.String r1 = "sdk"
            java.lang.String r2 = "1.1"
            a(r0, r1, r2)
            java.lang.String r1 = "uid"
            java.lang.String r2 = com.wooboo.adlib_android.d.i
            a(r0, r1, r2)
            java.lang.String r1 = com.wooboo.adlib_android.d.l
            if (r1 == 0) goto L_0x0089
            java.lang.String r1 = com.wooboo.adlib_android.d.u
            if (r1 != 0) goto L_0x0089
            java.lang.String r1 = com.wooboo.adlib_android.d.l
            java.lang.String r1 = g(r1)
            com.wooboo.adlib_android.d.u = r1
        L_0x0089:
            java.lang.String r1 = com.wooboo.adlib_android.d.u
            if (r1 == 0) goto L_0x0098
            java.lang.String r1 = "&"
            java.lang.StringBuilder r1 = r0.append(r1)
            java.lang.String r2 = com.wooboo.adlib_android.d.u
            r1.append(r2)
        L_0x0098:
            java.lang.String r1 = "ml"
            java.lang.String r2 = com.wooboo.adlib_android.d.o
            a(r0, r1, r2)
            java.lang.String r1 = "pn"
            android.telephony.TelephonyManager r2 = com.wooboo.adlib_android.d.f
            if (r2 == 0) goto L_0x0130
            android.telephony.TelephonyManager r2 = com.wooboo.adlib_android.d.f
            java.lang.String r2 = r2.getLine1Number()
        L_0x00ab:
            a(r0, r1, r2)
            java.lang.String r1 = "apn"
            java.lang.String r2 = com.wooboo.adlib_android.d.p
            a(r0, r1, r2)
            java.lang.String r1 = "sw"
            a(r0, r1, r8)
            java.lang.String r1 = "loc"
            android.content.Context r2 = com.wooboo.adlib_android.d.m
            android.location.Location r2 = g(r2)
            if (r2 == 0) goto L_0x027f
            java.text.DecimalFormat r3 = new java.text.DecimalFormat
            java.lang.String r4 = "#0.00000"
            r3.<init>(r4)
            double r4 = r2.getLatitude()
            java.lang.String r4 = r3.format(r4)
            double r5 = r2.getLongitude()
            java.lang.String r2 = r3.format(r5)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = java.lang.String.valueOf(r4)
            r3.<init>(r4)
            java.lang.String r4 = "|"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r2 = r2.toString()
        L_0x00f2:
            a(r0, r1, r2)
            int r1 = com.wooboo.adlib_android.d.j
            java.lang.String r1 = java.lang.String.valueOf(r1)
            java.lang.String r2 = "mid"
            a(r0, r2, r1)
            java.lang.String r1 = r0.toString()
            java.lang.String r2 = "pit=1&pf=android"
            int r0 = com.wooboo.adlib_android.d.e     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            int r3 = com.wooboo.adlib_android.d.a     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            if (r0 != r3) goto L_0x0151
            boolean r0 = com.wooboo.adlib_android.d.g     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            if (r0 == 0) goto L_0x0133
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            java.lang.String r3 = "http://ade.wooboo.com.cn/t/test"
            r0.<init>(r3)     // Catch:{ Exception -> 0x013b, all -> 0x018f }
        L_0x0117:
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            int r3 = com.wooboo.adlib_android.d.s     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            r0.setConnectTimeout(r3)     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            int r3 = com.wooboo.adlib_android.d.s     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            r0.setReadTimeout(r3)     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
        L_0x0127:
            if (r0 != 0) goto L_0x01e2
            if (r0 == 0) goto L_0x012e
            r0.disconnect()     // Catch:{ Exception -> 0x026c }
        L_0x012e:
            r0 = r8
        L_0x012f:
            return r0
        L_0x0130:
            r2 = r8
            goto L_0x00ab
        L_0x0133:
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            java.lang.String r3 = "http://ade.wooboo.com.cn/a/p1"
            r0.<init>(r3)     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            goto L_0x0117
        L_0x013b:
            r0 = move-exception
            r0 = r8
            r1 = r8
        L_0x013e:
            java.lang.String r2 = "Wooboo SDK"
            java.lang.String r3 = "Could not get ad from Wooboo servers."
            android.util.Log.w(r2, r3)     // Catch:{ all -> 0x0272 }
            if (r1 == 0) goto L_0x014a
            r1.close()     // Catch:{ Exception -> 0x0264 }
        L_0x014a:
            if (r0 == 0) goto L_0x0279
            r0.disconnect()     // Catch:{ Exception -> 0x0264 }
            r0 = r8
            goto L_0x012f
        L_0x0151:
            int r0 = com.wooboo.adlib_android.d.e     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            int r3 = com.wooboo.adlib_android.d.b     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            if (r0 == r3) goto L_0x015d
            int r0 = com.wooboo.adlib_android.d.e     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            int r3 = com.wooboo.adlib_android.d.c     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            if (r0 != r3) goto L_0x019d
        L_0x015d:
            boolean r0 = com.wooboo.adlib_android.d.g     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            if (r0 == 0) goto L_0x0187
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            java.lang.String r3 = "http://10.0.0.172/t/test"
            r0.<init>(r3)     // Catch:{ Exception -> 0x013b, all -> 0x018f }
        L_0x0168:
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            java.lang.String r3 = "X-Online-Host"
            java.lang.String r4 = "ade.wooboo.com.cn"
            r0.setRequestProperty(r3, r4)     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            int r3 = com.wooboo.adlib_android.d.s     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            int r3 = r3 * 2
            r0.setConnectTimeout(r3)     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            int r3 = com.wooboo.adlib_android.d.s     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            int r3 = r3 * 2
            r0.setReadTimeout(r3)     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            goto L_0x0127
        L_0x0184:
            r1 = move-exception
            r1 = r8
            goto L_0x013e
        L_0x0187:
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            java.lang.String r3 = "http://10.0.0.172/a/p1"
            r0.<init>(r3)     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            goto L_0x0168
        L_0x018f:
            r0 = move-exception
            r1 = r8
            r2 = r8
        L_0x0192:
            if (r2 == 0) goto L_0x0197
            r2.close()     // Catch:{ Exception -> 0x026f }
        L_0x0197:
            if (r1 == 0) goto L_0x019c
            r1.disconnect()     // Catch:{ Exception -> 0x026f }
        L_0x019c:
            throw r0
        L_0x019d:
            int r0 = com.wooboo.adlib_android.d.e     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            int r3 = com.wooboo.adlib_android.d.d     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            if (r0 != r3) goto L_0x027c
            boolean r0 = com.wooboo.adlib_android.d.g     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            if (r0 == 0) goto L_0x01da
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            java.lang.String r3 = "http://ade.wooboo.com.cn/t/test"
            r0.<init>(r3)     // Catch:{ Exception -> 0x013b, all -> 0x018f }
        L_0x01ae:
            java.net.Proxy r3 = new java.net.Proxy     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            java.net.Proxy$Type r4 = java.net.Proxy.Type.HTTP     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            java.net.InetSocketAddress r5 = new java.net.InetSocketAddress     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            java.lang.String r6 = "10.0.0.200"
            r7 = 80
            r5.<init>(r6, r7)     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            r3.<init>(r4, r5)     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            java.net.URLConnection r0 = r0.openConnection(r3)     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            int r3 = com.wooboo.adlib_android.d.s     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            int r3 = r3 * 2
            r0.setConnectTimeout(r3)     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            int r3 = com.wooboo.adlib_android.d.s     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            int r3 = r3 * 2
            r0.setReadTimeout(r3)     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            goto L_0x0127
        L_0x01d4:
            r1 = move-exception
            r2 = r8
            r10 = r0
            r0 = r1
            r1 = r10
            goto L_0x0192
        L_0x01da:
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            java.lang.String r3 = "http://ade.wooboo.com.cn/a/p1"
            r0.<init>(r3)     // Catch:{ Exception -> 0x013b, all -> 0x018f }
            goto L_0x01ae
        L_0x01e2:
            java.lang.String r3 = "POST"
            r0.setRequestMethod(r3)     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            r3 = 1
            r0.setDoOutput(r3)     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            java.lang.String r3 = "Content-Type"
            java.lang.String r4 = "application/x-www-form-urlencoded"
            r0.setRequestProperty(r3, r4)     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            boolean r3 = com.wooboo.adlib_android.d.g     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            if (r3 == 0) goto L_0x0242
            java.lang.String r3 = "Content-Length"
            int r4 = r2.length()     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            r0.setRequestProperty(r3, r4)     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
        L_0x0203:
            java.io.OutputStream r3 = r0.getOutputStream()     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            java.io.BufferedWriter r4 = new java.io.BufferedWriter     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            java.io.OutputStreamWriter r5 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            r5.<init>(r3)     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            boolean r3 = com.wooboo.adlib_android.d.g     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            if (r3 == 0) goto L_0x0250
            r4.write(r2)     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
        L_0x0218:
            r4.close()     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            java.io.InputStream r1 = r0.getInputStream()     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0258 }
            r2.<init>()     // Catch:{ Exception -> 0x0258 }
        L_0x0224:
            int r3 = r1.read()     // Catch:{ Exception -> 0x0258 }
            if (r3 != r9) goto L_0x0254
            byte[] r2 = r2.toByteArray()     // Catch:{ Exception -> 0x0258 }
            int r3 = r2.length     // Catch:{ Exception -> 0x0258 }
            if (r3 <= 0) goto L_0x025b
            com.wooboo.adlib_android.c r2 = com.wooboo.adlib_android.c.a(r11, r2)     // Catch:{ Exception -> 0x0258 }
        L_0x0235:
            if (r1 == 0) goto L_0x023a
            r1.close()     // Catch:{ Exception -> 0x0268 }
        L_0x023a:
            if (r0 == 0) goto L_0x0269
            r0.disconnect()     // Catch:{ Exception -> 0x0268 }
            r0 = r2
            goto L_0x012f
        L_0x0242:
            java.lang.String r3 = "Content-Length"
            int r4 = r1.length()     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            r0.setRequestProperty(r3, r4)     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            goto L_0x0203
        L_0x0250:
            r4.write(r1)     // Catch:{ Exception -> 0x0184, all -> 0x01d4 }
            goto L_0x0218
        L_0x0254:
            r2.write(r3)     // Catch:{ Exception -> 0x0258 }
            goto L_0x0224
        L_0x0258:
            r2 = move-exception
            goto L_0x013e
        L_0x025b:
            java.lang.String r2 = "Wooboo SDK"
            java.lang.String r3 = "Could not get ad from Wooboo servers."
            android.util.Log.w(r2, r3)     // Catch:{ Exception -> 0x0258 }
            r2 = r8
            goto L_0x0235
        L_0x0264:
            r0 = move-exception
            r0 = r8
            goto L_0x012f
        L_0x0268:
            r0 = move-exception
        L_0x0269:
            r0 = r2
            goto L_0x012f
        L_0x026c:
            r0 = move-exception
            goto L_0x012e
        L_0x026f:
            r1 = move-exception
            goto L_0x019c
        L_0x0272:
            r2 = move-exception
            r10 = r2
            r2 = r1
            r1 = r0
            r0 = r10
            goto L_0x0192
        L_0x0279:
            r0 = r8
            goto L_0x012f
        L_0x027c:
            r0 = r8
            goto L_0x0127
        L_0x027f:
            r2 = r8
            goto L_0x00f2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wooboo.adlib_android.d.f(android.content.Context):com.wooboo.adlib_android.c");
    }

    protected static void b(int i2) {
        t = i2;
    }

    private static void a(StringBuilder sb, String str, String str2) {
        if (str2 != null && str2.length() > 0) {
            try {
                sb.append("&").append(URLEncoder.encode(str, "UTF-8")).append("=").append(URLEncoder.encode(str2, "UTF-8"));
            } catch (UnsupportedEncodingException e2) {
                Log.e("Wooboo SDK", "UTF-8 encoding is not supported on this device.  Ad requests are impossible.", e2);
            }
        }
    }

    private static String g(String str) {
        int length = str.length();
        StringBuilder sb = new StringBuilder();
        if (length >= 6) {
            String substring = str.substring(4, 6);
            if (substring.endsWith("0") || substring.endsWith("2") || substring.endsWith("7")) {
                sb.append("on").append("=").append(5);
                v = 5;
                if (length >= 7) {
                    String substring2 = str.substring(6, 7);
                    if (substring2.equals("0")) {
                        a(sb, "so", "0");
                    } else if ("1".equals(substring2)) {
                        a(sb, "so", "1");
                    } else if ("2".equals(substring2)) {
                        a(sb, "so", "2");
                    } else if ("3".equals(substring2)) {
                        a(sb, "so", "3");
                    } else if ("4".equals(substring2)) {
                        a(sb, "so", "4");
                    } else if ("5".equals(substring2)) {
                        a(sb, "so", "5");
                    } else if ("6".equals(substring2)) {
                        a(sb, "so", "6");
                    } else if ("7".equals(substring2)) {
                        a(sb, "so", "7");
                    } else if ("8".equals(substring2)) {
                        a(sb, "so", "8");
                    } else if ("9".equals(substring2)) {
                        a(sb, "so", "9");
                    } else if ("A".equals(substring2)) {
                        a(sb, "so", "10");
                    } else if ("B".equals(substring2)) {
                        a(sb, "so", "11");
                    } else if ("C".equals(substring2)) {
                        a(sb, "so", "12");
                    } else if ("D".equals(substring2)) {
                        a(sb, "so", "13");
                    } else if ("E".equals(substring2)) {
                        a(sb, "so", "14");
                    }
                    if (length >= 10) {
                        String substring3 = str.substring(8, 10);
                        if ("01".equals(substring3)) {
                            a(sb, "ac", "01");
                        } else if ("02".equals(substring3)) {
                            a(sb, "ac", "03");
                        } else if ("03".equals(substring3)) {
                            a(sb, "ac", "09");
                        } else if ("04".equals(substring3)) {
                            a(sb, "ac", "12");
                        } else if ("05".equals(substring3)) {
                            a(sb, "ac", "08");
                        } else if ("06".equals(substring3)) {
                            a(sb, "ac", "07");
                        } else if ("07".equals(substring3)) {
                            a(sb, "ac", "06");
                        } else if ("08".equals(substring3)) {
                            a(sb, "ac", "05");
                        } else if ("09".equals(substring3)) {
                            a(sb, "ac", "02");
                        } else if ("10".equals(substring3)) {
                            a(sb, "ac", "14");
                        } else if ("11".equals(substring3)) {
                            a(sb, "ac", "18");
                        } else if ("12".equals(substring3)) {
                            a(sb, "ac", "13");
                        } else if ("13".equals(substring3)) {
                            a(sb, "ac", "19");
                        } else if ("14".equals(substring3)) {
                            a(sb, "ac", "15");
                        } else if ("15".equals(substring3)) {
                            a(sb, "ac", "11");
                        } else if ("16".equals(substring3)) {
                            a(sb, "ac", "10");
                        } else if ("17".equals(substring3)) {
                            a(sb, "ac", "17");
                        } else if ("18".equals(substring3)) {
                            a(sb, "ac", "16");
                        } else if ("19".equals(substring3)) {
                            a(sb, "ac", "20");
                        } else if ("20".equals(substring3)) {
                            a(sb, "ac", "29");
                        } else if ("21".equals(substring3)) {
                            a(sb, "ac", "27");
                        } else if ("22".equals(substring3)) {
                            a(sb, "ac", "24");
                        } else if ("23".equals(substring3)) {
                            a(sb, "ac", "25");
                        } else if ("24".equals(substring3)) {
                            a(sb, "ac", "26");
                        } else if ("25".equals(substring3)) {
                            a(sb, "ac", "30");
                        } else if ("26".equals(substring3)) {
                            a(sb, "ac", "21");
                        } else if ("27".equals(substring3)) {
                            a(sb, "ac", "22");
                        } else if ("28".equals(substring3)) {
                            a(sb, "ac", "23");
                        } else if ("29".equals(substring3)) {
                            a(sb, "ac", "28");
                        } else if ("30".equals(substring3)) {
                            a(sb, "ac", "31");
                        } else if ("31".equals(substring3)) {
                            a(sb, "ac", "04");
                        }
                    }
                }
            } else if (substring.endsWith("1")) {
                sb.append("on").append("=").append(11);
                v = 11;
                if (length >= 9) {
                    String substring4 = str.substring(8, 9);
                    if ("0".equals(substring4)) {
                        a(sb, "so", "24");
                    } else if ("1".equals(substring4)) {
                        a(sb, "so", "15");
                    } else if ("2".equals(substring4)) {
                        a(sb, "so", "16");
                    } else if ("5".equals(substring4)) {
                        a(sb, "so", "19");
                    } else if ("6".equals(substring4)) {
                        a(sb, "so", "20");
                    }
                    if (length >= 13) {
                        String substring5 = str.substring(10, 13);
                        if (substring5.equals("010")) {
                            a(sb, "ac", "01");
                        } else if (substring5.equals("022")) {
                            a(sb, "ac", "03");
                        } else if (substring5.startsWith("31") || substring5.startsWith("33")) {
                            a(sb, "ac", "09");
                        } else if (substring5.startsWith("35") || substring5.startsWith("34")) {
                            a(sb, "ac", "12");
                        } else if (substring5.startsWith("47") || substring5.startsWith("48")) {
                            a(sb, "ac", "08");
                        } else if (substring5.equals("024") || substring5.startsWith("41") || substring5.startsWith("42")) {
                            a(sb, "ac", "07");
                        } else if (substring5.startsWith("43")) {
                            a(sb, "ac", "06");
                        } else if (substring5.startsWith("45") || substring5.startsWith("46")) {
                            a(sb, "ac", "05");
                        } else if (substring5.equals("021")) {
                            a(sb, "ac", "02");
                        } else if (substring5.equals("025") || substring5.startsWith("51") || substring5.startsWith("52")) {
                            a(sb, "ac", "14");
                        } else if (substring5.startsWith("57")) {
                            a(sb, "ac", "18");
                        } else if (substring5.startsWith("55") || substring5.startsWith("56")) {
                            a(sb, "ac", "13");
                        } else if (substring5.startsWith("59")) {
                            a(sb, "ac", "19");
                        } else if (substring5.startsWith("79") || substring5.startsWith("70")) {
                            a(sb, "ac", "15");
                        } else if (substring5.startsWith("53") || substring5.startsWith("54") || substring5.startsWith("63")) {
                            a(sb, "ac", "11");
                        } else if (substring5.startsWith("37") || substring5.startsWith("39")) {
                            a(sb, "ac", "10");
                        } else if (substring5.equals("027") || substring5.startsWith("71") || substring5.startsWith("72")) {
                            a(sb, "ac", "17");
                        } else if (substring5.startsWith("73") || substring5.startsWith("74")) {
                            a(sb, "ac", "16");
                        } else if (substring5.equals("020") || substring5.startsWith("75") || substring5.startsWith("76") || substring5.startsWith("66")) {
                            a(sb, "ac", "20");
                        } else if (substring5.startsWith("77")) {
                            a(sb, "ac", "29");
                        } else if (substring5.equals("898")) {
                            a(sb, "ac", "27");
                        } else if (substring5.equals("028") || substring5.startsWith("81") || substring5.startsWith("82") || substring5.startsWith("83")) {
                            a(sb, "ac", "24");
                        } else if (substring5.startsWith("85")) {
                            a(sb, "ac", "25");
                        } else if (substring5.startsWith("87") || substring5.startsWith("88") || substring5.startsWith("69")) {
                            a(sb, "ac", "26");
                        } else if (substring5.startsWith("89")) {
                            a(sb, "ac", "30");
                        } else if (substring5.equals("029") || substring5.startsWith("91")) {
                            a(sb, "ac", "21");
                        } else if (substring5.startsWith("93") || substring5.startsWith("94")) {
                            a(sb, "ac", "22");
                        } else if (substring5.startsWith("97")) {
                            a(sb, "ac", "23");
                        } else if (substring5.startsWith("95")) {
                            a(sb, "ac", "28");
                        } else if (substring5.startsWith("90") || substring5.startsWith("99")) {
                            a(sb, "ac", "31");
                        } else if (substring5.equals("023")) {
                            a(sb, "ac", "04");
                        }
                    }
                }
            } else if (substring.endsWith("3")) {
                sb.append("on").append("=").append(12);
                v = 12;
                if (length >= 9) {
                    String substring6 = str.substring(8, 9);
                    if ("3".equals(substring6)) {
                        a(sb, "so", "17");
                    } else if ("4".equals(substring6)) {
                        a(sb, "so", "18");
                    } else if ("7".equals(substring6)) {
                        a(sb, "so", "21");
                    } else if ("8".equals(substring6)) {
                        a(sb, "so", "22");
                    } else if ("9".equals(substring6)) {
                        a(sb, "so", "23");
                    }
                    if (length >= 13) {
                        String substring7 = str.substring(10, 13);
                        if ("010".equals(substring7)) {
                            a(sb, "ac", "01");
                        } else if ("022".equals(substring7)) {
                            a(sb, "ac", "03");
                        } else if (substring7.startsWith("31") || substring7.startsWith("33")) {
                            a(sb, "ac", "09");
                        } else if (substring7.startsWith("35") || substring7.startsWith("34")) {
                            a(sb, "ac", "12");
                        } else if (substring7.startsWith("47") || substring7.startsWith("48")) {
                            a(sb, "ac", "08");
                        } else if ("024".equals(substring7) || substring7.startsWith("41") || substring7.startsWith("42")) {
                            a(sb, "ac", "07");
                        } else if (substring7.startsWith("43")) {
                            a(sb, "ac", "06");
                        } else if (substring7.startsWith("45") || substring7.startsWith("46")) {
                            a(sb, "ac", "05");
                        } else if ("021".equals(substring7)) {
                            a(sb, "ac", "02");
                        } else if (substring7.equals("025") || substring7.startsWith("51") || substring7.startsWith("52")) {
                            a(sb, "ac", "14");
                        } else if (substring7.startsWith("57") || substring7.startsWith("58")) {
                            a(sb, "ac", "18");
                        } else if (substring7.startsWith("55") || substring7.startsWith("56")) {
                            a(sb, "ac", "13");
                        } else if (substring7.startsWith("59")) {
                            a(sb, "ac", "19");
                        } else if (substring7.startsWith("79") || substring7.startsWith("70")) {
                            a(sb, "ac", "15");
                        } else if (substring7.startsWith("53") || substring7.startsWith("54") || substring7.startsWith("63")) {
                            a(sb, "ac", "11");
                        } else if (substring7.startsWith("37") || substring7.startsWith("39")) {
                            a(sb, "ac", "10");
                        } else if ("027".equals(substring7) || substring7.startsWith("71") || substring7.startsWith("72")) {
                            a(sb, "ac", "17");
                        } else if (substring7.startsWith("73") || substring7.startsWith("74")) {
                            a(sb, "ac", "16");
                        } else if ("020".equals(substring7) || substring7.startsWith("75") || substring7.startsWith("76") || substring7.startsWith("66")) {
                            a(sb, "ac", "20");
                        } else if (substring7.startsWith("77")) {
                            a(sb, "ac", "29");
                        } else if ("898".equals(substring7)) {
                            a(sb, "ac", "27");
                        } else if ("028".equals(substring7) || substring7.startsWith("81") || substring7.startsWith("82") || substring7.startsWith("83")) {
                            a(sb, "ac", "24");
                        } else if (substring7.startsWith("85")) {
                            a(sb, "ac", "25");
                        } else if (substring7.startsWith("87") || substring7.startsWith("88") || substring7.startsWith("69")) {
                            a(sb, "ac", "26");
                        } else if (substring7.startsWith("89")) {
                            a(sb, "ac", "30");
                        } else if ("029".equals(substring7) || substring7.startsWith("91")) {
                            a(sb, "ac", "21");
                        } else if (substring7.startsWith("93") || substring7.startsWith("94")) {
                            a(sb, "ac", "22");
                        } else if (substring7.startsWith("97")) {
                            a(sb, "ac", "23");
                        } else if (substring7.startsWith("95")) {
                            a(sb, "ac", "28");
                        } else if (substring7.startsWith("90") || substring7.startsWith("99")) {
                            a(sb, "ac", "31");
                        } else if ("023".equals(substring7)) {
                            a(sb, "ac", "04");
                        }
                    }
                }
            }
        }
        return sb.toString();
    }
}
