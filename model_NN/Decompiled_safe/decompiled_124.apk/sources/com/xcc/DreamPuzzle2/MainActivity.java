package com.xcc.DreamPuzzle2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageButton;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class MainActivity extends Activity {
    private AdView adView;
    /* access modifiers changed from: private */
    public AnimationSet animationSet;
    /* access modifiers changed from: private */
    public ImageButton continueButton;
    /* access modifiers changed from: private */
    public int level;
    /* access modifiers changed from: private */
    public MediaPlayer mediaPlayer;
    /* access modifiers changed from: private */
    public ImageButton optionsButton;
    public long[] patten;
    /* access modifiers changed from: private */
    public ImageButton quitButton;
    /* access modifiers changed from: private */
    public ImageButton startButton;
    /* access modifiers changed from: private */
    public Vibrator vibrator;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.animationSet = new AnimationSet(true);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 2.0f, 1.0f, 2.0f, 1, 0.5f, 1, 0.5f);
        this.animationSet.addAnimation(alphaAnimation);
        this.animationSet.addAnimation(scaleAnimation);
        this.animationSet.setDuration(300);
        this.startButton = (ImageButton) findViewById(R.id.startButton);
        this.startButton.setOnClickListener(new startOnclick());
        this.continueButton = (ImageButton) findViewById(R.id.continueButton);
        this.continueButton.setOnClickListener(new continueOnclick());
        this.optionsButton = (ImageButton) findViewById(R.id.optionButton);
        this.optionsButton.setOnClickListener(new optionsOnclick());
        this.quitButton = (ImageButton) findViewById(R.id.quitButton);
        this.quitButton.setOnClickListener(new quitOnclick());
        this.adView = (AdView) findViewById(R.id.adView);
        this.adView.loadAd(new AdRequest());
    }

    class startOnclick implements View.OnClickListener {
        startOnclick() {
        }

        public void onClick(View v) {
            MainActivity.this.mediaPlayer.start();
            MainActivity.this.vibrator.vibrate(MainActivity.this.patten, -1);
            MainActivity.this.startButton.startAnimation(MainActivity.this.animationSet);
            if (MainActivity.this.level != 0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle((int) R.string.app_name);
                builder.setMessage((int) R.string.newgame);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        int[] picureArray = new CreateRondom().createRondomPG();
                        StringBuffer pBuffer = new StringBuffer();
                        for (int append : picureArray) {
                            pBuffer.append("/");
                            pBuffer.append(append);
                        }
                        SharedPreferences getDate = MainActivity.this.getSharedPreferences("DreamPuzzle", 0);
                        SharedPreferences.Editor editor = getDate.edit();
                        boolean msound = getDate.getBoolean("soundEnable", true);
                        boolean mvibrat = getDate.getBoolean("vibratEnable", true);
                        editor.clear().commit();
                        editor.putString("pBuff", pBuffer.toString());
                        editor.putBoolean("soundEnable", msound);
                        editor.putBoolean("vibratEnable", mvibrat);
                        editor.commit();
                        Intent intent = new Intent();
                        intent.setClass(MainActivity.this, GameActivity.class);
                        intent.putExtra("pictureArray", picureArray);
                        intent.putExtra("level", 0);
                        MainActivity.this.startActivity(intent);
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.create().show();
                return;
            }
            int[] picureArray = new CreateRondom().createRondomPG();
            StringBuffer pBuffer = new StringBuffer();
            for (int append : picureArray) {
                pBuffer.append("/");
                pBuffer.append(append);
            }
            SharedPreferences getDate = MainActivity.this.getSharedPreferences("DreamPuzzle", 0);
            SharedPreferences.Editor editor = getDate.edit();
            boolean msound = getDate.getBoolean("soundEnable", true);
            boolean mvibrat = getDate.getBoolean("vibratEnable", true);
            editor.clear().commit();
            editor.putString("pBuff", pBuffer.toString());
            editor.putBoolean("soundEnable", msound);
            editor.putBoolean("vibratEnable", mvibrat);
            editor.commit();
            Intent intent = new Intent();
            intent.setClass(MainActivity.this, GameActivity.class);
            intent.putExtra("pictureArray", picureArray);
            intent.putExtra("level", 0);
            MainActivity.this.startActivity(intent);
        }
    }

    class continueOnclick implements View.OnClickListener {
        continueOnclick() {
        }

        public void onClick(View v) {
            MainActivity.this.mediaPlayer.start();
            MainActivity.this.vibrator.vibrate(MainActivity.this.patten, -1);
            MainActivity.this.continueButton.startAnimation(MainActivity.this.animationSet);
            if (MainActivity.this.level == 0) {
                Toast.makeText(MainActivity.this, (int) R.string.creatgame, 1).show();
                return;
            }
            Intent intent = new Intent();
            intent.setClass(MainActivity.this, GridViewActivity.class);
            MainActivity.this.startActivity(intent);
        }
    }

    class optionsOnclick implements View.OnClickListener {
        optionsOnclick() {
        }

        public void onClick(View v) {
            MainActivity.this.mediaPlayer.start();
            MainActivity.this.vibrator.vibrate(MainActivity.this.patten, -1);
            MainActivity.this.optionsButton.startAnimation(MainActivity.this.animationSet);
            Intent intent = new Intent();
            intent.setClass(MainActivity.this, OptionActivity.class);
            MainActivity.this.startActivity(intent);
        }
    }

    class quitOnclick implements View.OnClickListener {
        quitOnclick() {
        }

        public void onClick(View v) {
            MainActivity.this.mediaPlayer.start();
            MainActivity.this.quitButton.startAnimation(MainActivity.this.animationSet);
            MainActivity.this.finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mediaPlayer = MediaPlayer.create(this, (int) R.raw.sound_hint);
        this.mediaPlayer.setLooping(false);
        this.vibrator = (Vibrator) getSystemService("vibrator");
        SharedPreferences settings = getSharedPreferences("DreamPuzzle", 0);
        this.level = settings.getInt("levels", 0);
        if (settings.getBoolean("soundEnable", true)) {
            this.mediaPlayer.setVolume(1.0f, 1.0f);
        } else {
            this.mediaPlayer.setVolume(0.0f, 0.0f);
        }
        if (settings.getBoolean("vibratEnable", true)) {
            long[] jArr = new long[4];
            jArr[0] = 100;
            jArr[1] = 50;
            jArr[2] = 50;
            this.patten = jArr;
            return;
        }
        long[] jArr2 = new long[4];
        jArr2[0] = 100;
        jArr2[2] = 50;
        this.patten = jArr2;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.mediaPlayer != null) {
            this.mediaPlayer.release();
        }
        super.onDestroy();
    }
}
