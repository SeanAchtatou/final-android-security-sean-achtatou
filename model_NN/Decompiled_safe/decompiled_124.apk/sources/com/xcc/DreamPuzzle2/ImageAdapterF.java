package com.xcc.DreamPuzzle2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;

public class ImageAdapterF extends BaseAdapter {
    private int levels;
    private Context mContext;
    private int[] pictureArray;
    private int[] scoreSave;

    public ImageAdapterF(Context c, int levels2, int[] scoreSave2, int[] pictureArray2) {
        this.mContext = c;
        this.levels = levels2;
        this.scoreSave = scoreSave2;
        this.pictureArray = pictureArray2;
    }

    public int getCount() {
        return DateContent.PICTURE_GROUP.length;
    }

    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View convertView2 = LayoutInflater.from(this.mContext.getApplicationContext()).inflate((int) R.layout.frame_layout_style, (ViewGroup) null);
        ((ImageView) convertView2.findViewById(R.id.bottomP)).setImageResource(DateContent.GRID_GROUP[this.pictureArray[position]]);
        ImageView topP = (ImageView) convertView2.findViewById(R.id.topP);
        if (position >= this.levels) {
            topP.setImageResource(R.drawable.lock);
        }
        ((RatingBar) convertView2.findViewById(R.id.ratingBarInLevel)).setRating((float) this.scoreSave[position]);
        return convertView2;
    }
}
