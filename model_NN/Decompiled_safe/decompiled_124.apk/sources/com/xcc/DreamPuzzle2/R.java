package com.xcc.DreamPuzzle2;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int bg_firsttheme = 2130837504;
        public static final int btn_continue = 2130837505;
        public static final int btn_options = 2130837506;
        public static final int btn_quit = 2130837507;
        public static final int btn_start = 2130837508;
        public static final int complete_excellent = 2130837509;
        public static final int complete_good = 2130837510;
        public static final int complete_perfect = 2130837511;
        public static final int control_bg = 2130837512;
        public static final int dock = 2130837513;
        public static final int gamelogo = 2130837514;
        public static final int icon = 2130837515;
        public static final int levelnext = 2130837516;
        public static final int levelprev = 2130837517;
        public static final int levelrestart = 2130837518;
        public static final int levelviewpic = 2130837519;
        public static final int lock = 2130837520;
        public static final int m1 = 2130837521;
        public static final int m10 = 2130837522;
        public static final int m11 = 2130837523;
        public static final int m12 = 2130837524;
        public static final int m13 = 2130837525;
        public static final int m14 = 2130837526;
        public static final int m15 = 2130837527;
        public static final int m16 = 2130837528;
        public static final int m17 = 2130837529;
        public static final int m18 = 2130837530;
        public static final int m19 = 2130837531;
        public static final int m2 = 2130837532;
        public static final int m20 = 2130837533;
        public static final int m21 = 2130837534;
        public static final int m22 = 2130837535;
        public static final int m23 = 2130837536;
        public static final int m24 = 2130837537;
        public static final int m25 = 2130837538;
        public static final int m26 = 2130837539;
        public static final int m27 = 2130837540;
        public static final int m28 = 2130837541;
        public static final int m29 = 2130837542;
        public static final int m3 = 2130837543;
        public static final int m30 = 2130837544;
        public static final int m31 = 2130837545;
        public static final int m32 = 2130837546;
        public static final int m33 = 2130837547;
        public static final int m34 = 2130837548;
        public static final int m35 = 2130837549;
        public static final int m36 = 2130837550;
        public static final int m37 = 2130837551;
        public static final int m38 = 2130837552;
        public static final int m39 = 2130837553;
        public static final int m4 = 2130837554;
        public static final int m40 = 2130837555;
        public static final int m5 = 2130837556;
        public static final int m6 = 2130837557;
        public static final int m7 = 2130837558;
        public static final int m8 = 2130837559;
        public static final int m9 = 2130837560;
        public static final int music_close = 2130837561;
        public static final int music_open = 2130837562;
        public static final int n1 = 2130837563;
        public static final int n10 = 2130837564;
        public static final int n11 = 2130837565;
        public static final int n12 = 2130837566;
        public static final int n13 = 2130837567;
        public static final int n14 = 2130837568;
        public static final int n15 = 2130837569;
        public static final int n16 = 2130837570;
        public static final int n17 = 2130837571;
        public static final int n18 = 2130837572;
        public static final int n19 = 2130837573;
        public static final int n2 = 2130837574;
        public static final int n20 = 2130837575;
        public static final int n21 = 2130837576;
        public static final int n22 = 2130837577;
        public static final int n23 = 2130837578;
        public static final int n24 = 2130837579;
        public static final int n25 = 2130837580;
        public static final int n26 = 2130837581;
        public static final int n27 = 2130837582;
        public static final int n28 = 2130837583;
        public static final int n29 = 2130837584;
        public static final int n3 = 2130837585;
        public static final int n30 = 2130837586;
        public static final int n31 = 2130837587;
        public static final int n32 = 2130837588;
        public static final int n33 = 2130837589;
        public static final int n34 = 2130837590;
        public static final int n35 = 2130837591;
        public static final int n36 = 2130837592;
        public static final int n37 = 2130837593;
        public static final int n38 = 2130837594;
        public static final int n39 = 2130837595;
        public static final int n4 = 2130837596;
        public static final int n40 = 2130837597;
        public static final int n5 = 2130837598;
        public static final int n6 = 2130837599;
        public static final int n7 = 2130837600;
        public static final int n8 = 2130837601;
        public static final int n9 = 2130837602;
        public static final int nextlevel = 2130837603;
        public static final int ratingbar_empty = 2130837604;
        public static final int ratingbar_full = 2130837605;
        public static final int savesd = 2130837606;
        public static final int self_ratingbar = 2130837607;
        public static final int shake_close = 2130837608;
        public static final int shake_open = 2130837609;
        public static final int toolbar_menu_item = 2130837610;
        public static final int toolbar_menu_release = 2130837611;
        public static final int wallpaper = 2130837612;
    }

    public static final class id {
        public static final int GridView_toolbar = 2131165208;
        public static final int IconView = 2131165203;
        public static final int RelativeLayout_Item = 2131165200;
        public static final int adView = 2131165234;
        public static final int appname = 2131165204;
        public static final int bottomP = 2131165196;
        public static final int chances = 2131165186;
        public static final int compelete = 2131165187;
        public static final int congratulate = 2131165188;
        public static final int continueButton = 2131165237;
        public static final int countMoves = 2131165189;
        public static final int countTimes = 2131165190;
        public static final int enableSound = 2131165240;
        public static final int enableVibrat = 2131165242;
        public static final int frame_layout_root = 2131165195;
        public static final int gameLogo = 2131165235;
        public static final int gotoNextLevel = 2131165194;
        public static final int gridview = 2131165199;
        public static final int item_image = 2131165201;
        public static final int item_text = 2131165202;
        public static final int layout_root = 2131165184;
        public static final int levels = 2131165205;
        public static final int moves = 2131165206;
        public static final int optionButton = 2131165238;
        public static final int quitButton = 2131165239;
        public static final int ratingBarInLevel = 2131165198;
        public static final int savesd = 2131165193;
        public static final int scoreRating = 2131165191;
        public static final int second0 = 2131165209;
        public static final int second1 = 2131165210;
        public static final int second10 = 2131165219;
        public static final int second11 = 2131165220;
        public static final int second12 = 2131165221;
        public static final int second13 = 2131165222;
        public static final int second14 = 2131165223;
        public static final int second15 = 2131165224;
        public static final int second16 = 2131165225;
        public static final int second17 = 2131165226;
        public static final int second18 = 2131165227;
        public static final int second19 = 2131165228;
        public static final int second2 = 2131165211;
        public static final int second20 = 2131165229;
        public static final int second21 = 2131165230;
        public static final int second22 = 2131165231;
        public static final int second23 = 2131165232;
        public static final int second24 = 2131165233;
        public static final int second3 = 2131165212;
        public static final int second4 = 2131165213;
        public static final int second5 = 2131165214;
        public static final int second6 = 2131165215;
        public static final int second7 = 2131165216;
        public static final int second8 = 2131165217;
        public static final int second9 = 2131165218;
        public static final int selectedP = 2131165185;
        public static final int soundOn = 2131165241;
        public static final int startButton = 2131165236;
        public static final int times = 2131165207;
        public static final int topP = 2131165197;
        public static final int vibratOn = 2131165243;
        public static final int wallp = 2131165192;
    }

    public static final class layout {
        public static final int check_dialog = 2130903040;
        public static final int compelete_dialog = 2130903041;
        public static final int frame_layout_style = 2130903042;
        public static final int graidview = 2130903043;
        public static final int item_menu = 2130903044;
        public static final int level_five = 2130903045;
        public static final int level_four = 2130903046;
        public static final int level_one = 2130903047;
        public static final int level_three = 2130903048;
        public static final int level_two = 2130903049;
        public static final int main = 2130903050;
        public static final int option = 2130903051;
    }

    public static final class raw {
        public static final int sound_hint = 2130968576;
        public static final int sound_start = 2130968577;
        public static final int sound_win = 2130968578;
    }

    public static final class string {
        public static final int app_name = 2131099649;
        public static final int choiceLevel = 2131099659;
        public static final int company = 2131099652;
        public static final int congratulate = 2131099657;
        public static final int creatgame = 2131099665;
        public static final int enableSounds = 2131099653;
        public static final int enableVibrat = 2131099654;
        public static final int firstLevel = 2131099660;
        public static final int gamePlace = 2131099651;
        public static final int hello = 2131099648;
        public static final int lastLevel = 2131099662;
        public static final int newgame = 2131099664;
        public static final int noChances = 2131099661;
        public static final int options = 2131099650;
        public static final int score = 2131099658;
        public static final int soundOff = 2131099656;
        public static final int soundOn = 2131099655;
        public static final int unlockFirst = 2131099663;
    }

    public static final class style {
        public static final int selfRatingBar = 2131034112;
    }
}
