package com.xcc.DreamPuzzle2;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.util.HashMap;

public class DividePicture {
    Bitmap bitmap = null;

    public HashMap<String, Object> divideIntoNine(Resources res, int pictureId) {
        new HashMap();
        Bitmap temp = BitmapFactory.decodeResource(res, pictureId);
        return divide(temp, 3, 3, temp.getWidth(), temp.getHeight());
    }

    public HashMap<String, Object> divideIntoTwelve(Resources res, int pictureId) {
        new HashMap();
        Bitmap temp = BitmapFactory.decodeResource(res, pictureId);
        return divide(temp, 3, 4, temp.getWidth(), temp.getHeight());
    }

    public HashMap<String, Object> divideIntoSixteen(Resources res, int pictureId) {
        new HashMap();
        Bitmap temp = BitmapFactory.decodeResource(res, pictureId);
        return divide(temp, 4, 4, temp.getWidth(), temp.getHeight());
    }

    public HashMap<String, Object> divideIntoTwenty(Resources res, int pictureId) {
        new HashMap();
        Bitmap temp = BitmapFactory.decodeResource(res, pictureId);
        return divide(temp, 4, 5, temp.getWidth(), temp.getHeight());
    }

    public HashMap<String, Object> divideIntoTwentyFive(Resources res, int pictureId) {
        new HashMap();
        Bitmap temp = BitmapFactory.decodeResource(res, pictureId);
        return divide(temp, 5, 5, temp.getWidth(), temp.getHeight());
    }

    private HashMap<String, Object> divide(Bitmap temp, int m, int n, int width, int height) {
        HashMap<String, Object> map = new HashMap<>();
        int z = 0;
        int a = width / m;
        int b = height / n;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                this.bitmap = Bitmap.createBitmap(temp, (width * j) / m, (height * i) / n, a, b);
                map.put("bitmap" + z, this.bitmap);
                z++;
            }
        }
        return map;
    }
}
