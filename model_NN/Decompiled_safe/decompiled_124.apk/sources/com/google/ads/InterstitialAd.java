package com.google.ads;

import android.app.Activity;
import com.google.ads.util.a;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
public class InterstitialAd implements Ad {
    private d a;

    public InterstitialAd(Activity activity, String adUnitId) {
        this(activity, adUnitId, false);
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: d.<init>(android.app.Activity, com.google.ads.Ad, com.google.ads.AdSize, java.lang.String, boolean):void in method: com.google.ads.InterstitialAd.<init>(android.app.Activity, java.lang.String, boolean):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:59)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: d.<init>(android.app.Activity, com.google.ads.Ad, com.google.ads.AdSize, java.lang.String, boolean):void
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:543)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 6 more
        */
    public InterstitialAd(android.app.Activity r1, java.lang.String r2, boolean r3) {
        /*
            r6 = this;
            r6.<init>()
            d r0 = new d
            r3 = 0
            r1 = r7
            r2 = r6
            r4 = r8
            r5 = r9
            r0.<init>(r1, r2, r3, r4, r5)
            r6.a = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.InterstitialAd.<init>(android.app.Activity, java.lang.String, boolean):void");
    }

    public boolean isReady() {
        return this.a.o();
    }

    public void loadAd(AdRequest adRequest) {
        this.a.a(adRequest);
    }

    public void setAdListener(AdListener adListener) {
        this.a.a(adListener);
    }

    public void show() {
        if (isReady()) {
            this.a.y();
            this.a.v();
            AdActivity.launchAdActivity(this.a, new e("interstitial"));
            return;
        }
        a.c("Cannot show interstitial because it is not loaded and ready.");
    }

    public void stopLoading() {
        this.a.z();
    }
}
