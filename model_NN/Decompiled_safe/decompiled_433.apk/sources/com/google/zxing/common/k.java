package com.google.zxing.common;

import com.google.zxing.NotFoundException;
import com.google.zxing.b;
import com.google.zxing.e;

public class k extends b {

    /* renamed from: a  reason: collision with root package name */
    private byte[] f164a = null;
    private int[] b = null;

    public k(e eVar) {
        super(eVar);
    }

    private static int a(int[] iArr) {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        int length = iArr.length;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        for (int i8 = 0; i8 < length; i8++) {
            if (iArr[i8] > i5) {
                i5 = iArr[i8];
                i6 = i8;
            }
            if (iArr[i8] > i7) {
                i7 = iArr[i8];
            }
        }
        int i9 = 0;
        int i10 = 0;
        while (i4 < length) {
            int i11 = i4 - i6;
            int i12 = i11 * iArr[i4] * i11;
            if (i12 > i9) {
                i3 = i4;
            } else {
                i12 = i9;
                i3 = i10;
            }
            i4++;
            i10 = i3;
            i9 = i12;
        }
        if (i6 > i10) {
            i = i10;
            i10 = i6;
        } else {
            i = i6;
        }
        if (i10 - i <= (length >> 4)) {
            throw NotFoundException.a();
        }
        int i13 = i10 - 1;
        int i14 = -1;
        int i15 = i10 - 1;
        while (i15 > i) {
            int i16 = i15 - i;
            int i17 = i16 * i16 * (i10 - i15) * (i7 - iArr[i15]);
            if (i17 > i14) {
                i2 = i15;
            } else {
                i17 = i14;
                i2 = i13;
            }
            i15--;
            i13 = i2;
            i14 = i17;
        }
        return i13 << 3;
    }

    private void a(int i) {
        if (this.f164a == null || this.f164a.length < i) {
            this.f164a = new byte[i];
        }
        if (this.b == null) {
            this.b = new int[32];
            return;
        }
        for (int i2 = 0; i2 < 32; i2++) {
            this.b[i2] = 0;
        }
    }

    public b a(e eVar) {
        return new k(eVar);
    }

    public a a(int i, a aVar) {
        int i2 = 1;
        e a2 = a();
        int b2 = a2.b();
        if (aVar == null || aVar.a() < b2) {
            aVar = new a(b2);
        } else {
            aVar.b();
        }
        a(b2);
        byte[] a3 = a2.a(i, this.f164a);
        int[] iArr = this.b;
        for (int i3 = 0; i3 < b2; i3++) {
            int i4 = (a3[i3] & 255) >> 3;
            iArr[i4] = iArr[i4] + 1;
        }
        int a4 = a(iArr);
        byte b3 = a3[0] & 255;
        byte b4 = a3[1] & 255;
        byte b5 = b3;
        while (i2 < b2 - 1) {
            byte b6 = a3[i2 + 1] & 255;
            if (((((b4 << 2) - b5) - b6) >> 1) < a4) {
                aVar.b(i2);
            }
            i2++;
            b5 = b4;
            b4 = b6;
        }
        return aVar;
    }

    public b b() {
        e a2 = a();
        int b2 = a2.b();
        int c = a2.c();
        b bVar = new b(b2, c);
        a(b2);
        int[] iArr = this.b;
        int i = 1;
        while (true) {
            int i2 = i;
            if (i2 >= 5) {
                break;
            }
            byte[] a3 = a2.a((c * i2) / 5, this.f164a);
            int i3 = (b2 << 2) / 5;
            for (int i4 = b2 / 5; i4 < i3; i4++) {
                int i5 = (a3[i4] & 255) >> 3;
                iArr[i5] = iArr[i5] + 1;
            }
            i = i2 + 1;
        }
        int a4 = a(iArr);
        byte[] a5 = a2.a();
        for (int i6 = 0; i6 < c; i6++) {
            int i7 = i6 * b2;
            for (int i8 = 0; i8 < b2; i8++) {
                if ((a5[i7 + i8] & 255) < a4) {
                    bVar.b(i8, i6);
                }
            }
        }
        return bVar;
    }
}
