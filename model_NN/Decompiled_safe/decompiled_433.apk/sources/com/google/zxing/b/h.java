package com.google.zxing.b;

import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.common.a;
import com.google.zxing.d;
import com.google.zxing.j;
import java.util.Hashtable;

public final class h extends k {

    /* renamed from: a  reason: collision with root package name */
    static final int[][] f144a = {new int[]{1, 1, 3, 3, 1}, new int[]{3, 1, 1, 1, 3}, new int[]{1, 3, 1, 1, 3}, new int[]{3, 3, 1, 1, 1}, new int[]{1, 1, 3, 1, 3}, new int[]{3, 1, 3, 1, 1}, new int[]{1, 3, 3, 1, 1}, new int[]{1, 1, 1, 3, 3}, new int[]{3, 1, 1, 3, 1}, new int[]{1, 3, 1, 3, 1}};
    private static final int[] b = {6, 10, 12, 14, 44};
    private static final int[] d = {1, 1, 1, 1};
    private static final int[] e = {1, 1, 3};
    private int c = -1;

    private static int a(int[] iArr) {
        int i = 107;
        int i2 = -1;
        int length = f144a.length;
        int i3 = 0;
        while (i3 < length) {
            int a2 = a(iArr, f144a[i3], 204);
            if (a2 < i) {
                i2 = i3;
            } else {
                a2 = i;
            }
            i3++;
            i = a2;
        }
        if (i2 >= 0) {
            return i2;
        }
        throw NotFoundException.a();
    }

    private void a(a aVar, int i) {
        int i2 = this.c * 10;
        int i3 = i - 1;
        while (i2 > 0 && i3 >= 0 && !aVar.a(i3)) {
            i2--;
            i3--;
        }
        if (i2 != 0) {
            throw NotFoundException.a();
        }
    }

    private static void a(a aVar, int i, int i2, StringBuffer stringBuffer) {
        int i3;
        int[] iArr = new int[10];
        int[] iArr2 = new int[5];
        int[] iArr3 = new int[5];
        for (int i4 = i; i4 < i2; i4 = i3) {
            a(aVar, i4, iArr);
            for (int i5 = 0; i5 < 5; i5++) {
                int i6 = i5 << 1;
                iArr2[i5] = iArr[i6];
                iArr3[i5] = iArr[i6 + 1];
            }
            stringBuffer.append((char) (a(iArr2) + 48));
            stringBuffer.append((char) (a(iArr3) + 48));
            i3 = i4;
            for (int i7 : iArr) {
                i3 += i7;
            }
        }
    }

    private static int c(a aVar) {
        int a2 = aVar.a();
        int i = 0;
        while (i < a2 && !aVar.a(i)) {
            i++;
        }
        if (i != a2) {
            return i;
        }
        throw NotFoundException.a();
    }

    private static int[] c(a aVar, int i, int[] iArr) {
        int length = iArr.length;
        int[] iArr2 = new int[length];
        int a2 = aVar.a();
        int i2 = i;
        int i3 = 0;
        boolean z = false;
        while (i < a2) {
            if (aVar.a(i) ^ z) {
                iArr2[i3] = iArr2[i3] + 1;
            } else {
                if (i3 != length - 1) {
                    i3++;
                } else if (a(iArr2, iArr, 204) < 107) {
                    return new int[]{i2, i};
                } else {
                    i2 += iArr2[0] + iArr2[1];
                    for (int i4 = 2; i4 < length; i4++) {
                        iArr2[i4 - 2] = iArr2[i4];
                    }
                    iArr2[length - 2] = 0;
                    iArr2[length - 1] = 0;
                    i3--;
                }
                iArr2[i3] = 1;
                z = !z;
            }
            i++;
        }
        throw NotFoundException.a();
    }

    public com.google.zxing.h a(int i, a aVar, Hashtable hashtable) {
        boolean z;
        int[] a2 = a(aVar);
        int[] b2 = b(aVar);
        StringBuffer stringBuffer = new StringBuffer(20);
        a(aVar, a2[1], b2[0], stringBuffer);
        String stringBuffer2 = stringBuffer.toString();
        int[] iArr = hashtable != null ? (int[]) hashtable.get(d.f) : null;
        if (iArr == null) {
            iArr = b;
        }
        int length = stringBuffer2.length();
        int i2 = 0;
        while (true) {
            if (i2 >= iArr.length) {
                z = false;
                break;
            } else if (length == iArr[i2]) {
                z = true;
                break;
            } else {
                i2++;
            }
        }
        if (!z) {
            throw FormatException.a();
        }
        return new com.google.zxing.h(stringBuffer2, null, new j[]{new j((float) a2[1], (float) i), new j((float) b2[0], (float) i)}, com.google.zxing.a.l);
    }

    /* access modifiers changed from: package-private */
    public int[] a(a aVar) {
        int[] c2 = c(aVar, c(aVar), d);
        this.c = (c2[1] - c2[0]) >> 2;
        a(aVar, c2[0]);
        return c2;
    }

    /* access modifiers changed from: package-private */
    public int[] b(a aVar) {
        aVar.c();
        try {
            int[] c2 = c(aVar, c(aVar), e);
            a(aVar, c2[0]);
            int i = c2[0];
            c2[0] = aVar.a() - c2[1];
            c2[1] = aVar.a() - i;
            return c2;
        } finally {
            aVar.c();
        }
    }
}
