package com.tencent.assistantv2.component;

import android.content.Context;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.protocol.jce.NewsInfo;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class y extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ListItemRelateNewsView f2012a;
    private Context b;
    private NewsInfo c;

    public y(ListItemRelateNewsView listItemRelateNewsView, Context context, NewsInfo newsInfo) {
        this.f2012a = listItemRelateNewsView;
        this.b = context;
        this.c = newsInfo;
    }

    public void onTMAClick(View view) {
        if (this.c != null) {
            b.a(this.f2012a.getContext(), this.c.f);
        }
    }
}
