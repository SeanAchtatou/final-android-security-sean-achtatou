package com.tencent.assistantv2.component;

import android.os.Handler;
import android.os.Message;
import com.tencent.assistant.component.txscrollview.TXTabBarLayout;

/* compiled from: ProGuard */
class bl extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UserTagAnimationView f1983a;

    bl(UserTagAnimationView userTagAnimationView) {
        this.f1983a = userTagAnimationView;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case TXTabBarLayout.TABITEM_TIPS_TEXT_ID:
                this.f1983a.invalidate();
                return;
            default:
                return;
        }
    }
}
