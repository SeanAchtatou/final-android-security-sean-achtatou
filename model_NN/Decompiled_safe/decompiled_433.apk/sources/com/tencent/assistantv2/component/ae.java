package com.tencent.assistantv2.component;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class ae implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MainActionHeaderView f1952a;

    ae(MainActionHeaderView mainActionHeaderView) {
        this.f1952a = mainActionHeaderView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.tencent.assistantv2.component.MainActionHeaderView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void run() {
        try {
            ((LayoutInflater) this.f1952a.b.getSystemService("layout_inflater")).inflate((int) R.layout.v2_main_action_header, (ViewGroup) this.f1952a, true);
            this.f1952a.h();
            this.f1952a.j.register(this.f1952a.p);
        } catch (Throwable th) {
        }
    }
}
