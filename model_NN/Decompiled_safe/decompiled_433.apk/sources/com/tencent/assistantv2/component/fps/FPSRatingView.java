package com.tencent.assistantv2.component.fps;

import android.content.Context;
import android.util.AttributeSet;
import com.tencent.assistant.component.RatingView;

/* compiled from: ProGuard */
public class FPSRatingView extends RatingView {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1995a = true;
    private int b = -1;
    private int c = -1;

    public FPSRatingView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public FPSRatingView(Context context) {
        super(context);
    }

    public void requestLayout() {
        if (this.f1995a) {
            super.requestLayout();
        }
    }

    public void setVisibility(int i) {
        if (getVisibility() != i) {
            this.f1995a = false;
            super.setVisibility(i);
            this.f1995a = true;
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void onMeasure(int i, int i2) {
        if (FPSTextView.a()) {
            super.onMeasure(i, i2);
        } else if (this.b == 16777215 || this.b == -1 || this.c == -1 || this.c == 16777215 || this.c > 300 || this.b > 300) {
            super.onMeasure(i, i2);
            this.b = getMeasuredWidth();
            this.c = getMeasuredHeight();
        } else {
            setMeasuredDimension(this.b, this.c);
        }
    }
}
