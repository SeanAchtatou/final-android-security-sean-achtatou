package com.tencent.assistantv2.activity;

import android.content.Context;
import android.os.Bundle;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.module.b;
import com.tencent.assistant.st.STConst;
import com.tencent.pangu.adapter.RankNormalListAdapter;
import com.tencent.pangu.component.RankNormalListPage;

/* compiled from: ProGuard */
public class MainTestActivity extends BaseActivity {
    RankNormalListPage n = null;
    RankNormalListAdapter u = null;
    private LinearLayout v;
    private Context w;
    private ListViewScrollListener x = new ad(this);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.w = this;
        setContentView((int) R.layout.main_test_layout);
        this.v = (LinearLayout) findViewById(R.id.main_test_layout);
        b bVar = new b(0, 5, 20);
        this.n = new RankNormalListPage(this.w, TXScrollViewBase.ScrollMode.PULL_FROM_END, bVar);
        this.u = new RankNormalListAdapter(this.w, this.n, bVar.a());
        this.u.a((int) STConst.ST_PAGE_RANK_CLASSIC, -100);
        this.u.a(RankNormalListAdapter.ListType.LISTTYPEGAMESORT);
        this.u.a(true);
        this.n.a(this.u, this.x);
        this.v.addView(this.n, new LinearLayout.LayoutParams(-1, -1));
        this.n.b();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.u.b();
        this.n.c();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.u.c();
        this.n.d();
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
