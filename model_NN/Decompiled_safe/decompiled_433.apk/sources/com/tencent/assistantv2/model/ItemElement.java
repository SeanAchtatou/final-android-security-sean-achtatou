package com.tencent.assistantv2.model;

import java.io.Serializable;

/* compiled from: ProGuard */
public class ItemElement implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public String f2018a;
    public String b;
    public int c;
    public int d;
    public int e;

    public ItemElement() {
    }

    public ItemElement(String str, String str2, int i, int i2, int i3) {
        this.f2018a = str;
        this.b = str2;
        this.c = i;
        this.d = i2;
        this.e = i3;
    }
}
