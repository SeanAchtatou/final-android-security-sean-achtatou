package com.tencent.assistantv2.st.business;

import com.tencent.assistant.db.table.y;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.XLog;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class q implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LocalApkInfo f2042a;
    final /* synthetic */ p b;

    q(p pVar, LocalApkInfo localApkInfo) {
        this.b = pVar;
        this.f2042a = localApkInfo;
    }

    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        XLog.d("InstallStManager", "mApkResListener.onInstalledApkDataChanged begin, packageName=" + this.f2042a.mPackageName + ", versionCode=" + this.f2042a.mVersionCode);
        if (this.b.f2041a.a(this.f2042a.mPackageName, this.f2042a.mVersionCode) == null) {
            XLog.d("InstallStManager", "mApkResListener.onInstalledApkDataChanged begin, packageName=" + this.f2042a.mPackageName + ", versionCode=" + this.f2042a.mVersionCode + " ignored!");
            return;
        }
        this.b.f2041a.a(this.f2042a.mPackageName, this.f2042a.mVersionCode, (byte) 0, (byte) 0, Constants.STR_EMPTY);
        this.b.f2041a.b(this.f2042a.mPackageName, this.f2042a.mVersionCode, (byte) 0, (byte) 0, Constants.STR_EMPTY);
        this.b.f2041a.a(this.f2042a.mPackageName, this.f2042a.mVersionCode, (byte) 0, (byte) 0, Constants.STR_EMPTY, true);
        y.a().a(this.f2042a.mPackageName, this.f2042a.mVersionCode);
    }
}
