package com.tencent.assistantv2.st.page;

import android.content.Context;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.module.k;
import com.tencent.assistantv2.st.model.StatInfo;

/* compiled from: ProGuard */
public class STInfoBuilder {
    public static STInfoV2 buildSTInfo(Context context, int i) {
        STPageInfo a2 = a.a(context, (String) null);
        if (a2 != null) {
            return new STInfoV2(a2.f2060a, a2.b, a2.c, a2.d, i);
        }
        return null;
    }

    public static STInfoV2 buildSTInfo(Context context, SimpleAppModel simpleAppModel, String str, int i, String str2) {
        STInfoV2 sTInfoV2 = null;
        STPageInfo a2 = a.a(context, (String) null);
        if (a2 != null) {
            sTInfoV2 = new STInfoV2(a2.f2060a, str, a2.c, a2.d, i);
            sTInfoV2.status = str2;
        }
        if (sTInfoV2 != null) {
            if (!(i == 100 || simpleAppModel == null)) {
                sTInfoV2.status = a.a(k.d(simpleAppModel), simpleAppModel);
            }
            sTInfoV2.updateWithSimpleAppModel(simpleAppModel);
        }
        return sTInfoV2;
    }

    public static STInfoV2 buildSTInfo(Context context, SimpleAppModel simpleAppModel, String str, int i, String str2, d dVar) {
        STInfoV2 sTInfoV2 = null;
        AppConst.AppState appState = dVar == null ? null : dVar.c;
        STPageInfo a2 = a.a(context, (String) null);
        if (a2 != null) {
            sTInfoV2 = new STInfoV2(a2.f2060a, str, a2.c, a2.d, i);
            sTInfoV2.status = str2;
        }
        if (sTInfoV2 != null) {
            if (!(i == 100 || simpleAppModel == null)) {
                sTInfoV2.status = a.a(appState, simpleAppModel);
            }
            sTInfoV2.updateWithSimpleAppModel(simpleAppModel);
        }
        return sTInfoV2;
    }

    public static StatInfo buildDownloadSTInfo(Context context, SimpleAppModel simpleAppModel) {
        StatInfo statInfo = new StatInfo();
        STPageInfo a2 = a.a(context, (String) null);
        if (a2 != null) {
            statInfo.scene = a2.f2060a;
            statInfo.slotId = a2.b;
            statInfo.sourceScene = a2.c;
            statInfo.sourceSceneSlotId = a2.d;
        }
        if (simpleAppModel != null) {
            statInfo.status = a.a(k.d(simpleAppModel), simpleAppModel);
        }
        return statInfo;
    }
}
