package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.by;
import com.tencent.cloud.model.SimpleVideoModel;
import com.tencent.pangu.model.b;

/* compiled from: ProGuard */
public class ak extends ag {
    private IViewInvalidater c;
    private aa d;

    public ak(Context context, aa aaVar, IViewInvalidater iViewInvalidater) {
        super(context, aaVar, iViewInvalidater);
        this.c = iViewInvalidater;
        this.d = aaVar;
    }

    public Pair<View, Object> a() {
        View inflate = LayoutInflater.from(this.f1900a).inflate((int) R.layout.video_rich_universal_item, (ViewGroup) null);
        am amVar = new am();
        amVar.f1907a = (TextView) inflate.findViewById(R.id.video_match_result);
        amVar.b = (TXAppIconView) inflate.findViewById(R.id.video_icon);
        amVar.b.setInvalidater(this.c);
        amVar.c = (TextView) inflate.findViewById(R.id.video_title);
        amVar.d = (TextView) inflate.findViewById(R.id.video_update_info);
        amVar.e = (TextView) inflate.findViewById(R.id.video_view_count);
        amVar.f = (TextView) inflate.findViewById(R.id.video_year);
        amVar.g = (TextView) inflate.findViewById(R.id.video_author);
        amVar.h = (TextView) inflate.findViewById(R.id.video_label);
        amVar.i = (TextView) inflate.findViewById(R.id.video_desc);
        return Pair.create(inflate, amVar);
    }

    public void a(View view, Object obj, int i, b bVar) {
        SimpleVideoModel simpleVideoModel;
        am amVar = (am) obj;
        if (bVar != null) {
            simpleVideoModel = bVar.d();
        } else {
            simpleVideoModel = null;
        }
        view.setOnClickListener(new ai(this, this.f1900a, i, simpleVideoModel, this.b));
        a(amVar, simpleVideoModel, i);
    }

    private void a(am amVar, SimpleVideoModel simpleVideoModel, int i) {
        if (simpleVideoModel != null && amVar != null) {
            amVar.f1907a.setText(simpleVideoModel.k);
            amVar.b.updateImageView(simpleVideoModel.c, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            amVar.c.setText(simpleVideoModel.b);
            if (!TextUtils.isEmpty(bm.f(simpleVideoModel.i))) {
                amVar.d.setPadding(0, 0, by.a(this.f1900a, 10.0f), 0);
                amVar.d.setText(bm.f(simpleVideoModel.i));
            } else {
                amVar.d.setPadding(0, 0, 0, 0);
            }
            amVar.e.setText(simpleVideoModel.h);
            if (!TextUtils.isEmpty(bm.f(simpleVideoModel.d))) {
                amVar.f.setPadding(0, 0, by.a(this.f1900a, 10.0f), 0);
                amVar.f.setText(bm.f(simpleVideoModel.d));
            } else {
                amVar.f.setPadding(0, 0, 0, 0);
            }
            amVar.g.setText(simpleVideoModel.e);
            amVar.h.setText(simpleVideoModel.g);
            amVar.i.setText(bm.e(simpleVideoModel.f));
        }
    }
}
