package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class r extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    private Context f1917a;
    private int b;
    private SimpleAppModel c;
    private aa d;
    private STInfoV2 e;

    public r(Context context, int i, SimpleAppModel simpleAppModel, aa aaVar, STInfoV2 sTInfoV2) {
        this.f1917a = context;
        this.b = i;
        this.c = simpleAppModel;
        this.d = aaVar;
        this.e = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.f1917a, AppDetailActivityV5.class);
        intent.putExtra("simpleModeInfo", this.c);
        intent.putExtra("st_common_data", this.e);
        this.f1917a.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (this.e != null) {
            this.e.actionId = 200;
            this.e.status = "01";
        }
        return this.e;
    }
}
