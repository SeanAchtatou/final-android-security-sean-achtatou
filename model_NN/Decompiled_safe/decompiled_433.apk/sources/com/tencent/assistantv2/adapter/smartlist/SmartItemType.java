package com.tencent.assistantv2.adapter.smartlist;

/* compiled from: ProGuard */
public enum SmartItemType {
    NORMAL,
    NORMAL_NO_REASON,
    COMPETITIVE,
    TOPBANNER,
    BANNER,
    SMARTCARD,
    VIDEO_NORMAL,
    VIDEO_RICH,
    EBOOK_NORMAL,
    EBOOK_RICH,
    NORMAL_LIST
}
