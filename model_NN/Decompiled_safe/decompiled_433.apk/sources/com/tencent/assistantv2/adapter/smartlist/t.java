package com.tencent.assistantv2.adapter.smartlist;

import android.widget.TextView;
import com.tencent.assistant.adapter.a.c;
import com.tencent.assistant.adapter.a.d;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class t implements d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ s f1918a;

    t(s sVar) {
        this.f1918a = sVar;
    }

    public c a(Object obj) {
        if (obj != null && (obj instanceof v)) {
            v vVar = (v) obj;
            if (vVar.j != null) {
                return vVar.j;
            }
        }
        return null;
    }

    public c b(Object obj) {
        if (obj == null || !(obj instanceof v)) {
            return null;
        }
        return ((v) obj).j;
    }

    public TextView c(Object obj) {
        if (obj != null && (obj instanceof v)) {
            v vVar = (v) obj;
            if (vVar.j != null) {
                return vVar.j.h;
            }
        }
        return null;
    }

    public TextView d(Object obj) {
        if (obj == null || !(obj instanceof v)) {
            return null;
        }
        return ((v) obj).k;
    }

    public String a() {
        if (this.f1918a.b == null || this.f1918a.b.e() == null) {
            return Constants.STR_EMPTY;
        }
        return this.f1918a.b.e().contentId;
    }
}
