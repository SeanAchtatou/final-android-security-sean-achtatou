package com.tencent.nucleus.manager.main;

import android.view.animation.Animation;

/* compiled from: ProGuard */
class f implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AssistantTabActivity f2948a;

    f(AssistantTabActivity assistantTabActivity) {
        this.f2948a = assistantTabActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.main.AssistantTabActivity.a(com.tencent.nucleus.manager.main.AssistantTabActivity, boolean):boolean
     arg types: [com.tencent.nucleus.manager.main.AssistantTabActivity, int]
     candidates:
      com.tencent.nucleus.manager.main.AssistantTabActivity.a(com.tencent.nucleus.manager.main.AssistantTabActivity, java.lang.Class):java.lang.Class
      com.tencent.nucleus.manager.main.AssistantTabActivity.a(com.tencent.nucleus.manager.main.AssistantTabActivity, float):void
      com.tencent.nucleus.manager.main.AssistantTabActivity.a(com.tencent.nucleus.manager.main.AssistantTabActivity, int):void
      com.tencent.nucleus.manager.main.AssistantTabActivity.a(com.tencent.nucleus.manager.main.AssistantTabActivity, java.lang.String):void
      com.tencent.nucleus.manager.main.AssistantTabActivity.a(boolean, long):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.nucleus.manager.main.AssistantTabActivity.a(com.tencent.nucleus.manager.main.AssistantTabActivity, boolean):boolean */
    public void onAnimationStart(Animation animation) {
        boolean unused = this.f2948a.aW = false;
    }

    public void onAnimationRepeat(Animation animation) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.main.AssistantTabActivity.a(com.tencent.nucleus.manager.main.AssistantTabActivity, boolean):boolean
     arg types: [com.tencent.nucleus.manager.main.AssistantTabActivity, int]
     candidates:
      com.tencent.nucleus.manager.main.AssistantTabActivity.a(com.tencent.nucleus.manager.main.AssistantTabActivity, java.lang.Class):java.lang.Class
      com.tencent.nucleus.manager.main.AssistantTabActivity.a(com.tencent.nucleus.manager.main.AssistantTabActivity, float):void
      com.tencent.nucleus.manager.main.AssistantTabActivity.a(com.tencent.nucleus.manager.main.AssistantTabActivity, int):void
      com.tencent.nucleus.manager.main.AssistantTabActivity.a(com.tencent.nucleus.manager.main.AssistantTabActivity, java.lang.String):void
      com.tencent.nucleus.manager.main.AssistantTabActivity.a(boolean, long):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.nucleus.manager.main.AssistantTabActivity.a(com.tencent.nucleus.manager.main.AssistantTabActivity, boolean):boolean */
    public void onAnimationEnd(Animation animation) {
        boolean unused = this.f2948a.aW = true;
    }
}
