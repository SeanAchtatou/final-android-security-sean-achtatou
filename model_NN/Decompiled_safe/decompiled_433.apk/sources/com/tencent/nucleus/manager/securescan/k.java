package com.tencent.nucleus.manager.securescan;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.component.txscrollview.TXTabBarLayout;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class k extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f2989a;
    final /* synthetic */ int b;
    final /* synthetic */ int c;
    final /* synthetic */ e d;

    k(e eVar, SimpleAppModel simpleAppModel, int i, int i2) {
        this.d = eVar;
        this.f2989a = simpleAppModel;
        this.b = i;
        this.c = i2;
    }

    public void onTMAClick(View view) {
        this.d.a(this.f2989a, (int) TXTabBarLayout.TABITEM_TIPS_TEXT_ID);
    }

    public STInfoV2 getStInfo() {
        return STInfoBuilder.buildSTInfo(this.d.f, this.f2989a, this.d.a(this.b, this.c, (int) TXTabBarLayout.TABITEM_TIPS_TEXT_ID), 200, null);
    }
}
