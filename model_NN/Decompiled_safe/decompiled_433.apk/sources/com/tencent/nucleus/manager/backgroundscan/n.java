package com.tencent.nucleus.manager.backgroundscan;

import com.tencent.assistant.db.table.f;
import com.tencent.assistant.utils.XLog;
import com.tencent.nucleus.manager.backgroundscan.BackgroundScanManager;
import com.tencent.securemodule.impl.AppInfo;
import com.tencent.securemodule.service.CloudScanListener;
import java.util.List;

/* compiled from: ProGuard */
class n implements CloudScanListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BackgroundScanManager f2835a;

    n(BackgroundScanManager backgroundScanManager) {
        this.f2835a = backgroundScanManager;
    }

    @Deprecated
    public void onRiskFound() {
    }

    public void onRiskFoud(List<AppInfo> list) {
        BackgroundScan a2 = this.f2835a.a((byte) 6);
        a2.e = (long) list.size();
        f.a().b(a2);
        this.f2835a.i.put((byte) 6, BackgroundScanManager.SStatus.finish);
    }

    public void onFinish(int i) {
        XLog.i("BackgroundScan", "<scan> virus scan finish !");
        this.f2835a.i.put((byte) 6, BackgroundScanManager.SStatus.finish);
    }
}
