package com.tencent.nucleus.manager.backgroundscan;

import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.at;
import com.tencent.nucleus.manager.backgroundscan.BackgroundScanManager;
import com.tencent.nucleus.manager.spaceclean.ac;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.optimize.f;
import org.json.JSONException;

/* compiled from: ProGuard */
class k extends f {

    /* renamed from: a  reason: collision with root package name */
    long f2832a = 0;
    final /* synthetic */ BackgroundScanManager b;

    k(BackgroundScanManager backgroundScanManager) {
        this.b = backgroundScanManager;
    }

    public void c() {
        XLog.i("BackgroundScan", "<scan> rubbish scan start !");
        this.f2832a = 0;
        ac.a().b();
    }

    public void a(int i) {
    }

    public void b() {
        a(this.f2832a);
        XLog.i("BackgroundScan", "<scan> rubbish scan finish, rubbish size = " + at.c(this.f2832a));
    }

    public void a() {
        a(0L);
        XLog.i("BackgroundScan", "<scan> rubbish  scan canceled !");
    }

    public void a(int i, DataEntity dataEntity) {
        try {
            this.f2832a += dataEntity.getLong("rubbish.size");
        } catch (JSONException e) {
            XLog.e("BackgroundScan", "<scan> mRubbishScanListener -> onRubbishFound", e);
        }
        ac.a().a(i, dataEntity);
    }

    private void a(long j) {
        BackgroundScan a2 = this.b.a((byte) 4);
        a2.e = j;
        com.tencent.assistant.db.table.f.a().b(a2);
        this.b.i.put((byte) 4, BackgroundScanManager.SStatus.finish);
        ac.a().c();
    }
}
