package com.tencent.nucleus.manager.usagestats;

import a.a.a;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* compiled from: ProGuard */
final class h implements Set<Map.Entry<K, V>> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f3066a;

    h(f fVar) {
        this.f3066a = fVar;
    }

    /* renamed from: a */
    public boolean add(Map.Entry<K, V> entry) {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(Collection<? extends Map.Entry<K, V>> collection) {
        int a2 = this.f3066a.a();
        for (Map.Entry entry : collection) {
            this.f3066a.a(entry.getKey(), entry.getValue());
        }
        return a2 != this.f3066a.a();
    }

    public void clear() {
        this.f3066a.c();
    }

    public boolean contains(Object obj) {
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        int a2 = this.f3066a.a(entry.getKey());
        if (a2 >= 0) {
            return a.a(this.f3066a.a(a2, 1), entry.getValue());
        }
        return false;
    }

    public boolean containsAll(Collection<?> collection) {
        for (Object contains : collection) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    public boolean isEmpty() {
        return this.f3066a.a() == 0;
    }

    public Iterator<Map.Entry<K, V>> iterator() {
        return new j(this.f3066a);
    }

    public boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    public boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    public boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    public int size() {
        return this.f3066a.a();
    }

    public Object[] toArray() {
        throw new UnsupportedOperationException();
    }

    public <T> T[] toArray(T[] tArr) {
        throw new UnsupportedOperationException();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.usagestats.f.a(java.util.Set, java.lang.Object):boolean
     arg types: [com.tencent.nucleus.manager.usagestats.h, java.lang.Object]
     candidates:
      com.tencent.nucleus.manager.usagestats.f.a(java.util.Map, java.util.Collection<?>):boolean
      com.tencent.nucleus.manager.usagestats.f.a(int, int):java.lang.Object
      com.tencent.nucleus.manager.usagestats.f.a(int, java.lang.Object):V
      com.tencent.nucleus.manager.usagestats.f.a(java.lang.Object, java.lang.Object):void
      com.tencent.nucleus.manager.usagestats.f.a(java.lang.Object[], int):T[]
      com.tencent.nucleus.manager.usagestats.f.a(java.util.Set, java.lang.Object):boolean */
    public boolean equals(Object obj) {
        return f.a((Set) this, obj);
    }

    public int hashCode() {
        int a2 = this.f3066a.a() - 1;
        int i = 0;
        while (a2 >= 0) {
            Object a3 = this.f3066a.a(a2, 0);
            Object a4 = this.f3066a.a(a2, 1);
            a2--;
            i += (a4 == null ? 0 : a4.hashCode()) ^ (a3 == null ? 0 : a3.hashCode());
        }
        return i;
    }
}
