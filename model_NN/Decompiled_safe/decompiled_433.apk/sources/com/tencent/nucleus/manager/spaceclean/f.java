package com.tencent.nucleus.manager.spaceclean;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class f extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BigFileCleanActivity f3029a;

    f(BigFileCleanActivity bigFileCleanActivity) {
        this.f3029a = bigFileCleanActivity;
    }

    public void onTMAClick(View view) {
        if (this.f3029a.x.getFooterViewEnable()) {
            this.f3029a.z.b();
            this.f3029a.z();
            this.f3029a.A();
            RubbishItemView.b = true;
            this.f3029a.x.updateContent(this.f3029a.getString(R.string.apkmgr_is_deleting));
            this.f3029a.x.setFooterViewEnable(false);
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3029a.n, 200);
        buildSTInfo.scene = STConst.ST_PAGE_BIG_FILE_CLEAN_STEWARD;
        buildSTInfo.slotId = "05_001";
        return buildSTInfo;
    }
}
