package com.tencent.nucleus.manager.securescan;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.tencent.assistant.manager.t;

/* compiled from: ProGuard */
class o extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ScanningAnimView f2993a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    o(ScanningAnimView scanningAnimView, Looper looper) {
        super(looper);
        this.f2993a = scanningAnimView;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 11:
                break;
            default:
                return;
        }
        while (this.f2993a.r < 1) {
            if (this.f2993a.m = this.f2993a.a(this.f2993a.a(this.f2993a.s)) == null) {
                ScanningAnimView.c(this.f2993a);
            } else if (!this.f2993a.m.isRecycled()) {
                try {
                    Bitmap unused = this.f2993a.n = this.f2993a.m.copy(this.f2993a.m.getConfig(), true);
                } catch (Throwable th) {
                    t.a().b();
                }
                int unused2 = this.f2993a.r = 1;
                ScanningAnimView.c(this.f2993a);
                if (this.f2993a.t.booleanValue()) {
                    Message message2 = new Message();
                    message2.what = 10;
                    this.f2993a.y.sendMessage(message2);
                }
            } else {
                return;
            }
        }
    }
}
