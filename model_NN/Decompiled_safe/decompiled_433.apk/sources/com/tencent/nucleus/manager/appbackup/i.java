package com.tencent.nucleus.manager.appbackup;

import android.view.View;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class i implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BackupApplistDialog f2808a;

    i(BackupApplistDialog backupApplistDialog) {
        this.f2808a = backupApplistDialog;
    }

    public void onClick(View view) {
        if (this.f2808a.i != null) {
            this.f2808a.i.a(this.f2808a.d.b());
        }
        l.a(new STInfoV2(STConst.ST_PAGE_APP_BACKUP_APPLIST, "04_001", 0, STConst.ST_DEFAULT_SLOT, 200));
        this.f2808a.dismiss();
    }
}
