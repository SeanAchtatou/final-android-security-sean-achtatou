package com.tencent.nucleus.manager.spaceclean;

import com.tencent.assistant.utils.XLog;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.optimize.f;
import java.lang.ref.WeakReference;

/* compiled from: ProGuard */
class am extends f {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpaceScanManager f3024a;
    private WeakReference<f> b;

    private am(SpaceScanManager spaceScanManager) {
        this.f3024a = spaceScanManager;
        this.b = null;
    }

    /* synthetic */ am(SpaceScanManager spaceScanManager, af afVar) {
        this(spaceScanManager);
    }

    public void a(f fVar) {
        if (fVar == null) {
            this.b = null;
        } else {
            this.b = new WeakReference<>(fVar);
        }
    }

    public void a(int i, DataEntity dataEntity) {
        if (this.b != null && this.b.get() != null) {
            this.b.get().a(i, dataEntity);
        }
    }

    public void a() {
        XLog.d("miles", "SpaceScanManager >> onScanCanceled");
        if (this.b != null && this.b.get() != null) {
            this.b.get().a();
        }
    }

    public void b() {
        boolean unused = this.f3024a.m = false;
        XLog.d("miles", "SpaceScanManager >> onScanFinished");
        if (this.b != null && this.b.get() != null) {
            this.b.get().b();
        }
    }

    public void a(int i) {
        if (this.b != null && this.b.get() != null) {
            this.b.get().a(i);
        }
    }

    public void c() {
        XLog.d("miles", "SpaceScanManager >> onScanStarted");
        if (this.b != null && this.b.get() != null) {
            this.b.get().c();
        }
    }
}
