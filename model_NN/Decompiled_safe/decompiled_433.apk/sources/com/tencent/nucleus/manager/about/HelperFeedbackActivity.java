package com.tencent.nucleus.manager.about;

import android.os.Bundle;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.SecondNavigationTitleView;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.connect.common.Constants;
import java.util.Timer;

/* compiled from: ProGuard */
public class HelperFeedbackActivity extends BaseActivity implements ITXRefreshListViewListener {
    private af A;
    /* access modifiers changed from: private */
    public String B = null;
    private View.OnClickListener C = new v(this);
    private TextWatcher D = new w(this);
    private View.OnKeyListener E = new x(this);
    private View.OnClickListener F = new y(this);
    private l G = new aa(this);
    private ad H = new ab(this);
    private SecondNavigationTitleView n;
    private LinearLayout u;
    /* access modifiers changed from: private */
    public FeedbackInputView v;
    /* access modifiers changed from: private */
    public TXGetMoreListView w;
    /* access modifiers changed from: private */
    public View x;
    /* access modifiers changed from: private */
    public HelperFeedbackAdapter y;
    /* access modifiers changed from: private */
    public LoadingView z;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_feedback);
        v();
        if (n.a().b() <= 0) {
            u();
        } else {
            this.v.clearFocus();
            this.z.setVisibility(0);
        }
        n.a().register(this.G);
        n.a().c();
        n.a().e();
        this.A = new af();
        this.A.register(this.H);
    }

    private void v() {
        this.z = (LoadingView) findViewById(R.id.loading_view);
        this.n = (SecondNavigationTitleView) findViewById(R.id.title_view);
        this.n.setActivityContext(this);
        this.n.setTitle(getString(R.string.help_feedback));
        this.n.hiddeSearch();
        this.u = (LinearLayout) findViewById(R.id.feedback_content_view);
        this.u.setVisibility(0);
        this.y = new HelperFeedbackAdapter(this, null);
        this.y.a(this.C);
        this.w = (TXGetMoreListView) findViewById(R.id.feedback_list);
        this.w.setRefreshListViewListener(this);
        this.w.setAdapter(this.y);
        this.w.setNeedShowSeaLevel(false);
        this.w.setDivider(null);
        this.v = (FeedbackInputView) findViewById(R.id.feedback_input_view);
        this.v.a(this.D);
        this.v.setOnKeyListener(this.E);
        this.v.a(this.F);
        this.x = findViewById(R.id.feedback_line);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        n.a().unregister(this.G);
        this.A.unregister(this.H);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        t();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0 || motionEvent.getAction() == 2) {
            t();
        }
        return super.onTouchEvent(motionEvent);
    }

    public void t() {
        this.v.a().setFocusable(true);
        this.v.a().setFocusableInTouchMode(true);
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
        if (inputMethodManager != null && inputMethodManager.isActive() && getCurrentFocus() != null) {
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 2);
        }
    }

    public void u() {
        new Timer().schedule(new z(this), 500);
    }

    /* access modifiers changed from: private */
    public void w() {
        String obj = this.v.a().getText().toString();
        if (TextUtils.isEmpty(obj.trim())) {
            this.v.a().setText(Constants.STR_EMPTY);
            this.v.a().setSelection(0);
            Toast.makeText(this, (int) R.string.feedback_text_empty, 0).show();
        } else if (!obj.equals(this.B)) {
            this.B = obj;
            this.A.a(obj);
        }
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (TXScrollViewBase.ScrollState.ScrollState_FromEnd == scrollState) {
            n.a().d();
        }
    }

    public boolean onMenuOpened(int i, Menu menu) {
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
}
