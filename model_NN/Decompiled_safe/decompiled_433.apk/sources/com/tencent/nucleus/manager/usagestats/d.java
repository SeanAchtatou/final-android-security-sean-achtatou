package com.tencent.nucleus.manager.usagestats;

import java.util.Map;

/* compiled from: ProGuard */
class d extends f<K, V> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f3063a;

    d(c cVar) {
        this.f3063a = cVar;
    }

    /* access modifiers changed from: protected */
    public int a() {
        return this.f3063a.i;
    }

    /* access modifiers changed from: protected */
    public Object a(int i, int i2) {
        return this.f3063a.h[(i << 1) + i2];
    }

    /* access modifiers changed from: protected */
    public int a(Object obj) {
        return obj == null ? this.f3063a.a() : this.f3063a.a(obj, obj.hashCode());
    }

    /* access modifiers changed from: protected */
    public int b(Object obj) {
        return this.f3063a.a(obj);
    }

    /* access modifiers changed from: protected */
    public Map<K, V> b() {
        return this.f3063a;
    }

    /* access modifiers changed from: protected */
    public void a(K k, V v) {
        this.f3063a.put(k, v);
    }

    /* access modifiers changed from: protected */
    public V a(int i, V v) {
        return this.f3063a.a(i, v);
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        this.f3063a.d(i);
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.f3063a.clear();
    }
}
