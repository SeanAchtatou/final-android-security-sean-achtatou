package com.tencent.nucleus.manager.appbackup;

import android.view.View;
import android.widget.TextView;
import com.tencent.assistant.component.AppIconView;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;

/* compiled from: ProGuard */
class g {

    /* renamed from: a  reason: collision with root package name */
    View f2806a;
    TextView b;
    AppIconView c;
    TextView d;
    TextView e;
    TextView f;
    DownloadButton g;
    ListItemInfoView h;
    AppBackupAppItemInfo i;

    private g() {
    }

    /* synthetic */ g(c cVar) {
        this();
    }
}
