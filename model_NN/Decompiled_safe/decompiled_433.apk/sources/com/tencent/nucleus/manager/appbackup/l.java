package com.tencent.nucleus.manager.appbackup;

import android.view.View;
import android.widget.ProgressBar;
import com.tencent.assistant.protocol.jce.BackupDevice;

/* compiled from: ProGuard */
class l implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BackupDevice f2810a;
    final /* synthetic */ ProgressBar b;
    final /* synthetic */ int c;
    final /* synthetic */ BackupDeviceAdapter d;

    l(BackupDeviceAdapter backupDeviceAdapter, BackupDevice backupDevice, ProgressBar progressBar, int i) {
        this.d = backupDeviceAdapter;
        this.f2810a = backupDevice;
        this.b = progressBar;
        this.c = i;
    }

    public void onClick(View view) {
        if (this.d.c == -1) {
            this.d.d.a(this.f2810a);
            this.b.setVisibility(0);
            int unused = this.d.c = this.c;
        }
    }
}
