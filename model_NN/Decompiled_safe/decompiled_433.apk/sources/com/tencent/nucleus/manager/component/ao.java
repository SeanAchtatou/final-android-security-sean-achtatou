package com.tencent.nucleus.manager.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class ao extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UserAppListView f2860a;

    ao(UserAppListView userAppListView) {
        this.f2860a = userAppListView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.component.UserAppListView.a(com.tencent.nucleus.manager.component.UserAppListView, boolean):boolean
     arg types: [com.tencent.nucleus.manager.component.UserAppListView, int]
     candidates:
      com.tencent.nucleus.manager.component.UserAppListView.a(com.tencent.nucleus.manager.component.UserAppListView, int):void
      com.tencent.nucleus.manager.component.UserAppListView.a(com.tencent.nucleus.manager.component.UserAppListView, com.tencent.assistant.localres.model.LocalApkInfo):void
      com.tencent.nucleus.manager.component.UserAppListView.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, int):void
      com.tencent.nucleus.manager.component.UserAppListView.a(com.tencent.nucleus.manager.component.UserAppListView, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.apkuninstall.UserInstalledAppListAdapter.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.tencent.nucleus.manager.apkuninstall.UserInstalledAppListAdapter.a(int, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.nucleus.manager.apkuninstall.UserInstalledAppListAdapter.a(android.widget.TextView, long):void
      com.tencent.nucleus.manager.apkuninstall.UserInstalledAppListAdapter.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, int):void
      com.tencent.nucleus.manager.apkuninstall.UserInstalledAppListAdapter.a(boolean, boolean):void */
    public void onTMAClick(View view) {
        if (this.f2860a.d.getFooterViewEnable() && this.f2860a.m.size() > 0) {
            this.f2860a.n.clear();
            this.f2860a.c((LocalApkInfo) this.f2860a.m.get(0));
            boolean unused = this.f2860a.k = true;
            boolean unused2 = this.f2860a.l = true;
            this.f2860a.i.a(this.f2860a.k, true);
            this.f2860a.i();
            this.f2860a.b(this.f2860a.m.size());
        }
    }
}
