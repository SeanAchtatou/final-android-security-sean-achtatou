package com.tencent.nucleus.manager.securescan;

import android.util.Log;
import android.view.animation.Animation;

/* compiled from: ProGuard */
class p implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ScanningAnimView f2994a;

    p(ScanningAnimView scanningAnimView) {
        this.f2994a = scanningAnimView;
    }

    public void onAnimationStart(Animation animation) {
        Log.i("ScanningAnimView", "onAnimationStart:currentViewIndex=" + this.f2994a.q);
        if (this.f2994a.q == 0) {
            long unused = this.f2994a.x = System.currentTimeMillis();
            Log.i("ScanningAnimView", "diff: " + (this.f2994a.x - this.f2994a.w));
        }
        if (this.f2994a.q > 0) {
            this.f2994a.g.setFlipInterval(1500);
            if (this.f2994a.q % 2 == 0) {
                if (this.f2994a.n != null && !this.f2994a.n.isRecycled()) {
                    this.f2994a.j.setImageBitmap(this.f2994a.n);
                }
                int unused2 = this.f2994a.r = 0;
                this.f2994a.v.sendEmptyMessageDelayed(11, 100);
            } else {
                if (this.f2994a.n != null && !this.f2994a.n.isRecycled()) {
                    this.f2994a.k.setImageBitmap(this.f2994a.n);
                }
                int unused3 = this.f2994a.r = 0;
                this.f2994a.v.sendEmptyMessageDelayed(11, 100);
            }
        }
        ScanningAnimView.o(this.f2994a);
    }

    public void onAnimationRepeat(Animation animation) {
        Log.i("ScanningAnimView", "onAnimationRepeat");
    }

    public void onAnimationEnd(Animation animation) {
        Log.i("ScanningAnimView", "onAnimationEnd");
    }
}
