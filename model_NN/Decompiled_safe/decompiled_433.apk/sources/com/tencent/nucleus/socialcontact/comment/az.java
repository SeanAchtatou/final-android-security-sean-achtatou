package com.tencent.nucleus.socialcontact.comment;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.PraiseReplyRequest;
import com.tencent.assistant.protocol.jce.PraiseReplyResponse;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class az extends BaseEngine<y> {
    public int a(long j, long j2, byte b) {
        PraiseReplyRequest praiseReplyRequest = new PraiseReplyRequest();
        praiseReplyRequest.b = j;
        praiseReplyRequest.f1443a = j2;
        praiseReplyRequest.c = b;
        XLog.d("comment", "CommentReplyPraiseEngine.sendRequest, PraiseReplyRequest=" + praiseReplyRequest.toString());
        return send(praiseReplyRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        PraiseReplyResponse praiseReplyResponse = (PraiseReplyResponse) jceStruct2;
        PraiseReplyRequest praiseReplyRequest = (PraiseReplyRequest) jceStruct;
        XLog.d("comment", "CommentReplyPraiseEngine.onRequestSuccessed, praiseResponse=" + (praiseReplyResponse != null ? praiseReplyResponse.toString() : "null"));
        if (praiseReplyResponse != null) {
            notifyDataChangedInMainThread(new ba(this, i, praiseReplyResponse, praiseReplyRequest));
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        PraiseReplyResponse praiseReplyResponse = (PraiseReplyResponse) jceStruct2;
        XLog.d("comment", "CommentReplyPraiseEngine.onRequestFailed, errorCode=" + i2);
        notifyDataChangedInMainThread(new bb(this, i, i2, (PraiseReplyRequest) jceStruct));
    }
}
