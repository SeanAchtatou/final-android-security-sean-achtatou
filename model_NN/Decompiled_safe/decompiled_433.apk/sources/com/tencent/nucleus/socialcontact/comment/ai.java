package com.tencent.nucleus.socialcontact.comment;

import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class ai implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentReplyListActivity f3091a;

    ai(CommentReplyListActivity commentReplyListActivity) {
        this.f3091a = commentReplyListActivity;
    }

    public void run() {
        String str;
        if (this.f3091a.T != null) {
            this.f3091a.T.setFocusable(true);
            this.f3091a.T.setFocusableInTouchMode(true);
            this.f3091a.T.requestFocus();
            this.f3091a.T.setText(Constants.STR_EMPTY);
            if (this.f3091a.V == 0) {
                str = "回复" + "楼主";
            } else {
                str = "回复" + this.f3091a.V + "楼";
            }
            this.f3091a.T.setHint(str);
            this.f3091a.A();
        }
    }
}
