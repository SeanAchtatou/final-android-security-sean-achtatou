package com.tencent.nucleus.socialcontact.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.plugin.PluginHelper;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.i;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class h extends f {
    public static String b = "com.tencent.mobileassistant_login";
    private static h c;

    public static synchronized h h() {
        h hVar;
        synchronized (h.class) {
            if (c == null) {
                c = new h();
            }
            hVar = c;
        }
        return hVar;
    }

    private h() {
    }

    public void d() {
        if (this.f3160a == null || !this.f3160a.containsKey(AppConst.KEY_FROM_TYPE) || this.f3160a.getInt(AppConst.KEY_FROM_TYPE) != 5) {
            int requireInstall = PluginHelper.requireInstall(b);
            if (requireInstall == 1) {
                PluginInfo a2 = i.b().a(b);
                if (this.f3160a != null) {
                    this.f3160a.putInt("pkgVersion", a2.getVersion());
                }
                a("login", AstApp.m(), this.f3160a);
            } else if (requireInstall == 0) {
                j();
            } else if (requireInstall == -1) {
                j();
            }
        } else {
            l.a(new STInfoV2(STConst.ST_PAGE_LOGIN_FROM_QQ, "03_001", STConst.ST_PAGE_LOGIN_FROM_QQ, STConst.ST_DEFAULT_SLOT, 100));
            a("loginWithA1", this.f3160a);
        }
    }

    private void j() {
        Intent intent = new Intent();
        Context m = AstApp.m() != null ? AstApp.m() : AstApp.i();
        intent.setClass(m, PluginLoadingActivity.class);
        if (!(m instanceof Activity)) {
            intent.setFlags(268435456);
        }
        m.startActivity(intent);
    }

    public AppConst.LoginEgnineType e() {
        return AppConst.LoginEgnineType.ENGINE_MOBILE_QQ;
    }

    public void f() {
        a("quit", new Object[0]);
    }

    public void g() {
        a("loadTicket", new Object[0]);
    }

    public void i() {
        d();
    }

    private void a(String str, Object... objArr) {
        XLog.d("LoginMoblieQEngine", "reflectPluginMethod--methodName = " + str);
        TemporaryThreadManager.get().start(new i(this, str, objArr));
    }
}
