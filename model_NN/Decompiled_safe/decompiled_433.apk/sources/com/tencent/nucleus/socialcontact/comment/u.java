package com.tencent.nucleus.socialcontact.comment;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

/* compiled from: ProGuard */
class u implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentReplyActivity f3137a;

    u(CommentReplyActivity commentReplyActivity) {
        this.f3137a = commentReplyActivity;
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
        String substring;
        this.f3137a.n.setEnabled(!TextUtils.isEmpty(editable.toString()));
        if (this.f3137a.u.getLineCount() > 6) {
            String obj = editable.toString();
            int selectionStart = this.f3137a.u.getSelectionStart();
            if (selectionStart != this.f3137a.u.getSelectionEnd() || selectionStart >= obj.length() || selectionStart < 1) {
                substring = obj.substring(0, obj.length() - 1);
            } else {
                substring = obj.substring(0, selectionStart - 1) + obj.substring(selectionStart);
            }
            this.f3137a.u.setText(substring);
            this.f3137a.u.setSelection(this.f3137a.u.getText().length());
        }
    }
}
