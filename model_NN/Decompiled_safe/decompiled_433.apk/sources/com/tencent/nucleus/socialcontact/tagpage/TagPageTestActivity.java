package com.tencent.nucleus.socialcontact.tagpage;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.t;

/* compiled from: ProGuard */
public class TagPageTestActivity extends BaseActivity implements View.OnClickListener, UIEventListener {
    private Context n = null;
    private AstApp u = null;
    private Button v = null;
    private Button w = null;
    private Button x = null;
    private Button y = null;
    private Button z = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.tag_page_test_activity_layout);
        this.n = this;
        this.u = AstApp.i();
        this.v = (Button) findViewById(R.id.btn);
        this.v.setOnClickListener(this);
        this.w = (Button) findViewById(R.id.btnGet);
        this.w.setOnClickListener(this);
        this.x = (Button) findViewById(R.id.btnPause);
        this.x.setOnClickListener(this);
        this.y = (Button) findViewById(R.id.btnDel);
        this.y.setOnClickListener(this);
        this.z = (Button) findViewById(R.id.btnSDVolume);
        this.z.setOnClickListener(this);
        u();
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_START:
                XLog.i("TagPageTestActivity", "video download start");
                return;
            case EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOADING:
                if (message.obj instanceof TPVideoDownInfo) {
                    XLog.i("TagPageTestActivity", "video downloading, getDownProgress = " + ((TPVideoDownInfo) message.obj).a());
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_PAUSE:
                XLog.i("TagPageTestActivity", "video pause");
                return;
            case EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_QUEUING:
                XLog.i("TagPageTestActivity", "video queuing");
                return;
            case EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_FAIL:
                XLog.i("TagPageTestActivity", "video failed");
                return;
            case EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_ADD:
                XLog.i("TagPageTestActivity", "video add");
                return;
            case EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_DELETE:
                XLog.i("TagPageTestActivity", "video delete");
                return;
            case EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_SUCC:
                XLog.i("TagPageTestActivity", "video download succeed");
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        v();
    }

    private void u() {
        this.u.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_START, this);
        this.u.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOADING, this);
        this.u.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_PAUSE, this);
        this.u.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_QUEUING, this);
        this.u.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_FAIL, this);
        this.u.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_ADD, this);
        this.u.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_DELETE, this);
        this.u.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_SUCC, this);
    }

    private void v() {
        this.u.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_START, this);
        this.u.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOADING, this);
        this.u.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_PAUSE, this);
        this.u.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_QUEUING, this);
        this.u.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_FAIL, this);
        this.u.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_ADD, this);
        this.u.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_DELETE, this);
        this.u.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_SUCC, this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.socialcontact.tagpage.l.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.nucleus.socialcontact.tagpage.l.a(com.tencent.nucleus.socialcontact.tagpage.l, java.lang.String):void
      com.tencent.nucleus.socialcontact.tagpage.l.a(java.lang.String, boolean):boolean */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn /*2131165596*/:
                TPVideoDownInfo b = l.c().b("8411AB7515B5C8EA9700AE1EBEAA6EC1");
                if (b == null) {
                    b = new TPVideoDownInfo();
                    b.b = "http://61.155.143.152/music.qqvideo.tc.qq.com/l001686mz5n.mp4?type=mp4&fmt=mp4&vkey=A4D0BDBAF486522E3822D3D86DA97602C1D9DDAA85808E32ECCCEAC560A2CDCF33DFB569A0887AEFA8B832DC8CC2A35BC0B1ED40F3AF7295";
                    b.f3171a = b.a(b.b);
                }
                if (l.c().a(b)) {
                    XLog.i("xxx", "startDownload succeed!!!");
                    return;
                } else {
                    XLog.i("xxx", "startDownload failed!!!");
                    return;
                }
            case R.id.btnPause /*2131166591*/:
                l.c().a("8411AB7515B5C8EA9700AE1EBEAA6EC1");
                return;
            case R.id.btnGet /*2131166592*/:
                XLog.i("TagPageTestActivity", "tpVideoDownInfo2 = " + l.c().b("8411AB7515B5C8EA9700AE1EBEAA6EC1"));
                return;
            case R.id.btnDel /*2131166593*/:
                l.c().a("8411AB7515B5C8EA9700AE1EBEAA6EC1", true);
                return;
            case R.id.btnSDVolume /*2131166594*/:
                t();
                return;
            default:
                return;
        }
    }

    public long t() {
        long[] s = t.s();
        XLog.i("TagPageTestActivity", "[getSDCardMemory] ---> SDCard 可用 : " + (((double) s[1]) / 1048576.0d) + " MB");
        XLog.i("TagPageTestActivity", "[getSDCardMemory] ---> SDCard 总共: " + (((double) s[0]) / 1048576.0d) + " MB");
        return (long) (((double) s[1]) / 1048576.0d);
    }
}
