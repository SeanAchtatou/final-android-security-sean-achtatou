package com.tencent.nucleus.socialcontact.comment;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.by;
import com.tencent.assistant.utils.r;

/* compiled from: ProGuard */
public class CommentReplyListFooterView extends RelativeLayout {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f3080a;
    private Button b;
    private Button c;
    /* access modifiers changed from: private */
    public EditText d;
    /* access modifiers changed from: private */
    public TextView e;
    private FrameLayout f;
    private RelativeLayout g;

    public CommentReplyListFooterView(Context context) {
        super(context);
        a(context);
    }

    public CommentReplyListFooterView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    private void a(Context context) {
        this.f3080a = context;
        View inflate = LayoutInflater.from(this.f3080a).inflate((int) R.layout.comment_reply_footer, this);
        this.b = (Button) inflate.findViewById(R.id.appdetail_reply_btn_for_login);
        this.c = (Button) inflate.findViewById(R.id.appdetail_reply_btn_for_send);
        this.d = (EditText) inflate.findViewById(R.id.input_more_reply);
        this.e = (TextView) inflate.findViewById(R.id.over_reply_tips);
        this.f = (FrameLayout) inflate.findViewById(R.id.appdetail_reply_btn_for_login_area);
        this.g = (RelativeLayout) inflate.findViewById(R.id.appdetail_reply_area);
        this.d.setLongClickable(false);
        if (r.d() >= 11) {
            this.d.setCustomSelectionActionModeCallback(new aw(this));
            this.d.setTextIsSelectable(false);
        }
        this.d.setOnClickListener(new ax(this));
        this.d.addTextChangedListener(new ay(this));
    }

    public void a(boolean z) {
        this.c.setClickable(z);
        this.c.setEnabled(z);
        if (z) {
            this.c.setTextColor(this.f3080a.getResources().getColor(R.color.white));
        } else {
            this.c.setTextColor(this.f3080a.getResources().getColor(R.color.comment_over_txt_tips));
        }
    }

    public void a() {
        this.d.setGravity(16);
        this.d.setPadding(by.a(this.f3080a, 8.0f), 0, 0, 0);
    }

    public void a(View.OnClickListener onClickListener) {
        this.b.setOnClickListener(onClickListener);
        this.c.setOnClickListener(onClickListener);
    }

    public void b(boolean z) {
        if (z) {
            this.f.setVisibility(8);
            this.g.setVisibility(0);
            return;
        }
        this.f.setVisibility(0);
        this.g.setVisibility(8);
    }

    public EditText b() {
        return this.d;
    }
}
