package com.tencent.nucleus.socialcontact.tagpage;

import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.XLog;
import java.io.File;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;

/* compiled from: ProGuard */
class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f3207a;

    m(l lVar) {
        this.f3207a = lVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.socialcontact.tagpage.l.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.nucleus.socialcontact.tagpage.l.a(com.tencent.nucleus.socialcontact.tagpage.l, java.lang.String):void
      com.tencent.nucleus.socialcontact.tagpage.l.a(java.lang.String, boolean):boolean */
    public void run() {
        int size;
        int i = 10;
        int i2 = 0;
        File file = new File(FileUtil.getTPVideoDir());
        if (file.isDirectory()) {
            File[] listFiles = file.listFiles(new n(this));
            XLog.i("TPVideoManager", "****** 排序前 ******");
            TreeMap treeMap = new TreeMap(new s(null));
            if (listFiles != null) {
                for (File file2 : listFiles) {
                    if (file2 != null) {
                        XLog.i("TPVideoManager", "key = " + file2.lastModified() + ", value = " + file2);
                        treeMap.put(file2, file2.getName());
                    }
                }
            }
            XLog.i("TPVideoManager", "****** 排序后 ******");
            for (File file3 : treeMap.keySet()) {
                XLog.i("TPVideoManager", "key = " + file3.lastModified() + ", value = " + file3);
            }
            Set keySet = treeMap.keySet();
            if (keySet != null && (size = keySet.size()) > 50) {
                int i3 = size - 50;
                Iterator it = keySet.iterator();
                int i4 = 0;
                while (true) {
                    int i5 = i4 + 1;
                    if (i4 >= i3 || it == null || !it.hasNext()) {
                        break;
                    }
                    File file4 = (File) it.next();
                    XLog.i("TPVideoManager", "[filecount] ---> delCount = " + i5 + ", filename = " + file4.getName());
                    file4.delete();
                    it.remove();
                    this.f3207a.a(file4.getName(), true);
                    i4 = i5;
                }
            }
            if (this.f3207a.b() < 50 && keySet != null) {
                if (10 >= keySet.size()) {
                    i = keySet.size();
                }
                Iterator it2 = keySet.iterator();
                while (true) {
                    int i6 = i2;
                    i2 = i6 + 1;
                    if (i6 < i && it2 != null && it2.hasNext()) {
                        File file5 = (File) it2.next();
                        XLog.i("TPVideoManager", "[SDCardVolume ] --->delCount = " + i2 + ", filename = " + file5.getName());
                        file5.delete();
                        it2.remove();
                        this.f3207a.a(file5.getName(), true);
                    } else {
                        return;
                    }
                }
            }
        }
    }
}
