package com.tencent.nucleus.socialcontact.usercenter;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.ActionUrl;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.STPageInfo;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.socialcontact.a;
import com.tencent.nucleus.socialcontact.login.j;
import com.tencent.open.SocialConstants;
import com.tencent.pangu.link.b;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
public class UserCenterActivityV2 extends BaseActivity implements AdapterView.OnItemClickListener, UIEventListener {
    private static final String n = UserCenterActivityV2.class.getCanonicalName();
    private static boolean u = false;
    private HashMap<String, ArrayList<Object>> A = new HashMap<>();
    private boolean B = false;
    private Context v = null;
    private UcTitleView w = null;
    private UcAccountView x = null;
    private ListView y = null;
    private UserCenterAdapter z = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.v = this;
        try {
            setContentView((int) R.layout.activity_user_center_v2);
            if (!j.a().j()) {
                finish();
                return;
            }
            t();
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_GET_USER_ACTIVITY_SUCCESS, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGOUT, this);
            a(STConst.ST_DEFAULT_SLOT, Constants.STR_EMPTY, 100);
        } catch (Throwable th) {
            this.B = true;
            t.a().b();
            finish();
        }
    }

    private void t() {
        this.w = (UcTitleView) findViewById(R.id.uc_title_view);
        this.w.a(this);
        this.x = (UcAccountView) findViewById(R.id.uc_account_view);
        this.x.a(this);
        this.y = (ListView) findViewById(R.id.entrance_listview);
        this.y.setDivider(null);
        this.y.setOnItemClickListener(this);
        this.y.setAdapter((ListAdapter) u());
    }

    private UserCenterAdapter u() {
        this.z = new UserCenterAdapter(this);
        return this.z;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.B) {
            j.a(true);
            j.d();
            if (this.x != null) {
                this.x.a();
            }
            if (u) {
                u = false;
                XLog.i(n, "onResume ---> [requestUserCenterData]");
                new h().a();
            } else {
                j.h();
            }
            if (this.z != null) {
                this.z.a(v());
                this.z.notifyDataSetChanged();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!this.B) {
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_GET_USER_ACTIVITY_SUCCESS, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGOUT, this);
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS:
                if (this.x != null) {
                    this.x.b();
                    XLog.i(n, "[UI_EVENT_GET_USERINFO_SUCCESS] : refreshUserInfo");
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_LOGOUT:
                XLog.i(n, "[UI_EVENT_LOGOUT] : logout");
                j.e();
                u = true;
                return;
            case EventDispatcherEnum.UI_EVENT_GET_USER_ACTIVITY_SUCCESS:
                if (this.z != null) {
                    XLog.i(n, "[handleUIEvent] ---> notifyDataSetChanged ");
                    this.z.a(v());
                    this.z.notifyDataSetChanged();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void a(String str, String str2, int i) {
        XLog.i(n, "[logReport] ---> actionId = " + i + ", slotId = " + str + ", extraData = " + str2);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, i);
        buildSTInfo.slotId = str;
        buildSTInfo.extraData = str2;
        l.a(buildSTInfo);
    }

    public int f() {
        return STConst.ST_PAGE_USER_CENTER;
    }

    public STPageInfo n() {
        this.p.f2060a = STConst.ST_PAGE_USER_CENTER;
        return this.p;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.socialcontact.usercenter.j.a(java.util.List<? extends com.qq.taf.jce.JceStruct>, boolean):void
     arg types: [java.util.List<? extends com.qq.taf.jce.JceStruct>, int]
     candidates:
      com.tencent.nucleus.socialcontact.usercenter.j.a(com.qq.taf.jce.JceStruct, int):com.tencent.nucleus.socialcontact.usercenter.a
      com.tencent.nucleus.socialcontact.usercenter.j.a(int, int):void
      com.tencent.nucleus.socialcontact.usercenter.j.a(com.tencent.assistant.protocol.jce.UserActivityItem, com.tencent.assistant.protocol.jce.UserActivityItem):boolean
      com.tencent.nucleus.socialcontact.usercenter.j.a(java.util.List<? extends com.qq.taf.jce.JceStruct>, boolean):void */
    private HashMap<String, ArrayList<Object>> v() {
        List<? extends JceStruct> a2 = j.a();
        if (a2 == null || a2.size() <= 0) {
            XLog.i(n, "[UserCenterActivityV2] ---> getData : not found jce cahce data");
            return w();
        }
        j.a(a2, false);
        HashMap<String, ArrayList<Object>> g = j.g();
        if (g == null || g.size() <= 0) {
            return w();
        }
        return g;
    }

    private HashMap<String, ArrayList<Object>> w() {
        if (this.A == null) {
            this.A = new HashMap<>();
        }
        ArrayList arrayList = new ArrayList();
        a aVar = new a();
        aVar.l = new ActionUrl("tmast://webview?url=http://gift.app.qq.com/zqb/index.html", 0);
        aVar.h = Constants.STR_EMPTY;
        aVar.i = Constants.STR_EMPTY;
        aVar.j = Constants.STR_EMPTY;
        aVar.e = 1;
        aVar.c = 1;
        aVar.d = R.drawable.uc_app_bar;
        arrayList.add(aVar);
        this.A.put("uc_list_group_1", arrayList);
        ArrayList arrayList2 = new ArrayList();
        a aVar2 = new a();
        aVar2.l = new ActionUrl("tmast://webview?url=http://gift.app.qq.com/zqb/index.html", 0);
        aVar2.h = "http://3gimg.qq.com/cafeteria/icon/zqb_personalcenter.png";
        aVar2.i = "我的任务";
        aVar2.j = Constants.STR_EMPTY;
        aVar2.e = 2;
        aVar2.c = arrayList2.size() + 1;
        aVar2.d = R.drawable.uc_task;
        arrayList2.add(aVar2);
        a aVar3 = new a();
        aVar3.l = new ActionUrl("tmast://webview?url=http://qzs.qq.com/open/mobile/giftcenter/yyb/index.html?qoback=1&v=4.0.3.6&max_age=432000&fromvia=ANDROIDYYB.GIFTCENTER.MYCENTER", 0);
        aVar3.h = "http://3gimg.qq.com/yingyongbao_yunying/wjunjin/myzone/uc_gift.png";
        aVar3.i = "我的礼包";
        aVar3.j = Constants.STR_EMPTY;
        aVar3.e = 2;
        aVar3.c = arrayList2.size() + 1;
        aVar3.d = R.drawable.uc_gift;
        arrayList2.add(aVar3);
        a aVar4 = new a();
        aVar4.l = new ActionUrl("tmast://webview?url=" + a.a(), 0);
        aVar4.h = "http://3gimg.qq.com/yingyongbao_yunying/wjunjin/myzone/uc_app_bar.png";
        aVar4.i = "我的应用吧";
        aVar4.j = Constants.STR_EMPTY;
        aVar4.e = 2;
        aVar4.c = arrayList2.size() + 1;
        aVar4.d = R.drawable.uc_app_bar;
        arrayList2.add(aVar4);
        this.A.put("uc_list_group_2", arrayList2);
        ArrayList arrayList3 = new ArrayList();
        a aVar5 = new a();
        aVar5.l = new ActionUrl("tmast://guessfavor", 0);
        aVar5.h = Constants.STR_EMPTY;
        aVar5.i = "猜你喜欢";
        aVar5.j = Constants.STR_EMPTY;
        aVar5.e = 3;
        aVar5.c = arrayList3.size() + 1;
        aVar5.d = R.drawable.uc_guess_favor;
        arrayList3.add(aVar5);
        this.A.put("uc_list_group_3", arrayList3);
        return this.A;
    }

    private String a(String str, int i, int i2) {
        if (TextUtils.isEmpty(str)) {
            return Constants.STR_EMPTY;
        }
        if (!"webview".equals(b(str))) {
            return str;
        }
        String b = b(str, SocialConstants.PARAM_URL);
        if (TextUtils.isEmpty(b)) {
            return Constants.STR_EMPTY;
        }
        StringBuilder sb = new StringBuilder(b);
        if (b.contains("?")) {
            sb.append("&groupid=" + i);
            sb.append("&index=" + i2);
        } else {
            sb.append("?groupid=" + i);
            sb.append("&index=" + i2);
        }
        if (str.contains(b)) {
            return str.replace(b, URLEncoder.encode(sb.toString()));
        }
        if (str.contains(URLEncoder.encode(b))) {
            return str.replace(URLEncoder.encode(b), URLEncoder.encode(sb.toString()));
        }
        return str;
    }

    private String c(String str, String str2) {
        String str3;
        int indexOf;
        if (str2 == null || str2.equals(Constants.STR_EMPTY) || (indexOf = str.indexOf((str3 = str2 + "="))) < 0) {
            return null;
        }
        int length = indexOf + str3.length();
        int indexOf2 = str.indexOf("&", length);
        if (indexOf2 == -1) {
            indexOf2 = str.length();
        }
        return str.substring(length, indexOf2);
    }

    public String b(String str, String str2) {
        return c(URLDecoder.decode(str), str2);
    }

    public String b(String str) {
        Uri parse = Uri.parse(str);
        if (parse == null || parse.getHost() == null) {
            return Constants.STR_EMPTY;
        }
        return parse.getHost().toLowerCase();
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        List<Object> a2;
        a aVar;
        if (adapterView != null && (adapterView.getAdapter() instanceof UserCenterAdapter) && (a2 = ((UserCenterAdapter) adapterView.getAdapter()).a()) != null && i < a2.size()) {
            Object obj = a2.get(i);
            if ((obj instanceof a) && (aVar = (a) obj) != null) {
                j.a(aVar.e, aVar.c);
                XLog.i(n, "[UserCenterActivityV2] ---> onItemClick ucd.nGroupId = " + aVar.e + ", ucd.nIndex = " + aVar.c);
                String a3 = aVar.l != null ? a(aVar.l.f1125a, aVar.e, aVar.c) : Constants.STR_EMPTY;
                XLog.i(n, "[onItemClick] ---> url = " + a3);
                Bundle bundle = new Bundle();
                if (this.v instanceof BaseActivity) {
                    bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.v).f());
                }
                bundle.putSerializable("com.tencent.assistant.ACTION_URL", aVar.l);
                b.b(this.v, a3, bundle);
                b(aVar);
            }
        }
    }

    private void b(a aVar) {
        if (aVar != null) {
            a(a(aVar), aVar.i, 200);
        }
    }

    public String a(a aVar) {
        if (aVar == null) {
            return STConst.ST_DEFAULT_SLOT;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%02d_", Integer.valueOf(aVar.e + 2)));
        sb.append(String.format("%03d_", Integer.valueOf(aVar.c)));
        if (aVar.f3225a || aVar.f > 0) {
            sb.append("01");
        } else {
            sb.append("02");
        }
        return sb.toString();
    }
}
