package com.tencent.nucleus.socialcontact.tagpage;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Message;
import android.os.Process;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.aq;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.component.ListRecommendReasonView;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.manager.component.TxTextureView;
import com.tencent.pangu.component.appdetail.process.s;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class TagPageCardAdapter extends BaseAdapter implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, TXImageView.ITXImageViewListener, UIEventListener {
    private static boolean A = false;
    private static String B = "tagpage";
    private static ArrayList<String> D = new ArrayList<>();

    /* renamed from: a  reason: collision with root package name */
    public static volatile int f3173a = -1;
    private ah C;
    /* access modifiers changed from: private */
    public Context b;
    private AstApp c;
    private ArrayList<SimpleAppModel> d;
    private b e;
    private int f;
    private String g;
    private boolean h;
    private an i;
    private String j;
    private String k;
    /* access modifiers changed from: private */
    public MediaPlayer l;
    /* access modifiers changed from: private */
    public ai m;
    private boolean n;
    private TxTextureView o;
    private SurfaceView p;
    private aj q;
    private int r;
    private int s;
    /* access modifiers changed from: private */
    public int t;
    /* access modifiers changed from: private */
    public boolean u;
    /* access modifiers changed from: private */
    public SurfaceTexture v;
    /* access modifiers changed from: private */
    public SurfaceHolder w;
    private RelativeLayout x;
    /* access modifiers changed from: private */
    public String y;
    /* access modifiers changed from: private */
    public boolean z;

    static {
        D.add("C6903");
    }

    public TagPageCardAdapter(Context context, ArrayList<SimpleAppModel> arrayList) {
        this.b = null;
        this.c = null;
        this.d = new ArrayList<>();
        this.e = null;
        this.f = STConst.ST_TAG_DETAIL_PAGE;
        this.g = Constants.STR_EMPTY;
        this.h = false;
        this.i = null;
        this.j = "http://122.228.215.139/music.qqvideo.tc.qq.com/h00162vyzpv.mp4?type=mp4&fmt=mp4&vkey=7A8C0BACEA0A87D0DFB5C43961C7ADA28AD53421B18835AA9F734CE82944AE4D073470DB1A028516AE58EDB34DFCDFB5C311B6970F1AF0A5&locid=2a8d303d-51ca-48bf-a2d7-0ff12245c4c9&size=16765963&ocid=313139116";
        this.k = "http://122.228.215.144/music.qqvideo.tc.qq.com/z0016sgwevh.mp4?type=mp4&fmt=mp4&vkey=EB89AF8F05998E5E99BD9A7765064C7F4CE78F0658BD07DC327BB37D0800B4CAC0DA5D90911455B19597774F6F8BE2C3584733E9A4675B1A&locid=4e255534-12f9-481c-890f-040290b6bb5e&size=26665772&ocid=212475820";
        this.l = null;
        this.m = null;
        this.n = true;
        this.o = null;
        this.p = null;
        this.q = null;
        this.r = 0;
        this.s = 0;
        this.t = 0;
        this.u = false;
        this.v = null;
        this.w = null;
        this.x = null;
        this.y = Constants.STR_EMPTY;
        this.z = false;
        this.C = new ah(this, null);
        this.c = AstApp.i();
        this.b = context;
        if (arrayList != null) {
            this.d = arrayList;
        }
    }

    public void a(ArrayList<SimpleAppModel> arrayList) {
        if (arrayList != null) {
            this.d = arrayList;
        }
    }

    public int getCount() {
        if (this.d != null) {
            return this.d.size();
        }
        return 0;
    }

    public Object getItem(int i2) {
        if (this.d == null || i2 < 0 || i2 >= this.d.size()) {
            return null;
        }
        return this.d.get(i2);
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i2, View view, ViewGroup viewGroup) {
        ai aiVar;
        if (view == null) {
            aiVar = new ai();
            view = LayoutInflater.from(this.b).inflate((int) R.layout.tap_page_list_item_v2, (ViewGroup) null, false);
            aiVar.f1920a = (TextView) view.findViewById(R.id.download_rate_desc);
            aiVar.b = (TXAppIconView) view.findViewById(R.id.app_icon_img);
            aiVar.c = (TextView) view.findViewById(R.id.title);
            aiVar.d = (DownloadButton) view.findViewById(R.id.state_app_btn);
            aiVar.e = (ListItemInfoView) view.findViewById(R.id.download_info);
            aiVar.f = (TextView) view.findViewById(R.id.desc);
            aiVar.g = (ListRecommendReasonView) view.findViewById(R.id.reasonView);
            aiVar.g.setVisibility(8);
            aiVar.l = (TextView) view.findViewById(R.id.sort_text);
            aiVar.n = (TagPageThumbnailView) view.findViewById(R.id.snap_shot_pics);
            aiVar.o = (RelativeLayout) view.findViewById(R.id.id_rl_video_layout);
            aiVar.p = (RelativeLayout) view.findViewById(R.id.id_rl_video_container);
            aiVar.q = (RelativeLayout) view.findViewById(R.id.id_rl_play_icon_layout);
            aiVar.r = (ImageView) view.findViewById(R.id.iv_video_play);
            aiVar.s = (LoadingView) view.findViewById(R.id.loading);
            aiVar.s.a(false);
            aiVar.t = (TXImageView) view.findViewById(R.id.iv_video_snapshot);
            aiVar.u = (TextView) view.findViewById(R.id.tv_video_src);
            aiVar.v = (ImageView) view.findViewById(R.id.iv_play_finish_anim);
            aiVar.v.setVisibility(8);
            view.setTag(aiVar);
        } else {
            aiVar = (ai) view.getTag();
        }
        aiVar.p.setTag(Integer.valueOf(i2));
        if (TagPageActivity.n != i2) {
            aiVar.p.setVisibility(8);
            aiVar.s.setVisibility(8);
            if (!aq.b()) {
                aiVar.q.setVisibility(0);
            } else {
                aiVar.q.setVisibility(8);
            }
            aiVar.t.setVisibility(0);
            if (Build.VERSION.SDK_INT >= 16) {
                aiVar.t.setImageAlpha(255);
            } else {
                aiVar.t.setAlpha(255);
            }
            aiVar.t.clearAnimation();
        }
        SimpleAppModel simpleAppModel = (SimpleAppModel) getItem(i2);
        a(aiVar, simpleAppModel, i2, a(simpleAppModel, i2));
        view.setOnClickListener(new x(this, simpleAppModel, i2));
        return view;
    }

    private void a(ai aiVar, SimpleAppModel simpleAppModel, int i2, STInfoV2 sTInfoV2) {
        boolean z2;
        CharSequence charSequence;
        if (aiVar != null && simpleAppModel != null) {
            if (aq.b()) {
                aiVar.r.setVisibility(8);
            } else {
                aiVar.r.setVisibility(0);
            }
            aiVar.b.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            aiVar.b.setTag(Integer.valueOf(i2));
            if (!this.h && i2 == 0) {
                this.h = true;
                aiVar.b.setListener(this);
            }
            aiVar.c.setText(simpleAppModel.d);
            aiVar.e.a(simpleAppModel);
            if (i2 == 0 && this.n && aq.b()) {
                this.n = false;
                a(aiVar, i2);
                TagPageActivity.n = i2;
            }
            if (!TextUtils.isEmpty(simpleAppModel.aD)) {
                XLog.i("TagPageCradAdapter", "position = " + i2 + ", appInfo.miniVideoUrl = " + simpleAppModel.aD + ", appid = " + simpleAppModel.f938a);
                aiVar.o.setVisibility(0);
                aiVar.n.setVisibility(8);
                aiVar.t.updateImageView(simpleAppModel.aF, R.drawable.tag_page_video_default_bg, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                a(aiVar, simpleAppModel.aE);
                aiVar.t.setOnClickListener(new z(this, aiVar, i2));
                aiVar.q.setOnClickListener(new aa(this, aiVar, i2));
                aiVar.p.setOnClickListener(new ab(this, aiVar, i2));
                if (!TextUtils.isEmpty(simpleAppModel.aH)) {
                    aiVar.u.setText("视频来自  " + simpleAppModel.aH);
                    aiVar.u.setVisibility(0);
                    z2 = false;
                } else {
                    aiVar.u.setVisibility(8);
                    z2 = false;
                }
            } else {
                aiVar.o.setVisibility(8);
                aiVar.u.setVisibility(8);
                if (simpleAppModel.an == null || simpleAppModel.an.size() < 2) {
                    aiVar.n.setVisibility(8);
                    z2 = false;
                } else {
                    aiVar.n.a(simpleAppModel, i2);
                    aiVar.n.setVisibility(0);
                    aiVar.n.requestLayout();
                    z2 = true;
                }
            }
            if (!TextUtils.isEmpty(simpleAppModel.ay)) {
                if (simpleAppModel.az != null) {
                    aiVar.f.setClickable(true);
                    aiVar.f.setOnClickListener(new ac(this, simpleAppModel, i2));
                } else {
                    aiVar.f.setClickable(false);
                }
                aiVar.f.setSingleLine(false);
                aiVar.f.setMaxLines(3);
                int a2 = by.a(this.b, 8.0f);
                aiVar.f.setPadding(a2, a2, a2, a2);
                aiVar.f.setLineSpacing((float) by.a(this.b, 3.0f), 1.2f);
                aiVar.f.setBackgroundResource(R.drawable.tag_page_media_recommend_bg);
                try {
                    charSequence = Html.fromHtml(simpleAppModel.ay);
                } catch (Exception e2) {
                    Exception exc = e2;
                    charSequence = simpleAppModel.ay;
                    exc.printStackTrace();
                }
                aiVar.f.setText(charSequence);
                aiVar.f.setVisibility(0);
                aiVar.n.setPadding(by.a(this.b, 1.0f), by.a(this.b, 6.0f), by.a(this.b, 10.0f), 0);
                aiVar.o.setPadding(by.a(this.b, 1.0f), by.a(this.b, 12.0f), by.a(this.b, 10.0f), by.a(this.b, 8.0f));
            } else if (!TextUtils.isEmpty(simpleAppModel.X)) {
                aiVar.f.setClickable(false);
                aiVar.f.setSingleLine(false);
                aiVar.f.setMaxLines(2);
                if (z2) {
                    aiVar.f.setPadding(0, 0, 0, 0);
                } else {
                    aiVar.f.setPadding(0, 0, 0, by.a(this.b, 10.0f));
                }
                aiVar.f.setLineSpacing((float) by.a(this.b, 1.0f), 1.0f);
                aiVar.f.setBackgroundResource(R.drawable.tag_page_editor_recommend_bg);
                aiVar.f.setText(simpleAppModel.X);
                aiVar.f.setVisibility(0);
                aiVar.n.setPadding(by.a(this.b, 1.0f), by.a(this.b, 5.0f), by.a(this.b, 10.0f), 0);
                aiVar.o.setPadding(by.a(this.b, 1.0f), 0, by.a(this.b, 10.0f), by.a(this.b, 8.0f));
            } else {
                aiVar.f.setVisibility(8);
            }
            aiVar.d.a(simpleAppModel);
            if (s.a(simpleAppModel)) {
                aiVar.d.setClickable(false);
                return;
            }
            aiVar.d.setClickable(true);
            aiVar.d.a(sTInfoV2, new ad(this), (d) null, aiVar.d, aiVar.e);
        }
    }

    /* access modifiers changed from: private */
    public String a(int i2) {
        return "07_" + bm.a(i2 + 1);
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_START:
                XLog.d("TagPageCradAdapter", "[handleUIEvent] ---> video download start");
                return;
            case EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOADING:
                if (message.obj instanceof TPVideoDownInfo) {
                    XLog.d("TagPageCradAdapter", "[handleUIEvent] ---> video downloading, getDownProgress = " + ((TPVideoDownInfo) message.obj).a());
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_PAUSE:
                XLog.d("TagPageCradAdapter", "[handleUIEvent] ---> video pause");
                return;
            case EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_QUEUING:
                XLog.d("TagPageCradAdapter", "[handleUIEvent] ---> video queuing");
                return;
            case EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_FAIL:
                if (message.obj instanceof TPVideoDownInfo) {
                    XLog.d("TagPageCradAdapter", "[handleUIEvent] ---> video failed = TPVideoDownInfo : " + ((TPVideoDownInfo) message.obj));
                    return;
                }
                XLog.d("TagPageCradAdapter", "[handleUIEvent] ---> video failed");
                return;
            case EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_ADD:
                XLog.d("TagPageCradAdapter", "[handleUIEvent] ---> video add");
                return;
            case EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_DELETE:
                XLog.d("TagPageCradAdapter", "[handleUIEvent] ---> video delete");
                return;
            case EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_SUCC:
                XLog.d("TagPageCradAdapter", "[handleUIEvent] ---> video download succeed");
                if (message.obj instanceof TPVideoDownInfo) {
                    TPVideoDownInfo tPVideoDownInfo = (TPVideoDownInfo) message.obj;
                    XLog.d("TagPageCradAdapter", "[handleUIEvent(TP_VIDEO_DOWNLOAD_SUCC)] ---> index = " + f3173a + ", tpVideoDownInfo.downIndex = " + tPVideoDownInfo.j);
                    a(tPVideoDownInfo);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void a(TPVideoDownInfo tPVideoDownInfo) {
        if (tPVideoDownInfo != null && this.v != null && f3173a == tPVideoDownInfo.j) {
            ah.a(B).post(new ae(this, tPVideoDownInfo));
        }
    }

    public void a(String str, String str2, int i2) {
        XLog.i("TagPageActivity", "[logReport] ---> actionId = " + i2 + ", slotId = " + str + ", extraData = " + str2);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b, i2);
        buildSTInfo.slotId = str;
        buildSTInfo.extraData = str2;
        l.a(buildSTInfo);
    }

    public void onTXImageViewLoadImageFinish(TXImageView tXImageView, Bitmap bitmap) {
        int i2;
        if (this.i != null && tXImageView != null) {
            Object tag = tXImageView.getTag();
            if (tag instanceof Integer) {
                i2 = ((Integer) tag).intValue();
            } else {
                i2 = -1;
            }
            this.i.a(tXImageView, bitmap, i2);
        }
    }

    public void a(an anVar) {
        this.i = anVar;
    }

    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i2) {
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        int videoWidth = mediaPlayer.getVideoWidth();
        int videoHeight = mediaPlayer.getVideoHeight();
        XLog.i("TagPageCradAdapter", "[onPrepared] ---> videoWidth = " + videoWidth + ", videoHeight = " + videoHeight);
        if (videoHeight != 0 && videoWidth != 0) {
            RelativeLayout.LayoutParams layoutParams = null;
            if (Build.VERSION.SDK_INT >= 14) {
                if (this.o != null) {
                    layoutParams = (RelativeLayout.LayoutParams) this.o.getLayoutParams();
                    if (layoutParams != null && f3173a >= 0) {
                        SimpleAppModel simpleAppModel = (SimpleAppModel) getItem(f3173a);
                        if (simpleAppModel == null || 1 != a(simpleAppModel.aE)) {
                            layoutParams.width = by.a(this.b, 130.0f);
                            layoutParams.height = by.a(this.b, 230.0f);
                        } else {
                            layoutParams.width = by.a(this.b, 230.0f);
                            layoutParams.height = by.a(this.b, 130.0f);
                        }
                    }
                    this.o.setLayoutParams(layoutParams);
                }
            } else if (this.p != null) {
                layoutParams = (RelativeLayout.LayoutParams) this.p.getLayoutParams();
                if (layoutParams != null && f3173a >= 0) {
                    SimpleAppModel simpleAppModel2 = (SimpleAppModel) getItem(f3173a);
                    if (simpleAppModel2 == null || 1 != a(simpleAppModel2.aE)) {
                        layoutParams.width = by.a(this.b, 130.0f);
                        layoutParams.height = by.a(this.b, 230.0f);
                    } else {
                        layoutParams.width = by.a(this.b, 230.0f);
                        layoutParams.height = by.a(this.b, 130.0f);
                    }
                }
                this.p.setLayoutParams(layoutParams);
            }
            mediaPlayer.start();
            if (layoutParams != null) {
                XLog.i("TagPageCradAdapter", "[onPrepared] --> mVideoWidth = " + layoutParams.width + ", mVideoHeight = " + layoutParams.height);
            }
            if (this.m != null) {
                ah.a().postDelayed(new af(this), (long) k());
                this.m.p.setVisibility(0);
                this.m.q.setVisibility(8);
            }
        }
    }

    public void a(ai aiVar, int i2) {
        ah.a().removeCallbacks(this.C);
        this.C.a(aiVar, i2);
        ah.a().postDelayed(this.C, 500);
    }

    public void a(ai aiVar, int i2, boolean z2) {
        ah.a().removeCallbacks(this.C);
        this.C.a(aiVar, i2);
        if (z2) {
            ah.a().post(this.C);
        } else {
            ah.a().postDelayed(this.C, 500);
        }
    }

    /* access modifiers changed from: private */
    public void b(ai aiVar, int i2) {
        boolean z2;
        TPVideoDownInfo b2;
        if (aiVar != null && i2 >= 0) {
            try {
                SimpleAppModel simpleAppModel = (SimpleAppModel) getItem(i2);
                if (simpleAppModel == null || (simpleAppModel != null && TextUtils.isEmpty(simpleAppModel.aD))) {
                    XLog.i("TagPageCradAdapter", "[addSurfaceView] retun");
                    f3173a = -1;
                    return;
                }
                XLog.i("TagPageCradAdapter", "[addSurfaceView] ---> position = " + i2 + ", (Integer)viewHolder.rlContainer.getTag() = " + ((Integer) aiVar.p.getTag()));
                if (i2 == ((Integer) aiVar.p.getTag()).intValue()) {
                    if (f3173a != i2) {
                        this.t = 0;
                    }
                    f3173a = i2;
                    if (this.m != null) {
                        this.m.s.setVisibility(8);
                        if (!aq.b()) {
                            this.m.q.setVisibility(0);
                        } else {
                            this.m.q.setVisibility(8);
                        }
                        this.m.v.clearAnimation();
                        this.m.v.setVisibility(8);
                        this.m.t.setVisibility(0);
                        if (Build.VERSION.SDK_INT >= 16) {
                            this.m.t.setImageAlpha(255);
                        } else {
                            this.m.t.setAlpha(255);
                        }
                        this.m.t.clearAnimation();
                    }
                    aiVar.s.setVisibility(0);
                    aiVar.s.c();
                    if (this.t == 0 || this.u) {
                        this.u = false;
                        String b3 = aq.b(simpleAppModel.aD);
                        if (TextUtils.isEmpty(b3) || (b2 = l.c().b(b3)) == null || !b2.e()) {
                            z2 = false;
                        } else {
                            z2 = true;
                        }
                        if (!z2) {
                            aiVar.s.a();
                        }
                    }
                    aiVar.p.setVisibility(0);
                    aiVar.q.setVisibility(8);
                    aiVar.t.setVisibility(0);
                    if (Build.VERSION.SDK_INT >= 16) {
                        aiVar.t.setImageAlpha(255);
                    } else {
                        aiVar.t.setAlpha(255);
                    }
                    aiVar.t.clearAnimation();
                    aiVar.v.setVisibility(8);
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(1, 1);
                    layoutParams.addRule(13, -1);
                    if (Build.VERSION.SDK_INT >= 14) {
                        this.o = new TxTextureView(this.b);
                        aiVar.p.removeAllViews();
                        aiVar.p.addView(this.o, layoutParams);
                        this.v = null;
                        this.o.setSurfaceTextureListener(new aj(this, f3173a));
                    } else {
                        this.w = null;
                        this.p = new SurfaceView(this.b);
                        this.p.getHolder().setFormat(-3);
                        aiVar.p.removeAllViews();
                        aiVar.p.addView(this.p, layoutParams);
                        SurfaceHolder holder = this.p.getHolder();
                        if (holder != null) {
                            holder.addCallback(new al(this, f3173a));
                            if (Build.VERSION.SDK_INT < 11) {
                                holder.setType(3);
                            }
                        }
                    }
                    this.m = aiVar;
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void a(RelativeLayout relativeLayout) {
        if (relativeLayout != null && this.x == null) {
            this.x = relativeLayout;
            this.x.setOnClickListener(new ag(this));
        }
    }

    public void a(boolean z2) {
        TPVideoDownInfo b2;
        int i2;
        SimpleAppModel simpleAppModel;
        if (A != z2) {
            A = z2;
            if (this.x == null) {
                return;
            }
            if (A) {
                if (this.l != null && this.l.isPlaying()) {
                    this.l.pause();
                }
                int i3 = 1;
                if (f3173a >= 0 && (simpleAppModel = (SimpleAppModel) getItem(f3173a)) != null) {
                    i3 = a(simpleAppModel.aE);
                }
                if (!TextUtils.isEmpty(this.y) && (b2 = l.c().b(this.y)) != null && b2.e()) {
                    if (this.l != null) {
                        i2 = this.l.getCurrentPosition();
                    } else {
                        i2 = 0;
                    }
                    as.a().a(this.b, this.x, i3, b2, i2);
                    this.x.setVisibility(0);
                    return;
                }
                return;
            }
            as.a().c();
            this.x.setVisibility(8);
            g();
            if (this.l != null && !this.l.isPlaying()) {
                this.l.start();
            }
        }
    }

    private void g() {
        if (this.b instanceof Activity) {
            WindowManager.LayoutParams attributes = ((Activity) this.b).getWindow().getAttributes();
            attributes.flags &= -1025;
            ((Activity) this.b).getWindow().setAttributes(attributes);
            ((Activity) this.b).getWindow().clearFlags(Process.PROC_PARENS);
        }
    }

    public boolean a() {
        if (this.x == null || this.x.getVisibility() != 0) {
            return false;
        }
        return true;
    }

    public void b() {
        if (this.l != null && this.l.isPlaying()) {
            this.l.stop();
            f3173a = -1;
            if (this.m != null) {
                this.m.p.setVisibility(8);
                this.m.s.setVisibility(8);
                if (!aq.b()) {
                    this.m.q.setVisibility(0);
                } else {
                    this.m.q.setVisibility(8);
                }
                this.m.t.setVisibility(0);
                if (Build.VERSION.SDK_INT >= 16) {
                    this.m.t.setImageAlpha(255);
                } else {
                    this.m.t.setAlpha(255);
                }
                this.m.t.clearAnimation();
            }
        }
    }

    public void c() {
        XLog.i("TagPageCradAdapter", "*** onResume ***");
        this.t++;
        if (this.m != null) {
            if (!aq.b()) {
                this.m.q.setVisibility(0);
            } else {
                this.m.q.setVisibility(8);
            }
            this.m.t.setVisibility(0);
            if (Build.VERSION.SDK_INT >= 16) {
                this.m.t.setImageAlpha(255);
            } else {
                this.m.t.setAlpha(255);
            }
            this.m.t.clearAnimation();
            a(this.m, f3173a);
        }
    }

    public void d() {
        XLog.i("TagPageCradAdapter", "*** onPause ***");
        if (this.l != null && this.l.isPlaying()) {
            this.l.stop();
        }
        if (a()) {
            a(false);
        }
    }

    public void e() {
        XLog.i("TagPageCradAdapter", "*** onCreate ***");
        f3173a = -1;
        A = false;
        h();
    }

    public void f() {
        l.c().d();
        if (this.l != null) {
            if (this.l.isPlaying()) {
                this.l.stop();
            }
            this.l.release();
            this.l = null;
        }
        i();
    }

    private void h() {
        this.c.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_START, this);
        this.c.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOADING, this);
        this.c.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_PAUSE, this);
        this.c.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_QUEUING, this);
        this.c.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_FAIL, this);
        this.c.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_ADD, this);
        this.c.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_DELETE, this);
        this.c.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_SUCC, this);
    }

    private void i() {
        this.c.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_START, this);
        this.c.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOADING, this);
        this.c.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_PAUSE, this);
        this.c.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_QUEUING, this);
        this.c.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_FAIL, this);
        this.c.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_ADD, this);
        this.c.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_DELETE, this);
        this.c.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_SUCC, this);
    }

    private int a(String str) {
        if (TextUtils.isEmpty(str) || !str.contains("x")) {
            return 1;
        }
        try {
            String[] split = str.split("x");
            if (split == null || 2 != split.length || Integer.valueOf(split[0]).intValue() > Integer.valueOf(split[1]).intValue()) {
                return 1;
            }
            return 2;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 1;
        }
    }

    private void a(ai aiVar, String str) {
        if (aiVar != null) {
            this.r = by.a(this.b, 230.0f);
            this.s = by.a(this.b, 130.0f);
            if (2 == a(str)) {
                this.r = by.a(this.b, 130.0f);
                this.s = by.a(this.b, 230.0f);
            }
            XLog.i("TagPageCradAdapter", "[adjustVideoSize] --> mVideoWidth = " + this.r + ", mVideoHeight = " + this.s);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) aiVar.p.getLayoutParams();
            layoutParams.width = this.r;
            layoutParams.height = this.s;
            aiVar.p.setLayoutParams(layoutParams);
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) aiVar.q.getLayoutParams();
            layoutParams2.width = this.r;
            layoutParams2.height = this.s;
            aiVar.q.setLayoutParams(layoutParams2);
            RelativeLayout.LayoutParams layoutParams3 = (RelativeLayout.LayoutParams) aiVar.s.getLayoutParams();
            layoutParams3.width = this.r;
            layoutParams3.height = this.s;
            aiVar.s.setLayoutParams(layoutParams3);
            RelativeLayout.LayoutParams layoutParams4 = (RelativeLayout.LayoutParams) aiVar.t.getLayoutParams();
            layoutParams4.width = this.r;
            layoutParams4.height = this.s;
            aiVar.t.setLayoutParams(layoutParams4);
            RelativeLayout.LayoutParams layoutParams5 = (RelativeLayout.LayoutParams) aiVar.v.getLayoutParams();
            layoutParams5.width = this.r;
            layoutParams5.height = this.s;
            aiVar.v.setLayoutParams(layoutParams5);
        }
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        if (this.m != null) {
            this.m.v.setVisibility(0);
            Animation loadAnimation = AnimationUtils.loadAnimation(this.b, R.anim.tag_page_video_play_finish);
            loadAnimation.setAnimationListener(new y(this));
            this.m.v.startAnimation(loadAnimation);
            if (!mediaPlayer.isPlaying()) {
                mediaPlayer.start();
            }
        }
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        XLog.i("TagPageCradAdapter", "[onError] ---> what = " + i2 + ", extra = " + i3);
        return false;
    }

    private STInfoV2 a(SimpleAppModel simpleAppModel, int i2) {
        if (simpleAppModel == null) {
            return STInfoBuilder.buildSTInfo(this.b, 100);
        }
        String a2 = a(i2);
        if (!(this.b instanceof BaseActivity)) {
            return STInfoBuilder.buildSTInfo(this.b, 100);
        }
        if (this.e == null) {
            this.e = new b();
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b, simpleAppModel, a2, 100, null);
        this.e.exposure(buildSTInfo);
        return buildSTInfo;
    }

    private boolean j() {
        return D.contains(Build.MODEL);
    }

    /* access modifiers changed from: private */
    public int k() {
        if (j()) {
            return 800;
        }
        return 200;
    }
}
