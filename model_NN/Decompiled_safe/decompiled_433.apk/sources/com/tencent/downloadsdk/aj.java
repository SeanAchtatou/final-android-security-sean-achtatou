package com.tencent.downloadsdk;

import android.text.TextUtils;
import com.tencent.downloadsdk.utils.f;
import com.tencent.downloadsdk.utils.i;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class aj {

    /* renamed from: a  reason: collision with root package name */
    private String f2459a = null;
    private String b = null;
    private RandomAccessFile c = null;
    private File d;
    private FileDescriptor e;

    public synchronized boolean a(String str, long j) {
        boolean z = true;
        synchronized (this) {
            f.b("SegFileWriter", "save path:" + str);
            this.f2459a = str;
            this.b = str + ".yyb";
            if (!TextUtils.isEmpty(str)) {
                if (!new File(str).exists()) {
                    try {
                        this.d = new File(this.b);
                        File parentFile = this.d.getParentFile();
                        if (!parentFile.exists()) {
                            parentFile.mkdirs();
                        }
                        this.c = new RandomAccessFile(this.d, "rw");
                        this.c.setLength(j);
                        this.e = this.c.getFD();
                    } catch (FileNotFoundException e2) {
                        e2.printStackTrace();
                    } catch (IOException e3) {
                        e3.printStackTrace();
                    } catch (OutOfMemoryError e4) {
                        e4.printStackTrace();
                    }
                } else {
                    try {
                        this.d = new File(str);
                        this.c = new RandomAccessFile(str, "rw");
                        this.e = this.c.getFD();
                    } catch (FileNotFoundException e5) {
                        e5.printStackTrace();
                    } catch (IOException e6) {
                        e6.printStackTrace();
                    }
                }
            }
            z = false;
        }
        return z;
    }

    public synchronized boolean a(boolean z) {
        boolean z2 = false;
        synchronized (this) {
            f.b("SegFileWriter", "close!");
            if (this.c != null) {
                try {
                    this.c.close();
                    this.c = null;
                    if (z) {
                        i.a(this.b, this.f2459a);
                    }
                    z2 = true;
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }
        return z2;
    }

    public synchronized boolean a(byte[] bArr, int i, long j) {
        boolean z = false;
        synchronized (this) {
            if (!(this.c == null || bArr == null)) {
                if (bArr.length > 0 && this.d.exists()) {
                    try {
                        this.c.seek(j);
                        this.c.write(bArr, 0, i);
                        z = true;
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                }
            }
        }
        return z;
    }
}
