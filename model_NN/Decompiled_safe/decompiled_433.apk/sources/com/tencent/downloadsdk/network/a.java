package com.tencent.downloadsdk.network;

import com.tencent.downloadsdk.network.HttpUtils;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class a {

    /* renamed from: a  reason: collision with root package name */
    public final String f2485a = "HttpClientWrapper";
    private volatile boolean b = false;
    private int c = 15000;
    /* access modifiers changed from: private */
    public int d = 30000;
    private int e = 4096;
    private String f = "UTF-8";
    private DefaultHttpClient g;
    private HttpUriRequest h;
    private boolean i = false;
    private HashMap<String, String> j;

    public a() {
    }

    public a(int i2, int i3) {
        if (i2 != 0) {
            this.d = i2;
        }
        if (i3 != 0) {
            this.c = i3;
        }
    }

    private DefaultHttpClient a(int i2) {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, this.d);
        HttpConnectionParams.setSoTimeout(basicHttpParams, this.c);
        HttpConnectionParams.setSocketBufferSize(basicHttpParams, i2);
        HttpClientParams.setRedirecting(basicHttpParams, false);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
        HttpUtils.a((HttpClient) defaultHttpClient);
        return defaultHttpClient;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0035 A[SYNTHETIC, Splitter:B:16:0x0035] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x003f A[SYNTHETIC, Splitter:B:22:0x003f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.lang.String r4, int r5, int r6) {
        /*
            r0 = 0
            r3 = 0
            java.net.Socket r2 = new java.net.Socket     // Catch:{ Throwable -> 0x002e, all -> 0x003b }
            r2.<init>()     // Catch:{ Throwable -> 0x002e, all -> 0x003b }
            java.net.InetSocketAddress r1 = new java.net.InetSocketAddress     // Catch:{ Throwable -> 0x0049 }
            r1.<init>(r4, r5)     // Catch:{ Throwable -> 0x0049 }
            r2.connect(r1, r6)     // Catch:{ Throwable -> 0x0049 }
            r0 = 1
            if (r2 == 0) goto L_0x0015
            r2.close()     // Catch:{ Throwable -> 0x0043 }
        L_0x0015:
            java.lang.String r1 = "downloadTest"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "isOnline:"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            android.util.Log.w(r1, r2)
            return r0
        L_0x002e:
            r1 = move-exception
            r2 = r3
        L_0x0030:
            r1.printStackTrace()     // Catch:{ all -> 0x0047 }
            if (r2 == 0) goto L_0x0015
            r2.close()     // Catch:{ Throwable -> 0x0039 }
            goto L_0x0015
        L_0x0039:
            r1 = move-exception
            goto L_0x0015
        L_0x003b:
            r0 = move-exception
            r2 = r3
        L_0x003d:
            if (r2 == 0) goto L_0x0042
            r2.close()     // Catch:{ Throwable -> 0x0045 }
        L_0x0042:
            throw r0
        L_0x0043:
            r1 = move-exception
            goto L_0x0015
        L_0x0045:
            r1 = move-exception
            goto L_0x0042
        L_0x0047:
            r0 = move-exception
            goto L_0x003d
        L_0x0049:
            r1 = move-exception
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.downloadsdk.network.a.a(java.lang.String, int, int):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:111:0x025c A[SYNTHETIC, Splitter:B:111:0x025c] */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0263 A[Catch:{ IOException -> 0x0271 }] */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x0283 A[SYNTHETIC, Splitter:B:122:0x0283] */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x028a A[Catch:{ IOException -> 0x0298 }] */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x02aa A[SYNTHETIC, Splitter:B:133:0x02aa] */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x02b1 A[Catch:{ IOException -> 0x02bf }] */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x02df A[SYNTHETIC, Splitter:B:149:0x02df] */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x02e6 A[Catch:{ IOException -> 0x02f4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:176:0x0330 A[SYNTHETIC, Splitter:B:176:0x0330] */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x0337 A[Catch:{ IOException -> 0x0345 }] */
    /* JADX WARNING: Removed duplicated region for block: B:189:0x035c A[SYNTHETIC, Splitter:B:189:0x035c] */
    /* JADX WARNING: Removed duplicated region for block: B:193:0x0363 A[Catch:{ IOException -> 0x0371 }] */
    /* JADX WARNING: Removed duplicated region for block: B:198:0x037c  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0092 A[SYNTHETIC, Splitter:B:31:0x0092] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0099 A[Catch:{ IOException -> 0x024a }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00e5 A[SYNTHETIC, Splitter:B:56:0x00e5] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00ec A[Catch:{ IOException -> 0x00f9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0151 A[SYNTHETIC, Splitter:B:75:0x0151] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0158 A[Catch:{ IOException -> 0x0166 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:108:0x0251=Splitter:B:108:0x0251, B:72:0x0146=Splitter:B:72:0x0146, B:141:0x02c6=Splitter:B:141:0x02c6, B:168:0x0317=Splitter:B:168:0x0317, B:119:0x0278=Splitter:B:119:0x0278, B:53:0x00da=Splitter:B:53:0x00da, B:130:0x029f=Splitter:B:130:0x029f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> com.tencent.downloadsdk.network.c<T> a(java.lang.String r8, com.tencent.downloadsdk.network.HttpUtils.HttpMethod r9, java.io.InputStream r10, java.util.Map<java.lang.String, java.lang.String> r11, boolean r12, com.tencent.downloadsdk.network.d<T> r13) {
        /*
            r7 = this;
            com.tencent.downloadsdk.network.c r2 = new com.tencent.downloadsdk.network.c
            r2.<init>()
            r0 = 0
            r2.j = r0
            if (r8 == 0) goto L_0x0012
            java.lang.String r0 = ""
            boolean r0 = r0.equals(r8)
            if (r0 == 0) goto L_0x0018
        L_0x0012:
            r0 = -16
            r2.j = r0
            r0 = r2
        L_0x0017:
            return r0
        L_0x0018:
            boolean r0 = com.tencent.downloadsdk.utils.k.a()
            if (r0 != 0) goto L_0x0024
            r0 = -15
            r2.j = r0
            r0 = r2
            goto L_0x0017
        L_0x0024:
            boolean r0 = r7.b
            if (r0 == 0) goto L_0x002d
            r0 = 1
            r2.j = r0
            r0 = r2
            goto L_0x0017
        L_0x002d:
            r3 = 0
            int r0 = r7.e     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.a(r0)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            r7.g = r0     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            r0 = 0
            r7.h = r0     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            com.tencent.downloadsdk.network.HttpUtils$HttpMethod r0 = com.tencent.downloadsdk.network.HttpUtils.HttpMethod.f2484a     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            if (r9 != r0) goto L_0x00cc
            org.apache.http.client.methods.HttpGet r0 = new org.apache.http.client.methods.HttpGet     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            r0.<init>(r8)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            r7.h = r0     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
        L_0x0044:
            org.apache.http.client.methods.HttpUriRequest r0 = r7.h     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.String r1 = "Accept-Charset"
            java.lang.String r4 = r7.f     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            r0.addHeader(r1, r4)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.util.HashMap<java.lang.String, java.lang.String> r0 = r7.j     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            if (r0 == 0) goto L_0x00fe
            java.util.HashMap<java.lang.String, java.lang.String> r0 = r7.j     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            int r0 = r0.size()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            if (r0 <= 0) goto L_0x00fe
            java.util.HashMap<java.lang.String, java.lang.String> r0 = r7.j     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.util.Set r0 = r0.keySet()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.util.Iterator r4 = r0.iterator()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
        L_0x0063:
            boolean r0 = r4.hasNext()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            if (r0 == 0) goto L_0x00fe
            java.lang.Object r0 = r4.next()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.String r1 = "range"
            boolean r1 = r0.equalsIgnoreCase(r1)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            if (r1 != 0) goto L_0x0063
            org.apache.http.client.methods.HttpUriRequest r5 = r7.h     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.util.HashMap<java.lang.String, java.lang.String> r1 = r7.j     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.Object r1 = r1.get(r0)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            r5.addHeader(r0, r1)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            goto L_0x0063
        L_0x0085:
            r0 = move-exception
            r1 = r3
        L_0x0087:
            r0.printStackTrace()     // Catch:{ all -> 0x0398 }
            r3 = -22
            r2.j = r3     // Catch:{ all -> 0x0398 }
            r2.k = r0     // Catch:{ all -> 0x0398 }
            if (r1 == 0) goto L_0x0095
            r1.close()     // Catch:{ IOException -> 0x024a }
        L_0x0095:
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.g     // Catch:{ IOException -> 0x024a }
            if (r0 == 0) goto L_0x00a5
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.g     // Catch:{ IOException -> 0x024a }
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ IOException -> 0x024a }
            r0.shutdown()     // Catch:{ IOException -> 0x024a }
            r0 = 0
            r7.g = r0     // Catch:{ IOException -> 0x024a }
        L_0x00a5:
            int r0 = r2.j
            r1 = -23
            if (r0 == r1) goto L_0x00bd
            int r0 = r2.j
            r1 = -27
            if (r0 == r1) goto L_0x00bd
            int r0 = r2.j
            r1 = -25
            if (r0 == r1) goto L_0x00bd
            int r0 = r2.j
            r1 = -29
            if (r0 != r1) goto L_0x00c9
        L_0x00bd:
            boolean r0 = com.tencent.downloadsdk.utils.k.a()
            if (r0 != 0) goto L_0x037c
            int r0 = r2.j
            int r0 = r0 + -50
            r2.j = r0
        L_0x00c9:
            r0 = r2
            goto L_0x0017
        L_0x00cc:
            com.tencent.downloadsdk.network.HttpUtils$HttpMethod r0 = com.tencent.downloadsdk.network.HttpUtils.HttpMethod.b     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            if (r9 != r0) goto L_0x0044
            org.apache.http.client.methods.HttpPost r0 = new org.apache.http.client.methods.HttpPost     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            r0.<init>(r8)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            r7.h = r0     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            goto L_0x0044
        L_0x00d9:
            r0 = move-exception
        L_0x00da:
            r0.printStackTrace()     // Catch:{ all -> 0x02ff }
            r1 = -29
            r2.j = r1     // Catch:{ all -> 0x02ff }
            r2.k = r0     // Catch:{ all -> 0x02ff }
            if (r3 == 0) goto L_0x00e8
            r3.close()     // Catch:{ IOException -> 0x00f9 }
        L_0x00e8:
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.g     // Catch:{ IOException -> 0x00f9 }
            if (r0 == 0) goto L_0x00a5
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.g     // Catch:{ IOException -> 0x00f9 }
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ IOException -> 0x00f9 }
            r0.shutdown()     // Catch:{ IOException -> 0x00f9 }
            r0 = 0
            r7.g = r0     // Catch:{ IOException -> 0x00f9 }
            goto L_0x00a5
        L_0x00f9:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00a5
        L_0x00fe:
            java.lang.String r0 = "HttpClientWrapper"
            java.lang.String r1 = "start request"
            com.tencent.downloadsdk.utils.f.b(r0, r1)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.String r0 = "HttpClientWrapper"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            r1.<init>()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.String r4 = "request url:"
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.StringBuilder r1 = r1.append(r8)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.String r1 = r1.toString()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            com.tencent.downloadsdk.utils.f.b(r0, r1)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            if (r11 == 0) goto L_0x0188
            java.util.Set r0 = r11.entrySet()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.util.Iterator r4 = r0.iterator()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
        L_0x0127:
            boolean r0 = r4.hasNext()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            if (r0 == 0) goto L_0x016c
            java.lang.Object r0 = r4.next()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            org.apache.http.client.methods.HttpUriRequest r5 = r7.h     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.Object r1 = r0.getKey()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.Object r0 = r0.getValue()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            r5.addHeader(r1, r0)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            goto L_0x0127
        L_0x0145:
            r0 = move-exception
        L_0x0146:
            r0.printStackTrace()     // Catch:{ all -> 0x02ff }
            r1 = -21
            r2.j = r1     // Catch:{ all -> 0x02ff }
            r2.k = r0     // Catch:{ all -> 0x02ff }
            if (r3 == 0) goto L_0x0154
            r3.close()     // Catch:{ IOException -> 0x0166 }
        L_0x0154:
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.g     // Catch:{ IOException -> 0x0166 }
            if (r0 == 0) goto L_0x00a5
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.g     // Catch:{ IOException -> 0x0166 }
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ IOException -> 0x0166 }
            r0.shutdown()     // Catch:{ IOException -> 0x0166 }
            r0 = 0
            r7.g = r0     // Catch:{ IOException -> 0x0166 }
            goto L_0x00a5
        L_0x0166:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00a5
        L_0x016c:
            java.lang.String r0 = "HttpClientWrapper"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            r1.<init>()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.String r4 = "request params:"
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.String r4 = r11.toString()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.String r1 = r1.toString()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            com.tencent.downloadsdk.utils.f.b(r0, r1)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
        L_0x0188:
            com.tencent.downloadsdk.network.HttpUtils$HttpMethod r0 = com.tencent.downloadsdk.network.HttpUtils.HttpMethod.b     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            if (r9 != r0) goto L_0x019f
            if (r10 == 0) goto L_0x019f
            org.apache.http.entity.InputStreamEntity r1 = new org.apache.http.entity.InputStreamEntity     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            int r0 = r10.available()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            long r4 = (long) r0     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            r1.<init>(r10, r4)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            org.apache.http.client.methods.HttpUriRequest r0 = r7.h     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            org.apache.http.client.methods.HttpPost r0 = (org.apache.http.client.methods.HttpPost) r0     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            r0.setEntity(r1)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
        L_0x019f:
            boolean r0 = r7.i     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            if (r0 == 0) goto L_0x01ad
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.g     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            com.tencent.downloadsdk.network.b r1 = new com.tencent.downloadsdk.network.b     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            r1.<init>(r7)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            r0.setHttpRequestRetryHandler(r1)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
        L_0x01ad:
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.g     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            org.apache.http.client.methods.HttpUriRequest r1 = r7.h     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            org.apache.http.HttpResponse r0 = r0.execute(r1)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            org.apache.http.StatusLine r1 = r0.getStatusLine()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            int r4 = r1.getStatusCode()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            r2.c = r4     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.String r1 = "HttpClientWrapper"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            r5.<init>()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.String r6 = "statusCode: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            int r6 = r2.c     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.String r5 = r5.toString()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            com.tencent.downloadsdk.utils.f.b(r1, r5)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            org.apache.http.Header[] r1 = r0.getAllHeaders()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            r2.h = r1     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            org.apache.http.HttpEntity r0 = r0.getEntity()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.String r1 = "Content-Encoding"
            java.lang.String r1 = r2.a(r1)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            r2.d = r1     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.String r1 = "Content-Type"
            java.lang.String r1 = r2.a(r1)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            r2.g = r1     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.lang.String r1 = "Content-Range"
            java.lang.String r1 = r2.a(r1)     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            r2.f = r1     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            long r5 = r0.getContentLength()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            r2.e = r5     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            java.io.InputStream r1 = r0.getContent()     // Catch:{ ClientProtocolException -> 0x0085, UnknownHostException -> 0x00d9, MalformedURLException -> 0x0145, ProtocolException -> 0x0250, ConnectTimeoutException -> 0x0277, SocketTimeoutException -> 0x029e, SocketException -> 0x02c5, IOException -> 0x0316, Exception -> 0x0350 }
            org.apache.http.client.methods.HttpUriRequest r0 = r7.h     // Catch:{ ClientProtocolException -> 0x03bb, UnknownHostException -> 0x03b7, MalformedURLException -> 0x03b3, ProtocolException -> 0x03af, ConnectTimeoutException -> 0x03ab, SocketTimeoutException -> 0x03a7, SocketException -> 0x03a3, IOException -> 0x039f, Exception -> 0x039c }
            java.net.URI r0 = r0.getURI()     // Catch:{ ClientProtocolException -> 0x03bb, UnknownHostException -> 0x03b7, MalformedURLException -> 0x03b3, ProtocolException -> 0x03af, ConnectTimeoutException -> 0x03ab, SocketTimeoutException -> 0x03a7, SocketException -> 0x03a3, IOException -> 0x039f, Exception -> 0x039c }
            java.lang.String r0 = r0.toString()     // Catch:{ ClientProtocolException -> 0x03bb, UnknownHostException -> 0x03b7, MalformedURLException -> 0x03b3, ProtocolException -> 0x03af, ConnectTimeoutException -> 0x03ab, SocketTimeoutException -> 0x03a7, SocketException -> 0x03a3, IOException -> 0x039f, Exception -> 0x039c }
            r2.f2487a = r0     // Catch:{ ClientProtocolException -> 0x03bb, UnknownHostException -> 0x03b7, MalformedURLException -> 0x03b3, ProtocolException -> 0x03af, ConnectTimeoutException -> 0x03ab, SocketTimeoutException -> 0x03a7, SocketException -> 0x03a3, IOException -> 0x039f, Exception -> 0x039c }
            org.apache.http.client.methods.HttpUriRequest r0 = r7.h     // Catch:{ ClientProtocolException -> 0x03bb, UnknownHostException -> 0x03b7, MalformedURLException -> 0x03b3, ProtocolException -> 0x03af, ConnectTimeoutException -> 0x03ab, SocketTimeoutException -> 0x03a7, SocketException -> 0x03a3, IOException -> 0x039f, Exception -> 0x039c }
            java.net.URI r0 = r0.getURI()     // Catch:{ ClientProtocolException -> 0x03bb, UnknownHostException -> 0x03b7, MalformedURLException -> 0x03b3, ProtocolException -> 0x03af, ConnectTimeoutException -> 0x03ab, SocketTimeoutException -> 0x03a7, SocketException -> 0x03a3, IOException -> 0x039f, Exception -> 0x039c }
            java.lang.String r0 = r0.getHost()     // Catch:{ ClientProtocolException -> 0x03bb, UnknownHostException -> 0x03b7, MalformedURLException -> 0x03b3, ProtocolException -> 0x03af, ConnectTimeoutException -> 0x03ab, SocketTimeoutException -> 0x03a7, SocketException -> 0x03a3, IOException -> 0x039f, Exception -> 0x039c }
            java.net.InetAddress r0 = java.net.InetAddress.getByName(r0)     // Catch:{ ClientProtocolException -> 0x03bb, UnknownHostException -> 0x03b7, MalformedURLException -> 0x03b3, ProtocolException -> 0x03af, ConnectTimeoutException -> 0x03ab, SocketTimeoutException -> 0x03a7, SocketException -> 0x03a3, IOException -> 0x039f, Exception -> 0x039c }
            java.lang.String r0 = r0.getHostAddress()     // Catch:{ ClientProtocolException -> 0x03bb, UnknownHostException -> 0x03b7, MalformedURLException -> 0x03b3, ProtocolException -> 0x03af, ConnectTimeoutException -> 0x03ab, SocketTimeoutException -> 0x03a7, SocketException -> 0x03a3, IOException -> 0x039f, Exception -> 0x039c }
            r2.b = r0     // Catch:{ ClientProtocolException -> 0x03bb, UnknownHostException -> 0x03b7, MalformedURLException -> 0x03b3, ProtocolException -> 0x03af, ConnectTimeoutException -> 0x03ab, SocketTimeoutException -> 0x03a7, SocketException -> 0x03a3, IOException -> 0x039f, Exception -> 0x039c }
            if (r13 == 0) goto L_0x022d
            java.lang.Object r0 = r13.b(r4, r2, r1)     // Catch:{ ClientProtocolException -> 0x03bb, UnknownHostException -> 0x03b7, MalformedURLException -> 0x03b3, ProtocolException -> 0x03af, ConnectTimeoutException -> 0x03ab, SocketTimeoutException -> 0x03a7, SocketException -> 0x03a3, IOException -> 0x039f, Exception -> 0x039c }
            r2.i = r0     // Catch:{ ClientProtocolException -> 0x03bb, UnknownHostException -> 0x03b7, MalformedURLException -> 0x03b3, ProtocolException -> 0x03af, ConnectTimeoutException -> 0x03ab, SocketTimeoutException -> 0x03a7, SocketException -> 0x03a3, IOException -> 0x039f, Exception -> 0x039c }
        L_0x022d:
            if (r1 == 0) goto L_0x0232
            r1.close()     // Catch:{ IOException -> 0x0244 }
        L_0x0232:
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.g     // Catch:{ IOException -> 0x0244 }
            if (r0 == 0) goto L_0x00a5
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.g     // Catch:{ IOException -> 0x0244 }
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ IOException -> 0x0244 }
            r0.shutdown()     // Catch:{ IOException -> 0x0244 }
            r0 = 0
            r7.g = r0     // Catch:{ IOException -> 0x0244 }
            goto L_0x00a5
        L_0x0244:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00a5
        L_0x024a:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00a5
        L_0x0250:
            r0 = move-exception
        L_0x0251:
            r0.printStackTrace()     // Catch:{ all -> 0x02ff }
            r1 = -22
            r2.j = r1     // Catch:{ all -> 0x02ff }
            r2.k = r0     // Catch:{ all -> 0x02ff }
            if (r3 == 0) goto L_0x025f
            r3.close()     // Catch:{ IOException -> 0x0271 }
        L_0x025f:
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.g     // Catch:{ IOException -> 0x0271 }
            if (r0 == 0) goto L_0x00a5
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.g     // Catch:{ IOException -> 0x0271 }
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ IOException -> 0x0271 }
            r0.shutdown()     // Catch:{ IOException -> 0x0271 }
            r0 = 0
            r7.g = r0     // Catch:{ IOException -> 0x0271 }
            goto L_0x00a5
        L_0x0271:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00a5
        L_0x0277:
            r0 = move-exception
        L_0x0278:
            r0.printStackTrace()     // Catch:{ all -> 0x02ff }
            r1 = -23
            r2.j = r1     // Catch:{ all -> 0x02ff }
            r2.k = r0     // Catch:{ all -> 0x02ff }
            if (r3 == 0) goto L_0x0286
            r3.close()     // Catch:{ IOException -> 0x0298 }
        L_0x0286:
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.g     // Catch:{ IOException -> 0x0298 }
            if (r0 == 0) goto L_0x00a5
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.g     // Catch:{ IOException -> 0x0298 }
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ IOException -> 0x0298 }
            r0.shutdown()     // Catch:{ IOException -> 0x0298 }
            r0 = 0
            r7.g = r0     // Catch:{ IOException -> 0x0298 }
            goto L_0x00a5
        L_0x0298:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00a5
        L_0x029e:
            r0 = move-exception
        L_0x029f:
            r0.printStackTrace()     // Catch:{ all -> 0x02ff }
            r1 = -25
            r2.j = r1     // Catch:{ all -> 0x02ff }
            r2.k = r0     // Catch:{ all -> 0x02ff }
            if (r3 == 0) goto L_0x02ad
            r3.close()     // Catch:{ IOException -> 0x02bf }
        L_0x02ad:
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.g     // Catch:{ IOException -> 0x02bf }
            if (r0 == 0) goto L_0x00a5
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.g     // Catch:{ IOException -> 0x02bf }
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ IOException -> 0x02bf }
            r0.shutdown()     // Catch:{ IOException -> 0x02bf }
            r0 = 0
            r7.g = r0     // Catch:{ IOException -> 0x02bf }
            goto L_0x00a5
        L_0x02bf:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00a5
        L_0x02c5:
            r0 = move-exception
        L_0x02c6:
            r0.printStackTrace()     // Catch:{ all -> 0x02ff }
            java.lang.String r1 = r0.getLocalizedMessage()     // Catch:{ all -> 0x02ff }
            if (r1 == 0) goto L_0x02fa
            java.lang.String r4 = "Socket closed"
            boolean r1 = r1.equals(r4)     // Catch:{ all -> 0x02ff }
            if (r1 == 0) goto L_0x02fa
            r1 = -38
            r2.j = r1     // Catch:{ all -> 0x02ff }
        L_0x02db:
            r2.k = r0     // Catch:{ all -> 0x02ff }
            if (r3 == 0) goto L_0x02e2
            r3.close()     // Catch:{ IOException -> 0x02f4 }
        L_0x02e2:
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.g     // Catch:{ IOException -> 0x02f4 }
            if (r0 == 0) goto L_0x00a5
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.g     // Catch:{ IOException -> 0x02f4 }
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ IOException -> 0x02f4 }
            r0.shutdown()     // Catch:{ IOException -> 0x02f4 }
            r0 = 0
            r7.g = r0     // Catch:{ IOException -> 0x02f4 }
            goto L_0x00a5
        L_0x02f4:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00a5
        L_0x02fa:
            r1 = -27
            r2.j = r1     // Catch:{ all -> 0x02ff }
            goto L_0x02db
        L_0x02ff:
            r0 = move-exception
        L_0x0300:
            if (r3 == 0) goto L_0x0305
            r3.close()     // Catch:{ IOException -> 0x0377 }
        L_0x0305:
            org.apache.http.impl.client.DefaultHttpClient r1 = r7.g     // Catch:{ IOException -> 0x0377 }
            if (r1 == 0) goto L_0x0315
            org.apache.http.impl.client.DefaultHttpClient r1 = r7.g     // Catch:{ IOException -> 0x0377 }
            org.apache.http.conn.ClientConnectionManager r1 = r1.getConnectionManager()     // Catch:{ IOException -> 0x0377 }
            r1.shutdown()     // Catch:{ IOException -> 0x0377 }
            r1 = 0
            r7.g = r1     // Catch:{ IOException -> 0x0377 }
        L_0x0315:
            throw r0
        L_0x0316:
            r0 = move-exception
        L_0x0317:
            r0.printStackTrace()     // Catch:{ all -> 0x02ff }
            java.lang.String r1 = r0.getLocalizedMessage()     // Catch:{ all -> 0x02ff }
            if (r1 == 0) goto L_0x034b
            java.lang.String r4 = "Request aborted"
            boolean r1 = r1.equals(r4)     // Catch:{ all -> 0x02ff }
            if (r1 == 0) goto L_0x034b
            r1 = -39
            r2.j = r1     // Catch:{ all -> 0x02ff }
        L_0x032c:
            r2.k = r0     // Catch:{ all -> 0x02ff }
            if (r3 == 0) goto L_0x0333
            r3.close()     // Catch:{ IOException -> 0x0345 }
        L_0x0333:
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.g     // Catch:{ IOException -> 0x0345 }
            if (r0 == 0) goto L_0x00a5
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.g     // Catch:{ IOException -> 0x0345 }
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ IOException -> 0x0345 }
            r0.shutdown()     // Catch:{ IOException -> 0x0345 }
            r0 = 0
            r7.g = r0     // Catch:{ IOException -> 0x0345 }
            goto L_0x00a5
        L_0x0345:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00a5
        L_0x034b:
            r1 = -27
            r2.j = r1     // Catch:{ all -> 0x02ff }
            goto L_0x032c
        L_0x0350:
            r0 = move-exception
        L_0x0351:
            r0.printStackTrace()     // Catch:{ all -> 0x02ff }
            r1 = -28
            r2.j = r1     // Catch:{ all -> 0x02ff }
            r2.k = r0     // Catch:{ all -> 0x02ff }
            if (r3 == 0) goto L_0x035f
            r3.close()     // Catch:{ IOException -> 0x0371 }
        L_0x035f:
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.g     // Catch:{ IOException -> 0x0371 }
            if (r0 == 0) goto L_0x00a5
            org.apache.http.impl.client.DefaultHttpClient r0 = r7.g     // Catch:{ IOException -> 0x0371 }
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ IOException -> 0x0371 }
            r0.shutdown()     // Catch:{ IOException -> 0x0371 }
            r0 = 0
            r7.g = r0     // Catch:{ IOException -> 0x0371 }
            goto L_0x00a5
        L_0x0371:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00a5
        L_0x0377:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0315
        L_0x037c:
            java.lang.String r0 = "info.3g.qq.com"
            r1 = 80
            int r3 = r7.d
            boolean r0 = a(r0, r1, r3)
            if (r0 == 0) goto L_0x0390
            int r0 = r2.j
            int r0 = r0 + -60
            r2.j = r0
            goto L_0x00c9
        L_0x0390:
            int r0 = r2.j
            int r0 = r0 + -70
            r2.j = r0
            goto L_0x00c9
        L_0x0398:
            r0 = move-exception
            r3 = r1
            goto L_0x0300
        L_0x039c:
            r0 = move-exception
            r3 = r1
            goto L_0x0351
        L_0x039f:
            r0 = move-exception
            r3 = r1
            goto L_0x0317
        L_0x03a3:
            r0 = move-exception
            r3 = r1
            goto L_0x02c6
        L_0x03a7:
            r0 = move-exception
            r3 = r1
            goto L_0x029f
        L_0x03ab:
            r0 = move-exception
            r3 = r1
            goto L_0x0278
        L_0x03af:
            r0 = move-exception
            r3 = r1
            goto L_0x0251
        L_0x03b3:
            r0 = move-exception
            r3 = r1
            goto L_0x0146
        L_0x03b7:
            r0 = move-exception
            r3 = r1
            goto L_0x00da
        L_0x03bb:
            r0 = move-exception
            goto L_0x0087
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.downloadsdk.network.a.a(java.lang.String, com.tencent.downloadsdk.network.HttpUtils$HttpMethod, java.io.InputStream, java.util.Map, boolean, com.tencent.downloadsdk.network.d):com.tencent.downloadsdk.network.c");
    }

    public <T> c<T> a(String str, InputStream inputStream, Map<String, String> map, boolean z, d<T> dVar) {
        return a(str, HttpUtils.HttpMethod.b, inputStream, map, z, dVar);
    }

    public <T> c<T> a(String str, Map<String, String> map, boolean z, d<T> dVar) {
        return a(str, HttpUtils.HttpMethod.f2484a, null, map, z, dVar);
    }

    public void a() {
        try {
            if (this.h != null) {
                this.h.abort();
                this.b = true;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void a(HashMap<String, String> hashMap) {
        this.j = hashMap;
    }

    public void a(boolean z) {
        this.i = z;
    }
}
