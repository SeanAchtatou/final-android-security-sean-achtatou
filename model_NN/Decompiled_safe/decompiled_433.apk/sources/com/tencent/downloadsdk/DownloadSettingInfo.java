package com.tencent.downloadsdk;

import android.net.NetworkInfo;
import com.tencent.downloadsdk.network.HttpUtils;
import com.tencent.downloadsdk.protocol.jce.DownloadSetting;
import com.tencent.downloadsdk.utils.k;
import java.io.Serializable;

public class DownloadSettingInfo implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public long f2446a;
    public int b;
    public long c;
    public int d;
    public int e;
    public int f;
    public int g;
    public int h;
    public int i;
    public boolean j;
    public long k;
    public int l;
    public int m;
    public HttpUtils.HttpMethod n;
    public long o;
    public long p;
    public long q;
    public long r;

    public DownloadSettingInfo(int i2) {
        this.b = i2;
        switch (i2) {
            case 1:
                this.f2446a = 0;
                this.c = 1048576;
                this.d = 3;
                this.e = 3;
                this.f = 20;
                this.g = 10;
                this.h = 4096;
                this.i = 2;
                this.j = false;
                this.k = 86400;
                this.l = 8;
                this.m = 1000;
                this.o = 4096;
                this.p = 30720;
                this.q = 30720;
                this.n = HttpUtils.HttpMethod.f2484a;
                this.r = -1;
                return;
            case 2:
                this.f2446a = 0;
                this.c = 512000;
                this.d = 1;
                this.e = 5;
                this.f = 30;
                this.g = 20;
                this.h = 4096;
                this.i = 1;
                this.j = false;
                this.k = 86400;
                this.l = 8;
                this.m = 1000;
                this.o = 4096;
                this.p = 30720;
                this.q = 30720;
                this.n = HttpUtils.HttpMethod.f2484a;
                this.r = 921600;
                return;
            case 3:
                this.f2446a = 0;
                this.c = 1048576;
                this.d = 3;
                this.e = 3;
                this.f = 20;
                this.g = 20;
                this.h = 4096;
                this.i = 1;
                this.j = false;
                this.k = 86400;
                this.l = 8;
                this.m = 1000;
                this.o = 4096;
                this.p = 30720;
                this.q = 30720;
                this.n = HttpUtils.HttpMethod.f2484a;
                this.r = -1;
                return;
            case 4:
                this.f2446a = 0;
                this.c = 1048576;
                this.d = 3;
                this.e = 3;
                this.f = 20;
                this.g = 10;
                this.h = 4096;
                this.i = 2;
                this.j = false;
                this.k = 86400;
                this.l = 8;
                this.m = 1000;
                this.o = 4096;
                this.p = 30720;
                this.q = 30720;
                this.n = HttpUtils.HttpMethod.f2484a;
                this.r = -1;
                return;
            default:
                this.f2446a = 0;
                this.c = 512000;
                this.d = 3;
                this.e = 3;
                this.f = 20;
                this.g = 20;
                this.h = 4096;
                this.i = 1;
                this.j = true;
                this.k = 86400;
                this.l = 8;
                this.m = 1000;
                this.o = 4096;
                this.p = 30720;
                this.q = 30720;
                this.n = HttpUtils.HttpMethod.f2484a;
                this.r = -1;
                return;
        }
    }

    public DownloadSettingInfo(long j2, DownloadSetting downloadSetting) {
        long j3 = 30720;
        int i2 = 30;
        int i3 = 3;
        this.f2446a = j2;
        this.b = downloadSetting.f2490a;
        this.c = downloadSetting.b >= 512000 ? downloadSetting.b : 512000;
        this.d = downloadSetting.c > 0 ? downloadSetting.c : 3;
        this.e = downloadSetting.d > 0 ? downloadSetting.d : 5;
        this.f = downloadSetting.e >= 5 ? downloadSetting.e : 30;
        this.g = downloadSetting.f >= 5 ? downloadSetting.f : i2;
        this.h = downloadSetting.g >= 1024 ? downloadSetting.g : 4096;
        this.i = downloadSetting.h > 0 ? downloadSetting.h : 1;
        this.j = downloadSetting.i;
        this.k = downloadSetting.j > 0 ? downloadSetting.j : 86400;
        this.l = downloadSetting.k > 0 ? downloadSetting.k : i3;
        this.m = downloadSetting.l >= 100 ? downloadSetting.l : 1000;
        this.o = downloadSetting.m >= 1024 ? downloadSetting.m : 4096;
        this.p = downloadSetting.n >= 10240 ? downloadSetting.n : 30720;
        this.q = downloadSetting.o >= 10240 ? downloadSetting.o : j3;
        this.n = HttpUtils.HttpMethod.a(downloadSetting.p);
        this.r = downloadSetting.r;
    }

    public static int a() {
        NetworkInfo a2 = k.a(DownloadManager.a().b());
        if (a2 == null) {
            return 0;
        }
        if (a2.getType() == 1) {
            return 1;
        }
        int subtype = a2.getSubtype();
        switch (subtype) {
            case 1:
            case 2:
            case 4:
            case 7:
            case 11:
                return 2;
            case 3:
            case 5:
            case 6:
            case 8:
            case 9:
            case 10:
            case 12:
            case 14:
            case 15:
                return 3;
            case 13:
                return 4;
            default:
                return subtype;
        }
    }

    public static String a(int i2) {
        switch (i2) {
            case 1:
                return "WIFI";
            case 2:
                return "2G";
            case 3:
                return "3G";
            case 4:
                return "4G";
            default:
                return "UNKOWN";
        }
    }
}
