package com.tencent.downloadsdk;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.text.TextUtils;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import com.tencent.downloadsdk.utils.g;
import com.tencent.downloadsdk.utils.m;

public class b {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f2465a = false;
    public static String b = Constants.STR_EMPTY;
    public static String c = Constants.STR_EMPTY;
    public static int d;
    public static String e = Constants.STR_EMPTY;
    public static int f;
    public static String g = Constants.STR_EMPTY;
    public static int h;
    public static String i = Constants.STR_EMPTY;
    private static String j = Constants.STR_EMPTY;
    private static int k;
    private static boolean l = false;

    public static synchronized int a() {
        int i2;
        synchronized (b.class) {
            i2 = k;
            k = i2 + 1;
        }
        return i2;
    }

    public static void a(Context context) {
        if (!l) {
            a.a(context);
            b = g.f(context);
            c = g.e(context);
            d = g.d(context);
            g = new m(context).a();
            h = EventDispatcherEnum.CONNECTION_EVENT_WCS_NETWORK_LOST;
            k = 0;
            i = Settings.Secure.getString(context.getContentResolver(), "android_id");
            l = true;
        }
    }

    public static void a(Context context, String str) {
        SharedPreferences sharedPreferences;
        if (context != null && !TextUtils.isEmpty(str) && (sharedPreferences = context.getSharedPreferences("TMAssistantSDKSharedPreference", 0)) != null) {
            sharedPreferences.edit().putString("TMAssistantSDKPhoneGUID", str).commit();
        }
    }

    public static String b() {
        if (TextUtils.isEmpty(j)) {
            try {
                j = a.d();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return j;
    }

    public static String b(Context context) {
        SharedPreferences sharedPreferences;
        return (context == null || (sharedPreferences = context.getSharedPreferences("TMAssistantSDKSharedPreference", 0)) == null) ? Constants.STR_EMPTY : sharedPreferences.getString("TMAssistantSDKPhoneGUID", Constants.STR_EMPTY);
    }
}
