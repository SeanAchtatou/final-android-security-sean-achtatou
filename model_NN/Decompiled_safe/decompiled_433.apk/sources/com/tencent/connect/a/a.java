package com.tencent.connect.a;

import android.content.Context;
import com.tencent.connect.auth.QQToken;
import com.tencent.open.utils.OpenConfig;
import java.lang.reflect.Method;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static Class<?> f2342a = null;
    private static Class<?> b = null;
    private static Method c = null;
    private static Method d = null;
    private static Method e = null;
    private static Method f = null;
    private static boolean g = false;

    public static boolean a(Context context, QQToken qQToken) {
        return OpenConfig.getInstance(context, qQToken.getAppId()).getBoolean("Common_ta_enable");
    }

    public static void b(Context context, QQToken qQToken) {
        try {
            if (a(context, qQToken)) {
                f.invoke(f2342a, true);
                return;
            }
            f.invoke(f2342a, false);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void c(Context context, QQToken qQToken) {
        String str = "Aqc" + qQToken.getAppId();
        try {
            f2342a = Class.forName("com.tencent.stat.StatConfig");
            b = Class.forName("com.tencent.stat.StatService");
            c = b.getMethod("reportQQ", new Class[0]);
            d = b.getMethod("trackCustomEvent", new Class[0]);
            e = b.getMethod("commitEvents", new Class[0]);
            f = f2342a.getMethod("setEnableStatService", new Class[0]);
            b(context, qQToken);
            f2342a.getMethod("setAutoExceptionCaught", new Class[0]).invoke(f2342a, false);
            f2342a.getMethod("setEnableSmartReporting", new Class[0]).invoke(f2342a, true);
            f2342a.getMethod("setSendPeriodMinutes", new Class[0]).invoke(f2342a, 1440);
            f2342a.getMethod("setStatSendStrategy", new Class[0]).invoke(f2342a, Integer.valueOf(Class.forName("com.tencent.stat.StatReportStrategy").getField("PERIOD").getInt(null)));
            f2342a.getMethod("setStatReportUrl", new Class[0]).invoke(f2342a, "http://cgi.connect.qq.com/qqconnectutil/sdk");
            b.getMethod("startStatService", new Class[0]).invoke(b, context, str, Integer.valueOf(Class.forName("com.tencent.stat.common.StatConstants").getField("VERSION").getInt(null)));
            g = true;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void d(Context context, QQToken qQToken) {
        if (g) {
            b(context, qQToken);
            if (qQToken.getOpenId() != null) {
                try {
                    c.invoke(b, context, qQToken.getOpenId());
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    public static void a(Context context, QQToken qQToken, String str, String... strArr) {
        if (g) {
            b(context, qQToken);
            try {
                d.invoke(b, context, str, strArr);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
