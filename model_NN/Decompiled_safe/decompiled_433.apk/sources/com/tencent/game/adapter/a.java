package com.tencent.game.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class a extends PagerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<View> f2634a;

    public void a(ArrayList<View> arrayList) {
        this.f2634a = arrayList;
    }

    public int getCount() {
        return this.f2634a.size();
    }

    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        viewGroup.addView(this.f2634a.get(i));
        return this.f2634a.get(i);
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        viewGroup.removeView(this.f2634a.get(i));
    }
}
