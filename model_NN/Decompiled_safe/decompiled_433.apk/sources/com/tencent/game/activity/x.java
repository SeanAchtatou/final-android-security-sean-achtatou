package com.tencent.game.activity;

import android.view.MotionEvent;
import com.tencent.assistant.component.txscrollview.TXRefreshGetMoreListView;
import com.tencent.game.component.GameBannerGuideView;

/* compiled from: ProGuard */
class x extends TXRefreshGetMoreListView.OnDispatchTouchEventListener {
    final /* synthetic */ w b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    x(w wVar, TXRefreshGetMoreListView tXRefreshGetMoreListView) {
        super();
        this.b = wVar;
        tXRefreshGetMoreListView.getClass();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.game.activity.w.a(com.tencent.game.activity.w, boolean):void
     arg types: [com.tencent.game.activity.w, int]
     candidates:
      com.tencent.game.activity.w.a(com.tencent.game.activity.w, com.tencent.assistantv2.adapter.smartlist.SmartListAdapter$BannerType):com.tencent.assistantv2.adapter.smartlist.SmartListAdapter$BannerType
      android.support.v4.app.Fragment.a(android.content.Context, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.Fragment.a(int, android.support.v4.app.Fragment):void
      android.support.v4.app.Fragment.a(android.content.Intent, int):void
      android.support.v4.app.Fragment.a(android.view.Menu, android.view.MenuInflater):void
      android.support.v4.app.Fragment.a(android.view.View, android.os.Bundle):void
      com.tencent.game.activity.w.a(com.tencent.game.activity.w, boolean):void */
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (GameBannerGuideView.f2650a && motionEvent.getAction() == 1) {
            this.b.e(false);
        }
        return false;
    }
}
