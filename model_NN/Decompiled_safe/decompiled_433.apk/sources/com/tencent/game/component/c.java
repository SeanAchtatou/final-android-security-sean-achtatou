package com.tencent.game.component;

import com.tencent.assistant.module.callback.b;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.protocol.jce.TagGroup;
import com.tencent.pangu.component.banner.f;
import java.util.List;

/* compiled from: ProGuard */
class c implements b {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameCategoryDetailListView f2659a;

    c(GameCategoryDetailListView gameCategoryDetailListView) {
        this.f2659a = gameCategoryDetailListView;
    }

    public void a(int i, int i2, boolean z, List<com.tencent.pangu.model.b> list, List<TagGroup> list2) {
        if (list != null) {
            if (this.f2659a.v == null) {
                this.f2659a.t();
            }
            this.f2659a.v.a(z, list, (List<f>) null, (List<ColorCardItem>) null);
        }
        if (!this.f2659a.y.isTagExisted()) {
            if (list2 != null && !list2.isEmpty()) {
                this.f2659a.y.setTagData(list2, this.f2659a.z);
                this.f2659a.a(list2);
            }
            this.f2659a.b(i, i2, z);
            return;
        }
        this.f2659a.a(i, i2, z);
    }
}
