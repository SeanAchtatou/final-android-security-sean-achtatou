package com.tencent.wcs.agent;

import com.tencent.wcs.agent.config.RemoteType;
import com.tencent.wcs.b.c;
import com.tencent.wcs.c.b;
import java.io.IOException;

/* compiled from: ProGuard */
class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f4055a;

    j(h hVar) {
        this.f4055a = hVar;
    }

    public void run() {
        try {
            if (this.f4055a.k.b() == RemoteType.WEB) {
                this.f4055a.g();
            } else if (this.f4055a.k.b() == RemoteType.PC) {
                this.f4055a.f();
            }
        } catch (IOException e) {
            e.printStackTrace();
            if (!this.f4055a.g) {
                if (c.f4064a) {
                    b.a("AgentSession read input exception channelId " + this.f4055a.m + " messageId " + this.f4055a.n + " port " + this.f4055a.o + " remote " + this.f4055a.k.a() + ", close it now");
                }
                synchronized (this.f4055a) {
                    this.f4055a.a(null, h.h(this.f4055a), 2);
                    if (this.f4055a.s != null) {
                        this.f4055a.s.a(this.f4055a.n);
                    }
                }
            }
        }
    }
}
