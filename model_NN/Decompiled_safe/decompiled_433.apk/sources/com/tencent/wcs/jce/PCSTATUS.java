package com.tencent.wcs.jce;

import java.io.Serializable;

/* compiled from: ProGuard */
public final class PCSTATUS implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final PCSTATUS f4092a = new PCSTATUS(0, 1, "PC_STATUS_IDLE");
    public static final PCSTATUS b = new PCSTATUS(1, 2, "PC_STATUS_BUSY");
    static final /* synthetic */ boolean c = (!PCSTATUS.class.desiredAssertionStatus());
    private static PCSTATUS[] d = new PCSTATUS[2];
    private int e;
    private String f = new String();

    public String toString() {
        return this.f;
    }

    private PCSTATUS(int i, int i2, String str) {
        this.f = str;
        this.e = i2;
        d[i] = this;
    }
}
