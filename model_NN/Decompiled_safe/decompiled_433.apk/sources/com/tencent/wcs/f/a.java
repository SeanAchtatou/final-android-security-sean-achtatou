package com.tencent.wcs.f;

import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/* compiled from: ProGuard */
public abstract class a<T extends Delayed> {

    /* renamed from: a  reason: collision with root package name */
    protected DelayQueue<T> f4069a;
    private Thread b;
    private volatile boolean c;

    public abstract void a(Delayed delayed);

    public a() {
        this.c = false;
        this.f4069a = new DelayQueue<>();
        this.c = true;
        this.b = new b(this);
        this.b.start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.concurrent.DelayQueue.poll(long, java.util.concurrent.TimeUnit):E throws java.lang.InterruptedException}
     arg types: [int, java.util.concurrent.TimeUnit]
     candidates:
      ClspMth{java.util.concurrent.DelayQueue.poll(long, java.util.concurrent.TimeUnit):java.lang.Object throws java.lang.InterruptedException}
      ClspMth{java.util.concurrent.BlockingQueue.poll(long, java.util.concurrent.TimeUnit):E throws java.lang.InterruptedException}
      ClspMth{java.util.concurrent.DelayQueue.poll(long, java.util.concurrent.TimeUnit):E throws java.lang.InterruptedException} */
    /* access modifiers changed from: private */
    public void b() {
        T t;
        while (this.c) {
            try {
                t = this.f4069a.poll(5L, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
                t = null;
            }
            if (t != null && this.c) {
                a((Delayed) t);
            }
        }
    }

    public void b(Delayed delayed) {
        if (this.f4069a != null && delayed != null) {
            this.f4069a.add(delayed);
        }
    }

    public void c(T t) {
        if (this.f4069a != null && t != null) {
            this.f4069a.remove(t);
        }
    }

    public void a() {
        if (this.f4069a != null) {
            this.f4069a.clear();
        }
    }
}
