package com.tencent.pangu.activity;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class ci extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f3360a;

    ci(SearchActivity searchActivity) {
        this.f3360a = searchActivity;
    }

    public void onTMAClick(View view) {
        this.f3360a.finish();
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3360a, 200);
        buildSTInfo.slotId = a.a("03", "001");
        return buildSTInfo;
    }
}
