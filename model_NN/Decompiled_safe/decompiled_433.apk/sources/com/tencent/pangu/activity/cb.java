package com.tencent.pangu.activity;

import android.view.View;
import com.tencent.pangu.activity.SearchActivity;

/* compiled from: ProGuard */
class cb implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f3353a;

    cb(SearchActivity searchActivity) {
        this.f3353a = searchActivity;
    }

    public void onClick(View view) {
        this.f3353a.B.setVisibility(8);
        if (this.f3353a.H() == SearchActivity.Layer.Search) {
            this.f3353a.C.setVisibility(0);
            this.f3353a.y();
            this.f3353a.C();
        }
    }
}
