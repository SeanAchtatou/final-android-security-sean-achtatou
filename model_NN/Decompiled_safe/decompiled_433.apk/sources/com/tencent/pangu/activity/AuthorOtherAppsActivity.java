package com.tencent.pangu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.adapter.AppAdapter;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.b;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.module.a.c;
import com.tencent.pangu.module.v;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class AuthorOtherAppsActivity extends BaseActivity implements ITXRefreshListViewListener, c {
    private TXGetMoreListView A;
    private AppAdapter B;
    private LoadingView C;
    private NormalErrorRecommendPage D;
    /* access modifiers changed from: private */
    public int E;
    private View.OnClickListener F = new ad(this);
    /* access modifiers changed from: private */
    public v n;
    /* access modifiers changed from: private */
    public String u;
    private String v;
    private ArrayList<SimpleAppModel> w;
    /* access modifiers changed from: private */
    public byte[] x;
    private boolean y;
    private SecondNavigationTitleViewV5 z;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.activity.AuthorOtherAppsActivity.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.pangu.activity.AuthorOtherAppsActivity.a(com.tencent.pangu.activity.AuthorOtherAppsActivity, int):int
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.activity.AuthorOtherAppsActivity.a(boolean, boolean):void */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        t();
        super.onCreate(bundle);
        setContentView((int) R.layout.author_other_apps_layout);
        this.n = new v();
        this.n.f3953a = 20;
        u();
        this.n.register(this);
        this.E = this.n.a(this.u);
        a(false, false);
    }

    /* access modifiers changed from: private */
    public void a(boolean z2, boolean z3) {
        if (z2) {
            this.C.setVisibility(4);
            if (z3) {
                this.A.setVisibility(0);
                this.D.setVisibility(4);
                return;
            }
            this.A.setVisibility(4);
            this.D.setVisibility(0);
            return;
        }
        this.C.setVisibility(0);
        this.A.setVisibility(4);
        this.D.setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.z.l();
        this.B.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.z.m();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.n.unregister(this);
        super.onDestroy();
    }

    public int f() {
        return STConst.ST_PAGE_APP_AUTHOR_RELATE;
    }

    private void t() {
        Intent intent = getIntent();
        this.u = intent.getStringExtra("pkgname");
        this.v = intent.getStringExtra("author_name");
        this.w = intent.getParcelableArrayListExtra("first_page_data");
        this.x = intent.getByteArrayExtra("page_context");
        this.y = intent.getBooleanExtra("has_next", false);
    }

    private void u() {
        this.z = (SecondNavigationTitleViewV5) findViewById(R.id.title);
        this.z.a(this);
        this.z.a(String.format(getString(R.string.author_other_title), this.v), TextUtils.TruncateAt.MIDDLE);
        this.z.i();
        this.A = (TXGetMoreListView) findViewById(R.id.list);
        this.C = (LoadingView) findViewById(R.id.loading_view);
        this.D = (NormalErrorRecommendPage) findViewById(R.id.error_page);
        this.D.setButtonClickListener(this.F);
        ImageView imageView = new ImageView(this);
        imageView.setLayoutParams(new AbsListView.LayoutParams(-1, by.a(this, 6.0f)));
        imageView.setBackgroundColor(getResources().getColor(17170445));
        this.A.addHeaderView(imageView);
        this.B = new AppAdapter(this, this.A, new b());
        this.B.a(f(), -100, "08");
        this.A.setAdapter(this.B);
        this.A.setDivider(null);
        this.A.onRefreshComplete(this.y);
        this.A.setRefreshListViewListener(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.activity.AuthorOtherAppsActivity.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.pangu.activity.AuthorOtherAppsActivity.a(com.tencent.pangu.activity.AuthorOtherAppsActivity, int):int
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.activity.AuthorOtherAppsActivity.a(boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.adapter.AppAdapter.a(boolean, java.util.List<com.tencent.assistant.model.SimpleAppModel>):void
     arg types: [int, java.util.List<com.tencent.assistant.model.SimpleAppModel>]
     candidates:
      com.tencent.assistant.adapter.AppAdapter.a(com.tencent.assistant.model.SimpleAppModel, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.assistant.adapter.AppAdapter.a(boolean, java.util.List<com.tencent.assistant.model.SimpleAppModel>):void */
    public void a(int i, int i2, boolean z2, List<SimpleAppModel> list, boolean z3, byte[] bArr) {
        if (i != this.E) {
            return;
        }
        if (i2 == 0) {
            if (list != null && list.size() != 0) {
                this.x = bArr;
                this.y = z3;
                this.B.a(false, list);
                this.B.notifyDataSetChanged();
                a(true, true);
                this.A.onRefreshComplete(this.y);
            } else if (this.B.getCount() > 0) {
                a(true, true);
                this.A.onRefreshComplete(false);
            } else {
                this.D.setErrorType(10);
                a(true, false);
            }
        } else if (i2 == -800) {
            this.D.setErrorType(30);
            a(true, false);
        } else if (this.B.getCount() > 0) {
            this.A.onRefreshComplete(z3, false);
            a(true, true);
        } else {
            this.D.setErrorType(20);
            a(true, false);
        }
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (TXScrollViewBase.ScrollState.ScrollState_FromEnd == scrollState && this.y) {
            this.E = this.n.a(this.u, this.x);
        }
    }

    public void q() {
        STInfoV2 p = p();
        if (p != null) {
            p.extraData = this.v + Constants.STR_EMPTY;
        }
        l.a(p);
    }
}
