package com.tencent.pangu.activity;

import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.invalidater.TXRefreshGetMoreListViewScrollListener;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXRefreshGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.activity.a;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.assistantv2.st.business.LaunchSpeedSTManager;
import com.tencent.pangu.a.b;
import com.tencent.pangu.adapter.DiscoverPageListAdapter;
import com.tencent.pangu.component.banner.f;
import com.tencent.pangu.component.treasurebox.AppTreasureEntryBlinkEyesView;
import com.tencent.pangu.manager.x;
import com.tencent.pangu.model.g;
import com.tencent.pangu.module.a.h;
import com.tencent.pangu.module.ar;
import java.util.List;

/* compiled from: ProGuard */
public class aw extends a implements ITXRefreshListViewListener, UIEventListener, h {
    b R = new b();
    com.tencent.pangu.component.topbanner.a S = new bd(this);
    private final String T = "FoundTabActivity";
    /* access modifiers changed from: private */
    public TXRefreshGetMoreListView U;
    private ViewStub V;
    /* access modifiers changed from: private */
    public NormalErrorRecommendPage W = null;
    /* access modifiers changed from: private */
    public LoadingView X;
    /* access modifiers changed from: private */
    public x Y = x.a();
    /* access modifiers changed from: private */
    public SmartListAdapter Z = null;
    /* access modifiers changed from: private */
    public ar aa = null;
    private boolean ab = false;
    private boolean ac = false;
    private TXRefreshGetMoreListViewScrollListener ad = new TXRefreshGetMoreListViewScrollListener();
    private LinearLayout ae;
    /* access modifiers changed from: private */
    public boolean af = false;
    /* access modifiers changed from: private */
    public ViewStub ag;
    private AppTreasureEntryBlinkEyesView ah;
    private TextView ai;

    public void d(Bundle bundle) {
        super.d(bundle);
        LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.Found_onCreate_Begin);
        System.currentTimeMillis();
        this.Y.a(this);
        this.ae = new LinearLayout(this.P);
        ImageView imageView = new ImageView(this.P);
        imageView.setBackgroundColor(this.P.getResources().getColor(R.color.rank_navigation_bg));
        this.ae.addView(imageView, new LinearLayout.LayoutParams(-1, -1));
        a(this.ae);
    }

    public void b(View view) {
        this.U = (TXRefreshGetMoreListView) view.findViewById(R.id.list);
        if (this.U != null) {
            this.U.setRefreshListViewListener(this);
            this.U.setVisibility(8);
            this.U.setDivider(null);
            this.U.setListSelector(17170445);
            this.V = (ViewStub) view.findViewById(R.id.error_stub);
            this.X = (LoadingView) view.findViewById(R.id.loading);
            this.U.setIScrollerListener(this.ad);
            this.ag = (ViewStub) view.findViewById(R.id.treasurebox_stub);
        }
    }

    private void H() {
        LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.Found_initFirstPage_Begin);
        this.Z = new DiscoverPageListAdapter(this.P, this.U, this.Y.l());
        this.U.setAdapter(this.Z);
        this.Z.a(true);
        this.Z.a(this.ad);
        this.Z.a(this.S);
        boolean g = this.Y.g();
        int c = this.Y.c();
        if (g && this.Z != null) {
            this.ab = true;
            ah.a().postDelayed(new ax(this), 300);
        } else if (c != 0) {
            d(30);
        } else {
            this.Y.b();
        }
    }

    private void d(int i) {
        if (this.W == null) {
            I();
        }
        this.W.setErrorType(i);
        if (this.U != null) {
            this.U.setVisibility(8);
        }
        this.X.setVisibility(8);
        this.W.setVisibility(0);
        e(false);
    }

    private void I() {
        this.V.inflate();
        this.W = (NormalErrorRecommendPage) this.ae.findViewById(R.id.error);
        this.W.setButtonClickListener(new ay(this));
    }

    public void d(boolean z) {
        XLog.d("YYB5_0", "found onResume");
        if (!this.ac) {
            this.ac = true;
            this.ae.removeAllViews();
            try {
                View inflate = this.Q.inflate((int) R.layout.act2, (ViewGroup) null);
                this.ae.addView(inflate);
                b(inflate);
                H();
                ah.a().postDelayed(new az(this), 2000);
                LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.Found_Init_End);
            } catch (Exception e) {
                XLog.d("FoundTabActivity", "has exception: " + e.getMessage());
                return;
            }
        } else if (this.Y != null) {
            this.Y.m();
        }
        if (this.Z != null) {
            if (this.Z.k()) {
                this.Z.b(true);
            }
            this.Z.notifyDataSetChanged();
        }
        if (this.Z != null && !this.Z.f() && this.Z.g()) {
            if (this.X != null) {
                this.X.setVisibility(8);
            }
            d(30);
        }
        if (this.R.a() == null) {
            return;
        }
        if (this.af) {
            ah.a().post(new ba(this));
        } else {
            ah.a().postDelayed(new bb(this), 2500);
        }
    }

    public void k() {
        super.k();
    }

    public void n() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_SHOW_TREASURE_BOX_ENTRY, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_DISMISS_TREASURE_BOX_ENTRY, this);
        super.n();
    }

    public void i() {
        super.i();
    }

    public aw() {
        super(MainActivity.t());
    }

    public void D() {
    }

    public int E() {
        return 0;
    }

    public int F() {
        return 1;
    }

    public int G() {
        return STConst.ST_PAGE_COMPETITIVE;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.x.a(boolean, com.tencent.pangu.module.ar):void
     arg types: [int, com.tencent.pangu.module.ar]
     candidates:
      com.tencent.pangu.manager.x.a(com.tencent.pangu.manager.x, int):int
      com.tencent.pangu.manager.x.a(com.tencent.pangu.manager.x, long):long
      com.tencent.pangu.manager.x.a(com.tencent.pangu.manager.x, java.util.List):java.util.List
      com.tencent.pangu.manager.x.a(com.tencent.assistant.protocol.jce.TopBanner, long):void
      com.tencent.pangu.manager.x.a(boolean, com.tencent.pangu.module.ar):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.x.a(boolean, com.tencent.pangu.module.ar):void
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.pangu.manager.x.a(com.tencent.pangu.manager.x, int):int
      com.tencent.pangu.manager.x.a(com.tencent.pangu.manager.x, long):long
      com.tencent.pangu.manager.x.a(com.tencent.pangu.manager.x, java.util.List):java.util.List
      com.tencent.pangu.manager.x.a(com.tencent.assistant.protocol.jce.TopBanner, long):void
      com.tencent.pangu.manager.x.a(boolean, com.tencent.pangu.module.ar):void */
    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (TXScrollViewBase.ScrollState.ScrollState_FromEnd == scrollState) {
            this.Y.a(true, this.aa);
        }
        if (TXScrollViewBase.ScrollState.ScrollState_FromStart == scrollState) {
            this.Y.a(false, (ar) null);
            this.ab = false;
        }
    }

    public void a(int i, int i2, boolean z, ar arVar, boolean z2, List<f> list, g gVar, List<com.tencent.pangu.model.b> list2) {
        if (z2 && (LaunchSpeedSTManager.d().b(i) || i == -1)) {
            LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.Found_onDataLoad_Begin);
        }
        if (this.X != null) {
            this.X.setVisibility(8);
        }
        XLog.d("icerao", "on page dataload.errorcode:" + i2 + ",data size:" + (list2 != null ? list2.size() : 0) + ",is firstpage:" + z2);
        if (i == -1 || i2 == 0) {
            if (this.W != null) {
                this.W.setVisibility(8);
            }
            this.U.setVisibility(0);
            this.aa = arVar;
            e(true);
            if (list2 == null || list2.size() == 0) {
                if (!z2) {
                    this.U.onRefreshComplete(z, false);
                } else if (!this.Z.f()) {
                    d(30);
                } else {
                    this.U.onRefreshComplete(false, z, a((int) R.string.refresh_fail));
                }
            } else if (!z2) {
                this.U.onRefreshComplete(z, true);
                this.Z.a(false, list2, list, gVar, null, this.Y.n());
            } else if (this.ab) {
                this.ab = false;
            } else {
                this.Z.a(true, list2, list, gVar, null, this.Y.n());
                this.U.setSelection(0);
                if (i != -1) {
                    this.U.onRefreshComplete(true, z, null);
                } else if (i2 == -800) {
                    this.U.onRefreshComplete(false, z, a((int) R.string.refresh_fail_network));
                } else {
                    this.U.onRefreshComplete(false, z, a((int) R.string.refresh_fail));
                }
                if (LaunchSpeedSTManager.d().b(i) || i == -1) {
                    LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.SmartListAdapter_refreshData_End);
                }
            }
        } else if (!z2) {
            this.U.onRefreshComplete(z, false);
        } else if (!this.Z.f()) {
            if (i2 == -800) {
                d(30);
            } else {
                d(20);
            }
        } else if (i2 == -800) {
            this.U.onRefreshComplete(false, z, a((int) R.string.refresh_fail_network));
        } else {
            this.U.onRefreshComplete(false, z, a((int) R.string.refresh_fail));
        }
    }

    /* access modifiers changed from: private */
    public boolean J() {
        MainActivity mainActivity = null;
        if (this.P instanceof MainActivity) {
            mainActivity = (MainActivity) this.P;
        }
        if (mainActivity == null) {
            return false;
        }
        Bundle u = mainActivity.u();
        return u != null ? u.getInt("param_competitive_tab_show_treasure_box_entry") == 1 : false;
    }

    /* access modifiers changed from: private */
    public void e(boolean z) {
        try {
            if (this.ag == null) {
                return;
            }
            if (z) {
                this.ag.setVisibility(0);
            } else {
                this.ag.setVisibility(8);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_SHOW_TREASURE_BOX_ENTRY:
                this.ah.setVisibility(0);
                this.ai.setVisibility(0);
                return;
            case EventDispatcherEnum.UI_EVENT_DISMISS_TREASURE_BOX_ENTRY:
                this.ah.setVisibility(8);
                this.ai.setVisibility(8);
                TemporaryThreadManager.get().start(new bc(this));
                return;
            default:
                return;
        }
    }

    public void C() {
        B();
    }
}
