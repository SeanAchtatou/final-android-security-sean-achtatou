package com.tencent.pangu.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListRequest;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse;
import com.tencent.assistant.utils.t;
import com.tencent.pangu.module.a.g;
import java.text.SimpleDateFormat;

/* compiled from: ProGuard */
public class au extends BaseEngine<g> {

    /* renamed from: a  reason: collision with root package name */
    SimpleDateFormat f3912a = new SimpleDateFormat("yyyy-MM-dd");
    GetPhoneUserAppListRequest b = new GetPhoneUserAppListRequest();

    public int a() {
        this.b.f1314a = t.y() / 1000;
        return send(this.b);
    }

    public void a(boolean z) {
        this.b.b = z;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new av(this, i, (GetPhoneUserAppListResponse) jceStruct2));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new aw(this, i, i2));
    }
}
