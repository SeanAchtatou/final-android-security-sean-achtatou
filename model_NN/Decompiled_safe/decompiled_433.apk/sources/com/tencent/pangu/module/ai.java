package com.tencent.pangu.module;

import com.tencent.assistant.manager.i;
import com.tencent.assistant.protocol.jce.GetDiscoverRequest;
import com.tencent.assistant.protocol.jce.GetDiscoverResponse;
import com.tencent.assistant.protocol.jce.GetSmartCardsResponse;

/* compiled from: ProGuard */
class ai implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GetDiscoverRequest f3900a;
    final /* synthetic */ GetDiscoverResponse b;
    final /* synthetic */ boolean c;
    final /* synthetic */ GetSmartCardsResponse d;
    final /* synthetic */ ad e;

    ai(ad adVar, GetDiscoverRequest getDiscoverRequest, GetDiscoverResponse getDiscoverResponse, boolean z, GetSmartCardsResponse getSmartCardsResponse) {
        this.e = adVar;
        this.f3900a = getDiscoverRequest;
        this.b = getDiscoverResponse;
        this.c = z;
        this.d = getSmartCardsResponse;
    }

    public void run() {
        i.y().a(this.f3900a.b, this.b);
        if (this.c) {
            i.y().a(this.d);
        }
    }
}
