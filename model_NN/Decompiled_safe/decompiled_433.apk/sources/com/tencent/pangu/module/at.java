package com.tencent.pangu.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.CardItem;
import com.tencent.assistant.protocol.jce.GetPopupNecessaryRequest;
import com.tencent.assistant.protocol.jce.GetPopupNecessaryResponse;
import com.tencent.assistant.protocol.jce.PopupScene;
import com.tencent.assistant.utils.XLog;
import com.tencent.pangu.module.callback.GetPopUpNecessaryCallback;
import java.util.Iterator;

/* compiled from: ProGuard */
public class at extends BaseEngine<GetPopUpNecessaryCallback> {

    /* renamed from: a  reason: collision with root package name */
    private static at f3911a = null;

    private at() {
    }

    public static synchronized at a() {
        at atVar;
        synchronized (at.class) {
            if (f3911a == null) {
                f3911a = new at();
            }
            atVar = f3911a;
        }
        return atVar;
    }

    public void b() {
        XLog.i("GetPopUpNecessaryEngine", "sendRequest!");
        send(new GetPopupNecessaryRequest());
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        if (jceStruct2 != null && (jceStruct2 instanceof GetPopupNecessaryResponse)) {
            GetPopupNecessaryResponse getPopupNecessaryResponse = (GetPopupNecessaryResponse) jceStruct2;
            Iterator<PopupScene> it = getPopupNecessaryResponse.a().iterator();
            while (it.hasNext()) {
                PopupScene next = it.next();
                XLog.i("GetPopUpNecessaryEngine", "scene imageUrl:" + next.c + " installThreshold:" + next.b + " title:" + next.f1437a);
                Iterator<CardItem> it2 = next.a().iterator();
                while (it2.hasNext()) {
                    CardItem next2 = it2.next();
                    XLog.i("GetPopUpNecessaryEngine", "cardItem list:" + next2.f.f + " appName:" + next2.f.b);
                }
            }
            getPopupNecessaryResponse.d.get(0).a();
            i.y().a(getPopupNecessaryResponse);
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.i("GetPopUpNecessaryEngine", "failed:" + i2);
    }

    public GetPopupNecessaryResponse c() {
        return i.y().c();
    }
}
