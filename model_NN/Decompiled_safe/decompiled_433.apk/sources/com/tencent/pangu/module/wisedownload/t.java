package com.tencent.pangu.module.wisedownload;

import android.text.TextUtils;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class t {
    public static List<DownloadInfo> a(boolean z, boolean z2) {
        ArrayList arrayList = new ArrayList();
        ArrayList<DownloadInfo> d = DownloadProxy.a().d();
        if (d == null || d.isEmpty()) {
            return null;
        }
        Iterator<DownloadInfo> it = d.iterator();
        while (it.hasNext()) {
            DownloadInfo next = it.next();
            if (next.uiType == SimpleDownloadInfo.UIType.WISE_BOOKING_DOWNLOAD) {
                arrayList.add(next);
            }
        }
        if (z) {
            if (arrayList.isEmpty()) {
                return null;
            }
            d.clear();
            d.addAll(arrayList);
            arrayList.clear();
            Iterator<DownloadInfo> it2 = d.iterator();
            while (it2.hasNext()) {
                DownloadInfo next2 = it2.next();
                if (next2.downloadState == SimpleDownloadInfo.DownloadState.SUCC) {
                    arrayList.add(next2);
                }
            }
        }
        if (z2) {
            if (arrayList.isEmpty()) {
                return null;
            }
            d.clear();
            d.addAll(arrayList);
            arrayList.clear();
            Iterator<DownloadInfo> it3 = d.iterator();
            while (it3.hasNext()) {
                DownloadInfo next3 = it3.next();
                String filePath = next3.getFilePath();
                if (!TextUtils.isEmpty(filePath) && new File(filePath).exists()) {
                    arrayList.add(next3);
                }
            }
        }
        return arrayList;
    }
}
