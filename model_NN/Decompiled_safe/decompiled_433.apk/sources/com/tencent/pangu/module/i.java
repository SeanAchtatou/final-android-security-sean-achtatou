package com.tencent.pangu.module;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class i extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f3937a;

    i(h hVar) {
        this.f3937a = hVar;
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        if (localApkInfo != null && i == 1) {
            this.f3937a.a(localApkInfo);
        }
    }
}
