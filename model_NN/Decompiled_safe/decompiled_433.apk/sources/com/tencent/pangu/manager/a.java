package com.tencent.pangu.manager;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.Global;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.g;
import com.tencent.assistant.manager.h;
import com.tencent.assistant.protocol.jce.ApkAutoOpenCfg;
import com.tencent.assistant.protocol.jce.ApkAutoOpenVec;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.an;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.utils.installuninstall.ac;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/* compiled from: ProGuard */
public class a implements h {

    /* renamed from: a  reason: collision with root package name */
    protected static a f3783a = null;
    protected ArrayList<ApkAutoOpenCfg> b = null;

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (f3783a == null) {
                f3783a = new a();
            }
            aVar = f3783a;
        }
        return aVar;
    }

    private a() {
        g.a().a(this);
        b();
    }

    public void a(HashMap<String, Object> hashMap) {
        b();
    }

    private void b() {
        ApkAutoOpenVec apkAutoOpenVec;
        byte[] e = m.a().e("key_get_apk_auto_open_action");
        if (!(e == null || e.length <= 0 || (apkAutoOpenVec = (ApkAutoOpenVec) an.b(e, ApkAutoOpenVec.class)) == null)) {
            this.b = new ArrayList<>(apkAutoOpenVec.f1133a);
        }
        XLog.d("ApkAutoOpenCfgManager", "loadConfigs, apkCfgs:" + this.b);
    }

    public ApkAutoOpenCfg a(String str, String str2) {
        XLog.d("ApkAutoOpenCfgManager", "getApkAutoOpenCfg:" + this.b);
        if (this.b == null) {
            return null;
        }
        Iterator<ApkAutoOpenCfg> it = this.b.iterator();
        while (it.hasNext()) {
            ApkAutoOpenCfg next = it.next();
            if (next != null && next.f1132a.equalsIgnoreCase(str) && next.d.equalsIgnoreCase(str2)) {
                return next;
            }
        }
        return null;
    }

    public boolean a(DownloadInfo downloadInfo) {
        return downloadInfo.installType == 1 || downloadInfo.installType == 2;
    }

    public boolean a(DownloadInfo downloadInfo, ApkAutoOpenCfg apkAutoOpenCfg) {
        boolean z;
        if (downloadInfo == null || apkAutoOpenCfg == null) {
            return false;
        }
        if (apkAutoOpenCfg.b != 1) {
            return false;
        }
        long an = m.a().an();
        boolean z2 = an > 0 && System.currentTimeMillis() - an < 300000;
        if (DownloadProxy.a().g() > 0 || !ac.a().c()) {
            z = true;
        } else {
            z = false;
        }
        if (!a(downloadInfo) || apkAutoOpenCfg.i != 1) {
            if (!a(downloadInfo) && apkAutoOpenCfg.i == 0) {
                if (apkAutoOpenCfg.j == 0 && !z2) {
                    return true;
                }
                if (apkAutoOpenCfg.j == 1 && b(downloadInfo) && !z) {
                    return true;
                }
            }
        } else if (apkAutoOpenCfg.j == 0 && !z2) {
            return true;
        } else {
            if (apkAutoOpenCfg.j == 1 && b(downloadInfo) && !z) {
                return true;
            }
        }
        return false;
    }

    public void a(LocalApkInfo localApkInfo, DownloadInfo downloadInfo, ApkAutoOpenCfg apkAutoOpenCfg) {
        if (!a(downloadInfo)) {
            XLog.d("ApkAutoOpenCfgManager", "packagename = " + downloadInfo.packageName + " 的最终安装方式=" + downloadInfo.installType);
        } else {
            XLog.d("ApkAutoOpenCfgManager", "packagename = " + downloadInfo.packageName + " 不打开，先走安装推荐，如果安装推荐不成功");
        }
    }

    public void b(LocalApkInfo localApkInfo, DownloadInfo downloadInfo, ApkAutoOpenCfg apkAutoOpenCfg) {
        XLog.d("ApkAutoOpenCfgManager", "processAutoOpenInstalledApp, packagename = " + downloadInfo.packageName + ", chanelid = " + downloadInfo.channelId + ", dowloadInfo.installtype = " + downloadInfo.installType + ", cfg.installType = " + apkAutoOpenCfg.i + ", cfg.callType = " + apkAutoOpenCfg.j + ", d.fromOutCall =" + downloadInfo.fromOutCall);
        if (!a(downloadInfo) || apkAutoOpenCfg.i != 1) {
            if (!a(downloadInfo) && apkAutoOpenCfg.i == 0) {
                if (apkAutoOpenCfg.j == 0) {
                    XLog.d("ApkAutoOpenCfgManager", " 命中，非静默安装不限是否外部call起 - 直接打开");
                    a(localApkInfo, downloadInfo, apkAutoOpenCfg.c);
                } else if (apkAutoOpenCfg.j == 1 && b(downloadInfo)) {
                    XLog.d("ApkAutoOpenCfgManager", " 命中，外部call起的非静默安装 - 直接打开");
                    b(downloadInfo, apkAutoOpenCfg);
                }
            }
        } else if (apkAutoOpenCfg.j == 0) {
            XLog.d("ApkAutoOpenCfgManager", " 命中，静默安装不限是否外部call起 - 直接打开");
            a(localApkInfo, downloadInfo, apkAutoOpenCfg.c);
        } else if (apkAutoOpenCfg.j == 1 && b(downloadInfo)) {
            XLog.d("ApkAutoOpenCfgManager", " 命中，外部call起的静默安装 - 直接打开");
            b(downloadInfo, apkAutoOpenCfg);
        }
    }

    private void a(LocalApkInfo localApkInfo, DownloadInfo downloadInfo, String str) {
        long an = m.a().an();
        long currentTimeMillis = System.currentTimeMillis();
        if (an <= 0 || currentTimeMillis - an >= 300000) {
            if (TextUtils.isEmpty(str)) {
                com.tencent.pangu.download.a.a().c(localApkInfo.mPackageName);
            } else {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                intent.addFlags(268435456);
                AstApp.i().getApplicationContext().startActivity(intent);
            }
            m.a().k(currentTimeMillis);
            STInfoV2 sTInfoV2 = new STInfoV2(-1, null, -1, null, STConstAction.ACTION_HIT_AUTO_OPEN);
            sTInfoV2.updateWithDownloadInfo(downloadInfo);
            sTInfoV2.recommendId = downloadInfo.statInfo.recommendId;
            sTInfoV2.isImmediately = true;
            l.a(sTInfoV2);
        }
    }

    private boolean b(DownloadInfo downloadInfo, ApkAutoOpenCfg apkAutoOpenCfg) {
        if (DownloadProxy.a().g() > 0 || !ac.a().c()) {
            XLog.d("ApkAutoOpenCfgManager", "downloading size > 0 || installing task size > 0");
            return false;
        }
        b(downloadInfo.packageName, apkAutoOpenCfg.c);
        XLog.d("ApkAutoOpenCfgManager", "match outer call");
        return true;
    }

    private boolean b(DownloadInfo downloadInfo) {
        if (downloadInfo != null) {
            return downloadInfo.fromOutCall;
        }
        return false;
    }

    private void b(String str, String str2) {
        if (str != null) {
            com.tencent.pangu.download.a.a().a(str, str2);
            HashMap hashMap = new HashMap();
            hashMap.put("B1", Global.getQUA());
            hashMap.put("B2", Global.getPhoneGuidAndGen());
            hashMap.put("B3", str);
            com.tencent.beacon.event.a.a("callActiveAfterInstall", true, -1, -1, hashMap, true);
        }
    }

    public void a(LocalApkInfo localApkInfo, ArrayList<DownloadInfo> arrayList) {
        ApkAutoOpenCfg a2;
        if (localApkInfo != null && !TextUtils.isEmpty(localApkInfo.mPackageName)) {
            Iterator<DownloadInfo> it = arrayList.iterator();
            while (it.hasNext()) {
                DownloadInfo next = it.next();
                if (next != null && next.packageName.equals(localApkInfo.mPackageName) && next.versionCode == localApkInfo.mVersionCode && (a2 = a(localApkInfo.mPackageName, next.channelId)) != null) {
                    switch (a2.b) {
                        case 1:
                            b(localApkInfo, next, a2);
                            continue;
                        case 2:
                            String str = a2.c;
                            if (!TextUtils.isEmpty(str)) {
                                try {
                                    AstApp.i().getApplicationContext().startService(new Intent(str));
                                    STInfoV2 sTInfoV2 = new STInfoV2(-1, null, -1, null, STConstAction.ACTION_HIT_AUTO_OPEN_PROCESS);
                                    sTInfoV2.updateWithDownloadInfo(next);
                                    sTInfoV2.recommendId = next.statInfo.recommendId;
                                    sTInfoV2.isImmediately = true;
                                    l.a(sTInfoV2);
                                    break;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    break;
                                }
                            } else {
                                continue;
                            }
                        case 4:
                            a(localApkInfo, next, a2);
                            continue;
                    }
                }
            }
        }
    }
}
