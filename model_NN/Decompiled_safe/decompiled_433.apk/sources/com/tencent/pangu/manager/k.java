package com.tencent.pangu.manager;

import com.tencent.downloadsdk.DownloadManager;
import com.tencent.pangu.download.DownloadInfo;
import java.util.List;

/* compiled from: ProGuard */
class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f3834a;
    final /* synthetic */ DownloadProxy b;

    k(DownloadProxy downloadProxy, List list) {
        this.b = downloadProxy;
        this.f3834a = list;
    }

    public void run() {
        for (DownloadInfo downloadInfo : this.f3834a) {
            DownloadManager.a().a(downloadInfo.getDownloadSubType(), downloadInfo.downloadTicket);
        }
    }
}
