package com.tencent.pangu.utils;

import android.widget.Toast;
import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.an;

/* compiled from: ProGuard */
class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f3981a;
    final /* synthetic */ f b;

    h(f fVar, boolean z) {
        this.b = fVar;
        this.f3981a = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.utils.e.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.tencent.pangu.utils.e.a(android.content.Context, com.tencent.assistant.protocol.jce.GetPopupNecessaryResponse):void
      com.tencent.pangu.utils.e.a(android.content.Context, boolean):void */
    public void run() {
        if (this.b.f3979a == null || !(this.b.f3979a.b == 1 || this.b.f3979a.b == 3)) {
            if (this.b.f3979a != null) {
                XLog.e("zhangyuanchao", "-------mRecoverAppList------:" + this.b.f3979a.b + "," + this.b.f3979a.c.size());
            }
            if (this.f3981a) {
                GetPhoneUserAppListResponse getPhoneUserAppListResponse = (GetPhoneUserAppListResponse) an.b(m.a().e("key_re_app_list_first_response"), GetPhoneUserAppListResponse.class);
                if (getPhoneUserAppListResponse != null) {
                    e.a(this.b.c, false, this.b.d, getPhoneUserAppListResponse, this.b.b);
                } else {
                    Toast.makeText(this.b.c, "恭喜你，你已经恢复了所有的应用！", 4).show();
                }
            } else {
                Toast.makeText(this.b.c, "恭喜你，你已经安装了装机必备的应用！", 4).show();
            }
        } else {
            e.a(this.b.c, true);
        }
    }
}
