package com.tencent.pangu.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.module.b;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.activity.a;
import com.tencent.pangu.adapter.RankNormalListAdapter;

/* compiled from: ProGuard */
public class RankNormalListPage extends RelativeLayout implements NetworkMonitor.ConnectivityChangeListener, ar {

    /* renamed from: a  reason: collision with root package name */
    protected Context f3498a = null;
    protected LayoutInflater b = null;
    protected LoadingView c;
    protected NormalErrorRecommendPage d;
    protected RankNormalListView e;
    protected View.OnClickListener f = new am(this);
    private APN g = APN.NO_NETWORK;

    public RankNormalListPage(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f3498a = context;
        a(context);
    }

    public RankNormalListPage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3498a = context;
        a(context);
    }

    public RankNormalListPage(Context context, TXScrollViewBase.ScrollMode scrollMode, b bVar) {
        super(context);
        this.f3498a = context;
        a(context);
        this.e.a(bVar);
        this.e.a(this);
        this.e.a((ar) this);
        this.e.setDivider(null);
    }

    /* access modifiers changed from: protected */
    public void a(Context context) {
        this.b = LayoutInflater.from(context);
        View inflate = this.b.inflate((int) R.layout.ranknormallist_component_view, this);
        this.e = (RankNormalListView) inflate.findViewById(R.id.applist);
        this.e.setVisibility(8);
        this.c = (LoadingView) inflate.findViewById(R.id.loading_view);
        this.c.setVisibility(0);
        this.d = (NormalErrorRecommendPage) inflate.findViewById(R.id.error_page);
        this.d.setButtonClickListener(this.f);
        this.d.setIsAutoLoading(true);
        t.a().a(this);
    }

    public void a(int i) {
        this.c.setVisibility(8);
        this.e.setVisibility(8);
        this.d.setVisibility(0);
        this.d.setErrorType(i);
    }

    public void e() {
        this.c.setVisibility(0);
        this.e.setVisibility(8);
        this.d.setVisibility(8);
    }

    public void a(RankNormalListAdapter rankNormalListAdapter, ListViewScrollListener listViewScrollListener) {
        this.e.t();
        this.e.setAdapter(rankNormalListAdapter);
        this.e.a(listViewScrollListener);
        rankNormalListAdapter.a(listViewScrollListener);
    }

    public void b() {
        e();
        this.e.b(h());
    }

    public void f() {
        this.e.setVisibility(0);
        this.d.setVisibility(8);
        this.c.setVisibility(8);
    }

    public void g() {
    }

    public void c() {
    }

    public void d() {
        if (this.e != null) {
            this.e.s();
        }
    }

    public int a() {
        b u = this.e.u();
        if (u == null || u.f975a != 0) {
            return 2000;
        }
        if (u.b == 6) {
            return STConst.ST_PAGE_RANK_HOT;
        }
        if (u.b == 5) {
            return STConst.ST_PAGE_RANK_CLASSIC;
        }
        if (u.b == 2) {
            return STConst.ST_PAGE_RANK_RECOMMEND;
        }
        return 2000;
    }

    public void onConnected(APN apn) {
        if (this.e.w() == null || this.e.w().getCount() <= 0) {
        }
    }

    public void onDisconnected(APN apn) {
        this.g = apn;
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        this.g = apn2;
    }

    public boolean h() {
        return this.e == null || this.e.x();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.component.RankNormalListView.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.tencent.pangu.component.RankNormalListView.a(com.tencent.pangu.component.RankNormalListView, java.util.List):boolean
      com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView.a(com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView, boolean):boolean
      com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView.a(android.content.Context, com.tencent.assistant.component.txscrollview.TXScrollViewBase$ScrollMode):com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase
      com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase.a(android.content.Context, com.tencent.assistant.component.txscrollview.TXScrollViewBase$ScrollMode):com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase
      com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase.a(int, int):void
      com.tencent.assistant.component.txscrollview.TXScrollViewBase.a(android.content.Context, android.view.View):void
      com.tencent.assistant.component.txscrollview.TXScrollViewBase.a(int, int):void
      com.tencent.pangu.component.RankNormalListView.a(boolean, boolean):void */
    public void a(boolean z) {
        if (this.e != null) {
            this.e.a(z, false);
        }
    }

    public void a(a aVar) {
        this.e.a(aVar);
    }
}
