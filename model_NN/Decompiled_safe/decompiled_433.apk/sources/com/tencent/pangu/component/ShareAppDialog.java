package com.tencent.pangu.component;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.activity.ShareBaseActivity;
import com.tencent.pangu.c.a;
import com.tencent.pangu.model.ShareAppModel;
import com.tencent.pangu.model.ShareBaseModel;

/* compiled from: ProGuard */
public class ShareAppDialog extends Dialog implements bn {

    /* renamed from: a  reason: collision with root package name */
    protected int f3505a;
    protected View b;
    protected TextView c;
    private ShareBaseActivity d;
    private ShareAppBar e;
    private a f;
    private String g;
    private int h = 0;
    private LinearLayout i = null;
    private TextView j = null;
    private ShareAppBar k = null;

    public ShareAppDialog(ShareBaseActivity shareBaseActivity, int i2, View view) {
        super(shareBaseActivity, i2);
        this.d = shareBaseActivity;
        this.b = view;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.f3505a != 0) {
            this.b = LayoutInflater.from(this.d).inflate(this.f3505a, (ViewGroup) null);
            setContentView(this.b);
        } else if (this.b != null) {
            setContentView(this.b);
        } else {
            this.b = LayoutInflater.from(this.d).inflate((int) R.layout.dialog_share_yyb, (ViewGroup) null);
            this.i = (LinearLayout) this.b.findViewById(R.id.share_app_bottom_bar_layout);
            this.j = (TextView) this.b.findViewById(R.id.msg);
            this.k = (ShareAppBar) this.b.findViewById(R.id.layout_share);
            if (this.k != null) {
                this.k.a(this);
            }
            setContentView(this.b);
        }
        this.c = (TextView) findViewById(R.id.title);
        if (!TextUtils.isEmpty(this.g)) {
            this.c.setText(this.g);
        }
        this.e = (ShareAppBar) findViewById(R.id.layout_share);
        this.f = new bo(this, this.d, this.e);
        this.f.a(e());
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        XLog.i("ShareYYBDialog", "onStart");
        this.f.e();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        XLog.i("ShareYYBDialog", "onStop");
        this.f.f();
    }

    public void show() {
        super.show();
        a(100, STConst.ST_DEFAULT_SLOT);
    }

    public void a(ShareAppModel shareAppModel) {
        if (this.f != null) {
            this.f.a(shareAppModel);
        }
    }

    public void a(ShareBaseModel shareBaseModel) {
        if (this.f != null) {
            this.f.a(shareBaseModel);
        }
    }

    public void a(int i2) {
        this.h = i2;
    }

    public void b(int i2) {
        if (i2 != 0) {
            this.g = this.d.getString(i2);
        }
    }

    public void b() {
        this.e.a();
    }

    private int d() {
        if (this.d == null || !(this.d instanceof BaseActivity)) {
            return 0;
        }
        return this.d.f();
    }

    private int e() {
        return this.h == 0 ? STConst.ST_PAGE_SHARE_YYB : this.h;
    }

    private void a(int i2, String str) {
        a(i2, str, Constants.STR_EMPTY);
    }

    private void a(int i2, String str, String str2) {
        STInfoV2 sTInfoV2 = new STInfoV2(e(), str, d(), STConst.ST_DEFAULT_SLOT, i2);
        if (sTInfoV2 != null) {
            sTInfoV2.extraData = str2;
            XLog.i("ShareYYBDialog", "[logReportV2] --> pageId = " + sTInfoV2.scene + ", slotId = " + str + ", prePageId = " + sTInfoV2.sourceScene + ", actionId = " + i2 + ", extraData = " + str2);
            l.a(sTInfoV2);
        }
    }

    public Bitmap c() {
        if (this.b != null) {
            return by.a(this.b);
        }
        return null;
    }

    public void a() {
        if (this.i != null) {
            this.i.setVisibility(8);
        }
        if (this.j != null) {
            this.j.setVisibility(8);
        }
    }
}
