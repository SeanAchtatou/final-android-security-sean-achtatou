package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;

/* compiled from: ProGuard */
public class CustomTextView extends TextView {

    /* renamed from: a  reason: collision with root package name */
    private ak f3542a = null;
    private boolean b = false;

    public CustomTextView(Context context) {
        super(context);
    }

    public CustomTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public CustomTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void a(ak akVar) {
        this.f3542a = akVar;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.b) {
            if (this.f3542a != null) {
                this.f3542a.a();
            }
            this.b = true;
        }
    }
}
