package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.nucleus.socialcontact.comment.bc;
import com.tencent.nucleus.socialcontact.comment.g;
import com.tencent.nucleus.socialcontact.login.j;
import com.tencent.pangu.component.CommentDetailView;
import com.tencent.pangu.component.l;
import java.util.Map;

/* compiled from: ProGuard */
public class CommentDetailTabView extends LinearLayout implements bc, be {

    /* renamed from: a  reason: collision with root package name */
    private InnerScrollView f3539a;
    private CommentDetailView b;
    private boolean c = false;

    public CommentDetailTabView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public CommentDetailTabView(Context context, l lVar) {
        super(context);
        a(context);
        if (this.b != null) {
            this.b.a(lVar);
        }
    }

    private void a(Context context) {
        try {
            View inflate = LayoutInflater.from(context).inflate((int) R.layout.commentdetail_view_layout, this);
            this.c = true;
            this.f3539a = (InnerScrollView) inflate.findViewById(R.id.inner_scrollview);
            this.f3539a.a(this);
            this.b = (CommentDetailView) inflate.findViewById(R.id.detail_view);
        } catch (Throwable th) {
            th.printStackTrace();
            t.a().b();
        }
    }

    public bd c() {
        return this.f3539a;
    }

    public void a(Map<String, Object> map) {
        if (this.c) {
            this.b.a(map);
            f();
        }
    }

    public void a() {
        if (this.b != null) {
            this.b.g();
        }
    }

    public void b() {
        if (this.b != null) {
            this.b.d();
        }
    }

    public void e() {
        if (this.b != null) {
            this.b.f();
        }
    }

    public void f() {
        if (this.b != null) {
            this.b.a((CommentTagInfo) null);
        }
    }

    public void a(int i) {
        if (this.b != null) {
            this.b.a(i);
        }
    }

    public g g() {
        if (this.b != null) {
            return this.b.a();
        }
        return null;
    }

    public bc h() {
        if (this.b != null) {
            return this.b.j();
        }
        return null;
    }

    public void d() {
        if (1 == this.b.c()) {
            this.b.b();
        }
    }

    public void a(CommentTagInfo commentTagInfo) {
        if (this.b != null) {
            this.b.b(commentTagInfo);
        }
    }

    public boolean i() {
        if (!j.a().j() || this.b == null || this.b.e()) {
            return false;
        }
        return true;
    }

    public void j() {
        if (this.b != null) {
            this.b.h();
        }
    }

    public void a(long j, long j2, int i, long j3) {
        if (this.b != null) {
            this.b.a(j, j2, i, j3);
        }
    }

    public void a(ViewPageScrollListener viewPageScrollListener) {
        if (this.b != null) {
            this.b.a(viewPageScrollListener);
        }
    }
}
