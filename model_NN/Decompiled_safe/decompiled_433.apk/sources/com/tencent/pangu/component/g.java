package com.tencent.pangu.component;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.protocol.jce.CommentReply;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistant.utils.bm;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.socialcontact.comment.j;
import com.tencent.nucleus.socialcontact.login.l;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
class g implements j {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentDetailView f3669a;

    g(CommentDetailView commentDetailView) {
        this.f3669a = commentDetailView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.component.CommentDetailView.b(com.tencent.pangu.component.CommentDetailView, boolean):boolean
     arg types: [com.tencent.pangu.component.CommentDetailView, int]
     candidates:
      com.tencent.pangu.component.CommentDetailView.b(com.tencent.pangu.component.CommentDetailView, int):int
      com.tencent.pangu.component.CommentDetailView.b(com.tencent.pangu.component.CommentDetailView, boolean):boolean */
    public void a(int i, int i2, boolean z, CommentTagInfo commentTagInfo, List<CommentDetail> list, List<CommentDetail> list2, List<CommentTagInfo> list3, boolean z2, byte[] bArr, CommentDetail commentDetail) {
        if (this.f3669a.q && z) {
            boolean unused = this.f3669a.q = false;
        }
        if (this.f3669a.ae == null) {
            this.f3669a.a(i2, z, commentTagInfo, list, list2, list3, z2, commentDetail);
            return;
        }
        ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(1, null, this.f3669a.af);
        viewInvalidateMessage.arg1 = i2;
        HashMap hashMap = new HashMap();
        hashMap.put("isFirstPage", Boolean.valueOf(z));
        hashMap.put("data", list);
        hashMap.put("selectedData", list2);
        hashMap.put("hasNext", Boolean.valueOf(z2));
        hashMap.put("curVerComment", commentDetail);
        hashMap.put("taglist", list3);
        hashMap.put("reqTagInfo", commentTagInfo);
        viewInvalidateMessage.params = hashMap;
        this.f3669a.ae.sendMessage(viewInvalidateMessage);
    }

    public void a(int i, int i2, CommentDetail commentDetail, long j) {
        if (this.f3669a.ae == null) {
            this.f3669a.a(i2, commentDetail, j);
            return;
        }
        ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(2, null, this.f3669a.af);
        viewInvalidateMessage.arg1 = i2;
        HashMap hashMap = new HashMap();
        hashMap.put("comment", commentDetail);
        hashMap.put("oldCommentId", Long.valueOf(j));
        viewInvalidateMessage.params = hashMap;
        this.f3669a.ae.sendMessage(viewInvalidateMessage);
    }

    public void a(int i, int i2, long j, String str, String str2, long j2) {
        if (l.b(i2) && i2 == 0) {
            CommentReply commentReply = new CommentReply();
            commentReply.d = str2;
            commentReply.c = str;
            commentReply.b = j2;
            commentReply.e = true;
            if (this.f3669a.i != null) {
                for (int i3 = 0; i3 < this.f3669a.i.size(); i3++) {
                    CommentDetail commentDetail = (CommentDetail) this.f3669a.i.get(i3);
                    if (commentDetail.h == j) {
                        m mVar = (m) this.f3669a.h.getChildAt(i3).getTag();
                        commentDetail.k.add(commentReply);
                        return;
                    }
                }
            }
            if (this.f3669a.g != null) {
                for (int i4 = 0; i4 < this.f3669a.g.size(); i4++) {
                    CommentDetail commentDetail2 = (CommentDetail) this.f3669a.g.get(i4);
                    if (commentDetail2.h == j) {
                        m mVar2 = (m) this.f3669a.f.getChildAt(i4).getTag();
                        commentDetail2.k.add(commentReply);
                        return;
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v36, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v3, resolved type: com.tencent.assistant.protocol.jce.CommentDetail} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v59, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v3, resolved type: com.tencent.assistant.protocol.jce.CommentDetail} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00c8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r16, int r17, long r18, java.lang.String r20, int r21, long r22) {
        /*
            r15 = this;
            boolean r2 = com.tencent.nucleus.socialcontact.login.l.b(r17)
            if (r2 != 0) goto L_0x0007
        L_0x0006:
            return
        L_0x0007:
            r10 = 0
            if (r17 != 0) goto L_0x0197
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            android.content.Context r2 = r2.d
            com.tencent.pangu.component.CommentDetailView r3 = r15.f3669a
            android.content.Context r3 = r3.d
            r4 = 2131362051(0x7f0a0103, float:1.8343872E38)
            java.lang.String r3 = r3.getString(r4)
            r4 = 0
            android.widget.Toast r2 = android.widget.Toast.makeText(r2, r3, r4)
            r2.show()
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            java.util.ArrayList r2 = r2.i
            if (r2 == 0) goto L_0x01b8
            r2 = 0
            r11 = r2
        L_0x002f:
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            java.util.ArrayList r2 = r2.i
            int r2 = r2.size()
            if (r11 >= r2) goto L_0x01b8
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            java.util.ArrayList r2 = r2.i
            java.lang.Object r2 = r2.get(r11)
            com.tencent.assistant.protocol.jce.CommentDetail r2 = (com.tencent.assistant.protocol.jce.CommentDetail) r2
            long r2 = r2.h
            int r2 = (r2 > r18 ? 1 : (r2 == r18 ? 0 : -1))
            if (r2 != 0) goto L_0x018d
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            java.util.ArrayList r2 = r2.i
            java.lang.Object r2 = r2.get(r11)
            r10 = r2
            com.tencent.assistant.protocol.jce.CommentDetail r10 = (com.tencent.assistant.protocol.jce.CommentDetail) r10
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            android.widget.LinearLayout r2 = r2.h
            android.view.View r12 = r2.getChildAt(r11)
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            com.tencent.pangu.component.CommentDetailView r3 = r15.f3669a
            android.widget.LinearLayout r3 = r3.h
            android.view.View r3 = r3.getChildAt(r11)
            java.lang.Object r3 = r3.getTag()
            com.tencent.pangu.component.m r3 = (com.tencent.pangu.component.m) r3
            r8 = 0
            r9 = 1
            r4 = r20
            r5 = r21
            r6 = r22
            r2.a(r3, r4, r5, r6, r8, r9)
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            android.widget.LinearLayout r2 = r2.h
            r2.removeViewAt(r11)
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            java.util.ArrayList r2 = r2.i
            r2.remove(r11)
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            java.util.ArrayList r2 = r2.i
            int r2 = r2.size()
            if (r2 != 0) goto L_0x00a6
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            r3 = 8
            r2.b(r3)
        L_0x00a6:
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            r2.p()
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            android.widget.LinearLayout r2 = r2.f
            r3 = 0
            r2.addView(r12, r3)
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            com.tencent.nucleus.socialcontact.comment.CommentFooterView r2 = r2.n
            r3 = 0
            r2.setVisibility(r3)
            r3 = r10
        L_0x00c0:
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            java.util.ArrayList r2 = r2.g
            if (r2 == 0) goto L_0x01b6
            r2 = 0
            r12 = r2
        L_0x00ca:
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            java.util.ArrayList r2 = r2.g
            int r2 = r2.size()
            if (r12 >= r2) goto L_0x01b6
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            android.widget.LinearLayout r2 = r2.f
            r4 = 0
            android.view.View r13 = r2.getChildAt(r4)
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            java.util.ArrayList r2 = r2.g
            r4 = 0
            java.lang.Object r2 = r2.get(r4)
            r10 = r2
            com.tencent.assistant.protocol.jce.CommentDetail r10 = (com.tencent.assistant.protocol.jce.CommentDetail) r10
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            java.util.ArrayList r2 = r2.g
            java.lang.Object r2 = r2.get(r12)
            com.tencent.assistant.protocol.jce.CommentDetail r2 = (com.tencent.assistant.protocol.jce.CommentDetail) r2
            long r4 = r2.h
            int r2 = (r4 > r18 ? 1 : (r4 == r18 ? 0 : -1))
            if (r2 != 0) goto L_0x0192
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            java.util.ArrayList r2 = r2.g
            java.lang.Object r2 = r2.get(r12)
            r11 = r2
            com.tencent.assistant.protocol.jce.CommentDetail r11 = (com.tencent.assistant.protocol.jce.CommentDetail) r11
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            android.widget.LinearLayout r2 = r2.f
            android.view.View r14 = r2.getChildAt(r12)
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            java.lang.Object r3 = r14.getTag()
            com.tencent.pangu.component.m r3 = (com.tencent.pangu.component.m) r3
            com.tencent.pangu.component.CommentDetailView r4 = r15.f3669a
            android.content.Context r4 = r4.d
            r5 = 2131362020(0x7f0a00e4, float:1.8343809E38)
            java.lang.String r8 = r4.getString(r5)
            r9 = 1
            r4 = r20
            r5 = r21
            r6 = r22
            r2.a(r3, r4, r5, r6, r8, r9)
            if (r12 == 0) goto L_0x0160
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            android.widget.LinearLayout r2 = r2.f
            r2.removeViewAt(r12)
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            android.widget.LinearLayout r2 = r2.f
            r3 = 0
            r2.addView(r14, r3)
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            java.lang.Object r3 = r13.getTag()
            com.tencent.pangu.component.m r3 = (com.tencent.pangu.component.m) r3
            java.lang.String r4 = r10.g
            int r5 = r10.b
            long r6 = r10.m
            java.lang.String r8 = r10.c
            r9 = 0
            r2.a(r3, r4, r5, r6, r8, r9)
        L_0x0160:
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            com.tencent.pangu.component.l r2 = r2.p
            if (r2 == 0) goto L_0x0006
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            com.tencent.pangu.component.l r3 = r2.p
            if (r17 != 0) goto L_0x01b4
            r2 = 1
        L_0x0171:
            r0 = r18
            r3.a(r2, r0)
            if (r17 != 0) goto L_0x0006
            if (r11 == 0) goto L_0x0006
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            com.tencent.pangu.component.l r2 = r2.p
            long r3 = r11.h
            int r7 = r11.d
            r5 = r20
            r6 = r21
            r2.a(r3, r5, r6, r7)
            goto L_0x0006
        L_0x018d:
            int r2 = r11 + 1
            r11 = r2
            goto L_0x002f
        L_0x0192:
            int r2 = r12 + 1
            r12 = r2
            goto L_0x00ca
        L_0x0197:
            com.tencent.pangu.component.CommentDetailView r2 = r15.f3669a
            android.content.Context r2 = r2.d
            com.tencent.pangu.component.CommentDetailView r3 = r15.f3669a
            android.content.Context r3 = r3.d
            r4 = 2131362048(0x7f0a0100, float:1.8343866E38)
            java.lang.String r3 = r3.getString(r4)
            r4 = 0
            android.widget.Toast r2 = android.widget.Toast.makeText(r2, r3, r4)
            r2.show()
            r11 = r10
            goto L_0x0160
        L_0x01b4:
            r2 = 0
            goto L_0x0171
        L_0x01b6:
            r11 = r3
            goto L_0x0160
        L_0x01b8:
            r3 = r10
            goto L_0x00c0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.pangu.component.g.a(int, int, long, java.lang.String, int, long):void");
    }

    public void a(int i, int i2, long j, int i3, long j2) {
        long j3;
        if (i2 == -800 && this.f3669a.g != null) {
            int i4 = 0;
            while (true) {
                int i5 = i4;
                if (i5 < this.f3669a.g.size()) {
                    View childAt = this.f3669a.f.getChildAt(i5);
                    if (!(childAt == null || childAt.getTag() == null)) {
                        m mVar = (m) childAt.getTag();
                        CommentDetail commentDetail = (CommentDetail) this.f3669a.g.get(i5);
                        if (!(mVar == null || commentDetail == null || commentDetail.h != j)) {
                            Drawable drawable = this.f3669a.getResources().getDrawable(R.drawable.pinglun_icon_zan);
                            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                            Drawable drawable2 = this.f3669a.getResources().getDrawable(R.drawable.pinglun_icon_yizan);
                            drawable2.setBounds(0, 0, drawable2.getMinimumWidth(), drawable2.getMinimumHeight());
                            long parseInt = (long) Integer.parseInt(mVar.g.getTag(R.id.comment_praise_count) + Constants.STR_EMPTY);
                            if (((Boolean) mVar.g.getTag()).booleanValue()) {
                                mVar.g.setTag(false);
                                mVar.g.setCompoundDrawables(drawable, null, null, null);
                                j3 = parseInt - 1;
                                mVar.g.setText(bm.a(j3) + Constants.STR_EMPTY);
                                mVar.g.setTextColor(Color.parseColor("#6e6e6e"));
                            } else {
                                mVar.g.setTag(true);
                                mVar.g.setCompoundDrawables(drawable2, null, null, null);
                                j3 = 1 + parseInt;
                                mVar.g.setText(bm.a(j3) + Constants.STR_EMPTY);
                                mVar.g.setTextColor(Color.parseColor("#b68a46"));
                            }
                            mVar.g.setTag(R.id.comment_praise_count, Long.valueOf(j3));
                            return;
                        }
                    }
                    i4 = i5 + 1;
                } else {
                    return;
                }
            }
        }
    }
}
