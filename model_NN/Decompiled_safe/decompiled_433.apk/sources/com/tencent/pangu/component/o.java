package com.tencent.pangu.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class o extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f3685a;
    final /* synthetic */ FileDownloadButton b;

    o(FileDownloadButton fileDownloadButton, STInfoV2 sTInfoV2) {
        this.b = fileDownloadButton;
        this.f3685a = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.b.b(this.b.b, this.f3685a);
    }

    public STInfoV2 getStInfo() {
        if (this.f3685a != null) {
            this.f3685a.actionId = a.b(this.b.b.s);
            this.f3685a.status = a.d(this.b.b.s);
        }
        return this.f3685a;
    }
}
