package com.tencent.pangu.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAClickListener;

/* compiled from: ProGuard */
class bl extends OnTMAClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ShareAppBar f3656a;

    bl(ShareAppBar shareAppBar) {
        this.f3656a = shareAppBar;
    }

    public void onTMAClick(View view) {
        switch (view.getId()) {
            case R.id.tv_share_qq /*2131165717*/:
                this.f3656a.b();
                return;
            case R.id.tv_share_qz /*2131165718*/:
                this.f3656a.c();
                return;
            case R.id.tv_share_wx /*2131165719*/:
                this.f3656a.d();
                return;
            case R.id.iv_divide /*2131165720*/:
            case R.id.layout_down /*2131165721*/:
            default:
                return;
            case R.id.tv_share_timeline /*2131165722*/:
                this.f3656a.e();
                return;
        }
    }
}
