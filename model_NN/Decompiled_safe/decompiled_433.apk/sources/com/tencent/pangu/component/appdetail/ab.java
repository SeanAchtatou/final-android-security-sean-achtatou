package com.tencent.pangu.component.appdetail;

import android.view.GestureDetector;
import android.view.MotionEvent;

/* compiled from: ProGuard */
class ab extends GestureDetector.SimpleOnGestureListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppdetailViewPager f3562a;

    ab(AppdetailViewPager appdetailViewPager) {
        this.f3562a = appdetailViewPager;
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return false;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        if (this.f3562a.h) {
            if (this.f3562a.k != null) {
                this.f3562a.k.b(-((int) f));
            }
            boolean unused = this.f3562a.h = false;
        }
        return false;
    }
}
