package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.module.k;
import com.tencent.assistantv2.component.fps.FPSProgressBar;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener;
import com.tencent.pangu.component.appdetail.process.a;

/* compiled from: ProGuard */
public class AppdetailDownloadBar extends RelativeLayout {
    private TextView A;
    private TextView B;
    private boolean C = true;
    /* access modifiers changed from: private */
    public a D;
    /* access modifiers changed from: private */
    public ViewGroup E;
    /* access modifiers changed from: private */
    public LinearLayout F;
    /* access modifiers changed from: private */
    public LinearLayout G;
    /* access modifiers changed from: private */
    public ImageView H;
    /* access modifiers changed from: private */
    public TextView I;
    /* access modifiers changed from: private */
    public ImageView J;
    /* access modifiers changed from: private */
    public TextView K;
    /* access modifiers changed from: private */
    public InnerScrollView L;
    private boolean M = false;
    /* access modifiers changed from: private */
    public boolean N = true;
    private OnTMAParamClickListener O = new m(this);
    private AppdetailActionUIListener P = new p(this);

    /* renamed from: a  reason: collision with root package name */
    ExchangeColorTextView f3530a;
    AppdetailScrollView b;
    String c = "15_001";
    String d = "07_001";
    boolean e = false;
    boolean f;
    int g = -1;
    View h;
    public int i = 0;
    public int j = 0;
    /* access modifiers changed from: private */
    public Context k;
    /* access modifiers changed from: private */
    public FPSProgressBar l;
    private FPSProgressBar m;
    private View n;
    /* access modifiers changed from: private */
    public ImageView o;
    /* access modifiers changed from: private */
    public TextView p;
    /* access modifiers changed from: private */
    public Button q;
    private Button r;
    private Button s;
    /* access modifiers changed from: private */
    public Button t;
    /* access modifiers changed from: private */
    public Button u;
    private long v = 0;
    private View w;
    private View x;
    private View y;
    private TextView z;

    public AppdetailDownloadBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public AppdetailDownloadBar(Context context) {
        super(context);
        a(context);
    }

    public int a() {
        return this.g;
    }

    public void a(int i2) {
        this.g = i2;
    }

    private void a(Context context) {
        this.k = context;
        LayoutInflater.from(context).inflate((int) R.layout.appdetail_download_bar, this);
        this.l = (FPSProgressBar) findViewById(R.id.appdetail_progress_bar);
        this.m = (FPSProgressBar) findViewById(R.id.appdetail_progress_bar_1);
        this.m.setId(R.id.appdetail_progress_bar);
        this.m.setOnClickListener(this.O);
        this.l.setOnClickListener(this.O);
        this.q = (Button) findViewById(R.id.appdetail_progress_btn);
        this.q.setOnClickListener(this.O);
        this.n = findViewById(R.id.appdetail_floating_layout);
        this.o = (ImageView) findViewById(R.id.appdetail_floating_icon);
        this.p = (TextView) findViewById(R.id.appdetail_floating_txt);
        this.w = findViewById(R.id.app_updatesizeinfo);
        this.z = (TextView) findViewById(R.id.app_size_tips);
        this.A = (TextView) findViewById(R.id.app_size_sumsize);
        this.B = (TextView) findViewById(R.id.app_size_slimsize);
        this.E = (ViewGroup) findViewById(R.id.two_btn_auth);
        this.F = (LinearLayout) findViewById(R.id.wx_auth);
        this.G = (LinearLayout) findViewById(R.id.qq_auth);
        this.H = (ImageView) findViewById(R.id.wx_auth_icon);
        this.I = (TextView) findViewById(R.id.wx_auth_txt);
        this.J = (ImageView) findViewById(R.id.qq_auth_icon);
        this.K = (TextView) findViewById(R.id.qq_auth_txt);
        this.F.setOnClickListener(this.O);
        this.G.setOnClickListener(this.O);
        this.r = (Button) findViewById(R.id.btn_pause_download);
        this.s = (Button) findViewById(R.id.btn_delete_download);
        this.r.setOnClickListener(this.O);
        this.s.setOnClickListener(this.O);
        this.f3530a = (ExchangeColorTextView) findViewById(R.id.appdetail_floating_txt_b);
        this.t = (Button) findViewById(R.id.appdetail_progress_btn_for_cmd);
        this.x = findViewById(R.id.appdetail_progress_btn_for_cmd_area);
        this.u = (Button) findViewById(R.id.appdetail_progress_btn_for_appbar);
        this.y = findViewById(R.id.appdetail_progress_btn_for_appbar_area);
    }

    public void a(a aVar) {
        this.D = aVar;
    }

    public void a(boolean z2, AppConst.AppState appState, long j2, String str, int i2, int i3, boolean z3) {
        this.M = true;
        this.e = z2;
        if (this.i == 2 && this.t != null) {
            this.x.setVisibility(0);
            this.t.setOnClickListener(this.O);
        }
        if (this.i == 2 && z2) {
            if (this.D != null) {
                this.D.a(z2, appState, j2, str, i2, i3, z3);
            }
            if (this.t != null && z2) {
                this.t.setText((int) R.string.comment_txt_tips_already_cmd);
                this.t.setTextColor(this.k.getResources().getColor(R.color.comment_over_txt_tips));
                this.t.setBackgroundResource(R.drawable.common_btn_big_disabled);
                this.t.setOnClickListener(null);
            }
            this.q.setVisibility(8);
        }
    }

    public void a(boolean z2) {
        if (this.i == 2 && this.t != null) {
            this.t.setClickable(z2);
            this.t.setEnabled(z2);
        }
    }

    public void b(boolean z2) {
        if (this.t != null && this.e && z2) {
            this.t.setText((int) R.string.comment_txt_tips_already_cmd);
            this.t.setTextColor(this.k.getResources().getColor(R.color.comment_over_txt_tips));
            this.t.setBackgroundResource(R.drawable.common_btn_big_disabled);
            this.t.setOnClickListener(null);
        } else if (this.t != null) {
            this.t.setText((int) R.string.comment_detail_write_text);
        }
    }

    public void a(int i2, int i3) {
        if (this.l != null && this.m != null) {
            this.l.a(FPSProgressBar.SIZE.MIDDLE);
            if (this.C) {
                this.C = false;
                this.l.a(i2);
            } else {
                this.l.b(i2);
            }
            this.l.setSecondaryProgress(i3);
            this.m.setProgress(this.l.getProgress());
            this.m.setSecondaryProgress(i3);
            if (i2 > 0 && i2 < 100) {
                this.f3530a.setTextWhiteLenth(((float) i2) / 100.0f);
            } else if (i2 == 0) {
                this.f3530a.setTextWhiteLenth(((float) i2) / 100.0f);
            }
        }
    }

    public void a(InnerScrollView innerScrollView, AppdetailScrollView appdetailScrollView, View view, boolean z2) {
        this.L = innerScrollView;
        this.b = appdetailScrollView;
        this.h = view;
        this.f = z2;
    }

    /* access modifiers changed from: private */
    public void g() {
        if (this.D != null && this.j != 3) {
            AppConst.AppState d2 = k.d(this.D.a());
            if (this.L == null) {
                return;
            }
            if ((d2 == AppConst.AppState.DOWNLOAD || d2 == AppConst.AppState.UPDATE || d2 == AppConst.AppState.PAUSED) && this.i == 1) {
                int measuredHeight = this.h.getMeasuredHeight() - this.L.getHeight();
                if (this.h.findViewById(R.id.my_tag_area).getVisibility() == 0) {
                    measuredHeight = this.h.findViewById(R.id.my_tag_area).getTop();
                } else if (this.h.findViewById(R.id.recommend_view).getVisibility() == 0) {
                    measuredHeight = this.h.findViewById(R.id.recommend_view).getTop();
                }
                if (measuredHeight < 0) {
                    measuredHeight = 0;
                }
                this.L.b(measuredHeight);
                this.b.a(true);
            }
        }
    }

    public void c(boolean z2) {
        if (this.L != null) {
            this.L.postDelayed(new n(this), 1000);
        }
    }

    public void a(String str, String str2) {
        if (this.w != null) {
            this.w.setVisibility(8);
        }
        if (this.m != null) {
            this.m.setVisibility(8);
        }
        this.n.setVisibility(0);
        this.f3530a.setVisibility(8);
        this.p.setText(Constants.STR_EMPTY);
        if (!TextUtils.isEmpty(str)) {
            this.p.append(new SpannableString(str));
        }
        if (!TextUtils.isEmpty(str2)) {
            SpannableString spannableString = new SpannableString(" (" + str2 + ")");
            spannableString.setSpan(new AbsoluteSizeSpan(16, true), 0, spannableString.length(), 17);
            this.p.append(spannableString);
        }
    }

    public void a(String str, int i2) {
        if (this.w != null) {
            this.w.setVisibility(8);
        }
        if (this.m != null) {
            this.m.setVisibility(8);
        }
        if (str.startsWith(getResources().getString(R.string.update)) && str.endsWith("B")) {
            this.n.setVisibility(0);
            this.f3530a.setVisibility(8);
            this.p.setText(Constants.STR_EMPTY);
            this.p.append(new SpannableString(str.subSequence(0, 2)));
            SpannableString spannableString = new SpannableString(" (" + str.substring(3) + ")");
            spannableString.setSpan(new AbsoluteSizeSpan(16, true), 0, spannableString.length(), 17);
            this.p.append(spannableString);
        } else if (!(TextUtils.isEmpty(str) || this.p == null || this.l == null || this.m == null)) {
            this.p.setText(str);
            int progress = this.l.getProgress();
            if ((progress <= 0 || progress >= 100) && !str.endsWith("%") && !str.equals("继续") && !str.equals("等待中") && !str.equals("暂停")) {
                this.n.setVisibility(0);
                this.f3530a.setVisibility(8);
            } else {
                this.n.setVisibility(8);
                this.q.setVisibility(8);
                this.f3530a.setVisibility(0);
                if (str.equals("继续")) {
                    this.m.setVisibility(0);
                    this.l.setVisibility(8);
                    this.f3530a.setOrangeMode(true);
                    this.f3530a.postDelayed(new o(this, progress), 200);
                } else {
                    this.m.setVisibility(8);
                    this.l.setVisibility(0);
                    this.f3530a.setOrangeMode(false);
                }
                this.f3530a.setText(str);
                this.f3530a.setTextWhiteLenth(((float) progress) / 100.0f);
            }
            if (i2 > 0) {
                this.o.setVisibility(0);
                this.o.setImageResource(i2);
            } else {
                this.o.setVisibility(8);
            }
        }
        if (!TextUtils.isEmpty(str) && str.equals("安装") && this.l != null) {
            this.l.setVisibility(8);
            this.m.setVisibility(8);
            this.f3530a.setVisibility(8);
        }
    }

    public void a(String str, String str2, String str3) {
        if (!TextUtils.isEmpty(str) && this.n != null && !TextUtils.isEmpty(str2)) {
            this.n.setVisibility(8);
            this.f3530a.setVisibility(8);
            this.w.setVisibility(0);
            this.z.setText(str);
            this.A.setText("(" + str2);
            this.B.setText(str3 + ")");
        }
    }

    public void b() {
        if (this.D != null) {
            this.D.f();
        }
    }

    public void c() {
        if (this.D != null) {
            this.D.g();
        }
    }

    public void d() {
        if (this.D != null) {
            this.D.h();
        }
    }

    public void e() {
        if (this.D != null) {
            this.D.i();
        }
    }

    public void b(int i2) {
        if (this.D != null) {
            this.D.a(i2);
            this.i = i2;
        }
        if (i2 == 3) {
            this.y.setVisibility(0);
            this.u.setOnClickListener(this.O);
            return;
        }
        this.y.setVisibility(8);
        if (i2 != 2) {
            this.x.setVisibility(8);
        } else if (this.M) {
            this.x.setVisibility(0);
            if (!this.e) {
                this.t.setOnClickListener(this.O);
            }
        }
    }

    public AppdetailActionUIListener f() {
        return this.P;
    }
}
