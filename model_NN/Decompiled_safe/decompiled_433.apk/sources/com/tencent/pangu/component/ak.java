package com.tencent.pangu.component;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.nucleus.socialcontact.login.j;

/* compiled from: ProGuard */
class ak extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RankFriendsLoginErrorView f3519a;

    ak(RankFriendsLoginErrorView rankFriendsLoginErrorView) {
        this.f3519a = rankFriendsLoginErrorView;
    }

    public void onTMAClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConst.KEY_LOGIN_TYPE, 5);
        bundle.putInt(AppConst.KEY_FROM_TYPE, 13);
        j.a().a(AppConst.IdentityType.MOBILEQ, bundle);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3519a.getContext(), 200);
        buildSTInfo.slotId = a.a("08", "001");
        return buildSTInfo;
    }
}
