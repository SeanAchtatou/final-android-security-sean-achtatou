package com.tencent.pangu.component.treasurebox;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import com.tencent.pangu.model.h;

/* compiled from: ProGuard */
public class AppTreasureBoxCell extends RelativeLayout implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f3730a = false;
    protected int b = 0;
    protected a c;

    public AppTreasureBoxCell(Context context) {
        super(context);
    }

    public AppTreasureBoxCell(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void onClick(View view) {
    }

    public void a() {
    }

    public void b() {
    }

    public void a(a aVar) {
        this.c = aVar;
    }

    public void c() {
        if (this.c != null) {
            this.c.b(this.b);
        }
    }

    public void a(int i) {
        if (this.c != null) {
            this.c.c(this.b);
        }
    }

    public void b(int i) {
        this.b = i;
    }

    public void a(h hVar) {
    }

    public static AppTreasureBoxCell a(boolean z, h hVar, Context context, a aVar, int i) {
        AppTreasureBoxCell appTreasureBoxCell;
        if (z) {
            appTreasureBoxCell = new AppTreasureBoxCell1(context);
            if (hVar == null) {
                return null;
            }
        } else {
            appTreasureBoxCell = new AppTreasureBoxCell2(context);
        }
        appTreasureBoxCell.b(i);
        if (appTreasureBoxCell != null) {
            appTreasureBoxCell.a(hVar);
        }
        if (aVar != null) {
            appTreasureBoxCell.a(aVar);
        }
        appTreasureBoxCell.a(i);
        return appTreasureBoxCell;
    }
}
