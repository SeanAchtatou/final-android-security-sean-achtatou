package com.tencent.pangu.component;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.m;
import com.tencent.assistant.utils.FunctionUtils;
import com.tencent.assistant.utils.at;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.download.a;
import com.tencent.pangu.manager.SelfUpdateManager;

/* compiled from: ProGuard */
public class SelfNormalUpdateView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    public AstApp f3502a;
    public SelfUpdateManager.SelfUpdateInfo b;
    private View c;
    private TextView d;
    private TextView e;
    private TextView f;
    private TXImageView g;
    private Button h;
    private Button i;
    private bk j;

    public SelfNormalUpdateView(Context context) {
        this(context, null);
    }

    public SelfNormalUpdateView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3502a = AstApp.i();
        e();
    }

    private void e() {
        this.c = LayoutInflater.from(getContext()).inflate((int) R.layout.dialog_selfupdate_normal, this);
        this.g = (TXImageView) this.c.findViewById(R.id.self_update_banner);
        this.f = (TextView) this.c.findViewById(R.id.msg_versionfeature);
        this.h = (Button) this.c.findViewById(R.id.btn_ignore);
        this.i = (Button) this.c.findViewById(R.id.btn_update);
        this.d = (TextView) this.c.findViewById(R.id.title_tip_1);
        this.e = (TextView) this.c.findViewById(R.id.title_tip_2);
    }

    public void a() {
        this.b = SelfUpdateManager.a().d();
        if (this.b != null) {
            if (!TextUtils.isEmpty(this.b.h)) {
                this.f.setText(String.format(getResources().getString(R.string.dialog_update_feature), this.b.h));
            }
            if (SelfUpdateManager.a().a(this.b.e)) {
                this.d.setVisibility(0);
                this.d.setText((int) R.string.selfupdate_apk_file_ready);
                this.i.setText((int) R.string.selfupdate_update_right_now);
                this.i.setTextColor(getResources().getColor(R.color.appdetail_btn_text_color_to_install));
                this.i.setBackgroundResource(R.drawable.appdetail_bar_btn_to_install_selector_v5);
            } else {
                this.d.setVisibility(8);
                this.i.setText(Constants.STR_EMPTY);
                this.i.append(new SpannableString(getResources().getString(R.string.download)));
                SpannableString spannableString = new SpannableString(" (" + at.b(this.b.a()) + "B)");
                spannableString.setSpan(new AbsoluteSizeSpan(15, true), 0, spannableString.length(), 17);
                this.i.append(spannableString);
            }
            try {
                this.e.setText(Html.fromHtml(this.b.q));
            } catch (Exception e2) {
            }
            this.g.setImageBitmap(FunctionUtils.a(BitmapFactory.decodeResource(this.f3502a.getResources(), R.drawable.self_update_banner)));
            this.h.setOnClickListener(new bi(this));
            this.i.setOnClickListener(new bj(this));
        }
    }

    public void a(bk bkVar) {
        this.j = bkVar;
    }

    /* access modifiers changed from: private */
    public void f() {
        m.a().b("update_newest_versioncode", Integer.valueOf(this.b.e));
        m.a().b("update_newest_versionname", this.b.f);
        SelfUpdateManager.a().n().a();
        if (this.j != null) {
            this.j.u();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.download.a.a(com.tencent.pangu.download.DownloadInfo, boolean):boolean
     arg types: [com.tencent.pangu.download.DownloadInfo, int]
     candidates:
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.a, java.util.List):java.util.List
      com.tencent.pangu.download.a.a(java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>, boolean):void
      com.tencent.pangu.download.a.a(long, long):boolean
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.a, boolean):boolean
      com.tencent.pangu.download.a.a(java.lang.String, int):boolean
      com.tencent.pangu.download.a.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, com.tencent.assistantv2.st.model.StatInfo):int
      com.tencent.pangu.download.a.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo):void
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.DownloadInfo, com.tencent.pangu.download.SimpleDownloadInfo$UIType):void
      com.tencent.pangu.download.a.a(java.lang.String, com.tencent.cloud.a.f):void
      com.tencent.pangu.download.a.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.download.a.a(java.lang.String, boolean):boolean
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.DownloadInfo, boolean):boolean */
    /* access modifiers changed from: private */
    public void g() {
        SelfUpdateManager.a().n().a();
        if (!SelfUpdateManager.a().a(this.b.e)) {
            h();
        } else if (!a.a().a(SelfUpdateManager.a().b(SelfUpdateManager.SelfUpdateType.NORMAL), false)) {
            h();
        }
        b();
    }

    private void h() {
        SelfUpdateManager.a().a(SelfUpdateManager.SelfUpdateType.NORMAL);
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (this.j != null) {
            this.j.u();
        }
    }

    public void c() {
    }

    public void d() {
    }
}
