package com.tencent.pangu.component.appdetail;

import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class n implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppdetailDownloadBar f3601a;

    n(AppdetailDownloadBar appdetailDownloadBar) {
        this.f3601a = appdetailDownloadBar;
    }

    public void run() {
        int measuredHeight = this.f3601a.h.getMeasuredHeight() - this.f3601a.L.getHeight();
        if (this.f3601a.h.findViewById(R.id.my_tag_area).getVisibility() == 0) {
            measuredHeight = this.f3601a.h.findViewById(R.id.my_tag_area).getTop();
        } else if (this.f3601a.h.findViewById(R.id.recommend_view).getVisibility() == 0) {
            measuredHeight = this.f3601a.h.findViewById(R.id.recommend_view).getTop();
        }
        if (measuredHeight < 0) {
            measuredHeight = 0;
        }
        this.f3601a.L.b(measuredHeight);
        this.f3601a.b.a(true);
    }
}
