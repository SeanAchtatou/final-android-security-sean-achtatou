package com.tencent.pangu.c;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.n;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.ShareRequest;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.thumbnailCache.o;
import com.tencent.assistant.thumbnailCache.p;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.nucleus.socialcontact.login.j;
import com.tencent.nucleus.socialcontact.login.l;
import com.tencent.pangu.model.ShareAppModel;
import com.tencent.pangu.model.ShareBaseModel;
import com.tencent.pangu.model.ShareModel;
import com.tencent.tauth.Tencent;
import java.io.ByteArrayOutputStream;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class e extends BaseEngine<d> implements UIEventListener, p {
    /* access modifiers changed from: private */
    public static List<WeakReference<Dialog>> g = new ArrayList();

    /* renamed from: a  reason: collision with root package name */
    protected Tencent f3474a;
    protected IWXAPI b;
    protected int c = -1;
    protected ShareModel d;
    protected WeakReference<Activity> e;
    protected WXMediaMessage f;
    private String h = null;
    private String i = null;
    private String j = null;
    private String k = null;
    private boolean l = false;
    /* access modifiers changed from: private */
    public boolean m = false;
    /* access modifiers changed from: private */
    public boolean n = true;
    /* access modifiers changed from: private */
    public AppConst.TwoBtnDialogInfo o = new i(this);
    private d p = new n(this);

    public e(Activity activity, int i2) {
        register(this.p);
        this.e = new WeakReference<>(activity);
        TemporaryThreadManager.get().start(new f(this, i2));
    }

    public void a(boolean z) {
        this.n = z;
    }

    public void a(int i2) {
        TemporaryThreadManager.get().start(new g(this, i2));
        b(false);
        e();
    }

    private void e() {
        this.h = null;
        this.i = null;
        this.j = null;
        this.k = null;
    }

    public void b(boolean z) {
        this.l = z;
    }

    private void f() {
        AppConst.LoadingDialogInfo loadingDialogInfo = new AppConst.LoadingDialogInfo();
        loadingDialogInfo.loadingText = Constants.STR_EMPTY;
        loadingDialogInfo.blockCaller = true;
        g.add(new WeakReference(DialogUtils.showLoadingDialog(loadingDialogInfo)));
    }

    private void g() {
        runOnUiThread(new h(this));
    }

    private void h() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_SHARE_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_SHARE_FAIL, this);
    }

    private void i() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_SHARE_SUCCESS, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_SHARE_FAIL, this);
    }

    public void a(Activity activity, ShareAppModel shareAppModel) {
        if (shareAppModel != null) {
            this.d = shareAppModel;
            this.e = new WeakReference<>(activity);
            a(l(), (int) STConstAction.ACTION_HIT_SHARE_QZONE);
            if (!j.a().k() || this.e == null) {
                Bundle bundle = new Bundle();
                bundle.putInt(AppConst.KEY_LOGIN_TYPE, 6);
                bundle.putInt(AppConst.KEY_FROM_TYPE, 1);
                l.a(1);
                j.a().a(AppConst.IdentityType.MOBILEQ, bundle);
                return;
            }
            DialogUtils.showShareQzDialog(this.e.get(), shareAppModel, this.o);
        }
    }

    public void a(Activity activity, ShareAppModel shareAppModel, boolean z) {
        if (shareAppModel != null) {
            this.m = true;
            this.d = shareAppModel;
            this.e = new WeakReference<>(activity);
            a(l(), (int) STConstAction.ACTION_HIT_SHARE_QZONE);
            if (!j.a().k() || this.e == null) {
                Bundle bundle = new Bundle();
                bundle.putInt(AppConst.KEY_LOGIN_TYPE, 6);
                bundle.putInt(AppConst.KEY_FROM_TYPE, 1);
                l.a(1);
                j.a().a(AppConst.IdentityType.MOBILEQ, bundle);
            } else if (z) {
                DialogUtils.showShareQzDialog(this.e.get(), shareAppModel, this.o);
            } else {
                if (shareAppModel.b == null) {
                    shareAppModel.b = Constants.STR_EMPTY;
                }
                a(shareAppModel.e, c(shareAppModel), b(shareAppModel), shareAppModel.b, shareAppModel.f, null);
            }
        }
    }

    public void a(long j2, String str, String str2, String str3, String str4, String str5) {
        ShareRequest shareRequest = new ShareRequest();
        shareRequest.a(j2);
        shareRequest.a(str);
        shareRequest.b(str2);
        shareRequest.c(str3);
        shareRequest.d(str4);
        shareRequest.a(j2 > 0 ? 0 : 1);
        shareRequest.e(str5);
        this.c = send(shareRequest);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareBaseModel, boolean):void
     arg types: [android.app.Activity, com.tencent.pangu.model.ShareBaseModel, int]
     candidates:
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareAppModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareAppModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareBaseModel, boolean):void
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareBaseModel, boolean):void */
    public void a(Activity activity, ShareBaseModel shareBaseModel) {
        a(activity, shareBaseModel, true);
    }

    public void a(Activity activity, ShareBaseModel shareBaseModel, boolean z) {
        if (shareBaseModel != null) {
            this.d = shareBaseModel;
            this.e = new WeakReference<>(activity);
            a(l(), (int) STConstAction.ACTION_HIT_SHARE_QZONE);
            if (!j.a().k() || this.e == null) {
                Bundle bundle = new Bundle();
                bundle.putInt(AppConst.KEY_LOGIN_TYPE, 6);
                bundle.putInt(AppConst.KEY_FROM_TYPE, 1);
                l.a(1);
                j.a().a(AppConst.IdentityType.MOBILEQ, bundle);
            } else if (z) {
                DialogUtils.showShareQzDialog(this.e.get(), shareBaseModel, this.o);
            } else {
                a(0, shareBaseModel.f3880a, shareBaseModel.b, shareBaseModel.e, shareBaseModel.c, shareBaseModel.d);
            }
        }
    }

    public void b(Activity activity, ShareAppModel shareAppModel) {
        if (shareAppModel != null) {
            ShareBaseModel shareBaseModel = new ShareBaseModel();
            shareBaseModel.b = b(shareAppModel);
            shareBaseModel.c = shareAppModel.f;
            shareBaseModel.d = String.format("http://fusion.qq.com/cgi-bin/qzapps/unified_jump?appid=%1$s&from=%2$s", Long.valueOf(shareAppModel.e), "mqq" + a(shareAppModel));
            shareBaseModel.f3880a = c(shareAppModel);
            b(activity, shareBaseModel);
        }
    }

    public void b(Activity activity, ShareBaseModel shareBaseModel) {
        f();
        this.e = new WeakReference<>(activity);
        a(k(), (int) STConstAction.ACTION_HIT_SHARE_QQ);
        if (this.f3474a == null) {
            this.f3474a = Tencent.createInstance(n.e, AstApp.i().getApplicationContext());
        }
        Bundle bundle = new Bundle();
        bundle.putInt("req_type", 1);
        bundle.putString("title", shareBaseModel.f3880a);
        bundle.putString("summary", shareBaseModel.b);
        bundle.putString("targetUrl", shareBaseModel.d);
        bundle.putString("imageUrl", shareBaseModel.c);
        bundle.putInt("cflag", 2);
        if (this.f3474a != null && this.e != null && this.e.get() != null) {
            this.f3474a.shareToQQ(this.e.get(), bundle, null);
        }
    }

    public void a(Context context, ShareBaseModel shareBaseModel, boolean z) {
        a(z ? m() : j(), z ? STConstAction.ACTION_HIT_SHARE_FIRENDS : STConstAction.ACTION_HIT_SHARE_WX_FIREND);
        if (shareBaseModel != null) {
            o.a(z ? 1 : 2);
            f();
            TemporaryThreadManager.get().start(new j(this, context, shareBaseModel));
        }
    }

    public void a(Context context, ShareAppModel shareAppModel, boolean z) {
        if (shareAppModel != null) {
            o.a(z ? 1 : 2);
            ShareBaseModel shareBaseModel = new ShareBaseModel();
            shareBaseModel.b = z ? Constants.STR_EMPTY : b(shareAppModel);
            shareBaseModel.c = shareAppModel.f;
            Object[] objArr = new Object[2];
            objArr[0] = Long.valueOf(shareAppModel.e);
            objArr[1] = z ? "wx&isTimeline=true" : "wx&isTimeline=false";
            shareBaseModel.d = String.format("http://fusion.qq.com/cgi-bin/qzapps/unified_jump?appid=%1$s&from=%2$s", objArr);
            shareBaseModel.d += a(shareAppModel);
            shareBaseModel.f3880a = z ? c(shareAppModel) + b(shareAppModel) : c(shareAppModel);
            a(context, shareBaseModel, z);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareAppModel, boolean):void
     arg types: [android.content.Context, com.tencent.pangu.model.ShareAppModel, int]
     candidates:
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareAppModel, boolean):void
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareBaseModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareBaseModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareAppModel, boolean):void */
    public void a(Context context, ShareAppModel shareAppModel, boolean z, boolean z2) {
        this.m = z2;
        a(context, shareAppModel, true);
    }

    private String a(String str) {
        return str == null ? String.valueOf(System.currentTimeMillis()) : str + System.currentTimeMillis();
    }

    public byte[] a(Bitmap bitmap, boolean z) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        try {
            byteArrayOutputStream.close();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (z) {
            bitmap.recycle();
        }
        return byteArray;
    }

    private String a(ShareAppModel shareAppModel) {
        return "&actionFlag=" + (shareAppModel == null ? 0 : shareAppModel.d) + "&params=" + URLEncoder.encode(a(shareAppModel == null ? null : shareAppModel.c));
    }

    private String a(Bundle bundle) {
        StringBuilder sb = new StringBuilder();
        if (bundle == null || bundle.isEmpty()) {
            return sb.toString();
        }
        for (String next : bundle.keySet()) {
            sb.append("&" + next + "=" + bundle.getString(next));
        }
        String sb2 = sb.toString();
        if (sb2.startsWith("&")) {
            return sb2.substring(1);
        }
        return sb2;
    }

    /* access modifiers changed from: private */
    public String b(ShareAppModel shareAppModel) {
        if (shareAppModel == null) {
            return Constants.STR_EMPTY;
        }
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(shareAppModel.m)) {
            return shareAppModel.m;
        }
        if (!TextUtils.isEmpty(shareAppModel.f3879a)) {
            sb.append(shareAppModel.f3879a);
            sb.append("|");
        }
        if (shareAppModel.j != 0) {
            sb.append(at.a(shareAppModel.j));
            sb.append("|");
        }
        if (shareAppModel.i != 0) {
            sb.append(bm.a(shareAppModel.i, 0));
            sb.append("|");
        }
        if (shareAppModel.h != 0.0d) {
            sb.append(a((int) R.string.share_rate, String.valueOf(shareAppModel.h).substring(0, 3)));
        }
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public String c(ShareAppModel shareAppModel) {
        switch (shareAppModel.d) {
            case 1:
                return AstApp.i().getApplicationContext().getString(R.string.share_box_title_n, shareAppModel.g);
            case 2:
                return AstApp.i().getApplicationContext().getString(R.string.share_box_title_q, shareAppModel.g);
            default:
                return AstApp.i().getApplicationContext().getString(R.string.share_box_title, shareAppModel.g);
        }
    }

    private String a(int i2, Object... objArr) {
        return AstApp.i().getApplicationContext().getString(i2, objArr);
    }

    public void handleUIEvent(Message message) {
        int i2;
        int i3;
        int e2;
        int i4 = 2000;
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
                Bundle bundle = (Bundle) message.obj;
                if (bundle == null || !bundle.containsKey(AppConst.KEY_FROM_TYPE)) {
                    e2 = l.e();
                    l.a(0);
                } else {
                    e2 = bundle.getInt(AppConst.KEY_FROM_TYPE);
                    l.a(0);
                }
                if (e2 == 1) {
                    runOnUiThread(new k(this));
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_FAIL:
            default:
                return;
            case EventDispatcherEnum.UI_EVENT_SHARE_SUCCESS:
                if (this.e == null || this.e.get() == null || !(this.e.get() instanceof BaseActivity)) {
                    i2 = 2000;
                } else {
                    i2 = o.b();
                    i4 = ((BaseActivity) this.e.get()).m();
                }
                if (message.obj == null || !(message.obj instanceof Integer)) {
                    i3 = 0;
                } else {
                    i3 = ((Integer) message.obj).intValue();
                }
                if (4 == i3) {
                    XLog.i("xjp", "*** share to timeline succeed ***");
                    if (this.n) {
                        Toast.makeText(AstApp.i().getApplicationContext(), AstApp.i().getApplicationContext().getString(R.string.share_success), 0).show();
                    }
                    a(i2, "03_004", i4, STConstAction.ACTION_HIT_SHARE_SUCCESS_FIRENDS, o.h());
                    return;
                } else if (3 == i3) {
                    XLog.i("xjp", "*** share to WX succeed ***");
                    a(i2, "03_003", i4, STConstAction.ACTION_HIT_SHARE_SUCCESS_WX_FIREND, o.h());
                    return;
                } else if (2 == i3) {
                    XLog.i("xjp", "*** share QZ wx succeed ***");
                    a(i2, l(), i4, STConstAction.ACTION_HIT_SHARE_SUCCESS_QZONE, o.h());
                    return;
                } else {
                    return;
                }
            case EventDispatcherEnum.UI_EVENT_SHARE_FAIL:
                if (4 == ((Integer) message.obj).intValue()) {
                    XLog.i("xjp", "*** share timeline failed ***");
                    this.m = false;
                    return;
                }
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str, int i2) {
        if (!this.l) {
            int b2 = o.b();
            int i3 = 2000;
            if (AstApp.m() != null) {
                i3 = AstApp.m().m();
            }
            a(b2, str, i3, i2, o.h());
        }
    }

    private void a(int i2, String str, int i3, int i4, String str2) {
        STInfoV2 sTInfoV2 = new STInfoV2(i2, str, i3, STConst.ST_DEFAULT_SLOT, i4);
        if (sTInfoV2 != null) {
            sTInfoV2.extraData = str2;
            XLog.i("ShareEngine", "[logReportV2] --> pageId = " + i2 + ", slotId = " + str + ", prePageId = " + i3 + ", actionId = " + i4 + ", extraData = " + str2);
            com.tencent.assistantv2.st.l.a(sTInfoV2);
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new l(this, i2, jceStruct, jceStruct2));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new m(this, i2, i3, jceStruct, jceStruct2));
    }

    public void a() {
        g();
        XLog.i("ShareEngine", "onPause");
    }

    public void b() {
        XLog.i("ShareEngine", "onResume");
        h();
        g();
    }

    public void c() {
        i();
        unregister(this.p);
        g();
        this.d = null;
        this.b = null;
        this.f3474a = null;
        this.e = null;
        this.f = null;
    }

    public void thumbnailRequestCompleted(o oVar) {
        a(this.f, oVar == null ? null : oVar.f);
    }

    public void thumbnailRequestFailed(o oVar) {
        a(this.f, oVar == null ? null : oVar.f);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.c.e.a(android.graphics.Bitmap, boolean):byte[]
     arg types: [android.graphics.Bitmap, int]
     candidates:
      com.tencent.pangu.c.e.a(android.graphics.Bitmap, int):android.graphics.Bitmap
      com.tencent.pangu.c.e.a(int, java.lang.Object[]):java.lang.String
      com.tencent.pangu.c.e.a(com.tencent.pangu.c.e, com.tencent.pangu.model.ShareAppModel):java.lang.String
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareAppModel):void
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareBaseModel):void
      com.tencent.pangu.c.e.a(com.tencent.mm.sdk.modelmsg.WXMediaMessage, android.graphics.Bitmap):void
      com.tencent.pangu.c.e.a(java.lang.String, int):void
      com.tencent.pangu.c.e.a(android.graphics.Bitmap, boolean):byte[] */
    /* access modifiers changed from: protected */
    public void a(WXMediaMessage wXMediaMessage, Bitmap bitmap) {
        Bitmap decodeResource;
        if (bitmap == null) {
            bitmap = BitmapFactory.decodeResource(AstApp.i().getResources(), R.drawable.pic_default_app);
        }
        Log.i("ShareEngine", "share");
        try {
            decodeResource = a(bitmap, 75);
        } catch (Exception e2) {
            decodeResource = BitmapFactory.decodeResource(AstApp.i().getResources(), R.drawable.pic_default_app);
        }
        wXMediaMessage.thumbData = a(decodeResource, true);
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = a("webpage");
        req.message = wXMediaMessage;
        req.scene = o.a() == 1 ? 1 : 0;
        this.b.sendReq(req);
    }

    public static Bitmap a(Bitmap bitmap, int i2) {
        Bitmap decodeResource;
        int i3;
        if (bitmap != null) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            if (width >= i2 || height < i2) {
            }
            if (width > height) {
                i3 = (width * i2) / height;
            } else {
                int i4 = (height * i2) / width;
                i3 = i2;
                i2 = i4;
            }
            decodeResource = Bitmap.createScaledBitmap(bitmap, i3, i2, true);
        } else {
            decodeResource = BitmapFactory.decodeResource(AstApp.i().getResources(), R.drawable.pic_default_app);
        }
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        Bitmap createBitmap = Bitmap.createBitmap(decodeResource.getWidth(), decodeResource.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawColor(-1250068);
        canvas.drawBitmap(decodeResource, new Matrix(), paint);
        return createBitmap;
    }

    private String j() {
        return TextUtils.isEmpty(this.j) ? "03_003" : this.j;
    }

    private String k() {
        return TextUtils.isEmpty(this.h) ? "03_001" : this.h;
    }

    private String l() {
        return TextUtils.isEmpty(this.i) ? "03_002" : this.i;
    }

    private String m() {
        return TextUtils.isEmpty(this.k) ? "03_004" : this.k;
    }

    public void a(String[] strArr) {
        if (strArr.length < 4) {
            throw new IllegalArgumentException("arguments number must be 4");
        }
        this.h = strArr[0];
        this.i = strArr[1];
        this.j = strArr[2];
        this.k = strArr[3];
    }
}
