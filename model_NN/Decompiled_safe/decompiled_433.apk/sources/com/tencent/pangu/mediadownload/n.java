package com.tencent.pangu.mediadownload;

import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.downloadsdk.DownloadManager;
import com.tencent.pangu.model.AbstractDownloadInfo;
import com.tencent.pangu.model.d;

/* compiled from: ProGuard */
class n implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f3865a;

    n(e eVar) {
        this.f3865a = eVar;
    }

    public void run() {
        for (d next : this.f3865a.a()) {
            DownloadManager.a().a(100, next.m);
            if (next.s == AbstractDownloadInfo.DownState.DOWNLOADING || next.s == AbstractDownloadInfo.DownState.QUEUING) {
                next.s = AbstractDownloadInfo.DownState.FAIL;
                this.f3865a.c.sendMessage(this.f3865a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FAIL, next));
                this.f3865a.b.a(next);
            }
        }
    }
}
