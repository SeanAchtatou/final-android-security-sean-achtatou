package com.tencent.pangu.adapter;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.BrowserActivity;
import com.tencent.assistant.model.SimpleAppModel;

/* compiled from: ProGuard */
class ap implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f3418a;
    final /* synthetic */ al b;

    ap(al alVar, SimpleAppModel simpleAppModel) {
        this.b = alVar;
        this.f3418a = simpleAppModel;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.b.e, BrowserActivity.class);
        intent.putExtra("com.tencent.assistant.BROWSER_URL", this.f3418a.Z);
        this.b.e.startActivity(intent);
    }
}
