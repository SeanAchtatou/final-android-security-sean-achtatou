package com.tencent.pangu.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.AppIconView;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.b;
import com.tencent.assistant.model.d;
import com.tencent.assistant.module.k;
import com.tencent.assistant.net.c;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.e;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.component.fps.FPSRankNormalItem;
import com.tencent.assistantv2.manager.a;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.download.DownloadInfo;
import java.util.List;

/* compiled from: ProGuard */
public class RankNormalListAdapter extends BaseAdapter implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    protected static int f3404a = 0;
    protected static int b = (f3404a + 1);
    protected static int c = (b + 1);
    protected ListType d = ListType.LISTTYPENORMAL;
    protected AstApp e = AstApp.i();
    protected Context f;
    protected LayoutInflater g;
    protected au h;
    protected View i;
    protected int j = 2000;
    b k = null;
    protected Bitmap l = null;
    protected Bitmap m = null;
    protected Bitmap n = null;
    protected Bitmap o = null;
    protected String p = "07";
    public com.tencent.assistantv2.st.b.b q = new com.tencent.assistantv2.st.b.b();
    private boolean r = false;
    private long s = -100;
    private boolean t = false;
    private int u = 0;
    private IViewInvalidater v;
    private String w = this.p;
    private String x = Constants.STR_EMPTY;

    /* compiled from: ProGuard */
    public enum ListType {
        LISTTYPENORMAL,
        LISTTYPEGAMESORT
    }

    public RankNormalListAdapter(Context context, View view, b bVar) {
        this.f = context;
        this.h = new au(a.a().b().c());
        if (!(bVar == null || bVar.c() == null)) {
            this.h.a(bVar.c());
            notifyDataSetChanged();
        }
        this.g = LayoutInflater.from(context);
        this.i = view;
        this.k = bVar;
    }

    public void a(boolean z, List<SimpleAppModel> list) {
        if (list != null) {
            if (z) {
                this.h.b();
            }
            this.h.a(list);
            notifyDataSetChanged();
        }
    }

    public void a() {
        this.h.b();
        notifyDataSetChanged();
    }

    public void a(int i2, long j2) {
        this.j = i2;
        this.s = j2;
    }

    public void a(ListType listType) {
        this.d = listType;
    }

    public void a(boolean z) {
        this.t = z;
    }

    public void b(int i2) {
        this.u = i2;
    }

    public void a(IViewInvalidater iViewInvalidater) {
        this.v = iViewInvalidater;
    }

    public void b() {
        this.e.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        this.e.k().removeUIEventListener(1016, this);
        this.e.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY, this);
    }

    public void c() {
        this.e.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        this.e.k().addUIEventListener(1016, this);
        this.e.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY, this);
        d();
    }

    public void d() {
        if (this.h.a(a.a().b().c())) {
            notifyDataSetChanged();
        }
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        SimpleAppModel simpleAppModel;
        if (this.h == null || i2 >= this.h.a()) {
            simpleAppModel = null;
        } else {
            simpleAppModel = this.h.a(i2);
        }
        d e2 = k.e(simpleAppModel);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f, simpleAppModel, com.tencent.assistantv2.st.page.a.a(this.w, i2), 100, null, e2);
        if (buildSTInfo != null) {
            buildSTInfo.setCategoryId(this.s);
            buildSTInfo.contentId = this.x;
        }
        if (!(buildSTInfo == null || this.j != buildSTInfo.scene || this.q == null)) {
            this.q.exposure(buildSTInfo);
        }
        if (f3404a == getItemViewType(i2)) {
            return a(view, simpleAppModel, i2, buildSTInfo, e2);
        }
        if (b == getItemViewType(i2)) {
            return b(view, simpleAppModel, i2, buildSTInfo, e2);
        }
        return view;
    }

    public int getItemViewType(int i2) {
        if (!e()) {
            return f3404a;
        }
        SimpleAppModel.CARD_TYPE card_type = this.h.a(i2).U;
        if (SimpleAppModel.CARD_TYPE.NORMAL == card_type) {
            return f3404a;
        }
        if (SimpleAppModel.CARD_TYPE.QUALITY == card_type) {
            return b;
        }
        return f3404a;
    }

    public int getViewTypeCount() {
        return c;
    }

    public int getCount() {
        if (this.h == null) {
            return 0;
        }
        return this.h.a();
    }

    public Object getItem(int i2) {
        if (this.h == null) {
            return null;
        }
        return this.h.a(i2);
    }

    public void a(String str) {
        this.w = str;
    }

    public void b(String str) {
        this.x = str;
    }

    /* access modifiers changed from: protected */
    public View a(View view, SimpleAppModel simpleAppModel, int i2, STInfoV2 sTInfoV2, d dVar) {
        bb bbVar;
        if (view == null || view.getTag() == null || ((ba) view.getTag()).f3430a == null) {
            ba baVar = new ba(this, null);
            bb bbVar2 = new bb(this, null);
            bbVar2.f3431a = new FPSRankNormalItem(this.f);
            view = bbVar2.f3431a;
            baVar.f3430a = bbVar2;
            view.setTag(baVar);
            bbVar = bbVar2;
        } else {
            bbVar = ((ba) view.getTag()).f3430a;
        }
        bbVar.f3431a.a(this.r, this.t, getCount(), this.v, this.d, this.u);
        bbVar.f3431a.a(simpleAppModel, i2, sTInfoV2, dVar, this.i);
        return view;
    }

    /* access modifiers changed from: protected */
    public View b(View view, SimpleAppModel simpleAppModel, int i2, STInfoV2 sTInfoV2, d dVar) {
        az azVar;
        if (view == null || view.getTag() == null || ((ba) view.getTag()).b == null) {
            ba baVar = new ba(this, null);
            azVar = new az(this, null);
            view = this.g.inflate((int) R.layout.competitive_card_v5, (ViewGroup) null);
            azVar.f3428a = (TextView) view.findViewById(R.id.title);
            azVar.b = (TXImageView) view.findViewById(R.id.pic);
            azVar.d = (ImageView) view.findViewById(R.id.vedio);
            azVar.e = (AppIconView) view.findViewById(R.id.icon);
            azVar.f = (DownloadButton) view.findViewById(R.id.download_soft_btn);
            azVar.g = (ListItemInfoView) view.findViewById(R.id.download_info);
            azVar.g.a(ListItemInfoView.InfoType.values()[this.u]);
            azVar.h = (TextView) view.findViewById(R.id.name);
            azVar.c = (ImageView) view.findViewById(R.id.sort_num_image);
            azVar.i = (TextView) view.findViewById(R.id.description);
            baVar.b = azVar;
            view.setTag(baVar);
        } else {
            azVar = ((ba) view.getTag()).b;
        }
        view.setOnClickListener(new av(this, simpleAppModel, sTInfoV2));
        a(azVar, simpleAppModel, i2, sTInfoV2, dVar);
        return view;
    }

    private void a(az azVar, SimpleAppModel simpleAppModel, int i2, STInfoV2 sTInfoV2, d dVar) {
        if (simpleAppModel != null && azVar != null) {
            if (!TextUtils.isEmpty(simpleAppModel.Y)) {
                azVar.b.updateImageView(simpleAppModel.Y, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            } else {
                azVar.b.setImageResource(R.drawable.pic_defaule);
            }
            if (simpleAppModel.Z == null || simpleAppModel.Z.length() == 0) {
                azVar.d.setVisibility(8);
                azVar.b.setDuplicateParentStateEnabled(true);
                azVar.b.setClickable(false);
            } else {
                azVar.d.setVisibility(0);
                azVar.b.setDuplicateParentStateEnabled(false);
                azVar.b.setClickable(true);
                azVar.b.setOnClickListener(new aw(this, simpleAppModel));
            }
            azVar.e.setSimpleAppModel(simpleAppModel, com.tencent.assistantv2.st.page.a.a(sTInfoV2), this.s);
            azVar.f.a(simpleAppModel, dVar);
            azVar.g.a(simpleAppModel, dVar);
            azVar.h.setText(simpleAppModel.d);
            if (TextUtils.isEmpty(simpleAppModel.X)) {
                azVar.i.setVisibility(8);
            } else {
                azVar.i.setVisibility(0);
                azVar.i.setText(simpleAppModel.X);
            }
            if (a(simpleAppModel, dVar.b)) {
                azVar.f.setClickable(false);
            } else {
                azVar.f.setClickable(true);
                azVar.f.a(sTInfoV2, new ax(this), dVar);
            }
            azVar.c.setVisibility(8);
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
                DownloadInfo downloadInfo = null;
                if (message.obj instanceof DownloadInfo) {
                    downloadInfo = (DownloadInfo) message.obj;
                }
                if (downloadInfo != null && !TextUtils.isEmpty(downloadInfo.downloadTicket) && this.h.a(downloadInfo.downloadTicket)) {
                    f();
                    return;
                }
                return;
            case 1016:
                this.h.c();
                f();
                return;
            case EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY:
                f();
                return;
            default:
                return;
        }
    }

    private void f() {
        ah.a().post(new ay(this));
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        if ((!c.e() || c.l()) && m.a().p()) {
            return false;
        }
        return true;
    }

    private boolean a(SimpleAppModel simpleAppModel, DownloadInfo downloadInfo) {
        return b(simpleAppModel, downloadInfo) || c(simpleAppModel, downloadInfo);
    }

    private boolean b(SimpleAppModel simpleAppModel, DownloadInfo downloadInfo) {
        if (simpleAppModel == null) {
            return false;
        }
        if ((downloadInfo == null || downloadInfo.versionCode != simpleAppModel.g) && simpleAppModel.d() && (simpleAppModel.P & 3) > 0 && !e.a(simpleAppModel.c, simpleAppModel.g, simpleAppModel.ad)) {
            return true;
        }
        return false;
    }

    private boolean c(SimpleAppModel simpleAppModel, DownloadInfo downloadInfo) {
        if (simpleAppModel == null) {
            return false;
        }
        if ((downloadInfo == null || downloadInfo.versionCode != simpleAppModel.g) && simpleAppModel.e() && (simpleAppModel.P & 3) > 0 && !e.a(simpleAppModel.c, simpleAppModel.g, simpleAppModel.ad)) {
            return true;
        }
        return false;
    }
}
