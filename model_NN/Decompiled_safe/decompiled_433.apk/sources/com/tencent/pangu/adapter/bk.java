package com.tencent.pangu.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import com.tencent.pangu.component.search.k;
import java.util.List;

/* compiled from: ProGuard */
public class bk extends PagerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private List<k> f3440a = null;

    public bk(List<k> list) {
        this.f3440a = list;
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        if (this.f3440a != null && this.f3440a.get(i) != null) {
            viewGroup.removeView(this.f3440a.get(i).a());
        }
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        if (this.f3440a == null || this.f3440a.get(i) == null) {
            return null;
        }
        viewGroup.addView(this.f3440a.get(i).a());
        return this.f3440a.get(i).a();
    }

    public int getCount() {
        if (this.f3440a == null) {
            return 0;
        }
        return this.f3440a.size();
    }

    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }
}
