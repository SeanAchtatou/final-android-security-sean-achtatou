package com.tencent.smtt.export.external;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;
import com.tencent.smtt.export.external.interfaces.IX5WebChromeClient;
import com.tencent.smtt.export.external.interfaces.IX5WebViewBase;
import com.tencent.smtt.export.external.interfaces.IX5WebViewClient;
import com.tencent.smtt.export.external.proxy.ProxyWebChromeClient;
import com.tencent.smtt.export.external.proxy.ProxyWebViewClient;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class WebViewWizardBase {
    private DexLoader mDexLoader = null;
    protected boolean mIsDynamicMode = false;
    protected boolean mX5Used = true;

    public boolean isDynamicMode() {
        return this.mIsDynamicMode;
    }

    public void setWizardMode(boolean x5Used, boolean isDynamicMode) {
        this.mX5Used = x5Used;
        this.mIsDynamicMode = isDynamicMode;
    }

    public void setDexLoader(Context context, String[] dexPaths, String dexOutputDir) {
        if (this.mX5Used && this.mIsDynamicMode && this.mDexLoader == null) {
            this.mDexLoader = new DexLoader(context, dexPaths, dexOutputDir);
        }
    }

    public void setDexLoader(Context context, String dexPath, String dexOutputDir) {
        if (this.mX5Used && this.mIsDynamicMode && this.mDexLoader == null) {
            this.mDexLoader = new DexLoader(context, dexPath, dexOutputDir);
        }
    }

    public IX5WebViewBase createSDKWebview(Context context) {
        if (!this.mX5Used) {
            return null;
        }
        Class<?>[] args = {Context.class, Boolean.TYPE};
        return (IX5WebViewBase) newInstance(this.mIsDynamicMode, "com.tencent.smtt.webkit.adapter.X5WebViewAdapter", args, context, false);
    }

    public IX5WebViewClient createWebViewClient(ProxyWebViewClient proxy) {
        if (this.mX5Used) {
            proxy.setWebViewClient((IX5WebViewClient) newInstance(this.mIsDynamicMode, "com.tencent.smtt.webkit.WebViewClient"));
        }
        return proxy;
    }

    public IX5WebChromeClient createWebChromeClient(ProxyWebChromeClient proxy) {
        if (this.mX5Used) {
            proxy.setWebChromeClient((IX5WebChromeClient) newInstance(this.mIsDynamicMode, "com.tencent.smtt.webkit.WebChromeClient"));
        }
        return proxy;
    }

    public boolean setContextHolderParams(Context appContext, String dynamicLibFolderPath) {
        Class<?>[] parameterTypes;
        Object[] args;
        if (!this.mX5Used) {
            return false;
        }
        if (!this.mIsDynamicMode || TextUtils.isEmpty(dynamicLibFolderPath)) {
            parameterTypes = new Class[]{Context.class};
            args = new Object[]{appContext};
        } else {
            parameterTypes = new Class[]{Context.class, String.class};
            args = new Object[]{appContext, dynamicLibFolderPath};
        }
        Object contextHolder = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.ContextHolder", "getInstance", null, new Object[0]);
        if (contextHolder != null) {
            return ((Boolean) invokeMethod(this.mIsDynamicMode, contextHolder, "com.tencent.smtt.webkit.ContextHolder", "setContext", parameterTypes, args)).booleanValue();
        }
        return false;
    }

    public void setContextHolderDevelopMode(boolean flag) {
        Object contextHolder;
        if (this.mX5Used && (contextHolder = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.ContextHolder", "getInstance", null, new Object[0])) != null) {
            invokeMethod(this.mIsDynamicMode, contextHolder, "com.tencent.smtt.webkit.ContextHolder", "setSdkDevelopMode", new Class[]{Boolean.TYPE}, Boolean.valueOf(flag));
        }
    }

    public void initCookieSyncManager(Context appContext) {
        if (this.mX5Used) {
            invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CookieSyncManager", "createInstance", new Class[]{Context.class}, appContext);
        }
    }

    public void cookieSyncManager_Sync() {
        Object target;
        if (this.mX5Used && (target = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CookieSyncManager", "getInstance", null, new Object[0])) != null) {
            invokeMethod(this.mIsDynamicMode, target, "com.tencent.smtt.webkit.CookieSyncManager", "sync", null, new Object[0]);
        }
    }

    public void cookieSyncManager_stopSync() {
        Object target;
        if (this.mX5Used && (target = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CookieSyncManager", "getInstance", null, new Object[0])) != null) {
            invokeMethod(this.mIsDynamicMode, target, "com.tencent.smtt.webkit.CookieSyncManager", "stopSync", null, new Object[0]);
        }
    }

    public void cookieSyncManager_startSync() {
        Object target;
        if (this.mX5Used && (target = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CookieSyncManager", "getInstance", null, new Object[0])) != null) {
            invokeMethod(this.mIsDynamicMode, target, "com.tencent.smtt.webkit.CookieSyncManager", "startSync", null, new Object[0]);
        }
    }

    public void cookieManager_removeSessionCookie() {
        Object target;
        if (this.mX5Used && (target = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CookieManager", "getInstance", null, new Object[0])) != null) {
            invokeMethod(this.mIsDynamicMode, target, "com.tencent.smtt.webkit.CookieManager", "removeSessionCookie", null, new Object[0]);
        }
    }

    public void cookieManager_removeAllCookie() {
        Object target;
        if (this.mX5Used && (target = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CookieManager", "getInstance", null, new Object[0])) != null) {
            invokeMethod(this.mIsDynamicMode, target, "com.tencent.smtt.webkit.CookieManager", "removeAllCookie", null, new Object[0]);
        }
    }

    public void cookieManager_removeExpiredCookie() {
        Object target;
        if (this.mX5Used && (target = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CookieManager", "getInstance", null, new Object[0])) != null) {
            invokeMethod(this.mIsDynamicMode, target, "com.tencent.smtt.webkit.CookieManager", "removeExpiredCookie", null, new Object[0]);
        }
    }

    public void cookieManager_setAcceptCookie(boolean accept) {
        Object target;
        if (this.mX5Used && (target = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CookieManager", "getInstance", null, new Object[0])) != null) {
            invokeMethod(this.mIsDynamicMode, target, "com.tencent.smtt.webkit.CookieManager", "setAcceptCookie", new Class[]{Boolean.TYPE}, Boolean.valueOf(accept));
        }
    }

    public boolean cookieManager_acceptCookie() {
        Object ret;
        if (!this.mX5Used) {
            return false;
        }
        Object target = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CookieManager", "getInstance", null, new Object[0]);
        if (target == null || (ret = invokeMethod(this.mIsDynamicMode, target, "com.tencent.smtt.webkit.CookieManager", "acceptCookie", null, new Object[0])) == null) {
            return false;
        }
        return ((Boolean) ret).booleanValue();
    }

    public boolean cookieManager_hasCookies() {
        Object ret;
        if (!this.mX5Used) {
            return false;
        }
        Object target = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CookieManager", "getInstance", null, new Object[0]);
        if (target == null || (ret = invokeMethod(this.mIsDynamicMode, target, "com.tencent.smtt.webkit.CookieManager", "hasCookies", null, new Object[0])) == null) {
            return false;
        }
        return ((Boolean) ret).booleanValue();
    }

    public void cookieManager_setCookie(String url, String value) {
        Object target;
        if (this.mX5Used && (target = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CookieManager", "getInstance", null, new Object[0])) != null) {
            invokeMethod(this.mIsDynamicMode, target, "com.tencent.smtt.webkit.CookieManager", "setCookie", new Class[]{String.class, String.class}, url, value);
        }
    }

    public void initCookieModule(Context appContext) {
        if (this.mX5Used) {
            invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CookieSyncManager", "createInstance", new Class[]{Context.class}, appContext);
            Object target = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CookieManager", "getInstance", new Class[]{Boolean.TYPE}, false);
            if (target != null) {
                Boolean enablePreInitCookieStore = (Boolean) invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.export.internal.utils.ChromiumUtil", "isChromiumBuiltIn", null, new Object[0]);
                if (!enablePreInitCookieStore.booleanValue() || !enablePreInitCookieStore.booleanValue()) {
                    invokeMethod(this.mIsDynamicMode, target, "com.tencent.smtt.webkit.CookieManager", "removeExpiredCookie", null, new Object[0]);
                    invokeMethod(this.mIsDynamicMode, target, "com.tencent.smtt.webkit.CookieManager", "removeSessionCookie", null, new Object[0]);
                    return;
                }
                invokeMethod(this.mIsDynamicMode, target, "com.tencent.smtt.webkit.ChromiumCookieManager", "preInitCookieStore", null, new Object[0]);
            }
        }
    }

    public void initChromiumCookieModule(Context appContext) {
        if (this.mX5Used) {
            invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CookieSyncManager", "createInstance", new Class[]{Context.class}, appContext);
            Object target = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.ChromiumCookieManager", "getChromiumInstance", null, new Object[0]);
            if (target != null) {
                invokeMethod(this.mIsDynamicMode, target, "com.tencent.smtt.webkit.ChromiumCookieManager", "removeExpiredCookie", null, new Object[0]);
                invokeMethod(this.mIsDynamicMode, target, "com.tencent.smtt.webkit.ChromiumCookieManager", "removeSessionCookie", null, new Object[0]);
                invokeMethod(this.mIsDynamicMode, target, "com.tencent.smtt.webkit.ChromiumCookieManager", "preInitCookieStore", null, new Object[0]);
            }
        }
    }

    public void setLocalSmttService(Object localSmttService) {
        if (this.mX5Used) {
            try {
                Class smttServiceClass = Class.forName("com.tencent.smtt.service.SmttService");
                Object target = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.service.SmttServiceProxy", "getInstance", null, new Object[0]);
                if (target != null) {
                    invokeMethod(this.mIsDynamicMode, target, "com.tencent.smtt.webkit.service.SmttServiceProxy", "setLocalSmttService", new Class[]{smttServiceClass}, localSmttService);
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void preConnect(boolean isSetQProxy, String url) {
        if (this.mX5Used) {
            Boolean isUseChromium = (Boolean) invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.export.internal.utils.ChromiumUtil", "getIsUseChromium", null, new Object[0]);
            if (!((Boolean) invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.export.internal.utils.ChromiumUtil", "isChromiumBuiltIn", null, new Object[0])).booleanValue() || !isUseChromium.booleanValue()) {
                invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.net.http.NetworkInterfaces", "preConnect", new Class[]{Boolean.TYPE, String.class}, Boolean.valueOf(isSetQProxy), url);
            } else if (url != null) {
                invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.export.internal.utils.JniUtil", "setPreConnect", new Class[]{String.class, Integer.TYPE}, url, 1);
            }
        }
    }

    public void traceBegin(int category) {
        if (this.mX5Used) {
            invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.util.MttTraceEvent", "begin", new Class[]{Integer.TYPE}, Integer.valueOf(category));
        }
    }

    public void traceBegin(int category, String name) {
        if (this.mX5Used) {
            invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.util.MttTraceEvent", "begin", new Class[]{Integer.TYPE, String.class}, Integer.valueOf(category), name);
        }
    }

    public void traceBegin(int category, String name, String arg) {
        if (this.mX5Used) {
            invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.util.MttTraceEvent", "begin", new Class[]{Integer.TYPE, String.class, String.class}, Integer.valueOf(category), name, arg);
        }
    }

    public void traceEnd(int category) {
        if (this.mX5Used) {
            invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.util.MttTraceEvent", "end", new Class[]{Integer.TYPE}, Integer.valueOf(category));
        }
    }

    public void traceEnd(int category, String name) {
        if (this.mX5Used) {
            invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.util.MttTraceEvent", "end", new Class[]{Integer.TYPE, String.class}, Integer.valueOf(category), name);
        }
    }

    public void traceEnd(int category, String name, String arg) {
        if (this.mX5Used) {
            invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.util.MttTraceEvent", "end", new Class[]{Integer.TYPE, String.class, String.class}, Integer.valueOf(category), name, arg);
        }
    }

    public void liveLog(String msg) {
        if (this.mX5Used) {
            invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.util.MttLog", "liveLog", new Class[]{String.class}, msg);
        }
    }

    public Object getCrashExtraMessage() {
        if (this.mX5Used) {
            return invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.util.CrashTracker", "getCrashExtraInfo", null, new Object[0]);
        }
        Log.e("pb", "WebViewWizard.getCrashExtraMessage(), but mX5Used is false");
        return new String();
    }

    public void stopActiveH5VideoProxy() {
        Object h5video;
        if (this.mX5Used && (h5video = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.h5video.H5VideoHolder", "getInstance", null, new Object[0])) != null) {
            invokeMethod(this.mIsDynamicMode, h5video, "com.tencent.smtt.webkit.h5video.H5VideoHolder", "stopActiveH5VideoProxy", null, new Object[0]);
        }
    }

    public void resumeActiveH5VideoProxy() {
        Object h5video;
        if (this.mX5Used && (h5video = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.h5video.H5VideoHolder", "getInstance", null, new Object[0])) != null) {
            invokeMethod(this.mIsDynamicMode, h5video, "com.tencent.smtt.webkit.h5video.H5VideoHolder", "resumeActiveH5VideoProxy", null, new Object[0]);
        }
    }

    public void setGUID(String guid) {
        if (this.mX5Used) {
            Object jniSmttService = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.net.http.JNISmttService", "getInstance", null, new Object[0]);
            Class<?>[] parameterTypes = {String.class};
            Object[] args = {guid};
            if (jniSmttService != null) {
                invokeMethod(this.mIsDynamicMode, jniSmttService, "com.tencent.smtt.net.http.JNISmttService", "setGUID", parameterTypes, args);
            }
        }
    }

    public Object base64Encode(byte[] data) {
        if (!this.mX5Used) {
            return null;
        }
        return invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.AlgorithmIdentifier.Base64", "encode", new Class[]{byte[].class}, data);
    }

    public Object base64Decode(String imgStr, int flag) {
        if (!this.mX5Used) {
            return null;
        }
        return invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.AlgorithmIdentifier.Base64", "decode", new Class[]{String.class, Integer.TYPE}, imgStr, Integer.valueOf(flag));
    }

    public Object getInputStream(String imageUrl) {
        if (!this.mX5Used) {
            return null;
        }
        Object target = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CacheManager", "getCacheFile", new Class[]{String.class, Map.class}, imageUrl, null);
        if (target != null) {
            return invokeMethod(this.mIsDynamicMode, target, "com.tencent.smtt.webkit.CacheManager$CacheResult", "getInputStream", null, new Object[0]);
        }
        return null;
    }

    public Object getCacheFile(String url, Map<String, String> header) {
        if (!this.mX5Used) {
            return null;
        }
        return invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CacheManager", "getCacheFile", new Class[]{String.class, Map.class}, url, header);
    }

    public Object getCachFileBaseDir() {
        if (!this.mX5Used) {
            return null;
        }
        return invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CacheManager", "getCacheFileBaseDir", new Class[0], new Object[0]);
    }

    public Object cacheDisabled() {
        if (!this.mX5Used) {
            return null;
        }
        return invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CacheManager", "cacheDisabled", new Class[0], new Object[0]);
    }

    public Object getLocalPath(String imageUrl) {
        if (!this.mX5Used) {
            return null;
        }
        Object target = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CacheManager", "getCacheFile", new Class[]{String.class, Map.class}, imageUrl, null);
        if (target != null) {
            return invokeMethod(this.mIsDynamicMode, target, "com.tencent.smtt.webkit.CacheManager$CacheResult", "getLocalPath", null, new Object[0]);
        }
        return null;
    }

    public void refreshPlugins(Context context, boolean arg1) {
        Object target;
        if (this.mX5Used && (target = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.PluginManager", "getInstance", new Class[]{Context.class}, context)) != null) {
            invokeMethod(this.mIsDynamicMode, target, "com.tencent.smtt.webkit.PluginManager", "refreshPlugins", new Class[]{Boolean.TYPE}, Boolean.valueOf(arg1));
        }
    }

    public String getPluginDownloadURL(Context context, String arg1, String arg2, String arg3) {
        if (!this.mX5Used) {
            return null;
        }
        Object target = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.PluginManager", "getInstance", new Class[]{Context.class}, context);
        if (target == null) {
            return null;
        }
        return (String) invokeMethod(this.mIsDynamicMode, target, "com.tencent.smtt.webkit.PluginManager", "getPluginDownloadURL", new Class[]{String.class, String.class, String.class}, arg1, arg2, arg3);
    }

    public void setNetworkOnLine(boolean bool) {
        if (this.mX5Used) {
            invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.JWebCoreJavaBridge", "setNetworkOnLine", new Class[]{Boolean.TYPE}, Boolean.valueOf(bool));
        }
    }

    public void clearCache() {
        if (this.mX5Used) {
            invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CacheManager", "removeAllCacheFiles", null, new Object[0]);
            invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CacheManager", "clearLocalStorage", null, new Object[0]);
        }
    }

    public void HTML5NotificationPresenter_exitCleanUp() {
        if (this.mX5Used) {
            invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.HTML5NotificationPresenter", "exitCleanUp", null, new Object[0]);
        }
    }

    public void clearCookie(Context context) {
        if (this.mX5Used) {
            invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CookieSyncManager", "createInstance", new Class[]{Context.class}, context);
            Object cookieMgr = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CookieManager", "getInstance", new Class[]{Boolean.TYPE}, false);
            if (cookieMgr != null) {
                invokeMethod(this.mIsDynamicMode, cookieMgr, "com.tencent.smtt.webkit.CookieManager", "removeAllCookie", null, new Object[0]);
                invokeMethod(this.mIsDynamicMode, cookieMgr, "com.tencent.smtt.webkit.CookieManager", "removeExpiredCookie", null, new Object[0]);
            }
            Object result2 = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.WebViewDatabase", "getInstance", new Class[]{Context.class}, context);
            if (result2 != null) {
                invokeMethod(this.mIsDynamicMode, result2, "com.tencent.smtt.webkit.WebViewDatabase", "clearFormData", null, new Object[0]);
            }
        }
    }

    public void clearDns() {
        if (this.mX5Used) {
            if (((Boolean) invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.export.internal.utils.ChromiumUtil", "getIsUseChromium", null, new Object[0])).booleanValue()) {
                invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CacheManager", "clearDns", null, new Object[0]);
                return;
            }
            Object dnsMgr = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.net.http.DnsManager", "getInstance", null, new Object[0]);
            if (dnsMgr != null) {
                invokeMethod(this.mIsDynamicMode, dnsMgr, "com.tencent.smtt.net.http.DnsManager", "removeAllDns", null, new Object[0]);
            }
        }
    }

    public void clearChromiumCookie(Context context) {
        if (this.mX5Used) {
            invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.CookieSyncManager", "createInstance", new Class[]{Context.class}, context);
            Object result1 = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.ChromiumCookieManager", "getChromiumInstance", null, new Object[0]);
            if (result1 != null) {
                invokeMethod(this.mIsDynamicMode, result1, "com.tencent.smtt.webkit.ChromiumCookieManager", "removeAllCookie", null, new Object[0]);
                invokeMethod(this.mIsDynamicMode, result1, "com.tencent.smtt.webkit.ChromiumCookieManager", "removeExpiredCookie", null, new Object[0]);
            }
            Object result2 = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.WebViewDatabase", "getInstance", new Class[]{Context.class}, context);
            if (result2 != null) {
                invokeMethod(this.mIsDynamicMode, result2, "com.tencent.smtt.webkit.WebViewDatabase", "clearFormData", null, new Object[0]);
            }
        }
    }

    public void clearFormData(Context context) {
        Object result2;
        if (this.mX5Used && (result2 = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.WebViewDatabase", "getInstance", new Class[]{Context.class}, context)) != null) {
            invokeMethod(this.mIsDynamicMode, result2, "com.tencent.smtt.webkit.WebViewDatabase", "clearFormData", null, new Object[0]);
        }
    }

    public void clearPasswords(Context context) {
        Object result1;
        if (this.mX5Used && (result1 = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.WebViewDatabase", "getInstance", new Class[]{Context.class}, context)) != null) {
            invokeMethod(this.mIsDynamicMode, result1, "com.tencent.smtt.webkit.WebViewDatabase", "clearUsernamePassword", null, new Object[0]);
            invokeMethod(this.mIsDynamicMode, result1, "com.tencent.smtt.webkit.WebViewDatabase", "clearHttpAuthUsernamePassword", null, new Object[0]);
        }
    }

    public void setX5ShowMemValueEnabled(boolean v) {
        if (this.mX5Used) {
            setStaticBooleanField("com.tencent.smtt.webkit.WebSettings", "isX5ShowMemValueEnabled", v);
        }
    }

    public boolean isX5ShowMemValueEnabled() {
        if (!this.mX5Used) {
            return false;
        }
        return ((Boolean) getStaticField(this.mIsDynamicMode, "com.tencent.smtt.webkit.WebSettings", "isX5ShowMemValueEnabled")).booleanValue();
    }

    public void setAllowQHead(boolean v) {
        if (this.mX5Used) {
            setStaticBooleanField("com.tencent.smtt.webkit.WebSettings", "isAllowQHead", v);
        }
    }

    public boolean isAllowQHead() {
        if (!this.mX5Used) {
            return false;
        }
        return ((Boolean) getStaticField(this.mIsDynamicMode, "com.tencent.smtt.webkit.WebSettings", "isAllowQHead")).booleanValue();
    }

    public void syncImmediately() {
        Object obj;
        if (this.mX5Used && (obj = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.SMTTCookieManager", "getInstance", null, new Object[0])) != null) {
            invokeMethod(this.mIsDynamicMode, obj, "com.tencent.smtt.webkit.SMTTCookieManager", "syncImmediately", null, new Object[0]);
        }
    }

    public void appendDomain(URL url) {
        Object obj;
        if (this.mX5Used && (obj = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.SMTTCookieManager", "getInstance", null, new Object[0])) != null) {
            invokeMethod(this.mIsDynamicMode, obj, "com.tencent.smtt.webkit.SMTTCookieManager", "appendDomain", new Class[]{URL.class}, url);
        }
    }

    public void setCookie(URL url, Map<String, List<String>> responseHeaders) {
        Object obj;
        if (this.mX5Used && (obj = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.SMTTCookieManager", "getInstance", null, new Object[0])) != null) {
            invokeMethod(this.mIsDynamicMode, obj, "com.tencent.smtt.webkit.SMTTCookieManager", "setCookie", new Class[]{URL.class, Map.class}, url, responseHeaders);
        }
    }

    public void setQCookie(String url, String value) {
        Object obj;
        if (this.mX5Used && (obj = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.SMTTCookieManager", "getInstance", null, new Object[0])) != null) {
            invokeMethod(this.mIsDynamicMode, obj, "com.tencent.smtt.webkit.SMTTCookieManager", "setQCookie", new Class[]{String.class, String.class}, url, value);
        }
    }

    public String getCookie(String url) {
        Object obj;
        if (this.mX5Used && (obj = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.SMTTCookieManager", "getInstance", null, new Object[0])) != null) {
            return (String) invokeMethod(this.mIsDynamicMode, obj, "com.tencent.smtt.webkit.SMTTCookieManager", "getCookie", new Class[]{String.class}, url);
        }
        return null;
    }

    public String getCookie(String url, boolean privateBrowsering) {
        if (!this.mX5Used) {
            return null;
        }
        Object obj = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.SMTTCookieManager", "getInstance", new Class[]{Boolean.TYPE}, Boolean.valueOf(privateBrowsering));
        if (obj == null) {
            return null;
        }
        return (String) invokeMethod(this.mIsDynamicMode, obj, "com.tencent.smtt.webkit.SMTTCookieManager", "getCookie", new Class[]{String.class}, url);
    }

    public String getQCookie(String url) {
        Object obj;
        if (this.mX5Used && (obj = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.SMTTCookieManager", "getInstance", null, new Object[0])) != null) {
            return (String) invokeMethod(this.mIsDynamicMode, obj, "com.tencent.smtt.webkit.SMTTCookieManager", "getQCookie", new Class[]{String.class}, url);
        }
        return null;
    }

    public void openIconDB(String path) {
        Object obj;
        if (this.mX5Used && (obj = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.WebIconDatabase", "getInstance", null, new Object[0])) != null) {
            invokeMethod(this.mIsDynamicMode, obj, "com.tencent.smtt.webkit.WebIconDatabase", "open", new Class[]{String.class}, path);
        }
    }

    public void closeIconDB() {
        Object obj;
        if (this.mX5Used && (obj = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.WebIconDatabase", "getInstance", null, new Object[0])) != null) {
            invokeMethod(this.mIsDynamicMode, obj, "com.tencent.smtt.webkit.WebIconDatabase", "close", null, new Object[0]);
        }
    }

    public Bitmap getIconForPageUrl(String url) {
        Object obj;
        if (this.mX5Used && (obj = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.WebIconDatabase", "getInstance", null, new Object[0])) != null) {
            return (Bitmap) invokeMethod(this.mIsDynamicMode, obj, "com.tencent.smtt.webkit.WebIconDatabase", "getIconForPageUrl", new Class[]{String.class}, url);
        }
        return null;
    }

    public void ScaleManager_destroy() {
        Object obj;
        if (this.mX5Used && (obj = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.ScaleManager", "getInstance", null, new Object[0])) != null) {
            invokeMethod(this.mIsDynamicMode, obj, "com.tencent.smtt.webkit.ScaleManager", "destroy", null, new Object[0]);
        }
    }

    public void SmttPermanentPermissions_clearAllPermanentPermission() {
        Object obj;
        if (this.mX5Used && (obj = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.SmttPermanentPermissions", "getInstance", null, new Object[0])) != null) {
            invokeMethod(this.mIsDynamicMode, obj, "com.tencent.smtt.webkit.SmttPermanentPermissions", "clearAllPermanentPermission", null, new Object[0]);
        }
    }

    public boolean isUploadingWebCoreLog2Server() {
        if (!this.mX5Used) {
            return true;
        }
        return ((Boolean) invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.util.MttLog", "isUploadingLog", null, new Object[0])).booleanValue();
    }

    public void uploadWebCoreLog2Server() {
        if (this.mX5Used) {
            invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.util.MttLog", "uploadLogFilesToServer", null, new Object[0]);
        }
    }

    public void setWebCoreLogWrite2FileFlag(boolean b) {
        if (this.mX5Used) {
            invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.util.MttLog", "setLogWrite2FileFlag", new Class[]{Boolean.TYPE}, Boolean.valueOf(b));
        }
    }

    public boolean isWritingWebCoreLogToFile() {
        if (!this.mX5Used) {
            return true;
        }
        return ((Boolean) invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.util.MttLog", "isLogWritten2File", null, new Object[0])).booleanValue();
    }

    public void SmttResource_UpdateContext(Context context) {
        if (this.mX5Used) {
            invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.SmttResource", "updateContext", new Class[]{Context.class}, context);
        }
    }

    public Object newInstance(boolean isDynamicMode, String className) {
        if (!isDynamicMode) {
            try {
                Class c = Class.forName(className);
                if (c != null) {
                    return c.newInstance();
                }
                return null;
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), "create '" + className + "' instance failed", e);
                return null;
            }
        } else if (this.mDexLoader != null) {
            return this.mDexLoader.newInstance(className);
        } else {
            return null;
        }
    }

    public Object newInstance(boolean isDynamicMode, String className, Class<?>[] parameterTypes, Object... args) {
        if (!isDynamicMode) {
            try {
                Class c = Class.forName(className);
                if (c != null) {
                    return c.getConstructor(parameterTypes).newInstance(args);
                }
                return null;
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), "create '" + className + "' instance failed", e);
                return null;
            }
        } else if (this.mDexLoader != null) {
            return this.mDexLoader.newInstance(className, parameterTypes, args);
        } else {
            return null;
        }
    }

    public Class<?> loadClass(boolean isDynamicMode, String className) {
        if (!isDynamicMode) {
            try {
                return Class.forName(className);
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), "loadClass '" + className + "' failed", e);
                return null;
            }
        } else if (this.mDexLoader != null) {
            return this.mDexLoader.loadClass(className);
        } else {
            return null;
        }
    }

    public Object invokeStaticMethod(boolean isDynamicMode, String className, String methodName, Class<?>[] parameterTypes, Object... args) {
        if (!isDynamicMode) {
            try {
                Method method = Class.forName(className).getMethod(methodName, parameterTypes);
                method.setAccessible(true);
                return method.invoke(null, args);
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), "'" + className + "' invoke static method '" + methodName + "' failed", e);
                return null;
            }
        } else if (this.mDexLoader != null) {
            return this.mDexLoader.invokeStaticMethod(className, methodName, parameterTypes, args);
        } else {
            return null;
        }
    }

    public Object invokeMethod(boolean isDynamicMode, Object target, String className, String methodName, Class<?>[] parameterTypes, Object... args) {
        if (!isDynamicMode) {
            try {
                Method method = Class.forName(className).getMethod(methodName, parameterTypes);
                method.setAccessible(true);
                return method.invoke(target, args);
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), "'" + className + "' invoke method '" + methodName + "' failed", e);
                return null;
            }
        } else if (this.mDexLoader != null) {
            return this.mDexLoader.invokeMethod(target, className, methodName, parameterTypes, args);
        } else {
            return null;
        }
    }

    public Object getStaticField(boolean isDynamicMode, String className, String fieldName) {
        if (!isDynamicMode) {
            try {
                Field field = Class.forName(className).getField(fieldName);
                field.setAccessible(true);
                return field.get(null);
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), "'" + className + "' get field '" + fieldName + "' failed", e);
                return null;
            }
        } else if (this.mDexLoader != null) {
            return this.mDexLoader.getStaticField(className, fieldName);
        } else {
            return null;
        }
    }

    public void setStaticIntField(String className, String fieldName, int value) {
        try {
            Field field = loadClass(true, className).getField(fieldName);
            field.setAccessible(true);
            field.setInt(null, value);
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "'" + className + "' set field '" + fieldName + "' failed", e);
        }
    }

    public void setStaticBooleanField(String className, String fieldName, boolean value) {
        try {
            Field field = loadClass(true, className).getField(fieldName);
            field.setAccessible(true);
            field.setBoolean(null, value);
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "'" + className + "' set field '" + fieldName + "' failed", e);
        }
    }

    public void setSdkVersion(int version) {
        Object contextHolder;
        if (this.mX5Used && (contextHolder = invokeStaticMethod(this.mIsDynamicMode, "com.tencent.smtt.webkit.ContextHolder", "getInstance", null, new Object[0])) != null) {
            invokeMethod(this.mIsDynamicMode, contextHolder, "com.tencent.smtt.webkit.ContextHolder", "setSdkVersion", new Class[]{Integer.TYPE}, Integer.valueOf(version));
        }
    }
}
