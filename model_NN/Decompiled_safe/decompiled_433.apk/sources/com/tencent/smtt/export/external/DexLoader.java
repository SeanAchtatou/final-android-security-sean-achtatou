package com.tencent.smtt.export.external;

import android.content.Context;
import android.util.Log;
import dalvik.system.DexClassLoader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class DexLoader {
    private DexClassLoader mClassLoader;

    public DexLoader(Context context, String[] dexPaths, String dexOutputDir) {
        ClassLoader loader = context.getClassLoader();
        int i = 0;
        while (i < dexPaths.length) {
            DexClassLoader dexClassLoader = new DexClassLoader(dexPaths[i], dexOutputDir, null, loader);
            this.mClassLoader = dexClassLoader;
            i++;
            loader = dexClassLoader;
        }
    }

    public DexLoader(Context context, String dexPath, String dexOutputDir) {
        this(context, new String[]{dexPath}, dexOutputDir);
    }

    public Object newInstance(String className) {
        try {
            return this.mClassLoader.loadClass(className).newInstance();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "create " + className + " instance failed", e);
            return null;
        }
    }

    public Object newInstance(String className, Class<?>[] parameterTypes, Object... args) {
        try {
            return this.mClassLoader.loadClass(className).getConstructor(parameterTypes).newInstance(args);
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "create '" + className + "' instance failed", e);
            return null;
        }
    }

    public Class<?> loadClass(String className) {
        try {
            return this.mClassLoader.loadClass(className);
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "loadClass '" + className + "' failed", e);
            return null;
        }
    }

    public Object invokeStaticMethod(String className, String methodName, Class<?>[] parameterTypes, Object... args) {
        try {
            Method method = this.mClassLoader.loadClass(className).getMethod(methodName, parameterTypes);
            method.setAccessible(true);
            return method.invoke(null, args);
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "'" + className + "' invoke static method '" + methodName + "' failed", e);
            return null;
        }
    }

    public Object invokeMethod(Object target, String className, String methodName, Class<?>[] parameterTypes, Object... args) {
        try {
            Method method = this.mClassLoader.loadClass(className).getMethod(methodName, parameterTypes);
            method.setAccessible(true);
            return method.invoke(target, args);
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "'" + className + "' invoke method '" + methodName + "' failed", e);
            return null;
        }
    }

    public Object getStaticField(String className, String fieldName) {
        try {
            Field field = this.mClassLoader.loadClass(className).getField(fieldName);
            field.setAccessible(true);
            return field.get(null);
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "'" + className + "' get field '" + fieldName + "' failed", e);
            return null;
        }
    }
}
