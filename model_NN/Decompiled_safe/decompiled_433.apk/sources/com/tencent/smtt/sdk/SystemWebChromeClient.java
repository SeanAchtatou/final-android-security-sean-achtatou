package com.tencent.smtt.sdk;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Message;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.GeolocationPermissions;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebStorage;
import android.webkit.WebView;
import com.tencent.smtt.export.external.interfaces.ConsoleMessage;
import com.tencent.smtt.export.external.interfaces.GeolocationPermissionsCallback;
import com.tencent.smtt.export.external.interfaces.IX5WebChromeClient;
import com.tencent.smtt.export.external.interfaces.QuotaUpdater;
import com.tencent.smtt.sdk.WebView;

class SystemWebChromeClient extends WebChromeClient {
    private WebChromeClient mChromeClient;
    private WebView mWebView;

    SystemWebChromeClient(WebView webView, WebChromeClient chromeClient) {
        this.mWebView = webView;
        this.mChromeClient = chromeClient;
    }

    @TargetApi(7)
    public Bitmap getDefaultVideoPoster() {
        return this.mChromeClient.getDefaultVideoPoster();
    }

    @TargetApi(7)
    public View getVideoLoadingProgressView() {
        return this.mChromeClient.getVideoLoadingProgressView();
    }

    public void getVisitedHistory(ValueCallback<String[]> callback) {
        this.mChromeClient.getVisitedHistory(callback);
    }

    public void onCloseWindow(WebView window) {
        this.mWebView.setSysWebView(window);
        this.mChromeClient.onCloseWindow(this.mWebView);
    }

    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        return this.mChromeClient.onConsoleMessage(new ConsoleMessageImpl(consoleMessage));
    }

    public void onConsoleMessage(String message, int lineNumber, String sourceID) {
        this.mChromeClient.onConsoleMessage(new ConsoleMessageImpl(message, sourceID, lineNumber));
    }

    public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, final Message resultMsg) {
        WebView webView = this.mWebView;
        webView.getClass();
        final WebView.WebViewTransport transport = new WebView.WebViewTransport();
        Message wrapper = Message.obtain(resultMsg.getTarget(), new Runnable() {
            public void run() {
                WebView newWebView = transport.getWebView();
                if (newWebView != null) {
                    ((WebView.WebViewTransport) resultMsg.obj).setWebView(newWebView.getSysWebView());
                }
                resultMsg.sendToTarget();
            }
        });
        wrapper.obj = transport;
        return this.mChromeClient.onCreateWindow(this.mWebView, isDialog, isUserGesture, wrapper);
    }

    @TargetApi(5)
    @Deprecated
    public void onExceededDatabaseQuota(String url, String databaseIdentifier, long quota, long estimatedDatabaseSize, long totalQuota, WebStorage.QuotaUpdater quotaUpdater) {
        this.mChromeClient.onExceededDatabaseQuota(url, databaseIdentifier, quota, estimatedDatabaseSize, totalQuota, new QuotaUpdaterImpl(quotaUpdater));
    }

    @TargetApi(5)
    public void onGeolocationPermissionsHidePrompt() {
        this.mChromeClient.onGeolocationPermissionsHidePrompt();
    }

    @TargetApi(5)
    public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
        this.mChromeClient.onGeolocationPermissionsShowPrompt(origin, new GeolocationPermissionsCallbackImpl(callback));
    }

    @TargetApi(7)
    public void onHideCustomView() {
        this.mChromeClient.onHideCustomView();
    }

    public boolean onJsAlert(android.webkit.WebView view, String url, String message, JsResult result) {
        this.mWebView.setSysWebView(view);
        return this.mChromeClient.onJsAlert(this.mWebView, url, message, new JsResultImpl(result));
    }

    public boolean onJsBeforeUnload(android.webkit.WebView view, String url, String message, JsResult result) {
        this.mWebView.setSysWebView(view);
        return this.mChromeClient.onJsBeforeUnload(this.mWebView, url, message, new JsResultImpl(result));
    }

    public boolean onJsConfirm(android.webkit.WebView view, String url, String message, JsResult result) {
        this.mWebView.setSysWebView(view);
        return this.mChromeClient.onJsConfirm(this.mWebView, url, message, new JsResultImpl(result));
    }

    public boolean onJsPrompt(android.webkit.WebView view, String url, String message, String defaultValue, JsPromptResult result) {
        this.mWebView.setSysWebView(view);
        return this.mChromeClient.onJsPrompt(this.mWebView, url, message, defaultValue, new JsPromptResultImpl(result));
    }

    @TargetApi(7)
    public boolean onJsTimeout() {
        return this.mChromeClient.onJsTimeout();
    }

    public void onProgressChanged(android.webkit.WebView view, int newProgress) {
        this.mWebView.setSysWebView(view);
        this.mChromeClient.onProgressChanged(this.mWebView, newProgress);
    }

    @TargetApi(7)
    @Deprecated
    public void onReachedMaxAppCacheSize(long requiredStorage, long quota, WebStorage.QuotaUpdater quotaUpdater) {
        this.mChromeClient.onReachedMaxAppCacheSize(requiredStorage, quota, new QuotaUpdaterImpl(quotaUpdater));
    }

    public void onReceivedIcon(android.webkit.WebView view, Bitmap icon) {
        this.mWebView.setSysWebView(view);
        this.mChromeClient.onReceivedIcon(this.mWebView, icon);
    }

    public void onReceivedTitle(android.webkit.WebView view, String title) {
        this.mWebView.setSysWebView(view);
        this.mChromeClient.onReceivedTitle(this.mWebView, title);
    }

    @TargetApi(7)
    public void onReceivedTouchIconUrl(android.webkit.WebView view, String url, boolean precomposed) {
        this.mWebView.setSysWebView(view);
        this.mChromeClient.onReceivedTouchIconUrl(this.mWebView, url, precomposed);
    }

    public void onRequestFocus(android.webkit.WebView view) {
        this.mWebView.setSysWebView(view);
        this.mChromeClient.onRequestFocus(this.mWebView);
    }

    @TargetApi(7)
    public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback) {
        this.mChromeClient.onShowCustomView(view, new CustomViewCallbackImpl(callback));
    }

    @TargetApi(14)
    @Deprecated
    public void onShowCustomView(View view, int requestedOrientation, WebChromeClient.CustomViewCallback callback) {
        this.mChromeClient.onShowCustomView(view, requestedOrientation, new CustomViewCallbackImpl(callback));
    }

    public void openFileChooser(ValueCallback<Uri> uploadFile) {
        openFileChooser(uploadFile, null, null);
    }

    public void openFileChooser(ValueCallback<Uri> uploadFile, String acceptType) {
        openFileChooser(uploadFile, acceptType, null);
    }

    public void openFileChooser(ValueCallback<Uri> uploadFile, String acceptType, String capture) {
        this.mChromeClient.openFileChooser(uploadFile, acceptType, capture);
    }

    public void setupAutoFill(Message msg) {
    }

    private class JsResultImpl implements com.tencent.smtt.export.external.interfaces.JsResult {
        JsResult mJsResult;

        JsResultImpl(JsResult jsResult) {
            this.mJsResult = jsResult;
        }

        public void cancel() {
            this.mJsResult.cancel();
        }

        public void confirm() {
            this.mJsResult.confirm();
        }
    }

    private class JsPromptResultImpl implements com.tencent.smtt.export.external.interfaces.JsPromptResult {
        JsPromptResult mJsPromptResult;

        JsPromptResultImpl(JsPromptResult jsPromptResult) {
            this.mJsPromptResult = jsPromptResult;
        }

        public void cancel() {
            this.mJsPromptResult.cancel();
        }

        public void confirm() {
            this.mJsPromptResult.confirm();
        }

        public void confirm(String result) {
            this.mJsPromptResult.confirm(result);
        }
    }

    private static class ConsoleMessageImpl implements com.tencent.smtt.export.external.interfaces.ConsoleMessage {
        private int mLineNumber;
        private String mMessage;
        private ConsoleMessage.MessageLevel mMessageLevel;
        private String mSourceId;

        ConsoleMessageImpl(android.webkit.ConsoleMessage cm) {
            this.mMessageLevel = ConsoleMessage.MessageLevel.valueOf(cm.messageLevel().name());
            this.mMessage = cm.message();
            this.mSourceId = cm.sourceId();
            this.mLineNumber = cm.lineNumber();
        }

        ConsoleMessageImpl(String message, String sourceId, int lineNumber) {
            this.mMessageLevel = ConsoleMessage.MessageLevel.LOG;
            this.mMessage = message;
            this.mSourceId = sourceId;
            this.mLineNumber = lineNumber;
        }

        public ConsoleMessage.MessageLevel messageLevel() {
            return this.mMessageLevel;
        }

        public String message() {
            return this.mMessage;
        }

        public String sourceId() {
            return this.mSourceId;
        }

        public int lineNumber() {
            return this.mLineNumber;
        }
    }

    class QuotaUpdaterImpl implements QuotaUpdater {
        WebStorage.QuotaUpdater mQuotaUpdater;

        QuotaUpdaterImpl(WebStorage.QuotaUpdater quotaUpdater) {
            this.mQuotaUpdater = quotaUpdater;
        }

        public void updateQuota(long newQuota) {
            this.mQuotaUpdater.updateQuota(newQuota);
        }
    }

    class CustomViewCallbackImpl implements IX5WebChromeClient.CustomViewCallback {
        WebChromeClient.CustomViewCallback mCustomViewCallback;

        CustomViewCallbackImpl(WebChromeClient.CustomViewCallback customViewCallback) {
            this.mCustomViewCallback = customViewCallback;
        }

        public void onCustomViewHidden() {
            this.mCustomViewCallback.onCustomViewHidden();
        }
    }

    class GeolocationPermissionsCallbackImpl implements GeolocationPermissionsCallback {
        GeolocationPermissions.Callback mCallback;

        GeolocationPermissionsCallbackImpl(GeolocationPermissions.Callback callback) {
            this.mCallback = callback;
        }

        public void invoke(String origin, boolean allow, boolean remember) {
            this.mCallback.invoke(origin, allow, remember);
        }
    }
}
