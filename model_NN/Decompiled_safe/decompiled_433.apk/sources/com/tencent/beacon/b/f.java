package com.tencent.beacon.b;

import android.content.Context;
import com.tencent.beacon.a.b.b;
import com.tencent.beacon.a.b.c;
import com.tencent.beacon.a.b.g;
import com.tencent.beacon.a.b.h;
import com.tencent.beacon.a.d;
import com.tencent.beacon.d.a;
import com.tencent.beacon.event.t;
import com.tencent.beacon.f.i;
import com.tencent.beacon.f.j;
import java.util.Arrays;
import java.util.List;

/* compiled from: ProGuard */
public final class f implements b {

    /* renamed from: a  reason: collision with root package name */
    private static f f2109a = null;
    private Context b;
    private j c;
    private i d = new g();

    public static synchronized f a(Context context, j jVar) {
        f fVar;
        synchronized (f.class) {
            if (f2109a == null && !t.f2145a) {
                a.e(" SpeedMonitorModule create instance", new Object[0]);
                f2109a = new f(context, jVar);
            }
            fVar = f2109a;
        }
        return fVar;
    }

    public static synchronized f d() {
        f fVar;
        synchronized (f.class) {
            fVar = f2109a;
        }
        return fVar;
    }

    private f(Context context, j jVar) {
        this.b = context;
        this.c = jVar;
        if (this.c != null) {
            this.c.a(105, this.d);
        }
        c a2 = c.a(this.b);
        a2.a(this);
        a2.a(2, this.c);
    }

    public static boolean e() {
        g a2 = g.a();
        if (a2 != null) {
            return a2.c(2);
        }
        return false;
    }

    public final boolean a(b[] bVarArr) {
        if (bVarArr != null && bVarArr.length > 0) {
            d.a().a(new e(this.b, Arrays.asList(bVarArr)));
        }
        return true;
    }

    public final void a() {
    }

    public final void b() {
        g b2 = c.a(this.b).b();
        if (b2 != null) {
            h b3 = b2.b(2);
            if (b3.a() && b3 != null && b3.e() != null) {
                try {
                    i iVar = this.d;
                    List<b> a2 = g.a(b3.e());
                    if (a2 != null) {
                        a((b[]) a2.toArray(new b[0]));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public final void c() {
    }
}
