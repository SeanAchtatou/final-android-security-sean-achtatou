package com.tencent.beacon.f;

import android.content.Context;
import com.tencent.beacon.a.b.c;
import com.tencent.beacon.a.b.g;
import com.tencent.beacon.d.a;

/* compiled from: ProGuard */
public abstract class b {

    /* renamed from: a  reason: collision with root package name */
    protected final int f2149a;
    protected final int b;
    protected Context c;
    protected String d;

    public abstract com.tencent.beacon.c.a.b a();

    public abstract void b(boolean z);

    public b(Context context, int i, int i2) {
        this.c = context;
        this.f2149a = i2;
        this.b = i;
    }

    public final int c() {
        return this.f2149a;
    }

    public final synchronized String d() {
        return this.d;
    }

    public final String e() {
        try {
            if (this.b == 0) {
                return c.a(this.c).b().b();
            }
            return c.a(this.c).b().b(this.b).b();
        } catch (Throwable th) {
            a.c("imposiable comStrategy error:%s", th.toString());
            th.printStackTrace();
            return null;
        }
    }

    public static com.tencent.beacon.c.a.b a(Context context, int i, byte[] bArr) {
        byte b2;
        String str;
        byte b3 = -1;
        if (bArr != null) {
            try {
                g b4 = c.a(context).b();
                if (b4 != null) {
                    byte i2 = b4.i();
                    b3 = b4.j();
                    b2 = i2;
                    str = b4.k();
                } else {
                    str = null;
                    b2 = -1;
                }
                bArr = com.tencent.beacon.b.a.a(bArr, b3, b2, str);
                if (bArr == null) {
                    a.c("enzp error! :%d %d ", Integer.valueOf(b3), Integer.valueOf(b2));
                    return null;
                }
            } catch (Throwable th) {
                a.c("imposiable comStrategy error:%s", th.toString());
                th.printStackTrace();
                return null;
            }
        } else {
            b2 = -1;
        }
        return com.tencent.beacon.b.a.a(i, com.tencent.beacon.a.g.m(), bArr, b3, b2);
    }

    public void b() {
        a.c("encode failed, clear db data", new Object[0]);
    }
}
