package com.tencent.beacon.a;

import android.content.Context;
import com.tencent.assistant.st.STConst;
import com.tencent.beacon.d.a;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public final class g {
    private static g n = null;

    /* renamed from: a  reason: collision with root package name */
    private Context f2100a = null;
    private String b = Constants.STR_EMPTY;
    private String c = Constants.STR_EMPTY;
    private byte d = -1;
    private String e = Constants.STR_EMPTY;
    private String f = Constants.STR_EMPTY;
    private String g = Constants.STR_EMPTY;
    private String h = Constants.STR_EMPTY;
    private String i = Constants.STR_EMPTY;
    private long j = 0;
    private String k = Constants.STR_EMPTY;
    private String l = Constants.STR_EMPTY;
    private String m = Constants.STR_EMPTY;

    public final String a() {
        return this.b;
    }

    public final String b() {
        return this.c;
    }

    public final synchronized byte c() {
        return this.d;
    }

    private synchronized void a(byte b2) {
        this.d = 1;
    }

    private synchronized String n() {
        return this.e;
    }

    private synchronized void e(String str) {
        this.e = str;
    }

    public final synchronized String d() {
        return this.f;
    }

    private synchronized void f(String str) {
        this.f = str;
    }

    public final synchronized String e() {
        return this.g;
    }

    private synchronized void g(String str) {
        this.g = str;
    }

    public final synchronized String f() {
        return this.h;
    }

    private synchronized void h(String str) {
        this.h = str;
    }

    public final synchronized String g() {
        return this.i;
    }

    public final synchronized void a(String str) {
        this.i = str;
    }

    public final synchronized long h() {
        return this.j;
    }

    public final synchronized void a(long j2) {
        this.j = j2;
    }

    public final synchronized String i() {
        if (Constants.STR_EMPTY.equals(this.k)) {
            try {
                this.k = a.b(this.f2100a, "IMEI_DENGTA", Constants.STR_EMPTY);
            } catch (Exception e2) {
            }
        }
        return this.k;
    }

    public final synchronized void b(String str) {
        if (!Constants.STR_EMPTY.equals(str)) {
            try {
                a.a(this.f2100a, "IMEI_DENGTA", str);
            } catch (Exception e2) {
            }
        }
        this.k = str;
    }

    public final synchronized String j() {
        return this.l;
    }

    public final synchronized String k() {
        return this.m;
    }

    public final synchronized void c(String str) {
        this.m = str;
    }

    public final synchronized void d(String str) {
        this.l = str;
    }

    private g() {
    }

    public final synchronized Context l() {
        return this.f2100a;
    }

    public static synchronized void a(Context context) {
        synchronized (g.class) {
            if (context != null) {
                if (n == null) {
                    n = new g();
                }
                synchronized (n) {
                    a.e("init cominfo", new Object[0]);
                    n.f2100a = context;
                    h.a(context);
                    n.b = h.a();
                    g gVar = n;
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("Android ");
                    stringBuffer.append(h.b());
                    stringBuffer.append(",level ");
                    stringBuffer.append(h.c());
                    gVar.c = stringBuffer.toString();
                    n.a((byte) 1);
                    n.e(a.e(context));
                    n.f(a.f(context));
                    n.g("beacon");
                    n.h("1.8.5");
                    n.a(STConst.ST_INSTALL_FAIL_STR_UNKNOWN);
                    n.b(h.b(context));
                    String a2 = a.a(context);
                    if (!Constants.STR_EMPTY.equals(a2)) {
                        n.d(a2);
                    } else {
                        n.d(n.n());
                    }
                }
            }
        }
    }

    public static synchronized g m() {
        g gVar;
        synchronized (g.class) {
            gVar = n;
        }
        return gVar;
    }
}
