package com.tencent.feedback.proguard;

import android.os.Process;
import com.tencent.feedback.eup.jni.c;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public final class ag {

    /* renamed from: a  reason: collision with root package name */
    private ByteBuffer f2585a;
    private String b = "GBK";

    public ag() {
    }

    public ag(byte[] bArr) {
        this.f2585a = ByteBuffer.wrap(bArr);
    }

    public ag(byte[] bArr, int i) {
        this.f2585a = ByteBuffer.wrap(bArr);
        this.f2585a.position(4);
    }

    public final void a(byte[] bArr) {
        if (this.f2585a != null) {
            this.f2585a.clear();
        }
        this.f2585a = ByteBuffer.wrap(bArr);
    }

    private static int a(c cVar, ByteBuffer byteBuffer) {
        byte b2 = byteBuffer.get();
        cVar.f2561a = (byte) (b2 & 15);
        cVar.b = (b2 & 240) >> 4;
        if (cVar.b != 15) {
            return 1;
        }
        cVar.b = byteBuffer.get();
        return 2;
    }

    private boolean a(int i) {
        try {
            c cVar = new c();
            while (true) {
                int a2 = a(cVar, this.f2585a.duplicate());
                if (i > cVar.b && cVar.f2561a != 11) {
                    this.f2585a.position(a2 + this.f2585a.position());
                    a(cVar.f2561a);
                }
            }
            if (i == cVar.b) {
                return true;
            }
            return false;
        } catch (C0007g | BufferUnderflowException e) {
            return false;
        }
    }

    private void a() {
        c cVar = new c();
        do {
            a(cVar, this.f2585a);
            a(cVar.f2561a);
        } while (cVar.f2561a != 11);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int */
    private void a(byte b2) {
        int i = 0;
        switch (b2) {
            case 0:
                this.f2585a.position(this.f2585a.position() + 1);
                return;
            case 1:
                this.f2585a.position(2 + this.f2585a.position());
                return;
            case 2:
                this.f2585a.position(this.f2585a.position() + 4);
                return;
            case 3:
                this.f2585a.position(this.f2585a.position() + 8);
                return;
            case 4:
                this.f2585a.position(this.f2585a.position() + 4);
                return;
            case 5:
                this.f2585a.position(this.f2585a.position() + 8);
                return;
            case 6:
                int i2 = this.f2585a.get();
                if (i2 < 0) {
                    i2 += Process.PROC_COMBINE;
                }
                this.f2585a.position(i2 + this.f2585a.position());
                return;
            case 7:
                this.f2585a.position(this.f2585a.getInt() + this.f2585a.position());
                return;
            case 8:
                int a2 = a(0, 0, true);
                while (i < (a2 << 1)) {
                    c cVar = new c();
                    a(cVar, this.f2585a);
                    a(cVar.f2561a);
                    i++;
                }
                return;
            case 9:
                int a3 = a(0, 0, true);
                while (i < a3) {
                    c cVar2 = new c();
                    a(cVar2, this.f2585a);
                    a(cVar2.f2561a);
                    i++;
                }
                return;
            case 10:
                a();
                return;
            case 11:
            case 12:
                return;
            case 13:
                c cVar3 = new c();
                a(cVar3, this.f2585a);
                if (cVar3.f2561a != 0) {
                    throw new C0007g("skipField with invalid type, type value: " + ((int) b2) + ", " + ((int) cVar3.f2561a));
                }
                this.f2585a.position(a(0, 0, true) + this.f2585a.position());
                return;
            default:
                throw new C0007g("invalid type.");
        }
    }

    public final boolean a(int i, boolean z) {
        if (a((byte) 0, i, z) != 0) {
            return true;
        }
        return false;
    }

    public final byte a(byte b2, int i, boolean z) {
        if (a(i)) {
            c cVar = new c();
            a(cVar, this.f2585a);
            switch (cVar.f2561a) {
                case 0:
                    return this.f2585a.get();
                case 12:
                    return 0;
                default:
                    throw new C0007g("type mismatch.");
            }
        } else if (!z) {
            return b2;
        } else {
            throw new C0007g("require field not exist.");
        }
    }

    public final short a(short s, int i, boolean z) {
        if (a(i)) {
            c cVar = new c();
            a(cVar, this.f2585a);
            switch (cVar.f2561a) {
                case 0:
                    return (short) this.f2585a.get();
                case 1:
                    return this.f2585a.getShort();
                case 12:
                    return 0;
                default:
                    throw new C0007g("type mismatch.");
            }
        } else if (!z) {
            return s;
        } else {
            throw new C0007g("require field not exist.");
        }
    }

    public final int a(int i, int i2, boolean z) {
        if (a(i2)) {
            c cVar = new c();
            a(cVar, this.f2585a);
            switch (cVar.f2561a) {
                case 0:
                    return this.f2585a.get();
                case 1:
                    return this.f2585a.getShort();
                case 2:
                    return this.f2585a.getInt();
                case 12:
                    return 0;
                default:
                    throw new C0007g("type mismatch.");
            }
        } else if (!z) {
            return i;
        } else {
            throw new C0007g("require field not exist.");
        }
    }

    public final long a(long j, int i, boolean z) {
        if (a(i)) {
            c cVar = new c();
            a(cVar, this.f2585a);
            switch (cVar.f2561a) {
                case 0:
                    return (long) this.f2585a.get();
                case 1:
                    return (long) this.f2585a.getShort();
                case 2:
                    return (long) this.f2585a.getInt();
                case 3:
                    return this.f2585a.getLong();
                case 12:
                    return 0;
                default:
                    throw new C0007g("type mismatch.");
            }
        } else if (!z) {
            return j;
        } else {
            throw new C0007g("require field not exist.");
        }
    }

    public final float a(float f, int i, boolean z) {
        if (a(i)) {
            c cVar = new c();
            a(cVar, this.f2585a);
            switch (cVar.f2561a) {
                case 4:
                    return this.f2585a.getFloat();
                case 12:
                    return 0.0f;
                default:
                    throw new C0007g("type mismatch.");
            }
        } else if (!z) {
            return f;
        } else {
            throw new C0007g("require field not exist.");
        }
    }

    private double a(double d, int i, boolean z) {
        if (a(i)) {
            c cVar = new c();
            a(cVar, this.f2585a);
            switch (cVar.f2561a) {
                case 4:
                    return (double) this.f2585a.getFloat();
                case 5:
                    return this.f2585a.getDouble();
                case 12:
                    return 0.0d;
                default:
                    throw new C0007g("type mismatch.");
            }
        } else if (!z) {
            return d;
        } else {
            throw new C0007g("require field not exist.");
        }
    }

    public final String b(int i, boolean z) {
        if (a(i)) {
            c cVar = new c();
            a(cVar, this.f2585a);
            switch (cVar.f2561a) {
                case 6:
                    int i2 = this.f2585a.get();
                    if (i2 < 0) {
                        i2 += Process.PROC_COMBINE;
                    }
                    byte[] bArr = new byte[i2];
                    this.f2585a.get(bArr);
                    try {
                        return new String(bArr, this.b);
                    } catch (UnsupportedEncodingException e) {
                        return new String(bArr);
                    }
                case 7:
                    int i3 = this.f2585a.getInt();
                    if (i3 > 104857600 || i3 < 0) {
                        throw new C0007g("String too long: " + i3);
                    }
                    byte[] bArr2 = new byte[i3];
                    this.f2585a.get(bArr2);
                    try {
                        return new String(bArr2, this.b);
                    } catch (UnsupportedEncodingException e2) {
                        return new String(bArr2);
                    }
                default:
                    throw new C0007g("type mismatch.");
            }
        } else if (!z) {
            return null;
        } else {
            throw new C0007g("require field not exist.");
        }
    }

    public final <K, V> HashMap<K, V> a(Map map, int i, boolean z) {
        return (HashMap) a(new HashMap(), map, i, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.lang.Object, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object */
    private <K, V> Map<K, V> a(Map<K, V> map, Map<K, V> map2, int i, boolean z) {
        if (map2 == null || map2.isEmpty()) {
            return new HashMap();
        }
        Map.Entry next = map2.entrySet().iterator().next();
        Object key = next.getKey();
        Object value = next.getValue();
        if (a(i)) {
            c cVar = new c();
            a(cVar, this.f2585a);
            switch (cVar.f2561a) {
                case 8:
                    int a2 = a(0, 0, true);
                    if (a2 < 0) {
                        throw new C0007g("size invalid: " + a2);
                    }
                    for (int i2 = 0; i2 < a2; i2++) {
                        map.put(a(key, 0, true), a(value, 1, true));
                    }
                    return map;
                default:
                    throw new C0007g("type mismatch.");
            }
        } else if (!z) {
            return map;
        } else {
            throw new C0007g("require field not exist.");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte */
    private boolean[] d(int i, boolean z) {
        boolean z2;
        if (a(i)) {
            c cVar = new c();
            a(cVar, this.f2585a);
            switch (cVar.f2561a) {
                case 9:
                    int a2 = a(0, 0, true);
                    if (a2 < 0) {
                        throw new C0007g("size invalid: " + a2);
                    }
                    boolean[] zArr = new boolean[a2];
                    for (int i2 = 0; i2 < a2; i2++) {
                        if (a((byte) 0, 0, true) != 0) {
                            z2 = true;
                        } else {
                            z2 = false;
                        }
                        zArr[i2] = z2;
                    }
                    return zArr;
                default:
                    throw new C0007g("type mismatch.");
            }
        } else if (!z) {
            return null;
        } else {
            throw new C0007g("require field not exist.");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte */
    public final byte[] c(int i, boolean z) {
        if (a(i)) {
            c cVar = new c();
            a(cVar, this.f2585a);
            switch (cVar.f2561a) {
                case 9:
                    int a2 = a(0, 0, true);
                    if (a2 < 0) {
                        throw new C0007g("size invalid: " + a2);
                    }
                    byte[] bArr = new byte[a2];
                    for (int i2 = 0; i2 < a2; i2++) {
                        bArr[i2] = a(bArr[0], 0, true);
                    }
                    return bArr;
                case 13:
                    c cVar2 = new c();
                    a(cVar2, this.f2585a);
                    if (cVar2.f2561a != 0) {
                        throw new C0007g("type mismatch, tag: " + i + ", type: " + ((int) cVar.f2561a) + ", " + ((int) cVar2.f2561a));
                    }
                    int a3 = a(0, 0, true);
                    if (a3 < 0) {
                        throw new C0007g("invalid size, tag: " + i + ", type: " + ((int) cVar.f2561a) + ", " + ((int) cVar2.f2561a) + ", size: " + a3);
                    }
                    byte[] bArr2 = new byte[a3];
                    this.f2585a.get(bArr2);
                    return bArr2;
                default:
                    throw new C0007g("type mismatch.");
            }
        } else if (!z) {
            return null;
        } else {
            throw new C0007g("require field not exist.");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(short, int, boolean):short
     arg types: [short, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short */
    private short[] e(int i, boolean z) {
        short[] sArr = null;
        if (a(i)) {
            c cVar = new c();
            a(cVar, this.f2585a);
            switch (cVar.f2561a) {
                case 9:
                    break;
                default:
                    throw new C0007g("type mismatch.");
            }
            int a2 = a(0, 0, true);
            if (a2 < 0) {
                throw new C0007g("size invalid: " + a2);
            }
            sArr = new short[a2];
            for (int i2 = 0; i2 < a2; i2++) {
                sArr[i2] = a(sArr[0], 0, true);
            }
        } else if (z) {
            throw new C0007g("require field not exist.");
        }
        return sArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int */
    private int[] f(int i, boolean z) {
        int[] iArr = null;
        if (a(i)) {
            c cVar = new c();
            a(cVar, this.f2585a);
            switch (cVar.f2561a) {
                case 9:
                    break;
                default:
                    throw new C0007g("type mismatch.");
            }
            int a2 = a(0, 0, true);
            if (a2 < 0) {
                throw new C0007g("size invalid: " + a2);
            }
            iArr = new int[a2];
            for (int i2 = 0; i2 < a2; i2++) {
                iArr[i2] = a(iArr[0], 0, true);
            }
        } else if (z) {
            throw new C0007g("require field not exist.");
        }
        return iArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long */
    private long[] g(int i, boolean z) {
        long[] jArr = null;
        if (a(i)) {
            c cVar = new c();
            a(cVar, this.f2585a);
            switch (cVar.f2561a) {
                case 9:
                    break;
                default:
                    throw new C0007g("type mismatch.");
            }
            int a2 = a(0, 0, true);
            if (a2 < 0) {
                throw new C0007g("size invalid: " + a2);
            }
            jArr = new long[a2];
            for (int i2 = 0; i2 < a2; i2++) {
                jArr[i2] = a(jArr[0], 0, true);
            }
        } else if (z) {
            throw new C0007g("require field not exist.");
        }
        return jArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(float, int, boolean):float
     arg types: [float, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float */
    private float[] h(int i, boolean z) {
        float[] fArr = null;
        if (a(i)) {
            c cVar = new c();
            a(cVar, this.f2585a);
            switch (cVar.f2561a) {
                case 9:
                    break;
                default:
                    throw new C0007g("type mismatch.");
            }
            int a2 = a(0, 0, true);
            if (a2 < 0) {
                throw new C0007g("size invalid: " + a2);
            }
            fArr = new float[a2];
            for (int i2 = 0; i2 < a2; i2++) {
                fArr[i2] = a(fArr[0], 0, true);
            }
        } else if (z) {
            throw new C0007g("require field not exist.");
        }
        return fArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(double, int, boolean):double
     arg types: [double, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double */
    private double[] i(int i, boolean z) {
        double[] dArr = null;
        if (a(i)) {
            c cVar = new c();
            a(cVar, this.f2585a);
            switch (cVar.f2561a) {
                case 9:
                    break;
                default:
                    throw new C0007g("type mismatch.");
            }
            int a2 = a(0, 0, true);
            if (a2 < 0) {
                throw new C0007g("size invalid: " + a2);
            }
            dArr = new double[a2];
            for (int i2 = 0; i2 < a2; i2++) {
                dArr[i2] = a(dArr[0], 0, true);
            }
        } else if (z) {
            throw new C0007g("require field not exist.");
        }
        return dArr;
    }

    private <T> T[] a(Object[] objArr, int i, boolean z) {
        if (objArr != null && objArr.length != 0) {
            return b(objArr[0], i, z);
        }
        throw new C0007g("unable to get type of key and value.");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [T, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object */
    private <T> T[] b(T t, int i, boolean z) {
        if (a(i)) {
            c cVar = new c();
            a(cVar, this.f2585a);
            switch (cVar.f2561a) {
                case 9:
                    int a2 = a(0, 0, true);
                    if (a2 < 0) {
                        throw new C0007g("size invalid: " + a2);
                    }
                    T[] tArr = (Object[]) Array.newInstance(t.getClass(), a2);
                    for (int i2 = 0; i2 < a2; i2++) {
                        tArr[i2] = a((Object) t, 0, true);
                    }
                    return tArr;
                default:
                    throw new C0007g("type mismatch.");
            }
        } else if (!z) {
            return null;
        } else {
            throw new C0007g("require field not exist.");
        }
    }

    public final C0008j a(C0008j jVar, int i, boolean z) {
        C0008j jVar2 = null;
        if (a(i)) {
            try {
                jVar2 = (C0008j) jVar.getClass().newInstance();
                c cVar = new c();
                a(cVar, this.f2585a);
                if (cVar.f2561a != 10) {
                    throw new C0007g("type mismatch.");
                }
                jVar2.a(this);
                a();
            } catch (Exception e) {
                throw new C0007g(e.getMessage());
            }
        } else if (z) {
            throw new C0007g("require field not exist.");
        }
        return jVar2;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v0 */
    /* JADX WARN: Type inference failed for: r0v23, types: [int] */
    /* JADX WARN: Type inference failed for: r0v45, types: [boolean] */
    /* JADX WARN: Type inference failed for: r0v47 */
    /* JADX WARN: Type inference failed for: r0v50 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(short, int, boolean):short
     arg types: [int, int, boolean]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(long, int, boolean):long
     arg types: [int, int, boolean]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(float, int, boolean):float
     arg types: [int, int, boolean]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(double, int, boolean):double
     arg types: [int, int, boolean]
     candidates:
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <T> java.lang.Object a(java.lang.Object r5, int r6, boolean r7) {
        /*
            r4 = this;
            r0 = 0
            boolean r1 = r5 instanceof java.lang.Byte
            if (r1 == 0) goto L_0x000e
            byte r0 = r4.a(r0, r6, r7)
            java.lang.Byte r0 = java.lang.Byte.valueOf(r0)
        L_0x000d:
            return r0
        L_0x000e:
            boolean r1 = r5 instanceof java.lang.Boolean
            if (r1 == 0) goto L_0x001e
            byte r1 = r4.a(r0, r6, r7)
            if (r1 == 0) goto L_0x0019
            r0 = 1
        L_0x0019:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            goto L_0x000d
        L_0x001e:
            boolean r1 = r5 instanceof java.lang.Short
            if (r1 == 0) goto L_0x002b
            short r0 = r4.a(r0, r6, r7)
            java.lang.Short r0 = java.lang.Short.valueOf(r0)
            goto L_0x000d
        L_0x002b:
            boolean r1 = r5 instanceof java.lang.Integer
            if (r1 == 0) goto L_0x0038
            int r0 = r4.a(r0, r6, r7)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            goto L_0x000d
        L_0x0038:
            boolean r1 = r5 instanceof java.lang.Long
            if (r1 == 0) goto L_0x0047
            r0 = 0
            long r0 = r4.a(r0, r6, r7)
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            goto L_0x000d
        L_0x0047:
            boolean r1 = r5 instanceof java.lang.Float
            if (r1 == 0) goto L_0x0055
            r0 = 0
            float r0 = r4.a(r0, r6, r7)
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            goto L_0x000d
        L_0x0055:
            boolean r1 = r5 instanceof java.lang.Double
            if (r1 == 0) goto L_0x0064
            r0 = 0
            double r0 = r4.a(r0, r6, r7)
            java.lang.Double r0 = java.lang.Double.valueOf(r0)
            goto L_0x000d
        L_0x0064:
            boolean r1 = r5 instanceof java.lang.String
            if (r1 == 0) goto L_0x0071
            java.lang.String r0 = r4.b(r6, r7)
            java.lang.String r0 = java.lang.String.valueOf(r0)
            goto L_0x000d
        L_0x0071:
            boolean r1 = r5 instanceof java.util.Map
            if (r1 == 0) goto L_0x0083
            java.util.Map r5 = (java.util.Map) r5
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            java.util.Map r0 = r4.a(r0, r5, r6, r7)
            java.util.HashMap r0 = (java.util.HashMap) r0
            goto L_0x000d
        L_0x0083:
            boolean r1 = r5 instanceof java.util.List
            if (r1 == 0) goto L_0x00b8
            java.util.List r5 = (java.util.List) r5
            if (r5 == 0) goto L_0x0091
            boolean r1 = r5.isEmpty()
            if (r1 == 0) goto L_0x0098
        L_0x0091:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            goto L_0x000d
        L_0x0098:
            java.lang.Object r1 = r5.get(r0)
            java.lang.Object[] r2 = r4.b(r1, r6, r7)
            if (r2 != 0) goto L_0x00a5
            r0 = 0
            goto L_0x000d
        L_0x00a5:
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
        L_0x00aa:
            int r3 = r2.length
            if (r0 >= r3) goto L_0x00b5
            r3 = r2[r0]
            r1.add(r3)
            int r0 = r0 + 1
            goto L_0x00aa
        L_0x00b5:
            r0 = r1
            goto L_0x000d
        L_0x00b8:
            boolean r0 = r5 instanceof com.tencent.feedback.proguard.C0008j
            if (r0 == 0) goto L_0x00c4
            com.tencent.feedback.proguard.j r5 = (com.tencent.feedback.proguard.C0008j) r5
            com.tencent.feedback.proguard.j r0 = r4.a(r5, r6, r7)
            goto L_0x000d
        L_0x00c4:
            java.lang.Class r0 = r5.getClass()
            boolean r0 = r0.isArray()
            if (r0 == 0) goto L_0x0120
            boolean r0 = r5 instanceof byte[]
            if (r0 != 0) goto L_0x00d6
            boolean r0 = r5 instanceof java.lang.Byte[]
            if (r0 == 0) goto L_0x00dc
        L_0x00d6:
            byte[] r0 = r4.c(r6, r7)
            goto L_0x000d
        L_0x00dc:
            boolean r0 = r5 instanceof boolean[]
            if (r0 == 0) goto L_0x00e6
            boolean[] r0 = r4.d(r6, r7)
            goto L_0x000d
        L_0x00e6:
            boolean r0 = r5 instanceof short[]
            if (r0 == 0) goto L_0x00f0
            short[] r0 = r4.e(r6, r7)
            goto L_0x000d
        L_0x00f0:
            boolean r0 = r5 instanceof int[]
            if (r0 == 0) goto L_0x00fa
            int[] r0 = r4.f(r6, r7)
            goto L_0x000d
        L_0x00fa:
            boolean r0 = r5 instanceof long[]
            if (r0 == 0) goto L_0x0104
            long[] r0 = r4.g(r6, r7)
            goto L_0x000d
        L_0x0104:
            boolean r0 = r5 instanceof float[]
            if (r0 == 0) goto L_0x010e
            float[] r0 = r4.h(r6, r7)
            goto L_0x000d
        L_0x010e:
            boolean r0 = r5 instanceof double[]
            if (r0 == 0) goto L_0x0118
            double[] r0 = r4.i(r6, r7)
            goto L_0x000d
        L_0x0118:
            java.lang.Object[] r5 = (java.lang.Object[]) r5
            java.lang.Object[] r0 = r4.a(r5, r6, r7)
            goto L_0x000d
        L_0x0120:
            com.tencent.feedback.proguard.g r0 = new com.tencent.feedback.proguard.g
            java.lang.String r1 = "read object error: unsupport type."
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object");
    }

    public final int a(String str) {
        this.b = str;
        return 0;
    }
}
