package com.tencent.feedback.proguard;

import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public final class Y extends C0008j {
    private static Map<String, byte[]> k;
    private static ArrayList<X> l;
    private static J m;

    /* renamed from: a  reason: collision with root package name */
    public String f2581a = Constants.STR_EMPTY;
    public String b = Constants.STR_EMPTY;
    public String c = Constants.STR_EMPTY;
    public String d = Constants.STR_EMPTY;
    public byte e = 0;
    public ArrayList<X> f = null;
    public J g = null;
    private Map<String, byte[]> h = null;
    private String i = Constants.STR_EMPTY;
    private String j = Constants.STR_EMPTY;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.String, byte[]>, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.ArrayList<com.tencent.feedback.proguard.X>, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
     arg types: [com.tencent.feedback.proguard.J, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j */
    public final void a(ag agVar) {
        this.f2581a = agVar.b(0, true);
        this.b = agVar.b(1, false);
        this.c = agVar.b(2, false);
        this.d = agVar.b(3, false);
        if (k == null) {
            k = new HashMap();
            byte[] bArr = new byte[1];
            bArr[0] = 0;
            k.put(Constants.STR_EMPTY, bArr);
        }
        this.h = (Map) agVar.a((Object) k, 4, false);
        this.e = agVar.a(this.e, 5, false);
        if (l == null) {
            l = new ArrayList<>();
            l.add(new X());
        }
        this.f = (ArrayList) agVar.a((Object) l, 6, false);
        this.i = agVar.b(7, false);
        this.j = agVar.b(8, false);
        if (m == null) {
            m = new J();
        }
        this.g = (J) agVar.a((C0008j) m, 9, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ah.a(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, byte[]>, int]
     candidates:
      com.tencent.feedback.proguard.ah.a(byte, int):void
      com.tencent.feedback.proguard.ah.a(float, int):void
      com.tencent.feedback.proguard.ah.a(int, int):void
      com.tencent.feedback.proguard.ah.a(long, int):void
      com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.String, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void
      com.tencent.feedback.proguard.ah.a(short, int):void
      com.tencent.feedback.proguard.ah.a(boolean, int):void
      com.tencent.feedback.proguard.ah.a(byte[], int):void
      com.tencent.feedback.proguard.ah.a(java.util.Map, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void
     arg types: [java.util.ArrayList<com.tencent.feedback.proguard.X>, int]
     candidates:
      com.tencent.feedback.proguard.ah.a(byte, int):void
      com.tencent.feedback.proguard.ah.a(float, int):void
      com.tencent.feedback.proguard.ah.a(int, int):void
      com.tencent.feedback.proguard.ah.a(long, int):void
      com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.String, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Map, int):void
      com.tencent.feedback.proguard.ah.a(short, int):void
      com.tencent.feedback.proguard.ah.a(boolean, int):void
      com.tencent.feedback.proguard.ah.a(byte[], int):void
      com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void
     arg types: [com.tencent.feedback.proguard.J, int]
     candidates:
      com.tencent.feedback.proguard.ah.a(byte, int):void
      com.tencent.feedback.proguard.ah.a(float, int):void
      com.tencent.feedback.proguard.ah.a(int, int):void
      com.tencent.feedback.proguard.ah.a(long, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.String, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Map, int):void
      com.tencent.feedback.proguard.ah.a(short, int):void
      com.tencent.feedback.proguard.ah.a(boolean, int):void
      com.tencent.feedback.proguard.ah.a(byte[], int):void
      com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void */
    public final void a(ah ahVar) {
        ahVar.a(this.f2581a, 0);
        if (this.b != null) {
            ahVar.a(this.b, 1);
        }
        if (this.c != null) {
            ahVar.a(this.c, 2);
        }
        if (this.d != null) {
            ahVar.a(this.d, 3);
        }
        if (this.h != null) {
            ahVar.a((Map) this.h, 4);
        }
        ahVar.a(this.e, 5);
        if (this.f != null) {
            ahVar.a((Collection) this.f, 6);
        }
        if (this.i != null) {
            ahVar.a(this.i, 7);
        }
        if (this.j != null) {
            ahVar.a(this.j, 8);
        }
        if (this.g != null) {
            ahVar.a((C0008j) this.g, 9);
        }
    }

    public final void a(StringBuilder sb, int i2) {
    }
}
