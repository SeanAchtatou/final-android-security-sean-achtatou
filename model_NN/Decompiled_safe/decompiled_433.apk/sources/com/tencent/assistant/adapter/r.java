package com.tencent.assistant.adapter;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class r extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f602a;
    final /* synthetic */ int b;
    final /* synthetic */ RankClassicListAdapter c;

    r(RankClassicListAdapter rankClassicListAdapter, SimpleAppModel simpleAppModel, int i) {
        this.c = rankClassicListAdapter;
        this.f602a = simpleAppModel;
        this.b = i;
    }

    public void onTMAClick(View view) {
        Bundle bundle = new Bundle();
        if (this.c.e instanceof BaseActivity) {
            bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.c.e).f());
        }
        bundle.putSerializable("statInfo", new StatInfo(this.f602a.b, this.c.i, this.c.l.d(), this.c.l.b(), this.c.l.a()));
        bundle.putSerializable("com.tencent.assistant.ACTION_URL", this.f602a.aa);
        b.b(this.c.e, this.f602a.aa.f1125a, bundle);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.c.e, this.f602a, this.c.b(this.b), 200, null);
        if (buildSTInfo != null) {
            buildSTInfo.extraData = String.valueOf(this.c.i) + "|" + this.c.k;
        }
        return buildSTInfo;
    }
}
