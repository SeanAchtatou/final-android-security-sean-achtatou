package com.tencent.assistant.activity.pictureprocessor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.StrinptTipsView;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class ShowPictureActivity extends Activity implements Animation.AnimationListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ArrayList<String> f549a;
    private List<String> b = new ArrayList();
    private String c;
    private MyViewPager d;
    /* access modifiers changed from: private */
    public h e;
    private View f;
    private int g = 0;
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public StrinptTipsView i;
    private ArrayList<int[]> j = new ArrayList<>();
    private boolean k = false;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        overridePendingTransition(-1, -1);
        setContentView((int) R.layout.show_pic_view);
        this.e = new h(this);
        a(getIntent());
        a();
        c();
    }

    private void a(Intent intent) {
        ArrayList<String> stringArrayListExtra = intent.getStringArrayListExtra("picUrls");
        if (stringArrayListExtra == null || stringArrayListExtra.size() <= 0) {
            this.f549a = new ArrayList<>();
            Toast.makeText(this, (int) R.string.no_picture_to_show, 0).show();
            finish();
            return;
        }
        this.f549a = stringArrayListExtra;
        int intExtra = intent.getIntExtra("startPos", 0);
        if (intExtra < this.f549a.size()) {
            this.c = this.f549a.get(intExtra);
        } else {
            this.c = this.f549a.get(0);
        }
        ArrayList<String> stringArrayListExtra2 = intent.getStringArrayListExtra("thumbnails");
        if (stringArrayListExtra2 != null) {
            this.b.addAll(stringArrayListExtra2);
        }
        this.j.add(intent.getIntArrayExtra("imagePos"));
        this.e.a(this.j);
        this.g = intent.getIntExtra("startPos", 0);
        b();
        this.e.a(this.g);
    }

    private void a() {
        if (this.f549a != null && this.f549a.size() != 0) {
            this.i = (StrinptTipsView) findViewById(R.id.point);
            this.f = findViewById(R.id.cover);
            this.d = (MyViewPager) findViewById(R.id.myViewPager);
            this.d.setAdapter(this.e);
            this.e.a(this.f549a, this.b);
            this.e.a(new k(this));
            this.d.setOnPageChangeListener(new l(this));
            this.d.setCurrentItem(this.g);
            if (this.f549a != null) {
                this.i.setSelect(this.f549a.size(), this.g, 0);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        this.c = this.e.c(i2);
    }

    private void b() {
        if (this.f549a != null && this.c != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= this.f549a.size()) {
                    break;
                }
                if (this.c.equals(this.f549a.get(i3))) {
                    this.g = i3;
                    break;
                }
                i2 = i3 + 1;
            }
        }
        this.h = this.g;
    }

    public void finish() {
        d();
        if (!this.e.a(this, this.h)) {
            super.finish();
            overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
        }
    }

    private void c() {
        if (this.f549a != null && this.f549a.size() != 0) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(500);
            this.f.startAnimation(alphaAnimation);
            this.f.setVisibility(0);
        }
    }

    private void d() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(350);
        if (this.f != null) {
            this.f.startAnimation(alphaAnimation);
        }
    }

    public void onAnimationStart(Animation animation) {
        this.k = true;
    }

    public void onAnimationEnd(Animation animation) {
        this.f.setVisibility(8);
        this.k = false;
        super.finish();
        overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onDestroy() {
        if (this.d != null) {
            for (int childCount = this.d.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = this.d.getChildAt(childCount);
                if (childAt instanceof PicView) {
                    ((PicView) childAt).d();
                }
            }
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (this.k) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }
}
