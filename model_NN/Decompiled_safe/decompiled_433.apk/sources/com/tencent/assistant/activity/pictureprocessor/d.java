package com.tencent.assistant.activity.pictureprocessor;

import android.os.Handler;
import android.os.Message;
import com.tencent.assistant.thumbnailCache.o;

/* compiled from: ProGuard */
class d extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PicView f552a;

    d(PicView picView) {
        this.f552a = picView;
    }

    public void handleMessage(Message message) {
        this.f552a.f.setVisibility(8);
        this.f552a.e.setVisibility(8);
        switch (message.what) {
            case 0:
                this.f552a.a((o) message.obj);
                return;
            case 1:
                this.f552a.f();
                return;
            default:
                return;
        }
    }
}
