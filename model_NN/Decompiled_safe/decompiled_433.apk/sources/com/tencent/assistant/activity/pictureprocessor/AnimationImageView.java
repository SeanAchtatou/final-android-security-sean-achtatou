package com.tencent.assistant.activity.pictureprocessor;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import com.tencent.assistant.component.Shieldable;
import com.tencent.assistant.component.TouchAnalizer;
import com.tencent.assistant.component.TouchBehaviorListener;
import com.tencent.assistant.utils.by;
import com.tencent.assistant.utils.r;

/* compiled from: ProGuard */
public class AnimationImageView extends View implements Animation.AnimationListener, TouchBehaviorListener {
    private float A = -1.0f;
    private float B = -1.0f;
    private boolean C = false;
    private Animation.AnimationListener D;
    /* access modifiers changed from: private */
    public View.OnClickListener E;
    private Handler F = new a(this);
    private boolean G = false;
    private boolean H = false;
    private Shieldable I;

    /* renamed from: a  reason: collision with root package name */
    private Bitmap f545a;
    private Drawable b;
    private Paint c = new Paint(1);
    private int d;
    private int e;
    private Animation f;
    private Transformation g;
    private float[] h;
    private boolean i = false;
    private boolean j = true;
    private int[] k;
    private boolean l = false;
    private Transformation m;
    private Animation n;
    private Drawable o;
    private Drawable p;
    private float q = 1.0f;
    private float r = 0.0f;
    private float s = 0.0f;
    private int t = 0;
    private int u = 0;
    private TouchAnalizer v = new TouchAnalizer();
    private boolean w = false;
    private float x = 0.0f;
    private float y = 0.0f;
    private boolean z = true;

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.E = onClickListener;
    }

    public AnimationImageView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context);
    }

    public AnimationImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public AnimationImageView(Context context) {
        super(context);
        a(context);
    }

    private void a(Context context) {
        this.c.setFilterBitmap(true);
        this.v.setListener(TouchAnalizer.BehaviorType.SINGLE_CLICK, this);
        if (r.d() <= 4) {
            this.v.setListener(TouchAnalizer.BehaviorType.SINGLE_DRAG, this);
        } else {
            this.v.setListener(TouchAnalizer.BehaviorType.DRAG, this);
            this.v.setListener(TouchAnalizer.BehaviorType.PINCH, this);
        }
        this.v.setListener(TouchAnalizer.BehaviorType.DOUBLE_CLICK, this);
        this.v.setListener(TouchAnalizer.BehaviorType.LONG_CLICK, this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.pictureprocessor.AnimationImageView.a(android.view.animation.Animation, boolean):void
     arg types: [android.view.animation.Animation, int]
     candidates:
      com.tencent.assistant.activity.pictureprocessor.AnimationImageView.a(float, float):boolean
      com.tencent.assistant.activity.pictureprocessor.AnimationImageView.a(android.view.animation.Animation, boolean):void */
    public void startAnimation(Animation animation) {
        a(animation, false);
    }

    public void a(Animation animation, boolean z2) {
        this.f = animation;
        if (this.f != null) {
            this.f.start();
            this.g = new Transformation();
            this.h = new float[9];
            invalidate();
        }
        this.i = z2;
    }

    public void a(int[] iArr) {
        this.j = true;
        this.k = iArr;
    }

    public void b(int[] iArr) {
        this.k = iArr;
        this.j = false;
    }

    private void c() {
        int i2;
        float f2;
        if (this.k != null && this.k.length >= 4) {
            int[] iArr = new int[2];
            by.a(this, null, iArr);
            iArr[0] = 0;
            int[] iArr2 = this.k;
            float f3 = ((float) iArr2[2]) / ((float) this.d);
            int i3 = (int) (((float) iArr2[3]) / f3);
            if (this.f545a != null) {
                i2 = (this.f545a.getHeight() * this.d) / this.f545a.getWidth();
            } else if (this.b != null) {
                i2 = (this.b.getIntrinsicHeight() * this.d) / this.b.getIntrinsicWidth();
            } else {
                i2 = i3;
            }
            ScaleAnimation scaleAnimation = new ScaleAnimation(f3, 1.0f, f3, 1.0f, 0.0f, 0.0f);
            scaleAnimation.setInterpolator(new DecelerateInterpolator());
            scaleAnimation.setDuration(300);
            float f4 = (float) (iArr2[0] - iArr[0]);
            float f5 = (float) (iArr2[1] - iArr[1]);
            if (this.e > i2) {
                f2 = (float) ((this.e - i2) / 2);
            } else {
                f2 = 0.0f;
            }
            TranslateAnimation translateAnimation = new TranslateAnimation(f4, 0.0f, f5, f2);
            translateAnimation.initialize(this.d, this.e, this.d, this.e);
            translateAnimation.setInterpolator(new DecelerateInterpolator());
            translateAnimation.setDuration(300);
            AnimationSet animationSet = new AnimationSet(true);
            animationSet.addAnimation(scaleAnimation);
            animationSet.addAnimation(translateAnimation);
            if (this.D != null) {
                animationSet.setAnimationListener(this.D);
            }
            startAnimation(animationSet);
        } else if (this.f545a != null) {
            int height = (this.f545a.getHeight() * this.d) / this.f545a.getWidth();
            this.u = this.e > height ? (this.e - height) / 2 : 0;
            invalidate();
        }
    }

    public boolean a(Animation.AnimationListener animationListener) {
        int i2;
        float f2;
        if (this.k != null && this.k.length >= 4) {
            int[] iArr = new int[2];
            by.a(this, null, iArr);
            iArr[0] = 0;
            int[] iArr2 = this.k;
            float f3 = ((float) iArr2[2]) / ((float) this.d);
            int i3 = (int) (((float) iArr2[3]) / f3);
            if (this.f545a != null) {
                i2 = (this.f545a.getHeight() * this.d) / this.f545a.getWidth();
            } else if (this.b != null) {
                i2 = (this.b.getIntrinsicHeight() * this.d) / this.b.getIntrinsicWidth();
            } else {
                i2 = i3;
            }
            ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, f3, 1.0f, f3, 0.0f, 0.0f);
            scaleAnimation.setInterpolator(new DecelerateInterpolator());
            scaleAnimation.setDuration(300);
            float f4 = (float) (iArr2[0] - iArr[0]);
            if (this.e > i2) {
                f2 = (float) ((this.e - i2) / 2);
            } else {
                f2 = 0.0f;
            }
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, f4, f2, (float) (iArr2[1] - iArr[1]));
            translateAnimation.initialize(this.d, this.e, this.d, this.e);
            translateAnimation.setInterpolator(new DecelerateInterpolator());
            translateAnimation.setDuration(300);
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.3f);
            alphaAnimation.setInterpolator(new LinearInterpolator());
            alphaAnimation.setStartOffset(150);
            alphaAnimation.setDuration(100);
            AnimationSet animationSet = new AnimationSet(true);
            animationSet.addAnimation(scaleAnimation);
            animationSet.addAnimation(translateAnimation);
            animationSet.addAnimation(alphaAnimation);
            if (animationListener != null) {
                animationSet.setAnimationListener(animationListener);
            }
            startAnimation(animationSet);
            return true;
        } else if (this.f545a == null) {
            return false;
        } else {
            int height = (this.f545a.getHeight() * this.d) / this.f545a.getWidth();
            this.u = this.e > height ? (this.e - height) / 2 : 0;
            invalidate();
            return false;
        }
    }

    public void a(Bitmap bitmap) {
        this.f545a = bitmap;
        this.z = true;
        this.b = null;
        invalidate();
    }

    public void invalidate(int i2, int i3, int i4, int i5) {
        super.invalidate();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        super.onLayout(z2, i2, i3, i4, i5);
        this.d = i4 - i2;
        this.e = i5 - i3;
        if (this.b != null) {
            this.b.setBounds(0, 0, i4 - i2, i5 - i3);
        }
        if (this.j) {
            c();
            this.j = false;
        }
        a(0.0f, 0.0f);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        boolean z2 = false;
        this.H = false;
        this.G = false;
        if (motionEvent.getAction() == 0) {
            this.C = false;
            this.w = false;
            this.F.removeMessages(0);
            if (this.q > 1.0f) {
                this.G = true;
            }
        }
        this.v.inputTouchEvent(motionEvent);
        if (this.I != null) {
            Shieldable shieldable = this.I;
            if (this.G || this.H) {
                z2 = true;
            }
            shieldable.setShielded(z2);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return this.b == drawable || super.verifyDrawable(drawable);
    }

    public void draw(Canvas canvas) {
        int i2;
        int i3;
        int i4;
        int i5;
        boolean z2;
        float f2;
        int i6;
        int i7;
        boolean z3 = false;
        super.draw(canvas);
        int i8 = this.t;
        int i9 = this.u;
        if (this.f545a == null || !this.f545a.isRecycled()) {
            if (this.f545a != null) {
                i3 = this.f545a.getWidth();
                i2 = this.f545a.getHeight();
            } else if (this.b != null) {
                i3 = this.b.getIntrinsicWidth();
                i2 = this.b.getIntrinsicHeight();
            } else {
                i2 = 0;
                i3 = 0;
            }
            float f3 = ((float) this.d) / ((float) (i3 > 0 ? i3 : this.d));
            int i10 = (int) (((float) i2) * f3 * this.q);
            int i11 = (int) (((float) i3) * f3 * this.q);
            if (i10 < this.e) {
                i4 = (this.e - i10) / 2;
            } else if (i10 < this.e || this.u <= 0) {
                i4 = i9;
            } else {
                this.u = 0;
                i4 = 0;
            }
            float f4 = this.q;
            if (this.f != null) {
                boolean transformation = this.f.getTransformation(System.currentTimeMillis(), this.g);
                this.g.getMatrix().getValues(this.h);
                this.c.setAlpha((int) (this.g.getAlpha() * 255.0f));
                float f5 = this.h[0];
                int i12 = (int) this.h[2];
                int i13 = (int) this.h[5];
                if (this.i) {
                    int i14 = (int) (((float) i2) * f3 * f5);
                    if (i14 < this.e) {
                        i13 = (this.e - i14) / 2;
                    }
                    int i15 = (int) (((float) i3) * f3 * f5);
                    if (i12 > 0 || i15 <= this.d) {
                        z2 = transformation;
                        int i16 = i13;
                        i6 = (this.d - i15) / 2;
                        f2 = f5;
                        i4 = i16;
                    }
                }
                f2 = f5;
                i4 = i13;
                i6 = i12;
                z2 = transformation;
            } else {
                if (i8 > 0 || i11 <= this.d) {
                    i5 = (this.d - i11) / 2;
                } else {
                    i5 = i8;
                }
                this.c.setAlpha(255);
                z2 = false;
                int i17 = i5;
                f2 = f4;
                i6 = i17;
            }
            int i18 = (int) (((float) this.d) * f2);
            int i19 = (int) (((float) i2) * f3 * f2);
            canvas.rotate((float) (((((double) (this.r + this.s)) / 3.141592653589793d) * 180.0d) + 360.0d), this.x, this.y);
            if (this.f545a != null) {
                canvas.drawBitmap(this.f545a, new Rect(0, 0, this.f545a.getWidth(), this.f545a.getHeight()), new Rect(i6, i4, i18 + i6, i19 + i4), this.c);
            } else if (this.b != null) {
                this.b.setBounds(i6, i4, i18 + i6, i19 + i4);
                this.b.draw(canvas);
            }
            this.t = i6;
            this.u = i4;
            if (this.l && (i18 > this.d || i19 > this.e)) {
                if (this.n != null) {
                    z3 = this.n.getTransformation(System.currentTimeMillis(), this.m);
                    i7 = (int) (this.m.getAlpha() * 255.0f);
                } else {
                    i7 = 255;
                }
                if (i18 > this.d && this.o != null) {
                    this.o.setBounds(((-i6) * this.d) / i18, this.e - this.o.getIntrinsicHeight(), (((-i6) + this.d) * this.d) / i18, this.e);
                    this.o.setAlpha(i7);
                    this.o.draw(canvas);
                }
                if (i19 > this.e && this.p != null) {
                    this.p.setBounds(this.d - this.p.getIntrinsicWidth(), ((-i4) * this.e) / i19, this.d, (((-i4) + this.e) * this.e) / i19);
                    this.p.setAlpha(i7);
                    this.p.draw(canvas);
                }
                z2 |= z3;
            }
            if (z2) {
                invalidate();
            } else {
                this.f = null;
            }
        }
    }

    public boolean onInvoke(TouchAnalizer.BehaviorType behaviorType, float f2, float f3, int i2) {
        if (!this.w) {
            switch (b.f551a[behaviorType.ordinal()]) {
                case 1:
                    this.F.sendEmptyMessageDelayed(0, 550);
                    break;
                case 2:
                case 3:
                    if (this.z) {
                        if (!this.C) {
                            this.A = f2;
                            this.B = f3;
                            this.C = true;
                        } else {
                            this.G = a(f2 - this.A, f3 - this.B);
                            this.A = f2;
                            this.B = f3;
                            if (!this.G) {
                                this.C = false;
                            }
                        }
                        if (i2 == 2) {
                            this.C = false;
                            break;
                        }
                    }
                    break;
            }
        }
        return true;
    }

    private void d() {
        this.l = true;
        this.F.removeMessages(1);
        this.F.sendEmptyMessageDelayed(1, 1000);
        invalidate();
    }

    /* access modifiers changed from: private */
    public void e() {
        this.n = new AlphaAnimation(1.0f, 0.0f);
        this.n.setDuration(800);
        this.n.start();
        this.n.setAnimationListener(this);
        if (this.m == null) {
            this.m = new Transformation();
        }
        invalidate();
    }

    public void onAnimationEnd(Animation animation) {
        this.l = false;
        this.n = null;
        invalidate();
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    private boolean a(float f2, float f3) {
        int i2;
        int intrinsicHeight;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7;
        boolean z8 = false;
        float sqrt = (float) Math.sqrt((double) ((f2 * f2) + (f3 * f3)));
        if (f2 == 0.0f) {
            f2 = 1.0E-7f;
        }
        float atan = (float) Math.atan((double) (f3 / Math.abs(f2)));
        if (f2 < 0.0f) {
            atan = (float) (3.141592653589793d - ((double) atan));
        }
        float cos = ((float) Math.cos((double) ((atan - this.r) - this.s))) * sqrt;
        float sin = ((float) Math.sin((double) ((atan - this.r) - this.s))) * sqrt;
        int i3 = this.d;
        if (this.f545a != null) {
            float width = ((float) this.d) / ((float) this.f545a.getWidth());
            i2 = (int) (((float) this.d) * this.q);
            intrinsicHeight = (int) (width * ((float) this.f545a.getHeight()) * this.q);
        } else {
            if (this.b != null) {
                float intrinsicWidth = ((float) this.d) / ((float) this.b.getIntrinsicWidth());
                i2 = (int) (((float) this.d) * this.q);
                intrinsicHeight = (int) (intrinsicWidth * ((float) this.b.getIntrinsicHeight()) * this.q);
            }
            return z8;
        }
        if (i2 > this.d) {
            if (((float) this.t) + cos > ((float) (this.d - i2))) {
                this.t = (int) (((float) this.t) + cos);
                z6 = false;
            } else {
                this.t = this.d - i2;
                z6 = true;
            }
            if (this.t > 0) {
                z7 = true;
            } else {
                z7 = false;
            }
            boolean z9 = z7 | z6;
            this.t = this.t > 0 ? 0 : this.t;
            z2 = z9;
        } else {
            this.t = (this.d - i2) / 2;
            z2 = true;
        }
        if (intrinsicHeight > this.e) {
            if (((float) this.u) + sin > ((float) (this.e - intrinsicHeight))) {
                this.u = (int) (((float) this.u) + sin);
                z4 = false;
            } else {
                this.u = this.e - intrinsicHeight;
                z4 = true;
            }
            if (this.u > 0) {
                z5 = true;
            } else {
                z5 = false;
            }
            boolean z10 = z5 | z4;
            this.u = this.u > 0 ? 0 : this.u;
            z3 = z10;
        } else {
            this.u = (this.e - intrinsicHeight) / 2;
            z3 = true;
        }
        if ((!z2 || !z3) && ((!z3 || Math.abs(sin) <= Math.abs(cos) * 2.0f) && (!z2 || Math.abs(cos) <= Math.abs(sin) * 2.0f))) {
            z8 = true;
        }
        if (z8) {
            d();
        }
        invalidate();
        return z8;
    }

    public void a(Drawable drawable) {
        this.o = drawable;
    }

    public void b(Drawable drawable) {
        this.p = drawable;
    }

    public int a() {
        return this.d;
    }

    public int b() {
        return this.e;
    }

    public void b(Animation.AnimationListener animationListener) {
        this.D = animationListener;
    }
}
