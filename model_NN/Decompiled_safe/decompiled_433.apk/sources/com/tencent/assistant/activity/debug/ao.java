package com.tencent.assistant.activity.debug;

import android.content.Intent;
import android.view.View;
import com.tencent.nucleus.socialcontact.tagpage.TagPageTestActivity;

/* compiled from: ProGuard */
class ao implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DActivity f488a;

    ao(DActivity dActivity) {
        this.f488a = dActivity;
    }

    public void onClick(View view) {
        Intent intent = new Intent();
        intent.setClass(this.f488a, TagPageTestActivity.class);
        this.f488a.startActivity(intent);
    }
}
