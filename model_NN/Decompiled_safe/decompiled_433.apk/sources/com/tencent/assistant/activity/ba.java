package com.tencent.assistant.activity;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.model.AppGroupInfo;

/* compiled from: ProGuard */
class ba implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GroupListActivity f412a;

    ba(GroupListActivity groupListActivity) {
        this.f412a = groupListActivity;
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.f412a.z == null || this.f412a.n == null) {
            RelativeLayout unused = this.f412a.z = (RelativeLayout) this.f412a.findViewById(R.id.pop_bar);
            this.f412a.z.setOnClickListener(new bb(this));
            return;
        }
        if (i == 0) {
            this.f412a.z.setVisibility(8);
        }
        int pointToPosition = this.f412a.n.pointToPosition(0, this.f412a.y() - 10);
        int pointToPosition2 = this.f412a.n.pointToPosition(0, 0);
        long j = 0;
        if (pointToPosition2 != -1) {
            long expandableListPosition = this.f412a.n.getExpandableListPosition(pointToPosition2);
            int packedPositionChild = ExpandableListView.getPackedPositionChild(expandableListPosition);
            int packedPositionGroup = ExpandableListView.getPackedPositionGroup(expandableListPosition);
            if (packedPositionChild == -1) {
                View expandChildAt = this.f412a.n.getExpandChildAt(pointToPosition2 - this.f412a.n.getFirstVisiblePosition());
                if (expandChildAt == null) {
                    int unused2 = this.f412a.D = 100;
                } else {
                    int unused3 = this.f412a.D = expandChildAt.getHeight();
                }
            }
            if (this.f412a.D != 0) {
                if (this.f412a.E > 0) {
                    int unused4 = this.f412a.C = packedPositionGroup;
                    AppGroupInfo appGroupInfo = (AppGroupInfo) this.f412a.x.getGroup(packedPositionGroup);
                    if (appGroupInfo != null) {
                        this.f412a.A.setText(appGroupInfo.a());
                    }
                    if (this.f412a.C != packedPositionGroup || !this.f412a.n.isGroupExpanded(packedPositionGroup)) {
                        this.f412a.z.setVisibility(8);
                    } else {
                        this.f412a.z.setVisibility(0);
                    }
                }
                if (this.f412a.E == 0) {
                    this.f412a.z.setVisibility(8);
                }
                j = expandableListPosition;
            } else {
                return;
            }
        }
        if (this.f412a.C == -1) {
            return;
        }
        if (j == 0 && pointToPosition == 0) {
            this.f412a.z.setVisibility(8);
            return;
        }
        int g = this.f412a.y();
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.f412a.z.getLayoutParams();
        marginLayoutParams.topMargin = -(this.f412a.D - g);
        this.f412a.z.setLayoutParams(marginLayoutParams);
    }
}
