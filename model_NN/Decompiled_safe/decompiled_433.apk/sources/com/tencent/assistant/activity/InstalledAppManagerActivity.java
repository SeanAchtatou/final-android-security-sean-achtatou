package com.tencent.assistant.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.adapter.TXTabViewPageAdapter;
import com.tencent.assistant.component.txscrollview.TXTabViewPage;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.StatAppInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.b;
import com.tencent.assistant.utils.e;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.business.u;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.beacon.event.a;
import com.tencent.nucleus.manager.apkuninstall.PreInstallAppListView;
import com.tencent.nucleus.manager.component.UserAppListView;
import com.tencent.nucleus.manager.main.AssistantTabActivity;
import com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
public class InstalledAppManagerActivity extends BaseActivity implements TXTabViewPage.ITXTabViewPageListener, UIEventListener {
    /* access modifiers changed from: private */
    public ImageView A;
    private String B;
    /* access modifiers changed from: private */
    public PopupWindow C = null;
    private boolean D = false;
    /* access modifiers changed from: private */
    public int E = 0;
    private TXTabViewPage F;
    /* access modifiers changed from: private */
    public UserAppListView G;
    /* access modifiers changed from: private */
    public PreInstallAppListView H;
    private TextView I;
    private short J = 0;
    private short K = 0;
    private u L = null;
    /* access modifiers changed from: private */
    public int M = 0;
    /* access modifiers changed from: private */
    public int N = 0;
    /* access modifiers changed from: private */
    public int O = 0;
    private boolean P = false;
    private boolean Q = true;
    private boolean R = false;
    private boolean S = false;
    private boolean T = false;
    private List<StatAppInfo> U = new ArrayList();
    private boolean V = false;
    private boolean W = false;
    /* access modifiers changed from: private */
    public Handler X = new bl(this);
    /* access modifiers changed from: private */
    public PopupWindow Y;
    /* access modifiers changed from: private */
    public ApkResourceManager n = ApkResourceManager.getInstance();
    /* access modifiers changed from: private */
    public List<LocalApkInfo> u = Collections.synchronizedList(new ArrayList());
    /* access modifiers changed from: private */
    public List<LocalApkInfo> v = Collections.synchronizedList(new ArrayList());
    /* access modifiers changed from: private */
    public bn w = new bn(this, null);
    private SecondNavigationTitleViewV5 x;
    private RelativeLayout y;
    /* access modifiers changed from: private */
    public TextView z;

    static /* synthetic */ int n(InstalledAppManagerActivity installedAppManagerActivity) {
        int i = installedAppManagerActivity.O;
        installedAppManagerActivity.O = i + 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            setContentView((int) R.layout.installed_app_manager_layout);
            if (bundle != null) {
                this.M = bundle.getInt("pageIndex");
            } else {
                this.M = getIntent().getIntExtra("pageIndex", 0);
            }
            v();
            u();
            t();
            w();
            AstApp.i().k().addUIEventListener(1024, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC, this);
        } catch (Exception e) {
            this.V = true;
            finish();
        }
    }

    public void t() {
        this.L = new u();
        this.B = getIntent().getStringExtra("activityTitleName");
        TemporaryThreadManager.get().start(new bc(this));
    }

    private void w() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", t.g());
        XLog.d("beacon", "beacon report >> expose_appuninstall. " + hashMap.toString());
        a.a("expose_appuninstall", true, -1, -1, hashMap, true);
    }

    public void u() {
        y();
        x();
    }

    public void v() {
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            this.P = false;
        } else {
            this.P = extras.getBoolean(com.tencent.assistant.a.a.N, false);
        }
        if (this.P) {
            this.E = 1;
        } else {
            this.E = m.a().a("key_app_uninstall_last_sort_type", 3);
        }
    }

    private void x() {
        this.F = new TXTabViewPage(this);
        TXTabViewPageAdapter tXTabViewPageAdapter = new TXTabViewPageAdapter();
        this.I = (TextView) findViewById(R.id.dialog);
        this.G = new UserAppListView(this);
        this.G.a(this.I);
        this.G.a(this.X);
        this.H = new PreInstallAppListView(this);
        this.H.a(this.I);
        this.H.a(this.X);
        String string = getString(R.string.title_user_app);
        String string2 = getString(R.string.title_preinstall_app);
        tXTabViewPageAdapter.addPageItem(string, this.G);
        tXTabViewPageAdapter.addPageItem(string2, this.H);
        this.F.setAdapter(tXTabViewPageAdapter);
        this.F.setListener(this);
        ((RelativeLayout) findViewById(R.id.content_view)).addView(this.F, -1, -1);
        this.F.setPageSelected(this.M);
    }

    private void y() {
        int i;
        int i2;
        this.x = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.x.b(getResources().getString(R.string.soft_admin));
        this.x.a(this);
        this.x.b(this.B);
        this.x.d();
        this.x.c(new bf(this));
        this.y = (RelativeLayout) findViewById(R.id.right_layout_ext);
        this.A = (ImageView) findViewById(R.id.right_img_ext);
        this.z = (TextView) findViewById(R.id.sort_type_name);
        this.y.setOnClickListener(new bg(this));
        if (this.E == 0) {
            i = R.drawable.common_icon_time;
            i2 = R.string.sort_type_by_time;
        } else if (this.E == 1) {
            i = R.drawable.common_icon_kongjian;
            i2 = R.string.sort_type_by_size;
        } else if (this.E == 2) {
            i = R.drawable.common_icon_name;
            i2 = R.string.sort_type_by_name;
        } else {
            i = R.drawable.common_icon_instal;
            i2 = R.string.sort_type_by_install_time;
        }
        this.A.setImageDrawable(getResources().getDrawable(i));
        this.z.setText(i2);
    }

    /* access modifiers changed from: private */
    public void z() {
        if (!this.D) {
            if (this.C == null || !this.C.isShowing()) {
                if (this.C == null) {
                    try {
                        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(this).inflate((int) R.layout.app_manage_sorttype_pop_layout, (ViewGroup) null);
                        LinearLayout linearLayout2 = (LinearLayout) linearLayout.findViewById(R.id.ll_sort_by_time);
                        ImageView imageView = (ImageView) linearLayout.findViewById(R.id.iv_sort_by_time);
                        TextView textView = (TextView) linearLayout.findViewById(R.id.tv_sort_by_time);
                        LinearLayout linearLayout3 = (LinearLayout) linearLayout.findViewById(R.id.ll_sort_by_size);
                        ImageView imageView2 = (ImageView) linearLayout.findViewById(R.id.iv_sort_by_size);
                        TextView textView2 = (TextView) linearLayout.findViewById(R.id.tv_sort_by_size);
                        LinearLayout linearLayout4 = (LinearLayout) linearLayout.findViewById(R.id.ll_sort_by_name);
                        ImageView imageView3 = (ImageView) linearLayout.findViewById(R.id.iv_sort_by_name);
                        TextView textView3 = (TextView) linearLayout.findViewById(R.id.tv_sort_by_name);
                        LinearLayout linearLayout5 = (LinearLayout) linearLayout.findViewById(R.id.ll_sort_by_install_time);
                        ImageView imageView4 = (ImageView) linearLayout.findViewById(R.id.iv_sort_by_install_time);
                        TextView textView4 = (TextView) linearLayout.findViewById(R.id.tv_sort_by_install_time);
                        if (this.E == 0) {
                            this.A.setImageDrawable(getResources().getDrawable(R.drawable.common_icon_time));
                            this.z.setText((int) R.string.sort_type_by_time);
                            imageView.setSelected(true);
                            textView.setTextColor(getResources().getColor(R.color.appadmin_sort_text_selected));
                        } else if (this.E == 1) {
                            this.A.setImageDrawable(getResources().getDrawable(R.drawable.common_icon_kongjian));
                            this.z.setText((int) R.string.sort_type_by_size);
                            imageView2.setSelected(true);
                            textView2.setTextColor(getResources().getColor(R.color.appadmin_sort_text_selected));
                        } else if (this.E == 2) {
                            this.A.setImageDrawable(getResources().getDrawable(R.drawable.common_icon_name));
                            this.z.setText((int) R.string.sort_type_by_name);
                            imageView3.setSelected(true);
                            textView3.setTextColor(getResources().getColor(R.color.appadmin_sort_text_selected));
                        } else {
                            this.A.setImageDrawable(getResources().getDrawable(R.drawable.common_icon_instal));
                            this.z.setText((int) R.string.sort_type_by_install_time);
                            imageView4.setSelected(true);
                            textView4.setTextColor(getResources().getColor(R.color.appadmin_sort_text_selected));
                        }
                        linearLayout2.setOnClickListener(new bh(this, imageView, imageView2, imageView3, imageView4, textView, textView2, textView3, textView4));
                        linearLayout3.setOnClickListener(new bi(this, imageView, imageView2, imageView3, imageView4, textView, textView2, textView3, textView4));
                        linearLayout4.setOnClickListener(new bj(this, imageView, imageView2, imageView3, imageView4, textView, textView2, textView3, textView4));
                        linearLayout5.setOnClickListener(new bk(this, imageView, imageView2, imageView3, imageView4, textView, textView2, textView3, textView4));
                        this.C = new PopupWindow(linearLayout, -2, -2);
                    } catch (Throwable th) {
                        com.tencent.assistant.manager.t.a().b();
                        return;
                    }
                }
                if (this.C != null) {
                    this.C.showAsDropDown(this.x, this.x.getWidth() - this.C.getWidth(), 0);
                    return;
                }
                return;
            }
            this.C.dismiss();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.InstalledAppManagerActivity.a(boolean, java.lang.String, java.lang.String):void
     arg types: [int, java.lang.String, java.lang.String]
     candidates:
      com.tencent.assistant.activity.InstalledAppManagerActivity.a(com.tencent.assistant.activity.InstalledAppManagerActivity, com.tencent.assistant.localres.model.LocalApkInfo, int):void
      com.tencent.assistant.activity.BaseActivity.a(com.tencent.assistantv2.st.page.STPageInfo, java.lang.String, java.lang.String):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, boolean, boolean):android.support.v4.app.z
      android.support.v4.app.FragmentActivity.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.tencent.assistant.activity.InstalledAppManagerActivity.a(boolean, java.lang.String, java.lang.String):void */
    /* access modifiers changed from: private */
    public void a(LocalApkInfo localApkInfo, int i) {
        if (localApkInfo != null) {
            if (i == 1) {
                if (e.b(localApkInfo.flags)) {
                    localApkInfo.mIsSelect = this.H.d().contains(localApkInfo);
                    this.v.add(localApkInfo);
                    return;
                }
                localApkInfo.mIsSelect = this.G.d().contains(localApkInfo);
                this.u.add(localApkInfo);
            } else if (i == 2 || i == 5) {
                ArrayList arrayList = new ArrayList();
                if (e.b(localApkInfo.flags)) {
                    for (LocalApkInfo next : this.v) {
                        if (!localApkInfo.mPackageName.equals(next.mPackageName)) {
                            arrayList.add(next);
                        }
                    }
                    this.v.clear();
                    this.v.addAll(arrayList);
                    this.S = this.H.a(localApkInfo, null, true);
                    a(true, localApkInfo.mPackageName, "system");
                } else {
                    for (LocalApkInfo next2 : this.u) {
                        if (!localApkInfo.mPackageName.equals(next2.mPackageName)) {
                            arrayList.add(next2);
                        }
                    }
                    this.u.clear();
                    this.u.addAll(arrayList);
                    this.R = this.G.b(localApkInfo);
                    this.T = false;
                    a(true, localApkInfo.mPackageName, "personal");
                }
                StatAppInfo statAppInfo = new StatAppInfo();
                statAppInfo.f1540a = localApkInfo.mPackageName;
                statAppInfo.c = localApkInfo.mVersionCode;
                statAppInfo.b = localApkInfo.signature;
                this.U.add(statAppInfo);
                this.K = (short) (this.K + 1);
            } else {
                if (i == 4) {
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    /* access modifiers changed from: private */
    public void A() {
        ArrayList arrayList = new ArrayList(this.u);
        ArrayList arrayList2 = new ArrayList(this.v);
        if (this.E == 0) {
            com.tencent.assistant.localres.m.c(arrayList);
            com.tencent.assistant.localres.m.c(arrayList2);
            this.G.a();
            this.H.a();
        } else if (this.E == 1) {
            com.tencent.assistant.localres.m.a(arrayList);
            com.tencent.assistant.localres.m.a(arrayList2);
            this.G.a();
            this.H.a();
        } else if (this.E == 2) {
            com.tencent.assistant.localres.m.g(arrayList);
            com.tencent.assistant.localres.m.g(arrayList2);
            this.G.b();
            this.H.b();
        } else if (this.E == 3) {
            com.tencent.assistant.localres.m.b(arrayList);
            com.tencent.assistant.localres.m.b(arrayList2);
            this.G.a();
            this.H.a();
        }
        if ((this.M != 0 || !arrayList.isEmpty()) && (this.M != 1 || !arrayList2.isEmpty())) {
            this.y.setVisibility(0);
        } else {
            this.y.setVisibility(8);
        }
        this.G.a(arrayList, this.E);
        this.H.a(arrayList2, this.E);
        if (!m.a().a("key_has_report_preinstall_applist", false) && !this.v.isEmpty()) {
            m.a().b("key_has_report_preinstall_applist", (Object) true);
            TemporaryThreadManager.get().start(new bm(this));
        }
    }

    /* access modifiers changed from: private */
    public void a(LocalApkInfo localApkInfo) {
        if (e.b(localApkInfo.flags)) {
            this.H.a(localApkInfo);
        } else {
            this.G.a(localApkInfo);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!this.V) {
            this.L.a(this.K, this.J, (ArrayList) this.U);
            ApkResourceManager.getInstance().unRegisterApkResCallback(this.w);
            this.X.removeMessages(10702);
            this.X.removeMessages(10703);
            this.X.removeMessages(10701);
            this.X.removeMessages(10704);
            this.X.removeMessages(10705);
            this.X.removeMessages(10706);
            AstApp.i().k().removeUIEventListener(1024, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC, this);
            b.a();
            if (this.P) {
                com.tencent.pangu.utils.installuninstall.a.a().c();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.V) {
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!this.V && this.x != null) {
            this.x.m();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.V) {
            if (this.H != null) {
                if (m.a().n() != AppConst.ROOT_STATUS.ROOTED) {
                    m.a().a(t.n());
                }
                if (m.a().i()) {
                    m.a().a(AppConst.ROOT_STATUS.ROOTED);
                }
                if (!this.S) {
                    this.H.f();
                }
            }
            if (this.G != null && (!this.R || this.T)) {
                this.G.f();
            }
            if (this.x != null) {
                this.x.l();
            }
            if (!this.u.isEmpty()) {
                A();
            }
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (4 != i) {
            return super.onKeyDown(i, keyEvent);
        }
        B();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void B() {
        if (!this.s) {
            finish();
            return;
        }
        Intent intent = new Intent(this, AssistantTabActivity.class);
        intent.setFlags(67108864);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
        intent.putExtra(com.tencent.assistant.a.a.G, true);
        startActivity(intent);
        this.s = false;
        finish();
        XLog.i("miles", "InstalledAppManagerActivity >> key back finish");
    }

    /* access modifiers changed from: private */
    public synchronized void a(List<LocalApkInfo> list) {
        if (list != null) {
            if (!(this.u == null || this.v == null)) {
                this.u.clear();
                this.v.clear();
                if (list != null) {
                    String c = e.c();
                    for (LocalApkInfo next : list) {
                        if (!"com.tencent.android.qqdownloader".equals(next.mPackageName) && next.mIsEnabled) {
                            if (c != null && c.equals(next.mPackageName)) {
                                long currentTimeMillis = System.currentTimeMillis();
                                next.mLastLaunchTime = currentTimeMillis;
                                next.mFakeLastLaunchTime = currentTimeMillis;
                            }
                            if (this.Q) {
                                next.mIsSelect = false;
                            }
                            if ((next.flags & 1) == 0 || (next.flags & 128) != 0) {
                                this.u.add(next);
                            } else if (b(next)) {
                                this.v.add(next);
                            }
                        }
                    }
                    this.Q = false;
                }
            }
        }
    }

    private boolean b(LocalApkInfo localApkInfo) {
        if (localApkInfo.occupySize < 524288 || localApkInfo.mUid == 1000 || localApkInfo.mUid == 1001) {
            return false;
        }
        return true;
    }

    private void C() {
        if (this.Y == null) {
            View inflate = getLayoutInflater().inflate((int) R.layout.inter_manage_interface_layout_v6, (ViewGroup) null);
            this.Y = new PopupWindow(inflate, -1, -2);
            this.Y.setOutsideTouchable(true);
            inflate.findViewById(R.id.inter_manage_layout).setVisibility(0);
            TextView textView = (TextView) inflate.findViewById(R.id.footer_title_txt);
            ((ImageView) inflate.findViewById(R.id.icon_xiaobao)).setImageResource(R.drawable.baomih);
            textView.setText(Html.fromHtml("<html ><font color='#ffffff'>多款应用卸载成功，桌面图标空了一块，</font><font color='#00adff'>去整理吧></font></html>"));
            textView.setOnClickListener(new bd(this));
        }
        this.Y.showAsDropDown(getWindow().getDecorView(), 0, 0 - getApplicationContext().getResources().getDimensionPixelSize(R.dimen.uc_pop_menu_width));
        new Handler().postDelayed(new be(this), 5000);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.InstalledAppManagerActivity.a(boolean, java.lang.String, java.lang.String):void
     arg types: [int, java.lang.String, java.lang.String]
     candidates:
      com.tencent.assistant.activity.InstalledAppManagerActivity.a(com.tencent.assistant.activity.InstalledAppManagerActivity, com.tencent.assistant.localres.model.LocalApkInfo, int):void
      com.tencent.assistant.activity.BaseActivity.a(com.tencent.assistantv2.st.page.STPageInfo, java.lang.String, java.lang.String):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, boolean, boolean):android.support.v4.app.z
      android.support.v4.app.FragmentActivity.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.tencent.assistant.activity.InstalledAppManagerActivity.a(boolean, java.lang.String, java.lang.String):void */
    public void handleUIEvent(Message message) {
        LocalApkInfo localApkInfo;
        LocalApkInfo localApkInfo2;
        boolean z2 = false;
        switch (message.what) {
            case 1024:
                if (message.obj != null && (message.obj instanceof String)) {
                    this.T = true;
                    XLog.d("miles", "InstalledAppManageActivity handleUIEvent. event = " + message.what + ", pkgname = " + ((String) message.obj));
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START /*1025*/:
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC /*1026*/:
            case 1027:
            default:
                return;
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START /*1028*/:
                XLog.d("miles", "InstalledAppManageActivity handleUIEvent. event = " + message.what);
                if (message.obj != null && (message.obj instanceof InstallUninstallTaskBean) && (localApkInfo2 = ApkResourceManager.getInstance().getLocalApkInfo(((InstallUninstallTaskBean) message.obj).packageName)) != null) {
                    if (e.b(localApkInfo2.flags)) {
                        this.S = true;
                        return;
                    } else {
                        this.R = true;
                        return;
                    }
                } else {
                    return;
                }
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC /*1029*/:
                XLog.d("miles", "InstalledAppManageActivity handleUIEvent. event = " + message.what);
                if (!(message.obj == null || !(message.obj instanceof InstallUninstallTaskBean) || (localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(((InstallUninstallTaskBean) message.obj).packageName)) == null)) {
                    if (e.b(localApkInfo.flags)) {
                        this.S = this.H.e() != null;
                    } else {
                        if (this.G.e() != null) {
                            z2 = true;
                        }
                        this.R = z2;
                    }
                }
                if (this.G != null) {
                    this.G.c();
                }
                if (this.H != null) {
                    this.H.c();
                }
                if (!this.W && !this.S && !this.R && !this.S && this.K > 4 && !e.a("com.tencent.qlauncher.lite") && !e.a("com.tencent.qlauncher.lite")) {
                    C();
                    this.W = true;
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL /*1030*/:
                if (message.obj != null && (message.obj instanceof InstallUninstallTaskBean)) {
                    InstallUninstallTaskBean installUninstallTaskBean = (InstallUninstallTaskBean) message.obj;
                    XLog.d("miles", "handleUIEvent. appName = " + installUninstallTaskBean.appName + ", pkg = " + installUninstallTaskBean.packageName);
                    try {
                        Toast.makeText(this, installUninstallTaskBean.appName + "卸载失败", 0).show();
                    } catch (Exception e) {
                    }
                    LocalApkInfo localApkInfo3 = ApkResourceManager.getInstance().getLocalApkInfo(installUninstallTaskBean.packageName);
                    if (localApkInfo3 == null || !e.b(localApkInfo3.flags)) {
                        a(false, installUninstallTaskBean.packageName, "personal");
                    } else {
                        this.S = this.H.a(null, installUninstallTaskBean.packageName, false);
                        a(false, installUninstallTaskBean.packageName, "system");
                    }
                }
                XLog.d("miles", "InstalledAppManageActivity handleUIEvent. event = " + message.what);
                if (this.G != null) {
                    this.G.c();
                }
                if (this.H != null) {
                    this.H.c();
                    return;
                }
                return;
        }
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (motionEvent == null) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                if (this.C != null && this.C.isShowing()) {
                    this.C.dismiss();
                    this.D = true;
                    break;
                } else {
                    this.D = false;
                    break;
                }
                break;
        }
        try {
            return super.dispatchTouchEvent(motionEvent);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void onTxTabViewPageSelected(int i) {
        XLog.d("miles", "Tab " + i + " selected.");
        this.M = i;
        b(i);
        if ((i != 0 || !this.u.isEmpty()) && (i != 1 || !this.v.isEmpty())) {
            this.y.setVisibility(0);
        } else {
            this.y.setVisibility(8);
        }
    }

    public void onTxTabViewPageWillSelect(int i) {
    }

    private void b(int i) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, 100);
        if (i == 0) {
            buildSTInfo.scene = STConst.ST_PAGE_USER_APP_UNINSTALL;
        } else if (i == 1) {
            if (D()) {
                buildSTInfo.scene = STConst.ST_PAGE_PRE_APP_UNINSTALL_ROOT;
            } else {
                buildSTInfo.scene = STConst.ST_PAGE_PRE_APP_UNINSTALL_NO_ROOT;
            }
        }
        com.tencent.assistantv2.st.b.b.getInstance().exposure(buildSTInfo);
    }

    private boolean D() {
        return com.tencent.nucleus.manager.root.e.a().c() || m.a().n() == AppConst.ROOT_STATUS.ROOTED;
    }

    private void a(boolean z2, String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", z2 ? "success" : "fail");
        hashMap.put("B2", str);
        hashMap.put("B3", str2);
        hashMap.put("B4", t.v());
        hashMap.put("B5", Build.VERSION.RELEASE);
        hashMap.put("B6", Global.getPhoneGuidAndGen());
        hashMap.put("B7", Global.getQUAForBeacon());
        hashMap.put("B8", t.g());
        a.a("AppUninstallResult", z2, -1, -1, hashMap, false);
        XLog.d("beacon", "beacon report >> event: AppUninstallResult, params : " + hashMap.toString());
    }
}
