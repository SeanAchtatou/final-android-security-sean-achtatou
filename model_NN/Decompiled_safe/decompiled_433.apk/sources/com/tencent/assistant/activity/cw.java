package com.tencent.assistant.activity;

import android.view.View;
import com.tencent.assistant.component.SecondNavigationTitleView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class cw extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SplashActivity f461a;

    cw(SplashActivity splashActivity) {
        this.f461a = splashActivity;
    }

    public void onTMAClick(View view) {
        this.f461a.c();
    }

    public STInfoV2 getStInfo() {
        return new STInfoV2(STConst.ST_PAGE_SPLASH_URL, SecondNavigationTitleView.TMA_ST_NAVBAR_SEARCH_TAG, 2000, STConst.ST_DEFAULT_SLOT, 200);
    }
}
