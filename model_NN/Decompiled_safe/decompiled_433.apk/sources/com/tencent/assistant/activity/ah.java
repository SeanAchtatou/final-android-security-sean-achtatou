package com.tencent.assistant.activity;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.nucleus.socialcontact.login.j;

/* compiled from: ProGuard */
class ah extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f393a;
    final /* synthetic */ int b;
    final /* synthetic */ AppBackupActivity c;

    ah(AppBackupActivity appBackupActivity, int i, int i2) {
        this.c = appBackupActivity;
        this.f393a = i;
        this.b = i2;
    }

    public void onRightBtnClick() {
        if (this.f393a == 1) {
            this.c.b(3);
            l.a(new STInfoV2(STConst.ST_PAGE_APP_BACKUP_SWITCHDEVICE, "03_001", 0, STConst.ST_DEFAULT_SLOT, 100));
        } else if (this.f393a == 2) {
            if (this.c.x == null || this.c.x.a() == null || this.c.x.a().isEmpty()) {
                this.c.F();
                this.c.E();
            } else {
                this.c.b(4);
            }
            l.a(new STInfoV2(STConst.ST_PAGE_APP_BACKUP_REFRESHDEVICE, "03_001", 0, STConst.ST_DEFAULT_SLOT, 100));
        }
    }

    public void onLeftBtnClick() {
        if (this.b == 2 && j.a().j()) {
            if (this.c.w.d()) {
                this.c.H();
            } else {
                this.c.G();
            }
        }
        if (this.f393a == 1) {
            l.a(new STInfoV2(STConst.ST_PAGE_APP_BACKUP_SWITCHDEVICE, "04_001", 0, STConst.ST_DEFAULT_SLOT, 100));
        } else {
            l.a(new STInfoV2(STConst.ST_PAGE_APP_BACKUP_REFRESHDEVICE, "04_001", 0, STConst.ST_DEFAULT_SLOT, 100));
        }
    }

    public void onCancell() {
    }
}
