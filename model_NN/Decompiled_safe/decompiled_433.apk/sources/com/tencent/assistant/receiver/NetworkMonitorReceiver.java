package com.tencent.assistant.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import com.tencent.assistant.manager.t;

/* compiled from: ProGuard */
public class NetworkMonitorReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            t.a().a((NetworkInfo) intent.getParcelableExtra("networkInfo"));
        }
    }
}
