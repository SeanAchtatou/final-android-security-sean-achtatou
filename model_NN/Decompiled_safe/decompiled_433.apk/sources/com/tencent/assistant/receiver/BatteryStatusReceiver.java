package com.tencent.assistant.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.l;

/* compiled from: ProGuard */
public class BatteryStatusReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        XLog.d("BatteryStatusReceiver", "onReceive action = " + action);
        if (!action.equals("android.intent.action.ACTION_POWER_CONNECTED")) {
            if (action.equals("android.intent.action.BATTERY_CHANGED")) {
                TemporaryThreadManager.get().start(new a(this, intent));
            } else if (action.equals("android.intent.action.ACTION_POWER_DISCONNECTED")) {
                l.a(false);
            }
        }
    }
}
