package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.b.e;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.STPageInfo;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public abstract class NormalSmartcardBaseItem extends ISmartcard {
    protected View.OnClickListener j = new n(this);
    protected View.OnClickListener k = new o(this);

    public NormalSmartcardBaseItem(Context context) {
        super(context);
    }

    public NormalSmartcardBaseItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NormalSmartcardBaseItem(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
        a();
    }

    /* access modifiers changed from: protected */
    public SimpleAppModel c() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void d() {
        if (this.e != null) {
            this.e.a(this.d.j, this.d.k);
        }
        SimpleAppModel c = c();
        long j2 = -1;
        if (c != null) {
            j2 = c.f938a;
        }
        if (this.d != null) {
            a(STConst.ST_DEFAULT_SLOT, 100, this.d.s, j2);
        }
    }

    /* access modifiers changed from: protected */
    public void a(SimpleAppModel simpleAppModel, STInfoV2 sTInfoV2) {
        if (this.e != null) {
            this.e.a(this.d.j, this.d.k, simpleAppModel);
        }
        if (this.h == null) {
            this.h = new e();
        }
        this.h.a(sTInfoV2);
    }

    /* access modifiers changed from: protected */
    public int c(int i) {
        int i2 = 0;
        switch (i) {
            case STConst.ST_PAGE_COMPETITIVE /*2001*/:
                i2 = 1;
                break;
            case 200501:
                i2 = 3;
                break;
            case STConst.ST_PAGE_GAME_POPULAR /*200601*/:
                i2 = 2;
                break;
            case STConst.ST_PAGE_GAME_RANK_SIX /*2006021*/:
                i2 = 5;
                break;
            case STConst.ST_PAGE_GAME_RANK_THREE /*2006022*/:
                i2 = 4;
                break;
        }
        if (this.d == null) {
            return STConst.ST_PAGE_SMARTCARD;
        }
        return (i2 * 100) + 20290000 + this.d.j;
    }

    /* access modifiers changed from: protected */
    public String e() {
        if (this.d == null) {
            return STConst.ST_DEFAULT_SLOT;
        }
        return this.d.j + Constants.STR_EMPTY;
    }

    /* access modifiers changed from: protected */
    public String d(int i) {
        return this.d.k + "||" + this.d.j + "|" + i;
    }

    /* access modifiers changed from: protected */
    public void a(String str, int i, byte[] bArr, long j2) {
        STInfoV2 b = b(str, i, bArr, j2);
        if (b != null) {
            l.a(b);
        }
    }

    /* access modifiers changed from: protected */
    public STInfoV2 b(String str, int i, byte[] bArr, long j2) {
        STInfoV2 a2 = a(str, i);
        if (a2 != null) {
            if (this.d != null) {
                a2.pushInfo = d(0);
            }
            if (j2 > 0) {
                a2.appId = j2;
            }
            if (a2.recommendId == null || a2.recommendId.length == 0) {
                a2.recommendId = bArr;
            }
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public STInfoV2 a(String str, int i) {
        STPageInfo sTPageInfo;
        String str2;
        if (this.f1693a instanceof BaseActivity) {
            sTPageInfo = ((BaseActivity) this.f1693a).n();
        } else {
            sTPageInfo = null;
        }
        if (sTPageInfo == null) {
            return null;
        }
        String str3 = sTPageInfo.b;
        if (this.d.u >= 0) {
            str2 = (this.d.u < 9 ? "0" : Constants.STR_EMPTY) + String.valueOf(this.d.u + 1);
        } else {
            str2 = str3;
        }
        STInfoV2 sTInfoV2 = new STInfoV2(c(sTPageInfo.f2060a), str, sTPageInfo.f2060a, a.b(str2, STConst.ST_STATUS_DEFAULT), i);
        sTInfoV2.pushInfo = d(0);
        sTInfoV2.updateWithSimpleAppModel(c());
        return sTInfoV2;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.f) {
            this.f = true;
            d();
        }
    }

    public int a(Context context) {
        STPageInfo n;
        if (!(context instanceof BaseActivity) || (n = ((BaseActivity) context).n()) == null) {
            return 0;
        }
        return n.f2060a;
    }
}
