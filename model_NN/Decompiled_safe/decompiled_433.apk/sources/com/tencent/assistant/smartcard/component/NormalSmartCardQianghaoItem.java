package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Process;
import android.util.AttributeSet;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.manager.o;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.s;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class NormalSmartCardQianghaoItem extends NormalSmartcardBaseItem {
    s i;
    private TextView l;
    private TXImageView m;
    private TextView n;
    private ListItemInfoView o;
    private TextView p;
    private DownloadButton q;
    private boolean r;

    public NormalSmartCardQianghaoItem(Context context) {
        this(context, null);
    }

    public NormalSmartCardQianghaoItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.i = null;
        this.r = false;
    }

    public NormalSmartCardQianghaoItem(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
        this.i = null;
        this.r = false;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.f) {
            this.f = true;
            d();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        try {
            this.b.inflate((int) R.layout.smartcard_qianghao, this);
            this.r = false;
            this.l = (TextView) findViewById(R.id.qianghao_num);
            this.m = (TXImageView) findViewById(R.id.iconimg);
            this.n = (TextView) findViewById(R.id.appname);
            this.q = (DownloadButton) findViewById(R.id.downloadbtn);
            this.o = (ListItemInfoView) findViewById(R.id.iteminfo);
            this.o.a(ListItemInfoView.InfoType.CATEGORY_SIZE);
            this.p = (TextView) findViewById(R.id.desc);
            f();
        } catch (Throwable th) {
            t.a().b();
            this.r = true;
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (this.r) {
            a();
        } else {
            f();
        }
    }

    private void f() {
        if (!this.r) {
            if (this.d instanceof s) {
                this.i = (s) this.d;
            }
            if (this.i != null && this.i.b != null) {
                this.q.a(this.i.b);
                STInfoV2 a2 = a("03_001", 200);
                if (a2 != null) {
                    a2.updateWithSimpleAppModel(this.i.b);
                }
                if (com.tencent.pangu.component.appdetail.process.s.a(this.i.b)) {
                    this.q.setClickable(false);
                } else {
                    this.q.setClickable(true);
                    this.q.a(a2);
                }
                setOnClickListener(new h(this, this.i.i, a2));
                this.m.updateImageView(this.i.b.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                this.n.setText(this.i.b.d);
                this.o.a(this.i.b);
                if (o.a().b(this.i.b.f938a)) {
                    this.l.setText(a(String.valueOf(this.i.c + Process.LAST_APPLICATION_UID + 1)));
                } else {
                    this.l.setText(a(String.valueOf(this.i.c + Process.LAST_APPLICATION_UID)));
                }
                this.p.setText(this.i.d);
            }
        }
    }

    private String a(String str) {
        if (str == null || str.length() < 2) {
            return str;
        }
        String str2 = Constants.STR_EMPTY;
        for (int i2 = 0; i2 < str.length(); i2++) {
            str2 = str2 + str.charAt(i2);
            if (i2 < str.length() - 1) {
                str2 = str2 + " ";
            }
        }
        return str2;
    }

    /* access modifiers changed from: protected */
    public SimpleAppModel c() {
        if (this.i != null) {
            return this.i.b;
        }
        return null;
    }
}
