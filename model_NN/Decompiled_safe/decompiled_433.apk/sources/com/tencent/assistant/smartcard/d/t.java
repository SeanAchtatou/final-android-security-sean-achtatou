package com.tencent.assistant.smartcard.d;

import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.protocol.jce.SmartCardAppList;
import com.tencent.pangu.manager.SelfUpdateManager;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class t extends n {

    /* renamed from: a  reason: collision with root package name */
    public SelfUpdateManager.SelfUpdateInfo f1763a;
    public LocalApkInfo b;

    public void a(SmartCardAppList smartCardAppList, int i) {
        this.j = i;
        if (smartCardAppList != null) {
            this.l = smartCardAppList.f1499a;
            this.n = smartCardAppList.b;
            this.o = smartCardAppList.c;
            this.p = smartCardAppList.d;
        }
    }

    public List<Long> d() {
        ArrayList arrayList = new ArrayList();
        if (this.f1763a != null) {
            arrayList.add(Long.valueOf(this.f1763a.f3781a));
        }
        return arrayList;
    }
}
