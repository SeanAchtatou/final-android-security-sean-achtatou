package com.tencent.assistant.smartcard.d;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.SmartCardCommon;
import com.tencent.assistant.protocol.jce.SmartCardTitle;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public abstract class a extends n {

    /* renamed from: a  reason: collision with root package name */
    protected byte f1744a;
    protected String b;
    protected int c;
    protected int d;

    public abstract List<SimpleAppModel> a();

    public abstract boolean a(byte b2, JceStruct jceStruct);

    public void b() {
    }

    public x c() {
        if (this.c <= 0 || this.d <= 0) {
            return null;
        }
        x xVar = new x();
        xVar.f = this.k;
        xVar.e = this.j;
        xVar.f1767a = this.c;
        xVar.b = this.d;
        return xVar;
    }

    public List<Long> d() {
        List<SimpleAppModel> a2 = a();
        if (a2 == null || a2.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(a2.size());
        for (SimpleAppModel simpleAppModel : a2) {
            arrayList.add(Long.valueOf(simpleAppModel.f938a));
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void a(SmartCardTitle smartCardTitle) {
        if (smartCardTitle != null) {
            this.f1744a = smartCardTitle.f1527a;
            this.l = smartCardTitle.b;
            this.p = smartCardTitle.c;
            this.o = smartCardTitle.d;
            this.m = smartCardTitle.e;
        }
    }

    /* access modifiers changed from: protected */
    public void a(byte b2, SmartCardCommon smartCardCommon) {
        if (smartCardCommon != null) {
            this.b = smartCardCommon.c;
            this.k = smartCardCommon.d;
            this.c = smartCardCommon.f1503a;
            this.d = smartCardCommon.b;
            this.j = b2;
        }
    }

    public String e() {
        return this.b;
    }

    public byte f() {
        return this.f1744a;
    }
}
