package com.tencent.assistant.smartcard.c;

import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.r;
import com.tencent.assistant.smartcard.d.v;
import com.tencent.assistant.smartcard.d.w;
import com.tencent.assistant.smartcard.d.x;
import com.tencent.assistant.utils.bo;
import java.util.List;

/* compiled from: ProGuard */
public class l extends z {
    public boolean a(n nVar, List<Long> list) {
        if (nVar == null || !(nVar instanceof r)) {
            return false;
        }
        r rVar = (r) nVar;
        return a(rVar, (w) this.f1692a.get(Integer.valueOf(rVar.l())), (x) this.b.get(Integer.valueOf(rVar.l())), list);
    }

    /* access modifiers changed from: protected */
    public boolean a(r rVar, w wVar, x xVar, List<Long> list) {
        if (xVar == null) {
            return false;
        }
        rVar.a(list);
        if (rVar.d == null || System.currentTimeMillis() / 1000 > xVar.h) {
            return false;
        }
        return true;
    }

    public void a(v vVar) {
        w wVar;
        int i;
        if (vVar != null) {
            w wVar2 = (w) this.f1692a.get(Integer.valueOf(vVar.a()));
            if (wVar2 == null) {
                w wVar3 = new w();
                wVar3.e = vVar.f1765a;
                wVar3.f = vVar.b;
                wVar = wVar3;
            } else {
                wVar = wVar2;
            }
            if (vVar.d) {
                wVar.d = true;
            }
            if (vVar.c) {
                if (bo.b(vVar.e * 1000)) {
                    wVar.f1766a++;
                }
                x xVar = (x) this.b.get(Integer.valueOf(vVar.a()));
                if (xVar != null) {
                    i = xVar.i;
                } else {
                    i = 7;
                }
                if (bo.a(vVar.e * 1000, i)) {
                    wVar.b++;
                }
                wVar.c++;
            }
            this.f1692a.put(Integer.valueOf(wVar.a()), wVar);
        }
    }

    public void a(x xVar) {
        this.b.put(Integer.valueOf(xVar.a()), xVar);
    }

    public void a(n nVar) {
        if (nVar != null && nVar.j == 23) {
            r rVar = (r) nVar;
            x xVar = (x) this.b.get(Integer.valueOf(rVar.l()));
            if (xVar != null) {
                rVar.q = xVar.d;
            }
        }
    }
}
