package com.tencent.assistant.smartcard.c;

import com.tencent.assistant.smartcard.d.h;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.u;
import com.tencent.assistant.smartcard.d.v;
import com.tencent.assistant.smartcard.d.w;
import com.tencent.assistant.smartcard.d.x;
import com.tencent.assistant.utils.bo;
import java.util.List;

/* compiled from: ProGuard */
public class j extends z {
    public boolean a(n nVar, List<Long> list) {
        if (nVar == null || !(nVar instanceof h)) {
            return false;
        }
        h hVar = (h) nVar;
        return a(hVar, (w) this.f1692a.get(Integer.valueOf(hVar.j)), (x) this.b.get(Integer.valueOf(hVar.j)), (List) this.c.get(Integer.valueOf(nVar.j)));
    }

    private boolean a(h hVar, w wVar, x xVar, List<u> list) {
        if (xVar == null) {
            return false;
        }
        if (wVar == null) {
            wVar = new w();
            wVar.f = hVar.k;
            wVar.e = hVar.j;
            this.f1692a.put(Integer.valueOf(wVar.e), wVar);
        }
        if (hVar.c == null || hVar.c.size() < xVar.g) {
            a(hVar.t, hVar.k + "||" + hVar.j + "|" + 3, hVar.j);
            return false;
        } else if (wVar.b >= xVar.b) {
            a(hVar.t, hVar.k + "||" + hVar.j + "|" + 1, hVar.j);
            return false;
        } else if (wVar.f1766a < xVar.f1767a) {
            return true;
        } else {
            a(hVar.t, hVar.k + "||" + hVar.j + "|" + 2, hVar.j);
            return false;
        }
    }

    public void a(v vVar) {
        w wVar;
        int i;
        if (vVar != null) {
            w wVar2 = (w) this.f1692a.get(Integer.valueOf(vVar.f1765a));
            if (wVar2 == null) {
                w wVar3 = new w();
                wVar3.e = vVar.f1765a;
                wVar3.f = vVar.b;
                wVar = wVar3;
            } else {
                wVar = wVar2;
            }
            if (vVar.d) {
                wVar.d = true;
            }
            if (vVar.c) {
                if (bo.b(vVar.e * 1000)) {
                    wVar.f1766a++;
                }
                x xVar = (x) this.b.get(Integer.valueOf(vVar.f1765a));
                if (xVar != null) {
                    i = xVar.i;
                } else {
                    i = 7;
                }
                if (bo.a(vVar.e * 1000, i)) {
                    wVar.b++;
                }
                wVar.c++;
            }
            this.f1692a.put(Integer.valueOf(wVar.e), wVar);
        }
    }

    public void a(x xVar) {
        this.b.put(Integer.valueOf(xVar.e), xVar);
    }

    public void a(n nVar) {
        if (nVar != null && nVar.j == 1) {
            h hVar = (h) nVar;
            x xVar = (x) this.b.get(Integer.valueOf(hVar.j));
            if (xVar != null) {
                hVar.q = xVar.d;
                hVar.f1754a = a(hVar, xVar.g, xVar.j);
            }
        }
    }

    private int a(h hVar, int i, int i2) {
        if (!(hVar == null || hVar.c == null)) {
            int size = hVar.c.size();
            if (size >= i && size < i2) {
                return i;
            }
            if (size >= i2) {
                return i2;
            }
        }
        return 0;
    }
}
