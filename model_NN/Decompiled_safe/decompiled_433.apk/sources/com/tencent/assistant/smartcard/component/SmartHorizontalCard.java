package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.y;
import com.tencent.assistant.smartcard.d.z;
import com.tencent.assistant.st.i;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.b.e;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class SmartHorizontalCard extends NormalSmartcardLibaoItem {
    private LinearLayout i;
    private List<y> l;

    public SmartHorizontalCard(Context context) {
        this(context, null);
    }

    public SmartHorizontalCard(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.l = new ArrayList();
    }

    public SmartHorizontalCard(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
        this.l = new ArrayList();
    }

    public void a(List<y> list) {
        this.l = list;
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.b.inflate((int) R.layout.smartcard_horizontal, this);
        setMinimumHeight(by.a(getContext(), 123.0f));
        this.i = (LinearLayout) findViewById(R.id.content);
        f();
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    private void f() {
        z zVar;
        if (this.d instanceof z) {
            zVar = (z) this.d;
        } else {
            zVar = null;
        }
        if (zVar != null) {
            a(zVar.f1769a);
            this.i.removeAllViews();
            for (y next : this.l) {
                SmartSquareAppItem smartSquareAppItem = new SmartSquareAppItem(getContext());
                smartSquareAppItem.a(next, a(zVar, this.l.indexOf(next)));
                this.i.addView(smartSquareAppItem);
            }
        }
    }

    private STInfoV2 a(z zVar, int i2) {
        STInfoV2 a2 = a(i.a(zVar, i2), 100);
        if (!(a2 == null || zVar == null || zVar.a() == null || zVar.a().size() <= i2)) {
            a2.updateWithSimpleAppModel(zVar.a().get(i2).f1768a);
        }
        if (this.h == null) {
            this.h = new e();
        }
        this.h.a(a2);
        return a2;
    }
}
