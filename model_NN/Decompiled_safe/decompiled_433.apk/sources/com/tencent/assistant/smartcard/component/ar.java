package com.tencent.assistant.smartcard.component;

/* compiled from: ProGuard */
public class ar {

    /* renamed from: a  reason: collision with root package name */
    int f1718a;
    int b;
    int c;

    public ar(int i, int i2, int i3) {
        this.f1718a = i;
        this.b = i2;
        this.c = i3;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ar)) {
            return false;
        }
        ar arVar = (ar) obj;
        if (this.f1718a == arVar.f1718a && this.b == arVar.b && this.c == arVar.c) {
            return true;
        }
        return false;
    }

    public String toString() {
        return "SmartCardShowKey{scene=" + this.f1718a + ", cardType=" + this.b + ", cardId=" + this.c + '}';
    }

    public int hashCode() {
        return ("|" + this.f1718a + "|" + this.b + "|" + this.c).hashCode();
    }
}
