package com.tencent.assistant.sdk;

import android.content.Context;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.plugin.UserStateInfo;
import com.tencent.assistant.sdk.param.jce.IPCBaseParam;
import com.tencent.assistant.sdk.param.jce.IPCRequest;
import com.tencent.assistant.sdk.param.jce.PluginLoginInfo;
import com.tencent.assistant.sdk.param.jce.QueryLoginStateResponse;
import com.tencent.pangu.download.DownloadInfo;

/* compiled from: ProGuard */
public class c extends p {
    private UserStateInfo k;

    public c(Context context, IPCRequest iPCRequest) {
        super(context, iPCRequest);
    }

    /* access modifiers changed from: protected */
    public void a(JceStruct jceStruct) {
    }

    public void a(UserStateInfo userStateInfo) {
        this.k = userStateInfo;
    }

    /* access modifiers changed from: protected */
    public JceStruct a() {
        QueryLoginStateResponse queryLoginStateResponse = new QueryLoginStateResponse();
        if (this.k != null) {
            queryLoginStateResponse.a(this.k.getStateChangeType());
            if (this.k.getUserLoginInfo() != null) {
                PluginLoginInfo pluginLoginInfo = new PluginLoginInfo();
                pluginLoginInfo.a(this.k.getUserLoginInfo().getUin());
                pluginLoginInfo.a(this.k.getUserLoginInfo().getA2());
                pluginLoginInfo.b(this.k.getUserLoginInfo().getPic());
                pluginLoginInfo.a(this.k.getUserLoginInfo().getNickName());
                queryLoginStateResponse.a(pluginLoginInfo);
            }
        }
        return queryLoginStateResponse;
    }

    /* access modifiers changed from: protected */
    public IPCBaseParam b() {
        return null;
    }

    public String toString() {
        return "QueryLoginStateResolver{userStateInfo=" + this.k + '}';
    }

    /* access modifiers changed from: protected */
    public boolean a(DownloadInfo downloadInfo) {
        return false;
    }
}
