package com.tencent.assistant.module.a;

import java.util.Comparator;

/* compiled from: ProGuard */
public class h implements Comparator<g> {
    /* renamed from: a */
    public int compare(g gVar, g gVar2) {
        if (gVar.c() == gVar2.c()) {
            return gVar2.b() - gVar.b();
        }
        return gVar.c() - gVar2.c();
    }
}
