package com.tencent.assistant.module;

import android.os.Message;
import android.util.Log;
import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.AppSecretUserProfile;
import com.tencent.assistant.protocol.jce.SetUserProfileRequest;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.an;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public class bb extends p {

    /* renamed from: a  reason: collision with root package name */
    private static bb f977a;
    /* access modifiers changed from: private */
    public Map<Integer, Integer> b = Collections.synchronizedMap(new HashMap());

    private bb() {
    }

    public static synchronized bb a() {
        bb bbVar;
        synchronized (bb.class) {
            if (f977a == null) {
                f977a = new bb();
            }
            bbVar = f977a;
        }
        return bbVar;
    }

    public void a(boolean z, AppConst.WISE_DOWNLOAD_SWITCH_TYPE wise_download_switch_type) {
        TemporaryThreadManager.get().start(new bc(this, wise_download_switch_type, z));
    }

    public void a(boolean z, int i) {
        TemporaryThreadManager.get().start(new bd(this, i, z));
    }

    public void b() {
        boolean e = m.a().e(AppConst.WISE_DOWNLOAD_SWITCH_TYPE.UPDATE);
        if (!e) {
            a().a(e, AppConst.WISE_DOWNLOAD_SWITCH_TYPE.UPDATE);
        }
        boolean e2 = m.a().e(AppConst.WISE_DOWNLOAD_SWITCH_TYPE.NEW_DOWNLOAD);
        if (!e2) {
            a().a(e2, AppConst.WISE_DOWNLOAD_SWITCH_TYPE.UPDATE);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
     arg types: [com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean */
    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        if (this.b.containsKey(Integer.valueOf(i))) {
            int intValue = this.b.get(Integer.valueOf(i)).intValue();
            if (intValue == 1) {
                m.a().b(AppConst.WISE_DOWNLOAD_SWITCH_TYPE.UPDATE, true);
            } else if (intValue == 3) {
                m.a().b(AppConst.WISE_DOWNLOAD_SWITCH_TYPE.NEW_DOWNLOAD, true);
            } else if (intValue == 4) {
                Message obtainMessage = AstApp.i().j().obtainMessage();
                obtainMessage.obj = (AppSecretUserProfile) an.b(((SetUserProfileRequest) jceStruct).a().get(0).a(), AppSecretUserProfile.class);
                obtainMessage.what = EventDispatcherEnum.UI_EVENT_SETUSERPROFILE_STATUS_CHANGED_SUCC;
                AstApp.i().j().sendMessage(obtainMessage);
            }
            this.b.remove(Integer.valueOf(i));
            Log.v("SetUserProfileEngine", "onRequestSuccessed");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
     arg types: [com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean */
    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        if (this.b.containsKey(Integer.valueOf(i))) {
            int intValue = this.b.get(Integer.valueOf(i)).intValue();
            if (intValue == 1) {
                m.a().b(AppConst.WISE_DOWNLOAD_SWITCH_TYPE.UPDATE, false);
            } else if (intValue == 3) {
                m.a().b(AppConst.WISE_DOWNLOAD_SWITCH_TYPE.NEW_DOWNLOAD, false);
            } else if (intValue == 4) {
                Message obtainMessage = AstApp.i().j().obtainMessage();
                obtainMessage.what = EventDispatcherEnum.UI_EVENT_SETUSERPROFILE_STATUS_CHANGED_FAIL;
                AstApp.i().j().sendMessage(obtainMessage);
            }
            this.b.remove(Integer.valueOf(i));
        }
    }
}
