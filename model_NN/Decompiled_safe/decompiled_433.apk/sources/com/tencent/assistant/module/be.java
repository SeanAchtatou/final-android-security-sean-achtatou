package com.tencent.assistant.module;

import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.b.c;
import com.tencent.assistant.b.i;
import com.tencent.assistant.module.callback.n;
import com.tencent.assistant.protocol.jce.GetSmartCardsRequest;
import com.tencent.assistant.protocol.jce.GetSmartCardsResponse;
import com.tencent.assistant.protocol.jce.LbsData;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
public class be extends BaseEngine<n> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public c f980a;
    /* access modifiers changed from: private */
    public LbsData b;
    private i c;

    public be() {
        this.f980a = null;
        this.c = new bi(this);
        this.f980a = new c(AstApp.i().getApplicationContext(), this.c);
    }

    public int a(int i) {
        TemporaryThreadManager.get().start(new bf(this));
        GetSmartCardsRequest getSmartCardsRequest = new GetSmartCardsRequest();
        getSmartCardsRequest.f1334a = i;
        getSmartCardsRequest.b = this.b;
        return send(getSmartCardsRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChanged(new bg(this, i, (GetSmartCardsResponse) jceStruct2));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChanged(new bh(this, i, i2));
    }
}
