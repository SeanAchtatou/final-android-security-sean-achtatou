package com.tencent.assistant.module.update;

import android.text.TextUtils;
import com.tencent.assistant.protocol.jce.UpdateItemCfg;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class y {

    /* renamed from: a  reason: collision with root package name */
    private List<Integer> f1051a = new ArrayList();
    private List<Integer> b = new ArrayList();

    public y(List<UpdateItemCfg> list) {
        a(list);
    }

    private void a(List<UpdateItemCfg> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            UpdateItemCfg updateItemCfg = list.get(i);
            if (updateItemCfg != null) {
                this.f1051a.add(Integer.valueOf(a(updateItemCfg.f1598a, updateItemCfg.b)));
                this.b.addAll(b(updateItemCfg.a()));
            }
        }
    }

    private int a(String str, String str2) {
        return (a(str) * 100) + a(str2);
    }

    private int a(String str) {
        if (TextUtils.isEmpty(str) || str.length() < 2) {
            return 0;
        }
        if ('0' == str.charAt(0)) {
            return Integer.valueOf(str.substring(1, 2)).intValue();
        }
        return Integer.valueOf(str.substring(0, 2)).intValue();
    }

    private List<Integer> b(List<String> list) {
        ArrayList arrayList = new ArrayList();
        for (String next : list) {
            if (!TextUtils.isEmpty(next) && next.length() == 9) {
                if ('0' == next.charAt(0)) {
                    arrayList.add(Integer.valueOf(next.substring(1, next.length())));
                } else {
                    arrayList.add(Integer.valueOf(next));
                }
            }
        }
        return arrayList;
    }

    public List<Integer> a() {
        return this.f1051a;
    }

    public List<Integer> b() {
        return this.b;
    }
}
