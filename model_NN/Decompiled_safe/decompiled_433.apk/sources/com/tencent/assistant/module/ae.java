package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.h;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;

/* compiled from: ProGuard */
class ae implements CallbackHelper.Caller<h> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f958a;
    final /* synthetic */ AppSimpleDetail b;
    final /* synthetic */ ad c;

    ae(ad adVar, int i, AppSimpleDetail appSimpleDetail) {
        this.c = adVar;
        this.f958a = i;
        this.b = appSimpleDetail;
    }

    /* renamed from: a */
    public void call(h hVar) {
        hVar.onGetAppInfoSuccess(this.f958a, 0, this.b);
    }
}
