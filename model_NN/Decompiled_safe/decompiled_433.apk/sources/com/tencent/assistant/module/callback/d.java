package com.tencent.assistant.module.callback;

import com.tencent.assistant.module.callback.CallbackHelper;

/* compiled from: ProGuard */
class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CallbackHelper.Caller f989a;
    final /* synthetic */ CallbackHelper b;

    d(CallbackHelper callbackHelper, CallbackHelper.Caller caller) {
        this.b = callbackHelper;
        this.f989a = caller;
    }

    public void run() {
        this.b.broadcast(this.f989a);
    }
}
