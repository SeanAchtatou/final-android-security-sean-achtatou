package com.tencent.assistant.module.a;

import com.tencent.assistant.Global;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f954a;
    final /* synthetic */ int b;
    final /* synthetic */ l c;

    m(l lVar, int i, int i2) {
        this.c = lVar;
        this.f954a = i;
        this.b = i2;
    }

    public void run() {
        if (this.f954a > com.tencent.assistant.m.a().a("bao_history_version_code", -1)) {
            String a2 = com.tencent.assistant.m.a().a("key_current_qua", Constants.STR_EMPTY);
            com.tencent.assistant.m.a().b("bao_history_version_code", Integer.valueOf(this.f954a));
            com.tencent.assistant.m.a().b("key_history_qua", a2);
        }
        try {
            this.c.e();
            com.tencent.assistant.m.a().b("bao_current_version_code", Integer.valueOf(this.b));
            Global.init();
            com.tencent.assistant.m.a().b("key_current_qua", Global.getQUA());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
