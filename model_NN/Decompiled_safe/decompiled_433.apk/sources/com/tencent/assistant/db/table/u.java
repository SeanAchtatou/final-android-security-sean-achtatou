package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.AstDbHelper;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.protocol.jce.PopUpInfo;

/* compiled from: ProGuard */
public class u implements IBaseTable {
    public synchronized long a(PopUpInfo popUpInfo) {
        long j;
        int i = 0;
        synchronized (this) {
            SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
            if (writableDatabaseWrapper != null) {
                try {
                    writableDatabaseWrapper.beginTransaction();
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("_id", Long.valueOf(popUpInfo.f1436a));
                    contentValues.put("showTime", Long.valueOf(popUpInfo.d));
                    contentValues.put("cycle", Integer.valueOf(popUpInfo.f));
                    int update = writableDatabaseWrapper.update("pop_up_info_table", contentValues, "_id = ? ", new String[]{String.valueOf(popUpInfo.f1436a)});
                    if (update <= 0) {
                        i = (int) (writableDatabaseWrapper.insert("pop_up_info_table", null, contentValues) + ((long) 0));
                    } else {
                        i = 0 + update;
                    }
                    writableDatabaseWrapper.setTransactionSuccessful();
                    writableDatabaseWrapper.endTransaction();
                } catch (Exception e) {
                    e.printStackTrace();
                    writableDatabaseWrapper.endTransaction();
                } catch (Throwable th) {
                    writableDatabaseWrapper.endTransaction();
                    throw th;
                }
            }
            j = (long) i;
        }
        return j;
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "pop_up_info_table";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists pop_up_info_table(_id INTEGER PRIMARY KEY AUTOINCREMENT,showTime INTEGER,cycle INTEGER);";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i2 != 4) {
            return null;
        }
        return new String[]{"CREATE TABLE if not exists pop_up_info_table(_id INTEGER PRIMARY KEY AUTOINCREMENT,showTime INTEGER,cycle INTEGER);"};
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return AstDbHelper.get(AstApp.i());
    }
}
