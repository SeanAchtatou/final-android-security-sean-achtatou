package com.tencent.assistant.db.contentprovider;

/* compiled from: ProGuard */
public class NotSupportOperationException extends Exception {
    public NotSupportOperationException() {
    }

    public NotSupportOperationException(String str) {
        super(str);
    }
}
