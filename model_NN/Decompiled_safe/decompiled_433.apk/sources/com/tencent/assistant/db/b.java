package com.tencent.assistant.db;

import java.io.File;
import java.io.FilenameFilter;

/* compiled from: ProGuard */
final class b implements FilenameFilter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f741a;

    b(String str) {
        this.f741a = str;
    }

    public boolean accept(File file, String str) {
        return str.endsWith("." + this.f741a);
    }
}
