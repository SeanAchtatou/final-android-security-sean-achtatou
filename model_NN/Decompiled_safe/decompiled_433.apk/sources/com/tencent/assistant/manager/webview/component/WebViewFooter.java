package com.tencent.assistant.manager.webview.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class WebViewFooter extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private Context f910a;
    private View b;
    private View c;
    private View d;
    /* access modifiers changed from: private */
    public k e;
    private View.OnClickListener f = new j(this);

    public WebViewFooter(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public WebViewFooter(Context context) {
        super(context);
        a(context);
    }

    private void a(Context context) {
        this.f910a = context;
        View inflate = LayoutInflater.from(context).inflate((int) R.layout.webview_footer_layout, this);
        this.b = inflate.findViewById(R.id.webview_back_img);
        this.c = inflate.findViewById(R.id.webview_forward_img);
        this.d = inflate.findViewById(R.id.webview_fresh_img);
        this.b.setOnClickListener(this.f);
        this.c.setOnClickListener(this.f);
        this.d.setOnClickListener(this.f);
    }

    public void a(boolean z) {
        this.b.setEnabled(z);
    }

    public void b(boolean z) {
        this.c.setEnabled(z);
    }

    public void a(k kVar) {
        this.e = kVar;
    }
}
