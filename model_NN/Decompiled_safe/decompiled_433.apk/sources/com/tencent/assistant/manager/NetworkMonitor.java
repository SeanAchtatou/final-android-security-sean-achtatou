package com.tencent.assistant.manager;

import android.content.Context;
import android.content.IntentFilter;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.receiver.NetworkMonitorReceiver;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: ProGuard */
public class NetworkMonitor {

    /* renamed from: a  reason: collision with root package name */
    protected ReferenceQueue<ConnectivityChangeListener> f851a = new ReferenceQueue<>();
    protected ConcurrentLinkedQueue<WeakReference<ConnectivityChangeListener>> b = new ConcurrentLinkedQueue<>();
    private Context c;
    private NetworkMonitorReceiver d = new NetworkMonitorReceiver();

    /* compiled from: ProGuard */
    public interface ConnectivityChangeListener {
        void onConnected(APN apn);

        void onConnectivityChanged(APN apn, APN apn2);

        void onDisconnected(APN apn);
    }

    protected NetworkMonitor() {
    }

    /* access modifiers changed from: protected */
    public void a(ConnectivityChangeListener connectivityChangeListener) {
        if (connectivityChangeListener != null) {
            while (true) {
                Reference<? extends ConnectivityChangeListener> poll = this.f851a.poll();
                if (poll == null) {
                    break;
                }
                this.b.remove(poll);
            }
            Iterator<WeakReference<ConnectivityChangeListener>> it = this.b.iterator();
            while (it.hasNext()) {
                if (((ConnectivityChangeListener) it.next().get()) == connectivityChangeListener) {
                    return;
                }
            }
            this.b.add(new WeakReference(connectivityChangeListener, this.f851a));
        }
    }

    /* access modifiers changed from: protected */
    public void b(ConnectivityChangeListener connectivityChangeListener) {
        if (connectivityChangeListener != null) {
            Iterator<WeakReference<ConnectivityChangeListener>> it = this.b.iterator();
            while (it.hasNext()) {
                WeakReference next = it.next();
                if (((ConnectivityChangeListener) next.get()) == connectivityChangeListener) {
                    this.b.remove(next);
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Context context) {
        try {
            this.c = context.getApplicationContext();
            this.c.registerReceiver(this.d, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void a(APN apn) {
        Iterator<WeakReference<ConnectivityChangeListener>> it = this.b.iterator();
        while (it.hasNext()) {
            ConnectivityChangeListener connectivityChangeListener = (ConnectivityChangeListener) it.next().get();
            if (connectivityChangeListener != null) {
                try {
                    connectivityChangeListener.onConnected(apn);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b(APN apn) {
        Iterator<WeakReference<ConnectivityChangeListener>> it = this.b.iterator();
        while (it.hasNext()) {
            ConnectivityChangeListener connectivityChangeListener = (ConnectivityChangeListener) it.next().get();
            if (connectivityChangeListener != null) {
                connectivityChangeListener.onDisconnected(apn);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(APN apn, APN apn2) {
        Iterator<WeakReference<ConnectivityChangeListener>> it = this.b.iterator();
        while (it.hasNext()) {
            ConnectivityChangeListener connectivityChangeListener = (ConnectivityChangeListener) it.next().get();
            if (connectivityChangeListener != null) {
                connectivityChangeListener.onConnectivityChanged(apn, apn2);
            }
        }
    }
}
