package com.tencent.assistant.manager;

import com.tencent.assistant.model.l;
import com.tencent.assistant.module.bj;
import com.tencent.assistant.module.callback.o;
import java.util.HashMap;

/* compiled from: ProGuard */
public abstract class c {
    protected static HashMap<Long, Byte> d = new HashMap<>();
    private static int e = 1;

    /* renamed from: a  reason: collision with root package name */
    protected int f855a = -1;
    protected bj b = new bj();
    protected HashMap<Integer, l> c = new HashMap<>();
    private o f = new d(this);

    /* access modifiers changed from: protected */
    public abstract void a(int i, int i2);

    /* access modifiers changed from: protected */
    public abstract void a(int i, int i2, int i3);

    public int a(l lVar) {
        if (lVar == null) {
            return -1;
        }
        return b(lVar);
    }

    private int b(l lVar) {
        if (lVar == null) {
            return -1;
        }
        int a2 = this.b.a(lVar.f948a);
        lVar.c = a();
        this.c.put(Integer.valueOf(a2), lVar);
        return lVar.c;
    }

    public c() {
        this.b.register(this.f);
    }

    /* access modifiers changed from: protected */
    public synchronized int a() {
        int i;
        i = e;
        e = i + 1;
        return i;
    }

    public static void a(long j, byte b2) {
        d.put(Long.valueOf(j), Byte.valueOf(b2));
    }

    public static byte a(long j) {
        if (d.containsKey(Long.valueOf(j))) {
            return d.get(Long.valueOf(j)).byteValue();
        }
        return 0;
    }

    public static void b(long j, byte b2) {
        if (d.containsKey(Long.valueOf(j))) {
            d.put(Long.valueOf(j), Byte.valueOf((byte) (d.get(Long.valueOf(j)).byteValue() | b2)));
        } else {
            d.put(Long.valueOf(j), Byte.valueOf(b2));
        }
    }
}
