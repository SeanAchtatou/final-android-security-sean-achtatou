package com.tencent.assistant.manager.notification.a;

import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.PushInfo;

/* compiled from: ProGuard */
public class q extends e {
    public q(int i, PushInfo pushInfo, byte[] bArr) {
        super(i, pushInfo, bArr);
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        if (super.c() && !TextUtils.isEmpty(this.c.c)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        if (this.c.f == null || this.c.f.f1445a != 3) {
            b(R.layout.notification_card_1);
        } else {
            b(R.layout.notification_card_1_cover);
        }
        if (this.i == null) {
            return false;
        }
        i();
        m();
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        this.j = c(R.layout.notification_card1_right2);
        this.i.removeAllViews(R.id.rightContainer);
        this.i.addView(R.id.rightContainer, this.j);
        this.i.setViewVisibility(R.id.rightContainer, 0);
        this.j.setOnClickPendingIntent(R.id.rightButton, k());
        this.i.setTextViewText(R.id.rightButton, TextUtils.isEmpty(this.c.m.f1450a) ? AstApp.i().getString(R.string.notification_see_now) : this.c.m.f1450a);
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean h() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void m() {
        int i;
        if (this.c.m == null || this.c.m.b <= 0) {
            this.i.setViewVisibility(R.id.tag_icon, 8);
            return;
        }
        switch ((int) this.c.m.b) {
            case 2:
                i = R.drawable.common_tag_gift;
                break;
            case 4:
                i = R.drawable.common_tag_activity;
                break;
            case 8:
                i = R.drawable.common_tag_topic;
                break;
            case 16:
                i = R.drawable.common_tag_first;
                break;
            case 32:
                i = R.drawable.common_tag_video;
                break;
            default:
                return;
        }
        this.i.setViewVisibility(R.id.tag_icon, 0);
        this.i.setImageViewResource(R.id.tag_icon, i);
    }
}
