package com.tencent.assistant.manager.webview.component;

import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.net.c;

/* compiled from: ProGuard */
class h implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TxWebViewContainer f915a;

    h(TxWebViewContainer txWebViewContainer) {
        this.f915a = txWebViewContainer;
    }

    public int a() {
        try {
            return ((BaseActivity) this.f915a.t()).f();
        } catch (Exception e) {
            return 0;
        }
    }

    public void b() {
        if (!c.a()) {
            this.f915a.c(30);
        } else {
            this.f915a.c(20);
        }
    }

    public void c() {
        if (this.f915a.g != null && !this.f915a.g.c) {
            this.f915a.d.setVisibility(0);
        }
        if (this.f915a.h != null) {
            this.f915a.h.a();
        }
    }

    public void d() {
        this.f915a.d.setVisibility(4);
        if (this.f915a.h != null) {
            this.f915a.h.b();
        }
    }
}
