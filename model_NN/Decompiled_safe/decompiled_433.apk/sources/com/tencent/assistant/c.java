package com.tencent.assistant;

import com.tencent.connect.common.Constants;
import java.io.File;

/* compiled from: ProGuard */
public class c {
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0047 A[SYNTHETIC, Splitter:B:35:0x0047] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x004c A[SYNTHETIC, Splitter:B:38:0x004c] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x005a A[SYNTHETIC, Splitter:B:46:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x005f A[SYNTHETIC, Splitter:B:49:0x005f] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x006b A[SYNTHETIC, Splitter:B:55:0x006b] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0070 A[SYNTHETIC, Splitter:B:58:0x0070] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:32:0x0042=Splitter:B:32:0x0042, B:43:0x0055=Splitter:B:43:0x0055} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a() {
        /*
            r0 = 0
            java.io.FileReader r3 = new java.io.FileReader     // Catch:{ FileNotFoundException -> 0x003f, IOException -> 0x0052, all -> 0x0065 }
            java.lang.String r1 = "/proc/cpuinfo"
            r3.<init>(r1)     // Catch:{ FileNotFoundException -> 0x003f, IOException -> 0x0052, all -> 0x0065 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ FileNotFoundException -> 0x009f, IOException -> 0x009a, all -> 0x0094 }
            r2.<init>(r3)     // Catch:{ FileNotFoundException -> 0x009f, IOException -> 0x009a, all -> 0x0094 }
            java.lang.String r1 = r2.readLine()     // Catch:{ FileNotFoundException -> 0x00a2, IOException -> 0x009d }
            if (r1 != 0) goto L_0x001e
            if (r3 == 0) goto L_0x0018
            r3.close()     // Catch:{ IOException -> 0x0088 }
        L_0x0018:
            if (r2 == 0) goto L_0x001d
            r2.close()     // Catch:{ IOException -> 0x008d }
        L_0x001d:
            return r0
        L_0x001e:
            java.lang.String r4 = ":\\s+"
            r5 = 2
            java.lang.String[] r4 = r1.split(r4, r5)     // Catch:{ FileNotFoundException -> 0x00a2, IOException -> 0x009d }
            r1 = 0
        L_0x0026:
            int r5 = r4.length     // Catch:{ FileNotFoundException -> 0x00a2, IOException -> 0x009d }
            if (r1 >= r5) goto L_0x002c
            int r1 = r1 + 1
            goto L_0x0026
        L_0x002c:
            r1 = 1
            r0 = r4[r1]     // Catch:{ FileNotFoundException -> 0x00a2, IOException -> 0x009d }
            if (r3 == 0) goto L_0x0034
            r3.close()     // Catch:{ IOException -> 0x008f }
        L_0x0034:
            if (r2 == 0) goto L_0x001d
            r2.close()     // Catch:{ IOException -> 0x003a }
            goto L_0x001d
        L_0x003a:
            r1 = move-exception
        L_0x003b:
            r1.printStackTrace()
            goto L_0x001d
        L_0x003f:
            r1 = move-exception
            r2 = r0
            r3 = r0
        L_0x0042:
            r1.printStackTrace()     // Catch:{ all -> 0x0098 }
            if (r3 == 0) goto L_0x004a
            r3.close()     // Catch:{ IOException -> 0x007e }
        L_0x004a:
            if (r2 == 0) goto L_0x001d
            r2.close()     // Catch:{ IOException -> 0x0050 }
            goto L_0x001d
        L_0x0050:
            r1 = move-exception
            goto L_0x003b
        L_0x0052:
            r1 = move-exception
            r2 = r0
            r3 = r0
        L_0x0055:
            r1.printStackTrace()     // Catch:{ all -> 0x0098 }
            if (r3 == 0) goto L_0x005d
            r3.close()     // Catch:{ IOException -> 0x0083 }
        L_0x005d:
            if (r2 == 0) goto L_0x001d
            r2.close()     // Catch:{ IOException -> 0x0063 }
            goto L_0x001d
        L_0x0063:
            r1 = move-exception
            goto L_0x003b
        L_0x0065:
            r1 = move-exception
            r2 = r0
            r3 = r0
            r0 = r1
        L_0x0069:
            if (r3 == 0) goto L_0x006e
            r3.close()     // Catch:{ IOException -> 0x0074 }
        L_0x006e:
            if (r2 == 0) goto L_0x0073
            r2.close()     // Catch:{ IOException -> 0x0079 }
        L_0x0073:
            throw r0
        L_0x0074:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x006e
        L_0x0079:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0073
        L_0x007e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004a
        L_0x0083:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x005d
        L_0x0088:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0018
        L_0x008d:
            r1 = move-exception
            goto L_0x003b
        L_0x008f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0034
        L_0x0094:
            r1 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x0069
        L_0x0098:
            r0 = move-exception
            goto L_0x0069
        L_0x009a:
            r1 = move-exception
            r2 = r0
            goto L_0x0055
        L_0x009d:
            r1 = move-exception
            goto L_0x0055
        L_0x009f:
            r1 = move-exception
            r2 = r0
            goto L_0x0042
        L_0x00a2:
            r1 = move-exception
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.c.a():java.lang.String");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r3v0 */
    /* JADX WARN: Type inference failed for: r3v1, types: [java.io.BufferedReader] */
    /* JADX WARN: Type inference failed for: r3v2, types: [java.io.BufferedReader] */
    /* JADX WARN: Type inference failed for: r3v3, types: [java.io.FileReader] */
    /* JADX WARN: Type inference failed for: r3v4 */
    /* JADX WARN: Type inference failed for: r3v5 */
    /* JADX WARN: Type inference failed for: r3v6 */
    /* JADX WARN: Type inference failed for: r3v7 */
    /* JADX WARN: Type inference failed for: r3v8 */
    /* JADX WARN: Type inference failed for: r3v9 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x003e A[SYNTHETIC, Splitter:B:30:0x003e] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0043 A[SYNTHETIC, Splitter:B:33:0x0043] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0050 A[SYNTHETIC, Splitter:B:41:0x0050] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0055 A[SYNTHETIC, Splitter:B:44:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x005f A[SYNTHETIC, Splitter:B:50:0x005f] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0064 A[SYNTHETIC, Splitter:B:53:0x0064] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int b() {
        /*
            r3 = 0
            r0 = 0
            java.io.FileReader r4 = new java.io.FileReader     // Catch:{ FileNotFoundException -> 0x0037, Throwable -> 0x0049, all -> 0x005b }
            java.lang.String r1 = "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq"
            r4.<init>(r1)     // Catch:{ FileNotFoundException -> 0x0037, Throwable -> 0x0049, all -> 0x005b }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ FileNotFoundException -> 0x0096, Throwable -> 0x0091 }
            r2.<init>(r4)     // Catch:{ FileNotFoundException -> 0x0096, Throwable -> 0x0091 }
            java.lang.String r1 = r2.readLine()     // Catch:{ FileNotFoundException -> 0x009a, Throwable -> 0x0093, all -> 0x008a }
            if (r1 != 0) goto L_0x001f
            if (r4 == 0) goto L_0x0019
            r4.close()     // Catch:{ IOException -> 0x007c }
        L_0x0019:
            if (r2 == 0) goto L_0x001e
            r2.close()     // Catch:{ IOException -> 0x0081 }
        L_0x001e:
            return r0
        L_0x001f:
            java.lang.String r1 = r1.trim()     // Catch:{ FileNotFoundException -> 0x009a, Throwable -> 0x0093, all -> 0x008a }
            int r0 = java.lang.Integer.parseInt(r1)     // Catch:{ FileNotFoundException -> 0x009a, Throwable -> 0x0093, all -> 0x008a }
            if (r4 == 0) goto L_0x002c
            r4.close()     // Catch:{ IOException -> 0x0083 }
        L_0x002c:
            if (r2 == 0) goto L_0x001e
            r2.close()     // Catch:{ IOException -> 0x0032 }
            goto L_0x001e
        L_0x0032:
            r1 = move-exception
        L_0x0033:
            r1.printStackTrace()
            goto L_0x001e
        L_0x0037:
            r1 = move-exception
            r2 = r3
        L_0x0039:
            r1.printStackTrace()     // Catch:{ all -> 0x008d }
            if (r3 == 0) goto L_0x0041
            r3.close()     // Catch:{ IOException -> 0x0072 }
        L_0x0041:
            if (r2 == 0) goto L_0x001e
            r2.close()     // Catch:{ IOException -> 0x0047 }
            goto L_0x001e
        L_0x0047:
            r1 = move-exception
            goto L_0x0033
        L_0x0049:
            r1 = move-exception
            r4 = r3
        L_0x004b:
            r1.printStackTrace()     // Catch:{ all -> 0x0088 }
            if (r4 == 0) goto L_0x0053
            r4.close()     // Catch:{ IOException -> 0x0077 }
        L_0x0053:
            if (r3 == 0) goto L_0x001e
            r3.close()     // Catch:{ IOException -> 0x0059 }
            goto L_0x001e
        L_0x0059:
            r1 = move-exception
            goto L_0x0033
        L_0x005b:
            r0 = move-exception
            r4 = r3
        L_0x005d:
            if (r4 == 0) goto L_0x0062
            r4.close()     // Catch:{ IOException -> 0x0068 }
        L_0x0062:
            if (r3 == 0) goto L_0x0067
            r3.close()     // Catch:{ IOException -> 0x006d }
        L_0x0067:
            throw r0
        L_0x0068:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0062
        L_0x006d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0067
        L_0x0072:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0041
        L_0x0077:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0053
        L_0x007c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0019
        L_0x0081:
            r1 = move-exception
            goto L_0x0033
        L_0x0083:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x002c
        L_0x0088:
            r0 = move-exception
            goto L_0x005d
        L_0x008a:
            r0 = move-exception
            r3 = r2
            goto L_0x005d
        L_0x008d:
            r0 = move-exception
            r4 = r3
            r3 = r2
            goto L_0x005d
        L_0x0091:
            r1 = move-exception
            goto L_0x004b
        L_0x0093:
            r1 = move-exception
            r3 = r2
            goto L_0x004b
        L_0x0096:
            r1 = move-exception
            r2 = r3
            r3 = r4
            goto L_0x0039
        L_0x009a:
            r1 = move-exception
            r3 = r4
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.c.b():int");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r3v0 */
    /* JADX WARN: Type inference failed for: r3v1, types: [java.io.BufferedReader] */
    /* JADX WARN: Type inference failed for: r3v2, types: [java.io.BufferedReader] */
    /* JADX WARN: Type inference failed for: r3v3, types: [java.io.FileReader] */
    /* JADX WARN: Type inference failed for: r3v4 */
    /* JADX WARN: Type inference failed for: r3v5 */
    /* JADX WARN: Type inference failed for: r3v6 */
    /* JADX WARN: Type inference failed for: r3v7 */
    /* JADX WARN: Type inference failed for: r3v8 */
    /* JADX WARN: Type inference failed for: r3v9 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x003e A[SYNTHETIC, Splitter:B:30:0x003e] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0043 A[SYNTHETIC, Splitter:B:33:0x0043] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0050 A[SYNTHETIC, Splitter:B:41:0x0050] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0055 A[SYNTHETIC, Splitter:B:44:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x005f A[SYNTHETIC, Splitter:B:50:0x005f] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0064 A[SYNTHETIC, Splitter:B:53:0x0064] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int c() {
        /*
            r3 = 0
            r0 = 0
            java.io.FileReader r4 = new java.io.FileReader     // Catch:{ FileNotFoundException -> 0x0037, IOException -> 0x0049, all -> 0x005b }
            java.lang.String r1 = "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq"
            r4.<init>(r1)     // Catch:{ FileNotFoundException -> 0x0037, IOException -> 0x0049, all -> 0x005b }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ FileNotFoundException -> 0x0096, IOException -> 0x0091 }
            r2.<init>(r4)     // Catch:{ FileNotFoundException -> 0x0096, IOException -> 0x0091 }
            java.lang.String r1 = r2.readLine()     // Catch:{ FileNotFoundException -> 0x009a, IOException -> 0x0093, all -> 0x008a }
            if (r1 != 0) goto L_0x001f
            if (r4 == 0) goto L_0x0019
            r4.close()     // Catch:{ IOException -> 0x007c }
        L_0x0019:
            if (r2 == 0) goto L_0x001e
            r2.close()     // Catch:{ IOException -> 0x0081 }
        L_0x001e:
            return r0
        L_0x001f:
            java.lang.String r1 = r1.trim()     // Catch:{ FileNotFoundException -> 0x009a, IOException -> 0x0093, all -> 0x008a }
            int r0 = java.lang.Integer.parseInt(r1)     // Catch:{ FileNotFoundException -> 0x009a, IOException -> 0x0093, all -> 0x008a }
            if (r4 == 0) goto L_0x002c
            r4.close()     // Catch:{ IOException -> 0x0083 }
        L_0x002c:
            if (r2 == 0) goto L_0x001e
            r2.close()     // Catch:{ IOException -> 0x0032 }
            goto L_0x001e
        L_0x0032:
            r1 = move-exception
        L_0x0033:
            r1.printStackTrace()
            goto L_0x001e
        L_0x0037:
            r1 = move-exception
            r2 = r3
        L_0x0039:
            r1.printStackTrace()     // Catch:{ all -> 0x008d }
            if (r3 == 0) goto L_0x0041
            r3.close()     // Catch:{ IOException -> 0x0072 }
        L_0x0041:
            if (r2 == 0) goto L_0x001e
            r2.close()     // Catch:{ IOException -> 0x0047 }
            goto L_0x001e
        L_0x0047:
            r1 = move-exception
            goto L_0x0033
        L_0x0049:
            r1 = move-exception
            r4 = r3
        L_0x004b:
            r1.printStackTrace()     // Catch:{ all -> 0x0088 }
            if (r4 == 0) goto L_0x0053
            r4.close()     // Catch:{ IOException -> 0x0077 }
        L_0x0053:
            if (r3 == 0) goto L_0x001e
            r3.close()     // Catch:{ IOException -> 0x0059 }
            goto L_0x001e
        L_0x0059:
            r1 = move-exception
            goto L_0x0033
        L_0x005b:
            r0 = move-exception
            r4 = r3
        L_0x005d:
            if (r4 == 0) goto L_0x0062
            r4.close()     // Catch:{ IOException -> 0x0068 }
        L_0x0062:
            if (r3 == 0) goto L_0x0067
            r3.close()     // Catch:{ IOException -> 0x006d }
        L_0x0067:
            throw r0
        L_0x0068:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0062
        L_0x006d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0067
        L_0x0072:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0041
        L_0x0077:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0053
        L_0x007c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0019
        L_0x0081:
            r1 = move-exception
            goto L_0x0033
        L_0x0083:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x002c
        L_0x0088:
            r0 = move-exception
            goto L_0x005d
        L_0x008a:
            r0 = move-exception
            r3 = r2
            goto L_0x005d
        L_0x008d:
            r0 = move-exception
            r4 = r3
            r3 = r2
            goto L_0x005d
        L_0x0091:
            r1 = move-exception
            goto L_0x004b
        L_0x0093:
            r1 = move-exception
            r3 = r2
            goto L_0x004b
        L_0x0096:
            r1 = move-exception
            r2 = r3
            r3 = r4
            goto L_0x0039
        L_0x009a:
            r1 = move-exception
            r3 = r4
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.c.c():int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x005c A[SYNTHETIC, Splitter:B:34:0x005c] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0066 A[SYNTHETIC, Splitter:B:40:0x0066] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:31:0x0057=Splitter:B:31:0x0057, B:23:0x004a=Splitter:B:23:0x004a} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long d() {
        /*
            r0 = 0
            r4 = 0
            java.lang.String r2 = "/proc/meminfo"
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ FileNotFoundException -> 0x0048, Exception -> 0x0055, all -> 0x0062 }
            java.io.FileReader r5 = new java.io.FileReader     // Catch:{ FileNotFoundException -> 0x0048, Exception -> 0x0055, all -> 0x0062 }
            r5.<init>(r2)     // Catch:{ FileNotFoundException -> 0x0048, Exception -> 0x0055, all -> 0x0062 }
            r2 = 80
            r3.<init>(r5, r2)     // Catch:{ FileNotFoundException -> 0x0048, Exception -> 0x0055, all -> 0x0062 }
            java.lang.String r2 = r3.readLine()     // Catch:{ FileNotFoundException -> 0x0075, Exception -> 0x0073 }
            if (r2 == 0) goto L_0x0018
            r4 = r2
        L_0x0018:
            if (r4 != 0) goto L_0x0020
            if (r3 == 0) goto L_0x001f
            r3.close()     // Catch:{ IOException -> 0x006f }
        L_0x001f:
            return r0
        L_0x0020:
            r2 = 58
            int r2 = r4.indexOf(r2)     // Catch:{ FileNotFoundException -> 0x0075, Exception -> 0x0073 }
            r5 = 107(0x6b, float:1.5E-43)
            int r5 = r4.indexOf(r5)     // Catch:{ FileNotFoundException -> 0x0075, Exception -> 0x0073 }
            int r2 = r2 + 1
            java.lang.String r2 = r4.substring(r2, r5)     // Catch:{ FileNotFoundException -> 0x0075, Exception -> 0x0073 }
            java.lang.String r2 = r2.trim()     // Catch:{ FileNotFoundException -> 0x0075, Exception -> 0x0073 }
            int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ FileNotFoundException -> 0x0075, Exception -> 0x0073 }
            int r0 = r2 / 1024
            long r0 = (long) r0
            if (r3 == 0) goto L_0x001f
            r3.close()     // Catch:{ IOException -> 0x0043 }
            goto L_0x001f
        L_0x0043:
            r2 = move-exception
        L_0x0044:
            r2.printStackTrace()
            goto L_0x001f
        L_0x0048:
            r2 = move-exception
            r3 = r4
        L_0x004a:
            r2.printStackTrace()     // Catch:{ all -> 0x0071 }
            if (r3 == 0) goto L_0x001f
            r3.close()     // Catch:{ IOException -> 0x0053 }
            goto L_0x001f
        L_0x0053:
            r2 = move-exception
            goto L_0x0044
        L_0x0055:
            r2 = move-exception
            r3 = r4
        L_0x0057:
            r2.printStackTrace()     // Catch:{ all -> 0x0071 }
            if (r3 == 0) goto L_0x001f
            r3.close()     // Catch:{ IOException -> 0x0060 }
            goto L_0x001f
        L_0x0060:
            r2 = move-exception
            goto L_0x0044
        L_0x0062:
            r0 = move-exception
            r3 = r4
        L_0x0064:
            if (r3 == 0) goto L_0x0069
            r3.close()     // Catch:{ IOException -> 0x006a }
        L_0x0069:
            throw r0
        L_0x006a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0069
        L_0x006f:
            r2 = move-exception
            goto L_0x0044
        L_0x0071:
            r0 = move-exception
            goto L_0x0064
        L_0x0073:
            r2 = move-exception
            goto L_0x0057
        L_0x0075:
            r2 = move-exception
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.c.d():long");
    }

    public static int e() {
        try {
            File[] listFiles = new File("/sys/devices/system/cpu/").listFiles(new d());
            if (listFiles == null) {
                return 1;
            }
            return listFiles.length;
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    public static String[] f() {
        String[] strArr = {Constants.STR_EMPTY, Constants.STR_EMPTY};
        if (j.a()) {
            return j.b();
        }
        if (e.a()) {
            return e.b();
        }
        return strArr;
    }
}
