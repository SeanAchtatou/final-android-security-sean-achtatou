package com.tencent.assistant.plugin;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class PluginSTInfoV2 extends STInfoV2 {
    public PluginSTInfoV2(int i, String str, int i2, String str2, int i3) {
        super(i, str, i2, str2, i3);
    }

    public PluginSTInfoV2(STInfoV2 sTInfoV2) {
        super(sTInfoV2.scene, sTInfoV2.slotId, sTInfoV2.sourceScene, sTInfoV2.sourceSceneSlotId, sTInfoV2.actionId);
    }

    public void updateWithSimpleAppModel(SimpleAppModel simpleAppModel) {
        super.updateWithSimpleAppModel(simpleAppModel);
    }

    public void updateStatus(SimpleAppModel simpleAppModel) {
        super.updateStatus(simpleAppModel);
    }

    public void setCategoryId(long j) {
        super.setCategoryId(j);
    }

    public void updateContentId(STCommonInfo.ContentIdType contentIdType, String str) {
        super.updateContentId(contentIdType, str);
    }

    public String getFinalSlotId() {
        return super.getFinalSlotId();
    }

    /* access modifiers changed from: protected */
    public Object clone() {
        return super.clone();
    }
}
