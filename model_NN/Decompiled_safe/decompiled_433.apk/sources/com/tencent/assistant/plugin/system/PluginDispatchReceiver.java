package com.tencent.assistant.plugin.system;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.qq.AppService.SmsSentReceiver;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.i;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bn;
import java.util.List;

/* compiled from: ProGuard */
public abstract class PluginDispatchReceiver extends BroadcastReceiver {
    /* access modifiers changed from: protected */
    public abstract int a();

    /* access modifiers changed from: protected */
    public abstract BroadcastReceiver a(Context context, PluginInfo pluginInfo);

    public void onReceive(Context context, Intent intent) {
        if (getClass().getName().equals(SmsSentReceiver.class.getName())) {
            intent.putExtra("r_code", getResultCode());
        }
        List<PluginInfo> a2 = i.b().a(a());
        if (a2 != null) {
            for (PluginInfo a3 : a2) {
                BroadcastReceiver a4 = a(context, a3);
                if (a4 != null) {
                    try {
                        bn bnVar = new bn();
                        a4.onReceive(context, intent);
                        XLog.i("Plugin", "receiver:" + a4 + ",cost " + bnVar);
                    } catch (Throwable th) {
                        th.printStackTrace();
                    }
                }
            }
        }
    }
}
