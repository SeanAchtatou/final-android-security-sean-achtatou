package com.tencent.assistant.plugin.component;

import android.view.View;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.module.k;
import com.tencent.assistant.plugin.PluginDownloadInfo;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.c;
import com.tencent.assistant.plugin.mgr.i;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.pangu.download.DownloadInfo;

/* compiled from: ProGuard */
class a implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PluginDownStateButton f1088a;

    a(PluginDownStateButton pluginDownStateButton) {
        this.f1088a = pluginDownStateButton;
    }

    public void onClick(View view) {
        PluginDownloadInfo pluginDownloadInfo;
        PluginDownloadInfo a2 = c.a().a(this.f1088a.pluginStartEntry.getPluginId());
        if (a2 == null) {
            pluginDownloadInfo = c.a().a(this.f1088a.pluginStartEntry.getPackageName());
        } else {
            pluginDownloadInfo = a2;
        }
        if (pluginDownloadInfo != null) {
            PluginInfo a3 = i.b().a(this.f1088a.pluginStartEntry.getPackageName());
            boolean unused = this.f1088a.isPreDownload = false;
            if (a3 == null || a3.getVersion() < pluginDownloadInfo.version) {
                DownloadInfo b = c.a().b(pluginDownloadInfo);
                if (b == null) {
                    b = c.a().a(pluginDownloadInfo);
                }
                AppConst.AppState a4 = k.a(b, pluginDownloadInfo, (PluginInfo) null);
                if (a4 == AppConst.AppState.DOWNLOADING || a4 == AppConst.AppState.QUEUING) {
                    c.a().e(pluginDownloadInfo);
                    this.f1088a.pluginSTReport(STConstAction.ACTION_HIT_PAUSE);
                } else if (a4 == AppConst.AppState.DOWNLOADED) {
                    String downloadingPath = b.getDownloadingPath();
                    if (pluginDownloadInfo != null) {
                        TemporaryThreadManager.get().start(new b(this, downloadingPath, pluginDownloadInfo.pluginPackageName));
                    }
                    this.f1088a.pluginSTReport(305);
                } else {
                    c.a().c(pluginDownloadInfo);
                    int i = STConstAction.ACTION_HIT_PLUGIN_DOWNLOAD;
                    if (this.f1088a.isUpdate) {
                        i = STConstAction.ACTION_HIT_PLUGIN_UPDATE;
                    }
                    if (a4 == AppConst.AppState.PAUSED) {
                        i = STConstAction.ACTION_HIT_CONTINUE;
                    }
                    this.f1088a.pluginSTReport(i);
                }
            } else {
                this.f1088a.pluginStartEntry.setVersionCode(a3.getVersion());
                TemporaryThreadManager.get().start(new c(this));
            }
        }
    }
}
