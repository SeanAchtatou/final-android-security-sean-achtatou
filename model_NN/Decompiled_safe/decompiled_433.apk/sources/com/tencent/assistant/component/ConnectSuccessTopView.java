package com.tencent.assistant.component;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class ConnectSuccessTopView extends LinearLayout {
    public static final byte USB = 0;
    public static final byte WIFI = 1;
    private Activity activity;
    private ImageView connect_pc_icon;
    private TextView connect_pc_name;
    private Context context;
    /* access modifiers changed from: private */
    public byte cur_connect = 0;
    private ImageView left_arrow;
    private View view;

    public ConnectSuccessTopView(Context context2) {
        super(context2);
        this.context = context2;
        init();
    }

    public ConnectSuccessTopView(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        this.context = context2;
        init();
    }

    public void setActivity(Activity activity2) {
        this.activity = activity2;
    }

    private void init() {
        this.view = LayoutInflater.from(this.context).inflate((int) R.layout.photo_backup_top_right_layout, this);
        this.connect_pc_icon = (ImageView) this.view.findViewById(R.id.connect_pc_icon);
        this.left_arrow = (ImageView) this.view.findViewById(R.id.left_arrow);
        this.connect_pc_name = (TextView) this.view.findViewById(R.id.connect_pc_name);
        setOnClickListener(new h(this));
    }

    public void switchConnectModel(byte b, String str) {
        if (b >= 0 && b <= 1 && !TextUtils.isEmpty(str)) {
            this.cur_connect = b;
            switch (b) {
                case 0:
                    this.view.setBackgroundResource(R.color.transparent);
                    this.connect_pc_icon.setImageResource(R.drawable.common_manage_backup_icon_usb);
                    this.left_arrow.setVisibility(4);
                    break;
                case 1:
                    this.view.setBackgroundResource(R.color.transparent);
                    this.connect_pc_icon.setImageResource(R.drawable.common_manage_backup_icon_computer);
                    this.left_arrow.setVisibility(0);
                    break;
            }
            this.connect_pc_name.setText(str);
        }
    }
}
