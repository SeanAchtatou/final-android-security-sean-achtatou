package com.tencent.assistant.component.dialog;

import android.app.Dialog;
import android.view.View;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.TwoBtnDialogInfo f648a;
    final /* synthetic */ Dialog b;

    b(AppConst.TwoBtnDialogInfo twoBtnDialogInfo, Dialog dialog) {
        this.f648a = twoBtnDialogInfo;
        this.b = dialog;
    }

    public void onClick(View view) {
        if (this.f648a != null) {
            this.f648a.onRightBtnClick();
            this.b.dismiss();
        }
    }
}
