package com.tencent.assistant.component.txscrollview;

import android.view.View;
import com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
class e implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TXGetMoreListView f707a;

    e(TXGetMoreListView tXGetMoreListView) {
        this.f707a = tXGetMoreListView;
    }

    public void onClick(View view) {
        if (this.f707a.k != null && this.f707a.f691a == TXRefreshScrollViewBase.RefreshState.RESET) {
            this.f707a.k.onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState.ScrollState_FromEnd);
        }
    }
}
