package com.tencent.assistant.component;

import android.content.Context;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.module.k;
import com.tencent.assistant.utils.ah;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/* compiled from: ProGuard */
public class DownloadProgressButton extends RelativeLayout implements UIEventListener {
    public static final String TMA_ST_NAVBAR_DOWNLOAD_TAG = "02_001";
    private Context context;
    /* access modifiers changed from: private */
    public TextView downCountText;
    /* access modifiers changed from: private */
    public ImageView downImage;
    private Set<String> downloadingSet = new HashSet();
    private int lastDownloadingSetCount = 0;
    private int lastPauseSetCount = 0;
    private Set<String> pauseSet = new HashSet();

    public DownloadProgressButton(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        this.context = context2;
        LayoutInflater.from(context2).inflate((int) R.layout.download_progress_button_layout, this);
        this.downImage = (ImageView) findViewById(R.id.down_arrow_img);
        this.downCountText = (TextView) findViewById(R.id.down_task_count);
        setTag(R.id.tma_st_slot_tag, TMA_ST_NAVBAR_DOWNLOAD_TAG);
        setOnClickListener(new j(this, context2));
    }

    public void onResume() {
        if (DownloadProxy.a().j()) {
            init();
            updateView(false);
        }
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, this);
        AstApp.i().k().addUIEventListener(1007, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY, this);
    }

    public void onPause() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, this);
        AstApp.i().k().removeUIEventListener(1007, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY, this);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void handleUIEvent(Message message) {
        boolean z;
        DownloadInfo d;
        if (DownloadProxy.a().j()) {
            String str = Constants.STR_EMPTY;
            if (message.obj instanceof String) {
                str = (String) message.obj;
                if (TextUtils.isEmpty(str) || (d = DownloadProxy.a().d(str)) == null || d.isUiTypeWiseDownload()) {
                    return;
                }
            }
            switch (message.what) {
                case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START /*1004*/:
                    this.downloadingSet.add(str);
                    this.pauseSet.remove(str);
                    z = true;
                    break;
                case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE /*1005*/:
                    this.pauseSet.add(str);
                    this.downloadingSet.remove(str);
                    z = true;
                    break;
                case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC /*1006*/:
                    this.pauseSet.remove(str);
                    this.downloadingSet.remove(str);
                    z = true;
                    break;
                case 1007:
                    this.pauseSet.add(str);
                    this.downloadingSet.remove(str);
                    z = true;
                    break;
                case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING /*1008*/:
                    this.pauseSet.add(str);
                    this.downloadingSet.remove(str);
                    z = true;
                    break;
                case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE /*1009*/:
                    DownloadInfo downloadInfo = (DownloadInfo) message.obj;
                    if (downloadInfo != null) {
                        this.pauseSet.remove(downloadInfo.downloadTicket);
                        this.downloadingSet.remove(downloadInfo.downloadTicket);
                        z = true;
                        break;
                    }
                    z = true;
                    break;
                case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD /*1015*/:
                    DownloadInfo downloadInfo2 = (DownloadInfo) message.obj;
                    if (downloadInfo2 != null) {
                        if (!downloadInfo2.isUiTypeWiseDownload()) {
                            if (k.b(downloadInfo2) == AppConst.AppState.DOWNLOADING) {
                                this.downloadingSet.add(downloadInfo2.downloadTicket);
                            } else {
                                this.pauseSet.add(downloadInfo2.downloadTicket);
                            }
                            z = true;
                            break;
                        } else {
                            return;
                        }
                    }
                    z = true;
                    break;
                case EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY /*1038*/:
                    init();
                    z = false;
                    break;
                default:
                    z = true;
                    break;
            }
            updateView(z);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>
     arg types: [com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, int]
     candidates:
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, android.app.Dialog):android.app.Dialog
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, com.tencent.assistant.net.APN):com.tencent.assistant.net.APN
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager):com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>):java.lang.String
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.DownloadInfo, com.tencent.pangu.download.SimpleDownloadInfo$DownloadState):void
      com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, int):com.tencent.pangu.download.DownloadInfo
      com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, boolean):void
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.pangu.download.DownloadInfo> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.pangu.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.plugin.PluginDownloadInfo, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.k.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.m, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    private void init() {
        ArrayList<DownloadInfo> a2 = DownloadProxy.a().a(SimpleDownloadInfo.DownloadType.APK, true);
        this.downloadingSet.clear();
        this.pauseSet.clear();
        if (a2 != null && a2.size() > 0) {
            for (DownloadInfo next : a2) {
                if (next.uiType != SimpleDownloadInfo.UIType.WISE_APP_UPDATE) {
                    switch (l.f677a[k.a(next, true, true).ordinal()]) {
                        case 1:
                        case 2:
                            this.downloadingSet.add(next.downloadTicket);
                            break;
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                            this.pauseSet.add(next.downloadTicket);
                            break;
                    }
                } else {
                    return;
                }
            }
        }
    }

    public void updateView(boolean z) {
        if (this.downloadingSet.size() != this.lastDownloadingSetCount || this.pauseSet.size() != this.lastPauseSetCount) {
            this.lastDownloadingSetCount = this.downloadingSet.size();
            this.lastPauseSetCount = this.pauseSet.size();
            if (this.downloadingSet.size() > 0 || this.pauseSet.size() > 0) {
                int displayCount = getDisplayCount();
                if (z) {
                    ah.a().postDelayed(new k(this, displayCount), 800);
                    return;
                }
                this.downCountText.setText(String.valueOf(displayCount));
                this.downCountText.setVisibility(0);
                this.downImage.setImageResource(R.drawable.icon_xiazai_empty);
                return;
            }
            this.downCountText.setVisibility(8);
            this.downImage.setImageResource(R.drawable.icon_download);
        }
    }

    /* access modifiers changed from: private */
    public int getDisplayCount() {
        int size = this.downloadingSet.size() + this.pauseSet.size();
        if (size > 99) {
            return 99;
        }
        return size;
    }
}
