package com.tencent.assistant.component.invalidater;

/* compiled from: ProGuard */
public class ViewInvalidateMessage {
    public int arg1;
    public int arg2;
    public Object params;
    public ViewInvalidateMessageHandler target;
    public int what;

    public ViewInvalidateMessage(int i) {
        this.what = i;
    }

    public ViewInvalidateMessage(int i, Object obj, ViewInvalidateMessageHandler viewInvalidateMessageHandler) {
        this.what = i;
        this.params = obj;
        this.target = viewInvalidateMessageHandler;
    }

    public String toString() {
        return "ViewInvalidateMessage{what=" + this.what + ", arg1=" + this.arg1 + ", arg2=" + this.arg2 + ", params=" + this.params + '}';
    }
}
