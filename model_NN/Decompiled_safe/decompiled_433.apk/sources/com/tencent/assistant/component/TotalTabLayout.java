package com.tencent.assistant.component;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.r;

/* compiled from: ProGuard */
public class TotalTabLayout {
    public static final int LAYOUTANIMATION = 1;
    public static final int STARTANIMATION = 0;
    public static final int START_ID_INDEX = 136;
    static final String TAG = TotalTabLayout.class.getSimpleName();
    public Handler animationHandler = new an(this);
    private int lastTabId = -1;
    private TextView mAnimImage;
    int mAnimImageWidth = 0;
    /* access modifiers changed from: private */
    public Context mContext;
    int mCurSkin = -1;
    /* access modifiers changed from: private */
    public int mCurrentTabIndex = -1;
    boolean mIsAniming = false;
    boolean mIsFirst = true;
    boolean mIsLayoutFirst = true;
    int mLastAnimIndex = -1;
    float mLastToX = 0.0f;
    private View mRelLayout;
    private View[] mTabArray;
    private LinearLayout mTabLayout;
    /* access modifiers changed from: private */
    public int[] mTabStrArray;
    /* access modifiers changed from: private */
    public View.OnClickListener viewClickListener;

    public void selectTab(int i, boolean z) {
        int i2 = 0;
        if (this.lastTabId != i) {
            Message obtain = Message.obtain();
            obtain.arg1 = this.lastTabId;
            obtain.arg2 = i;
            obtain.what = 0;
            this.animationHandler.sendMessageDelayed(obtain, 0);
            this.lastTabId = i;
            while (true) {
                int i3 = i2;
                if (i3 < this.mTabArray.length) {
                    TextView textView = (TextView) this.mTabArray[i3].findViewById(R.id.total_tab_produce);
                    if (i3 == i) {
                        textView.setTextColor(this.mContext.getResources().getColor(R.color.second_tab_selected_color));
                    } else {
                        textView.setTextColor(this.mContext.getResources().getColor(R.color.second_tab_unselected_color));
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public void init(int i) {
        initAnimationViewLayout(i);
        selectTab(i, false);
        aq aqVar = new aq(this, null);
        for (int i2 = 0; i2 < this.mTabStrArray.length; i2++) {
            View tabView = getTabView(i2);
            if (tabView != null) {
                tabView.setId(i2 + START_ID_INDEX);
                tabView.setOnClickListener(aqVar);
            }
        }
    }

    public void setTabClickListener(View.OnClickListener onClickListener) {
        this.viewClickListener = onClickListener;
    }

    public int getcurrentIndex() {
        return this.mCurrentTabIndex;
    }

    public TotalTabLayout(Context context, int[] iArr) {
        if (iArr == null || iArr.length == 0) {
            this.mTabArray = new View[1];
        } else {
            this.mTabArray = new View[iArr.length];
        }
        this.mTabStrArray = iArr;
        this.mContext = context;
        initTotalTab();
    }

    public void initTotalTab() {
        LayoutInflater layoutInflater = (LayoutInflater) this.mContext.getSystemService("layout_inflater");
        this.mRelLayout = layoutInflater.inflate((int) R.layout.total_tab_custom, (ViewGroup) null);
        this.mTabLayout = (LinearLayout) this.mRelLayout.findViewById(R.id.navigate_layout);
        this.mAnimImage = (TextView) this.mRelLayout.findViewById(R.id.animation_image);
        ViewGroup.LayoutParams layoutParams = this.mAnimImage.getLayoutParams();
        if (this.mTabArray.length == 2) {
            this.mAnimImageWidth = this.mContext.getResources().getDimensionPixelSize(R.dimen.total_tab_animation_tv_length_two);
        } else {
            this.mAnimImageWidth = this.mContext.getResources().getDimensionPixelSize(R.dimen.total_tab_animation_tv_length_three);
        }
        layoutParams.width = this.mAnimImageWidth;
        this.mAnimImage.setLayoutParams(layoutParams);
        if (this.mTabStrArray == null || this.mTabStrArray.length <= 1) {
            View inflate = layoutInflater.inflate((int) R.layout.total_tab_item, (ViewGroup) null);
            if (this.mTabStrArray != null && this.mTabStrArray.length == 1) {
                ((TextView) inflate.findViewById(R.id.total_tab_produce)).setText(this.mTabStrArray[0]);
            }
            this.mTabArray[0] = inflate;
            this.mTabLayout.setWeightSum(1.0f);
            this.mTabLayout.addView(inflate);
            return;
        }
        this.mTabLayout.setWeightSum((float) this.mTabStrArray.length);
        for (int i = 0; i < this.mTabStrArray.length; i++) {
            View inflate2 = layoutInflater.inflate((int) R.layout.total_tab_item, (ViewGroup) null);
            ((TextView) inflate2.findViewById(R.id.total_tab_produce)).setText(this.mTabStrArray[i]);
            this.mTabLayout.addView(inflate2);
            inflate2.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
            this.mTabArray[i] = inflate2;
            ((ImageView) inflate2.findViewById(R.id.cut_line_img)).setVisibility(4);
        }
    }

    public void initAnimationViewLayout(int i) {
        float b = (float) (r.b() / this.mTabStrArray.length);
        this.mLastToX = ((float) i) * b;
        this.mCurrentTabIndex = i;
        if (this.mAnimImage != null) {
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.mAnimImage.getLayoutParams();
            layoutParams.width = (int) b;
            this.mAnimImage.setLayoutParams(layoutParams);
            this.mAnimImage.setVisibility(0);
        }
    }

    public View getTabView(int i) {
        if (i >= 0 && this.mTabArray.length >= i + 1) {
            return this.mTabArray[i];
        }
        return null;
    }

    public View getTabLayout() {
        return this.mRelLayout;
    }

    public void reLayout(int i) {
        int b = (int) (((float) (r.b() / this.mTabStrArray.length)) * ((float) i));
        this.mAnimImage.layout(b, this.mAnimImage.getTop(), this.mAnimImage.getWidth() + b, this.mAnimImage.getBottom());
        getTabLayout().invalidate();
    }

    public void startTranslateAnimation(int i, int i2) {
        this.mLastAnimIndex = i2;
        if (!this.mIsAniming) {
            if (this.mIsFirst) {
                this.mIsFirst = false;
                if (this.mAnimImage != null && ((FrameLayout.LayoutParams) this.mAnimImage.getLayoutParams()).leftMargin == 0) {
                    startTranslateAnimation(-1, this.mCurrentTabIndex);
                    return;
                }
                return;
            }
            if (this.mAnimImage != null) {
                FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.mAnimImage.getLayoutParams();
                layoutParams.leftMargin = 0;
                this.mAnimImage.setLayoutParams(layoutParams);
            }
            this.mIsAniming = true;
            float b = (float) (r.b() / this.mTabStrArray.length);
            float f = ((float) i2) * b;
            float f2 = b * ((float) i);
            if (f2 < 0.0f) {
                f2 = 0.0f;
            }
            this.mLastToX = f;
            TranslateAnimation translateAnimation = new TranslateAnimation(f2, f, 0.0f, 0.0f);
            translateAnimation.setAnimationListener(new ao(this));
            translateAnimation.setFillEnabled(true);
            translateAnimation.setFillAfter(true);
            translateAnimation.setDuration(300);
            if (this.mAnimImage != null) {
                this.mAnimImage.startAnimation(translateAnimation);
            }
            this.mCurrentTabIndex = i2;
        }
    }

    public void layoutAnimImageOnResume() {
        if (this.mAnimImage == null || this.mIsAniming || this.mIsLayoutFirst) {
            this.mIsLayoutFirst = false;
            return;
        }
        this.mAnimImage.setVisibility(0);
        float b = ((float) this.mCurrentTabIndex) * ((float) (r.b() / this.mTabStrArray.length));
        if (((double) Math.abs(this.mLastToX - b)) >= 0.01d) {
            if (this.mAnimImage != null) {
                FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.mAnimImage.getLayoutParams();
                layoutParams.leftMargin = 0;
                this.mAnimImage.setLayoutParams(layoutParams);
            }
            this.mIsAniming = true;
            TranslateAnimation translateAnimation = new TranslateAnimation(this.mLastToX, b, 0.0f, 0.0f);
            this.mLastToX = b;
            translateAnimation.setAnimationListener(new ap(this));
            translateAnimation.setFillEnabled(true);
            translateAnimation.setFillAfter(true);
            translateAnimation.setDuration(300);
            this.mAnimImage.startAnimation(translateAnimation);
        }
    }

    public void isNeedLayoutAnim() {
        if (!this.mIsAniming && this.mAnimImage != null) {
            int i = 0;
            if (this.mAnimImage != null) {
                i = this.mAnimImage.getLeft();
            }
            int b = r.b();
            if (i <= 0 || i >= b) {
                layoutAnimImageOnResume();
            }
        }
    }

    public void destroy() {
        this.mTabLayout = null;
        if (this.mTabArray != null) {
            for (int i = 0; i < this.mTabArray.length; i++) {
                this.mTabArray[i] = null;
            }
            this.mTabArray = null;
        }
        this.mContext = null;
        this.mAnimImage = null;
        this.mRelLayout = null;
    }

    public void setTextSize(float f) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.mTabArray.length) {
                ((TextView) this.mTabArray[i2].findViewById(R.id.total_tab_produce)).setTextSize(f);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void updateTextValue(String str, String str2) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.mTabArray.length) {
                TextView textView = (TextView) this.mTabArray[i2].findViewById(R.id.total_tab_produce);
                if (textView.getText() != null && textView.getText().toString().equals(str)) {
                    textView.setText(str2);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }
}
