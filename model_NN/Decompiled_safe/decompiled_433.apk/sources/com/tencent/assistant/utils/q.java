package com.tencent.assistant.utils;

import android.text.TextUtils;
import com.tencent.connect.common.Constants;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: ProGuard */
public class q implements ThreadFactory {

    /* renamed from: a  reason: collision with root package name */
    private static final AtomicInteger f1848a = new AtomicInteger(1);
    private final ThreadGroup b;
    private final AtomicInteger c = new AtomicInteger(1);
    private final String d;

    public q(String str) {
        SecurityManager securityManager = System.getSecurityManager();
        this.b = securityManager != null ? securityManager.getThreadGroup() : Thread.currentThread().getThreadGroup();
        this.d = "pool-" + f1848a.getAndIncrement() + (TextUtils.isEmpty(str) ? Constants.STR_EMPTY : "-" + str) + "-thread-";
    }

    public Thread newThread(Runnable runnable) {
        Thread thread = new Thread(this.b, runnable, this.d + this.c.getAndIncrement(), 0);
        if (thread.isDaemon()) {
            thread.setDaemon(false);
        }
        if (thread.getPriority() != 1) {
            thread.setPriority(1);
        }
        return thread;
    }
}
