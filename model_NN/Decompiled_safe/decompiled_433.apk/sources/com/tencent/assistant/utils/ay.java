package com.tencent.assistant.utils;

import android.content.res.AssetManager;
import android.content.res.XmlResourceParser;
import android.os.Environment;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.manager.ak;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import org.json.JSONArray;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;

/* compiled from: ProGuard */
public class ay {

    /* renamed from: a  reason: collision with root package name */
    private static XmlResourceParser f1827a = null;
    private static Method b = null;
    private static Constructor<AssetManager> c = null;
    private static Class<?> d = null;
    private static Field e = null;
    private static int[] f = null;
    private static Field g = null;
    private static DisplayMetrics h = null;
    private static TreeSet<HashMap<String, String>> i = new TreeSet<>(new ba());
    private static long j = 0;
    private static boolean k = true;

    /* JADX WARNING: Removed duplicated region for block: B:26:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00e6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.tencent.assistant.utils.bb a(java.lang.String r10) {
        /*
            r4 = 0
            r3 = 1
            r2 = 0
            com.tencent.assistant.utils.bb r5 = new com.tencent.assistant.utils.bb
            r5.<init>()
            boolean r0 = b(r10)
            if (r0 != 0) goto L_0x0028
            java.lang.String r0 = "ThemeUtils"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r1 = r1.append(r10)
            java.lang.String r2 = " is not apk file"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.i(r0, r1)
            r0 = r5
        L_0x0027:
            return r0
        L_0x0028:
            java.lang.reflect.Constructor<android.content.res.AssetManager> r0 = com.tencent.assistant.utils.ay.c     // Catch:{ Exception -> 0x00c8 }
            if (r0 != 0) goto L_0x0037
            java.lang.Class<android.content.res.AssetManager> r0 = android.content.res.AssetManager.class
            r1 = 0
            java.lang.Class[] r1 = new java.lang.Class[r1]     // Catch:{ Exception -> 0x00c8 }
            java.lang.reflect.Constructor r0 = r0.getDeclaredConstructor(r1)     // Catch:{ Exception -> 0x00c8 }
            com.tencent.assistant.utils.ay.c = r0     // Catch:{ Exception -> 0x00c8 }
        L_0x0037:
            java.lang.reflect.Method r0 = com.tencent.assistant.utils.ay.b     // Catch:{ Exception -> 0x00c8 }
            if (r0 != 0) goto L_0x0053
            java.lang.Class<android.content.res.AssetManager> r0 = android.content.res.AssetManager.class
            java.lang.String r1 = "addAssetPath"
            r6 = 1
            java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Exception -> 0x00c8 }
            r7 = 0
            java.lang.Class<java.lang.String> r8 = java.lang.String.class
            r6[r7] = r8     // Catch:{ Exception -> 0x00c8 }
            java.lang.reflect.Method r0 = r0.getDeclaredMethod(r1, r6)     // Catch:{ Exception -> 0x00c8 }
            com.tencent.assistant.utils.ay.b = r0     // Catch:{ Exception -> 0x00c8 }
            java.lang.reflect.Method r0 = com.tencent.assistant.utils.ay.b     // Catch:{ Exception -> 0x00c8 }
            r1 = 1
            r0.setAccessible(r1)     // Catch:{ Exception -> 0x00c8 }
        L_0x0053:
            android.util.DisplayMetrics r0 = com.tencent.assistant.utils.ay.h     // Catch:{ Exception -> 0x00c8 }
            if (r0 != 0) goto L_0x0063
            android.util.DisplayMetrics r0 = new android.util.DisplayMetrics     // Catch:{ Exception -> 0x00c8 }
            r0.<init>()     // Catch:{ Exception -> 0x00c8 }
            com.tencent.assistant.utils.ay.h = r0     // Catch:{ Exception -> 0x00c8 }
            android.util.DisplayMetrics r0 = com.tencent.assistant.utils.ay.h     // Catch:{ Exception -> 0x00c8 }
            r0.setToDefaults()     // Catch:{ Exception -> 0x00c8 }
        L_0x0063:
            java.lang.reflect.Constructor<android.content.res.AssetManager> r0 = com.tencent.assistant.utils.ay.c     // Catch:{ Exception -> 0x00c8 }
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x00c8 }
            java.lang.Object r0 = r0.newInstance(r1)     // Catch:{ Exception -> 0x00c8 }
            android.content.res.AssetManager r0 = (android.content.res.AssetManager) r0     // Catch:{ Exception -> 0x00c8 }
            java.lang.reflect.Method r1 = com.tencent.assistant.utils.ay.b     // Catch:{ Exception -> 0x0107 }
            if (r1 == 0) goto L_0x0111
            java.lang.reflect.Method r1 = com.tencent.assistant.utils.ay.b     // Catch:{ Exception -> 0x0107 }
            r6 = 1
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x0107 }
            r7 = 0
            r6[r7] = r10     // Catch:{ Exception -> 0x0107 }
            java.lang.Object r1 = r1.invoke(r0, r6)     // Catch:{ Exception -> 0x0107 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ Exception -> 0x0107 }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x0107 }
            r6 = r1
        L_0x0085:
            if (r6 == 0) goto L_0x00ad
            if (r0 == 0) goto L_0x00ad
            android.content.res.Resources r1 = new android.content.res.Resources     // Catch:{ Exception -> 0x0107 }
            android.util.DisplayMetrics r7 = com.tencent.assistant.utils.ay.h     // Catch:{ Exception -> 0x0107 }
            r8 = 0
            r1.<init>(r0, r7, r8)     // Catch:{ Exception -> 0x0107 }
            java.lang.String r4 = "AndroidManifest.xml"
            android.content.res.XmlResourceParser r4 = r0.openXmlResourceParser(r6, r4)     // Catch:{ Exception -> 0x010c }
            com.tencent.assistant.utils.ay.f1827a = r4     // Catch:{ Exception -> 0x010c }
            r9 = r2
            r2 = r1
            r1 = r9
        L_0x009c:
            if (r1 == 0) goto L_0x00e6
            if (r0 == 0) goto L_0x00a3
            r0.close()
        L_0x00a3:
            java.lang.String r0 = "ThemeUtils"
            java.lang.String r1 = "assetError"
            android.util.Log.i(r0, r1)
            r0 = r5
            goto L_0x0027
        L_0x00ad:
            java.lang.String r1 = "ThemeUtils"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0107 }
            r2.<init>()     // Catch:{ Exception -> 0x0107 }
            java.lang.String r6 = "Failed adding asset path:"
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Exception -> 0x0107 }
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ Exception -> 0x0107 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0107 }
            android.util.Log.i(r1, r2)     // Catch:{ Exception -> 0x0107 }
            r1 = r3
            r2 = r4
            goto L_0x009c
        L_0x00c8:
            r0 = move-exception
            r1 = r4
        L_0x00ca:
            java.lang.String r2 = "ThemeUtils"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "Unable to read AndroidManifest.xml of "
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r6 = r6.append(r10)
            java.lang.String r6 = r6.toString()
            android.util.Log.i(r2, r6, r0)
            r2 = r4
            r0 = r1
            r1 = r3
            goto L_0x009c
        L_0x00e6:
            android.content.res.XmlResourceParser r1 = com.tencent.assistant.utils.ay.f1827a
            android.content.res.XmlResourceParser r3 = com.tencent.assistant.utils.ay.f1827a
            java.lang.String r3 = a(r3, r1)
            r5.f1830a = r3
            int r1 = a(r2, r1)
            r5.b = r1
            android.content.res.XmlResourceParser r1 = com.tencent.assistant.utils.ay.f1827a
            if (r1 == 0) goto L_0x00ff
            android.content.res.XmlResourceParser r1 = com.tencent.assistant.utils.ay.f1827a
            r1.close()
        L_0x00ff:
            if (r0 == 0) goto L_0x0104
            r0.close()
        L_0x0104:
            r0 = r5
            goto L_0x0027
        L_0x0107:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x00ca
        L_0x010c:
            r2 = move-exception
            r4 = r1
            r1 = r0
            r0 = r2
            goto L_0x00ca
        L_0x0111:
            r6 = r2
            goto L_0x0085
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.ay.a(java.lang.String):com.tencent.assistant.utils.bb");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0079, code lost:
        if (r2 != null) goto L_0x007b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x007b, code lost:
        r2.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x007e, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0086, code lost:
        if (r2 == null) goto L_0x007e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x008c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(android.content.res.Resources r5, android.util.AttributeSet r6) {
        /*
            r3 = 0
            r1 = 0
            java.lang.Class<?> r0 = com.tencent.assistant.utils.ay.d     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
            if (r0 != 0) goto L_0x000e
            java.lang.String r0 = "com.android.internal.R$styleable"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
            com.tencent.assistant.utils.ay.d = r0     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
        L_0x000e:
            java.lang.reflect.Field r0 = com.tencent.assistant.utils.ay.e     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
            if (r0 != 0) goto L_0x0022
            java.lang.Class<?> r0 = com.tencent.assistant.utils.ay.d     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
            java.lang.String r2 = "AndroidManifest"
            java.lang.reflect.Field r0 = r0.getDeclaredField(r2)     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
            com.tencent.assistant.utils.ay.e = r0     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
            java.lang.reflect.Field r0 = com.tencent.assistant.utils.ay.e     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
            r2 = 1
            r0.setAccessible(r2)     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
        L_0x0022:
            int[] r0 = com.tencent.assistant.utils.ay.f     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
            if (r0 != 0) goto L_0x0034
            java.lang.reflect.Field r0 = com.tencent.assistant.utils.ay.e     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
            java.lang.Class<?> r2 = com.tencent.assistant.utils.ay.d     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
            int[] r0 = (int[]) r0     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
            int[] r0 = (int[]) r0     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
            com.tencent.assistant.utils.ay.f = r0     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
        L_0x0034:
            java.lang.reflect.Field r0 = com.tencent.assistant.utils.ay.g     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
            if (r0 != 0) goto L_0x0048
            java.lang.Class<?> r0 = com.tencent.assistant.utils.ay.d     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
            java.lang.String r2 = "AndroidManifest_versionCode"
            java.lang.reflect.Field r0 = r0.getDeclaredField(r2)     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
            com.tencent.assistant.utils.ay.g = r0     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
            java.lang.reflect.Field r0 = com.tencent.assistant.utils.ay.g     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
            r2 = 1
            r0.setAccessible(r2)     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
        L_0x0048:
            int[] r0 = com.tencent.assistant.utils.ay.f     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
            android.content.res.TypedArray r2 = r5.obtainAttributes(r6, r0)     // Catch:{ Exception -> 0x007f, all -> 0x0089 }
            java.lang.reflect.Field r0 = com.tencent.assistant.utils.ay.g     // Catch:{ Exception -> 0x0093 }
            java.lang.Class<?> r1 = com.tencent.assistant.utils.ay.d     // Catch:{ Exception -> 0x0093 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Exception -> 0x0093 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0093 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0093 }
            r1 = 0
            int r0 = r2.getInteger(r0, r1)     // Catch:{ Exception -> 0x0093 }
            java.lang.String r1 = "ThemeUtils"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0097 }
            r3.<init>()     // Catch:{ Exception -> 0x0097 }
            java.lang.String r4 = "[parsePackageName] ---> versionCode = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0097 }
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0097 }
            android.util.Log.i(r1, r3)     // Catch:{ Exception -> 0x0097 }
            if (r2 == 0) goto L_0x007e
        L_0x007b:
            r2.recycle()
        L_0x007e:
            return r0
        L_0x007f:
            r0 = move-exception
            r2 = r1
            r1 = r0
            r0 = r3
        L_0x0083:
            r1.printStackTrace()     // Catch:{ all -> 0x0090 }
            if (r2 == 0) goto L_0x007e
            goto L_0x007b
        L_0x0089:
            r0 = move-exception
        L_0x008a:
            if (r1 == 0) goto L_0x008f
            r1.recycle()
        L_0x008f:
            throw r0
        L_0x0090:
            r0 = move-exception
            r1 = r2
            goto L_0x008a
        L_0x0093:
            r0 = move-exception
            r1 = r0
            r0 = r3
            goto L_0x0083
        L_0x0097:
            r1 = move-exception
            goto L_0x0083
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.ay.a(android.content.res.Resources, android.util.AttributeSet):int");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.ay.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.utils.ay.a(android.content.res.Resources, android.util.AttributeSet):int
      com.tencent.assistant.utils.ay.a(java.util.Set<java.util.HashMap<java.lang.String, java.lang.String>>, java.lang.String):java.lang.String
      com.tencent.assistant.utils.ay.a(org.xmlpull.v1.XmlPullParser, android.util.AttributeSet):java.lang.String
      com.tencent.assistant.utils.ay.a(java.lang.String, boolean):java.lang.String */
    private static String a(XmlPullParser xmlPullParser, AttributeSet attributeSet) {
        int next;
        do {
            next = xmlPullParser.next();
            if (next == 2) {
                break;
            }
        } while (next != 1);
        if (next != 2) {
            Log.i("ThemeUtils", "[parsePackageName] ---> errMsg : No start tag found");
            return null;
        } else if (!xmlPullParser.getName().equals("manifest")) {
            Log.i("ThemeUtils", "[parsePackageName] ---> errMsg : No <manifest> tag");
            return null;
        } else {
            String attributeValue = attributeSet.getAttributeValue(null, "package");
            if (attributeValue == null || attributeValue.length() == 0) {
                Log.i("ThemeUtils", "[parsePackageName] ---> errMsg : <manifest> does not specify package");
                return null;
            }
            String a2 = a(attributeValue, true);
            if (a2 == null || "android".equals(attributeValue)) {
                return attributeValue.intern();
            }
            Log.i("ThemeUtils", "[parsePackageName] ---> errMsg : <manifest> specifies bad package name \"" + attributeValue + "\": " + a2);
            return null;
        }
    }

    private static String a(String str, boolean z) {
        int length = str.length();
        boolean z2 = true;
        boolean z3 = false;
        for (int i2 = 0; i2 < length; i2++) {
            char charAt = str.charAt(i2);
            if ((charAt >= 'a' && charAt <= 'z') || (charAt >= 'A' && charAt <= 'Z')) {
                z2 = false;
            } else if (z2 || ((charAt < '0' || charAt > '9') && charAt != '_')) {
                if (charAt != '.') {
                    return "bad character '" + charAt + "'";
                }
                z2 = true;
                z3 = true;
            }
        }
        if (z3 || !z) {
            return null;
        }
        return "must have at least one '.' separator";
    }

    public static final boolean b(String str) {
        return !TextUtils.isEmpty(str) && str.endsWith(".apk");
    }

    public static final String a(Set<HashMap<String, String>> set, String str) {
        if (set == null) {
            return Constants.STR_EMPTY;
        }
        JSONArray jSONArray = new JSONArray();
        JSONObject jSONObject = new JSONObject();
        int size = set.size();
        Iterator<HashMap<String, String>> it = set.iterator();
        while (it != null && it.hasNext()) {
            JSONObject jSONObject2 = new JSONObject();
            HashMap next = it.next();
            String str2 = (String) next.get("packageName");
            jSONObject2.put("packageName", str2);
            jSONObject2.put("versionCode", (String) next.get("versionCode"));
            if (TextUtils.isEmpty(str) || !str.equals(str2)) {
                jSONObject2.put("useState", 0);
            } else {
                jSONObject2.put("useState", 1);
            }
            jSONArray.put(jSONObject2);
        }
        for (int i2 = 0; i2 < size; i2++) {
        }
        jSONObject.put("LocalQubeThemeSize", size);
        jSONObject.put("LocalQubeThemeList", jSONArray);
        return jSONObject.toString();
    }

    public static boolean c(String str) {
        return ak.a().d(str);
    }

    public static int a(HashMap<String, String> hashMap) {
        if (hashMap == null) {
            Log.e("ThemeUtils", "[installQubeTheme] ---> param is null");
            return 1;
        }
        String str = hashMap.get("themeFilePath");
        String str2 = hashMap.get("packageName");
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            Log.e("ThemeUtils", "[installQubeTheme] ---> strThemeFilePath is null or strThemePkgName is null");
            return 2;
        }
        try {
            File file = new File(str);
            if (!file.exists()) {
                Log.e("ThemeUtils", "[installQubeTheme] ---> Theme file not exist");
                return 3;
            }
            String c2 = c();
            if (c2 == null) {
                return 4;
            }
            File file2 = new File(c2);
            if (!file2.exists()) {
                file2.mkdirs();
            }
            String str3 = c2 + File.separator + file.getName();
            File file3 = new File(str3);
            if (!file3.exists()) {
                file3.createNewFile();
                if (FileUtil.copy(file.getAbsolutePath(), str3)) {
                    XLog.i("ThemeUtils", "Theme copy succeed!");
                } else {
                    XLog.i("ThemeUtils", "Theme copy failed!");
                    if (!file3.exists()) {
                        XLog.i("ThemeUtils", "Theme copy failed ---> file is not exist!");
                    }
                }
            }
            return 0;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private static String c() {
        if (FileUtil.isSDCardExistAndCanWrite()) {
            return Environment.getExternalStorageDirectory().getPath() + "/tencent/qlauncher_themes";
        }
        return null;
    }

    public static final boolean d(String str) {
        return ak.a().c(str);
    }

    private static TreeSet<HashMap<String, String>> a(File file) {
        XLog.i("ThemeUtils", "[scanAllQubeThemesInApkDir] ---> begin");
        if (i == null) {
            i = new TreeSet<>(new ba());
        }
        if (file != null) {
            i.clear();
            for (File file2 : file.listFiles()) {
                if (file2 != null) {
                    String absolutePath = file2.getAbsolutePath();
                    XLog.i("ThemeUtils", absolutePath);
                    HashMap hashMap = new HashMap();
                    if (absolutePath != null && b(absolutePath)) {
                        try {
                            long currentTimeMillis = System.currentTimeMillis();
                            bb a2 = a(absolutePath);
                            hashMap.put("lastModifiedTime", String.valueOf(file2.lastModified()));
                            hashMap.put("packageName", a2.f1830a);
                            hashMap.put("versionCode", String.valueOf(a2.b));
                            hashMap.put("themeFilePath", absolutePath);
                            XLog.i("ThemeUtils", "time : " + (System.currentTimeMillis() - currentTimeMillis));
                            XLog.e("ThemeUtils", "packageName : " + a2.f1830a + ", versionCode : " + a2.b);
                            if (d(a2.f1830a)) {
                                i.add(hashMap);
                            }
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }
                }
            }
        }
        XLog.i("ThemeUtils", "[scanAllQubeThemesInApkDir] ---> end");
        return i;
    }

    public static TreeSet<HashMap<String, String>> a() {
        String aPKDir = FileUtil.getAPKDir();
        XLog.i("ThemeUtils", "[getAllLocalThemes] ---> strApkDir = " + aPKDir);
        File file = new File(aPKDir);
        if (file.exists()) {
            long lastModified = file.lastModified();
            if (k || j != lastModified) {
                k = false;
                j = lastModified;
                return a(file);
            }
        }
        return i;
    }

    public static boolean b(HashMap<String, String> hashMap) {
        boolean z = false;
        if (!(i == null || hashMap == null)) {
            z = i.add(hashMap);
        }
        b();
        return z;
    }

    public static boolean c(HashMap<String, String> hashMap) {
        if (!(i == null || hashMap == null || !i.contains(hashMap))) {
            i.remove(hashMap);
        }
        b();
        return false;
    }

    public static void b() {
        XLog.i("ThemeUtils", "[setApkDirModifiedTimeRecord]");
        File file = new File(FileUtil.getAPKDir());
        if (file.exists()) {
            j = file.lastModified();
        }
        XLog.i("ThemeUtils", "[setApkDirModifiedTimeRecord] ---> lastModifiedTime = " + j);
    }
}
