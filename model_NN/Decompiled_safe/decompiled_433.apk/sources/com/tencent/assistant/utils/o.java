package com.tencent.assistant.utils;

import java.util.Comparator;

/* compiled from: ProGuard */
final class o implements Comparator<byte[]> {
    o() {
    }

    /* renamed from: a */
    public int compare(byte[] bArr, byte[] bArr2) {
        return bArr.length - bArr2.length;
    }
}
