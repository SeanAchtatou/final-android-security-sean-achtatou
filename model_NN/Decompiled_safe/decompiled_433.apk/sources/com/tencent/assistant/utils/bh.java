package com.tencent.assistant.utils;

import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.protocol.jce.DownloadButtonSpecailInfo;
import com.tencent.assistant.protocol.jce.DownloadButtonSpecailInfoList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public class bh {

    /* renamed from: a  reason: collision with root package name */
    private static bh f1834a;
    private Map<Long, DownloadButtonSpecailInfo> b = Collections.synchronizedMap(new HashMap());

    public static synchronized bh a() {
        bh bhVar;
        synchronized (bh.class) {
            if (f1834a == null) {
                f1834a = new bh();
            }
            bhVar = f1834a;
        }
        return bhVar;
    }

    public void b() {
        a(false, i.y().u());
    }

    public void a(boolean z, DownloadButtonSpecailInfoList downloadButtonSpecailInfoList) {
        if (downloadButtonSpecailInfoList != null && downloadButtonSpecailInfoList.f1218a != null && !downloadButtonSpecailInfoList.f1218a.isEmpty()) {
            if (z) {
                i.y().a(downloadButtonSpecailInfoList);
            }
            this.b.clear();
            int size = downloadButtonSpecailInfoList.f1218a.size();
            for (int i = 0; i < size; i++) {
                DownloadButtonSpecailInfo downloadButtonSpecailInfo = downloadButtonSpecailInfoList.f1218a.get(i);
                if (downloadButtonSpecailInfo != null) {
                    this.b.put(Long.valueOf(downloadButtonSpecailInfo.f1217a), downloadButtonSpecailInfo);
                }
            }
        } else if (z) {
            i.y().v();
        }
    }

    public synchronized boolean a(long j) {
        boolean z;
        DownloadButtonSpecailInfo downloadButtonSpecailInfo = this.b.get(Long.valueOf(j));
        if (downloadButtonSpecailInfo != null) {
            long currentTimeMillis = System.currentTimeMillis() / 1000;
            z = currentTimeMillis > downloadButtonSpecailInfo.e && currentTimeMillis <= downloadButtonSpecailInfo.f;
        } else {
            z = false;
        }
        return z;
    }

    public synchronized String b(long j) {
        DownloadButtonSpecailInfo downloadButtonSpecailInfo;
        downloadButtonSpecailInfo = this.b.get(Long.valueOf(j));
        return downloadButtonSpecailInfo != null ? downloadButtonSpecailInfo.d : "下载";
    }

    public synchronized int c(long j) {
        DownloadButtonSpecailInfo downloadButtonSpecailInfo;
        downloadButtonSpecailInfo = this.b.get(Long.valueOf(j));
        return (downloadButtonSpecailInfo == null || downloadButtonSpecailInfo.c != 1) ? R.color.state_normal : R.color.state_install;
    }

    public synchronized int a(int i) {
        return i == R.color.state_install ? R.drawable.state_bg_install_selector : R.drawable.state_bg_common_selector;
    }

    public synchronized int d(long j) {
        DownloadButtonSpecailInfo downloadButtonSpecailInfo;
        downloadButtonSpecailInfo = this.b.get(Long.valueOf(j));
        return (downloadButtonSpecailInfo == null || downloadButtonSpecailInfo.c != 1) ? R.drawable.btn_green_selector : R.drawable.appdetail_bar_btn_downloaded_selector_v5;
    }
}
