package com.tencent.assistant.utils;

import android.os.Process;
import java.io.ByteArrayOutputStream;

/* compiled from: ProGuard */
public class aw extends ByteArrayOutputStream {

    /* renamed from: a  reason: collision with root package name */
    private final n f1825a;

    public aw(n nVar) {
        this(nVar, Process.PROC_COMBINE);
    }

    public aw(n nVar, int i) {
        this.f1825a = nVar;
        this.buf = this.f1825a.a(Math.max(i, (int) Process.PROC_COMBINE));
    }

    public void close() {
        this.f1825a.a(this.buf);
        this.buf = null;
        super.close();
    }

    public void finalize() {
        this.f1825a.a(this.buf);
    }

    private void a(int i) {
        if (this.count + i > this.buf.length) {
            byte[] a2 = this.f1825a.a((this.count + i) * 2);
            System.arraycopy(this.buf, 0, a2, 0, this.count);
            this.f1825a.a(this.buf);
            this.buf = a2;
        }
    }

    public synchronized void write(byte[] bArr, int i, int i2) {
        a(i2);
        super.write(bArr, i, i2);
    }

    public synchronized void write(int i) {
        a(1);
        super.write(i);
    }

    public synchronized byte[] a() {
        byte[] a2;
        a2 = this.f1825a.a(this.count);
        System.arraycopy(this.buf, 0, a2, 0, this.count);
        return a2;
    }
}
