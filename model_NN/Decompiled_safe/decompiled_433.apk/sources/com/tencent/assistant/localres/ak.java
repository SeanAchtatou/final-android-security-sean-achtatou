package com.tencent.assistant.localres;

import android.text.TextUtils;
import com.tencent.assistant.localres.model.LocalApkInfo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: ProGuard */
public class ak {

    /* renamed from: a  reason: collision with root package name */
    Map<Integer, ArrayList<LocalApkInfo>> f817a = Collections.synchronizedMap(new LinkedHashMap());
    ArrayList<LocalApkInfo> b = new ArrayList<>();
    HashMap<String, LocalApkInfo> c = new HashMap<>();
    private boolean d = false;

    public void a() {
        this.f817a.clear();
        this.d = false;
    }

    public void a(LocalApkInfo localApkInfo) {
        ArrayList arrayList;
        if (localApkInfo != null) {
            ArrayList arrayList2 = this.f817a.get(2);
            if (arrayList2 == null) {
                ArrayList arrayList3 = new ArrayList();
                this.f817a.put(2, arrayList3);
                arrayList = arrayList3;
            } else {
                arrayList = arrayList2;
            }
            ArrayList arrayList4 = this.f817a.get(1);
            if (arrayList4 == null) {
                arrayList4 = new ArrayList();
                this.f817a.put(1, arrayList4);
            }
            if (localApkInfo.mInstall) {
                arrayList4.add(localApkInfo);
            } else {
                arrayList.add(localApkInfo);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.localres.ak.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, boolean):java.util.ArrayList<com.tencent.assistant.localres.model.LocalApkInfo>
     arg types: [java.util.List, int]
     candidates:
      com.tencent.assistant.localres.ak.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, java.lang.String):int
      com.tencent.assistant.localres.ak.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, java.util.List<java.lang.Integer>):void
      com.tencent.assistant.localres.ak.a(com.tencent.assistant.localres.model.LocalApkInfo, boolean):void
      com.tencent.assistant.localres.ak.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, boolean):java.util.ArrayList<com.tencent.assistant.localres.model.LocalApkInfo> */
    public void b() {
        List list = this.f817a.get(2);
        if (list == null || list.isEmpty()) {
            this.f817a.remove(2);
        } else {
            m.e(list);
            this.f817a.put(2, a((List<LocalApkInfo>) list, false));
        }
        List list2 = this.f817a.get(1);
        if (list2 == null || list2.isEmpty()) {
            this.f817a.remove(1);
            return;
        }
        m.e(list2);
        this.f817a.put(1, a((List<LocalApkInfo>) list2, true));
    }

    public void c() {
        ArrayList arrayList = this.f817a.get(3);
        if (arrayList != null) {
            arrayList.clear();
        }
        ArrayList arrayList2 = this.f817a.get(4);
        if (arrayList2 != null) {
            arrayList2.clear();
        }
        List<LocalApkInfo> list = this.f817a.get(1);
        if (list != null && !list.isEmpty()) {
            for (LocalApkInfo localApkInfo : list) {
                if (localApkInfo.mIsUpdateApk) {
                    e(localApkInfo);
                } else {
                    d(localApkInfo);
                }
            }
        }
        ArrayList<LocalApkInfo> arrayList3 = null;
        if (this.f817a.get(2) != null) {
            arrayList3 = new ArrayList<>(this.f817a.get(2));
        }
        if (arrayList3 != null && !arrayList3.isEmpty()) {
            long currentTimeMillis = System.currentTimeMillis();
            for (LocalApkInfo localApkInfo2 : arrayList3) {
                if (localApkInfo2 != null) {
                    if (currentTimeMillis - localApkInfo2.mLastModified < 2592000000L) {
                        e(localApkInfo2);
                    } else {
                        d(localApkInfo2);
                    }
                }
            }
        }
        g();
        this.c.clear();
    }

    private void g() {
        ArrayList arrayList;
        ArrayList arrayList2 = this.f817a.get(4);
        if (arrayList2 == null) {
            ArrayList arrayList3 = new ArrayList();
            this.f817a.put(4, arrayList3);
            arrayList = arrayList3;
        } else {
            arrayList = arrayList2;
        }
        ArrayList arrayList4 = new ArrayList();
        ArrayList arrayList5 = new ArrayList();
        ArrayList arrayList6 = new ArrayList();
        ArrayList arrayList7 = new ArrayList();
        Set<Map.Entry<String, LocalApkInfo>> entrySet = this.c.entrySet();
        if (entrySet != null) {
            for (Map.Entry<String, LocalApkInfo> value : entrySet) {
                LocalApkInfo localApkInfo = (LocalApkInfo) value.getValue();
                if (localApkInfo != null) {
                    if (localApkInfo.mIsUpdateApk && localApkInfo.mIsInternalDownload) {
                        arrayList4.add(localApkInfo);
                    } else if (localApkInfo.mIsUpdateApk && !localApkInfo.mIsInternalDownload) {
                        arrayList6.add(localApkInfo);
                    } else if (localApkInfo.mIsUpdateApk || !localApkInfo.mIsInternalDownload) {
                        arrayList7.add(localApkInfo);
                    } else {
                        arrayList5.add(localApkInfo);
                    }
                }
            }
        }
        arrayList.addAll(arrayList4);
        arrayList.addAll(arrayList5);
        arrayList.addAll(arrayList6);
        arrayList.addAll(arrayList7);
    }

    private void d(LocalApkInfo localApkInfo) {
        ArrayList arrayList;
        ArrayList arrayList2 = this.f817a.get(3);
        if (arrayList2 == null) {
            ArrayList arrayList3 = new ArrayList();
            this.f817a.put(3, arrayList3);
            arrayList = arrayList3;
        } else {
            arrayList = arrayList2;
        }
        if (arrayList != null && localApkInfo != null) {
            int size = arrayList.size();
            int i = 0;
            while (i < size && ((LocalApkInfo) arrayList.get(i)).mPackageName != localApkInfo.mPackageName) {
                i++;
            }
            if (i == 0) {
                arrayList.add(localApkInfo);
            } else {
                arrayList.add(i, localApkInfo);
            }
        }
    }

    private void e(LocalApkInfo localApkInfo) {
        boolean z = false;
        if (localApkInfo != null) {
            Set<String> keySet = this.c.keySet();
            if (keySet == null || !keySet.contains(localApkInfo.mPackageName)) {
                this.c.put(localApkInfo.mPackageName, localApkInfo);
                return;
            }
            LocalApkInfo localApkInfo2 = this.c.get(localApkInfo.mPackageName);
            if (localApkInfo2 != null) {
                if (localApkInfo2.mVersionCode <= localApkInfo.mVersionCode) {
                    if (localApkInfo2.mVersionCode != localApkInfo.mVersionCode) {
                        z = true;
                    } else if (localApkInfo2.mGrayVersionCode < localApkInfo.mGrayVersionCode) {
                        z = true;
                    }
                }
                if (z) {
                    this.c.put(localApkInfo.mPackageName, localApkInfo);
                    d(localApkInfo2);
                    return;
                }
                d(localApkInfo);
            }
        }
    }

    private ArrayList<LocalApkInfo> a(List<LocalApkInfo> list, boolean z) {
        ArrayList<LocalApkInfo> arrayList = new ArrayList<>();
        if (list == null || list.isEmpty()) {
            return arrayList;
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        Iterator it = new ArrayList(list).iterator();
        while (it.hasNext()) {
            LocalApkInfo localApkInfo = (LocalApkInfo) it.next();
            if (localApkInfo != null) {
                ArrayList arrayList2 = (ArrayList) linkedHashMap.get(localApkInfo.mPackageName);
                if (arrayList2 == null) {
                    arrayList2 = new ArrayList();
                    linkedHashMap.put(localApkInfo.mPackageName, arrayList2);
                }
                arrayList2.add(localApkInfo);
            }
        }
        for (String str : linkedHashMap.keySet()) {
            ArrayList arrayList3 = (ArrayList) linkedHashMap.get(str);
            if (arrayList3 != null) {
                List<LocalApkInfo> f = m.f(new ArrayList(arrayList3));
                if (f != null) {
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 >= f.size()) {
                            break;
                        }
                        a(f.get(i2), i2, z);
                        i = i2 + 1;
                    }
                }
                arrayList.addAll(f);
            }
        }
        return arrayList;
    }

    private void a(LocalApkInfo localApkInfo, int i, boolean z) {
        this.d = true;
        if (z || i != 0) {
            localApkInfo.mIsSelect = true;
            localApkInfo.mIsSmartSlect = true;
            if (i > 0) {
                localApkInfo.mIsHistory = true;
            }
        } else if (localApkInfo.mIsSelect) {
            this.d = false;
        }
    }

    public Map<Integer, ArrayList<LocalApkInfo>> d() {
        return this.f817a;
    }

    public boolean e() {
        return this.d;
    }

    public void f() {
        this.d = false;
    }

    public void a(LocalApkInfo localApkInfo, boolean z) {
        ArrayList arrayList;
        if (localApkInfo != null) {
            if (z) {
                arrayList = this.f817a.get(1);
                if (arrayList == null) {
                    arrayList = new ArrayList();
                    this.f817a.put(1, arrayList);
                }
            } else {
                arrayList = this.f817a.get(2);
                if (arrayList == null) {
                    arrayList = new ArrayList();
                    this.f817a.put(2, arrayList);
                }
            }
            arrayList.add(0, localApkInfo);
        }
    }

    public void b(LocalApkInfo localApkInfo) {
        int indexOf;
        LocalApkInfo localApkInfo2;
        if (localApkInfo != null) {
            Iterator it = new HashSet(this.f817a.keySet()).iterator();
            while (it.hasNext()) {
                ArrayList arrayList = this.f817a.get((Integer) it.next());
                if (arrayList != null && (indexOf = arrayList.indexOf(localApkInfo)) >= 0) {
                    if (!localApkInfo.mIsHistory && arrayList != null && arrayList.size() > indexOf + 1 && (localApkInfo2 = (LocalApkInfo) arrayList.get(indexOf + 1)) != null) {
                        localApkInfo2.mIsHistory = false;
                    }
                    b(arrayList, localApkInfo.mLocalFilePath);
                }
                if (arrayList == null || arrayList.isEmpty()) {
                    it.remove();
                }
            }
        }
    }

    public void c(LocalApkInfo localApkInfo) {
        ArrayList arrayList;
        if (localApkInfo != null) {
            ArrayList arrayList2 = this.f817a.get(1);
            if (arrayList2 == null) {
                ArrayList arrayList3 = new ArrayList();
                this.f817a.put(1, arrayList3);
                arrayList = arrayList3;
            } else {
                arrayList = arrayList2;
            }
            int i = 0;
            while (i < arrayList.size()) {
                LocalApkInfo localApkInfo2 = (LocalApkInfo) arrayList.get(i);
                if (localApkInfo2 == null || !localApkInfo2.getLocalApkInfoKey().equals(localApkInfo.getLocalApkInfoKey())) {
                    i++;
                } else {
                    return;
                }
            }
            ArrayList arrayList4 = new ArrayList();
            ArrayList arrayList5 = this.f817a.get(2);
            if (arrayList5 != null) {
                int a2 = a(arrayList5, localApkInfo.mLocalFilePath);
                for (int size = arrayList5.size() - 1; size > a2; size--) {
                    LocalApkInfo localApkInfo3 = (LocalApkInfo) arrayList5.get(size);
                    if (localApkInfo3.mPackageName.equals(localApkInfo.mPackageName)) {
                        arrayList4.add(Integer.valueOf(size));
                        arrayList.add(0, localApkInfo3);
                    }
                }
                arrayList4.add(Integer.valueOf(a2));
            }
            arrayList.add(0, localApkInfo);
            if (arrayList != null) {
                a(arrayList5, arrayList4);
                if (arrayList5 == null || arrayList5.isEmpty()) {
                    this.f817a.remove(2);
                }
            }
        }
    }

    private int a(List<LocalApkInfo> list, String str) {
        if (list == null || TextUtils.isEmpty(str)) {
            return -1;
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                return -1;
            }
            LocalApkInfo localApkInfo = list.get(i2);
            if (localApkInfo != null && localApkInfo.mLocalFilePath.equals(str)) {
                return i2;
            }
            i = i2 + 1;
        }
    }

    private void b(List<LocalApkInfo> list, String str) {
        if (list != null && !TextUtils.isEmpty(str)) {
            try {
                list.remove(a(list, str));
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    private void a(List<LocalApkInfo> list, List<Integer> list2) {
        if (list != null && list2 != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < list2.size()) {
                    int intValue = list2.get(i2).intValue();
                    if (intValue >= 0 && intValue < list.size()) {
                        try {
                            list.remove(intValue);
                        } catch (Throwable th) {
                            th.printStackTrace();
                        }
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }
}
