package com.tencent.assistant.localres;

import com.tencent.assistant.localres.model.LocalApkInfo;
import java.util.Comparator;

/* compiled from: ProGuard */
final class o implements Comparator<LocalApkInfo> {
    o() {
    }

    /* renamed from: a */
    public int compare(LocalApkInfo localApkInfo, LocalApkInfo localApkInfo2) {
        if (localApkInfo == null || localApkInfo.mSortKey == null || localApkInfo2 == null || localApkInfo2.mSortKey == null) {
            return 0;
        }
        int i = 0;
        while (i < localApkInfo.mSortKey.length() && i < localApkInfo2.mSortKey.length()) {
            char charAt = localApkInfo.mSortKey.charAt(i);
            char charAt2 = localApkInfo2.mSortKey.charAt(i);
            if (charAt != charAt2) {
                return charAt - charAt2;
            }
            i++;
        }
        if (i == localApkInfo.mSortKey.length()) {
            return -1;
        }
        if (i == localApkInfo2.mSortKey.length()) {
            return 1;
        }
        return 0;
    }
}
