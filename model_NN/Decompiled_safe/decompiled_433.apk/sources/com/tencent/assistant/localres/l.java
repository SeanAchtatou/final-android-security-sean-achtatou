package com.tencent.assistant.localres;

import android.content.pm.IPackageStatsObserver;
import android.content.pm.PackageStats;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class l extends IPackageStatsObserver.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkResourceManager f831a;

    l(ApkResourceManager apkResourceManager) {
        this.f831a = apkResourceManager;
    }

    public void onGetStatsCompleted(PackageStats packageStats, boolean z) {
        LocalApkInfo localApkInfo = (LocalApkInfo) this.f831a.h.get(packageStats.packageName);
        if (localApkInfo != null) {
            localApkInfo.occupySize = packageStats.codeSize + packageStats.dataSize;
            this.f831a.a(localApkInfo);
        }
    }
}
