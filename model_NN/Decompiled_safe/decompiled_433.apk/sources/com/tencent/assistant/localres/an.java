package com.tencent.assistant.localres;

import com.tencent.assistant.localres.LocalMediaLoader;
import java.lang.ref.WeakReference;
import java.util.Iterator;

/* compiled from: ProGuard */
class an implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LocalMediaLoader f820a;

    an(LocalMediaLoader localMediaLoader) {
        this.f820a = localMediaLoader;
    }

    public void run() {
        synchronized (this.f820a.e) {
            Iterator<WeakReference<LocalMediaLoader.ILocalMediaLoaderListener<T>>> it = this.f820a.e.iterator();
            while (it.hasNext()) {
                LocalMediaLoader.ILocalMediaLoaderListener iLocalMediaLoaderListener = (LocalMediaLoader.ILocalMediaLoaderListener) it.next().get();
                if (iLocalMediaLoaderListener != null) {
                    iLocalMediaLoaderListener.onLocalMediaLoaderOver();
                }
            }
        }
    }
}
