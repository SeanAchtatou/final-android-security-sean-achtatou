package com.tencent.connector;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import com.qq.AppService.AppService;
import com.qq.AppService.AstApp;
import com.qq.AppService.s;
import com.qq.l.p;
import com.qq.provider.h;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.b;
import com.tencent.assistant.model.OnlinePCListItemModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.plugin.UserStateInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.f;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.m;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.connector.a.a;
import com.tencent.connector.a.c;
import com.tencent.connector.component.ContentConnectionException;
import com.tencent.connector.component.ContentConnectionInitLogined;
import com.tencent.connector.component.ContentConnectionInitNoLogin;
import com.tencent.connector.component.ContentConnectionTip;
import com.tencent.connector.component.ContentCurrentConnection;
import com.tencent.connector.component.ContentQQConnectionRequest;
import com.tencent.connector.component.ContentQrcodeConnecting;
import com.tencent.connector.component.ContentVersionLowTip;
import com.tencent.pangu.utils.d;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
public class ConnectionActivity extends Activity implements b {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Status f2376a = Status.NONE;
    private SecondNavigationTitleViewV5 b;
    private ContentCurrentConnection c;
    private ContentQQConnectionRequest d;
    private ContentConnectionInitNoLogin e;
    private ContentConnectionInitLogined f;
    private ContentConnectionException g;
    private ContentConnectionTip h;
    private ContentQrcodeConnecting i;
    private ContentVersionLowTip j;
    private List<View> k = new ArrayList();
    private Handler l;
    private a m;
    private c n;
    private com.tencent.connector.a.b o;
    /* access modifiers changed from: private */
    public OnlinePCListItemModel p;
    /* access modifiers changed from: private */
    public i q;
    private boolean r = false;
    private volatile boolean s = false;
    private boolean t = false;

    /* compiled from: ProGuard */
    enum Status {
        NONE,
        INITIALIZATION,
        REQUEST_CONNECTION,
        CONNECTION_FAILED,
        CONNECTOIN_ESTABLISHED,
        WAIT_CONNECTION,
        VERSION_LOW_TIP
    }

    public int a() {
        return STConst.ST_PAGE_CONNECTION_PC;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.utils.d.a(android.content.Intent, java.lang.String, boolean):boolean
     arg types: [android.content.Intent, java.lang.String, int]
     candidates:
      com.tencent.pangu.utils.d.a(android.content.Intent, java.lang.String, int):int
      com.tencent.pangu.utils.d.a(android.content.Intent, java.lang.String, boolean):boolean */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        overridePendingTransition(R.anim.activity_right_in, R.anim.activity_left_out);
        setContentView((int) R.layout.activity_connection);
        m();
        l();
        o();
        AstApp.i().k().addConnectionEventListener(5001, this);
        AstApp.i().k().addConnectionEventListener(5002, this);
        AstApp.i().k().addConnectionEventListener(5003, this);
        AstApp.i().k().addConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_WIFI_CHANGE, this);
        AstApp.i().k().addConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_SERVICE_DESTROY, this);
        AstApp.i().k().addConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_USB_PLUGIN, this);
        AstApp.i().k().addConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_LOGIN, this);
        AstApp.i().k().addConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_PC_PING, this);
        AstApp.i().k().addConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_CANCEL_LOGIN, this);
        AstApp.i().k().addConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_WCS_START, this);
        AstApp.i().k().addConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_WCS_STOP, this);
        AstApp.i().k().addConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_WCS_NETWORK_LOST, this);
        AstApp.i().k().addConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_QUERY_LOGIN_INFO, this);
        AstApp.i().k().addConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_LOGIN_QQ_RESULT, this);
        AstApp.i().k().addConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_QQ_MISMATCH, this);
        if (d.a(getIntent(), "pushLaunchTag", false)) {
            f.a((byte) 5);
        }
        a(d.a(getIntent(), PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, 2000));
        k();
        c();
        j();
    }

    private void j() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", t.g());
        XLog.d("beacon", "beacon report >> expose_connectpc. " + hashMap.toString());
        com.tencent.beacon.event.a.a("expose_connectpc", true, -1, -1, hashMap, true);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.s) {
            this.s = false;
        } else if (this.f2376a == Status.VERSION_LOW_TIP) {
            b();
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.utils.d.a(android.content.Intent, java.lang.String, boolean):boolean
     arg types: [android.content.Intent, java.lang.String, int]
     candidates:
      com.tencent.pangu.utils.d.a(android.content.Intent, java.lang.String, int):int
      com.tencent.pangu.utils.d.a(android.content.Intent, java.lang.String, boolean):boolean */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00c4 A[Catch:{ Exception -> 0x00cd }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00db A[Catch:{ Exception -> 0x00cd }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00f2 A[Catch:{ Exception -> 0x00cd }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void k() {
        /*
            r8 = this;
            r1 = 0
            r7 = -1
            android.content.Intent r2 = r8.getIntent()     // Catch:{ Exception -> 0x00cd }
            if (r2 == 0) goto L_0x0079
            android.net.Uri r0 = r2.getData()     // Catch:{ Exception -> 0x007a }
            java.lang.String r0 = r0.getScheme()     // Catch:{ Exception -> 0x007a }
            java.lang.String r3 = r2.getDataString()     // Catch:{ Exception -> 0x007a }
            java.lang.String r4 = "pcyyb"
            boolean r0 = r0.equalsIgnoreCase(r4)     // Catch:{ Exception -> 0x007a }
            if (r0 == 0) goto L_0x0080
            java.lang.String r0 = "pcyyb://connect?"
            int r0 = r3.indexOf(r0)     // Catch:{ Exception -> 0x007a }
            if (r0 == r7) goto L_0x0080
            com.qq.l.p r0 = com.qq.l.p.m()     // Catch:{ Exception -> 0x007a }
            r4 = 513(0x201, float:7.19E-43)
            r5 = 1
            r6 = -1
            r0.c(r4, r5, r6)     // Catch:{ Exception -> 0x007a }
            r0 = 0
            r8.p = r0     // Catch:{ Exception -> 0x007a }
            java.lang.String r0 = "harry"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007a }
            r4.<init>()     // Catch:{ Exception -> 0x007a }
            java.lang.String r5 = "qrcode: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x007a }
            java.lang.StringBuilder r4 = r4.append(r3)     // Catch:{ Exception -> 0x007a }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x007a }
            com.tencent.assistant.utils.XLog.d(r0, r4)     // Catch:{ Exception -> 0x007a }
            com.tencent.connector.i r0 = new com.tencent.connector.i     // Catch:{ Exception -> 0x007a }
            r4 = 0
            r0.<init>(r8, r4)     // Catch:{ Exception -> 0x007a }
            r8.q = r0     // Catch:{ Exception -> 0x007a }
            com.tencent.connector.i r0 = r8.q     // Catch:{ Exception -> 0x007a }
            java.lang.String r4 = com.tencent.connector.m.b(r3)     // Catch:{ Exception -> 0x007a }
            r0.b = r4     // Catch:{ Exception -> 0x007a }
            com.tencent.connector.i r0 = r8.q     // Catch:{ Exception -> 0x007a }
            java.lang.String r3 = com.tencent.connector.m.c(r3)     // Catch:{ Exception -> 0x007a }
            r0.f2415a = r3     // Catch:{ Exception -> 0x007a }
            com.tencent.connector.i r0 = r8.q     // Catch:{ Exception -> 0x007a }
            java.lang.String r0 = r0.b     // Catch:{ Exception -> 0x007a }
            if (r0 == 0) goto L_0x0080
            com.tencent.connector.i r0 = r8.q     // Catch:{ Exception -> 0x007a }
            java.lang.String r0 = r0.f2415a     // Catch:{ Exception -> 0x007a }
            if (r0 == 0) goto L_0x0080
            com.tencent.connector.i r0 = r8.q     // Catch:{ Exception -> 0x00f6 }
            java.lang.String r0 = r0.b     // Catch:{ Exception -> 0x00f6 }
            com.tencent.connector.i r2 = r8.q     // Catch:{ Exception -> 0x00f6 }
            java.lang.String r2 = r2.f2415a     // Catch:{ Exception -> 0x00f6 }
            r8.a(r0, r2)     // Catch:{ Exception -> 0x00f6 }
        L_0x0079:
            return
        L_0x007a:
            r0 = move-exception
            r1 = r2
        L_0x007c:
            r0.printStackTrace()     // Catch:{ Exception -> 0x00cd }
            r2 = r1
        L_0x0080:
            java.lang.String r0 = "name"
            java.lang.String r0 = com.tencent.pangu.utils.d.a(r2, r0)     // Catch:{ Exception -> 0x00cd }
            if (r0 == 0) goto L_0x00d2
            java.lang.String r0 = "ip"
            java.lang.String r0 = com.tencent.pangu.utils.d.a(r2, r0)     // Catch:{ Exception -> 0x00cd }
            if (r0 == 0) goto L_0x00d2
            java.lang.String r0 = "key"
            java.lang.String r0 = com.tencent.pangu.utils.d.a(r2, r0)     // Catch:{ Exception -> 0x00cd }
            if (r0 == 0) goto L_0x00d2
            r0 = 1
            r8.r = r0     // Catch:{ Exception -> 0x00cd }
            android.os.Bundle r0 = com.tencent.pangu.utils.d.a(r2)     // Catch:{ Exception -> 0x00cd }
            r8.b(r0)     // Catch:{ Exception -> 0x00cd }
            java.lang.String r0 = "name"
            r2.removeExtra(r0)     // Catch:{ Exception -> 0x00cd }
            java.lang.String r0 = "ip"
            r2.removeExtra(r0)     // Catch:{ Exception -> 0x00cd }
            java.lang.String r0 = "key"
            r2.removeExtra(r0)     // Catch:{ Exception -> 0x00cd }
            java.lang.String r0 = "uin"
            r2.removeExtra(r0)     // Catch:{ Exception -> 0x00cd }
            java.lang.String r0 = "nickname"
            r2.removeExtra(r0)     // Catch:{ Exception -> 0x00cd }
        L_0x00bb:
            java.lang.String r0 = "alertUsbDebugMode"
            r1 = 0
            boolean r0 = com.tencent.pangu.utils.d.a(r2, r0, r1)     // Catch:{ Exception -> 0x00cd }
            if (r0 == 0) goto L_0x0079
            r8.F()     // Catch:{ Exception -> 0x00cd }
            java.lang.String r0 = "alertUsbDebugMode"
            r2.removeExtra(r0)     // Catch:{ Exception -> 0x00cd }
            goto L_0x0079
        L_0x00cd:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0079
        L_0x00d2:
            java.lang.String r0 = "push_type"
            r1 = -1
            int r0 = r2.getIntExtra(r0, r1)     // Catch:{ Exception -> 0x00cd }
            if (r0 == r7) goto L_0x00f2
            android.os.Bundle r0 = com.tencent.pangu.utils.d.a(r2)     // Catch:{ Exception -> 0x00cd }
            r8.a(r0)     // Catch:{ Exception -> 0x00cd }
            java.lang.String r0 = "push_type"
            r2.removeExtra(r0)     // Catch:{ Exception -> 0x00cd }
            java.lang.String r0 = "channelid"
            r2.removeExtra(r0)     // Catch:{ Exception -> 0x00cd }
            java.lang.String r0 = "versionCode"
            r2.removeExtra(r0)     // Catch:{ Exception -> 0x00cd }
            goto L_0x00bb
        L_0x00f2:
            r8.y()     // Catch:{ Exception -> 0x00cd }
            goto L_0x00bb
        L_0x00f6:
            r0 = move-exception
            goto L_0x007c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.connector.ConnectionActivity.k():void");
    }

    private void a(Bundle bundle) {
        if (bundle != null) {
            int i2 = bundle.getInt("push_type");
            String string = bundle.getString("channelid");
            bundle.getString("versionCode");
            if (i2 == 2) {
                if (this.n != null) {
                    this.n.b();
                }
                this.s = true;
                a(1, string);
            } else if (i2 == 3) {
                if (this.n != null) {
                    this.n.b();
                }
                this.s = true;
                a(0, string);
            }
        }
    }

    public void a(int i2) {
        STInfoV2 sTInfoV2 = new STInfoV2(a(), STConst.ST_DEFAULT_SLOT, i2, STConst.ST_DEFAULT_SLOT, 100);
        if (sTInfoV2 != null) {
            l.a(sTInfoV2);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        AstApp.i().k().removeConnectionEventListener(5001, this);
        AstApp.i().k().removeConnectionEventListener(5002, this);
        AstApp.i().k().removeConnectionEventListener(5003, this);
        AstApp.i().k().removeConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_WIFI_CHANGE, this);
        AstApp.i().k().removeConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_SERVICE_DESTROY, this);
        AstApp.i().k().removeConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_USB_PLUGIN, this);
        AstApp.i().k().removeConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_LOGIN, this);
        AstApp.i().k().removeConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_PC_PING, this);
        AstApp.i().k().removeConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_CANCEL_LOGIN, this);
        AstApp.i().k().removeConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_WCS_START, this);
        AstApp.i().k().removeConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_WCS_STOP, this);
        AstApp.i().k().removeConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_WCS_NETWORK_LOST, this);
        AstApp.i().k().removeConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_QUERY_LOGIN_INFO, this);
        AstApp.i().k().removeConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_LOGIN_QQ_RESULT, this);
        AstApp.i().k().removeConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_QQ_MISMATCH, this);
        this.k.clear();
        if (this.m != null) {
            this.m.b();
            this.m = null;
        }
        if (this.n != null) {
            this.n.b();
            this.n = null;
        }
        if (this.o != null) {
            this.o.b();
            this.o = null;
        }
        this.l = null;
        TemporaryThreadManager.get().start(new d(this));
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        k();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        switch (h.f2414a[this.f2376a.ordinal()]) {
            case 1:
                D();
                b();
                return true;
            case 2:
            case 3:
            case 4:
                b();
                return true;
            default:
                return super.onKeyDown(i2, keyEvent);
        }
    }

    public void a(String str, String str2) {
        a(str);
        s.a(str2);
        if (AppService.w()) {
            AppService.R();
            if (AppService.x()) {
                b(str2);
            } else if (AppService.y()) {
                com.tencent.wcs.c.b.a("invite twodimcode, but long connection disable,maybe reconnect,waiting...");
            } else {
                com.tencent.wcs.c.b.a("invite twodimcode, but long connection disable,maybe not connect,waiting...");
                y();
            }
        } else {
            AppService.z();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 40960 && i3 == -1 && intent != null) {
            com.tencent.assistant.st.a.a().a((byte) 3);
            p.m().c(504, 1, -1);
            p.m().b(505, 0, -1);
            String stringExtra = intent.getStringExtra("result");
            int a2 = m.a(stringExtra);
            if (a2 == 0) {
                p.m().c(505, 1, -1);
                this.p = null;
                this.q = new i(this, null);
                this.q.b = m.b(stringExtra);
                this.q.f2415a = m.c(stringExtra);
                a(this.q.b, this.q.f2415a);
                return;
            }
            p.m().b(stringExtra);
            b(m.a(a2));
        } else if (i3 == 0 && intent != null) {
            if (intent.getBooleanExtra("result", false)) {
                p.m().c(504, 3, 1);
            } else {
                p.m().c(504, 3, 0);
            }
        }
    }

    private void l() {
        this.l = new e(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void m() {
        if (!AppService.c) {
            Context applicationContext = getApplicationContext();
            Intent intent = new Intent();
            intent.setClass(applicationContext, AppService.class);
            intent.putExtra("isWifi", true);
            applicationContext.startService(intent);
        }
    }

    private void n() {
        y();
    }

    private void o() {
        x();
        q();
        r();
        s();
        t();
        u();
        v();
        w();
        p();
    }

    private void p() {
        this.j = (ContentVersionLowTip) findViewById(R.id.content_low_version_page);
        if (this.j != null) {
            this.j.setHostActivity(this);
            this.k.add(this.j);
        }
    }

    private void q() {
        this.e = (ContentConnectionInitNoLogin) findViewById(R.id.content_connection_init_no_login);
        if (this.e != null) {
            ImageView imageView = (ImageView) findViewById(R.id.connection_circle_bg);
            try {
                Bitmap a2 = m.a(R.drawable.connection_no_login_header);
                if (a2 != null && !a2.isRecycled()) {
                    imageView.setImageBitmap(a2);
                }
            } catch (Throwable th) {
                com.tencent.assistant.manager.t.a().b();
            }
            this.e.setHostActivity(this);
            this.k.add(this.e);
        }
    }

    private void r() {
        this.f = (ContentConnectionInitLogined) findViewById(R.id.content_connection_init_logined);
        if (this.f != null) {
            ImageView imageView = (ImageView) findViewById(R.id.connection_square_bg);
            try {
                Bitmap a2 = m.a(R.drawable.connection_login_header);
                if (a2 != null && !a2.isRecycled()) {
                    imageView.setImageBitmap(a2);
                }
            } catch (Throwable th) {
                com.tencent.assistant.manager.t.a().b();
            }
            this.f.setHostActivity(this);
            this.k.add(this.f);
        }
    }

    private void s() {
        this.c = (ContentCurrentConnection) findViewById(R.id.content_current_connection);
        if (this.c != null) {
            ImageView imageView = (ImageView) findViewById(R.id.ic_connection_media);
            try {
                Bitmap a2 = m.a(R.drawable.logo_usb_on);
                if (a2 != null && !a2.isRecycled()) {
                    imageView.setImageBitmap(a2);
                }
            } catch (Throwable th) {
                com.tencent.assistant.manager.t.a().b();
            }
            this.c.setHostActivity(this);
            this.k.add(this.c);
        }
    }

    private void t() {
        this.d = (ContentQQConnectionRequest) findViewById(R.id.content_qqconnection_request);
        if (this.d != null) {
            ImageView imageView = (ImageView) findViewById(R.id.logo_qrcode);
            try {
                Bitmap a2 = m.a(R.drawable.logo_qrcode);
                if (a2 != null && !a2.isRecycled()) {
                    imageView.setImageBitmap(a2);
                }
            } catch (Throwable th) {
                com.tencent.assistant.manager.t.a().b();
            }
            this.d.setHostActivity(this);
            this.k.add(this.d);
        }
    }

    private void u() {
        this.g = (ContentConnectionException) findViewById(R.id.content_connection_exception);
        if (this.g != null) {
            ImageView imageView = (ImageView) findViewById(R.id.exception_logo);
            try {
                Bitmap a2 = m.a(R.drawable.logo_network_error);
                if (a2 != null && !a2.isRecycled()) {
                    imageView.setImageBitmap(a2);
                }
            } catch (Throwable th) {
                com.tencent.assistant.manager.t.a().b();
            }
            this.g.setHostActivity(this);
            this.k.add(this.g);
        }
    }

    private void v() {
        this.h = (ContentConnectionTip) findViewById(R.id.content_connection_tip);
        if (this.h != null) {
            ImageView imageView = (ImageView) findViewById(R.id.img_internet_access);
            ImageView imageView2 = (ImageView) findViewById(R.id.img_same_qq);
            try {
                Bitmap a2 = m.a(R.drawable.tip_connection_internet_access);
                if (a2 != null && !a2.isRecycled()) {
                    imageView.setImageBitmap(a2);
                }
                Bitmap a3 = m.a(R.drawable.tip_connection_same_qq);
                if (a3 != null && !a3.isRecycled()) {
                    imageView2.setImageBitmap(a3);
                }
            } catch (Throwable th) {
                com.tencent.assistant.manager.t.a().b();
            }
            this.h.setHostActivity(this);
            this.k.add(this.h);
        }
    }

    private void w() {
        this.i = (ContentQrcodeConnecting) findViewById(R.id.content_qrcode_connecting);
        if (this.i != null) {
            this.i.setHostActivity(this);
            this.k.add(this.i);
        }
    }

    private void x() {
        this.b = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.b.a(this);
        String a2 = d.a(getIntent(), "activityTitleName");
        if (TextUtils.isEmpty(a2)) {
            a2 = getString(R.string.cmn_to_pc);
        }
        this.b.b(a2);
        this.b.e(new f(this));
        this.b.d();
    }

    /* access modifiers changed from: private */
    public void y() {
        boolean r2 = AppService.r();
        boolean q2 = AppService.q();
        boolean u = AppService.u();
        XLog.d("kevin", "isUsbModel: " + r2 + " isWifiModel: " + q2 + " isTrans: " + u);
        if (h.e != null) {
            XLog.d("kevin", "isConnect: " + h.e.b);
        }
        if (r2) {
            C();
            if (this.c != null) {
                this.c.displayUsbConnection(AppService.h);
            }
        } else if (q2 || u) {
            C();
            AppService.BusinessConnectionType Q = AppService.Q();
            if (Q == AppService.BusinessConnectionType.QRCODE) {
                if (this.c != null) {
                    this.c.displayQrcodeConnection(AppService.h);
                }
            } else if (Q == AppService.BusinessConnectionType.QQ && this.c != null) {
                this.c.displayQQConnection(AppService.h, AppService.N(), AppService.O());
            }
        } else {
            b();
        }
    }

    public void b() {
        this.f2376a = Status.INITIALIZATION;
        if (!com.tencent.connector.ipc.a.c(getApplicationContext())) {
            z();
        } else if (AppService.J()) {
            B();
        } else {
            A();
        }
    }

    public void a(int i2, String str) {
        this.f2376a = Status.VERSION_LOW_TIP;
        a(this.j);
        if (this.j != null) {
            this.j.setChannelID(str);
            this.j.switchPage(i2);
        }
    }

    private void z() {
        this.f2376a = Status.INITIALIZATION;
        a(this.g);
        if (this.g != null) {
            this.g.display(1);
        }
    }

    private void A() {
        this.f2376a = Status.INITIALIZATION;
        a(this.e);
        this.e.displayInit();
    }

    private void B() {
        this.f2376a = Status.INITIALIZATION;
        a(this.f);
        this.f.displayInit(AppService.L(), AppService.K());
    }

    private void C() {
        this.f2376a = Status.CONNECTOIN_ESTABLISHED;
        a(this.c);
    }

    /* access modifiers changed from: private */
    public void D() {
        if (this.d.getVisibility() == 0) {
            this.d.denyRequest();
        }
    }

    private void a(String str, String str2, String str3, String str4, String str5) {
        this.f2376a = Status.REQUEST_CONNECTION;
        a(this.d);
        if (this.d != null) {
            this.d.displayQQRequest(str, str2, str3, str5, str4);
        }
    }

    private void a(String str, String str2, String str3) {
        this.f2376a = Status.REQUEST_CONNECTION;
        a(this.d);
        if (this.d != null) {
            this.d.displayQrcodeRequest(str, str2, str3);
        }
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        this.f2376a = Status.CONNECTION_FAILED;
        a(this.g);
        if (this.g != null) {
            this.g.display(i2);
        }
    }

    /* access modifiers changed from: private */
    public void E() {
        this.f2376a = Status.CONNECTION_FAILED;
        a(this.h);
    }

    private void a(String str) {
        this.f2376a = Status.WAIT_CONNECTION;
        a(this.i);
        if (this.i != null) {
            this.i.display(str);
        }
    }

    /* access modifiers changed from: private */
    public void b(String str, String str2, String str3) {
        this.f2376a = Status.WAIT_CONNECTION;
        a(this.d);
        if (this.d != null) {
            this.d.displayQQConnecting(str, str2, str3);
        }
    }

    private void a(View view) {
        if (view != null) {
            int size = this.k.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (this.k.get(i2) == view) {
                    this.k.get(i2).setVisibility(0);
                } else {
                    this.k.get(i2).setVisibility(4);
                }
            }
        }
    }

    private void b(Bundle bundle) {
        if (bundle != null) {
            String string = bundle.getString("name");
            String string2 = bundle.getString("ip");
            String string3 = bundle.getString("key");
            String string4 = bundle.getString("uin");
            String string5 = bundle.getString("nickname");
            if (string != null && string2 != null && string3 != null) {
                if (string4 == null || string5 == null) {
                    a(string, string2, string3);
                } else {
                    a(string, string2, string3, string4, string5);
                }
            }
        }
    }

    private void F() {
        com.tencent.pangu.link.b.a(this, Uri.parse("tmast://usbdebugmode"));
    }

    public void c() {
        if (AppService.c) {
            AppService.T();
        } else if (this.l != null) {
            this.l.postDelayed(new g(this), 200);
        }
    }

    public void d() {
        AppService.U();
        this.t = true;
    }

    public void e() {
        d();
    }

    public void f() {
        p.m().a(new com.qq.l.m());
        p.m().a(1, 3, -1);
        p.m().b(503, 0, -1);
        startActivityForResult(new Intent(getApplicationContext(), CaptureActivity.class), 40960);
        p.m().c(503, 1, -1);
        p.m().b(504, 0, -1);
    }

    public void g() {
        com.tencent.pangu.link.b.a(this, Uri.parse("tmast://wifisetting"));
    }

    public void h() {
        if (this.p != null) {
            if (this.f2376a != Status.WAIT_CONNECTION) {
                b(this.p.b, AppService.K(), AppService.L());
            }
            a(this.p.f936a, AppService.M());
        }
        if (this.q != null) {
            if (this.f2376a != Status.WAIT_CONNECTION) {
                a(this.q.b);
            }
            b(this.q.f2415a);
        }
    }

    public void i() {
        if (this.f2376a != Status.WAIT_CONNECTION) {
            b(Constants.STR_EMPTY, AppService.K(), AppService.L());
        }
        if (this.o != null) {
            this.o.b();
        }
        this.o = new com.tencent.connector.a.b(getApplicationContext(), this.l, AppService.M());
        this.o.a();
    }

    private void b(String str) {
        if (this.m != null) {
            this.m.b();
        }
        AppService.a(AppService.BusinessConnectionType.QRCODE);
        this.m = new a(getApplicationContext(), this.l, str);
        this.m.a();
        if (!(this.j == null || this.q == null)) {
            this.j.pcName = this.q.b;
            this.j.qrcode = this.q.f2415a;
        }
        this.q = null;
    }

    /* access modifiers changed from: private */
    public void a(String str, long j2) {
        if (this.m != null) {
            this.m.b();
        }
        s.b(str);
        AppService.a(AppService.BusinessConnectionType.QQ);
        this.m = new a(getApplicationContext(), this.l, str, j2);
        this.m.a();
        this.p = null;
    }

    public void a(AppService.BusinessConnectionType businessConnectionType) {
        if (this.n != null) {
            this.n.b();
        }
        this.n = new c(getApplicationContext(), this.l, businessConnectionType);
        this.n.a();
    }

    public void a(Message message) {
        switch (message.what) {
            case 5001:
                y();
                return;
            case 5002:
                this.p = null;
                this.q = null;
                if (this.n != null) {
                    this.n.b();
                }
                if (this.r) {
                    finish();
                    return;
                }
                y();
                this.r = false;
                return;
            case 5003:
                y();
                return;
            case EventDispatcherEnum.CONNECTION_EVENT_WIFI_CHANGE:
                if (this.f2376a == Status.INITIALIZATION) {
                    b();
                    return;
                }
                return;
            case EventDispatcherEnum.CONNECTION_EVENT_SERVICE_DESTROY:
                y();
                return;
            case EventDispatcherEnum.CONNECTION_EVENT_USB_PLUGIN:
            case EventDispatcherEnum.CONNECTION_EVENT_LOGIN:
            case EventDispatcherEnum.CONNECTION_EVENT_WCS_STOP:
            default:
                return;
            case EventDispatcherEnum.CONNECTION_EVENT_PC_PING:
                a((OnlinePCListItemModel) message.obj);
                return;
            case EventDispatcherEnum.CONNECTION_EVENT_CANCEL_LOGIN:
                if (this.f2376a == Status.REQUEST_CONNECTION) {
                    D();
                    b();
                    return;
                }
                return;
            case EventDispatcherEnum.CONNECTION_EVENT_WCS_START:
                if (((Boolean) message.obj).booleanValue()) {
                    h();
                    return;
                } else if (this.p != null || this.q != null) {
                    b(0);
                    return;
                } else {
                    return;
                }
            case EventDispatcherEnum.CONNECTION_EVENT_WCS_NETWORK_LOST:
                b(0);
                return;
            case EventDispatcherEnum.CONNECTION_EVENT_QUERY_LOGIN_INFO:
                if (this.f2376a != Status.REQUEST_CONNECTION && this.f2376a != Status.VERSION_LOW_TIP && this.f2376a != Status.WAIT_CONNECTION) {
                    n();
                    return;
                }
                return;
            case EventDispatcherEnum.CONNECTION_EVENT_LOGIN_QQ_RESULT:
                if (this.f2376a != Status.REQUEST_CONNECTION && this.f2376a != Status.VERSION_LOW_TIP) {
                    int stateChangeType = ((UserStateInfo) message.obj).getStateChangeType();
                    y();
                    if (this.f2376a != Status.INITIALIZATION) {
                        return;
                    }
                    if ((stateChangeType == 2 || stateChangeType == 3) && this.t) {
                        this.t = false;
                        p.m().b(0);
                        i();
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.CONNECTION_EVENT_QQ_MISMATCH:
                if (this.f2376a == Status.WAIT_CONNECTION) {
                    E();
                    return;
                }
                return;
        }
    }

    private void a(OnlinePCListItemModel onlinePCListItemModel) {
    }
}
