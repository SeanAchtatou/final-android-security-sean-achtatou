package com.tencent.cloud.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.module.s;
import com.tencent.assistant.module.update.j;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import com.tencent.assistant.protocol.jce.PicInfo;
import com.tencent.assistant.protocol.jce.StatUpdateManageAction;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.cloud.component.UpdateListFooterView;
import com.tencent.cloud.component.UpdateListView;
import com.tencent.cloud.e.a;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.manager.ak;
import com.tencent.pangu.module.wisedownload.e;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class UpdateListActivity extends BaseActivity implements UIEventListener {
    private StatUpdateManageAction A = null;
    private String B = null;
    private s C = new s();
    private ab D = new ab(this, null);
    /* access modifiers changed from: private */
    public int E = -1;
    /* access modifiers changed from: private */
    public LinearLayout F;
    private TXImageView G;
    private TextView H;
    private STInfoV2 I;
    /* access modifiers changed from: private */
    public boolean J = false;
    private boolean K = false;
    private View.OnClickListener L = new x(this);
    /* access modifiers changed from: private */
    public Context n = this;
    private AstApp u;
    private SecondNavigationTitleViewV5 v;
    private RelativeLayout w;
    /* access modifiers changed from: private */
    public UpdateListView x = null;
    private NormalErrorRecommendPage y;
    private UpdateListFooterView z;

    public int f() {
        return STConst.ST_PAGE_UPDATE;
    }

    public boolean g() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_updatelist);
        w();
        x();
        t.postDelayed(new w(this), 500);
        v();
        this.u.k().addUIEventListener(1016, this);
        this.u.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED, this);
        this.u.k().addUIEventListener(1019, this);
        this.u.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE, this);
        this.u.k().addUIEventListener(1013, this);
        AstApp.i().j().sendEmptyMessage(EventDispatcherEnum.UI_EVENT_ENTRANCE_UPDATE_PAGE);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.K = true;
        setIntent(intent);
        w();
        x();
    }

    /* access modifiers changed from: private */
    public STInfoV2 b(String str) {
        if (this.I == null) {
            this.I = STInfoBuilder.buildSTInfo(this, 200);
        }
        if (this.I != null) {
            this.I.slotId = str;
            this.I.actionId = 200;
        }
        return this.I;
    }

    private void v() {
        byte[] bArr;
        List<SimpleAppModel> a2 = k.a(this.J);
        byte[] bArr2 = null;
        String str = Constants.STR_EMPTY;
        if (a2 != null && !a2.isEmpty()) {
            String str2 = str;
            byte[] bArr3 = null;
            int i = 0;
            while (i < a2.size()) {
                SimpleAppModel simpleAppModel = a2.get(i);
                if (simpleAppModel != null) {
                    if (str2.length() > 0) {
                        str2 = str2 + "|";
                    }
                    str2 = str2 + simpleAppModel.f938a + "_" + simpleAppModel.b;
                }
                if (i == 0) {
                    bArr = simpleAppModel.y;
                } else {
                    bArr = bArr3;
                }
                i++;
                bArr3 = bArr;
            }
            bArr2 = bArr3;
            str = str2;
        }
        STInfoV2 p = p();
        if (p != null) {
            p.updateWithExternalPara(this.q);
            p.extraData = str;
            p.recommendId = bArr2;
            p.pushInfo = getIntent().getStringExtra("preActivityPushInfo");
            l.a(p);
        }
    }

    private void w() {
        a.a();
        this.A = new StatUpdateManageAction();
        this.B = getIntent().getStringExtra(STConst.ST_PUSH_TO_UPDATE_KEY);
        try {
            if (!TextUtils.isEmpty(this.B)) {
                a(Integer.valueOf(this.B).intValue());
            } else {
                this.B = String.valueOf(m());
            }
        } catch (Throwable th) {
            this.B = String.valueOf(m());
        }
        this.C.register(this.D);
    }

    @SuppressLint({"ResourceAsColor"})
    private void x() {
        boolean z2 = true;
        this.u = AstApp.i();
        this.v = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.v.b(this.L);
        this.w = (RelativeLayout) findViewById(R.id.layout_container);
        this.y = (NormalErrorRecommendPage) findViewById(R.id.error_page);
        this.F = (LinearLayout) findViewById(R.id.tips_layout);
        this.G = (TXImageView) findViewById(R.id.tips_img);
        this.H = (TextView) findViewById(R.id.tips);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String string = extras.getString(com.tencent.assistant.a.a.D);
            if (extras.getBoolean(com.tencent.assistant.a.a.t) && TextUtils.isEmpty(string)) {
                this.v.a(getResources().getString(R.string.app_update));
            }
            this.J = AppConst.TENCENT_MOBILE_MANAGER_PKGNAME.equals(extras.getString(com.tencent.assistant.a.a.ae)) || AppConst.TENCENT_MOBILE_MANAGER_PKGNAME.equals(extras.getString(com.tencent.assistant.a.a.n));
        }
        if (this.x == null) {
            this.x = new UpdateListView(this.n, this.A);
            this.x.a(this);
        } else {
            z2 = false;
        }
        if (this.J) {
            y();
        } else {
            z();
        }
        this.z = new UpdateListFooterView(this.n, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, TXScrollViewBase.ScrollMode.NONE);
        this.z.a(new y(this));
        B();
        this.v.d(false);
        this.v.a(this);
        this.v.i();
        this.v.d();
        this.x.a(this.q, getIntent().getStringExtra("preActivityPushInfo"));
        this.x.a(this.B, getIntent());
        if (z2) {
            this.w.addView(this.x);
            A();
        }
        this.E = this.C.a(this.x.d());
    }

    private void y() {
        this.v.b();
        this.x.f();
    }

    private void z() {
        this.v.c();
        this.x.g();
    }

    private void A() {
        DownloadInfo c;
        e aa = m.a().aa();
        if (aa != null && (c = DownloadProxy.a().c(aa.f3963a)) != null && c.versionCode == aa.c) {
            this.F.setVisibility(0);
            this.H.setText(String.format(getResources().getString(R.string.wise_important_update_tips), c.name, c.versionName));
            this.G.updateImageView(c.iconUrl, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.F.setOnClickListener(new z(this, c));
            m.a().a((e) null);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        t();
        this.x.a();
        this.v.l();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.x.f2269a.a();
        super.onPause();
        this.v.m();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.u.k().removeUIEventListener(1016, this);
        this.u.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED, this);
        this.u.k().removeUIEventListener(1019, this);
        this.u.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE, this);
        this.u.k().removeUIEventListener(1013, this);
        this.x.f2269a.b();
        super.onDestroy();
    }

    public void handleUIEvent(Message message) {
        int i;
        switch (message.what) {
            case 1013:
            case 1016:
            case EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED:
            case 1019:
            case EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE:
                this.x.b();
                t();
                if (this.J) {
                    i = k.h();
                } else {
                    i = k.i();
                }
                ak.a().a(i);
                return;
            case EventDispatcherEnum.UI_EVENT_APP_STATE_UNINSTALL:
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD:
            case EventDispatcherEnum.UI_EVENT_CHECKUPDATE_FAIL:
            default:
                return;
        }
    }

    public void t() {
        int i;
        if (this.J) {
            i = k.h();
        } else {
            i = k.i();
        }
        this.v.b(getResources().getString(R.string.app_update));
        if (i > 0) {
            this.v.c("(" + i + ")");
        } else {
            this.v.c(Constants.STR_EMPTY);
        }
        B();
    }

    public void u() {
        if (!m.a().D() && com.tencent.pangu.module.wisedownload.s.e()) {
            aa aaVar = new aa(this);
            Context baseContext = AstApp.i().getBaseContext();
            aaVar.titleRes = baseContext.getString(R.string.dialog_title_auto_download_tips);
            aaVar.contentRes = baseContext.getString(R.string.dialog_content_auto_download_tips);
            aaVar.btnTxtRes = baseContext.getString(R.string.dialog_btn_auto_download_tips);
            aaVar.blockCaller = true;
            DialogUtils.show1BtnDialog(aaVar);
            m.a().o(true);
        }
    }

    private void B() {
        int i;
        int i2;
        if (this.J) {
            i = k.h();
        } else {
            i = k.i();
        }
        if (this.x != null) {
            i2 = this.x.c();
        } else {
            i2 = 0;
        }
        if (i > 0 || i2 > 0) {
            this.w.setVisibility(0);
            this.y.setVisibility(8);
        } else if (k.b(this.J) > 0) {
            if (this.y.getVisibility() != 0) {
                this.w.setVisibility(8);
                this.y.setActivityPageId(STConst.ST_PAGE_UPDATE_ERROR_PAGE);
                this.y.setErrorType(60);
                this.y.setErrorText(getString(R.string.update_ingore_list_empty_txt));
                this.y.setErrorImage(R.drawable.emptypage_pic_05);
                this.y.setVisibility(0);
            }
            this.z.a((int) R.drawable.bg_card_selector);
            this.z.b(2);
            this.z.a(String.format(getResources().getString(R.string.view_ignorelist), Integer.valueOf(j.b().e().size())));
            this.y.setErrorTextSetting(this.z);
        } else if (this.y.getVisibility() != 0) {
            this.w.setVisibility(8);
            this.y.setVisibility(0);
            this.y.setActivityPageId(STConst.ST_PAGE_UPDATE_ERROR_PAGE);
            this.y.setErrorType(60);
            this.y.setErrorText(getString(R.string.update_list_empty_txt));
        }
    }

    /* access modifiers changed from: private */
    public List<AppUpdateInfo> a(List<AppSimpleDetail> list) {
        ArrayList arrayList = new ArrayList();
        if (list != null) {
            for (AppSimpleDetail next : list) {
                if (next != null) {
                    AppUpdateInfo appUpdateInfo = new AppUpdateInfo();
                    appUpdateInfo.o = next.f1159a;
                    appUpdateInfo.r = next.b;
                    appUpdateInfo.b = next.c;
                    appUpdateInfo.e = new PicInfo(next.d);
                    appUpdateInfo.f1162a = next.e;
                    appUpdateInfo.n = next.f;
                    appUpdateInfo.d = next.g;
                    appUpdateInfo.c = next.h;
                    appUpdateInfo.h = next.i;
                    appUpdateInfo.j = next.j;
                    appUpdateInfo.s = next.k;
                    appUpdateInfo.k = next.l;
                    appUpdateInfo.m = next.m;
                    arrayList.add(appUpdateInfo);
                }
            }
        }
        return arrayList;
    }
}
