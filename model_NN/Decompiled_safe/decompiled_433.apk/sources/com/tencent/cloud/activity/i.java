package com.tencent.cloud.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXRefreshGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.activity.a;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.assistantv2.component.banner.floatheader.FloatBannerViewSwitcher;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.l;
import com.tencent.cloud.adapter.SoftwareListPageAdapter;
import com.tencent.cloud.d.a.c;
import com.tencent.pangu.component.banner.f;
import com.tencent.pangu.model.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class i extends a implements ITXRefreshListViewListener, NetworkMonitor.ConnectivityChangeListener, c {
    boolean R = true;
    private final String S = "AppTabActivity:";
    private LinearLayout T;
    private TXRefreshGetMoreListView U;
    private ViewStub V;
    /* access modifiers changed from: private */
    public NormalErrorRecommendPage W = null;
    /* access modifiers changed from: private */
    public LoadingView X;
    private com.tencent.cloud.c.a Y = new com.tencent.cloud.c.a();
    /* access modifiers changed from: private */
    public SoftwareListPageAdapter Z = null;
    private byte[] aa = null;
    private boolean ab = false;
    private int ac = 1;
    /* access modifiers changed from: private */
    public SmartListAdapter.BannerType ad = SmartListAdapter.BannerType.None;
    /* access modifiers changed from: private */
    public FloatBannerViewSwitcher ae;
    private APN af = APN.NO_NETWORK;

    public void d(Bundle bundle) {
        super.d(bundle);
        this.T = new LinearLayout(this.P);
        a(this.T);
        this.Y.a(this);
    }

    private void b(View view) {
        this.U = (TXRefreshGetMoreListView) view.findViewById(R.id.list);
        this.Z = new SoftwareListPageAdapter(this.P, this.U, this.Y.d());
        this.Z.a(true);
        this.U.setAdapter(this.Z);
        this.U.setRefreshListViewListener(this);
        this.U.setVisibility(8);
        this.U.setDivider(null);
        this.U.setListSelector(17170445);
        k kVar = new k(this, null);
        this.U.setIScrollerListener(kVar);
        this.Z.a(kVar);
        this.V = (ViewStub) view.findViewById(R.id.error_stub);
        this.X = (LoadingView) view.findViewById(R.id.loading);
        this.ae = (FloatBannerViewSwitcher) view.findViewById(R.id.float_banner_switcher);
        this.ae.a(this.U.getListView(), SmartListAdapter.SmartListType.AppPage.ordinal());
    }

    /* access modifiers changed from: private */
    public void H() {
        l.a(200501, CostTimeSTManager.TIMETYPE.START, System.currentTimeMillis());
        this.Y.a();
    }

    private void d(int i) {
        if (this.W == null) {
            I();
        }
        this.W.setErrorType(i);
        if (this.U != null) {
            this.U.setVisibility(8);
        }
        this.W.setVisibility(0);
    }

    private void I() {
        this.V.inflate();
        this.W = (NormalErrorRecommendPage) h().findViewById(R.id.error);
        this.W.setButtonClickListener(new j(this));
    }

    public void d(boolean z) {
        if (this.R) {
            this.R = false;
            this.T.removeAllViews();
            View inflate = this.Q.inflate((int) R.layout.act4, (ViewGroup) null);
            this.T.addView(inflate);
            this.T.requestLayout();
            this.T.forceLayout();
            this.T.invalidate();
            b(inflate);
            H();
            t.a().a(this);
        } else {
            this.Y.e();
        }
        if (this.Z != null) {
            this.Z.i();
            this.Z.notifyDataSetChanged();
        }
        if (this.ae != null) {
            this.ae.f();
        }
        Log.d("YYB5_0", "AppTabActivity:onResume------------3");
    }

    public void k() {
        super.k();
        if (this.Z != null) {
            this.Z.h();
        }
        if (this.ae != null) {
            this.ae.g();
        }
        t.a().b(this);
        Log.d("YYB5_0", "AppTabActivity:onDestroy------------3");
    }

    public i() {
        super(MainActivity.t());
    }

    public void D() {
        if (this.Z != null) {
            this.Z.h();
        }
        Log.e("YYB5_0", "AppTabActivity:onPageTurnBackground------------3:::");
    }

    public int E() {
        return 1;
    }

    public int F() {
        return 3;
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (TXScrollViewBase.ScrollState.ScrollState_FromEnd == scrollState) {
            this.Y.b();
        }
        if (TXScrollViewBase.ScrollState.ScrollState_FromStart == scrollState) {
            H();
        }
    }

    public void a(int i, int i2, boolean z, byte[] bArr, boolean z2, ArrayList<ColorCardItem> arrayList, List<b> list) {
        if (this.X != null) {
            this.X.setVisibility(8);
        }
        if (i2 == 0) {
            if (this.W != null) {
                this.W.setVisibility(8);
            }
            this.U.setVisibility(0);
            this.aa = bArr;
            if (list == null || list.size() == 0) {
                l.a(200501, CostTimeSTManager.TIMETYPE.CANCEL, System.currentTimeMillis());
                d(10);
                return;
            }
            this.Z.a(z2, list, (List<f>) null, arrayList);
            if (z2) {
                this.ae.a(arrayList);
                if (i == -1) {
                    l.a(200501, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                    this.U.onRefreshComplete(false, z, a((int) R.string.refresh_fail));
                    return;
                }
                l.a(200501, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                this.U.onRefreshComplete(true, z, null);
                return;
            }
            this.U.onRefreshComplete(z, true);
        } else if (z2) {
            l.a(200501, CostTimeSTManager.TIMETYPE.CANCEL, System.currentTimeMillis());
            if (-800 == i2) {
                d(30);
            } else if (this.ac <= 0) {
                d(20);
            } else {
                this.ac--;
                this.Y.c();
            }
        } else {
            this.U.onRefreshComplete(z, false);
        }
    }

    public int G() {
        return 200501;
    }

    public void C() {
        B();
    }

    public void onConnected(APN apn) {
        if (this.Z != null) {
            XLog.d("leobi", "app onConnected" + this.Z.e());
            if (this.Z.e() <= 0) {
                H();
            }
        }
    }

    public void onDisconnected(APN apn) {
        this.af = apn;
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        this.af = apn2;
    }
}
