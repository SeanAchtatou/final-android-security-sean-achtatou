package com.tencent.cloud.activity;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import java.util.List;

/* compiled from: ProGuard */
class r extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TencentAppListActivity f2200a;

    r(TencentAppListActivity tencentAppListActivity) {
        this.f2200a = tencentAppListActivity;
    }

    public void onLoadInstalledApkSuccess(List<LocalApkInfo> list) {
        this.f2200a.runOnUiThread(new s(this));
    }
}
