package com.tencent.cloud.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Pair;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.module.update.j;
import com.tencent.assistant.protocol.jce.StatUpdateManageAction;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.activity.AppDetailActivityV5;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class AppUpdateIgnoreListAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    public View f2208a;
    public SparseIntArray b = new SparseIntArray();
    private AstApp c = AstApp.i();
    /* access modifiers changed from: private */
    public Context d;
    private LayoutInflater e;
    private ArrayList<SimpleAppModel> f = new ArrayList<>();
    /* access modifiers changed from: private */
    public SparseBooleanArray g = new SparseBooleanArray();
    private StatUpdateManageAction h = null;
    private String i = null;
    private final int j = 10;
    private final int k = 2;
    /* access modifiers changed from: private */
    public String l;
    private b m = null;

    public AppUpdateIgnoreListAdapter(Context context, View view, StatUpdateManageAction statUpdateManageAction) {
        this.d = context;
        this.e = LayoutInflater.from(this.d);
        this.h = statUpdateManageAction;
        this.f2208a = view;
    }

    public void a(String str) {
        this.i = str;
    }

    public void a() {
        Iterator<SimpleAppModel> it = this.f.iterator();
        while (it.hasNext()) {
            SimpleAppModel next = it.next();
            AppConst.AppState d2 = k.d(next);
            if (d2 == AppConst.AppState.QUEUING || d2 == AppConst.AppState.DOWNLOADING || d2 == AppConst.AppState.DOWNLOADED || d2 == AppConst.AppState.INSTALLING) {
                this.b.put(next.c.hashCode(), next.g);
            }
        }
    }

    public void a(List<SimpleAppModel> list, boolean z) {
        boolean z2;
        if (list != null && list.size() > 0) {
            if (this.f.size() == 0 || z) {
                this.f.clear();
                for (SimpleAppModel next : list) {
                    if (a(next)) {
                        this.f.add(next);
                    }
                }
            } else {
                ArrayList arrayList = new ArrayList();
                Iterator<SimpleAppModel> it = this.f.iterator();
                while (it.hasNext()) {
                    arrayList.add(Integer.valueOf(it.next().c.hashCode()));
                }
                ArrayList arrayList2 = new ArrayList();
                int i2 = 0;
                for (SimpleAppModel next2 : list) {
                    arrayList2.add(Integer.valueOf(next2.c.hashCode()));
                    if (!arrayList.contains(Integer.valueOf(next2.c.hashCode())) && a(next2)) {
                        this.f.add(i2, next2);
                    }
                    i2++;
                }
                Iterator<SimpleAppModel> it2 = this.f.iterator();
                while (it2.hasNext()) {
                    SimpleAppModel next3 = it2.next();
                    if (!arrayList2.contains(Integer.valueOf(next3.c.hashCode()))) {
                        if (this.b == null || this.b.indexOfKey(next3.c.hashCode()) < 0) {
                            z2 = false;
                        } else {
                            z2 = true;
                        }
                        if (!z2) {
                            it2.remove();
                        }
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

    private boolean a(SimpleAppModel simpleAppModel) {
        return simpleAppModel != null && !TextUtils.isEmpty(simpleAppModel.c);
    }

    public void b() {
    }

    public void c() {
        if (this.g != null) {
            this.g.clear();
            this.g = null;
        }
        if (this.b != null) {
            this.b.clear();
            this.b = null;
        }
    }

    public int getCount() {
        if (this.f != null) {
            return this.f.size();
        }
        return 0;
    }

    /* renamed from: a */
    public SimpleAppModel getItem(int i2) {
        if (this.f != null) {
            return this.f.get(i2);
        }
        return null;
    }

    public long getItemId(int i2) {
        return 0;
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        Pair<View, b> a2 = a.a(this.d, this.e, view, 2);
        View view2 = (View) a2.first;
        a((b) a2.second, i2);
        return view2;
    }

    private String b(int i2) {
        return a.a("03_", i2);
    }

    private void a(b bVar, int i2) {
        SimpleAppModel a2 = getItem(i2);
        if (bVar != null && a2 != null) {
            STInfoV2 a3 = a(a2, i2);
            bVar.f2211a.setOnClickListener(new c(this, a2, a3));
            a.a(this.d, bVar, a2, a3);
            AppConst.AppState d2 = k.d(a2);
            if (d2 == AppConst.AppState.INSTALLED) {
                bVar.l.setVisibility(8);
            } else {
                bVar.l.setVisibility(0);
                bVar.l.setOnClickListener(new d(this, a2, a3));
            }
            if (d2 == AppConst.AppState.QUEUING || d2 == AppConst.AppState.DOWNLOADING || d2 == AppConst.AppState.DOWNLOADED || d2 == AppConst.AppState.INSTALLING) {
                this.b.put(a2.c.hashCode(), a2.g);
            }
            String str = a2.o;
            if (TextUtils.isEmpty(str)) {
                bVar.i.setVisibility(8);
                return;
            }
            String format = String.format(this.d.getResources().getString(R.string.update_feature), "\n" + str);
            bVar.k.setText(format);
            String replace = format.replaceAll("\r\n", "\n").replaceAll("\n\n", "\n").replace("\n", Constants.STR_EMPTY);
            bVar.i.setVisibility(0);
            bVar.i.setOnClickListener(new e(this, a2, bVar, a3));
            bVar.j.setTag(Long.valueOf(a2.f938a));
            bVar.j.a(new f(this, bVar, a2.f938a));
            bVar.j.setText(replace);
            if (this.l == null || !this.l.equals(a2.c)) {
                bVar.j.setVisibility(0);
                bVar.k.setVisibility(8);
                bVar.m.setImageDrawable(this.d.getResources().getDrawable(R.drawable.icon_open));
                return;
            }
            bVar.k.setVisibility(0);
            bVar.j.setVisibility(8);
            bVar.m.setImageDrawable(this.d.getResources().getDrawable(R.drawable.icon_close));
            notifyDataSetChanged();
        }
    }

    private void b(SimpleAppModel simpleAppModel) {
        if (this.g.indexOfKey(simpleAppModel.c.hashCode()) > 0) {
            this.g.clear();
        }
    }

    /* access modifiers changed from: private */
    public void c(SimpleAppModel simpleAppModel) {
        StatUpdateManageAction statUpdateManageAction = this.h;
        statUpdateManageAction.e = (short) (statUpdateManageAction.e + 1);
        Intent intent = new Intent(this.d, AppDetailActivityV5.class);
        intent.putExtra("simpleModeInfo", simpleAppModel);
        this.d.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void d(SimpleAppModel simpleAppModel) {
        if (simpleAppModel != null) {
            j.b().b(simpleAppModel);
            b(simpleAppModel);
            this.f.remove(simpleAppModel);
            this.b.delete(simpleAppModel.c.hashCode());
            notifyDataSetChanged();
        }
    }

    private STInfoV2 a(SimpleAppModel simpleAppModel, int i2) {
        if (simpleAppModel == null) {
            return null;
        }
        if (this.m == null) {
            this.m = new b();
        }
        AppConst.AppState d2 = k.d(simpleAppModel);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.d, simpleAppModel, b(i2), 100, a.a(d2, simpleAppModel));
        this.m.exposure(buildSTInfo);
        return buildSTInfo;
    }
}
