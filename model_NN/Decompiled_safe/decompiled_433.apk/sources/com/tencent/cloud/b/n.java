package com.tencent.cloud.b;

import com.tencent.assistant.manager.i;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.CftGetAppListResponse;
import java.util.ArrayList;

/* compiled from: ProGuard */
class n implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ m f2246a;

    n(m mVar) {
        this.f2246a = mVar;
    }

    public void run() {
        boolean z = true;
        CftGetAppListResponse b = i.y().b(this.f2246a.f2245a.f2239a, this.f2246a.f2245a.b, this.f2246a.c);
        if (b == null || b.e != this.f2246a.f2245a.e || b.b == null || b.b.size() <= 0) {
            int unused = this.f2246a.h = this.f2246a.f2245a.a(this.f2246a.h, this.f2246a.c);
            return;
        }
        m mVar = this.f2246a;
        long j = b.e;
        ArrayList<SimpleAppModel> b2 = k.b(b.b);
        if (b.d != 1) {
            z = false;
        }
        mVar.a(j, b2, z, b.c);
    }
}
