package com.tencent.cloud.b;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.CFTGetOneMoreAppRequest;
import com.tencent.assistant.protocol.jce.CFTGetOneMoreAppResponse;
import com.tencent.assistant.protocol.jce.LbsData;
import com.tencent.assistant.utils.ah;
import com.tencent.cloud.updaterec.a;

/* compiled from: ProGuard */
public class r extends BaseEngine<a> {

    /* renamed from: a  reason: collision with root package name */
    private static r f2250a;
    /* access modifiers changed from: private */
    public int b;

    public static synchronized r a() {
        r rVar;
        synchronized (r.class) {
            if (f2250a == null) {
                f2250a = new r();
            }
            rVar = f2250a;
        }
        return rVar;
    }

    public int a(int i, long j, LbsData lbsData, String str) {
        CFTGetOneMoreAppRequest cFTGetOneMoreAppRequest = new CFTGetOneMoreAppRequest();
        cFTGetOneMoreAppRequest.f1179a = i;
        cFTGetOneMoreAppRequest.b = j;
        cFTGetOneMoreAppRequest.d = lbsData;
        cFTGetOneMoreAppRequest.c = str;
        this.b = send(cFTGetOneMoreAppRequest);
        ah.a().postDelayed(new s(this, this.b), 1500);
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        CFTGetOneMoreAppResponse cFTGetOneMoreAppResponse;
        if (i == this.b && (cFTGetOneMoreAppResponse = (CFTGetOneMoreAppResponse) jceStruct2) != null) {
            notifyDataChangedInMainThread(new u(this, cFTGetOneMoreAppResponse));
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        if (i == this.b) {
            notifyDataChangedInMainThread(new v(this, i2));
        }
    }
}
