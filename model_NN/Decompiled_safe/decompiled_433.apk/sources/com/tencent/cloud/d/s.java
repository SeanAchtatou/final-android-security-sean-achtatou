package com.tencent.cloud.d;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.module.p;
import com.tencent.assistant.protocol.jce.ApkFileInfo;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import com.tencent.assistant.protocol.jce.ReportApkFileInfoRequest;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.a.a;
import com.tencent.assistant.utils.h;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: ProGuard */
public class s extends p {

    /* renamed from: a  reason: collision with root package name */
    private static s f2315a = null;
    /* access modifiers changed from: private */
    public List<AppUpdateInfo> b;
    /* access modifiers changed from: private */
    public AtomicInteger c = new AtomicInteger(0);

    private s() {
    }

    public void a(List<AppUpdateInfo> list) {
        this.b = new ArrayList(list.size() / 2);
        for (AppUpdateInfo next : list) {
            if (h.a(next.q)) {
                this.b.add(next);
            }
        }
        this.c.set(0);
    }

    public void a() {
        TemporaryThreadManager.get().start(new t(this));
    }

    /* access modifiers changed from: private */
    public JceStruct a(LocalApkInfo localApkInfo, AppUpdateInfo appUpdateInfo) {
        ReportApkFileInfoRequest reportApkFileInfoRequest;
        try {
            a aVar = new a();
            aVar.a(localApkInfo.mLocalFilePath);
            if (!aVar.b()) {
                return null;
            }
            ArrayList arrayList = new ArrayList(1);
            arrayList.add(new ApkFileInfo(appUpdateInfo.r, localApkInfo.manifestMd5, localApkInfo.mPackageName, aVar.a(), null, null, 0, null, null));
            reportApkFileInfoRequest = new ReportApkFileInfoRequest(arrayList);
            return reportApkFileInfoRequest;
        } catch (Throwable th) {
            reportApkFileInfoRequest = null;
        }
    }

    public static synchronized s b() {
        s sVar;
        synchronized (s.class) {
            if (f2315a == null) {
                f2315a = new s();
            }
            sVar = f2315a;
        }
        return sVar;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        a();
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        a();
    }
}
