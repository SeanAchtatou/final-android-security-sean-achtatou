package com.tencent.cloud.component;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.l;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
class n extends ViewInvalidateMessageHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CftAppRankListView f2287a;

    n(CftAppRankListView cftAppRankListView) {
        this.f2287a = cftAppRankListView;
    }

    public void handleMessage(ViewInvalidateMessage viewInvalidateMessage) {
        int i;
        int i2;
        if (viewInvalidateMessage.what == 1) {
            int i3 = viewInvalidateMessage.arg1;
            int i4 = viewInvalidateMessage.arg2;
            Map map = (Map) viewInvalidateMessage.params;
            boolean booleanValue = ((Boolean) map.get("isFirstPage")).booleanValue();
            Object obj = map.get("key_data");
            if (!(obj == null || this.f2287a.f == null)) {
                this.f2287a.f.a(booleanValue, (List) obj);
                if (booleanValue) {
                    l.a((int) STConst.ST_PAGE_RANK_CLASSIC, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                }
            }
            this.f2287a.a(i4, i3, booleanValue);
            if (booleanValue) {
                if (obj != null) {
                    int i5 = 0;
                    i = 0;
                    for (SimpleAppModel simpleAppModel : (List) obj) {
                        i5++;
                        if (i5 > 20) {
                            break;
                        }
                        if (simpleAppModel.u() == AppConst.AppState.INSTALLED) {
                            i2 = i + 1;
                        } else {
                            i2 = i;
                        }
                        i = i2;
                    }
                } else {
                    i = 0;
                }
                if ((i < 5 || !m.a().aj()) && !m.a().am()) {
                    this.f2287a.isHideInstalledAppAreaAdded = false;
                    this.f2287a.y.setVisibility(8);
                } else {
                    m.a().x(false);
                    this.f2287a.isHideInstalledAppAreaAdded = true;
                    m.a().A(false);
                    this.f2287a.t();
                }
                this.f2287a.f.d();
                return;
            }
            return;
        }
        this.f2287a.u.d();
        if (this.f2287a.f != null) {
            this.f2287a.f.notifyDataSetChanged();
        }
    }
}
