package com.tencent.cloud.component;

import android.view.View;

/* compiled from: ProGuard */
class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppRankTabBarView f2277a;

    private b(AppRankTabBarView appRankTabBarView) {
        this.f2277a = appRankTabBarView;
    }

    /* synthetic */ b(AppRankTabBarView appRankTabBarView, a aVar) {
        this(appRankTabBarView);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.cloud.component.AppRankTabBarView.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.cloud.component.AppRankTabBarView.a(android.content.Context, android.util.AttributeSet):void
      com.tencent.cloud.component.AppRankTabBarView.a(int, float):void
      com.tencent.cloud.component.AppRankTabBarView.a(int, boolean):void */
    public void onClick(View view) {
        int id = view.getId();
        for (int i = 0; i < this.f2277a.i.length; i++) {
            if (this.f2277a.b(i) != null && id == this.f2277a.b(i).getId()) {
                this.f2277a.a(i, true);
                if (this.f2277a.l != null) {
                    this.f2277a.l.a(view, 1);
                }
            }
        }
    }
}
