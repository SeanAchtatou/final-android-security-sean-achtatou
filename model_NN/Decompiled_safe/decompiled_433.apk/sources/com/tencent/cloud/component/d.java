package com.tencent.cloud.component;

import com.tencent.assistant.module.callback.b;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.protocol.jce.TagGroup;
import com.tencent.pangu.component.banner.f;
import java.util.List;

/* compiled from: ProGuard */
class d implements b {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CategoryDetailListView f2279a;

    d(CategoryDetailListView categoryDetailListView) {
        this.f2279a = categoryDetailListView;
    }

    public void a(int i, int i2, boolean z, List<com.tencent.pangu.model.b> list, List<TagGroup> list2) {
        if (list != null) {
            if (this.f2279a.v == null) {
                this.f2279a.s();
            }
            this.f2279a.v.a(z, list, (List<f>) null, (List<ColorCardItem>) null);
        }
        if (!this.f2279a.y.isTagExisted()) {
            if (list2 != null && !list2.isEmpty()) {
                this.f2279a.y.setTagData(list2, this.f2279a.z);
                this.f2279a.a(list2);
            }
            this.f2279a.b(i, i2, z);
            return;
        }
        this.f2279a.a(i, i2, z);
    }
}
