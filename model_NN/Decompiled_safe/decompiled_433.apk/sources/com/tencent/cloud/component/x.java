package com.tencent.cloud.component;

import android.app.Dialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.cloud.component.UpdateListView;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.manager.au;
import com.tencent.pangu.manager.bj;
import java.util.List;

/* compiled from: ProGuard */
class x extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UpdateListView f2296a;

    x(UpdateListView updateListView) {
        this.f2296a = updateListView;
    }

    public void onTMAClick(View view) {
        boolean z;
        boolean z2;
        boolean z3 = false;
        UpdateListView.UpdateAllType b = this.f2296a.m();
        if (b == UpdateListView.UpdateAllType.ALLDOWNLOADING) {
            Toast.makeText(this.f2296a.b, (int) R.string.toast_all_updating, 0).show();
        } else if (b == UpdateListView.UpdateAllType.ALLUPDATED) {
            Toast.makeText(this.f2296a.b, (int) R.string.toast_all_updated, 0).show();
        } else if (k.a(this.f2296a.f2269a.b) != null) {
            bj c = au.a().c();
            if (c == null || !c.a()) {
                z = false;
            } else {
                z = true;
            }
            int F = m.a().F();
            long currentTimeMillis = System.currentTimeMillis() / 1000;
            if (F <= 0 || currentTimeMillis - ((long) F) >= 86400) {
                z2 = z;
            } else {
                z2 = false;
            }
            if (z2) {
                View unused = this.f2296a.o = this.f2296a.d.inflate((int) R.layout.updatelist_updateall_extra, (ViewGroup) null);
                if (this.f2296a.o.findViewById(R.id.select_check) != null) {
                    if (c.j == 1) {
                        z3 = true;
                    }
                    CheckBox unused2 = this.f2296a.p = (CheckBox) this.f2296a.o.findViewById(R.id.select_check);
                    this.f2296a.p.setChecked(z3);
                    this.f2296a.p.setText(c.h);
                }
            } else {
                View unused3 = this.f2296a.o = (View) null;
            }
            y yVar = new y(this, c, b);
            yVar.hasTitle = true;
            yVar.titleRes = AstApp.i().getString(R.string.dialog_updatelist_updateall_title);
            yVar.blockCaller = true;
            yVar.contentRes = AstApp.i().getString(R.string.dialog_updateall_enter);
            yVar.extraMsgView = this.f2296a.o;
            Dialog dialog = DialogUtils.get2BtnDialog(this.f2296a.getContext(), yVar);
            if (dialog != null && dialog.getOwnerActivity() != null && !dialog.getOwnerActivity().isFinishing()) {
                STInfoV2 sTInfoV2 = new STInfoV2(STConst.ST_PAGE_UPDATE_UPDATEALL, STConst.ST_DEFAULT_SLOT, STConst.ST_PAGE_UPDATE, STConst.ST_DEFAULT_SLOT, 100);
                sTInfoV2.pushInfo = this.f2296a.C;
                l.a(sTInfoV2);
                try {
                    dialog.show();
                } catch (Exception e) {
                }
                if (z2) {
                    c.c();
                }
            }
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 e = this.f2296a.e();
        if (e != null) {
            e.slotId = a.a("07", "001");
            List<SimpleAppModel> a2 = k.a(this.f2296a.f2269a.b);
            String str = Constants.STR_EMPTY;
            if (a2 != null) {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= a2.size()) {
                        break;
                    }
                    SimpleAppModel simpleAppModel = a2.get(i2);
                    if (simpleAppModel != null) {
                        if (str.length() > 0) {
                            str = str + "|";
                        }
                        str = str + simpleAppModel.f938a + "_" + simpleAppModel.b;
                    }
                    i = i2 + 1;
                }
            }
            e.extraData = str;
        }
        return e;
    }
}
