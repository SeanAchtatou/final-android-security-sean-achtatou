package com.tencent.cloud.component;

import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.a;
import com.tencent.assistant.protocol.jce.TagGroup;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
class m implements a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CftAppRankListView f2286a;

    m(CftAppRankListView cftAppRankListView) {
        this.f2286a = cftAppRankListView;
    }

    public void a(int i, int i2, boolean z, List<SimpleAppModel> list, List<TagGroup> list2) {
        if (this.f2286a.w != null) {
            ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(1, null, this.f2286a.J);
            viewInvalidateMessage.arg1 = i2;
            viewInvalidateMessage.arg2 = i;
            HashMap hashMap = new HashMap();
            hashMap.put("isFirstPage", Boolean.valueOf(z));
            viewInvalidateMessage.params = hashMap;
            hashMap.put("key_data", list);
            this.f2286a.w.sendMessage(viewInvalidateMessage);
        }
    }
}
