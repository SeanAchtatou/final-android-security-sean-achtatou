package com.tencent.a.a;

import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.DownloadDbHelper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.db.table.IBaseTable;

/* compiled from: ProGuard */
public class a implements IBaseTable {
    public int tableVersion() {
        return 0;
    }

    public String tableName() {
        return "tbl_download";
    }

    public String createTableSQL() {
        return "CREATE TABLE IF NOT EXISTS tbl_download (_id INTEGER PRIMARY KEY AUTOINCREMENT,type INTEGER,id TEXT,urls TEXT,urls_mapping BLOB,status INTEGER,last_modify INTEGER,priority INTEGER,save_dir TEXT,save_name TEXT,total_length INTEGER,rangeable INTEGER default 1,received_length INTEGER,rangeable2 INTEGER default 1);";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i == 1 && i2 == 2) {
            return new String[]{"alter table tbl_download add column rangeable INTEGER default 1;"};
        } else if (i != 4 || i2 != 5) {
            return null;
        } else {
            return new String[]{"alter table tbl_download add column rangeable2 INTEGER default 1;"};
        }
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return DownloadDbHelper.get(AstApp.i());
    }
}
