package com.tencent.b.b;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import com.tencent.b.a.h;
import com.tencent.b.c.g;
import com.tencent.b.d.k;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private static Context f2072a = null;
    private static Handler b = null;
    private static c c = null;
    private static final List<String> d = new ArrayList(Arrays.asList("android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE", "android.permission.ACCESS_WIFI_STATE"));
    private static boolean e = false;

    private c(Context context) {
        HandlerThread handlerThread = new HandlerThread(getClass().getSimpleName());
        handlerThread.start();
        b = new Handler(handlerThread.getLooper());
        f2072a = context.getApplicationContext();
    }

    static synchronized c a(Context context) {
        c cVar;
        synchronized (c.class) {
            if (c == null) {
                c = new c(context);
            }
            cVar = c;
        }
        return cVar;
    }

    public static void a(Context context, a aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("error, callback is null!");
        } else if (context == null) {
            aVar.a(-10000, "content is null!");
        } else {
            c(context, new d(aVar));
        }
    }

    public static boolean a() {
        return e;
    }

    public static boolean a(String str) {
        return k.c(str);
    }

    public static String b(Context context) {
        if (context == null) {
            Log.e("MID", "context==null in getMid()");
            return null;
        }
        a(context);
        b a2 = g.a(context).a();
        if (a2 == null) {
            a2 = new b();
        }
        if (!a(a2.c())) {
            k.a("request new mid entity.");
            if (b != null) {
                b.post(new h(context, 1, new e()));
            }
        } else if (b != null) {
            b.post(new h(context, 2, new f()));
        }
        return a2.c();
    }

    private static boolean b(Context context, a aVar) {
        for (String next : d) {
            if (!k.a(context, next)) {
                aVar.a(-10001, "permission :" + next + " is denyed, please set it on AndroidManifest.xml first");
                return false;
            }
        }
        if (k.a(context, "android.permission.WRITE_SETTINGS") || k.a(context, "android.permission.WRITE_EXTERNAL_STORAGE")) {
            if (!k.a(context, "android.permission.READ_PHONE_STATE")) {
                Log.e("MID", "android.permission.READ_PHONE_STATE is denyed.");
            }
            return true;
        }
        aVar.a(-10001, "failed to get permission either permission android.permission.WRITE_SETTINGS or android.permission.WRITE_EXTERNAL_STORAGE");
        return false;
    }

    private static void c(Context context, a aVar) {
        if (b(context, aVar)) {
            a(context);
            b a2 = g.a(context).a();
            if (a2 == null || !a2.a()) {
                k.a("request new mid entity.");
                if (b != null) {
                    b.post(new h(context, 1, aVar));
                    return;
                }
                return;
            }
            k.a("get local mid entity:" + a2.toString());
            aVar.a(a2.toString());
            if (b != null) {
                b.post(new h(context, 2, aVar));
            }
        }
    }
}
