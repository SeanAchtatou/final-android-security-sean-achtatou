package com.tencent.b.c;

import com.tencent.b.d.k;
import org.json.JSONException;
import org.json.JSONObject;

public class a {

    /* renamed from: a  reason: collision with root package name */
    public static String f2074a = "ts";
    public static String b = "times";
    public static String c = "mfreq";
    public static String d = "mdays";
    private long e = 0;
    private int f = 1;
    private int g = 1024;
    private int h = 3;

    public a() {
    }

    public a(String str) {
        if (k.b(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (!jSONObject.isNull(f2074a)) {
                    this.e = jSONObject.getLong(f2074a);
                }
                if (!jSONObject.isNull(c)) {
                    this.g = jSONObject.getInt(c);
                }
                if (!jSONObject.isNull(b)) {
                    this.f = jSONObject.getInt(b);
                }
                if (!jSONObject.isNull(d)) {
                    this.h = jSONObject.getInt(d);
                }
            } catch (JSONException e2) {
                k.a(e2);
            }
        }
    }

    public int a() {
        return this.h;
    }

    public void a(int i) {
        this.h = i;
    }

    public void a(long j) {
        this.e = j;
    }

    public long b() {
        return this.e;
    }

    public void b(int i) {
        this.f = i;
    }

    public int c() {
        return this.f;
    }

    public void c(int i) {
        this.g = i;
    }

    public int d() {
        return this.g;
    }

    public String toString() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(f2074a, this.e);
            jSONObject.put(b, this.f);
            jSONObject.put(c, this.g);
            jSONObject.put(d, this.h);
        } catch (JSONException e2) {
            k.a(e2);
        }
        return jSONObject.toString();
    }
}
