package com.tencent.b.a;

import android.content.Context;
import android.util.Log;
import com.tencent.b.b.b;
import com.tencent.b.d.e;
import com.tencent.b.d.k;
import org.json.JSONObject;

public class g {

    /* renamed from: a  reason: collision with root package name */
    protected Context f2068a = null;
    private int b = 0;

    public g(Context context) {
        this.f2068a = context;
        this.b = (int) (System.currentTimeMillis() / 1000);
    }

    public int a() {
        return 2;
    }

    public JSONObject a(JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = new JSONObject();
        }
        jSONObject.put("et", a());
        b(jSONObject);
        return jSONObject;
    }

    /* access modifiers changed from: protected */
    public void b(JSONObject jSONObject) {
        try {
            jSONObject.put("mid", "0");
            jSONObject.put("ts", this.b);
            jSONObject.put("si", this.b);
            k.a(jSONObject, "ui", k.c(this.f2068a));
            k.a(jSONObject, "mc", k.d(this.f2068a));
            b a2 = com.tencent.b.c.g.a(this.f2068a).a();
            if (a2 != null && k.c(a2.c())) {
                jSONObject.put("mid", a2.c());
            }
            new e(this.f2068a).a(jSONObject);
        } catch (Throwable th) {
            Log.e("MID", "encode error.", th);
        }
    }
}
