package com.tencent.tmsecurelite.virusscan;

import android.os.IInterface;
import com.tencent.tmsecurelite.commom.DataEntity;
import java.util.List;

/* compiled from: ProGuard */
public interface IScanListener extends IInterface {
    public static final int T_onCloudScan = 4;
    public static final int T_onCloudScanError = 5;
    public static final int T_onPackageScanProgress = 2;
    public static final int T_onScanCanceled = 8;
    public static final int T_onScanContinue = 7;
    public static final int T_onScanFinished = 10;
    public static final int T_onScanPaused = 6;
    public static final int T_onScanStarted = 1;
    public static final int T_onSdcardScanProgress = 3;

    void onCloudScan();

    void onCloudScanError(int i);

    void onPackageScanProgress(int i, DataEntity dataEntity);

    void onScanCanceled();

    void onScanContinue();

    void onScanFinished(List<DataEntity> list);

    void onScanPaused();

    void onScanStarted();

    void onSdcardScanProgress(int i, DataEntity dataEntity);
}
