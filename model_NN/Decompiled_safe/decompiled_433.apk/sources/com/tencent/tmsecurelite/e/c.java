package com.tencent.tmsecurelite.e;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* compiled from: ProGuard */
public abstract class c extends Binder implements a {
    public c() {
        attachInterface(this, "com.tencent.tmsecurelite.IRootService");
    }

    public static a a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.tencent.tmsecurelite.IRootService");
        if (queryLocalInterface == null || !(queryLocalInterface instanceof a)) {
            return new b(iBinder);
        }
        return (a) queryLocalInterface;
    }

    /* access modifiers changed from: protected */
    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        switch (i) {
            case 1:
                parcel.enforceInterface("com.tencent.tmsecurelite.IRootService");
                a(parcel.readString());
                parcel2.writeNoException();
                return true;
            default:
                return true;
        }
    }
}
