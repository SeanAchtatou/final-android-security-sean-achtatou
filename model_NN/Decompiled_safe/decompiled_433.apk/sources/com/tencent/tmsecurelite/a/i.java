package com.tencent.tmsecurelite.a;

import android.os.IBinder;
import android.os.Parcel;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.commom.b;
import java.util.ArrayList;
import org.json.JSONException;

/* compiled from: ProGuard */
public final class i implements q {

    /* renamed from: a  reason: collision with root package name */
    private IBinder f4030a;

    public i(IBinder iBinder) {
        this.f4030a = iBinder;
    }

    public IBinder asBinder() {
        return this.f4030a;
    }

    public int a() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            this.f4030a.transact(1, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void a(int i) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeInt(i);
            this.f4030a.transact(2, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void a(p pVar, ArrayList<String> arrayList) {
        if (arrayList != null && !arrayList.isEmpty()) {
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
                obtain.writeStrongBinder((IBinder) pVar);
                obtain.writeStringList(arrayList);
                this.f4030a.transact(3, obtain, obtain2, 0);
                obtain2.readException();
            } finally {
                obtain.recycle();
                obtain2.recycle();
            }
        }
    }

    public void a(boolean z) {
        int i = 0;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            if (z) {
                i = 1;
            }
            obtain.writeInt(i);
            this.f4030a.transact(10, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public boolean b() {
        boolean z = false;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            this.f4030a.transact(11, obtain, obtain2, 0);
            obtain2.readException();
            if (obtain2.readInt() == 0) {
                z = true;
            }
            return z;
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public ArrayList<DataEntity> b(int i) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeInt(i);
            this.f4030a.transact(5, obtain, obtain2, 0);
            obtain2.readException();
            return DataEntity.readFromParcel(obtain2);
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void a(int i, r rVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeStrongBinder((IBinder) rVar);
            obtain.writeInt(i);
            this.f4030a.transact(19, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public boolean a(o oVar) {
        boolean z = false;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeStrongBinder((IBinder) oVar);
            this.f4030a.transact(6, obtain, obtain2, 0);
            obtain2.readException();
            if (obtain2.readInt() == 0) {
                z = true;
            }
            return z;
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public boolean b(o oVar) {
        boolean z = false;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeStrongBinder((IBinder) oVar);
            this.f4030a.transact(7, obtain, obtain2, 0);
            obtain2.readException();
            if (obtain2.readInt() == 0) {
                z = true;
            }
            return z;
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void a(m mVar, ArrayList<String> arrayList) {
        if (arrayList != null && !arrayList.isEmpty()) {
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
                obtain.writeStrongBinder((IBinder) mVar);
                obtain.writeStringList(arrayList);
                this.f4030a.transact(8, obtain, obtain2, 0);
                obtain2.readException();
            } finally {
                obtain.recycle();
                obtain2.recycle();
            }
        }
    }

    public void a(n nVar, ArrayList<String> arrayList) {
        if (arrayList != null && !arrayList.isEmpty()) {
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
                obtain.writeStrongBinder((IBinder) nVar);
                obtain.writeStringList(arrayList);
                this.f4030a.transact(9, obtain, obtain2, 0);
                obtain2.readException();
            } finally {
                obtain.recycle();
                obtain2.recycle();
            }
        }
    }

    public boolean c() {
        boolean z = false;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            this.f4030a.transact(12, obtain, obtain2, 0);
            obtain2.readException();
            if (obtain2.readInt() == 0) {
                z = true;
            }
            return z;
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public boolean d() {
        boolean z = false;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            this.f4030a.transact(13, obtain, obtain2, 0);
            obtain2.readException();
            if (obtain2.readInt() == 0) {
                z = true;
            }
            return z;
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int c(int i) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeInt(i);
            this.f4030a.transact(14, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public ArrayList<String> a(int i, int i2) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeInt(i);
            obtain.writeInt(i2);
            this.f4030a.transact(15, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.createStringArrayList();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public DataEntity a(String str) {
        DataEntity dataEntity;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeString(str);
            this.f4030a.transact(16, obtain, obtain2, 0);
            obtain2.readException();
            dataEntity = new DataEntity(obtain2);
        } catch (JSONException e) {
            e.printStackTrace();
            dataEntity = null;
        } catch (Throwable th) {
            obtain.recycle();
            obtain2.recycle();
            throw th;
        }
        obtain.recycle();
        obtain2.recycle();
        return dataEntity;
    }

    public int a(String str, String str2, int i) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeString(str);
            obtain.writeString(str2);
            obtain.writeInt(i);
            this.f4030a.transact(17, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int b(String str, String str2, int i) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeString(str);
            obtain.writeString(str2);
            obtain.writeInt(i);
            this.f4030a.transact(18, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void a(b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4030a.transact(20, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void a(int i, b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeInt(i);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4030a.transact(21, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int b(p pVar, ArrayList<String> arrayList) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeStrongBinder((IBinder) pVar);
            obtain.writeStringList(arrayList);
            this.f4030a.transact(22, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int b(m mVar, ArrayList<String> arrayList) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeStrongBinder((IBinder) mVar);
            obtain.writeStringList(arrayList);
            this.f4030a.transact(27, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int b(n nVar, ArrayList<String> arrayList) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeStrongBinder((IBinder) nVar);
            obtain.writeStringList(arrayList);
            this.f4030a.transact(28, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void a(boolean z, b bVar) {
        int i = 0;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            if (z) {
                i = 1;
            }
            obtain.writeInt(i);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4030a.transact(29, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void b(b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4030a.transact(30, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void b(int i, b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeInt(i);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4030a.transact(24, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int c(o oVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeStrongBinder((IBinder) oVar);
            this.f4030a.transact(25, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int d(o oVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeStrongBinder((IBinder) oVar);
            this.f4030a.transact(26, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void c(b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4030a.transact(31, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void d(b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            this.f4030a.transact(32, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void c(int i, b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeInt(i);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4030a.transact(33, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void a(int i, int i2, b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeInt(i);
            obtain.writeInt(i2);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4030a.transact(34, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void a(String str, b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeString(str);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4030a.transact(35, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void a(String str, String str2, int i, b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeString(str);
            obtain.writeString(str2);
            obtain.writeInt(i);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4030a.transact(36, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void b(String str, String str2, int i, b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IFileSafeEncrypt");
            obtain.writeString(str);
            obtain.writeString(str2);
            obtain.writeInt(i);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4030a.transact(37, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }
}
