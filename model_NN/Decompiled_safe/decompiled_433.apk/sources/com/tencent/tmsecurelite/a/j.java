package com.tencent.tmsecurelite.a;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.commom.b;
import com.tencent.tmsecurelite.commom.d;
import com.tencent.tmsecurelite.optimize.ISystemOptimize;
import java.util.ArrayList;

/* compiled from: ProGuard */
public abstract class j extends Binder implements q {
    public j() {
        attachInterface(this, "com.tencent.tmsecurelite.IFileSafeEncrypt");
    }

    public static q a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
        if (queryLocalInterface == null || !(queryLocalInterface instanceof q)) {
            return new i(iBinder);
        }
        return (q) queryLocalInterface;
    }

    /* access modifiers changed from: protected */
    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        boolean z = false;
        switch (i) {
            case 1:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                int a2 = a();
                parcel2.writeNoException();
                parcel2.writeInt(a2);
                break;
            case 2:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                a(parcel.readInt());
                parcel2.writeNoException();
                break;
            case 3:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                p a3 = h.a(parcel.readStrongBinder());
                ArrayList arrayList = new ArrayList();
                parcel.readStringList(arrayList);
                a(a3, arrayList);
                parcel2.writeNoException();
                break;
            case 4:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                boolean d = d(parcel.readInt());
                parcel2.writeNoException();
                if (!d) {
                    z = true;
                }
                parcel2.writeInt(z ? 1 : 0);
                break;
            case 5:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                ArrayList<DataEntity> b = b(parcel.readInt());
                parcel2.writeNoException();
                DataEntity.writeToParcel(b, parcel2);
                break;
            case 6:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                boolean a4 = a(f.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                if (!a4) {
                    z = true;
                }
                parcel2.writeInt(z ? 1 : 0);
                break;
            case 7:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                boolean b2 = b(f.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                if (!b2) {
                    z = true;
                }
                parcel2.writeInt(z ? 1 : 0);
                break;
            case 8:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                m a5 = b.a(parcel.readStrongBinder());
                ArrayList arrayList2 = new ArrayList();
                parcel.readStringList(arrayList2);
                a(a5, arrayList2);
                parcel2.writeNoException();
                break;
            case 9:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                n a6 = d.a(parcel.readStrongBinder());
                ArrayList arrayList3 = new ArrayList();
                parcel.readStringList(arrayList3);
                a(a6, arrayList3);
                parcel2.writeNoException();
                break;
            case 10:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                if (parcel.readInt() != 0) {
                    z = true;
                }
                a(z);
                parcel2.writeNoException();
                break;
            case 11:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                boolean b3 = b();
                parcel2.writeNoException();
                if (!b3) {
                    z = true;
                }
                parcel2.writeInt(z ? 1 : 0);
                break;
            case 12:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                boolean c = c();
                parcel2.writeNoException();
                if (!c) {
                    z = true;
                }
                parcel2.writeInt(z ? 1 : 0);
                break;
            case 13:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                boolean d2 = d();
                parcel2.writeNoException();
                if (!d2) {
                    z = true;
                }
                parcel2.writeInt(z ? 1 : 0);
                break;
            case 14:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                int c2 = c(parcel.readInt());
                parcel2.writeNoException();
                parcel2.writeInt(c2);
                break;
            case 15:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                ArrayList<String> a7 = a(parcel.readInt(), parcel.readInt());
                parcel2.writeNoException();
                parcel2.writeStringList(a7);
                break;
            case 16:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                DataEntity a8 = a(parcel.readString());
                parcel2.writeNoException();
                a8.writeToParcel(parcel2, 0);
                break;
            case 17:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                int a9 = a(parcel.readString(), parcel.readString(), parcel.readInt());
                parcel2.writeNoException();
                parcel2.writeInt(a9);
                break;
            case 18:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                int b4 = b(parcel.readString(), parcel.readString(), parcel.readInt());
                parcel2.writeNoException();
                parcel2.writeInt(b4);
                break;
            case 19:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                a(parcel.readInt(), l.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 20:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                a(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 21:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                a(parcel.readInt(), d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case ISystemOptimize.T_killTaskAsync /*22*/:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                p a10 = h.a(parcel.readStrongBinder());
                ArrayList arrayList4 = new ArrayList();
                parcel.readStringList(arrayList4);
                int b5 = b(a10, arrayList4);
                parcel2.writeNoException();
                parcel2.writeInt(b5);
                break;
            case ISystemOptimize.T_checkVersionAsync /*24*/:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                b(parcel.readInt(), d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case ISystemOptimize.T_hasRootAsync /*25*/:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                int c3 = c(f.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                parcel2.writeInt(c3);
                break;
            case ISystemOptimize.T_askForRootAsync /*26*/:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                int d3 = d(f.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                parcel2.writeInt(d3);
                break;
            case ISystemOptimize.T_getMemoryUsageAsync /*27*/:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                m a11 = b.a(parcel.readStrongBinder());
                ArrayList arrayList5 = new ArrayList();
                parcel.readStringList(arrayList5);
                int b6 = b(a11, arrayList5);
                parcel2.writeNoException();
                parcel2.writeInt(b6);
                break;
            case ISystemOptimize.T_isMemoryReachWarningAsync /*28*/:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                n a12 = d.a(parcel.readStrongBinder());
                ArrayList arrayList6 = new ArrayList();
                parcel.readStringList(arrayList6);
                int b7 = b(a12, arrayList6);
                parcel2.writeNoException();
                parcel2.writeInt(b7);
                break;
            case ISystemOptimize.T_getSysRubbishAsync /*29*/:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                int readInt = parcel.readInt();
                b a13 = d.a(parcel.readStrongBinder());
                if (readInt != 0) {
                    z = true;
                }
                a(z, a13);
                parcel2.writeNoException();
                break;
            case 30:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                b(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 31:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                c(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 32:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                d(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case ISystemOptimize.T_cancelScanRubbish /*33*/:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                c(parcel.readInt(), d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case ISystemOptimize.T_cleanRubbishAsync /*34*/:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                a(parcel.readInt(), parcel.readInt(), d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 35:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                a(parcel.readString(), d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 36:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                a(parcel.readString(), parcel.readString(), parcel.readInt(), d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 37:
                parcel.enforceInterface("com.tencent.tmsecurelite.IFileSafeEncrypt");
                b(parcel.readString(), parcel.readString(), parcel.readInt(), d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
        }
        return true;
    }

    public boolean d(int i) {
        return 1 >= i;
    }
}
