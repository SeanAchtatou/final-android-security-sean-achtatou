package com.tencent.securemodule.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.qq.jce.wup.UniAttribute;
import com.tencent.assistant.AppConst;
import com.tencent.securemodule.impl.AppInfo;
import com.tencent.securemodule.impl.SecureService;
import com.tencent.securemodule.service.IControlService;

public class TransparentActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private final int f4024a = 1;
    private final int b = 2;
    private final int c = 3;
    private final int d = 4;
    private final int e = 1;
    private final int f = 0;
    private final int g = 5;
    /* access modifiers changed from: private */
    public Context h;
    /* access modifiers changed from: private */
    public e i;
    private AppInfo j;
    /* access modifiers changed from: private */
    public ProgressDialog k = null;
    /* access modifiers changed from: private */
    public Handler l = new ah(this);
    /* access modifiers changed from: private */
    public IControlService m;
    private ServiceConnection n = new aj(this);

    public class a extends Thread implements Runnable {
        private int b;

        public a(int i) {
            this.b = i;
        }

        /* access modifiers changed from: private */
        public synchronized void a() {
            if (this.b < 5) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Message obtainMessage = TransparentActivity.this.l.obtainMessage(4);
                obtainMessage.arg1 = this.b + 1;
                TransparentActivity.this.l.sendMessage(obtainMessage);
            } else {
                Message obtainMessage2 = TransparentActivity.this.l.obtainMessage(2);
                obtainMessage2.arg1 = 0;
                TransparentActivity.this.l.sendMessage(obtainMessage2);
            }
            return;
        }

        public void run() {
            boolean z = false;
            try {
                if (TransparentActivity.this.m != null) {
                    UniAttribute uniAttribute = new UniAttribute();
                    uniAttribute.setEncodeName("UTF-8");
                    uniAttribute.put("data", TransparentActivity.this.i);
                    byte[] encode = uniAttribute.encode();
                    if (encode != null) {
                        TransparentActivity.this.m.doRemoteTask(ay.a(encode), new aq(this));
                    }
                    z = true;
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
            if (!z) {
                a();
            }
        }
    }

    private void a() {
        this.i = (e) getIntent().getSerializableExtra("data");
        if (this.i != null) {
            c();
        } else {
            finish();
        }
    }

    private void a(boolean z) {
        new AlertDialog.Builder(this).setTitle("QQ安全登录扫描").setMessage(("发现“" + this.j.getSoftName() + "应用”被病毒感染，") + (z ? "建议立即启动腾讯手机管家查杀" : "建议立即安装最新版腾讯手机管家查杀")).setPositiveButton("确定", new ap(this, z)).setNegativeButton("取消", new ao(this)).setOnCancelListener(new an(this)).show();
    }

    private void b() {
        this.j = (AppInfo) getIntent().getSerializableExtra("data");
        if (this.j != null) {
            AppInfo a2 = ar.a(this.h, AppConst.TENCENT_MOBILE_MANAGER_PKGNAME);
            if (a2 == null || !"00B1208638DE0FCD3E920886D658DAF6".equals(a2.getCertMd5()) || a2.getVersionCode() < 77) {
                a(false);
            } else {
                a(true);
            }
        } else {
            finish();
        }
    }

    private void c() {
        t b2 = this.i.b();
        if (b2 != null) {
            String a2 = b2.a();
            AlertDialog.Builder onCancelListener = new AlertDialog.Builder(this).setTitle(a2).setMessage(b2.b()).setPositiveButton("确定", new al(this)).setOnCancelListener(new ak(this));
            if (b2.d() != 1) {
                onCancelListener.setNegativeButton("取消", new am(this));
            }
            onCancelListener.show();
            return;
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setBackgroundDrawable(getResources().getDrawable(17170445));
        this.h = this;
        Intent intent = new Intent();
        intent.setClass(this.h, SecureService.class);
        bindService(intent, this.n, 1);
        String action = getIntent().getAction();
        if ("1000040".equals(action)) {
            a();
        } else if ("1000041".equals(action)) {
            b();
        } else {
            finish();
        }
    }

    public void onDestroy() {
        if (this.n != null) {
            unbindService(this.n);
        }
        super.onDestroy();
    }
}
