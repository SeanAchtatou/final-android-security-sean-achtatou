package com.qq.k;

import android.content.Context;
import android.content.SharedPreferences;
import com.qq.util.j;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public static int f300a = 0;
    public static int b = -1;
    public static int c = -1;
    public static boolean d = false;

    public static void a(Context context, long j) {
        SharedPreferences sharedPreferences;
        if (j.c >= 11) {
            sharedPreferences = context.getSharedPreferences("com.qq.connect", 4);
        } else {
            sharedPreferences = context.getSharedPreferences("com.qq.connect", 0);
        }
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putLong("photo_backup_timestamp", j);
        edit.commit();
    }

    public static long a(Context context) {
        SharedPreferences sharedPreferences;
        if (j.c >= 11) {
            sharedPreferences = context.getSharedPreferences("com.qq.connect", 4);
        } else {
            sharedPreferences = context.getSharedPreferences("com.qq.connect", 0);
        }
        try {
            return sharedPreferences.getLong("photo_backup_timestamp", 0);
        } catch (Throwable th) {
            th.printStackTrace();
            return 0;
        }
    }

    public static void b(Context context) {
        if (!d) {
            SharedPreferences.Editor edit = context.getSharedPreferences("com.qq.connect", 0).edit();
            edit.putBoolean("bAppCache", true);
            edit.commit();
            d = true;
        }
    }

    public static boolean c(Context context) {
        if (d) {
            return d;
        }
        d = context.getSharedPreferences("com.qq.connect", 0).getBoolean("bAppCache", false);
        return d;
    }
}
