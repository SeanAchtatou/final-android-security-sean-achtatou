package com.qq.m;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.qq.AppService.AppService;
import com.qq.i.a;
import com.tencent.wcs.jce.ReportQQLoginStatusRequest;

/* compiled from: ProGuard */
public class f extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private a f314a = null;
    private Context b = null;
    private ReportQQLoginStatusRequest c = null;
    private g d = null;

    public f(Context context, long j, String str, int i, g gVar) {
        this.b = context;
        this.c = new ReportQQLoginStatusRequest();
        this.c.b(i);
        this.c.a(2);
        this.c.a(j);
        this.c.b(str);
        this.c.a(AppService.v());
        this.d = gVar;
    }

    private boolean a() {
        NetworkInfo b2 = b();
        if (b2 == null || !b2.isConnected() || b2.getType() != 1) {
            return false;
        }
        return true;
    }

    private NetworkInfo b() {
        return ((ConnectivityManager) this.b.getSystemService("connectivity")).getActiveNetworkInfo();
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x014e A[Catch:{ Throwable -> 0x0188 }] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0190  */
    /* JADX WARNING: Removed duplicated region for block: B:90:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r14 = this;
            r13 = 4
            r12 = 1
            r0 = 0
            r9 = 0
            super.run()
            java.lang.String r1 = "com.qq.connect"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "ReportQQLoginThread Thread start for uin "
            java.lang.StringBuilder r2 = r2.append(r3)
            com.tencent.wcs.jce.ReportQQLoginStatusRequest r3 = r14.c
            long r3 = r3.a()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = " type "
            java.lang.StringBuilder r2 = r2.append(r3)
            com.tencent.wcs.jce.ReportQQLoginStatusRequest r3 = r14.c
            int r3 = r3.b()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            android.util.Log.d(r1, r2)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "ReportQQLoginThread Thread start for uin "
            java.lang.StringBuilder r1 = r1.append(r2)
            com.tencent.wcs.jce.ReportQQLoginStatusRequest r2 = r14.c
            long r2 = r2.a()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " type "
            java.lang.StringBuilder r1 = r1.append(r2)
            com.tencent.wcs.jce.ReportQQLoginStatusRequest r2 = r14.c
            int r2 = r2.b()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.tencent.wcs.c.b.a(r1)
            r11 = 5
            r10 = r0
            r6 = r0
            r7 = r0
            r8 = r9
        L_0x0066:
            if (r10 > r11) goto L_0x020f
            boolean r0 = r14.a()     // Catch:{ Throwable -> 0x01a3 }
            if (r0 == 0) goto L_0x020f
            com.qq.h.b r0 = com.qq.h.b.a()     // Catch:{ Throwable -> 0x01a3 }
            java.lang.String r1 = r0.j()     // Catch:{ Throwable -> 0x01a3 }
            com.qq.i.a r0 = new com.qq.i.a     // Catch:{ Throwable -> 0x01a3 }
            r2 = 1
            r3 = 0
            r4 = 0
            r5 = 0
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x01a3 }
            r14.f314a = r0     // Catch:{ Throwable -> 0x01a3 }
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01a3 }
            r1 = 10000(0x2710, float:1.4013E-41)
            r0.a(r1)     // Catch:{ Throwable -> 0x01a3 }
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01a3 }
            r1 = 15000(0x3a98, float:2.102E-41)
            r0.b(r1)     // Catch:{ Throwable -> 0x01a3 }
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01a3 }
            r0.b()     // Catch:{ Throwable -> 0x01a3 }
            com.tencent.wcs.jce.ReportQQLoginStatusRequest r0 = r14.c     // Catch:{ Throwable -> 0x01a3 }
            int r0 = r0.b()     // Catch:{ Throwable -> 0x01a3 }
            if (r0 != r12) goto L_0x0199
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01a3 }
            java.net.InetAddress r0 = r0.e()     // Catch:{ Throwable -> 0x01a3 }
            if (r0 != 0) goto L_0x00bc
            r0 = r6
            r1 = r7
            r2 = r8
        L_0x00a7:
            com.qq.i.a r3 = r14.f314a
            if (r3 == 0) goto L_0x00b2
            com.qq.i.a r3 = r14.f314a
            r3.f()
            r14.f314a = r9
        L_0x00b2:
            com.qq.m.g r3 = r14.d
            if (r3 == 0) goto L_0x00bb
            com.qq.m.g r3 = r14.d
            r3.a(r1, r0, r2)
        L_0x00bb:
            return
        L_0x00bc:
            com.qq.g.a r1 = new com.qq.g.a     // Catch:{ Throwable -> 0x01a3 }
            r1.<init>()     // Catch:{ Throwable -> 0x01a3 }
            java.lang.String r2 = com.qq.AppService.AppService.i     // Catch:{ Throwable -> 0x01a3 }
            r1.n = r2     // Catch:{ Throwable -> 0x01a3 }
            android.content.Context r2 = r14.b     // Catch:{ Throwable -> 0x01a3 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01a3 }
            r3.<init>()     // Catch:{ Throwable -> 0x01a3 }
            com.tencent.wcs.jce.ReportQQLoginStatusRequest r4 = r14.c     // Catch:{ Throwable -> 0x01a3 }
            long r4 = r4.a()     // Catch:{ Throwable -> 0x01a3 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x01a3 }
            java.lang.String r4 = ""
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x01a3 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x01a3 }
            r1.a(r2, r3)     // Catch:{ Throwable -> 0x01a3 }
            android.net.NetworkInfo r2 = r14.b()     // Catch:{ Throwable -> 0x01a3 }
            r1.a(r2)     // Catch:{ Throwable -> 0x01a3 }
            android.content.Context r2 = r14.b     // Catch:{ Throwable -> 0x01a3 }
            r1.a(r2, r0)     // Catch:{ Throwable -> 0x01a3 }
            byte[] r0 = com.qq.g.a.a(r1)     // Catch:{ Throwable -> 0x01a3 }
            com.tencent.wcs.jce.ReportQQLoginStatusRequest r1 = r14.c     // Catch:{ Throwable -> 0x01a3 }
            r1.a(r0)     // Catch:{ Throwable -> 0x01a3 }
        L_0x00f8:
            com.tencent.wcs.jce.ReportQQLoginStatusRequest r0 = r14.c     // Catch:{ Throwable -> 0x01a3 }
            byte[] r0 = com.tencent.assistant.utils.an.a(r0)     // Catch:{ Throwable -> 0x01a3 }
            r1 = 1017(0x3f9, float:1.425E-42)
            byte[] r1 = com.qq.AppService.r.a(r1)     // Catch:{ Throwable -> 0x01a3 }
            byte[] r0 = com.qq.AppService.r.a(r1, r0)     // Catch:{ Throwable -> 0x01a3 }
            com.qq.i.a r1 = r14.f314a     // Catch:{ Throwable -> 0x01a3 }
            if (r1 == 0) goto L_0x0111
            com.qq.i.a r1 = r14.f314a     // Catch:{ Throwable -> 0x01a3 }
            r1.a(r0)     // Catch:{ Throwable -> 0x01a3 }
        L_0x0111:
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01a3 }
            if (r0 == 0) goto L_0x011a
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01a9 }
            r0.c()     // Catch:{ Throwable -> 0x01a9 }
        L_0x011a:
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01a3 }
            if (r0 == 0) goto L_0x020c
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01bc }
            r1 = 1
            byte[] r0 = r0.a(r1)     // Catch:{ Throwable -> 0x01bc }
        L_0x0125:
            if (r0 == 0) goto L_0x020c
            int r1 = r0.length     // Catch:{ Throwable -> 0x01a3 }
            if (r1 <= r13) goto L_0x020c
            com.qq.AppService.r.a(r0)     // Catch:{ Throwable -> 0x01a3 }
            com.qq.taf.jce.JceInputStream r1 = new com.qq.taf.jce.JceInputStream     // Catch:{ Throwable -> 0x01a3 }
            r2 = 4
            r1.<init>(r0, r2)     // Catch:{ Throwable -> 0x01a3 }
            com.tencent.wcs.jce.ReportQQLoginStatusResponse r2 = new com.tencent.wcs.jce.ReportQQLoginStatusResponse     // Catch:{ Throwable -> 0x01a3 }
            r2.<init>()     // Catch:{ Throwable -> 0x01a3 }
            r0 = -1
            r2.f4100a = r0     // Catch:{ Throwable -> 0x01c9 }
            r2.readFrom(r1)     // Catch:{ Exception -> 0x01c3 }
        L_0x013e:
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01c9 }
            int r1 = r0.d()     // Catch:{ Throwable -> 0x01c9 }
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x0207 }
            int r0 = r0.a()     // Catch:{ Throwable -> 0x0207 }
            com.qq.i.a r3 = r14.f314a     // Catch:{ Throwable -> 0x0188 }
            if (r3 == 0) goto L_0x0156
            com.qq.i.a r3 = r14.f314a     // Catch:{ Throwable -> 0x0188 }
            r3.f()     // Catch:{ Throwable -> 0x0188 }
            r3 = 0
            r14.f314a = r3     // Catch:{ Throwable -> 0x0188 }
        L_0x0156:
            if (r2 == 0) goto L_0x01ce
            int r3 = r2.f4100a     // Catch:{ Throwable -> 0x0188 }
            if (r3 != 0) goto L_0x01ce
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0188 }
            r3.<init>()     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r4 = "ReportQQLoginThread success,response:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x0188 }
            java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r4 = " err:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x0188 }
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r4 = "ReportQQLoginStatusResponse  ..."
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x0188 }
            java.lang.StringBuilder r3 = r3.append(r2)     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x0188 }
            com.tencent.wcs.c.b.a(r3)     // Catch:{ Throwable -> 0x0188 }
            goto L_0x00a7
        L_0x0188:
            r3 = move-exception
        L_0x0189:
            r3.printStackTrace()     // Catch:{ all -> 0x01af }
            com.qq.i.a r3 = r14.f314a
            if (r3 == 0) goto L_0x00b2
            com.qq.i.a r3 = r14.f314a
            r3.f()
            r14.f314a = r9
            goto L_0x00b2
        L_0x0199:
            com.tencent.wcs.jce.ReportQQLoginStatusRequest r0 = r14.c     // Catch:{ Throwable -> 0x01a3 }
            r1 = 0
            byte[] r1 = new byte[r1]     // Catch:{ Throwable -> 0x01a3 }
            r0.a(r1)     // Catch:{ Throwable -> 0x01a3 }
            goto L_0x00f8
        L_0x01a3:
            r0 = move-exception
            r3 = r0
            r1 = r7
            r2 = r8
            r0 = r6
            goto L_0x0189
        L_0x01a9:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Throwable -> 0x01a3 }
            goto L_0x011a
        L_0x01af:
            r0 = move-exception
            com.qq.i.a r1 = r14.f314a
            if (r1 == 0) goto L_0x01bb
            com.qq.i.a r1 = r14.f314a
            r1.f()
            r14.f314a = r9
        L_0x01bb:
            throw r0
        L_0x01bc:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Throwable -> 0x01a3 }
            r0 = r9
            goto L_0x0125
        L_0x01c3:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Throwable -> 0x01c9 }
            goto L_0x013e
        L_0x01c9:
            r0 = move-exception
            r3 = r0
            r1 = r7
            r0 = r6
            goto L_0x0189
        L_0x01ce:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0188 }
            r3.<init>()     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r4 = "ReportQQLoginThread failed,response:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x0188 }
            java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r4 = " err:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x0188 }
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r4 = " retry: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x0188 }
            java.lang.StringBuilder r3 = r3.append(r10)     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x0188 }
            com.tencent.wcs.c.b.a(r3)     // Catch:{ Throwable -> 0x0188 }
            com.qq.h.b r3 = com.qq.h.b.a()     // Catch:{ Throwable -> 0x0188 }
            r3.k()     // Catch:{ Throwable -> 0x0188 }
            int r3 = r10 + 1
            r10 = r3
            r6 = r0
            r7 = r1
            r8 = r2
            goto L_0x0066
        L_0x0207:
            r0 = move-exception
            r3 = r0
            r0 = r6
            goto L_0x0189
        L_0x020c:
            r2 = r8
            goto L_0x013e
        L_0x020f:
            r0 = r6
            r1 = r7
            r2 = r8
            goto L_0x00a7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.m.f.run():void");
    }
}
