package com.qq.m;

import android.content.Context;
import com.qq.AppService.AppService;
import com.qq.i.a;
import com.tencent.wcs.jce.MInviteRequest;

/* compiled from: ProGuard */
public class b extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private a f312a = null;
    private volatile boolean b = false;
    private Context c = null;
    private MInviteRequest d = null;
    private String e;
    private c f;

    public b(Context context, String str, String str2, c cVar) {
        a(context, str, str2, cVar);
    }

    public b(Context context, String str, String str2, c cVar, String str3) {
        this.e = str3;
        a(context, str, str2, cVar);
    }

    private void a(Context context, String str, String str2, c cVar) {
        this.c = context;
        this.d = new MInviteRequest();
        this.d.b(str);
        this.d.d(str2);
        this.d.a(AppService.v());
        this.f = cVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:97:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r14 = this;
            super.run()
            r0 = 1
            r14.b = r0
            r8 = 0
            r7 = 0
            r6 = 0
            java.lang.String r0 = "com.qq.connect"
            java.lang.String r1 = "Invite Thread start"
            android.util.Log.d(r0, r1)
            android.content.Context r0 = r14.c
            java.lang.String r1 = "connectivity"
            java.lang.Object r0 = r0.getSystemService(r1)
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0
            android.net.NetworkInfo r10 = r0.getActiveNetworkInfo()
            if (r10 == 0) goto L_0x0026
            boolean r0 = r10.isConnected()
            if (r0 != 0) goto L_0x0029
        L_0x0026:
            r0 = 0
            r14.b = r0
        L_0x0029:
            boolean r0 = r14.b
            if (r0 != 0) goto L_0x0034
            java.lang.String r0 = "com.qq.connect"
            java.lang.String r1 = "Invite Thread check network failed::"
            android.util.Log.d(r0, r1)
        L_0x0034:
            long r11 = java.lang.System.currentTimeMillis()
            r0 = 0
            r9 = r0
        L_0x003a:
            if (r9 > 0) goto L_0x0099
            boolean r0 = r14.b     // Catch:{ Throwable -> 0x022c }
            if (r0 == 0) goto L_0x0099
            com.qq.i.a r0 = r14.f312a     // Catch:{ Throwable -> 0x022c }
            if (r0 == 0) goto L_0x0049
            com.qq.i.a r0 = r14.f312a     // Catch:{ Throwable -> 0x022c }
            r0.f()     // Catch:{ Throwable -> 0x022c }
        L_0x0049:
            com.qq.h.b r0 = com.qq.h.b.a()     // Catch:{ Throwable -> 0x022c }
            java.lang.String r1 = r0.j()     // Catch:{ Throwable -> 0x022c }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x022c }
            r0.<init>()     // Catch:{ Throwable -> 0x022c }
            java.lang.String r2 = "InvitePCThread url: "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x022c }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x022c }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x022c }
            com.tencent.wcs.c.b.a(r0)     // Catch:{ Throwable -> 0x022c }
            com.qq.i.a r0 = new com.qq.i.a     // Catch:{ Throwable -> 0x022c }
            r2 = 1
            r3 = 0
            r4 = 0
            r5 = 0
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x022c }
            r14.f312a = r0     // Catch:{ Throwable -> 0x022c }
            com.qq.i.a r0 = r14.f312a     // Catch:{ Throwable -> 0x022c }
            if (r0 == 0) goto L_0x0099
            com.qq.i.a r0 = r14.f312a     // Catch:{ Throwable -> 0x022c }
            java.lang.String r1 = "text/json;charset=utf-8"
            r0.a(r1)     // Catch:{ Throwable -> 0x022c }
            com.qq.i.a r0 = r14.f312a     // Catch:{ Throwable -> 0x022c }
            r1 = 35000(0x88b8, float:4.9045E-41)
            r0.a(r1)     // Catch:{ Throwable -> 0x022c }
            com.qq.i.a r0 = r14.f312a     // Catch:{ Throwable -> 0x022c }
            r1 = 15000(0x3a98, float:2.102E-41)
            r0.b(r1)     // Catch:{ Throwable -> 0x022c }
            com.qq.i.a r0 = r14.f312a     // Catch:{ Throwable -> 0x022c }
            r0.b()     // Catch:{ Throwable -> 0x022c }
            com.qq.i.a r0 = r14.f312a     // Catch:{ Throwable -> 0x022c }
            if (r0 == 0) goto L_0x0099
            boolean r0 = r14.b     // Catch:{ Throwable -> 0x022c }
            if (r0 != 0) goto L_0x00a6
        L_0x0099:
            r0 = r6
            r1 = r7
            r2 = r8
        L_0x009c:
            com.qq.m.c r3 = r14.f
            if (r3 == 0) goto L_0x00a5
            com.qq.m.c r3 = r14.f
            r3.a(r1, r0, r2)
        L_0x00a5:
            return
        L_0x00a6:
            com.qq.i.a r0 = r14.f312a     // Catch:{ Throwable -> 0x022c }
            java.net.InetAddress r0 = r0.e()     // Catch:{ Throwable -> 0x022c }
            if (r0 == 0) goto L_0x0099
            com.tencent.wcs.jce.MInviteRequest r1 = r14.d     // Catch:{ Throwable -> 0x022c }
            java.lang.String r2 = r0.getHostAddress()     // Catch:{ Throwable -> 0x022c }
            r1.c(r2)     // Catch:{ Throwable -> 0x022c }
            com.qq.g.a r3 = new com.qq.g.a     // Catch:{ Throwable -> 0x022c }
            r3.<init>()     // Catch:{ Throwable -> 0x022c }
            java.lang.String r1 = com.qq.AppService.AppService.i     // Catch:{ Throwable -> 0x022c }
            r3.n = r1     // Catch:{ Throwable -> 0x022c }
            java.lang.String r1 = r14.e     // Catch:{ Throwable -> 0x022c }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Throwable -> 0x022c }
            if (r1 == 0) goto L_0x0220
            android.content.Context r1 = r14.c     // Catch:{ Throwable -> 0x022c }
            r3.a(r1)     // Catch:{ Throwable -> 0x022c }
            r1 = 2
            r3.f290a = r1     // Catch:{ Throwable -> 0x022c }
        L_0x00d0:
            r3.a(r10)     // Catch:{ Throwable -> 0x022c }
            android.content.Context r1 = r14.c     // Catch:{ Throwable -> 0x022c }
            r3.a(r1, r0)     // Catch:{ Throwable -> 0x022c }
            byte[] r0 = com.qq.g.a.a(r3)     // Catch:{ Throwable -> 0x022c }
            com.tencent.wcs.jce.MInviteRequest r1 = r14.d     // Catch:{ Throwable -> 0x022c }
            r1.a(r0)     // Catch:{ Throwable -> 0x022c }
            com.tencent.wcs.jce.MInviteRequest r0 = r14.d     // Catch:{ Throwable -> 0x022c }
            byte[] r0 = r0.toByteArray()     // Catch:{ Throwable -> 0x022c }
            r1 = 1005(0x3ed, float:1.408E-42)
            byte[] r1 = com.qq.AppService.r.a(r1)     // Catch:{ Throwable -> 0x022c }
            byte[] r4 = com.qq.AppService.r.a(r1, r0)     // Catch:{ Throwable -> 0x022c }
            com.qq.i.a r0 = r14.f312a     // Catch:{ Throwable -> 0x022c }
            if (r0 == 0) goto L_0x00fa
            com.qq.i.a r0 = r14.f312a     // Catch:{ Throwable -> 0x022c }
            r0.a(r4)     // Catch:{ Throwable -> 0x022c }
        L_0x00fa:
            com.qq.i.a r0 = r14.f312a     // Catch:{ Throwable -> 0x022c }
            if (r0 == 0) goto L_0x0107
            boolean r0 = r14.b     // Catch:{ Throwable -> 0x022c }
            if (r0 == 0) goto L_0x0107
            com.qq.i.a r0 = r14.f312a     // Catch:{ Throwable -> 0x0236 }
            r0.c()     // Catch:{ Throwable -> 0x0236 }
        L_0x0107:
            com.qq.i.a r0 = r14.f312a     // Catch:{ Throwable -> 0x022c }
            if (r0 == 0) goto L_0x015c
            boolean r0 = r14.b     // Catch:{ Throwable -> 0x022c }
            if (r0 == 0) goto L_0x015c
            r1 = 0
            com.qq.i.a r0 = r14.f312a     // Catch:{ Throwable -> 0x023c }
            r2 = 1
            byte[] r0 = r0.a(r2)     // Catch:{ Throwable -> 0x023c }
        L_0x0117:
            if (r0 == 0) goto L_0x015c
            int r1 = r0.length     // Catch:{ Throwable -> 0x022c }
            r2 = 4
            if (r1 <= r2) goto L_0x015c
            int r1 = com.qq.AppService.r.a(r0)     // Catch:{ Throwable -> 0x022c }
            java.lang.String r2 = "com.qq.connect"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x022c }
            r5.<init>()     // Catch:{ Throwable -> 0x022c }
            java.lang.String r13 = "InvitePCThread ...."
            java.lang.StringBuilder r5 = r5.append(r13)     // Catch:{ Throwable -> 0x022c }
            java.lang.StringBuilder r1 = r5.append(r1)     // Catch:{ Throwable -> 0x022c }
            java.lang.String r5 = ".["
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ Throwable -> 0x022c }
            int r5 = r0.length     // Catch:{ Throwable -> 0x022c }
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ Throwable -> 0x022c }
            java.lang.String r5 = "]"
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ Throwable -> 0x022c }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x022c }
            android.util.Log.d(r2, r1)     // Catch:{ Throwable -> 0x022c }
            com.qq.taf.jce.JceInputStream r1 = new com.qq.taf.jce.JceInputStream     // Catch:{ Throwable -> 0x022c }
            r2 = 4
            r1.<init>(r0, r2)     // Catch:{ Throwable -> 0x022c }
            com.tencent.wcs.jce.MInviteResponse r2 = new com.tencent.wcs.jce.MInviteResponse     // Catch:{ Throwable -> 0x022c }
            r2.<init>()     // Catch:{ Throwable -> 0x022c }
            r0 = -1
            r2.b = r0     // Catch:{ Throwable -> 0x0274 }
            r2.readFrom(r1)     // Catch:{ Exception -> 0x0243 }
            r8 = r2
        L_0x015c:
            com.qq.i.a r0 = r14.f312a     // Catch:{ Throwable -> 0x022c }
            int r7 = r0.d()     // Catch:{ Throwable -> 0x022c }
            com.qq.i.a r0 = r14.f312a     // Catch:{ Throwable -> 0x022c }
            int r6 = r0.a()     // Catch:{ Throwable -> 0x022c }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x022c }
            r0.<init>()     // Catch:{ Throwable -> 0x022c }
            java.lang.String r1 = "InvitePCThread response "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x022c }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Throwable -> 0x022c }
            java.lang.String r1 = " err "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x022c }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ Throwable -> 0x022c }
            java.lang.String r1 = " response "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x022c }
            java.lang.StringBuilder r0 = r0.append(r8)     // Catch:{ Throwable -> 0x022c }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x022c }
            com.tencent.wcs.c.b.a(r0)     // Catch:{ Throwable -> 0x022c }
            if (r8 == 0) goto L_0x024a
            if (r4 == 0) goto L_0x024a
            java.lang.String r0 = "InvitePCThread"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x022c }
            r1.<init>()     // Catch:{ Throwable -> 0x022c }
            java.lang.String r2 = "response "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x022c }
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ Throwable -> 0x022c }
            java.lang.String r2 = " err "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x022c }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ Throwable -> 0x022c }
            java.lang.String r2 = " postData "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x022c }
            int r2 = r4.length     // Catch:{ Throwable -> 0x022c }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x022c }
            java.lang.String r2 = " obj.password "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x022c }
            java.lang.String r2 = r3.h     // Catch:{ Throwable -> 0x022c }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x022c }
            java.lang.String r2 = " result "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x022c }
            int r2 = r8.b     // Catch:{ Throwable -> 0x022c }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x022c }
            java.lang.String r2 = " mid "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x022c }
            java.lang.String r2 = r8.f4089a     // Catch:{ Throwable -> 0x022c }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x022c }
            java.lang.String r2 = " pc "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x022c }
            com.tencent.wcs.jce.MInviteRequest r2 = r14.d     // Catch:{ Throwable -> 0x022c }
            java.lang.String r2 = r2.b     // Catch:{ Throwable -> 0x022c }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x022c }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x022c }
            android.util.Log.d(r0, r1)     // Catch:{ Throwable -> 0x022c }
        L_0x01f5:
            com.qq.i.a r0 = r14.f312a     // Catch:{ Throwable -> 0x022c }
            if (r0 == 0) goto L_0x01fe
            com.qq.i.a r0 = r14.f312a     // Catch:{ Throwable -> 0x022c }
            r0.f()     // Catch:{ Throwable -> 0x022c }
        L_0x01fe:
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x022c }
            java.lang.String r2 = "com.qq.connect"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x022c }
            r3.<init>()     // Catch:{ Throwable -> 0x022c }
            java.lang.String r4 = "Invite spent"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x022c }
            long r0 = r0 - r11
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Throwable -> 0x022c }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x022c }
            android.util.Log.d(r2, r0)     // Catch:{ Throwable -> 0x022c }
            int r0 = r9 + 1
            r9 = r0
            goto L_0x003a
        L_0x0220:
            android.content.Context r1 = r14.c     // Catch:{ Throwable -> 0x022c }
            java.lang.String r2 = r14.e     // Catch:{ Throwable -> 0x022c }
            r3.a(r1, r2)     // Catch:{ Throwable -> 0x022c }
            r1 = 1
            r3.f290a = r1     // Catch:{ Throwable -> 0x022c }
            goto L_0x00d0
        L_0x022c:
            r0 = move-exception
            r3 = r0
            r1 = r7
            r2 = r8
            r0 = r6
        L_0x0231:
            r3.printStackTrace()
            goto L_0x009c
        L_0x0236:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Throwable -> 0x022c }
            goto L_0x0107
        L_0x023c:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Throwable -> 0x022c }
            r0 = r1
            goto L_0x0117
        L_0x0243:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Throwable -> 0x0274 }
            r8 = r2
            goto L_0x015c
        L_0x024a:
            java.lang.String r0 = "InvitePCThread"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x022c }
            r1.<init>()     // Catch:{ Throwable -> 0x022c }
            java.lang.String r2 = "response "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x022c }
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ Throwable -> 0x022c }
            java.lang.String r2 = " err "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x022c }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ Throwable -> 0x022c }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x022c }
            android.util.Log.d(r0, r1)     // Catch:{ Throwable -> 0x022c }
            com.qq.h.b r0 = com.qq.h.b.a()     // Catch:{ Throwable -> 0x022c }
            r0.k()     // Catch:{ Throwable -> 0x022c }
            goto L_0x01f5
        L_0x0274:
            r0 = move-exception
            r3 = r0
            r1 = r7
            r0 = r6
            goto L_0x0231
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.m.b.run():void");
    }
}
