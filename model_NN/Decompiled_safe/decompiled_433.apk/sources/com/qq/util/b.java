package com.qq.util;

import android.text.TextUtils;
import com.qq.AppService.AstApp;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

/* compiled from: ProGuard */
public class b {
    private static b d;

    /* renamed from: a  reason: collision with root package name */
    private d f361a;
    private ConcurrentHashMap<String, WeakReference<f>> b;
    private ReferenceQueue<f> c;

    private b() {
        this.b = null;
        this.c = null;
        this.b = new ConcurrentHashMap<>();
        this.c = new ReferenceQueue<>();
    }

    public void a() {
        this.b.clear();
    }

    public static synchronized b b() {
        b bVar;
        synchronized (b.class) {
            if (d == null) {
                d = new b();
            }
            bVar = d;
        }
        return bVar;
    }

    public int a(String str) {
        if (TextUtils.isEmpty(str)) {
            return 0;
        }
        WeakReference weakReference = this.b.get(str);
        if (weakReference != null) {
            f fVar = (f) weakReference.get();
            if (fVar != null) {
                return fVar.f365a;
            }
            this.b.remove(str);
        }
        return 0;
    }

    private String a(Reference<? extends f> reference) {
        for (String next : this.b.keySet()) {
            if (this.b.get(next) == reference) {
                return next;
            }
        }
        return null;
    }

    public void a(int i, String str) {
        if (!TextUtils.isEmpty(str)) {
            while (true) {
                Reference<? extends f> poll = this.c.poll();
                if (poll == null) {
                    break;
                }
                String a2 = a(poll);
                if (a2 != null) {
                    this.b.remove(a2);
                }
            }
            WeakReference weakReference = this.b.get(str);
            if ((weakReference == null || weakReference.get() == null) && !this.b.containsKey(str)) {
                this.b.put(str, new WeakReference(new f(this, i, str), this.c));
            }
        }
    }

    public ArrayList<String> c() {
        if (this.f361a == null || this.f361a.d == null || this.f361a.d.size() <= 0) {
            return null;
        }
        ArrayList<String> arrayList = new ArrayList<>();
        Iterator<e> it = this.f361a.d.iterator();
        while (it.hasNext()) {
            e next = it.next();
            if (next != null && !TextUtils.isEmpty(next.b)) {
                arrayList.add(next.b);
            }
        }
        return arrayList;
    }

    private boolean a(String str, ArrayList<String> arrayList) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        if (arrayList == null || arrayList.size() <= 0) {
            return false;
        }
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            String str2 = arrayList.get(i);
            if (str2 != null && str.endsWith(str2)) {
                return true;
            }
        }
        return false;
    }

    public int b(String str) {
        if (str == null || str.trim().length() == 0) {
            return 0;
        }
        int a2 = a(str);
        if (a2 > 0) {
            return a2;
        }
        int lastIndexOf = str.lastIndexOf(47);
        if (lastIndexOf == -1) {
            return 0;
        }
        String substring = str.substring(0, lastIndexOf);
        if (this.f361a != null) {
            if (i(substring) || g(substring)) {
                a2 = 1;
            } else if (e(substring)) {
                a2 = 2;
            } else if (d(substring)) {
                a2 = 3;
            } else {
                a2 = f(substring);
            }
        }
        if (a2 <= 0) {
            return a2;
        }
        a(a2, str);
        return a2;
    }

    private boolean d(String str) {
        if (this.f361a != null) {
            return a(str, this.f361a.f);
        }
        return false;
    }

    private boolean e(String str) {
        if (this.f361a != null) {
            return a(str, this.f361a.g);
        }
        return false;
    }

    public int c(String str) {
        int lastIndexOf;
        if (str == null || str.trim().length() == 0 || (lastIndexOf = str.lastIndexOf(47)) == -1) {
            return 0;
        }
        return f(str.substring(0, lastIndexOf));
    }

    private int f(String str) {
        int i;
        if (this.f361a == null || this.f361a.d == null || this.f361a.d.size() <= 0) {
            return 0;
        }
        ArrayList<e> arrayList = this.f361a.d;
        int size = arrayList.size();
        int i2 = 0;
        while (true) {
            if (i2 < size) {
                e eVar = arrayList.get(i2);
                if (eVar != null && !TextUtils.isEmpty(eVar.b) && str.endsWith(eVar.b)) {
                    i = eVar.f364a;
                    break;
                }
                i2++;
            } else {
                i = 0;
                break;
            }
        }
        if (i == 0) {
            return h(str);
        }
        return i;
    }

    private boolean g(String str) {
        ArrayList<String> arrayList = this.f361a.c;
        if (arrayList == null) {
            return false;
        }
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            String str2 = arrayList.get(i);
            if (str2 != null && Pattern.compile(str2).matcher(str).find()) {
                return true;
            }
        }
        return false;
    }

    private int h(String str) {
        if (this.f361a == null || this.f361a.e == null || this.f361a.e.size() <= 0) {
            return 0;
        }
        ArrayList<e> arrayList = this.f361a.e;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            e eVar = arrayList.get(i);
            if (eVar != null && !TextUtils.isEmpty(eVar.b) && Pattern.compile(eVar.b).matcher(str).find()) {
                return eVar.f364a;
            }
        }
        return 0;
    }

    private boolean i(String str) {
        if (this.f361a != null) {
            return a(str, this.f361a.b);
        }
        return false;
    }

    public void d() {
        InputStream inputStream = null;
        try {
            inputStream = AstApp.i().getAssets().open("CameraFilter.xml");
            this.f361a = a(inputStream);
            if (inputStream != null) {
                try {
                    inputStream.close();
                    return;
                } catch (IOException e) {
                    e = e;
                }
            } else {
                return;
            }
            e.printStackTrace();
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
            throw th;
        }
    }

    public void e() {
        new c(this).start();
    }

    private d a(InputStream inputStream) {
        XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
        newPullParser.setInput(inputStream, "utf-8");
        String str = null;
        int i = -1;
        ArrayList<String> arrayList = null;
        ArrayList<String> arrayList2 = null;
        ArrayList<e> arrayList3 = null;
        ArrayList<e> arrayList4 = null;
        ArrayList<String> arrayList5 = null;
        ArrayList<String> arrayList6 = null;
        d dVar = null;
        for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
            String name = newPullParser.getName();
            if (eventType == 2) {
                if ("A".equals(name)) {
                    dVar = new d();
                } else if ("V".equals(name)) {
                    str = "V";
                } else if ("B".equals(name)) {
                    arrayList6 = new ArrayList<>();
                    arrayList5 = new ArrayList<>();
                } else if ("C".equals(name)) {
                    str = "C";
                } else if ("D".equals(name)) {
                    str = "D";
                } else if ("E".equals(name)) {
                    arrayList4 = new ArrayList<>();
                    arrayList3 = new ArrayList<>();
                } else if ("F".equals(name)) {
                    str = "F";
                    try {
                        i = Integer.parseInt(newPullParser.getAttributeValue(0));
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                        i = -1;
                    }
                } else if ("G".equals(name)) {
                    str = "G";
                    try {
                        i = Integer.parseInt(newPullParser.getAttributeValue(0));
                    } catch (NumberFormatException e2) {
                        e2.printStackTrace();
                        i = -1;
                    }
                } else if ("O".equals(name)) {
                    arrayList = new ArrayList<>();
                } else if ("P".equals(name)) {
                    str = "P";
                } else if ("H".equals(name)) {
                    arrayList2 = new ArrayList<>();
                } else if ("S".equals(name)) {
                    str = "S";
                }
            } else if (eventType == 3) {
                if ("B".equals(name)) {
                    if (dVar != null) {
                        if (arrayList6 != null && arrayList6.size() > 0) {
                            dVar.b = arrayList6;
                        }
                        if (arrayList5 != null && arrayList5.size() > 0) {
                            dVar.c = arrayList5;
                        }
                    }
                } else if ("E".equals(name)) {
                    if (dVar != null) {
                        if (arrayList4 != null && arrayList4.size() > 0) {
                            dVar.d = arrayList4;
                        }
                        if (arrayList3 != null && arrayList3.size() > 0) {
                            dVar.e = arrayList3;
                        }
                    }
                } else if ("O".equals(name)) {
                    if (!(dVar == null || arrayList == null || arrayList.size() <= 0)) {
                        dVar.g = arrayList;
                    }
                } else if ("H".equals(name) && dVar != null && arrayList2 != null && arrayList2.size() > 0) {
                    dVar.f = arrayList2;
                }
            } else if (eventType == 4) {
                String text = newPullParser.getText();
                if ("V".equals(str)) {
                    if (dVar != null && !TextUtils.isEmpty(text)) {
                        dVar.f363a = Integer.parseInt(text);
                    }
                } else if ("C".equals(str)) {
                    if (arrayList6 != null && !TextUtils.isEmpty(text)) {
                        arrayList6.add(text);
                    }
                } else if ("D".equals(str)) {
                    if (arrayList5 != null && !TextUtils.isEmpty(text)) {
                        arrayList5.add(text);
                    }
                } else if ("F".equals(str)) {
                    if (arrayList4 != null && !TextUtils.isEmpty(text) && i > 0) {
                        arrayList4.add(new e(this, i, text));
                    }
                } else if ("G".equals(str)) {
                    if (arrayList3 != null && !TextUtils.isEmpty(text) && i > 0) {
                        arrayList3.add(new e(this, i, text));
                    }
                } else if ("P".equals(str)) {
                    if (arrayList != null && !TextUtils.isEmpty(text)) {
                        arrayList.add(text);
                    }
                } else if ("S".equals(str) && arrayList2 != null && !TextUtils.isEmpty(text)) {
                    arrayList2.add(text);
                }
                str = null;
            }
        }
        return dVar;
    }
}
