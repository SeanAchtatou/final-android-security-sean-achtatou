package com.qq.AppService;

import android.content.Context;
import java.net.Socket;

/* compiled from: ProGuard */
public final class u extends Thread {

    /* renamed from: a  reason: collision with root package name */
    public static volatile boolean f252a = true;
    public volatile boolean b = false;
    private Socket c = null;
    private Context d = null;

    public static void a() {
        f252a = false;
    }

    public u(Context context, Socket socket) {
        this.d = context;
        this.c = socket;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00ae, code lost:
        r0 = e;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x007b A[SYNTHETIC, Splitter:B:34:0x007b] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0080 A[Catch:{ Throwable -> 0x01f2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0087 A[Catch:{ Throwable -> 0x01f2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0096 A[SYNTHETIC, Splitter:B:44:0x0096] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00ae A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:10:0x0041] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x00db A[SYNTHETIC, Splitter:B:74:0x00db] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x00e0 A[SYNTHETIC, Splitter:B:77:0x00e0] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x00e7 A[Catch:{ Exception -> 0x00f0 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r7 = this;
            r4 = 10
            r3 = 4
            r2 = 0
            android.content.Context r0 = r7.d
            java.lang.String r1 = r7.toString()
            com.qq.provider.h.a(r0, r1)
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.setPriority(r4)
            r0 = 8
            byte[] r0 = new byte[r0]
            byte[] r4 = new byte[r3]
            boolean r1 = com.qq.AppService.u.f252a     // Catch:{ Throwable -> 0x01f2 }
            if (r1 == 0) goto L_0x00c8
            java.net.Socket r1 = r7.c     // Catch:{ Throwable -> 0x01f2 }
            if (r1 == 0) goto L_0x00c8
            java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x01e1 }
            java.net.Socket r1 = r7.c     // Catch:{ Exception -> 0x01e1 }
            java.io.InputStream r1 = r1.getInputStream()     // Catch:{ Exception -> 0x01e1 }
            r3.<init>(r1)     // Catch:{ Exception -> 0x01e1 }
            r3.read(r0)     // Catch:{ Exception -> 0x01ff, Throwable -> 0x01f7 }
            r1 = 4
            r5 = 0
            r6 = 4
            java.lang.System.arraycopy(r0, r1, r4, r5, r6)     // Catch:{ Exception -> 0x01ff, Throwable -> 0x01f7 }
            java.io.BufferedOutputStream r1 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x01ff, Throwable -> 0x01f7 }
            java.net.Socket r0 = r7.c     // Catch:{ Exception -> 0x01ff, Throwable -> 0x01f7 }
            java.io.OutputStream r0 = r0.getOutputStream()     // Catch:{ Exception -> 0x01ff, Throwable -> 0x01f7 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x01ff, Throwable -> 0x01f7 }
            byte[] r0 = com.qq.AppService.r.dy     // Catch:{ Exception -> 0x00ae }
            boolean r0 = com.qq.AppService.r.b(r4, r0)     // Catch:{ Exception -> 0x00ae }
            if (r0 == 0) goto L_0x00f5
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ Exception -> 0x00ae }
            r4 = 10
            r0.setPriority(r4)     // Catch:{ Exception -> 0x00ae }
            com.qq.provider.n r0 = com.qq.provider.n.a()     // Catch:{ Throwable -> 0x00a1 }
            r0.a(r3, r1)     // Catch:{ Throwable -> 0x00a1 }
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ Exception -> 0x00ae }
            r4 = 5
            r0.setPriority(r4)     // Catch:{ Exception -> 0x00ae }
        L_0x0061:
            if (r3 == 0) goto L_0x0066
            r3.close()     // Catch:{ Exception -> 0x01cb }
        L_0x0066:
            if (r1 == 0) goto L_0x006b
            r1.close()     // Catch:{ Exception -> 0x01d1 }
        L_0x006b:
            java.net.Socket r0 = r7.c     // Catch:{ Exception -> 0x01e1 }
            if (r0 == 0) goto L_0x0074
            java.net.Socket r0 = r7.c     // Catch:{ Exception -> 0x01db }
            r0.close()     // Catch:{ Exception -> 0x01db }
        L_0x0074:
            r0 = 0
            r7.c = r0     // Catch:{ Exception -> 0x01e1 }
            r0 = r2
            r1 = r2
        L_0x0079:
            if (r2 == 0) goto L_0x007e
            r1.close()     // Catch:{ Throwable -> 0x01f2 }
        L_0x007e:
            if (r2 == 0) goto L_0x0083
            r0.close()     // Catch:{ Throwable -> 0x01f2 }
        L_0x0083:
            java.net.Socket r0 = r7.c     // Catch:{ Throwable -> 0x01f2 }
            if (r0 == 0) goto L_0x008c
            java.net.Socket r0 = r7.c     // Catch:{ Throwable -> 0x01f2 }
            r0.close()     // Catch:{ Throwable -> 0x01f2 }
        L_0x008c:
            r0 = 0
            r7.c = r0     // Catch:{ Throwable -> 0x01f2 }
        L_0x008f:
            r0 = 1
            r7.b = r0
            java.net.Socket r0 = r7.c
            if (r0 == 0) goto L_0x009b
            java.net.Socket r0 = r7.c     // Catch:{ IOException -> 0x01e6 }
            r0.close()     // Catch:{ IOException -> 0x01e6 }
        L_0x009b:
            r7.c = r2
            com.qq.provider.h.a()
            return
        L_0x00a1:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00cb }
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ Exception -> 0x00ae }
            r4 = 5
            r0.setPriority(r4)     // Catch:{ Exception -> 0x00ae }
            goto L_0x0061
        L_0x00ae:
            r0 = move-exception
        L_0x00af:
            r0.printStackTrace()     // Catch:{ Throwable -> 0x00d5 }
            if (r3 == 0) goto L_0x00b7
            r3.close()     // Catch:{ Throwable -> 0x00d5 }
        L_0x00b7:
            if (r1 == 0) goto L_0x00bc
            r1.close()     // Catch:{ Throwable -> 0x01fb }
        L_0x00bc:
            java.net.Socket r0 = r7.c     // Catch:{ Throwable -> 0x01f2 }
            if (r0 == 0) goto L_0x00c5
            java.net.Socket r0 = r7.c     // Catch:{ Throwable -> 0x01f2 }
            r0.close()     // Catch:{ Throwable -> 0x01f2 }
        L_0x00c5:
            r0 = 0
            r7.c = r0     // Catch:{ Throwable -> 0x01f2 }
        L_0x00c8:
            r0 = r2
            r1 = r2
            goto L_0x0079
        L_0x00cb:
            r0 = move-exception
            java.lang.Thread r4 = java.lang.Thread.currentThread()     // Catch:{ Exception -> 0x00ae }
            r5 = 5
            r4.setPriority(r5)     // Catch:{ Exception -> 0x00ae }
            throw r0     // Catch:{ Exception -> 0x00ae }
        L_0x00d5:
            r0 = move-exception
        L_0x00d6:
            r0.printStackTrace()
            if (r3 == 0) goto L_0x00de
            r3.close()     // Catch:{ Exception -> 0x01ec }
        L_0x00de:
            if (r1 == 0) goto L_0x00e3
            r1.close()     // Catch:{ Exception -> 0x01ef }
        L_0x00e3:
            java.net.Socket r0 = r7.c     // Catch:{ Exception -> 0x00f0 }
            if (r0 == 0) goto L_0x00ec
            java.net.Socket r0 = r7.c     // Catch:{ Exception -> 0x00f0 }
            r0.close()     // Catch:{ Exception -> 0x00f0 }
        L_0x00ec:
            r0 = 0
            r7.c = r0     // Catch:{ Exception -> 0x00f0 }
            goto L_0x008f
        L_0x00f0:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x008f
        L_0x00f5:
            byte[] r0 = com.qq.AppService.r.dz     // Catch:{ Exception -> 0x00ae }
            boolean r0 = com.qq.AppService.r.b(r4, r0)     // Catch:{ Exception -> 0x00ae }
            if (r0 == 0) goto L_0x012f
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ Exception -> 0x00ae }
            r4 = 10
            r0.setPriority(r4)     // Catch:{ Exception -> 0x00ae }
            com.qq.provider.n r0 = com.qq.provider.n.a()     // Catch:{ Throwable -> 0x0117 }
            r0.b(r3, r1)     // Catch:{ Throwable -> 0x0117 }
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ Exception -> 0x00ae }
            r4 = 5
            r0.setPriority(r4)     // Catch:{ Exception -> 0x00ae }
            goto L_0x0061
        L_0x0117:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0125 }
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ Exception -> 0x00ae }
            r4 = 5
            r0.setPriority(r4)     // Catch:{ Exception -> 0x00ae }
            goto L_0x0061
        L_0x0125:
            r0 = move-exception
            java.lang.Thread r4 = java.lang.Thread.currentThread()     // Catch:{ Exception -> 0x00ae }
            r5 = 5
            r4.setPriority(r5)     // Catch:{ Exception -> 0x00ae }
            throw r0     // Catch:{ Exception -> 0x00ae }
        L_0x012f:
            byte[] r0 = com.qq.AppService.r.eb     // Catch:{ Exception -> 0x00ae }
            boolean r0 = com.qq.AppService.r.b(r4, r0)     // Catch:{ Exception -> 0x00ae }
            if (r0 == 0) goto L_0x0146
            com.qq.provider.n r0 = com.qq.provider.n.a()     // Catch:{ Throwable -> 0x0140, Exception -> 0x00ae }
            r0.c(r3, r1)     // Catch:{ Throwable -> 0x0140, Exception -> 0x00ae }
            goto L_0x0061
        L_0x0140:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x00ae }
            goto L_0x0061
        L_0x0146:
            byte[] r0 = com.qq.AppService.r.dK     // Catch:{ Exception -> 0x00ae }
            boolean r0 = com.qq.AppService.r.b(r4, r0)     // Catch:{ Exception -> 0x00ae }
            if (r0 == 0) goto L_0x0158
            com.qq.provider.m r0 = new com.qq.provider.m     // Catch:{ Exception -> 0x00ae }
            r0.<init>(r3, r1)     // Catch:{ Exception -> 0x00ae }
            r0.e()     // Catch:{ Exception -> 0x00ae }
            goto L_0x0061
        L_0x0158:
            byte[] r0 = com.qq.AppService.r.bu     // Catch:{ Exception -> 0x00ae }
            boolean r0 = com.qq.AppService.r.b(r4, r0)     // Catch:{ Exception -> 0x00ae }
            if (r0 == 0) goto L_0x0172
            java.lang.String r0 = "kevin"
            java.lang.String r4 = "MXCA_GETSCREENSHOT2"
            com.tencent.assistant.utils.XLog.d(r0, r4)     // Catch:{ Exception -> 0x00ae }
            com.qq.provider.ab r0 = com.qq.provider.ab.a()     // Catch:{ Exception -> 0x00ae }
            android.content.Context r4 = r7.d     // Catch:{ Exception -> 0x00ae }
            r0.a(r4, r1)     // Catch:{ Exception -> 0x00ae }
            goto L_0x0061
        L_0x0172:
            byte[] r0 = com.qq.AppService.r.f250a     // Catch:{ Exception -> 0x00ae }
            boolean r0 = com.qq.AppService.r.b(r4, r0)     // Catch:{ Exception -> 0x00ae }
            if (r0 != 0) goto L_0x0061
            byte[] r0 = com.qq.AppService.r.gK     // Catch:{ Exception -> 0x00ae }
            boolean r0 = com.qq.AppService.r.b(r4, r0)     // Catch:{ Exception -> 0x00ae }
            if (r0 != 0) goto L_0x0061
            byte[] r0 = com.qq.AppService.r.gk     // Catch:{ Exception -> 0x00ae }
            boolean r0 = com.qq.AppService.r.b(r4, r0)     // Catch:{ Exception -> 0x00ae }
            if (r0 != 0) goto L_0x0061
            byte[] r0 = com.qq.AppService.r.hl     // Catch:{ Exception -> 0x00ae }
            boolean r0 = com.qq.AppService.r.b(r4, r0)     // Catch:{ Exception -> 0x00ae }
            if (r0 == 0) goto L_0x01b2
            java.net.Socket r0 = r7.c     // Catch:{ Exception -> 0x00ae }
            r4 = 50
            r0.setSoTimeout(r4)     // Catch:{ Exception -> 0x00ae }
            android.content.Context r0 = r7.d     // Catch:{ Exception -> 0x00ae }
            com.qq.provider.ak.a(r0)     // Catch:{ Exception -> 0x00ae }
            com.qq.provider.ak r0 = new com.qq.provider.ak     // Catch:{ Exception -> 0x00ae }
            r0.<init>()     // Catch:{ Exception -> 0x00ae }
            android.content.Context r4 = r7.d     // Catch:{ Exception -> 0x00ae }
            r0.a(r4, r3, r1)     // Catch:{ Exception -> 0x00ae }
            java.lang.Thread r4 = new java.lang.Thread     // Catch:{ Exception -> 0x00ae }
            r4.<init>(r0)     // Catch:{ Exception -> 0x00ae }
            r4.run()     // Catch:{ Exception -> 0x00ae }
            goto L_0x0061
        L_0x01b2:
            byte[] r0 = com.qq.AppService.r.bw     // Catch:{ Exception -> 0x00ae }
            boolean r0 = com.qq.AppService.r.b(r4, r0)     // Catch:{ Exception -> 0x00ae }
            if (r0 == 0) goto L_0x0061
            java.net.Socket r0 = r7.c     // Catch:{ Exception -> 0x00ae }
            int r0 = com.qq.util.q.a(r0)     // Catch:{ Exception -> 0x00ae }
            com.qq.provider.ab r4 = com.qq.provider.ab.a()     // Catch:{ Exception -> 0x00ae }
            android.content.Context r5 = r7.d     // Catch:{ Exception -> 0x00ae }
            r4.a(r5, r0)     // Catch:{ Exception -> 0x00ae }
            goto L_0x0061
        L_0x01cb:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x00ae }
            goto L_0x0066
        L_0x01d1:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x01d7 }
            goto L_0x006b
        L_0x01d7:
            r0 = move-exception
            r3 = r2
            goto L_0x00af
        L_0x01db:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x01e1 }
            goto L_0x0074
        L_0x01e1:
            r0 = move-exception
            r1 = r2
            r3 = r2
            goto L_0x00af
        L_0x01e6:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x009b
        L_0x01ec:
            r0 = move-exception
            goto L_0x00de
        L_0x01ef:
            r0 = move-exception
            goto L_0x00e3
        L_0x01f2:
            r0 = move-exception
            r1 = r2
            r3 = r2
            goto L_0x00d6
        L_0x01f7:
            r0 = move-exception
            r1 = r2
            goto L_0x00d6
        L_0x01fb:
            r0 = move-exception
            r3 = r2
            goto L_0x00d6
        L_0x01ff:
            r0 = move-exception
            r1 = r2
            goto L_0x00af
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.AppService.u.run():void");
    }
}
