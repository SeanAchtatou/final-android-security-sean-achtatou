package com.qq.AppService;

import android.content.Context;
import java.net.Socket;

/* compiled from: ProGuard */
public final class ao {

    /* renamed from: a  reason: collision with root package name */
    private Context f222a = null;
    private af b = null;
    private af c = null;
    private af d = null;
    private af e = null;

    public ao(Context context) {
        this.f222a = context;
    }

    public void a() {
        if (this.b != null) {
            this.b.b();
            this.b = null;
        }
        if (this.c != null) {
            this.c.b();
            this.c = null;
        }
        if (this.d != null) {
            this.d.b();
            this.d = null;
        }
        if (this.e != null) {
            this.e.b();
            this.e = null;
        }
    }

    public void a(Socket socket) {
        if (this.b == null) {
            this.b = new af(this.f222a, socket);
            this.b.start();
        } else if (this.b.f215a) {
            this.b.a(socket);
        } else if (this.c == null) {
            this.c = new af(this.f222a, socket);
            this.c.start();
        } else if (this.c.f215a) {
            this.c.a(socket);
        } else if (this.d == null) {
            this.d = new af(this.f222a, socket);
            this.d.start();
        } else if (this.d.f215a) {
            this.d.a(socket);
        } else if (this.e == null) {
            this.e = new af(this.f222a, socket);
            this.e.start();
        } else if (this.e.f215a) {
            this.e.a(socket);
        } else {
            af afVar = new af(this.f222a, socket);
            afVar.a();
            afVar.start();
        }
    }
}
