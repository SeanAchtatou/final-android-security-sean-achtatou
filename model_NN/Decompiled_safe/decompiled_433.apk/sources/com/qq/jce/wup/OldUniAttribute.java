package com.qq.jce.wup;

import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;
import com.tencent.connect.common.Constants;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

class OldUniAttribute {
    protected HashMap<String, HashMap<String, byte[]>> _data = new HashMap<>();
    JceInputStream _is = new JceInputStream();
    protected HashMap<String, Object> cachedClassName = new HashMap<>();
    private HashMap<String, Object> cachedData = new HashMap<>();
    protected String encodeName = "GBK";

    OldUniAttribute() {
    }

    public String getEncodeName() {
        return this.encodeName;
    }

    public void setEncodeName(String encodeName2) {
        this.encodeName = encodeName2;
    }

    public void clearCacheData() {
        this.cachedData.clear();
    }

    public Set<String> getKeySet() {
        return Collections.unmodifiableSet(this._data.keySet());
    }

    public boolean isEmpty() {
        return this._data.isEmpty();
    }

    public int size() {
        return this._data.size();
    }

    public boolean containsKey(String key) {
        return this._data.containsKey(key);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
     arg types: [T, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void */
    public <T> void put(String name, T t) {
        if (name == null) {
            throw new IllegalArgumentException("put key can not is null");
        } else if (t == null) {
            throw new IllegalArgumentException("put value can not is null");
        } else if (t instanceof Set) {
            throw new IllegalArgumentException("can not support Set");
        } else {
            JceOutputStream _out = new JceOutputStream();
            _out.setServerEncoding(this.encodeName);
            _out.write((Object) t, 0);
            byte[] _sBuffer = JceUtil.getJceBufArray(_out.getByteBuffer());
            HashMap<String, byte[]> pair = new HashMap<>(1);
            ArrayList<String> listType = new ArrayList<>(1);
            checkObjectType(listType, t);
            pair.put(BasicClassTypeUtil.transTypeList(listType), _sBuffer);
            this.cachedData.remove(name);
            this._data.put(name, pair);
        }
    }

    public <T> T getJceStruct(String name, boolean initialize, ClassLoader loader) throws ObjectCreateException {
        if (!this._data.containsKey(name)) {
            return null;
        }
        if (this.cachedData.containsKey(name)) {
            return this.cachedData.get(name);
        }
        String className = null;
        byte[] data = new byte[0];
        Iterator i$ = this._data.get(name).entrySet().iterator();
        if (i$.hasNext()) {
            Map.Entry<String, byte[]> e = (Map.Entry) i$.next();
            className = e.getKey();
            data = e.getValue();
        }
        try {
            T proxy = getCacheProxy(className, initialize, loader);
            this._is.wrap(data);
            this._is.setServerEncoding(this.encodeName);
            JceStruct o = this._is.directRead((JceStruct) proxy, 0, true);
            saveDataCache(name, o);
            return o;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ObjectCreateException(ex);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
     arg types: [T, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object */
    public <T> T get(String name, boolean initialize, ClassLoader loader) throws ObjectCreateException {
        if (!this._data.containsKey(name)) {
            return null;
        }
        if (this.cachedData.containsKey(name)) {
            return this.cachedData.get(name);
        }
        String className = null;
        byte[] data = new byte[0];
        Iterator i$ = this._data.get(name).entrySet().iterator();
        if (i$.hasNext()) {
            Map.Entry<String, byte[]> e = (Map.Entry) i$.next();
            className = e.getKey();
            data = e.getValue();
        }
        try {
            T proxy = getCacheProxy(className, initialize, loader);
            this._is.wrap(data);
            this._is.setServerEncoding(this.encodeName);
            Object o = this._is.read((Object) proxy, 0, true);
            saveDataCache(name, o);
            return o;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ObjectCreateException(ex);
        }
    }

    private Object getCacheProxy(String className, boolean initialize, ClassLoader loader) {
        if (this.cachedClassName.containsKey(className)) {
            return this.cachedClassName.get(className);
        }
        Object proxy = BasicClassTypeUtil.createClassByUni(className, initialize, loader);
        this.cachedClassName.put(className, proxy);
        return proxy;
    }

    private void saveDataCache(String name, Object o) {
        this.cachedData.put(name, o);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
     arg types: [T, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object */
    public <T> T get(String name, Object defaultValue, boolean initialize, ClassLoader loader) {
        if (!this._data.containsKey(name)) {
            return defaultValue;
        }
        if (this.cachedData.containsKey(name)) {
            return this.cachedData.get(name);
        }
        String className = Constants.STR_EMPTY;
        byte[] data = new byte[0];
        Iterator i$ = this._data.get(name).entrySet().iterator();
        if (i$.hasNext()) {
            Map.Entry<String, byte[]> e = (Map.Entry) i$.next();
            className = e.getKey();
            data = e.getValue();
        }
        try {
            T proxy = getCacheProxy(className, initialize, loader);
            this._is.wrap(data);
            this._is.setServerEncoding(this.encodeName);
            Object o = this._is.read((Object) proxy, 0, true);
            saveDataCache(name, o);
            return o;
        } catch (Exception ex) {
            ex.printStackTrace();
            saveDataCache(name, defaultValue);
            return defaultValue;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
     arg types: [T, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object */
    public <T> T remove(String name, boolean initialize, ClassLoader loader) throws ObjectCreateException {
        if (!this._data.containsKey(name)) {
            return null;
        }
        String className = Constants.STR_EMPTY;
        byte[] data = new byte[0];
        Iterator i$ = this._data.remove(name).entrySet().iterator();
        if (i$.hasNext()) {
            Map.Entry<String, byte[]> e = (Map.Entry) i$.next();
            className = e.getKey();
            data = e.getValue();
        }
        try {
            T proxy = BasicClassTypeUtil.createClassByUni(className, initialize, loader);
            this._is.wrap(data);
            this._is.setServerEncoding(this.encodeName);
            return this._is.read((Object) proxy, 0, true);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ObjectCreateException(ex);
        }
    }

    private void checkObjectType(ArrayList<String> listTpye, Object o) {
        if (o.getClass().isArray()) {
            if (!o.getClass().getComponentType().toString().equals("byte")) {
                throw new IllegalArgumentException("only byte[] is supported");
            } else if (Array.getLength(o) > 0) {
                listTpye.add("java.util.List");
                checkObjectType(listTpye, Array.get(o, 0));
            } else {
                listTpye.add("Array");
                listTpye.add("?");
            }
        } else if (o instanceof Array) {
            throw new IllegalArgumentException("can not support Array, please use List");
        } else if (o instanceof List) {
            listTpye.add("java.util.List");
            List list = (List) o;
            if (list.size() > 0) {
                checkObjectType(listTpye, list.get(0));
            } else {
                listTpye.add("?");
            }
        } else if (o instanceof Map) {
            listTpye.add("java.util.Map");
            Map map = (Map) o;
            if (map.size() > 0) {
                Object key = map.keySet().iterator().next();
                Object value = map.get(key);
                listTpye.add(key.getClass().getName());
                checkObjectType(listTpye, value);
                return;
            }
            listTpye.add("?");
            listTpye.add("?");
        } else {
            listTpye.add(o.getClass().getName());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
     arg types: [java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, byte[]>>, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void */
    public byte[] encode() {
        JceOutputStream _os = new JceOutputStream(0);
        _os.setServerEncoding(this.encodeName);
        _os.write((Map) this._data, 0);
        return JceUtil.getJceBufArray(_os.getByteBuffer());
    }

    public void decode(byte[] buffer) {
        this._is.wrap(buffer);
        this._is.setServerEncoding(this.encodeName);
        HashMap<String, HashMap<String, byte[]>> _tempdata = new HashMap<>(1);
        HashMap<String, byte[]> h = new HashMap<>(1);
        h.put(Constants.STR_EMPTY, new byte[0]);
        _tempdata.put(Constants.STR_EMPTY, h);
        this._data = this._is.readMap(_tempdata, 0, false);
    }
}
