package android.support.v4.app;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabWidget;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class FragmentTabHost extends TabHost implements TabHost.OnTabChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private final ArrayList<v> f44a = new ArrayList<>();
    private FrameLayout b;
    private Context c;
    private l d;
    private int e;
    private TabHost.OnTabChangeListener f;
    private v g;
    private boolean h;

    /* compiled from: ProGuard */
    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new u();

        /* renamed from: a  reason: collision with root package name */
        String f45a;

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.f45a = parcel.readString();
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeString(this.f45a);
        }

        public String toString() {
            return "FragmentTabHost.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " curTab=" + this.f45a + "}";
        }
    }

    public FragmentTabHost(Context context) {
        super(context, null);
        a(context, (AttributeSet) null);
    }

    public FragmentTabHost(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    private void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, new int[]{16842995}, 0, 0);
        this.e = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.recycle();
        super.setOnTabChangedListener(this);
        if (findViewById(16908307) == null) {
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setOrientation(1);
            addView(linearLayout, new FrameLayout.LayoutParams(-1, -1));
            TabWidget tabWidget = new TabWidget(context);
            tabWidget.setId(16908307);
            tabWidget.setOrientation(0);
            linearLayout.addView(tabWidget, new LinearLayout.LayoutParams(-1, -2, 0.0f));
            FrameLayout frameLayout = new FrameLayout(context);
            frameLayout.setId(16908305);
            linearLayout.addView(frameLayout, new LinearLayout.LayoutParams(0, 0, 0.0f));
            FrameLayout frameLayout2 = new FrameLayout(context);
            this.b = frameLayout2;
            this.b.setId(this.e);
            linearLayout.addView(frameLayout2, new LinearLayout.LayoutParams(-1, 0, 1.0f));
        }
    }

    @Deprecated
    public void setup() {
        throw new IllegalStateException("Must call setup() that takes a Context and FragmentManager");
    }

    public void setOnTabChangedListener(TabHost.OnTabChangeListener onTabChangeListener) {
        this.f = onTabChangeListener;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        String currentTabTag = getCurrentTabTag();
        w wVar = null;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f44a.size()) {
                break;
            }
            v vVar = this.f44a.get(i2);
            Fragment unused = vVar.d = this.d.a(vVar.f65a);
            if (vVar.d != null && !vVar.d.f()) {
                if (vVar.f65a.equals(currentTabTag)) {
                    this.g = vVar;
                } else {
                    if (wVar == null) {
                        wVar = this.d.a();
                    }
                    wVar.a(vVar.d);
                }
            }
            i = i2 + 1;
        }
        this.h = true;
        w a2 = a(currentTabTag, wVar);
        if (a2 != null) {
            a2.a();
            this.d.b();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.h = false;
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f45a = getCurrentTabTag();
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCurrentTabByTag(savedState.f45a);
    }

    public void onTabChanged(String str) {
        w a2;
        if (this.h && (a2 = a(str, (w) null)) != null) {
            a2.a();
        }
        if (this.f != null) {
            this.f.onTabChanged(str);
        }
    }

    private w a(String str, w wVar) {
        v vVar = null;
        int i = 0;
        while (i < this.f44a.size()) {
            v vVar2 = this.f44a.get(i);
            if (!vVar2.f65a.equals(str)) {
                vVar2 = vVar;
            }
            i++;
            vVar = vVar2;
        }
        if (vVar == null) {
            throw new IllegalStateException("No tab known for tag " + str);
        }
        if (this.g != vVar) {
            if (wVar == null) {
                wVar = this.d.a();
            }
            if (!(this.g == null || this.g.d == null)) {
                wVar.a(this.g.d);
            }
            if (vVar != null) {
                if (vVar.d == null) {
                    Fragment unused = vVar.d = Fragment.a(this.c, vVar.b.getName(), vVar.c);
                    wVar.a(this.e, vVar.d, vVar.f65a);
                } else {
                    wVar.b(vVar.d);
                }
            }
            this.g = vVar;
        }
        return wVar;
    }
}
