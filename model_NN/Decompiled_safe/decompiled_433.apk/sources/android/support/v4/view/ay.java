package android.support.v4.view;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.view.a.a;
import android.support.v4.view.a.g;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

/* compiled from: ProGuard */
class ay implements be {
    ay() {
    }

    public boolean a(View view, int i) {
        return false;
    }

    public boolean b(View view, int i) {
        return false;
    }

    public int a(View view) {
        return 2;
    }

    public void c(View view, int i) {
    }

    public void a(View view, AccessibilityDelegateCompat accessibilityDelegateCompat) {
    }

    public void a(View view, AccessibilityEvent accessibilityEvent) {
    }

    public void b(View view, AccessibilityEvent accessibilityEvent) {
    }

    public void a(View view, a aVar) {
    }

    public boolean b(View view) {
        return false;
    }

    public void a(View view, boolean z) {
    }

    public void c(View view) {
        view.postInvalidateDelayed(a());
    }

    public void a(View view, int i, int i2, int i3, int i4) {
        view.postInvalidateDelayed(a(), i, i2, i3, i4);
    }

    public void a(View view, Runnable runnable) {
        view.postDelayed(runnable, a());
    }

    public void a(View view, Runnable runnable, long j) {
        view.postDelayed(runnable, a() + j);
    }

    /* access modifiers changed from: package-private */
    public long a() {
        return 10;
    }

    public int d(View view) {
        return 0;
    }

    public void d(View view, int i) {
    }

    public boolean a(View view, int i, Bundle bundle) {
        return false;
    }

    public g e(View view) {
        return null;
    }

    public void a(View view, int i, Paint paint) {
    }

    public int f(View view) {
        return 0;
    }

    public int g(View view) {
        return 0;
    }

    public void e(View view, int i) {
    }
}
