package android.support.v4.view;

import android.os.Bundle;
import android.support.v4.view.a.a;
import android.support.v4.view.a.o;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

/* compiled from: ProGuard */
class n extends AccessibilityDelegateCompat {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DirectionalViewPager f95a;

    n(DirectionalViewPager directionalViewPager) {
        this.f95a = directionalViewPager;
    }

    public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(view, accessibilityEvent);
        accessibilityEvent.setClassName(ViewPager.class.getName());
        o a2 = o.a();
        a2.a(a());
        if (accessibilityEvent.getEventType() == 4096 && this.f95a.mAdapter != null) {
            a2.a(this.f95a.mAdapter.getCount());
            a2.b(this.f95a.mCurItem);
            a2.c(this.f95a.mCurItem);
        }
    }

    public void onInitializeAccessibilityNodeInfo(View view, a aVar) {
        super.onInitializeAccessibilityNodeInfo(view, aVar);
        aVar.a(ViewPager.class.getName());
        aVar.a(a());
        if (this.f95a.canScrollHorizontally(1)) {
            aVar.a(4096);
        }
        if (this.f95a.canScrollHorizontally(-1)) {
            aVar.a(8192);
        }
    }

    public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
        if (super.performAccessibilityAction(view, i, bundle)) {
            return true;
        }
        switch (i) {
            case 4096:
                if (!this.f95a.canScrollHorizontally(1)) {
                    return false;
                }
                this.f95a.setCurrentItem(this.f95a.mCurItem + 1);
                return true;
            case 8192:
                if (!this.f95a.canScrollHorizontally(-1)) {
                    return false;
                }
                this.f95a.setCurrentItem(this.f95a.mCurItem - 1);
                return true;
            default:
                return false;
        }
    }

    private boolean a() {
        return this.f95a.mAdapter != null && this.f95a.mAdapter.getCount() > 1;
    }
}
