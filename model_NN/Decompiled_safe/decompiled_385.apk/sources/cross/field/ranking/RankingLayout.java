package cross.field.ranking;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.TextView;
import cross.field.MeteorBreaker.R;
import java.util.List;

public class RankingLayout {
    public static final int ADLANTIS_HEIGHT = 100;
    public static final int ADLANTIS_WIDTH = 480;
    public static final int ADLANTIS_X = 0;
    public static final int ADLANTIS_Y = 70;
    public static final int BACK_HEIGHT = 85;
    public static final int BACK_WIDTH = 240;
    public static final int BACK_X = 0;
    public static final int BACK_Y = 769;
    public static final int LIST_COUNTRY_WIDTH = 48;
    public static final int LIST_DATE_WIDTH = 288;
    public static final int LIST_FLAG_WIDTH = 24;
    public static final int LIST_NAME_WIDTH = 177;
    public static final int LIST_NUMBER_WIDTH = 81;
    public static final int LIST_TIME_WIDTH = 144;
    public static final int MORE_HEIGHT = 85;
    public static final int MORE_WIDTH = 240;
    public static final int MORE_X = 240;
    public static final int MORE_Y = 769;
    public static final int REDIO_INTERVAL_HEIGHT = 70;
    public static final int REDIO_INTERVAL_WIDTH = 480;
    public static final int REDIO_INTERVAL_X = 0;
    public static final int REDIO_INTERVAL_Y = 0;
    public static final int TAB_RANKING_HEIGHT = 599;
    public static final int TAB_RANKING_WIDTH = 480;
    public static final int TAB_RANKING_X = 0;
    public static final int TAB_RANKING_Y = 170;
    /* access modifiers changed from: private */
    public RankingActivity activity;
    private int adlantis_height = 100;
    private int adlantis_width = 480;
    private int adlantis_x = 0;
    private int adlantis_y = 70;
    private int back_height = 85;
    private int back_width = 240;
    private int back_x = 0;
    /* access modifiers changed from: private */
    public int list_country_width = 48;
    /* access modifiers changed from: private */
    public int list_date_width = LIST_DATE_WIDTH;
    /* access modifiers changed from: private */
    public int list_name_width = LIST_NAME_WIDTH;
    /* access modifiers changed from: private */
    public int list_number_width = 81;
    /* access modifiers changed from: private */
    public int list_time_width = LIST_TIME_WIDTH;
    private int more_height = 85;
    private int more_width = 240;
    private int more_y = 769;
    /* access modifiers changed from: private */
    public Resources r;
    private int radioGroup_interval_height = 70;
    private int radioGroup_interval_width = 480;
    private int radioGroup_interval_x = 0;
    private int radioGroup_interval_y = 0;
    private int tabGroup_ranking_height = TAB_RANKING_HEIGHT;
    private int tabGroup_ranking_width = 480;
    private int tabGroup_ranking_x = 0;
    private int tabGroup_ranking_y = 170;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.widget.FrameLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public RankingLayout(Context context) {
        this.activity = (RankingActivity) context;
        this.r = this.activity.getResources();
        this.activity.getDisplaySize();
        this.activity.getDisplayRatio();
        this.radioGroup_interval_x = (int) (0.0f * this.activity.getRatioWidth());
        this.radioGroup_interval_y = (int) (0.0f * this.activity.getRatioHeight());
        this.radioGroup_interval_width = (int) (480.0f * this.activity.getRatioWidth());
        this.radioGroup_interval_height = (int) (70.0f * this.activity.getRatioHeight());
        new RadioGroup(this.activity);
        RadioGroup radioGroup_interval = (RadioGroup) this.activity.findViewById(R.id.radioGroup_interval);
        radioGroup_interval.setLayoutParams(new AbsoluteLayout.LayoutParams(this.radioGroup_interval_width, this.radioGroup_interval_height, this.radioGroup_interval_x, this.radioGroup_interval_y));
        radioGroup_interval.check(R.id.radioButton_daily);
        new RadioButton(this.activity);
        ((RadioButton) this.activity.findViewById(R.id.radioButton_daily)).setLayoutParams(new RadioGroup.LayoutParams(this.radioGroup_interval_width / 4, this.radioGroup_interval_height));
        RadioButton radioButton = (RadioButton) this.activity.findViewById(R.id.radioButton_weekly);
        ((RadioButton) this.activity.findViewById(R.id.radioButton_weekly)).setLayoutParams(new RadioGroup.LayoutParams(this.radioGroup_interval_width / 4, this.radioGroup_interval_height));
        RadioButton radioButton2 = (RadioButton) this.activity.findViewById(R.id.radioButton_monthly);
        ((RadioButton) this.activity.findViewById(R.id.radioButton_monthly)).setLayoutParams(new RadioGroup.LayoutParams(this.radioGroup_interval_width / 4, this.radioGroup_interval_height));
        RadioButton radioButton3 = (RadioButton) this.activity.findViewById(R.id.radioButton_monthly);
        ((RadioButton) this.activity.findViewById(R.id.radioButton_monthly)).setLayoutParams(new RadioGroup.LayoutParams(this.radioGroup_interval_width / 4, this.radioGroup_interval_height));
        this.back_x = (int) (0.0f * this.activity.getRatioWidth());
        this.back_width = (int) (240.0f * this.activity.getRatioWidth());
        this.back_height = (int) (85.0f * this.activity.getRatioHeight());
        new Button(this.activity);
        ((Button) this.activity.findViewById(R.id.button_back)).setLayoutParams(new LinearLayout.LayoutParams(this.back_width, this.back_height));
        this.more_y = (int) (769.0f * this.activity.getRatioHeight());
        this.more_width = (int) (240.0f * this.activity.getRatioWidth());
        this.more_height = (int) (85.0f * this.activity.getRatioHeight());
        new Button(this.activity);
        ((Button) this.activity.findViewById(R.id.button_more)).setLayoutParams(new LinearLayout.LayoutParams(this.more_width, this.more_height));
        new LinearLayout(this.activity);
        ((LinearLayout) this.activity.findViewById(R.id.linearLayout_back)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.more_width + this.back_width, this.more_height, this.back_x, this.more_y));
        this.tabGroup_ranking_x = (int) (0.0f * this.activity.getRatioWidth());
        this.tabGroup_ranking_y = (int) (170.0f * this.activity.getRatioHeight());
        this.tabGroup_ranking_width = (int) (480.0f * this.activity.getRatioWidth());
        this.tabGroup_ranking_height = (int) (599.0f * this.activity.getRatioHeight());
        new LinearLayout(this.activity);
        ((LinearLayout) this.activity.findViewById(R.id.tabGroup_ranking)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.tabGroup_ranking_width, this.tabGroup_ranking_height, this.tabGroup_ranking_x, this.tabGroup_ranking_y));
        TabHost tabs = this.activity.getTabHost();
        tabs.setOnTabChangedListener(this.activity);
        LayoutInflater.from(this.activity).inflate((int) R.layout.ranking_tab, (ViewGroup) tabs.getTabContentView(), true);
        TabHost.TabSpec tab_private = tabs.newTabSpec("Private");
        tab_private.setIndicator("Private");
        tab_private.setContent(R.id.tab_private);
        tabs.addTab(tab_private);
        TabHost.TabSpec tab_domestic = tabs.newTabSpec("Domestic");
        tab_domestic.setIndicator("Domestic");
        tab_domestic.setContent(R.id.tab_domestic);
        tabs.addTab(tab_domestic);
        TabHost.TabSpec tab_world = tabs.newTabSpec("World");
        tab_world.setIndicator("World");
        tab_world.setContent(R.id.tab_world);
        tabs.addTab(tab_world);
        tabs.setCurrentTab(0);
        this.adlantis_x = (int) (0.0f * this.activity.getRatioWidth());
        this.adlantis_y = (int) (70.0f * this.activity.getRatioHeight());
        this.adlantis_width = (int) (480.0f * this.activity.getRatioWidth());
        this.adlantis_height = (int) (100.0f * this.activity.getRatioHeight());
        new LinearLayout(this.activity);
        ((LinearLayout) this.activity.findViewById(R.id.linearLayout_Adlantis)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.adlantis_width, this.adlantis_height, this.adlantis_x, this.adlantis_y));
    }

    public class ListAdapter extends ArrayAdapter<RankingBean> {
        private TextView country;
        private TextView date;
        private ImageView flag;
        private LayoutInflater mInflater;
        private TextView name;
        private TextView number;
        private TextView time;

        public ListAdapter(Context context, List<RankingBean> objects) {
            super(context, 0, objects);
            this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            RankingLayout.this.list_number_width = (int) (81.0f * RankingLayout.this.activity.getRatioWidth());
            RankingLayout.this.list_time_width = (int) (144.0f * RankingLayout.this.activity.getRatioWidth());
            RankingLayout.this.list_name_width = (int) (177.0f * RankingLayout.this.activity.getRatioWidth());
            RankingLayout.this.list_date_width = (int) (288.0f * RankingLayout.this.activity.getRatioWidth());
            RankingLayout.this.list_country_width = (int) (48.0f * RankingLayout.this.activity.getRatioWidth());
            if (convertView == null) {
                convertView = this.mInflater.inflate((int) R.layout.list, (ViewGroup) null);
            }
            RankingBean ranking = (RankingBean) getItem(position);
            if (ranking != null) {
                this.number = (TextView) convertView.findViewById(R.id.textView_number);
                this.number.setText(ranking.getNumber());
                this.number.setLayoutParams(new LinearLayout.LayoutParams(RankingLayout.this.list_number_width, -2));
                this.time = (TextView) convertView.findViewById(R.id.textView_time);
                this.time.setText(ranking.getTime());
                this.time.setLayoutParams(new LinearLayout.LayoutParams(RankingLayout.this.list_time_width, -2));
                this.name = (TextView) convertView.findViewById(R.id.textView_name);
                this.name.setText(ranking.getName());
                this.name.setLayoutParams(new LinearLayout.LayoutParams(RankingLayout.this.list_name_width, -2));
                this.date = (TextView) convertView.findViewById(R.id.textView_date);
                this.date.setText(ranking.getDate());
                this.date.setLayoutParams(new LinearLayout.LayoutParams(RankingLayout.this.list_date_width, -2));
                this.country = (TextView) convertView.findViewById(R.id.textView_country);
                this.country.setText(ranking.getCountry());
                this.country.setLayoutParams(new LinearLayout.LayoutParams(RankingLayout.this.list_country_width, -2));
                this.flag = (ImageView) convertView.findViewById(R.id.flagView);
                int id = flagName(ranking.getCountry());
                if (id != -1) {
                    this.flag.setImageDrawable(RankingLayout.this.r.getDrawable(id));
                }
            }
            return convertView;
        }

        /* JADX INFO: additional move instructions added (1) to help type inference */
        public int flagName(String flag2) {
            int id = -1;
            if (flag2.equalsIgnoreCase("IS")) {
                id = R.drawable.is;
            }
            int id2 = id;
            if (flag2.equalsIgnoreCase("IE")) {
                id2 = R.drawable.ie;
            }
            int id3 = id2;
            if (flag2.equalsIgnoreCase("AZ")) {
                id3 = R.drawable.az;
            }
            int id4 = id3;
            if (flag2.equalsIgnoreCase("AF")) {
                id4 = R.drawable.af;
            }
            int id5 = id4;
            if (flag2.equalsIgnoreCase("US")) {
                id5 = R.drawable.us;
            }
            int id6 = id5;
            if (flag2.equalsIgnoreCase("VI")) {
                id6 = R.drawable.vi;
            }
            int id7 = id6;
            if (flag2.equalsIgnoreCase("AS")) {
                id7 = R.drawable.as;
            }
            int id8 = id7;
            if (flag2.equalsIgnoreCase("AE")) {
                id8 = R.drawable.ae;
            }
            int id9 = id8;
            if (flag2.equalsIgnoreCase("DZ")) {
                id9 = R.drawable.dz;
            }
            int id10 = id9;
            if (flag2.equalsIgnoreCase("AR")) {
                id10 = R.drawable.ar;
            }
            int id11 = id10;
            if (flag2.equalsIgnoreCase("AW")) {
                id11 = R.drawable.aw;
            }
            int id12 = id11;
            if (flag2.equalsIgnoreCase("AL")) {
                id12 = R.drawable.al;
            }
            int id13 = id12;
            if (flag2.equalsIgnoreCase("AM")) {
                id13 = R.drawable.am;
            }
            int id14 = id13;
            if (flag2.equalsIgnoreCase("AI")) {
                id14 = R.drawable.ai;
            }
            int id15 = id14;
            if (flag2.equalsIgnoreCase("AO")) {
                id15 = R.drawable.ao;
            }
            int id16 = id15;
            if (flag2.equalsIgnoreCase("AG")) {
                id16 = R.drawable.ag;
            }
            int id17 = id16;
            if (flag2.equalsIgnoreCase("AD")) {
                id17 = R.drawable.ad;
            }
            int id18 = id17;
            if (flag2.equalsIgnoreCase("YE")) {
                id18 = R.drawable.ye;
            }
            int id19 = id18;
            if (flag2.equalsIgnoreCase("GB")) {
                id19 = R.drawable.gb;
            }
            int id20 = id19;
            if (flag2.equalsIgnoreCase("IO")) {
                id20 = R.drawable.io;
            }
            int id21 = id20;
            if (flag2.equalsIgnoreCase("VG")) {
                id21 = R.drawable.vg;
            }
            int id22 = id21;
            if (flag2.equalsIgnoreCase("IL")) {
                id22 = R.drawable.il;
            }
            int id23 = id22;
            if (flag2.equalsIgnoreCase("IT")) {
                id23 = R.drawable.it;
            }
            int id24 = id23;
            if (flag2.equalsIgnoreCase("IQ")) {
                id24 = R.drawable.iq;
            }
            int id25 = id24;
            if (flag2.equalsIgnoreCase("IR")) {
                id25 = R.drawable.ir;
            }
            int id26 = id25;
            if (flag2.equalsIgnoreCase("IN")) {
                id26 = R.drawable.in;
            }
            int id27 = id26;
            if (flag2.equalsIgnoreCase("ID")) {
                id27 = R.drawable.id;
            }
            int id28 = id27;
            if (flag2.equalsIgnoreCase("UG")) {
                id28 = R.drawable.ug;
            }
            int id29 = id28;
            if (flag2.equalsIgnoreCase("UA")) {
                id29 = R.drawable.ua;
            }
            int id30 = id29;
            if (flag2.equalsIgnoreCase("UZ")) {
                id30 = R.drawable.uz;
            }
            int id31 = id30;
            if (flag2.equalsIgnoreCase("UY")) {
                id31 = R.drawable.uy;
            }
            int id32 = id31;
            if (flag2.equalsIgnoreCase("EC")) {
                id32 = R.drawable.ec;
            }
            int id33 = id32;
            if (flag2.equalsIgnoreCase("EE")) {
                id33 = R.drawable.ee;
            }
            int id34 = id33;
            if (flag2.equalsIgnoreCase("ET")) {
                id34 = R.drawable.et;
            }
            int id35 = id34;
            if (flag2.equalsIgnoreCase("ER")) {
                id35 = R.drawable.er;
            }
            int id36 = id35;
            if (flag2.equalsIgnoreCase("SV")) {
                id36 = R.drawable.sv;
            }
            int id37 = id36;
            if (flag2.equalsIgnoreCase("AU")) {
                id37 = R.drawable.au;
            }
            int id38 = id37;
            if (flag2.equalsIgnoreCase("AT")) {
                id38 = R.drawable.at;
            }
            int id39 = id38;
            if (flag2.equalsIgnoreCase("OM")) {
                id39 = R.drawable.om;
            }
            int id40 = id39;
            if (flag2.equalsIgnoreCase("NL")) {
                id40 = R.drawable.nl;
            }
            int id41 = id40;
            if (flag2.equalsIgnoreCase("AN")) {
                id41 = R.drawable.an;
            }
            int id42 = id41;
            if (flag2.equalsIgnoreCase("GH")) {
                id42 = R.drawable.gh;
            }
            int id43 = id42;
            if (flag2.equalsIgnoreCase("GG")) {
                id43 = R.drawable.gg;
            }
            int id44 = id43;
            if (flag2.equalsIgnoreCase("GY")) {
                id44 = R.drawable.gy;
            }
            int id45 = id44;
            if (flag2.equalsIgnoreCase("KZ")) {
                id45 = R.drawable.kz;
            }
            int id46 = id45;
            if (flag2.equalsIgnoreCase("QA")) {
                id46 = R.drawable.qa;
            }
            int id47 = id46;
            if (flag2.equalsIgnoreCase("CA")) {
                id47 = R.drawable.ca;
            }
            int id48 = id47;
            if (flag2.equalsIgnoreCase("CV")) {
                id48 = R.drawable.cv;
            }
            int id49 = id48;
            if (flag2.equalsIgnoreCase("GA")) {
                id49 = R.drawable.ga;
            }
            int id50 = id49;
            if (flag2.equalsIgnoreCase("CM")) {
                id50 = R.drawable.cm;
            }
            int id51 = id50;
            if (flag2.equalsIgnoreCase("GM")) {
                id51 = R.drawable.gm;
            }
            int id52 = id51;
            if (flag2.equalsIgnoreCase("KH")) {
                id52 = R.drawable.kh;
            }
            int id53 = id52;
            if (flag2.equalsIgnoreCase("MP")) {
                id53 = R.drawable.mp;
            }
            int id54 = id53;
            if (flag2.equalsIgnoreCase("GN")) {
                id54 = R.drawable.gn;
            }
            int id55 = id54;
            if (flag2.equalsIgnoreCase("GW")) {
                id55 = R.drawable.gw;
            }
            int id56 = id55;
            if (flag2.equalsIgnoreCase("CY")) {
                id56 = R.drawable.cy;
            }
            int id57 = id56;
            if (flag2.equalsIgnoreCase("CU")) {
                id57 = R.drawable.cu;
            }
            int id58 = id57;
            if (flag2.equalsIgnoreCase("GR")) {
                id58 = R.drawable.gr;
            }
            int id59 = id58;
            if (flag2.equalsIgnoreCase("KI")) {
                id59 = R.drawable.ki;
            }
            int id60 = id59;
            if (flag2.equalsIgnoreCase("KG")) {
                id60 = R.drawable.kg;
            }
            int id61 = id60;
            if (flag2.equalsIgnoreCase("GT")) {
                id61 = R.drawable.gt;
            }
            int id62 = id61;
            if (flag2.equalsIgnoreCase("GU")) {
                id62 = R.drawable.gu;
            }
            int id63 = id62;
            if (flag2.equalsIgnoreCase("KW")) {
                id63 = R.drawable.kw;
            }
            int id64 = id63;
            if (flag2.equalsIgnoreCase("CK")) {
                id64 = R.drawable.ck;
            }
            int id65 = id64;
            if (flag2.equalsIgnoreCase("GL")) {
                id65 = R.drawable.gl;
            }
            int id66 = id65;
            if (flag2.equalsIgnoreCase("CX")) {
                id66 = R.drawable.cx;
            }
            int id67 = id66;
            if (flag2.equalsIgnoreCase("GE")) {
                id67 = R.drawable.ge;
            }
            int id68 = id67;
            if (flag2.equalsIgnoreCase("GD")) {
                id68 = R.drawable.gd;
            }
            int id69 = id68;
            if (flag2.equalsIgnoreCase("HR")) {
                id69 = R.drawable.hr;
            }
            int id70 = id69;
            if (flag2.equalsIgnoreCase("KY")) {
                id70 = R.drawable.ky;
            }
            int id71 = id70;
            if (flag2.equalsIgnoreCase("KE")) {
                id71 = R.drawable.ke;
            }
            int id72 = id71;
            if (flag2.equalsIgnoreCase("CI")) {
                id72 = R.drawable.ci;
            }
            int id73 = id72;
            if (flag2.equalsIgnoreCase("CC")) {
                id73 = R.drawable.cc;
            }
            int id74 = id73;
            if (flag2.equalsIgnoreCase("CR")) {
                id74 = R.drawable.cr;
            }
            int id75 = id74;
            if (flag2.equalsIgnoreCase("KM")) {
                id75 = R.drawable.km;
            }
            int id76 = id75;
            if (flag2.equalsIgnoreCase("CO")) {
                id76 = R.drawable.co;
            }
            int id77 = id76;
            if (flag2.equalsIgnoreCase("CG")) {
                id77 = R.drawable.cg;
            }
            int id78 = id77;
            if (flag2.equalsIgnoreCase("CD")) {
                id78 = R.drawable.cd;
            }
            int id79 = id78;
            if (flag2.equalsIgnoreCase("SA")) {
                id79 = R.drawable.sa;
            }
            int id80 = id79;
            if (flag2.equalsIgnoreCase("WS")) {
                id80 = R.drawable.ws;
            }
            int id81 = id80;
            if (flag2.equalsIgnoreCase("ST")) {
                id81 = R.drawable.st;
            }
            int id82 = id81;
            if (flag2.equalsIgnoreCase("ZM")) {
                id82 = R.drawable.zm;
            }
            int id83 = id82;
            if (flag2.equalsIgnoreCase("PM")) {
                id83 = R.drawable.pm;
            }
            int id84 = id83;
            if (flag2.equalsIgnoreCase("SM")) {
                id84 = R.drawable.sm;
            }
            int id85 = id84;
            if (flag2.equalsIgnoreCase("SL")) {
                id85 = R.drawable.sl;
            }
            int id86 = id85;
            if (flag2.equalsIgnoreCase("DJ")) {
                id86 = R.drawable.dj;
            }
            int id87 = id86;
            if (flag2.equalsIgnoreCase("GI")) {
                id87 = R.drawable.gi;
            }
            int id88 = id87;
            if (flag2.equalsIgnoreCase("JE")) {
                id88 = R.drawable.je;
            }
            int id89 = id88;
            if (flag2.equalsIgnoreCase("JM")) {
                id89 = R.drawable.jm;
            }
            int id90 = id89;
            if (flag2.equalsIgnoreCase("SY")) {
                id90 = R.drawable.sy;
            }
            int id91 = id90;
            if (flag2.equalsIgnoreCase("SG")) {
                id91 = R.drawable.sg;
            }
            int id92 = id91;
            if (flag2.equalsIgnoreCase("ZW")) {
                id92 = R.drawable.zw;
            }
            int id93 = id92;
            if (flag2.equalsIgnoreCase("CH")) {
                id93 = R.drawable.ch;
            }
            int id94 = id93;
            if (flag2.equalsIgnoreCase("SE")) {
                id94 = R.drawable.se;
            }
            int id95 = id94;
            if (flag2.equalsIgnoreCase("SD")) {
                id95 = R.drawable.sd;
            }
            int id96 = id95;
            if (flag2.equalsIgnoreCase("SJ")) {
                id96 = R.drawable.sj;
            }
            int id97 = id96;
            if (flag2.equalsIgnoreCase("ES")) {
                id97 = R.drawable.es;
            }
            int id98 = id97;
            if (flag2.equalsIgnoreCase("SR")) {
                id98 = R.drawable.sr;
            }
            int id99 = id98;
            if (flag2.equalsIgnoreCase("LK")) {
                id99 = R.drawable.lk;
            }
            int id100 = id99;
            if (flag2.equalsIgnoreCase("SK")) {
                id100 = R.drawable.sk;
            }
            int id101 = id100;
            if (flag2.equalsIgnoreCase("SI")) {
                id101 = R.drawable.si;
            }
            int id102 = id101;
            if (flag2.equalsIgnoreCase("SZ")) {
                id102 = R.drawable.sz;
            }
            int id103 = id102;
            if (flag2.equalsIgnoreCase("SC")) {
                id103 = R.drawable.sc;
            }
            int id104 = id103;
            if (flag2.equalsIgnoreCase("GQ")) {
                id104 = R.drawable.gq;
            }
            int id105 = id104;
            if (flag2.equalsIgnoreCase("SN")) {
                id105 = R.drawable.sn;
            }
            int id106 = id105;
            if (flag2.equalsIgnoreCase("RS")) {
                id106 = R.drawable.rs;
            }
            int id107 = id106;
            if (flag2.equalsIgnoreCase("KN")) {
                id107 = R.drawable.kn;
            }
            int id108 = id107;
            if (flag2.equalsIgnoreCase("VC")) {
                id108 = R.drawable.vc;
            }
            int id109 = id108;
            if (flag2.equalsIgnoreCase("SH")) {
                id109 = R.drawable.sh;
            }
            int id110 = id109;
            if (flag2.equalsIgnoreCase("LC")) {
                id110 = R.drawable.lc;
            }
            int id111 = id110;
            if (flag2.equalsIgnoreCase("SO")) {
                id111 = R.drawable.so;
            }
            int id112 = id111;
            if (flag2.equalsIgnoreCase("SB")) {
                id112 = R.drawable.sb;
            }
            int id113 = id112;
            if (flag2.equalsIgnoreCase("TC")) {
                id113 = R.drawable.tc;
            }
            int id114 = id113;
            if (flag2.equalsIgnoreCase("TH")) {
                id114 = R.drawable.th;
            }
            int id115 = id114;
            if (flag2.equalsIgnoreCase("KR")) {
                id115 = R.drawable.kr;
            }
            int id116 = id115;
            if (flag2.equalsIgnoreCase("TW")) {
                id116 = R.drawable.tw;
            }
            int id117 = id116;
            if (flag2.equalsIgnoreCase("TJ")) {
                id117 = R.drawable.tj;
            }
            int id118 = id117;
            if (flag2.equalsIgnoreCase("TZ")) {
                id118 = R.drawable.tz;
            }
            int id119 = id118;
            if (flag2.equalsIgnoreCase("CZ")) {
                id119 = R.drawable.cz;
            }
            int id120 = id119;
            if (flag2.equalsIgnoreCase("TD")) {
                id120 = R.drawable.td;
            }
            int id121 = id120;
            if (flag2.equalsIgnoreCase("CF")) {
                id121 = R.drawable.cf;
            }
            int id122 = id121;
            if (flag2.equalsIgnoreCase("CN")) {
                id122 = R.drawable.cn;
            }
            int id123 = id122;
            if (flag2.equalsIgnoreCase("TN")) {
                id123 = R.drawable.tn;
            }
            int id124 = id123;
            if (flag2.equalsIgnoreCase("KP")) {
                id124 = R.drawable.kp;
            }
            int id125 = id124;
            if (flag2.equalsIgnoreCase("CL")) {
                id125 = R.drawable.cl;
            }
            int id126 = id125;
            if (flag2.equalsIgnoreCase("TV")) {
                id126 = R.drawable.tv;
            }
            int id127 = id126;
            if (flag2.equalsIgnoreCase("DK")) {
                id127 = R.drawable.dk;
            }
            int id128 = id127;
            if (flag2.equalsIgnoreCase("DE")) {
                id128 = R.drawable.de;
            }
            int id129 = id128;
            if (flag2.equalsIgnoreCase("TG")) {
                id129 = R.drawable.tg;
            }
            int id130 = id129;
            if (flag2.equalsIgnoreCase("TK")) {
                id130 = R.drawable.tk;
            }
            int id131 = id130;
            if (flag2.equalsIgnoreCase("DO")) {
                id131 = R.drawable.doe;
            }
            int id132 = id131;
            if (flag2.equalsIgnoreCase("DM")) {
                id132 = R.drawable.dm;
            }
            int id133 = id132;
            if (flag2.equalsIgnoreCase("TT")) {
                id133 = R.drawable.tt;
            }
            int id134 = id133;
            if (flag2.equalsIgnoreCase("TM")) {
                id134 = R.drawable.tm;
            }
            int id135 = id134;
            if (flag2.equalsIgnoreCase("TR")) {
                id135 = R.drawable.tr;
            }
            int id136 = id135;
            if (flag2.equalsIgnoreCase("TO")) {
                id136 = R.drawable.to;
            }
            int id137 = id136;
            if (flag2.equalsIgnoreCase("NG")) {
                id137 = R.drawable.ng;
            }
            int id138 = id137;
            if (flag2.equalsIgnoreCase("NR")) {
                id138 = R.drawable.nr;
            }
            int id139 = id138;
            if (flag2.equalsIgnoreCase("NA")) {
                id139 = R.drawable.na;
            }
            int id140 = id139;
            if (flag2.equalsIgnoreCase("NU")) {
                id140 = R.drawable.nu;
            }
            int id141 = id140;
            if (flag2.equalsIgnoreCase("NI")) {
                id141 = R.drawable.ni;
            }
            int id142 = id141;
            if (flag2.equalsIgnoreCase("NE")) {
                id142 = R.drawable.ne;
            }
            int id143 = id142;
            if (flag2.equalsIgnoreCase("JP")) {
                id143 = R.drawable.f0jp;
            }
            int id144 = id143;
            if (flag2.equalsIgnoreCase("EH")) {
                id144 = R.drawable.eh;
            }
            int id145 = id144;
            if (flag2.equalsIgnoreCase("NC")) {
                id145 = R.drawable.nc;
            }
            int id146 = id145;
            if (flag2.equalsIgnoreCase("NZ")) {
                id146 = R.drawable.nz;
            }
            int id147 = id146;
            if (flag2.equalsIgnoreCase("NP")) {
                id147 = R.drawable.np;
            }
            int id148 = id147;
            if (flag2.equalsIgnoreCase("NF")) {
                id148 = R.drawable.nf;
            }
            int id149 = id148;
            if (flag2.equalsIgnoreCase("NO")) {
                id149 = R.drawable.no;
            }
            int id150 = id149;
            if (flag2.equalsIgnoreCase("BH")) {
                id150 = R.drawable.bh;
            }
            int id151 = id150;
            if (flag2.equalsIgnoreCase("HT")) {
                id151 = R.drawable.ht;
            }
            int id152 = id151;
            if (flag2.equalsIgnoreCase("PK")) {
                id152 = R.drawable.pk;
            }
            int id153 = id152;
            if (flag2.equalsIgnoreCase("VA")) {
                id153 = R.drawable.va;
            }
            int id154 = id153;
            if (flag2.equalsIgnoreCase("PA")) {
                id154 = R.drawable.pa;
            }
            int id155 = id154;
            if (flag2.equalsIgnoreCase("VU")) {
                id155 = R.drawable.vu;
            }
            int id156 = id155;
            if (flag2.equalsIgnoreCase("BS")) {
                id156 = R.drawable.bs;
            }
            int id157 = id156;
            if (flag2.equalsIgnoreCase("PG")) {
                id157 = R.drawable.pg;
            }
            int id158 = id157;
            if (flag2.equalsIgnoreCase("BM")) {
                id158 = R.drawable.bm;
            }
            int id159 = id158;
            if (flag2.equalsIgnoreCase("PW")) {
                id159 = R.drawable.pw;
            }
            int id160 = id159;
            if (flag2.equalsIgnoreCase("PY")) {
                id160 = R.drawable.py;
            }
            int id161 = id160;
            if (flag2.equalsIgnoreCase("BB")) {
                id161 = R.drawable.bb;
            }
            int id162 = id161;
            if (flag2.equalsIgnoreCase("PS")) {
                id162 = R.drawable.ps;
            }
            int id163 = id162;
            if (flag2.equalsIgnoreCase("HU")) {
                id163 = R.drawable.hu;
            }
            int id164 = id163;
            if (flag2.equalsIgnoreCase("BD")) {
                id164 = R.drawable.bd;
            }
            int id165 = id164;
            if (flag2.equalsIgnoreCase("TL")) {
                id165 = R.drawable.tl;
            }
            int id166 = id165;
            if (flag2.equalsIgnoreCase("PN")) {
                id166 = R.drawable.pn;
            }
            int id167 = id166;
            if (flag2.equalsIgnoreCase("FJ")) {
                id167 = R.drawable.fj;
            }
            int id168 = id167;
            if (flag2.equalsIgnoreCase("PH")) {
                id168 = R.drawable.ph;
            }
            int id169 = id168;
            if (flag2.equalsIgnoreCase("FI")) {
                id169 = R.drawable.fi;
            }
            int id170 = id169;
            if (flag2.equalsIgnoreCase("BT")) {
                id170 = R.drawable.bt;
            }
            int id171 = id170;
            if (flag2.equalsIgnoreCase("BV")) {
                id171 = R.drawable.bv;
            }
            int id172 = id171;
            if (flag2.equalsIgnoreCase("PR")) {
                id172 = R.drawable.pr;
            }
            int id173 = id172;
            if (flag2.equalsIgnoreCase("FO")) {
                id173 = R.drawable.fo;
            }
            int id174 = id173;
            if (flag2.equalsIgnoreCase("FK")) {
                id174 = R.drawable.fk;
            }
            int id175 = id174;
            if (flag2.equalsIgnoreCase("BR")) {
                id175 = R.drawable.br;
            }
            int id176 = id175;
            if (flag2.equalsIgnoreCase("FR")) {
                id176 = R.drawable.fr;
            }
            int id177 = id176;
            if (flag2.equalsIgnoreCase("GF")) {
                id177 = R.drawable.gf;
            }
            int id178 = id177;
            if (flag2.equalsIgnoreCase("PF")) {
                id178 = R.drawable.pf;
            }
            int id179 = id178;
            if (flag2.equalsIgnoreCase("TF")) {
                id179 = R.drawable.tf;
            }
            int id180 = id179;
            if (flag2.equalsIgnoreCase("BG")) {
                id180 = R.drawable.bg;
            }
            int id181 = id180;
            if (flag2.equalsIgnoreCase("BF")) {
                id181 = R.drawable.bf;
            }
            int id182 = id181;
            if (flag2.equalsIgnoreCase("BN")) {
                id182 = R.drawable.bn;
            }
            int id183 = id182;
            if (flag2.equalsIgnoreCase("BI")) {
                id183 = R.drawable.bi;
            }
            int id184 = id183;
            if (flag2.equalsIgnoreCase("HM")) {
                id184 = R.drawable.hm;
            }
            int id185 = id184;
            if (flag2.equalsIgnoreCase("VN")) {
                id185 = R.drawable.vn;
            }
            int id186 = id185;
            if (flag2.equalsIgnoreCase("BJ")) {
                id186 = R.drawable.bj;
            }
            int id187 = id186;
            if (flag2.equalsIgnoreCase("VE")) {
                id187 = R.drawable.ve;
            }
            int id188 = id187;
            if (flag2.equalsIgnoreCase("BY")) {
                id188 = R.drawable.by;
            }
            int id189 = id188;
            if (flag2.equalsIgnoreCase("BZ")) {
                id189 = R.drawable.bz;
            }
            int id190 = id189;
            if (flag2.equalsIgnoreCase("PE")) {
                id190 = R.drawable.pe;
            }
            int id191 = id190;
            if (flag2.equalsIgnoreCase("BE")) {
                id191 = R.drawable.be;
            }
            int id192 = id191;
            if (flag2.equalsIgnoreCase("PL")) {
                id192 = R.drawable.pl;
            }
            int id193 = id192;
            if (flag2.equalsIgnoreCase("BA")) {
                id193 = R.drawable.ba;
            }
            int id194 = id193;
            if (flag2.equalsIgnoreCase("BW")) {
                id194 = R.drawable.bw;
            }
            int id195 = id194;
            if (flag2.equalsIgnoreCase("BO")) {
                id195 = R.drawable.bo;
            }
            int id196 = id195;
            if (flag2.equalsIgnoreCase("PT")) {
                id196 = R.drawable.pt;
            }
            int id197 = id196;
            if (flag2.equalsIgnoreCase("HK")) {
                id197 = R.drawable.hk;
            }
            int id198 = id197;
            if (flag2.equalsIgnoreCase("HN")) {
                id198 = R.drawable.hn;
            }
            int id199 = id198;
            if (flag2.equalsIgnoreCase("MH")) {
                id199 = R.drawable.mh;
            }
            int id200 = id199;
            if (flag2.equalsIgnoreCase("MO")) {
                id200 = R.drawable.mo;
            }
            int id201 = id200;
            if (flag2.equalsIgnoreCase("MK")) {
                id201 = R.drawable.mk;
            }
            int id202 = id201;
            if (flag2.equalsIgnoreCase("MG")) {
                id202 = R.drawable.mg;
            }
            int id203 = id202;
            if (flag2.equalsIgnoreCase("YT")) {
                id203 = R.drawable.yt;
            }
            int id204 = id203;
            if (flag2.equalsIgnoreCase("MW")) {
                id204 = R.drawable.mw;
            }
            int id205 = id204;
            if (flag2.equalsIgnoreCase("ML")) {
                id205 = R.drawable.ml;
            }
            int id206 = id205;
            if (flag2.equalsIgnoreCase("MT")) {
                id206 = R.drawable.mt;
            }
            int id207 = id206;
            if (flag2.equalsIgnoreCase("MQ")) {
                id207 = R.drawable.mq;
            }
            int id208 = id207;
            if (flag2.equalsIgnoreCase("MY")) {
                id208 = R.drawable.my;
            }
            int id209 = id208;
            if (flag2.equalsIgnoreCase("IM")) {
                id209 = R.drawable.im;
            }
            int id210 = id209;
            if (flag2.equalsIgnoreCase("FM")) {
                id210 = R.drawable.fm;
            }
            int id211 = id210;
            if (flag2.equalsIgnoreCase("ZA")) {
                id211 = R.drawable.za;
            }
            int id212 = id211;
            if (flag2.equalsIgnoreCase("GS")) {
                id212 = R.drawable.gs;
            }
            int id213 = id212;
            if (flag2.equalsIgnoreCase("MM")) {
                id213 = R.drawable.mm;
            }
            int id214 = id213;
            if (flag2.equalsIgnoreCase("MX")) {
                id214 = R.drawable.mx;
            }
            int id215 = id214;
            if (flag2.equalsIgnoreCase("MU")) {
                id215 = R.drawable.mu;
            }
            int id216 = id215;
            if (flag2.equalsIgnoreCase("MR")) {
                id216 = R.drawable.mr;
            }
            int id217 = id216;
            if (flag2.equalsIgnoreCase("MZ")) {
                id217 = R.drawable.mz;
            }
            int id218 = id217;
            if (flag2.equalsIgnoreCase("MC")) {
                id218 = R.drawable.mc;
            }
            int id219 = id218;
            if (flag2.equalsIgnoreCase("MV")) {
                id219 = R.drawable.mv;
            }
            int id220 = id219;
            if (flag2.equalsIgnoreCase("MD")) {
                id220 = R.drawable.md;
            }
            int id221 = id220;
            if (flag2.equalsIgnoreCase("MA")) {
                id221 = R.drawable.ma;
            }
            int id222 = id221;
            if (flag2.equalsIgnoreCase("MN")) {
                id222 = R.drawable.mn;
            }
            int id223 = id222;
            if (flag2.equalsIgnoreCase("ME")) {
                id223 = R.drawable.me;
            }
            int id224 = id223;
            if (flag2.equalsIgnoreCase("MS")) {
                id224 = R.drawable.ms;
            }
            int id225 = id224;
            if (flag2.equalsIgnoreCase("JO")) {
                id225 = R.drawable.jo;
            }
            int id226 = id225;
            if (flag2.equalsIgnoreCase("LA")) {
                id226 = R.drawable.la;
            }
            int id227 = id226;
            if (flag2.equalsIgnoreCase("LV")) {
                id227 = R.drawable.lv;
            }
            int id228 = id227;
            if (flag2.equalsIgnoreCase("LT")) {
                id228 = R.drawable.lt;
            }
            int id229 = id228;
            if (flag2.equalsIgnoreCase("LY")) {
                id229 = R.drawable.ly;
            }
            int id230 = id229;
            if (flag2.equalsIgnoreCase("LI")) {
                id230 = R.drawable.li;
            }
            int id231 = id230;
            if (flag2.equalsIgnoreCase("LR")) {
                id231 = R.drawable.lr;
            }
            int id232 = id231;
            if (flag2.equalsIgnoreCase("RO\t")) {
                id232 = R.drawable.ro;
            }
            int id233 = id232;
            if (flag2.equalsIgnoreCase("LU")) {
                id233 = R.drawable.lu;
            }
            int id234 = id233;
            if (flag2.equalsIgnoreCase("RW")) {
                id234 = R.drawable.rw;
            }
            int id235 = id234;
            if (flag2.equalsIgnoreCase("LS")) {
                id235 = R.drawable.ls;
            }
            int id236 = id235;
            if (flag2.equalsIgnoreCase("LB")) {
                id236 = R.drawable.lb;
            }
            int id237 = id236;
            if (flag2.equalsIgnoreCase("RE")) {
                id237 = R.drawable.re;
            }
            int id238 = id237;
            if (flag2.equalsIgnoreCase("RU")) {
                id238 = R.drawable.ru;
            }
            if (flag2.equalsIgnoreCase("WF")) {
                return R.drawable.wf;
            }
            return id238;
        }
    }
}
