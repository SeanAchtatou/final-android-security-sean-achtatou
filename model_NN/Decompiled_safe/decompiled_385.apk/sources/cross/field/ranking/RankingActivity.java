package cross.field.ranking;

import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.TextView;
import com.crossfd.android.MeteorBreaker.common.database.model.UserPointDto;
import com.crossfd.android.MeteorBreaker.utility.Constants;
import com.crossfd.android.MeteorBreaker.utility.RestWebServiceClient;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.google.gson.Gson;
import cross.field.MeteorBreaker.Analytics;
import cross.field.MeteorBreaker.PreferenceKeys;
import cross.field.MeteorBreaker.R;
import cross.field.ranking.RankingLayout;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class RankingActivity extends TabActivity implements TabHost.OnTabChangeListener {
    public static final int WRAP_CONTENT = -2;
    public static final float XPERIA_HEIGHT = 854.0f;
    public static final float XPERIA_WIDTH = 480.0f;
    private String country = Analytics.ID;
    private int disp_height;
    private int disp_width;
    RankingLayout layout;
    private float ratio_height;
    private float ratio_width;
    Analytics tracker;

    public void getDisplaySize() {
        Display disp = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        setDispWidth(disp.getWidth());
        setDispHeight(disp.getHeight());
    }

    public void getDisplayRatio() {
        getDisplaySize();
        setRatioWidth(((float) getDispWidth()) / 480.0f);
        setRatioHeight(((float) getDispHeight()) / 854.0f);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        setContentView((int) R.layout.ranking);
        this.layout = new RankingLayout(this);
        List<RankingBean> ranking_area = new ArrayList<>();
        ListView list_area = (ListView) findViewById(R.id.list_private);
        SharedPreferences pref = getSharedPreferences(PreferenceKeys.PREFERENCE, 3);
        int num = 0;
        int i = 0;
        while (i < 100 && ((int) pref.getLong(PreferenceKeys.SCORE + i, -1)) != -1) {
            num++;
            ranking_area.add(setRanking(String.valueOf(num) + "   ", "   " + String.valueOf((int) pref.getLong(PreferenceKeys.SCORE + i, -1)) + "pt       ", Analytics.ID, Analytics.ID, getCountry()));
            i++;
        }
        RankingLayout rankingLayout = this.layout;
        rankingLayout.getClass();
        RankingLayout.ListAdapter adapter_area = new RankingLayout.ListAdapter(getApplicationContext(), ranking_area);
        if (list_area != null) {
            list_area.setAdapter((ListAdapter) adapter_area);
        }
        ((TextView) findViewById(R.id.tab_sub)).setText(Analytics.ID);
        this.tracker = new Analytics(GoogleAnalyticsTracker.getInstance(), this);
        this.tracker.trackPageView("Ranking");
    }

    private void populateRankingData(String category) {
        TelephonyManager tel = (TelephonyManager) getSystemService("phone");
        String name = getSharedPreferences(PreferenceKeys.PREFERENCE, 3).getString(PreferenceKeys.NAME, Analytics.ID);
        if (name.length() != 0) {
            RestWebServiceClient restWebServiceClient = new RestWebServiceClient(Constants.URL_GET_RANKING);
            HashMap hashMap = new HashMap();
            hashMap.put("category", category);
            hashMap.put("country_code", getCountry());
            hashMap.put("user_name", name);
            hashMap.put("device_id", tel.getDeviceId());
            try {
                JSONObject object = (JSONObject) new JSONTokener(restWebServiceClient.webGet(Analytics.ID, hashMap)).nextValue();
                int userRank = object.getInt("self_rank");
                JSONArray arrayJson = object.getJSONArray("list_rank");
                ArrayList<UserPointDto> arrayList = new ArrayList<>();
                for (int i = 0; i < arrayJson.length(); i++) {
                    arrayList.add((UserPointDto) new Gson().fromJson(arrayJson.getJSONObject(i).toString(), UserPointDto.class));
                }
                TextView tub_sub = (TextView) findViewById(R.id.tab_sub);
                if (userRank != -1) {
                    tub_sub.setText(String.valueOf(userRank) + "th");
                    if (userRank == 1) {
                        tub_sub.setText(String.valueOf(userRank) + "st");
                    }
                    if (userRank == 2) {
                        tub_sub.setText(String.valueOf(userRank) + "nd");
                    }
                    if (userRank == 3) {
                        tub_sub.setText(String.valueOf(userRank) + "rd");
                    }
                }
                if (userRank == -1) {
                    tub_sub.setText(Analytics.ID);
                }
                List<RankingBean> ranking_area = new ArrayList<>();
                ListView list_area = null;
                switch (getTabHost().getCurrentTab()) {
                    case 1:
                        list_area = (ListView) findViewById(R.id.list_domestic);
                        break;
                    case 2:
                        list_area = (ListView) findViewById(R.id.list_world);
                        break;
                }
                int num = 0;
                for (UserPointDto dto : arrayList) {
                    num++;
                    ranking_area.add(setRanking(String.valueOf(num) + "   ", "   " + String.valueOf((int) dto.getUserPoint()) + "pt       ", dto.getUserName(), dto.getInsertTime(), dto.getCountryCode()));
                }
                RankingLayout rankingLayout = this.layout;
                rankingLayout.getClass();
                RankingLayout.ListAdapter listAdapter = new RankingLayout.ListAdapter(getApplicationContext(), ranking_area);
                if (list_area != null) {
                    list_area.setAdapter((ListAdapter) listAdapter);
                }
            } catch (Exception e) {
                showDialog(this, "Failure", "Connection Failure!");
            }
        }
    }

    public RankingBean setRanking(String number, String time, String name, String date, String country2) {
        RankingBean ranking = new RankingBean();
        ranking.setNumber(number);
        ranking.setTime(time);
        ranking.setName(name);
        SimpleDateFormat formatReceive = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            ranking.setDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(formatReceive.parse(date)));
        } catch (ParseException e) {
        }
        ranking.setCountry(country2);
        return ranking;
    }

    private static void showDialog(Context context, String title, String text) {
        AlertDialog.Builder ad = new AlertDialog.Builder(context);
        ad.setTitle(title);
        ad.setMessage(text);
        ad.setPositiveButton("OK", (DialogInterface.OnClickListener) null);
        ad.show();
    }

    public void showDialog(String title, String text) {
        AlertDialog.Builder ad = new AlertDialog.Builder(this);
        ad.setTitle(title);
        ad.setMessage(text);
        ad.setPositiveButton("OK", (DialogInterface.OnClickListener) null);
        ad.show();
    }

    private String getCountry() {
        this.country = getResources().getConfiguration().locale.getCountry();
        switch (getTabHost().getCurrentTab()) {
            case 0:
                return getResources().getConfiguration().locale.getCountry();
            case 1:
                return getResources().getConfiguration().locale.getCountry();
            case 2:
                return Constants.RANKING_COUNTRY_WORLD;
            default:
                return this.country;
        }
    }

    public void radioDaily(View view) {
        switch (getTabHost().getCurrentTab()) {
            case 0:
            default:
                return;
            case 1:
            case 2:
                populateRankingData(Constants.RANKING_CATEGORY_TODAY);
                return;
        }
    }

    public void radioWeekly(View view) {
        switch (getTabHost().getCurrentTab()) {
            case 0:
            default:
                return;
            case 1:
            case 2:
                populateRankingData(Constants.RANKING_CATEGORY_WEEKLY);
                return;
        }
    }

    public void radioMonthly(View view) {
        switch (getTabHost().getCurrentTab()) {
            case 0:
            default:
                return;
            case 1:
            case 2:
                populateRankingData(Constants.RANKING_CATEGORY_MONTHLY);
                return;
        }
    }

    public void radioAll(View view) {
        switch (getTabHost().getCurrentTab()) {
            case 0:
            default:
                return;
            case 1:
            case 2:
                populateRankingData(Constants.RANKING_CATEGORY_ALL);
                return;
        }
    }

    public void onTabChanged(String s) {
        new RadioGroup(this);
        String interval = Analytics.ID;
        switch (((RadioButton) findViewById(((RadioGroup) findViewById(R.id.radioGroup_interval)).getCheckedRadioButtonId())).getId()) {
            case R.id.radioButton_daily:
                interval = Constants.RANKING_CATEGORY_TODAY;
                break;
            case R.id.radioButton_weekly:
                interval = Constants.RANKING_CATEGORY_WEEKLY;
                break;
            case R.id.radioButton_monthly:
                interval = Constants.RANKING_CATEGORY_MONTHLY;
                break;
            case R.id.radioButton_all:
                interval = Constants.RANKING_CATEGORY_ALL;
                break;
        }
        switch (getTabHost().getCurrentTab()) {
            case 0:
                ((TextView) findViewById(R.id.tab_sub)).setText(Analytics.ID);
                return;
            case 1:
                populateRankingData(interval);
                return;
            case 2:
                populateRankingData(interval);
                return;
            default:
                return;
        }
    }

    public void buttonBack(View view) {
        this.tracker.trackEvent("Ranking", "Click", "Back", 1);
        finish();
    }

    public void buttonMore(View v) {
        this.tracker.trackEvent("Ranking", "Click", "More", 1);
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://androida.me")));
    }

    public void buttonBuy(View v) {
    }

    public void setDispWidth(int disp_width2) {
        this.disp_width = disp_width2;
    }

    public int getDispWidth() {
        return this.disp_width;
    }

    public void setDispHeight(int disp_height2) {
        this.disp_height = disp_height2;
    }

    public int getDispHeight() {
        return this.disp_height;
    }

    public void setRatioWidth(float ratio_width2) {
        this.ratio_width = ratio_width2;
    }

    public float getRatioWidth() {
        return this.ratio_width;
    }

    public void setRatioHeight(float ratio_height2) {
        this.ratio_height = ratio_height2;
    }

    public float getRatioHeight() {
        return this.ratio_height;
    }
}
