package cross.field.MeteorBreaker;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.text.SpannableStringBuilder;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainLayout {
    public static final int ADLANTIS_HEIGHT = 85;
    public static final int ADLANTIS_WIDTH = 480;
    public static final int ADLANTIS_X = 0;
    public static final int ADLANTIS_Y = 512;
    public static final int BUTTON_GROUP_HEIGHT = 256;
    public static final int BUTTON_GROUP_WIDTH = 480;
    public static final int BUTTON_GROUP_X = 0;
    public static final int BUTTON_GROUP_Y = 597;
    public static final String DEFOAULT_NAME = "Breaker";
    public static final int NAME_HEIGHT = -2;
    public static final int NAME_WIDTH = 480;
    public static final int NAME_X = 0;
    public static final int NAME_Y = 427;
    public static final int TITLE_HEIGHT = -2;
    public static final int TITLE_WIDTH = 480;
    public static final int TITLE_X = 0;
    public static final int TITLE_Y = 170;
    public static final int WRAP_CONTENT = -2;
    /* access modifiers changed from: private */
    public MainActivity activity;
    private int adlantis_height = 85;
    private int adlantis_width = 480;
    private int adlantis_x = 0;
    private int adlantis_y = ADLANTIS_Y;
    private int buttonGroup_height = BUTTON_GROUP_HEIGHT;
    private int buttonGroup_width = 480;
    private int buttonGroup_x = 0;
    private int buttonGroup_y = BUTTON_GROUP_Y;
    private int name_height = -2;
    private int name_width = 480;
    private int name_x = 0;
    private int name_y = NAME_Y;
    private int title_height = -2;
    private int title_width = 480;
    private int title_x = 0;
    private int title_y = 170;

    public MainLayout(Context context) {
        this.activity = (MainActivity) context;
        this.title_x = (int) (0.0f * this.activity.getRatioWidth());
        this.title_y = (int) (170.0f * this.activity.getRatioHeight());
        this.title_width = (int) (480.0f * this.activity.getRatioWidth());
        this.title_height = (int) (-2.0f * this.activity.getRatioHeight());
        new LinearLayout(this.activity);
        ((LinearLayout) this.activity.findViewById(R.id.linearLayout_title)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.title_width, this.title_height, this.title_x, this.title_y));
        this.name_x = (int) (0.0f * this.activity.getRatioWidth());
        this.name_y = (int) (427.0f * this.activity.getRatioHeight());
        this.name_width = (int) (480.0f * this.activity.getRatioWidth());
        this.name_height = (int) (-2.0f * this.activity.getRatioHeight());
        new LinearLayout(this.activity);
        ((LinearLayout) this.activity.findViewById(R.id.linearLayout_name)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.name_width, this.name_height, this.name_x, this.name_y));
        this.buttonGroup_x = (int) (0.0f * this.activity.getRatioWidth());
        this.buttonGroup_y = (int) (597.0f * this.activity.getRatioHeight());
        this.buttonGroup_width = (int) (480.0f * this.activity.getRatioWidth());
        this.buttonGroup_height = (int) (256.0f * this.activity.getRatioHeight());
        new LinearLayout(this.activity);
        ((LinearLayout) this.activity.findViewById(R.id.linearLayout_button)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.buttonGroup_width, this.buttonGroup_height, this.buttonGroup_x, this.buttonGroup_y));
        new Button(this.activity);
        ((Button) this.activity.findViewById(R.id.button_start)).setLayoutParams(new LinearLayout.LayoutParams((int) (((double) this.buttonGroup_width) * 1.0d), (int) (85.4d * ((double) this.activity.getRatioHeight()))));
        new Button(this.activity);
        ((Button) this.activity.findViewById(R.id.button_ranking)).setLayoutParams(new LinearLayout.LayoutParams((int) (((double) this.buttonGroup_width) * 1.0d), (int) (85.4d * ((double) this.activity.getRatioHeight()))));
        new Button(this.activity);
        ((Button) this.activity.findViewById(R.id.button_more)).setLayoutParams(new LinearLayout.LayoutParams((int) (((double) this.buttonGroup_width) * 1.0d), (int) (85.4d * ((double) this.activity.getRatioHeight()))));
        String name = this.activity.getSharedPreferences(PreferenceKeys.PREFERENCE, 3).getString(PreferenceKeys.NAME, Analytics.ID);
        new EditText(this.activity);
        EditText edit = (EditText) this.activity.findViewById(R.id.editText_edit);
        if (!name.equalsIgnoreCase(Analytics.ID)) {
            edit.setText(name);
        }
        if (name.equalsIgnoreCase(Analytics.ID)) {
            edit.setText(DEFOAULT_NAME);
        }
        edit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean decision = false;
                if (actionId != 6) {
                    return false;
                }
                String str = ((SpannableStringBuilder) ((EditText) MainLayout.this.activity.findViewById(R.id.editText_edit)).getText()).toString();
                String oldName = MainLayout.this.activity.getSharedPreferences(PreferenceKeys.PREFERENCE, 3).getString(PreferenceKeys.NAME, Analytics.ID);
                if (str.equalsIgnoreCase(oldName)) {
                    decision = false;
                }
                if (str.equalsIgnoreCase(Analytics.ID)) {
                    return MainLayout.this.nameless();
                }
                if (!str.equalsIgnoreCase(oldName)) {
                    return MainLayout.this.nameCheck();
                }
                return decision;
            }
        });
        final EditText editText = (EditText) this.activity.findViewById(R.id.editText_edit);
        edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean flag) {
                if (!flag) {
                    ((InputMethodManager) MainLayout.this.activity.getSystemService("input_method")).hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                editText.setFocusable(true);
                editText.setFocusableInTouchMode(true);
            }
        });
        this.adlantis_x = (int) (0.0f * this.activity.getRatioWidth());
        this.adlantis_y = (int) (512.0f * this.activity.getRatioHeight());
        this.adlantis_width = (int) (480.0f * this.activity.getRatioWidth());
        this.adlantis_height = (int) (85.0f * this.activity.getRatioHeight());
        new LinearLayout(this.activity);
        ((LinearLayout) this.activity.findViewById(R.id.linearLayout_Adlantis)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.adlantis_width, this.adlantis_height, this.adlantis_x, this.adlantis_y));
    }

    public boolean nameCheck() {
        boolean decision = false;
        final EditText name = (EditText) this.activity.findViewById(R.id.editText_edit);
        final String str = ((SpannableStringBuilder) name.getText()).toString();
        String newName = this.activity.getSharedPreferences(PreferenceKeys.PREFERENCE, 3).getString(PreferenceKeys.NAME, Analytics.ID);
        new AlertDialog.Builder(this.activity).setIcon((int) R.drawable.icon).setTitle("Look!").setMessage("Change name ?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                SharedPreferences.Editor editor = MainLayout.this.activity.getSharedPreferences(PreferenceKeys.PREFERENCE, 0).edit();
                editor.putString(PreferenceKeys.NAME, str);
                editor.commit();
                name.setFocusable(false);
                name.setFocusableInTouchMode(true);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                name.setText(MainLayout.this.activity.getSharedPreferences(PreferenceKeys.PREFERENCE, 3).getString(PreferenceKeys.NAME, Analytics.ID));
                name.setFocusable(false);
                name.setFocusableInTouchMode(true);
            }
        }).show();
        if (newName.equalsIgnoreCase(str)) {
            decision = false;
        }
        if (!newName.equalsIgnoreCase(str)) {
            return true;
        }
        return decision;
    }

    public boolean nameless() {
        new AlertDialog.Builder(this.activity).setIcon((int) R.drawable.icon).setTitle("Look!").setMessage("Input name").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
        return true;
    }
}
