package cross.field.stage;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

public class Graphics extends SurfaceView implements SurfaceHolder.Callback, Runnable {
    public static final float GAME_RATIO_HEGHT = 0.8f;
    public static final float GAME_RATIO_WIDTH = 1.0f;
    public static final float XPERIA_HEIGHT = 854.0f;
    public static final float XPERIA_WIDTH = 480.0f;
    public Canvas canvas;
    private Context context;
    private float disp_height;
    private float disp_width;
    private float game_height;
    private float game_width;
    private SurfaceHolder holder;
    private Drawable[] image;
    private Paint paint = new Paint();
    private float ratio_height;
    private float ratio_width;

    public Graphics(SurfaceHolder holder2, Context context2) {
        super(context2);
        this.context = context2;
        this.holder = holder2;
        this.paint.setAntiAlias(true);
        this.image = new ImageRegister(context2).getImage();
        getDisplaySize();
        getGameSize();
        getDisplayRatio();
    }

    public void getDisplaySize() {
        Display disp = ((WindowManager) this.context.getSystemService("window")).getDefaultDisplay();
        setDispWidth((float) disp.getWidth());
        setDispHeight((float) disp.getHeight());
    }

    public void getGameSize() {
        setGameWidth(getDispWidth() * 1.0f);
        setGameHeight(getDispHeight() * 0.8f);
    }

    public void getDisplayRatio() {
        getDisplaySize();
        setRatioWidth(getDispWidth() / 480.0f);
        setRatioHeight(getDispHeight() / 854.0f);
    }

    public void drawImage(int id, int x, int y, int w, int h) {
        this.image[id].setBounds(x, y, x + w, y + h);
        this.image[id].draw(this.canvas);
    }

    public void drawAlpha(int id, int x, int y, int w, int h, int alpha) {
        this.image[id].setBounds(x, y, x + w, y + h);
        this.image[id].setAlpha(alpha);
        this.image[id].draw(this.canvas);
    }

    public void fade(int alpha) {
        for (Drawable alpha2 : this.image) {
            alpha2.setAlpha(alpha);
        }
    }

    public void drawString(String string, int x, int y, int font, int color) {
        this.paint.setTextSize((float) font);
        this.paint.setColor(color);
        this.canvas.drawText(string, (float) x, (float) y, this.paint);
    }

    public void drawSquare(int x, int y, int w, int h, int color, boolean fill) {
        this.paint.setColor(color);
        this.canvas.drawRect(new RectF((float) x, (float) y, (float) (x + w), (float) (y + h)), this.paint);
        if (fill) {
            this.paint.setStyle(Paint.Style.FILL);
        } else {
            this.paint.setStyle(Paint.Style.STROKE);
        }
    }

    public void drawRoundedSquare(int x, int y, int w, int h, float round, int color, boolean fill) {
        this.paint.setColor(color);
        this.canvas.drawRoundRect(new RectF((float) x, (float) y, (float) (x + w), (float) (y + h)), round, round, this.paint);
        if (fill) {
            this.paint.setStyle(Paint.Style.FILL);
        } else {
            this.paint.setStyle(Paint.Style.STROKE);
        }
    }

    public void lock() {
        this.canvas = this.holder.lockCanvas();
    }

    public void unlock() {
        this.holder.unlockCanvasAndPost(this.canvas);
    }

    public void setcolor(int color) {
        this.paint.setColor(color);
    }

    public void setfontsize(int fontsize) {
        this.paint.setTextSize((float) fontsize);
    }

    public int stringwidth(String string) {
        return (int) this.paint.measureText(string);
    }

    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
    }

    public void surfaceCreated(SurfaceHolder arg0) {
    }

    public void surfaceDestroyed(SurfaceHolder arg0) {
    }

    public void run() {
    }

    public void setDispWidth(float disp_width2) {
        this.disp_width = disp_width2;
    }

    public float getDispWidth() {
        return this.disp_width;
    }

    public void setDispHeight(float disp_height2) {
        this.disp_height = disp_height2;
    }

    public float getDispHeight() {
        return this.disp_height;
    }

    public void setRatioWidth(float ratio_width2) {
        this.ratio_width = ratio_width2;
    }

    public float getRatioWidth() {
        return this.ratio_width;
    }

    public void setRatioHeight(float ratio_height2) {
        this.ratio_height = ratio_height2;
    }

    public float getRatioHeight() {
        return this.ratio_height;
    }

    public void setGameWidth(float game_width2) {
        this.game_width = game_width2;
    }

    public float getGameWidth() {
        return this.game_width;
    }

    public void setGameHeight(float game_height2) {
        this.game_height = game_height2;
    }

    public float getGameHeight() {
        return this.game_height;
    }
}
