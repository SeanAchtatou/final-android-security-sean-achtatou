package cross.field.stage;

import android.content.Context;
import android.view.SurfaceView;
import android.widget.AbsoluteLayout;
import android.widget.LinearLayout;
import cross.field.MeteorBreaker.R;

public class StageLayout {
    public static final int ADLANTIS_HEIGHT = 85;
    public static final int ADLANTIS_WIDTH = 480;
    public static final int ADLANTIS_X = 0;
    public static final int ADLANTIS_Y = 768;
    public static final int VIEW_HEIGHT = 768;
    public static final int VIEW_WIDTH = 480;
    public static final int VIEW_X = 0;
    public static final int VIEW_Y = 0;
    public static final int WRAP_CONTENT = -2;
    private StageActivity activity;
    private int adlantis_height = 85;
    private int adlantis_width = 480;
    private int adlantis_x = 0;
    private int adlantis_y = 768;
    private StageView view;
    private int view_height = 768;
    private int view_width = 480;
    private int view_x = 0;
    private int view_y = 0;

    public StageLayout(Context context) {
        this.activity = (StageActivity) context;
        this.view_x = (int) (this.activity.getRatioWidth() * 0.0f);
        this.view_y = (int) (this.activity.getRatioHeight() * 0.0f);
        this.view_width = (int) (this.activity.getRatioWidth() * 480.0f);
        this.view_height = (int) (this.activity.getRatioHeight() * 768.0f);
        new LinearLayout(this.activity);
        ((LinearLayout) this.activity.findViewById(R.id.lnearLayout_surfaceView)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.view_width, this.view_height, this.view_x, this.view_y));
        setView(new StageView(this.activity, (SurfaceView) this.activity.findViewById(R.id.surfaceView)));
        this.adlantis_x = (int) (this.activity.getRatioWidth() * 0.0f);
        this.adlantis_y = (int) (this.activity.getRatioHeight() * 768.0f);
        this.adlantis_width = (int) (this.activity.getRatioWidth() * 480.0f);
        this.adlantis_height = (int) (85.0f * this.activity.getRatioHeight());
        new LinearLayout(this.activity);
        ((LinearLayout) this.activity.findViewById(R.id.linearLayout_Adlantis)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.adlantis_width, this.adlantis_height, this.adlantis_x, this.adlantis_y));
    }

    public void setView(StageView view2) {
        this.view = view2;
    }

    public StageView getView() {
        return this.view;
    }
}
