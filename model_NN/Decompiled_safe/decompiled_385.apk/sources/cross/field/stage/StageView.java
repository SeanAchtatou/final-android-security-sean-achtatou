package cross.field.stage;

import android.content.Context;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.games.shootObject.Button01;
import com.games.shootObject.Enemy;
import com.games.shootObject.Field01;
import com.games.shootObject.Panel01;
import com.games.shootObject.Player;
import com.games.shootObject.PlayerBullet01;
import java.util.ArrayList;
import java.util.Iterator;

public class StageView extends SurfaceView implements SurfaceHolder.Callback, Runnable {
    public static final int END = 4;
    public static final int INIT = 0;
    public static final int PLAY = 1;
    public static final int RECORD = 3;
    public static final float SCROLL_SPEED = 15.0f;
    private static ArrayList<Enemy> enemys;
    private static ArrayList<PlayerBullet01> playerBullets01;
    private StageActivity activity;
    private float adh;
    private Paint bgPaint = new Paint();
    private Button01 button01;
    private int count = 0;
    private boolean down;
    private boolean end_flag;
    private Paint foreground;
    private Graphics g;
    private SurfaceHolder holder;
    private Iterator it;
    private Iterator it2;
    private long last_score = 0;
    private boolean left;
    private Field01 main_field;
    private int nbSt = 0;
    private Panel01 panel01;
    private Player player;
    private boolean right;
    private int scene;
    private ArrayList<Long> score;
    private Thread thread;
    private boolean up;

    public StageView(Context context, SurfaceView sv) {
        super(context);
        this.holder = sv.getHolder();
        this.holder.addCallback(this);
        this.holder.setFixedSize(getWidth(), getHeight());
        setFocusable(true);
        setClickable(true);
        this.g = new Graphics(this.holder, context);
        this.activity = (StageActivity) context;
        enemys = new ArrayList<>();
        playerBullets01 = new ArrayList<>();
        this.adh = this.g.getDispHeight() / 10.0f;
        this.foreground = new Paint(1);
        this.foreground.setTextAlign(Paint.Align.CENTER);
        init();
    }

    public void init() {
        this.main_field = new Field01(this.g.getDispWidth() / 2.0f, (this.g.getDispHeight() - this.adh) / 2.0f, this.g.getDispWidth(), this.g.getDispHeight() - this.adh);
        this.player = new Player(this.main_field.getImagex() + this.main_field.getWidthPer(2), this.main_field.getImagey() + (this.main_field.getHeightPer(5) * 4.0f), this.main_field.getWidthPer(10), this.main_field.getWidthPer(9));
        this.panel01 = new Panel01(this.main_field.getx(), this.main_field.getImagey() + (this.main_field.getHeightPer(18) / 2.0f), this.main_field.getw(), this.main_field.getHeightPer(18));
        this.button01 = new Button01(this.main_field.getx(), this.main_field.getImageh() - ((this.main_field.getHeightPer(10) - 10.0f) / 2.0f), this.main_field.getw(), this.main_field.getHeightPer(10) - 10.0f);
    }

    public void run() {
        while (this.thread != null) {
            draw();
            update();
            scene();
            this.count++;
            try {
                Thread.sleep(15);
            } catch (Exception e) {
            }
        }
    }

    public void update() {
        this.it = this.main_field.getEnemys().iterator();
        while (this.it.hasNext()) {
            if (this.player.isHit((Enemy) this.it.next())) {
                this.last_score = (long) this.nbSt;
                if (this.scene != 4) {
                    this.scene = 3;
                }
                this.player.aaa = false;
                this.left = false;
                this.right = false;
                this.down = false;
                this.up = false;
            }
        }
        this.it = this.main_field.getEnemys2().iterator();
        while (this.it.hasNext()) {
            if (this.player.isHit((Enemy) this.it.next())) {
                this.last_score = (long) this.nbSt;
                if (this.scene != 4) {
                    this.scene = 3;
                }
                this.player.aaa = false;
                this.left = false;
                this.right = false;
                this.down = false;
                this.up = false;
            }
        }
        this.main_field.setEnemys(this.count / 10);
        this.main_field.update(this.count);
        this.player.update(this.up, this.down, this.right, this.left, this.main_field, this.count);
        this.main_field.enemyMove(this.count / 10);
        this.it = this.player.getBullets01().iterator();
        while (this.it.hasNext()) {
            PlayerBullet01 b = (PlayerBullet01) this.it.next();
            boolean hit = false;
            this.it2 = this.main_field.getEnemys2().iterator();
            while (this.it2.hasNext()) {
                Enemy e = (Enemy) this.it2.next();
                if (b.isHit(e)) {
                    this.main_field.hitAction2(e, this.count);
                    this.it2.remove();
                    this.nbSt++;
                    hit = true;
                }
            }
            this.it2 = this.main_field.getEnemys().iterator();
            while (this.it2.hasNext()) {
                Enemy e2 = (Enemy) this.it2.next();
                if (b.isHit(e2)) {
                    this.main_field.hitAction(e2, this.count);
                    this.it2.remove();
                    this.nbSt++;
                    hit = true;
                }
            }
            if (hit) {
                this.it.remove();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, float, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    public void draw() {
        this.g.lock();
        this.bgPaint.setShader(new LinearGradient(0.0f, 0.0f, (float) this.g.getWidth(), (float) this.g.getHeight(), -1, -7829368, Shader.TileMode.CLAMP));
        this.g.canvas.drawPaint(this.bgPaint);
        this.main_field.draw(this.g, 0);
        this.player.draw(this.g, 1);
        this.main_field.drawEnemy(this.g, 2);
        this.panel01.draw(this.g, this.foreground, this.nbSt, this.end_flag);
        this.button01.draw(this.g, this.foreground, this.end_flag);
        this.g.unlock();
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0135  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x021e A[LOOP:1: B:13:0x00c8->B:33:0x021e, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void scene() {
        /*
            r21 = this;
            r0 = r21
            int r0 = r0.scene
            r16 = r0
            switch(r16) {
                case 3: goto L_0x000a;
                case 4: goto L_0x024f;
                default: goto L_0x0009;
            }
        L_0x0009:
            return
        L_0x000a:
            r0 = r21
            cross.field.stage.StageActivity r0 = r0.activity
            r16 = r0
            java.lang.String r17 = "Preference"
            r18 = 3
            android.content.SharedPreferences r11 = r16.getSharedPreferences(r17, r18)
            android.content.SharedPreferences$Editor r9 = r11.edit()
            java.util.ArrayList r16 = new java.util.ArrayList
            r16.<init>()
            r0 = r16
            r1 = r21
            r1.score = r0
            r10 = 0
        L_0x0028:
            r16 = 100
            r0 = r10
            r1 = r16
            if (r0 < r1) goto L_0x00e4
        L_0x002f:
            r0 = r21
            java.util.ArrayList<java.lang.Long> r0 = r0.score
            r16 = r0
            r0 = r21
            long r0 = r0.last_score
            r17 = r0
            java.lang.Long r17 = java.lang.Long.valueOf(r17)
            r16.add(r17)
            r0 = r21
            java.util.ArrayList<java.lang.Long> r0 = r0.score
            r16 = r0
            java.util.Collections.sort(r16)
            r0 = r21
            java.util.ArrayList<java.lang.Long> r0 = r0.score
            r16 = r0
            java.util.Collections.reverse(r16)
            r4 = 0
            java.lang.String r16 = "Score0"
            r17 = -1
            r0 = r11
            r1 = r16
            r2 = r17
            long r16 = r0.getLong(r1, r2)     // Catch:{ Exception -> 0x025a }
            r0 = r16
            int r0 = (int) r0
            r4 = r0
        L_0x0066:
            java.lang.String r16 = "Best.Flag"
            r17 = 0
            r0 = r9
            r1 = r16
            r2 = r17
            r0.putBoolean(r1, r2)
            java.util.Date r6 = new java.util.Date
            r6.<init>()
            long r16 = r6.getTime()
            r18 = 86400000(0x5265c00, double:4.2687272E-316)
            long r16 = r16 / r18
            r0 = r16
            double r0 = (double) r0
            r16 = r0
            double r16 = java.lang.Math.floor(r16)
            r0 = r16
            long r0 = (long) r0
            r7 = r0
            r0 = r21
            long r0 = r0.last_score
            r16 = r0
            r0 = r16
            int r0 = (int) r0
            r16 = r0
            r0 = r4
            r1 = r16
            if (r0 >= r1) goto L_0x0135
            java.lang.String r16 = "Daily.Record"
            r0 = r21
            long r0 = r0.last_score
            r17 = r0
            r0 = r17
            int r0 = (int) r0
            r17 = r0
            r0 = r9
            r1 = r16
            r2 = r17
            r0.putInt(r1, r2)
            java.lang.String r16 = "Daily.Time"
            r0 = r9
            r1 = r16
            r2 = r7
            r0.putLong(r1, r2)
            java.lang.String r16 = "Best.Flag"
            r17 = 1
            r0 = r9
            r1 = r16
            r2 = r17
            r0.putBoolean(r1, r2)
        L_0x00c7:
            r10 = 0
        L_0x00c8:
            r0 = r21
            java.util.ArrayList<java.lang.Long> r0 = r0.score
            r16 = r0
            int r16 = r16.size()
            r0 = r10
            r1 = r16
            if (r0 < r1) goto L_0x021e
            r9.commit()
            r16 = 4
            r0 = r16
            r1 = r21
            r1.scene = r0
            goto L_0x0009
        L_0x00e4:
            r16 = -1
            java.lang.StringBuilder r18 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x025d }
            java.lang.String r19 = "Score"
            r18.<init>(r19)     // Catch:{ Exception -> 0x025d }
            r0 = r18
            r1 = r10
            java.lang.StringBuilder r18 = r0.append(r1)     // Catch:{ Exception -> 0x025d }
            java.lang.String r18 = r18.toString()     // Catch:{ Exception -> 0x025d }
            r19 = -1
            r0 = r11
            r1 = r18
            r2 = r19
            long r18 = r0.getLong(r1, r2)     // Catch:{ Exception -> 0x025d }
            int r16 = (r16 > r18 ? 1 : (r16 == r18 ? 0 : -1))
            if (r16 == 0) goto L_0x002f
            java.lang.StringBuilder r16 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x025d }
            java.lang.String r17 = "Score"
            r16.<init>(r17)     // Catch:{ Exception -> 0x025d }
            r0 = r16
            r1 = r10
            java.lang.StringBuilder r16 = r0.append(r1)     // Catch:{ Exception -> 0x025d }
            java.lang.String r16 = r16.toString()     // Catch:{ Exception -> 0x025d }
            r17 = -1
            r0 = r11
            r1 = r16
            r2 = r17
            long r12 = r0.getLong(r1, r2)     // Catch:{ Exception -> 0x025d }
            r0 = r21
            java.util.ArrayList<java.lang.Long> r0 = r0.score     // Catch:{ Exception -> 0x025d }
            r16 = r0
            java.lang.Long r17 = java.lang.Long.valueOf(r12)     // Catch:{ Exception -> 0x025d }
            r16.add(r17)     // Catch:{ Exception -> 0x025d }
        L_0x0131:
            int r10 = r10 + 1
            goto L_0x0028
        L_0x0135:
            java.lang.String r16 = "Daily.Time"
            r17 = -1
            r0 = r11
            r1 = r16
            r2 = r17
            long r14 = r0.getLong(r1, r2)
            java.lang.String r16 = "Daily.Time"
            r17 = -1
            r0 = r11
            r1 = r16
            r2 = r17
            long r14 = r0.getLong(r1, r2)
            java.lang.String r16 = "Daily.Flag"
            r17 = 0
            r0 = r9
            r1 = r16
            r2 = r17
            r0.putBoolean(r1, r2)
            r16 = -1
            int r16 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r16 != 0) goto L_0x01a3
            java.lang.String r16 = "Daily.Flag"
            r17 = 1
            r0 = r9
            r1 = r16
            r2 = r17
            r0.putBoolean(r1, r2)
            java.lang.String r16 = "Daily.Record"
            r0 = r21
            long r0 = r0.last_score
            r17 = r0
            r0 = r17
            int r0 = (int) r0
            r17 = r0
            r0 = r9
            r1 = r16
            r2 = r17
            r0.putInt(r1, r2)
            long r16 = r6.getTime()
            r18 = 86400000(0x5265c00, double:4.2687272E-316)
            long r16 = r16 / r18
            r0 = r16
            double r0 = (double) r0
            r16 = r0
            double r16 = java.lang.Math.floor(r16)
            r0 = r16
            long r0 = (long) r0
            r14 = r0
            java.lang.String r16 = "Daily.Time"
            r0 = r9
            r1 = r16
            r2 = r14
            r0.putLong(r1, r2)
            goto L_0x00c7
        L_0x01a3:
            int r16 = (r14 > r7 ? 1 : (r14 == r7 ? 0 : -1))
            if (r16 >= 0) goto L_0x01d1
            java.lang.String r16 = "Daily.Time"
            r0 = r9
            r1 = r16
            r2 = r7
            r0.putLong(r1, r2)
            java.lang.String r16 = "Daily.Record"
            r0 = r21
            long r0 = r0.last_score
            r17 = r0
            r0 = r17
            int r0 = (int) r0
            r17 = r0
            r0 = r9
            r1 = r16
            r2 = r17
            r0.putInt(r1, r2)
            java.lang.String r16 = "Daily.Flag"
            r17 = 1
            r0 = r9
            r1 = r16
            r2 = r17
            r0.putBoolean(r1, r2)
        L_0x01d1:
            int r16 = (r14 > r7 ? 1 : (r14 == r7 ? 0 : -1))
            if (r16 != 0) goto L_0x00c7
            java.lang.String r16 = "Daily.Record"
            r0 = r21
            long r0 = r0.last_score
            r17 = r0
            r0 = r17
            int r0 = (int) r0
            r17 = r0
            r0 = r11
            r1 = r16
            r2 = r17
            int r5 = r0.getInt(r1, r2)
            r0 = r21
            long r0 = r0.last_score
            r16 = r0
            r0 = r16
            int r0 = (int) r0
            r16 = r0
            r0 = r5
            r1 = r16
            if (r0 >= r1) goto L_0x00c7
            java.lang.String r16 = "Daily.Record"
            r0 = r21
            long r0 = r0.last_score
            r17 = r0
            r0 = r17
            int r0 = (int) r0
            r17 = r0
            r0 = r9
            r1 = r16
            r2 = r17
            r0.putInt(r1, r2)
            java.lang.String r16 = "Daily.Flag"
            r17 = 1
            r0 = r9
            r1 = r16
            r2 = r17
            r0.putBoolean(r1, r2)
            goto L_0x00c7
        L_0x021e:
            java.lang.StringBuilder r16 = new java.lang.StringBuilder
            java.lang.String r17 = "Score"
            r16.<init>(r17)
            r0 = r16
            r1 = r10
            java.lang.StringBuilder r16 = r0.append(r1)
            java.lang.String r16 = r16.toString()
            r0 = r21
            java.util.ArrayList<java.lang.Long> r0 = r0.score
            r17 = r0
            r0 = r17
            r1 = r10
            java.lang.Object r4 = r0.get(r1)
            java.lang.Long r4 = (java.lang.Long) r4
            long r17 = r4.longValue()
            r0 = r9
            r1 = r16
            r2 = r17
            r0.putLong(r1, r2)
            int r10 = r10 + 1
            goto L_0x00c8
        L_0x024f:
            r0 = r21
            cross.field.stage.StageActivity r0 = r0.activity
            r16 = r0
            r16.finish()
            goto L_0x0009
        L_0x025a:
            r16 = move-exception
            goto L_0x0066
        L_0x025d:
            r16 = move-exception
            goto L_0x0131
        */
        throw new UnsupportedOperationException("Method not decompiled: cross.field.stage.StageView.scene():void");
    }

    public boolean touchEvent(MotionEvent event) {
        if (this.end_flag) {
            if (this.end_flag) {
                this.left = false;
                this.right = false;
                this.down = false;
                this.up = false;
                switch (event.getAction()) {
                    case 0:
                        if (event.getX() <= this.main_field.getImagex() + this.main_field.getWidthPer(2) || this.button01.getImagey() >= event.getY() || event.getY() >= this.button01.getImageh()) {
                            if (event.getX() <= this.main_field.getImagex() + this.main_field.getWidthPer(2) && this.button01.getImagey() < event.getY() && event.getY() < this.button01.getImageh()) {
                                this.count = 0;
                                this.nbSt = 0;
                                this.end_flag = false;
                                init();
                                break;
                            }
                        } else {
                            this.activity.finish();
                            break;
                        }
                        break;
                }
            }
        } else {
            switch (event.getAction()) {
                case 0:
                    if (event.getX() <= this.main_field.getImagex() + this.main_field.getWidthPer(2) || this.button01.getImagey() >= event.getY() || event.getY() >= this.button01.getImageh()) {
                        if (event.getX() <= this.main_field.getImagex() + this.main_field.getWidthPer(2) && this.button01.getImagey() < event.getY() && event.getY() < this.button01.getImageh()) {
                            this.left = true;
                            this.right = false;
                            break;
                        }
                    } else {
                        this.right = true;
                        this.left = false;
                        break;
                    }
                    break;
                case 1:
                    this.left = false;
                    this.right = false;
                    this.down = false;
                    this.up = false;
                    break;
                case 2:
                    if (this.button01.getImagey() < event.getY() && event.getY() < this.button01.getImageh()) {
                        if (event.getX() <= this.main_field.getImagex() + this.main_field.getWidthPer(2)) {
                            if (event.getX() <= this.main_field.getImagex() + this.main_field.getWidthPer(2)) {
                                this.left = true;
                                this.right = false;
                                break;
                            }
                        } else {
                            this.right = true;
                            this.left = false;
                            break;
                        }
                    } else {
                        this.left = false;
                        this.right = false;
                        break;
                    }
                    break;
            }
        }
        return false;
    }

    public void surfaceDestroyed(SurfaceHolder arg0) {
        this.thread = null;
    }

    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
    }

    public void surfaceCreated(SurfaceHolder arg0) {
        this.thread = new Thread(this);
        this.thread.start();
    }
}
