package cross.field.stage;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.SurfaceView;
import cross.field.MeteorBreaker.R;

public class ImageRegister extends SurfaceView {
    public static final int BG_MAIN = 0;
    public static final int BLOCK = 5;
    public static final int BUTTON = 6;
    public static final int BUTTON_BACK = 7;
    public static final int LOGO_0 = 10;
    public static final int LOGO_1 = 11;
    public static final int LOGO_2 = 12;
    public static final int LOGO_3 = 13;
    public static final int LOGO_4 = 14;
    public static final int LOGO_5 = 15;
    public static final int LOGO_6 = 16;
    public static final int LOGO_7 = 17;
    public static final int LOGO_8 = 18;
    public static final int LOGO_9 = 19;
    public static final int LOGO_JUMP = 8;
    public static final int LOGO_PUSH = 9;
    public static final int MAX = 20;
    public static final int PLAYER_DOWN = 2;
    public static final int PLAYER_RUN00 = 3;
    public static final int PLAYER_RUN01 = 4;
    public static final int PLAYER_UP = 1;

    public ImageRegister(Context context) {
        super(context);
    }

    public Drawable[] getImage() {
        Resources r = getResources();
        return new Drawable[]{r.getDrawable(R.drawable.bg), r.getDrawable(R.drawable.fighter), r.getDrawable(R.drawable.stone), r.getDrawable(R.drawable.laser), r.getDrawable(R.drawable.explosion), r.getDrawable(R.drawable.shot), r.getDrawable(R.drawable.button), r.getDrawable(R.drawable.button_back), r.getDrawable(R.drawable.logo_jump), r.getDrawable(R.drawable.logo_push), r.getDrawable(R.drawable.logo_0), r.getDrawable(R.drawable.logo_1), r.getDrawable(R.drawable.logo_2), r.getDrawable(R.drawable.logo_3), r.getDrawable(R.drawable.logo_4), r.getDrawable(R.drawable.logo_5), r.getDrawable(R.drawable.logo_6), r.getDrawable(R.drawable.logo_7), r.getDrawable(R.drawable.logo_8), r.getDrawable(R.drawable.logo_9)};
    }

    public void setImage() {
    }
}
