package cross.field.stage;

import android.view.MotionEvent;
import java.util.ArrayList;
import java.util.Iterator;

public class PlayerManager {
    public static final float ADVANCE_SPEED = 1.0f;
    public static final int ANM_SPEED = 2;
    public static final float FALL_SPEED = 8.0f;
    public static final int INIT = 0;
    public static final int JUMP = 1;
    public static final float JUMP_SPEED = -55.0f;
    public static final float PLAYER_H = 80.0f;
    public static final float PLAYER_W = 80.0f;
    public static final float PLAYER_X = 160.0f;
    public static final float PLAYER_Y = 0.0f;
    public static final int RUN = 2;
    private int anm_speed = 0;
    private Graphics g;
    private Player player;
    private float player_h = 0.0f;
    private float player_w = 0.0f;
    private float player_x = 0.0f;
    private float player_y = 0.0f;

    public PlayerManager(Graphics g2) {
        this.g = g2;
        init();
    }

    public void init() {
        this.player_x = 160.0f * this.g.getRatioWidth();
        this.player_y = 0.0f * this.g.getRatioHeight();
        this.player_w = this.g.getRatioWidth() * 80.0f;
        this.player_h = this.g.getRatioHeight() * 80.0f;
        this.player = new Player(this.g, 2, this.player_x, this.player_y, this.player_w, this.player_h);
        this.player.setMode(0);
    }

    public void action() {
        fall();
        advance();
        switch (this.player.getMode()) {
            case 2:
                run();
                break;
        }
        roof();
    }

    public void draw() {
        animation();
        this.player.draw();
    }

    public void animation() {
        switch (this.player.getMode()) {
            case 0:
            default:
                return;
            case 1:
                if (this.player.getYSpeed() > 0.0f) {
                    this.player.setId(2);
                    return;
                } else if (this.player.getYSpeed() < 0.0f) {
                    this.player.setId(1);
                    return;
                } else {
                    return;
                }
            case 2:
                this.anm_speed++;
                if (this.anm_speed >= 2) {
                    this.anm_speed = 0;
                    if (this.player.getId() == 3) {
                        this.player.setId(4);
                        return;
                    } else if (this.player.getId() == 4) {
                        this.player.setId(3);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
        }
    }

    public void fall() {
        this.player.setYSpeed(this.player.getYSpeed() + (8.0f * this.g.getRatioHeight()));
        this.player.setY(this.player.getY() + this.player.getYSpeed());
    }

    public void jump() {
        this.player.setYSpeed(-55.0f * this.g.getRatioHeight());
    }

    public void run() {
    }

    public void roof() {
        if (((double) this.player.getY()) + (((double) this.player.getH()) * 0.3d) <= 0.0d) {
            this.player.setY((float) (((double) this.player.getH()) * -0.3d));
        }
    }

    public void advance() {
        this.player.setX(this.player.getX() + (1.0f * this.g.getRatioWidth()));
        if (this.player.getX() >= (this.g.getRatioWidth() * 160.0f) - this.player.getW()) {
            this.player.setX((this.g.getRatioWidth() * 160.0f) - this.player.getW());
        }
    }

    public void hitPB(ArrayList<BaseObject> block) {
        Iterator<BaseObject> it = block.iterator();
        while (it.hasNext()) {
            BaseObject b = it.next();
            if (b.getX() <= this.player.getX() + this.player.getW()) {
                float top1 = this.player.getY() + ((float) (((double) this.player.getH()) * 0.3d));
                float bottom1 = this.player.getY() + this.player.getH();
                float left1 = this.player.getX() + ((float) (((double) this.player.getW()) * 0.5d));
                float right1 = this.player.getX() + this.player.getW();
                float top2 = b.getY();
                float bottom2 = top2 + b.getH();
                float left2 = b.getX();
                if (left1 > left2 + b.getW() || right1 < left2 || bottom1 < top2 || top1 > bottom2) {
                    if (this.player.getMode() == 2 && this.player.getYSpeed() >= 8.0f * this.g.getRatioHeight()) {
                        this.player.setMode(1);
                    }
                } else if (b.getY() >= (this.player.getY() - this.player.getYSpeed()) + this.player.getH() && this.player.getYSpeed() >= 0.0f) {
                    this.player.setMode(2);
                    if (!(this.player.getId() == 3 || this.player.getId() == 4)) {
                        this.player.setId(3);
                    }
                    this.player.setYSpeed(0.0f);
                    this.player.setY(b.getY() - this.player.getH());
                } else if (b.getY() + b.getH() > top1 - this.player.getYSpeed() || this.player.getYSpeed() > 0.0f) {
                    this.player.setX(b.getX() - this.player.getW());
                } else {
                    this.player.setY((b.getY() + b.getH()) - ((float) (((double) this.player.getH()) * 0.3d)));
                }
            }
        }
    }

    public boolean touchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                jump();
                this.player.setMode(1);
                break;
        }
        return false;
    }

    public void setG(Graphics g2) {
        this.g = g2;
    }

    public Graphics getG() {
        return this.g;
    }

    public void setPlayer(Player player2) {
        this.player = player2;
    }

    public Player getPlayer() {
        return this.player;
    }

    public void setAnmSpeed(int anm_speed2) {
        this.anm_speed = anm_speed2;
    }

    public int getAnmSpeed() {
        return this.anm_speed;
    }
}
