package cross.field.stage;

public class Player extends BaseObject {
    public Player(Graphics g, int id, float x, float y, float w, float h) {
        super(g, id, x, y, w, h);
    }

    public Player(Graphics g, int id, float x, float y, float w, float h, float xoff, float yoff) {
        super(g, id, x, y, w, h, xoff, yoff);
    }

    public Player(Graphics g, int id, float x, float y, float w, float h, float xoff, float yoff, float xhit, float yhit) {
        super(g, id, x, y, w, h, xoff, yoff, xhit, yhit);
    }
}
