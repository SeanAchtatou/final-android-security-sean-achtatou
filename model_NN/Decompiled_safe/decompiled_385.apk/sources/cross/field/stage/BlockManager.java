package cross.field.stage;

import android.view.MotionEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class BlockManager {
    public static final float BLOCK_H = 0.0f;
    public static final float BLOCK_H_MAX = 563.2f;
    public static final float BLOCK_H_MIN = 213.5f;
    public static final float BLOCK_SPACE_MAX = 360.0f;
    public static final float BLOCK_SPACE_MIN = 80.0f;
    public static final float BLOCK_W = 50.0f;
    public static final float BLOCK_X = 0.0f;
    public static final float BLOCK_Y = 0.0f;
    public static final int BOTTOM = 1;
    public static final int DELETE = 3;
    public static final int DOUBLE = 3;
    public static final int INIT = 0;
    public static final int INNER = 2;
    public static final float LOGO_H = 30.0f;
    public static final float LOGO_W = 300.0f;
    public static final float LOGO_X = 0.0f;
    public static final float LOGO_Y = 0.0f;
    public static final int SCROLL = 2;
    public static final float SCROLL_SPEED = -20.0f;
    public static final int SET = 1;
    public static final int SET_MODE = 4;
    public static final int TOP = 0;
    private ArrayList<BaseObject> block;
    private Graphics g;
    private int mode;
    private Random random;
    private float scroll_speed = 0.0f;
    private float space = 0.0f;

    public BlockManager(Graphics g2) {
        this.g = g2;
        init();
    }

    public void init() {
        this.mode = 0;
        this.block = new ArrayList<>();
        createBlock();
        float set_x = (float) (((double) this.g.getGameWidth()) * 0.5d);
        float set_y = (float) (((double) this.g.getGameHeight()) * 0.75d);
        float set_w = (float) (((double) this.g.getGameWidth()) * 1.25d);
        float set_h = (float) (((double) this.g.getGameHeight()) * 0.5d);
        this.block.get(this.block.size() - 1).setX(set_x);
        this.block.get(this.block.size() - 1).setY(set_y);
        this.block.get(this.block.size() - 1).setW(set_w);
        this.block.get(this.block.size() - 1).setH(set_h);
        this.block.get(this.block.size() - 1).setMode(2);
        this.random = new Random();
        this.scroll_speed = -20.0f * this.g.getRatioWidth();
    }

    public void action() {
        if (this.mode == 0) {
            Iterator<BaseObject> it = this.block.iterator();
            while (it.hasNext()) {
                BaseObject b = it.next();
                switch (b.getMode()) {
                    case 0:
                        b.setMode(1);
                        break;
                    case 1:
                        b.setMode(2);
                        break;
                    case 2:
                        b.setX(b.getX() + this.scroll_speed);
                        if (b.getX() + b.getW() >= 0.0f) {
                            break;
                        } else {
                            b.setMode(3);
                            break;
                        }
                    case 3:
                        it.remove();
                        this.mode = 1;
                        break;
                }
            }
            return;
        }
        this.space += this.scroll_speed;
        if (this.space < (-(50.0f * this.g.getRatioWidth()))) {
            this.space = (((float) this.random.nextInt(280)) + 80.0f) * this.g.getRatioWidth();
            createBlock();
            switch (this.random.nextInt(4)) {
                case 0:
                    model(this.block.get(this.block.size() - 1), 0);
                    break;
                case 1:
                    model(this.block.get(this.block.size() - 1), 1);
                    break;
                case 2:
                    model(this.block.get(this.block.size() - 1), 2);
                    break;
                case 3:
                    createBlock();
                    model(this.block.get(this.block.size() - 1), this.block.get(this.block.size() - 2), 3);
                    break;
            }
        }
        Iterator<BaseObject> it2 = this.block.iterator();
        while (it2.hasNext()) {
            BaseObject b2 = it2.next();
            switch (b2.getMode()) {
                case 0:
                    b2.setMode(1);
                    break;
                case 1:
                    b2.setMode(2);
                    break;
                case 2:
                    b2.setX(b2.getX() + this.scroll_speed);
                    if (b2.getX() + b2.getW() >= 0.0f) {
                        break;
                    } else {
                        b2.setMode(3);
                        break;
                    }
                case 3:
                    it2.remove();
                    break;
            }
        }
    }

    public void draw() {
        if (this.mode == 0) {
            Iterator<BaseObject> it = this.block.iterator();
            while (it.hasNext()) {
                BaseObject b = it.next();
                b.draw();
                if (((double) b.getX()) + (((double) b.getW()) * 0.25d) < 0.0d) {
                    this.g.drawImage(9, (int) (((double) this.g.getGameWidth()) * 0.3d), (int) (((double) this.g.getGameHeight()) * 0.55d), (int) (300.0f * this.g.getRatioWidth()), (int) (30.0f * this.g.getRatioHeight()));
                }
            }
            return;
        }
        Iterator<BaseObject> it2 = this.block.iterator();
        while (it2.hasNext()) {
            it2.next().draw();
        }
    }

    public void createBlock() {
        this.block.add(new BaseObject(this.g, 5, this.g.getGameWidth(), 0.0f, 50.0f * this.g.getRatioHeight(), this.g.getRatioHeight() * 0.0f));
    }

    public void deleteBlock() {
        Iterator<BaseObject> it = this.block.iterator();
        while (it.hasNext()) {
            BaseObject b = it.next();
            if (b.getX() + b.getW() <= 0.0f) {
                it.remove();
            }
        }
    }

    public void model(BaseObject block2, int mode2) {
        float set_x = this.g.getGameWidth();
        float set_y = 0.0f;
        float set_w = 50.0f * this.g.getRatioWidth();
        float set_h = 0.0f;
        switch (mode2) {
            case 0:
                set_y = 0.0f;
                set_h = (((float) this.random.nextInt(349)) + 213.5f) * this.g.getRatioHeight();
                break;
            case 1:
                set_h = (((float) this.random.nextInt(349)) + 213.5f) * this.g.getRatioHeight();
                set_y = this.g.getGameHeight() - set_h;
                break;
            case 2:
                set_h = (((float) this.random.nextInt(269)) + 213.5f) * this.g.getRatioHeight();
                set_y = ((float) this.random.nextInt((int) (this.g.getGameHeight() - (160.0f * this.g.getRatioHeight())))) + (80.0f * this.g.getRatioHeight());
                break;
        }
        block2.setX(set_x);
        block2.setY(set_y);
        block2.setW(set_w);
        block2.setH(set_h);
        block2.setMode(2);
    }

    public void model(BaseObject block1, BaseObject block2, int mode2) {
        float set_x1 = this.g.getGameWidth();
        float set_y1 = 0.0f;
        float set_w1 = 50.0f * this.g.getRatioWidth();
        float set_h1 = 0.0f;
        float set_x2 = this.g.getGameWidth();
        float set_y2 = 0.0f;
        float set_w2 = 50.0f * this.g.getRatioWidth();
        float set_h2 = 0.0f;
        switch (mode2) {
            case 3:
                set_y1 = 0.0f;
                set_h1 = (float) this.random.nextInt((int) (this.g.getRatioHeight() * 563.2f));
                set_h2 = (float) this.random.nextInt((int) ((this.g.getRatioHeight() * 563.2f) - set_h1));
                set_y2 = this.g.getGameHeight() - set_h2;
                break;
        }
        block1.setX(set_x1);
        block1.setY(set_y1);
        block1.setW(set_w1);
        block1.setH(set_h1);
        block1.setMode(2);
        block2.setX(set_x2);
        block2.setY(set_y2);
        block2.setW(set_w2);
        block2.setH(set_h2);
        block2.setMode(2);
    }

    public boolean touchEvent(MotionEvent event) {
        switch (event.getAction()) {
        }
        return false;
    }

    public void setBlock(ArrayList<BaseObject> block2) {
        this.block = block2;
    }

    public ArrayList<BaseObject> getBlock() {
        return this.block;
    }

    public void setG(Graphics g2) {
        this.g = g2;
    }

    public Graphics getG() {
        return this.g;
    }

    public void setMode(int mode2) {
        this.mode = mode2;
    }

    public int getMode() {
        return this.mode;
    }

    public void setSpace(float space2) {
        this.space = space2;
    }

    public float getSpace() {
        return this.space;
    }

    public void setScrollSpeed(float scroll_speed2) {
        this.scroll_speed = scroll_speed2;
    }

    public float getScrollSpeed() {
        return this.scroll_speed;
    }
}
