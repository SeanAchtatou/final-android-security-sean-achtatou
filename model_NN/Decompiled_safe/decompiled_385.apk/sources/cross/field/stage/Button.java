package cross.field.stage;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;

public class Button extends View {
    public static final int ALPHA_MAX = 255;
    public static final int TRANSMITTANCE = 127;
    public int alpha = ALPHA_MAX;
    private Graphics g;
    private int h;
    private int hs;
    private int id;
    public boolean on = false;
    public int onEvent;
    private int string_h;
    private int string_w;
    private int string_x;
    private int string_y;
    private int w;
    private int ws;
    private int x;
    private int y;

    public int getY() {
        return this.y;
    }

    public void setY(int y2) {
        this.y = y2;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public Button(Context context, Graphics g2, int id2, int x2, int y2, int w2, int h2, int ws2, int hs2) {
        super(context);
        this.g = g2;
        this.x = x2;
        this.y = y2;
        this.w = w2;
        this.h = h2;
        this.id = id2;
        resizeButton(w2, ws2, h2, hs2);
    }

    public void action() {
    }

    public void draw() {
        this.g.drawImage(7, this.x, this.y, this.w, this.h);
        this.g.drawAlpha(6, this.x, this.y, this.w, this.h, this.alpha);
        if (this.id != -1) {
            this.string_x = this.x + this.ws;
            this.string_y = this.y + this.hs;
            this.string_w = this.w - (this.ws * 2);
            this.string_h = this.h - (this.hs * 2);
            this.g.drawAlpha(this.id, this.string_x, this.string_y, this.string_w, this.string_h, this.alpha);
        }
        this.on = false;
    }

    public MotionEvent buttonEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                if (!onButton((int) event.getX(), (int) event.getY())) {
                    return null;
                }
                this.alpha = TRANSMITTANCE;
                return event;
            case 1:
                if (!onButton((int) event.getX(), (int) event.getY())) {
                    return null;
                }
                this.alpha = ALPHA_MAX;
                return event;
            case 2:
                if (onButton((int) event.getX(), (int) event.getY())) {
                    this.alpha = TRANSMITTANCE;
                    return event;
                }
                this.alpha = ALPHA_MAX;
                return null;
            case 3:
                return event;
            default:
                this.alpha = ALPHA_MAX;
                return null;
        }
    }

    public boolean onButton(int x2, int y2) {
        int xt = x2;
        int yt = y2;
        int top = this.y;
        int bottom = this.y + this.h;
        int left = this.x;
        int right = this.x + this.w;
        if (yt < top || yt > bottom || xt < left || xt > right) {
            return false;
        }
        return true;
    }

    public void resizeButton(int w2, int ws2, int h2, int hs2) {
        this.w = w2;
        this.h = h2;
        this.ws = ws2;
        this.hs = hs2;
        this.string_x = this.x + ws2;
        this.string_y = this.y + hs2;
        this.string_w = this.w - (ws2 * 2);
        this.string_h = this.h - (hs2 * 2);
    }
}
