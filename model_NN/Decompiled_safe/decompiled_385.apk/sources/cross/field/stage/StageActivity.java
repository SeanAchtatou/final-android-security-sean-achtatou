package cross.field.stage;

import android.app.Activity;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.WindowManager;
import cross.field.MeteorBreaker.R;

public class StageActivity extends Activity {
    public static final float XPERIA_HEIGHT = 854.0f;
    public static final float XPERIA_WIDTH = 480.0f;
    private int disp_height;
    private int disp_width;
    private StageLayout layout;
    private float ratio_height;
    private float ratio_width;

    public void getDisplaySize() {
        Display disp = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        setDispWidth(disp.getWidth());
        setDispHeight(disp.getHeight());
    }

    public void getDisplayRatio() {
        getDisplaySize();
        setRatioWidth(((float) getDispWidth()) / 480.0f);
        setRatioHeight(((float) getDispHeight()) / 854.0f);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        getDisplaySize();
        getDisplayRatio();
        setContentView((int) R.layout.stage);
        this.layout = new StageLayout(this);
    }

    public boolean onTouchEvent(MotionEvent event) {
        this.layout.getView().touchEvent(event);
        switch (event.getAction()) {
        }
        return super.onTouchEvent(event);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            switch (event.getKeyCode()) {
                case 4:
                    finish();
                    break;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    public void setDispWidth(int disp_width2) {
        this.disp_width = disp_width2;
    }

    public int getDispWidth() {
        return this.disp_width;
    }

    public void setDispHeight(int disp_height2) {
        this.disp_height = disp_height2;
    }

    public int getDispHeight() {
        return this.disp_height;
    }

    public void setRatioWidth(float ratio_width2) {
        this.ratio_width = ratio_width2;
    }

    public float getRatioWidth() {
        return this.ratio_width;
    }

    public void setRatioHeight(float ratio_height2) {
        this.ratio_height = ratio_height2;
    }

    public float getRatioHeight() {
        return this.ratio_height;
    }

    public void setLayout(StageLayout layout2) {
        this.layout = layout2;
    }

    public StageLayout getLayout() {
        return this.layout;
    }
}
