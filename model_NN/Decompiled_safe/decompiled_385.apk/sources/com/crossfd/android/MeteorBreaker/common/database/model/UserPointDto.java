package com.crossfd.android.MeteorBreaker.common.database.model;

public class UserPointDto {
    private String countryCode;
    private String insertTime;
    private String userName;
    private double userPoint;

    public String getInsertTime() {
        return this.insertTime;
    }

    public void setInsertTime(String insertTime2) {
        this.insertTime = insertTime2;
    }

    public String getCountryCode() {
        return this.countryCode;
    }

    public void setCountryCode(String countryCode2) {
        this.countryCode = countryCode2;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName2) {
        this.userName = userName2;
    }

    public double getUserPoint() {
        return this.userPoint;
    }

    public void setUserPoint(double userPoint2) {
        this.userPoint = userPoint2;
    }
}
