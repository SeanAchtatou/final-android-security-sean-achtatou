package com.google.android.apps.analytics;

import cross.field.MeteorBreaker.Analytics;
import java.util.Locale;

class NetworkRequestUtil {
    private static final String FAKE_DOMAIN_HASH = "999";
    private static final String GOOGLE_ANALYTICS_GIF_PATH = "/__utm.gif";
    private static final int X10_PROJECT_NAMES = 8;
    private static final int X10_PROJECT_SCOPES = 11;
    private static final int X10_PROJECT_VALUES = 9;

    NetworkRequestUtil() {
    }

    static void appendCurrencyValue(StringBuilder sb, String str, double d) {
        sb.append(str).append("=");
        double floor = Math.floor((d * 1000000.0d) + 0.5d) / 1000000.0d;
        if (floor != 0.0d) {
            sb.append(Double.toString(floor));
        }
    }

    private static void appendStringValue(StringBuilder sb, String str, String str2) {
        sb.append(str).append("=");
        if (str2 != null && str2.trim().length() > 0) {
            sb.append(AnalyticsParameterEncoder.encode(str2));
        }
    }

    public static String constructEventRequestPath(Event event, String str) {
        Locale locale = Locale.getDefault();
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(String.format("5(%s*%s", encode(event.category), encode(event.action)));
        if (event.label != null) {
            sb2.append("*").append(encode(event.label));
        }
        sb2.append(")");
        if (event.value > -1) {
            sb2.append(String.format("(%d)", Integer.valueOf(event.value)));
        }
        sb2.append(getCustomVariableParams(event));
        sb.append(GOOGLE_ANALYTICS_GIF_PATH);
        sb.append("?utmwv=4.6ma");
        sb.append("&utmn=").append(event.randomVal);
        sb.append("&utmt=event");
        sb.append("&utme=").append(sb2.toString());
        sb.append("&utmcs=UTF-8");
        sb.append(String.format("&utmsr=%dx%d", Integer.valueOf(event.screenWidth), Integer.valueOf(event.screenHeight)));
        sb.append(String.format("&utmul=%s-%s", locale.getLanguage(), locale.getCountry()));
        sb.append("&utmac=").append(event.accountId);
        sb.append("&utmcc=").append(getEscapedCookieString(event, str));
        return sb.toString();
    }

    public static String constructItemRequestPath(Event event, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(GOOGLE_ANALYTICS_GIF_PATH);
        sb.append("?utmwv=4.6ma");
        sb.append("&utmn=").append(event.randomVal);
        sb.append("&utmt=item");
        Item item = event.getItem();
        if (item != null) {
            appendStringValue(sb, "&utmtid", item.getOrderId());
            appendStringValue(sb, "&utmipc", item.getItemSKU());
            appendStringValue(sb, "&utmipn", item.getItemName());
            appendStringValue(sb, "&utmiva", item.getItemCategory());
            appendCurrencyValue(sb, "&utmipr", item.getItemPrice());
            sb.append("&utmiqt=");
            if (item.getItemCount() != 0) {
                sb.append(item.getItemCount());
            }
        }
        sb.append("&utmac=").append(event.accountId);
        sb.append("&utmcc=").append(getEscapedCookieString(event, str));
        return sb.toString();
    }

    public static String constructPageviewRequestPath(Event event, String str) {
        String str2 = Analytics.ID;
        if (event.action != null) {
            str2 = event.action;
        }
        if (!str2.startsWith("/")) {
            str2 = "/" + str2;
        }
        String encode = encode(str2);
        String customVariableParams = getCustomVariableParams(event);
        Locale locale = Locale.getDefault();
        StringBuilder sb = new StringBuilder();
        sb.append(GOOGLE_ANALYTICS_GIF_PATH);
        sb.append("?utmwv=4.6ma");
        sb.append("&utmn=").append(event.randomVal);
        if (customVariableParams.length() > 0) {
            sb.append("&utme=").append(customVariableParams);
        }
        sb.append("&utmcs=UTF-8");
        sb.append(String.format("&utmsr=%dx%d", Integer.valueOf(event.screenWidth), Integer.valueOf(event.screenHeight)));
        sb.append(String.format("&utmul=%s-%s", locale.getLanguage(), locale.getCountry()));
        sb.append("&utmp=").append(encode);
        sb.append("&utmac=").append(event.accountId);
        sb.append("&utmcc=").append(getEscapedCookieString(event, str));
        return sb.toString();
    }

    public static String constructTransactionRequestPath(Event event, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(GOOGLE_ANALYTICS_GIF_PATH);
        sb.append("?utmwv=4.6ma");
        sb.append("&utmn=").append(event.randomVal);
        sb.append("&utmt=tran");
        Transaction transaction = event.getTransaction();
        if (transaction != null) {
            appendStringValue(sb, "&utmtid", transaction.getOrderId());
            appendStringValue(sb, "&utmtst", transaction.getStoreName());
            appendCurrencyValue(sb, "&utmtto", transaction.getTotalCost());
            appendCurrencyValue(sb, "&utmttx", transaction.getTotalTax());
            appendCurrencyValue(sb, "&utmtsp", transaction.getShippingCost());
            appendStringValue(sb, "&utmtci", Analytics.ID);
            appendStringValue(sb, "&utmtrg", Analytics.ID);
            appendStringValue(sb, "&utmtco", Analytics.ID);
        }
        sb.append("&utmac=").append(event.accountId);
        sb.append("&utmcc=").append(getEscapedCookieString(event, str));
        return sb.toString();
    }

    private static void createX10Project(CustomVariable[] customVariableArr, StringBuilder sb, int i) {
        sb.append(i).append("(");
        boolean z = true;
        for (int i2 = 0; i2 < customVariableArr.length; i2++) {
            if (customVariableArr[i2] != null) {
                CustomVariable customVariable = customVariableArr[i2];
                if (!z) {
                    sb.append("*");
                } else {
                    z = false;
                }
                sb.append(customVariable.getIndex()).append("!");
                switch (i) {
                    case 8:
                        sb.append(x10Escape(encode(customVariable.getName())));
                        continue;
                    case 9:
                        sb.append(x10Escape(encode(customVariable.getValue())));
                        continue;
                    case 11:
                        sb.append(customVariable.getScope());
                        continue;
                }
            }
        }
        sb.append(")");
    }

    private static String encode(String str) {
        return AnalyticsParameterEncoder.encode(str);
    }

    public static String getCustomVariableParams(Event event) {
        StringBuilder sb = new StringBuilder();
        CustomVariableBuffer customVariableBuffer = event.getCustomVariableBuffer();
        if (customVariableBuffer == null || !customVariableBuffer.hasCustomVariables()) {
            return Analytics.ID;
        }
        CustomVariable[] customVariableArray = customVariableBuffer.getCustomVariableArray();
        createX10Project(customVariableArray, sb, 8);
        createX10Project(customVariableArray, sb, 9);
        createX10Project(customVariableArray, sb, 11);
        return sb.toString();
    }

    public static String getEscapedCookieString(Event event, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("__utma=");
        sb.append(FAKE_DOMAIN_HASH).append(".");
        sb.append(event.userId).append(".");
        sb.append(event.timestampFirst).append(".");
        sb.append(event.timestampPrevious).append(".");
        sb.append(event.timestampCurrent).append(".");
        sb.append(event.visits);
        if (str != null) {
            sb.append("+__utmz=");
            sb.append(FAKE_DOMAIN_HASH).append(".");
            sb.append(event.timestampFirst).append(".");
            sb.append("1.1.");
            sb.append(str);
        }
        return encode(sb.toString());
    }

    private static String x10Escape(String str) {
        return str.replace("'", "'0").replace(")", "'1").replace("*", "'2").replace("!", "'3");
    }
}
