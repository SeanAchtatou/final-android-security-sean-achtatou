package com.games.gameObject;

import android.graphics.Point;

public class PolygonGameObject extends InFrameObject {
    private Point[] points;

    public PolygonGameObject(float x, float y, float w, float h, Point[] points2) {
        super(x, y, w, h);
        this.points = points2;
    }

    public PolygonGameObject(float x, float y, float w, float h) {
        super(x, y, w, h);
        this.points = new Point[4];
        this.points[0] = new Point((int) getImagex(), (int) getImagey());
        this.points[1] = new Point((int) getImagex(), (int) getImageh());
        this.points[2] = new Point((int) getImagew(), (int) getImageh());
        this.points[3] = new Point((int) getImagew(), (int) getImagey());
    }

    public Point[] getPoints() {
        return this.points;
    }

    public void setPoints(Point[] points2) {
        this.points = points2;
    }

    public boolean isHit(CircleGameObject go) {
        for (int i = 0; i < this.points.length - 2; i++) {
            double oa_x = (double) (go.getx() - ((float) this.points[i].x));
            double oa_y = (double) (go.gety() - ((float) this.points[i].y));
            double ob_x = (double) (this.points[i + 1].x - this.points[i].x);
            double ob_y = (double) (this.points[i + 1].y - this.points[i].y);
            double abs_ob2 = (ob_x * ob_x) + (ob_y * ob_y);
            double ba_x = (double) (go.getx() - ((float) this.points[i + 1].x));
            double ba_y = (double) (go.gety() - ((float) this.points[i + 1].y));
            double hit_r2 = (double) (go.getHitr() * go.getHitr());
            if (Math.pow((oa_x * ob_y) - (oa_y * ob_x), 2.0d) / abs_ob2 <= hit_r2 && (((oa_x * ob_x) + (oa_y * ob_y)) * ((ba_x * ob_x) + (ba_y * ob_y)) <= 0.0d || (ba_x * ba_x) + (ba_y * ba_y) <= hit_r2 || (oa_x * oa_x) + (oa_y * oa_y) <= hit_r2)) {
                return true;
            }
        }
        return false;
    }

    public boolean isHit(LineGameObject lo) {
        return false;
    }

    public boolean isHit(PolygonGameObject po) {
        return false;
    }
}
