package com.games.gameObject;

public abstract class InFrameObject extends GameObject {
    public abstract boolean isHit(CircleGameObject circleGameObject);

    public abstract boolean isHit(LineGameObject lineGameObject);

    public abstract boolean isHit(PolygonGameObject polygonGameObject);

    public InFrameObject(float x, float y, float w, float h) {
        super(x, y, w, h);
    }

    public boolean isInFrame(PolygonGameObject frame) {
        return isHit(frame);
    }

    public boolean isInTopLine(PolygonGameObject frame) {
        if (getImageh() < frame.getImagey()) {
            return false;
        }
        return true;
    }

    public boolean isInBottomLine(PolygonGameObject frame) {
        if (getImagey() > frame.getImageh()) {
            return false;
        }
        return true;
    }

    public boolean isInLeftLine(PolygonGameObject frame) {
        if (getImagew() < frame.getImagex()) {
            return false;
        }
        return true;
    }

    public boolean isInRightLine(PolygonGameObject frame) {
        if (getImagex() > frame.getImagew()) {
            return false;
        }
        return true;
    }

    public boolean isOutFrame(PolygonGameObject frame) {
        return !isInFrame(frame);
    }

    public boolean isOutTopLine(PolygonGameObject frame) {
        return !isInTopLine(frame);
    }

    public boolean isOutBottomLine(PolygonGameObject frame) {
        return !isInBottomLine(frame);
    }

    public boolean isOutLeftLine(PolygonGameObject frame) {
        return !isInLeftLine(frame);
    }

    public boolean isOutRightLine(PolygonGameObject frame) {
        return !isInRightLine(frame);
    }
}
