package com.games.gameObject;

import android.graphics.Point;

public class LineGameObject extends InFrameObject {
    private Point[] points = new Point[2];

    public LineGameObject(float x, float y, float w, float h, Point[] p) {
        super(x, y, w, h);
        this.points = p;
    }

    public Point[] getPoints() {
        return this.points;
    }

    public boolean isHit(CircleGameObject co) {
        return false;
    }

    public boolean isHit(PolygonGameObject po) {
        return false;
    }

    public boolean isHit(LineGameObject lo) {
        return false;
    }
}
