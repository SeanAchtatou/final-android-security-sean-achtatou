package com.games.gameObject;

import android.graphics.Point;

public class CircleGameObject extends InFrameObject {
    private float hit_r;

    public CircleGameObject(float x, float y, float w, float h, float hr) {
        super(x, y, w, h);
        this.hit_r = hr;
    }

    public CircleGameObject(float x, float y, float w, float h) {
        super(x, y, w, h);
        this.hit_r = Math.min(w, h) / 2.0f;
    }

    public float getHitr() {
        return this.hit_r;
    }

    public void setHitr(float hr) {
        this.hit_r = hr;
    }

    public boolean isHit(CircleGameObject cgo) {
        if (Math.pow((double) (cgo.getx() - getx()), 2.0d) + Math.pow((double) (cgo.gety() - gety()), 2.0d) <= Math.pow((double) (cgo.getHitr() + getHitr()), 2.0d)) {
            return true;
        }
        return false;
    }

    public boolean isHit(PolygonGameObject go) {
        Point[] points = go.getPoints();
        for (int i = 0; i < points.length - 2; i++) {
            double oa_x = (double) (getx() - ((float) points[i].x));
            double oa_y = (double) (gety() - ((float) points[i].y));
            double ob_x = (double) (points[i + 1].y - points[0].x);
            double ob_y = (double) (points[i + 1].x - points[0].y);
            double abs_ob2 = (ob_x * ob_x) + (ob_y * ob_y);
            double ba_x = (double) (getx() - ((float) points[i + 1].x));
            double ba_y = (double) (gety() - ((float) points[i + 1].y));
            double hit_r2 = (double) (getHitr() * getHitr());
            if (Math.pow((oa_x * ob_y) - (oa_y * ob_x), 2.0d) / abs_ob2 <= hit_r2 && (((oa_x * ob_x) + (oa_y * ob_y)) * ((ba_x * ob_x) + (ba_y * ob_y)) <= 0.0d || (ba_x * ba_x) + (ba_y * ba_y) <= hit_r2 || (oa_x * oa_x) + (oa_y * oa_y) <= hit_r2)) {
                return true;
            }
        }
        return false;
    }

    public boolean isHit(LineGameObject go) {
        Point[] points = go.getPoints();
        double oa_x = (double) (getx() - ((float) points[0].x));
        double oa_y = (double) (gety() - ((float) points[0].y));
        double ob_x = (double) (points[1].x - points[0].x);
        double ob_y = (double) (points[1].y - points[0].y);
        double abs_ob2 = (ob_x * ob_x) + (ob_y * ob_y);
        double ba_x = (double) (getx() - ((float) points[1].x));
        double ba_y = (double) (gety() - ((float) points[1].y));
        double hit_r2 = (double) (getHitr() * getHitr());
        if (Math.pow((oa_x * ob_y) - (oa_y * ob_x), 2.0d) / abs_ob2 > hit_r2 || (((oa_x * ob_x) + (oa_y * ob_y)) * ((ba_x * ob_x) + (ba_y * ob_y)) > 0.0d && (ba_x * ba_x) + (ba_y * ba_y) > hit_r2 && (oa_x * oa_x) + (oa_y * oa_y) > hit_r2)) {
            return false;
        }
        return true;
    }
}
