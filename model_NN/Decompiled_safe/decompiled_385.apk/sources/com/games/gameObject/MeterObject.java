package com.games.gameObject;

import android.graphics.Canvas;
import android.graphics.Paint;
import cross.field.stage.Graphics;

public class MeterObject extends PolygonGameObject {
    private float default_point;
    private float max_point;
    private float min_point;
    private float point;

    public MeterObject(float x, float y, float w, float h, int max, int min) {
        super(x, y, w, h);
        this.max_point = (float) max;
        this.min_point = (float) min;
        this.default_point = this.max_point;
        this.point = this.default_point;
    }

    public MeterObject(float x, float y, float w, float h, int max, int min, int def) {
        super(x, y, w, h);
        this.max_point = (float) max;
        this.min_point = (float) min;
        this.default_point = (float) def;
        this.point = this.default_point;
    }

    public float getMaxPoint() {
        return this.max_point;
    }

    public float getMinPoint() {
        return this.min_point;
    }

    public float getDefaultPoint() {
        return this.default_point;
    }

    public float getPoint() {
        return this.point;
    }

    public void setMaxPoint(float p) {
        this.max_point = p;
    }

    public void setMinPoint(float p) {
        this.min_point = p;
    }

    public void setDefaultPoint(float p) {
        this.default_point = p;
    }

    public void setPoint(float p) {
        this.point = p;
    }

    public void addMaxPoint(float p) {
        setMaxPoint(getMaxPoint() + p);
    }

    public void addMinPoint(float p) {
        setMinPoint(getMinPoint() + p);
    }

    public void addDefaultPoint(float p) {
        setDefaultPoint(getDefaultPoint() + p);
    }

    public void addPoint(float p) {
        setPoint(getPoint() + p);
    }

    public void draw(Canvas canvas) {
        if (this.draw_flg) {
            Paint paint = new Paint();
            if (this.max_point == this.point) {
                paint.setColor(-65536);
                canvas.drawRect(getImagex(), getImagey(), getImagew(), getImageh(), paint);
            } else if (this.min_point == this.point) {
                paint.setColor(-16777216);
                canvas.drawRect(getImagex(), getImagey(), getImagew(), getImageh(), paint);
            } else {
                paint.setColor(-16777216);
                canvas.drawRect(getImagex(), getImagey(), getImagew(), getImageh(), paint);
                paint.setColor(-65536);
                Canvas canvas2 = canvas;
                canvas2.drawRect(getImagex(), getImagey(), getPoint() * (getImagew() / getMaxPoint()), getImageh(), paint);
            }
        }
    }

    public void draw(Graphics graphics) {
    }

    public void drawText() {
    }
}
