package com.games.gameObject;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import cross.field.stage.Graphics;

public abstract class GameObject {
    public boolean draw_flg = true;
    private float h;
    private float w;
    private float x;
    private float y;

    public GameObject(float x2, float y2, float w2, float h2) {
        this.x = x2;
        this.y = y2;
        this.w = w2;
        this.h = h2;
    }

    public float getx() {
        return this.x;
    }

    public float gety() {
        return this.y;
    }

    public float getw() {
        return this.w;
    }

    public float geth() {
        return this.h;
    }

    public float getImagex() {
        return getx() - (getw() / 2.0f);
    }

    public float getImagey() {
        return gety() - (geth() / 2.0f);
    }

    public float getImagew() {
        return getx() + (getw() / 2.0f);
    }

    public float getImageh() {
        return gety() + (geth() / 2.0f);
    }

    public float getWidthPer(int d) {
        if (d == 0) {
            return getw();
        }
        return getw() / ((float) d);
    }

    public float getHeightPer(int d) {
        if (d == 0) {
            return geth();
        }
        return geth() / ((float) d);
    }

    public void setx(float x2) {
        this.x = x2;
    }

    public void sety(float y2) {
        this.y = y2;
    }

    public void setw(float w2) {
        this.w = w2;
    }

    public void seth(float h2) {
        this.h = h2;
    }

    public void setVisible() {
        this.draw_flg = true;
    }

    public void setInvisible() {
        this.draw_flg = false;
    }

    public void draw(Canvas canvas) {
        if (this.draw_flg) {
            Paint paint = new Paint();
            paint.setColor(-16777216);
            canvas.drawRect(getImagex(), getImagey(), getImagew(), getImageh(), paint);
        }
    }

    public void draw(Canvas canvas, Paint paint) {
        if (this.draw_flg) {
            canvas.drawRect(getImagex(), getImagey(), getImagew(), getImageh(), paint);
        }
    }

    public void draw(Canvas canvas, Drawable image) {
        if (this.draw_flg) {
            image.setBounds((int) getImagex(), (int) getImagey(), (int) getImagew(), (int) getImageh());
            image.draw(canvas);
        }
    }

    public void draw(Graphics g, int image_id) {
        if (this.draw_flg) {
            g.drawImage(image_id, (int) getImagex(), (int) getImagey(), (int) getw(), (int) geth());
        }
    }
}
