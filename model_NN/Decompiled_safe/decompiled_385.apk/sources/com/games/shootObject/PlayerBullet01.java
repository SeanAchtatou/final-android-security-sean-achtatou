package com.games.shootObject;

public class PlayerBullet01 extends Bullet {
    public PlayerBullet01(float x, float y, float w, float h) {
        super(x, y, w, h);
        setHitr((getw() / 10.0f) * 6.0f);
        setUpSpeed(7.0f);
    }
}
