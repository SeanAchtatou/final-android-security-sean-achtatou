package com.games.shootObject;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import com.games.gameObject.PolygonGameObject;
import cross.field.stage.Graphics;
import java.util.ArrayList;
import java.util.Iterator;

public class Field01 extends PolygonGameObject {
    private static final int MAX_IN_FRAME_ENEMYS = 40;
    private static final int MAX_IN_FRAME_ENEMY_BULLETS = 30;
    private static final int MAX_IN_FRAME_PLAYERS = 1;
    private static final int MAX_IN_FRAME_PLAYER_BULLETS = 10;
    private static ArrayList<EffectCircleObject> effects;
    private static ArrayList<Enemy> enemys;
    private static ArrayList<Enemy> enemys2;
    private int in_frame_enemys;
    private Iterator it;
    private Point[] points = new Point[4];

    public Field01(float disp_x, float disp_y, float disp_w, float disp_h) {
        super(disp_x, disp_y, disp_w, disp_h);
        float disp_w2 = disp_w + 1.0f;
        float disp_h2 = disp_h + 1.0f;
        this.points[0] = new Point(0, 0);
        this.points[1] = new Point(0, (int) disp_h2);
        this.points[2] = new Point((int) disp_w2, (int) disp_h2);
        this.points[3] = new Point((int) disp_w2, 0);
        enemys = new ArrayList<>();
        enemys2 = new ArrayList<>();
        effects = new ArrayList<>();
        this.in_frame_enemys = 10;
    }

    public void setEnemys(int count) {
        this.in_frame_enemys = (count / 10) + 4;
        if (this.in_frame_enemys > MAX_IN_FRAME_ENEMYS) {
            this.in_frame_enemys = MAX_IN_FRAME_ENEMYS;
        }
        if (enemys.size() < this.in_frame_enemys && Math.random() * 100.0d < 10.0d) {
            enemys.add(new Enemy((float) (Math.random() * (((double) ((int) getWidthPer(1))) + 0.5d)), -1.0f * getWidthPer(12), getWidthPer(12), getWidthPer(12), count));
        }
    }

    public void hitAction(Enemy e, int count) {
        if (e.getw() > getWidthPer(12) * 2.0f) {
            float xdif1 = 1.0f + ((((float) Math.random()) * 300.0f) / 100.0f);
            float xdif2 = 1.0f + ((((float) Math.random()) * 300.0f) / 100.0f);
            float ydif1 = 1.0f + ((((float) Math.random()) * 200.0f) / 100.0f);
            float ydif2 = 1.0f + ((((float) Math.random()) * 200.0f) / 100.0f);
            float wdif1 = 1.0f + ((((float) Math.random()) * 500.0f) / 100.0f);
            float wdif2 = 1.0f + ((((float) Math.random()) * 500.0f) / 100.0f);
            float random = 1.0f + ((((float) Math.random()) * 400.0f) / 100.0f);
            float random2 = 1.0f + ((((float) Math.random()) * 400.0f) / 100.0f);
            Enemy enemy = new Enemy(e.getx() - (e.getw() / 5.0f), e.gety() - (e.geth() / 5.0f), e.getw() / 5.0f, e.geth() / 5.0f, count);
            enemy.setSpeed(e.getUpSpeed() + ydif1, e.getDownSpeed(), e.getRightSpeed(), e.getLeftSpeed() + xdif1);
            enemys2.add(enemy);
            Enemy enemy2 = new Enemy(e.getx() - (e.getw() / 5.0f), e.gety() + (e.geth() / 5.0f), e.getw() / 5.0f, e.geth() / 5.0f, count);
            enemy2.setSpeed(e.getUpSpeed(), e.getDownSpeed(), e.getRightSpeed(), e.getLeftSpeed() + wdif1);
            enemys2.add(enemy2);
            Enemy enemy3 = new Enemy(e.getx() + (e.getw() / 5.0f), e.gety() - (e.geth() / 5.0f), e.getw() / 5.0f, e.geth() / 5.0f, count);
            enemy3.setSpeed(e.getUpSpeed() + ydif2, e.getDownSpeed(), e.getRightSpeed() + xdif2, e.getLeftSpeed());
            enemys2.add(enemy3);
            Enemy enemy4 = new Enemy(e.getx() + (e.getw() / 5.0f), e.gety() + (e.geth() / 5.0f), e.getw() / 5.0f, e.geth() / 5.0f, count);
            enemy4.setSpeed(e.getUpSpeed(), e.getDownSpeed(), e.getRightSpeed() + wdif2, e.getLeftSpeed());
            enemys2.add(enemy4);
        }
        effects.add(new EffectCircleObject((float) ((int) e.getx()), (float) ((int) e.gety()), (float) ((int) e.getw()), (float) ((int) e.geth()), 20));
    }

    public void hitAction2(Enemy e, int count) {
        effects.add(new EffectCircleObject((float) ((int) e.getx()), (float) ((int) e.gety()), (float) ((int) e.getw()), (float) ((int) e.geth()), 20));
    }

    public void update(int count) {
        this.it = effects.iterator();
        while (this.it.hasNext()) {
            EffectCircleObject e = (EffectCircleObject) this.it.next();
            e.mulLife(1);
            e.seth((e.geth() / 20.0f) * ((float) e.getLife()));
            e.setw((e.getw() / 20.0f) * ((float) e.getLife()));
            if (e.getw() <= 0.0f) {
                this.it.remove();
            }
        }
    }

    public void enemyMove(int count) {
        this.it = enemys.iterator();
        while (this.it.hasNext()) {
            Enemy e = (Enemy) this.it.next();
            e.move(false, true, true, true, count);
            if (e.isOutBottomLine(this) || e.isOutRightLine(this) || e.isOutLeftLine(this)) {
                this.it.remove();
            }
        }
        this.it = enemys2.iterator();
        while (this.it.hasNext()) {
            Enemy e2 = (Enemy) this.it.next();
            e2.move(false, true, true, true, count);
            if (e2.isOutBottomLine(this) || e2.isOutRightLine(this) || e2.isOutLeftLine(this)) {
                this.it.remove();
            }
        }
    }

    public void enemyAction() {
    }

    public ArrayList<Enemy> getEnemys() {
        return enemys;
    }

    public ArrayList<Enemy> getEnemys2() {
        return enemys2;
    }

    public int getEnemyNumber() {
        return enemys.size();
    }

    public void drawEnemy(Graphics g, int image_id) {
        Canvas canvas = g.canvas;
        this.it = effects.iterator();
        while (this.it.hasNext()) {
            ((EffectCircleObject) this.it.next()).draw(g, 4);
        }
        this.it = enemys.iterator();
        while (this.it.hasNext()) {
            ((Enemy) this.it.next()).draw(g, image_id);
        }
        this.it = enemys2.iterator();
        while (this.it.hasNext()) {
            ((Enemy) this.it.next()).draw(g, image_id);
        }
    }

    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(-16711936);
        canvas.drawRect(getImagex(), getImagey(), getImagew(), getImageh(), paint);
    }
}
