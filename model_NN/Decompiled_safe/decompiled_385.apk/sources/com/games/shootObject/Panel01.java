package com.games.shootObject;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import com.games.gameObject.PolygonGameObject;
import cross.field.stage.Button;
import cross.field.stage.Graphics;
import java.util.ArrayList;
import java.util.Iterator;

public class Panel01 extends PolygonGameObject {
    private static final int MAX_IN_FRAME_ENEMYS = 10;
    private static final int MAX_IN_FRAME_ENEMY_BULLETS = 30;
    private static final int MAX_IN_FRAME_PLAYERS = 1;
    private static final int MAX_IN_FRAME_PLAYER_BULLETS = 10;
    private static ArrayList<Enemy> enemys;
    static int svcount;
    private Iterator it;
    private Point[] points = new Point[4];

    public Panel01(float disp_x, float disp_y, float disp_w, float disp_h) {
        super(disp_x, disp_y, disp_w, disp_h);
        float disp_w2 = disp_w + 1.0f;
        float disp_h2 = disp_h + 1.0f;
        this.points[0] = new Point(0, 0);
        this.points[1] = new Point(0, (int) disp_h2);
        this.points[2] = new Point((int) disp_w2, (int) disp_h2);
        this.points[3] = new Point((int) disp_w2, 0);
        enemys = new ArrayList<>();
    }

    public void draw(Graphics g, Paint paint, int param01, boolean emd_flag) {
        Paint paint1 = new Paint();
        Canvas canvas = g.canvas;
        if (!emd_flag) {
            svcount = param01;
        }
        paint1.setColor(Color.argb((int) Button.ALPHA_MAX, 100, 100, 100));
        canvas.drawLine(getImagex() + 1.0f, getImagey(), getImagex() + 1.0f, getImageh(), paint1);
        paint1.setColor(Color.argb((int) Button.ALPHA_MAX, 100, 100, 100));
        canvas.drawLine(getImagew() + 1.0f, getImagey(), getImagew() + 1.0f, getImageh(), paint1);
        paint1.setColor(Color.argb((int) Button.ALPHA_MAX, 100, 100, 100));
        canvas.drawLine(getImagex() - 1.0f, getImagey(), getImagex() - 1.0f, getImageh(), paint1);
        paint1.setColor(Color.argb((int) Button.ALPHA_MAX, 100, 100, 100));
        canvas.drawLine(getImagew() - 1.0f, getImagey(), getImagew() - 1.0f, getImageh(), paint1);
        paint1.setColor(Color.argb((int) Button.ALPHA_MAX, 0, 0, 0));
        canvas.drawLine(getImagex(), getImagey(), getImagex(), getImageh(), paint1);
        paint1.setColor(Color.argb((int) Button.ALPHA_MAX, 0, 0, 0));
        canvas.drawLine(getImagew(), getImagey(), getImagew(), getImageh(), paint1);
        paint.setTextSize((geth() * 8.0f) / 10.0f);
        paint.setColor(Color.argb((int) Button.ALPHA_MAX, 0, (int) Button.ALPHA_MAX, 0));
        canvas.drawText("Broken stones : ", getx() - (getw() / 4.0f), getImageh() - ((geth() * 3.0f) / 11.0f), paint);
        canvas.drawText(new StringBuilder().append(svcount).toString(), getx() + (getw() / 8.0f), getImageh() - ((geth() * 3.0f) / 11.0f), paint);
    }
}
