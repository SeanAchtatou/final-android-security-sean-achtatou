package com.games.shootObject;

import com.games.gameObject.CircleGameObject;
import cross.field.stage.Graphics;

public class EffectCircleObject extends CircleGameObject {
    int life;

    public EffectCircleObject(float x, float y, float w, float h, int life2) {
        super(x, y, w, h);
        this.life = life2;
    }

    public void draw(Graphics g, int image_id) {
        super.draw(g, 4);
    }

    public void setLife(int l) {
        this.life = l;
    }

    public int getLife() {
        return this.life;
    }

    public void mulLife(int l) {
        setLife(getLife() - l);
    }
}
