package com.games.shootObject;

import com.games.gameObject.CircleGameObject;

public class Enemy extends CircleGameObject {
    private static final int MAX_DOWN_SPEED = 30;
    private static final int MAX_LEFT_SPEED = 30;
    private static final int MAX_RIGHT_SPEED = 30;
    private static final int MAX_UP_SPEED = 30;
    private static final int MIN_DOWN_SPEED = 4;
    private static final int MIN_LEFT_SPEED = 4;
    private static final int MIN_RIGHT_SPEED = 4;
    private static final int MIN_UP_SPEED = 4;
    private float down_speed;
    private float left_speed;
    private float right_speed;
    private float up_speed;

    public Enemy(float x, float y, float w, float h, int count) {
        super(x, y, w, h);
        this.up_speed = 0.0f;
        this.down_speed = 20.0f;
        this.right_speed = 5.0f;
        this.left_speed = 5.0f;
        this.up_speed = 0.0f;
        this.down_speed = ((float) (Math.random() * 7.0d)) + 5.0f;
        this.right_speed = (float) (Math.random() * 5.0d);
        this.left_speed = (float) (Math.random() * 5.0d);
        setw((float) ((((double) getw()) * ((Math.random() * 300.0d) + 100.0d)) / 100.0d));
        seth(getw());
    }

    public void move(boolean up, boolean down, boolean right, boolean left, int count) {
        float x = getx();
        float y = gety();
        if (up) {
            y -= getUpSpeed();
        }
        if (down) {
            y += getDownSpeed();
        }
        if (right) {
            x += getRightSpeed();
        }
        if (left) {
            x -= getLeftSpeed();
        }
        setx(x);
        sety(y);
    }

    public float getUpSpeed() {
        return this.up_speed;
    }

    public float getDownSpeed() {
        return this.down_speed;
    }

    public float getRightSpeed() {
        return this.right_speed;
    }

    public float getLeftSpeed() {
        return this.left_speed;
    }

    public void setUpSpeed(float us) {
        if (us > 30.0f) {
            this.up_speed = 30.0f;
        } else if (us < 4.0f) {
            this.up_speed = 4.0f;
        } else {
            this.up_speed = us;
        }
    }

    public void setDownSpeed(float ds) {
        if (ds > 30.0f) {
            this.down_speed = 30.0f;
        } else if (ds < 4.0f) {
            this.down_speed = 4.0f;
        } else {
            this.down_speed = ds;
        }
    }

    public void setRightSpeed(float rs) {
        if (rs > 30.0f) {
            this.right_speed = 30.0f;
        } else if (rs < 4.0f) {
            this.right_speed = 4.0f;
        } else {
            this.right_speed = rs;
        }
    }

    public void setLeftSpeed(float ls) {
        if (ls > 30.0f) {
            this.left_speed = 30.0f;
        } else if (ls < 4.0f) {
            this.left_speed = 4.0f;
        } else {
            this.left_speed = ls;
        }
    }

    public void setSpeed(float us, float ds, float rs, float ls) {
        setUpSpeed(us);
        setDownSpeed(ds);
        setRightSpeed(rs);
        setLeftSpeed(ls);
    }

    public void addUpSpeed(float ds) {
        setUpSpeed(getUpSpeed() + ds);
    }

    public void addDownSpeed(float ds) {
        setDownSpeed(getDownSpeed() + ds);
    }

    public void addRightSpeed(float ds) {
        setRightSpeed(getRightSpeed() + ds);
    }

    public void addLeftSpeed(float ds) {
        setLeftSpeed(getLeftSpeed() + ds);
    }
}
