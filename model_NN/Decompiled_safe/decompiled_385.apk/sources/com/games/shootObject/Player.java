package com.games.shootObject;

import com.games.gameObject.CircleGameObject;
import com.games.gameObject.PolygonGameObject;
import cross.field.stage.Graphics;
import java.util.ArrayList;
import java.util.Iterator;

public class Player extends CircleGameObject {
    private static final int MAX_DOWN_SPEED = 10;
    private static final int MAX_LEFT_SPEED = 10;
    private static final int MAX_RIGHT_SPEED = 10;
    private static final int MAX_UP_SPEED = 10;
    private static final int MIN_DOWN_SPEED = 1;
    private static final int MIN_LEFT_SPEED = 1;
    private static final int MIN_RIGHT_SPEED = 1;
    private static final int MIN_UP_SPEED = 1;
    private static int inFrameBullets01 = 20;
    private static ArrayList<PlayerBullet01> playerBullets01;
    public boolean aaa = true;
    private float down_speed = 5.0f;
    private Iterator it;
    private float left_speed = 6.0f;
    private float right_speed = 6.0f;
    private float up_speed = 5.0f;

    public Player(float x, float y, float w, float h) {
        super(x, y, w, h);
        setHitr((getHitr() * 7.0f) / 10.0f);
        playerBullets01 = new ArrayList<>();
    }

    public void update(boolean up, boolean down, boolean right, boolean left, PolygonGameObject flame, int count) {
        move(up, down, right, left, flame);
        if (this.aaa) {
            shootBullet01(flame, count);
        } else if (getw() > 0.0f) {
            setw(getw() - 1.0f);
            seth(geth() - 1.0f);
        }
    }

    public void draw(Graphics g, int image_id) {
        drawBullets01(g, 5, playerBullets01);
        if (this.aaa) {
            super.draw(g, 1);
        } else {
            super.draw(g, 4);
        }
    }

    public void move(boolean up, boolean down, boolean right, boolean left, PolygonGameObject flame) {
        float x = getx();
        float y = gety();
        if (up) {
            y -= getUpSpeed();
        }
        if (down) {
            y += getDownSpeed();
        }
        if (right) {
            x += getRightSpeed();
        }
        if (left) {
            x -= getLeftSpeed();
        }
        if (getx() < flame.getImagex()) {
            x = flame.getImagex();
        }
        if (getx() > flame.getImagew()) {
            x = flame.getImagew();
        }
        sety(y);
        setx(x);
    }

    public void shootBullet01(PolygonGameObject flame, int count) {
        if (count % 20 == 0) {
            playerBullets01.add(new PlayerBullet01(getx(), gety() - (geth() / 2.0f), getw() * 2.5f, geth() * 2.5f));
        }
        this.it = playerBullets01.iterator();
        while (this.it.hasNext()) {
            PlayerBullet01 b = (PlayerBullet01) this.it.next();
            b.move(true, false, false, false);
            if (b.isOutTopLine(flame) || b.isOutRightLine(flame) || b.isOutLeftLine(flame)) {
                this.it.remove();
            }
        }
    }

    public void drawBullets01(Graphics g, int image_id, ArrayList<PlayerBullet01> playerBullets012) {
        this.it = playerBullets012.iterator();
        while (this.it.hasNext()) {
            ((PlayerBullet01) this.it.next()).draw(g, image_id);
        }
    }

    public float getUpSpeed() {
        return this.up_speed;
    }

    public float getDownSpeed() {
        return this.down_speed;
    }

    public float getRightSpeed() {
        return this.right_speed;
    }

    public float getLeftSpeed() {
        return this.left_speed;
    }

    public ArrayList<PlayerBullet01> getBullets01() {
        return playerBullets01;
    }

    public void setUpSpeed(float us) {
        if (us > 10.0f) {
            this.up_speed = 10.0f;
        } else if (us < 1.0f) {
            this.up_speed = 1.0f;
        } else {
            this.up_speed = us;
        }
    }

    public void setDownSpeed(float ds) {
        if (ds > 10.0f) {
            this.down_speed = 10.0f;
        } else if (ds < 1.0f) {
            this.down_speed = 1.0f;
        } else {
            this.down_speed = ds;
        }
    }

    public void setRightSpeed(float rs) {
        if (rs > 10.0f) {
            this.right_speed = 10.0f;
        } else if (rs < 1.0f) {
            this.right_speed = 1.0f;
        } else {
            this.right_speed = rs;
        }
    }

    public void setLeftSpeed(float ls) {
        if (ls > 10.0f) {
            this.left_speed = 10.0f;
        } else if (ls < 1.0f) {
            this.left_speed = 1.0f;
        } else {
            this.left_speed = ls;
        }
    }

    public void addUpSpeed(float ds) {
        setUpSpeed(getUpSpeed() + ds);
    }

    public void addDownSpeed(float ds) {
        setDownSpeed(getDownSpeed() + ds);
    }

    public void addRightSpeed(float ds) {
        setRightSpeed(getRightSpeed() + ds);
    }

    public void addLeftSpeed(float ds) {
        setLeftSpeed(getLeftSpeed() + ds);
    }
}
