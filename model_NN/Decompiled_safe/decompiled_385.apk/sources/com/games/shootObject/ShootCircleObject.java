package com.games.shootObject;

import com.games.gameObject.CircleGameObject;

public class ShootCircleObject extends CircleGameObject {
    private static final int MAX_DOWN_SPEED = 40;
    private static final int MAX_LEFT_SPEED = 40;
    private static final int MAX_RIGHT_SPEED = 40;
    private static final int MAX_UP_SPEED = 40;
    private static final int MIN_DOWN_SPEED = 0;
    private static final int MIN_LEFT_SPEED = 0;
    private static final int MIN_RIGHT_SPEED = 0;
    private static final int MIN_UP_SPEED = 0;
    private float down_speed = 0.0f;
    private float left_speed = 0.0f;
    private float right_speed = 0.0f;
    private float up_speed = 0.0f;

    public ShootCircleObject(float x, float y, float w, float h) {
        super(x, y, w, h);
    }

    public void move(boolean up, boolean down, boolean right, boolean left) {
        float x = getx();
        float y = gety();
        if (up) {
            y -= getUpSpeed();
        }
        if (down) {
            y += getDownSpeed();
        }
        if (right) {
            x += getRightSpeed();
        }
        if (left) {
            x -= getLeftSpeed();
        }
        sety(y);
        setx(x);
    }

    public float getUpSpeed() {
        return this.up_speed;
    }

    public float getDownSpeed() {
        return this.down_speed;
    }

    public float getRightSpeed() {
        return this.right_speed;
    }

    public float getLeftSpeed() {
        return this.left_speed;
    }

    public void addUpSpeed(float ds) {
        setUpSpeed(getUpSpeed() + ds);
    }

    public void addDownSpeed(float ds) {
        setDownSpeed(getDownSpeed() + ds);
    }

    public void addRightSpeed(float ds) {
        setRightSpeed(getRightSpeed() + ds);
    }

    public void addLeftSpeed(float ds) {
        setLeftSpeed(getLeftSpeed() + ds);
    }

    public void setUpSpeed(float us) {
        if (us > 40.0f) {
            this.up_speed = 40.0f;
        } else if (us < 0.0f) {
            this.up_speed = 0.0f;
        } else {
            this.up_speed = us;
        }
    }

    public void setDownSpeed(float ds) {
        if (ds > 40.0f) {
            this.down_speed = 40.0f;
        } else if (ds < 0.0f) {
            this.down_speed = 0.0f;
        } else {
            this.down_speed = ds;
        }
    }

    public void setRightSpeed(float rs) {
        if (rs > 40.0f) {
            this.right_speed = 40.0f;
        } else if (rs < 0.0f) {
            this.right_speed = 0.0f;
        } else {
            this.right_speed = rs;
        }
    }

    public void setLeftSpeed(float ls) {
        if (ls > 40.0f) {
            this.left_speed = 40.0f;
        } else if (ls < 0.0f) {
            this.left_speed = 0.0f;
        } else {
            this.left_speed = ls;
        }
    }
}
