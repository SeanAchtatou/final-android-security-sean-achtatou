package com.tearn.kbigkittens;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int keys = 2130837504;
        public static final int pack = 2130837505;
        public static final int vt = 2130837506;
        public static final int wt = 2130837507;
    }

    public static final class id {
        public static final int webview = 2131034112;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app = 2130968578;
        public static final int app_name = 2130968579;
        public static final int hello = 2130968576;
        public static final int pack = 2130968577;
    }
}
