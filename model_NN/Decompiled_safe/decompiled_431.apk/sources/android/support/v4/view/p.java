package android.support.v4.view;

import android.database.DataSetObserver;

/* compiled from: ProGuard */
class p extends DataSetObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DirectionalViewPager f96a;

    private p(DirectionalViewPager directionalViewPager) {
        this.f96a = directionalViewPager;
    }

    /* synthetic */ p(DirectionalViewPager directionalViewPager, i iVar) {
        this(directionalViewPager);
    }

    public void onChanged() {
        this.f96a.dataSetChanged();
    }

    public void onInvalidated() {
        this.f96a.dataSetChanged();
    }
}
