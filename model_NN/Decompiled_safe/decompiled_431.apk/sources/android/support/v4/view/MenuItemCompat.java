package android.support.v4.view;

import android.os.Build;
import android.view.MenuItem;
import android.view.View;

/* compiled from: ProGuard */
public class MenuItemCompat {
    static final af IMPL;
    public static final int SHOW_AS_ACTION_ALWAYS = 2;
    public static final int SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW = 8;
    public static final int SHOW_AS_ACTION_IF_ROOM = 1;
    public static final int SHOW_AS_ACTION_NEVER = 0;
    public static final int SHOW_AS_ACTION_WITH_TEXT = 4;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            IMPL = new ae();
        } else {
            IMPL = new ad();
        }
    }

    public static boolean setShowAsAction(MenuItem menuItem, int i) {
        return IMPL.a(menuItem, i);
    }

    public static MenuItem setActionView(MenuItem menuItem, View view) {
        return IMPL.a(menuItem, view);
    }
}
