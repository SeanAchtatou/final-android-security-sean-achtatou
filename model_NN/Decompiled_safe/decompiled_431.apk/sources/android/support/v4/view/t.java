package android.support.v4.view;

import android.content.Context;
import android.os.Handler;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;

/* compiled from: ProGuard */
class t implements s {
    private static final int e = ViewConfiguration.getLongPressTimeout();
    private static final int f = ViewConfiguration.getTapTimeout();
    private static final int g = ViewConfiguration.getDoubleTapTimeout();

    /* renamed from: a  reason: collision with root package name */
    private int f97a;
    private int b;
    private int c;
    private int d;
    private final Handler h;
    /* access modifiers changed from: private */
    public final GestureDetector.OnGestureListener i;
    /* access modifiers changed from: private */
    public GestureDetector.OnDoubleTapListener j;
    /* access modifiers changed from: private */
    public boolean k;
    private boolean l;
    private boolean m;
    private boolean n;
    /* access modifiers changed from: private */
    public MotionEvent o;
    private MotionEvent p;
    private boolean q;
    private float r;
    private float s;
    private float t;
    private float u;
    private boolean v;
    private VelocityTracker w;

    public t(Context context, GestureDetector.OnGestureListener onGestureListener, Handler handler) {
        if (handler != null) {
            this.h = new u(this, handler);
        } else {
            this.h = new u(this);
        }
        this.i = onGestureListener;
        if (onGestureListener instanceof GestureDetector.OnDoubleTapListener) {
            a((GestureDetector.OnDoubleTapListener) onGestureListener);
        }
        a(context);
    }

    private void a(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Context must not be null");
        } else if (this.i == null) {
            throw new IllegalArgumentException("OnGestureListener must not be null");
        } else {
            this.v = true;
            ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
            int scaledTouchSlop = viewConfiguration.getScaledTouchSlop();
            int scaledDoubleTapSlop = viewConfiguration.getScaledDoubleTapSlop();
            this.c = viewConfiguration.getScaledMinimumFlingVelocity();
            this.d = viewConfiguration.getScaledMaximumFlingVelocity();
            this.f97a = scaledTouchSlop * scaledTouchSlop;
            this.b = scaledDoubleTapSlop * scaledDoubleTapSlop;
        }
    }

    public void a(GestureDetector.OnDoubleTapListener onDoubleTapListener) {
        this.j = onDoubleTapListener;
    }

    public void a(boolean z) {
        this.v = z;
    }

    public boolean a() {
        return this.v;
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0102  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(android.view.MotionEvent r14) {
        /*
            r13 = this;
            r6 = 0
            r12 = 2
            r11 = 3
            r8 = 1
            r3 = 0
            int r9 = r14.getAction()
            android.view.VelocityTracker r0 = r13.w
            if (r0 != 0) goto L_0x0013
            android.view.VelocityTracker r0 = android.view.VelocityTracker.obtain()
            r13.w = r0
        L_0x0013:
            android.view.VelocityTracker r0 = r13.w
            r0.addMovement(r14)
            r0 = r9 & 255(0xff, float:3.57E-43)
            r1 = 6
            if (r0 != r1) goto L_0x0032
            r7 = r8
        L_0x001e:
            if (r7 == 0) goto L_0x0034
            int r0 = android.support.v4.view.MotionEventCompat.getActionIndex(r14)
        L_0x0024:
            int r4 = android.support.v4.view.MotionEventCompat.getPointerCount(r14)
            r5 = r3
            r1 = r6
            r2 = r6
        L_0x002b:
            if (r5 >= r4) goto L_0x0041
            if (r0 != r5) goto L_0x0036
        L_0x002f:
            int r5 = r5 + 1
            goto L_0x002b
        L_0x0032:
            r7 = r3
            goto L_0x001e
        L_0x0034:
            r0 = -1
            goto L_0x0024
        L_0x0036:
            float r10 = android.support.v4.view.MotionEventCompat.getX(r14, r5)
            float r2 = r2 + r10
            float r10 = android.support.v4.view.MotionEventCompat.getY(r14, r5)
            float r1 = r1 + r10
            goto L_0x002f
        L_0x0041:
            if (r7 == 0) goto L_0x004f
            int r0 = r4 + -1
        L_0x0045:
            float r5 = (float) r0
            float r2 = r2 / r5
            float r0 = (float) r0
            float r1 = r1 / r0
            r0 = r9 & 255(0xff, float:3.57E-43)
            switch(r0) {
                case 0: goto L_0x00a8;
                case 1: goto L_0x01b1;
                case 2: goto L_0x013d;
                case 3: goto L_0x0235;
                case 4: goto L_0x004e;
                case 5: goto L_0x0051;
                case 6: goto L_0x005d;
                default: goto L_0x004e;
            }
        L_0x004e:
            return r3
        L_0x004f:
            r0 = r4
            goto L_0x0045
        L_0x0051:
            r13.r = r2
            r13.t = r2
            r13.s = r1
            r13.u = r1
            r13.c()
            goto L_0x004e
        L_0x005d:
            r13.r = r2
            r13.t = r2
            r13.s = r1
            r13.u = r1
            android.view.VelocityTracker r0 = r13.w
            r1 = 1000(0x3e8, float:1.401E-42)
            int r2 = r13.d
            float r2 = (float) r2
            r0.computeCurrentVelocity(r1, r2)
            int r1 = android.support.v4.view.MotionEventCompat.getActionIndex(r14)
            int r0 = android.support.v4.view.MotionEventCompat.getPointerId(r14, r1)
            android.view.VelocityTracker r2 = r13.w
            float r2 = android.support.v4.view.VelocityTrackerCompat.getXVelocity(r2, r0)
            android.view.VelocityTracker r5 = r13.w
            float r5 = android.support.v4.view.VelocityTrackerCompat.getYVelocity(r5, r0)
            r0 = r3
        L_0x0084:
            if (r0 >= r4) goto L_0x004e
            if (r0 != r1) goto L_0x008b
        L_0x0088:
            int r0 = r0 + 1
            goto L_0x0084
        L_0x008b:
            int r7 = android.support.v4.view.MotionEventCompat.getPointerId(r14, r0)
            android.view.VelocityTracker r8 = r13.w
            float r8 = android.support.v4.view.VelocityTrackerCompat.getXVelocity(r8, r7)
            float r8 = r8 * r2
            android.view.VelocityTracker r9 = r13.w
            float r7 = android.support.v4.view.VelocityTrackerCompat.getYVelocity(r9, r7)
            float r7 = r7 * r5
            float r7 = r7 + r8
            int r7 = (r7 > r6 ? 1 : (r7 == r6 ? 0 : -1))
            if (r7 >= 0) goto L_0x0088
            android.view.VelocityTracker r0 = r13.w
            r0.clear()
            goto L_0x004e
        L_0x00a8:
            android.view.GestureDetector$OnDoubleTapListener r0 = r13.j
            if (r0 == 0) goto L_0x013b
            android.os.Handler r0 = r13.h
            boolean r0 = r0.hasMessages(r11)
            if (r0 == 0) goto L_0x00b9
            android.os.Handler r4 = r13.h
            r4.removeMessages(r11)
        L_0x00b9:
            android.view.MotionEvent r4 = r13.o
            if (r4 == 0) goto L_0x0133
            android.view.MotionEvent r4 = r13.p
            if (r4 == 0) goto L_0x0133
            if (r0 == 0) goto L_0x0133
            android.view.MotionEvent r0 = r13.o
            android.view.MotionEvent r4 = r13.p
            boolean r0 = r13.a(r0, r4, r14)
            if (r0 == 0) goto L_0x0133
            r13.q = r8
            android.view.GestureDetector$OnDoubleTapListener r0 = r13.j
            android.view.MotionEvent r4 = r13.o
            boolean r0 = r0.onDoubleTap(r4)
            r0 = r0 | r3
            android.view.GestureDetector$OnDoubleTapListener r4 = r13.j
            boolean r4 = r4.onDoubleTapEvent(r14)
            r0 = r0 | r4
        L_0x00df:
            r13.r = r2
            r13.t = r2
            r13.s = r1
            r13.u = r1
            android.view.MotionEvent r1 = r13.o
            if (r1 == 0) goto L_0x00f0
            android.view.MotionEvent r1 = r13.o
            r1.recycle()
        L_0x00f0:
            android.view.MotionEvent r1 = android.view.MotionEvent.obtain(r14)
            r13.o = r1
            r13.m = r8
            r13.n = r8
            r13.k = r8
            r13.l = r3
            boolean r1 = r13.v
            if (r1 == 0) goto L_0x011a
            android.os.Handler r1 = r13.h
            r1.removeMessages(r12)
            android.os.Handler r1 = r13.h
            android.view.MotionEvent r2 = r13.o
            long r2 = r2.getDownTime()
            int r4 = android.support.v4.view.t.f
            long r4 = (long) r4
            long r2 = r2 + r4
            int r4 = android.support.v4.view.t.e
            long r4 = (long) r4
            long r2 = r2 + r4
            r1.sendEmptyMessageAtTime(r12, r2)
        L_0x011a:
            android.os.Handler r1 = r13.h
            android.view.MotionEvent r2 = r13.o
            long r2 = r2.getDownTime()
            int r4 = android.support.v4.view.t.f
            long r4 = (long) r4
            long r2 = r2 + r4
            r1.sendEmptyMessageAtTime(r8, r2)
            android.view.GestureDetector$OnGestureListener r1 = r13.i
            boolean r1 = r1.onDown(r14)
            r3 = r0 | r1
            goto L_0x004e
        L_0x0133:
            android.os.Handler r0 = r13.h
            int r4 = android.support.v4.view.t.g
            long r4 = (long) r4
            r0.sendEmptyMessageDelayed(r11, r4)
        L_0x013b:
            r0 = r3
            goto L_0x00df
        L_0x013d:
            boolean r0 = r13.l
            if (r0 != 0) goto L_0x004e
            float r0 = r13.r
            float r0 = r0 - r2
            float r4 = r13.s
            float r4 = r4 - r1
            boolean r5 = r13.q
            if (r5 == 0) goto L_0x0154
            android.view.GestureDetector$OnDoubleTapListener r0 = r13.j
            boolean r0 = r0.onDoubleTapEvent(r14)
            r3 = r3 | r0
            goto L_0x004e
        L_0x0154:
            boolean r5 = r13.m
            if (r5 == 0) goto L_0x018f
            float r5 = r13.t
            float r5 = r2 - r5
            int r5 = (int) r5
            float r6 = r13.u
            float r6 = r1 - r6
            int r6 = (int) r6
            int r5 = r5 * r5
            int r6 = r6 * r6
            int r5 = r5 + r6
            int r6 = r13.f97a
            if (r5 <= r6) goto L_0x023c
            android.view.GestureDetector$OnGestureListener r6 = r13.i
            android.view.MotionEvent r7 = r13.o
            boolean r0 = r6.onScroll(r7, r14, r0, r4)
            r13.r = r2
            r13.s = r1
            r13.m = r3
            android.os.Handler r1 = r13.h
            r1.removeMessages(r11)
            android.os.Handler r1 = r13.h
            r1.removeMessages(r8)
            android.os.Handler r1 = r13.h
            r1.removeMessages(r12)
        L_0x0186:
            int r1 = r13.f97a
            if (r5 <= r1) goto L_0x018c
            r13.n = r3
        L_0x018c:
            r3 = r0
            goto L_0x004e
        L_0x018f:
            float r5 = java.lang.Math.abs(r0)
            r6 = 1065353216(0x3f800000, float:1.0)
            int r5 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
            if (r5 >= 0) goto L_0x01a3
            float r5 = java.lang.Math.abs(r4)
            r6 = 1065353216(0x3f800000, float:1.0)
            int r5 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
            if (r5 < 0) goto L_0x004e
        L_0x01a3:
            android.view.GestureDetector$OnGestureListener r3 = r13.i
            android.view.MotionEvent r5 = r13.o
            boolean r3 = r3.onScroll(r5, r14, r0, r4)
            r13.r = r2
            r13.s = r1
            goto L_0x004e
        L_0x01b1:
            r13.k = r3
            android.view.MotionEvent r1 = android.view.MotionEvent.obtain(r14)
            boolean r0 = r13.q
            if (r0 == 0) goto L_0x01e8
            android.view.GestureDetector$OnDoubleTapListener r0 = r13.j
            boolean r0 = r0.onDoubleTapEvent(r14)
            r0 = r0 | r3
        L_0x01c2:
            android.view.MotionEvent r2 = r13.p
            if (r2 == 0) goto L_0x01cb
            android.view.MotionEvent r2 = r13.p
            r2.recycle()
        L_0x01cb:
            r13.p = r1
            android.view.VelocityTracker r1 = r13.w
            if (r1 == 0) goto L_0x01d9
            android.view.VelocityTracker r1 = r13.w
            r1.recycle()
            r1 = 0
            r13.w = r1
        L_0x01d9:
            r13.q = r3
            android.os.Handler r1 = r13.h
            r1.removeMessages(r8)
            android.os.Handler r1 = r13.h
            r1.removeMessages(r12)
            r3 = r0
            goto L_0x004e
        L_0x01e8:
            boolean r0 = r13.l
            if (r0 == 0) goto L_0x01f5
            android.os.Handler r0 = r13.h
            r0.removeMessages(r11)
            r13.l = r3
            r0 = r3
            goto L_0x01c2
        L_0x01f5:
            boolean r0 = r13.m
            if (r0 == 0) goto L_0x0200
            android.view.GestureDetector$OnGestureListener r0 = r13.i
            boolean r0 = r0.onSingleTapUp(r14)
            goto L_0x01c2
        L_0x0200:
            android.view.VelocityTracker r0 = r13.w
            int r2 = android.support.v4.view.MotionEventCompat.getPointerId(r14, r3)
            r4 = 1000(0x3e8, float:1.401E-42)
            int r5 = r13.d
            float r5 = (float) r5
            r0.computeCurrentVelocity(r4, r5)
            float r4 = android.support.v4.view.VelocityTrackerCompat.getYVelocity(r0, r2)
            float r0 = android.support.v4.view.VelocityTrackerCompat.getXVelocity(r0, r2)
            float r2 = java.lang.Math.abs(r4)
            int r5 = r13.c
            float r5 = (float) r5
            int r2 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r2 > 0) goto L_0x022c
            float r2 = java.lang.Math.abs(r0)
            int r5 = r13.c
            float r5 = (float) r5
            int r2 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r2 <= 0) goto L_0x023a
        L_0x022c:
            android.view.GestureDetector$OnGestureListener r2 = r13.i
            android.view.MotionEvent r5 = r13.o
            boolean r0 = r2.onFling(r5, r14, r0, r4)
            goto L_0x01c2
        L_0x0235:
            r13.b()
            goto L_0x004e
        L_0x023a:
            r0 = r3
            goto L_0x01c2
        L_0x023c:
            r0 = r3
            goto L_0x0186
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.t.a(android.view.MotionEvent):boolean");
    }

    private void b() {
        this.h.removeMessages(1);
        this.h.removeMessages(2);
        this.h.removeMessages(3);
        this.w.recycle();
        this.w = null;
        this.q = false;
        this.k = false;
        this.m = false;
        this.n = false;
        if (this.l) {
            this.l = false;
        }
    }

    private void c() {
        this.h.removeMessages(1);
        this.h.removeMessages(2);
        this.h.removeMessages(3);
        this.q = false;
        this.m = false;
        this.n = false;
        if (this.l) {
            this.l = false;
        }
    }

    private boolean a(MotionEvent motionEvent, MotionEvent motionEvent2, MotionEvent motionEvent3) {
        if (!this.n || motionEvent3.getEventTime() - motionEvent2.getEventTime() > ((long) g)) {
            return false;
        }
        int x = ((int) motionEvent.getX()) - ((int) motionEvent3.getX());
        int y = ((int) motionEvent.getY()) - ((int) motionEvent3.getY());
        if ((x * x) + (y * y) < this.b) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void d() {
        this.h.removeMessages(3);
        this.l = true;
        this.i.onLongPress(this.o);
    }
}
