package com.tencent.beacon.event;

import android.content.Context;
import com.tencent.assistant.st.STConst;
import com.tencent.beacon.a.d;
import com.tencent.beacon.a.g;
import com.tencent.beacon.a.h;
import com.tencent.beacon.d.a;
import com.tencent.beacon.f.k;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class b implements l {

    /* renamed from: a  reason: collision with root package name */
    private Context f2133a;
    private boolean b = false;
    private List<i> c;
    private Runnable d = new c(this);
    private Runnable e = new d(this);

    public static com.tencent.beacon.c.d.b b(i iVar) {
        if (iVar == null || !"IP".equals(iVar.b())) {
            return null;
        }
        Map<String, String> e2 = iVar.e();
        if (e2 == null) {
            return null;
        }
        try {
            com.tencent.beacon.c.d.b bVar = new com.tencent.beacon.c.d.b();
            bVar.f2117a = e2.get("A19");
            String str = e2.get("A26");
            if (str == null) {
                str = STConst.ST_DEFAULT_SLOT;
            }
            bVar.e = Long.parseLong(str);
            String[] split = iVar.d().split(":");
            bVar.c = split[0];
            bVar.d = Integer.parseInt(split[1]);
            bVar.b = e2.get("A28");
            bVar.f = iVar.c();
            HashMap hashMap = new HashMap();
            hashMap.put("A33", e2.get("A33"));
            hashMap.put("A3", e2.get("A3"));
            hashMap.put("A20", e2.get("A20"));
            hashMap.put("A74", e2.get("A74"));
            if (e2.get("test") != null) {
                hashMap.put("test", "Y");
            }
            bVar.g = h.a(hashMap);
            return bVar;
        } catch (Throwable th) {
            th.printStackTrace();
            a.d(th.getMessage(), new Object[0]);
            return null;
        }
    }

    public b(Context context) {
        this.f2133a = context;
        this.c = new ArrayList(25);
    }

    private synchronized List<i> b() {
        ArrayList arrayList;
        if (this.c == null || this.c.size() <= 0 || !c()) {
            arrayList = null;
        } else {
            arrayList = new ArrayList();
            arrayList.addAll(this.c);
            this.c.clear();
            a.b(" get MN:" + arrayList.size(), new Object[0]);
        }
        return arrayList;
    }

    public static com.tencent.beacon.c.d.a c(i iVar) {
        if (iVar == null || !"DN".equals(iVar.b())) {
            return null;
        }
        Map<String, String> e2 = iVar.e();
        if (e2 == null) {
            return null;
        }
        try {
            com.tencent.beacon.c.d.a aVar = new com.tencent.beacon.c.d.a();
            aVar.f2116a = e2.get("A19");
            aVar.c = iVar.d();
            aVar.j = e2.get("A34");
            aVar.d = Long.parseLong(e2.get("A35"));
            aVar.f = Long.parseLong(e2.get("A36"));
            aVar.g = Long.parseLong(e2.get("A37"));
            aVar.h = Long.parseLong(e2.get("A38"));
            aVar.b = e2.get("A28");
            aVar.i = e2.get("A39");
            aVar.e = Long.parseLong(e2.get("A40"));
            aVar.k = iVar.c();
            HashMap hashMap = new HashMap();
            hashMap.put("A33", e2.get("A33"));
            hashMap.put("A3", e2.get("A3"));
            hashMap.put("A20", e2.get("A20"));
            hashMap.put("A74", e2.get("A74"));
            if (e2.get("test") != null) {
                hashMap.put("test", "Y");
            }
            aVar.l = h.a(hashMap);
            return aVar;
        } catch (Throwable th) {
            th.printStackTrace();
            a.d(th.getMessage(), new Object[0]);
            return null;
        }
    }

    public final synchronized boolean a(i iVar) {
        boolean z = false;
        synchronized (this) {
            Object[] objArr = new Object[3];
            objArr[0] = iVar == null ? "null" : iVar.d();
            objArr[1] = false;
            objArr[2] = iVar == null ? "null" : Boolean.valueOf(iVar.f());
            a.f(" BF eN:%s   isRT:%b  isCR:%b", objArr);
            if (this.f2133a == null || iVar == null || !this.b) {
                a.d(" err BF 1R", new Object[0]);
            } else if (!c()) {
                a.d(" err BF 2R", new Object[0]);
            } else {
                g g = t.d().g();
                int c2 = g.c();
                long d2 = (long) (g.d() * 1000);
                int size = this.c.size();
                if (size >= c2) {
                    a.d(" err BF 3R! list size:" + size, new Object[0]);
                } else {
                    this.c.add(iVar);
                    if (size + 1 >= c2) {
                        a.f(" BF mN!}", new Object[0]);
                        d.a().a(this.d);
                        d.a().a(102, this.d, d2, d2);
                    }
                    z = true;
                }
            }
        }
        return z;
    }

    public static com.tencent.beacon.c.d.d d(i iVar) {
        if (iVar == null || !"HO".equals(iVar.b())) {
            return null;
        }
        Map<String, String> e2 = iVar.e();
        if (e2 == null) {
            return null;
        }
        try {
            com.tencent.beacon.c.d.d dVar = new com.tencent.beacon.c.d.d();
            dVar.f2119a = e2.get("A19");
            String[] split = e2.get("hostip").split(":");
            dVar.m = Integer.parseInt(split[1]);
            dVar.b = e2.get("A28");
            dVar.c = e2.get("A34");
            dVar.k = split[0];
            dVar.l = iVar.d();
            dVar.d = Long.parseLong(e2.get("A35"));
            dVar.e = Long.parseLong(e2.get("A40"));
            dVar.f = Long.parseLong(e2.get("A36"));
            dVar.g = Long.parseLong(e2.get("A37"));
            dVar.h = Long.parseLong(e2.get("A38"));
            dVar.j = iVar.c();
            dVar.i = e2.get("A39");
            HashMap hashMap = new HashMap();
            hashMap.put("A33", e2.get("A33"));
            hashMap.put("A3", e2.get("A3"));
            hashMap.put("A20", e2.get("A20"));
            hashMap.put("A74", e2.get("A74"));
            dVar.n = h.a(hashMap);
            return dVar;
        } catch (Throwable th) {
            th.printStackTrace();
            a.d(th.getMessage(), new Object[0]);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (!c()) {
            a.c(" err su 1R", new Object[0]);
            return;
        }
        List<i> b2 = b();
        if (b2 != null && b2.size() > 0) {
            Long[] a2 = h.a(this.f2133a, b2);
            k a3 = k.a(this.f2133a);
            if (a2 != null) {
                long e2 = (long) t.d().g().e();
                if (com.tencent.beacon.d.b.a(this.f2133a)) {
                    a.e(" onwifi, so half mSZ " + e2, new Object[0]);
                    e2 /= 2;
                }
                if ((((long) h.p(this.f2133a)) >= e2) && a3.b() && a3.c()) {
                    if (a3.a() >= 10) {
                        a.c(" doUpload request failed 10 times sleep...", new Object[0]);
                        d.a().a(this.e, 600000);
                        k.a(this.f2133a).a(0);
                    } else {
                        this.e.run();
                    }
                    a.e(" max Up", new Object[0]);
                }
            }
        }
    }

    public static com.tencent.beacon.c.b.a e(i iVar) {
        int i;
        if (iVar == null || !"UA".equals(iVar.b())) {
            return null;
        }
        Map<String, String> e2 = iVar.e();
        if (e2 == null) {
            return null;
        }
        try {
            com.tencent.beacon.c.b.a aVar = new com.tencent.beacon.c.b.a();
            aVar.f2113a = e2.get("A19");
            aVar.c = iVar.d();
            aVar.h = iVar.c();
            aVar.b = e2.get("A28");
            aVar.f = Long.parseLong(e2.get("A26"));
            aVar.d = Boolean.parseBoolean(e2.get("A25"));
            aVar.e = Long.parseLong(e2.get("A27"));
            if (iVar.h()) {
                e2.put("C9", new StringBuilder().append(iVar.i()).toString());
            }
            if (iVar.f()) {
                e2.put("C3", new StringBuilder().append(iVar.g()).toString());
            }
            aVar.g = h.a(e2);
            if (iVar.f()) {
                i = 1;
            } else {
                i = 0;
            }
            aVar.i = i;
            a.b("new event record:\neventName:%s\neventResult:%b\neventValue:%s", aVar.c, Boolean.valueOf(aVar.d), aVar.g);
            return aVar;
        } catch (Throwable th) {
            th.printStackTrace();
            a.d(th.getMessage(), new Object[0]);
            return null;
        }
    }

    private synchronized boolean c() {
        return this.b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.a.d.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.beacon.a.d.a(java.lang.Runnable, long):void
      com.tencent.beacon.a.d.a(int, boolean):void */
    public final synchronized void a(boolean z) {
        if (this.b != z) {
            if (z) {
                this.b = z;
                long d2 = (long) (t.d().g().d() * 1000);
                d.a().a(102, this.d, d2, d2);
            } else {
                d.a().a(102, true);
                a();
                this.b = z;
            }
        }
    }

    public static i a(Context context, String str, boolean z, long j, long j2, Map<String, String> map) {
        if (str == null) {
            return null;
        }
        g m = g.m();
        if (m == null) {
            a.d("  CommonInfo have not been Created return null!", new Object[0]);
            return null;
        }
        String a2 = a.a();
        long h = m.h() + new Date().getTime();
        String g = m.g();
        String c2 = com.tencent.beacon.d.b.c(context);
        if (c2 == null) {
            c2 = "null";
        }
        HashMap hashMap = new HashMap();
        if (a.f2132a != null) {
            hashMap.putAll(a.f2132a);
        }
        if (map != null) {
            hashMap.putAll(map);
        }
        hashMap.put("A1", a2);
        hashMap.put(Constants.SOURCE_QQ, a.b());
        hashMap.put("A19", c2);
        hashMap.put("A28", g);
        hashMap.put("A25", new StringBuilder().append(z).toString());
        hashMap.put("A26", new StringBuilder().append(j).toString());
        hashMap.put("A27", new StringBuilder().append(j2).toString());
        hashMap.put("A2", m.i());
        f a3 = f.a(context);
        hashMap.put("A4", a3.c());
        hashMap.put("A6", a3.b());
        hashMap.put("A7", a3.d());
        hashMap.put("A3", com.tencent.beacon.b.a.a(context).a());
        hashMap.put("A23", a3.e());
        hashMap.put("A67", com.tencent.beacon.a.a.i(context));
        hashMap.put("A76", com.tencent.beacon.a.a.a());
        i iVar = new i();
        iVar.b(str);
        iVar.b(h);
        iVar.a("UA");
        iVar.a(hashMap);
        iVar.c(1);
        int i = 0;
        if (j >= 1200000) {
            i = 1;
        }
        if (j2 >= 50000000) {
            i++;
        }
        if (i > 0) {
            iVar.d((long) i);
            iVar.a(true);
            return iVar;
        }
        iVar.d(0);
        iVar.a(false);
        return iVar;
    }
}
