package com.tencent.game.smartcard.component;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.a;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.component.fps.FPSDownloadButton;
import com.tencent.assistantv2.component.fps.FPSTextView;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.component.appdetail.process.s;

/* compiled from: ProGuard */
public class NormalSmartCardAppNodeWithRank extends RelativeLayout {
    private static int i = AstApp.i().getResources().getColor(R.color.rank_sort_txt_top1);
    private static int j = AstApp.i().getResources().getColor(R.color.rank_sort_txt_top2);
    private static int k = AstApp.i().getResources().getColor(R.color.rank_sort_txt_top3);
    private static int l = AstApp.i().getResources().getColor(R.color.rank_sort_txt_normal);

    /* renamed from: a  reason: collision with root package name */
    private FPSTextView f2719a;
    private TXAppIconView b;
    private FPSTextView c;
    private FPSDownloadButton d;
    private ListItemInfoView e;
    private ImageView f;
    private Context g;
    private LayoutInflater h;
    private boolean m = true;

    public NormalSmartCardAppNodeWithRank(Context context) {
        super(context);
        this.h = LayoutInflater.from(context);
        a();
        this.g = context;
    }

    public NormalSmartCardAppNodeWithRank(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.h = LayoutInflater.from(context);
        a();
        this.g = context;
    }

    private void a() {
        this.h.inflate((int) R.layout.rank_item_normal, this);
        this.f2719a = (FPSTextView) findViewById(R.id.sort_text);
        this.b = (TXAppIconView) findViewById(R.id.app_icon_img);
        this.c = (FPSTextView) findViewById(R.id.app_name_txt);
        this.d = (FPSDownloadButton) findViewById(R.id.state_app_btn);
        this.e = (ListItemInfoView) findViewById(R.id.download_info);
        this.f = (ImageView) findViewById(R.id.last_line);
    }

    public void a(SimpleAppModel simpleAppModel, STInfoV2 sTInfoV2, int i2, ListItemInfoView.InfoType infoType, int i3, int i4) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.b.getLayoutParams();
        layoutParams.leftMargin = 0;
        try {
            this.f2719a.setVisibility(0);
            a(this.f2719a, simpleAppModel.al);
        } catch (Throwable th) {
            t.a().b();
        }
        this.b.setLayoutParams(layoutParams);
        if (i3 == 0) {
            if (com.tencent.assistant.utils.t.b > 320) {
            }
            this.f.setVisibility(0);
        } else if (i3 == i4 - 1) {
            this.f.setVisibility(8);
        } else {
            this.f.setVisibility(0);
        }
        a(simpleAppModel, i3, sTInfoV2, infoType);
        setOnClickListener(new b(this, i2, simpleAppModel, sTInfoV2));
    }

    private void a(SimpleAppModel simpleAppModel, int i2, STInfoV2 sTInfoV2, ListItemInfoView.InfoType infoType) {
        if (simpleAppModel != null) {
            this.c.setText(simpleAppModel.d);
            if (this.m) {
                if (1 == (((int) (simpleAppModel.B >> 2)) & 3)) {
                    Drawable drawable = this.g.getResources().getDrawable(R.drawable.appdownload_icon_original);
                    drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                    this.c.setCompoundDrawablePadding(by.b(6.0f));
                    this.c.setCompoundDrawables(null, null, drawable, null);
                } else {
                    this.c.setCompoundDrawables(null, null, null, null);
                }
            }
            a.a(this.g, simpleAppModel, this.c, false);
            this.b.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.d.a(simpleAppModel);
            this.e.a(infoType);
            this.e.a(simpleAppModel);
            if (s.a(simpleAppModel, k.e(simpleAppModel).c)) {
                this.d.setClickable(false);
                return;
            }
            this.d.setClickable(true);
            this.d.a(sTInfoV2);
        }
    }

    public static void a(TextView textView, int i2) {
        textView.setText(String.valueOf(i2));
        TextPaint paint = textView.getPaint();
        if (i2 <= 3) {
            paint.setFakeBoldText(true);
            textView.setTextSize(16.0f);
            if (i2 == 1) {
                textView.setTextColor(i);
            } else if (i2 == 2) {
                textView.setTextColor(j);
            } else if (i2 == 3) {
                textView.setTextColor(k);
            }
        } else {
            paint.setFakeBoldText(false);
            textView.setTextSize(14.0f);
            textView.setTextColor(l);
        }
    }
}
