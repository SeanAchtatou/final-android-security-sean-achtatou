package com.tencent.game.smartcard.b;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.CardItem;
import com.tencent.assistant.protocol.jce.CardsInfo;
import com.tencent.assistant.smartcard.d.a;
import com.tencent.assistant.smartcard.d.y;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class b extends a {
    public List<y> e;
    public int f;

    public boolean a(byte b, JceStruct jceStruct) {
        if (!(jceStruct instanceof CardsInfo)) {
            return false;
        }
        CardsInfo cardsInfo = (CardsInfo) jceStruct;
        this.j = b;
        if (cardsInfo != null) {
            this.l = cardsInfo.d;
            this.p = "全部";
            this.o = cardsInfo.b;
            this.f = cardsInfo.c;
        }
        if (this.e == null) {
            this.e = new ArrayList();
        } else {
            this.e.clear();
        }
        if (cardsInfo.f1185a != null) {
            Iterator<CardItem> it = cardsInfo.f1185a.iterator();
            while (it.hasNext()) {
                y yVar = new y();
                yVar.f1768a = k.a(it.next());
                if (yVar.f1768a != null) {
                    k.a(yVar.f1768a);
                }
                this.e.add(yVar);
            }
        }
        return true;
    }

    public List<SimpleAppModel> a() {
        if (this.e == null || this.e.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(this.e.size());
        for (y yVar : this.e) {
            arrayList.add(yVar.f1768a);
        }
        return arrayList;
    }
}
