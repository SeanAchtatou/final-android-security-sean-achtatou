package com.tencent.game.b;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewStub;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.NpcCfg;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ah;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
public class c extends a {
    /* access modifiers changed from: private */
    public NpcCfg c = null;
    private TXImageView d;
    private View e;
    private b f;

    public c(Context context, ViewStub viewStub, NpcCfg npcCfg, b bVar) {
        super(context, viewStub);
        this.c = npcCfg;
        this.f = bVar;
        b();
    }

    /* access modifiers changed from: protected */
    public void b() {
        e();
    }

    public void a() {
        if (c()) {
            f();
        }
    }

    /* access modifiers changed from: private */
    public boolean c() {
        boolean a2 = this.f.a(this.c);
        if (this.d == null) {
            return false;
        }
        if (a2) {
            this.d.setVisibility(0);
            return true;
        }
        this.d.setVisibility(8);
        return false;
    }

    /* access modifiers changed from: private */
    public void d() {
        ah.a().postDelayed(new d(this), 1000);
    }

    private void e() {
        this.b.setLayoutResource(R.layout.game_stub_treasureboxentry);
        this.e = this.b.inflate();
        this.d = (TXImageView) this.e.findViewById(R.id.treasure_box_entry);
        this.d.setOnClickListener(new e(this, (BaseActivity) this.f2636a));
    }

    private void f() {
        this.d.updateImageView(g(), -1, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
        a(false);
    }

    private String g() {
        if (this.c == null || this.c.f == null || this.c.f.size() <= 0) {
            return Constants.STR_EMPTY;
        }
        return this.c.f.get(0);
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        STInfoV2 buildSTInfo;
        this.f.a(this.c, z);
        if (!z && this.f2636a != null && (this.f2636a instanceof BaseActivity) && (buildSTInfo = STInfoBuilder.buildSTInfo(this.f2636a, 100)) != null) {
            buildSTInfo.scene = STConst.ST_PAGE_GAME_NPC;
            buildSTInfo.slotId = "05_001";
            l.a(buildSTInfo);
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        Bundle bundle = new Bundle();
        if (this.f2636a instanceof BaseActivity) {
            bundle.putSerializable(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, Integer.valueOf(((BaseActivity) this.f2636a).f()));
        }
        b.b(this.f2636a, this.c.e, bundle);
    }
}
