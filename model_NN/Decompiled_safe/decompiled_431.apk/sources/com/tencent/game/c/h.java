package com.tencent.game.c;

import com.tencent.assistant.localres.y;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.protocol.jce.CardItemWrapper;
import com.tencent.assistant.smartcard.b.c;
import com.tencent.assistant.smartcard.c.p;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.game.d.a.f;
import com.tencent.game.d.aa;
import com.tencent.pangu.model.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class h extends y<f> implements NetworkMonitor.ConnectivityChangeListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public long f2646a = 0;
    private aa b = aa.a();
    private k c = new k(this, null);
    /* access modifiers changed from: private */
    public volatile boolean d = false;
    private int e;
    private List<Long> f = new ArrayList();
    private List<b> g = new ArrayList();
    private APN h = APN.NO_NETWORK;

    public h() {
        t.a().a(this);
    }

    public void a() {
        this.b.register(this.c);
        this.b.d();
    }

    public int b() {
        return this.b.f();
    }

    public int c() {
        return this.b.e();
    }

    public com.tencent.assistant.model.b d() {
        com.tencent.assistant.model.b b2 = this.b.b();
        b2.c(this.g);
        b2.b((List<SimpleAppModel>) null);
        return b2;
    }

    public void e() {
        if (this.f2646a > 0 && System.currentTimeMillis() - this.f2646a > m.a().ai()) {
            c();
        }
    }

    public void a(j jVar, c cVar) {
        if (jVar != null) {
            int i = this.e;
            if (jVar.e) {
                i = 0;
            }
            List<b> a2 = a(jVar.h, i, cVar);
            if (jVar.e) {
                this.g.clear();
                this.g.addAll(a2);
            }
            a(new i(this, jVar, a2));
        }
    }

    public synchronized List<b> a(List<CardItemWrapper> list, int i, c cVar) {
        ArrayList arrayList;
        ArrayList arrayList2 = new ArrayList();
        if (list == null || list.size() <= 0) {
            arrayList = arrayList2;
        } else {
            if (i == 0) {
                this.f.clear();
            }
            ArrayList arrayList3 = new ArrayList(list);
            int i2 = 0;
            while (i2 < arrayList3.size()) {
                b bVar = new b();
                List<n> a2 = cVar.a(i);
                if (a2 != null && a2.size() > 0) {
                    bVar.g = p.a().a(a2, this.f, i);
                    if (bVar.g != null) {
                        p.a().a(bVar.g);
                        bVar.b = 2;
                        List<Long> d2 = bVar.g.d();
                        if (d2 != null) {
                            this.f.addAll(d2);
                        }
                    }
                }
                if (bVar.g == null) {
                    while (true) {
                        int i3 = i2;
                        CardItemWrapper cardItemWrapper = (CardItemWrapper) arrayList3.get(i3);
                        if (cardItemWrapper.f1184a != 0) {
                            i2 = i3 + 1;
                            break;
                        }
                        SimpleAppModel a3 = k.a(cardItemWrapper.b);
                        if (!this.f.contains(Long.valueOf(a3.f938a)) && !this.f.contains(Long.valueOf(a3.f938a))) {
                            bVar.c = a3;
                            bVar.b = 1;
                            this.f.add(Long.valueOf(a3.f938a));
                        }
                        i2 = i3 + 1;
                        if (bVar.c == null) {
                            if (bVar.d == null) {
                                if (i2 >= arrayList3.size()) {
                                    break;
                                }
                            } else {
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
                if (bVar != null && bVar.h()) {
                    arrayList2.add(bVar);
                }
                i++;
            }
            if (arrayList2 != null) {
                this.e += arrayList2.size();
            }
            arrayList = arrayList2;
        }
        return arrayList;
    }

    public void onConnected(APN apn) {
        if (!this.d) {
            this.b.d();
        }
    }

    public void onDisconnected(APN apn) {
        this.h = apn;
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        this.h = apn2;
    }
}
