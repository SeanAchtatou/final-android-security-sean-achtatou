package com.tencent.game.activity;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXRefreshGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.activity.a;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.assistantv2.component.banner.floatheader.FloatBannerViewSwitcher;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.l;
import com.tencent.game.adapter.GameListPageAdapter;
import com.tencent.game.b.b;
import com.tencent.game.c.h;
import com.tencent.game.d.a.f;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class w extends a implements ITXRefreshListViewListener, f {
    b R;
    boolean S = true;
    private final String T = "GameTabActivity:";
    private LinearLayout U;
    private TXRefreshGetMoreListView V;
    private ViewStub W;
    /* access modifiers changed from: private */
    public NormalErrorRecommendPage X = null;
    /* access modifiers changed from: private */
    public LoadingView Y;
    /* access modifiers changed from: private */
    public h Z = new h();
    /* access modifiers changed from: private */
    public GameListPageAdapter aa = null;
    /* access modifiers changed from: private */
    public SmartListAdapter.BannerType ab = SmartListAdapter.BannerType.None;
    /* access modifiers changed from: private */
    public FloatBannerViewSwitcher ac;
    /* access modifiers changed from: private */
    public ViewStub ad;
    private boolean ae = false;
    private Handler af;
    /* access modifiers changed from: private */
    public boolean ag = false;

    public void d(Bundle bundle) {
        super.d(bundle);
        Log.d("YYB5_0", "GameTabActivity:onCreate------------3");
        this.U = new LinearLayout(this.P);
        this.U.addView(new ImageView(this.P), new LinearLayout.LayoutParams(-1, -1));
        a(this.U);
        this.Z.a(this);
        this.af = new Handler();
    }

    private void b(View view) {
        this.V = (TXRefreshGetMoreListView) view.findViewById(R.id.list);
        this.aa = new GameListPageAdapter(this.P, this.V, this.Z.d());
        this.aa.a(true);
        this.V.setAdapter(this.aa);
        this.V.setRefreshListViewListener(this);
        this.V.setListSelector(17170445);
        this.V.setVisibility(8);
        this.V.setDivider(null);
        TXRefreshGetMoreListView tXRefreshGetMoreListView = this.V;
        TXRefreshGetMoreListView tXRefreshGetMoreListView2 = this.V;
        tXRefreshGetMoreListView2.getClass();
        tXRefreshGetMoreListView.setOnTouchEventListener(new x(this, tXRefreshGetMoreListView2));
        ae aeVar = new ae(this, null);
        this.V.setIScrollerListener(aeVar);
        this.aa.a(aeVar);
        this.W = (ViewStub) view.findViewById(R.id.error_stub);
        this.Y = (LoadingView) view.findViewById(R.id.loading);
        this.ac = (FloatBannerViewSwitcher) view.findViewById(R.id.float_banner_switcher);
        this.ac.a(this.V.getListView(), SmartListAdapter.SmartListType.GamePage.ordinal());
        this.ad = (ViewStub) view.findViewById(R.id.treasurebox_stub);
        this.R = new b();
    }

    private void H() {
        XLog.d("leobi", "initFirstPage=====================");
        l.a((int) STConst.ST_PAGE_GAME_POPULAR, CostTimeSTManager.TIMETYPE.START, System.currentTimeMillis());
        this.Z.a();
        J();
    }

    private void d(int i) {
        if (this.X == null) {
            I();
        }
        this.X.setErrorType(i);
        if (this.V != null) {
            this.V.setVisibility(8);
        }
        this.X.setVisibility(0);
    }

    private void I() {
        this.W.inflate();
        this.X = (NormalErrorRecommendPage) h().findViewById(R.id.error);
        this.X.setButtonClickListener(new y(this));
        this.X.setIsAutoLoading(true);
    }

    public void d(boolean z) {
        if (this.S) {
            this.S = false;
            this.U.removeAllViews();
            View inflate = this.Q.inflate((int) R.layout.act4, (ViewGroup) null);
            this.U.addView(inflate);
            this.U.requestLayout();
            this.U.forceLayout();
            this.U.invalidate();
            b(inflate);
            H();
        } else {
            this.Z.e();
        }
        if (!this.ag) {
            ah.a().post(new z(this));
        }
        if (this.aa != null) {
            this.aa.i();
            this.aa.notifyDataSetChanged();
        }
        if (this.ac != null) {
            this.ac.f();
        }
        if (this.ag) {
            ah.a().post(new aa(this));
        } else {
            ah.a().postDelayed(new ab(this), 2500);
        }
        Log.d("YYB5_0", "GameTabActivity:onResume------------3");
    }

    private void J() {
        TemporaryThreadManager.get().start(new ac(this));
    }

    public void k() {
        super.k();
        if (this.aa != null) {
            this.aa.h();
        }
        if (this.ac != null) {
            this.ac.g();
        }
        Log.d("YYB5_0", "GameTabActivity:onPause------------3");
    }

    public int G() {
        return STConst.ST_PAGE_GAME_POPULAR;
    }

    public w() {
        super(MainActivity.t());
    }

    public void D() {
        if (this.aa != null) {
            this.aa.h();
        }
        Log.e("YYB5_0", "GameTabActivity:onPageTurnBackground------------3:::");
    }

    public int E() {
        return 0;
    }

    public int F() {
        return 4;
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (TXScrollViewBase.ScrollState.ScrollState_FromEnd == scrollState) {
            this.Z.b();
        }
        if (TXScrollViewBase.ScrollState.ScrollState_FromStart == scrollState) {
            this.Z.c();
        }
    }

    public void a(int i, int i2, boolean z, byte[] bArr, boolean z2, ArrayList<ColorCardItem> arrayList, List<com.tencent.pangu.model.b> list) {
        boolean z3;
        XLog.d("jasonnzhang", "onRecommendDataLoadedFinished");
        if (this.Y != null) {
            this.Y.setVisibility(8);
        }
        if (i2 == 0 || i == -1) {
            if (this.X != null) {
                this.X.setVisibility(8);
            }
            this.V.setVisibility(0);
            if (list == null || list.size() == 0) {
                l.a((int) STConst.ST_PAGE_GAME_POPULAR, CostTimeSTManager.TIMETYPE.CANCEL, System.currentTimeMillis());
                d(10);
                return;
            }
            if (arrayList != null) {
                Iterator<ColorCardItem> it = arrayList.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    ColorCardItem next = it.next();
                    if (next.c.equals("礼包")) {
                        next.setTag(new Object());
                        break;
                    }
                }
            }
            this.aa.a(z2, list, (List<com.tencent.pangu.component.banner.f>) null, arrayList);
            if (z2) {
                this.ac.a(arrayList);
                if (i == -1) {
                    l.a((int) STConst.ST_PAGE_GAME_POPULAR, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                    this.V.onRefreshComplete(false, z, a((int) R.string.refresh_fail));
                } else {
                    l.a((int) STConst.ST_PAGE_GAME_POPULAR, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                    this.V.onRefreshComplete(true, z, null);
                }
                if (!this.ae) {
                    this.ae = true;
                    Iterator<ColorCardItem> it2 = arrayList.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            z3 = false;
                            break;
                        }
                        ColorCardItem next2 = it2.next();
                        XLog.d("jasonnzhang", "color card:" + next2.c + " guide:" + ((int) next2.h));
                        if (next2.h > 0) {
                            z3 = true;
                            break;
                        }
                    }
                    if (z3) {
                        XLog.d("jasonnzhang", "guide :" + m.a().a("key_game_user_guide", 0));
                        z3 = m.a().a("key_game_user_guide", 0) < 2;
                    }
                    if (z3) {
                        this.af.postDelayed(new ad(this), 2000);
                        return;
                    }
                    return;
                }
                return;
            }
            this.V.onRefreshComplete(z, true);
        } else if (z2) {
            l.a((int) STConst.ST_PAGE_GAME_POPULAR, CostTimeSTManager.TIMETYPE.CANCEL, System.currentTimeMillis());
            if (-800 == i2) {
                d(30);
            } else {
                d(20);
            }
        } else {
            this.V.onRefreshComplete(z, false);
        }
    }

    public void C() {
        B();
    }

    /* access modifiers changed from: private */
    public void e(boolean z) {
        if (this.aa != null) {
            this.aa.d(z);
        }
    }
}
