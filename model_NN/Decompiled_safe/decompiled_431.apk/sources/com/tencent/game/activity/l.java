package com.tencent.game.activity;

import android.support.v4.view.ViewPager;
import com.tencent.assistantv2.activity.a;

/* compiled from: ProGuard */
public class l implements ViewPager.OnPageChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameRankActivity f2625a;

    public l(GameRankActivity gameRankActivity) {
        this.f2625a = gameRankActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.TabBarView.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistantv2.component.TabBarView.a(android.content.Context, android.util.AttributeSet):void
      com.tencent.assistantv2.component.TabBarView.a(int, float):void
      com.tencent.assistantv2.component.TabBarView.a(int, boolean):void */
    public void onPageSelected(int i) {
        if (this.f2625a.x != null) {
            this.f2625a.x.a(i, true);
            if (!(this.f2625a.w == i || this.f2625a.v.a(this.f2625a.w) == null)) {
                ((a) this.f2625a.v.a(this.f2625a.w)).D();
            }
            ((a) this.f2625a.v.a(i)).C();
            int unused = this.f2625a.w = i;
            this.f2625a.d(i);
        }
    }

    public void onPageScrolled(int i, float f, int i2) {
        if (this.f2625a.x != null) {
            this.f2625a.x.a(i, f);
        }
    }

    public void onPageScrollStateChanged(int i) {
    }
}
