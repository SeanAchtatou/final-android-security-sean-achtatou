package com.tencent.game.e;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.SimpleAppInfo;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.e;
import com.tencent.game.activity.GameDesktopShortActivity;
import com.tencent.game.d.p;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class a {
    public static void a() {
        new p().b();
    }

    public static boolean b() {
        return m.a().N();
    }

    public static void a(List<SimpleAppInfo> list) {
        Context applicationContext = AstApp.i().getApplicationContext();
        if (applicationContext != null) {
            Bitmap b = b(list);
            if (b == null) {
                b = ((BitmapDrawable) applicationContext.getResources().getDrawable(R.drawable.desktop_icon_total)).getBitmap();
            }
            Intent intent = new Intent(applicationContext, GameDesktopShortActivity.class);
            intent.setFlags(67108864);
            e.a(applicationContext, applicationContext.getResources().getString(R.string.game_desktop_my_game), intent);
            e.a(applicationContext, b, applicationContext.getResources().getString(R.string.game_desktop_my_game), intent);
            TemporaryThreadManager.get().start(new b());
        }
    }

    private static Bitmap b(List<SimpleAppInfo> list) {
        PackageInfo d;
        int i = 3;
        if (list == null || list.size() == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (SimpleAppInfo next : list) {
            if (!(next == null || (d = e.d(next.f, 0)) == null || (d.applicationInfo.flags & 1) != 0)) {
                arrayList.add(next);
            }
        }
        if (arrayList.size() == 0) {
            return null;
        }
        try {
            Bitmap decodeResource = BitmapFactory.decodeResource(AstApp.i().getApplicationContext().getResources(), R.drawable.desktop_icon_total);
            int height = decodeResource.getHeight();
            int rint = (int) Math.rint(((double) height) * 0.392d);
            int rint2 = (int) Math.rint(((double) height) * 0.071d);
            ArrayList arrayList2 = new ArrayList();
            if (arrayList.size() <= 3) {
                i = arrayList.size();
            }
            for (int i2 = 0; i2 < i; i2++) {
                SimpleAppInfo simpleAppInfo = (SimpleAppInfo) arrayList.get(i2);
                if (simpleAppInfo != null && !TextUtils.isEmpty(simpleAppInfo.f)) {
                    try {
                        Drawable applicationIcon = AstApp.i().getPackageManager().getApplicationIcon(simpleAppInfo.f);
                        if (applicationIcon != null && (applicationIcon instanceof BitmapDrawable)) {
                            XLog.d("hamlingong", "iconDrawable != null");
                            arrayList2.add(a(((BitmapDrawable) applicationIcon).getBitmap(), rint, rint));
                        }
                    } catch (Exception e) {
                    }
                }
            }
            return a(decodeResource, arrayList2, rint, rint2);
        } catch (OutOfMemoryError e2) {
            return null;
        }
    }

    private static Bitmap a(Bitmap bitmap, List<Bitmap> list, int i, int i2) {
        RectF rectF;
        if (bitmap == null || list == null) {
            return null;
        }
        int width = bitmap.getWidth();
        XLog.d("hamlingong", "src.width: " + width);
        int height = bitmap.getHeight();
        XLog.d("hamlingong", "src.height: " + height);
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, (Paint) null);
        int i3 = 0;
        Iterator<Bitmap> it = list.iterator();
        while (true) {
            int i4 = i3;
            if (it.hasNext()) {
                Bitmap next = it.next();
                if (next != null) {
                    switch (i4) {
                        case 0:
                            rectF = new RectF((float) i2, (float) i2, (float) (i2 + i), (float) (i2 + i));
                            break;
                        case 1:
                            rectF = new RectF((float) (i2 + i + i2), (float) i2, (float) (i2 + i + i2 + i), (float) (i2 + i));
                            break;
                        case 2:
                            rectF = new RectF((float) i2, (float) (i2 + i + i2), (float) (i2 + i), (float) (i2 + i + i2 + i));
                            break;
                        default:
                            rectF = new RectF((float) i2, (float) i2, (float) (i2 + i), (float) (i2 + i));
                            break;
                    }
                    i4++;
                    canvas.drawBitmap(next, (Rect) null, rectF, (Paint) null);
                }
                i3 = i4;
            } else {
                canvas.save(31);
                canvas.restore();
                return createBitmap;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap a(Bitmap bitmap, int i, int i2) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(((float) i) / ((float) width), ((float) i2) / ((float) height));
        return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
    }
}
