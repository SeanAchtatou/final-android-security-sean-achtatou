package com.tencent.game.component;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.by;

/* compiled from: ProGuard */
public class GameBannerGuideView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f2650a = false;
    private TextView b;

    public GameBannerGuideView(Context context) {
        super(context);
        c();
    }

    public GameBannerGuideView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        c();
    }

    public GameBannerGuideView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        c();
    }

    private void c() {
        try {
            LayoutInflater.from(getContext()).inflate((int) R.layout.game_new_guide, this);
            this.b = (TextView) findViewById(R.id.game_new_tip);
        } catch (Resources.NotFoundException e) {
            XLog.e("GameBannerGuideView", "Resources.NotFoundException: " + e.getMessage());
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        int i5;
        super.onSizeChanged(i, i2, i3, i4);
        int a2 = (getContext().getResources().getDisplayMetrics().widthPixels - (by.a(getContext(), 9.0f) * 2)) / 4;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        if ((a2 - i) / 2 > 0) {
            i5 = (a2 - i) / 2;
        } else {
            i5 = 0;
        }
        layoutParams.setMargins(i5 + by.a(getContext(), 9.0f), 0, 0, 0);
        setLayoutParams(layoutParams);
    }

    public void a(int i) {
        int a2 = (getContext().getResources().getDisplayMetrics().widthPixels - (by.a(getContext(), 9.0f) * 2)) / 4;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.setMargins(by.a(getContext(), 9.0f) + ((a2 - getWidth()) / 2 > 0 ? (a2 - getWidth()) / 2 : 0), i > by.a(getContext(), 150.0f) ? by.a(getContext(), 110.0f) : 0, 0, 0);
        setLayoutParams(layoutParams);
    }

    public void a() {
        setVisibility(0);
        f2650a = true;
        Animation loadAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.game_guide_in);
        loadAnimation.setInterpolator(new OvershootInterpolator());
        startAnimation(loadAnimation);
    }

    public void b() {
        Animation loadAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.game_guide_out);
        startAnimation(loadAnimation);
        loadAnimation.setAnimationListener(new a(this));
    }
}
