package com.tencent.game.component;

import android.view.View;
import com.tencent.assistant.component.categorydetail.SmoothShrinkListener;
import com.tencent.assistant.utils.by;

/* compiled from: ProGuard */
class d implements SmoothShrinkListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameCategoryDetailListView f2660a;

    d(GameCategoryDetailListView gameCategoryDetailListView) {
        this.f2660a = gameCategoryDetailListView;
    }

    public void onStop() {
        int f;
        int a2 = by.a(this.f2660a.getContext(), 4.0f);
        int i = 0;
        for (int i2 = 0; i2 < this.f2660a.G; i2++) {
            View childAt = this.f2660a.getListView().getChildAt(i2);
            if (childAt != null) {
                i += childAt.getHeight();
            }
        }
        if (this.f2660a.G <= 1) {
            f = a2 + 0;
        } else {
            f = this.f2660a.H - i;
        }
        if (this.f2660a.y != null) {
            this.f2660a.y.hideEmptyHeader();
        }
        this.f2660a.getListView().setSelectionFromTop(this.f2660a.G, f);
        this.f2660a.getListView().postInvalidate();
    }

    public void onStart() {
    }

    public void onShrink(int i) {
    }
}
