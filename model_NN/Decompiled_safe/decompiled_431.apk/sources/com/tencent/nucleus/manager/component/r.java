package com.tencent.nucleus.manager.component;

import android.view.ViewGroup;
import android.view.animation.Transformation;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;

/* compiled from: ProGuard */
class r implements q {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ScaleRelativeLayout f2873a;

    r(ScaleRelativeLayout scaleRelativeLayout) {
        this.f2873a = scaleRelativeLayout;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.component.ScaleRelativeLayout.a(com.tencent.nucleus.manager.component.ScaleRelativeLayout, boolean):boolean
     arg types: [com.tencent.nucleus.manager.component.ScaleRelativeLayout, int]
     candidates:
      com.tencent.nucleus.manager.component.ScaleRelativeLayout.a(com.tencent.nucleus.manager.component.ScaleRelativeLayout, int):int
      com.tencent.nucleus.manager.component.ScaleRelativeLayout.a(com.tencent.nucleus.manager.component.ScaleRelativeLayout, boolean):boolean */
    public void a(float f, Transformation transformation, float f2, float f3) {
        ViewGroup.LayoutParams layoutParams;
        if (this.f2873a.c != null) {
            if (f == 0.0f) {
                boolean unused = this.f2873a.f = false;
                this.f2873a.c.a();
            }
            if (f == 1.0f && !this.f2873a.f) {
                boolean unused2 = this.f2873a.f = true;
                this.f2873a.c.b();
                this.f2873a.clearAnimation();
                this.f2873a.requestLayout();
            }
            this.f2873a.c.a(f, transformation);
        }
        if (this.f2873a.g == 0) {
            if (this.f2873a.a()) {
                transformation.setAlpha(f);
            }
            if (!(f == 1.0f || (layoutParams = this.f2873a.getLayoutParams()) == null)) {
                layoutParams.width = (int) (this.f2873a.d * f);
                layoutParams.height = (int) (this.f2873a.e * f);
                this.f2873a.setLayoutParams(layoutParams);
            }
        }
        if (this.f2873a.g == -1) {
            if (this.f2873a.a()) {
                transformation.setAlpha(1.0f - f);
            }
            if (f != 1.0f) {
                ViewGroup.LayoutParams layoutParams2 = this.f2873a.getLayoutParams();
                if (layoutParams2 != null) {
                    layoutParams2.width = (int) (this.f2873a.d * (1.0f - f));
                    layoutParams2.height = (int) (this.f2873a.e * (1.0f - f));
                    this.f2873a.setLayoutParams(layoutParams2);
                }
            } else {
                this.f2873a.setVisibility(8);
                int unused3 = this.f2873a.g = -1;
            }
        }
        if (this.f2873a.b.hasEnded()) {
            XLog.i(this.f2873a.f2842a, ">>onEnded>>");
            ah.a().postDelayed(new s(this), 50);
        }
    }
}
