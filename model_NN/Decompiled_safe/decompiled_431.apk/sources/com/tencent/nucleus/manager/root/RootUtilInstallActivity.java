package com.tencent.nucleus.manager.root;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.ad;
import com.tencent.assistant.module.callback.h;
import com.tencent.assistant.module.k;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.e;
import com.tencent.assistant.utils.m;
import com.tencent.assistantv2.component.AppStateButtonV5;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.a;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean;
import java.util.HashMap;

/* compiled from: ProGuard */
public class RootUtilInstallActivity extends BaseActivity implements UIEventListener, NetworkMonitor.ConnectivityChangeListener, h {
    /* access modifiers changed from: private */
    public SimpleAppModel A;
    /* access modifiers changed from: private */
    public ad B = new ad();
    private boolean C = false;
    private String D = "000116083139343433333633";
    private Handler E = new a(this);
    /* access modifiers changed from: private */
    public Context n;
    private RelativeLayout u;
    private SecondNavigationTitleViewV5 v;
    private ImageView w;
    /* access modifiers changed from: private */
    public AppStateButtonV5 x;
    /* access modifiers changed from: private */
    public Button y;
    private LoadingView z;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_root_util_install_layout);
        t();
        u();
        this.n = this;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        try {
            if (this.v != null) {
                this.v.m();
            }
            super.onPause();
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.v != null) {
            this.v.l();
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
        AstApp.i().k().removeUIEventListener(1013, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, this);
        t.a().b(this);
    }

    public int f() {
        return STConst.ST_PAGE_SOFTWARE_UNINSTALL_DOWNLOAD_ROOT_UTIL;
    }

    private void t() {
        this.u = (RelativeLayout) findViewById(R.id.layout_main);
        this.v = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.v.b(getResources().getString(R.string.get_root_title));
        this.v.a(this);
        this.v.d();
        this.w = (ImageView) findViewById(R.id.image_banner);
        try {
            Bitmap a2 = m.a(R.drawable.guide_to_king_root);
            if (a2 != null && !a2.isRecycled()) {
                this.w.setImageBitmap(a2);
            }
        } catch (Throwable th) {
            t.a().b();
        }
        this.x = (AppStateButtonV5) findViewById(R.id.appdownload_button);
        this.x.a((int) getResources().getDimension(R.dimen.app_detail_float_bar_btn_height));
        this.x.b(16);
        this.y = (Button) findViewById(R.id.btn_no_net);
        this.z = (LoadingView) findViewById(R.id.loading);
        b(true);
    }

    private void u() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
        AstApp.i().k().addUIEventListener(1013, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, this);
        t.a().a(this);
        Bundle extras = getIntent().getExtras();
        if (!(extras == null || extras.getString("king_root_channelid") == null)) {
            this.D = extras.getString("king_root_channelid");
        }
        if (this.A == null) {
            this.A = new SimpleAppModel();
            this.A.c = AppConst.KING_ROOT_PKGNAME;
            this.A.ac = this.D;
        }
        this.x.a(this.A);
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(AppConst.KING_ROOT_PKGNAME);
        boolean z2 = false;
        if (localApkInfo == null) {
            z2 = e.a(AppConst.KING_ROOT_PKGNAME);
        }
        if (localApkInfo != null || z2) {
            this.E.sendEmptyMessage(10002);
        } else {
            this.E.sendEmptyMessage(EventDispatcherEnum.PLUGIN_EVENT_LOGIN_START);
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        int i = 0;
        if (this.u != null) {
            this.u.setVisibility(z2 ? 8 : 0);
        }
        if (this.z != null) {
            LoadingView loadingView = this.z;
            if (!z2) {
                i = 8;
            }
            loadingView.setVisibility(i);
        }
    }

    /* access modifiers changed from: private */
    public CharSequence a(DownloadInfo downloadInfo) {
        String format;
        if (downloadInfo.isSslUpdate()) {
            String a2 = at.a(downloadInfo.sllFileSize);
            format = String.format(getResources().getString(R.string.mobile_rubbish_clear_btn_update), a2);
        } else if (downloadInfo.isUpdate == 1) {
            String a3 = at.a(downloadInfo.fileSize);
            format = String.format(getResources().getString(R.string.mobile_rubbish_clear_btn_update), a3);
        } else {
            String a4 = at.a(downloadInfo.fileSize);
            format = String.format(getResources().getString(R.string.mobile_rubbish_clear_btn_download), a4);
        }
        SpannableString spannableString = new SpannableString(format);
        spannableString.setSpan(new AbsoluteSizeSpan(14, true), 4, format.length(), 33);
        return spannableString;
    }

    public void onGetAppInfoSuccess(int i, int i2, AppSimpleDetail appSimpleDetail) {
        if (appSimpleDetail == null) {
            this.E.sendEmptyMessage(10004);
            return;
        }
        this.A = k.a(appSimpleDetail);
        this.E.sendEmptyMessage(10003);
    }

    public void onGetAppInfoFail(int i, int i2) {
        this.E.sendEmptyMessage(10004);
    }

    /* access modifiers changed from: private */
    public void v() {
        if (this.A != null) {
            DownloadInfo a2 = DownloadProxy.a().a(this.A);
            StatInfo buildDownloadSTInfo = STInfoBuilder.buildDownloadSTInfo(this, this.A);
            buildDownloadSTInfo.recommendId = this.A.y;
            if (a2 != null && a2.needReCreateInfo(this.A)) {
                DownloadProxy.a().b(a2.downloadTicket);
                a2 = null;
            }
            if (a2 == null) {
                a2 = DownloadInfo.createDownloadInfo(this.A, buildDownloadSTInfo, this.x);
            } else {
                a2.updateDownloadInfoStatInfo(buildDownloadSTInfo);
            }
            switch (d.f2972a[k.d(this.A).ordinal()]) {
                case 1:
                case 2:
                    a.a().a(a2);
                    if (!this.C) {
                        w();
                        this.C = true;
                        return;
                    }
                    return;
                case 3:
                case 4:
                    a.a().b(a2.downloadTicket);
                    return;
                case 5:
                    a.a().b(a2);
                    return;
                case 6:
                    a.a().d(a2);
                    return;
                case 7:
                    a.a().c(a2);
                    finish();
                    return;
                case 8:
                case 9:
                    a.a().a(a2);
                    return;
                case 10:
                    Toast.makeText(this, (int) R.string.unsupported, 0).show();
                    return;
                case 11:
                    Toast.makeText(this, (int) R.string.tips_slicent_install, 0).show();
                    return;
                case 12:
                    Toast.makeText(this, (int) R.string.tips_slicent_uninstall, 0).show();
                    return;
                default:
                    return;
            }
        }
    }

    private void w() {
        String str;
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", com.tencent.assistant.utils.t.g());
        if (this.D.equals("000116083139343433333633")) {
            str = "KingRootFromUninstall";
        } else {
            str = "KingRootFromAccelerate";
        }
        com.tencent.beacon.event.a.a(str, true, -1, -1, hashMap, true);
        XLog.d("beacon", "beacon report >> Event code : " + str + ", paras : " + hashMap.toString());
    }

    public void onConnected(APN apn) {
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(AppConst.KING_ROOT_PKGNAME);
        boolean z2 = false;
        if (localApkInfo == null) {
            z2 = e.a(AppConst.KING_ROOT_PKGNAME);
        }
        if (localApkInfo != null || z2) {
            this.E.sendEmptyMessage(10002);
        } else {
            this.E.sendEmptyMessage(EventDispatcherEnum.PLUGIN_EVENT_LOGIN_START);
        }
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
    }

    public void handleUIEvent(Message message) {
        XLog.d("miles", "RootUtilInstallActivity.  handleUIEvent. msg.what=" + message.what);
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_INSTALL:
            case 1013:
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC:
                String str = Constants.STR_EMPTY;
                String str2 = Constants.STR_EMPTY;
                if (message.obj instanceof String) {
                    str = (String) message.obj;
                    str2 = (String) message.obj;
                } else if (message.obj instanceof InstallUninstallTaskBean) {
                    InstallUninstallTaskBean installUninstallTaskBean = (InstallUninstallTaskBean) message.obj;
                    String str3 = installUninstallTaskBean.downloadTicket;
                    str2 = installUninstallTaskBean.packageName;
                    str = str3;
                }
                if ((!TextUtils.isEmpty(str) && str.equals(this.A.q())) || (!TextUtils.isEmpty(str2) && str2.equals(AppConst.KING_ROOT_PKGNAME))) {
                    this.E.sendEmptyMessage(10002);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_APP_UNINSTALL:
                if ((message.obj instanceof String) && ((String) message.obj).equals(AppConst.KING_ROOT_PKGNAME)) {
                    this.E.sendEmptyMessage(EventDispatcherEnum.PLUGIN_EVENT_LOGIN_START);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
