package com.tencent.nucleus.manager.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMASwitchButtonClickListener;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;

/* compiled from: ProGuard */
public class SwitchButton extends RelativeLayout implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private ImageView f2844a;
    private ImageView b;
    private ImageView c;
    private ImageView d;
    private ImageView e;
    private TextView f;
    private TextView g;
    private Animation h;
    private Animation i;
    private Animation j;
    private Animation k;
    private int l;
    private int m;
    private Context n;
    private LayoutInflater o;
    /* access modifiers changed from: private */
    public View p;
    private int q;
    private aa r = new x(this);
    private aa s = new y(this);
    /* access modifiers changed from: private */
    public boolean t = false;
    /* access modifiers changed from: private */
    public OnTMASwitchButtonClickListener u;
    /* access modifiers changed from: private */
    public boolean v = false;

    public SwitchButton(Context context) {
        super(context);
        this.n = context;
        this.p = this;
        this.q = 150;
        c();
    }

    public SwitchButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.n = context;
        this.p = this;
        this.q = 150;
        c();
    }

    public int a() {
        return this.q;
    }

    private void c() {
        setOnClickListener(this);
        this.o = LayoutInflater.from(this.n);
        this.o.inflate((int) R.layout.setting_switch_view_layout, this);
        this.c = (ImageView) findViewById(R.id.img_default_bg);
        this.f2844a = (ImageView) findViewById(R.id.img_open_bg);
        this.b = (ImageView) findViewById(R.id.img_close_bg);
        this.d = (ImageView) findViewById(R.id.img_switch_left);
        this.e = (ImageView) findViewById(R.id.img_switch_right);
        this.f = (TextView) findViewById(R.id.text_left);
        this.g = (TextView) findViewById(R.id.text_right);
        a(this.n.getString(R.string.setting_left_title), this.n.getString(R.string.setting_right_title));
    }

    private void d() {
        this.l = getWidth();
        if (this.d.getVisibility() == 0) {
            this.m = this.d.getWidth();
        } else {
            this.m = this.e.getWidth();
        }
        XLog.i("ighuang", "switch-width= " + this.l + "cycle-width= " + this.m);
        this.j = AnimationUtils.loadAnimation(getContext(), R.anim.setting_switch_scale_out);
        this.k = AnimationUtils.loadAnimation(getContext(), R.anim.setting_switch_scale_in);
        this.j.setDuration((long) this.q);
        this.k.setDuration((long) this.q);
        this.h = new TranslateAnimation(0.0f, (float) (this.l - this.m), 0.0f, 0.0f);
        this.h.setDuration((long) this.q);
        this.h.setInterpolator(new AccelerateInterpolator());
        this.h.setAnimationListener(this.r);
        this.i = new TranslateAnimation(0.0f, (float) (-(this.l - this.m)), 0.0f, 0.0f);
        this.i.setDuration((long) this.q);
        this.i.setInterpolator(new AccelerateInterpolator());
        this.i.setAnimationListener(this.s);
    }

    public void a(boolean z) {
        this.t = z;
        e();
    }

    public void a(String str, String str2) {
        this.f.setText(str);
        this.g.setText(str2);
    }

    public boolean b() {
        return this.t;
    }

    public void b(boolean z) {
        this.t = z;
        h();
    }

    private void e() {
        if (this.t) {
            f();
        } else {
            g();
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        this.b.clearAnimation();
        this.d.setVisibility(4);
        this.e.setVisibility(0);
        this.b.setVisibility(4);
        this.f.setVisibility(0);
        this.g.setVisibility(4);
        this.f2844a.setVisibility(0);
        XLog.i("ighuang_auto", "setOnView_finish");
    }

    /* access modifiers changed from: private */
    public void g() {
        this.b.clearAnimation();
        this.d.setVisibility(0);
        this.e.setVisibility(4);
        this.b.setVisibility(0);
        this.f.setVisibility(4);
        this.g.setVisibility(0);
        this.f2844a.setVisibility(4);
        XLog.i("ighuang_auto", "setOffView_finish");
    }

    private void h() {
        d();
        if (this.t) {
            this.d.startAnimation(this.h);
            this.b.setVisibility(0);
            this.b.startAnimation(this.j);
            this.g.setVisibility(4);
            this.f2844a.setVisibility(0);
            return;
        }
        this.e.startAnimation(this.i);
        this.b.setVisibility(0);
        this.b.startAnimation(this.k);
        this.f.setVisibility(4);
    }

    public void onClick(View view) {
        b(!b());
        ah.a().postDelayed(new z(this), (long) this.q);
    }

    public void a(OnTMASwitchButtonClickListener onTMASwitchButtonClickListener) {
        this.u = onTMASwitchButtonClickListener;
        this.v = true;
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        if (z) {
            this.c.setVisibility(8);
            this.b.setVisibility(0);
            this.f2844a.setVisibility(0);
            return;
        }
        this.c.setVisibility(0);
        this.b.setVisibility(4);
        this.f2844a.setVisibility(4);
    }
}
