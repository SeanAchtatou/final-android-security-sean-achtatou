package com.tencent.nucleus.manager.backgroundscan;

import com.tencent.assistant.utils.XLog;
import com.tencent.nucleus.manager.backgroundscan.BackgroundScanManager;
import com.tencent.securemodule.service.ISecureModuleService;

/* compiled from: ProGuard */
class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ISecureModuleService f2834a;
    final /* synthetic */ BackgroundScanManager b;

    m(BackgroundScanManager backgroundScanManager, ISecureModuleService iSecureModuleService) {
        this.b = backgroundScanManager;
        this.f2834a = iSecureModuleService;
    }

    public void run() {
        this.f2834a.unregisterCloudScanListener(this.b.e, this.b.f2819a);
        this.b.i.put((byte) 6, BackgroundScanManager.SStatus.none);
        XLog.e("BackgroundScan", "<scan> virus scan timeout !!!");
    }
}
