package com.tencent.nucleus.manager.main;

import android.view.animation.Animation;

/* compiled from: ProGuard */
class ab implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AssistantTabActivity f2931a;

    ab(AssistantTabActivity assistantTabActivity) {
        this.f2931a = assistantTabActivity;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.f2931a.J.setVisibility(8);
        if (this.f2931a.bh > 0) {
            AssistantTabActivity.n(this.f2931a);
            this.f2931a.G();
        }
    }
}
