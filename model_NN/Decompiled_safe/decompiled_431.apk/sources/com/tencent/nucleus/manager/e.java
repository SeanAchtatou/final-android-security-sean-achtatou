package com.tencent.nucleus.manager;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.XLog;
import com.tencent.nucleus.manager.spaceclean.SpaceScanManager;

/* compiled from: ProGuard */
class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MobileManagerInstallActivity f2882a;

    e(MobileManagerInstallActivity mobileManagerInstallActivity) {
        this.f2882a = mobileManagerInstallActivity;
    }

    public void run() {
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME);
        boolean z = false;
        if (localApkInfo == null) {
            z = com.tencent.assistant.utils.e.a(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME);
        }
        if (localApkInfo == null && !z) {
            XLog.d("miles", "MobileManagerInstallActivity >> 手机管家未安装");
            this.f2882a.J.sendEmptyMessage(-1);
        } else if (!SpaceScanManager.a().e()) {
            XLog.d("miles", "MobileManagerInstallActivity >> 手机管家服务没有绑定，尝试再绑定一次服务");
            SpaceScanManager.a().c();
        } else {
            XLog.d("miles", "MobileManagerInstallActivity >> 手机管家已绑定");
            this.f2882a.J.sendEmptyMessage(20);
        }
    }
}
