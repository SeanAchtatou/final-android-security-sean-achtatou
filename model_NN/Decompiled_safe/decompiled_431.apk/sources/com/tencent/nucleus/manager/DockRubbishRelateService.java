package com.tencent.nucleus.manager;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;
import com.tencent.assistant.Global;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.t;
import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.manager.spaceclean.SpaceScanManager;
import com.tencent.tmsecurelite.optimize.f;
import java.util.HashMap;

/* compiled from: ProGuard */
public class DockRubbishRelateService extends Service {

    /* renamed from: a  reason: collision with root package name */
    Handler f2728a = ah.a("dockRubbishScan");
    Runnable b = new d(this);
    /* access modifiers changed from: private */
    public long c = 0;
    /* access modifiers changed from: private */
    public long d = 0;
    /* access modifiers changed from: private */
    public long e = 0;
    /* access modifiers changed from: private */
    public long f = 0;
    /* access modifiers changed from: private */
    public long g = 0;
    private a h = new b(this);
    /* access modifiers changed from: private */
    public f i = new c(this);

    static /* synthetic */ long f(DockRubbishRelateService dockRubbishRelateService, long j) {
        long j2 = dockRubbishRelateService.d + j;
        dockRubbishRelateService.d = j2;
        return j2;
    }

    static /* synthetic */ long g(DockRubbishRelateService dockRubbishRelateService, long j) {
        long j2 = dockRubbishRelateService.e + j;
        dockRubbishRelateService.e = j2;
        return j2;
    }

    static /* synthetic */ long h(DockRubbishRelateService dockRubbishRelateService, long j) {
        long j2 = dockRubbishRelateService.f + j;
        dockRubbishRelateService.f = j2;
        return j2;
    }

    static /* synthetic */ long i(DockRubbishRelateService dockRubbishRelateService, long j) {
        long j2 = dockRubbishRelateService.g + j;
        dockRubbishRelateService.g = j2;
        return j2;
    }

    static /* synthetic */ long j(DockRubbishRelateService dockRubbishRelateService, long j) {
        long j2 = dockRubbishRelateService.c + j;
        dockRubbishRelateService.c = j2;
        return j2;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        XLog.d("DockRubbish", "DockRubbishRelateService >> onCreate.");
        super.onCreate();
        SpaceScanManager.a().a(this.h);
    }

    public void onDestroy() {
        super.onDestroy();
        SpaceScanManager.a().b(this.h);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.DockRubbishRelateService.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.nucleus.manager.DockRubbishRelateService.a(com.tencent.nucleus.manager.DockRubbishRelateService, long):long
      com.tencent.nucleus.manager.DockRubbishRelateService.a(com.tencent.nucleus.manager.DockRubbishRelateService, java.lang.String):void
      com.tencent.nucleus.manager.DockRubbishRelateService.a(int, boolean):void */
    public int onStartCommand(Intent intent, int i2, int i3) {
        int i4;
        XLog.d("DockRubbish", "DockRubbishRelateService >> onStartCommand.");
        if (intent == null) {
            return 2;
        }
        if ("com.tencent.android.qqdownloader.action.START_RUBBISH_SCAN".equals(intent.getAction())) {
            c();
            a(Constants.STR_EMPTY);
            SpaceScanManager.ManagerAvaliableState l = SpaceScanManager.a().l();
            if (SpaceScanManager.ManagerAvaliableState.NOT_INSTALLED == l) {
                XLog.d("DockRubbish", "DockRubbishRelateService >> 手机管家未安装");
                i4 = -1;
            } else if (SpaceScanManager.ManagerAvaliableState.VERSION_LOW == l) {
                XLog.d("DockRubbish", "DockRubbishRelateService >> 手机管家版本过低");
                i4 = -2;
            } else if (SpaceScanManager.a().m()) {
                XLog.d("DockRubbish", "DockRubbishRelateService >> 已有另一个扫描进行中，请稍后再试");
                i4 = -3;
            } else if (!SpaceScanManager.a().e()) {
                XLog.d("DockRubbish", "DockRubbishRelateService >> 管家服务未绑定，开始绑定..");
                SpaceScanManager.a().c();
                i4 = 1;
            } else {
                XLog.d("DockRubbish", "DockRubbishRelateService >> 管家服务已绑定，开始扫描...");
                this.c = 0;
                this.d = 0;
                this.e = 0;
                this.f = 0;
                this.g = 0;
                a();
                SpaceScanManager.a().a(this.i);
                i4 = 1;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("[status]\nresultcode=").append(i4);
            a(sb.toString());
            if (i4 != 1) {
                a(i4, false);
                stopSelf();
            }
        } else {
            stopSelf();
        }
        return super.onStartCommand(intent, i2, i3);
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        XLog.d("DockRubbish", "write result to file.\n" + str);
        boolean writeToAppData = FileUtil.writeToAppData("rubbishscanresult", str, 1);
        if (!TextUtils.isEmpty(str)) {
            HashMap hashMap = new HashMap();
            hashMap.put("B1", Global.getPhoneGuidAndGen());
            hashMap.put("B2", Global.getQUAForBeacon());
            hashMap.put("B3", t.g());
            XLog.d("beacon", "beacon report >> WriteRubbishScanResult. " + hashMap.toString());
            a.a("WriteRubbishScanResult", writeToAppData, -1, -1, hashMap, true);
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        this.f2728a.postDelayed(this.b, 180000);
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.f2728a != null && this.b != null) {
            this.f2728a.removeCallbacks(this.b);
        }
    }

    private void c() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", t.g());
        a.a("DockRubbishScanTimes", true, -1, -1, hashMap, true);
        XLog.d("beacon", "beacon report >> event: DockRubbishScanTimes, params : " + hashMap.toString());
    }

    /* access modifiers changed from: private */
    public void a(int i2, boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", i2 + Constants.STR_EMPTY);
        hashMap.put("B2", Global.getPhoneGuidAndGen());
        hashMap.put("B3", Global.getQUAForBeacon());
        hashMap.put("B4", t.g());
        a.a("DockRubbishScanResult", z, -1, -1, hashMap, true);
        XLog.d("beacon", "beacon report >> event: DockRubbishScanResult, params : " + hashMap.toString());
    }
}
