package com.tencent.nucleus.manager.floatingwindow;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class FloatWindowReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        XLog.i("floatingwindow", "<FloatWindowReceiver> onReceive. action = " + action);
        if ("android.intent.action.SCREEN_ON".equals(action)) {
            n.a().b();
        } else if ("android.intent.action.SCREEN_OFF".equals(action)) {
            n.a().d();
        }
    }
}
