package com.tencent.nucleus.manager.component;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;
import android.view.ViewStub;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.component.FooterView;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorPage;
import com.tencent.assistant.component.SideBar;
import com.tencent.assistant.component.txscrollview.TXRefreshListView;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.t;
import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.manager.apkuninstall.UserInstalledAppListAdapter;
import com.tencent.pangu.manager.au;
import com.tencent.pangu.utils.installuninstall.p;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* compiled from: ProGuard */
public class UserAppListView extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    private Context f2846a;
    /* access modifiers changed from: private */
    public TXRefreshListView b;
    private SideBar c;
    /* access modifiers changed from: private */
    public FooterView d;
    private TextView e = null;
    private LoadingView f;
    private ViewStub g = null;
    private NormalErrorPage h = null;
    /* access modifiers changed from: private */
    public UserInstalledAppListAdapter i;
    private ap j;
    /* access modifiers changed from: private */
    public boolean k = false;
    /* access modifiers changed from: private */
    public boolean l = false;
    /* access modifiers changed from: private */
    public List<LocalApkInfo> m = Collections.synchronizedList(new ArrayList());
    /* access modifiers changed from: private */
    public Set<String> n = new HashSet();
    private int o;

    public UserAppListView(Context context) {
        super(context);
        this.f2846a = context;
        g();
    }

    public UserAppListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f2846a = context;
        g();
    }

    private void g() {
        this.j = new ap(this);
        LayoutInflater.from(this.f2846a).inflate((int) R.layout.installed_applist_layout, this);
        this.b = (TXRefreshListView) findViewById(R.id.user_applist);
        this.b.setOnScrollListener(this.j);
        this.b.setDivider(null);
        this.c = (SideBar) findViewById(R.id.sidrbar);
        this.d = (FooterView) findViewById(R.id.foot_view);
        this.c.setTextView(this.e);
        this.c.setOnTouchingLetterChangedListener(new am(this));
        this.d.updateContent(this.f2846a.getString(R.string.app_admin_one_key_uninstall));
        this.d.setFooterViewEnable(false);
        this.d.setOnFooterViewClickListener(new ao(this));
        this.f = (LoadingView) findViewById(R.id.loading);
        this.g = (ViewStub) findViewById(R.id.error_stub);
        this.i = new UserInstalledAppListAdapter(this.f2846a);
        this.i.a(this.j);
        this.b.setAdapter(this.i);
        this.f.setVisibility(0);
        this.b.setVisibility(8);
        this.c.setVisibility(8);
        this.d.setVisibility(8);
    }

    public void a(List<LocalApkInfo> list, int i2) {
        this.o = i2;
        if (list == null || list.isEmpty()) {
            h();
            return;
        }
        if (i2 == 2) {
            this.c.setVisibility(0);
        }
        this.b.setVisibility(0);
        this.d.setVisibility(0);
        this.f.setVisibility(8);
        this.i.a(list, i2);
        if (this.h != null) {
            this.h.setVisibility(8);
        }
    }

    private void h() {
        if (this.h == null) {
            this.g.inflate();
            this.h = (NormalErrorPage) findViewById(R.id.error);
        }
        this.h.setErrorType(1);
        this.h.setErrorHint(getResources().getString(R.string.no_user_apps));
        this.h.setErrorImage(R.drawable.emptypage_pic_03);
        this.h.setErrorHintTextColor(getResources().getColor(R.color.common_listiteminfo));
        this.h.setErrorHintTextSize(getResources().getDimension(R.dimen.appadmin_empty_page_text_size));
        this.h.setErrorTextVisibility(8);
        this.h.setErrorHintVisibility(0);
        this.h.setFreshButtonVisibility(8);
        this.h.setVisibility(0);
        this.f.setVisibility(8);
        this.b.setVisibility(8);
        this.c.setVisibility(8);
        this.d.setVisibility(8);
    }

    public void a() {
        this.c.setVisibility(8);
        this.i.a(true);
    }

    public void b() {
        this.c.setVisibility(0);
        this.i.a(false);
    }

    public void a(TextView textView) {
        this.e = textView;
        if (this.c != null) {
            this.c.setTextView(this.e);
        }
    }

    public void c() {
        if (this.i != null) {
            this.i.notifyDataSetChanged();
        }
    }

    public void a(int i2) {
        this.b.setSelection(i2);
    }

    public List<LocalApkInfo> d() {
        return this.m;
    }

    public void a(LocalApkInfo localApkInfo) {
        boolean contains = this.m.contains(localApkInfo);
        if (localApkInfo.mIsSelect && !contains) {
            this.m.add(localApkInfo);
        } else if (!localApkInfo.mIsSelect && contains) {
            this.m.remove(localApkInfo);
        }
        i();
    }

    public void a(Handler handler) {
        if (this.i != null) {
            this.i.a(handler);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.apkuninstall.UserInstalledAppListAdapter.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.tencent.nucleus.manager.apkuninstall.UserInstalledAppListAdapter.a(int, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.nucleus.manager.apkuninstall.UserInstalledAppListAdapter.a(android.widget.TextView, long):void
      com.tencent.nucleus.manager.apkuninstall.UserInstalledAppListAdapter.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, int):void
      com.tencent.nucleus.manager.apkuninstall.UserInstalledAppListAdapter.a(boolean, boolean):void */
    public boolean b(LocalApkInfo localApkInfo) {
        this.m.remove(localApkInfo);
        this.n.remove(localApkInfo.mPackageName);
        LocalApkInfo e2 = e();
        if (e2 == null || !this.l) {
            this.k = false;
            this.l = false;
            if (this.i != null) {
                this.i.a(this.k, false);
            }
            i();
        } else {
            this.k = true;
            if (this.i != null) {
                this.i.a(this.k, true);
            }
            i();
            c(e2);
        }
        return this.k;
    }

    /* access modifiers changed from: private */
    public void c(LocalApkInfo localApkInfo) {
        if (!p.a().a(localApkInfo.mPackageName)) {
            au.a().c(localApkInfo.mAppName);
            au.a().d(localApkInfo.mPackageName);
            p.a().a(this.f2846a, String.valueOf(localApkInfo.mAppid), localApkInfo.mPackageName, (long) localApkInfo.mVersionCode, localApkInfo.mAppName, localApkInfo.mLocalFilePath, true, true);
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        long j2;
        if (this.k) {
            this.d.updateContent(this.f2846a.getString(R.string.uninstalling));
            this.d.setFooterViewEnable(false);
            return;
        }
        int size = this.m.size();
        long j3 = 0;
        Iterator<LocalApkInfo> it = this.m.iterator();
        while (true) {
            j2 = j3;
            if (!it.hasNext()) {
                break;
            }
            j3 = j2 + it.next().occupySize;
        }
        if (this.d != null) {
            String c2 = at.c(j2);
            String string = this.f2846a.getString(R.string.app_admin_one_key_uninstall);
            if (size > 0) {
                this.d.setFooterViewEnable(true);
                this.d.updateContent(string, " " + String.format(this.f2846a.getString(R.string.apkmgr_delete_format), Integer.valueOf(size), c2));
                return;
            }
            this.d.setFooterViewEnable(false);
            this.d.updateContent(string);
        }
    }

    public LocalApkInfo e() {
        if (this.m.size() > 0) {
            for (LocalApkInfo next : this.m) {
                if (!this.n.contains(next.mPackageName)) {
                    return next;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", i2 + Constants.STR_EMPTY);
        hashMap.put("B2", this.o + Constants.STR_EMPTY);
        hashMap.put("B3", Global.getPhoneGuidAndGen());
        hashMap.put("B4", Global.getQUAForBeacon());
        hashMap.put("B5", t.g());
        a.a("batchUninstallUserApp", true, -1, -1, hashMap, true);
        XLog.d("beacon", "beacon report >> event: batchUninstallUserApp, params : " + hashMap.toString());
    }

    /* access modifiers changed from: private */
    public float j() {
        try {
            Field declaredField = AbsListView.class.getDeclaredField("mVelocityTracker");
            declaredField.setAccessible(true);
            try {
                VelocityTracker velocityTracker = (VelocityTracker) declaredField.get(this.b.getListView());
                velocityTracker.computeCurrentVelocity(1000);
                return velocityTracker.getYVelocity();
            } catch (IllegalArgumentException e2) {
                e2.printStackTrace();
                return 0.0f;
            } catch (IllegalAccessException e3) {
                e3.printStackTrace();
                return 0.0f;
            } catch (Exception e4) {
                e4.printStackTrace();
                return 0.0f;
            }
        } catch (NoSuchFieldException e5) {
            e5.printStackTrace();
            return 0.0f;
        }
    }

    /* access modifiers changed from: private */
    public boolean k() {
        return Math.abs(j()) < ((float) (ViewConfiguration.get(getContext()).getScaledMinimumFlingVelocity() * 50));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.apkuninstall.UserInstalledAppListAdapter.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.tencent.nucleus.manager.apkuninstall.UserInstalledAppListAdapter.a(int, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.nucleus.manager.apkuninstall.UserInstalledAppListAdapter.a(android.widget.TextView, long):void
      com.tencent.nucleus.manager.apkuninstall.UserInstalledAppListAdapter.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, int):void
      com.tencent.nucleus.manager.apkuninstall.UserInstalledAppListAdapter.a(boolean, boolean):void */
    public void f() {
        this.k = false;
        if (this.i != null) {
            this.i.a(this.k, true);
        }
        i();
    }
}
