package com.tencent.nucleus.manager.floatingwindow;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/* compiled from: ProGuard */
public class Rotate3dAnimation extends Animation {

    /* renamed from: a  reason: collision with root package name */
    private final float f2889a;
    private final float b;
    private final float c;
    private final float d;
    private final float e;
    private final boolean f;
    private Camera g;
    private RotateAxis h = RotateAxis.X;

    /* compiled from: ProGuard */
    public enum RotateAxis {
        X,
        Y
    }

    public Rotate3dAnimation(float f2, float f3, float f4, float f5, float f6, boolean z) {
        this.f2889a = f2;
        this.b = f3;
        this.c = f4;
        this.d = f5;
        this.e = f6;
        this.f = z;
    }

    public void initialize(int i, int i2, int i3, int i4) {
        super.initialize(i, i2, i3, i4);
        this.g = new Camera();
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f2, Transformation transformation) {
        float f3 = this.f2889a;
        float f4 = f3 + ((this.b - f3) * f2);
        float f5 = this.c;
        float f6 = this.d;
        Camera camera = this.g;
        Matrix matrix = transformation.getMatrix();
        camera.save();
        if (this.f) {
            camera.translate(0.0f, 0.0f, this.e * f2);
        } else {
            camera.translate(0.0f, 0.0f, this.e * (1.0f - f2));
        }
        if (this.h == RotateAxis.X) {
            camera.rotateX(f4);
        } else {
            camera.rotateY(f4);
        }
        camera.getMatrix(matrix);
        camera.restore();
        matrix.preTranslate(-f5, -f6);
        matrix.postTranslate(f5, f6);
        super.applyTransformation(f2, transformation);
    }

    public void a(RotateAxis rotateAxis) {
        this.h = rotateAxis;
    }
}
