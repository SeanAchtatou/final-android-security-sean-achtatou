package com.tencent.nucleus.manager.floatingwindow;

import android.view.animation.Animation;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.t;

/* compiled from: ProGuard */
class a implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FloatWindowBigView f2895a;

    a(FloatWindowBigView floatWindowBigView) {
        this.f2895a = floatWindowBigView;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.f2895a.v.setVisibility(0);
        try {
            this.f2895a.B.setBackgroundResource(R.drawable.admin_launch_circle_light);
        } catch (Throwable th) {
            t.a().b();
        }
        this.f2895a.B.startAnimation(this.f2895a.C);
    }
}
