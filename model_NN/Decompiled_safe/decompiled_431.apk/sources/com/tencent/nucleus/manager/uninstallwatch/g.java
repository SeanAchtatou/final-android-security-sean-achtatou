package com.tencent.nucleus.manager.uninstallwatch;

import android.content.Intent;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.utils.r;
import com.tencent.open.SocialConstants;

/* compiled from: ProGuard */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Intent f3054a;
    final /* synthetic */ e b;

    g(e eVar, Intent intent) {
        this.b = eVar;
        this.f3054a = intent;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.uninstallwatch.e.a(com.tencent.nucleus.manager.uninstallwatch.e, boolean):boolean
     arg types: [com.tencent.nucleus.manager.uninstallwatch.e, int]
     candidates:
      com.tencent.nucleus.manager.uninstallwatch.e.a(com.tencent.nucleus.manager.uninstallwatch.e, android.content.Context):void
      com.tencent.nucleus.manager.uninstallwatch.e.a(android.content.Context, boolean):void
      com.tencent.nucleus.manager.uninstallwatch.e.a(com.tencent.nucleus.manager.uninstallwatch.e, boolean):boolean */
    public void run() {
        if (!this.b.e) {
            boolean unused = this.b.e = true;
            if (this.f3054a != null) {
                String stringExtra = this.f3054a.getStringExtra("stopped");
                if (!TextUtils.isEmpty(stringExtra) && stringExtra.equals("true")) {
                    e.a("forcestop");
                    boolean unused2 = this.b.e = false;
                    return;
                }
            }
            if (!AstApp.f201a) {
                r.c("wake");
                if (AstApp.b) {
                    e.a(SocialConstants.PARAM_RECEIVER);
                }
            } else if (AstApp.b) {
                e.a(SocialConstants.PARAM_RECEIVER);
            } else {
                e.a("other");
            }
            boolean unused3 = this.b.e = false;
        }
    }
}
