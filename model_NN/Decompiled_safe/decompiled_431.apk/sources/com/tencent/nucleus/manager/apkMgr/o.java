package com.tencent.nucleus.manager.apkMgr;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class o extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f2777a;
    final /* synthetic */ m b;

    o(m mVar, int i) {
        this.b = mVar;
        this.f2777a = i;
    }

    public void onTMAClick(View view) {
        this.b.f2775a.f2763a.a(this.f2777a, !this.b.f2775a.h.isSelected());
        this.b.f2775a.f2763a.notifyDataSetChanged();
        if (this.b.f2775a.f2763a.f2764a != null) {
            this.b.f2775a.f2763a.f2764a.sendMessage(this.b.f2775a.f2763a.f2764a.obtainMessage(110005, new LocalApkInfo()));
        }
    }

    public STInfoV2 getStInfo() {
        return STInfoBuilder.buildSTInfo(this.b.f2775a.getContext(), 200);
    }
}
