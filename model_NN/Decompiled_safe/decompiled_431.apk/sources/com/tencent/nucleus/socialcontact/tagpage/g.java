package com.tencent.nucleus.socialcontact.tagpage;

/* compiled from: ProGuard */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LoadingView f3203a;

    g(LoadingView loadingView) {
        this.f3203a = loadingView;
    }

    public void run() {
        if (this.f3203a.c <= 100) {
            LoadingView.a(this.f3203a, 3);
            this.f3203a.b.a(this.f3203a.c);
            if (this.f3203a.e != null) {
                this.f3203a.e.postDelayed(this.f3203a.g, 10);
                return;
            }
            return;
        }
        this.f3203a.b.a(0);
        if (this.f3203a.e != null) {
            this.f3203a.e.removeCallbacks(this.f3203a.g);
        }
    }
}
