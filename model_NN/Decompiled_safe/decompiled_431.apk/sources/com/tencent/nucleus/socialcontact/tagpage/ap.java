package com.tencent.nucleus.socialcontact.tagpage;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.tencent.assistant.protocol.jce.AppTagInfo;
import com.tencent.assistant.utils.XLog;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/* compiled from: ProGuard */
public class ap {
    public static void a(Context context, AppTagInfo appTagInfo, String str, Intent intent) {
        if (context != null && appTagInfo != null && !TextUtils.isEmpty(appTagInfo.f1161a)) {
            StringBuffer stringBuffer = new StringBuffer("tmast://tagpage?tagID=");
            stringBuffer.append(appTagInfo.f1161a);
            stringBuffer.append("&tagName=");
            stringBuffer.append(appTagInfo.b);
            stringBuffer.append("&tagSubTitle=");
            stringBuffer.append(appTagInfo.e);
            if (!TextUtils.isEmpty(str)) {
                try {
                    stringBuffer.append("&firstIconUrl=");
                    stringBuffer.append(URLEncoder.encode(str, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse(stringBuffer.toString()));
            intent2.setFlags(268435456);
            if (!(intent == null || intent.getExtras() == null)) {
                intent2.putExtra("from_activity", intent.getExtras().getString("from_activity"));
            }
            context.startActivity(intent2);
            XLog.i("TagPageUtils", "[openNewTagPage] ---> url : " + stringBuffer.toString());
        }
    }
}
