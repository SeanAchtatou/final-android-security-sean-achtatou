package com.tencent.nucleus.socialcontact.tagpage;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.utils.XLog;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class ac implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f3178a;
    final /* synthetic */ int b;
    final /* synthetic */ TagPageCardAdapter c;

    ac(TagPageCardAdapter tagPageCardAdapter, SimpleAppModel simpleAppModel, int i) {
        this.c = tagPageCardAdapter;
        this.f3178a = simpleAppModel;
        this.b = i;
    }

    public void onClick(View view) {
        String str = this.f3178a.az.f1125a;
        XLog.i("TagPageCradAdapter", "[onItemClick] ---> url = " + str);
        Bundle bundle = new Bundle();
        if (this.c.b instanceof BaseActivity) {
            bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.c.b).f());
        }
        bundle.putSerializable("com.tencent.assistant.ACTION_URL", this.f3178a.az);
        b.b(this.c.b, str, bundle);
        this.c.a(this.c.a(this.b) + "_09", Constants.STR_EMPTY, 200);
    }
}
