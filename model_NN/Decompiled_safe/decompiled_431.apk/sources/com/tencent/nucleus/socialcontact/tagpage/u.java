package com.tencent.nucleus.socialcontact.tagpage;

import android.view.View;
import android.widget.AbsListView;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.by;

/* compiled from: ProGuard */
class u implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AbsListView f3214a;
    final /* synthetic */ TagPageActivity b;

    u(TagPageActivity tagPageActivity, AbsListView absListView) {
        this.b = tagPageActivity;
        this.f3214a = absListView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter.a(com.tencent.nucleus.socialcontact.tagpage.ai, int, boolean):void
     arg types: [com.tencent.nucleus.socialcontact.tagpage.ai, int, int]
     candidates:
      com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter.a(com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter, com.tencent.nucleus.socialcontact.tagpage.ai, int):void
      com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter.a(java.lang.String, java.lang.String, int):void
      com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter.a(com.tencent.nucleus.socialcontact.tagpage.ai, int, boolean):void */
    public void run() {
        XLog.i("TagPageActivity", "view.getFirstVisiblePosition() = " + this.f3214a.getFirstVisiblePosition() + ", view.getLastVisiblePosition() = " + this.f3214a.getLastVisiblePosition());
        int childCount = this.f3214a.getChildCount();
        int[] iArr = new int[2];
        int c = by.c() / 2;
        for (int i = 0; i < childCount; i++) {
            View childAt = this.f3214a.getChildAt(i);
            if (childAt != null) {
                childAt.getLocationOnScreen(iArr);
                XLog.i("TagPageActivity", "y[" + i + "] = " + iArr[1]);
                if (iArr[1] < c && iArr[1] + childAt.getHeight() > c) {
                    if (childAt.getTag() instanceof ai) {
                        ai unused = this.b.T = (ai) childAt.getTag();
                        if (TagPageActivity.n != (this.f3214a.getFirstVisiblePosition() + i) - 1) {
                            TagPageActivity.n = (this.f3214a.getFirstVisiblePosition() + i) - 1;
                            XLog.d("TagPageActivity", "nLastPosition = " + TagPageActivity.n + ", TagPageCardAdapter.index = " + TagPageCardAdapter.f3173a);
                            if (TagPageActivity.n != TagPageCardAdapter.f3173a && aq.b()) {
                                this.b.C.a(this.b.T, TagPageActivity.n, true);
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    return;
                }
            }
        }
    }
}
