package com.tencent.nucleus.socialcontact.comment;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class CommentProgressBar extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private static int f3078a = 5;
    private TextView[] b = new TextView[f3078a];
    private ProgressBar[] c = new ProgressBar[f3078a];
    private TextView[] d = new TextView[f3078a];
    private int[] e = {R.id.text1, R.id.text2, R.id.text3, R.id.text4, R.id.text5};
    private int[] f = {R.id.bar1, R.id.bar2, R.id.bar3, R.id.bar4, R.id.bar5};
    private int[] g = {R.drawable.comment_progressbar1, R.drawable.comment_progressbar2, R.drawable.comment_progressbar3, R.drawable.comment_progressbar4, R.drawable.comment_progressbar5};
    private int[] h = {R.id.count1, R.id.count2, R.id.count3, R.id.count4, R.id.count5};
    private ArrayList<Integer> i = new ArrayList<>();
    private ArrayList<String> j = new ArrayList<>();
    private boolean k = false;
    private LayoutInflater l;

    public CommentProgressBar(Context context) {
        super(context);
        a(context);
    }

    public CommentProgressBar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context);
    }

    public CommentProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    private void a(Context context) {
        this.l = LayoutInflater.from(context);
        View inflate = this.l.inflate((int) R.layout.comment_detail_progress_layout, this);
        for (int i2 = 0; i2 < f3078a; i2++) {
            this.c[i2] = (ProgressBar) inflate.findViewById(this.f[i2]);
        }
        for (int i3 = 0; i3 < f3078a; i3++) {
            this.d[i3] = (TextView) inflate.findViewById(this.h[i3]);
        }
    }
}
