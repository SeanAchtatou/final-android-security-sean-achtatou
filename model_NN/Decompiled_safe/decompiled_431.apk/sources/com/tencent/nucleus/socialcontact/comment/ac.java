package com.tencent.nucleus.socialcontact.comment;

import android.widget.TextView;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.ReplyDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.List;

/* compiled from: ProGuard */
class ac implements y {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentReplyListActivity f3085a;

    ac(CommentReplyListActivity commentReplyListActivity) {
        this.f3085a = commentReplyListActivity;
    }

    public void a(int i, int i2, boolean z, long j, List<ReplyDetail> list, boolean z2, long j2) {
        aj ajVar;
        TextView textView;
        if (i2 == 0) {
            this.f3085a.S.setVisibility(0);
            boolean unused = this.f3085a.ad = z2;
            if (!(list == null || this.f3085a.H == null)) {
                this.f3085a.H.a(z, list);
            }
            if (this.f3085a.H.getCount() > 0) {
                this.f3085a.u();
                this.f3085a.F.onRefreshComplete(z2, true);
                if (this.f3085a.K != null) {
                    this.f3085a.K.v = j2;
                }
                if (this.f3085a.W != null && (ajVar = (aj) this.f3085a.W.getTag()) != null && (textView = ajVar.i) != null) {
                    textView.setText(bm.a(j2) + Constants.STR_EMPTY);
                    return;
                }
                return;
            }
            this.f3085a.F.onRefreshComplete(false, true);
            this.f3085a.b(100);
            return;
        }
        this.f3085a.S.setVisibility(8);
        if (!z) {
            this.f3085a.F.onRefreshComplete(z2, false);
        } else if (-800 == i2) {
            this.f3085a.b(30);
        } else {
            this.f3085a.b(20);
        }
    }

    public void a(int i, int i2, long j, String str, String str2, String str3) {
        aj ajVar;
        TextView textView;
        this.f3085a.S.a(true);
        if (i2 == 0) {
            Toast.makeText(this.f3085a, (int) R.string.reply_success, 0).show();
            long unused = this.f3085a.ac = j;
            this.f3085a.C.setVisibility(8);
            if (this.f3085a.T != null) {
                int unused2 = this.f3085a.V = 0;
                this.f3085a.T.setText(Constants.STR_EMPTY);
                this.f3085a.T.setHint("回复楼主");
            }
            if (this.f3085a.K != null) {
                this.f3085a.K.v++;
            }
            if (!(this.f3085a.W == null || (ajVar = (aj) this.f3085a.W.getTag()) == null || (textView = ajVar.i) == null)) {
                textView.setText(bm.a(this.f3085a.K.v) + Constants.STR_EMPTY);
            }
            l.a(new STInfoV2(STConst.ST_PAGE_COMMENT_REPLY, this.f3085a.aa + bm.a(this.f3085a.V + 1), STConst.ST_PAGE_APP_DETAIL_COMMENT, STConst.ST_DEFAULT_SLOT, 200));
            return;
        }
        Toast.makeText(this.f3085a, (int) R.string.comment_failed, 0).show();
    }

    public void a(int i, int i2, long j, int i3, long j2) {
        List<ReplyDetail> a2;
        if (i2 == -800 && this.f3085a.H != null && (a2 = this.f3085a.H.a()) != null) {
            int i4 = 0;
            while (true) {
                int i5 = i4;
                if (i5 < a2.size()) {
                    ReplyDetail replyDetail = a2.get(i5);
                    if (replyDetail == null || replyDetail.f1459a != j) {
                        i4 = i5 + 1;
                    } else {
                        if (replyDetail.k == 1) {
                            replyDetail.k = 0;
                            replyDetail.j--;
                        } else {
                            replyDetail.k = 1;
                            replyDetail.j++;
                        }
                        this.f3085a.H.notifyDataSetChanged();
                        return;
                    }
                } else {
                    return;
                }
            }
        }
    }
}
