package com.tencent.nucleus.socialcontact.tagpage;

import android.graphics.SurfaceTexture;
import android.view.Surface;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class ax implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SurfaceTexture f3195a;
    final /* synthetic */ aw b;

    ax(aw awVar, SurfaceTexture surfaceTexture) {
        this.b = awVar;
        this.f3195a = surfaceTexture;
    }

    public void run() {
        try {
            this.b.f3194a.j.reset();
            this.b.f3194a.j.setDataSource(this.b.f3194a.l);
            this.b.f3194a.j.setOnPreparedListener(this.b.f3194a);
            this.b.f3194a.j.setOnCompletionListener(this.b.f3194a);
            this.b.f3194a.j.setOnBufferingUpdateListener(this.b.f3194a);
            this.b.f3194a.j.setOnErrorListener(this.b.f3194a);
            this.b.f3194a.j.setOnSeekCompleteListener(this.b.f3194a);
            this.b.f3194a.j.setVolume(0.0f, 0.0f);
            this.b.f3194a.j.setSurface(new Surface(this.f3195a));
            this.b.f3194a.j.prepare();
        } catch (Exception e) {
            XLog.e("VideoFullScreenManager", "[onSurfaceTextureAvailable] ---> e.printStackTrace() = " + e.toString());
            e.printStackTrace();
        }
    }
}
