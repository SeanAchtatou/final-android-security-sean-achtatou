package com.tencent.nucleus.socialcontact.tagpage;

import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.ah;
import com.tencent.downloadsdk.ae;
import com.tencent.nucleus.socialcontact.tagpage.TPVideoDownInfo;

/* compiled from: ProGuard */
class p implements ae {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f3210a;

    p(l lVar) {
        this.f3210a = lVar;
    }

    public void a(int i, String str, String str2) {
        TPVideoDownInfo tPVideoDownInfo = (TPVideoDownInfo) this.f3210a.d.get(str);
        if (tPVideoDownInfo != null) {
            tPVideoDownInfo.c = str2;
            tPVideoDownInfo.g = TPVideoDownInfo.DownState.SUCC;
            tPVideoDownInfo.h = 0;
            tPVideoDownInfo.f = System.currentTimeMillis();
            this.f3210a.f.sendMessage(this.f3210a.f.obtainMessage(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_SUCC, tPVideoDownInfo));
            this.f3210a.e.a(tPVideoDownInfo);
        }
    }

    public void a(int i, String str) {
        TPVideoDownInfo tPVideoDownInfo = (TPVideoDownInfo) this.f3210a.d.get(str);
        if (tPVideoDownInfo != null) {
            tPVideoDownInfo.g = TPVideoDownInfo.DownState.DOWNLOADING;
            tPVideoDownInfo.h = 0;
            this.f3210a.f.sendMessage(this.f3210a.f.obtainMessage(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_START, tPVideoDownInfo));
            this.f3210a.e.a(tPVideoDownInfo);
        }
    }

    public void a(int i, String str, long j, String str2, String str3) {
        TPVideoDownInfo tPVideoDownInfo = (TPVideoDownInfo) this.f3210a.d.get(str);
        if (tPVideoDownInfo != null) {
            if (tPVideoDownInfo.k == null) {
                tPVideoDownInfo.k = new j();
            }
            tPVideoDownInfo.k.f3205a = j;
            tPVideoDownInfo.d = j;
            this.f3210a.e.a(tPVideoDownInfo);
        }
    }

    public void a(int i, String str, long j, long j2, double d) {
        TPVideoDownInfo tPVideoDownInfo = (TPVideoDownInfo) this.f3210a.d.get(str);
        if (tPVideoDownInfo != null) {
            if (tPVideoDownInfo.g != TPVideoDownInfo.DownState.DOWNLOADING) {
                this.f3210a.f.sendMessage(this.f3210a.f.obtainMessage(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_START, tPVideoDownInfo));
            }
            tPVideoDownInfo.g = TPVideoDownInfo.DownState.DOWNLOADING;
            tPVideoDownInfo.h = 0;
            tPVideoDownInfo.k.b = j2;
            tPVideoDownInfo.k.f3205a = j;
            this.f3210a.f.sendMessage(this.f3210a.f.obtainMessage(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOADING, tPVideoDownInfo));
        }
    }

    public void a(int i, String str, String str2, String str3) {
        TPVideoDownInfo tPVideoDownInfo = (TPVideoDownInfo) this.f3210a.d.get(str);
        if (tPVideoDownInfo != null) {
            tPVideoDownInfo.c = str2;
            tPVideoDownInfo.g = TPVideoDownInfo.DownState.SUCC;
            tPVideoDownInfo.h = 0;
            tPVideoDownInfo.f = System.currentTimeMillis();
            tPVideoDownInfo.k.b = tPVideoDownInfo.k.f3205a;
            this.f3210a.f.sendMessage(this.f3210a.f.obtainMessage(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_SUCC, tPVideoDownInfo));
            this.f3210a.e.a(tPVideoDownInfo);
        }
    }

    public void a(int i, String str, int i2, byte[] bArr, String str2) {
        TPVideoDownInfo tPVideoDownInfo = (TPVideoDownInfo) this.f3210a.d.get(str);
        if (tPVideoDownInfo != null) {
            tPVideoDownInfo.g = TPVideoDownInfo.DownState.FAIL;
            tPVideoDownInfo.h = i2;
            this.f3210a.f.sendMessage(this.f3210a.f.obtainMessage(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_FAIL, tPVideoDownInfo));
            this.f3210a.e.a(tPVideoDownInfo);
        }
        if (i2 == -12) {
            TemporaryThreadManager.get().start(new q(this));
        } else if (i2 == -11) {
            ah.a().post(new r(this));
        }
        FileUtil.tryRefreshPath(i2);
    }

    public void b(int i, String str) {
        TPVideoDownInfo tPVideoDownInfo = (TPVideoDownInfo) this.f3210a.d.get(str);
        if (tPVideoDownInfo == null) {
            return;
        }
        if (tPVideoDownInfo.g == TPVideoDownInfo.DownState.QUEUING || tPVideoDownInfo.g == TPVideoDownInfo.DownState.DOWNLOADING) {
            tPVideoDownInfo.g = TPVideoDownInfo.DownState.PAUSED;
            tPVideoDownInfo.h = 0;
            this.f3210a.f.sendMessage(this.f3210a.f.obtainMessage(EventDispatcherEnum.UI_EVENT_TP_VIDEO_DOWNLOAD_PAUSE, tPVideoDownInfo));
            this.f3210a.e.a(tPVideoDownInfo);
        }
    }

    public void b(int i, String str, String str2) {
    }
}
