package com.tencent.nucleus.socialcontact.comment;

import com.tencent.assistant.module.callback.ActionCallback;
import com.tencent.assistant.protocol.jce.ReplyDetail;
import java.util.List;

/* compiled from: ProGuard */
public interface y extends ActionCallback {
    void a(int i, int i2, long j, int i3, long j2);

    void a(int i, int i2, long j, String str, String str2, String str3);

    void a(int i, int i2, boolean z, long j, List<ReplyDetail> list, boolean z2, long j2);
}
