package com.tencent.nucleus.socialcontact.tagpage;

import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.view.TextureView;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class aw implements TextureView.SurfaceTextureListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ as f3194a;

    private aw(as asVar) {
        this.f3194a = asVar;
    }

    /* synthetic */ aw(as asVar, at atVar) {
        this(asVar);
    }

    public void a(String str) {
        String unused = this.f3194a.l = str;
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        if (this.f3194a.j == null) {
            MediaPlayer unused = this.f3194a.j = new MediaPlayer();
        }
        TemporaryThreadManager.get().start(new ax(this, surfaceTexture));
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return true;
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }
}
