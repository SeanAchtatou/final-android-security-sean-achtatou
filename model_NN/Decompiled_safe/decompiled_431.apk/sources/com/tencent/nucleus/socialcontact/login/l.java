package com.tencent.nucleus.socialcontact.login;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.protocol.jce.AppSecretUserProfile;
import com.tencent.assistant.utils.XLog;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class l {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f3164a = true;

    public static void a(String str, String str2, String str3) {
        a(str, 0, str2, str3);
    }

    public static void a(String str, long j, String str2, String str3) {
        SharedPreferences i = i();
        XLog.d("LoginUtils", "savaTicket-- before --sig = " + str2 + "userId = " + str);
        SharedPreferences.Editor edit = i.edit();
        edit.putLong("uin", j);
        if (TextUtils.isEmpty(str)) {
            str = Constants.STR_EMPTY;
        }
        edit.putString("userId", str);
        if (TextUtils.isEmpty(str2)) {
            str2 = Constants.STR_EMPTY;
        }
        edit.putString("sig", str2);
        edit.putString("refreshToken", str3);
        edit.putBoolean("needDelToken", false);
        edit.commit();
    }

    public static String a() {
        return i().getString("userId", Constants.STR_EMPTY);
    }

    public static long b() {
        return i().getLong("uin", 0);
    }

    public static String c() {
        return i().getString("refreshToken", Constants.STR_EMPTY);
    }

    public static String d() {
        String string = i().getString("sig", Constants.STR_EMPTY);
        XLog.d("LoginUtils", "getSig -- before --sig = " + string);
        return string;
    }

    public static void a(int i) {
        SharedPreferences.Editor edit = i().edit();
        edit.putInt(AppConst.KEY_FROM_TYPE, i);
        edit.commit();
    }

    public static int e() {
        return i().getInt(AppConst.KEY_FROM_TYPE, 0);
    }

    public static void a(AppSecretUserProfile appSecretUserProfile, boolean z) {
        if (!z) {
            SharedPreferences.Editor edit = i().edit();
            if (appSecretUserProfile == null) {
                edit.putLong("bitmap", 0);
            } else {
                edit.putLong("bitmap", (long) appSecretUserProfile.a());
            }
            edit.commit();
        }
    }

    public static void a(m mVar) {
        if (mVar != null) {
            XLog.d("LoginUtils", "saveProfileInfo url =" + mVar.f3165a + " nickName = " + mVar.b);
        } else {
            XLog.d("LoginUtils", "saveProfileInfo profile = null");
        }
        SharedPreferences.Editor edit = i().edit();
        if (mVar == null) {
            edit.putString("profileIcon", Constants.STR_EMPTY);
            edit.putString("nickName", Constants.STR_EMPTY);
            edit.putLong("bitmap", 0);
        } else {
            edit.putString("profileIcon", TextUtils.isEmpty(mVar.f3165a) ? Constants.STR_EMPTY : mVar.f3165a);
            edit.putString("nickName", TextUtils.isEmpty(mVar.b) ? Constants.STR_EMPTY : mVar.b);
            edit.putLong("bitmap", mVar.c);
        }
        edit.commit();
    }

    public static synchronized m f() {
        m mVar;
        synchronized (l.class) {
            XLog.d("LoginUtils", "getProfileInfo");
            SharedPreferences i = i();
            String string = i.getString("profileIcon", Constants.STR_EMPTY);
            String string2 = i.getString("nickName", Constants.STR_EMPTY);
            long j = i.getLong("bitmap", 0);
            if (TextUtils.isEmpty(string2) && TextUtils.isEmpty(string) && j.a().j() && f3164a) {
                a.a().b();
            }
            f3164a = false;
            mVar = new m(string, string2, j);
        }
        return mVar;
    }

    public static boolean g() {
        m f = f();
        if (!TextUtils.isEmpty(f.b) || !TextUtils.isEmpty(f.f3165a)) {
            return true;
        }
        return false;
    }

    private static SharedPreferences i() {
        return AstApp.i().getApplicationContext().getSharedPreferences("ticket", 0);
    }

    public static boolean b(int i) {
        if (i != -1 && i != -2 && i != -3) {
            return true;
        }
        Bundle i2 = j.a().i();
        if (i2 == null) {
            i2 = new Bundle();
        }
        int i3 = i2.getInt(AppConst.KEY_LOGIN_TYPE, 0);
        if (i3 <= 0) {
            i3 = 2;
        }
        j.a().x();
        Bundle bundle = new Bundle();
        bundle.putInt(AppConst.KEY_LOGIN_TYPE, i3);
        bundle.putString(AppConst.KEY_ERROR_MSG, "身份失效，请重新登录");
        j.a().a(AppConst.IdentityType.MOBILEQ, bundle);
        return false;
    }

    public static void c(int i) {
        if (i == -1 || i == -2 || i == -3) {
            j.a().x();
            Bundle bundle = new Bundle();
            bundle.putString(AppConst.KEY_ERROR_MSG, "身份失效，请重新登录");
            bundle.putInt(AppConst.KEY_LOGIN_TYPE, 6);
            bundle.putInt(AppConst.KEY_FROM_TYPE, 1);
            j.a().a(AppConst.IdentityType.MOBILEQ, bundle);
        }
    }

    public static void h() {
        SharedPreferences i = i();
        boolean z = i.getBoolean("needDelToken", true);
        XLog.i("LoginUtils", ">>needDelToken=" + z);
        if (z && !TextUtils.isEmpty(d())) {
            a(null, null, null);
            SharedPreferences.Editor edit = i.edit();
            edit.putBoolean("needDelToken", false);
            edit.commit();
        }
    }
}
