package com.tencent.open.utils;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import java.lang.reflect.Field;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: ProGuard */
public final class ThreadManager {
    public static final boolean DEBUG_THREAD = false;
    public static final Executor NETWORK_EXECUTOR = a();

    /* renamed from: a  reason: collision with root package name */
    private static Handler f3281a;
    private static HandlerThread b;
    private static Handler c;
    private static HandlerThread d;

    private static Executor a() {
        Executor threadPoolExecutor;
        ThreadPoolExecutor threadPoolExecutor2;
        if (Build.VERSION.SDK_INT >= 11) {
            threadPoolExecutor2 = new ThreadPoolExecutor(1, 1, 0, TimeUnit.SECONDS, new LinkedBlockingQueue());
        } else {
            try {
                Field declaredField = AsyncTask.class.getDeclaredField("sExecutor");
                declaredField.setAccessible(true);
                threadPoolExecutor = (Executor) declaredField.get(null);
            } catch (Exception e) {
                threadPoolExecutor = new ThreadPoolExecutor(1, 1, 0, TimeUnit.SECONDS, new LinkedBlockingQueue());
            }
            threadPoolExecutor2 = threadPoolExecutor;
        }
        if (threadPoolExecutor2 instanceof ThreadPoolExecutor) {
            ((ThreadPoolExecutor) threadPoolExecutor2).setCorePoolSize(3);
        }
        return threadPoolExecutor2;
    }

    public static void init() {
    }

    public static void executeOnNetWorkThread(Runnable runnable) {
        try {
            NETWORK_EXECUTOR.execute(runnable);
        } catch (RejectedExecutionException e) {
        }
    }

    public static Handler getFileThreadHandler() {
        if (c == null) {
            synchronized (ThreadManager.class) {
                d = new HandlerThread("QQ_FILE_RW");
                d.start();
                c = new Handler(d.getLooper());
            }
        }
        return c;
    }

    public static Looper getFileThreadLooper() {
        return getFileThreadHandler().getLooper();
    }

    public static Thread getSubThread() {
        if (b == null) {
            getSubThreadHandler();
        }
        return b;
    }

    public static Handler getSubThreadHandler() {
        if (f3281a == null) {
            synchronized (ThreadManager.class) {
                b = new HandlerThread("QQ_SUB");
                b.start();
                f3281a = new Handler(b.getLooper());
            }
        }
        return f3281a;
    }

    public static Looper getSubThreadLooper() {
        return getSubThreadHandler().getLooper();
    }

    public static void executeOnSubThread(Runnable runnable) {
        getSubThreadHandler().post(runnable);
    }
}
