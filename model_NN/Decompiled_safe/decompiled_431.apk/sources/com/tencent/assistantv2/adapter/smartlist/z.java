package com.tencent.assistantv2.adapter.smartlist;

import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class z {

    /* renamed from: a  reason: collision with root package name */
    private int f1922a;
    private int b;
    private long c;
    private String d;
    private STInfoV2 e = null;

    public z a(int i) {
        this.f1922a = i;
        return this;
    }

    public int a() {
        return this.f1922a;
    }

    public z b(int i) {
        this.b = i;
        return this;
    }

    public int b() {
        return this.b;
    }

    public z a(long j) {
        this.c = j;
        return this;
    }

    public long c() {
        return this.c;
    }

    public z a(String str) {
        this.d = str;
        return this;
    }

    public String d() {
        return this.d;
    }
}
