package com.tencent.assistantv2.adapter.smartlist;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class c extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f1908a;
    final /* synthetic */ b b;

    c(b bVar, SimpleAppModel simpleAppModel) {
        this.b = bVar;
        this.f1908a = simpleAppModel;
    }

    public void onTMAClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("com.tencent.assistant.ACTION_URL", this.f1908a.aa);
        b.b(this.b.f1900a, this.f1908a.aa.f1125a, bundle);
    }

    public STInfoV2 getStInfo() {
        if (this.b.b.e() != null) {
            this.b.b.e().updateStatus(this.f1908a);
        }
        return this.b.b.e();
    }
}
