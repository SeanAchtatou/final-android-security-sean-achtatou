package com.tencent.assistantv2.adapter;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.l;
import android.support.v4.app.r;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import com.tencent.assistantv2.a.i;
import com.tencent.cloud.activity.EBookTabActivity;
import com.tencent.cloud.activity.ak;
import com.tencent.cloud.activity.al;
import com.tencent.cloud.activity.o;
import com.tencent.connect.common.Constants;
import com.tencent.game.activity.w;
import com.tencent.pangu.activity.aw;
import com.tencent.pangu.activity.be;
import java.lang.ref.SoftReference;
import java.util.List;

/* compiled from: ProGuard */
public class b extends r {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1894a = b.class.getName();
    private SparseArray<SoftReference<Fragment>> b = new SparseArray<>();
    private SparseArray<SoftReference<View>> c = new SparseArray<>();
    private Activity d;
    private List<i> e;
    private List<i> f;
    private String g = Constants.STR_EMPTY;
    private int h = -1;

    public b(l lVar, Activity activity, List<i> list, List<i> list2) {
        super(lVar);
        this.d = activity;
        this.e = list;
        this.f = list2;
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        Log.e("YYB5_0", "-----------instantiateItem:" + i);
        return super.instantiateItem(viewGroup, i);
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        Log.e("YYB5_0", "-----------destroyItem:" + i);
        super.destroyItem(viewGroup, i, obj);
    }

    public int getCount() {
        return this.e.size();
    }

    private Fragment a(i iVar, String str) {
        switch (c.f1895a[iVar.a().ordinal()]) {
            case 1:
                return new com.tencent.cloud.activity.i();
            case 2:
                return new aw();
            case 3:
                return new be();
            case 4:
                return new w();
            case 5:
                return new o();
            case 6:
                return new o();
            case 7:
                return new ak();
            case 8:
            case 9:
                return new al().a(str);
            default:
                return null;
        }
    }

    public int getItemPosition(Object obj) {
        return super.getItemPosition(obj);
    }

    public Fragment a(int i) {
        Fragment fragment;
        i iVar = this.e.get(i);
        SoftReference softReference = this.b.get(i);
        if (softReference == null || softReference.get() == null) {
            fragment = null;
        } else {
            fragment = (Fragment) softReference.get();
        }
        if (fragment != null) {
            return fragment;
        }
        Fragment a2 = a(iVar, this.e.get(i).d);
        this.b.put(i, new SoftReference(a2));
        return a2;
    }

    public View c(int i) {
        View view;
        SoftReference softReference = this.c.get(i);
        if (softReference == null || softReference.get() == null) {
            view = null;
        } else {
            view = (View) softReference.get();
        }
        if (view == null) {
            this.c.put(i, new SoftReference(view));
        }
        return view;
    }

    public void a(boolean z, int i) {
        EBookTabActivity eBookTabActivity;
        if ((c(i) instanceof EBookTabActivity) && (eBookTabActivity = (EBookTabActivity) c(i)) != null) {
            eBookTabActivity.a(z);
        }
    }
}
