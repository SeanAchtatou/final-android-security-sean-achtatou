package com.tencent.assistantv2.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.game.component.GameBannerGuideView;
import com.tencent.pangu.component.banner.f;
import java.util.List;

/* compiled from: ProGuard */
public class GameBannerView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private SoftwareBannerView f1930a;
    private GameBannerGuideView b;
    private Context c;

    public GameBannerView(Context context) {
        this(context, null);
        this.c = context;
        this.f1930a = new SoftwareBannerView(context);
        addView(this.f1930a);
    }

    public GameBannerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, SmartListAdapter.SmartListType.AppPage.ordinal());
        this.c = context;
        this.f1930a = new SoftwareBannerView(context, attributeSet, SmartListAdapter.SmartListType.AppPage.ordinal());
        addView(this.f1930a);
    }

    public GameBannerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.c = context;
        this.f1930a = new SoftwareBannerView(context, attributeSet, i);
        this.f1930a.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        addView(this.f1930a);
        this.b = new GameBannerGuideView(this.c);
        this.b.setVisibility(4);
        addView(this.b);
    }

    public void a(long j, List<f> list, List<ColorCardItem> list2) {
        this.f1930a.a(j, list, list2);
    }

    public void a(boolean z) {
        if (z) {
            XLog.d("jasonnzhang", "showGuideView ");
            if (this.b == null) {
                this.b = new GameBannerGuideView(this.c);
                addView(this.b);
            }
            this.b.a(getHeight());
            this.b.a();
            m.a().b("key_game_user_guide", Integer.valueOf(m.a().a("key_game_user_guide", 0) + 1));
        } else if (this.b != null && this.b.isShown()) {
            this.b.b();
        }
    }

    public SoftwareBannerView a() {
        return this.f1930a;
    }
}
