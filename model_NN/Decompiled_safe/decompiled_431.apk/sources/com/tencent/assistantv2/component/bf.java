package com.tencent.assistantv2.component;

import android.view.View;

/* compiled from: ProGuard */
class bf implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TabBarView f1979a;

    private bf(TabBarView tabBarView) {
        this.f1979a = tabBarView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.TabBarView.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistantv2.component.TabBarView.a(android.content.Context, android.util.AttributeSet):void
      com.tencent.assistantv2.component.TabBarView.a(int, float):void
      com.tencent.assistantv2.component.TabBarView.a(int, boolean):void */
    public void onClick(View view) {
        int id = view.getId();
        for (int i = 0; i < this.f1979a.i.length; i++) {
            if (this.f1979a.b(i) != null && id == this.f1979a.b(i).getId()) {
                this.f1979a.a(i, true);
                if (this.f1979a.l != null) {
                    this.f1979a.l.a(view, 1);
                }
            }
        }
    }
}
