package com.tencent.assistantv2.st.page;

import android.text.TextUtils;
import com.tencent.assistant.protocol.jce.PushInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistantv2.st.l;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class c {
    public static void a(PushInfo pushInfo, byte[] bArr) {
        b(STConst.ST_PAGE_RECEIVE_PUSH, pushInfo, bArr, 0);
    }

    public static void a(PushInfo pushInfo, byte[] bArr, int i, String str) {
        b(STConst.ST_PAGE_UPDATE_PUSH, pushInfo, bArr, i, str);
    }

    public static void a(PushInfo pushInfo) {
        if (pushInfo == null || pushInfo.f() != 2036) {
            b(STConst.ST_PAGE_RECEIVE_PUSH_MSG, pushInfo, null, 0);
        } else {
            b(STConst.ST_PAGE_PERSONAL_CENTER_RECEIVE_PUSH, pushInfo, null, 0);
        }
    }

    public static void a(int i, PushInfo pushInfo, byte[] bArr, int i2) {
        a(i, pushInfo, bArr, i2, (String) null);
    }

    public static void a(int i, PushInfo pushInfo, byte[] bArr, int i2, String str) {
        b(a(i, pushInfo), pushInfo, bArr, i2, str);
    }

    public static void a(int i, PushInfo pushInfo, byte[] bArr, String str, String str2) {
        String str3;
        int a2 = a(i, pushInfo);
        long j = pushInfo != null ? pushInfo.f1446a : 0;
        if (!TextUtils.isEmpty(str2)) {
            str3 = str + "|" + str2;
        } else {
            str3 = str;
        }
        a(j, a2, STConst.ST_DEFAULT_SLOT, 100, bArr, str3, true, null);
    }

    public static void a(int i, int i2, long j, String str, byte[] bArr, String str2) {
        a(j, a(i, i2), "03_001", 200, bArr, str, true, str2);
    }

    public static void a(int i, int i2, long j, String str, byte[] bArr) {
        a(j, a(i, i2), "03_002", STConstAction.ACTION_CANCEL, bArr, str, true, null);
    }

    public static void a(int i, int i2, boolean z) {
        String b = b(i, i2);
        if (TextUtils.isEmpty(b)) {
            return;
        }
        if (z) {
            a(0, STConst.ST_PAGE_LOCAL_PUSH, STConst.ST_DEFAULT_SLOT, 100, null, b, false, null);
        } else {
            a(0, STConst.ST_PAGE_LOCAL_PUSH, "03_001", 200, null, b, false, null);
        }
    }

    public static void a(int i, int i2, String str, String str2, boolean z) {
        if (!TextUtils.isEmpty(str2)) {
            if (z) {
                a(0, STConst.ST_PAGE_LOCAL_PUSH, str, 100, null, str2, false, null);
            } else {
                a(0, STConst.ST_PAGE_LOCAL_PUSH, str, 200, null, str2, false, null);
            }
        }
    }

    private static void b(int i, PushInfo pushInfo, byte[] bArr, int i2) {
        b(i, pushInfo, bArr, i2, null);
    }

    private static void b(int i, PushInfo pushInfo, byte[] bArr, int i2, String str) {
        String a2 = a(pushInfo, i2);
        if (!TextUtils.isEmpty(str)) {
            a2 = a2 + "|" + str;
        }
        a(pushInfo != null ? pushInfo.f1446a : 0, i, STConst.ST_DEFAULT_SLOT, 100, bArr, a2, true, null);
    }

    private static void a(long j, int i, String str, int i2, byte[] bArr, String str2, boolean z, String str3) {
        STInfoV2 sTInfoV2 = new STInfoV2(i, str, 2000, STConst.ST_DEFAULT_SLOT, i2);
        sTInfoV2.pushId = j;
        sTInfoV2.recommendId = bArr;
        sTInfoV2.pushInfo = str2;
        sTInfoV2.isImmediately = z;
        if (!TextUtils.isEmpty(str3)) {
            sTInfoV2.status = str3;
        }
        l.a(sTInfoV2);
    }

    public static int a(int i, PushInfo pushInfo) {
        if (i == 112 || i == 123) {
            return STConst.ST_PAGE_UPDATE_PUSH;
        }
        if (i != 115 || pushInfo == null) {
            return 2000;
        }
        return pushInfo.f();
    }

    public static int a(int i, int i2) {
        if (i == 112 || i == 123) {
            return STConst.ST_PAGE_UPDATE_PUSH;
        }
        if (i != 115) {
            return 2000;
        }
        return i2;
    }

    public static String a(PushInfo pushInfo, int i) {
        if (pushInfo != null) {
            return pushInfo.f1446a + "|" + String.valueOf(pushInfo.g()) + "|" + ((int) pushInfo.g) + "|" + i;
        }
        return "0||0|" + i;
    }

    public static String b(int i, int i2) {
        switch (i) {
            case 115:
                if (i2 == 13) {
                    return "0|10|6|0";
                }
                return "0|0|6|0";
            case 116:
                return "0|2|6|0";
            case 117:
                if (i2 == 6) {
                    return "0|3|6|0";
                }
                if (i2 == 7) {
                    return "0|4|6|0";
                }
                if (i2 == 8) {
                    return "0|5|6|0";
                }
                return Constants.STR_EMPTY;
            case 118:
                if (i2 == 9) {
                    return "0|6|6|0";
                }
                if (i2 == 10) {
                    return "0|7|6|0";
                }
                if (i2 == 12) {
                    return "0|9|6|0";
                }
                return Constants.STR_EMPTY;
            case 119:
                if (i2 == 13) {
                    return "0|10|6|0";
                }
                return Constants.STR_EMPTY;
            case 120:
            default:
                return Constants.STR_EMPTY;
            case 121:
                if (i2 == 13) {
                    return "0|13|6|0";
                }
                return Constants.STR_EMPTY;
            case 122:
                if (i2 == 14) {
                    return "0|14|6|0";
                }
                return Constants.STR_EMPTY;
            case 123:
                return "0|11|12|0";
            case 124:
                return "0|15|6|0";
        }
    }
}
