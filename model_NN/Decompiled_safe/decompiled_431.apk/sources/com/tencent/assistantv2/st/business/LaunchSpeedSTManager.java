package com.tencent.assistantv2.st.business;

import android.util.SparseArray;
import com.tencent.assistant.Global;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ao;
import com.tencent.assistant.utils.t;
import com.tencent.beacon.event.a;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public class LaunchSpeedSTManager {
    private static LaunchSpeedSTManager h;

    /* renamed from: a  reason: collision with root package name */
    boolean f2026a = false;
    public boolean b;
    public int c;
    public int d;
    public boolean e = false;
    private SparseArray<Long> f = new SparseArray<>();
    private boolean g = false;
    private HashMap<Integer, Long> i = new HashMap<>(50);
    private int j = 0;
    private int k = START_UP_TYPE.unknown.ordinal();
    private int l = 0;
    private int m = REQUEST_ST_CODE.Request_Success.ordinal();
    private String n = "UNKNOWN";
    private boolean o = false;

    /* compiled from: ProGuard */
    public enum REQUEST_ST_CODE {
        Request_Success,
        Network_Unavailable,
        Request_Failed
    }

    /* compiled from: ProGuard */
    public enum START_UP_TYPE {
        unknown,
        main,
        splash,
        guide,
        other
    }

    /* compiled from: ProGuard */
    public enum TYPE_TIME_POINT {
        AstApp_onCreate_Begin,
        AstApp_onCreate_End,
        Splash_onCreate_Begin,
        Splash_onCreate_End,
        Main_onCreate_Begin,
        Main_onCreate_End,
        Main_onResume_Enter,
        Main_onWindowFocusChanged_Enter,
        Found_onCreate_Begin,
        Found_initFirstPage_Begin,
        Found_Init_End,
        Home_reqFirstPageData_Start,
        Engine_send_Begin,
        SecurityPackage_packageRequest_Begin,
        SecurityPackage_packageRequest_End,
        Protocol_sendRequest_End,
        NetWorkTask_Run_Start,
        NetWorkTask_packageRequestHead_End,
        NetWorkTask_encodePkgReqBySo_End,
        Http_Execute_Begin,
        Http_Execute_End,
        DecodeResponse_End,
        DecodePkgRspBySo_End,
        Protocol_onNetWorkFinish_Begin,
        Protocol_onNetWorkStateChangeForNAC_End,
        Protocol_onUpdateRspHeadData_End,
        Protocol_onProtocoRequestFinish_Begin,
        Protocol_Ticket_Cert_Handle_End,
        Protocol_unpackageResponse_End,
        Protocol_Merge_End,
        HomeEngine_onRequestSuccessed_Begin,
        HomeEngine_transferCardList_End,
        HomeEngine_SmartCardCache_End,
        HomeManager_onPageLoad_Begin,
        HomeManager_CardMergeProcess_End,
        Found_onDataLoad_Begin,
        SmartListAdapter_refreshData_End,
        Draw_End
    }

    public boolean a(int i2) {
        if (this.f.get(i2) != null) {
            return false;
        }
        this.f.put(i2, Long.valueOf(System.currentTimeMillis()));
        return true;
    }

    public synchronized boolean a() {
        return this.g;
    }

    public synchronized void b() {
        this.i.put(Integer.valueOf(TYPE_TIME_POINT.Draw_End.ordinal()), this.f.valueAt(this.f.size() - 1));
        this.g = true;
        if (this.f2026a) {
            e();
        }
    }

    public synchronized void c() {
        this.f2026a = true;
        if (this.g) {
            e();
        }
    }

    public static synchronized LaunchSpeedSTManager d() {
        LaunchSpeedSTManager launchSpeedSTManager;
        synchronized (LaunchSpeedSTManager.class) {
            if (h == null) {
                h = new LaunchSpeedSTManager();
            }
            launchSpeedSTManager = h;
        }
        return launchSpeedSTManager;
    }

    public void a(TYPE_TIME_POINT type_time_point) {
        if (this.i.get(Integer.valueOf(type_time_point.ordinal())) == null) {
            this.i.put(Integer.valueOf(type_time_point.ordinal()), Long.valueOf(System.currentTimeMillis()));
        } else {
            XLog.e("LaunchSpeedSTManager", "tagTimePoint, duplicate event: " + type_time_point.name());
        }
    }

    public synchronized void a(boolean z) {
        this.j = z ? 1 : 0;
    }

    public synchronized void a(START_UP_TYPE start_up_type) {
        this.k = start_up_type.ordinal();
    }

    public synchronized void a(REQUEST_ST_CODE request_st_code, int i2) {
        this.m = request_st_code.ordinal();
        this.l = i2;
    }

    public void a(String str) {
        this.n = str;
    }

    private void h() {
        this.i.clear();
        this.l = 0;
        this.m = 0;
        this.j = 0;
        this.k = START_UP_TYPE.unknown.ordinal();
    }

    public synchronized void e() {
        long j2;
        if (this.k != START_UP_TYPE.main.ordinal()) {
            XLog.e("LaunchSpeedSTManager", "start_up_type is not main activity");
        } else if (!this.o && this.i.size() > 0) {
            Long l2 = this.i.get(Integer.valueOf(TYPE_TIME_POINT.Splash_onCreate_Begin.ordinal()));
            Long l3 = this.i.get(Integer.valueOf(TYPE_TIME_POINT.AstApp_onCreate_End.ordinal()));
            if (l2 == null || l3 == null) {
                j2 = 0;
            } else {
                j2 = l2.longValue() - l3.longValue();
            }
            a(j2 >= 0 && j2 <= 150);
            i();
            g();
            h();
            this.o = true;
        }
    }

    private void i() {
        a.a("LaunchSpeedSTManager", true, 0, 0, j(), true);
    }

    private Map<String, String> j() {
        HashMap hashMap = new HashMap();
        try {
            hashMap.put("B1", URLEncoder.encode(Global.getQUA(), "UTF-8"));
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        hashMap.put("B2", Global.getPhoneGuidAndGen());
        hashMap.put("B3", t.g());
        hashMap.put("B4", this.n);
        hashMap.put("B5", String.valueOf(this.j));
        hashMap.put("B6", String.valueOf(this.k));
        a(hashMap, "B7", TYPE_TIME_POINT.AstApp_onCreate_Begin);
        a(hashMap, "B8", TYPE_TIME_POINT.Splash_onCreate_Begin);
        a(hashMap, "B9", TYPE_TIME_POINT.Home_reqFirstPageData_Start);
        Long l2 = this.i.get(Integer.valueOf(TYPE_TIME_POINT.Http_Execute_Begin.ordinal()));
        Long l3 = this.i.get(Integer.valueOf(TYPE_TIME_POINT.Http_Execute_End.ordinal()));
        if (l2 == null) {
            l2 = l3;
        }
        if (l3 != null) {
            hashMap.put("B10", String.valueOf(l2));
            hashMap.put("B11", String.valueOf(l3) + "," + this.l + "," + this.m);
        }
        a(hashMap, "B12", TYPE_TIME_POINT.HomeEngine_onRequestSuccessed_Begin);
        a(hashMap, "B13", TYPE_TIME_POINT.HomeEngine_transferCardList_End);
        a(hashMap, "B14", TYPE_TIME_POINT.HomeEngine_SmartCardCache_End);
        a(hashMap, "B15", TYPE_TIME_POINT.Found_onDataLoad_Begin);
        a(hashMap, "B16", TYPE_TIME_POINT.Main_onCreate_Begin);
        a(hashMap, "B17", TYPE_TIME_POINT.Found_initFirstPage_Begin);
        a(hashMap, "B18", TYPE_TIME_POINT.Main_onWindowFocusChanged_Enter);
        a(hashMap, "B19", TYPE_TIME_POINT.SmartListAdapter_refreshData_End);
        a(hashMap, "B20", TYPE_TIME_POINT.Draw_End);
        return hashMap;
    }

    private void a(Map<String, String> map, String str, TYPE_TIME_POINT type_time_point) {
        Long l2 = this.i.get(Integer.valueOf(type_time_point.ordinal()));
        if (l2 != null) {
            map.put(str, String.valueOf(l2));
        }
    }

    public synchronized void a(int i2, int i3) {
        if (!this.b && !this.e) {
            this.e = true;
            if (this.c == 0) {
                this.c = i2;
            }
            if (this.d == 0) {
                this.d = i3;
            }
        }
    }

    public synchronized void f() {
        this.b = true;
    }

    public synchronized boolean b(int i2) {
        return i2 == this.c || i2 == this.d;
    }

    public void g() {
        String str;
        if (ao.a() && this.i.size() > 0) {
            int length = TYPE_TIME_POINT.values().length;
            ao.c("==================================================", "LaunchSpeedFLog", true);
            String str2 = null;
            long j2 = 0;
            for (Integer intValue : this.i.keySet()) {
                int intValue2 = intValue.intValue();
                if (this.i.get(Integer.valueOf(intValue2)) != null) {
                    j2 = this.i.get(Integer.valueOf(intValue2)).longValue();
                }
                if (intValue2 < 0 || intValue2 >= length) {
                    str = str2;
                } else {
                    str = TYPE_TIME_POINT.values()[intValue2].name() + "=" + j2;
                }
                ao.c(str, "LaunchSpeedFLog", true);
                str2 = str;
            }
            ao.b();
        }
    }
}
