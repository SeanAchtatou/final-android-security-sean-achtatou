package com.tencent.pangu.component;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.manager.BookingManager;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class c extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f3665a;
    final /* synthetic */ BookingButton b;

    c(BookingButton bookingButton, STInfoV2 sTInfoV2) {
        this.b = bookingButton;
        this.f3665a = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Bundle bundle = new Bundle();
        if (this.f3665a != null) {
            bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f3665a.scene);
        }
        b.a(this.b.getContext(), this.b.b.o, bundle);
    }

    public STInfoV2 getStInfo() {
        if (this.f3665a != null) {
            this.f3665a.status = this.b.b() == BookingManager.BOOKINGSTATUS.UNBOOKED ? "01" : "02";
        }
        return this.f3665a;
    }
}
