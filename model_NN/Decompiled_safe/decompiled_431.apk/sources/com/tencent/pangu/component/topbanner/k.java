package com.tencent.pangu.component.topbanner;

import android.graphics.Bitmap;
import com.tencent.assistant.manager.notification.a.a.e;
import com.tencent.assistant.utils.ah;
import com.tencent.pangu.component.topbanner.TopBannerView;

/* compiled from: ProGuard */
class k implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TopBannerView f3727a;

    k(TopBannerView topBannerView) {
        this.f3727a = topBannerView;
    }

    public void a(Bitmap bitmap) {
        if (bitmap == null || bitmap.isRecycled()) {
            ah.a().post(new m(this));
            return;
        }
        this.f3727a.a(bitmap, TopBannerView.ImageType.BANNERBG);
        Bitmap unused = this.f3727a.f = this.f3727a.b(bitmap, TopBannerView.ImageType.BANNERBG);
        ah.a().postDelayed(new l(this), 500);
    }
}
