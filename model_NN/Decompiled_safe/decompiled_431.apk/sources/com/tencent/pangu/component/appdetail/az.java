package com.tencent.pangu.component.appdetail;

/* compiled from: ProGuard */
class az implements ac {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HorizonScrollPicViewer f3583a;

    az(HorizonScrollPicViewer horizonScrollPicViewer) {
        this.f3583a = horizonScrollPicViewer;
    }

    public boolean a() {
        if (this.f3583a.e != null) {
            return this.f3583a.e.a();
        }
        return false;
    }

    public void a(int i) {
        if (this.f3583a.e != null) {
            int scrollX = this.f3583a.e.getScrollX();
            int scrollY = this.f3583a.e.getScrollY();
            this.f3583a.e.scrollTo(scrollX + i, scrollY);
        }
    }

    public void b(int i) {
        if (this.f3583a.e != null) {
            this.f3583a.e.fling(i);
        }
    }
}
