package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.a;
import com.tencent.assistant.manager.b;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.ah;
import com.tencent.assistantv2.component.fps.FPSProgressBar;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;

/* compiled from: ProGuard */
public class TXDwonloadProcessBar extends FrameLayout implements UIEventListener, b {

    /* renamed from: a  reason: collision with root package name */
    FPSProgressBar f3557a;
    View[] b;
    /* access modifiers changed from: private */
    public SimpleAppModel c;

    /* compiled from: ProGuard */
    public enum InfoType {
        STAR_DOWNTIMES_SIZE,
        CATEGORY_SIZE
    }

    public TXDwonloadProcessBar(Context context) {
        this(context, null);
        b();
    }

    public TXDwonloadProcessBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        b();
    }

    private void c(SimpleAppModel simpleAppModel) {
        this.c = simpleAppModel;
        a(this.c.u());
        a.a().a(this.c.q(), this);
    }

    public void a(SimpleAppModel simpleAppModel, View view) {
        this.b = new View[]{view};
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) getLayoutParams();
        layoutParams2.height = layoutParams.height;
        setLayoutParams(layoutParams2);
        c(simpleAppModel);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
    }

    public void a(SimpleAppModel simpleAppModel, View[] viewArr) {
        this.b = viewArr;
        c(simpleAppModel);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
    }

    /* access modifiers changed from: private */
    public void a(AppConst.AppState appState) {
        int i;
        DownloadInfo d = DownloadProxy.a().d(this.c.q());
        if (d != null) {
            i = d.getProgress();
        } else {
            i = 0;
        }
        if (appState == AppConst.AppState.DOWNLOADING || appState == AppConst.AppState.PAUSED || appState == AppConst.AppState.FAIL || appState == AppConst.AppState.QUEUING) {
            a(0);
            if (this.f3557a == null) {
                return;
            }
            if (appState != AppConst.AppState.DOWNLOADING || d == null) {
                this.f3557a.a(i);
            } else {
                this.f3557a.b(i);
            }
        } else {
            a(8);
        }
    }

    public void a(SimpleAppModel simpleAppModel) {
        AppConst.AppState u = this.c.u();
        if (u == AppConst.AppState.DOWNLOAD || u == AppConst.AppState.DOWNLOADED || u == AppConst.AppState.INSTALLED) {
            a(8);
        } else {
            a(0);
        }
    }

    public void b(SimpleAppModel simpleAppModel) {
        AppConst.AppState u = this.c.u();
        if (u == AppConst.AppState.DOWNLOAD || u == AppConst.AppState.DOWNLOADED || u == AppConst.AppState.INSTALLED) {
            a(8);
        } else {
            a(0);
        }
    }

    private void b() {
        this.f3557a = (FPSProgressBar) inflate(getContext(), R.layout.download_progress_bar_layout, this).findViewById(R.id.down_progress);
        this.f3557a.a(FPSProgressBar.SIZE.TINY);
    }

    private void a(int i) {
        if (i == 8) {
            setVisibility(8);
            if (this.b != null) {
                for (View view : this.b) {
                    if (view != null) {
                        view.setVisibility(0);
                    }
                }
            }
        } else if (i == 0) {
            setVisibility(0);
            if (this.b != null) {
                for (View view2 : this.b) {
                    if (view2 != null) {
                        view2.setVisibility(8);
                    }
                }
            }
        }
    }

    public void a() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
    }

    public void onAppStateChange(String str, AppConst.AppState appState) {
        if (str != null && this.c != null && str.equals(this.c.q())) {
            ah.a().post(new bn(this, str, appState));
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
                if (message.obj instanceof DownloadInfo) {
                    DownloadInfo downloadInfo = (DownloadInfo) message.obj;
                    if (this.f3557a != null && downloadInfo != null && this.c != null && downloadInfo.downloadTicket != null && this.c.q().equals(downloadInfo.downloadTicket)) {
                        c(this.c);
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }
}
