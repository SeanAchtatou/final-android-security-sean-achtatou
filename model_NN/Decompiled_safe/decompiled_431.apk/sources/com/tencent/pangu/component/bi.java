package com.tencent.pangu.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.SelfUpdateActivity;
import com.tencent.pangu.manager.SelfUpdateManager;

/* compiled from: ProGuard */
class bi extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SelfNormalUpdateView f3654a;

    bi(SelfNormalUpdateView selfNormalUpdateView) {
        this.f3654a = selfNormalUpdateView;
    }

    public void onTMAClick(View view) {
        this.f3654a.f();
        SelfUpdateManager.a().o().b(true);
    }

    public STInfoV2 getStInfo() {
        if (!(this.f3654a.getContext() instanceof SelfUpdateActivity)) {
            return null;
        }
        STInfoV2 t = ((SelfUpdateActivity) this.f3654a.getContext()).t();
        t.slotId = "01_002";
        t.actionId = 200;
        return t;
    }
}
