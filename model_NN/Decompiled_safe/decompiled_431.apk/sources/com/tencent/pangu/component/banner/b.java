package com.tencent.pangu.component.banner;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.m;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.ah;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.game.component.GameBannerGuideView;

/* compiled from: ProGuard */
class b extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f3638a;
    final /* synthetic */ int b;
    final /* synthetic */ boolean c;
    final /* synthetic */ ImageView d;
    final /* synthetic */ a e;

    b(a aVar, Context context, int i, boolean z, ImageView imageView) {
        this.e = aVar;
        this.f3638a = context;
        this.b = i;
        this.c = z;
        this.d = imageView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    public void onTMAClick(View view) {
        com.tencent.pangu.link.b.a(this.f3638a, this.e.f3641a.e);
        if ("最新".equals(this.e.f3641a.b) && GameBannerGuideView.f2650a) {
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3638a, 200);
            buildSTInfo.slotId = a.a(this.e.b, this.b + 100);
            l.a(buildSTInfo);
            m.a().b("key_game_user_guide", (Object) 2);
            GameBannerGuideView.f2650a = false;
        }
        if (this.c && this.d != null) {
            TemporaryThreadManager.get().start(new c(this));
            ah.a().postDelayed(new d(this), 1000);
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3638a, 200);
        buildSTInfo.slotId = a.a(this.e.b, this.b);
        return buildSTInfo;
    }
}
