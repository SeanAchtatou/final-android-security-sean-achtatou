package com.tencent.pangu.component.appdetail;

import android.view.GestureDetector;
import android.view.MotionEvent;

/* compiled from: ProGuard */
class x extends GestureDetector.SimpleOnGestureListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppdetailScrollView f3624a;

    x(AppdetailScrollView appdetailScrollView) {
        this.f3624a = appdetailScrollView;
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return false;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        if (f2 < 0.0f) {
            if (this.f3624a.getScrollY() >= this.f3624a.f3536a && this.f3624a.i != null) {
                this.f3624a.i.fling(-((int) f2));
                this.f3624a.i.a(0);
            }
        } else if (this.f3624a.i != null && this.f3624a.i.a()) {
            this.f3624a.i.fling(-((int) f2));
            this.f3624a.i.a(0);
        }
        return false;
    }
}
