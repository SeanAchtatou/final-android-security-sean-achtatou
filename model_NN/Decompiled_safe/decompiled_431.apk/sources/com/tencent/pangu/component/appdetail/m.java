package com.tencent.pangu.component.appdetail;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class m extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppdetailDownloadBar f3600a;

    m(AppdetailDownloadBar appdetailDownloadBar) {
        this.f3600a = appdetailDownloadBar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.component.appdetail.AppdetailDownloadBar.a(com.tencent.pangu.component.appdetail.AppdetailDownloadBar, boolean):boolean
     arg types: [com.tencent.pangu.component.appdetail.AppdetailDownloadBar, int]
     candidates:
      com.tencent.pangu.component.appdetail.AppdetailDownloadBar.a(int, int):void
      com.tencent.pangu.component.appdetail.AppdetailDownloadBar.a(java.lang.String, int):void
      com.tencent.pangu.component.appdetail.AppdetailDownloadBar.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.component.appdetail.AppdetailDownloadBar.a(com.tencent.pangu.component.appdetail.AppdetailDownloadBar, boolean):boolean */
    public void onTMAClick(View view) {
        SimpleAppModel a2;
        boolean z = true;
        if (view == this.f3600a.t) {
            this.f3600a.D.a(view);
        } else if (view == this.f3600a.u) {
            this.f3600a.D.a(view);
        } else if (this.f3600a.D != null && (a2 = this.f3600a.D.a()) != null) {
            AppConst.AppState d = k.d(a2);
            if (!(a2.h() || a2.i() || (a2.d() && (a2.P & 1) > 0)) || !(d == AppConst.AppState.DOWNLOAD || d == AppConst.AppState.UPDATE)) {
                z = false;
            }
            if ((view.getId() == R.id.wx_auth || view.getId() == R.id.qq_auth || z) && this.f3600a.g == -1) {
                this.f3600a.g++;
            }
            if (this.f3600a.g < 0) {
                if (this.f3600a.N) {
                    boolean unused = this.f3600a.N = false;
                    this.f3600a.g();
                }
            } else if (this.f3600a.g == 0) {
                this.f3600a.g++;
            }
            this.f3600a.D.a(view);
        }
    }

    public STInfoV2 getStInfo() {
        if (!(this.f3600a.k instanceof AppDetailActivityV5)) {
            return null;
        }
        STInfoV2 t = ((AppDetailActivityV5) this.f3600a.k).t();
        t.slotId = this.f3600a.c;
        if (this.f3600a.D == null || t == null || this.f3600a.u == null || this.f3600a.t == null) {
            return null;
        }
        SimpleAppModel a2 = this.f3600a.D.a();
        if (a2 != null) {
            t.isImmediately = a2.au == 1;
        }
        if (this.clickViewId != R.id.wx_auth && this.clickViewId != R.id.qq_auth && this.clickViewId != this.f3600a.t.getId() && this.clickViewId != this.f3600a.u.getId()) {
            AppConst.AppState d = k.d(a2);
            t.actionId = a.a(d);
            t.status = a.a(d, a2);
        } else if (this.clickViewId == R.id.qq_auth) {
            t.actionId = 200;
            t.status = "03";
        } else if (this.clickViewId == R.id.wx_auth) {
            t.actionId = 200;
            t.status = "04";
        } else if (this.clickViewId == this.f3600a.t.getId()) {
            t.slotId = this.f3600a.d;
            t.actionId = 200;
            if (a2 == null || ApkResourceManager.getInstance().getLocalApkInfo(a2.c) == null) {
                t.status = "03";
            } else if (this.f3600a.e) {
                t.status = "02";
            } else {
                t.status = "01";
            }
        }
        t.actionFlag = this.f3600a.D.e();
        return t;
    }
}
