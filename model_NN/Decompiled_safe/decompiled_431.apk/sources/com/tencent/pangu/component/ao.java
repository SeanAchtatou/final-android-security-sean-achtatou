package com.tencent.pangu.component;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.l;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
class ao extends ViewInvalidateMessageHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RankNormalListView f3523a;

    ao(RankNormalListView rankNormalListView) {
        this.f3523a = rankNormalListView;
    }

    public void handleMessage(ViewInvalidateMessage viewInvalidateMessage) {
        int i;
        if (viewInvalidateMessage.what == 1) {
            int i2 = viewInvalidateMessage.arg1;
            int i3 = viewInvalidateMessage.arg2;
            Map map = (Map) viewInvalidateMessage.params;
            boolean booleanValue = ((Boolean) map.get("isFirstPage")).booleanValue();
            Object obj = map.get("key_data");
            if (obj != null && this.f3523a.u != null) {
                List<SimpleAppModel> list = (List) obj;
                this.f3523a.u.a(booleanValue, list);
                if (booleanValue) {
                    l.a((int) STConst.ST_PAGE_RANK_CLASSIC, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                }
                this.f3523a.a(i3, i2, booleanValue, ((List) obj).size());
                if (booleanValue) {
                    int i4 = 0;
                    int i5 = 0;
                    for (SimpleAppModel simpleAppModel : list) {
                        i4++;
                        if (i4 > 20) {
                            break;
                        }
                        if (simpleAppModel.u() == AppConst.AppState.INSTALLED) {
                            i = i5 + 1;
                        } else {
                            i = i5;
                        }
                        i5 = i;
                    }
                    if ((i5 < 5 || !m.a().aj()) && ((!this.f3523a.G && !m.a().al()) || (this.f3523a.G && !m.a().am()))) {
                        this.f3523a.isHideInstalledAppAreaAdded = false;
                        this.f3523a.A.setVisibility(8);
                    } else {
                        this.f3523a.isHideInstalledAppAreaAdded = true;
                        this.f3523a.r();
                        m.a().x(false);
                        if (this.f3523a.G) {
                            m.a().A(false);
                        } else {
                            m.a().z(false);
                        }
                    }
                    this.f3523a.u.d();
                }
            } else if (this.f3523a.u != null) {
                this.f3523a.a(i3, i2, booleanValue, 0);
            }
        } else {
            this.f3523a.v.f();
            if (this.f3523a.u != null) {
                this.f3523a.u.notifyDataSetChanged();
            }
        }
    }
}
