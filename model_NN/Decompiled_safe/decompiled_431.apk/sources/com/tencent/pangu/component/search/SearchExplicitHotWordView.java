package com.tencent.pangu.component.search;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.ExplicitHotWord;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.component.appdetail.process.s;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.module.h;

/* compiled from: ProGuard */
public class SearchExplicitHotWordView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private final String f3693a;
    private View b;
    private RelativeLayout c;
    private RelativeLayout d;
    private TXAppIconView e;
    private TextView f;
    private TextView g;
    private ListItemInfoView h;
    private DownloadButton i;
    private ExplicitHotWord j;
    /* access modifiers changed from: private */
    public View.OnClickListener k;
    private b l;
    private int m;
    /* access modifiers changed from: private */
    public String n;
    /* access modifiers changed from: private */
    public SimpleAppModel o;
    /* access modifiers changed from: private */
    public Context p;
    private View.OnClickListener q;

    public SearchExplicitHotWordView(Context context) {
        this(context, null);
    }

    public SearchExplicitHotWordView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3693a = "SearchExplicitHotWordView";
        this.k = null;
        this.l = null;
        this.m = STConst.ST_PAGE_SEARCH;
        this.q = new d(this);
        this.p = context;
        a(context);
    }

    public void a(View.OnClickListener onClickListener) {
        if (onClickListener != null) {
            this.k = onClickListener;
        }
    }

    private void a(Context context) {
        inflate(context, R.layout.search_explicit_hot_words_view, this);
        this.b = findViewById(R.id.container);
        this.b.setVisibility(8);
        b();
    }

    private void b() {
        this.j = h.a().d();
        if (this.j == null || this.j.i != 1 || this.j.j == null) {
            this.b.setVisibility(8);
            return;
        }
        this.n = this.j.f1230a;
        this.b.setVisibility(0);
        this.f = (TextView) findViewById(R.id.explicit_title);
        this.e = (TXAppIconView) findViewById(R.id.app_icon_img);
        this.g = (TextView) findViewById(R.id.app_name_txt);
        this.h = (ListItemInfoView) findViewById(R.id.download_info);
        this.i = (DownloadButton) findViewById(R.id.state_app_btn);
        this.c = (RelativeLayout) findViewById(R.id.explicit_all_view);
        this.d = (RelativeLayout) findViewById(R.id.app_view);
        this.f.setText(this.j.b);
        this.g.setText(this.j.f1230a);
        this.o = k.a(this.j.j);
        this.e.updateImageView(this.o.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        a();
        this.i.setOnClickListener(this.q);
        this.b.setTag(R.id.search_key_word, this.j.f1230a);
        this.b.setTag(R.id.search_source_scene, 200705);
        this.b.setOnClickListener(this.q);
        this.d.setOnClickListener(this.q);
        a(this.o, (String) null, 100);
    }

    public void a() {
        if (this.i != null && this.o != null && this.h != null) {
            try {
                this.h.a(this.o);
                this.i.a(this.o);
                this.i.setTag(R.id.search_app_info, this.o);
                this.i.setTag(R.id.search_key_word, this.o.d);
                if (s.a(this.o)) {
                    this.i.setClickable(false);
                } else {
                    this.i.setClickable(true);
                }
            } catch (Exception e2) {
                XLog.d("SearchExplicitHotWordView", "resetDownloadBtnState failed>>>", e2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(SimpleAppModel simpleAppModel, View view, String str) {
        DownloadInfo downloadInfo;
        if (simpleAppModel != null) {
            DownloadInfo a2 = DownloadProxy.a().a(simpleAppModel);
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.p, 200);
            if (buildSTInfo != null) {
                buildSTInfo.scene = this.m;
                buildSTInfo.slotId = str;
                buildSTInfo.extraData = this.n + ";" + simpleAppModel.f938a;
                buildSTInfo.updateWithSimpleAppModel(simpleAppModel);
            }
            StatInfo a3 = a.a(buildSTInfo);
            if (a2 != null && a2.needReCreateInfo(simpleAppModel)) {
                DownloadProxy.a().b(a2.downloadTicket);
                a2 = null;
            }
            if (a2 != null) {
                a2.updateDownloadInfoStatInfo(a3);
                downloadInfo = a2;
            } else if (this.i == null || this.h == null) {
                downloadInfo = DownloadInfo.createDownloadInfo(simpleAppModel, a3);
            } else {
                downloadInfo = DownloadInfo.createDownloadInfo(simpleAppModel, a3, this.i, this.h);
            }
            switch (e.f3702a[k.d(simpleAppModel).ordinal()]) {
                case 1:
                case 2:
                    com.tencent.pangu.download.a.a().a(downloadInfo);
                    com.tencent.assistant.utils.a.a((ImageView) this.b.findViewWithTag(downloadInfo.downloadTicket));
                    return;
                case 3:
                case 4:
                    com.tencent.pangu.download.a.a().b(downloadInfo.downloadTicket);
                    return;
                case 5:
                    com.tencent.pangu.download.a.a().b(downloadInfo);
                    return;
                case 6:
                    com.tencent.pangu.download.a.a().d(downloadInfo);
                    return;
                case 7:
                    com.tencent.pangu.download.a.a().c(downloadInfo);
                    return;
                case 8:
                case 9:
                    com.tencent.pangu.download.a.a().a(downloadInfo);
                    return;
                case 10:
                    Toast.makeText(this.p, (int) R.string.tips_slicent_install, 0).show();
                    return;
                case 11:
                    Toast.makeText(this.p, (int) R.string.tips_slicent_uninstall, 0).show();
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(SimpleAppModel simpleAppModel, String str, int i2) {
        STInfoV2 buildSTInfo;
        if ((this.p instanceof BaseActivity) && (buildSTInfo = STInfoBuilder.buildSTInfo(this.p, i2)) != null) {
            buildSTInfo.scene = this.m;
            buildSTInfo.slotId = a.a("05", "001");
            if (simpleAppModel != null) {
                buildSTInfo.extraData = this.n + ";" + simpleAppModel.f938a;
            } else {
                buildSTInfo.extraData = this.n;
            }
            buildSTInfo.updateWithSimpleAppModel(simpleAppModel);
            if (str != null) {
                buildSTInfo.status = str;
            }
            if (i2 == 100) {
                if (this.l == null) {
                    this.l = b.getInstance();
                }
                this.l.exposure(buildSTInfo);
                return;
            }
            l.a(buildSTInfo);
        }
    }
}
