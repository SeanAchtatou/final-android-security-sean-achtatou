package com.tencent.pangu.component.search;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.st.STConst;

/* compiled from: ProGuard */
public abstract class ISearchResultPage extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    protected String f3690a;
    protected boolean b;
    protected int c;
    protected int d;
    protected SimpleAppModel e;
    protected long f;
    protected boolean g;
    private int h;
    private int i;

    /* compiled from: ProGuard */
    public enum PageType {
        NONE,
        NATIVE,
        WEB
    }

    /* access modifiers changed from: protected */
    public abstract void a(Context context);

    public abstract void a(String str);

    public abstract void a(String str, int i2);

    public abstract void a(String str, int i2, SimpleAppModel simpleAppModel);

    public abstract void b(String str);

    public abstract void d(int i2);

    public abstract PageType h();

    public abstract void i();

    public abstract void j();

    public abstract void k();

    public ISearchResultPage(Context context) {
        this(context, null);
    }

    public ISearchResultPage(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ISearchResultPage(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.b = false;
        this.c = 1;
        this.d = STConst.ST_PAGE_SEARCH;
        this.e = null;
        this.f = 0;
        this.h = 4;
        this.i = STConst.ST_PAGE_SEARCH_RESULT_ALL;
        this.g = false;
    }

    public void a() {
        if (!this.g) {
            this.g = true;
            a(getContext());
        }
    }

    public View a(int i2) {
        return LayoutInflater.from(getContext()).inflate(i2, this);
    }

    public void b(int i2) {
        this.h = i2;
    }

    public int b() {
        return this.h;
    }

    public void c(int i2) {
        this.i = i2;
    }

    public int c() {
        return this.i;
    }

    public void d() {
        a(this.f3690a);
        e();
    }

    public void e() {
        this.f3690a = null;
    }

    public String f() {
        return this.f3690a;
    }

    public void a(boolean z) {
        this.b = z;
    }

    /* access modifiers changed from: protected */
    public void g() {
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 2);
        }
    }
}
