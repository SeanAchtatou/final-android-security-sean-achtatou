package com.tencent.pangu.component;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.protocol.jce.ApkAutoOpenCfg;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.download.DownloadInfo;

/* compiled from: ProGuard */
public class RecommandApplinkDialog extends RelativeLayout implements TXImageView.ITXImageViewListener {
    private static int n = 201617;

    /* renamed from: a  reason: collision with root package name */
    public AstApp f3500a;
    public ApkAutoOpenCfg b;
    public DownloadInfo c;
    public long d;
    Dialog e;
    private View f;
    private TXImageView g;
    private TXImageView h;
    private TXImageView i;
    private TextView j;
    private TextView k;
    private Button l;
    private Button m;
    /* access modifiers changed from: private */
    public String o;

    public RecommandApplinkDialog(Context context) {
        this(context, null);
    }

    public RecommandApplinkDialog(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.o = "_001";
        this.f3500a = AstApp.i();
        b();
    }

    private void b() {
        this.f = LayoutInflater.from(getContext()).inflate((int) R.layout.recommand_applink_dialog, this);
        this.g = (TXImageView) this.f.findViewById(R.id.recommand_app_icon);
        this.h = (TXImageView) this.f.findViewById(R.id.recommand_succc_icon);
        this.i = (TXImageView) this.f.findViewById(R.id.recommand_guide_icon);
        this.j = (TextView) this.f.findViewById(R.id.title_tip);
        this.k = (TextView) this.f.findViewById(R.id.guide_tip);
        this.l = (Button) this.f.findViewById(R.id.btn_cancel);
        this.m = (Button) this.f.findViewById(R.id.btn_open);
    }

    public void a() {
        if (this.b != null) {
            if (!TextUtils.isEmpty(this.b.e)) {
                String str = this.b.e;
                if (this.b.e.length() > 8) {
                    str = this.b.e.substring(0, 8) + "...";
                }
                this.j.setText(Html.fromHtml("<font color=\"#000000\">" + str + "</font><font color=\"#6cbc5f\">" + "已安装成功</font>"));
            }
            if (!TextUtils.isEmpty(this.b.g)) {
                this.k.setText(Html.fromHtml(this.b.g));
            }
            this.g.updateImageView(this.b.f1132a, R.drawable.app_treasure_box_above_icon, TXImageView.TXImageViewType.INSTALL_APK_ICON);
            if (!TextUtils.isEmpty(this.b.h)) {
                this.i.updateImageView(this.b.h, 0, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            } else {
                this.i.setVisibility(8);
            }
            if (!TextUtils.isEmpty(this.b.c)) {
                this.o = "_002";
            }
        }
    }

    public void a(BaseActivity baseActivity) {
        Dialog dialog = new Dialog(baseActivity, R.style.dialog);
        dialog.setContentView(this);
        dialog.setCancelable(true);
        dialog.setOwnerActivity(baseActivity);
        dialog.getWindow().getAttributes().height = -2;
        dialog.getWindow().getAttributes().width = -1;
        dialog.getWindow().getAttributes().gravity = 83;
        this.e = dialog;
        dialog.setOnCancelListener(new av(this, dialog));
        a(new aw(this, dialog), new ax(this, dialog));
        if (!baseActivity.isFinishing()) {
            try {
                dialog.show();
                a(STConst.ST_DEFAULT_SLOT, 100, Constants.STR_EMPTY);
                a("03_001", 100, String.valueOf(this.d) + ";" + this.b.d);
                XLog.d("RecommandApplinkDialog", "<install> <GuideOpen> 浮层打开推荐显示 " + this.b.f1132a);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void a(View.OnClickListener onClickListener, View.OnClickListener onClickListener2) {
        if (onClickListener != null) {
            this.l.setOnClickListener(onClickListener);
        }
        if (onClickListener2 != null) {
            this.m.setOnClickListener(onClickListener2);
        }
    }

    public void onTXImageViewLoadImageFinish(TXImageView tXImageView, Bitmap bitmap) {
    }

    /* access modifiers changed from: private */
    public void a(String str, int i2, String str2) {
        STInfoV2 sTInfoV2 = new STInfoV2(n, str, n, STConst.ST_DEFAULT_SLOT, i2);
        if (this.b != null) {
            sTInfoV2.packageName = this.b.f1132a;
        }
        if (!TextUtils.isEmpty(str2)) {
            sTInfoV2.extraData = str2;
        }
        l.a(sTInfoV2);
    }
}
