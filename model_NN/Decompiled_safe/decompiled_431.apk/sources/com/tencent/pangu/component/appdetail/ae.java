package com.tencent.pangu.component.appdetail;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class ae extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CustomRelateAppView f3564a;

    ae(CustomRelateAppView customRelateAppView) {
        this.f3564a = customRelateAppView;
    }

    public void onTMAClick(View view) {
        switch (view.getId()) {
            case R.id.ralate_app_layout1 /*2131165636*/:
                if (this.f3564a.j.size() > 0) {
                    this.f3564a.a((SimpleAppModel) this.f3564a.j.get(0));
                    return;
                }
                return;
            case R.id.ralate_app_layout2 /*2131165640*/:
                if (this.f3564a.j.size() > 1) {
                    this.f3564a.a((SimpleAppModel) this.f3564a.j.get(1));
                    return;
                }
                return;
            case R.id.ralate_app_layout3 /*2131165644*/:
                if (this.f3564a.j.size() > 2) {
                    this.f3564a.a((SimpleAppModel) this.f3564a.j.get(2));
                    return;
                }
                return;
            default:
                return;
        }
    }

    public STInfoV2 getStInfo() {
        SimpleAppModel simpleAppModel;
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3564a.e, 200);
        if (buildSTInfo != null) {
            buildSTInfo.slotId = this.f3564a.a(this.f3564a.c, this.f3564a.a(getClickViewId()));
            int a2 = this.f3564a.a(getClickViewId());
            if (this.f3564a.j.size() > a2 && (simpleAppModel = (SimpleAppModel) this.f3564a.j.get(a2)) != null) {
                buildSTInfo.updateWithSimpleAppModel(simpleAppModel);
            }
        }
        return buildSTInfo;
    }
}
