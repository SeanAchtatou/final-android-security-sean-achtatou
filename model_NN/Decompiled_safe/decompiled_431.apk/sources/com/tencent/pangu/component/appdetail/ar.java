package com.tencent.pangu.component.appdetail;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class ar extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FriendTalkView f3576a;

    ar(FriendTalkView friendTalkView) {
        this.f3576a = friendTalkView;
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3576a.d, 200);
        buildSTInfo.scene = STConst.ST_PAGE_APP_DETAIL;
        buildSTInfo.slotId = this.f3576a.c + "_" + "001";
        return buildSTInfo;
    }

    public void onTMAClick(View view) {
        if (this.f3576a.k != null) {
            this.f3576a.k.a(view);
        }
    }
}
