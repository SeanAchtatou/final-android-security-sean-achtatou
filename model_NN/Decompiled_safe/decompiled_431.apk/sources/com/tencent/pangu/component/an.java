package com.tencent.pangu.component;

import com.qq.AppService.AstApp;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.a;
import com.tencent.assistant.protocol.jce.TagGroup;
import com.tencent.assistant.utils.XLog;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
class an implements a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RankNormalListView f3522a;

    an(RankNormalListView rankNormalListView) {
        this.f3522a = rankNormalListView;
    }

    public void a(int i, int i2, boolean z, List<SimpleAppModel> list, List<TagGroup> list2) {
        if (this.f3522a.x != null) {
            ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(1, null, this.f3522a.M);
            viewInvalidateMessage.arg1 = i2;
            viewInvalidateMessage.arg2 = i;
            HashMap hashMap = new HashMap();
            hashMap.put("isFirstPage", Boolean.valueOf(z));
            viewInvalidateMessage.params = hashMap;
            hashMap.put("key_data", list);
            boolean c = com.tencent.assistantv2.manager.a.a().b().c();
            XLog.d("voken", "filter = " + c);
            if (!c && z && com.tencent.assistantv2.manager.a.a().b().b() < 2 && this.f3522a.a(list) && !com.tencent.assistantv2.manager.a.a().b().e()) {
                AstApp.i().j().sendEmptyMessage(EventDispatcherEnum.UI_EVENT_HOTTAB_DOWNLOAD_FILTER_SHOW);
            }
            this.f3522a.x.sendMessage(viewInvalidateMessage);
        }
    }
}
