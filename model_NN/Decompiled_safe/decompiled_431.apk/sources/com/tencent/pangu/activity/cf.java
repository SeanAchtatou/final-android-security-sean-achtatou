package com.tencent.pangu.activity;

import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class cf implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f3357a;

    cf(SearchActivity searchActivity) {
        this.f3357a = searchActivity;
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
        String unused = this.f3357a.P = (String) null;
        String unused2 = this.f3357a.Q = (String) null;
        this.f3357a.z.d();
        this.f3357a.z.a(editable.toString());
        if (!TextUtils.isEmpty(editable.toString()) && TextUtils.isEmpty(editable.toString().trim())) {
            return;
        }
        if (editable.length() > 0) {
            if (this.f3357a.v != null) {
                this.f3357a.v.a(0);
            }
            if (!this.f3357a.n && this.f3357a.K != null) {
                Message obtainMessage = this.f3357a.K.obtainMessage(0);
                obtainMessage.obj = editable;
                this.f3357a.K.removeMessages(0);
                this.f3357a.G.a(this.f3357a.I);
                this.f3357a.K.sendMessageDelayed(obtainMessage, 500);
            }
            this.f3357a.n = false;
        } else if (!this.f3357a.J) {
            this.f3357a.finish();
        } else {
            if (this.f3357a.v != null) {
                this.f3357a.v.a(8);
                this.f3357a.v.a(this.f3357a.getString(R.string.search_hint_default));
                String unused3 = this.f3357a.P = (String) null;
                String unused4 = this.f3357a.Q = (String) null;
            }
            this.f3357a.B();
            this.f3357a.C();
            this.f3357a.K.removeMessages(0);
            this.f3357a.G.a(this.f3357a.I);
        }
    }
}
