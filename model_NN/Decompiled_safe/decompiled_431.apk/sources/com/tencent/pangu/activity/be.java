package com.tencent.pangu.activity;

import android.os.Bundle;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.RankFriendsListAdapter;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.module.b;
import com.tencent.assistant.module.y;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ah;
import com.tencent.assistantv2.a.h;
import com.tencent.assistantv2.a.i;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.activity.a;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.manager.RankTabType;
import com.tencent.pangu.adapter.RankNormalListAdapter;
import com.tencent.pangu.adapter.al;
import com.tencent.pangu.adapter.bp;
import com.tencent.pangu.component.RankCustomizeListPage;
import com.tencent.pangu.component.RankFriendsListPage;
import com.tencent.pangu.component.RankNormalListPage;
import com.tencent.pangu.component.RankSecondNavigationView;
import com.tencent.pangu.component.TXSecondViewPager;
import com.tencent.pangu.component.au;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class be extends a implements UIEventListener, au {
    protected List<View> R = new ArrayList();
    protected List<Object> S = new ArrayList();
    private final String T = "HotTabActivity:";
    private h U;
    private HashMap<String, bl> V = new HashMap<>();
    private HashMap<Integer, Boolean> W = new HashMap<>();
    private LinearLayout X;
    private TXSecondViewPager Y;
    private View Z;
    private View aa;
    /* access modifiers changed from: private */
    public int ab = -1;
    private int ac = -1;
    private bp ad = null;
    /* access modifiers changed from: private */
    public RankSecondNavigationView ae;
    private boolean af = true;
    private ApkResCallback.Stub ag = new bh(this);
    private ViewPager.OnPageChangeListener ah = new bj(this);
    private ListViewScrollListener ai = new bk(this);

    public void d(Bundle bundle) {
        super.d(bundle);
        System.currentTimeMillis();
        this.X = new LinearLayout(this.P);
        a(this.X);
    }

    public void b(View view) {
        this.Z = view.findViewById(R.id.filterTip);
        this.aa = this.Z.findViewById(R.id.tip_filterBtn);
        this.Z.setVisibility(8);
        this.Y = (TXSecondViewPager) view.findViewById(R.id.vPager);
        this.ae = (RankSecondNavigationView) view.findViewById(R.id.navigationview);
        this.ae.a(this);
        this.U = com.tencent.assistantv2.a.a.a().a(1);
        ArrayList arrayList = new ArrayList();
        for (i iVar : this.U.b) {
            arrayList.add(iVar.f1863a);
        }
        this.ae.a(arrayList);
        this.ad = new bp(this.R);
        this.Y.setAdapter(this.ad);
        this.Y.setOnPageChangeListener(this.ah);
        this.Y.a(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.activity.be.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.pangu.activity.be.a(com.tencent.pangu.activity.be, int):int
      com.tencent.pangu.activity.be.a(int, long):boolean
      com.tencent.pangu.activity.be.a(com.tencent.assistantv2.a.i, com.tencent.pangu.activity.bl):boolean
      android.support.v4.app.Fragment.a(android.content.Context, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.Fragment.a(int, android.support.v4.app.Fragment):void
      android.support.v4.app.Fragment.a(android.content.Intent, int):void
      android.support.v4.app.Fragment.a(android.view.Menu, android.view.MenuInflater):void
      android.support.v4.app.Fragment.a(android.view.View, android.os.Bundle):void
      com.tencent.pangu.activity.be.a(int, boolean):void */
    public void d(boolean z) {
        int i;
        MainActivity mainActivity = null;
        super.j();
        if (this.af) {
            this.X.removeAllViews();
            try {
                View inflate = this.Q.inflate((int) R.layout.act3, (ViewGroup) null);
                this.X.addView(inflate);
                this.X.requestLayout();
                this.X.forceLayout();
                this.X.invalidate();
                b(inflate);
                this.af = false;
            } catch (Throwable th) {
                t.a().b();
                return;
            }
        }
        if (this.P instanceof MainActivity) {
            mainActivity = (MainActivity) this.P;
        }
        if (mainActivity != null) {
            i = mainActivity.u().getInt("colnum") - 1;
        } else {
            i = -1;
        }
        if (i >= 0 && i <= this.U.b.size() - 1) {
            a(i, false);
            ah.a().postDelayed(new bf(this), 200);
        } else if (this.ab == -1) {
            a(0, false);
            this.ae.a(0);
        } else {
            a(this.ab, false);
        }
        for (Object next : this.S) {
            if (next instanceof RankNormalListAdapter) {
                ((RankNormalListAdapter) next).c();
                ((RankNormalListAdapter) next).notifyDataSetChanged();
            } else if (next instanceof al) {
                ((al) next).a();
                ((al) next).notifyDataSetChanged();
            }
        }
        Iterator<String> it = this.V.keySet().iterator();
        if (it != null) {
            while (it.hasNext()) {
                bl blVar = this.V.get(it.next());
                if (!(blVar == null || blVar.f3336a == null)) {
                    if (blVar.f3336a instanceof RankNormalListPage) {
                        ((RankNormalListPage) blVar.f3336a).d();
                    }
                    if (blVar.f3336a instanceof RankCustomizeListPage) {
                        ((RankCustomizeListPage) blVar.f3336a).e();
                    }
                }
            }
        }
        ApkResourceManager.getInstance().registerApkResCallback(this.ag);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_HOTTAB_DOWNLOAD_FILTER_SHOW, this);
        ah.a().postDelayed(new bg(this), 200);
    }

    public void k() {
        super.k();
        for (Object next : this.S) {
            if (next instanceof RankNormalListAdapter) {
                ((RankNormalListAdapter) next).b();
            } else if (next instanceof al) {
                ((al) next).b();
            }
        }
        ApkResourceManager.getInstance().unRegisterApkResCallback(this.ag);
    }

    public void n() {
        super.n();
        Iterator<String> it = this.V.keySet().iterator();
        if (it != null) {
            while (it.hasNext()) {
                bl blVar = this.V.get(it.next());
                if (!(blVar == null || blVar.f3336a == null)) {
                    if (blVar.f3336a instanceof RankNormalListPage) {
                        ((RankNormalListPage) blVar.f3336a).c();
                    }
                    if (blVar.f3336a instanceof RankCustomizeListPage) {
                        ((RankCustomizeListPage) blVar.f3336a).d();
                    }
                }
            }
        }
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_HOTTAB_DOWNLOAD_FILTER_SHOW, this);
    }

    public int G() {
        if (this.R == null || this.R.size() <= this.ab || this.ab < 0) {
            return STConst.ST_PAGE_RANK_HOT;
        }
        View view = this.R.get(this.ab);
        if (view instanceof RankNormalListPage) {
            return ((RankNormalListPage) this.R.get(this.ab)).a();
        }
        return view instanceof RankCustomizeListPage ? ((RankCustomizeListPage) this.R.get(this.ab)).f() : STConst.ST_PAGE_RANK_HOT;
    }

    public be() {
        super(MainActivity.t());
    }

    public void D() {
    }

    public int E() {
        return 0;
    }

    public int F() {
        return 2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.activity.be.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.pangu.activity.be.a(com.tencent.pangu.activity.be, int):int
      com.tencent.pangu.activity.be.a(int, long):boolean
      com.tencent.pangu.activity.be.a(com.tencent.assistantv2.a.i, com.tencent.pangu.activity.bl):boolean
      android.support.v4.app.Fragment.a(android.content.Context, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.Fragment.a(int, android.support.v4.app.Fragment):void
      android.support.v4.app.Fragment.a(android.content.Intent, int):void
      android.support.v4.app.Fragment.a(android.view.Menu, android.view.MenuInflater):void
      android.support.v4.app.Fragment.a(android.view.View, android.os.Bundle):void
      com.tencent.pangu.activity.be.a(int, boolean):void */
    public void d(int i) {
        a(i, true);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0047 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0157  */
    /* JADX WARNING: Removed duplicated region for block: B:63:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(int r8, boolean r9) {
        /*
            r7 = this;
            r6 = 1
            com.tencent.assistantv2.manager.a r0 = com.tencent.assistantv2.manager.a.a()
            com.tencent.assistantv2.manager.b r0 = r0.b()
            boolean r4 = r0.d()
            r2 = 0
            com.tencent.assistantv2.a.h r0 = r7.U
            java.util.List<com.tencent.assistantv2.a.i> r0 = r0.b
            java.lang.Object r0 = r0.get(r8)
            com.tencent.assistantv2.a.i r0 = (com.tencent.assistantv2.a.i) r0
            java.util.HashMap<java.lang.String, com.tencent.pangu.activity.bl> r1 = r7.V
            java.lang.String r3 = r0.f1863a
            java.lang.Object r1 = r1.get(r3)
            com.tencent.pangu.activity.bl r1 = (com.tencent.pangu.activity.bl) r1
            if (r0 == 0) goto L_0x0173
            if (r1 == 0) goto L_0x0173
            boolean r3 = r7.a(r0, r1)
            if (r3 == 0) goto L_0x0173
            int r3 = r0.b
            com.tencent.assistantv2.manager.RankTabType r5 = com.tencent.assistantv2.manager.RankTabType.APPLIST
            int r5 = r5.ordinal()
            if (r3 != r5) goto L_0x004c
            com.tencent.pangu.adapter.RankNormalListAdapter r3 = r1.b
            if (r3 == 0) goto L_0x0173
            android.view.View r1 = r1.f3336a
            com.tencent.pangu.component.RankNormalListPage r1 = (com.tencent.pangu.component.RankNormalListPage) r1
            boolean r1 = r1.h()
            r3 = r1
        L_0x0043:
            int r1 = r7.ab
            if (r8 != r1) goto L_0x005a
            if (r4 != 0) goto L_0x005a
            if (r3 != 0) goto L_0x005a
        L_0x004b:
            return
        L_0x004c:
            com.tencent.pangu.adapter.al r3 = r1.c
            if (r3 == 0) goto L_0x0173
            android.view.View r1 = r1.f3336a
            com.tencent.pangu.component.RankCustomizeListPage r1 = (com.tencent.pangu.component.RankCustomizeListPage) r1
            boolean r1 = r1.g()
            r3 = r1
            goto L_0x0043
        L_0x005a:
            com.tencent.assistantv2.a.h r1 = r7.U
            java.util.List<com.tencent.assistantv2.a.i> r1 = r1.b
            java.util.Iterator r4 = r1.iterator()
        L_0x0062:
            boolean r1 = r4.hasNext()
            if (r1 == 0) goto L_0x0090
            java.lang.Object r1 = r4.next()
            com.tencent.assistantv2.a.i r1 = (com.tencent.assistantv2.a.i) r1
            java.util.HashMap<java.lang.String, com.tencent.pangu.activity.bl> r2 = r7.V
            java.lang.String r5 = r1.f1863a
            boolean r2 = r2.containsKey(r5)
            if (r2 != 0) goto L_0x007c
            r7.b(r1)
            goto L_0x0062
        L_0x007c:
            java.util.HashMap<java.lang.String, com.tencent.pangu.activity.bl> r2 = r7.V
            java.lang.String r5 = r1.f1863a
            java.lang.Object r2 = r2.get(r5)
            com.tencent.pangu.activity.bl r2 = (com.tencent.pangu.activity.bl) r2
            boolean r2 = r7.a(r1, r2)
            if (r2 != 0) goto L_0x0062
            r7.b(r1)
            goto L_0x0062
        L_0x0090:
            if (r0 == 0) goto L_0x00e6
            java.util.HashMap<java.lang.String, com.tencent.pangu.activity.bl> r1 = r7.V
            java.lang.String r2 = r0.f1863a
            java.lang.Object r1 = r1.get(r2)
            com.tencent.pangu.activity.bl r1 = (com.tencent.pangu.activity.bl) r1
            boolean r2 = r7.a(r0, r1)
            if (r2 == 0) goto L_0x00e6
            int r2 = r0.b
            com.tencent.assistantv2.manager.RankTabType r4 = com.tencent.assistantv2.manager.RankTabType.APPLIST
            int r4 = r4.ordinal()
            if (r2 != r4) goto L_0x010e
            com.tencent.pangu.adapter.RankNormalListAdapter r2 = r1.b
            if (r2 == 0) goto L_0x00df
            java.util.HashMap<java.lang.Integer, java.lang.Boolean> r2 = r7.W
            java.lang.Integer r4 = java.lang.Integer.valueOf(r8)
            boolean r2 = r2.containsKey(r4)
            if (r2 == 0) goto L_0x00be
            if (r3 == 0) goto L_0x0108
        L_0x00be:
            java.util.HashMap<java.lang.Integer, java.lang.Boolean> r2 = r7.W
            java.lang.Integer r3 = java.lang.Integer.valueOf(r8)
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r6)
            r2.put(r3, r4)
            int r0 = r7.a(r0)
            com.tencent.assistantv2.st.business.CostTimeSTManager$TIMETYPE r2 = com.tencent.assistantv2.st.business.CostTimeSTManager.TIMETYPE.START
            long r3 = java.lang.System.currentTimeMillis()
            com.tencent.assistantv2.st.l.a(r0, r2, r3)
            android.view.View r0 = r1.f3336a
            com.tencent.pangu.component.RankNormalListPage r0 = (com.tencent.pangu.component.RankNormalListPage) r0
            r0.b()
        L_0x00df:
            com.tencent.pangu.component.TXSecondViewPager r0 = r7.Y
            r0.setCurrentItem(r8)
            r7.ab = r8
        L_0x00e6:
            java.util.List<android.view.View> r0 = r7.R
            int r1 = r7.ab
            java.lang.Object r0 = r0.get(r1)
            boolean r0 = r0 instanceof com.tencent.pangu.component.RankNormalListPage
            if (r0 == 0) goto L_0x0157
            java.util.List<android.view.View> r0 = r7.R
            int r1 = r7.ab
            java.lang.Object r0 = r0.get(r1)
            com.tencent.pangu.component.RankNormalListPage r0 = (com.tencent.pangu.component.RankNormalListPage) r0
            com.tencent.pangu.component.RankNormalListPage r0 = (com.tencent.pangu.component.RankNormalListPage) r0
            r0.d()
        L_0x0101:
            if (r9 == 0) goto L_0x004b
            r7.B()
            goto L_0x004b
        L_0x0108:
            com.tencent.pangu.adapter.RankNormalListAdapter r0 = r1.b
            r0.notifyDataSetChanged()
            goto L_0x00df
        L_0x010e:
            int r2 = r0.b
            com.tencent.assistantv2.manager.RankTabType r4 = com.tencent.assistantv2.manager.RankTabType.LISTGROUP
            int r4 = r4.ordinal()
            if (r2 != r4) goto L_0x00df
            com.tencent.pangu.adapter.al r2 = r1.c
            if (r2 == 0) goto L_0x00df
            java.util.HashMap<java.lang.Integer, java.lang.Boolean> r2 = r7.W
            java.lang.Integer r4 = java.lang.Integer.valueOf(r8)
            boolean r2 = r2.containsKey(r4)
            if (r2 == 0) goto L_0x012a
            if (r3 == 0) goto L_0x0151
        L_0x012a:
            java.util.HashMap<java.lang.Integer, java.lang.Boolean> r2 = r7.W
            java.lang.Integer r3 = java.lang.Integer.valueOf(r8)
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r6)
            r2.put(r3, r4)
            int r2 = r7.a(r0)
            com.tencent.assistantv2.st.business.CostTimeSTManager$TIMETYPE r3 = com.tencent.assistantv2.st.business.CostTimeSTManager.TIMETYPE.START
            long r4 = java.lang.System.currentTimeMillis()
            com.tencent.assistantv2.st.l.a(r2, r3, r4)
            android.view.View r1 = r1.f3336a
            com.tencent.pangu.component.RankCustomizeListPage r1 = (com.tencent.pangu.component.RankCustomizeListPage) r1
            int r2 = r0.f
            long r3 = r0.e
            int r0 = (int) r3
            r1.a(r2, r0)
            goto L_0x00df
        L_0x0151:
            com.tencent.pangu.adapter.al r0 = r1.c
            r0.notifyDataSetChanged()
            goto L_0x00df
        L_0x0157:
            java.util.List<android.view.View> r0 = r7.R
            int r1 = r7.ab
            java.lang.Object r0 = r0.get(r1)
            boolean r0 = r0 instanceof com.tencent.pangu.component.RankCustomizeListPage
            if (r0 == 0) goto L_0x0101
            java.util.List<android.view.View> r0 = r7.R
            int r1 = r7.ab
            java.lang.Object r0 = r0.get(r1)
            com.tencent.pangu.component.RankCustomizeListPage r0 = (com.tencent.pangu.component.RankCustomizeListPage) r0
            com.tencent.pangu.component.RankCustomizeListPage r0 = (com.tencent.pangu.component.RankCustomizeListPage) r0
            r0.e()
            goto L_0x0101
        L_0x0173:
            r3 = r2
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.pangu.activity.be.a(int, boolean):void");
    }

    private int a(i iVar) {
        if (iVar != null) {
            if (iVar.b == RankTabType.APPLIST.ordinal()) {
                if (iVar.e == 0) {
                    if (iVar.f == 6) {
                        return STConst.ST_PAGE_RANK_HOT;
                    }
                    if (iVar.f == 5) {
                        return STConst.ST_PAGE_RANK_CLASSIC;
                    }
                }
                if (iVar.f == 99) {
                    return STConst.ST_PAGE_RANK_FRIENDS;
                }
            } else if (iVar.b == RankTabType.LISTGROUP.ordinal() && iVar.f == 2) {
                return STConst.ST_PAGE_RANK_RECOMMEND;
            } else {
                return -1;
            }
        }
        return -1;
    }

    private boolean a(i iVar, bl blVar) {
        if (iVar == null) {
            return true;
        }
        if (blVar != null) {
            if (blVar.f3336a == null) {
                return false;
            }
            if (iVar.b == RankTabType.APPLIST.ordinal() && blVar.b == null) {
                return false;
            }
            if (iVar.b == RankTabType.LISTGROUP.ordinal() && blVar.c == null) {
                return false;
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.adapter.al.a(int, long):void
     arg types: [?, int]
     candidates:
      com.tencent.pangu.adapter.al.a(int, int):int
      com.tencent.pangu.adapter.al.a(java.util.Map<com.tencent.assistant.model.AppGroupInfo, java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>>, long):void
      com.tencent.pangu.adapter.al.a(int, long):void */
    private void b(i iVar) {
        RankNormalListPage rankNormalListPage;
        RankNormalListAdapter rankNormalListAdapter;
        if (iVar == null) {
            return;
        }
        if (iVar.b == RankTabType.APPLIST.ordinal()) {
            b bVar = new b(iVar.e, iVar.f, (short) iVar.g);
            if (c(iVar)) {
                rankNormalListPage = new RankFriendsListPage(this.P, TXScrollViewBase.ScrollMode.PULL_FROM_END, bVar);
            } else {
                rankNormalListPage = new RankNormalListPage(this.P, TXScrollViewBase.ScrollMode.PULL_FROM_END, bVar);
            }
            rankNormalListPage.a(this);
            rankNormalListPage.d();
            this.R.add(rankNormalListPage);
            if (c(iVar)) {
                rankNormalListAdapter = new RankFriendsListAdapter(this.P, rankNormalListPage, bVar.a());
            } else {
                rankNormalListAdapter = new RankNormalListAdapter(this.P, rankNormalListPage, bVar.a());
            }
            rankNormalListAdapter.a(a(iVar), -100);
            rankNormalListAdapter.a(RankNormalListAdapter.ListType.LISTTYPEGAMESORT);
            rankNormalListAdapter.b(ListItemInfoView.InfoType.DOWNTIMES_SIZE.ordinal());
            rankNormalListAdapter.a(a(iVar.h));
            rankNormalListPage.a(rankNormalListAdapter, this.ai);
            rankNormalListAdapter.c();
            this.S.add(rankNormalListAdapter);
            this.V.put(iVar.f1863a, new bl(this, rankNormalListPage, rankNormalListAdapter, null));
        } else if (iVar.b == RankTabType.LISTGROUP.ordinal()) {
            RankCustomizeListPage rankCustomizeListPage = new RankCustomizeListPage(this.P, new y(a(iVar.f, iVar.e)), iVar.f, (int) iVar.e);
            rankCustomizeListPage.a(this);
            rankCustomizeListPage.e();
            this.R.add(rankCustomizeListPage);
            al alVar = new al(this.P, rankCustomizeListPage, STConst.ST_PAGE_COMPETITIVE);
            alVar.a((int) STConst.ST_PAGE_RANK_RECOMMEND, -100L);
            rankCustomizeListPage.a(alVar, this.ai);
            alVar.a();
            this.S.add(alVar);
            this.V.put(iVar.f1863a, new bl(this, rankCustomizeListPage, null, alVar));
        }
    }

    private boolean a(int i, long j) {
        return b(i, j);
    }

    private boolean b(int i, long j) {
        return i == 2 && j == 0;
    }

    private boolean a(byte b) {
        return (b & 1) == 1;
    }

    private void H() {
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_HOTTAB_DOWNLOAD_FILTER_SHOW:
                H();
                return;
            default:
                return;
        }
    }

    private boolean c(i iVar) {
        if (iVar == null || iVar.f != 99) {
            return false;
        }
        return true;
    }

    public void C() {
        B();
    }
}
