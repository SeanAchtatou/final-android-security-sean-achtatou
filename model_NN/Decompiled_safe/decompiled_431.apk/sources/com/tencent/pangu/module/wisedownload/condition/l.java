package com.tencent.pangu.module.wisedownload.condition;

import com.qq.ndk.NativeFileObject;
import com.tencent.assistant.protocol.jce.AutoDownloadCfg;
import com.tencent.assistant.protocol.jce.AutoDownloadItemCfg;
import com.tencent.assistant.utils.bo;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.module.wisedownload.b;
import com.tencent.pangu.module.wisedownload.condition.ThresholdCondition;
import com.tencent.pangu.module.wisedownload.s;
import com.tencent.pangu.module.wisedownload.v;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class l extends ThresholdCondition {
    private int c;
    private int d;
    private int e;

    public l(b bVar) {
        a(bVar);
    }

    public void a(b bVar) {
        AutoDownloadCfg j;
        AutoDownloadItemCfg autoDownloadItemCfg;
        if (bVar != null && (j = bVar.j()) != null) {
            this.c = j.c;
            if (j.n != null && (autoDownloadItemCfg = j.n.get(4)) != null) {
                this.d = autoDownloadItemCfg.f1167a;
                this.e = autoDownloadItemCfg.b;
            }
        }
    }

    public boolean a() {
        a(ThresholdCondition.CONDITION_RESULT_CODE.OK);
        return n() && b();
    }

    private boolean n() {
        long i = s.i();
        if (i == 0) {
            a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_OTHER_NO_APP);
            return false;
        }
        boolean a2 = a(i + ((long) (this.c * NativeFileObject.S_IFREG)));
        if (a2) {
            return a2;
        }
        a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_OTHER_SPACE);
        return a2;
    }

    public boolean b() {
        List<DownloadInfo> j = s.j();
        ArrayList<DownloadInfo> arrayList = new ArrayList<>();
        ArrayList arrayList2 = new ArrayList();
        if (j == null || j.isEmpty()) {
            boolean z = this.d > 0 || this.e > 0;
            if (z) {
                return z;
            }
            a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_OTHER_MAX_COUNT_NOT_SPECIFIED);
            return z;
        }
        for (DownloadInfo next : j) {
            if (next != null && bo.d(next.downloadEndTime)) {
                arrayList.add(next);
            }
        }
        if (arrayList.size() >= this.e) {
            a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_OTHER_WEEK);
            return false;
        }
        for (DownloadInfo downloadInfo : arrayList) {
            if (downloadInfo != null && bo.b(downloadInfo.downloadEndTime)) {
                arrayList2.add(downloadInfo);
            }
        }
        if (arrayList2.size() < this.d) {
            return true;
        }
        a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_OTHER_DAY);
        return false;
    }

    public boolean h() {
        long i = s.i();
        if (i == 0) {
            return true;
        }
        return a(i + ((long) (this.c * NativeFileObject.S_IFREG)));
    }

    public boolean i() {
        return s.i() != 0;
    }

    public int j() {
        return this.d;
    }

    public int k() {
        List<DownloadInfo> j = s.j();
        ArrayList arrayList = new ArrayList();
        if (j == null || j.isEmpty()) {
            return 0;
        }
        for (DownloadInfo next : j) {
            if (next != null && bo.b(next.downloadEndTime)) {
                arrayList.add(next);
            }
        }
        return arrayList.size();
    }

    public int l() {
        return this.e;
    }

    public int m() {
        int i = 0;
        List<DownloadInfo> a2 = v.a(true, true);
        if (a2 == null || a2.isEmpty()) {
            return 0;
        }
        Iterator<DownloadInfo> it = a2.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            DownloadInfo next = it.next();
            if (next != null && bo.d(next.downloadEndTime)) {
                i2++;
            }
            i = i2;
        }
    }
}
