package com.tencent.pangu.module.wisedownload;

import android.text.TextUtils;
import android.util.Log;

/* compiled from: ProGuard */
public class p {
    boolean A;
    boolean B;
    int C = 0;
    int D = 0;
    int E = 0;
    int F = 0;
    boolean G;
    boolean H;
    int I = 0;
    int J = 0;
    int K = 0;
    int L = 0;
    private boolean M = false;

    /* renamed from: a  reason: collision with root package name */
    int f3970a = 0;
    int b = 0;
    boolean c;
    boolean d;
    boolean e;
    int f = 0;
    boolean g;
    boolean h;
    long i = 0;
    boolean j;
    boolean k;
    boolean l;
    boolean m;
    boolean n;
    boolean o;
    int p = 0;
    int q = 0;
    int r = 0;
    int s = 0;
    boolean t;
    boolean u;
    boolean v;
    int w = 0;
    int x = 0;
    int y = 0;
    int z = 0;

    public void a(int i2) {
        this.f3970a = i2;
    }

    public void b(int i2) {
        this.b = i2;
    }

    public boolean a() {
        return this.M;
    }

    private void b(boolean z2) {
        this.M = z2;
    }

    public void a(boolean z2, boolean z3, boolean z4, int i2, boolean z5, boolean z6, long j2) {
        this.c = z2;
        this.d = z3;
        this.e = z4;
        this.f = i2;
        this.g = z5;
        this.h = z6;
        this.i = j2;
        b(true);
    }

    public void a(boolean z2, boolean z3, boolean z4) {
        this.j = z2;
        this.k = z3;
        this.l = z4;
    }

    public void a(boolean z2, boolean z3, boolean z4, int i2, int i3, int i4, int i5) {
        this.m = z2;
        this.n = z3;
        this.o = z4;
        this.p = i2;
        this.q = i3;
        this.r = i4;
        this.s = i5;
    }

    public void b(boolean z2, boolean z3, boolean z4, int i2, int i3, int i4, int i5) {
        this.t = z2;
        this.u = z3;
        this.v = z4;
        this.w = i2;
        this.x = i3;
        this.y = i4;
        this.z = i5;
    }

    public void a(boolean z2, boolean z3, int i2, int i3, int i4, int i5) {
        this.A = z2;
        this.B = z3;
        this.C = i2;
        this.D = i3;
        this.E = i4;
        this.F = i5;
    }

    public void b(boolean z2, boolean z3, int i2, int i3, int i4, int i5) {
        this.G = z2;
        this.H = z3;
        this.I = i2;
        this.J = this.D;
        this.K = i4;
        this.L = i5;
    }

    public String a(boolean z2) {
        return z2 ? "1" : "0";
    }

    public String a(String str) {
        return !TextUtils.isEmpty(str) ? str : "defalut";
    }

    public String b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(a(this.c));
        stringBuffer.append("_");
        stringBuffer.append(a(this.d));
        stringBuffer.append("_");
        stringBuffer.append(a(this.e));
        stringBuffer.append("_");
        stringBuffer.append(this.f);
        stringBuffer.append("_");
        stringBuffer.append(a(this.g));
        stringBuffer.append("_");
        stringBuffer.append(a(a(this.h)));
        stringBuffer.append("_");
        stringBuffer.append(this.i);
        return stringBuffer.toString();
    }

    public String c() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(a(this.j));
        stringBuffer.append("_");
        stringBuffer.append(a(this.k));
        stringBuffer.append("_");
        stringBuffer.append(a(this.l));
        return stringBuffer.toString();
    }

    public String d() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(a(this.m));
        stringBuffer.append("_");
        stringBuffer.append(a(this.n));
        stringBuffer.append("_");
        stringBuffer.append(a(this.o));
        stringBuffer.append("_");
        stringBuffer.append(this.p);
        stringBuffer.append("_");
        stringBuffer.append(this.q);
        stringBuffer.append("_");
        stringBuffer.append(this.r);
        stringBuffer.append("_");
        stringBuffer.append(this.s);
        return stringBuffer.toString();
    }

    public String e() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(a(this.t));
        stringBuffer.append("_");
        stringBuffer.append(a(this.u));
        stringBuffer.append("_");
        stringBuffer.append(a(this.v));
        stringBuffer.append("_");
        stringBuffer.append(this.w);
        stringBuffer.append("_");
        stringBuffer.append(this.x);
        stringBuffer.append("_");
        stringBuffer.append(this.y);
        stringBuffer.append("_");
        stringBuffer.append(this.z);
        return stringBuffer.toString();
    }

    public String f() {
        String b2 = b();
        String c2 = c();
        String d2 = d();
        String e2 = e();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.f3970a);
        Log.v("AAA", "trigger_action_code=" + this.f3970a);
        stringBuffer.append("_");
        stringBuffer.append(this.b);
        Log.v("AAA", "result=" + this.b);
        stringBuffer.append("_");
        stringBuffer.append(b2);
        Log.v("AAA", "primaryStr=" + b2);
        stringBuffer.append("_");
        stringBuffer.append(c2);
        Log.v("AAA", "selfStr=" + c2);
        stringBuffer.append("_");
        stringBuffer.append(d2);
        Log.v("AAA", "updateStr=" + d2);
        stringBuffer.append("_");
        stringBuffer.append(e2);
        Log.v("AAA", "newAppStr=" + e2);
        return stringBuffer.toString();
    }
}
