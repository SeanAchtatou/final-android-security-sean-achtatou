package com.tencent.pangu.module.c;

import com.tencent.assistant.db.a.a;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class c extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f3930a;

    c(b bVar) {
        this.f3930a = bVar;
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        a aVar;
        if (localApkInfo != null && i == 2 && (aVar = (a) this.f3930a.i.get(localApkInfo.mPackageName)) != null && aVar.f == localApkInfo.mVersionCode) {
            this.f3930a.b(aVar);
        }
    }
}
