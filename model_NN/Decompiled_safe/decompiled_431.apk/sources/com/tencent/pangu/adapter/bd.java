package com.tencent.pangu.adapter;

import android.view.View;

/* compiled from: ProGuard */
class bd implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchMatchAdapter f3433a;

    bd(SearchMatchAdapter searchMatchAdapter) {
        this.f3433a = searchMatchAdapter;
    }

    public void onClick(View view) {
        if (this.f3433a.g != null) {
            this.f3433a.g.onClick(view);
        }
        int i = 0;
        try {
            i = view.getId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.f3433a.a(null, i, null, 200, SearchMatchAdapter.b);
    }
}
