package com.tencent.pangu.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import com.tencent.assistant.model.b;
import com.tencent.assistant.model.d;
import com.tencent.assistantv2.adapter.smartlist.SmartItemType;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class SearchPageListAdapter extends SmartListAdapter {
    public SearchPageListAdapter(Context context, View view, b bVar) {
        super(context, view, bVar);
    }

    /* access modifiers changed from: protected */
    public SmartListAdapter.SmartListType o() {
        return SmartListAdapter.SmartListType.SearchPage;
    }

    /* access modifiers changed from: protected */
    public String b(int i) {
        return "07";
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public STInfoV2 a(int i, com.tencent.pangu.model.b bVar, int i2, d dVar) {
        String str;
        String str2;
        String str3;
        if (bVar == null) {
            return null;
        }
        String str4 = "07";
        String b = b();
        switch (bj.f3439a[SmartItemType.values()[getItemViewType(i)].ordinal()]) {
            case 1:
            case 2:
                if (bVar.c != null) {
                    if (!TextUtils.isEmpty(bVar.c.ae)) {
                        if (bVar.c.ao == null || bVar.c.ao.size() <= 0) {
                            str4 = "05";
                        } else {
                            str4 = "06";
                        }
                    }
                    String str5 = b + ";" + bVar.c.f938a;
                    str3 = str4;
                    str2 = str5;
                    break;
                }
                String str6 = b;
                str3 = str4;
                str2 = str6;
                break;
            case 3:
                str = Constants.VIA_REPORT_TYPE_SET_AVATAR;
                if (bVar.e != null) {
                    str2 = b + ";" + bVar.e.f2320a;
                    str3 = str;
                    break;
                }
                str2 = b;
                str3 = str;
                break;
            case 4:
                str = "08";
                if (bVar.e != null) {
                    str2 = b + ";" + bVar.e.f2320a;
                    str3 = str;
                    break;
                }
                str2 = b;
                str3 = str;
                break;
            case 5:
                str = Constants.VIA_REPORT_TYPE_JOININ_GROUP;
                if (bVar.f != null) {
                    str2 = b + ";" + bVar.f.f2318a;
                    str3 = str;
                    break;
                }
                str2 = b;
                str3 = str;
                break;
            case 6:
                str = "09";
                if (bVar.f != null) {
                    str2 = b + ";" + bVar.f.f2318a;
                    str3 = str;
                    break;
                }
                str2 = b;
                str3 = str;
                break;
            default:
                String str62 = b;
                str3 = str4;
                str2 = str62;
                break;
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(d(), bVar.c, a.a(str3, i), i2, null);
        buildSTInfo.extraData = str2;
        buildSTInfo.searchId = c();
        return buildSTInfo;
    }

    public boolean l() {
        return false;
    }

    public boolean m() {
        return true;
    }
}
