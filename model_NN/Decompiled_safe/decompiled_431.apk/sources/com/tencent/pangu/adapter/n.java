package com.tencent.pangu.adapter;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class n extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ab f3455a;
    final /* synthetic */ DownloadInfoMultiAdapter b;

    n(DownloadInfoMultiAdapter downloadInfoMultiAdapter, ab abVar) {
        this.b = downloadInfoMultiAdapter;
        this.f3455a = abVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.DownloadInfoMultiAdapter, boolean):boolean
     arg types: [com.tencent.pangu.adapter.DownloadInfoMultiAdapter, int]
     candidates:
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(int, int):java.lang.String
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.mediadownload.o, com.tencent.pangu.model.d):java.lang.String
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.DownloadInfoMultiAdapter, java.util.List):java.util.ArrayList
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(java.util.List<com.tencent.pangu.download.DownloadInfoWrapper>, int):java.util.ArrayList<com.tencent.assistant.protocol.jce.InstalledAppItem>
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.DownloadInfoMultiAdapter, com.tencent.assistant.component.invalidater.ViewInvalidateMessage):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.DownloadInfoMultiAdapter, com.tencent.pangu.download.DownloadInfo):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.DownloadInfoMultiAdapter, com.tencent.pangu.download.DownloadInfoWrapper):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.ad, com.tencent.pangu.adapter.DownloadInfoMultiAdapter$CreatingTaskStatusEnum):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.af, com.tencent.pangu.model.d):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.ai, com.tencent.pangu.mediadownload.q):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.aj, boolean):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.download.DownloadInfoWrapper, android.view.View):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(java.util.List<com.tencent.pangu.download.DownloadInfoWrapper>, long):boolean
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(java.util.List<com.tencent.pangu.download.DownloadInfoWrapper>, com.tencent.pangu.download.DownloadInfoWrapper):boolean
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(java.lang.String, com.tencent.assistant.protocol.jce.InstalledAppItem):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.DownloadInfoMultiAdapter, boolean):boolean */
    public void onTMAClick(View view) {
        this.f3455a.c.setVisibility(8);
        boolean unused = this.b.s = true;
        this.b.l();
        this.b.notifyDataSetChanged();
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b.m, 200);
        buildSTInfo.status = Constants.VIA_REPORT_TYPE_JOININ_GROUP;
        return buildSTInfo;
    }
}
