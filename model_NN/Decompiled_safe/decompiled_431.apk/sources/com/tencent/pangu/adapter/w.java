package com.tencent.pangu.adapter;

import android.view.View;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.download.DownloadInfo;

/* compiled from: ProGuard */
class w extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f3464a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ DownloadInfoMultiAdapter c;

    w(DownloadInfoMultiAdapter downloadInfoMultiAdapter, DownloadInfo downloadInfo, STInfoV2 sTInfoV2) {
        this.c = downloadInfoMultiAdapter;
        this.f3464a = downloadInfo;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.c.a(this.f3464a);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.pangu.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.plugin.PluginDownloadInfo, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.k.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.m, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    public STInfoV2 getStInfo() {
        if (this.b != null) {
            AppConst.AppState a2 = k.a(this.f3464a, true, true);
            this.b.status = a.a(a2, (SimpleAppModel) null);
            this.b.actionId = a.a(a2);
        }
        return this.b;
    }
}
