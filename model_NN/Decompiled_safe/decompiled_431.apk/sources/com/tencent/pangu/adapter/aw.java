package com.tencent.pangu.adapter;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.BrowserActivity;
import com.tencent.assistant.model.SimpleAppModel;

/* compiled from: ProGuard */
class aw implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f3425a;
    final /* synthetic */ RankNormalListAdapter b;

    aw(RankNormalListAdapter rankNormalListAdapter, SimpleAppModel simpleAppModel) {
        this.b = rankNormalListAdapter;
        this.f3425a = simpleAppModel;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.b.f, BrowserActivity.class);
        intent.putExtra("com.tencent.assistant.BROWSER_URL", this.f3425a.Z);
        this.b.f.startActivity(intent);
    }
}
