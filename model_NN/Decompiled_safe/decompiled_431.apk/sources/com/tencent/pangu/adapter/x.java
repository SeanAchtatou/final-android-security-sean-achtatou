package com.tencent.pangu.adapter;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.download.DownloadInfo;

/* compiled from: ProGuard */
class x extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f3465a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ DownloadInfoMultiAdapter c;

    x(DownloadInfoMultiAdapter downloadInfoMultiAdapter, DownloadInfo downloadInfo, STInfoV2 sTInfoV2) {
        this.c = downloadInfoMultiAdapter;
        this.f3465a = downloadInfo;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        if (this.f3465a != null) {
            y yVar = new y(this);
            yVar.titleRes = this.c.m.getResources().getString(R.string.downloaded_delete_confirm_title);
            yVar.contentRes = this.c.m.getResources().getString(R.string.downloaded_delete_confirm);
            yVar.rBtnTxtRes = this.c.m.getResources().getString(R.string.downloaded_delete_confirm_btn);
            DialogUtils.show2BtnDialog(yVar);
        }
    }

    public STInfoV2 getStInfo() {
        if (this.b != null) {
            this.b.status = Constants.VIA_REPORT_TYPE_MAKE_FRIEND;
            this.b.actionId = 200;
        }
        return this.b;
    }
}
