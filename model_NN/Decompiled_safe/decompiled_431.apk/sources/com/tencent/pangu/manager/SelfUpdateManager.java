package com.tencent.pangu.manager;

import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.download.a;
import com.tencent.pangu.module.a.l;
import com.tencent.pangu.module.ax;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class SelfUpdateManager implements l {

    /* renamed from: a  reason: collision with root package name */
    private static SelfUpdateManager f3780a;
    private static bo b;
    private static bp c;
    private DownloadInfo d;
    /* access modifiers changed from: private */
    public SelfUpdateInfo e;
    private int f;
    private boolean g;
    /* access modifiers changed from: private */
    public LocalApkInfo h;
    /* access modifiers changed from: private */
    public SelfUpdateType i;
    private SimpleAppModel j;
    private boolean k;
    private boolean l;
    private NetworkMonitor.ConnectivityChangeListener m;

    /* compiled from: ProGuard */
    public enum SelfUpdateType {
        NORMAL,
        FORCE,
        SILENT
    }

    public static synchronized SelfUpdateManager a() {
        SelfUpdateManager selfUpdateManager;
        synchronized (SelfUpdateManager.class) {
            if (f3780a == null) {
                f3780a = new SelfUpdateManager();
            }
            selfUpdateManager = f3780a;
        }
        return selfUpdateManager;
    }

    private SelfUpdateManager() {
        this.d = null;
        this.e = null;
        this.f = 0;
        this.g = false;
        this.i = SelfUpdateType.SILENT;
        this.k = false;
        this.l = false;
        this.m = new bm(this);
        this.f = t.o();
        com.tencent.assistant.manager.t.a().a(this.m);
        ax.a().register(this);
        b = new bo(this);
        c = new bp(this);
    }

    public boolean a(SelfUpdateType selfUpdateType) {
        boolean z;
        boolean z2;
        boolean z3 = false;
        if (this.e == null) {
            return false;
        }
        this.d = b(selfUpdateType);
        if (this.d == null) {
            return false;
        }
        XLog.d("SelfUpdateManager", "startSelfUpdate:uiType:" + this.d.uiType + ",downloadInfo:" + this.d);
        if (selfUpdateType != SelfUpdateType.SILENT) {
            a.a().a(this.d, this.d.uiType);
        } else {
            List<DownloadInfo> e2 = DownloadProxy.a().e(AstApp.i().getPackageName());
            if (e2 == null || e2.isEmpty()) {
                z = true;
            } else {
                boolean z4 = false;
                z = true;
                for (DownloadInfo next : e2) {
                    if (next != null) {
                        if (!l()) {
                            if (next.versionCode >= this.d.versionCode) {
                                z = false;
                            }
                        } else if (next.versionCode > this.d.versionCode) {
                            z = false;
                        }
                    }
                    if (z4 || next.uiType != SimpleDownloadInfo.UIType.NORMAL) {
                        z2 = z4;
                    } else {
                        z2 = true;
                    }
                    z4 = z2;
                }
                z3 = z4;
            }
            if (z && !z3) {
                DownloadProxy.a().d(this.d);
            }
        }
        return true;
    }

    public DownloadInfo b(SelfUpdateType selfUpdateType) {
        if (this.e == null) {
            return null;
        }
        this.i = selfUpdateType;
        this.j = a(this.h, this.e);
        if (this.j != null) {
            this.d = DownloadInfo.createDownloadInfo(this.j, null);
            this.d.autoInstall = false;
        } else {
            StatInfo statInfo = new StatInfo(this.d.apkId, STConst.ST_PAGE_SELF_UPDATE, 0, null, 0);
            statInfo.scene = STConst.ST_PAGE_SELF_UPDATE;
            this.d.updateDownloadInfoStatInfo(statInfo);
        }
        if (this.d == null) {
            return null;
        }
        this.d.uiType = this.i == SelfUpdateType.SILENT ? SimpleDownloadInfo.UIType.WISE_SELF_UPDAET : SimpleDownloadInfo.UIType.NORMAL;
        if (this.d.uiType == SimpleDownloadInfo.UIType.NORMAL) {
            this.d.autoInstall = false;
        }
        return this.d;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.download.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.a, java.util.List):java.util.List
      com.tencent.pangu.download.a.a(java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>, boolean):void
      com.tencent.pangu.download.a.a(long, long):boolean
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.a, boolean):boolean
      com.tencent.pangu.download.a.a(java.lang.String, int):boolean
      com.tencent.pangu.download.a.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, com.tencent.assistantv2.st.model.StatInfo):int
      com.tencent.pangu.download.a.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo):void
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.DownloadInfo, com.tencent.pangu.download.SimpleDownloadInfo$UIType):void
      com.tencent.pangu.download.a.a(java.lang.String, com.tencent.cloud.a.f):void
      com.tencent.pangu.download.a.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.DownloadInfo, boolean):boolean
      com.tencent.pangu.download.a.a(java.lang.String, boolean):boolean */
    public boolean b() {
        if (this.d == null || this.i == SelfUpdateType.SILENT) {
            return false;
        }
        return a.a().a(this.d.downloadTicket, false);
    }

    public synchronized boolean c() {
        XLog.d("SelfUpdateManager", "updateDownloadFail:downloadinfo:" + this.d);
        DownloadProxy.a().c(this.d);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.SelfUpdateManager.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.tencent.pangu.manager.SelfUpdateManager.a(com.tencent.pangu.manager.SelfUpdateManager, com.tencent.assistant.localres.model.LocalApkInfo):com.tencent.assistant.localres.model.LocalApkInfo
      com.tencent.pangu.manager.SelfUpdateManager.a(com.tencent.assistant.localres.model.LocalApkInfo, com.tencent.pangu.manager.SelfUpdateManager$SelfUpdateInfo):com.tencent.assistant.model.SimpleAppModel
      com.tencent.pangu.manager.SelfUpdateManager.a(boolean, boolean):void */
    public void a(boolean z) {
        a(z, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    public void a(boolean z, boolean z2) {
        b(z);
        m.a().b("update_force_downloading", (Object) false);
        if (this.h != null) {
            ax.a().a(this.h.manifestMd5, h(), z2, this.h.mInstallDate);
            return;
        }
        ApkResourceManager.getInstance().getSelfInfo(new bn(this, z2));
    }

    public void a(SelfUpdateInfo selfUpdateInfo) {
        this.e = selfUpdateInfo;
        if (!k()) {
            b.a(selfUpdateInfo);
        }
    }

    public SelfUpdateInfo d() {
        if (this.e == null) {
            this.e = ax.a().b();
        }
        return this.e;
    }

    public String e() {
        if (this.h != null) {
            return this.h.mLocalFilePath;
        }
        return null;
    }

    public String f() {
        if (this.h != null) {
            return this.h.mAppName;
        }
        return AstApp.i().getString(R.string.app_name);
    }

    public String g() {
        if (this.h == null) {
            return "000000";
        }
        if (this.j == null) {
            this.j = a(this.h, this.e);
        }
        return this.j.q();
    }

    public void b(boolean z) {
        this.g = z;
    }

    public boolean h() {
        return this.g;
    }

    public DownloadInfo i() {
        return this.d;
    }

    public boolean j() {
        boolean k2 = k();
        int m2 = m();
        if (h() || ((m2 > this.f || (m2 == this.f && l())) && this.e != null && (k2 || (this.e.u && b.a(m2) && !this.k)))) {
            return true;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean k() {
        return m.a().a("update_force", false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean l() {
        return m.a().a("same_version_cover", false);
    }

    public int m() {
        return m.a().a("update_newest_versioncode", 0);
    }

    public static SimpleAppModel a(LocalApkInfo localApkInfo, SelfUpdateInfo selfUpdateInfo) {
        SimpleAppModel simpleAppModel;
        if (localApkInfo == null) {
            simpleAppModel = new SimpleAppModel();
        } else {
            simpleAppModel = new SimpleAppModel(localApkInfo);
        }
        if (selfUpdateInfo != null) {
            simpleAppModel.b = selfUpdateInfo.b;
            simpleAppModel.f938a = selfUpdateInfo.f3781a;
            if (!TextUtils.isEmpty(selfUpdateInfo.c)) {
                simpleAppModel.e = selfUpdateInfo.c;
            } else {
                simpleAppModel.e = "http://appimg1.3g.qq.com/android/50801/16629897/icon_72.png";
            }
            simpleAppModel.g = selfUpdateInfo.e;
            simpleAppModel.f = selfUpdateInfo.f;
            simpleAppModel.o = selfUpdateInfo.h;
            simpleAppModel.n = selfUpdateInfo.g;
            simpleAppModel.m = selfUpdateInfo.t;
            simpleAppModel.d = selfUpdateInfo.d;
            simpleAppModel.i = selfUpdateInfo.i;
            simpleAppModel.j = selfUpdateInfo.j;
            simpleAppModel.k = selfUpdateInfo.k;
            simpleAppModel.l = selfUpdateInfo.l;
            simpleAppModel.t = selfUpdateInfo.m;
            simpleAppModel.u = selfUpdateInfo.n;
            simpleAppModel.v = selfUpdateInfo.p;
            simpleAppModel.w = selfUpdateInfo.o;
            simpleAppModel.z = selfUpdateInfo.r;
            simpleAppModel.A = selfUpdateInfo.s;
            simpleAppModel.ac = selfUpdateInfo.y;
        }
        return simpleAppModel;
    }

    public void onCheckSelfUpdateFinish(int i2, int i3, SelfUpdateInfo selfUpdateInfo) {
        if (!a().j()) {
            a().a(SelfUpdateType.SILENT);
        }
    }

    /* compiled from: ProGuard */
    public class SelfUpdateInfo implements Serializable {
        public int A = 0;

        /* renamed from: a  reason: collision with root package name */
        public long f3781a;
        public long b;
        public String c;
        public String d;
        public int e;
        public String f;
        public long g;
        public String h;
        public String i;
        public ArrayList<String> j = new ArrayList<>();
        public long k;
        public String l;
        public String m;
        public ArrayList<String> n = new ArrayList<>();
        public String o;
        public long p;
        public String q;
        public String r;
        public int s;
        public String t = null;
        public boolean u = false;
        public int v = 1;
        public long w = 0;
        public String x = null;
        public String y = null;
        public int z = 0;

        public long a() {
            if (b()) {
                return this.p;
            }
            return this.k;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
         arg types: [java.lang.String, int]
         candidates:
          com.tencent.assistant.m.a(java.lang.String, byte):byte
          com.tencent.assistant.m.a(java.lang.String, int):int
          com.tencent.assistant.m.a(java.lang.String, long):long
          com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
          com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
          com.tencent.assistant.m.a(byte, byte):void
          com.tencent.assistant.m.a(byte, int):void
          com.tencent.assistant.m.a(byte, long):void
          com.tencent.assistant.m.a(byte, java.lang.String):void
          com.tencent.assistant.m.a(int, byte[]):void
          com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
          com.tencent.assistant.m.a(java.lang.String, byte[]):void
          com.tencent.assistant.m.a(long, int):boolean
          com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
          com.tencent.assistant.m.a(java.lang.Long, int):boolean
          com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
        public boolean b() {
            boolean z2;
            boolean z3;
            String e2 = SelfUpdateManager.a().e();
            if (TextUtils.isEmpty(e2)) {
                z2 = false;
            } else if (!new File(e2).exists()) {
                z2 = false;
            } else {
                z2 = true;
            }
            boolean a2 = m.a().a("update_isdiff", true);
            if (!TextUtils.isEmpty(this.m)) {
                z3 = true;
            } else {
                z3 = false;
            }
            if (!a2 || !z3 || !z2) {
                return false;
            }
            DownloadInfo d2 = DownloadProxy.a().d(SelfUpdateManager.a().g());
            if (d2 == null || d2.sllUpdate == 1) {
                return true;
            }
            return false;
        }
    }

    public bo n() {
        return b;
    }

    public bp o() {
        return c;
    }

    public boolean p() {
        return this.k;
    }

    public void c(boolean z) {
        this.k = z;
    }

    public boolean a(int i2) {
        LocalApkInfo localApkInfoByFileName = ApkResourceManager.getInstance().getLocalApkInfoByFileName(AstApp.i().getPackageName(), i2, 0);
        if (localApkInfoByFileName == null || TextUtils.isEmpty(localApkInfoByFileName.mLocalFilePath)) {
            return false;
        }
        return true;
    }

    public boolean b(int i2) {
        LocalApkInfo localApkInfoByFileName = ApkResourceManager.getInstance().getLocalApkInfoByFileName(AstApp.i().getPackageName(), i2, 0);
        if (localApkInfoByFileName == null || TextUtils.isEmpty(localApkInfoByFileName.mLocalFilePath)) {
            return true;
        }
        File file = new File(localApkInfoByFileName.mLocalFilePath);
        if (file.exists()) {
            return file.delete();
        }
        return true;
    }

    public boolean q() {
        return this.l;
    }

    public void d(boolean z) {
        this.l = z;
    }
}
