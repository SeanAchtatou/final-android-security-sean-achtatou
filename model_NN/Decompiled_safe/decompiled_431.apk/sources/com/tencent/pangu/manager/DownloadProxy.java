package com.tencent.pangu.manager;

import android.app.Dialog;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.db.table.l;
import com.tencent.assistant.event.EventDispatcher;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.notification.v;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.g;
import com.tencent.connect.common.Constants;
import com.tencent.downloadsdk.DownloadManager;
import com.tencent.downloadsdk.DownloadTask;
import com.tencent.downloadsdk.ae;
import com.tencent.downloadsdk.af;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.download.a;
import com.tencent.pangu.download.m;
import com.tencent.pangu.mediadownload.b;
import com.tencent.pangu.mediadownload.e;
import com.tencent.pangu.mediadownload.r;
import com.tencent.pangu.model.c;
import com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager;
import com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

/* compiled from: ProGuard */
public class DownloadProxy implements UIEventListener, NetworkMonitor.ConnectivityChangeListener {

    /* renamed from: a  reason: collision with root package name */
    static DownloadProxy f3778a;
    DownloadTaskQueue b;
    public ae c = new o(this);
    /* access modifiers changed from: private */
    public EventDispatcher d = AstApp.i().j();
    /* access modifiers changed from: private */
    public l e = new l(AstApp.i());
    private c f = new c();
    private u g = new u(this, null);
    /* access modifiers changed from: private */
    public Dialog h = null;
    private Set<String> i = new HashSet(5);
    private volatile boolean j = false;
    /* access modifiers changed from: private */
    public InstallUninstallDialogManager k;
    private boolean l = false;
    /* access modifiers changed from: private */
    public APN m = APN.NO_NETWORK;
    private r n = r.c();
    private e o = e.c();

    private DownloadProxy() {
        if (this.g != null) {
            ApkResourceManager.getInstance().registerApkResCallback(this.g);
        }
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        AstApp.i().k().addUIEventListener(1027, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_GOFRONT, this);
        t.a().a(this);
        TemporaryThreadManager.get().start(new g(this));
    }

    public static synchronized DownloadProxy a() {
        DownloadProxy downloadProxy;
        synchronized (DownloadProxy.class) {
            if (f3778a == null) {
                f3778a = new DownloadProxy();
            }
            downloadProxy = f3778a;
        }
        return downloadProxy;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.DownloadProxy.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.pangu.manager.DownloadProxy.b(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>):java.lang.String
      com.tencent.pangu.manager.DownloadProxy.b(java.lang.String, boolean):void */
    public void b() {
        Message message;
        EventDispatcher eventDispatcher;
        AppConst.AppState b2;
        ArrayList<DownloadInfo> a2 = this.e.a();
        if (a2 == null) {
            this.j = true;
            return;
        }
        Collections.sort(a2);
        ak.a().f();
        Iterator<DownloadInfo> it = a2.iterator();
        while (it.hasNext()) {
            DownloadInfo next = it.next();
            if (next.fileType != SimpleDownloadInfo.DownloadType.APK || b(next) || (b2 = k.b(next)) == AppConst.AppState.INSTALLED || b2 == AppConst.AppState.DOWNLOADED) {
                try {
                    if (next.downloadState == SimpleDownloadInfo.DownloadState.INSTALLED && !ak.a().b(next) && com.tencent.assistant.utils.e.d(next.packageName, 0) == null) {
                        this.e.b(next.downloadTicket);
                    } else {
                        if (next.response == null || next.response.f <= 0) {
                            if (next.response == null) {
                                next.response = new m();
                            }
                            next.response.f = DownloadInfo.getRandomPercent(next);
                        }
                        if (next.downloadState == SimpleDownloadInfo.DownloadState.QUEUING || next.downloadState == SimpleDownloadInfo.DownloadState.DOWNLOADING || next.downloadState == SimpleDownloadInfo.DownloadState.COMPLETE || next.downloadState == SimpleDownloadInfo.DownloadState.FAIL) {
                            next.downloadState = SimpleDownloadInfo.DownloadState.PAUSED;
                        }
                        if (!a(next)) {
                            next.response.f3766a = 0;
                            if (!(next.downloadState == SimpleDownloadInfo.DownloadState.WAITTING_FOR_WIFI || next.downloadState == SimpleDownloadInfo.DownloadState.SUCC || next.downloadState == SimpleDownloadInfo.DownloadState.INSTALLED)) {
                                next.downloadState = SimpleDownloadInfo.DownloadState.PAUSED;
                            }
                            DownloadManager.a().b(next.getDownloadSubType(), next.downloadTicket);
                        }
                        this.f.a(next.downloadTicket, next);
                        this.d.sendMessage(this.d.obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, null));
                        if (ak.a().b(next)) {
                            ak.a().g(next);
                        }
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                    this.j = true;
                    eventDispatcher = this.d;
                    message = this.d.obtainMessage(EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY, null);
                } catch (Throwable th) {
                    this.j = true;
                    this.d.sendMessage(this.d.obtainMessage(EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY, null));
                    throw th;
                }
            } else {
                b(next.downloadTicket, true);
            }
        }
        r.c();
        b.a();
        this.j = true;
        eventDispatcher = this.d;
        message = this.d.obtainMessage(EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY, null);
        eventDispatcher.sendMessage(message);
    }

    public boolean a(DownloadInfo downloadInfo) {
        if (downloadInfo == null) {
            return false;
        }
        if (downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.INSTALLING || downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.INSTALLED) {
            return true;
        }
        if (downloadInfo.downloadState != SimpleDownloadInfo.DownloadState.SUCC) {
            return af.a(downloadInfo.getDownloadingPath(), downloadInfo.response.e);
        }
        String filePath = downloadInfo.getFilePath();
        if (!TextUtils.isEmpty(filePath)) {
            return new File(filePath).exists();
        }
        return false;
    }

    private boolean e(DownloadInfo downloadInfo) {
        try {
            if (!this.f.a(downloadInfo.downloadTicket)) {
                this.f.a(downloadInfo.downloadTicket, downloadInfo);
                if (downloadInfo.fileType == SimpleDownloadInfo.DownloadType.APK) {
                    w.a().a(downloadInfo.downloadTicket, downloadInfo.packageName, downloadInfo.versionCode, 0);
                }
                return true;
            }
            DownloadInfo b2 = this.f.b(downloadInfo.downloadTicket);
            if (b2 == downloadInfo) {
                return false;
            }
            b2.autoInstall = downloadInfo.autoInstall;
            b2.uiType = downloadInfo.uiType;
            if (b2.response == null || downloadInfo.response == null) {
                return false;
            }
            b2.response.f3766a = downloadInfo.response.f3766a;
            return false;
        } catch (NullPointerException e2) {
            return false;
        }
    }

    public boolean b(DownloadInfo downloadInfo) {
        if (g.a(downloadInfo)) {
            return true;
        }
        if (!downloadInfo.isSslUpdate()) {
            return false;
        }
        downloadInfo.setSslUpdate(false);
        return g.a(downloadInfo);
    }

    private DownloadInfo g(String str) {
        try {
            a.a().b.remove(str);
            DownloadInfo a2 = this.f.a((Object) str);
            if (a2 == null || a2.fileType != SimpleDownloadInfo.DownloadType.APK) {
                return a2;
            }
            w.a().a(str);
            return a2;
        } catch (NullPointerException e2) {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.DownloadProxy.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.pangu.manager.DownloadProxy.b(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>):java.lang.String
      com.tencent.pangu.manager.DownloadProxy.b(java.lang.String, boolean):void */
    public void c(DownloadInfo downloadInfo) {
        if (downloadInfo != null) {
            if (downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.WAITTING_FOR_WIFI && (com.tencent.assistant.net.c.j() != APN.WIFI || com.tencent.assistant.net.c.l())) {
                downloadInfo.initStartDownload();
                e(downloadInfo);
                this.d.sendMessage(this.d.obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, downloadInfo));
                this.e.a(downloadInfo);
            } else if (downloadInfo.fileType != SimpleDownloadInfo.DownloadType.APK || b(downloadInfo)) {
                if (downloadInfo.uiType == SimpleDownloadInfo.UIType.NORMAL && this.h != null && this.h.isShowing()) {
                    this.h.dismiss();
                    this.h = null;
                }
                if (!TextUtils.isEmpty(downloadInfo.downloadTicket)) {
                    int checkCurrentDownloadSucc = downloadInfo.checkCurrentDownloadSucc();
                    if (checkCurrentDownloadSucc > 0) {
                        d(downloadInfo);
                        a(downloadInfo, SimpleDownloadInfo.DownloadState.SUCC);
                        return;
                    }
                    if (checkCurrentDownloadSucc == 0 && ((downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.SUCC || downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.INSTALLING || downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.INSTALLED) && downloadInfo.response != null)) {
                        downloadInfo.response.f3766a = 0;
                    }
                    downloadInfo.initStartDownload();
                    g(downloadInfo);
                    if (downloadInfo.isUpdateApk() && downloadInfo.isSllUpdate() && !ApkResourceManager.getInstance().hasLocalPack(downloadInfo.packageName) && bm.b(downloadInfo.apkUrlList)) {
                        this.d.sendMessage(this.d.obtainMessage(EventDispatcherEnum.UI_EVENT_ST_DOWNLOAD_REPLACE, new String[]{downloadInfo.downloadTicket}));
                        downloadInfo.updateToFullUpdate();
                        this.e.a(downloadInfo);
                    }
                    if (downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.COMPLETE) {
                        boolean z = false;
                        try {
                            z = downloadInfo.makeFinalFile();
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                        if (!z) {
                            downloadInfo.downloadState = SimpleDownloadInfo.DownloadState.PAUSED;
                        }
                        e(downloadInfo);
                        this.e.a(downloadInfo);
                    } else {
                        ArrayList arrayList = new ArrayList(4);
                        if (downloadInfo.fileType == SimpleDownloadInfo.DownloadType.PLUGIN) {
                            downloadInfo.statInfo.b = downloadInfo.versionCode;
                            downloadInfo.statInfo.extraData = downloadInfo.packageName + "_" + downloadInfo.name;
                            arrayList.addAll(downloadInfo.apkUrlList);
                        } else if (downloadInfo.fileType == SimpleDownloadInfo.DownloadType.APK) {
                            if (downloadInfo.getDownloadSubType() == 3) {
                                arrayList.addAll(downloadInfo.sllApkUrlList);
                            } else {
                                arrayList.addAll(downloadInfo.apkUrlList);
                            }
                            downloadInfo.statInfo.b = downloadInfo.versionCode;
                            downloadInfo.statInfo.c = downloadInfo.channelId;
                        }
                        ae a2 = com.tencent.assistantv2.st.l.a(downloadInfo.downloadTicket, downloadInfo.appId, downloadInfo.apkId, (byte) downloadInfo.getDownloadSubType(), downloadInfo.statInfo, downloadInfo.uiType, downloadInfo.fileType);
                        if (downloadInfo.uiType == SimpleDownloadInfo.UIType.NORMAL) {
                            f(downloadInfo);
                        }
                        HashMap hashMap = new HashMap();
                        hashMap.put("B24", Constants.STR_EMPTY + downloadInfo.uiType.ordinal());
                        DownloadTask downloadTask = new DownloadTask(downloadInfo.getDownloadSubType(), downloadInfo.downloadTicket, downloadInfo.appId, downloadInfo.apkId, DownloadInfo.getDownloadingDir(downloadInfo.fileType), downloadInfo.getTmpSaveName(), arrayList, hashMap);
                        if (!downloadInfo.isSslUpdate() || downloadInfo.getDownloadSubType() != 3) {
                            if (com.tencent.assistant.m.a().ag()) {
                                downloadTask.j = downloadInfo.fileSize;
                            }
                        } else if (com.tencent.assistant.m.a().ah()) {
                            downloadTask.j = downloadInfo.sllFileSize;
                        }
                        downloadTask.b = DownloadInfo.getPriority(downloadInfo.fileType, downloadInfo.uiType);
                        downloadInfo.updateDownloadingApkSavePath(downloadTask.d());
                        downloadTask.a(this.c);
                        downloadTask.a(a2);
                        boolean z2 = h() < 2;
                        if (DownloadManager.a().a(downloadTask)) {
                            if (DownloadManager.a().c(downloadInfo.getDownloadSubType(), downloadInfo.downloadTicket) || z2) {
                                downloadInfo.downloadState = SimpleDownloadInfo.DownloadState.DOWNLOADING;
                                if (downloadInfo.fileType == SimpleDownloadInfo.DownloadType.APK && downloadInfo.downloadState != SimpleDownloadInfo.DownloadState.DOWNLOADING) {
                                    this.d.sendMessage(this.d.obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START, downloadInfo.downloadTicket));
                                }
                                a(downloadInfo, SimpleDownloadInfo.DownloadState.DOWNLOADING);
                            } else {
                                downloadInfo.downloadState = SimpleDownloadInfo.DownloadState.QUEUING;
                                a(downloadInfo, SimpleDownloadInfo.DownloadState.QUEUING);
                            }
                        }
                        if (e(downloadInfo) && downloadInfo.fileType == SimpleDownloadInfo.DownloadType.APK) {
                            if (downloadInfo.isUpdate == 1) {
                                this.d.sendMessage(this.d.obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_START));
                            }
                            this.d.sendMessage(this.d.obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, downloadInfo));
                        }
                        TemporaryThreadManager.get().start(new j(this, downloadInfo));
                    }
                }
                if (ak.a().b(downloadInfo)) {
                    ak.a().g(downloadInfo);
                }
            } else {
                b(downloadInfo.downloadTicket, true);
                if (!downloadInfo.isUiTypeWiseDownload()) {
                    e((int) R.string.download_taost_tip_user_current_version_already_new);
                }
            }
        }
    }

    private void f(DownloadInfo downloadInfo) {
        ArrayList<DownloadInfo> a2 = a(false);
        if (a2 != null) {
            ArrayList arrayList = new ArrayList();
            for (DownloadInfo next : a2) {
                if (next != null && next.isUiTypeWiseDownload()) {
                    if (next.downloadState == SimpleDownloadInfo.DownloadState.DOWNLOADING || next.downloadState == SimpleDownloadInfo.DownloadState.QUEUING) {
                        arrayList.add(next);
                    }
                }
            }
            if (arrayList.size() > 0) {
                TemporaryThreadManager.get().start(new k(this, arrayList));
            }
        }
    }

    private void e(int i2) {
        ah.a().post(new l(this, i2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.DownloadProxy.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.pangu.manager.DownloadProxy.b(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>):java.lang.String
      com.tencent.pangu.manager.DownloadProxy.b(java.lang.String, boolean):void */
    private void g(DownloadInfo downloadInfo) {
        DownloadInfo d2;
        if (downloadInfo.fileType == SimpleDownloadInfo.DownloadType.APK) {
            DownloadInfo a2 = a(downloadInfo.packageName, downloadInfo.versionCode);
            if (!(a2 == null || downloadInfo.grayVersionCode == a2.grayVersionCode)) {
                a().b(a2.downloadTicket, true);
            }
            k(downloadInfo);
        }
        if (downloadInfo.isUpdateApk() && (d2 = d(downloadInfo.downloadTicket)) != null && d2.versionCode < downloadInfo.versionCode) {
            a().b(d2.downloadTicket, true);
        }
    }

    public boolean a(String str) {
        return c(str, false);
    }

    private boolean c(String str, boolean z) {
        DownloadInfo d2;
        if (TextUtils.isEmpty(str) || (d2 = d(str)) == null) {
            return false;
        }
        if (d2.downloadState != SimpleDownloadInfo.DownloadState.DOWNLOADING && d2.downloadState != SimpleDownloadInfo.DownloadState.QUEUING && d2.downloadState != SimpleDownloadInfo.DownloadState.COMPLETE && (!z || d2.downloadState != SimpleDownloadInfo.DownloadState.FAIL)) {
            return false;
        }
        DownloadManager.a().a(d2.getDownloadSubType(), str);
        d2.downloadState = SimpleDownloadInfo.DownloadState.PAUSED;
        d2.errorCode = 0;
        a(d2, SimpleDownloadInfo.DownloadState.PAUSED);
        this.e.a(d2);
        if (ak.a().b(d2)) {
            ak.a().g(d2);
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, android.app.Dialog):android.app.Dialog
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, com.tencent.assistant.net.APN):com.tencent.assistant.net.APN
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager):com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>):java.lang.String
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.DownloadInfo, com.tencent.pangu.download.SimpleDownloadInfo$DownloadState):void
      com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, int):com.tencent.pangu.download.DownloadInfo
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>
      com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, boolean):void */
    public void b(String str) {
        try {
            a(str, true);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.DownloadProxy.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.pangu.manager.DownloadProxy.b(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>):java.lang.String
      com.tencent.pangu.manager.DownloadProxy.b(java.lang.String, boolean):void */
    public void a(String str, boolean z) {
        DownloadInfo c2;
        if (!TextUtils.isEmpty(str) && (c2 = c(str)) != null) {
            if (c2.fileType == SimpleDownloadInfo.DownloadType.APK) {
                b(c2.downloadTicket, false);
            } else if (c2.fileType == SimpleDownloadInfo.DownloadType.PLUGIN) {
                this.f.a((Object) str);
                DownloadManager.a().b(c2.getDownloadSubType(), str);
                DownloadManager.a().d(c2.getDownloadSubType(), str);
                this.e.b(str);
                if (z) {
                    this.d.sendMessage(this.d.obtainMessage(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_DELETE, c2));
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, boolean, boolean):void
     arg types: [java.lang.String, boolean, int]
     candidates:
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList):java.lang.String
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, com.tencent.pangu.download.DownloadInfo, com.tencent.pangu.download.SimpleDownloadInfo$DownloadState):void
      com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, int, int):com.tencent.pangu.download.DownloadInfo
      com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, boolean, boolean):void */
    public void b(String str, boolean z) {
        a(str, z, false);
    }

    public void a(String str, boolean z, boolean z2) {
        a(str, z, false, z2);
    }

    public void a(String str, boolean z, boolean z2, boolean z3) {
        DownloadInfo g2;
        if (!TextUtils.isEmpty(str) && (g2 = g(str)) != null) {
            DownloadManager.a().b(g2.getDownloadSubType(), str);
            this.e.b(str);
            if (z || g2.isDownloadInfoNotFinish()) {
                boolean d2 = DownloadManager.a().d(g2.getDownloadSubType(), str);
                af.c(g2.filePath);
                if (g2.downloadState == SimpleDownloadInfo.DownloadState.SUCC && z2) {
                    h(g2.name);
                } else if (z3 && d2) {
                    j(g2);
                }
                if (g2.sllUpdate == 1) {
                    h(g2);
                }
            } else {
                DownloadManager.a().e(g2.getDownloadSubType(), str);
            }
            if (g2.sllUpdate == 1) {
                i(g2);
            }
            if (g2.isUpdate == 1) {
                this.d.sendMessage(this.d.obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_DELETE, str));
            }
            this.d.sendMessage(this.d.obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, g2));
            if (ak.a().b(g2)) {
                ak.a().h(g2);
            }
        }
    }

    private void h(DownloadInfo downloadInfo) {
        if (downloadInfo != null) {
            String filePath = downloadInfo.getFilePath();
            if (!TextUtils.isEmpty(filePath)) {
                File file = new File(filePath);
                if (file.exists()) {
                    file.delete();
                }
            }
        }
    }

    private void i(DownloadInfo downloadInfo) {
        if (downloadInfo != null) {
            String downloadingPath = downloadInfo.getDownloadingPath();
            if (!TextUtils.isEmpty(downloadingPath)) {
                File file = new File(downloadingPath);
                if (file.exists()) {
                    file.delete();
                }
            }
        }
    }

    public DownloadInfo c(String str) {
        if (!TextUtils.isEmpty(str)) {
            return this.f.b(str);
        }
        return null;
    }

    public DownloadInfo d(String str) {
        if (!TextUtils.isEmpty(str)) {
            return this.f.b(str);
        }
        return null;
    }

    public DownloadInfo a(SimpleAppModel simpleAppModel) {
        DownloadInfo d2;
        if (simpleAppModel == null || (d2 = d(simpleAppModel.q())) == null) {
            return null;
        }
        return d2;
    }

    public DownloadInfo a(String str, int i2, int i3) {
        List<DownloadInfo> e2 = e(str);
        if (e2 == null || e2.size() == 0) {
            return null;
        }
        for (DownloadInfo next : e2) {
            if (next.fileType == SimpleDownloadInfo.DownloadType.APK) {
                if (next.versionCode > i2) {
                    return next;
                }
                if (next.versionCode < i2) {
                    continue;
                } else if (next.grayVersionCode == i3) {
                    return next;
                } else {
                    if (next.grayVersionCode > 0 && i3 > 0 && next.grayVersionCode >= i3) {
                        return next;
                    }
                }
            }
        }
        return null;
    }

    public DownloadInfo a(String str, int i2) {
        List<DownloadInfo> e2 = e(str);
        if (e2 == null || e2.size() == 0) {
            return null;
        }
        for (DownloadInfo next : e2) {
            if (next.fileType == SimpleDownloadInfo.DownloadType.APK && next.versionCode == i2) {
                return next;
            }
        }
        return null;
    }

    public List<DownloadInfo> e(String str) {
        if (!TextUtils.isEmpty(str)) {
            ArrayList<DownloadInfo> a2 = a(SimpleDownloadInfo.DownloadType.APK);
            if (!a2.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                Iterator<DownloadInfo> it = a2.iterator();
                while (it.hasNext()) {
                    DownloadInfo next = it.next();
                    if (next != null && next.fileType == SimpleDownloadInfo.DownloadType.APK && !TextUtils.isEmpty(next.packageName) && next.packageName.equals(str)) {
                        arrayList.add(next);
                    }
                }
                return arrayList;
            }
        }
        return null;
    }

    public DownloadInfo f(String str) {
        if (!TextUtils.isEmpty(str)) {
            ArrayList<DownloadInfo> a2 = a(SimpleDownloadInfo.DownloadType.APK);
            if (a2.isEmpty()) {
                a2 = this.e.a();
            }
            Iterator<DownloadInfo> it = a2.iterator();
            while (it.hasNext()) {
                DownloadInfo next = it.next();
                if (next != null && next.fileType == SimpleDownloadInfo.DownloadType.APK && !TextUtils.isEmpty(next.packageName) && next.packageName.equals(str)) {
                    return next;
                }
            }
        }
        return null;
    }

    public void d(DownloadInfo downloadInfo) {
        if (downloadInfo != null && !TextUtils.isEmpty(downloadInfo.downloadTicket)) {
            this.f.a(downloadInfo.downloadTicket, downloadInfo);
            this.e.a(downloadInfo);
        }
    }

    public ArrayList<DownloadInfo> c() {
        DownloadInfo b2;
        ArrayList<DownloadInfo> arrayList = new ArrayList<>();
        for (String next : this.f.a()) {
            if (!TextUtils.isEmpty(next) && (b2 = this.f.b(next)) != null) {
                arrayList.add(b2);
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>
     arg types: [com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, int]
     candidates:
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, android.app.Dialog):android.app.Dialog
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, com.tencent.assistant.net.APN):com.tencent.assistant.net.APN
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager):com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>):java.lang.String
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.DownloadInfo, com.tencent.pangu.download.SimpleDownloadInfo$DownloadState):void
      com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, int):com.tencent.pangu.download.DownloadInfo
      com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, boolean):void
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.pangu.download.DownloadInfo> */
    public ArrayList<DownloadInfo> a(SimpleDownloadInfo.DownloadType downloadType) {
        return a(downloadType, false);
    }

    public ArrayList<DownloadInfo> a(SimpleDownloadInfo.DownloadType downloadType, boolean z) {
        DownloadInfo b2;
        ArrayList<DownloadInfo> arrayList = new ArrayList<>();
        if (downloadType != SimpleDownloadInfo.DownloadType.APK || z) {
            for (String next : this.f.a()) {
                if (!TextUtils.isEmpty(next) && (b2 = this.f.b(next)) != null && b2.fileType == downloadType) {
                    if (!z || !b2.isUiTypeWiseDownload()) {
                        arrayList.add(this.f.b(next));
                    }
                }
            }
            return arrayList;
        }
        arrayList.addAll(this.f.c());
        return arrayList;
    }

    public ArrayList<DownloadInfo> d() {
        DownloadInfo b2;
        ArrayList<DownloadInfo> arrayList = new ArrayList<>();
        for (String next : this.f.a()) {
            if (!TextUtils.isEmpty(next) && (b2 = this.f.b(next)) != null && b2.isUiTypeWiseDownload()) {
                arrayList.add(b2);
            }
        }
        return arrayList;
    }

    private void o() {
        TemporaryThreadManager.get().start(new m(this));
        this.n.e();
        this.o.e();
    }

    public boolean e() {
        TemporaryThreadManager.get().start(new n(this));
        this.n.d();
        this.o.d();
        return true;
    }

    /* access modifiers changed from: private */
    public void a(DownloadInfo downloadInfo, SimpleDownloadInfo.DownloadState downloadState) {
        if (downloadInfo == null) {
            return;
        }
        if (downloadInfo.fileType == SimpleDownloadInfo.DownloadType.APK) {
            if (downloadState == SimpleDownloadInfo.DownloadState.PAUSED || downloadState == SimpleDownloadInfo.DownloadState.WAITTING_FOR_WIFI) {
                this.d.sendMessage(this.d.obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, downloadInfo.downloadTicket));
            } else if (downloadState == SimpleDownloadInfo.DownloadState.FAIL) {
                this.d.sendMessage(this.d.obtainMessage(1007, downloadInfo.downloadTicket));
            } else if (downloadState == SimpleDownloadInfo.DownloadState.COMPLETE) {
                this.d.sendMessage(this.d.obtainMessage(1010, downloadInfo.downloadTicket));
            } else if (downloadState == SimpleDownloadInfo.DownloadState.DOWNLOADING) {
                this.d.sendMessage(this.d.obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, downloadInfo.downloadTicket));
            } else if (downloadState == SimpleDownloadInfo.DownloadState.QUEUING) {
                this.d.sendMessage(this.d.obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, downloadInfo.downloadTicket));
            } else if (downloadState == SimpleDownloadInfo.DownloadState.SUCC) {
                this.d.sendMessage(this.d.obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, downloadInfo.downloadTicket));
            }
        } else if (downloadInfo.fileType != SimpleDownloadInfo.DownloadType.PLUGIN) {
        } else {
            if (downloadState == SimpleDownloadInfo.DownloadState.PAUSED) {
                this.d.sendMessage(this.d.obtainMessage(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_PAUSE, downloadInfo.downloadTicket));
            } else if (downloadState == SimpleDownloadInfo.DownloadState.FAIL) {
                this.d.sendMessage(this.d.obtainMessage(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_FAIL, downloadInfo.downloadTicket));
            } else if (downloadState == SimpleDownloadInfo.DownloadState.COMPLETE) {
                this.d.sendMessage(this.d.obtainMessage(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_SUCC, downloadInfo.downloadTicket));
            } else if (downloadState == SimpleDownloadInfo.DownloadState.DOWNLOADING) {
                this.d.sendMessage(this.d.obtainMessage(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_DOWNLOADING, downloadInfo.downloadTicket));
            } else if (downloadState == SimpleDownloadInfo.DownloadState.QUEUING) {
                this.d.sendMessage(this.d.obtainMessage(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_QUEUING, downloadInfo.downloadTicket));
            }
        }
    }

    /* access modifiers changed from: private */
    public String a(LocalApkInfo localApkInfo, ArrayList<DownloadInfo> arrayList) {
        Iterator<DownloadInfo> it = arrayList.iterator();
        while (it.hasNext()) {
            DownloadInfo next = it.next();
            if (next.packageName.equals(localApkInfo.mPackageName) && next.versionCode == localApkInfo.mVersionCode) {
                String str = next.downloadTicket;
                if (next.downloadState == SimpleDownloadInfo.DownloadState.INSTALLING || next.downloadState == SimpleDownloadInfo.DownloadState.SUCC) {
                    next.downloadState = SimpleDownloadInfo.DownloadState.INSTALLED;
                }
                a().d(next);
                return str;
            }
        }
        return Constants.STR_EMPTY;
    }

    /* access modifiers changed from: private */
    public String b(LocalApkInfo localApkInfo, ArrayList<DownloadInfo> arrayList) {
        boolean z;
        String str;
        LocalApkInfo localApkInfo2;
        File file;
        Iterator<DownloadInfo> it = arrayList.iterator();
        while (true) {
            if (!it.hasNext()) {
                z = false;
                str = Constants.STR_EMPTY;
                break;
            }
            DownloadInfo next = it.next();
            if (next.packageName.equals(localApkInfo.mPackageName) && next.versionCode == localApkInfo.mVersionCode && next.grayVersionCode == localApkInfo.mGrayVersionCode) {
                str = next.downloadTicket;
                h(next.name);
                DownloadManager.a().d(next.getDownloadSubType(), str);
                z = true;
                break;
            }
        }
        if (localApkInfo != null && !z && (localApkInfo2 = ApkResourceManager.getInstance().getLocalApkInfo(localApkInfo.mPackageName, localApkInfo.mVersionCode, localApkInfo.mGrayVersionCode)) != null && !TextUtils.isEmpty(localApkInfo2.mLocalFilePath) && (file = new File(localApkInfo2.mLocalFilePath)) != null && file.exists() && file.delete()) {
            h(localApkInfo2.mAppName);
        }
        return str;
    }

    private void h(String str) {
        if (!TextUtils.isEmpty(str)) {
            ah.a().post(new s(this, str));
        }
    }

    private void j(DownloadInfo downloadInfo) {
        String str = downloadInfo.name;
        if (!TextUtils.isEmpty(str)) {
            ah.a().post(new t(this, str));
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START:
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC:
            case 1027:
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START:
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC:
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL:
                InstallUninstallTaskBean installUninstallTaskBean = (InstallUninstallTaskBean) message.obj;
                int i2 = message.what;
                if (installUninstallTaskBean != null && !TextUtils.isEmpty(installUninstallTaskBean.packageName)) {
                    DownloadInfo d2 = a().d(installUninstallTaskBean.downloadTicket);
                    if (d2 == null) {
                        switch (i2) {
                            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START:
                                k.f997a.add(installUninstallTaskBean.downloadTicket);
                                if (installUninstallTaskBean.promptForInstallUninstall) {
                                    this.i.add(installUninstallTaskBean.downloadTicket);
                                    return;
                                }
                                return;
                            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC:
                                k.f997a.remove(installUninstallTaskBean.downloadTicket);
                                if (installUninstallTaskBean.promptForInstallUninstall) {
                                    this.i.remove(installUninstallTaskBean.downloadTicket);
                                    return;
                                }
                                return;
                            case 1027:
                                k.f997a.remove(installUninstallTaskBean.downloadTicket);
                                if (installUninstallTaskBean.promptForInstallUninstall) {
                                    this.i.remove(installUninstallTaskBean.downloadTicket);
                                    return;
                                }
                                return;
                            default:
                                return;
                        }
                    } else if (d2.packageName != null && d2.packageName.equals(installUninstallTaskBean.packageName) && d2.versionCode == installUninstallTaskBean.versionCode) {
                        switch (i2) {
                            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START:
                                d2.downloadState = SimpleDownloadInfo.DownloadState.INSTALLING;
                                if (installUninstallTaskBean.promptForInstallUninstall) {
                                    this.i.add(installUninstallTaskBean.downloadTicket);
                                    return;
                                }
                                return;
                            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC:
                                d2.downloadState = SimpleDownloadInfo.DownloadState.INSTALLED;
                                if (installUninstallTaskBean.promptForInstallUninstall) {
                                    this.i.remove(installUninstallTaskBean.downloadTicket);
                                    return;
                                }
                                return;
                            case 1027:
                                d2.downloadState = SimpleDownloadInfo.DownloadState.SUCC;
                                if (installUninstallTaskBean.promptForInstallUninstall) {
                                    this.i.remove(installUninstallTaskBean.downloadTicket);
                                    return;
                                }
                                return;
                            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START:
                            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC:
                            default:
                                return;
                            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL:
                                d2.downloadState = SimpleDownloadInfo.DownloadState.ILLEGAL;
                                return;
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case EventDispatcherEnum.UI_EVENT_APP_ATFRONT:
            case 1032:
            default:
                return;
            case EventDispatcherEnum.UI_EVENT_APP_GOFRONT:
                this.l = true;
                if (com.tencent.assistant.net.c.e() && !com.tencent.assistant.net.c.l()) {
                    e();
                    return;
                }
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.notification.v.a(boolean, int):void
     arg types: [int, int]
     candidates:
      com.tencent.assistant.manager.notification.v.a(int, long):void
      com.tencent.assistant.manager.notification.v.a(int, android.app.Notification):void
      com.tencent.assistant.manager.notification.v.a(int, java.lang.String):void
      com.tencent.assistant.manager.notification.v.a(com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, boolean):void
      com.tencent.assistant.manager.notification.v.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.manager.notification.v.a(java.lang.String, boolean):void
      com.tencent.assistant.manager.notification.v.a(boolean, int):void */
    public void onConnected(APN apn) {
        if (this.m == APN.WIFI && apn != APN.WIFI) {
            v.a().a(true, f());
            if (AstApp.i().l() && a().f() > 0) {
                p();
            }
        } else if (apn == APN.WIFI && !com.tencent.assistant.net.c.l() && this.l) {
            v.a().a(false, f());
            if (this.h != null && this.h.isShowing()) {
                this.h.cancel();
                this.h = null;
            }
            a().e();
            v.a().a(true);
        } else if (apn == APN.WIFI && com.tencent.assistant.net.c.l()) {
            this.m = APN.NO_NETWORK;
            v.a().a(false, f());
        }
    }

    public void onDisconnected(APN apn) {
        if (!com.tencent.assistant.net.c.l()) {
            this.m = apn;
            if (this.m == APN.WIFI) {
                o();
                new Handler().postDelayed(new h(this), 30000);
            }
            v.a().b(true, a().f() + a().h());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.notification.v.a(boolean, int):void
     arg types: [int, int]
     candidates:
      com.tencent.assistant.manager.notification.v.a(int, long):void
      com.tencent.assistant.manager.notification.v.a(int, android.app.Notification):void
      com.tencent.assistant.manager.notification.v.a(int, java.lang.String):void
      com.tencent.assistant.manager.notification.v.a(com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, boolean):void
      com.tencent.assistant.manager.notification.v.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.manager.notification.v.a(java.lang.String, boolean):void
      com.tencent.assistant.manager.notification.v.a(boolean, int):void */
    public void onConnectivityChanged(APN apn, APN apn2) {
        if (apn != APN.WIFI && apn2 == APN.WIFI && !com.tencent.assistant.net.c.l()) {
            v.a().a(false, f());
            if (this.h != null && this.h.isShowing()) {
                try {
                    this.h.cancel();
                } catch (Exception e2) {
                }
                this.h = null;
            }
            a().e();
            v.a().a(true);
        } else if (apn != APN.WIFI && apn2 == APN.WIFI && com.tencent.assistant.net.c.l()) {
            v.a().a(false, f());
        } else if (apn == APN.WIFI && apn2 != APN.WIFI) {
            int h2 = a().h();
            o();
            v.a().a(true, h2);
            if (AstApp.i().l() && h2 > 0) {
                p();
            }
        }
    }

    public int f() {
        int i2 = 0;
        for (String next : this.f.a()) {
            if (!TextUtils.isEmpty(next)) {
                DownloadInfo b2 = this.f.b(next);
                if (b2 != null && b2.fileType == SimpleDownloadInfo.DownloadType.APK && b2.downloadState == SimpleDownloadInfo.DownloadState.FAIL && !b2.isUiTypeWiseDownload()) {
                    i2++;
                }
                i2 = i2;
            }
        }
        return r.c().h() + i2;
    }

    public ArrayList<DownloadInfo> a(boolean z) {
        DownloadInfo b2;
        ArrayList<DownloadInfo> arrayList = new ArrayList<>();
        for (String next : this.f.a()) {
            if (!TextUtils.isEmpty(next) && (b2 = this.f.b(next)) != null && b2.fileType == SimpleDownloadInfo.DownloadType.APK) {
                if (b2.downloadState == SimpleDownloadInfo.DownloadState.QUEUING || b2.downloadState == SimpleDownloadInfo.DownloadState.DOWNLOADING) {
                    if (!z) {
                        arrayList.add(b2);
                    } else if (!b2.isUiTypeWiseDownload()) {
                        arrayList.add(b2);
                    }
                }
            }
        }
        return arrayList;
    }

    public int g() {
        return a(true).size();
    }

    public int h() {
        return g() + this.n.g() + this.o.g();
    }

    private void p() {
        i iVar = new i(this);
        iVar.titleRes = AstApp.i().getString(R.string.dialog_continue_download_title);
        iVar.contentRes = AstApp.i().getBaseContext().getString(R.string.dialog_continue_download);
        iVar.rBtnTxtRes = AstApp.i().getString(R.string.dialog_continue_download_rbtnres);
        this.h = DialogUtils.get2BtnDialog(iVar);
        if (this.h != null && !this.h.getOwnerActivity().isFinishing()) {
            this.h.show();
        }
    }

    public void i() {
        Iterator<DownloadInfo> it = c().iterator();
        while (it.hasNext()) {
            a(it.next().downloadTicket);
        }
        this.n.f();
        this.o.f();
    }

    public boolean j() {
        return this.j;
    }

    public long k() {
        long j2 = 0;
        if (this.f == null) {
            return 0;
        }
        Iterator<Map.Entry<String, DownloadInfo>> it = this.f.b().iterator();
        while (true) {
            long j3 = j2;
            if (!it.hasNext()) {
                return j3;
            }
            j2 = j3 + ((DownloadInfo) it.next().getValue()).response.b;
        }
    }

    public long l() {
        long j2 = 0;
        if (this.f == null) {
            return 0;
        }
        Iterator<Map.Entry<String, DownloadInfo>> it = this.f.b().iterator();
        while (true) {
            long j3 = j2;
            if (!it.hasNext()) {
                return j3;
            }
            DownloadInfo downloadInfo = (DownloadInfo) it.next().getValue();
            if (downloadInfo != null) {
                if (downloadInfo.response == null) {
                    downloadInfo.response = new m();
                }
                j2 = j3 + downloadInfo.response.f3766a;
            } else {
                j2 = j3;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.DownloadProxy.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.pangu.manager.DownloadProxy.b(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>):java.lang.String
      com.tencent.pangu.manager.DownloadProxy.b(java.lang.String, boolean):void */
    private void k(DownloadInfo downloadInfo) {
        List<DownloadInfo> e2 = e(downloadInfo.packageName);
        if (e2 != null) {
            for (DownloadInfo next : e2) {
                if (next.versionCode < downloadInfo.versionCode) {
                    a().b(next.downloadTicket, true);
                }
            }
        }
    }

    public void a(List<DownloadInfo> list) {
        if (list != null && list.size() > 0) {
            ArrayList arrayList = new ArrayList();
            for (DownloadInfo vVar : list) {
                arrayList.add(new v(this, vVar, SimpleDownloadInfo.DownloadState.PAUSED));
            }
            if (this.b != null) {
                this.b.a(arrayList);
            }
        }
    }

    public void b(List<DownloadInfo> list) {
        if (list != null && list.size() > 0) {
            ArrayList arrayList = new ArrayList();
            for (DownloadInfo vVar : list) {
                arrayList.add(new v(this, vVar, SimpleDownloadInfo.DownloadState.DOWNLOADING));
            }
            if (this.b != null) {
                this.b.a(arrayList);
            }
        }
    }

    public void m() {
        if (this.f != null) {
            DownloadManager.a().a(new int[]{2, 3, 1});
            ArrayList arrayList = new ArrayList();
            for (Map.Entry<String, DownloadInfo> value : this.f.b()) {
                arrayList.add(new v(this, (DownloadInfo) value.getValue(), SimpleDownloadInfo.DownloadState.PAUSED));
            }
            if (this.b != null) {
                this.b.a(arrayList);
            }
        }
    }

    public void n() {
        if (this.f != null) {
            ArrayList arrayList = new ArrayList();
            for (Map.Entry<String, DownloadInfo> value : this.f.b()) {
                arrayList.add(new v(this, (DownloadInfo) value.getValue(), SimpleDownloadInfo.DownloadState.DOWNLOADING));
            }
            if (this.b != null) {
                this.b.a(arrayList);
            }
        }
    }

    /* compiled from: ProGuard */
    class DownloadTaskQueue extends LinkedBlockingQueue<v> implements Runnable {
        private Object b;

        private DownloadTaskQueue() {
            this.b = new Object();
        }

        /* synthetic */ DownloadTaskQueue(DownloadProxy downloadProxy, g gVar) {
            this();
        }

        public void run() {
            while (true) {
                synchronized (this.b) {
                    while (size() == 0) {
                        try {
                            this.b.wait();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    try {
                        v vVar = (v) take();
                        if (vVar != null) {
                            if (vVar.b == SimpleDownloadInfo.DownloadState.PAUSED) {
                                if (vVar.f3845a != null) {
                                    DownloadProxy.this.a(vVar.f3845a.downloadTicket);
                                }
                            } else if (vVar.b == SimpleDownloadInfo.DownloadState.DOWNLOADING) {
                                a.a().a(vVar.f3845a);
                            }
                        }
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }

        public void a(List<v> list) {
            if (list != null && list.size() > 0) {
                synchronized (this.b) {
                    Iterator it = iterator();
                    ArrayList<v> arrayList = new ArrayList<>();
                    while (it.hasNext()) {
                        v vVar = (v) it.next();
                        if (vVar != null) {
                            for (v next : list) {
                                if (vVar.a(next)) {
                                    if (vVar.b == next.b) {
                                        arrayList.add(next);
                                    } else {
                                        it.remove();
                                    }
                                }
                            }
                            continue;
                        }
                    }
                    for (v vVar2 : arrayList) {
                        if (list.contains(vVar2)) {
                            list.remove(vVar2);
                        }
                    }
                    try {
                        for (v next2 : list) {
                            if (!(next2 == null || next2.f3845a == null)) {
                                put(next2);
                            }
                        }
                        this.b.notify();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public boolean a(int i2) {
        return i2 == -10;
    }

    public boolean b(int i2) {
        return i2 == -15;
    }

    public boolean c(int i2) {
        return i2 == -12;
    }

    public boolean d(int i2) {
        return i2 == -11;
    }
}
