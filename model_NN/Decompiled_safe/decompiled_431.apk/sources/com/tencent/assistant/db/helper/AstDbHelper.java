package com.tencent.assistant.db.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.tencent.assistant.db.table.SwitchPhoneTable;
import com.tencent.assistant.db.table.a;
import com.tencent.assistant.db.table.ab;
import com.tencent.assistant.db.table.ac;
import com.tencent.assistant.db.table.ad;
import com.tencent.assistant.db.table.ae;
import com.tencent.assistant.db.table.af;
import com.tencent.assistant.db.table.ag;
import com.tencent.assistant.db.table.b;
import com.tencent.assistant.db.table.c;
import com.tencent.assistant.db.table.d;
import com.tencent.assistant.db.table.f;
import com.tencent.assistant.db.table.j;
import com.tencent.assistant.db.table.k;
import com.tencent.assistant.db.table.p;
import com.tencent.assistant.db.table.r;
import com.tencent.assistant.db.table.s;
import com.tencent.assistant.db.table.u;
import com.tencent.assistant.db.table.x;

/* compiled from: ProGuard */
public class AstDbHelper extends SqliteHelper {
    private static final String DB_NAME = "mobile_ast.db";
    private static final int DB_VERSION = 16;
    private static final Class<?>[] TABLESS = {b.class, d.class, c.class, p.class, x.class, a.class, u.class, af.class, s.class, ac.class, ae.class, ad.class, ab.class, j.class, r.class, k.class, SwitchPhoneTable.class, f.class, com.tencent.game.a.a.a.class, ag.class};
    private static volatile SqliteHelper instance;

    public static synchronized SqliteHelper get(Context context) {
        SqliteHelper sqliteHelper;
        synchronized (AstDbHelper.class) {
            if (instance == null) {
                instance = new AstDbHelper(context, DB_NAME, null, 16);
            }
            sqliteHelper = instance;
        }
        return sqliteHelper;
    }

    public AstDbHelper(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, DB_NAME, null, i);
    }

    public Class<?>[] getTables() {
        return TABLESS;
    }

    public int getDBVersion() {
        return 16;
    }
}
