package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.AstDbHelper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.model.k;
import com.tencent.assistant.utils.XLog;
import com.tencent.open.SocialConstants;
import java.util.List;

/* compiled from: ProGuard */
public class af implements IBaseTable {
    private k a(Cursor cursor) {
        k kVar = new k();
        kVar.a(cursor.getInt(cursor.getColumnIndex("splash_id")));
        kVar.a(cursor.getString(cursor.getColumnIndex("title")));
        kVar.b(cursor.getString(cursor.getColumnIndex(SocialConstants.PARAM_APP_DESC)));
        kVar.a(cursor.getLong(cursor.getColumnIndex("beginTime")));
        kVar.b(cursor.getLong(cursor.getColumnIndex("endTime")));
        kVar.b(cursor.getInt(cursor.getColumnIndex("runTime")));
        kVar.c(cursor.getInt(cursor.getColumnIndex("runTimes")));
        kVar.d(cursor.getInt(cursor.getColumnIndex("hasRunTimes")));
        kVar.c(cursor.getString(cursor.getColumnIndex("imageUrl")));
        kVar.e(cursor.getString(cursor.getColumnIndex("imageDataDesc")));
        kVar.e(cursor.getInt(cursor.getColumnIndex("status")));
        kVar.j(cursor.getInt(cursor.getColumnIndex("splashType")));
        kVar.f(cursor.getString(cursor.getColumnIndex("btnImage")));
        kVar.g(cursor.getString(cursor.getColumnIndex("target")));
        kVar.i(cursor.getInt(cursor.getColumnIndex("btnWidth")));
        kVar.a(cursor.getFloat(cursor.getColumnIndex("splashBitmapDensity")));
        kVar.h(cursor.getInt(cursor.getColumnIndex("btnHeight")));
        kVar.f(cursor.getInt(cursor.getColumnIndex("btnMarginLeft")));
        kVar.g(cursor.getInt(cursor.getColumnIndex("btnMarginBottom")));
        kVar.k(cursor.getInt(cursor.getColumnIndex("targetType")));
        kVar.d(cursor.getString(cursor.getColumnIndex("imagePath")));
        kVar.h(cursor.getString(cursor.getColumnIndex("btnPath")));
        return kVar;
    }

    public synchronized int a(List<k> list) {
        int i;
        int i2;
        i = 0;
        for (k next : list) {
            if (a(next)) {
                i2 = i + 1;
            } else {
                XLog.e("splashInfo", "error insert, splashInfo = " + next);
                i2 = i;
            }
            i = i2;
        }
        return i;
    }

    private void a(ContentValues contentValues, k kVar) {
        contentValues.put("title", kVar.b());
        contentValues.put(SocialConstants.PARAM_APP_DESC, kVar.c());
        contentValues.put("beginTime", Long.valueOf(kVar.d()));
        contentValues.put("endTime", Long.valueOf(kVar.e()));
        contentValues.put("imageUrl", kVar.i());
        contentValues.put("imageDataDesc", kVar.k());
        contentValues.put("runTime", Integer.valueOf(kVar.f()));
        contentValues.put("runTimes", Integer.valueOf(kVar.g()));
        contentValues.put("hasRunTimes", Integer.valueOf(kVar.h()));
        contentValues.put("status", Integer.valueOf(kVar.l()));
        contentValues.put("splashType", Integer.valueOf(kVar.r()));
        contentValues.put("btnImage", kVar.m());
        contentValues.put("target", kVar.t());
        contentValues.put("btnWidth", Integer.valueOf(kVar.q()));
        contentValues.put("splashBitmapDensity", Float.valueOf(kVar.u()));
        contentValues.put("btnHeight", Integer.valueOf(kVar.p()));
        contentValues.put("btnMarginLeft", Integer.valueOf(kVar.n()));
        contentValues.put("btnMarginBottom", Integer.valueOf(kVar.o()));
        contentValues.put("targetType", Integer.valueOf(kVar.s()));
        contentValues.put("imagePath", kVar.j());
        contentValues.put("btnPath", kVar.v());
    }

    public synchronized boolean a(k kVar) {
        ContentValues contentValues;
        contentValues = new ContentValues();
        contentValues.put("splash_id", Integer.valueOf(kVar.a()));
        a(contentValues, kVar);
        return getHelper().getWritableDatabaseWrapper().insert("splash_infos", null, contentValues) > 0;
    }

    public synchronized boolean b(k kVar) {
        boolean z = true;
        synchronized (this) {
            ContentValues contentValues = new ContentValues();
            a(contentValues, kVar);
            if (getHelper().getWritableDatabaseWrapper().update("splash_infos", contentValues, "splash_id=?", new String[]{String.valueOf(kVar.a())}) <= 0) {
                z = false;
            }
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003d, code lost:
        if (r1 != null) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003f, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0042, code lost:
        return r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0056, code lost:
        if (r1 == null) goto L_0x0042;
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x005c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.tencent.assistant.model.k> a(boolean r11) {
        /*
            r10 = this;
            r8 = 0
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            com.tencent.assistant.db.helper.SqliteHelper r0 = r10.getHelper()
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()
            if (r11 == 0) goto L_0x0043
            java.lang.String r1 = "splash_infos"
            r2 = 0
            java.lang.String r3 = "status = ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0051, all -> 0x0059 }
            r5 = 0
            r6 = 1
            java.lang.String r6 = java.lang.Integer.toString(r6)     // Catch:{ Exception -> 0x0051, all -> 0x0059 }
            r4[r5] = r6     // Catch:{ Exception -> 0x0051, all -> 0x0059 }
            r5 = 0
            r6 = 0
            java.lang.String r7 = "beginTime desc,endTime desc,splash_id desc"
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0051, all -> 0x0059 }
        L_0x0028:
            if (r1 == 0) goto L_0x003d
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x0063 }
            if (r0 == 0) goto L_0x003d
        L_0x0030:
            com.tencent.assistant.model.k r0 = r10.a(r1)     // Catch:{ Exception -> 0x0063 }
            r9.add(r0)     // Catch:{ Exception -> 0x0063 }
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x0063 }
            if (r0 != 0) goto L_0x0030
        L_0x003d:
            if (r1 == 0) goto L_0x0042
        L_0x003f:
            r1.close()
        L_0x0042:
            return r9
        L_0x0043:
            java.lang.String r1 = "splash_infos"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            java.lang.String r7 = "beginTime desc,endTime desc,splash_id desc"
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0051, all -> 0x0059 }
            goto L_0x0028
        L_0x0051:
            r0 = move-exception
            r1 = r8
        L_0x0053:
            r0.printStackTrace()     // Catch:{ all -> 0x0060 }
            if (r1 == 0) goto L_0x0042
            goto L_0x003f
        L_0x0059:
            r0 = move-exception
        L_0x005a:
            if (r8 == 0) goto L_0x005f
            r8.close()
        L_0x005f:
            throw r0
        L_0x0060:
            r0 = move-exception
            r8 = r1
            goto L_0x005a
        L_0x0063:
            r0 = move-exception
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.af.a(boolean):java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.tencent.assistant.model.k a(java.lang.String r10) {
        /*
            r9 = this;
            r8 = 0
            com.tencent.assistant.db.helper.SqliteHelper r0 = r9.getHelper()
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()
            java.lang.String r1 = "splash_infos"
            r2 = 0
            java.lang.String r3 = "imageUrl = ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x002e, all -> 0x003a }
            r5 = 0
            r4[r5] = r10     // Catch:{ Exception -> 0x002e, all -> 0x003a }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x002e, all -> 0x003a }
            if (r1 == 0) goto L_0x0046
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x0044 }
            if (r0 == 0) goto L_0x0046
            com.tencent.assistant.model.k r8 = r9.a(r1)     // Catch:{ Exception -> 0x0044 }
            r0 = r8
        L_0x0028:
            if (r1 == 0) goto L_0x002d
            r1.close()
        L_0x002d:
            return r0
        L_0x002e:
            r0 = move-exception
            r1 = r8
        L_0x0030:
            r0.printStackTrace()     // Catch:{ all -> 0x0041 }
            if (r1 == 0) goto L_0x0038
            r1.close()
        L_0x0038:
            r0 = r8
            goto L_0x002d
        L_0x003a:
            r0 = move-exception
        L_0x003b:
            if (r8 == 0) goto L_0x0040
            r8.close()
        L_0x0040:
            throw r0
        L_0x0041:
            r0 = move-exception
            r8 = r1
            goto L_0x003b
        L_0x0044:
            r0 = move-exception
            goto L_0x0030
        L_0x0046:
            r0 = r8
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.af.a(java.lang.String):com.tencent.assistant.model.k");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.tencent.assistant.model.k b(java.lang.String r10) {
        /*
            r9 = this;
            r8 = 0
            com.tencent.assistant.db.helper.SqliteHelper r0 = r9.getHelper()
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()
            java.lang.String r1 = "splash_infos"
            r2 = 0
            java.lang.String r3 = "btnImage = ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x002e, all -> 0x003a }
            r5 = 0
            r4[r5] = r10     // Catch:{ Exception -> 0x002e, all -> 0x003a }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x002e, all -> 0x003a }
            if (r1 == 0) goto L_0x0046
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x0044 }
            if (r0 == 0) goto L_0x0046
            com.tencent.assistant.model.k r8 = r9.a(r1)     // Catch:{ Exception -> 0x0044 }
            r0 = r8
        L_0x0028:
            if (r1 == 0) goto L_0x002d
            r1.close()
        L_0x002d:
            return r0
        L_0x002e:
            r0 = move-exception
            r1 = r8
        L_0x0030:
            r0.printStackTrace()     // Catch:{ all -> 0x0041 }
            if (r1 == 0) goto L_0x0038
            r1.close()
        L_0x0038:
            r0 = r8
            goto L_0x002d
        L_0x003a:
            r0 = move-exception
        L_0x003b:
            if (r8 == 0) goto L_0x0040
            r8.close()
        L_0x0040:
            throw r0
        L_0x0041:
            r0 = move-exception
            r8 = r1
            goto L_0x003b
        L_0x0044:
            r0 = move-exception
            goto L_0x0030
        L_0x0046:
            r0 = r8
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.af.b(java.lang.String):com.tencent.assistant.model.k");
    }

    public int a() {
        return getHelper().getWritableDatabaseWrapper().delete("splash_infos", null, null);
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "splash_infos";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists splash_infos (row_id INTEGER PRIMARY KEY AUTOINCREMENT,splash_id INTEGER,title TEXT,imagePath TEXT, btnPath TEXT, btnImage TEXT,target TEXT,btnWidth INTEGER,splashType INTEGER,btnHeight INTEGER,targetType INTEGER,btnMarginLeft INTEGER,btnMarginBottom INTEGER,splashBitmapDensity FLOAT,desc TEXT,beginTime INTEGER,endTime INTEGER,runTime INTEGER,runTimes INTEGER,hasRunTimes INTEGER,imageUrl TEXT,imageDataDesc TEXT,status INTEGER);";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i2 == 4) {
            return new String[]{"CREATE TABLE if not exists splash_infos (row_id INTEGER PRIMARY KEY AUTOINCREMENT,splash_id INTEGER,title TEXT,imagePath TEXT, btnPath TEXT, btnImage TEXT,target TEXT,btnWidth INTEGER,splashType INTEGER,btnHeight INTEGER,targetType INTEGER,btnMarginLeft INTEGER,btnMarginBottom INTEGER,splashBitmapDensity FLOAT,desc TEXT,beginTime INTEGER,endTime INTEGER,runTime INTEGER,runTimes INTEGER,hasRunTimes INTEGER,imageUrl TEXT,imageDataDesc TEXT,status INTEGER);"};
        } else if (i != 15 || i2 != 16) {
            return null;
        } else {
            return new String[]{"alter table splash_infos add column btnImage TEXT;", "alter table splash_infos add column target TEXT;", "alter table splash_infos add column btnWidth INTEGER;", "alter table splash_infos add column splashType INTEGER;", "alter table splash_infos add column btnHeight INTEGER;", "alter table splash_infos add column targetType INTEGER;", "alter table splash_infos add column btnMarginLeft INTEGER;", "alter table splash_infos add column btnMarginBottom INTEGER;", "alter table splash_infos add column splashBitmapDensity FLOAT;", "alter table splash_infos add column imagePath TEXT;", "alter table splash_infos add column btnPath TEXT;"};
        }
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return AstDbHelper.get(AstApp.i());
    }
}
