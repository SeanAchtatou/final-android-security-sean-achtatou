package com.tencent.assistant.activity.debug;

import android.view.View;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class ak implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DActivity f484a;

    ak(DActivity dActivity) {
        this.f484a = dActivity;
    }

    public void onClick(View view) {
        TemporaryThreadManager.get().start(new al(this));
    }
}
