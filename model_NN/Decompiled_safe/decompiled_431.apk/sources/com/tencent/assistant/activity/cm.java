package com.tencent.assistant.activity;

import com.tencent.assistant.utils.ar;
import com.tencent.nucleus.manager.spaceclean.SpaceScanManager;
import java.util.ArrayList;

/* compiled from: ProGuard */
class cm implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpaceCleanActivity f451a;

    cm(SpaceCleanActivity spaceCleanActivity) {
        this.f451a = spaceCleanActivity;
    }

    public void run() {
        ArrayList q = this.f451a.B();
        if (SpaceScanManager.a().j()) {
            SpaceScanManager.a().a(this.f451a.Z, q);
        } else {
            SpaceScanManager.a().a(this.f451a.ac, q);
        }
        ar.g();
        ar.b(0, SpaceCleanActivity.class);
    }
}
