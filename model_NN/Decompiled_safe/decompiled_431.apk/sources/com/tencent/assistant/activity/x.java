package com.tencent.assistant.activity;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class x extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppBackupActivity f566a;

    x(AppBackupActivity appBackupActivity) {
        this.f566a = appBackupActivity;
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        this.f566a.runOnUiThread(new y(this));
    }
}
