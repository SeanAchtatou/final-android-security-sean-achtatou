package com.tencent.assistant.activity.pictureprocessor;

import android.support.v4.view.ViewPager;

/* compiled from: ProGuard */
class l implements ViewPager.OnPageChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ShowPictureActivity f558a;

    l(ShowPictureActivity showPictureActivity) {
        this.f558a = showPictureActivity;
    }

    public void onPageSelected(int i) {
        int unused = this.f558a.h = i;
        if (this.f558a.f549a != null) {
            this.f558a.i.setSelect(this.f558a.f549a.size(), i, 0);
        }
        this.f558a.e.b(i);
        this.f558a.a(i);
    }

    public void onPageScrolled(int i, float f, int i2) {
    }

    public void onPageScrollStateChanged(int i) {
    }
}
