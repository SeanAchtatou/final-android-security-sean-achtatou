package com.tencent.assistant.activity;

import com.tencent.assistant.protocol.jce.BackupApp;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.nucleus.manager.appbackup.k;
import java.util.ArrayList;

/* compiled from: ProGuard */
class ai implements k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppBackupActivity f394a;

    private ai(AppBackupActivity appBackupActivity) {
        this.f394a = appBackupActivity;
    }

    /* synthetic */ ai(AppBackupActivity appBackupActivity, t tVar) {
        this(appBackupActivity);
    }

    public void a(ArrayList<BackupApp> arrayList) {
        TemporaryThreadManager.get().start(new aj(this, arrayList));
    }
}
