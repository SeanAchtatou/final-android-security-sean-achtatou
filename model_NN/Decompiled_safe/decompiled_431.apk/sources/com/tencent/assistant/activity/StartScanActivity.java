package com.tencent.assistant.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.ViewStub;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.txscrollview.TXExpandableListView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.e;
import com.tencent.assistant.module.s;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ar;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.manager.main.AssistantTabActivity;
import com.tencent.nucleus.manager.securescan.NoRiskFoundPage;
import com.tencent.nucleus.manager.securescan.ScanningAnimView;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean;
import com.tencent.securemodule.impl.AppInfo;
import com.tencent.securemodule.impl.SecureModuleService;
import com.tencent.securemodule.service.ISecureModuleService;
import com.tencent.securemodule.service.ProductInfo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;

/* compiled from: ProGuard */
public class StartScanActivity extends BaseActivity implements UIEventListener, e {
    private TextView A;
    /* access modifiers changed from: private */
    public RelativeLayout B;
    /* access modifiers changed from: private */
    public di C;
    /* access modifiers changed from: private */
    public NormalErrorRecommendPage D;
    private NoRiskFoundPage E;
    private ViewStub F = null;
    private ViewStub G = null;
    private ViewStub H = null;
    /* access modifiers changed from: private */
    public ScanningAnimView I;
    /* access modifiers changed from: private */
    public List<AppInfo> J;
    private ArrayList<SimpleAppModel> K;
    /* access modifiers changed from: private */
    public List<SimpleAppModel> L;
    private ApkResourceManager M = ApkResourceManager.getInstance();
    private dk N;
    /* access modifiers changed from: private */
    public s O;
    private TXExpandableListView P;
    /* access modifiers changed from: private */
    public com.tencent.nucleus.manager.securescan.e Q;
    private AstApp R;
    /* access modifiers changed from: private */
    public Boolean S = false;
    /* access modifiers changed from: private */
    public Boolean T = false;
    private Boolean U = true;
    private Animation V;
    /* access modifiers changed from: private */
    public int W = -1;
    private int X = 2000;
    /* access modifiers changed from: private */
    public long Y = 0;
    /* access modifiers changed from: private */
    public long Z = 0;
    /* access modifiers changed from: private */
    public Handler aa = new da(this);
    private boolean ab = false;
    /* access modifiers changed from: private */
    public Handler ac = new dc(this);
    private final Object ad = new Object();
    /* access modifiers changed from: private */
    public dl n;
    /* access modifiers changed from: private */
    public int u = -1;
    private int v = -1;
    /* access modifiers changed from: private */
    public ISecureModuleService w;
    /* access modifiers changed from: private */
    public Context x;
    /* access modifiers changed from: private */
    public SecondNavigationTitleViewV5 y;
    private FrameLayout z;

    /* access modifiers changed from: private */
    public void x() {
        try {
            new Timer().schedule(this.n, 30000);
        } catch (Exception e) {
            Log.i("StartScanActivity", e.getMessage());
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            setContentView((int) R.layout.activity_startscan);
            this.x = this;
            this.X = getIntent().getIntExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, 2000);
            z();
            A();
            if (a(this.x)) {
                this.T = true;
                TemporaryThreadManager.get().start(new db(this));
            } else {
                c(30);
            }
            y();
        } catch (Throwable th) {
            this.ab = true;
            t.a().b();
            finish();
        }
    }

    private void y() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", com.tencent.assistant.utils.t.g());
        XLog.d("beacon", "beacon report >> expose_securescan. " + hashMap.toString());
        a.a("expose_securescan", true, -1, -1, hashMap, true);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.ab) {
            if (this.T.booleanValue()) {
                this.I.b();
            }
            if (this.y != null) {
                this.y.l();
            }
            this.R.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START, this);
            this.R.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL, this);
            this.R.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        }
    }

    private void z() {
        this.J = new ArrayList();
        this.K = new ArrayList<>();
        this.L = new ArrayList();
        this.Q = new com.tencent.nucleus.manager.securescan.e(this.x, this.J, this.K, this.L, this.P);
        this.C = new di(this, null);
        this.O = new s();
        this.w = SecureModuleService.getInstance(getApplicationContext());
        this.N = new dk(this, null);
        this.M.registerApkResCallback(this.N);
        this.O.register(this);
        this.R = AstApp.i();
        this.n = new dl(this);
    }

    private void A() {
        this.y = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.y.a(this);
        this.y.b(getString(R.string.secure_scan));
        this.y.d();
        this.y.c(new dd(this));
        this.V = AnimationUtils.loadAnimation(this, R.anim.safescan_fade_out);
        this.V.setAnimationListener(new dj(this, null));
        this.I = (ScanningAnimView) findViewById(R.id.scan_anim_content);
        this.B = (RelativeLayout) findViewById(R.id.scanning_layout);
        this.z = (FrameLayout) findViewById(R.id.header_view);
        this.z.setVisibility(8);
        this.A = (TextView) findViewById(R.id.result_text);
        this.F = (ViewStub) findViewById(R.id.group_list_stub);
        this.H = (ViewStub) findViewById(R.id.nofound_stub);
        this.G = (ViewStub) findViewById(R.id.error_stub);
    }

    public static boolean a(Context context) {
        NetworkInfo activeNetworkInfo;
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null || !activeNetworkInfo.isConnected() || activeNetworkInfo.getState() != NetworkInfo.State.CONNECTED) {
                return false;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void t() {
        String str;
        String channelId = Global.getChannelId();
        int parseInt = Integer.parseInt(Global.getBuildNo());
        try {
            str = AstApp.i().getPackageManager().getPackageInfo(AstApp.i().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            str = null;
        }
        Log.i("StartScanActivity", "channelId:" + channelId + " versionName:" + str + " buildNo:" + parseInt);
        if (this.w.register(new ProductInfo(41, str, parseInt, 0, channelId, null)) == 0) {
            this.w.registerCloudScanListener(this, this.C);
            this.w.setNotificationUIEnable(false);
            this.Y = System.currentTimeMillis();
            this.w.cloudScan();
            Log.i("StartScanActivity", "startScan");
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        Log.i("StartScanActivity", "onPause");
        super.onPause();
        if (!this.ab) {
            this.I.c();
            if (this.y != null) {
                this.y.m();
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();
        Log.i("StartScanActivity", "onDestroy---this = " + this);
        if (!this.ab) {
            if (this.n != null) {
                this.n.cancel();
            }
            if (this.w != null) {
                this.w.unregisterCloudScanListener(this, this.C);
            }
            if (this.O != null) {
                this.O.unregister(this);
            }
            if (this.I != null) {
                this.I.d();
            }
            ApkResourceManager.getInstance().unRegisterApkResCallback(this.N);
            this.R.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START, this);
            this.R.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL, this);
            this.R.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (4 != i) {
            return super.onKeyDown(i, keyEvent);
        }
        B();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void B() {
        if (!this.s) {
            finish();
            return;
        }
        Intent intent = new Intent(this, AssistantTabActivity.class);
        intent.setFlags(67108864);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
        intent.putExtra(com.tencent.assistant.a.a.G, true);
        startActivity(intent);
        this.s = false;
        finish();
        XLog.i("StartScanActivity", "StartScanActivity >> key back finish");
    }

    /* access modifiers changed from: private */
    public void C() {
        this.Q.a(this.J, this.K, this.L);
        D();
        if (this.P == null) {
            this.F.inflate();
            this.P = (TXExpandableListView) findViewById(R.id.group_list);
            this.P.setGroupIndicator(null);
            this.P.setAdapter(this.Q);
            this.P.setDivider(null);
            this.P.setOnGroupClickListener(new de(this));
            this.P.setVisibility(8);
        }
        for (int i = 0; i < this.Q.getGroupCount(); i++) {
            this.P.expandGroup(i);
        }
        this.Q.notifyDataSetChanged();
        this.P.setVisibility(0);
        this.z.setVisibility(0);
    }

    private void D() {
        if (this.J.size() > 0) {
            this.A.setText(Html.fromHtml("<html >扫描出" + this.Q.b() + "个风险应用</html>"));
            this.A.setTag(Integer.valueOf((int) R.id.result_text));
        }
    }

    public void b(int i) {
        if (i >= 0) {
            this.A.setText(Html.fromHtml("<html >扫描出" + i + "个风险应用</html>"));
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE /*1009*/:
                if (message.obj != null && (message.obj instanceof DownloadInfo) && this.U.booleanValue()) {
                    DownloadInfo downloadInfo = (DownloadInfo) message.obj;
                    switch (dh.f528a[downloadInfo.downloadState.ordinal()]) {
                        case 1:
                            break;
                        case 2:
                            if (this.Q != null) {
                                if (!this.Q.j(downloadInfo.packageName).booleanValue() || this.Q.k(downloadInfo.packageName) != 10) {
                                    this.Q.i(downloadInfo.packageName);
                                    this.Q.g(downloadInfo.packageName);
                                    Log.i("StartScanActivity", "delete item from UI_EVENT_APP_DOWNLOAD_DELETE");
                                    break;
                                } else {
                                    return;
                                }
                            }
                            break;
                        default:
                            if (this.Q != null) {
                                if (!this.Q.j(downloadInfo.packageName).booleanValue() || this.Q.k(downloadInfo.packageName) != 10) {
                                    this.Q.g(downloadInfo.packageName);
                                    Log.i("StartScanActivity", "delete item from UI_EVENT_APP_DOWNLOAD_DELETE");
                                    break;
                                } else {
                                    return;
                                }
                            }
                            break;
                    }
                }
                a((Boolean) true);
                return;
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START /*1028*/:
                if (message.obj != null && (message.obj instanceof InstallUninstallTaskBean)) {
                    InstallUninstallTaskBean installUninstallTaskBean = (InstallUninstallTaskBean) message.obj;
                    if (this.Q == null) {
                        return;
                    }
                    if (a(installUninstallTaskBean).booleanValue()) {
                        this.Q.d(installUninstallTaskBean.packageName);
                        return;
                    } else {
                        this.Q.c(installUninstallTaskBean.packageName);
                        return;
                    }
                } else {
                    return;
                }
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC /*1029*/:
                if (message.obj instanceof InstallUninstallTaskBean) {
                    ar.b(((InstallUninstallTaskBean) message.obj).packageName);
                    ar.b(0, StartScanActivity.class);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL /*1030*/:
                if (message.obj != null && (message.obj instanceof InstallUninstallTaskBean)) {
                    InstallUninstallTaskBean installUninstallTaskBean2 = (InstallUninstallTaskBean) message.obj;
                    if (this.Q == null) {
                        return;
                    }
                    if (a(installUninstallTaskBean2).booleanValue()) {
                        this.Q.a(installUninstallTaskBean2.packageName);
                        return;
                    } else {
                        this.Q.b(installUninstallTaskBean2.packageName);
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }

    private Boolean a(InstallUninstallTaskBean installUninstallTaskBean) {
        if (installUninstallTaskBean != null) {
            Iterator<SimpleAppModel> it = this.K.iterator();
            while (it.hasNext()) {
                SimpleAppModel next = it.next();
                if (next != null && next.c.equals(installUninstallTaskBean.packageName)) {
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public Boolean a(LocalApkInfo localApkInfo) {
        if (localApkInfo != null) {
            Iterator<SimpleAppModel> it = this.K.iterator();
            while (it.hasNext()) {
                SimpleAppModel next = it.next();
                if (next != null && next.c.equals(localApkInfo.mPackageName)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void u() {
        try {
            if (this.E == null) {
                this.H.inflate();
                this.E = (NoRiskFoundPage) findViewById(R.id.nofound_page);
            }
            if (this.B != null && this.B.getVisibility() == 0) {
                this.B.setVisibility(8);
            }
            if (this.z != null && this.z.getVisibility() == 0) {
                this.z.setVisibility(8);
            }
            if (this.D != null && this.D.getVisibility() == 0) {
                this.D.setVisibility(8);
            }
            this.E.setVisibility(0);
            this.v = 11;
            w();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void c(int i) {
        if (this.D == null) {
            this.G.inflate();
            this.D = (NormalErrorRecommendPage) findViewById(R.id.error_page);
        }
        if (this.B != null && this.B.getVisibility() == 0) {
            this.B.setVisibility(8);
        }
        if (this.E != null && this.E.getVisibility() == 0) {
            this.E.setVisibility(8);
        }
        if (this.z != null && this.z.getVisibility() == 0) {
            this.z.setVisibility(8);
        }
        this.D.setErrorType(i);
        this.D.setVisibility(0);
        if (i == 30) {
            this.D.setErrorText(this.x.getResources().getString(R.string.secure_scan_error_page_tip));
        }
        this.D.setButtonClickListener(new df(this));
        this.v = 10;
        w();
    }

    /* access modifiers changed from: protected */
    public void v() {
        d(12);
        this.v = 12;
        this.y.i();
        w();
    }

    /* access modifiers changed from: private */
    public void d(int i) {
        this.u = i;
        this.B.startAnimation(this.V);
    }

    public void a(Boolean bool) {
        synchronized (this.ad) {
            this.U = bool;
        }
    }

    public int f() {
        switch (this.v) {
            case 10:
                return STConst.ST_PAGE_ERROR_NETWORK_SAFE_SCAN;
            case 11:
                return STConst.ST_PAGE_EMPTY_SAFE_SCAN;
            case 12:
                return STConst.ST_PAGE_RESULT_SAFE_SCAN;
            default:
                return STConst.ST_PAGE_SAFE_SCAN;
        }
    }

    public void w() {
        STInfoV2 sTInfoV2 = new STInfoV2(f(), STConst.ST_DEFAULT_SLOT, this.X, STConst.ST_DEFAULT_SLOT, 100);
        if (sTInfoV2 != null) {
            l.a(sTInfoV2);
        }
    }

    public void a(int i, int i2, List<AppSimpleDetail> list) {
        Log.i("StartScanActivity", "onGetBatchAppInfoSuccess:batchRequestId=" + this.W + "seq=" + i);
        this.n.cancel();
        if (this.W == i) {
            if (list == null || list.size() <= 0) {
            }
            v();
        }
    }

    public void a(int i, int i2) {
        this.n.cancel();
        Log.i("StartScanActivity", "onGetAppInfoFail:batchRequestId=" + this.W + "seq=" + i);
        if (this.W == i) {
            d(10);
        }
    }

    /* access modifiers changed from: private */
    public void a(long j, List<AppInfo> list) {
        int i;
        HashMap hashMap = new HashMap();
        StringBuilder sb = new StringBuilder();
        if (list != null) {
            int size = list.size();
            for (AppInfo appInfo : list) {
                sb.append(appInfo.softName).append(";");
            }
            i = size;
        } else {
            i = 0;
        }
        hashMap.put("B1", i + Constants.STR_EMPTY);
        hashMap.put("B2", Global.getPhoneGuidAndGen());
        hashMap.put("B3", Global.getQUAForBeacon());
        hashMap.put("B4", com.tencent.assistant.utils.t.g());
        if (list != null) {
            hashMap.put("B5", sb.toString());
        }
        a.a("VirusScan", true, j, -1, hashMap, false);
        XLog.d("beacon", "beacon report >> event: VirusScan, totalTime : " + j + ", params : " + hashMap.toString());
    }
}
