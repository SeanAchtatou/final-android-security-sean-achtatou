package com.tencent.assistant.activity.debug;

import android.view.View;
import android.widget.Toast;
import com.tencent.assistant.plugin.QReaderClient;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class g implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DActivity f506a;

    g(DActivity dActivity) {
        this.f506a = dActivity;
    }

    public void onClick(View view) {
        Toast.makeText(this.f506a, QReaderClient.getInstance().getUserLoginInfo() + Constants.STR_EMPTY, 0).show();
        QReaderClient.getInstance().triggerLogin(null, null);
    }
}
