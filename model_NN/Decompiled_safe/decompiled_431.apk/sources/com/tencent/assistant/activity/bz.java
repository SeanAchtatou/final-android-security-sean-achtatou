package com.tencent.assistant.activity;

import android.os.Handler;
import android.os.Message;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.module.k;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;

/* compiled from: ProGuard */
class bz extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RootUtilInstallActivity f437a;

    bz(RootUtilInstallActivity rootUtilInstallActivity) {
        this.f437a = rootUtilInstallActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.RootUtilInstallActivity.a(com.tencent.assistant.activity.RootUtilInstallActivity, boolean):void
     arg types: [com.tencent.assistant.activity.RootUtilInstallActivity, int]
     candidates:
      com.tencent.assistant.activity.RootUtilInstallActivity.a(com.tencent.assistant.activity.RootUtilInstallActivity, com.tencent.pangu.download.DownloadInfo):java.lang.CharSequence
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.activity.RootUtilInstallActivity.a(com.tencent.assistant.activity.RootUtilInstallActivity, boolean):void */
    public void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case EventDispatcherEnum.PLUGIN_EVENT_LOGIN_START /*10001*/:
                this.f437a.B.a(this.f437a.A);
                this.f437a.B.register(this.f437a);
                return;
            case 10002:
                this.f437a.x.a("打开");
                this.f437a.b(false);
                this.f437a.x.a(this.f437a.A);
                this.f437a.x.setOnClickListener(new ca(this));
                return;
            case 10003:
                XLog.d("miles", "get KingRoot [com.kingroot.RushRoot] from server success...");
                this.f437a.x.setVisibility(0);
                this.f437a.y.setVisibility(8);
                this.f437a.b(false);
                this.f437a.x.a(this.f437a.A);
                StatInfo buildDownloadSTInfo = STInfoBuilder.buildDownloadSTInfo(this.f437a.n, this.f437a.A);
                DownloadInfo a2 = DownloadProxy.a().a(this.f437a.A);
                if (a2 != null && a2.needReCreateInfo(this.f437a.A)) {
                    DownloadProxy.a().b(a2.downloadTicket);
                    a2 = null;
                }
                if (a2 == null) {
                    a2 = DownloadInfo.createDownloadInfo(this.f437a.A, buildDownloadSTInfo, this.f437a.x);
                    this.f437a.x.a(this.f437a.A);
                } else {
                    a2.updateDownloadInfoStatInfo(buildDownloadSTInfo);
                }
                AppConst.AppState d = k.d(this.f437a.A);
                if (d == AppConst.AppState.DOWNLOADED) {
                    this.f437a.x.a(this.f437a.getResources().getString(R.string.appbutton_install));
                } else if (d == AppConst.AppState.DOWNLOAD || d == AppConst.AppState.UPDATE || d == AppConst.AppState.ILLEGAL) {
                    this.f437a.x.a(this.f437a.a(a2));
                    if (d == AppConst.AppState.ILLEGAL) {
                        this.f437a.x.a(AppConst.AppState.DOWNLOAD);
                    }
                }
                this.f437a.x.setOnClickListener(new cb(this));
                return;
            case 10004:
                this.f437a.x.setVisibility(8);
                this.f437a.y.setVisibility(0);
                this.f437a.b(false);
                this.f437a.x.a(this.f437a.getResources().getString(R.string.disconnected));
                XLog.e("miles", "get KingRoot [com.kingroot.RushRoot] from server fail...");
                return;
            default:
                return;
        }
    }
}
