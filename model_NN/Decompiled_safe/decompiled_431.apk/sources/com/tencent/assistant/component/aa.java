package com.tencent.assistant.component;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.ah;

/* compiled from: ProGuard */
class aa extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NormalErrorRecommendPage f622a;

    aa(NormalErrorRecommendPage normalErrorRecommendPage) {
        this.f622a = normalErrorRecommendPage;
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        if (localApkInfo != null) {
            ah.a().post(new ab(this, i, localApkInfo));
        }
    }
}
