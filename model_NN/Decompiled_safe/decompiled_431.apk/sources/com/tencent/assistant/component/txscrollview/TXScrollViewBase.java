package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PointF;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.b;
import com.tencent.assistant.utils.t;

/* compiled from: ProGuard */
public abstract class TXScrollViewBase<T extends View> extends LinearLayout {
    public static final float FRICTION = 2.0f;
    public static final int SMOOTH_SCROLL_DURATION_MS = 200;

    /* renamed from: a  reason: collision with root package name */
    private int f697a = 0;
    /* access modifiers changed from: private */
    public Interpolator b = new DecelerateInterpolator();
    private TXScrollViewBase<T>.SmoothScrollRunnable c = null;
    private View d = null;
    protected boolean m = false;
    protected ScrollState n = ScrollState.ScrollState_Initial;
    protected ScrollMode o = ScrollMode.BOTH;
    protected ScrollDirection p = ScrollDirection.SCROLL_DIRECTION_VERTICAL;
    protected PointF q = new PointF(0.0f, 0.0f);
    protected PointF r = new PointF(0.0f, 0.0f);
    protected T s;
    protected FrameLayout t;

    /* compiled from: ProGuard */
    public interface ISmoothScrollRunnableListener {
        void onSmoothScrollFinished();
    }

    /* compiled from: ProGuard */
    public enum ScrollState {
        ScrollState_Initial,
        ScrollState_FromStart,
        ScrollState_FromEnd
    }

    /* access modifiers changed from: protected */
    public abstract T b(Context context);

    /* access modifiers changed from: protected */
    public abstract boolean e();

    /* access modifiers changed from: protected */
    public abstract boolean f();

    public TXScrollViewBase(Context context, ScrollDirection scrollDirection, ScrollMode scrollMode) {
        super(context);
        this.p = scrollDirection;
        this.o = scrollMode;
        c(context);
    }

    public TXScrollViewBase(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, b.n);
        if (obtainStyledAttributes != null) {
            this.p = ScrollDirection.a(obtainStyledAttributes.getInt(1, 0));
            this.o = ScrollMode.mapIntToValue(obtainStyledAttributes.getInt(0, 3));
            obtainStyledAttributes.recycle();
        }
        c(context);
    }

    public TXScrollViewBase(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, b.n);
        if (obtainStyledAttributes != null) {
            this.p = ScrollDirection.a(obtainStyledAttributes.getInt(1, 0));
            this.o = ScrollMode.mapIntToValue(obtainStyledAttributes.getInt(0, 3));
            obtainStyledAttributes.recycle();
        }
        c(context);
    }

    public TXScrollViewBase(Context context) {
        super(context);
        c(context);
    }

    /* access modifiers changed from: protected */
    public void c(Context context) {
        if (this.p == ScrollDirection.SCROLL_DIRECTION_HORIZONTAL) {
            setOrientation(0);
        } else {
            setOrientation(1);
        }
        setGravity(17);
        this.f697a = ViewConfiguration.get(context).getScaledTouchSlop();
        this.s = b(context);
        a(context, this.s);
    }

    public void setTipsView(View view) {
        if (view != null) {
            removeAllViews();
            super.addView(view, -1, new LinearLayout.LayoutParams(-1, -1));
        }
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        float f;
        float f2;
        int action = motionEvent.getAction();
        if (action == 3 || action == 1) {
            this.m = false;
            return false;
        } else if (this.m && action != 0) {
            return true;
        } else {
            switch (action) {
                case 0:
                    if (p()) {
                        PointF pointF = this.r;
                        PointF pointF2 = this.q;
                        float x = motionEvent.getX();
                        pointF2.x = x;
                        pointF.x = x;
                        PointF pointF3 = this.r;
                        PointF pointF4 = this.q;
                        float y = motionEvent.getY();
                        pointF4.y = y;
                        pointF3.y = y;
                        this.m = false;
                        break;
                    }
                    break;
                case 2:
                    if (p()) {
                        float x2 = motionEvent.getX();
                        float y2 = motionEvent.getY();
                        if (this.p == ScrollDirection.SCROLL_DIRECTION_HORIZONTAL) {
                            f = x2 - this.r.x;
                            f2 = y2 - this.r.y;
                        } else {
                            f = y2 - this.r.y;
                            f2 = x2 - this.r.x;
                        }
                        float abs = Math.abs(f);
                        if (abs > ((float) this.f697a) && abs > Math.abs(f2)) {
                            if (f >= 1.0f && e()) {
                                this.r.x = x2;
                                this.r.y = y2;
                                this.m = true;
                                this.n = ScrollState.ScrollState_FromStart;
                            }
                            if (f <= -1.0f && f()) {
                                this.r.x = x2;
                                this.r.y = y2;
                                this.m = true;
                                this.n = ScrollState.ScrollState_FromEnd;
                                break;
                            }
                        }
                    }
                    break;
            }
            return this.m;
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                if (!p()) {
                    return false;
                }
                PointF pointF = this.r;
                PointF pointF2 = this.q;
                float x = motionEvent.getX();
                pointF2.x = x;
                pointF.x = x;
                PointF pointF3 = this.r;
                PointF pointF4 = this.q;
                float y = motionEvent.getY();
                pointF4.y = y;
                pointF3.y = y;
                return true;
            case 1:
            case 3:
                return j();
            case 2:
                if (!this.m) {
                    return false;
                }
                this.r.x = motionEvent.getX();
                this.r.y = motionEvent.getY();
                if (this.o != ScrollMode.NOSCROLL) {
                    i();
                }
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean j() {
        if (!this.m) {
            return false;
        }
        this.m = false;
        smoothScrollTo(0);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        post(new s(this, i, i2));
    }

    /* access modifiers changed from: protected */
    public void a(int i, int i2) {
        b(i, i2);
    }

    /* access modifiers changed from: protected */
    public int o() {
        if (this.p == ScrollDirection.SCROLL_DIRECTION_HORIZONTAL) {
            return Math.round(((float) getWidth()) / 2.0f);
        }
        return Math.round(((float) getHeight()) / 2.0f);
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        if (view != null && this.s != null && (this.s instanceof ViewGroup)) {
            ((ViewGroup) this.s).addView(view, i, layoutParams);
        }
    }

    /* access modifiers changed from: protected */
    public void a(View view, int i, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i, layoutParams);
    }

    /* access modifiers changed from: protected */
    public final void b(int i, int i2) {
        if (this.t != null) {
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.t.getLayoutParams();
            if (this.p == ScrollDirection.SCROLL_DIRECTION_HORIZONTAL) {
                if (layoutParams.width != i) {
                    layoutParams.width = i;
                    this.t.requestLayout();
                }
            } else if (layoutParams.height != i2) {
                layoutParams.height = i2;
                this.t.requestLayout();
            }
        }
    }

    private void a(Context context, View view) {
        if (view != null) {
            super.addView(view, -1, new LinearLayout.LayoutParams(-1, -1));
        }
    }

    /* access modifiers changed from: protected */
    public boolean p() {
        if (e() || f()) {
            return true;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* access modifiers changed from: protected */
    public int i() {
        float f;
        float f2;
        int round;
        if (this.p == ScrollDirection.SCROLL_DIRECTION_HORIZONTAL) {
            f = this.q.x;
            f2 = this.r.x;
        } else {
            f = this.q.y;
            f2 = this.r.y;
        }
        if (this.n == ScrollState.ScrollState_FromStart) {
            round = Math.round(Math.min(f - f2, 0.0f) / 2.0f);
        } else {
            round = Math.round(Math.max(f - f2, 0.0f) / 2.0f);
        }
        a(round);
        return round;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        int o2 = o();
        int min = Math.min(o2, Math.max(-o2, i));
        if (this.p == ScrollDirection.SCROLL_DIRECTION_HORIZONTAL) {
            scrollTo(min, 0);
        } else {
            scrollTo(0, min);
        }
    }

    public final void smoothScrollTo(int i) {
        smoothScrollTo(i, (long) h(), 0, null);
    }

    public final void smoothScrollTo(int i, ISmoothScrollRunnableListener iSmoothScrollRunnableListener) {
        smoothScrollTo(i, (long) h(), 0, iSmoothScrollRunnableListener);
    }

    public final void smoothScrollTo(int i, long j, long j2, ISmoothScrollRunnableListener iSmoothScrollRunnableListener) {
        int scrollY;
        if (this.c != null) {
            this.c.stop();
        }
        if (this.p == ScrollDirection.SCROLL_DIRECTION_HORIZONTAL) {
            scrollY = getScrollX();
        } else {
            scrollY = getScrollY();
        }
        if (scrollY != i) {
            this.c = new SmoothScrollRunnable(scrollY, i, j, iSmoothScrollRunnableListener);
            if (j2 > 0) {
                postDelayed(this.c, j2);
            } else {
                post(this.c);
            }
        }
    }

    /* compiled from: ProGuard */
    public enum ScrollMode {
        PULL_FROM_START,
        PULL_FROM_END,
        BOTH,
        NONE,
        NOSCROLL;

        static ScrollMode mapIntToValue(int i) {
            switch (i) {
                case 0:
                    return PULL_FROM_START;
                case 1:
                    return PULL_FROM_END;
                case 2:
                    return BOTH;
                case 3:
                    return NONE;
                case 4:
                    return NOSCROLL;
                default:
                    return BOTH;
            }
        }
    }

    /* compiled from: ProGuard */
    public enum ScrollDirection {
        SCROLL_DIRECTION_VERTICAL,
        SCROLL_DIRECTION_HORIZONTAL;

        static ScrollDirection a(int i) {
            switch (i) {
                case 0:
                    return SCROLL_DIRECTION_VERTICAL;
                case 1:
                    return SCROLL_DIRECTION_HORIZONTAL;
                default:
                    return SCROLL_DIRECTION_VERTICAL;
            }
        }
    }

    /* compiled from: ProGuard */
    public class SmoothScrollRunnable implements Runnable {
        private final Interpolator b;
        private final int c;
        private final int d;
        private final long e;
        private boolean f = true;
        private long g = -1;
        private int h = -1;
        private ISmoothScrollRunnableListener i;

        public SmoothScrollRunnable(int i2, int i3, long j, ISmoothScrollRunnableListener iSmoothScrollRunnableListener) {
            this.d = i2;
            this.c = i3;
            this.e = j;
            this.i = iSmoothScrollRunnableListener;
            this.b = TXScrollViewBase.this.b;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.max(long, long):long}
         arg types: [long, int]
         candidates:
          ClspMth{java.lang.Math.max(double, double):double}
          ClspMth{java.lang.Math.max(int, int):int}
          ClspMth{java.lang.Math.max(float, float):float}
          ClspMth{java.lang.Math.max(long, long):long} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(long, long):long}
         arg types: [long, int]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(float, float):float}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(long, long):long} */
        public void run() {
            if (this.g == -1) {
                this.g = System.currentTimeMillis();
            } else {
                this.h = this.d - Math.round(this.b.getInterpolation(((float) Math.max(Math.min(((System.currentTimeMillis() - this.g) * 1000) / this.e, 1000L), 0L)) / 1000.0f) * ((float) (this.d - this.c)));
                TXScrollViewBase.this.a(this.h);
            }
            if (this.f && this.c != this.h) {
                String u = t.u();
                if (TextUtils.isEmpty(u) || (!u.contains("OZZO138T") && !u.contains("W9800B"))) {
                    ViewCompat.postOnAnimation(TXScrollViewBase.this, this);
                }
            } else if (this.i != null) {
                ISmoothScrollRunnableListener iSmoothScrollRunnableListener = this.i;
                this.i = null;
                iSmoothScrollRunnableListener.onSmoothScrollFinished();
            }
        }

        public void stop() {
            this.f = false;
            TXScrollViewBase.this.removeCallbacks(this);
            if (this.i != null) {
                ISmoothScrollRunnableListener iSmoothScrollRunnableListener = this.i;
                this.i = null;
                iSmoothScrollRunnableListener.onSmoothScrollFinished();
            }
        }
    }

    public void recycleData() {
    }

    /* access modifiers changed from: protected */
    public int h() {
        return 200;
    }
}
