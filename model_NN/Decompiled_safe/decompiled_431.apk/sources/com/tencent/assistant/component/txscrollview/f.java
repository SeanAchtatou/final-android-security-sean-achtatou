package com.tencent.assistant.component.txscrollview;

import com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase;

/* compiled from: ProGuard */
/* synthetic */ class f {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f708a = new int[TXRefreshScrollViewBase.RefreshState.values().length];

    static {
        try {
            f708a[TXRefreshScrollViewBase.RefreshState.RESET.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f708a[TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f708a[TXRefreshScrollViewBase.RefreshState.REFRESHING.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
    }
}
