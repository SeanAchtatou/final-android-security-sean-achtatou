package com.tencent.assistant.component.categorydetail;

/* compiled from: ProGuard */
public interface SmoothShrinkListener {
    void onShrink(int i);

    void onStart();

    void onStop();
}
