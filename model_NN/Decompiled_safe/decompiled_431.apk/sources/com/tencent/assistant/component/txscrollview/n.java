package com.tencent.assistant.component.txscrollview;

import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
class n implements TXScrollViewBase.ISmoothScrollRunnableListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TXRefreshGetMoreListView f716a;

    n(TXRefreshGetMoreListView tXRefreshGetMoreListView) {
        this.f716a = tXRefreshGetMoreListView;
    }

    public void onSmoothScrollFinished() {
        if (this.f716a.k != null) {
            this.f716a.k.onTXRefreshListViewRefresh(this.f716a.n);
        }
    }
}
