package com.tencent.assistant.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.listener.OnTMAClickListener;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.pangu.activity.SearchActivity;

/* compiled from: ProGuard */
class ak extends OnTMAClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SecondNavigationTitleView f632a;

    ak(SecondNavigationTitleView secondNavigationTitleView) {
        this.f632a = secondNavigationTitleView;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.f632a.context, SearchActivity.class);
        if (this.f632a.context instanceof BaseActivity) {
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.f632a.context).f());
        }
        this.f632a.context.startActivity(intent);
    }
}
