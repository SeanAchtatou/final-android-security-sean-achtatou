package com.tencent.assistant.component;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.PhotoBackupNewActivity;
import com.tencent.assistant.localres.model.LocalImage;
import com.tencent.connector.ipc.a;
import java.io.File;

/* compiled from: ProGuard */
class ah implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PhotoBackupMiddleGridView f629a;

    ah(PhotoBackupMiddleGridView photoBackupMiddleGridView) {
        this.f629a = photoBackupMiddleGridView;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        File file;
        if (a.a().d()) {
            LocalImage localImage = (LocalImage) this.f629a.mImageAdapter.getItem(i);
            if (localImage.path == null || ((file = new File(localImage.path)) != null && file.exists())) {
                if (this.f629a.mImageAdapter.isCheckItem(localImage.id)) {
                    this.f629a.mImageAdapter.unCheckItem(localImage.id);
                } else {
                    this.f629a.mImageAdapter.checkItem(localImage.id);
                }
                int checkedSize = this.f629a.mImageAdapter.getCheckedSize();
                if (this.f629a.activity instanceof PhotoBackupNewActivity) {
                    ((PhotoBackupNewActivity) this.f629a.activity).f(checkedSize);
                    return;
                }
                return;
            }
            Toast.makeText(this.f629a.getContext(), this.f629a.getContext().getString(R.string.wifi_file_del), 1).show();
            if (this.f629a.mImageAdapter != null) {
                this.f629a.mImageAdapter.removeItem2(i);
            }
        }
    }
}
