package com.tencent.assistant.component;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class u implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NormalErrorPage f723a;

    u(NormalErrorPage normalErrorPage) {
        this.f723a = normalErrorPage;
    }

    /* access modifiers changed from: protected */
    public Object clone() {
        return super.clone();
    }

    public void onClick(View view) {
        if (3 == this.f723a.currentState) {
            Intent intent = new Intent("android.settings.SETTINGS");
            if (b.a(this.f723a.getContext(), intent)) {
                this.f723a.getContext().startActivity(intent);
            } else {
                Toast.makeText(this.f723a.getContext(), (int) R.string.dialog_not_found_activity, 0).show();
            }
        } else {
            if (4 == this.f723a.currentState) {
                this.f723a.context.startActivity(new Intent(this.f723a.context, MainActivity.class));
            }
            if (this.f723a.listener != null) {
                this.f723a.listener.onClick(view);
            }
        }
    }
}
