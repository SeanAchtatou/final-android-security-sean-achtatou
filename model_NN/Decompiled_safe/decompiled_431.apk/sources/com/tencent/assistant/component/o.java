package com.tencent.assistant.component;

import android.os.Handler;
import android.os.Message;

/* compiled from: ProGuard */
class o extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HomePageBanner f683a;

    o(HomePageBanner homePageBanner) {
        this.f683a = homePageBanner;
    }

    public void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case 888:
                if (this.f683a.playing) {
                    this.f683a.mHorizonScrollLayout.displayNextScreen();
                }
                if (this.f683a.handler != null) {
                    this.f683a.handler.sendEmptyMessageDelayed(888, 5000);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
