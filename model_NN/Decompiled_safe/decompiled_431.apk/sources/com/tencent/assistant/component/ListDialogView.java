package com.tencent.assistant.component;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class ListDialogView extends RelativeLayout {
    private ListDialogAdapter adapter = null;
    private Context context = null;
    private boolean hasTitle = true;
    /* access modifiers changed from: private */
    public CharSequence[] list = null;
    private ListView listView = null;
    private AdapterView.OnItemClickListener listener = null;
    private TextView title = null;

    public ListDialogView(Context context2) {
        super(context2);
        this.context = context2;
        init();
    }

    public ListDialogView(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        this.context = context2;
        init();
    }

    public ListDialogView(Context context2, AttributeSet attributeSet, int i) {
        super(context2, attributeSet, i);
        this.context = context2;
        init();
    }

    private void init() {
        inflate(this.context, R.layout.list_dialog_view, this);
        this.title = (TextView) findViewById(R.id.title);
        this.listView = (ListView) findViewById(R.id.list);
        this.adapter = new ListDialogAdapter();
        this.listView.setDivider(null);
        this.listView.setSelector(new ColorDrawable(0));
        this.listView.setAdapter((ListAdapter) this.adapter);
    }

    public void setData(boolean z, CharSequence charSequence, CharSequence[] charSequenceArr) {
        this.hasTitle = z;
        if (this.hasTitle) {
            this.title.setVisibility(0);
            this.title.setText(charSequence);
        } else {
            this.title.setVisibility(8);
        }
        this.list = charSequenceArr;
    }

    public void setOnClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.listener = onItemClickListener;
        this.listView.setOnItemClickListener(this.listener);
    }

    /* compiled from: ProGuard */
    class ListDialogAdapter extends BaseAdapter {
        private LayoutInflater b;

        private ListDialogAdapter() {
            this.b = LayoutInflater.from(ListDialogView.this.getContext());
        }

        public int getCount() {
            if (ListDialogView.this.list != null) {
                return ListDialogView.this.list.length;
            }
            return 0;
        }

        public Object getItem(int i) {
            if (ListDialogView.this.list == null || getCount() <= i) {
                return null;
            }
            return ListDialogView.this.list[i];
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            q qVar;
            if (view == null) {
                qVar = new q(this);
                view = this.b.inflate((int) R.layout.popview_update, (ViewGroup) null);
                qVar.f684a = (TextView) view.findViewById(R.id.text);
                qVar.b = view.findViewById(R.id.line1);
                qVar.c = view.findViewById(R.id.line2);
                view.setTag(qVar);
            } else {
                qVar = (q) view.getTag();
            }
            qVar.f684a.setText((CharSequence) getItem(i));
            if (i == getCount() - 1) {
                qVar.b.setVisibility(8);
                qVar.c.setVisibility(8);
            } else {
                qVar.b.setVisibility(0);
                qVar.c.setVisibility(0);
            }
            return view;
        }
    }
}
