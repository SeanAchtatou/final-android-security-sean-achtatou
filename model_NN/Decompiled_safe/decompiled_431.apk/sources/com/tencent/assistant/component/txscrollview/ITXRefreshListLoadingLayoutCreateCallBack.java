package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
public interface ITXRefreshListLoadingLayoutCreateCallBack {
    TXLoadingLayoutBase createLoadingLayout(Context context, TXScrollViewBase.ScrollMode scrollMode);
}
