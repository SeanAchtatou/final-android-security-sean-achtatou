package com.tencent.assistant.component.listener;

import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.Map;

/* compiled from: ProGuard */
public abstract class OnTMAParamClickListener extends OnTMAClickListener {
    /* access modifiers changed from: protected */
    public void userActionReport(View view) {
        STInfoV2 stInfo = getStInfo();
        if (stInfo != null && (view.getContext() instanceof BaseActivity)) {
            ((BaseActivity) view.getContext()).a(stInfo.slotId, stInfo.status);
        }
        l.a(stInfo);
    }

    public int getClickViewId() {
        return this.clickViewId;
    }

    @Deprecated
    public String getSTSlotId() {
        return Constants.STR_EMPTY;
    }

    @Deprecated
    public int getActionType() {
        return 200;
    }

    @Deprecated
    public Map<String, String> getSTParameter() {
        return null;
    }

    @Deprecated
    public byte[] getOtherData() {
        return null;
    }

    @Deprecated
    public byte getNeedReportTimely() {
        return 0;
    }

    @Deprecated
    public int getPageId() {
        return 2000;
    }

    public STInfoV2 getStInfo() {
        return null;
    }
}
