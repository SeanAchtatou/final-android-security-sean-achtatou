package com.tencent.assistant.component.txscrollview;

import android.graphics.Bitmap;
import android.text.TextUtils;
import com.tencent.assistant.manager.t;

/* compiled from: ProGuard */
class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f710a;
    final /* synthetic */ Bitmap b;
    final /* synthetic */ g c;

    h(g gVar, String str, Bitmap bitmap) {
        this.c = gVar;
        this.f710a = str;
        this.b = bitmap;
    }

    public void run() {
        try {
            if (TextUtils.isEmpty(this.f710a) || !this.f710a.equals(this.c.f709a.mImageUrlString) || this.b == null || this.b.isRecycled()) {
                this.c.f709a.setImageResource(this.c.f709a.defaultResId);
                boolean unused = this.c.f709a.mIsLoadFinish = true;
                this.c.f709a.onImageLoadFinishCallListener(this.b);
            }
            this.c.f709a.setImageBitmap(this.b);
            boolean unused2 = this.c.f709a.mIsLoadFinish = true;
            this.c.f709a.onImageLoadFinishCallListener(this.b);
        } catch (Throwable th) {
            t.a().b();
        }
    }
}
