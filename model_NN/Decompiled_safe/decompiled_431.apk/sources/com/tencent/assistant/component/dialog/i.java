package com.tencent.assistant.component.dialog;

import android.app.Dialog;
import android.view.inputmethod.InputMethodManager;

/* compiled from: ProGuard */
final class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Dialog f655a;

    i(Dialog dialog) {
        this.f655a = dialog;
    }

    public void run() {
        if (this.f655a.getOwnerActivity() != null && !this.f655a.getOwnerActivity().isFinishing() && this.f655a.isShowing()) {
            try {
                ((InputMethodManager) this.f655a.getOwnerActivity().getSystemService("input_method")).toggleSoftInput(0, 2);
            } catch (Throwable th) {
            }
        }
    }
}
