package com.tencent.assistant.module.update;

import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
class l implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ j f1038a;

    l(j jVar) {
        this.f1038a = jVar;
    }

    public void run() {
        Map<Integer, ArrayList<AppUpdateInfo>> i = i.y().i();
        boolean z = false;
        if (i != null && !i.isEmpty()) {
            Iterator<Integer> it = i.keySet().iterator();
            while (it.hasNext()) {
                List list = i.get(it.next());
                if (list != null && !list.isEmpty()) {
                    Iterator it2 = list.iterator();
                    while (it2.hasNext()) {
                        AppUpdateInfo appUpdateInfo = (AppUpdateInfo) it2.next();
                        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(appUpdateInfo.f1162a);
                        if (localApkInfo == null || localApkInfo.mVersionCode >= appUpdateInfo.d) {
                            it2.remove();
                            z = true;
                        }
                    }
                }
                if (list == null || list.isEmpty()) {
                    it.remove();
                }
            }
        }
        if (z) {
            i.y().a(i);
            this.f1038a.g();
        }
    }
}
