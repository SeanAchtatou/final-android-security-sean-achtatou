package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.callback.l;
import com.tencent.assistant.protocol.jce.CardItem;
import com.tencent.assistant.protocol.jce.GetOneMoreAppRequest;
import com.tencent.assistant.protocol.jce.GetOneMoreAppResponse;
import com.tencent.assistant.utils.ah;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class as extends BaseEngine<l> {

    /* renamed from: a  reason: collision with root package name */
    private static as f968a;
    /* access modifiers changed from: private */
    public int b;

    public static synchronized as a() {
        as asVar;
        synchronized (as.class) {
            if (f968a == null) {
                f968a = new as();
            }
            asVar = f968a;
        }
        return asVar;
    }

    public int a(int i, long j) {
        this.b = send(new GetOneMoreAppRequest(i, j));
        ah.a().postDelayed(new at(this, this.b), 5000);
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        GetOneMoreAppResponse getOneMoreAppResponse;
        ArrayList<CardItem> arrayList;
        if (i == this.b && (getOneMoreAppResponse = (GetOneMoreAppResponse) jceStruct2) != null && (arrayList = getOneMoreAppResponse.b) != null) {
            notifyDataChangedInMainThread(new aw(this, k.a(arrayList, new av(this), getOneMoreAppResponse.a())));
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        if (i == this.b) {
            notifyDataChangedInMainThread(new ax(this, i2));
        }
    }
}
