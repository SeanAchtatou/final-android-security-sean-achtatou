package com.tencent.assistant.module.timer;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* compiled from: ProGuard */
class b extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f1013a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    b(a aVar, Looper looper) {
        super(looper);
        this.f1013a = aVar;
    }

    public void handleMessage(Message message) {
        ScheduleJob scheduleJob = (ScheduleJob) message.obj;
        scheduleJob.c();
        this.f1013a.a(scheduleJob);
    }
}
