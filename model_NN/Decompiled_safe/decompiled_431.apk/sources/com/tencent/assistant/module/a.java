package com.tencent.assistant.module;

import com.tencent.assistant.protocol.jce.AppCategory;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public ArrayList<a> f949a = null;
    public a b;
    public AppCategory c;

    public a(AppCategory appCategory) {
        this.c = appCategory;
    }

    public void a(a aVar) {
        if (aVar != null) {
            aVar.b = this;
            if (this.f949a == null) {
                this.f949a = new ArrayList<>();
            }
            this.f949a.add(aVar);
        }
    }
}
