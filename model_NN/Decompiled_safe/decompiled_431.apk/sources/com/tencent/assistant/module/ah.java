package com.tencent.assistant.module;

import android.os.Message;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;

/* compiled from: ProGuard */
class ah implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ag f961a;

    ah(ag agVar) {
        this.f961a = agVar;
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_GOFRONT:
                com.tencent.assistant.utils.ah.a().postDelayed(new ai(this), 2000);
                return;
            default:
                return;
        }
    }
}
