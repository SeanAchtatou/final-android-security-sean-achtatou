package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.a;
import com.tencent.assistant.protocol.jce.GetAppListResponse;
import java.util.ArrayList;

/* compiled from: ProGuard */
class g implements CallbackHelper.Caller<a> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f993a;
    final /* synthetic */ boolean b;
    final /* synthetic */ ArrayList c;
    final /* synthetic */ GetAppListResponse d;
    final /* synthetic */ b e;

    g(b bVar, int i, boolean z, ArrayList arrayList, GetAppListResponse getAppListResponse) {
        this.e = bVar;
        this.f993a = i;
        this.b = z;
        this.c = arrayList;
        this.d = getAppListResponse;
    }

    /* renamed from: a */
    public void call(a aVar) {
        aVar.a(this.f993a, 0, this.b, this.c, this.d.f);
    }
}
