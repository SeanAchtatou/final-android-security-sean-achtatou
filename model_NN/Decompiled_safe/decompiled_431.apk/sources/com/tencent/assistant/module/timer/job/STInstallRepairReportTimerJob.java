package com.tencent.assistant.module.timer.job;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.SystemClock;
import com.qq.AppService.AstApp;
import com.tencent.assistant.module.timer.SimpleBaseScheduleJob;
import com.tencent.assistantv2.st.b;

/* compiled from: ProGuard */
public class STInstallRepairReportTimerJob extends SimpleBaseScheduleJob {

    /* renamed from: a  reason: collision with root package name */
    private static STInstallRepairReportTimerJob f1020a;

    public static synchronized STInstallRepairReportTimerJob e() {
        STInstallRepairReportTimerJob sTInstallRepairReportTimerJob;
        synchronized (STInstallRepairReportTimerJob.class) {
            if (f1020a == null) {
                f1020a = new STInstallRepairReportTimerJob();
            }
            sTInstallRepairReportTimerJob = f1020a;
        }
        return sTInstallRepairReportTimerJob;
    }

    public int h() {
        return 180;
    }

    public void c() {
        b.a().c();
    }

    public void d() {
        long j;
        Intent intent = new Intent("com.tencent.android.qqdownloader.action.SCHEDULE_JOB");
        intent.putExtra("com.tencent.android.qqdownloader.key.SCHEDULE_JOB", getClass().getName());
        PendingIntent broadcast = PendingIntent.getBroadcast(AstApp.i(), b(), intent, 134217728);
        int h = h() * 1000;
        long j2 = (long) h;
        if (h > 0) {
            j = ((long) h) - (SystemClock.elapsedRealtime() % ((long) h));
        } else {
            j = j2;
        }
        try {
            ((AlarmManager) AstApp.i().getSystemService("alarm")).setRepeating(3, j + SystemClock.elapsedRealtime(), (long) h, broadcast);
        } catch (Throwable th) {
        }
    }
}
