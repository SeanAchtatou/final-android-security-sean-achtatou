package com.tencent.assistant.module.timer;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.SystemClock;
import com.qq.AppService.AstApp;
import com.tencent.assistant.Global;
import com.tencent.assistant.module.update.z;
import com.tencent.assistant.utils.XLog;
import com.tencent.nucleus.manager.backgroundscan.BackgroundScanTimerJob;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/* compiled from: ProGuard */
public abstract class BaseTimePointJob implements TimePointJob {
    /* access modifiers changed from: protected */
    public abstract void d();

    public final int b() {
        return getClass().getSimpleName().hashCode();
    }

    private int h() {
        return b();
    }

    public boolean a() {
        return true;
    }

    public final void c() {
        d();
        SystemClock.sleep(100);
        i();
    }

    public void e() {
        i();
    }

    public void f() {
        Intent intent = new Intent("com.tencent.android.qqdownloader.action.SCHEDULE_JOB");
        intent.putExtra("com.tencent.android.qqdownloader.key.SCHEDULE_JOB", getClass().getName());
        ((AlarmManager) AstApp.i().getSystemService("alarm")).cancel(PendingIntent.getBroadcast(AstApp.i(), h(), intent, 268435456));
    }

    private long a(long j) {
        int i;
        boolean z = false;
        int[] g = g();
        if (g == null) {
            return -1;
        }
        int i2 = g[0];
        int length = g.length;
        int i3 = 0;
        while (true) {
            if (i3 >= length) {
                z = true;
                i = i2;
                break;
            }
            int i4 = g[i3];
            if (i4 > 0 && z.a(j) < i4) {
                i = i4;
                break;
            }
            i3++;
        }
        Calendar c = z.c(i);
        if (z) {
            c.set(6, c.get(6) + 1);
        }
        return c.getTimeInMillis();
    }

    private void i() {
        long a2 = a(System.currentTimeMillis());
        if (a2 > 0) {
            if (Global.isDev() && getClass().getSimpleName().equals(BackgroundScanTimerJob.class.getSimpleName())) {
                XLog.d("BackgroundScan", "<timer> Background scan will be triggered at : " + new SimpleDateFormat("yyyy年M月d日 HH:mm", Locale.getDefault()).format(new Date(a2)));
            }
            Intent intent = new Intent("com.tencent.android.qqdownloader.action.SCHEDULE_JOB");
            intent.putExtra("com.tencent.android.qqdownloader.key.SCHEDULE_JOB", getClass().getName());
            ((AlarmManager) AstApp.i().getSystemService("alarm")).set(0, a2, PendingIntent.getBroadcast(AstApp.i(), h(), intent, 268435456));
        }
    }
}
