package com.tencent.assistant.kapalaiadapter.a;

import android.content.Context;
import com.tencent.assistant.kapalaiadapter.g;

/* compiled from: ProGuard */
public class a implements j {

    /* renamed from: a  reason: collision with root package name */
    private Object[] f780a = null;

    public Object a(int i, Context context) {
        char c = 0;
        if (this.f780a == null) {
            try {
                this.f780a = new Object[2];
                this.f780a[0] = context.getSystemService("phone");
                this.f780a[1] = context.getSystemService("phone2");
            } catch (Exception e) {
            }
        }
        if (this.f780a == null || this.f780a.length <= i) {
            return null;
        }
        Object[] objArr = this.f780a;
        if (i > 0) {
            c = 1;
        }
        return objArr[c];
    }

    public String b(int i, Context context) {
        Object a2 = a(i, context);
        if (a2 == null) {
            return null;
        }
        try {
            return (String) g.a(a2, "getSubscriberId");
        } catch (Exception e) {
            return null;
        }
    }

    public String c(int i, Context context) {
        Object a2 = a(i, context);
        if (a2 == null) {
            return null;
        }
        try {
            return (String) g.a(a2, "getDeviceId");
        } catch (Exception e) {
            return null;
        }
    }
}
