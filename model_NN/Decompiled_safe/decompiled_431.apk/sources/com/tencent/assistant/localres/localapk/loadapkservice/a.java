package com.tencent.assistant.localres.localapk.loadapkservice;

import android.content.Context;
import android.os.Process;
import com.tencent.assistant.utils.XLog;
import java.lang.Thread;

/* compiled from: ProGuard */
public class a implements Thread.UncaughtExceptionHandler {

    /* renamed from: a  reason: collision with root package name */
    private static a f842a = null;
    private Thread.UncaughtExceptionHandler b;

    public static a a() {
        if (f842a == null) {
            f842a = new a();
        }
        return f842a;
    }

    public void a(Context context) {
        XLog.d("CrashHandler", "ExceptionCatcher init, Thread: " + Thread.currentThread().getName() + "-" + System.identityHashCode(Thread.currentThread()) + "-" + Thread.currentThread().hashCode());
        this.b = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    public void uncaughtException(Thread thread, Throwable th) {
        if (a(th) || this.b == null) {
            XLog.d("CrashHandler", "uncaughtException kill and exit");
            Process.killProcess(Process.myPid());
            System.exit(10);
            return;
        }
        XLog.d("CrashHandler", "uncaughtException call defaultHandler");
        this.b.uncaughtException(thread, th);
    }

    private boolean a(Throwable th) {
        XLog.d("CrashHandler", "handleException");
        if (th != null) {
            th.printStackTrace();
        }
        return true;
    }
}
