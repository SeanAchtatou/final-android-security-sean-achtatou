package com.tencent.assistant.localres;

import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.Map;

/* compiled from: ProGuard */
class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkResourceManager f825a;

    f(ApkResourceManager apkResourceManager) {
        this.f825a = apkResourceManager;
    }

    public void run() {
        XLog.d("AppUpdateEngine", "***** startHeavyLoad启动扫描");
        Map<String, LocalApkInfo> a2 = this.f825a.f.a(this.f825a.h, false);
        if (a2 != null) {
            this.f825a.h.clear();
            this.f825a.h.putAll(a2);
            boolean unused = ApkResourceManager.m = true;
            this.f825a.updateAppLauncherTime();
            XLog.d("AppUpdateEngine", "***** startHeavyLoad启动扫描成功");
            this.f825a.f();
            this.f825a.g();
            if (this.f825a.f.a()) {
                ArrayList arrayList = new ArrayList(this.f825a.h.values());
                this.f825a.c.b(arrayList, 1);
                this.f825a.c.a(arrayList);
            }
        } else {
            boolean unused2 = ApkResourceManager.m = true;
            this.f825a.i();
        }
        boolean unused3 = ApkResourceManager.k = false;
    }
}
