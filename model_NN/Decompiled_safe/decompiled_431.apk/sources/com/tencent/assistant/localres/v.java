package com.tencent.assistant.localres;

import com.tencent.assistant.localres.model.LocalApkInfo;
import java.util.Comparator;

/* compiled from: ProGuard */
final class v implements Comparator<LocalApkInfo> {
    v() {
    }

    /* renamed from: a */
    public int compare(LocalApkInfo localApkInfo, LocalApkInfo localApkInfo2) {
        if (localApkInfo == null || localApkInfo2 == null) {
            return 0;
        }
        if (localApkInfo2.mVersionCode > localApkInfo.mVersionCode) {
            return 1;
        }
        if (localApkInfo2.mVersionCode != localApkInfo.mVersionCode) {
            return -1;
        }
        return 0;
    }
}
