package com.tencent.assistant.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class TXTabViewPageAdapter extends PagerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<ag> f577a = new ArrayList<>();

    public void addPageItem(String str, View view) {
        if (str != null && view != null) {
            this.f577a.add(new ag(this, str, view));
        }
    }

    public ArrayList<String> getTitleList() {
        if (this.f577a == null || this.f577a.size() <= 0) {
            return null;
        }
        ArrayList<String> arrayList = new ArrayList<>();
        Iterator<ag> it = this.f577a.iterator();
        while (it.hasNext()) {
            arrayList.add(it.next().f585a);
        }
        return arrayList;
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        ag agVar = this.f577a.get(i);
        if (agVar != null) {
            viewGroup.removeView(agVar.b);
        }
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        ag agVar = this.f577a.get(i);
        if (agVar == null) {
            return null;
        }
        viewGroup.addView(agVar.b);
        return agVar.b;
    }

    public int getCount() {
        if (this.f577a != null) {
            return this.f577a.size();
        }
        return 0;
    }

    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }
}
