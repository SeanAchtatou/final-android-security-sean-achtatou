package com.tencent.assistant.adapter;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class p extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f600a;
    final /* synthetic */ int b;
    final /* synthetic */ RankClassicListAdapter c;

    p(RankClassicListAdapter rankClassicListAdapter, SimpleAppModel simpleAppModel, int i) {
        this.c = rankClassicListAdapter;
        this.f600a = simpleAppModel;
        this.b = i;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.c.e, AppDetailActivityV5.class);
        if (this.c.e instanceof BaseActivity) {
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.c.e).f());
        }
        intent.putExtra("simpleModeInfo", this.f600a);
        intent.putExtra("statInfo", new StatInfo(this.f600a.b, this.c.i, this.c.l.d(), this.c.l.b() + "|" + this.c.j + "|" + this.c.k, this.c.l.a()));
        this.c.e.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.c.e, this.f600a, this.c.b(this.b), 200, null);
        if (buildSTInfo != null) {
            String valueOf = String.valueOf(this.c.i);
            if (!TextUtils.isEmpty(this.c.l.b())) {
                valueOf = valueOf + "|" + this.c.l.b() + "|" + this.f600a.f938a + "|" + this.f600a.b + "|" + this.c.k;
            }
            buildSTInfo.extraData = valueOf;
        }
        return buildSTInfo;
    }
}
