package com.tencent.assistant.plugin;

import com.tencent.assistant.module.callback.CallbackHelper;

/* compiled from: ProGuard */
class c implements CallbackHelper.Caller<GetPluginListCallback> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GetPluginListEngine f1087a;

    c(GetPluginListEngine getPluginListEngine) {
        this.f1087a = getPluginListEngine;
    }

    /* renamed from: a */
    public void call(GetPluginListCallback getPluginListCallback) {
        getPluginListCallback.success(this.f1087a.e);
    }
}
