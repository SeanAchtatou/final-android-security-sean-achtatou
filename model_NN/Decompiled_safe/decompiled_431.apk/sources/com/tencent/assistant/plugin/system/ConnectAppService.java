package com.tencent.assistant.plugin.system;

import com.tencent.assistant.plugin.PluginInfo;

/* compiled from: ProGuard */
public class ConnectAppService extends BaseAppService {
    public int a() {
        return 1;
    }

    public String a(PluginInfo pluginInfo) {
        if (pluginInfo != null) {
            return pluginInfo.getAppServiceImpl();
        }
        return null;
    }
}
