package com.tencent.assistant.plugin;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* compiled from: ProGuard */
final class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Method f1094a;
    final /* synthetic */ Object b;

    e(Method method, Object obj) {
        this.f1094a = method;
        this.b = obj;
    }

    public void run() {
        try {
            this.f1094a.invoke(this.b, new Object[0]);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        } catch (InvocationTargetException e3) {
            e3.printStackTrace();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
