package com.tencent.assistant.plugin.system;

import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class FreewifiService extends BaseAppService {
    public int a() {
        return 0;
    }

    public String a(PluginInfo pluginInfo) {
        XLog.d("FreewifiService-frame", "<freewifi> getServiceClassPath... ");
        if (pluginInfo != null) {
            return pluginInfo.getExtendServiceImpl(PluginInfo.META_DATA_FREE_WIFI_SERVICE);
        }
        return null;
    }
}
