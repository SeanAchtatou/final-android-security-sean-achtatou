package com.tencent.assistant.plugin.mgr;

import com.tencent.assistant.Global;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.plugin.PluginDownloadInfo;
import com.tencent.assistant.plugin.PluginInfo;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static b[] f1095a = {new b("com.tencent.mobileassistant_login", 1), new b("com.tencent.mobileassistant_login", 2), new b("p.com.tencent.android.qqdownloader", 1), new b("p.com.tencent.android.qqdownloader", 2), new b("p.com.tencent.android.qqdownloader", 3), new b("p.com.tencent.android.qqdownloader", 0), new b("com.tencent.assistant.root", 1), new b("com.tencent.assistant.root", 2), new b("com.tencent.assistant.root", 3), new b("com.tencent.assistant.root", 4), new b("com.tencent.assistant.freewifi", 8), new b("com.tencent.assistant.freewifi", 9), new b("com.tencent.assistant.freewifi", 10), new b("com.tencent.assistant.freewifi", 104), new b("com.tencent.assistant.freewifi", 105), new b("com.tencent.assistant.freewifi", 106), new b("com.tencent.assistant.freewifi", 107), new b("com.tencent.assistant.freewifi", 108), new b("com.tencent.assistant.freewifi", 109), new b("com.tencent.assistant.freewifi", NormalErrorRecommendPage.ERROR_TYPE_SEARCH_RESULT_EMPTY), new b("com.tencent.assistant.freewifi", 112), new b("com.tencent.assistant.freewifi", 113), new b("com.tencent.assistant.freewifi", 114), new b("com.tencent.assistant.freewifi", 115), new b("com.tencent.assistant.freewifi", 116), new b("com.tencent.assistant.freewifi", 117), new b("com.tencent.assistant.freewifi", 118)};

    public static boolean a(PluginInfo pluginInfo) {
        if (pluginInfo != null && pluginInfo.getMinFLevel() <= j.b() && pluginInfo.getMinBaoVersion() <= Global.getAppVersionCode()) {
            return a(pluginInfo.getPackageName(), pluginInfo.getVersion());
        }
        return false;
    }

    public static boolean a(String str, int i) {
        if (f1095a != null && f1095a.length > 0) {
            for (b bVar : f1095a) {
                if (bVar.f1096a.equals(str) && (bVar.b == i || bVar.b == 0)) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean a(PluginDownloadInfo pluginDownloadInfo) {
        return true;
    }
}
