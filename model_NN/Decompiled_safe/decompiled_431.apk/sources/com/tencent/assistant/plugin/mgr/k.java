package com.tencent.assistant.plugin.mgr;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.plugin.PluginDownloadInfo;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.activity.PluginDownActivity;
import com.tencent.tauth.AuthActivity;

/* compiled from: ProGuard */
public class k implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private static Runnable f1105a = null;
    private static k b;

    private k() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DIALOG_NORMAL_INSTALLED, this);
    }

    public static synchronized k a() {
        k kVar;
        synchronized (k.class) {
            if (b == null) {
                b = new k();
            }
            kVar = b;
        }
        return kVar;
    }

    public int a(Context context, String str, String str2, int i, int i2, int i3, String str3) {
        Bundle bundle = new Bundle();
        bundle.putInt(AuthActivity.ACTION_KEY, 1000);
        bundle.putString("id", str);
        bundle.putString("name", str2);
        if (i != -1) {
            bundle.putInt("maxchap", i);
        }
        if (i3 != -1) {
            bundle.putInt("finish", i3);
        }
        if (i2 != -1) {
            bundle.putInt("chap", i2);
        }
        bundle.putString("author", str3);
        return a(context, bundle);
    }

    public int a(Context context, String str, String str2, int i, int i2, String str3) {
        return a(context, str, str2, i, -1, i2, str3);
    }

    public int a(Context context, String str, String str2, int i, int i2) {
        Bundle bundle = new Bundle();
        bundle.putInt(AuthActivity.ACTION_KEY, 1001);
        bundle.putString("id", str);
        bundle.putString("name", str2);
        bundle.putInt("maxchap", i);
        bundle.putInt("finish", i2);
        return a(context, bundle);
    }

    public int b(Context context, String str, String str2, int i, int i2, String str3) {
        Bundle bundle = new Bundle();
        bundle.putInt(AuthActivity.ACTION_KEY, EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE);
        bundle.putString("id", str);
        bundle.putString("name", str2);
        bundle.putInt("maxchap", i);
        bundle.putInt("finish", i2);
        bundle.putString("author", str3);
        return a(context, bundle);
    }

    public int a(Context context, long j, int i) {
        Bundle bundle = new Bundle();
        bundle.putInt(AuthActivity.ACTION_KEY, EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START);
        bundle.putLong("uin", j);
        bundle.putInt("pay_requestcode", i);
        return a(context, bundle);
    }

    private int a(Context context, Bundle bundle) {
        PluginInfo a2 = i.b().a("com.qqreader");
        l lVar = new l(this, bundle);
        PluginDownloadInfo a3 = c.a().a("com.qqreader");
        if (a2 == null || (a3 != null && a3.version > a2.getVersion() && h.a("com.qqreader") < 0)) {
            f1105a = lVar;
            Intent intent = new Intent(context, PluginDownActivity.class);
            intent.putExtra("package_name", "com.qqreader");
            context.startActivity(intent);
            return -1;
        }
        lVar.run();
        return 0;
    }

    public int a(Context context, String str, String str2) {
        Bundle bundle = new Bundle();
        bundle.putInt(AuthActivity.ACTION_KEY, 1002);
        bundle.putString("id", str);
        bundle.putString("name", str2);
        return a(context, bundle);
    }

    public int a(Context context, String str) {
        Bundle bundle = new Bundle();
        bundle.putInt(AuthActivity.ACTION_KEY, EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING);
        bundle.putString("id", str);
        return a(context, bundle);
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_PLUGIN_DIALOG_NORMAL_INSTALLED:
                if ((message.obj instanceof String) && "com.qqreader".equals(message.obj) && f1105a != null) {
                    f1105a.run();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
