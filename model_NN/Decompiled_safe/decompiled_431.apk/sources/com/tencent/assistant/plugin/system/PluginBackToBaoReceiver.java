package com.tencent.assistant.plugin.system;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class PluginBackToBaoReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private static String f1109a = Constants.STR_EMPTY;
    private static int b = 0;

    public static synchronized String a() {
        String str;
        synchronized (PluginBackToBaoReceiver.class) {
            str = f1109a;
        }
        return str;
    }

    public static synchronized int b() {
        int i;
        synchronized (PluginBackToBaoReceiver.class) {
            i = b;
        }
        return i;
    }

    public void onReceive(Context context, Intent intent) {
        if (intent != null && intent.getAction() != null && intent.getAction().equals("com.tencent.android.qqdownloader.action.CONNECT_PC_STATE")) {
            b = intent.getIntExtra("pc_state_result", 0);
            f1109a = intent.getStringExtra("pc_connect_name");
            AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_PLUGIN_CONNECTOR_STATE_CHANGE));
        }
    }
}
