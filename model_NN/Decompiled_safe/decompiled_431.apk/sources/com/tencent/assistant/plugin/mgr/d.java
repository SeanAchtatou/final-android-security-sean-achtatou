package com.tencent.assistant.plugin.mgr;

import com.qq.AppService.AstApp;
import com.tencent.assistant.plugin.PluginDownloadInfo;
import com.tencent.pangu.download.DownloadInfo;

/* compiled from: ProGuard */
class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f1098a;
    final /* synthetic */ PluginDownloadInfo b;
    final /* synthetic */ c c;

    d(c cVar, DownloadInfo downloadInfo, PluginDownloadInfo pluginDownloadInfo) {
        this.c = cVar;
        this.f1098a = downloadInfo;
        this.b = pluginDownloadInfo;
    }

    public void run() {
        try {
            i.b().a(AstApp.i(), this.f1098a.getDownloadingPath(), this.b.pluginPackageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
