package com.tencent.assistant.plugin.system;

import android.content.BroadcastReceiver;
import android.content.Context;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.h;

/* compiled from: ProGuard */
public class ConnectHeartBeatReceiver extends PluginDispatchReceiver {
    /* access modifiers changed from: protected */
    public int a() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public BroadcastReceiver a(Context context, PluginInfo pluginInfo) {
        return h.h(context, pluginInfo);
    }
}
