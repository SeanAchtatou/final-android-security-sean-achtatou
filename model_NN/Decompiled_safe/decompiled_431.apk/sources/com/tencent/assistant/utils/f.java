package com.tencent.assistant.utils;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;

/* compiled from: ProGuard */
public class f {
    public static int a(String str, int i) {
        LocalApkInfo b = e.b(str);
        if (b == null) {
            return 1;
        }
        if (b.mVersionCode >= i) {
            return 2;
        }
        return b(str, i) ? 4 : 3;
    }

    public static boolean b(String str, int i) {
        SimpleAppModel b = k.b(str);
        if (b != null && k.d(b) == AppConst.AppState.DOWNLOADED) {
            return true;
        }
        return false;
    }
}
