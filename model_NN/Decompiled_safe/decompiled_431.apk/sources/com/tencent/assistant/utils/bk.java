package com.tencent.assistant.utils;

import java.util.concurrent.FutureTask;

/* compiled from: ProGuard */
class bk implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FutureTask f1837a;
    final /* synthetic */ TemporaryThreadManager b;

    bk(TemporaryThreadManager temporaryThreadManager, FutureTask futureTask) {
        this.b = temporaryThreadManager;
        this.f1837a = futureTask;
    }

    public void run() {
        if (!this.f1837a.isDone() && !this.f1837a.isCancelled()) {
            this.f1837a.cancel(true);
        }
    }
}
