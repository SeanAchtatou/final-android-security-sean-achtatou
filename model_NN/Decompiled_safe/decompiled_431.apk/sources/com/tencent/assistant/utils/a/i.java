package com.tencent.assistant.utils.a;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.RandomAccessFile;

/* compiled from: ProGuard */
public class i {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f1810a = new byte[4096];

    /* JADX WARNING: Removed duplicated region for block: B:291:0x04da A[SYNTHETIC, Splitter:B:291:0x04da] */
    /* JADX WARNING: Removed duplicated region for block: B:294:0x04df A[SYNTHETIC, Splitter:B:294:0x04df] */
    /* JADX WARNING: Removed duplicated region for block: B:303:0x050a A[SYNTHETIC, Splitter:B:303:0x050a] */
    /* JADX WARNING: Removed duplicated region for block: B:306:0x050f A[SYNTHETIC, Splitter:B:306:0x050f] */
    /* JADX WARNING: Removed duplicated region for block: B:309:0x0514 A[SYNTHETIC, Splitter:B:309:0x0514] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.tencent.assistant.utils.a.b a(java.lang.String r13, java.lang.String r14, java.lang.String r15) {
        /*
            r12 = this;
            r0 = 0
            r2 = 0
            r11 = 13
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r1 = r1.append(r15)
            java.lang.String r3 = ".merging"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r6 = r1.toString()
            com.tencent.assistant.utils.a.b r1 = new com.tencent.assistant.utils.a.b
            r1.<init>()
            r3 = 0
            r7 = 0
            r8 = 0
            com.tencent.assistant.utils.a.k r9 = new com.tencent.assistant.utils.a.k     // Catch:{ Throwable -> 0x04b7, all -> 0x0504 }
            r9.<init>()     // Catch:{ Throwable -> 0x04b7, all -> 0x0504 }
            boolean r4 = r9.a(r13)     // Catch:{ IOException -> 0x0044 }
            if (r4 != 0) goto L_0x008c
            r0 = 1
            java.lang.String r4 = "oldApkParser  parse method return false"
            com.tencent.assistant.utils.a.b r0 = r1.a(r0, r4)     // Catch:{ IOException -> 0x0044 }
            if (r2 == 0) goto L_0x0036
            r8.close()     // Catch:{ IOException -> 0x0568 }
        L_0x0036:
            if (r2 == 0) goto L_0x003b
            r7.close()     // Catch:{ IOException -> 0x056e }
        L_0x003b:
            if (r2 == 0) goto L_0x0040
            r3.close()     // Catch:{ IOException -> 0x0574 }
        L_0x0040:
            com.tencent.assistant.utils.FileUtil.rename(r6, r15)     // Catch:{ Exception -> 0x057a }
        L_0x0043:
            return r0
        L_0x0044:
            r0 = move-exception
            r4 = 1
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x04b7, all -> 0x0504 }
            r5.<init>()     // Catch:{ Throwable -> 0x04b7, all -> 0x0504 }
            java.lang.String r9 = ""
            java.lang.StringBuilder r5 = r5.append(r9)     // Catch:{ Throwable -> 0x04b7, all -> 0x0504 }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ Throwable -> 0x04b7, all -> 0x0504 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x04b7, all -> 0x0504 }
            com.tencent.assistant.utils.a.b r0 = r1.a(r4, r0)     // Catch:{ Throwable -> 0x04b7, all -> 0x0504 }
            if (r2 == 0) goto L_0x0062
            r8.close()     // Catch:{ IOException -> 0x0556 }
        L_0x0062:
            if (r2 == 0) goto L_0x0067
            r7.close()     // Catch:{ IOException -> 0x055c }
        L_0x0067:
            if (r2 == 0) goto L_0x006c
            r3.close()     // Catch:{ IOException -> 0x0562 }
        L_0x006c:
            com.tencent.assistant.utils.FileUtil.rename(r6, r15)     // Catch:{ Exception -> 0x0070 }
            goto L_0x0043
        L_0x0070:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.assistant.utils.a.b r0 = r1.a(r11, r0)
            goto L_0x0043
        L_0x008c:
            java.io.DataInputStream r5 = new java.io.DataInputStream     // Catch:{ FileNotFoundException -> 0x00ca }
            java.io.BufferedInputStream r4 = new java.io.BufferedInputStream     // Catch:{ FileNotFoundException -> 0x00ca }
            java.io.FileInputStream r10 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x00ca }
            r10.<init>(r14)     // Catch:{ FileNotFoundException -> 0x00ca }
            r4.<init>(r10)     // Catch:{ FileNotFoundException -> 0x00ca }
            r5.<init>(r4)     // Catch:{ FileNotFoundException -> 0x00ca }
            java.io.DataOutputStream r4 = new java.io.DataOutputStream     // Catch:{ FileNotFoundException -> 0x0114 }
            java.io.BufferedOutputStream r3 = new java.io.BufferedOutputStream     // Catch:{ FileNotFoundException -> 0x0114 }
            java.io.FileOutputStream r10 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0114 }
            r10.<init>(r6)     // Catch:{ FileNotFoundException -> 0x0114 }
            r3.<init>(r10)     // Catch:{ FileNotFoundException -> 0x0114 }
            r4.<init>(r3)     // Catch:{ FileNotFoundException -> 0x0114 }
            java.io.RandomAccessFile r3 = new java.io.RandomAccessFile     // Catch:{ FileNotFoundException -> 0x015e }
            java.lang.String r7 = "r"
            r3.<init>(r13, r7)     // Catch:{ FileNotFoundException -> 0x015e }
            java.util.LinkedList r2 = new java.util.LinkedList     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            r2.<init>()     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            int r7 = r5.readInt()     // Catch:{ IOException -> 0x01a8 }
        L_0x00ba:
            if (r0 >= r7) goto L_0x02d1
            com.tencent.assistant.utils.a.f r8 = new com.tencent.assistant.utils.a.f     // Catch:{ IOException -> 0x01f2, ClassNotFoundException -> 0x023c, OutOfMemoryError -> 0x0286 }
            r8.<init>()     // Catch:{ IOException -> 0x01f2, ClassNotFoundException -> 0x023c, OutOfMemoryError -> 0x0286 }
            r8.b(r5)     // Catch:{ IOException -> 0x01f2, ClassNotFoundException -> 0x023c, OutOfMemoryError -> 0x0286 }
            r2.add(r8)     // Catch:{ IOException -> 0x01f2, ClassNotFoundException -> 0x023c, OutOfMemoryError -> 0x0286 }
            int r0 = r0 + 1
            goto L_0x00ba
        L_0x00ca:
            r0 = move-exception
            r4 = 2
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x04b7, all -> 0x0504 }
            r5.<init>()     // Catch:{ Throwable -> 0x04b7, all -> 0x0504 }
            java.lang.String r9 = ""
            java.lang.StringBuilder r5 = r5.append(r9)     // Catch:{ Throwable -> 0x04b7, all -> 0x0504 }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ Throwable -> 0x04b7, all -> 0x0504 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x04b7, all -> 0x0504 }
            com.tencent.assistant.utils.a.b r0 = r1.a(r4, r0)     // Catch:{ Throwable -> 0x04b7, all -> 0x0504 }
            if (r2 == 0) goto L_0x00e8
            r8.close()     // Catch:{ IOException -> 0x0597 }
        L_0x00e8:
            if (r2 == 0) goto L_0x00ed
            r7.close()     // Catch:{ IOException -> 0x059d }
        L_0x00ed:
            if (r2 == 0) goto L_0x00f2
            r3.close()     // Catch:{ IOException -> 0x05a3 }
        L_0x00f2:
            com.tencent.assistant.utils.FileUtil.rename(r6, r15)     // Catch:{ Exception -> 0x00f7 }
            goto L_0x0043
        L_0x00f7:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.assistant.utils.a.b r0 = r1.a(r11, r0)
            goto L_0x0043
        L_0x0114:
            r0 = move-exception
            r3 = 3
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x06b0, all -> 0x069e }
            r4.<init>()     // Catch:{ Throwable -> 0x06b0, all -> 0x069e }
            java.lang.String r9 = ""
            java.lang.StringBuilder r4 = r4.append(r9)     // Catch:{ Throwable -> 0x06b0, all -> 0x069e }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ Throwable -> 0x06b0, all -> 0x069e }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x06b0, all -> 0x069e }
            com.tencent.assistant.utils.a.b r0 = r1.a(r3, r0)     // Catch:{ Throwable -> 0x06b0, all -> 0x069e }
            if (r2 == 0) goto L_0x0132
            r8.close()     // Catch:{ IOException -> 0x05a9 }
        L_0x0132:
            if (r2 == 0) goto L_0x0137
            r7.close()     // Catch:{ IOException -> 0x05af }
        L_0x0137:
            if (r5 == 0) goto L_0x013c
            r5.close()     // Catch:{ IOException -> 0x05b5 }
        L_0x013c:
            com.tencent.assistant.utils.FileUtil.rename(r6, r15)     // Catch:{ Exception -> 0x0141 }
            goto L_0x0043
        L_0x0141:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.assistant.utils.a.b r0 = r1.a(r11, r0)
            goto L_0x0043
        L_0x015e:
            r0 = move-exception
            r3 = 1
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x06b5, all -> 0x06a3 }
            r7.<init>()     // Catch:{ Throwable -> 0x06b5, all -> 0x06a3 }
            java.lang.String r9 = ""
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ Throwable -> 0x06b5, all -> 0x06a3 }
            java.lang.StringBuilder r0 = r7.append(r0)     // Catch:{ Throwable -> 0x06b5, all -> 0x06a3 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x06b5, all -> 0x06a3 }
            com.tencent.assistant.utils.a.b r0 = r1.a(r3, r0)     // Catch:{ Throwable -> 0x06b5, all -> 0x06a3 }
            if (r2 == 0) goto L_0x017c
            r8.close()     // Catch:{ IOException -> 0x05bb }
        L_0x017c:
            if (r4 == 0) goto L_0x0181
            r4.close()     // Catch:{ IOException -> 0x05c1 }
        L_0x0181:
            if (r5 == 0) goto L_0x0186
            r5.close()     // Catch:{ IOException -> 0x05c7 }
        L_0x0186:
            com.tencent.assistant.utils.FileUtil.rename(r6, r15)     // Catch:{ Exception -> 0x018b }
            goto L_0x0043
        L_0x018b:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.assistant.utils.a.b r0 = r1.a(r11, r0)
            goto L_0x0043
        L_0x01a8:
            r0 = move-exception
            r2 = 4
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            r7.<init>()     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.String r8 = ""
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.StringBuilder r0 = r7.append(r0)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            com.tencent.assistant.utils.a.b r0 = r1.a(r2, r0)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            if (r3 == 0) goto L_0x01c6
            r3.close()     // Catch:{ IOException -> 0x05cd }
        L_0x01c6:
            if (r4 == 0) goto L_0x01cb
            r4.close()     // Catch:{ IOException -> 0x05d3 }
        L_0x01cb:
            if (r5 == 0) goto L_0x01d0
            r5.close()     // Catch:{ IOException -> 0x05d9 }
        L_0x01d0:
            com.tencent.assistant.utils.FileUtil.rename(r6, r15)     // Catch:{ Exception -> 0x01d5 }
            goto L_0x0043
        L_0x01d5:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.assistant.utils.a.b r0 = r1.a(r11, r0)
            goto L_0x0043
        L_0x01f2:
            r0 = move-exception
            r2 = 5
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            r7.<init>()     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.String r8 = ""
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.StringBuilder r0 = r7.append(r0)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            com.tencent.assistant.utils.a.b r0 = r1.a(r2, r0)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            if (r3 == 0) goto L_0x0210
            r3.close()     // Catch:{ IOException -> 0x05df }
        L_0x0210:
            if (r4 == 0) goto L_0x0215
            r4.close()     // Catch:{ IOException -> 0x05e5 }
        L_0x0215:
            if (r5 == 0) goto L_0x021a
            r5.close()     // Catch:{ IOException -> 0x05eb }
        L_0x021a:
            com.tencent.assistant.utils.FileUtil.rename(r6, r15)     // Catch:{ Exception -> 0x021f }
            goto L_0x0043
        L_0x021f:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.assistant.utils.a.b r0 = r1.a(r11, r0)
            goto L_0x0043
        L_0x023c:
            r0 = move-exception
            r2 = 6
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            r7.<init>()     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.String r8 = ""
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.StringBuilder r0 = r7.append(r0)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            com.tencent.assistant.utils.a.b r0 = r1.a(r2, r0)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            if (r3 == 0) goto L_0x025a
            r3.close()     // Catch:{ IOException -> 0x05f1 }
        L_0x025a:
            if (r4 == 0) goto L_0x025f
            r4.close()     // Catch:{ IOException -> 0x05f7 }
        L_0x025f:
            if (r5 == 0) goto L_0x0264
            r5.close()     // Catch:{ IOException -> 0x05fd }
        L_0x0264:
            com.tencent.assistant.utils.FileUtil.rename(r6, r15)     // Catch:{ Exception -> 0x0269 }
            goto L_0x0043
        L_0x0269:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.assistant.utils.a.b r0 = r1.a(r11, r0)
            goto L_0x0043
        L_0x0286:
            r0 = move-exception
            r2 = 11
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            r7.<init>()     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.String r8 = ""
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.StringBuilder r0 = r7.append(r0)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            com.tencent.assistant.utils.a.b r0 = r1.a(r2, r0)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            if (r3 == 0) goto L_0x02a5
            r3.close()     // Catch:{ IOException -> 0x0603 }
        L_0x02a5:
            if (r4 == 0) goto L_0x02aa
            r4.close()     // Catch:{ IOException -> 0x0609 }
        L_0x02aa:
            if (r5 == 0) goto L_0x02af
            r5.close()     // Catch:{ IOException -> 0x060f }
        L_0x02af:
            com.tencent.assistant.utils.FileUtil.rename(r6, r15)     // Catch:{ Exception -> 0x02b4 }
            goto L_0x0043
        L_0x02b4:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.assistant.utils.a.b r0 = r1.a(r11, r0)
            goto L_0x0043
        L_0x02d1:
            java.util.Iterator r7 = r2.iterator()     // Catch:{ Exception -> 0x02ec }
        L_0x02d5:
            boolean r0 = r7.hasNext()     // Catch:{ Exception -> 0x02ec }
            if (r0 == 0) goto L_0x0376
            java.lang.Object r0 = r7.next()     // Catch:{ Exception -> 0x02ec }
            com.tencent.assistant.utils.a.f r0 = (com.tencent.assistant.utils.a.f) r0     // Catch:{ Exception -> 0x02ec }
            r8 = 0
            r0.c = r8     // Catch:{ Exception -> 0x02ec }
            boolean r8 = r0.r     // Catch:{ Exception -> 0x02ec }
            if (r8 == 0) goto L_0x0337
            r12.a(r0, r5, r4)     // Catch:{ Exception -> 0x02ec }
            goto L_0x02d5
        L_0x02ec:
            r0 = move-exception
            r2 = 10
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            r7.<init>()     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.String r8 = ""
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.StringBuilder r0 = r7.append(r0)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            com.tencent.assistant.utils.a.b r0 = r1.a(r2, r0)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            if (r3 == 0) goto L_0x030b
            r3.close()     // Catch:{ IOException -> 0x0615 }
        L_0x030b:
            if (r4 == 0) goto L_0x0310
            r4.close()     // Catch:{ IOException -> 0x061b }
        L_0x0310:
            if (r5 == 0) goto L_0x0315
            r5.close()     // Catch:{ IOException -> 0x0621 }
        L_0x0315:
            com.tencent.assistant.utils.FileUtil.rename(r6, r15)     // Catch:{ Exception -> 0x031a }
            goto L_0x0043
        L_0x031a:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.assistant.utils.a.b r0 = r1.a(r11, r0)
            goto L_0x0043
        L_0x0337:
            boolean r0 = r12.a(r0, r3, r9, r4)     // Catch:{ Exception -> 0x02ec }
            if (r0 != 0) goto L_0x02d5
            r0 = 10
            java.lang.String r2 = "writeLocalFileHeaderAndDataV2 method fail"
            com.tencent.assistant.utils.a.b r0 = r1.a(r0, r2)     // Catch:{ Exception -> 0x02ec }
            if (r3 == 0) goto L_0x034a
            r3.close()     // Catch:{ IOException -> 0x0627 }
        L_0x034a:
            if (r4 == 0) goto L_0x034f
            r4.close()     // Catch:{ IOException -> 0x062d }
        L_0x034f:
            if (r5 == 0) goto L_0x0354
            r5.close()     // Catch:{ IOException -> 0x0633 }
        L_0x0354:
            com.tencent.assistant.utils.FileUtil.rename(r6, r15)     // Catch:{ Exception -> 0x0359 }
            goto L_0x0043
        L_0x0359:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.assistant.utils.a.b r0 = r1.a(r11, r0)
            goto L_0x0043
        L_0x0376:
            int r7 = r4.size()     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ IOException -> 0x038e }
        L_0x037e:
            boolean r0 = r2.hasNext()     // Catch:{ IOException -> 0x038e }
            if (r0 == 0) goto L_0x03d8
            java.lang.Object r0 = r2.next()     // Catch:{ IOException -> 0x038e }
            com.tencent.assistant.utils.a.f r0 = (com.tencent.assistant.utils.a.f) r0     // Catch:{ IOException -> 0x038e }
            r0.a(r4)     // Catch:{ IOException -> 0x038e }
            goto L_0x037e
        L_0x038e:
            r0 = move-exception
            r2 = 7
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            r7.<init>()     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.String r8 = ""
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.StringBuilder r0 = r7.append(r0)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            com.tencent.assistant.utils.a.b r0 = r1.a(r2, r0)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            if (r3 == 0) goto L_0x03ac
            r3.close()     // Catch:{ IOException -> 0x0639 }
        L_0x03ac:
            if (r4 == 0) goto L_0x03b1
            r4.close()     // Catch:{ IOException -> 0x063f }
        L_0x03b1:
            if (r5 == 0) goto L_0x03b6
            r5.close()     // Catch:{ IOException -> 0x0645 }
        L_0x03b6:
            com.tencent.assistant.utils.FileUtil.rename(r6, r15)     // Catch:{ Exception -> 0x03bb }
            goto L_0x0043
        L_0x03bb:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.assistant.utils.a.b r0 = r1.a(r11, r0)
            goto L_0x0043
        L_0x03d8:
            int r0 = r5.readInt()     // Catch:{ Exception -> 0x045e }
            int r2 = r4.size()     // Catch:{ Exception -> 0x045e }
            int r2 = r2 - r7
            r8 = 1347093766(0x504b0506, float:1.36244163E10)
            if (r0 != r8) goto L_0x040a
            com.tencent.assistant.utils.a.h r0 = new com.tencent.assistant.utils.a.h     // Catch:{ Exception -> 0x045e }
            r0.<init>()     // Catch:{ Exception -> 0x045e }
            r0.a(r5)     // Catch:{ Exception -> 0x045e }
            r0.f = r7     // Catch:{ Exception -> 0x045e }
            r0.e = r2     // Catch:{ Exception -> 0x045e }
            r0.a(r4)     // Catch:{ Exception -> 0x045e }
            if (r3 == 0) goto L_0x03fa
            r3.close()     // Catch:{ IOException -> 0x065d }
        L_0x03fa:
            if (r4 == 0) goto L_0x03ff
            r4.close()     // Catch:{ IOException -> 0x0663 }
        L_0x03ff:
            if (r5 == 0) goto L_0x0404
            r5.close()     // Catch:{ IOException -> 0x0669 }
        L_0x0404:
            com.tencent.assistant.utils.FileUtil.rename(r6, r15)     // Catch:{ Exception -> 0x066f }
            r0 = r1
            goto L_0x0043
        L_0x040a:
            r0 = 8
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x045e }
            r2.<init>()     // Catch:{ Exception -> 0x045e }
            java.lang.String r7 = "patch file size: "
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Exception -> 0x045e }
            long r7 = com.tencent.assistant.utils.a.c.a(r14)     // Catch:{ Exception -> 0x045e }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Exception -> 0x045e }
            java.lang.String r7 = " Signature != EndOfCentralDirRecord.SIGNATURE"
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Exception -> 0x045e }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x045e }
            com.tencent.assistant.utils.a.b r0 = r1.a(r0, r2)     // Catch:{ Exception -> 0x045e }
            if (r3 == 0) goto L_0x0432
            r3.close()     // Catch:{ IOException -> 0x068c }
        L_0x0432:
            if (r4 == 0) goto L_0x0437
            r4.close()     // Catch:{ IOException -> 0x0692 }
        L_0x0437:
            if (r5 == 0) goto L_0x043c
            r5.close()     // Catch:{ IOException -> 0x0698 }
        L_0x043c:
            com.tencent.assistant.utils.FileUtil.rename(r6, r15)     // Catch:{ Exception -> 0x0441 }
            goto L_0x0043
        L_0x0441:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.assistant.utils.a.b r0 = r1.a(r11, r0)
            goto L_0x0043
        L_0x045e:
            r0 = move-exception
            r2 = 9
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            r7.<init>()     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.String r8 = "patch file size: "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            long r8 = com.tencent.assistant.utils.a.c.a(r14)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.String r8 = " e: "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.StringBuilder r0 = r7.append(r0)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            com.tencent.assistant.utils.a.b r0 = r1.a(r2, r0)     // Catch:{ Throwable -> 0x06ba, all -> 0x06a7 }
            if (r3 == 0) goto L_0x048b
            r3.close()     // Catch:{ IOException -> 0x064b }
        L_0x048b:
            if (r4 == 0) goto L_0x0490
            r4.close()     // Catch:{ IOException -> 0x0651 }
        L_0x0490:
            if (r5 == 0) goto L_0x0495
            r5.close()     // Catch:{ IOException -> 0x0657 }
        L_0x0495:
            com.tencent.assistant.utils.FileUtil.rename(r6, r15)     // Catch:{ Exception -> 0x049a }
            goto L_0x0043
        L_0x049a:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.assistant.utils.a.b r0 = r1.a(r11, r0)
            goto L_0x0043
        L_0x04b7:
            r0 = move-exception
            r3 = r2
            r4 = r2
        L_0x04ba:
            r5 = 12
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x06aa }
            r7.<init>()     // Catch:{ all -> 0x06aa }
            java.lang.String r8 = ""
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x06aa }
            java.lang.StringBuilder r0 = r7.append(r0)     // Catch:{ all -> 0x06aa }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x06aa }
            com.tencent.assistant.utils.a.b r0 = r1.a(r5, r0)     // Catch:{ all -> 0x06aa }
            if (r2 == 0) goto L_0x04d8
            r2.close()     // Catch:{ IOException -> 0x0547 }
        L_0x04d8:
            if (r3 == 0) goto L_0x04dd
            r3.close()     // Catch:{ IOException -> 0x054c }
        L_0x04dd:
            if (r4 == 0) goto L_0x04e2
            r4.close()     // Catch:{ IOException -> 0x0551 }
        L_0x04e2:
            com.tencent.assistant.utils.FileUtil.rename(r6, r15)     // Catch:{ Exception -> 0x04e7 }
            goto L_0x0043
        L_0x04e7:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.assistant.utils.a.b r0 = r1.a(r11, r0)
            goto L_0x0043
        L_0x0504:
            r0 = move-exception
            r3 = r2
            r4 = r2
            r5 = r2
        L_0x0508:
            if (r3 == 0) goto L_0x050d
            r3.close()     // Catch:{ IOException -> 0x051b }
        L_0x050d:
            if (r4 == 0) goto L_0x0512
            r4.close()     // Catch:{ IOException -> 0x0520 }
        L_0x0512:
            if (r5 == 0) goto L_0x0517
            r5.close()     // Catch:{ IOException -> 0x0525 }
        L_0x0517:
            com.tencent.assistant.utils.FileUtil.rename(r6, r15)     // Catch:{ Exception -> 0x052a }
            throw r0
        L_0x051b:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x050d
        L_0x0520:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0512
        L_0x0525:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0517
        L_0x052a:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.assistant.utils.a.b r0 = r1.a(r11, r0)
            goto L_0x0043
        L_0x0547:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x04d8
        L_0x054c:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x04dd
        L_0x0551:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x04e2
        L_0x0556:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x0062
        L_0x055c:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x0067
        L_0x0562:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x006c
        L_0x0568:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x0036
        L_0x056e:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x003b
        L_0x0574:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0040
        L_0x057a:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.assistant.utils.a.b r0 = r1.a(r11, r0)
            goto L_0x0043
        L_0x0597:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x00e8
        L_0x059d:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x00ed
        L_0x05a3:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00f2
        L_0x05a9:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x0132
        L_0x05af:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0137
        L_0x05b5:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x013c
        L_0x05bb:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x017c
        L_0x05c1:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0181
        L_0x05c7:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0186
        L_0x05cd:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x01c6
        L_0x05d3:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x01cb
        L_0x05d9:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x01d0
        L_0x05df:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0210
        L_0x05e5:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0215
        L_0x05eb:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x021a
        L_0x05f1:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x025a
        L_0x05f7:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x025f
        L_0x05fd:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0264
        L_0x0603:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x02a5
        L_0x0609:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x02aa
        L_0x060f:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x02af
        L_0x0615:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x030b
        L_0x061b:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0310
        L_0x0621:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0315
        L_0x0627:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x034a
        L_0x062d:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x034f
        L_0x0633:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0354
        L_0x0639:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x03ac
        L_0x063f:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x03b1
        L_0x0645:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x03b6
        L_0x064b:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x048b
        L_0x0651:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0490
        L_0x0657:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0495
        L_0x065d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x03fa
        L_0x0663:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x03ff
        L_0x0669:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0404
        L_0x066f:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.assistant.utils.a.b r0 = r1.a(r11, r0)
            goto L_0x0043
        L_0x068c:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0432
        L_0x0692:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0437
        L_0x0698:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x043c
        L_0x069e:
            r0 = move-exception
            r3 = r2
            r4 = r2
            goto L_0x0508
        L_0x06a3:
            r0 = move-exception
            r3 = r2
            goto L_0x0508
        L_0x06a7:
            r0 = move-exception
            goto L_0x0508
        L_0x06aa:
            r0 = move-exception
            r5 = r4
            r4 = r3
            r3 = r2
            goto L_0x0508
        L_0x06b0:
            r0 = move-exception
            r3 = r2
            r4 = r5
            goto L_0x04ba
        L_0x06b5:
            r0 = move-exception
            r3 = r4
            r4 = r5
            goto L_0x04ba
        L_0x06ba:
            r0 = move-exception
            r2 = r3
            r3 = r4
            r4 = r5
            goto L_0x04ba
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.a.i.a(java.lang.String, java.lang.String, java.lang.String):com.tencent.assistant.utils.a.b");
    }

    private boolean a(f fVar, RandomAccessFile randomAccessFile, k kVar, DataOutputStream dataOutputStream) {
        int i;
        String str = new String(fVar.s, "utf-8");
        f b = kVar.b(str);
        if (b == null) {
            return false;
        }
        fVar.d = b.d;
        fVar.h = b.h;
        fVar.g = b.g;
        fVar.k = b.k;
        fVar.t = b.t;
        fVar.l = b.l;
        fVar.u = b.u;
        j.a(fVar, dataOutputStream);
        int i2 = fVar.h;
        if (i2 > 0) {
            randomAccessFile.seek((long) kVar.c(str));
            while (i2 > 0) {
                if (i2 > 4096) {
                    i = 4096;
                } else {
                    i = i2;
                }
                int read = randomAccessFile.read(this.f1810a, 0, i);
                if (read > 0) {
                    dataOutputStream.write(this.f1810a, 0, read);
                    i2 -= read;
                }
            }
        }
        if (fVar.a()) {
            g.a(fVar, dataOutputStream);
        }
        return true;
    }

    private void a(f fVar, DataInputStream dataInputStream, DataOutputStream dataOutputStream) {
        int i;
        j.a(fVar, dataOutputStream);
        int i2 = fVar.h;
        int i3 = 0;
        while (i2 > 0 && i3 >= 0) {
            if (i2 > 4096) {
                i = 4096;
            } else {
                i = i2;
            }
            i3 = dataInputStream.read(this.f1810a, 0, i);
            if (i3 > 0) {
                dataOutputStream.write(this.f1810a, 0, i3);
                i2 -= i3;
            }
        }
        if (fVar.a()) {
            g.a(fVar, dataOutputStream);
        }
    }
}
