package com.tencent.assistant.utils;

import android.util.Log;
import com.qq.AppService.AstApp;
import com.tencent.assistant.localres.model.LocalApkInfo;
import java.io.File;

/* compiled from: ProGuard */
public class bg {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1833a = AstApp.i().getPackageName();
    private static final String b = (AstApp.i().getApplicationInfo().dataDir + "/lib");

    public static void a(boolean z) {
        File file = new File(b);
        if (!file.exists()) {
            file.mkdirs();
            Log.w("BaoSo", "not exist lib path:" + b);
        } else if (!z) {
            return;
        }
        LocalApkInfo b2 = e.b(f1833a);
        if (b2 != null) {
            File file2 = new File(b2.mLocalFilePath);
            if (file2.exists() && file2.canRead()) {
                FileUtil.extractSoToFilePath(file2.getAbsolutePath(), b);
            }
        }
    }
}
