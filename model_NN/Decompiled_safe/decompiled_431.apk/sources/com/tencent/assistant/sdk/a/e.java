package com.tencent.assistant.sdk.a;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* compiled from: ProGuard */
public abstract class e extends Binder implements d {
    public e() {
        attachInterface(this, "com.tencent.assistant.sdk.remote.SDKActionCallback");
    }

    public static d a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.tencent.assistant.sdk.remote.SDKActionCallback");
        if (queryLocalInterface == null || !(queryLocalInterface instanceof d)) {
            return new f(iBinder);
        }
        return (d) queryLocalInterface;
    }

    public IBinder asBinder() {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        switch (i) {
            case 1:
                parcel.enforceInterface("com.tencent.assistant.sdk.remote.SDKActionCallback");
                a(parcel.createByteArray());
                return true;
            case 1598968902:
                parcel2.writeString("com.tencent.assistant.sdk.remote.SDKActionCallback");
                return true;
            default:
                return super.onTransact(i, parcel, parcel2, i2);
        }
    }
}
