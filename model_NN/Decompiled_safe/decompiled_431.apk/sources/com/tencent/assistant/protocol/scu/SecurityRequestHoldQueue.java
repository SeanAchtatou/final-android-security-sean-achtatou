package com.tencent.assistant.protocol.scu;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.protocol.a.e;
import com.tencent.assistant.protocol.jce.StatCSChannelData;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.PriorityBlockingQueue;

/* compiled from: ProGuard */
public class SecurityRequestHoldQueue extends PriorityBlockingQueue<m> implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private p f1623a;

    public SecurityRequestHoldQueue() {
        super(10, new o());
    }

    public void a(p pVar) {
        this.f1623a = pVar;
    }

    public void run() {
        while (true) {
            try {
                Thread.sleep(15000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            a();
        }
    }

    private void a() {
        ArrayList arrayList = new ArrayList();
        synchronized (this) {
            Iterator it = iterator();
            while (it.hasNext()) {
                m mVar = (m) it.next();
                if (System.currentTimeMillis() - mVar.e > ((long) e.a().q())) {
                    arrayList.add(mVar);
                }
            }
            if (arrayList.size() > 0) {
                removeAll(arrayList);
            }
        }
        if (arrayList.size() > 0 && this.f1623a != null) {
            this.f1623a.a(arrayList);
        }
    }

    public boolean a(m mVar, byte b) {
        boolean add = super.add(mVar);
        if (e.a().n()) {
            b(mVar, b);
        }
        return add;
    }

    private void b(m mVar, byte b) {
        if (mVar != null) {
            StatCSChannelData statCSChannelData = new StatCSChannelData();
            statCSChannelData.b = 2;
            statCSChannelData.c = 2;
            statCSChannelData.d = 4;
            statCSChannelData.f = mVar.f;
            StringBuilder sb = new StringBuilder();
            sb.append("holdType:").append((int) b).append(";");
            if (mVar.f1632a != null) {
                for (JceStruct a2 : mVar.f1632a) {
                    sb.append("cmdId:").append(k.a(a2)).append(";");
                }
            }
            statCSChannelData.g = sb.toString();
            e.a().a(statCSChannelData);
        }
    }
}
