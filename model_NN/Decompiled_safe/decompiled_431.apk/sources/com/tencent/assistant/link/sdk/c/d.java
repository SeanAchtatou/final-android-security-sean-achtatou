package com.tencent.assistant.link.sdk.c;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

/* compiled from: ProGuard */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private SQLiteDatabase f801a;

    public d(SQLiteDatabase sQLiteDatabase) {
        this.f801a = sQLiteDatabase;
    }

    public long a(String str, String str2, ContentValues contentValues) {
        try {
            return this.f801a.insert(str, str2, contentValues);
        } catch (Throwable th) {
            th.printStackTrace();
            return -1;
        }
    }

    public int a(String str, String str2, String[] strArr) {
        try {
            return this.f801a.delete(str, str2, strArr);
        } catch (Throwable th) {
            th.printStackTrace();
            return 0;
        }
    }

    public int a(String str, ContentValues contentValues, String str2, String[] strArr) {
        try {
            return this.f801a.update(str, contentValues, str2, strArr);
        } catch (Throwable th) {
            th.printStackTrace();
            return 0;
        }
    }
}
