package com.tencent.assistant.manager.webview.js;

import android.net.Uri;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class f extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Uri f927a;
    final /* synthetic */ String b;
    final /* synthetic */ int c;
    final /* synthetic */ String d;
    final /* synthetic */ JsBridge e;

    f(JsBridge jsBridge, Uri uri, String str, int i, String str2) {
        this.e = jsBridge;
        this.f927a = uri;
        this.b = str;
        this.c = i;
        this.d = str2;
    }

    public void onLeftBtnClick() {
    }

    public void onRightBtnClick() {
        TemporaryThreadManager.get().start(new g(this));
    }

    public void onCancell() {
    }
}
