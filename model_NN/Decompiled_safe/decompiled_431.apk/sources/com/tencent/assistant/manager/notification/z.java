package com.tencent.assistant.manager.notification;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.widget.RemoteViews;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.notification.a.a.c;
import com.tencent.assistant.module.update.t;
import com.tencent.assistant.protocol.jce.DownloadSubscriptionInfo;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bo;
import com.tencent.assistant.utils.r;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import java.util.Calendar;

/* compiled from: ProGuard */
public class z {
    private static z b = null;

    /* renamed from: a  reason: collision with root package name */
    private int[] f900a = {10, 23};

    public static synchronized z a() {
        z zVar;
        synchronized (z.class) {
            if (b == null) {
                b = new z();
            }
            zVar = b;
        }
        return zVar;
    }

    private z() {
    }

    public synchronized void b() {
        boolean z = false;
        synchronized (this) {
            XLog.d("SubscriptionDownload", "showCahcedNotification");
            String ae = m.a().ae();
            if (!TextUtils.isEmpty(ae)) {
                z = true;
            }
            if (!z) {
                XLog.d("SubscriptionDownload", "has no cache ticket!");
            } else {
                Calendar instance = Calendar.getInstance();
                instance.set(11, this.f900a[0]);
                instance.set(12, 2);
                instance.set(13, 0);
                instance.set(14, 0);
                if (Math.abs(System.currentTimeMillis() - instance.getTimeInMillis()) < 120000 || !z) {
                    XLog.d("SubscriptionDownload", "Will not do any scan push!");
                } else {
                    XLog.d("SubscriptionDownload", "show cache notification !");
                    XLog.d("SubscriptionDownload", "showCahcedNotification cachedTicket = " + ae);
                    if (!a(ae)) {
                        m.a().h(Constants.STR_EMPTY);
                    }
                }
            }
        }
    }

    public synchronized boolean a(String str) {
        boolean z = false;
        synchronized (this) {
            XLog.d("SubscriptionDownload", "showCahcedNotification cachedTicket = " + str);
            if (!TextUtils.isEmpty(str)) {
                DownloadInfo c = DownloadProxy.a().c(str);
                if (c != null && (c.downloadState == SimpleDownloadInfo.DownloadState.SUCC || c.downloadState == SimpleDownloadInfo.DownloadState.INSTALLED)) {
                    if (c()) {
                        XLog.d("SubscriptionDownload", "Time OK");
                        a(c);
                    } else {
                        m.a().h(str);
                        XLog.d("SubscriptionDownload", "Time Noet OK cacheTicket = " + str);
                        a(d());
                    }
                    z = true;
                }
            }
        }
        return z;
    }

    private boolean c() {
        long currentTimeMillis = System.currentTimeMillis();
        Calendar instance = Calendar.getInstance();
        instance.set(11, this.f900a[0] - 1);
        instance.set(12, 58);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar instance2 = Calendar.getInstance();
        instance2.set(11, this.f900a[1]);
        instance2.set(12, 2);
        instance2.set(13, 0);
        instance2.set(14, 0);
        if (currentTimeMillis < instance.getTimeInMillis() || currentTimeMillis > instance2.getTimeInMillis()) {
            return false;
        }
        return true;
    }

    private long d() {
        boolean z = true;
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(System.currentTimeMillis());
        int i = instance.get(11);
        if (i < this.f900a[1]) {
            if (i < this.f900a[0]) {
                z = false;
            } else {
                z = false;
            }
        }
        Calendar instance2 = Calendar.getInstance();
        instance2.set(11, this.f900a[0]);
        instance2.set(12, 2);
        instance2.set(13, 0);
        instance2.set(14, 0);
        if (!z) {
            return instance2.getTimeInMillis();
        }
        return instance2.getTimeInMillis() + TesDownloadConfig.TES_CONFIG_CHECK_PERIOD;
    }

    public void a(long j) {
        m.a().j(j);
    }

    private void a(DownloadInfo downloadInfo) {
        if (downloadInfo != null) {
            c cVar = new c(downloadInfo.iconUrl, 1);
            cVar.a(new aa(this, downloadInfo));
            cVar.b();
        }
    }

    public Notification a(String str, Bitmap bitmap) {
        DownloadSubscriptionInfo a2;
        DownloadInfo c = DownloadProxy.a().c(str);
        if (c == null || (a2 = t.a().a(c.uiType, c.packageName, c.versionCode)) == null || TextUtils.isEmpty(a2.f1221a) || TextUtils.isEmpty(a2.b)) {
            return null;
        }
        XLog.d("SubscriptionDownload", "subscriptionInfo ＝ " + a2.toString());
        return y.a(AstApp.i(), R.drawable.logo32, a(c, bitmap, a2), a(c, a2), System.currentTimeMillis(), c(c, a2), null, true, false);
    }

    private RemoteViews a(DownloadInfo downloadInfo, Bitmap bitmap, DownloadSubscriptionInfo downloadSubscriptionInfo) {
        if (downloadInfo == null) {
            return null;
        }
        RemoteViews remoteViews = new RemoteViews(AstApp.i().getPackageName(), (int) R.layout.notification_card_subscription_download);
        r rVar = new r(AstApp.i(), true);
        Integer c = rVar.c();
        if (c != null) {
            remoteViews.setTextColor(R.id.title, c.intValue());
        }
        Integer a2 = rVar.a();
        if (a2 != null) {
            remoteViews.setTextColor(R.id.content, a2.intValue());
        }
        if (bitmap == null || bitmap.isRecycled()) {
            remoteViews.setImageViewResource(R.id.big_icon, R.drawable.logo72);
        } else {
            remoteViews.setImageViewBitmap(R.id.big_icon, bitmap);
        }
        remoteViews.setFloat(R.id.title, "setTextSize", rVar.d());
        remoteViews.setFloat(R.id.content, "setTextSize", rVar.b());
        remoteViews.setTextViewText(R.id.title, a(downloadInfo, downloadSubscriptionInfo));
        remoteViews.setTextViewText(R.id.content, b(downloadInfo, downloadSubscriptionInfo));
        RemoteViews remoteViews2 = new RemoteViews(AstApp.i().getPackageName(), (int) R.layout.notification_card1_right6);
        if (a2 != null) {
            remoteViews2.setTextColor(R.id.timeText, a2.intValue());
        }
        remoteViews2.setFloat(R.id.timeText, "setTextSize", rVar.b());
        remoteViews2.setTextViewText(R.id.timeText, bo.d(Long.valueOf(System.currentTimeMillis())));
        remoteViews.removeAllViews(R.id.rightContainer);
        remoteViews.addView(R.id.rightContainer, remoteViews2);
        remoteViews.setViewVisibility(R.id.rightContainer, 0);
        if (r.d() < 20) {
            return remoteViews;
        }
        remoteViews.setInt(R.id.root, "setBackgroundResource", R.color.notification_bg_50);
        return remoteViews;
    }

    private String a(DownloadInfo downloadInfo, DownloadSubscriptionInfo downloadSubscriptionInfo) {
        if (downloadSubscriptionInfo != null) {
            return downloadSubscriptionInfo.f1221a;
        }
        return Constants.STR_EMPTY;
    }

    private String b(DownloadInfo downloadInfo, DownloadSubscriptionInfo downloadSubscriptionInfo) {
        if (downloadSubscriptionInfo != null) {
            return downloadSubscriptionInfo.b;
        }
        return Constants.STR_EMPTY;
    }

    private PendingIntent c(DownloadInfo downloadInfo, DownloadSubscriptionInfo downloadSubscriptionInfo) {
        Intent intent = new Intent(AstApp.i(), NotificationService.class);
        intent.putExtra("notification_id", 121);
        if (downloadSubscriptionInfo != null) {
            intent.putExtra("notification_action", downloadSubscriptionInfo.d);
        }
        return PendingIntent.getService(AstApp.i(), 121, intent, 268435456);
    }
}
