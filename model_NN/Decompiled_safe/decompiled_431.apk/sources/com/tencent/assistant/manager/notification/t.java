package com.tencent.assistant.manager.notification;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/* compiled from: ProGuard */
public class t {

    /* renamed from: a  reason: collision with root package name */
    private Map<String, String> f895a = Collections.synchronizedMap(new LinkedHashMap());

    public int a() {
        return this.f895a.size();
    }

    public boolean b() {
        return this.f895a.isEmpty();
    }

    public void c() {
        if (this.f895a != null) {
            this.f895a.clear();
        }
    }

    public void a(String str, String str2) {
        this.f895a.put(str, str2);
    }

    public boolean a(String str) {
        return this.f895a.remove(str) != null;
    }

    public String d() {
        if (this.f895a == null || this.f895a.isEmpty()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        ArrayList arrayList = new ArrayList(this.f895a.values());
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            sb.append((String) arrayList.get(i));
            if (i != size - 1) {
                sb.append("、");
            }
        }
        return sb.toString();
    }

    public String e() {
        if (this.f895a.size() == 1) {
            return this.f895a.keySet().iterator().next();
        }
        return null;
    }
}
