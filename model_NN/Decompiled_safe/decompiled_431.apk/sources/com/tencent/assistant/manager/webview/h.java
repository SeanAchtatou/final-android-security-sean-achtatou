package com.tencent.assistant.manager.webview;

import android.text.TextUtils;
import com.tencent.smtt.sdk.WebView;

/* compiled from: ProGuard */
class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f920a;
    final /* synthetic */ WebView b;
    final /* synthetic */ f c;

    h(f fVar, String str, WebView webView) {
        this.c = fVar;
        this.f920a = str;
        this.b = webView;
    }

    public void run() {
        if (!TextUtils.isEmpty(this.f920a)) {
            try {
                if (this.b != null) {
                    this.b.loadUrl(this.f920a);
                }
            } catch (Exception e) {
            }
        }
    }
}
