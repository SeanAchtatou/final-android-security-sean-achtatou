package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.STPageInfo;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
public abstract class SearchSmartCardBaseItem extends ISmartcard {
    protected long i = 0;
    protected View.OnClickListener j = new x(this);
    protected View.OnClickListener k = new y(this);

    /* access modifiers changed from: protected */
    public abstract String c();

    /* access modifiers changed from: protected */
    public abstract int d();

    /* access modifiers changed from: protected */
    public abstract String e();

    /* access modifiers changed from: protected */
    public abstract long f();

    /* access modifiers changed from: protected */
    public abstract int g();

    public SearchSmartCardBaseItem(Context context) {
        super(context);
    }

    public SearchSmartCardBaseItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SearchSmartCardBaseItem(Context context, n nVar, as asVar) {
        super(context, nVar, asVar, null);
        a();
    }

    public void a(long j2) {
        this.i = j2;
    }

    /* access modifiers changed from: protected */
    public void h() {
        if (this.e != null) {
            this.e.a(this.d.j, this.d.k);
        }
        a(c(), 100, this.d.s, -1);
    }

    /* access modifiers changed from: protected */
    public int b(int i2) {
        if (this.f1693a instanceof BaseActivity) {
            return ((BaseActivity) this.f1693a).f();
        }
        return STConst.ST_PAGE_SEARCH_RESULT_ALL;
    }

    /* access modifiers changed from: protected */
    public String c(int i2) {
        return this.d.k + "||" + this.d.j + "|" + i2;
    }

    /* access modifiers changed from: protected */
    public void a(String str, int i2, byte[] bArr, long j2) {
        STInfoV2 a2 = a(str, i2);
        if (a2 == null) {
            return;
        }
        if (a2.scene != g()) {
            XLog.d("SearchSmartCardBaseItem", "stInfo.scene != getScene(), return, report nothing!");
            return;
        }
        if (this.d != null) {
            a2.pushInfo = c(0);
        }
        if (j2 > 0) {
            a2.appId = j2;
        }
        a2.resourceType = d();
        a2.extraData = e();
        a2.searchId = f();
        a2.recommendId = bArr;
        l.a(a2);
    }

    /* access modifiers changed from: protected */
    public STInfoV2 a(String str, int i2) {
        STPageInfo sTPageInfo;
        if (this.f1693a instanceof BaseActivity) {
            sTPageInfo = ((BaseActivity) this.f1693a).n();
        } else {
            sTPageInfo = null;
        }
        if (sTPageInfo == null) {
            return null;
        }
        STInfoV2 sTInfoV2 = new STInfoV2(sTPageInfo.f2060a, str, sTPageInfo.c, a.b(sTPageInfo.b, STConst.ST_STATUS_DEFAULT), i2);
        sTInfoV2.pushInfo = c(0);
        return sTInfoV2;
    }
}
