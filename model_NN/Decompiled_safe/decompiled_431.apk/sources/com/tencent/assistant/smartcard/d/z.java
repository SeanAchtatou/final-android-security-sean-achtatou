package com.tencent.assistant.smartcard.d;

import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.SmartCardTemplate;
import com.tencent.assistant.protocol.jce.SmartCardTemplateItem;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class z extends n {

    /* renamed from: a  reason: collision with root package name */
    public List<y> f1769a;
    private boolean b = true;
    private int c;
    private String d;

    public boolean a(int i, int i2, SmartCardTemplate smartCardTemplate) {
        this.j = i;
        this.l = smartCardTemplate.f1523a;
        this.n = smartCardTemplate.d;
        this.p = smartCardTemplate.e;
        this.o = smartCardTemplate.f;
        this.c = smartCardTemplate.g;
        this.d = smartCardTemplate.h;
        this.b = smartCardTemplate.i;
        this.k = smartCardTemplate.j;
        if (this.f1769a == null) {
            this.f1769a = new ArrayList();
        } else {
            this.f1769a.clear();
        }
        if (smartCardTemplate.b == null || smartCardTemplate.b.size() <= 0) {
            XLog.d("SmartCard", "smartCardTemplate.items is null or zero.");
            return false;
        } else if (i2 < 1 || i2 > 5) {
            return false;
        } else {
            if (i2 == 2 && smartCardTemplate.a().size() < 2) {
                XLog.d("SmartCard", "template type is MATRIX BUT size is:" + smartCardTemplate.a().size());
                return false;
            } else if (i2 != 3 || smartCardTemplate.a().size() >= 2) {
                Iterator<SmartCardTemplateItem> it = smartCardTemplate.b.iterator();
                while (it.hasNext()) {
                    SmartCardTemplateItem next = it.next();
                    y yVar = new y();
                    yVar.b = next.b;
                    yVar.c = next.c;
                    yVar.f1768a = k.a(next.f1525a);
                    if (yVar.f1768a != null) {
                        k.a(yVar.f1768a);
                        if (next.f1525a != null) {
                            if (next.f1525a.h != null) {
                                int length = next.f1525a.h.length;
                            }
                            if (next.f1525a.h != null) {
                                new String(next.f1525a.h);
                            }
                        }
                    }
                    this.f1769a.add(yVar);
                }
                return true;
            } else {
                XLog.d("SmartCard", "template type is ONE_LINE BUT size is:" + smartCardTemplate.a().size());
                return false;
            }
        }
    }

    public List<y> a() {
        return this.f1769a;
    }

    public boolean b() {
        return this.b;
    }

    public int c() {
        return this.c;
    }

    public List<Long> d() {
        ArrayList arrayList = new ArrayList();
        if (this.f1769a != null && this.f1769a.size() > 0) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.f1769a.size()) {
                    break;
                }
                arrayList.add(Long.valueOf(this.f1769a.get(i2).f1768a.f938a));
                i = i2 + 1;
            }
        }
        return arrayList;
    }

    public String d_() {
        return j() + "_" + c();
    }

    public void a(List<Long> list) {
        if (list != null && list.size() != 0 && this.f1769a != null && this.f1769a.size() != 0) {
            ArrayList arrayList = new ArrayList();
            for (y next : this.f1769a) {
                if (list.contains(Long.valueOf(next.f1768a.f938a))) {
                    arrayList.add(next);
                }
            }
            this.f1769a.removeAll(arrayList);
        }
    }

    public String toString() {
        return "templateType:" + this.c + "," + super.toString();
    }
}
