package com.tencent.assistant.smartcard.b;

import com.tencent.assistant.protocol.jce.SmartCardWrapper;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.f.a;
import com.tencent.assistant.utils.XLog;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public abstract class c {

    /* renamed from: a  reason: collision with root package name */
    protected Map<Integer, List<n>> f1679a = Collections.synchronizedMap(new HashMap());

    /* access modifiers changed from: protected */
    public abstract int a();

    public void a(List<SmartCardWrapper> list) {
        a(list, true);
    }

    public void b(List<SmartCardWrapper> list) {
        a(list, false);
    }

    public synchronized void b() {
        this.f1679a.clear();
    }

    private synchronized void a(List<SmartCardWrapper> list, boolean z) {
        int i;
        if (z) {
            this.f1679a.clear();
        }
        if (list != null && list.size() > 0) {
            for (int i2 = 0; i2 < list.size(); i2++) {
                SmartCardWrapper smartCardWrapper = list.get(i2);
                if (a(smartCardWrapper)) {
                    int i3 = smartCardWrapper.f1530a;
                    XLog.d("SmartCard", "start.....select position:" + i3);
                    List<n> a2 = a.a(smartCardWrapper.b, a());
                    StringBuilder append = new StringBuilder().append("finish.....select position:").append(i3).append(",models size:");
                    if (a2 != null) {
                        i = a2.size();
                    } else {
                        i = 0;
                    }
                    XLog.d("SmartCard", append.append(i).toString());
                    if (a2 != null && a2.size() > 0) {
                        this.f1679a.put(Integer.valueOf(i3), a2);
                    }
                }
            }
        }
    }

    public boolean a(SmartCardWrapper smartCardWrapper) {
        if (smartCardWrapper == null || smartCardWrapper.b == null || smartCardWrapper.b.size() <= 0) {
            return false;
        }
        return true;
    }

    public synchronized List<n> a(int i) {
        return this.f1679a.get(Integer.valueOf(i));
    }
}
