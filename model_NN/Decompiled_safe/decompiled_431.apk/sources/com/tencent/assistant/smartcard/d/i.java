package com.tencent.assistant.smartcard.d;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.Template3App;

/* compiled from: ProGuard */
public class i {

    /* renamed from: a  reason: collision with root package name */
    public SimpleAppModel f1755a;
    public String b;
    public String c;
    public String d;
    public String e;

    public boolean a(Template3App template3App) {
        if (template3App == null) {
            return false;
        }
        this.f1755a = k.a(template3App.f1579a);
        this.b = template3App.b;
        this.c = template3App.c;
        this.d = template3App.d;
        this.e = template3App.e;
        return true;
    }
}
