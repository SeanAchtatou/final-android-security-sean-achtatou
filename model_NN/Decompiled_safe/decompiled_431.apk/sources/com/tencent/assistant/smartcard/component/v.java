package com.tencent.assistant.smartcard.component;

import android.widget.ImageView;
import com.tencent.assistantv2.component.k;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.a;

/* compiled from: ProGuard */
class v extends k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchSmartCardAppListItem f1739a;

    v(SearchSmartCardAppListItem searchSmartCardAppListItem) {
        this.f1739a = searchSmartCardAppListItem;
    }

    public void a(DownloadInfo downloadInfo) {
        a.a().a(downloadInfo);
        com.tencent.assistant.utils.a.a((ImageView) this.f1739a.findViewWithTag(downloadInfo.downloadTicket));
    }
}
