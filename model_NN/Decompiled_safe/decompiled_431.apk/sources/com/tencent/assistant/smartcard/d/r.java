package com.tencent.assistant.smartcard.d;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.SmartCardPlayerShow;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class r extends n {

    /* renamed from: a  reason: collision with root package name */
    public String f1761a;
    public boolean b;
    public long c;
    public SimpleAppModel d;
    public String e;

    public List<Long> d() {
        if (this.d == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(Long.valueOf(this.d.f938a));
        return arrayList;
    }

    public void a(List<Long> list) {
        if (list != null && list.size() != 0 && this.d != null) {
            for (Long longValue : list) {
                if (longValue.longValue() == this.d.f938a) {
                    this.d = null;
                    return;
                }
            }
        }
    }

    public void a(SmartCardPlayerShow smartCardPlayerShow, int i) {
        if (smartCardPlayerShow != null) {
            this.j = i;
            this.d = k.a(smartCardPlayerShow.f1515a);
            this.f1761a = smartCardPlayerShow.b;
            this.e = smartCardPlayerShow.c;
            this.c = smartCardPlayerShow.e;
        }
    }
}
