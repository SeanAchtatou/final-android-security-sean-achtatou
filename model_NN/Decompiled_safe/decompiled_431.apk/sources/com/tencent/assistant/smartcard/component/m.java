package com.tencent.assistant.smartcard.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class m extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f1730a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ NormalSmartcardAppListItem c;

    m(NormalSmartcardAppListItem normalSmartcardAppListItem, SimpleAppModel simpleAppModel, STInfoV2 sTInfoV2) {
        this.c = normalSmartcardAppListItem;
        this.f1730a = simpleAppModel;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.c.f1693a, AppDetailActivityV5.class);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.c.c(this.c.p));
        intent.putExtra("simpleModeInfo", this.f1730a);
        this.c.f1693a.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (this.b != null) {
            this.b.actionId = 200;
            this.b.updateStatusToDetail(this.f1730a);
        }
        return this.b;
    }
}
