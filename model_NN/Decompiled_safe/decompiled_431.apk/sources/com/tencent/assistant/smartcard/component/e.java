package com.tencent.assistant.smartcard.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.smartcard.d.r;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class e extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ r f1722a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ NormalSmartCardPlayerSHowItem c;

    e(NormalSmartCardPlayerSHowItem normalSmartCardPlayerSHowItem, r rVar, STInfoV2 sTInfoV2) {
        this.c = normalSmartCardPlayerSHowItem;
        this.f1722a = rVar;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.c.f1693a, AppDetailActivityV5.class);
        intent.putExtra("simpleModeInfo", this.f1722a.d);
        this.c.f1693a.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (this.b != null) {
            this.b.actionId = 200;
            this.b.updateStatusToDetail(this.f1722a.d);
        }
        return this.b;
    }
}
