package com.tencent.assistant.smartcard.c;

import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.s;
import com.tencent.assistant.smartcard.d.v;
import com.tencent.assistant.smartcard.d.w;
import com.tencent.assistant.smartcard.d.x;
import java.util.List;

/* compiled from: ProGuard */
public class m extends z {
    public boolean a(n nVar, List<Long> list) {
        if (nVar == null || nVar.j != 17) {
            return false;
        }
        return a((s) nVar, (w) this.f1692a.get(Integer.valueOf(nVar.l())), (x) this.b.get(Integer.valueOf(nVar.l())), list);
    }

    private boolean a(s sVar, w wVar, x xVar, List<Long> list) {
        if (xVar == null) {
            return false;
        }
        sVar.a(list);
        if (sVar.v) {
            return false;
        }
        if (wVar == null) {
            wVar = new w();
            wVar.f = sVar.k;
            wVar.e = sVar.j;
            this.f1692a.put(Integer.valueOf(sVar.l()), wVar);
        }
        if (wVar.b >= xVar.c) {
            a(sVar.t, sVar.k + "||" + sVar.j + "|" + 1, sVar.j);
            return false;
        } else if (wVar.f1766a < xVar.f1767a) {
            return true;
        } else {
            a(sVar.t, sVar.k + "||" + sVar.j + "|" + 2, sVar.j);
            return false;
        }
    }

    public void a(v vVar) {
        w wVar;
        int i;
        if (vVar != null) {
            w wVar2 = (w) this.f1692a.get(Integer.valueOf(vVar.a()));
            if (wVar2 == null) {
                w wVar3 = new w();
                wVar3.e = vVar.f1765a;
                wVar3.f = vVar.b;
                wVar = wVar3;
            } else {
                wVar = wVar2;
            }
            if (vVar.d) {
                wVar.d = true;
            }
            if (vVar.c) {
                if (a(vVar.e * 1000)) {
                    wVar.f1766a++;
                }
                x xVar = (x) this.b.get(Integer.valueOf(vVar.f1765a));
                if (xVar != null) {
                    i = xVar.i;
                } else {
                    i = 7;
                }
                if (a(vVar.e * 1000, (long) i)) {
                    wVar.b++;
                }
                wVar.c++;
            }
            this.f1692a.put(Integer.valueOf(wVar.a()), wVar);
        }
    }

    public void a(x xVar) {
        this.b.put(Integer.valueOf(xVar.a()), xVar);
    }

    public void a(n nVar) {
        if (nVar != null && nVar.j == 17) {
            s sVar = (s) nVar;
            x xVar = (x) this.b.get(Integer.valueOf(sVar.l()));
            if (xVar != null) {
                sVar.q = xVar.d;
                sVar.h = xVar.b;
                sVar.g = xVar.f1767a;
            }
        }
    }
}
