package com.tencent.cloud.b;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.model.AppGroupInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.AppGroup;
import com.tencent.assistant.protocol.jce.GetSubjectRequest;
import com.tencent.assistant.protocol.jce.GetSubjectResponse;
import com.tencent.cloud.b.a.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class o extends BaseEngine<b> {

    /* renamed from: a  reason: collision with root package name */
    private byte[] f2247a = null;
    private boolean b = false;
    private long c = -1;
    private int d = -1;
    private int e = -1;
    private String f = null;
    private List<SimpleAppModel> g = new ArrayList();
    private com.tencent.assistant.model.b h = new com.tencent.assistant.model.b();

    public o() {
    }

    public o(int i, String str) {
        this.e = i;
        this.f = str;
    }

    public int a() {
        if (this.d > 0) {
            cancel(this.d);
        }
        this.d = d();
        return this.d;
    }

    public int b() {
        if (this.f2247a == null || this.f2247a.length == 0) {
            return -1;
        }
        this.d = d();
        return this.d;
    }

    private int d() {
        GetSubjectRequest getSubjectRequest = new GetSubjectRequest();
        getSubjectRequest.f1336a = this.e;
        getSubjectRequest.b = this.f;
        getSubjectRequest.c = 10;
        getSubjectRequest.d = this.f2247a;
        return send(getSubjectRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        boolean z = true;
        GetSubjectRequest getSubjectRequest = (GetSubjectRequest) jceStruct;
        GetSubjectResponse getSubjectResponse = (GetSubjectResponse) jceStruct2;
        boolean z2 = getSubjectResponse.e != this.c || getSubjectRequest.d == null || getSubjectRequest.d.length == 0;
        if (getSubjectResponse.d != 1) {
            z = false;
        }
        this.b = z;
        ArrayList<SimpleAppModel> b2 = k.b(getSubjectResponse.b);
        this.c = getSubjectResponse.e;
        this.h.b(this.c);
        this.f2247a = getSubjectResponse.c;
        if (z2) {
            this.g.clear();
        }
        this.g.addAll(b2);
        AppGroupInfo appGroupInfo = new AppGroupInfo();
        if (getSubjectResponse.f != null) {
            AppGroup appGroup = getSubjectResponse.f;
            appGroupInfo.a(appGroup.f1151a);
            appGroupInfo.b(appGroup.c);
            appGroupInfo.c(appGroup.d);
            appGroupInfo.a(appGroup.b);
            appGroupInfo.a(appGroup.f);
            appGroupInfo.b(appGroup.g);
            if (appGroup.h != null) {
                appGroupInfo.a(appGroup.h);
            }
        }
        notifyDataChangedInMainThread(new p(this, i, z2, b2, appGroupInfo));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        GetSubjectRequest getSubjectRequest = (GetSubjectRequest) jceStruct;
        notifyDataChangedInMainThread(new q(this, i, i2, getSubjectRequest.d == null || getSubjectRequest.d.length == 0));
    }

    public boolean c() {
        return this.b;
    }
}
