package com.tencent.cloud.activity;

import android.content.pm.PackageInfo;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.module.callback.e;
import com.tencent.assistant.module.k;
import com.tencent.assistant.module.update.j;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
class ab implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UpdateListActivity f2169a;

    private ab(UpdateListActivity updateListActivity) {
        this.f2169a = updateListActivity;
    }

    /* synthetic */ ab(UpdateListActivity updateListActivity, w wVar) {
        this(updateListActivity);
    }

    public void a(int i, int i2) {
    }

    public void a(int i, int i2, List<AppSimpleDetail> list) {
        if (this.f2169a.E == i && list != null && list.size() > 0) {
            ArrayList<AppSimpleDetail> arrayList = new ArrayList<>();
            List<PackageInfo> c = com.tencent.assistant.utils.e.c(AstApp.i());
            for (AppSimpleDetail next : list) {
                if (next != null) {
                    int i3 = 0;
                    while (true) {
                        if (i3 >= (c == null ? 0 : c.size())) {
                            break;
                        }
                        PackageInfo packageInfo = c.get(i3);
                        if (packageInfo == null || TextUtils.isEmpty(packageInfo.packageName) || !packageInfo.packageName.equals(next.e)) {
                            i3++;
                        } else if (packageInfo.versionCode < next.g) {
                            arrayList.add(next);
                        }
                    }
                }
            }
            j.b().a(this.f2169a.a(arrayList));
            ArrayList arrayList2 = new ArrayList();
            for (AppSimpleDetail appSimpleDetail : arrayList) {
                if (appSimpleDetail != null) {
                    arrayList2.add(k.a(appSimpleDetail));
                }
            }
            if (this.f2169a.x != null) {
                this.f2169a.x.a(arrayList2);
            }
        }
    }
}
