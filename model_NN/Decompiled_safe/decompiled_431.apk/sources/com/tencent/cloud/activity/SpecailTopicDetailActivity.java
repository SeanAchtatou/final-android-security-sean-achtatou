package com.tencent.cloud.activity;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.invalidater.TXRefreshGetMoreListViewScrollListener;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.model.AppGroupInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.cloud.adapter.SpecailTopicDetailAdapter;
import com.tencent.cloud.b.a.b;
import com.tencent.cloud.b.o;
import com.tencent.connect.common.Constants;
import java.util.List;

/* compiled from: ProGuard */
public class SpecailTopicDetailActivity extends BaseActivity implements ITXRefreshListViewListener, b {
    private int A = 0;
    private String B;
    private int C = 0;
    /* access modifiers changed from: private */
    public o D;
    private int E = 3;
    private View.OnClickListener F = new q(this);
    private Context n;
    private AppGroupInfo u;
    private TXGetMoreListView v;
    private SecondNavigationTitleViewV5 w;
    /* access modifiers changed from: private */
    public NormalErrorRecommendPage x;
    private LoadingView y;
    private SpecailTopicDetailAdapter z;

    public int f() {
        u();
        if (this.A == 5) {
            return STConst.ST_PAGE_SPECIAL_DETAIL_REDFLOWER;
        }
        return STConst.ST_PAGE_SPECIAL_DETAIL;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.n = this;
        setContentView((int) R.layout.activity_specail_topic_detail_layout);
        v();
        t();
        q();
    }

    public boolean g() {
        return false;
    }

    public void q() {
        STInfoV2 p = p();
        if (p != null) {
            p.updateContentId(STCommonInfo.ContentIdType.SPECIAL, String.valueOf(this.A));
        }
        l.a(p);
    }

    private void t() {
        u();
        this.z = new SpecailTopicDetailAdapter(this, this.v, this.u, null);
        this.z.a(this.A);
        this.z.b(this.C);
        this.v.setAdapter(this.z);
        if (TextUtils.isEmpty(this.B)) {
            this.w.b(getString(R.string.special_topic_detail));
            this.B = Constants.STR_EMPTY;
        } else {
            this.w.b(this.B);
        }
        TXRefreshGetMoreListViewScrollListener tXRefreshGetMoreListViewScrollListener = new TXRefreshGetMoreListViewScrollListener();
        this.v.setIScrollerListener(tXRefreshGetMoreListViewScrollListener);
        this.z.a(tXRefreshGetMoreListViewScrollListener);
        this.D = new o(this.A, this.B);
        this.D.register(this);
        this.D.a();
    }

    private void u() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.u = (AppGroupInfo) extras.get("com.tencent.assistant.APP_GROUP_INFO");
            if (this.u != null) {
                this.B = this.u.b;
                this.A = this.u.f935a;
                return;
            }
            try {
                this.A = Integer.parseInt(extras.getString("com.tencent.assistant.TOPICID"));
            } catch (Exception e) {
            }
            try {
                this.C = Integer.parseInt(extras.getString("com.tencent.assistant.TOPICSTYLE"));
            } catch (Exception e2) {
            }
            this.B = extras.getString("com.tencent.assistant.TOPICNAME");
        }
    }

    private void v() {
        this.y = (LoadingView) findViewById(R.id.loading_view);
        this.x = (NormalErrorRecommendPage) findViewById(R.id.topic_network_error);
        this.x.setOnClickListener(this.F);
        this.x.setButtonClickListener(this.F);
        this.w = (SecondNavigationTitleViewV5) findViewById(R.id.topic_title_view);
        this.w.a(this);
        this.w.d(false);
        this.w.i();
        this.v = (TXGetMoreListView) findViewById(R.id.topic_list);
        this.v.setDivider(null);
        this.v.setSelector(getResources().getDrawable(R.drawable.transparent_selector));
        this.v.setRefreshListViewListener(this);
    }

    private void b(int i) {
        this.v.setVisibility(8);
        this.x.setVisibility(0);
        this.y.setVisibility(8);
        this.x.setErrorType(i);
    }

    private void w() {
        this.v.setVisibility(0);
        this.y.setVisibility(8);
        this.x.setVisibility(8);
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (scrollState == TXScrollViewBase.ScrollState.ScrollState_FromEnd) {
            this.D.b();
        }
    }

    public void a(int i, int i2, boolean z2, List<SimpleAppModel> list, AppGroupInfo appGroupInfo) {
        if (i2 == 0) {
            this.E = 3;
            if (this.u == null) {
                this.u = appGroupInfo;
                this.z.a(this.u);
            }
            this.z.a(z2, list);
            this.v.onRefreshComplete(this.D.c(), true);
            if (!z2) {
                return;
            }
            if (list == null || list.size() == 0) {
                b(50);
            } else {
                w();
            }
        } else if (-800 == i2) {
            if (z2 || this.z == null || this.z.getCount() == 0) {
                b(30);
            } else {
                this.v.onRefreshComplete(true, false);
            }
        } else if (this.E > 0) {
            if (this.z == null || this.z.a() == 0) {
                this.D.a();
            } else {
                this.D.b();
            }
            this.E--;
        } else if (this.z == null || this.z.a() == 0) {
            b(20);
        } else {
            this.v.onRefreshComplete(true, false);
        }
    }

    /* access modifiers changed from: private */
    public void x() {
        this.v.setVisibility(8);
        this.y.setVisibility(0);
        this.x.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.z != null) {
            this.z.notifyDataSetChanged();
            this.z.c();
        }
        this.w.l();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.w.m();
        if (this.z != null) {
            this.z.b();
        }
    }
}
