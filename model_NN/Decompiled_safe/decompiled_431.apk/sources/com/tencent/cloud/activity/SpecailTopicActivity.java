package com.tencent.cloud.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.model.AppGroupInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.g;
import com.tencent.assistant.module.y;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.cloud.adapter.SpecialTopicAdapter;
import java.util.ArrayList;
import java.util.Map;

/* compiled from: ProGuard */
public class SpecailTopicActivity extends BaseActivity implements ITXRefreshListViewListener, g {
    private SpecialTopicAdapter A;
    private View.OnClickListener B = new p(this);
    private Context n;
    private TXGetMoreListView u;
    private SecondNavigationTitleViewV5 v;
    /* access modifiers changed from: private */
    public NormalErrorRecommendPage w;
    private LoadingView x;
    /* access modifiers changed from: private */
    public y y;
    private int z = 3;

    public int f() {
        return STConst.ST_PAGE_SPECIAL;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.n = this;
        setContentView((int) R.layout.activity_specail_topic_layout);
        u();
        t();
    }

    private void t() {
        this.y = new y();
        this.y.register(this);
        this.y.a(3);
        this.A = new SpecialTopicAdapter(this.n, null);
        this.u.setAdapter(this.A);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.v.l();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.v.m();
    }

    private void u() {
        this.x = (LoadingView) findViewById(R.id.loading_view);
        this.w = (NormalErrorRecommendPage) findViewById(R.id.topic_network_error);
        this.w.setOnClickListener(this.B);
        this.w.setButtonClickListener(this.B);
        this.v = (SecondNavigationTitleViewV5) findViewById(R.id.topic_title_view);
        this.v.a(this);
        this.v.b(getResources().getString(R.string.special_topic));
        this.v.i();
        this.u = (TXGetMoreListView) findViewById(R.id.topic_list);
        this.u.setDivider(null);
        this.u.setSelector(getResources().getDrawable(R.drawable.transparent_selector));
        this.u.setRefreshListViewListener(this);
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (TXScrollViewBase.ScrollState.ScrollState_FromEnd == scrollState) {
            this.y.d();
        }
    }

    public void a(int i, int i2, boolean z2, Map<AppGroupInfo, ArrayList<SimpleAppModel>> map, ArrayList<Long> arrayList, boolean z3) {
        if (i2 == 0) {
            if (map != null && map.size() > 0) {
                v();
                this.A.a(map, z2);
                this.u.onRefreshComplete(z3, true);
            } else if (z2) {
                b(10);
            } else {
                Toast.makeText(this.n, (int) R.string.is_next_page_error_happen, 0).show();
            }
        } else if (-800 == i2) {
            if (z2 || this.A == null || this.A.getCount() == 0) {
                b(30);
                return;
            }
            Toast.makeText(this.n, (int) R.string.is_next_page_error_happen, 0).show();
            this.u.onRefreshComplete(true, false);
        } else if (this.z > 0) {
            if (this.A == null || this.A.a() == 0) {
                this.y.a(3);
            } else {
                this.y.d();
            }
            this.z--;
        } else if (this.A == null || this.A.a() == 0) {
            b(20);
        } else {
            Toast.makeText(this.n, (int) R.string.is_next_page_error_happen, 0).show();
            this.u.onRefreshComplete(true, false);
        }
    }

    private void b(int i) {
        this.u.setVisibility(8);
        this.w.setVisibility(0);
        this.x.setVisibility(8);
        this.w.setErrorType(i);
    }

    private void v() {
        this.u.setVisibility(0);
        this.x.setVisibility(8);
        this.w.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void w() {
        this.u.setVisibility(8);
        this.x.setVisibility(0);
        this.w.setVisibility(8);
    }
}
