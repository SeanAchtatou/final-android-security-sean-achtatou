package com.tencent.cloud.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.component.TXViewPager;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.cloud.component.AppRankTabBarView;
import com.tencent.cloud.d.k;
import com.tencent.cloud.d.p;
import com.tencent.downloadsdk.utils.j;
import com.tencent.pangu.component.search.c;

/* compiled from: ProGuard */
public class AppRankActivity extends BaseActivity {
    private SecondNavigationTitleViewV5 n;
    private p u;
    /* access modifiers changed from: private */
    public AppRankTabBarView v;
    /* access modifiers changed from: private */
    public TXViewPager w;
    /* access modifiers changed from: private */
    public d x;
    /* access modifiers changed from: private */
    public int y;
    private c z = new b(this);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_apprank);
        this.u = k.a().b(16);
        t();
        u();
        c(this.y);
        q();
    }

    private void t() {
        this.n = (SecondNavigationTitleViewV5) findViewById(R.id.apprank_title);
        this.n.d(false);
        this.n.a(this);
        this.n.b(getString(R.string.apprank_title));
        this.n.i();
    }

    private void u() {
        String[] strArr = new String[this.u.b.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.u.b.size()) {
                strArr[i2] = this.u.b.get(i2).f2313a;
                i = i2 + 1;
            } else {
                this.v = (AppRankTabBarView) findViewById(R.id.tab_view);
                this.v.a(strArr);
                this.v.a(this.y);
                this.v.a(this.z);
                return;
            }
        }
    }

    private void b(int i) {
        STInfoV2 p = p();
        if (p != null) {
            p.actionId = 200;
            p.slotId = a.a("04", i);
        }
        l.a(p);
    }

    private void c(int i) {
        this.w = (TXViewPager) findViewById(R.id.vPager);
        if (this.x == null) {
            this.x = new d(this, e(), this, this.u.b);
        }
        this.w.setAdapter(this.x);
        this.w.setCurrentItem(i);
        d(i);
        this.w.setOnPageChangeListener(new e(this));
    }

    /* access modifiers changed from: private */
    public void d(int i) {
        b(i);
        Fragment a2 = this.x.a(i);
        if (a2 != null) {
            ((com.tencent.assistantv2.activity.a) a2).C();
        }
        this.y = i;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.n != null) {
            this.n.l();
        }
        if (this.y < this.x.getCount()) {
            j.a().post(new c(this, (com.tencent.assistantv2.activity.a) this.x.a(this.y)));
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.n.m();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
    }

    public int f() {
        return 200502;
    }
}
