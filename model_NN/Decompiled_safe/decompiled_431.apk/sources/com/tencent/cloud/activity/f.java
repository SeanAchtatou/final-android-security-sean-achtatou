package com.tencent.cloud.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistantv2.activity.a;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.cloud.b.g;
import com.tencent.cloud.component.CftAppRankListPage;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.adapter.RankNormalListAdapter;

/* compiled from: ProGuard */
public class f extends a {
    private final String R = "AppRankBaseFragment:";
    private long S;
    private int T;
    private int U;
    private byte V;
    private CftAppRankListPage W;
    /* access modifiers changed from: private */
    public RankNormalListAdapter X;
    private int Y;
    private ApkResCallback.Stub Z = new g(this);

    public f() {
    }

    public f(Activity activity) {
        super(activity);
    }

    public void d(Bundle bundle) {
        super.d(bundle);
        b((int) R.layout.app_rank_base_fragment);
        H();
        J();
    }

    public void H() {
        this.W = (CftAppRankListPage) c((int) R.id.apprank_listpage);
    }

    private void J() {
        boolean z = true;
        this.S = b().getLong("subId");
        this.T = b().getInt("subAppListType");
        this.U = b().getInt("subPageSize");
        this.V = b().getByte("flag");
        this.Y = b().getInt("content_id");
        g gVar = new g(this.S, this.T, (short) this.U);
        this.X = new RankNormalListAdapter(this.P, this.W, gVar.a());
        this.X.a(RankNormalListAdapter.ListType.LISTTYPEGAMESORT);
        this.X.a(I());
        this.X.b(this.Y + Constants.STR_EMPTY);
        RankNormalListAdapter rankNormalListAdapter = this.X;
        if ((this.V & 1) != 1) {
            z = false;
        }
        rankNormalListAdapter.a(z);
        this.W.a(gVar);
        this.W.a(this.X);
        this.W.b();
        this.X.c();
    }

    public void d(boolean z) {
        this.X.notifyDataSetChanged();
        ApkResourceManager.getInstance().registerApkResCallback(this.Z);
    }

    public void k() {
        super.k();
        this.X.b();
        ApkResourceManager.getInstance().unRegisterApkResCallback(this.Z);
    }

    public void n() {
        super.n();
        this.X.b();
        this.W.c();
        ApkResourceManager.getInstance().unRegisterApkResCallback(this.Z);
    }

    public int E() {
        return 0;
    }

    public int F() {
        return 2;
    }

    public void C() {
        B();
    }

    public void B() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.P, 100);
        if (buildSTInfo != null) {
            buildSTInfo.scene = G();
            buildSTInfo.slotId = I();
            buildSTInfo.contentId = this.Y + Constants.STR_EMPTY;
            if (this.P instanceof BaseActivity) {
                buildSTInfo.updateWithExternalPara(((BaseActivity) this.P).q);
            }
            l.a(buildSTInfo);
        }
    }

    public int G() {
        return 200502;
    }

    public String I() {
        return "03";
    }

    public void D() {
        Log.e("YYB5_0", "AppRankBaseFragment:onPageTurnBackground------------2:::");
    }
}
