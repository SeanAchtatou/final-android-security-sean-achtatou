package com.tencent.wcs.proxy.b;

import java.nio.ByteBuffer;

/* compiled from: ProGuard */
public class h extends a<ByteBuffer, Integer> {
    public static h b() {
        return j.f4119a;
    }

    private h() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean b(Integer... numArr) {
        return numArr != null && numArr.length > 0 && numArr[0].intValue() > 51200;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public ByteBuffer a(Integer... numArr) {
        ByteBuffer allocate;
        if (b(numArr)) {
            allocate = ByteBuffer.allocate(numArr[0].intValue());
        } else {
            allocate = ByteBuffer.allocate(51200);
        }
        allocate.clear();
        return allocate;
    }

    /* access modifiers changed from: protected */
    public boolean a(ByteBuffer byteBuffer) {
        return byteBuffer != null && byteBuffer.capacity() < 51200;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void c(ByteBuffer byteBuffer) {
        if (byteBuffer != null) {
            byteBuffer.clear();
        }
    }
}
