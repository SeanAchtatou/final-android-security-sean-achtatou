package com.tencent.wcs.proxy.b;

import com.tencent.wcs.proxy.c.f;
import java.nio.ByteBuffer;

/* compiled from: ProGuard */
public class q extends a<f, Void> {
    public static q b() {
        return s.f4122a;
    }

    private q() {
    }

    /* access modifiers changed from: protected */
    public f a(Void... voidArr) {
        return new f();
    }

    /* access modifiers changed from: protected */
    public boolean b(Void... voidArr) {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean a(f fVar) {
        return fVar != null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void c(f fVar) {
        if (fVar != null) {
            fVar.c(0);
            fVar.a(0);
            fVar.b(0);
            h.b().b((Object) fVar.c());
            fVar.a((ByteBuffer) null);
        }
    }
}
