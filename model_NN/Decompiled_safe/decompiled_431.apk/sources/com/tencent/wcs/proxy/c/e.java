package com.tencent.wcs.proxy.c;

import com.qq.AppService.r;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.protocol.l;

/* compiled from: ProGuard */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private int f4128a;
    private JceStruct b;
    private byte[] c;

    public int a() {
        if (this.c != null) {
            return this.c.length + 4;
        }
        return 0;
    }

    public int b() {
        return this.f4128a;
    }

    public JceStruct c() {
        return this.b;
    }

    public byte[] d() {
        return r.a(this.f4128a);
    }

    public byte[] e() {
        if (this.c != null) {
            return this.c;
        }
        return new byte[0];
    }

    public void a(int i) {
        this.f4128a = i;
    }

    public void a(JceStruct jceStruct) {
        this.b = jceStruct;
        if (jceStruct != null) {
            this.c = l.b(jceStruct);
        } else {
            this.c = null;
        }
    }
}
