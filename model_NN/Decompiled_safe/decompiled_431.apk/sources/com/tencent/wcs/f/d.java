package com.tencent.wcs.f;

import com.tencent.wcs.agent.a;
import com.tencent.wcs.b.c;
import com.tencent.wcs.c.b;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/* compiled from: ProGuard */
public class d extends a<c> {
    private static final int d = c.i;
    protected Map<Integer, c> b = Collections.synchronizedMap(new LinkedHashMap());
    private a c;

    public d(a aVar) {
        this.c = aVar;
    }

    public void a(c cVar) {
        if (c.f4064a) {
            b.a("AgentSessionMonitor handleTimeOut msgid: " + cVar.a() + " startTime: " + cVar.b() + " nowTime: " + System.currentTimeMillis());
        }
        this.c.a(cVar.a());
        this.b.remove(Integer.valueOf(cVar.a()));
    }

    public void a(int i) {
        if (!this.b.containsKey(Integer.valueOf(i))) {
            c cVar = new c(i, (long) (d * 1000));
            this.b.put(Integer.valueOf(i), cVar);
            b(cVar);
            return;
        }
        b(i);
        a(i);
    }

    public void b(int i) {
        c remove = this.b.remove(Integer.valueOf(i));
        if (remove != null) {
            c(remove);
        }
    }

    public void b() {
        this.b.clear();
        a();
    }
}
