package com.tencent.wcs.jce;

import java.io.Serializable;

/* compiled from: ProGuard */
public final class enumPushType implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final enumPushType f4111a = new enumPushType(0, 0, "PUSH_TYPE_ACTIVE");
    public static final enumPushType b = new enumPushType(1, 1, "PUSH_TYPE_INACTIVE");
    public static final enumPushType c = new enumPushType(2, 2, "PUSH_TYPE_LOWWER");
    public static final enumPushType d = new enumPushType(3, 3, "PUSH_TYPE_UPPER");
    static final /* synthetic */ boolean e = (!enumPushType.class.desiredAssertionStatus());
    private static enumPushType[] f = new enumPushType[4];
    private int g;
    private String h = new String();

    public String toString() {
        return this.h;
    }

    private enumPushType(int i, int i2, String str) {
        this.h = str;
        this.g = i2;
        f[i] = this;
    }
}
