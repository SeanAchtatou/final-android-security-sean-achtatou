package com.tencent.b.c;

import android.content.Context;
import com.tencent.b.b.b;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class g {
    private static g b = null;

    /* renamed from: a  reason: collision with root package name */
    private Map<Integer, f> f2076a;

    private g(Context context) {
        this.f2076a = null;
        this.f2076a = new HashMap(3);
        this.f2076a.put(1, new e(context));
        this.f2076a.put(2, new c(context));
        this.f2076a.put(4, new d(context));
    }

    public static synchronized g a(Context context) {
        g gVar;
        synchronized (g.class) {
            if (b == null) {
                b = new g(context);
            }
            gVar = b;
        }
        return gVar;
    }

    public b a() {
        return a(new ArrayList(Arrays.asList(1, 2, 4)));
    }

    public b a(List<Integer> list) {
        b g;
        if (list == null || list.size() == 0) {
            return null;
        }
        for (Integer num : list) {
            f fVar = this.f2076a.get(num);
            if (fVar != null && (g = fVar.g()) != null && g.a()) {
                return g;
            }
        }
        return null;
    }

    public void a(int i, int i2) {
        a b2 = b();
        if (i > 0) {
            b2.c(i);
        }
        if (i2 > 0) {
            b2.a(i2);
        }
        b2.a(System.currentTimeMillis());
        b2.b(0);
        a(b2);
    }

    public void a(b bVar) {
        for (Map.Entry<Integer, f> value : this.f2076a.entrySet()) {
            ((f) value.getValue()).a(bVar);
        }
    }

    public void a(a aVar) {
        for (Map.Entry<Integer, f> value : this.f2076a.entrySet()) {
            ((f) value.getValue()).b(aVar);
        }
    }

    public a b() {
        return b(new ArrayList(Arrays.asList(1, 4)));
    }

    public a b(List<Integer> list) {
        a h;
        if (list == null || list.size() == 0) {
            return null;
        }
        for (Integer num : list) {
            f fVar = this.f2076a.get(num);
            if (fVar != null && (h = fVar.h()) != null) {
                return h;
            }
        }
        return null;
    }
}
