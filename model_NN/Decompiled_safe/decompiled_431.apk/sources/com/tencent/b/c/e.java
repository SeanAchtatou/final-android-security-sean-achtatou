package com.tencent.b.c;

import android.content.Context;
import android.provider.Settings;
import com.tencent.b.d.k;

public class e extends f {
    public e(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void a(a aVar) {
        synchronized (this) {
            k.a("write CheckEntity to Settings.System:" + aVar.toString());
            try {
                Settings.System.putString(this.f2075a.getContentResolver(), f(), aVar.toString());
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        synchronized (this) {
            k.a("write mid to Settings.System");
            try {
                Settings.System.putString(this.f2075a.getContentResolver(), i(), str);
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        return k.a(this.f2075a, "android.permission.WRITE_SETTINGS");
    }

    /* access modifiers changed from: protected */
    public String b() {
        String string;
        synchronized (this) {
            k.a("read mid from Settings.System");
            string = Settings.System.getString(this.f2075a.getContentResolver(), i());
        }
        return string;
    }

    /* access modifiers changed from: protected */
    public a c() {
        a aVar;
        synchronized (this) {
            aVar = new a(Settings.System.getString(this.f2075a.getContentResolver(), f()));
            k.a("read readCheckEntity from Settings.System:" + aVar.toString());
        }
        return aVar;
    }
}
