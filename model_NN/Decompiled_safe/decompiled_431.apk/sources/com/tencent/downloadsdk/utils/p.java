package com.tencent.downloadsdk.utils;

import android.text.TextUtils;
import com.tencent.connect.common.Constants;
import java.net.URLEncoder;
import java.util.Locale;

public class p {
    public static String a(String str, String str2, String str3, String str4) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2) || TextUtils.isEmpty(str3) || TextUtils.isEmpty(str4)) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(a.b((str2 + "_YYB").getBytes(), 0).replace("\r", Constants.STR_EMPTY).replace("\n", Constants.STR_EMPTY)).append("_");
        sb.append(str3).append("_").append(str4);
        StringBuilder sb2 = new StringBuilder(str);
        if (str.contains("?")) {
            sb2.append("&");
        } else {
            sb2.append('?');
        }
        sb2.append("__k2__").append("=").append(URLEncoder.encode(sb.toString(), "UTF-8"));
        return sb2.toString();
    }

    public static boolean a(String str) {
        return !TextUtils.isEmpty(str) && str.toLowerCase(Locale.getDefault()).contains("__k1__=y") && !str.contains("__k2__");
    }
}
