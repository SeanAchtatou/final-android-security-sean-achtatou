package com.tencent.downloadsdk;

import com.tencent.downloadsdk.a.c;

class f implements ah {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f2473a;

    f(c cVar) {
        this.f2473a = cVar;
    }

    public void a() {
        com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "onSave2FileSucceed");
        this.f2473a.g();
    }

    public void a(int i) {
        com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "onSave2FileFailed  tid: " + Thread.currentThread().getId() + " errorCode: " + i);
        int unused = this.f2473a.v = i;
        this.f2473a.e();
    }

    public void a(long j) {
        if (this.f2473a.o != null) {
            this.f2473a.o.a(j, c.c(this.f2473a.z.c()));
        }
    }

    public void a(String str) {
        if (this.f2473a.I != null) {
            this.f2473a.I.s = str;
        }
    }

    public void b() {
        com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "DownloadSetting.mMaxThreadNum: " + this.f2473a.p.i);
        com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "DownloadSetting.mSocketBufferSize: " + this.f2473a.p.h);
        com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "DownloadSetting.mReadBufferSize: " + this.f2473a.p.h);
        this.f2473a.o.e();
        this.f2473a.g();
    }
}
