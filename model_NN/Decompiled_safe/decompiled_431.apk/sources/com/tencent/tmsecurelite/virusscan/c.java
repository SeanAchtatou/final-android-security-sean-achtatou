package com.tencent.tmsecurelite.virusscan;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.tencent.tmsecurelite.commom.d;
import java.util.ArrayList;

/* compiled from: ProGuard */
public abstract class c extends Binder implements IVirusScan {
    public c() {
        attachInterface(this, IVirusScan.INTERFACE);
    }

    public static IVirusScan a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface(IVirusScan.INTERFACE);
        if (queryLocalInterface == null || !(queryLocalInterface instanceof IVirusScan)) {
            return new b(iBinder);
        }
        return (IVirusScan) queryLocalInterface;
    }

    /* access modifiers changed from: protected */
    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        boolean z = false;
        switch (i) {
            case 1:
                parcel.enforceInterface(IVirusScan.INTERFACE);
                IScanListener asInterface = ScanListenerStub.asInterface(parcel.readStrongBinder());
                if (parcel.readByte() == 1) {
                    z = true;
                }
                scanInstalledPackages(asInterface, z);
                parcel2.writeNoException();
                break;
            case 2:
                parcel.enforceInterface(IVirusScan.INTERFACE);
                IScanListener asInterface2 = ScanListenerStub.asInterface(parcel.readStrongBinder());
                if (parcel.readByte() == 1) {
                    z = true;
                }
                scanSdcardApks(asInterface2, z);
                parcel2.writeNoException();
                break;
            case 3:
                parcel.enforceInterface(IVirusScan.INTERFACE);
                IScanListener asInterface3 = ScanListenerStub.asInterface(parcel.readStrongBinder());
                if (parcel.readByte() == 1) {
                    z = true;
                }
                scanGlobal(asInterface3, z);
                parcel2.writeNoException();
                break;
            case 4:
                parcel.enforceInterface(IVirusScan.INTERFACE);
                ArrayList arrayList = new ArrayList();
                parcel.readStringList(arrayList);
                IScanListener asInterface4 = ScanListenerStub.asInterface(parcel.readStrongBinder());
                if (parcel.readByte() == 1) {
                    z = true;
                }
                scanPackages(arrayList, asInterface4, z);
                parcel2.writeNoException();
                break;
            case 5:
                parcel.enforceInterface(IVirusScan.INTERFACE);
                ArrayList arrayList2 = new ArrayList();
                parcel.readStringList(arrayList2);
                IScanListener asInterface5 = ScanListenerStub.asInterface(parcel.readStrongBinder());
                if (parcel.readByte() == 1) {
                    z = true;
                }
                scanApks(arrayList2, asInterface5, z);
                parcel2.writeNoException();
                break;
            case 6:
                parcel.enforceInterface(IVirusScan.INTERFACE);
                pauseScan();
                parcel2.writeNoException();
                break;
            case 7:
                parcel.enforceInterface(IVirusScan.INTERFACE);
                continueScan();
                parcel2.writeNoException();
                break;
            case 8:
                parcel.enforceInterface(IVirusScan.INTERFACE);
                cancelScan();
                parcel2.writeNoException();
                break;
            case 9:
                parcel.enforceInterface(IVirusScan.INTERFACE);
                freeScanner();
                parcel2.writeNoException();
                break;
            case 10:
                parcel.enforceInterface(IVirusScan.INTERFACE);
                boolean checkVersion = checkVersion(parcel.readInt());
                parcel2.writeNoException();
                if (checkVersion) {
                    z = true;
                }
                parcel2.writeByte(z ? (byte) 1 : 0);
                break;
            case 11:
                parcel.enforceInterface(IVirusScan.INTERFACE);
                IScanListener asInterface6 = ScanListenerStub.asInterface(parcel.readStrongBinder());
                if (parcel.readByte() == 1) {
                    z = true;
                }
                int scanInstalledPackagesAsync = scanInstalledPackagesAsync(asInterface6, z);
                parcel2.writeNoException();
                parcel2.writeInt(scanInstalledPackagesAsync);
                break;
            case 12:
                parcel.enforceInterface(IVirusScan.INTERFACE);
                IScanListener asInterface7 = ScanListenerStub.asInterface(parcel.readStrongBinder());
                if (parcel.readByte() == 1) {
                    z = true;
                }
                int scanSdcardApksAsync = scanSdcardApksAsync(asInterface7, z);
                parcel2.writeNoException();
                parcel2.writeInt(scanSdcardApksAsync);
                break;
            case 13:
                parcel.enforceInterface(IVirusScan.INTERFACE);
                IScanListener asInterface8 = ScanListenerStub.asInterface(parcel.readStrongBinder());
                if (parcel.readByte() == 1) {
                    z = true;
                }
                int scanGlobalAsync = scanGlobalAsync(asInterface8, z);
                parcel2.writeNoException();
                parcel2.writeInt(scanGlobalAsync);
                break;
            case 14:
                parcel.enforceInterface(IVirusScan.INTERFACE);
                ArrayList arrayList3 = new ArrayList();
                parcel.readStringList(arrayList3);
                IScanListener asInterface9 = ScanListenerStub.asInterface(parcel.readStrongBinder());
                if (parcel.readByte() == 1) {
                    z = true;
                }
                int scanPackagesAsync = scanPackagesAsync(arrayList3, asInterface9, z);
                parcel2.writeNoException();
                parcel2.writeInt(scanPackagesAsync);
                break;
            case 15:
                parcel.enforceInterface(IVirusScan.INTERFACE);
                ArrayList arrayList4 = new ArrayList();
                parcel.readStringList(arrayList4);
                IScanListener asInterface10 = ScanListenerStub.asInterface(parcel.readStrongBinder());
                if (parcel.readByte() == 1) {
                    z = true;
                }
                int scanApksAsync = scanApksAsync(arrayList4, asInterface10, z);
                parcel2.writeNoException();
                parcel2.writeInt(scanApksAsync);
                break;
            case 16:
                parcel.enforceInterface(IVirusScan.INTERFACE);
                int pauseScanAsync = pauseScanAsync();
                parcel2.writeNoException();
                parcel2.writeInt(pauseScanAsync);
                break;
            case 17:
                parcel.enforceInterface(IVirusScan.INTERFACE);
                int continueScanAsync = continueScanAsync();
                parcel2.writeNoException();
                parcel2.writeInt(continueScanAsync);
                break;
            case 18:
                parcel.enforceInterface(IVirusScan.INTERFACE);
                int cancelScanAsync = cancelScanAsync();
                parcel2.writeNoException();
                parcel2.writeInt(cancelScanAsync);
                break;
            case 19:
                parcel.enforceInterface(IVirusScan.INTERFACE);
                int freeScannerAsync = freeScannerAsync();
                parcel2.writeNoException();
                parcel2.writeInt(freeScannerAsync);
                break;
            case 21:
                parcel.enforceInterface(IVirusScan.INTERFACE);
                updateTmsConfigAsync(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
        }
        return true;
    }

    public boolean checkVersion(int i) {
        return 2 >= i;
    }
}
