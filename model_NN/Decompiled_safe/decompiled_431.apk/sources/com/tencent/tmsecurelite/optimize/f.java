package com.tencent.tmsecurelite.optimize;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.tencent.tmsecurelite.commom.DataEntity;
import org.json.JSONException;

/* compiled from: ProGuard */
public abstract class f extends Binder implements b {
    public f() {
        attachInterface(this, "com.tencent.tmsecurelite.IRubbishScanListener");
    }

    public IBinder asBinder() {
        return this;
    }

    public static b a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.tencent.tmsecurelite.IRubbishScanListener");
        if (queryLocalInterface == null || !(queryLocalInterface instanceof b)) {
            return new e(iBinder);
        }
        return (b) queryLocalInterface;
    }

    /* access modifiers changed from: protected */
    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        DataEntity dataEntity;
        switch (i) {
            case 1:
                c();
                parcel2.writeNoException();
                return true;
            case 2:
                int readInt = parcel.readInt();
                try {
                    dataEntity = new DataEntity(parcel);
                } catch (JSONException e) {
                    e.printStackTrace();
                    dataEntity = null;
                }
                a(readInt, dataEntity);
                parcel2.writeNoException();
                return true;
            case 3:
                a(parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 4:
                a();
                parcel2.writeNoException();
                return true;
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            default:
                return true;
            case 10:
                b();
                parcel2.writeNoException();
                return true;
        }
    }
}
