package com.tencent.tmsecurelite.b;

import android.os.IBinder;
import android.os.Parcel;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.commom.b;
import java.util.ArrayList;

/* compiled from: ProGuard */
public final class a implements c {

    /* renamed from: a  reason: collision with root package name */
    private IBinder f4032a;

    a(IBinder iBinder) {
        this.f4032a = iBinder;
    }

    public IBinder asBinder() {
        return this.f4032a;
    }

    public ArrayList<DataEntity> a(int i) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeInt(i);
            this.f4032a.transact(1, obtain, obtain2, 0);
            obtain2.readException();
            return DataEntity.readFromParcel(obtain2);
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public boolean a(int i, int i2) {
        boolean z = true;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeInt(i);
            obtain.writeInt(i2);
            this.f4032a.transact(2, obtain, obtain2, 0);
            obtain2.readException();
            if (obtain2.readInt() != 1) {
                z = false;
            }
            return z;
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void a(int i, int i2, boolean z) {
        int i3 = 0;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeInt(i);
            obtain.writeInt(i2);
            if (z) {
                i3 = 1;
            }
            obtain.writeInt(i3);
            this.f4032a.transact(3, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void b(int i, int i2, boolean z) {
        int i3 = 0;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeInt(i);
            obtain.writeInt(i2);
            if (z) {
                i3 = 1;
            }
            obtain.writeInt(i3);
            this.f4032a.transact(4, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void b(int i) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeInt(i);
            this.f4032a.transact(5, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public ArrayList<DataEntity> c(int i) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeInt(i);
            this.f4032a.transact(6, obtain, obtain2, 0);
            obtain2.readException();
            return DataEntity.readFromParcel(obtain2);
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public boolean a(String str, String str2, int i) {
        boolean z = true;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeString(str);
            obtain.writeString(str2);
            obtain.writeInt(i);
            this.f4032a.transact(7, obtain, obtain2, 0);
            obtain2.readException();
            if (obtain2.readInt() != 1) {
                z = false;
            }
            return z;
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void b(int i, int i2) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeInt(i);
            obtain.writeInt(i2);
            this.f4032a.transact(8, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int a() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            this.f4032a.transact(9, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int b() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            this.f4032a.transact(10, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void c(int i, int i2) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeInt(i);
            obtain.writeInt(i2);
            this.f4032a.transact(11, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int c() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            this.f4032a.transact(12, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void d(int i) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeInt(i);
            this.f4032a.transact(13, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void a(int i, b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeInt(i);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4032a.transact(15, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void a(int i, int i2, b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeInt(i);
            obtain.writeInt(i2);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4032a.transact(16, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void a(int i, int i2, boolean z, b bVar) {
        int i3 = 0;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeInt(i);
            obtain.writeInt(i2);
            if (z) {
                i3 = 1;
            }
            obtain.writeInt(i3);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4032a.transact(17, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void b(int i, int i2, boolean z, b bVar) {
        int i3 = 0;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeInt(i);
            obtain.writeInt(i2);
            if (z) {
                i3 = 1;
            }
            obtain.writeInt(i3);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4032a.transact(18, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void b(int i, b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeInt(i);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4032a.transact(19, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void c(int i, b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeInt(i);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4032a.transact(20, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void a(String str, String str2, int i, b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeString(str);
            obtain.writeString(str2);
            obtain.writeInt(i);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4032a.transact(21, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void b(int i, int i2, b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeInt(i);
            obtain.writeInt(i2);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4032a.transact(22, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void a(b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4032a.transact(23, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void b(b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4032a.transact(24, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void c(int i, int i2, b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeInt(i);
            obtain.writeInt(i2);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4032a.transact(25, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void c(b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4032a.transact(26, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void d(int i, b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeInt(i);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4032a.transact(27, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void d(b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IDisturbIntercept");
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4032a.transact(29, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }
}
