package com.tencent.feedback.eup.jni;

import android.content.Context;
import com.tencent.assistant.st.STConst;
import com.tencent.connect.common.Constants;
import com.tencent.feedback.eup.e;
import com.tencent.feedback.eup.g;

/* compiled from: ProGuard */
public final class b implements NativeExceptionHandler {
    private static b b;

    /* renamed from: a  reason: collision with root package name */
    private Context f2560a;
    private String c;

    private b(Context context) {
        this.f2560a = context;
    }

    public final void handleNativeException(int i, int i2, long j, long j2, String str, String str2, String str3, String str4) {
        handleNativeException(i, i2, j, j2, str, str2, str3, str4, -1234567890, Constants.STR_EMPTY, -1, -1, -1, Constants.STR_EMPTY, STConst.ST_INSTALL_FAIL_STR_UNKNOWN);
    }

    public static synchronized b a(Context context) {
        b bVar;
        synchronized (b.class) {
            if (b == null) {
                b = new b(context);
            }
            bVar = b;
        }
        return bVar;
    }

    public static synchronized b a() {
        b bVar;
        synchronized (b.class) {
            bVar = b;
        }
        return bVar;
    }

    private synchronized String b() {
        return this.c;
    }

    public final synchronized void a(String str) {
        this.c = str;
    }

    protected static e a(Context context, long j, String str, String str2, String str3, String str4, int i, String str5, int i2, String str6, byte[] bArr, String str7, String str8) {
        String str9;
        if (str3 == null) {
            str9 = null;
        } else {
            int indexOf = str3.indexOf("java.lang.Thread.getStackTrace");
            if (indexOf < 0) {
                str9 = str3;
            } else {
                int indexOf2 = str3.indexOf("\n", indexOf);
                if (indexOf2 < 0) {
                    str9 = str3;
                } else {
                    str9 = str3.substring(0, indexOf) + str3.substring(indexOf2);
                }
            }
        }
        com.tencent.feedback.common.e a2 = com.tencent.feedback.common.e.a(context);
        e a3 = g.a(context, a2.h(), a2.p(), a2.k(), a2.y(), a2.E(), Thread.currentThread().getName(), str2, str, str6, str9, j, str7, bArr);
        if (a3 == null) {
            return null;
        }
        if (i > 0) {
            a3.a(a3.e() + "(" + str5 + ")");
            a3.o("kernel");
        } else {
            a3.o(str5);
            if (i2 <= 0 || str.equalsIgnoreCase("SIGABRT")) {
                a3.n(STConst.ST_INSTALL_FAIL_STR_UNKNOWN + i2);
            } else {
                a3.n(com.tencent.feedback.common.b.a(context, i2));
            }
        }
        com.tencent.feedback.common.g.a("etype:%s,sType:%s,sPN:%s", a3.e(), a3.D(), a3.C());
        a3.a((byte) 2);
        a3.h(str4);
        a3.p(str8);
        return a3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x01a4  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01ce  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0241  */
    /* JADX WARNING: Removed duplicated region for block: B:75:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void handleNativeException(int r21, int r22, long r23, long r25, java.lang.String r27, java.lang.String r28, java.lang.String r29, java.lang.String r30, int r31, java.lang.String r32, int r33, int r34, int r35, java.lang.String r36, java.lang.String r37) {
        /*
            r20 = this;
            java.lang.String r1 = "rqdp{  na eup p:} %d , t:%d , exT:%d ,exTMS: %d, exTP:%s ,exADD:%s ,parsed exSTA:%s, TMB:%s , si_code:%d , si_CodeType:%s , sPid:%d ,sUid:%d,siErr:%d,siErrMsg:%s,naVersion:%s"
            r2 = 15
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.Integer r4 = java.lang.Integer.valueOf(r21)
            r2[r3] = r4
            r3 = 1
            java.lang.Integer r4 = java.lang.Integer.valueOf(r22)
            r2[r3] = r4
            r3 = 2
            java.lang.Long r4 = java.lang.Long.valueOf(r23)
            r2[r3] = r4
            r3 = 3
            java.lang.Long r4 = java.lang.Long.valueOf(r25)
            r2[r3] = r4
            r3 = 4
            r2[r3] = r27
            r3 = 5
            r2[r3] = r28
            r3 = 6
            r2[r3] = r29
            r3 = 7
            r2[r3] = r30
            r3 = 8
            java.lang.Integer r4 = java.lang.Integer.valueOf(r31)
            r2[r3] = r4
            r3 = 9
            r2[r3] = r32
            r3 = 10
            java.lang.Integer r4 = java.lang.Integer.valueOf(r33)
            r2[r3] = r4
            r3 = 11
            java.lang.Integer r4 = java.lang.Integer.valueOf(r34)
            r2[r3] = r4
            r3 = 12
            java.lang.Integer r4 = java.lang.Integer.valueOf(r35)
            r2[r3] = r4
            r3 = 13
            r2[r3] = r36
            r3 = 14
            r2[r3] = r37
            com.tencent.feedback.common.g.b(r1, r2)
            java.lang.String r1 = "eup"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "native crash happen:"
            r2.<init>(r3)
            r0 = r27
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            android.util.Log.e(r1, r2)
            java.lang.String r1 = "eup"
            r0 = r29
            android.util.Log.e(r1, r0)
            r0 = r20
            android.content.Context r1 = r0.f2560a
            com.tencent.feedback.a.a r1 = com.tencent.feedback.a.a.a(r1)
            boolean r1 = r1.a()
            if (r1 == 0) goto L_0x010b
            java.lang.String r16 = "This Crash Caused By ANR , PLS To Fix ANR , This Trace May Be Not Useful!"
        L_0x008a:
            if (r1 == 0) goto L_0x0093
            java.lang.String r1 = "eup"
            java.lang.String r2 = "This Crash Caused By ANR , PLS To Fix ANR , This Trace May Be Not Useful!"
            android.util.Log.e(r1, r2)
        L_0x0093:
            r17 = 0
            r18 = 0
            r1 = 1000(0x3e8, double:4.94E-321)
            long r1 = r1 * r23
            r3 = 1000(0x3e8, double:4.94E-321)
            long r3 = r25 / r3
            long r7 = r1 + r3
            com.tencent.feedback.eup.l r1 = com.tencent.feedback.eup.l.l()
            if (r1 != 0) goto L_0x010f
            java.lang.String r1 = "rqdp{  instance == null}"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.c(r1, r2)
            r1 = 0
        L_0x00b0:
            if (r1 == 0) goto L_0x00e8
            java.lang.String r2 = "your crhanlde start"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0114 }
            com.tencent.feedback.common.g.b(r2, r3)     // Catch:{ Throwable -> 0x0114 }
            r2 = 1
            r1.a(r2)     // Catch:{ Throwable -> 0x0114 }
        L_0x00be:
            java.lang.String r2 = "your crdata"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x012e }
            com.tencent.feedback.common.g.b(r2, r3)     // Catch:{ Throwable -> 0x012e }
            r2 = 1
            r3 = r27
            r4 = r28
            r5 = r29
            r6 = r31
            byte[] r17 = r1.a(r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x012e }
        L_0x00d3:
            java.lang.String r2 = "your crmsg"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0148 }
            com.tencent.feedback.common.g.b(r2, r3)     // Catch:{ Throwable -> 0x0148 }
            r2 = 1
            r3 = r27
            r4 = r28
            r5 = r29
            r6 = r31
            java.lang.String r18 = r1.b(r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x0148 }
        L_0x00e8:
            r0 = r20
            android.content.Context r6 = r0.f2560a
            r9 = r27
            r10 = r28
            r11 = r29
            r12 = r30
            r13 = r31
            r14 = r32
            r15 = r33
            r19 = r37
            com.tencent.feedback.eup.e r13 = a(r6, r7, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19)
            if (r13 != 0) goto L_0x0162
            java.lang.String r1 = "rqdp{  cr eup msg fail!}"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.c(r1, r2)
        L_0x010a:
            return
        L_0x010b:
            java.lang.String r16 = ""
            goto L_0x008a
        L_0x010f:
            com.tencent.feedback.eup.b r1 = r1.q()
            goto L_0x00b0
        L_0x0114:
            r2 = move-exception
            java.lang.String r3 = "on native hanlde start throw %s"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r5 = 0
            java.lang.String r6 = r2.toString()
            r4[r5] = r6
            com.tencent.feedback.common.g.d(r3, r4)
            boolean r3 = com.tencent.feedback.common.g.a(r2)
            if (r3 != 0) goto L_0x00be
            r2.printStackTrace()
            goto L_0x00be
        L_0x012e:
            r2 = move-exception
            java.lang.String r3 = "get extra data error %s"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r5 = 0
            java.lang.String r6 = r2.toString()
            r4[r5] = r6
            com.tencent.feedback.common.g.d(r3, r4)
            boolean r3 = com.tencent.feedback.common.g.a(r2)
            if (r3 != 0) goto L_0x00d3
            r2.printStackTrace()
            goto L_0x00d3
        L_0x0148:
            r2 = move-exception
            java.lang.String r3 = "get extra msg error %s"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r5 = 0
            java.lang.String r6 = r2.toString()
            r4[r5] = r6
            com.tencent.feedback.common.g.d(r3, r4)
            boolean r3 = com.tencent.feedback.common.g.a(r2)
            if (r3 != 0) goto L_0x00e8
            r2.printStackTrace()
            goto L_0x00e8
        L_0x0162:
            java.lang.String r2 = "rqdp{  get other stack}"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x01f7 }
            com.tencent.feedback.common.g.c(r2, r3)     // Catch:{ Throwable -> 0x01f7 }
            java.util.Map r2 = com.tencent.feedback.proguard.ac.b()     // Catch:{ Throwable -> 0x01f7 }
            if (r2 == 0) goto L_0x0177
            java.util.Map r3 = r13.F()     // Catch:{ Throwable -> 0x01f7 }
            r3.putAll(r2)     // Catch:{ Throwable -> 0x01f7 }
        L_0x0177:
            r12 = 1
            if (r1 == 0) goto L_0x0224
            java.lang.String r2 = "your ask2save"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]
            com.tencent.feedback.common.g.b(r2, r3)
            r2 = 1
            java.lang.String r9 = r13.m()     // Catch:{ Throwable -> 0x020b }
            java.lang.String r10 = r13.G()     // Catch:{ Throwable -> 0x020b }
            java.lang.String r11 = r13.x()     // Catch:{ Throwable -> 0x020b }
            r3 = r27
            r4 = r28
            r5 = r29
            r6 = r31
            boolean r2 = r1.a(r2, r3, r4, r5, r6, r7, r9, r10, r11)     // Catch:{ Throwable -> 0x020b }
        L_0x019b:
            r0 = r20
            android.content.Context r3 = r0.f2560a
            com.tencent.feedback.eup.BuglyBroadcastRecevier.a(r3, r13)
            if (r2 == 0) goto L_0x0241
            r0 = r20
            android.content.Context r2 = r0.f2560a     // Catch:{ Throwable -> 0x0227 }
            com.tencent.feedback.eup.j r2 = com.tencent.feedback.eup.j.a(r2)     // Catch:{ Throwable -> 0x0227 }
            if (r2 == 0) goto L_0x01cc
            com.tencent.feedback.eup.d r3 = com.tencent.feedback.eup.c.a()     // Catch:{ Throwable -> 0x0227 }
            boolean r2 = r2.a(r13, r3)     // Catch:{ Throwable -> 0x0227 }
            java.lang.String r3 = "rqdp{  eup save} %b"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x0227 }
            r5 = 0
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ Throwable -> 0x0227 }
            r4[r5] = r2     // Catch:{ Throwable -> 0x0227 }
            com.tencent.feedback.common.g.b(r3, r4)     // Catch:{ Throwable -> 0x0227 }
            java.lang.String r2 = r20.b()     // Catch:{ Throwable -> 0x0227 }
            com.tencent.feedback.eup.jni.c.a(r2)     // Catch:{ Throwable -> 0x0227 }
        L_0x01cc:
            if (r1 == 0) goto L_0x010a
            java.lang.String r2 = "your crhanlde end"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x01dc }
            com.tencent.feedback.common.g.b(r2, r3)     // Catch:{ Throwable -> 0x01dc }
            r2 = 1
            r1.b(r2)     // Catch:{ Throwable -> 0x01dc }
            goto L_0x010a
        L_0x01dc:
            r1 = move-exception
            java.lang.String r2 = "on native hanlde end throw %s"
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r4 = 0
            java.lang.String r5 = r1.toString()
            r3[r4] = r5
            com.tencent.feedback.common.g.d(r2, r3)
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x010a
            r1.printStackTrace()
            goto L_0x010a
        L_0x01f7:
            r2 = move-exception
            java.lang.String r3 = "get all threads stack fail"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]
            com.tencent.feedback.common.g.d(r3, r4)
            boolean r3 = com.tencent.feedback.common.g.a(r2)
            if (r3 != 0) goto L_0x0177
            r2.printStackTrace()
            goto L_0x0177
        L_0x020b:
            r2 = move-exception
            java.lang.String r3 = "on Crash Saving error %s"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r5 = 0
            java.lang.String r6 = r2.toString()
            r4[r5] = r6
            com.tencent.feedback.common.g.d(r3, r4)
            boolean r3 = com.tencent.feedback.common.g.a(r2)
            if (r3 != 0) goto L_0x0224
            r2.printStackTrace()
        L_0x0224:
            r2 = r12
            goto L_0x019b
        L_0x0227:
            r2 = move-exception
            java.lang.String r3 = "your crash handle happen error %s"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r5 = 0
            java.lang.String r6 = r2.toString()
            r4[r5] = r6
            com.tencent.feedback.common.g.d(r3, r4)
            boolean r3 = com.tencent.feedback.common.g.a(r2)
            if (r3 != 0) goto L_0x01cc
            r2.printStackTrace()
            goto L_0x01cc
        L_0x0241:
            java.lang.String r2 = "the eup no need to save!"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]
            com.tencent.feedback.common.g.c(r2, r3)
            goto L_0x01cc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.eup.jni.b.handleNativeException(int, int, long, long, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, java.lang.String, int, int, int, java.lang.String, java.lang.String):void");
    }
}
