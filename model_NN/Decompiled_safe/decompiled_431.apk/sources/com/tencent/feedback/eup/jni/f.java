package com.tencent.feedback.eup.jni;

import com.tencent.feedback.common.g;
import java.io.File;
import java.io.FilenameFilter;
import java.util.List;

/* compiled from: ProGuard */
final class f implements FilenameFilter {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ int f2563a;
    private /* synthetic */ int b;
    private /* synthetic */ List c;
    private /* synthetic */ d d;

    f(d dVar, int i, int i2, List list) {
        this.d = dVar;
        this.f2563a = i;
        this.b = i2;
        this.c = list;
    }

    public final boolean accept(File file, String str) {
        g.b("rqdp{  check dir} %s rqdp{  , filename} %s", file, str);
        if (str.startsWith(this.d.f2562a)) {
            d.a(this.d);
            g.b("rqdp{  accept }%s", str);
            try {
                long parseLong = Long.parseLong(str.substring(this.f2563a, str.length() - this.b));
                g.b("rqdp{  mRemoveBeforeDate }%d", Long.valueOf(this.d.d));
                if (parseLong <= this.d.d) {
                    g.b("rqdp{  recordTime} %d rqdp{  is old}", Long.valueOf(parseLong));
                    return true;
                }
                g.b("rqdp{  newFileTimeList add} %d", Long.valueOf(parseLong));
                this.c.add(Long.valueOf(parseLong));
            } catch (Throwable th) {
                g.c("rqdp{  filename is not formatted ,shoud do delete! \n path:}%s", str);
                if (g.a(th)) {
                    return true;
                }
                th.printStackTrace();
                return true;
            }
        }
        return false;
    }
}
