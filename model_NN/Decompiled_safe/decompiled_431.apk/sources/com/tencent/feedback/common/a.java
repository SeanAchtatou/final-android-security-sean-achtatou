package com.tencent.feedback.common;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
final class a implements Parcelable.Creator<PlugInInfo> {
    a() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new PlugInInfo(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new PlugInInfo[i];
    }
}
