package com.tencent.feedback.common;

import android.content.Context;
import com.tencent.feedback.b.b;
import com.tencent.feedback.b.j;
import com.tencent.feedback.proguard.ac;
import com.tencent.feedback.proguard.p;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* compiled from: ProGuard */
public final class i implements b {
    private static i d = null;

    /* renamed from: a  reason: collision with root package name */
    private p f2546a;
    private p b;
    private Context c = null;

    private i(Context context) {
        Context applicationContext;
        if (!(context == null || (applicationContext = context.getApplicationContext()) == null)) {
            context = applicationContext;
        }
        this.c = context;
        j.a(this.c).a(this);
        c();
        if (d() > 0) {
            e();
        }
    }

    public static synchronized i a(Context context) {
        i iVar;
        synchronized (i.class) {
            if (d == null) {
                d = new i(context);
            }
            iVar = d;
        }
        return iVar;
    }

    public final void a(int i) {
    }

    public final void a(int i, int i2, long j, long j2, boolean z, String str) {
        g.a("rqdp{  req:}%d rqdp{  res:}%d rqdp{  send:}%d rqdp{  recv:}%d rqdp{  result:}%b rqdp{  msg:}%s", Integer.valueOf(i), Integer.valueOf(i2), Long.valueOf(j), Long.valueOf(j2), Boolean.valueOf(z), str);
        c();
        d();
        a(j, j2, j.a(this.c));
        e();
        g.b("rqdp{  [total:}%s]rqdp{  \n[today:}%s]", a(), b());
    }

    private synchronized p a() {
        return this.f2546a;
    }

    private synchronized void a(p pVar) {
        this.f2546a = pVar;
    }

    private synchronized p b() {
        d();
        return this.b;
    }

    private synchronized void b(p pVar) {
        this.b = pVar;
    }

    private void c() {
        List<p> a2 = ac.a(this.c);
        if (a2 != null) {
            for (p next : a2) {
                if (next.f2604a == 0) {
                    a(next);
                } else if (next.f2604a == 1) {
                    b(next);
                }
            }
        }
    }

    private synchronized int d() {
        int i;
        long c2 = ac.c();
        long time = new Date().getTime();
        int i2 = 0;
        if (this.b == null || this.b.b < c2) {
            this.b = new p(1, time, 0, 0, 0, 0, 0);
            i2 = 1;
        }
        if (this.f2546a == null) {
            this.f2546a = new p(0, time, 0, 0, 0, 0, 0);
            i = i2 + 1;
        } else {
            i = i2;
        }
        return i;
    }

    private synchronized void a(long j, long j2, boolean z) {
        long time = new Date().getTime();
        long j3 = j + j2;
        long j4 = z ? j3 : 0;
        if (z) {
            j3 = 0;
        }
        if (this.b == null) {
            this.b = new p(1, time, 1, j4, j3, j, j2);
        } else {
            long a2 = this.b.a();
            this.b = new p(1, this.b.b, 1 + this.b.c, this.b.d + j4, this.b.e + j3, this.b.f + j, this.b.g + j2);
            this.b.a(a2);
        }
        if (this.f2546a == null) {
            this.f2546a = new p(0, time, 1, j4, j3, j, j2);
        } else {
            long a3 = this.f2546a.a();
            this.f2546a = new p(0, this.f2546a.b, this.f2546a.c + 1, j4 + this.f2546a.d, j3 + this.f2546a.e, this.f2546a.f + j, this.f2546a.g + j2);
            this.f2546a.a(a3);
        }
    }

    private void e() {
        ArrayList arrayList = new ArrayList();
        p a2 = a();
        if (a2 != null) {
            arrayList.add(a2);
        }
        p b2 = b();
        if (b2 != null) {
            arrayList.add(b2);
        }
        if (arrayList.size() > 0) {
            ac.a(this.c, (p[]) arrayList.toArray(new p[arrayList.size()]));
        }
    }
}
