package com.tencent.feedback.common;

import android.os.Build;
import com.tencent.feedback.proguard.ac;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public final class k {
    private static k b = null;

    /* renamed from: a  reason: collision with root package name */
    private boolean f2548a;

    public static synchronized k a() {
        k kVar;
        synchronized (k.class) {
            if (b == null) {
                b = new k();
            }
            kVar = b;
        }
        return kVar;
    }

    protected k() {
        boolean z = true;
        this.f2548a = false;
        this.f2548a = true;
        String str = Build.TAGS;
        if (str == null || !str.contains("test-keys")) {
            z = false;
        } else {
            g.b("rqdp{  test-keys}", new Object[0]);
        }
        if (!z && !c() && !d()) {
            this.f2548a = false;
        }
    }

    public final synchronized boolean b() {
        return this.f2548a;
    }

    private static boolean c() {
        try {
            if (!new File("/system/app/Superuser.apk").exists()) {
                return false;
            }
            g.b("rqdp{  super_apk}", new Object[0]);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private static boolean d() {
        ArrayList<String> a2 = ac.a(new String[]{"/system/bin/sh", "-c", "type su"});
        if (a2 == null || a2.size() <= 0) {
            g.b("rqdp{  no response}", new Object[0]);
            return false;
        }
        Iterator<String> it = a2.iterator();
        while (it.hasNext()) {
            String next = it.next();
            g.b(next, new Object[0]);
            if (next.contains("not found")) {
                return false;
            }
        }
        g.b("rqdp{  sufile}", new Object[0]);
        return true;
    }
}
