package com.tencent.feedback.common;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Process;
import android.util.Log;
import com.tencent.assistant.st.STConst;
import com.tencent.connect.common.Constants;
import com.tencent.feedback.proguard.ac;
import java.util.List;

/* compiled from: ProGuard */
public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static String f2541a = null;
    private static Boolean b = null;
    private static Boolean c = null;
    private static boolean d = false;

    public static String a(Context context) {
        if (context == null) {
            return Constants.STR_EMPTY;
        }
        try {
            Object obj = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.get("APPKEY_DENGTA");
            if (obj != null) {
                return obj.toString();
            }
        } catch (Throwable th) {
            g.a("rqdp{  no appkey !!}", new Object[0]);
        }
        return Constants.STR_EMPTY;
    }

    public static String b(Context context) {
        if (context == null) {
            return null;
        }
        return context.getPackageName();
    }

    public static synchronized String c(Context context) {
        String str;
        int i = 0;
        synchronized (b.class) {
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context == null ? null : context.getPackageName(), 0);
                String str2 = packageInfo.versionName;
                int i2 = packageInfo.versionCode;
                if (str2 == null || str2.trim().length() <= 0) {
                    str = Constants.STR_EMPTY;
                } else {
                    char[] charArray = str2.toCharArray();
                    for (char c2 : charArray) {
                        if (c2 == '.') {
                            i++;
                        }
                    }
                    if (i < 3) {
                        str = str2 + "." + i2;
                    } else {
                        str = str2;
                    }
                    g.a("rqdp{  version:} %s", str);
                }
            } catch (Throwable th) {
                if (!g.a(th)) {
                    th.printStackTrace();
                }
                str = null;
            }
        }
        return str;
    }

    public static String d(Context context) {
        g.b("rqdp{AppInfo.getUUID() Start}", new Object[0]);
        if (context == null) {
            g.d("context == null", new Object[0]);
            return Constants.STR_EMPTY;
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo == null || applicationInfo.metaData == null) {
                g.d("appInfo == null || appInfo.metaData == null", new Object[0]);
                return Constants.STR_EMPTY;
            }
            Object obj = applicationInfo.metaData.get("com.tencent.rdm.uuid");
            String obj2 = obj != null ? obj.toString() : Constants.STR_EMPTY;
            Log.d("rqdp{ RDMUUID }:%s", obj2);
            return obj2;
        } catch (Throwable th) {
            if (!g.a(th)) {
                th.printStackTrace();
            }
            g.d(th.toString(), new Object[0]);
            return Constants.STR_EMPTY;
        }
    }

    public static synchronized boolean e(Context context) {
        boolean z = false;
        synchronized (b.class) {
            g.b("rqdp{  Read Log Permittion! start}", new Object[0]);
            if (context != null) {
                if (b == null) {
                    b = Boolean.valueOf(c(context, "android.permission.READ_LOGS"));
                }
                z = b.booleanValue();
            }
        }
        return z;
    }

    public static synchronized boolean f(Context context) {
        boolean z = false;
        synchronized (b.class) {
            g.b("rqdp{  Read write Permittion! start}", new Object[0]);
            if (context != null) {
                if (c == null) {
                    c = Boolean.valueOf(c(context, "android.permission.WRITE_EXTERNAL_STORAGE"));
                }
                z = c.booleanValue();
            }
        }
        return z;
    }

    public static boolean g(Context context) {
        return b(context, context.getPackageName());
    }

    private static boolean c(Context context, String str) {
        String str2;
        g.b("rqdp{  AppInfo.isContainReadLogPermission() start}", new Object[0]);
        if (context == null || str == null || str.trim().length() <= 0) {
            return false;
        }
        try {
            String[] strArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions;
            if (strArr != null) {
                for (String equals : strArr) {
                    if (str.equals(equals)) {
                        g.b("rqdp{  AppInfo.isContainReadLogPermission() end}", new Object[0]);
                        return true;
                    }
                }
            }
            return false;
        } catch (Throwable th) {
            if (!g.a(th)) {
                th.printStackTrace();
            }
            return false;
        } finally {
            str2 = "rqdp{  AppInfo.isContainReadLogPermission() end}";
            g.b(str2, new Object[0]);
        }
    }

    public static String a(Context context, String str) {
        if (context == null || str == null) {
            return null;
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(str, 0);
            if (applicationInfo != null) {
                return applicationInfo.sourceDir;
            }
            return null;
        } catch (Throwable th) {
            if (g.a(th)) {
                return null;
            }
            th.printStackTrace();
            return null;
        }
    }

    public static String h(Context context) {
        if (context == null) {
            return null;
        }
        String a2 = a(context, context.getPackageName());
        if (a2 != null) {
            return ac.a(a2);
        }
        g.d("rqdp{  No found the apk file on the device,please check it!}", new Object[0]);
        return null;
    }

    public static boolean b(Context context, String str) {
        if (context == null || str == null || str.trim().length() <= 0) {
            return false;
        }
        try {
            List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
            if (runningAppProcesses == null || runningAppProcesses.size() == 0) {
                g.b("rqdp{  no running proc}", new Object[0]);
                return false;
            }
            for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                if (next.importance == 100) {
                    for (String equals : next.pkgList) {
                        if (str.equals(equals)) {
                            g.b("rqdp{  current seen pn:}%s", next.processName);
                            return true;
                        }
                    }
                    continue;
                }
            }
            g.b("rqdp{  current unseen pn:}%s", str);
            return false;
        } catch (Throwable th) {
            if (!g.a(th)) {
                th.printStackTrace();
            }
        }
    }

    public static String a(Context context, int i) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        if (activityManager == null) {
            return STConst.ST_INSTALL_FAIL_STR_UNKNOWN;
        }
        for (ActivityManager.RunningAppProcessInfo next : activityManager.getRunningAppProcesses()) {
            if (next.pid == i) {
                return next.processName;
            }
        }
        return STConst.ST_INSTALL_FAIL_STR_UNKNOWN;
    }

    public static String i(Context context) {
        int i = -1;
        try {
            i = Process.myPid();
            return a(context, i);
        } catch (Throwable th) {
            if (!g.a(th)) {
                th.printStackTrace();
            }
            return i + ":" + th.getClass().getName() + ":" + th.getMessage();
        }
    }
}
