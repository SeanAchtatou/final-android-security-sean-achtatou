package com.tencent.feedback.proguard;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.tencent.feedback.common.g;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class aj {

    /* renamed from: a  reason: collision with root package name */
    private long f2587a = -1;
    private int b = -1;
    private int c = -1;
    private long d = -1;
    private byte[] e = null;
    private long f = 0;
    private String g = null;
    private int h = 0;
    private int i = 0;
    private int j = -1;

    /* JADX WARNING: Removed duplicated region for block: B:102:0x0219  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0144 A[Catch:{ all -> 0x0229 }] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x015f  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0164  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x01d2  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x01f1  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x01f6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<com.tencent.feedback.proguard.al> a(android.content.Context r14, java.lang.String r15, int r16, int r17) {
        /*
            if (r14 == 0) goto L_0x0004
            if (r17 != 0) goto L_0x0015
        L_0x0004:
            java.lang.String r1 = "rqdp{  FileDAO.query , context == null || numLimit == 0 , pls check! num:}%d"
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.Integer r4 = java.lang.Integer.valueOf(r17)
            r2[r3] = r4
            com.tencent.feedback.common.g.c(r1, r2)
            r1 = 0
        L_0x0014:
            return r1
        L_0x0015:
            java.lang.String r2 = "rqdp{  FileDAO.query() start query name:}%s rqdp{   , type}:%d rqdp{  , num:}%d "
            r1 = 3
            java.lang.Object[] r3 = new java.lang.Object[r1]
            r4 = 0
            if (r15 != 0) goto L_0x0081
            java.lang.String r1 = "any"
        L_0x001f:
            r3[r4] = r1
            r1 = 1
            java.lang.Integer r4 = java.lang.Integer.valueOf(r16)
            r3[r1] = r4
            r1 = 2
            java.lang.Integer r4 = java.lang.Integer.valueOf(r17)
            r3[r1] = r4
            com.tencent.feedback.common.g.b(r2, r3)
            r3 = 0
            r2 = 0
            r11 = 0
            r10 = 0
            com.tencent.feedback.proguard.n r12 = new com.tencent.feedback.proguard.n     // Catch:{ Throwable -> 0x0230, all -> 0x01d4 }
            r12.<init>(r14)     // Catch:{ Throwable -> 0x0230, all -> 0x01d4 }
            android.database.sqlite.SQLiteDatabase r1 = r12.getWritableDatabase()     // Catch:{ Throwable -> 0x0237, all -> 0x021b }
            if (r1 != 0) goto L_0x0083
            java.lang.String r2 = "rqdp{  getWritableDatabase fail, insert fail and return!}"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
            com.tencent.feedback.common.g.d(r2, r3)     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
            java.lang.String r2 = "rqdp{  Error:queryFile getWritableDatabase fail!}"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
            com.tencent.feedback.common.g.d(r2, r3)     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
            if (r1 == 0) goto L_0x005c
            boolean r2 = r1.isOpen()
            if (r2 == 0) goto L_0x005c
            r1.close()
        L_0x005c:
            r12.close()
            java.lang.String r1 = "rqdp{  FileDAO.query() end success} %d"
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            r4 = -1
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r2[r3] = r4
            com.tencent.feedback.common.g.b(r1, r2)
            java.lang.String r1 = "rqdp{  queryFile result num } %d"
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            r4 = -1
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r2[r3] = r4
            com.tencent.feedback.common.g.b(r1, r2)
            r1 = 0
            goto L_0x0014
        L_0x0081:
            r1 = r15
            goto L_0x001f
        L_0x0083:
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
            r2.<init>()     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
            if (r15 == 0) goto L_0x00a7
            int r3 = r2.length()     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
            if (r3 <= 0) goto L_0x0095
            java.lang.String r3 = " and "
            r2.append(r3)     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
        L_0x0095:
            java.lang.String r3 = "_n"
            r2.append(r3)     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
            java.lang.String r3 = " = '"
            r2.append(r3)     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
            r2.append(r15)     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
            java.lang.String r3 = "' "
            r2.append(r3)     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
        L_0x00a7:
            if (r16 < 0) goto L_0x00c3
            int r3 = r2.length()     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
            if (r3 <= 0) goto L_0x00b4
            java.lang.String r3 = " and "
            r2.append(r3)     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
        L_0x00b4:
            java.lang.String r3 = "_t"
            r2.append(r3)     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
            java.lang.String r3 = " = "
            r2.append(r3)     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
            r0 = r16
            r2.append(r0)     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
        L_0x00c3:
            int r3 = r2.length()     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
            if (r3 <= 0) goto L_0x0189
            java.lang.String r4 = r2.toString()     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
        L_0x00cd:
            java.lang.String r2 = "file"
            r3 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            if (r17 <= 0) goto L_0x018c
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
            r9.<init>()     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
            r0 = r17
            java.lang.StringBuilder r9 = r9.append(r0)     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
            java.lang.String r9 = r9.toString()     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
        L_0x00e5:
            android.database.Cursor r3 = r1.query(r2, r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Throwable -> 0x023e, all -> 0x0220 }
            if (r3 == 0) goto L_0x024d
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ Throwable -> 0x0246, all -> 0x0222 }
            r4.<init>()     // Catch:{ Throwable -> 0x0246, all -> 0x0222 }
        L_0x00f0:
            boolean r2 = r3.moveToNext()     // Catch:{ Throwable -> 0x0128, all -> 0x0225 }
            if (r2 == 0) goto L_0x018f
            com.tencent.feedback.proguard.al r2 = b(r3)     // Catch:{ Throwable -> 0x0128, all -> 0x0225 }
            if (r2 == 0) goto L_0x018f
            java.lang.String r5 = "rqdp{  queryFile: n:}%s rqdp{  , sha:}%s rqdp{  , tp:}%d rqdp{  ,arh:}%s"
            r6 = 4
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x0128, all -> 0x0225 }
            r7 = 0
            java.lang.String r8 = r2.a()     // Catch:{ Throwable -> 0x0128, all -> 0x0225 }
            r6[r7] = r8     // Catch:{ Throwable -> 0x0128, all -> 0x0225 }
            r7 = 1
            java.lang.String r8 = r2.d()     // Catch:{ Throwable -> 0x0128, all -> 0x0225 }
            r6[r7] = r8     // Catch:{ Throwable -> 0x0128, all -> 0x0225 }
            r7 = 2
            int r8 = r2.e()     // Catch:{ Throwable -> 0x0128, all -> 0x0225 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Throwable -> 0x0128, all -> 0x0225 }
            r6[r7] = r8     // Catch:{ Throwable -> 0x0128, all -> 0x0225 }
            r7 = 3
            java.lang.String r8 = r2.f()     // Catch:{ Throwable -> 0x0128, all -> 0x0225 }
            r6[r7] = r8     // Catch:{ Throwable -> 0x0128, all -> 0x0225 }
            com.tencent.feedback.common.g.b(r5, r6)     // Catch:{ Throwable -> 0x0128, all -> 0x0225 }
            r4.add(r2)     // Catch:{ Throwable -> 0x0128, all -> 0x0225 }
            goto L_0x00f0
        L_0x0128:
            r2 = move-exception
            r5 = r12
            r13 = r4
            r4 = r1
            r1 = r2
            r2 = r13
        L_0x012e:
            java.lang.String r6 = "rqdp{  Error in FileDAO.query() fail!}"
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ all -> 0x0229 }
            com.tencent.feedback.common.g.d(r6, r7)     // Catch:{ all -> 0x0229 }
            java.lang.String r6 = "rqdp{  Error:queryFile getWritableDatabase fail!}"
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ all -> 0x0229 }
            com.tencent.feedback.common.g.d(r6, r7)     // Catch:{ all -> 0x0229 }
            boolean r6 = com.tencent.feedback.common.g.a(r1)     // Catch:{ all -> 0x0229 }
            if (r6 != 0) goto L_0x0147
            r1.printStackTrace()     // Catch:{ all -> 0x0229 }
        L_0x0147:
            if (r3 == 0) goto L_0x0152
            boolean r1 = r3.isClosed()
            if (r1 != 0) goto L_0x0152
            r3.close()
        L_0x0152:
            if (r4 == 0) goto L_0x015d
            boolean r1 = r4.isOpen()
            if (r1 == 0) goto L_0x015d
            r4.close()
        L_0x015d:
            if (r5 == 0) goto L_0x0162
            r5.close()
        L_0x0162:
            if (r2 == 0) goto L_0x01d2
            int r1 = r2.size()
        L_0x0168:
            java.lang.String r3 = "rqdp{  FileDAO.query() end success} %d"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r5 = 0
            java.lang.Integer r6 = java.lang.Integer.valueOf(r1)
            r4[r5] = r6
            com.tencent.feedback.common.g.b(r3, r4)
            java.lang.String r3 = "rqdp{  queryFile result num } %d"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r5 = 0
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r4[r5] = r1
            com.tencent.feedback.common.g.b(r3, r4)
            r1 = r2
            goto L_0x0014
        L_0x0189:
            r4 = 0
            goto L_0x00cd
        L_0x018c:
            r9 = 0
            goto L_0x00e5
        L_0x018f:
            r2 = r4
        L_0x0190:
            if (r3 == 0) goto L_0x019b
            boolean r4 = r3.isClosed()
            if (r4 != 0) goto L_0x019b
            r3.close()
        L_0x019b:
            if (r1 == 0) goto L_0x01a6
            boolean r3 = r1.isOpen()
            if (r3 == 0) goto L_0x01a6
            r1.close()
        L_0x01a6:
            r12.close()
            if (r2 == 0) goto L_0x01d0
            int r1 = r2.size()
        L_0x01af:
            java.lang.String r3 = "rqdp{  FileDAO.query() end success} %d"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r5 = 0
            java.lang.Integer r6 = java.lang.Integer.valueOf(r1)
            r4[r5] = r6
            com.tencent.feedback.common.g.b(r3, r4)
            java.lang.String r3 = "rqdp{  queryFile result num } %d"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r5 = 0
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r4[r5] = r1
            com.tencent.feedback.common.g.b(r3, r4)
            r1 = r2
            goto L_0x0014
        L_0x01d0:
            r1 = -1
            goto L_0x01af
        L_0x01d2:
            r1 = -1
            goto L_0x0168
        L_0x01d4:
            r1 = move-exception
            r12 = r3
            r13 = r2
            r2 = r1
            r1 = r13
        L_0x01d9:
            if (r10 == 0) goto L_0x01e4
            boolean r3 = r10.isClosed()
            if (r3 != 0) goto L_0x01e4
            r10.close()
        L_0x01e4:
            if (r1 == 0) goto L_0x01ef
            boolean r3 = r1.isOpen()
            if (r3 == 0) goto L_0x01ef
            r1.close()
        L_0x01ef:
            if (r12 == 0) goto L_0x01f4
            r12.close()
        L_0x01f4:
            if (r11 == 0) goto L_0x0219
            int r1 = r11.size()
        L_0x01fa:
            java.lang.String r3 = "rqdp{  FileDAO.query() end success} %d"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r5 = 0
            java.lang.Integer r6 = java.lang.Integer.valueOf(r1)
            r4[r5] = r6
            com.tencent.feedback.common.g.b(r3, r4)
            java.lang.String r3 = "rqdp{  queryFile result num } %d"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r5 = 0
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r4[r5] = r1
            com.tencent.feedback.common.g.b(r3, r4)
            throw r2
        L_0x0219:
            r1 = -1
            goto L_0x01fa
        L_0x021b:
            r1 = move-exception
            r13 = r1
            r1 = r2
            r2 = r13
            goto L_0x01d9
        L_0x0220:
            r2 = move-exception
            goto L_0x01d9
        L_0x0222:
            r2 = move-exception
            r10 = r3
            goto L_0x01d9
        L_0x0225:
            r2 = move-exception
            r10 = r3
            r11 = r4
            goto L_0x01d9
        L_0x0229:
            r1 = move-exception
            r10 = r3
            r11 = r2
            r12 = r5
            r2 = r1
            r1 = r4
            goto L_0x01d9
        L_0x0230:
            r1 = move-exception
            r4 = r2
            r5 = r3
            r3 = r10
            r2 = r11
            goto L_0x012e
        L_0x0237:
            r1 = move-exception
            r3 = r10
            r4 = r2
            r5 = r12
            r2 = r11
            goto L_0x012e
        L_0x023e:
            r2 = move-exception
            r3 = r10
            r4 = r1
            r5 = r12
            r1 = r2
            r2 = r11
            goto L_0x012e
        L_0x0246:
            r2 = move-exception
            r4 = r1
            r5 = r12
            r1 = r2
            r2 = r11
            goto L_0x012e
        L_0x024d:
            r2 = r11
            goto L_0x0190
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.aj.a(android.content.Context, java.lang.String, int, int):java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00bf  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r11, java.util.List<com.tencent.feedback.proguard.aj> r12) {
        /*
            r0 = 0
            r1 = 1
            r2 = 0
            java.lang.String r3 = "rqdp{  AnalyticsDAO.insert() start}"
            java.lang.Object[] r4 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.b(r3, r4)
            if (r11 == 0) goto L_0x000e
            if (r12 != 0) goto L_0x0017
        L_0x000e:
            java.lang.String r0 = "rqdp{  AnalyticsDAO.insert() have null args}"
            java.lang.Object[] r1 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.d(r0, r1)
            r0 = r2
        L_0x0016:
            return r0
        L_0x0017:
            int r3 = r12.size()
            if (r3 > 0) goto L_0x0026
            java.lang.String r0 = "rqdp{  list size == 0 return true}"
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.b(r0, r2)
            r0 = r1
            goto L_0x0016
        L_0x0026:
            com.tencent.feedback.proguard.n r4 = new com.tencent.feedback.proguard.n     // Catch:{ Throwable -> 0x008a, all -> 0x00ae }
            r4.<init>(r11)     // Catch:{ Throwable -> 0x008a, all -> 0x00ae }
            android.database.sqlite.SQLiteDatabase r3 = r4.getWritableDatabase()     // Catch:{ Throwable -> 0x00d6, all -> 0x00ca }
            r5 = r2
        L_0x0030:
            int r0 = r12.size()     // Catch:{ Throwable -> 0x00d9, all -> 0x00ce }
            if (r5 >= r0) goto L_0x0073
            java.lang.Object r0 = r12.get(r5)     // Catch:{ Throwable -> 0x00d9, all -> 0x00ce }
            com.tencent.feedback.proguard.aj r0 = (com.tencent.feedback.proguard.aj) r0     // Catch:{ Throwable -> 0x00d9, all -> 0x00ce }
            android.content.ContentValues r6 = a(r0)     // Catch:{ Throwable -> 0x00d9, all -> 0x00ce }
            java.lang.String r7 = "ao"
            java.lang.String r8 = "_id"
            long r6 = r3.insert(r7, r8, r6)     // Catch:{ Throwable -> 0x00d9, all -> 0x00ce }
            r8 = 0
            int r8 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r8 >= 0) goto L_0x006d
            java.lang.String r0 = "rqdp{  AnalyticsDAO.insert() failure! return}"
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Throwable -> 0x00d9, all -> 0x00ce }
            com.tencent.feedback.common.g.d(r0, r1)     // Catch:{ Throwable -> 0x00d9, all -> 0x00ce }
            if (r3 == 0) goto L_0x0061
            boolean r0 = r3.isOpen()
            if (r0 == 0) goto L_0x0061
            r3.close()
        L_0x0061:
            r4.close()
            java.lang.String r0 = "rqdp{  AnalyticsDAO.insert() end}"
            java.lang.Object[] r1 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.b(r0, r1)
            r0 = r2
            goto L_0x0016
        L_0x006d:
            r0.f2587a = r6     // Catch:{ Throwable -> 0x00d9, all -> 0x00ce }
            int r0 = r5 + 1
            r5 = r0
            goto L_0x0030
        L_0x0073:
            if (r3 == 0) goto L_0x007e
            boolean r0 = r3.isOpen()
            if (r0 == 0) goto L_0x007e
            r3.close()
        L_0x007e:
            r4.close()
            java.lang.String r0 = "rqdp{  AnalyticsDAO.insert() end}"
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.b(r0, r2)
            r0 = r1
            goto L_0x0016
        L_0x008a:
            r1 = move-exception
            r1 = r0
        L_0x008c:
            java.lang.String r3 = "AnalyticsDAO.insert() failure!"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x00d0 }
            com.tencent.feedback.common.g.b(r3, r4)     // Catch:{ all -> 0x00d0 }
            if (r0 == 0) goto L_0x009f
            boolean r3 = r0.isOpen()
            if (r3 == 0) goto L_0x009f
            r0.close()
        L_0x009f:
            if (r1 == 0) goto L_0x00a4
            r1.close()
        L_0x00a4:
            java.lang.String r0 = "rqdp{  AnalyticsDAO.insert() end}"
            java.lang.Object[] r1 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.b(r0, r1)
            r0 = r2
            goto L_0x0016
        L_0x00ae:
            r1 = move-exception
            r3 = r0
            r4 = r0
            r0 = r1
        L_0x00b2:
            if (r3 == 0) goto L_0x00bd
            boolean r1 = r3.isOpen()
            if (r1 == 0) goto L_0x00bd
            r3.close()
        L_0x00bd:
            if (r4 == 0) goto L_0x00c2
            r4.close()
        L_0x00c2:
            java.lang.String r1 = "rqdp{  AnalyticsDAO.insert() end}"
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.b(r1, r2)
            throw r0
        L_0x00ca:
            r1 = move-exception
            r3 = r0
            r0 = r1
            goto L_0x00b2
        L_0x00ce:
            r0 = move-exception
            goto L_0x00b2
        L_0x00d0:
            r3 = move-exception
            r4 = r1
            r10 = r0
            r0 = r3
            r3 = r10
            goto L_0x00b2
        L_0x00d6:
            r1 = move-exception
            r1 = r4
            goto L_0x008c
        L_0x00d9:
            r0 = move-exception
            r0 = r3
            r1 = r4
            goto L_0x008c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.aj.a(android.content.Context, java.util.List):boolean");
    }

    public aj() {
    }

    public aj(int i2, int i3, long j2, byte[] bArr) {
        this.b = i2;
        this.c = 0;
        this.d = j2;
        this.e = bArr;
        if (bArr != null) {
            this.f = (long) bArr.length;
        }
    }

    public final long a() {
        return this.f2587a;
    }

    public final aj a(long j2) {
        this.f2587a = j2;
        return this;
    }

    public final byte[] b() {
        return this.e;
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x012c A[Catch:{ all -> 0x0197 }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x013c  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0184  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int c(android.content.Context r12, java.util.List<com.tencent.feedback.proguard.al> r13) {
        /*
            if (r12 == 0) goto L_0x000a
            if (r13 == 0) goto L_0x000a
            int r0 = r13.size()
            if (r0 != 0) goto L_0x002d
        L_0x000a:
            java.lang.String r1 = "rqdp{  FileDAO.insert, context == null || fileList == null || fileList.size() , pls check! fileList }%s "
            r0 = 1
            java.lang.Object[] r2 = new java.lang.Object[r0]
            r3 = 0
            if (r13 != 0) goto L_0x001b
            java.lang.String r0 = "null"
        L_0x0014:
            r2[r3] = r0
            com.tencent.feedback.common.g.c(r1, r2)
            r1 = -1
        L_0x001a:
            return r1
        L_0x001b:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            int r4 = r13.size()
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r0 = r0.toString()
            goto L_0x0014
        L_0x002d:
            java.lang.String r0 = "rqdp{  FileDAO.insert() start num:}%d"
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]
            r2 = 0
            int r3 = r13.size()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r1[r2] = r3
            com.tencent.feedback.common.g.b(r0, r1)
            r4 = 0
            r2 = 0
            r1 = 0
            com.tencent.feedback.proguard.n r3 = new com.tencent.feedback.proguard.n     // Catch:{ Throwable -> 0x0199, all -> 0x0175 }
            r3.<init>(r12)     // Catch:{ Throwable -> 0x0199, all -> 0x0175 }
            android.database.sqlite.SQLiteDatabase r2 = r3.getWritableDatabase()     // Catch:{ Throwable -> 0x010e }
            if (r2 != 0) goto L_0x007e
            java.lang.String r0 = "rqdp{  getWritableDatabase fail, insert fail and return!}"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x010e }
            com.tencent.feedback.common.g.d(r0, r4)     // Catch:{ Throwable -> 0x010e }
            java.lang.String r0 = "rqdp{  Error:insertFile getWritableDatabase fail!}"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x010e }
            com.tencent.feedback.common.g.b(r0, r4)     // Catch:{ Throwable -> 0x010e }
            if (r2 == 0) goto L_0x0069
            boolean r0 = r2.isOpen()
            if (r0 == 0) goto L_0x0069
            r2.close()
        L_0x0069:
            r3.close()
            java.lang.String r0 = "rqdp{  FileDAO.insert() end success }%d"
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]
            r2 = 0
            r3 = 0
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r1[r2] = r3
            com.tencent.feedback.common.g.b(r0, r1)
            r1 = -1
            goto L_0x001a
        L_0x007e:
            java.util.Iterator r5 = r13.iterator()     // Catch:{ Throwable -> 0x010e }
        L_0x0082:
            boolean r0 = r5.hasNext()     // Catch:{ Throwable -> 0x010e }
            if (r0 == 0) goto L_0x0156
            java.lang.Object r0 = r5.next()     // Catch:{ Throwable -> 0x010e }
            com.tencent.feedback.proguard.al r0 = (com.tencent.feedback.proguard.al) r0     // Catch:{ Throwable -> 0x010e }
            if (r0 != 0) goto L_0x00c6
            r4 = 0
        L_0x0091:
            if (r4 == 0) goto L_0x00c3
            java.lang.String r6 = "file"
            java.lang.String r7 = "_id"
            long r6 = r2.insert(r6, r7, r4)     // Catch:{ Throwable -> 0x010e }
            r0.a(r6)     // Catch:{ Throwable -> 0x010e }
            r8 = 0
            int r4 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r4 < 0) goto L_0x0150
            r4 = 1
        L_0x00a5:
            int r1 = r1 + r4
            java.lang.String r4 = "rqdp{  insertFile name:}%s rqdp{  result:}%b"
            r8 = 2
            java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x010e }
            r9 = 0
            java.lang.String r0 = r0.a()     // Catch:{ Throwable -> 0x010e }
            r8[r9] = r0     // Catch:{ Throwable -> 0x010e }
            r9 = 1
            r10 = 0
            int r0 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
            if (r0 < 0) goto L_0x0153
            r0 = 1
        L_0x00ba:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ Throwable -> 0x010e }
            r8[r9] = r0     // Catch:{ Throwable -> 0x010e }
            com.tencent.feedback.common.g.b(r4, r8)     // Catch:{ Throwable -> 0x010e }
        L_0x00c3:
            r0 = r1
            r1 = r0
            goto L_0x0082
        L_0x00c6:
            android.content.ContentValues r4 = new android.content.ContentValues     // Catch:{ Throwable -> 0x010e }
            r4.<init>()     // Catch:{ Throwable -> 0x010e }
            java.lang.String r6 = "_n"
            java.lang.String r7 = r0.a()     // Catch:{ Throwable -> 0x010e }
            r4.put(r6, r7)     // Catch:{ Throwable -> 0x010e }
            java.lang.String r6 = "_sa"
            java.lang.String r7 = r0.d()     // Catch:{ Throwable -> 0x010e }
            r4.put(r6, r7)     // Catch:{ Throwable -> 0x010e }
            java.lang.String r6 = "_sz"
            long r7 = r0.c()     // Catch:{ Throwable -> 0x010e }
            java.lang.Long r7 = java.lang.Long.valueOf(r7)     // Catch:{ Throwable -> 0x010e }
            r4.put(r6, r7)     // Catch:{ Throwable -> 0x010e }
            java.lang.String r6 = "_ut"
            long r7 = r0.b()     // Catch:{ Throwable -> 0x010e }
            java.lang.Long r7 = java.lang.Long.valueOf(r7)     // Catch:{ Throwable -> 0x010e }
            r4.put(r6, r7)     // Catch:{ Throwable -> 0x010e }
            java.lang.String r6 = "_t"
            int r7 = r0.e()     // Catch:{ Throwable -> 0x010e }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Throwable -> 0x010e }
            r4.put(r6, r7)     // Catch:{ Throwable -> 0x010e }
            java.lang.String r6 = "_ac"
            java.lang.String r7 = r0.f()     // Catch:{ Throwable -> 0x010e }
            r4.put(r6, r7)     // Catch:{ Throwable -> 0x010e }
            goto L_0x0091
        L_0x010e:
            r0 = move-exception
        L_0x010f:
            java.lang.String r4 = "rqdp{  Error in FileDAO insert!}"
            r5 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x0197 }
            com.tencent.feedback.common.g.d(r4, r5)     // Catch:{ all -> 0x0197 }
            java.lang.String r4 = "rqdp{  Error:insertFile }%s"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x0197 }
            r6 = 0
            java.lang.String r7 = r0.getMessage()     // Catch:{ all -> 0x0197 }
            r5[r6] = r7     // Catch:{ all -> 0x0197 }
            com.tencent.feedback.common.g.d(r4, r5)     // Catch:{ all -> 0x0197 }
            boolean r4 = com.tencent.feedback.common.g.a(r0)     // Catch:{ all -> 0x0197 }
            if (r4 != 0) goto L_0x012f
            r0.printStackTrace()     // Catch:{ all -> 0x0197 }
        L_0x012f:
            if (r2 == 0) goto L_0x013a
            boolean r0 = r2.isOpen()
            if (r0 == 0) goto L_0x013a
            r2.close()
        L_0x013a:
            if (r3 == 0) goto L_0x013f
            r3.close()
        L_0x013f:
            java.lang.String r0 = "rqdp{  FileDAO.insert() end success }%d"
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.Integer r4 = java.lang.Integer.valueOf(r1)
            r2[r3] = r4
            com.tencent.feedback.common.g.b(r0, r2)
            goto L_0x001a
        L_0x0150:
            r4 = 0
            goto L_0x00a5
        L_0x0153:
            r0 = 0
            goto L_0x00ba
        L_0x0156:
            if (r2 == 0) goto L_0x0161
            boolean r0 = r2.isOpen()
            if (r0 == 0) goto L_0x0161
            r2.close()
        L_0x0161:
            r3.close()
            java.lang.String r0 = "rqdp{  FileDAO.insert() end success }%d"
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.Integer r4 = java.lang.Integer.valueOf(r1)
            r2[r3] = r4
            com.tencent.feedback.common.g.b(r0, r2)
            goto L_0x001a
        L_0x0175:
            r0 = move-exception
            r3 = r4
        L_0x0177:
            if (r2 == 0) goto L_0x0182
            boolean r4 = r2.isOpen()
            if (r4 == 0) goto L_0x0182
            r2.close()
        L_0x0182:
            if (r3 == 0) goto L_0x0187
            r3.close()
        L_0x0187:
            java.lang.String r2 = "rqdp{  FileDAO.insert() end success }%d"
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r4 = 0
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r3[r4] = r1
            com.tencent.feedback.common.g.b(r2, r3)
            throw r0
        L_0x0197:
            r0 = move-exception
            goto L_0x0177
        L_0x0199:
            r0 = move-exception
            r3 = r4
            goto L_0x010f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.aj.c(android.content.Context, java.util.List):int");
    }

    public final aj a(String str) {
        this.g = str;
        return this;
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x011e  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x013b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(android.content.Context r7, int[] r8, long r9, long r11, int r13, int r14) {
        /*
            r4 = -1
            r5 = 0
            r1 = 0
            java.lang.String r0 = "rqdp{  AnalyticsDAO.delete() start}"
            java.lang.Object[] r2 = new java.lang.Object[r1]
            com.tencent.feedback.common.g.b(r0, r2)
            if (r7 != 0) goto L_0x0015
            java.lang.String r0 = "rqdp{  deleteEup() context is null arg}"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.feedback.common.g.a(r0, r1)
            r1 = r4
        L_0x0014:
            return r1
        L_0x0015:
            int r0 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r0 > 0) goto L_0x0014
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r2 = "_time >= "
            r0.<init>(r2)
            java.lang.StringBuilder r0 = r0.append(r9)
            java.lang.String r2 = " and _time"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = " <= "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.StringBuilder r0 = r0.append(r11)
            java.lang.String r3 = r0.toString()
            if (r13 < 0) goto L_0x0051
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r2 = " and _upCounts >= "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.StringBuilder r0 = r0.append(r13)
            java.lang.String r3 = r0.toString()
        L_0x0051:
            if (r14 < 0) goto L_0x006a
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r2 = " and _state = "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.StringBuilder r0 = r0.append(r14)
            java.lang.String r3 = r0.toString()
        L_0x006a:
            if (r8 == 0) goto L_0x0155
            int r0 = r8.length
            if (r0 <= 0) goto L_0x0155
            java.lang.String r0 = ""
            r2 = r0
            r0 = r1
        L_0x0073:
            int r6 = r8.length
            if (r0 >= r6) goto L_0x0092
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.StringBuilder r2 = r6.append(r2)
            java.lang.String r6 = " or _type = "
            java.lang.StringBuilder r2 = r2.append(r6)
            r6 = r8[r0]
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r2 = r2.toString()
            int r0 = r0 + 1
            goto L_0x0073
        L_0x0092:
            r0 = 4
            java.lang.String r0 = r2.substring(r0)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = " and ( "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = " )"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
        L_0x00b4:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "rqdp{  delete where: }"
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            java.lang.Object[] r3 = new java.lang.Object[r1]
            com.tencent.feedback.common.g.b(r2, r3)
            com.tencent.feedback.proguard.n r3 = new com.tencent.feedback.proguard.n     // Catch:{ Throwable -> 0x0105, all -> 0x012b }
            r3.<init>(r7)     // Catch:{ Throwable -> 0x0105, all -> 0x012b }
            android.database.sqlite.SQLiteDatabase r2 = r3.getWritableDatabase()     // Catch:{ Throwable -> 0x014e, all -> 0x0146 }
            java.lang.String r5 = "ao"
            r6 = 0
            int r0 = r2.delete(r5, r0, r6)     // Catch:{ Throwable -> 0x0152, all -> 0x0149 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0152, all -> 0x0149 }
            java.lang.String r6 = "rqdp{  deleted num: }"
            r5.<init>(r6)     // Catch:{ Throwable -> 0x0152, all -> 0x0149 }
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Throwable -> 0x0152, all -> 0x0149 }
            java.lang.String r5 = r5.toString()     // Catch:{ Throwable -> 0x0152, all -> 0x0149 }
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x0152, all -> 0x0149 }
            com.tencent.feedback.common.g.b(r5, r6)     // Catch:{ Throwable -> 0x0152, all -> 0x0149 }
            if (r2 == 0) goto L_0x00f8
            boolean r4 = r2.isOpen()
            if (r4 == 0) goto L_0x00f8
            r2.close()
        L_0x00f8:
            r3.close()
            java.lang.String r2 = "rqdp{  AnalyticsDAO.delete() end}"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.feedback.common.g.b(r2, r1)
            r1 = r0
            goto L_0x0014
        L_0x0105:
            r0 = move-exception
            r2 = r5
        L_0x0107:
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x014b }
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x014b }
            com.tencent.feedback.common.g.b(r0, r3)     // Catch:{ all -> 0x014b }
            if (r2 == 0) goto L_0x011c
            boolean r0 = r2.isOpen()
            if (r0 == 0) goto L_0x011c
            r2.close()
        L_0x011c:
            if (r5 == 0) goto L_0x0121
            r5.close()
        L_0x0121:
            java.lang.String r0 = "rqdp{  AnalyticsDAO.delete() end}"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.feedback.common.g.b(r0, r1)
            r1 = r4
            goto L_0x0014
        L_0x012b:
            r0 = move-exception
            r2 = r5
            r3 = r5
        L_0x012e:
            if (r2 == 0) goto L_0x0139
            boolean r4 = r2.isOpen()
            if (r4 == 0) goto L_0x0139
            r2.close()
        L_0x0139:
            if (r3 == 0) goto L_0x013e
            r3.close()
        L_0x013e:
            java.lang.String r2 = "rqdp{  AnalyticsDAO.delete() end}"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.feedback.common.g.b(r2, r1)
            throw r0
        L_0x0146:
            r0 = move-exception
            r2 = r5
            goto L_0x012e
        L_0x0149:
            r0 = move-exception
            goto L_0x012e
        L_0x014b:
            r0 = move-exception
            r3 = r5
            goto L_0x012e
        L_0x014e:
            r0 = move-exception
            r2 = r5
            r5 = r3
            goto L_0x0107
        L_0x0152:
            r0 = move-exception
            r5 = r3
            goto L_0x0107
        L_0x0155:
            r0 = r3
            goto L_0x00b4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.aj.a(android.content.Context, int[], long, long, int, int):int");
    }

    public final synchronized int c() {
        return this.h;
    }

    public final synchronized aj a(int i2) {
        this.h = i2;
        return this;
    }

    public final synchronized int d() {
        return this.i;
    }

    public final synchronized aj b(int i2) {
        this.i = i2;
        return this;
    }

    public final synchronized int e() {
        return this.j;
    }

    public final synchronized aj c(int i2) {
        this.j = i2;
        return this;
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c8 A[Catch:{ all -> 0x0160 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00ef  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x014c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int d(android.content.Context r11, java.util.List<com.tencent.feedback.proguard.al> r12) {
        /*
            r1 = -1
            r3 = 0
            r9 = 1
            r2 = 0
            if (r11 == 0) goto L_0x000e
            if (r12 == 0) goto L_0x000e
            int r0 = r12.size()
            if (r0 != 0) goto L_0x002f
        L_0x000e:
            java.lang.String r3 = "rqdp{  FileDAO.delete, context == null || fileList == null || fileList.size() , pls check! fileList }%s "
            java.lang.Object[] r4 = new java.lang.Object[r9]
            if (r12 != 0) goto L_0x001d
            java.lang.String r0 = "null"
        L_0x0016:
            r4[r2] = r0
            com.tencent.feedback.common.g.c(r3, r4)
            r0 = r1
        L_0x001c:
            return r0
        L_0x001d:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            int r5 = r12.size()
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.String r0 = r0.toString()
            goto L_0x0016
        L_0x002f:
            java.lang.String r0 = "rqdp{  FileDAO.delete() start num:}%d"
            java.lang.Object[] r4 = new java.lang.Object[r9]
            int r5 = r12.size()
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r4[r2] = r5
            com.tencent.feedback.common.g.b(r0, r4)
            com.tencent.feedback.proguard.n r4 = new com.tencent.feedback.proguard.n     // Catch:{ Throwable -> 0x0165, all -> 0x013c }
            r4.<init>(r11)     // Catch:{ Throwable -> 0x0165, all -> 0x013c }
            android.database.sqlite.SQLiteDatabase r3 = r4.getWritableDatabase()     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            if (r3 != 0) goto L_0x0078
            java.lang.String r0 = "rqdp{  getWritableDatabase fail, delete fail and return!}"
            r5 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            com.tencent.feedback.common.g.d(r0, r5)     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            java.lang.String r0 = "rqdp{  Error:fileDelete getWritableDatabase fail!}"
            r5 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            com.tencent.feedback.common.g.d(r0, r5)     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            if (r3 == 0) goto L_0x0066
            boolean r0 = r3.isOpen()
            if (r0 == 0) goto L_0x0066
            r3.close()
        L_0x0066:
            r4.close()
            java.lang.String r0 = "rqdp{  FileDAO.delete() end success} %d"
            java.lang.Object[] r3 = new java.lang.Object[r9]
            java.lang.Integer r4 = java.lang.Integer.valueOf(r2)
            r3[r2] = r4
            com.tencent.feedback.common.g.b(r0, r3)
            r0 = r1
            goto L_0x001c
        L_0x0078:
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            r1.<init>()     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            java.util.Iterator r5 = r12.iterator()     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
        L_0x0081:
            boolean r0 = r5.hasNext()     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            if (r0 == 0) goto L_0x0101
            java.lang.Object r0 = r5.next()     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            com.tencent.feedback.proguard.al r0 = (com.tencent.feedback.proguard.al) r0     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            java.lang.String r6 = "or ("
            r1.append(r6)     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            java.lang.String r6 = "_n"
            r1.append(r6)     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            java.lang.String r6 = " = '"
            r1.append(r6)     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            java.lang.String r6 = r0.a()     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            r1.append(r6)     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            java.lang.String r6 = "' and "
            r1.append(r6)     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            java.lang.String r6 = "_t"
            r1.append(r6)     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            java.lang.String r6 = " = "
            r1.append(r6)     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            int r0 = r0.e()     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            r1.append(r0)     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            java.lang.String r0 = " ) "
            r1.append(r0)     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            goto L_0x0081
        L_0x00bf:
            r0 = move-exception
            r1 = r0
            r0 = r2
        L_0x00c2:
            boolean r5 = com.tencent.feedback.common.g.a(r1)     // Catch:{ all -> 0x0160 }
            if (r5 != 0) goto L_0x00cb
            r1.printStackTrace()     // Catch:{ all -> 0x0160 }
        L_0x00cb:
            java.lang.String r5 = "rqdp{  Error in FileDAO delete!}"
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ all -> 0x0160 }
            com.tencent.feedback.common.g.d(r5, r6)     // Catch:{ all -> 0x0160 }
            java.lang.String r5 = "rqdp{  Error:fileDelete} %s"
            r6 = 1
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ all -> 0x0160 }
            r7 = 0
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x0160 }
            r6[r7] = r1     // Catch:{ all -> 0x0160 }
            com.tencent.feedback.common.g.d(r5, r6)     // Catch:{ all -> 0x0160 }
            if (r3 == 0) goto L_0x00ed
            boolean r1 = r3.isOpen()
            if (r1 == 0) goto L_0x00ed
            r3.close()
        L_0x00ed:
            if (r4 == 0) goto L_0x00f2
            r4.close()
        L_0x00f2:
            java.lang.String r1 = "rqdp{  FileDAO.delete() end success} %d"
            java.lang.Object[] r3 = new java.lang.Object[r9]
            java.lang.Integer r4 = java.lang.Integer.valueOf(r0)
            r3[r2] = r4
            com.tencent.feedback.common.g.b(r1, r3)
            goto L_0x001c
        L_0x0101:
            r0 = 2
            java.lang.String r1 = r1.substring(r0)     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            java.lang.String r0 = "file"
            r5 = 0
            int r0 = r3.delete(r0, r1, r5)     // Catch:{ Throwable -> 0x00bf, all -> 0x015d }
            java.lang.String r5 = "rqdp{  fileDelete deletedNum:%d deleted List:\n}%s"
            r6 = 2
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x016b }
            r7 = 0
            java.lang.Integer r8 = java.lang.Integer.valueOf(r0)     // Catch:{ Throwable -> 0x016b }
            r6[r7] = r8     // Catch:{ Throwable -> 0x016b }
            r7 = 1
            r6[r7] = r1     // Catch:{ Throwable -> 0x016b }
            com.tencent.feedback.common.g.b(r5, r6)     // Catch:{ Throwable -> 0x016b }
            if (r3 == 0) goto L_0x012a
            boolean r1 = r3.isOpen()
            if (r1 == 0) goto L_0x012a
            r3.close()
        L_0x012a:
            r4.close()
            java.lang.String r1 = "rqdp{  FileDAO.delete() end success} %d"
            java.lang.Object[] r3 = new java.lang.Object[r9]
            java.lang.Integer r4 = java.lang.Integer.valueOf(r0)
            r3[r2] = r4
            com.tencent.feedback.common.g.b(r1, r3)
            goto L_0x001c
        L_0x013c:
            r0 = move-exception
            r1 = r2
            r4 = r3
        L_0x013f:
            if (r3 == 0) goto L_0x014a
            boolean r5 = r3.isOpen()
            if (r5 == 0) goto L_0x014a
            r3.close()
        L_0x014a:
            if (r4 == 0) goto L_0x014f
            r4.close()
        L_0x014f:
            java.lang.String r3 = "rqdp{  FileDAO.delete() end success} %d"
            java.lang.Object[] r4 = new java.lang.Object[r9]
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r4[r2] = r1
            com.tencent.feedback.common.g.b(r3, r4)
            throw r0
        L_0x015d:
            r0 = move-exception
            r1 = r2
            goto L_0x013f
        L_0x0160:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x013f
        L_0x0165:
            r0 = move-exception
            r1 = r0
            r4 = r3
            r0 = r2
            goto L_0x00c2
        L_0x016b:
            r1 = move-exception
            goto L_0x00c2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.aj.d(android.content.Context, java.util.List):int");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v1, resolved type: com.tencent.feedback.proguard.n} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v2, resolved type: com.tencent.feedback.proguard.n} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v3, resolved type: com.tencent.feedback.proguard.n} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v4, resolved type: com.tencent.feedback.proguard.n} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v6, resolved type: com.tencent.feedback.proguard.n} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v7, resolved type: com.tencent.feedback.proguard.n} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0103  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(android.content.Context r11, java.lang.Long[] r12) {
        /*
            r1 = -1
            r3 = 0
            r2 = 0
            java.lang.String r0 = "rqdp{  AnalyticsDAO.deleteList() start!}"
            java.lang.Object[] r4 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.b(r0, r4)
            if (r11 != 0) goto L_0x0015
            java.lang.String r0 = "rqdp{  deleteList() have null args!}"
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.d(r0, r2)
            r0 = r1
        L_0x0014:
            return r0
        L_0x0015:
            if (r12 == 0) goto L_0x001a
            int r0 = r12.length
            if (r0 > 0) goto L_0x001c
        L_0x001a:
            r0 = r2
            goto L_0x0014
        L_0x001c:
            com.tencent.feedback.proguard.n r4 = new com.tencent.feedback.proguard.n     // Catch:{ Throwable -> 0x00ce, all -> 0x00f4 }
            r4.<init>(r11)     // Catch:{ Throwable -> 0x00ce, all -> 0x00f4 }
            android.database.sqlite.SQLiteDatabase r3 = r4.getWritableDatabase()     // Catch:{ Throwable -> 0x0110 }
            java.lang.StringBuffer r6 = new java.lang.StringBuffer     // Catch:{ Throwable -> 0x0110 }
            r6.<init>()     // Catch:{ Throwable -> 0x0110 }
            r0 = r2
            r5 = r2
        L_0x002c:
            int r7 = r12.length     // Catch:{ Throwable -> 0x0110 }
            if (r5 >= r7) goto L_0x008b
            r7 = r12[r5]     // Catch:{ Throwable -> 0x0110 }
            long r7 = r7.longValue()     // Catch:{ Throwable -> 0x0110 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0110 }
            java.lang.String r10 = " or  _id = "
            r9.<init>(r10)     // Catch:{ Throwable -> 0x0110 }
            java.lang.StringBuilder r7 = r9.append(r7)     // Catch:{ Throwable -> 0x0110 }
            java.lang.String r7 = r7.toString()     // Catch:{ Throwable -> 0x0110 }
            r6.append(r7)     // Catch:{ Throwable -> 0x0110 }
            if (r5 <= 0) goto L_0x0088
            int r7 = r5 % 50
            if (r7 != 0) goto L_0x0088
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0110 }
            java.lang.String r8 = "rqdp{  current }"
            r7.<init>(r8)     // Catch:{ Throwable -> 0x0110 }
            java.lang.StringBuilder r7 = r7.append(r5)     // Catch:{ Throwable -> 0x0110 }
            java.lang.String r7 = r7.toString()     // Catch:{ Throwable -> 0x0110 }
            r8 = 0
            java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x0110 }
            com.tencent.feedback.common.g.b(r7, r8)     // Catch:{ Throwable -> 0x0110 }
            r7 = 4
            java.lang.String r7 = r6.substring(r7)     // Catch:{ Throwable -> 0x0110 }
            java.lang.String r8 = "ao"
            r9 = 0
            int r7 = r3.delete(r8, r7, r9)     // Catch:{ Throwable -> 0x0110 }
            int r0 = r0 + r7
            r7 = 0
            r6.setLength(r7)     // Catch:{ Throwable -> 0x0110 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0110 }
            java.lang.String r8 = "rqdp{  current deleteNum: }"
            r7.<init>(r8)     // Catch:{ Throwable -> 0x0110 }
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ Throwable -> 0x0110 }
            java.lang.String r7 = r7.toString()     // Catch:{ Throwable -> 0x0110 }
            r8 = 0
            java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x0110 }
            com.tencent.feedback.common.g.b(r7, r8)     // Catch:{ Throwable -> 0x0110 }
        L_0x0088:
            int r5 = r5 + 1
            goto L_0x002c
        L_0x008b:
            int r5 = r6.length()     // Catch:{ Throwable -> 0x0110 }
            if (r5 <= 0) goto L_0x00a2
            r5 = 4
            java.lang.String r5 = r6.substring(r5)     // Catch:{ Throwable -> 0x0110 }
            java.lang.String r7 = "ao"
            r8 = 0
            int r5 = r3.delete(r7, r5, r8)     // Catch:{ Throwable -> 0x0110 }
            int r0 = r0 + r5
            r5 = 0
            r6.setLength(r5)     // Catch:{ Throwable -> 0x0110 }
        L_0x00a2:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0110 }
            java.lang.String r6 = "rqdp{  total deleteNum: }"
            r5.<init>(r6)     // Catch:{ Throwable -> 0x0110 }
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Throwable -> 0x0110 }
            java.lang.String r5 = r5.toString()     // Catch:{ Throwable -> 0x0110 }
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x0110 }
            com.tencent.feedback.common.g.a(r5, r6)     // Catch:{ Throwable -> 0x0110 }
            if (r3 == 0) goto L_0x00c2
            boolean r1 = r3.isOpen()
            if (r1 == 0) goto L_0x00c2
            r3.close()
        L_0x00c2:
            r4.close()
            java.lang.String r1 = "rqdp{  AnalyticsDAO.deleteList() end!}"
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.b(r1, r2)
            goto L_0x0014
        L_0x00ce:
            r0 = move-exception
            r4 = r3
        L_0x00d0:
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x010e }
            r5 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x010e }
            com.tencent.feedback.common.g.d(r0, r5)     // Catch:{ all -> 0x010e }
            if (r3 == 0) goto L_0x00e5
            boolean r0 = r3.isOpen()
            if (r0 == 0) goto L_0x00e5
            r3.close()
        L_0x00e5:
            if (r4 == 0) goto L_0x00ea
            r4.close()
        L_0x00ea:
            java.lang.String r0 = "rqdp{  AnalyticsDAO.deleteList() end!}"
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.b(r0, r2)
            r0 = r1
            goto L_0x0014
        L_0x00f4:
            r0 = move-exception
            r4 = r3
        L_0x00f6:
            if (r3 == 0) goto L_0x0101
            boolean r1 = r3.isOpen()
            if (r1 == 0) goto L_0x0101
            r3.close()
        L_0x0101:
            if (r4 == 0) goto L_0x0106
            r4.close()
        L_0x0106:
            java.lang.String r1 = "rqdp{  AnalyticsDAO.deleteList() end!}"
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.b(r1, r2)
            throw r0
        L_0x010e:
            r0 = move-exception
            goto L_0x00f6
        L_0x0110:
            r0 = move-exception
            goto L_0x00d0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.aj.a(android.content.Context, java.lang.Long[]):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x00be A[Catch:{ all -> 0x0109 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00f1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(android.content.Context r11, int r12) {
        /*
            r0 = -1
            r1 = 0
            r9 = 1
            r2 = 0
            if (r11 != 0) goto L_0x000e
            java.lang.String r1 = "rqdp{  FileDAO.delete, context == null}"
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.c(r1, r2)
        L_0x000d:
            return r0
        L_0x000e:
            java.lang.String r3 = "rqdp{  FileDAO.deleteAll() start type:}%d"
            java.lang.Object[] r4 = new java.lang.Object[r9]
            java.lang.Integer r5 = java.lang.Integer.valueOf(r9)
            r4[r2] = r5
            com.tencent.feedback.common.g.b(r3, r4)
            com.tencent.feedback.proguard.n r4 = new com.tencent.feedback.proguard.n     // Catch:{ Throwable -> 0x009c, all -> 0x00e0 }
            r4.<init>(r11)     // Catch:{ Throwable -> 0x009c, all -> 0x00e0 }
            android.database.sqlite.SQLiteDatabase r3 = r4.getWritableDatabase()     // Catch:{ Throwable -> 0x010e, all -> 0x0102 }
            if (r3 != 0) goto L_0x0052
            java.lang.String r1 = "rqdp{  getWritableDatabase fail, delete fail and return!}"
            r5 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x0113, all -> 0x0106 }
            com.tencent.feedback.common.g.d(r1, r5)     // Catch:{ Throwable -> 0x0113, all -> 0x0106 }
            java.lang.String r1 = "rqdp{  Error:fileDeleteAll getWritableDatabase fail!}"
            r5 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x0113, all -> 0x0106 }
            com.tencent.feedback.common.g.d(r1, r5)     // Catch:{ Throwable -> 0x0113, all -> 0x0106 }
            if (r3 == 0) goto L_0x0041
            boolean r1 = r3.isOpen()
            if (r1 == 0) goto L_0x0041
            r3.close()
        L_0x0041:
            r4.close()
            java.lang.String r1 = "rqdp{  FileDAO.delete() end success }%d"
            java.lang.Object[] r3 = new java.lang.Object[r9]
            java.lang.Integer r4 = java.lang.Integer.valueOf(r2)
            r3[r2] = r4
            com.tencent.feedback.common.g.b(r1, r3)
            goto L_0x000d
        L_0x0052:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0113, all -> 0x0106 }
            java.lang.String r1 = "_t = "
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0113, all -> 0x0106 }
            r1 = 1
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x0113, all -> 0x0106 }
            java.lang.String r1 = r0.toString()     // Catch:{ Throwable -> 0x0113, all -> 0x0106 }
            java.lang.String r0 = "file"
            r5 = 0
            int r0 = r3.delete(r0, r1, r5)     // Catch:{ Throwable -> 0x0113, all -> 0x0106 }
            java.lang.String r5 = "rqdp{  fileDeleteAll deletedNum:%d deleted List:\n}%s"
            r6 = 2
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x0117 }
            r7 = 0
            java.lang.Integer r8 = java.lang.Integer.valueOf(r0)     // Catch:{ Throwable -> 0x0117 }
            r6[r7] = r8     // Catch:{ Throwable -> 0x0117 }
            r7 = 1
            if (r1 != 0) goto L_0x007a
            java.lang.String r1 = "all"
        L_0x007a:
            r6[r7] = r1     // Catch:{ Throwable -> 0x0117 }
            com.tencent.feedback.common.g.b(r5, r6)     // Catch:{ Throwable -> 0x0117 }
            if (r3 == 0) goto L_0x008a
            boolean r1 = r3.isOpen()
            if (r1 == 0) goto L_0x008a
            r3.close()
        L_0x008a:
            r4.close()
            java.lang.String r1 = "rqdp{  FileDAO.delete() end success }%d"
            java.lang.Object[] r3 = new java.lang.Object[r9]
            java.lang.Integer r4 = java.lang.Integer.valueOf(r0)
            r3[r2] = r4
            com.tencent.feedback.common.g.b(r1, r3)
            goto L_0x000d
        L_0x009c:
            r0 = move-exception
            r3 = r1
            r4 = r1
            r1 = r0
            r0 = r2
        L_0x00a1:
            java.lang.String r5 = "rqdp{  Error in FileDAO insert!}"
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ all -> 0x0109 }
            com.tencent.feedback.common.g.d(r5, r6)     // Catch:{ all -> 0x0109 }
            java.lang.String r5 = "rqdp{  Error:fileDeleteAll }%s"
            r6 = 1
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ all -> 0x0109 }
            r7 = 0
            java.lang.String r8 = r1.getMessage()     // Catch:{ all -> 0x0109 }
            r6[r7] = r8     // Catch:{ all -> 0x0109 }
            com.tencent.feedback.common.g.d(r5, r6)     // Catch:{ all -> 0x0109 }
            boolean r5 = com.tencent.feedback.common.g.a(r1)     // Catch:{ all -> 0x0109 }
            if (r5 != 0) goto L_0x00c1
            r1.printStackTrace()     // Catch:{ all -> 0x0109 }
        L_0x00c1:
            if (r3 == 0) goto L_0x00cc
            boolean r1 = r3.isOpen()
            if (r1 == 0) goto L_0x00cc
            r3.close()
        L_0x00cc:
            if (r4 == 0) goto L_0x00d1
            r4.close()
        L_0x00d1:
            java.lang.String r1 = "rqdp{  FileDAO.delete() end success }%d"
            java.lang.Object[] r3 = new java.lang.Object[r9]
            java.lang.Integer r4 = java.lang.Integer.valueOf(r0)
            r3[r2] = r4
            com.tencent.feedback.common.g.b(r1, r3)
            goto L_0x000d
        L_0x00e0:
            r0 = move-exception
            r3 = r1
            r4 = r1
            r1 = r2
        L_0x00e4:
            if (r3 == 0) goto L_0x00ef
            boolean r5 = r3.isOpen()
            if (r5 == 0) goto L_0x00ef
            r3.close()
        L_0x00ef:
            if (r4 == 0) goto L_0x00f4
            r4.close()
        L_0x00f4:
            java.lang.String r3 = "rqdp{  FileDAO.delete() end success }%d"
            java.lang.Object[] r4 = new java.lang.Object[r9]
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r4[r2] = r1
            com.tencent.feedback.common.g.b(r3, r4)
            throw r0
        L_0x0102:
            r0 = move-exception
            r3 = r1
            r1 = r2
            goto L_0x00e4
        L_0x0106:
            r0 = move-exception
            r1 = r2
            goto L_0x00e4
        L_0x0109:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x00e4
        L_0x010e:
            r0 = move-exception
            r3 = r1
            r1 = r0
            r0 = r2
            goto L_0x00a1
        L_0x0113:
            r0 = move-exception
            r1 = r0
            r0 = r2
            goto L_0x00a1
        L_0x0117:
            r1 = move-exception
            goto L_0x00a1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.aj.a(android.content.Context, int):int");
    }

    protected static al b(Cursor cursor) {
        if (cursor == null || cursor.isBeforeFirst() || cursor.isAfterLast()) {
            return null;
        }
        al alVar = new al();
        try {
            alVar.a(cursor.getString(cursor.getColumnIndex("_n")));
            alVar.b(cursor.getString(cursor.getColumnIndex("_sa")));
            alVar.a((long) cursor.getInt(cursor.getColumnIndex("_id")));
            alVar.a(cursor.getInt(cursor.getColumnIndex("_t")));
            alVar.c(cursor.getLong(cursor.getColumnIndex("_sz")));
            alVar.b(cursor.getLong(cursor.getColumnIndex("_ut")));
            alVar.c(cursor.getString(cursor.getColumnIndex("_ac")));
            return alVar;
        } catch (Throwable th) {
            if (!g.a(th)) {
                th.printStackTrace();
            }
            g.d("rqdp{  Error:getFileBean fail!}", new Object[0]);
            g.d("rqdp{  Error: getFileBean fail!}", new Object[0]);
            return null;
        }
    }

    protected static List<aj> a(Cursor cursor) {
        g.b("rqdp{  in AnalyticsDAO.paserCursor() start}", new Object[0]);
        if (cursor == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        int columnIndex = cursor.getColumnIndex("_id");
        int columnIndex2 = cursor.getColumnIndex("_prority");
        int columnIndex3 = cursor.getColumnIndex("_time");
        int columnIndex4 = cursor.getColumnIndex("_type");
        int columnIndex5 = cursor.getColumnIndex("_datas");
        int columnIndex6 = cursor.getColumnIndex("_length");
        int columnIndex7 = cursor.getColumnIndex("_key");
        int columnIndex8 = cursor.getColumnIndex("_count");
        int columnIndex9 = cursor.getColumnIndex("_upCounts");
        int columnIndex10 = cursor.getColumnIndex("_state");
        while (cursor.moveToNext()) {
            aj ajVar = new aj();
            ajVar.f2587a = cursor.getLong(columnIndex);
            ajVar.e = cursor.getBlob(columnIndex5);
            ajVar.c = cursor.getInt(columnIndex2);
            ajVar.d = cursor.getLong(columnIndex3);
            ajVar.b = cursor.getInt(columnIndex4);
            ajVar.f = cursor.getLong(columnIndex6);
            ajVar.g = cursor.getString(columnIndex7);
            ajVar.a(cursor.getInt(columnIndex8));
            ajVar.b(cursor.getInt(columnIndex9));
            ajVar.c(cursor.getInt(columnIndex10));
            arrayList.add(ajVar);
        }
        g.b("rqdp{  in AnalyticsDAO.paserCursor() end}", new Object[0]);
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:49:0x0143  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x016c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(android.content.Context r13, int[] r14, long r15, long r17, java.lang.String r19) {
        /*
            java.lang.String r2 = "rqdp{  AnalyticsDAO.querySum() start}"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]
            com.tencent.feedback.common.g.b(r2, r3)
            if (r13 != 0) goto L_0x0014
            java.lang.String r2 = "rqdp{  querySum() context is null arg}"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]
            com.tencent.feedback.common.g.a(r2, r3)
            r2 = -1
        L_0x0013:
            return r2
        L_0x0014:
            int r2 = (r15 > r17 ? 1 : (r15 == r17 ? 0 : -1))
            if (r2 <= 0) goto L_0x001a
            r2 = 0
            goto L_0x0013
        L_0x001a:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "_time >= "
            r2.<init>(r3)
            r0 = r15
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r3 = " and _time"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = " <= "
            java.lang.StringBuilder r2 = r2.append(r3)
            r0 = r17
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r5 = r2.toString()
            if (r14 == 0) goto L_0x0085
            int r2 = r14.length
            if (r2 <= 0) goto L_0x0085
            java.lang.String r3 = ""
            r2 = 0
        L_0x0044:
            int r4 = r14.length
            if (r2 >= r4) goto L_0x0063
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.StringBuilder r3 = r4.append(r3)
            java.lang.String r4 = " or _type = "
            java.lang.StringBuilder r3 = r3.append(r4)
            r4 = r14[r2]
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            int r2 = r2 + 1
            goto L_0x0044
        L_0x0063:
            r2 = 4
            java.lang.String r2 = r3.substring(r2)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r4 = " and ( "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " )"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r5 = r2.toString()
        L_0x0085:
            if (r19 == 0) goto L_0x00a6
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.String r3 = " and ( _key = '"
            java.lang.StringBuilder r2 = r2.append(r3)
            r0 = r19
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r3 = "' )"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r5 = r2.toString()
        L_0x00a6:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "rqdp{  query where: }"
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.String r2 = r2.toString()
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]
            com.tencent.feedback.common.g.b(r2, r3)
            r4 = 0
            r3 = 0
            r11 = 0
            com.tencent.feedback.proguard.n r10 = new com.tencent.feedback.proguard.n     // Catch:{ Throwable -> 0x011f, all -> 0x0151 }
            r10.<init>(r13)     // Catch:{ Throwable -> 0x011f, all -> 0x0151 }
            android.database.sqlite.SQLiteDatabase r2 = r10.getWritableDatabase()     // Catch:{ Throwable -> 0x018a, all -> 0x0178 }
            java.lang.String r3 = "ao"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Throwable -> 0x018e, all -> 0x017b }
            r6 = 0
            java.lang.String r7 = "count(*) as sum"
            r4[r6] = r7     // Catch:{ Throwable -> 0x018e, all -> 0x017b }
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r4 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Throwable -> 0x018e, all -> 0x017b }
            r4.moveToNext()     // Catch:{ Throwable -> 0x0195, all -> 0x0181 }
            java.lang.String r3 = "sum"
            int r3 = r4.getColumnIndex(r3)     // Catch:{ Throwable -> 0x0195, all -> 0x0181 }
            int r3 = r4.getInt(r3)     // Catch:{ Throwable -> 0x0195, all -> 0x0181 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0195, all -> 0x0181 }
            java.lang.String r6 = "rqdp{  query sum: }"
            r5.<init>(r6)     // Catch:{ Throwable -> 0x0195, all -> 0x0181 }
            java.lang.StringBuilder r5 = r5.append(r3)     // Catch:{ Throwable -> 0x0195, all -> 0x0181 }
            java.lang.String r5 = r5.toString()     // Catch:{ Throwable -> 0x0195, all -> 0x0181 }
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x0195, all -> 0x0181 }
            com.tencent.feedback.common.g.b(r5, r6)     // Catch:{ Throwable -> 0x0195, all -> 0x0181 }
            if (r4 == 0) goto L_0x0106
            boolean r5 = r4.isClosed()
            if (r5 != 0) goto L_0x0106
            r4.close()
        L_0x0106:
            if (r2 == 0) goto L_0x0111
            boolean r4 = r2.isOpen()
            if (r4 == 0) goto L_0x0111
            r2.close()
        L_0x0111:
            r10.close()
            java.lang.String r2 = "rqdp{  AnalyticsDAO.querySum() end}"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]
            com.tencent.feedback.common.g.b(r2, r4)
            r2 = r3
            goto L_0x0013
        L_0x011f:
            r2 = move-exception
            r5 = r11
        L_0x0121:
            java.lang.String r2 = r2.getMessage()     // Catch:{ all -> 0x0186 }
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ all -> 0x0186 }
            com.tencent.feedback.common.g.b(r2, r6)     // Catch:{ all -> 0x0186 }
            if (r5 == 0) goto L_0x0136
            boolean r2 = r5.isClosed()
            if (r2 != 0) goto L_0x0136
            r5.close()
        L_0x0136:
            if (r3 == 0) goto L_0x0141
            boolean r2 = r3.isOpen()
            if (r2 == 0) goto L_0x0141
            r3.close()
        L_0x0141:
            if (r4 == 0) goto L_0x0146
            r4.close()
        L_0x0146:
            java.lang.String r2 = "rqdp{  AnalyticsDAO.querySum() end}"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]
            com.tencent.feedback.common.g.b(r2, r3)
            r2 = -1
            goto L_0x0013
        L_0x0151:
            r2 = move-exception
            r10 = r4
            r4 = r11
        L_0x0154:
            if (r4 == 0) goto L_0x015f
            boolean r5 = r4.isClosed()
            if (r5 != 0) goto L_0x015f
            r4.close()
        L_0x015f:
            if (r3 == 0) goto L_0x016a
            boolean r4 = r3.isOpen()
            if (r4 == 0) goto L_0x016a
            r3.close()
        L_0x016a:
            if (r10 == 0) goto L_0x016f
            r10.close()
        L_0x016f:
            java.lang.String r3 = "rqdp{  AnalyticsDAO.querySum() end}"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]
            com.tencent.feedback.common.g.b(r3, r4)
            throw r2
        L_0x0178:
            r2 = move-exception
            r4 = r11
            goto L_0x0154
        L_0x017b:
            r3 = move-exception
            r4 = r11
            r12 = r2
            r2 = r3
            r3 = r12
            goto L_0x0154
        L_0x0181:
            r3 = move-exception
            r12 = r3
            r3 = r2
            r2 = r12
            goto L_0x0154
        L_0x0186:
            r2 = move-exception
            r10 = r4
            r4 = r5
            goto L_0x0154
        L_0x018a:
            r2 = move-exception
            r4 = r10
            r5 = r11
            goto L_0x0121
        L_0x018e:
            r3 = move-exception
            r4 = r10
            r5 = r11
            r12 = r3
            r3 = r2
            r2 = r12
            goto L_0x0121
        L_0x0195:
            r3 = move-exception
            r5 = r4
            r4 = r10
            r12 = r2
            r2 = r3
            r3 = r12
            goto L_0x0121
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.aj.a(android.content.Context, int[], long, long, java.lang.String):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:55:0x00e2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean b(android.content.Context r9, java.util.List<com.tencent.feedback.proguard.aj> r10) {
        /*
            r2 = 0
            r1 = 0
            java.lang.String r0 = "rqdp{  insertOrUpdate alyticsBeans start!}"
            java.lang.Object[] r3 = new java.lang.Object[r1]
            com.tencent.feedback.common.g.b(r0, r3)
            if (r9 == 0) goto L_0x0013
            if (r10 == 0) goto L_0x0013
            int r0 = r10.size()
            if (r0 > 0) goto L_0x001c
        L_0x0013:
            java.lang.String r0 = "rqdp{  context == null || list == null|| list.size() <= 0 ? pls check!}"
            java.lang.Object[] r2 = new java.lang.Object[r1]
            com.tencent.feedback.common.g.d(r0, r2)
            r0 = r1
        L_0x001b:
            return r0
        L_0x001c:
            com.tencent.feedback.proguard.n r3 = new com.tencent.feedback.proguard.n     // Catch:{ Throwable -> 0x00ef, all -> 0x00d3 }
            r3.<init>(r9)     // Catch:{ Throwable -> 0x00ef, all -> 0x00d3 }
            android.database.sqlite.SQLiteDatabase r2 = r3.getWritableDatabase()     // Catch:{ Throwable -> 0x0097 }
            java.util.Iterator r4 = r10.iterator()     // Catch:{ Throwable -> 0x0097 }
        L_0x0029:
            boolean r0 = r4.hasNext()     // Catch:{ Throwable -> 0x0097 }
            if (r0 == 0) goto L_0x00bb
            java.lang.Object r0 = r4.next()     // Catch:{ Throwable -> 0x0097 }
            com.tencent.feedback.proguard.aj r0 = (com.tencent.feedback.proguard.aj) r0     // Catch:{ Throwable -> 0x0097 }
            android.content.ContentValues r5 = a(r0)     // Catch:{ Throwable -> 0x0097 }
            if (r5 != 0) goto L_0x0052
            if (r2 == 0) goto L_0x0046
            boolean r0 = r2.isOpen()
            if (r0 == 0) goto L_0x0046
            r2.close()
        L_0x0046:
            r3.close()
            java.lang.String r0 = "rqdp{  insertOrUpdate alyticsBeans end}"
            java.lang.Object[] r2 = new java.lang.Object[r1]
            com.tencent.feedback.common.g.b(r0, r2)
            r0 = r1
            goto L_0x001b
        L_0x0052:
            java.lang.String r6 = "ao"
            java.lang.String r7 = "_id"
            long r5 = r2.replace(r6, r7, r5)     // Catch:{ Throwable -> 0x0097 }
            r7 = 0
            int r7 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r7 >= 0) goto L_0x007f
            java.lang.String r0 = "rqdp{  insertOrUpdate alyticsBeans failure! return}"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x0097 }
            com.tencent.feedback.common.g.d(r0, r4)     // Catch:{ Throwable -> 0x0097 }
            if (r2 == 0) goto L_0x0073
            boolean r0 = r2.isOpen()
            if (r0 == 0) goto L_0x0073
            r2.close()
        L_0x0073:
            r3.close()
            java.lang.String r0 = "rqdp{  insertOrUpdate alyticsBeans end}"
            java.lang.Object[] r2 = new java.lang.Object[r1]
            com.tencent.feedback.common.g.b(r0, r2)
            r0 = r1
            goto L_0x001b
        L_0x007f:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0097 }
            java.lang.String r8 = "rqdp{  result id:}"
            r7.<init>(r8)     // Catch:{ Throwable -> 0x0097 }
            java.lang.StringBuilder r7 = r7.append(r5)     // Catch:{ Throwable -> 0x0097 }
            java.lang.String r7 = r7.toString()     // Catch:{ Throwable -> 0x0097 }
            r8 = 0
            java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x0097 }
            com.tencent.feedback.common.g.b(r7, r8)     // Catch:{ Throwable -> 0x0097 }
            r0.f2587a = r5     // Catch:{ Throwable -> 0x0097 }
            goto L_0x0029
        L_0x0097:
            r0 = move-exception
        L_0x0098:
            boolean r4 = com.tencent.feedback.common.g.a(r0)     // Catch:{ all -> 0x00ed }
            if (r4 != 0) goto L_0x00a1
            r0.printStackTrace()     // Catch:{ all -> 0x00ed }
        L_0x00a1:
            if (r2 == 0) goto L_0x00ac
            boolean r0 = r2.isOpen()
            if (r0 == 0) goto L_0x00ac
            r2.close()
        L_0x00ac:
            if (r3 == 0) goto L_0x00b1
            r3.close()
        L_0x00b1:
            java.lang.String r0 = "rqdp{  insertOrUpdate alyticsBeans end}"
            java.lang.Object[] r2 = new java.lang.Object[r1]
            com.tencent.feedback.common.g.b(r0, r2)
            r0 = r1
            goto L_0x001b
        L_0x00bb:
            if (r2 == 0) goto L_0x00c6
            boolean r0 = r2.isOpen()
            if (r0 == 0) goto L_0x00c6
            r2.close()
        L_0x00c6:
            r3.close()
            java.lang.String r0 = "rqdp{  insertOrUpdate alyticsBeans end}"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.feedback.common.g.b(r0, r1)
            r0 = 1
            goto L_0x001b
        L_0x00d3:
            r0 = move-exception
            r3 = r2
        L_0x00d5:
            if (r2 == 0) goto L_0x00e0
            boolean r4 = r2.isOpen()
            if (r4 == 0) goto L_0x00e0
            r2.close()
        L_0x00e0:
            if (r3 == 0) goto L_0x00e5
            r3.close()
        L_0x00e5:
            java.lang.String r2 = "rqdp{  insertOrUpdate alyticsBeans end}"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.feedback.common.g.b(r2, r1)
            throw r0
        L_0x00ed:
            r0 = move-exception
            goto L_0x00d5
        L_0x00ef:
            r0 = move-exception
            r3 = r2
            goto L_0x0098
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.aj.b(android.content.Context, java.util.List):boolean");
    }

    public static ContentValues a(aj ajVar) {
        ContentValues contentValues = new ContentValues();
        if (ajVar.f2587a > 0) {
            contentValues.put("_id", Long.valueOf(ajVar.f2587a));
        }
        contentValues.put("_prority", Integer.valueOf(ajVar.c));
        contentValues.put("_time", Long.valueOf(ajVar.d));
        contentValues.put("_type", Integer.valueOf(ajVar.b));
        contentValues.put("_datas", ajVar.e);
        contentValues.put("_length", Long.valueOf(ajVar.f));
        contentValues.put("_key", ajVar.g);
        contentValues.put("_count", Integer.valueOf(ajVar.c()));
        contentValues.put("_upCounts", Integer.valueOf(ajVar.d()));
        contentValues.put("_state", Integer.valueOf(ajVar.e()));
        return contentValues;
    }

    public static List<aj> a(Context context, int[] iArr, int i2, int i3, long j2, int i4, String str, int i5, int i6, int i7, int i8, long j3, long j4) {
        return a(context, iArr, -1, -1, j2, i4, str, -1, -1, -1, -1, -1, Long.MAX_VALUE, -1);
    }

    /* JADX WARNING: Removed duplicated region for block: B:112:0x02f0  */
    /* JADX WARNING: Removed duplicated region for block: B:201:0x0506  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<com.tencent.feedback.proguard.aj> a(android.content.Context r17, int[] r18, int r19, int r20, long r21, int r23, java.lang.String r24, int r25, int r26, int r27, int r28, long r29, long r31, int r33) {
        /*
            java.lang.String r2 = "rqdp{  in AnalyticsDAO.query() start}"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]
            com.tencent.feedback.common.g.b(r2, r3)
            if (r17 == 0) goto L_0x002a
            r2 = 0
            int r2 = (r21 > r2 ? 1 : (r21 == r2 ? 0 : -1))
            if (r2 == 0) goto L_0x002a
            r2 = 0
            int r2 = (r31 > r2 ? 1 : (r31 == r2 ? 0 : -1))
            if (r2 <= 0) goto L_0x001a
            int r2 = (r29 > r31 ? 1 : (r29 == r31 ? 0 : -1))
            if (r2 > 0) goto L_0x002a
        L_0x001a:
            if (r26 <= 0) goto L_0x0022
            r0 = r25
            r1 = r26
            if (r0 > r1) goto L_0x002a
        L_0x0022:
            if (r28 <= 0) goto L_0x0034
            r0 = r27
            r1 = r28
            if (r0 <= r1) goto L_0x0034
        L_0x002a:
            java.lang.String r2 = "rqdp{  query() args context == null or totalSizeLimit == 0 || timeStart > timeEnd || miniCount > maxCount || miniUploadCount > maxUploadCount ,pls check}"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]
            com.tencent.feedback.common.g.d(r2, r3)
            r2 = 0
        L_0x0033:
            return r2
        L_0x0034:
            r2 = 0
            int r2 = (r21 > r2 ? 1 : (r21 == r2 ? 0 : -1))
            if (r2 >= 0) goto L_0x003f
            r21 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
        L_0x003f:
            java.lang.String r4 = ""
            if (r18 == 0) goto L_0x0549
            r0 = r18
            int r2 = r0.length
            if (r2 <= 0) goto L_0x0549
            java.lang.String r3 = ""
            r2 = 0
        L_0x004b:
            r0 = r18
            int r5 = r0.length
            if (r2 >= r5) goto L_0x006c
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.StringBuilder r3 = r5.append(r3)
            java.lang.String r5 = " or _type = "
            java.lang.StringBuilder r3 = r3.append(r5)
            r5 = r18[r2]
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r3 = r3.toString()
            int r2 = r2 + 1
            goto L_0x004b
        L_0x006c:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r2 = r2.append(r4)
            r4 = 4
            java.lang.String r3 = r3.substring(r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
        L_0x0082:
            int r3 = r2.length()
            if (r3 <= 0) goto L_0x02fe
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = " ( "
            r3.<init>(r4)
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " ) "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r5 = r2.toString()
        L_0x009d:
            if (r24 == 0) goto L_0x00ca
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r3 = r2.append(r5)
            int r2 = r5.length()
            if (r2 <= 0) goto L_0x0302
            java.lang.String r2 = " and "
        L_0x00b0:
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = "_key = '"
            java.lang.StringBuilder r2 = r2.append(r3)
            r0 = r24
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r3 = "' "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r5 = r2.toString()
        L_0x00ca:
            if (r25 < 0) goto L_0x00f7
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r3 = r2.append(r5)
            int r2 = r5.length()
            if (r2 <= 0) goto L_0x0306
            java.lang.String r2 = " and "
        L_0x00dd:
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = "_count >= "
            java.lang.StringBuilder r2 = r2.append(r3)
            r0 = r25
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r3 = " "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r5 = r2.toString()
        L_0x00f7:
            if (r26 < 0) goto L_0x0124
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r3 = r2.append(r5)
            int r2 = r5.length()
            if (r2 <= 0) goto L_0x030a
            java.lang.String r2 = " and "
        L_0x010a:
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = "_count <= "
            java.lang.StringBuilder r2 = r2.append(r3)
            r0 = r26
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r3 = " "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r5 = r2.toString()
        L_0x0124:
            if (r27 < 0) goto L_0x0151
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r3 = r2.append(r5)
            int r2 = r5.length()
            if (r2 <= 0) goto L_0x030e
            java.lang.String r2 = " and "
        L_0x0137:
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = "_upCounts >= "
            java.lang.StringBuilder r2 = r2.append(r3)
            r0 = r27
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r3 = " "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r5 = r2.toString()
        L_0x0151:
            if (r28 < 0) goto L_0x017e
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r3 = r2.append(r5)
            int r2 = r5.length()
            if (r2 <= 0) goto L_0x0312
            java.lang.String r2 = " and "
        L_0x0164:
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = "_upCounts <= "
            java.lang.StringBuilder r2 = r2.append(r3)
            r0 = r28
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r3 = " "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r5 = r2.toString()
        L_0x017e:
            r2 = 0
            int r2 = (r29 > r2 ? 1 : (r29 == r2 ? 0 : -1))
            if (r2 < 0) goto L_0x01af
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r3 = r2.append(r5)
            int r2 = r5.length()
            if (r2 <= 0) goto L_0x0316
            java.lang.String r2 = " and "
        L_0x0195:
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = "_time >= "
            java.lang.StringBuilder r2 = r2.append(r3)
            r0 = r29
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r3 = " "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r5 = r2.toString()
        L_0x01af:
            r2 = 0
            int r2 = (r31 > r2 ? 1 : (r31 == r2 ? 0 : -1))
            if (r2 < 0) goto L_0x01e0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r3 = r2.append(r5)
            int r2 = r5.length()
            if (r2 <= 0) goto L_0x031a
            java.lang.String r2 = " and "
        L_0x01c6:
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = "_time <= "
            java.lang.StringBuilder r2 = r2.append(r3)
            r0 = r31
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r3 = " "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r5 = r2.toString()
        L_0x01e0:
            if (r33 < 0) goto L_0x020d
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r3 = r2.append(r5)
            int r2 = r5.length()
            if (r2 <= 0) goto L_0x031e
            java.lang.String r2 = " and "
        L_0x01f3:
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = "_state = "
            java.lang.StringBuilder r2 = r2.append(r3)
            r0 = r33
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r3 = " "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r5 = r2.toString()
        L_0x020d:
            java.lang.String r2 = ""
            switch(r19) {
                case 1: goto L_0x0322;
                case 2: goto L_0x0337;
                default: goto L_0x0212;
            }
        L_0x0212:
            switch(r20) {
                case 1: goto L_0x034c;
                case 2: goto L_0x0361;
                default: goto L_0x0215;
            }
        L_0x0215:
            java.lang.String r3 = " , "
            boolean r3 = r2.endsWith(r3)
            if (r3 == 0) goto L_0x0546
            r3 = 0
            int r4 = r2.length()
            int r4 = r4 + -3
            java.lang.String r9 = r2.substring(r3, r4)
        L_0x0228:
            java.lang.String r2 = "rqdp{  query} %s"
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r4 = 0
            r3[r4] = r5
            com.tencent.feedback.common.g.b(r2, r3)
            r3 = 0
            r4 = 0
            r13 = 0
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            com.tencent.feedback.proguard.n r12 = new com.tencent.feedback.proguard.n     // Catch:{ Throwable -> 0x0529, all -> 0x04eb }
            r0 = r17
            r12.<init>(r0)     // Catch:{ Throwable -> 0x0529, all -> 0x04eb }
            android.database.sqlite.SQLiteDatabase r2 = r12.getWritableDatabase()     // Catch:{ Throwable -> 0x052d, all -> 0x0512 }
            r3 = 0
            int r3 = (r21 > r3 ? 1 : (r21 == r3 ? 0 : -1))
            if (r3 <= 0) goto L_0x04b9
            int r3 = r9.length()     // Catch:{ Throwable -> 0x0532, all -> 0x0515 }
            if (r3 <= 0) goto L_0x0265
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0532, all -> 0x0515 }
            r3.<init>()     // Catch:{ Throwable -> 0x0532, all -> 0x0515 }
            java.lang.StringBuilder r3 = r3.append(r9)     // Catch:{ Throwable -> 0x0532, all -> 0x0515 }
            java.lang.String r4 = " , "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x0532, all -> 0x0515 }
            java.lang.String r9 = r3.toString()     // Catch:{ Throwable -> 0x0532, all -> 0x0515 }
        L_0x0265:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0532, all -> 0x0515 }
            r3.<init>()     // Catch:{ Throwable -> 0x0532, all -> 0x0515 }
            java.lang.StringBuilder r3 = r3.append(r9)     // Catch:{ Throwable -> 0x0532, all -> 0x0515 }
            java.lang.String r4 = "_length ASC "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x0532, all -> 0x0515 }
            java.lang.String r9 = r3.toString()     // Catch:{ Throwable -> 0x0532, all -> 0x0515 }
            java.lang.String r3 = "ao"
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Throwable -> 0x0532, all -> 0x0515 }
            r6 = 0
            java.lang.String r7 = "_id"
            r4[r6] = r7     // Catch:{ Throwable -> 0x0532, all -> 0x0515 }
            r6 = 1
            java.lang.String r7 = "_length"
            r4[r6] = r7     // Catch:{ Throwable -> 0x0532, all -> 0x0515 }
            r6 = 0
            r7 = 0
            r8 = 0
            if (r23 < 0) goto L_0x0376
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0532, all -> 0x0515 }
            r10.<init>()     // Catch:{ Throwable -> 0x0532, all -> 0x0515 }
            r0 = r23
            java.lang.StringBuilder r10 = r10.append(r0)     // Catch:{ Throwable -> 0x0532, all -> 0x0515 }
            java.lang.String r10 = r10.toString()     // Catch:{ Throwable -> 0x0532, all -> 0x0515 }
        L_0x029b:
            android.database.Cursor r10 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Throwable -> 0x0532, all -> 0x0515 }
            java.lang.String r3 = "_id"
            int r3 = r10.getColumnIndex(r3)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.lang.String r4 = "_length"
            int r4 = r10.getColumnIndex(r4)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.util.LinkedHashMap r5 = new java.util.LinkedHashMap     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            r5.<init>()     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
        L_0x02b0:
            boolean r6 = r10.moveToNext()     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            if (r6 == 0) goto L_0x0379
            long r6 = r10.getLong(r3)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            long r13 = r10.getLong(r4)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.lang.Long r7 = java.lang.Long.valueOf(r13)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            r5.put(r6, r7)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            goto L_0x02b0
        L_0x02ca:
            r3 = move-exception
            r4 = r2
            r2 = r3
            r3 = r12
        L_0x02ce:
            java.lang.String r2 = r2.getMessage()     // Catch:{ all -> 0x0526 }
            r5 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x0526 }
            com.tencent.feedback.common.g.b(r2, r5)     // Catch:{ all -> 0x0526 }
            if (r10 == 0) goto L_0x02e3
            boolean r2 = r10.isClosed()
            if (r2 != 0) goto L_0x02e3
            r10.close()
        L_0x02e3:
            if (r4 == 0) goto L_0x02ee
            boolean r2 = r4.isOpen()
            if (r2 == 0) goto L_0x02ee
            r4.close()
        L_0x02ee:
            if (r3 == 0) goto L_0x02f3
            r3.close()
        L_0x02f3:
            java.lang.String r2 = "rqdp{  in AnalyticsDAO.query() end}"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]
            com.tencent.feedback.common.g.b(r2, r3)
            r2 = 0
            goto L_0x0033
        L_0x02fe:
            java.lang.String r5 = ""
            goto L_0x009d
        L_0x0302:
            java.lang.String r2 = ""
            goto L_0x00b0
        L_0x0306:
            java.lang.String r2 = ""
            goto L_0x00dd
        L_0x030a:
            java.lang.String r2 = ""
            goto L_0x010a
        L_0x030e:
            java.lang.String r2 = ""
            goto L_0x0137
        L_0x0312:
            java.lang.String r2 = ""
            goto L_0x0164
        L_0x0316:
            java.lang.String r2 = ""
            goto L_0x0195
        L_0x031a:
            java.lang.String r2 = ""
            goto L_0x01c6
        L_0x031e:
            java.lang.String r2 = ""
            goto L_0x01f3
        L_0x0322:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = "_prority ASC , "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            goto L_0x0212
        L_0x0337:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = "_prority DESC , "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            goto L_0x0212
        L_0x034c:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = "_time ASC "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            goto L_0x0215
        L_0x0361:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = "_time DESC "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            goto L_0x0215
        L_0x0376:
            r10 = 0
            goto L_0x029b
        L_0x0379:
            r10.close()     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            r0 = r21
            java.lang.Long[] r14 = com.tencent.feedback.proguard.ac.a(r5, r0)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            if (r14 == 0) goto L_0x0495
            int r3 = r14.length     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            if (r3 <= 0) goto L_0x0495
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.lang.String r4 = "rqdp{  cids num :}"
            r3.<init>(r4)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            int r4 = r14.length     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            com.tencent.feedback.common.g.a(r3, r4)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.lang.StringBuffer r15 = new java.lang.StringBuffer     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            r15.<init>()     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            r3 = 0
            r13 = r3
        L_0x03a4:
            int r3 = r14.length     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            if (r13 >= r3) goto L_0x041a
            r3 = r14[r13]     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            long r3 = r3.longValue()     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.lang.String r6 = " or  _id = "
            r5.<init>(r6)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            r15.append(r3)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            if (r13 <= 0) goto L_0x0543
            int r3 = r13 % 50
            if (r3 != 0) goto L_0x0543
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.lang.String r4 = "rqdp{  current }"
            r3.<init>(r4)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.lang.StringBuilder r3 = r3.append(r13)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            com.tencent.feedback.common.g.b(r3, r4)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            r3 = 4
            java.lang.String r5 = r15.substring(r3)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            r3 = 0
            r15.setLength(r3)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.lang.String r3 = "ao"
            r4 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r3 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.util.List r4 = a(r3)     // Catch:{ Throwable -> 0x0539, all -> 0x051e }
            if (r4 == 0) goto L_0x0412
            int r5 = r4.size()     // Catch:{ Throwable -> 0x0539, all -> 0x051e }
            if (r5 <= 0) goto L_0x0412
            r11.addAll(r4)     // Catch:{ Throwable -> 0x0539, all -> 0x051e }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0539, all -> 0x051e }
            java.lang.String r5 = "rqdp{  current addNum: }"
            r4.<init>(r5)     // Catch:{ Throwable -> 0x0539, all -> 0x051e }
            r5 = 0
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Throwable -> 0x0539, all -> 0x051e }
            java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x0539, all -> 0x051e }
            r5 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x0539, all -> 0x051e }
            com.tencent.feedback.common.g.b(r4, r5)     // Catch:{ Throwable -> 0x0539, all -> 0x051e }
        L_0x0412:
            r3.close()     // Catch:{ Throwable -> 0x0539, all -> 0x051e }
        L_0x0415:
            int r4 = r13 + 1
            r13 = r4
            r10 = r3
            goto L_0x03a4
        L_0x041a:
            int r3 = r15.length()     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            if (r3 <= 0) goto L_0x0458
            r3 = 4
            java.lang.String r5 = r15.substring(r3)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            r3 = 0
            r15.setLength(r3)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.lang.String r3 = "ao"
            r4 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r10 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.util.List r3 = a(r10)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            if (r3 == 0) goto L_0x0458
            int r4 = r3.size()     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            if (r4 <= 0) goto L_0x0458
            r11.addAll(r3)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.lang.String r4 = "rqdp{  current addNum: }"
            r3.<init>(r4)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            r4 = 0
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            com.tencent.feedback.common.g.b(r3, r4)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
        L_0x0458:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.lang.String r4 = "rqdp{  total num: }"
            r3.<init>(r4)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            int r4 = r11.size()     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            com.tencent.feedback.common.g.a(r3, r4)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            if (r10 == 0) goto L_0x047c
            boolean r3 = r10.isClosed()
            if (r3 != 0) goto L_0x047c
            r10.close()
        L_0x047c:
            if (r2 == 0) goto L_0x0487
            boolean r3 = r2.isOpen()
            if (r3 == 0) goto L_0x0487
            r2.close()
        L_0x0487:
            r12.close()
            java.lang.String r2 = "rqdp{  in AnalyticsDAO.query() end}"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]
            com.tencent.feedback.common.g.b(r2, r3)
            r2 = r11
            goto L_0x0033
        L_0x0495:
            if (r10 == 0) goto L_0x04a0
            boolean r3 = r10.isClosed()
            if (r3 != 0) goto L_0x04a0
            r10.close()
        L_0x04a0:
            if (r2 == 0) goto L_0x04ab
            boolean r3 = r2.isOpen()
            if (r3 == 0) goto L_0x04ab
            r2.close()
        L_0x04ab:
            r12.close()
            java.lang.String r2 = "rqdp{  in AnalyticsDAO.query() end}"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]
            com.tencent.feedback.common.g.b(r2, r3)
            r2 = 0
            goto L_0x0033
        L_0x04b9:
            java.lang.String r3 = "ao"
            r4 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r10 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Throwable -> 0x0532, all -> 0x0515 }
            java.util.List r3 = a(r10)     // Catch:{ Throwable -> 0x02ca, all -> 0x051a }
            if (r10 == 0) goto L_0x04d2
            boolean r4 = r10.isClosed()
            if (r4 != 0) goto L_0x04d2
            r10.close()
        L_0x04d2:
            if (r2 == 0) goto L_0x04dd
            boolean r4 = r2.isOpen()
            if (r4 == 0) goto L_0x04dd
            r2.close()
        L_0x04dd:
            r12.close()
            java.lang.String r2 = "rqdp{  in AnalyticsDAO.query() end}"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]
            com.tencent.feedback.common.g.b(r2, r4)
            r2 = r3
            goto L_0x0033
        L_0x04eb:
            r2 = move-exception
            r12 = r3
            r10 = r13
        L_0x04ee:
            if (r10 == 0) goto L_0x04f9
            boolean r3 = r10.isClosed()
            if (r3 != 0) goto L_0x04f9
            r10.close()
        L_0x04f9:
            if (r4 == 0) goto L_0x0504
            boolean r3 = r4.isOpen()
            if (r3 == 0) goto L_0x0504
            r4.close()
        L_0x0504:
            if (r12 == 0) goto L_0x0509
            r12.close()
        L_0x0509:
            java.lang.String r3 = "rqdp{  in AnalyticsDAO.query() end}"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]
            com.tencent.feedback.common.g.b(r3, r4)
            throw r2
        L_0x0512:
            r2 = move-exception
            r10 = r13
            goto L_0x04ee
        L_0x0515:
            r3 = move-exception
            r10 = r13
            r4 = r2
            r2 = r3
            goto L_0x04ee
        L_0x051a:
            r3 = move-exception
            r4 = r2
            r2 = r3
            goto L_0x04ee
        L_0x051e:
            r4 = move-exception
            r10 = r3
            r16 = r4
            r4 = r2
            r2 = r16
            goto L_0x04ee
        L_0x0526:
            r2 = move-exception
            r12 = r3
            goto L_0x04ee
        L_0x0529:
            r2 = move-exception
            r10 = r13
            goto L_0x02ce
        L_0x052d:
            r2 = move-exception
            r3 = r12
            r10 = r13
            goto L_0x02ce
        L_0x0532:
            r3 = move-exception
            r10 = r13
            r4 = r2
            r2 = r3
            r3 = r12
            goto L_0x02ce
        L_0x0539:
            r4 = move-exception
            r10 = r3
            r3 = r12
            r16 = r4
            r4 = r2
            r2 = r16
            goto L_0x02ce
        L_0x0543:
            r3 = r10
            goto L_0x0415
        L_0x0546:
            r9 = r2
            goto L_0x0228
        L_0x0549:
            r2 = r4
            goto L_0x0082
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.aj.a(android.content.Context, int[], int, int, long, int, java.lang.String, int, int, int, int, long, long, int):java.util.List");
    }
}
