package com.tencent.feedback.proguard;

import com.tencent.connect.common.Constants;

public final class K extends C0008j {

    /* renamed from: a  reason: collision with root package name */
    public String f2574a = Constants.STR_EMPTY;
    public String b = Constants.STR_EMPTY;
    public String c = Constants.STR_EMPTY;

    public final void a(ag agVar) {
        this.f2574a = agVar.b(0, true);
        this.b = agVar.b(1, true);
        this.c = agVar.b(2, false);
    }

    public final void a(ah ahVar) {
        ahVar.a(this.f2574a, 0);
        ahVar.a(this.b, 1);
        if (this.c != null) {
            ahVar.a(this.c, 2);
        }
    }

    public final void a(StringBuilder sb, int i) {
    }
}
