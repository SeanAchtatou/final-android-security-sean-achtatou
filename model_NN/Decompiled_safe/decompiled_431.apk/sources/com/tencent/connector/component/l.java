package com.tencent.connector.component;

import android.view.View;

/* compiled from: ProGuard */
class l implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ContentQQConnectionRequest f2408a;

    l(ContentQQConnectionRequest contentQQConnectionRequest) {
        this.f2408a = contentQQConnectionRequest;
    }

    public void onClick(View view) {
        this.f2408a.denyRequest();
        if (this.f2408a.b != null) {
            this.f2408a.b.finish();
        }
    }
}
