package com.tencent.connector;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.event.listener.b;
import com.tencent.connector.ipc.a;

/* compiled from: ProGuard */
public class UsbDebugModeAlertActivity extends Activity implements b {

    /* renamed from: a  reason: collision with root package name */
    private TextView f2378a;
    private TextView b;
    private View.OnClickListener c = new n(this);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_usb_debug_mode);
        a();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        AstApp.i().k().addConnectionEventListener(5002, this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (a.b(getApplicationContext())) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        AstApp.i().k().removeConnectionEventListener(5002, this);
    }

    private void a() {
        this.f2378a = (TextView) findViewById(R.id.cancel);
        this.b = (TextView) findViewById(R.id.ok);
        this.f2378a.setOnClickListener(this.c);
        this.b.setOnClickListener(this.c);
    }

    /* access modifiers changed from: private */
    public void b() {
        j.a().a(getTaskId());
        com.tencent.pangu.link.b.a(this, Uri.parse("tmast://devsetting"));
    }

    public void a(Message message) {
        switch (message.what) {
            case 5002:
                finish();
                return;
            default:
                return;
        }
    }
}
