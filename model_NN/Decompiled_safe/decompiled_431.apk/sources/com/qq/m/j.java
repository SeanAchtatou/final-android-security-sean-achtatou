package com.qq.m;

import com.qq.AppService.r;
import java.util.ArrayList;

/* compiled from: ProGuard */
public final class j {

    /* renamed from: a  reason: collision with root package name */
    public long f317a = 0;
    public int b = 0;
    public String c = null;
    public long d = 0;
    public long e = 0;
    public String f = null;
    public String g = null;
    public int h = 0;
    public String i = null;
    public byte[] j = null;
    public String k = null;
    public String l = null;
    public String m = null;
    public long n = 0;
    public long o = 0;
    public int p = 0;
    public String q = null;
    public String r;
    public boolean s = false;

    public void a(ArrayList<byte[]> arrayList) {
        int i2;
        if (arrayList != null) {
            arrayList.add(r.a(this.f317a));
            arrayList.add(r.a(this.b));
            arrayList.add(r.a(this.c));
            arrayList.add(r.a(this.d));
            arrayList.add(r.a(r.b(this.e)));
            arrayList.add(r.a(this.f));
            arrayList.add(r.a(this.g));
            arrayList.add(r.a(this.h));
            arrayList.add(r.a(this.i));
            if (this.j != null) {
                arrayList.add(this.j);
            } else {
                arrayList.add(r.hs);
            }
            arrayList.add(r.a(this.k));
            arrayList.add(r.a(this.l));
            arrayList.add(r.a(this.m));
            arrayList.add(r.a(this.n));
            if (this.o != 0) {
                arrayList.add(r.a(r.b(this.o)));
            } else {
                arrayList.add(r.hr);
            }
            arrayList.add(r.a(this.p));
            arrayList.add(r.a(this.q));
            arrayList.add(r.a(this.r));
            if (this.s) {
                i2 = 1;
            } else {
                i2 = 0;
            }
            arrayList.add(r.a(i2));
        }
    }
}
