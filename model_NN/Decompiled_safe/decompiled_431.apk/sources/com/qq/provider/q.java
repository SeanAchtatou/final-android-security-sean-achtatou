package com.qq.provider;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.RemoteException;
import android.util.Log;
import com.qq.AppService.AppService;
import com.qq.AppService.r;
import com.qq.d.a;
import com.qq.g.c;
import com.qq.util.j;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.commom.DataEntityKeyConst;
import com.tencent.tmsecurelite.commom.ServiceManager;
import com.tencent.tmsecurelite.optimize.ISystemOptimize;
import java.util.ArrayList;
import org.json.JSONException;

/* compiled from: ProGuard */
public final class q {

    /* renamed from: a  reason: collision with root package name */
    private static q f348a = null;
    /* access modifiers changed from: private */
    public ISystemOptimize b = null;
    private final t c = new t(this, null);
    /* access modifiers changed from: private */
    public volatile boolean d = false;
    /* access modifiers changed from: private */
    public volatile boolean e = false;
    private u f = new u(this, null);
    private s g = new s(this, null);
    /* access modifiers changed from: private */
    public a h = new a();
    private volatile int i = 0;

    /* access modifiers changed from: private */
    public void a(boolean z, ISystemOptimize iSystemOptimize) {
        if (!z) {
            this.b = iSystemOptimize;
        }
    }

    public static synchronized q a() {
        q qVar;
        synchronized (q.class) {
            if (f348a == null) {
                f348a = new q();
            }
            qVar = f348a;
        }
        return qVar;
    }

    public static synchronized void a(Context context) {
        synchronized (q.class) {
            if (f348a != null) {
                if (f348a.h != null && f348a.h.i) {
                    f348a.o(context, null);
                }
                f348a.b();
                Context applicationContext = context.getApplicationContext();
                if (applicationContext != null) {
                    context = applicationContext;
                }
                f348a.c(context);
                for (int i2 = 0; i2 < 1000 && f348a.b != null; i2++) {
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
                f348a = null;
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(Context context) {
        ak.a(context);
        if (this.c != null) {
            context.getApplicationContext().bindService(ServiceManager.getIntent(0), this.c, 1);
            Log.d("com.qq.connect", " bindService to com.tencent.qqpimsecure:PhonesecurManager");
        }
    }

    private void c(Context context) {
        if (this.b != null && this.c != null) {
            if (this.h != null && this.h.i) {
                try {
                    this.b.cancelScanRubbish();
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
            }
            try {
                context.getApplicationContext().unbindService(this.c);
            } catch (Throwable th) {
                th.printStackTrace();
            }
            Log.d("com.qq.connect", " unbindService  com.tencent.qqpimsecure:phoneSecurManager!");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.provider.q.a(boolean, com.tencent.tmsecurelite.optimize.ISystemOptimize):void
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.qq.provider.q.a(com.qq.provider.q, com.tencent.tmsecurelite.optimize.ISystemOptimize):com.tencent.tmsecurelite.optimize.ISystemOptimize
      com.qq.provider.q.a(com.qq.provider.q, android.content.Context):void
      com.qq.provider.q.a(com.qq.provider.q, com.qq.d.c):boolean
      com.qq.provider.q.a(com.qq.provider.q, boolean):boolean
      com.qq.provider.q.a(android.content.Context, com.qq.g.c):void
      com.qq.provider.q.a(boolean, com.tencent.tmsecurelite.optimize.ISystemOptimize):void */
    private void b() {
        a(true, (ISystemOptimize) null);
        if (this.c != null) {
            this.c.b();
        }
        a(false, (ISystemOptimize) null);
        this.h.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.provider.q.a(boolean, com.tencent.tmsecurelite.optimize.ISystemOptimize):void
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.qq.provider.q.a(com.qq.provider.q, com.tencent.tmsecurelite.optimize.ISystemOptimize):com.tencent.tmsecurelite.optimize.ISystemOptimize
      com.qq.provider.q.a(com.qq.provider.q, android.content.Context):void
      com.qq.provider.q.a(com.qq.provider.q, com.qq.d.c):boolean
      com.qq.provider.q.a(com.qq.provider.q, boolean):boolean
      com.qq.provider.q.a(android.content.Context, com.qq.g.c):void
      com.qq.provider.q.a(boolean, com.tencent.tmsecurelite.optimize.ISystemOptimize):void */
    public void a(Context context, c cVar) {
        PackageInfo packageInfo;
        boolean z;
        ArrayList arrayList = new ArrayList();
        if (this.b != null) {
            arrayList.add(r.a(2));
            cVar.a(arrayList);
            cVar.a(0);
            return;
        }
        ak.a(context);
        Log.d("com.qq.connect", "startService  phoneSecurie !!!!!!!!!!!");
        b();
        try {
            packageInfo = context.getPackageManager().getPackageInfo(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME, 0);
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            packageInfo = null;
        }
        if (packageInfo == null) {
            arrayList.add(r.a(-2));
            cVar.a(arrayList);
            cVar.a(0);
        } else if (packageInfo.versionCode < 1007) {
            arrayList.add(r.a(-1));
            cVar.a(arrayList);
            cVar.a(0);
        } else if (cVar.e() < 1) {
            cVar.a(1);
        } else {
            int h2 = cVar.h();
            if (j.c <= 10) {
                h2 += EventDispatcherEnum.CACHE_EVENT_END;
            }
            this.c.a(h2);
            Context b2 = AppService.b();
            if (b2 != null) {
                context = b2;
            }
            b(context);
            int b3 = this.c.b(h2);
            if (h2 > 0 || b3 > 0) {
                a(true, (ISystemOptimize) null);
                if (this.b == null) {
                    try {
                        Thread.sleep(450);
                    } catch (InterruptedException e3) {
                        e3.printStackTrace();
                    }
                }
                a(true, (ISystemOptimize) null);
                if (this.b == null) {
                    arrayList.add(r.a(-4));
                    cVar.a(arrayList);
                    cVar.a(0);
                    return;
                }
                try {
                    z = this.b.checkVersion(2);
                } catch (RemoteException e4) {
                    e4.printStackTrace();
                    z = false;
                }
                if (!z) {
                    b();
                    arrayList.add(r.a(-3));
                    cVar.a(arrayList);
                    cVar.a(0);
                    return;
                }
                arrayList.add(r.a(1));
                cVar.a(arrayList);
                cVar.a(0);
                return;
            }
            arrayList.add(r.a(b3));
            cVar.a(arrayList);
            cVar.a(0);
        }
    }

    public void b(Context context, c cVar) {
        a(context);
        cVar.a(0);
    }

    public void c(Context context, c cVar) {
        ArrayList arrayList = new ArrayList();
        if (this.c == null) {
            arrayList.add(r.a(-6));
            cVar.a(arrayList);
            cVar.a(0);
        } else if (this.b == null) {
            if (this.c.a()) {
                arrayList.add(r.a(0));
            } else {
                arrayList.add(r.a(-5));
            }
            cVar.a(arrayList);
            cVar.a(0);
        } else {
            arrayList.add(r.a(1));
            cVar.a(arrayList);
            cVar.a(0);
        }
    }

    public void d(Context context, c cVar) {
        if (this.b == null) {
            cVar.a(7);
            return;
        }
        try {
            boolean hasRoot = this.b.hasRoot();
            ArrayList arrayList = new ArrayList();
            if (hasRoot) {
                arrayList.add(r.a(1));
                cVar.a(arrayList);
                cVar.a(0);
                return;
            }
            this.d = false;
            this.e = true;
            new r(this).start();
            synchronized (this) {
                if (this.e) {
                    try {
                        wait(12000);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
            }
            if (this.d) {
                arrayList.add(r.a(1));
            } else {
                arrayList.add(r.a(0));
            }
            cVar.a(arrayList);
            cVar.a(0);
        } catch (RemoteException e3) {
            e3.printStackTrace();
            cVar.a(8);
        }
    }

    public void e(Context context, c cVar) {
        int i2;
        if (this.b == null) {
            cVar.a(7);
            return;
        }
        try {
            if (this.b.hasRoot()) {
                i2 = 1;
            } else {
                i2 = 0;
            }
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(i2));
            cVar.a(arrayList);
            cVar.a(0);
        } catch (RemoteException e2) {
            e2.printStackTrace();
            cVar.a(8);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0044  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void f(android.content.Context r13, com.qq.g.c r14) {
        /*
            r12 = this;
            r5 = 0
            r3 = 0
            com.tencent.tmsecurelite.optimize.ISystemOptimize r0 = r12.b
            if (r0 != 0) goto L_0x000b
            r0 = 7
            r14.a(r0)
        L_0x000a:
            return
        L_0x000b:
            com.tencent.tmsecurelite.optimize.ISystemOptimize r0 = r12.b     // Catch:{ RemoteException -> 0x0060 }
            java.util.ArrayList r7 = r0.findAppsWithCache()     // Catch:{ RemoteException -> 0x0060 }
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            byte[] r0 = com.qq.AppService.r.a(r3)
            r8.add(r0)
            if (r7 == 0) goto L_0x0072
            int r9 = r7.size()
            r6 = r3
            r1 = r3
        L_0x0025:
            if (r6 >= r9) goto L_0x0073
            java.lang.Object r0 = r7.get(r6)
            com.tencent.tmsecurelite.commom.DataEntity r0 = (com.tencent.tmsecurelite.commom.DataEntity) r0
            if (r0 == 0) goto L_0x0088
            java.lang.String r2 = "PackageName"
            java.lang.String r4 = r0.getString(r2)     // Catch:{ JSONException -> 0x006a }
            java.lang.String r2 = "CacheSize"
            long r10 = r0.getLong(r2)     // Catch:{ JSONException -> 0x0083 }
            int r2 = (int) r10
            java.lang.String r10 = "AppName"
            java.lang.String r0 = r0.getString(r10)     // Catch:{ JSONException -> 0x0086 }
        L_0x0042:
            if (r4 == 0) goto L_0x0088
            byte[] r4 = com.qq.AppService.r.a(r4)
            r8.add(r4)
            byte[] r0 = com.qq.AppService.r.a(r0)
            r8.add(r0)
            byte[] r0 = com.qq.AppService.r.a(r2)
            r8.add(r0)
            int r0 = r1 + 1
        L_0x005b:
            int r1 = r6 + 1
            r6 = r1
            r1 = r0
            goto L_0x0025
        L_0x0060:
            r0 = move-exception
            r0.printStackTrace()
            r0 = 8
            r14.a(r0)
            goto L_0x000a
        L_0x006a:
            r0 = move-exception
            r2 = r3
            r4 = r5
        L_0x006d:
            r0.printStackTrace()
            r0 = r5
            goto L_0x0042
        L_0x0072:
            r1 = r3
        L_0x0073:
            if (r1 <= 0) goto L_0x007c
            byte[] r0 = com.qq.AppService.r.a(r1)
            r8.set(r3, r0)
        L_0x007c:
            r14.a(r8)
            r14.a(r3)
            goto L_0x000a
        L_0x0083:
            r0 = move-exception
            r2 = r3
            goto L_0x006d
        L_0x0086:
            r0 = move-exception
            goto L_0x006d
        L_0x0088:
            r0 = r1
            goto L_0x005b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.q.f(android.content.Context, com.qq.g.c):void");
    }

    public void g(Context context, c cVar) {
        boolean z;
        if (this.b == null) {
            cVar.a(7);
            return;
        }
        try {
            z = this.b.clearAppsCache();
        } catch (RemoteException e2) {
            e2.printStackTrace();
            z = false;
        }
        if (!z) {
            cVar.a(4);
        } else {
            cVar.a(0);
        }
    }

    public void h(Context context, c cVar) {
        boolean z;
        if (this.b == null) {
            cVar.a(7);
        } else if (cVar.e() < 1) {
            cVar.a(1);
        } else {
            int h2 = cVar.h();
            if (h2 > 0) {
                ArrayList arrayList = new ArrayList();
                for (int i2 = 0; i2 < h2; i2++) {
                    arrayList.add(cVar.j());
                }
                try {
                    z = this.b.clearAppsCache();
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                    z = false;
                }
                if (!z) {
                    cVar.a(4);
                } else {
                    cVar.a(0);
                }
            }
        }
    }

    public void i(Context context, c cVar) {
        boolean z;
        if (this.b == null) {
            cVar.a(7);
        } else if (cVar.e() < 1) {
            cVar.a(1);
        } else {
            String j = cVar.j();
            if (r.b(j)) {
                cVar.a(1);
                return;
            }
            new ArrayList().add(j);
            try {
                z = this.b.clearAppsCache();
            } catch (RemoteException e2) {
                e2.printStackTrace();
                z = false;
            }
            if (!z) {
                cVar.a(4);
            } else {
                cVar.a(0);
            }
        }
    }

    public void j(Context context, c cVar) {
        boolean z;
        int i2;
        int i3;
        boolean z2;
        int i4;
        if (this.b == null) {
            cVar.a(7);
        } else if (cVar.e() < 1) {
            cVar.a(1);
        } else {
            if (cVar.h() > 0) {
                z = true;
            } else {
                z = false;
            }
            Log.d("com.qq.connect", "filter" + z);
            try {
                ArrayList<DataEntity> findAppsWithAutoboot = this.b.findAppsWithAutoboot(z);
                ArrayList arrayList = new ArrayList();
                arrayList.add(r.a(0));
                if (findAppsWithAutoboot != null) {
                    int size = findAppsWithAutoboot.size();
                    int i5 = 0;
                    i2 = 0;
                    while (i5 < size) {
                        DataEntity dataEntity = findAppsWithAutoboot.get(i5);
                        if (dataEntity != null) {
                            String str = null;
                            try {
                                str = dataEntity.getString(DataEntityKeyConst.PackageName_STR);
                                z2 = dataEntity.getBoolean(DataEntityKeyConst.AutoBoot_BOOL);
                            } catch (JSONException e2) {
                                e2.printStackTrace();
                                z2 = false;
                            }
                            if (str != null) {
                                arrayList.add(r.a(str));
                                if (z2) {
                                    i4 = 1;
                                } else {
                                    i4 = 0;
                                }
                                arrayList.add(r.a(i4));
                                i3 = i2 + 1;
                                i5++;
                                i2 = i3;
                            }
                        }
                        i3 = i2;
                        i5++;
                        i2 = i3;
                    }
                } else {
                    i2 = 0;
                }
                if (i2 > 0) {
                    arrayList.set(0, r.a(i2));
                }
                cVar.a(arrayList);
                cVar.a(0);
            } catch (RemoteException e3) {
                e3.printStackTrace();
                cVar.a(8);
            }
        }
    }

    public void k(Context context, c cVar) {
        boolean z;
        boolean z2;
        if (this.b == null) {
            cVar.a(7);
        } else if (cVar.e() < 2) {
            cVar.a(1);
        } else {
            int h2 = cVar.h();
            if (cVar.h() > 0) {
                z = true;
            } else {
                z = false;
            }
            if (h2 > 0) {
                ArrayList arrayList = new ArrayList();
                for (int i2 = 0; i2 < h2; i2++) {
                    arrayList.add(cVar.j());
                }
                try {
                    z2 = this.b.setAutobootStates(arrayList, z);
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                    z2 = false;
                }
                if (!z2) {
                    cVar.a(4);
                } else {
                    cVar.a(0);
                }
            }
        }
    }

    public void l(Context context, c cVar) {
        int i2;
        int i3;
        int i4;
        boolean z = true;
        if (this.b == null) {
            cVar.a(7);
        } else if (cVar.e() < 1) {
            cVar.a(1);
        } else {
            if (cVar.h() <= 0) {
                z = false;
            }
            try {
                ArrayList<DataEntity> allRuningTask = this.b.getAllRuningTask(z);
                ArrayList arrayList = new ArrayList();
                arrayList.add(r.a(0));
                if (allRuningTask != null) {
                    int size = allRuningTask.size();
                    int i5 = 0;
                    i2 = 0;
                    while (i5 < size) {
                        DataEntity dataEntity = allRuningTask.get(i5);
                        if (dataEntity != null) {
                            String str = null;
                            try {
                                str = dataEntity.getString(DataEntityKeyConst.PackageName_STR);
                                i4 = (int) dataEntity.getLong(DataEntityKeyConst.RamSize_LONG);
                            } catch (JSONException e2) {
                                e2.printStackTrace();
                                i4 = 0;
                            }
                            if (str != null) {
                                arrayList.add(r.a(str));
                                arrayList.add(r.a(i4));
                                i3 = i2 + 1;
                                i5++;
                                i2 = i3;
                            }
                        }
                        i3 = i2;
                        i5++;
                        i2 = i3;
                    }
                } else {
                    i2 = 0;
                }
                if (i2 > 0) {
                    arrayList.set(0, r.a(i2));
                }
                cVar.a(arrayList);
                cVar.a(0);
            } catch (RemoteException e3) {
                e3.printStackTrace();
                cVar.a(8);
            }
        }
    }

    public void m(Context context, c cVar) {
        boolean z;
        if (this.b == null) {
            cVar.a(7);
        } else if (cVar.e() < 1) {
            cVar.a(1);
        } else {
            String j = cVar.j();
            if (r.b(j)) {
                cVar.a(1);
                return;
            }
            try {
                z = this.b.killTask(j);
            } catch (RemoteException e2) {
                e2.printStackTrace();
                z = false;
            }
            if (!z) {
                cVar.a(4);
            } else {
                cVar.a(0);
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean a(com.qq.d.c cVar) {
        if (cVar == null) {
            return false;
        }
        if (cVar.g == 1) {
            if (this.i % 2 > 0) {
                return true;
            }
            return false;
        } else if (cVar.g == 2) {
            if (this.i % 4 >= 2) {
                return true;
            }
            return false;
        } else if (cVar.g == 3) {
            if (this.i % 8 >= 4) {
                return true;
            }
            return false;
        } else if (cVar.g != 4 || this.i % 16 < 8) {
            return false;
        } else {
            return true;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x003b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void n(android.content.Context r5, com.qq.g.c r6) {
        /*
            r4 = this;
            r1 = 0
            r0 = 1
            com.tencent.tmsecurelite.optimize.ISystemOptimize r2 = r4.b
            if (r2 != 0) goto L_0x000b
            r0 = 7
            r6.a(r0)
        L_0x000a:
            return
        L_0x000b:
            int r2 = r6.e()
            if (r2 >= r0) goto L_0x0015
            r6.a(r0)
            goto L_0x000a
        L_0x0015:
            int r2 = r6.h()
            r4.i = r2
            com.tencent.tmsecurelite.optimize.ISystemOptimize r2 = r4.b     // Catch:{ Throwable -> 0x0035 }
            com.qq.provider.s r3 = r4.g     // Catch:{ Throwable -> 0x0035 }
            int r2 = r2.startScanRubbish(r3)     // Catch:{ Throwable -> 0x0035 }
            if (r2 < 0) goto L_0x0039
            com.qq.d.a r2 = r4.h     // Catch:{ Throwable -> 0x0035 }
            r3 = 1
            r2.i = r3     // Catch:{ Throwable -> 0x0035 }
            com.qq.d.a r2 = r4.h     // Catch:{ Throwable -> 0x0035 }
            r3 = 1
            r2.k = r3     // Catch:{ Throwable -> 0x0035 }
        L_0x002f:
            if (r0 == 0) goto L_0x003b
            r6.a(r1)
            goto L_0x000a
        L_0x0035:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0039:
            r0 = r1
            goto L_0x002f
        L_0x003b:
            r0 = 8
            r6.a(r0)
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.q.n(android.content.Context, com.qq.g.c):void");
    }

    public void o(Context context, c cVar) {
        if (this.b != null) {
            try {
                this.b.cancelScanRubbish();
            } catch (Throwable th) {
                th.printStackTrace();
            }
        } else if (cVar != null) {
            cVar.a(7);
        }
    }

    public void p(Context context, c cVar) {
        boolean z = true;
        if (this.b == null) {
            cVar.a(7);
        } else if (cVar.e() < 1) {
            cVar.a(1);
        } else {
            int h2 = cVar.h();
            if (h2 <= 0) {
                cVar.a(1);
            } else if (cVar.e() < h2 + 1) {
                cVar.a(1);
            } else {
                ArrayList arrayList = new ArrayList();
                for (int i2 = 0; i2 < h2; i2++) {
                    String j = cVar.j();
                    if (j != null) {
                        arrayList.add(j);
                    }
                }
                u uVar = new u(this, null);
                try {
                    this.b.cleanRubbishAsync(uVar, arrayList);
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                    z = false;
                }
                if (!z) {
                    cVar.a(8);
                    return;
                }
                synchronized (uVar) {
                    try {
                        uVar.wait(7500);
                    } catch (InterruptedException e3) {
                        e3.printStackTrace();
                    }
                }
                cVar.a(0);
            }
        }
    }

    public void q(Context context, c cVar) {
        boolean z = true;
        if (this.b == null) {
            cVar.a(7);
        } else if (cVar.e() < 1) {
            cVar.a(1);
        } else {
            int h2 = cVar.h();
            if (h2 <= 0) {
                cVar.a(1);
            } else if (cVar.e() < h2 + 1) {
                cVar.a(1);
            } else {
                ArrayList arrayList = new ArrayList();
                for (int i2 = 0; i2 < h2; i2++) {
                    String j = cVar.j();
                    if (j != null) {
                        arrayList.add(j);
                    }
                }
                try {
                    this.b.cleanRubbishAsync(this.f, arrayList);
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                    z = false;
                }
                if (!z) {
                    cVar.a(8);
                } else {
                    cVar.a(0);
                }
            }
        }
    }
}
