package com.qq.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import com.qq.g.c;
import com.tencent.connect.common.Constants;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class aj {

    /* renamed from: a  reason: collision with root package name */
    private static aj f327a = null;

    private aj() {
    }

    public static aj a() {
        if (f327a != null) {
            return f327a;
        }
        f327a = new aj();
        return f327a;
    }

    public void a(Context context, c cVar) {
        File file;
        File file2;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (h <= 0) {
            cVar.a(1);
        } else if (cVar.e() < h + 1) {
            cVar.a(1);
        } else {
            StringBuilder sb = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();
            sb.append("video_id");
            sb.append(" in (");
            sb2.append("_id");
            sb2.append(" in (");
            for (int i = 0; i < h; i++) {
                int h2 = cVar.h();
                if (i != 0) {
                    sb.append(", ");
                }
                sb.append(h2);
                if (i != 0) {
                    sb2.append(", ");
                }
                sb2.append(h2);
            }
            sb2.append(")");
            sb.append(")");
            String sb3 = sb.toString();
            String sb4 = sb2.toString();
            Cursor query = context.getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, null, sb4, null, null);
            if (query == null) {
                cVar.a(8);
                return;
            }
            for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
                String string = query.getString(query.getColumnIndex("_data"));
                if (!(string == null || (file2 = new File(string)) == null || !file2.exists())) {
                    file2.delete();
                }
            }
            query.close();
            context.getContentResolver().delete(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, sb4, null);
            Cursor query2 = context.getContentResolver().query(MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI, null, sb3, null, null);
            if (query2 != null) {
                for (boolean moveToFirst2 = query2.moveToFirst(); moveToFirst2; moveToFirst2 = query2.moveToNext()) {
                    String string2 = query2.getString(query2.getColumnIndex("_data"));
                    if (!(string2 == null || (file = new File(string2)) == null || !file.exists())) {
                        file.delete();
                    }
                }
                query2.close();
                context.getContentResolver().delete(MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI, sb3, null);
                cVar.a(0);
            }
        }
    }

    public void b(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        Uri withAppendedPath = Uri.withAppendedPath(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        Cursor query = context.getContentResolver().query(withAppendedPath, null, null, null, null);
        if (query == null) {
            cVar.a(8);
        } else if (!query.moveToFirst()) {
            query.close();
            cVar.a(7);
        } else {
            String string = query.getString(query.getColumnIndex("_data"));
            query.close();
            if (context.getContentResolver().delete(withAppendedPath, null, null) <= 0) {
                cVar.a(8);
                return;
            }
            try {
                new File(string).delete();
            } catch (Exception e) {
                e.printStackTrace();
            }
            cVar.a(0);
            Cursor query2 = context.getContentResolver().query(MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI, null, "video_id = " + h, null, null);
            if (query2 != null) {
                boolean moveToFirst = query2.moveToFirst();
                if (!moveToFirst) {
                    query2.close();
                    return;
                }
                while (moveToFirst) {
                    try {
                        new File(query2.getString(query2.getColumnIndex("_data"))).delete();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    moveToFirst = query2.moveToNext();
                }
                query2.close();
                context.getContentResolver().delete(MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI, "video_id = " + h, null);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void c(Context context, c cVar) {
        Uri uri;
        int i;
        int i2;
        byte[] bArr;
        Uri withAppendedPath;
        String str;
        boolean z;
        boolean z2;
        Uri uri2;
        Uri withAppendedPath2;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        ArrayList arrayList = new ArrayList();
        if (h2 > 0) {
            uri = MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI;
        } else {
            uri = MediaStore.Video.Thumbnails.INTERNAL_CONTENT_URI;
        }
        Cursor query = context.getContentResolver().query(uri, null, "video_id = " + h, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        int i3 = 0;
        String str2 = null;
        boolean moveToFirst = query.moveToFirst();
        while (true) {
            if (!moveToFirst) {
                i = i3;
                break;
            }
            int i4 = query.getInt(query.getColumnIndex("kind"));
            if (i4 == 1) {
                i3 = query.getInt(query.getColumnIndex("_id"));
                str2 = query.getString(query.getColumnIndex("_data"));
                if (!new File(str2).exists()) {
                    str2 = null;
                }
                if (str2 != null) {
                    i = i3;
                    break;
                }
            } else if (i4 == 3 && str2 == null) {
                i3 = query.getInt(query.getColumnIndex("_id"));
                str2 = query.getString(query.getColumnIndex("_data"));
                if (!new File(str2).exists()) {
                    str2 = null;
                }
            }
            moveToFirst = query.moveToNext();
        }
        query.close();
        if (str2 == null) {
            context.getContentResolver().delete(uri, "video_id = " + h, null);
            str2 = null;
        }
        if (str2 == null) {
            if (h2 > 0) {
                withAppendedPath = Uri.withAppendedPath(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
            } else {
                withAppendedPath = Uri.withAppendedPath(MediaStore.Video.Media.INTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
            }
            Cursor query2 = context.getContentResolver().query(withAppendedPath, null, null, null, null);
            if (query2 == null) {
                cVar.a(8);
                return;
            }
            if (query2.moveToFirst()) {
                str = query2.getString(query2.getColumnIndex("_data"));
            } else {
                str = null;
            }
            query2.close();
            if (str == null) {
                cVar.a(1);
                return;
            }
            Bitmap createVideoThumbnail = ThumbnailUtils.createVideoThumbnail(str, 1);
            if (createVideoThumbnail == null) {
                cVar.a(8);
                return;
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            createVideoThumbnail.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            createVideoThumbnail.recycle();
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            try {
                byteArrayOutputStream.close();
                arrayList.add(byteArray);
                File externalStorageDirectory = Environment.getExternalStorageDirectory();
                if (externalStorageDirectory != null && externalStorageDirectory.exists()) {
                    File file = new File(externalStorageDirectory.getAbsolutePath() + File.separator + "Video" + File.separator + ".thumbnails" + File.separator + "." + createVideoThumbnail.hashCode() + ".png");
                    file.getParentFile().mkdirs();
                    try {
                        z = file.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                        z = false;
                    }
                    if (z) {
                        BufferedOutputStream bufferedOutputStream = null;
                        try {
                            bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
                        } catch (FileNotFoundException e2) {
                            e2.printStackTrace();
                        }
                        if (bufferedOutputStream != null) {
                            try {
                                bufferedOutputStream.write(byteArray);
                                bufferedOutputStream.flush();
                                z2 = z;
                            } catch (IOException e3) {
                                e3.printStackTrace();
                                z2 = false;
                            }
                            try {
                                bufferedOutputStream.close();
                            } catch (IOException e4) {
                                e4.printStackTrace();
                            }
                            if (z2) {
                                ContentValues contentValues = new ContentValues();
                                contentValues.put("video_id", Integer.valueOf(h));
                                contentValues.put("_data", file.getAbsolutePath());
                                contentValues.put("kind", (Integer) 1);
                                contentValues.put("height", Integer.valueOf(createVideoThumbnail.getHeight()));
                                contentValues.put("width", Integer.valueOf(createVideoThumbnail.getWidth()));
                                if (i > 0) {
                                    if (h2 > 0) {
                                        withAppendedPath2 = Uri.withAppendedPath(MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + i);
                                    } else {
                                        withAppendedPath2 = Uri.withAppendedPath(MediaStore.Video.Thumbnails.INTERNAL_CONTENT_URI, Constants.STR_EMPTY + i);
                                    }
                                    context.getContentResolver().update(withAppendedPath2, contentValues, null, null);
                                } else {
                                    if (h2 > 0) {
                                        uri2 = MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI;
                                    } else {
                                        uri2 = MediaStore.Video.Thumbnails.INTERNAL_CONTENT_URI;
                                    }
                                    context.getContentResolver().insert(uri2, contentValues);
                                }
                            }
                        }
                    }
                }
            } catch (IOException e5) {
                e5.printStackTrace();
                cVar.a(8);
                return;
            }
        } else {
            try {
                InputStream openInputStream = context.getContentResolver().openInputStream(Uri.fromFile(new File(str2)));
                try {
                    i2 = openInputStream.available();
                } catch (IOException e6) {
                    e6.printStackTrace();
                    i2 = 0;
                }
                if (i2 > 0) {
                    bArr = new byte[i2];
                } else {
                    bArr = null;
                }
                int i5 = 0;
                if (bArr != null) {
                    try {
                        i5 = openInputStream.read(bArr);
                    } catch (IOException e7) {
                        e7.printStackTrace();
                    }
                }
                try {
                    openInputStream.close();
                } catch (IOException e8) {
                    e8.printStackTrace();
                }
                if (bArr == null || i5 != i2) {
                    cVar.a(7);
                } else {
                    arrayList.add(bArr);
                }
            } catch (FileNotFoundException e9) {
                e9.printStackTrace();
                cVar.a(7);
                return;
            }
        }
        cVar.a(arrayList);
    }
}
