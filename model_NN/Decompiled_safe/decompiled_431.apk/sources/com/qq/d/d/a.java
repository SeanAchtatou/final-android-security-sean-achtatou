package com.qq.d.d;

import android.database.Cursor;
import com.qq.AppService.r;
import com.tencent.open.SocialConstants;
import java.io.File;

/* compiled from: ProGuard */
public class a {
    public static int p = 13;

    /* renamed from: a  reason: collision with root package name */
    public int f278a = 0;
    public String b = null;
    public String c = null;
    public long d = 0;
    public String e = null;
    public String f = null;
    public String g = null;
    public String h = null;
    public String i = null;
    public String j = null;
    public int k = 0;
    public int l = 0;
    public int m = 0;
    public String n = null;
    public int o = 0;

    public a() {
    }

    public a(Cursor cursor, String str) {
        String string;
        if (cursor != null) {
            if (str == null || ((string = cursor.getString(cursor.getColumnIndex("_data"))) != null && string.toLowerCase().startsWith(str))) {
                this.f278a = cursor.getInt(cursor.getColumnIndex("_id"));
                this.b = cursor.getString(cursor.getColumnIndex("_data"));
                this.c = cursor.getString(cursor.getColumnIndex("_display_name"));
                this.d = (long) cursor.getInt(cursor.getColumnIndex("_size"));
                this.e = cursor.getString(cursor.getColumnIndex("title"));
                this.f = cursor.getString(cursor.getColumnIndex("mime_type"));
                this.g = cursor.getString(cursor.getColumnIndex(SocialConstants.PARAM_COMMENT));
                long j2 = 1000 * cursor.getLong(cursor.getColumnIndex("date_added"));
                long j3 = 1000 * cursor.getLong(cursor.getColumnIndex("date_modified"));
                long j4 = cursor.getLong(cursor.getColumnIndex("datetaken"));
                if (j3 == 0 && j4 != 0) {
                    j3 = j4;
                } else if (j3 == 0 && j2 != 0) {
                    j3 = j2;
                } else if (j3 == 0 || j3 > System.currentTimeMillis()) {
                    j3 = new File(this.b).lastModified();
                }
                j2 = j2 < 0 ? 0 : j2;
                j2 = j2 > 3153600000000L ? j2 / 1000 : j2;
                j4 = j4 < 0 ? 0 : j4;
                j4 = j4 > 3153600000000L ? j4 / 1000 : j4;
                this.h = r.b(j2);
                this.i = r.b(j3);
                this.j = r.b(j4);
                this.k = cursor.getInt(cursor.getColumnIndex("isprivate"));
                if (this.d == 0 && this.b != null) {
                    this.d = new File(this.b).length();
                }
                this.o = cursor.getInt(cursor.getColumnIndex("orientation"));
            }
        }
    }
}
