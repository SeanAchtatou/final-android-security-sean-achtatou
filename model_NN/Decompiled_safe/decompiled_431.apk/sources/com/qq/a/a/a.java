package com.qq.a.a;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

/* compiled from: ProGuard */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    public static Uri f255a = null;
    public static boolean b = false;
    private final Uri[] c = {Uri.parse("content://com.android.alarmclock/alarm"), Uri.parse("content://com.android.deskclock/alarm"), Uri.parse("content://com.clock.alarm/alarm"), Uri.parse("content://com.htc.android.deskclock/alarm"), Uri.parse("content://com.htc.android.alarmclock/alarm"), Uri.parse("content://com.samsung.sec.android.clockpackage/alarm"), Uri.parse("content://com.motorola.blur.alarmclock/alarm"), Uri.parse("content://zte.com.cn.alarmclock/alarm")};

    public static Uri a(Context context) {
        if (f255a != null) {
            return f255a;
        }
        a aVar = new a();
        int i = 0;
        while (true) {
            if (i >= aVar.c.length) {
                break;
            }
            Cursor query = context.getContentResolver().query(aVar.c[i], new String[]{"_id"}, null, null, null);
            if (query != null) {
                query.close();
                f255a = aVar.c[i];
                if (i == 5) {
                    b = true;
                } else {
                    b = false;
                }
            } else {
                i++;
            }
        }
        return f255a;
    }
}
