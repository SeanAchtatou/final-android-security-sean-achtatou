package com.qq.AppService;

import com.qq.l.p;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.wcs.proxy.d.a;

/* compiled from: ProGuard */
class f implements a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppService f239a;

    f(AppService appService) {
        this.f239a = appService;
    }

    public void a() {
        if (!AppService.q() && AppService.u()) {
            AppService.j();
        }
        AstApp.i().j().sendEmptyMessage(EventDispatcherEnum.CONNECTION_EVENT_WCS_STOP);
    }

    public void a(boolean z) {
        if (z && !AppService.u()) {
            AppService.E();
        }
        if (!z) {
            p.m().c(p.m().p().c(), 2, -1);
            AppService.B();
        } else {
            p.m().c(p.m().p().c(), 1, -1);
        }
        AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.CONNECTION_EVENT_WCS_START, Boolean.valueOf(z)));
    }

    public void b() {
        if (!AppService.q() && AppService.u()) {
            AppService.j();
            AstApp.i().j().sendEmptyMessage(EventDispatcherEnum.CONNECTION_EVENT_WCS_NETWORK_LOST);
        }
        AppService.B();
    }
}
