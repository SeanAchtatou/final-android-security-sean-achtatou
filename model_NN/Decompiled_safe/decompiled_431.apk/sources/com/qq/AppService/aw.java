package com.qq.AppService;

import android.content.Context;
import android.content.Intent;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

/* compiled from: ProGuard */
public final class aw extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private Context f229a = null;
    private boolean b = true;
    private ServerSocket c = null;
    private ao d = null;

    public aw(Context context) {
        this.f229a = context;
        this.b = true;
        try {
            this.c = new ServerSocket();
            this.c.bind(new InetSocketAddress(14088), 6);
            this.c.setReuseAddress(true);
        } catch (Throwable th) {
            th.printStackTrace();
            this.b = false;
        }
        if (!this.b && this.c != null) {
            try {
                this.c.close();
            } catch (Throwable th2) {
                th2.printStackTrace();
            }
            this.c = null;
        }
        if (this.c == null) {
            Intent intent = new Intent();
            intent.setClass(context, AppService.class);
            context.stopService(intent);
        }
    }

    public void a() {
        this.b = false;
        if (this.d != null) {
            this.d.a();
        }
        try {
            if (this.c != null) {
                this.c.close();
            }
            this.c = null;
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void run() {
        Socket socket;
        while (this.b && this.c != null) {
            try {
                socket = this.c.accept();
            } catch (Exception e) {
                e.printStackTrace();
                socket = null;
            }
            if (socket != null) {
                if (this.d != null) {
                    this.d.a(socket);
                } else {
                    this.d = new ao(this.f229a);
                    this.d.a(socket);
                }
            }
        }
    }
}
