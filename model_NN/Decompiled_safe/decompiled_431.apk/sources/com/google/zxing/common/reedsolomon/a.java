package com.google.zxing.common.reedsolomon;

import android.os.Process;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    public static final a f169a = new a(285);
    public static final a b = new a(301);
    private final int[] c = new int[Process.PROC_COMBINE];
    private final int[] d = new int[Process.PROC_COMBINE];
    private final b e;
    private final b f;

    private a(int i) {
        int i2 = 1;
        for (int i3 = 0; i3 < 256; i3++) {
            this.c[i3] = i2;
            i2 <<= 1;
            if (i2 >= 256) {
                i2 ^= i;
            }
        }
        for (int i4 = 0; i4 < 255; i4++) {
            this.d[this.c[i4]] = i4;
        }
        this.e = new b(this, new int[]{0});
        this.f = new b(this, new int[]{1});
    }

    static int b(int i, int i2) {
        return i ^ i2;
    }

    /* access modifiers changed from: package-private */
    public int a(int i) {
        return this.c[i];
    }

    /* access modifiers changed from: package-private */
    public b a() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public b a(int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException();
        } else if (i2 == 0) {
            return this.e;
        } else {
            int[] iArr = new int[(i + 1)];
            iArr[0] = i2;
            return new b(this, iArr);
        }
    }

    /* access modifiers changed from: package-private */
    public int b(int i) {
        if (i != 0) {
            return this.d[i];
        }
        throw new IllegalArgumentException();
    }

    /* access modifiers changed from: package-private */
    public b b() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public int c(int i) {
        if (i != 0) {
            return this.c[255 - this.d[i]];
        }
        throw new ArithmeticException();
    }

    /* access modifiers changed from: package-private */
    public int c(int i, int i2) {
        if (i == 0 || i2 == 0) {
            return 0;
        }
        int i3 = this.d[i] + this.d[i2];
        return this.c[(i3 >>> 8) + (i3 & 255)];
    }
}
