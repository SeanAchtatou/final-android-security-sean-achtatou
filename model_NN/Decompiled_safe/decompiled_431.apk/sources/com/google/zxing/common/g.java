package com.google.zxing.common;

import com.google.zxing.d.a.o;
import java.util.Vector;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f161a;
    private final String b;
    private final Vector c;
    private final o d;

    public g(byte[] bArr, String str, Vector vector, o oVar) {
        if (bArr == null && str == null) {
            throw new IllegalArgumentException();
        }
        this.f161a = bArr;
        this.b = str;
        this.c = vector;
        this.d = oVar;
    }

    public byte[] a() {
        return this.f161a;
    }

    public String b() {
        return this.b;
    }

    public Vector c() {
        return this.c;
    }

    public o d() {
        return this.d;
    }
}
