package com.google.zxing.b;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.a;
import com.google.zxing.h;
import com.google.zxing.i;
import com.google.zxing.j;
import com.tencent.connect.common.Constants;
import java.util.Hashtable;

final class m {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f148a = {1, 1, 2};
    private static final int[] b = {24, 20, 18, 17, 12, 6, 3, 10, 9, 5};
    private final int[] c = new int[4];
    private final StringBuffer d = new StringBuffer();

    m() {
    }

    private static int a(int i) {
        for (int i2 = 0; i2 < 10; i2++) {
            if (i == b[i2]) {
                return i2;
            }
        }
        throw NotFoundException.a();
    }

    private static int a(String str) {
        int length = str.length();
        int i = 0;
        for (int i2 = length - 2; i2 >= 0; i2 -= 2) {
            i += str.charAt(i2) - '0';
        }
        int i3 = i * 3;
        for (int i4 = length - 1; i4 >= 0; i4 -= 2) {
            i3 += str.charAt(i4) - '0';
        }
        return (i3 * 3) % 10;
    }

    private static Hashtable b(String str) {
        i iVar;
        Object d2;
        switch (str.length()) {
            case 2:
                iVar = i.e;
                d2 = c(str);
                break;
            case 3:
            case 4:
            default:
                return null;
            case 5:
                iVar = i.f;
                d2 = d(str);
                break;
        }
        if (d2 == null) {
            return null;
        }
        Hashtable hashtable = new Hashtable(1);
        hashtable.put(iVar, d2);
        return hashtable;
    }

    private static Integer c(String str) {
        return Integer.valueOf(str);
    }

    private static String d(String str) {
        String str2 = null;
        switch (str.charAt(0)) {
            case '0':
                str2 = "拢";
                break;
            case '5':
                str2 = "$";
                break;
            case '9':
                if ("99991".equals(str)) {
                    return "0.00";
                }
                if ("99990".equals(str)) {
                    return "Used";
                }
                break;
            default:
                str2 = Constants.STR_EMPTY;
                break;
        }
        int parseInt = Integer.parseInt(str.substring(1));
        return new StringBuffer().append(str2).append(parseInt / 100).append('.').append(parseInt % 100).toString();
    }

    /* access modifiers changed from: package-private */
    public int a(a aVar, int[] iArr, StringBuffer stringBuffer) {
        int[] iArr2 = this.c;
        iArr2[0] = 0;
        iArr2[1] = 0;
        iArr2[2] = 0;
        iArr2[3] = 0;
        int a2 = aVar.a();
        int i = iArr[1];
        int i2 = 0;
        int i3 = 0;
        while (i2 < 5 && i < a2) {
            int a3 = n.a(aVar, iArr2, i, n.e);
            stringBuffer.append((char) ((a3 % 10) + 48));
            int i4 = i;
            for (int i5 : iArr2) {
                i4 += i5;
            }
            int i6 = a3 >= 10 ? (1 << (4 - i2)) | i3 : i3;
            if (i2 != 4) {
                while (i4 < a2 && !aVar.a(i4)) {
                    i4++;
                }
                while (i4 < a2 && aVar.a(i4)) {
                    i4++;
                }
            }
            i2++;
            i3 = i6;
            i = i4;
        }
        if (stringBuffer.length() != 5) {
            throw NotFoundException.a();
        }
        if (a(stringBuffer.toString()) == a(i3)) {
            return i;
        }
        throw NotFoundException.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.zxing.b.n.a(com.google.zxing.common.a, int, boolean, int[]):int[]
     arg types: [com.google.zxing.common.a, int, int, int[]]
     candidates:
      com.google.zxing.b.n.a(com.google.zxing.common.a, int[], int, int[][]):int
      com.google.zxing.b.n.a(int, com.google.zxing.common.a, int[], java.util.Hashtable):com.google.zxing.h
      com.google.zxing.b.n.a(com.google.zxing.common.a, int, boolean, int[]):int[] */
    /* access modifiers changed from: package-private */
    public h a(int i, a aVar, int i2) {
        int[] a2 = n.a(aVar, i2, false, f148a);
        StringBuffer stringBuffer = this.d;
        stringBuffer.setLength(0);
        int a3 = a(aVar, a2, stringBuffer);
        String stringBuffer2 = stringBuffer.toString();
        Hashtable b2 = b(stringBuffer2);
        h hVar = new h(stringBuffer2, null, new j[]{new j(((float) (a2[1] + a2[0])) / 2.0f, (float) i), new j((float) a3, (float) i)}, com.google.zxing.a.g);
        if (b2 != null) {
            hVar.a(b2);
        }
        return hVar;
    }
}
