package com.google.zxing.b;

import com.google.zxing.FormatException;
import com.google.zxing.a;
import com.google.zxing.c;
import com.google.zxing.h;
import java.util.Hashtable;

public final class l extends n {

    /* renamed from: a  reason: collision with root package name */
    private final n f147a = new e();

    private static h a(h hVar) {
        String a2 = hVar.a();
        if (a2.charAt(0) == '0') {
            return new h(a2.substring(1), null, hVar.b(), a.d);
        }
        throw FormatException.a();
    }

    /* access modifiers changed from: protected */
    public int a(com.google.zxing.common.a aVar, int[] iArr, StringBuffer stringBuffer) {
        return this.f147a.a(aVar, iArr, stringBuffer);
    }

    public h a(int i, com.google.zxing.common.a aVar, Hashtable hashtable) {
        return a(this.f147a.a(i, aVar, hashtable));
    }

    public h a(int i, com.google.zxing.common.a aVar, int[] iArr, Hashtable hashtable) {
        return a(this.f147a.a(i, aVar, iArr, hashtable));
    }

    public h a(c cVar, Hashtable hashtable) {
        return a(this.f147a.a(cVar, hashtable));
    }

    /* access modifiers changed from: package-private */
    public a b() {
        return a.d;
    }
}
