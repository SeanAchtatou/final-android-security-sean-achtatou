package com.google.zxing.a.b;

import com.google.zxing.j;

class c {

    /* renamed from: a  reason: collision with root package name */
    private final j f124a;
    private final j b;
    private final int c;

    private c(j jVar, j jVar2, int i) {
        this.f124a = jVar;
        this.b = jVar2;
        this.c = i;
    }

    c(j jVar, j jVar2, int i, b bVar) {
        this(jVar, jVar2, i);
    }

    public j a() {
        return this.f124a;
    }

    public j b() {
        return this.b;
    }

    public int c() {
        return this.c;
    }

    public String toString() {
        return new StringBuffer().append(this.f124a).append("/").append(this.b).append('/').append(this.c).toString();
    }
}
