package com.google.zxing.a.a;

final class g {

    /* renamed from: a  reason: collision with root package name */
    private final int f121a;
    private final int b;

    private g(int i, int i2) {
        this.f121a = i;
        this.b = i2;
    }

    g(int i, int i2, f fVar) {
        this(i, i2);
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.f121a;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.b;
    }
}
