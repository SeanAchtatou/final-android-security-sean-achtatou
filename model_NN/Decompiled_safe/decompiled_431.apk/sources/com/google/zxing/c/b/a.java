package com.google.zxing.c.b;

import com.google.zxing.NotFoundException;
import com.google.zxing.c;
import com.google.zxing.common.b;
import com.google.zxing.common.i;
import com.google.zxing.common.l;
import com.google.zxing.j;
import java.util.Hashtable;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f155a = {8, 1, 1, 1, 1, 1, 1, 3};
    private static final int[] b = {3, 1, 1, 1, 1, 1, 1, 8};
    private static final int[] c = {7, 1, 1, 3, 1, 1, 1, 2, 1};
    private static final int[] d = {1, 2, 1, 1, 1, 3, 1, 1, 7};
    private final c e;

    public a(c cVar) {
        this.e = cVar;
    }

    private static float a(j[] jVarArr) {
        return (((j.a(jVarArr[0], jVarArr[4]) + j.a(jVarArr[1], jVarArr[5])) / 34.0f) + ((j.a(jVarArr[6], jVarArr[2]) + j.a(jVarArr[7], jVarArr[3])) / 36.0f)) / 2.0f;
    }

    private static int a(float f) {
        return (int) (0.5f + f);
    }

    private static int a(j jVar, j jVar2, j jVar3, j jVar4, float f) {
        return ((((a(j.a(jVar, jVar2) / f) + a(j.a(jVar3, jVar4) / f)) >> 1) + 8) / 17) * 17;
    }

    private static int a(int[] iArr, int[] iArr2, int i) {
        int length = iArr.length;
        int i2 = 0;
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4++) {
            i3 += iArr[i4];
            i2 += iArr2[i4];
        }
        if (i3 < i2) {
            return Integer.MAX_VALUE;
        }
        int i5 = (i3 << 8) / i2;
        int i6 = (i * i5) >> 8;
        int i7 = 0;
        for (int i8 = 0; i8 < length; i8++) {
            int i9 = iArr[i8] << 8;
            int i10 = iArr2[i8] * i5;
            int i11 = i9 > i10 ? i9 - i10 : i10 - i9;
            if (i11 > i6) {
                return Integer.MAX_VALUE;
            }
            i7 += i11;
        }
        return i7 / i3;
    }

    private static b a(b bVar, j jVar, j jVar2, j jVar3, j jVar4, int i) {
        return l.a().a(bVar, i, 0.0f, 0.0f, (float) i, 0.0f, (float) i, (float) i, 0.0f, (float) i, jVar.a(), jVar.b(), jVar3.a(), jVar3.b(), jVar4.a(), jVar4.b(), jVar2.a(), jVar2.b());
    }

    private static void a(j[] jVarArr, boolean z) {
        float b2 = jVarArr[4].b() - jVarArr[6].b();
        if (z) {
            b2 = -b2;
        }
        if (b2 > 2.0f) {
            float a2 = jVarArr[4].a() - jVarArr[0].a();
            jVarArr[4] = new j(jVarArr[4].a(), ((a2 * (jVarArr[6].b() - jVarArr[0].b())) / (jVarArr[6].a() - jVarArr[0].a())) + jVarArr[4].b());
        } else if ((-b2) > 2.0f) {
            float a3 = jVarArr[2].a() - jVarArr[6].a();
            float a4 = jVarArr[2].a() - jVarArr[4].a();
            jVarArr[6] = new j(jVarArr[6].a(), jVarArr[6].b() - ((a3 * (jVarArr[2].b() - jVarArr[4].b())) / a4));
        }
        float b3 = jVarArr[7].b() - jVarArr[5].b();
        if (z) {
            b3 = -b3;
        }
        if (b3 > 2.0f) {
            float a5 = jVarArr[5].a() - jVarArr[1].a();
            jVarArr[5] = new j(jVarArr[5].a(), ((a5 * (jVarArr[7].b() - jVarArr[1].b())) / (jVarArr[7].a() - jVarArr[1].a())) + jVarArr[5].b());
        } else if ((-b3) > 2.0f) {
            float a6 = jVarArr[3].a() - jVarArr[7].a();
            float a7 = jVarArr[3].a() - jVarArr[5].a();
            jVarArr[7] = new j(jVarArr[7].a(), jVarArr[7].b() - ((a6 * (jVarArr[3].b() - jVarArr[5].b())) / a7));
        }
    }

    private static int[] a(b bVar, int i, int i2, int i3, boolean z, int[] iArr) {
        int length = iArr.length;
        int[] iArr2 = new int[length];
        int i4 = 0;
        int i5 = i;
        boolean z2 = z;
        for (int i6 = i; i6 < i + i3; i6++) {
            if (bVar.a(i6, i2) ^ z2) {
                iArr2[i4] = iArr2[i4] + 1;
            } else {
                if (i4 != length - 1) {
                    i4++;
                } else if (a(iArr2, iArr, 204) < 107) {
                    return new int[]{i5, i6};
                } else {
                    i5 += iArr2[0] + iArr2[1];
                    for (int i7 = 2; i7 < length; i7++) {
                        iArr2[i7 - 2] = iArr2[i7];
                    }
                    iArr2[length - 2] = 0;
                    iArr2[length - 1] = 0;
                    i4--;
                }
                iArr2[i4] = 1;
                z2 = !z2;
            }
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.zxing.c.b.a.a(com.google.zxing.common.b, int, int, int, boolean, int[]):int[]
     arg types: [com.google.zxing.common.b, int, int, int, int, int[]]
     candidates:
      com.google.zxing.c.b.a.a(com.google.zxing.common.b, com.google.zxing.j, com.google.zxing.j, com.google.zxing.j, com.google.zxing.j, int):com.google.zxing.common.b
      com.google.zxing.c.b.a.a(com.google.zxing.common.b, int, int, int, boolean, int[]):int[] */
    private static j[] a(b bVar) {
        boolean z;
        boolean z2 = false;
        int c2 = bVar.c();
        int b2 = bVar.b();
        j[] jVarArr = new j[8];
        int i = 0;
        while (true) {
            if (i >= c2) {
                z = false;
                break;
            }
            int[] a2 = a(bVar, 0, i, b2, false, f155a);
            if (a2 != null) {
                jVarArr[0] = new j((float) a2[0], (float) i);
                jVarArr[4] = new j((float) a2[1], (float) i);
                z = true;
                break;
            }
            i++;
        }
        if (z) {
            int i2 = c2 - 1;
            while (true) {
                if (i2 <= 0) {
                    z = false;
                    break;
                }
                int[] a3 = a(bVar, 0, i2, b2, false, f155a);
                if (a3 != null) {
                    jVarArr[1] = new j((float) a3[0], (float) i2);
                    jVarArr[5] = new j((float) a3[1], (float) i2);
                    z = true;
                    break;
                }
                i2--;
            }
        }
        if (z) {
            int i3 = 0;
            while (true) {
                if (i3 >= c2) {
                    z = false;
                    break;
                }
                int[] a4 = a(bVar, 0, i3, b2, false, c);
                if (a4 != null) {
                    jVarArr[2] = new j((float) a4[1], (float) i3);
                    jVarArr[6] = new j((float) a4[0], (float) i3);
                    z = true;
                    break;
                }
                i3++;
            }
        }
        if (z) {
            int i4 = c2 - 1;
            while (true) {
                if (i4 <= 0) {
                    break;
                }
                int[] a5 = a(bVar, 0, i4, b2, false, c);
                if (a5 != null) {
                    jVarArr[3] = new j((float) a5[1], (float) i4);
                    jVarArr[7] = new j((float) a5[0], (float) i4);
                    z2 = true;
                    break;
                }
                i4--;
            }
        } else {
            z2 = z;
        }
        if (z2) {
            return jVarArr;
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.zxing.c.b.a.a(com.google.zxing.common.b, int, int, int, boolean, int[]):int[]
     arg types: [com.google.zxing.common.b, int, int, int, int, int[]]
     candidates:
      com.google.zxing.c.b.a.a(com.google.zxing.common.b, com.google.zxing.j, com.google.zxing.j, com.google.zxing.j, com.google.zxing.j, int):com.google.zxing.common.b
      com.google.zxing.c.b.a.a(com.google.zxing.common.b, int, int, int, boolean, int[]):int[] */
    private static j[] b(b bVar) {
        boolean z;
        boolean z2 = true;
        int c2 = bVar.c();
        int b2 = bVar.b() >> 1;
        j[] jVarArr = new j[8];
        int i = c2 - 1;
        while (true) {
            if (i <= 0) {
                z = false;
                break;
            }
            int[] a2 = a(bVar, b2, i, b2, true, b);
            if (a2 != null) {
                jVarArr[0] = new j((float) a2[1], (float) i);
                jVarArr[4] = new j((float) a2[0], (float) i);
                z = true;
                break;
            }
            i--;
        }
        if (z) {
            int i2 = 0;
            while (true) {
                if (i2 >= c2) {
                    z = false;
                    break;
                }
                int[] a3 = a(bVar, b2, i2, b2, true, b);
                if (a3 != null) {
                    jVarArr[1] = new j((float) a3[1], (float) i2);
                    jVarArr[5] = new j((float) a3[0], (float) i2);
                    z = true;
                    break;
                }
                i2++;
            }
        }
        if (z) {
            int i3 = c2 - 1;
            while (true) {
                if (i3 <= 0) {
                    z = false;
                    break;
                }
                int[] a4 = a(bVar, 0, i3, b2, false, d);
                if (a4 != null) {
                    jVarArr[2] = new j((float) a4[0], (float) i3);
                    jVarArr[6] = new j((float) a4[1], (float) i3);
                    z = true;
                    break;
                }
                i3--;
            }
        }
        if (z) {
            int i4 = 0;
            while (true) {
                if (i4 >= c2) {
                    z2 = false;
                    break;
                }
                int[] a5 = a(bVar, 0, i4, b2, false, d);
                if (a5 != null) {
                    jVarArr[3] = new j((float) a5[0], (float) i4);
                    jVarArr[7] = new j((float) a5[1], (float) i4);
                    break;
                }
                i4++;
            }
        } else {
            z2 = z;
        }
        if (z2) {
            return jVarArr;
        }
        return null;
    }

    public i a() {
        return a((Hashtable) null);
    }

    public i a(Hashtable hashtable) {
        j[] jVarArr;
        b c2 = this.e.c();
        j[] a2 = a(c2);
        if (a2 == null) {
            a2 = b(c2);
            if (a2 != null) {
                a(a2, true);
                jVarArr = a2;
            }
            jVarArr = a2;
        } else {
            a(a2, false);
            jVarArr = a2;
        }
        if (jVarArr == null) {
            throw NotFoundException.a();
        }
        float a3 = a(jVarArr);
        if (a3 < 1.0f) {
            throw NotFoundException.a();
        }
        int a4 = a(jVarArr[4], jVarArr[6], jVarArr[5], jVarArr[7], a3);
        if (a4 < 1) {
            throw NotFoundException.a();
        }
        return new i(a(c2, jVarArr[4], jVarArr[5], jVarArr[6], jVarArr[7], a4), new j[]{jVarArr[4], jVarArr[5], jVarArr[6], jVarArr[7]});
    }
}
