package defpackage;

import android.content.Context;
import defpackage.ac;

/* renamed from: az  reason: default package */
public class az {
    /* JADX WARNING: Removed duplicated region for block: B:41:0x007e A[SYNTHETIC, Splitter:B:41:0x007e] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x008b A[SYNTHETIC, Splitter:B:48:0x008b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int a(android.content.Context r9, java.lang.String r10) {
        /*
            r6 = 16
            r1 = 0
            r0 = -1
            r2 = -2
            java.io.File r5 = new java.io.File
            r5.<init>(r10)
            boolean r3 = r5.exists()
            if (r3 != 0) goto L_0x0011
        L_0x0010:
            return r0
        L_0x0011:
            r4 = 0
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ IOException -> 0x009b, Exception -> 0x0077, all -> 0x0087 }
            r3.<init>(r5)     // Catch:{ IOException -> 0x009b, Exception -> 0x0077, all -> 0x0087 }
            r3.read()     // Catch:{ IOException -> 0x003e, Exception -> 0x0099 }
            r4 = 4
            byte[] r4 = new byte[r4]     // Catch:{ IOException -> 0x003e, Exception -> 0x0099 }
            r3.read(r4)     // Catch:{ IOException -> 0x003e, Exception -> 0x0099 }
            r4 = 16
            byte[] r4 = new byte[r4]     // Catch:{ IOException -> 0x003e, Exception -> 0x0099 }
            int r5 = r3.read(r4)     // Catch:{ IOException -> 0x003e, Exception -> 0x0099 }
            if (r5 != r6) goto L_0x0067
            java.io.ByteArrayOutputStream r5 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x003e, Exception -> 0x0099 }
            r5.<init>()     // Catch:{ IOException -> 0x003e, Exception -> 0x0099 }
            r6 = 1024(0x400, float:1.435E-42)
            byte[] r6 = new byte[r6]     // Catch:{ IOException -> 0x003e, Exception -> 0x0099 }
        L_0x0033:
            int r7 = r3.read(r6)     // Catch:{ IOException -> 0x003e, Exception -> 0x0099 }
            if (r7 == r0) goto L_0x004a
            r8 = 0
            r5.write(r6, r8, r7)     // Catch:{ IOException -> 0x003e, Exception -> 0x0099 }
            goto L_0x0033
        L_0x003e:
            r0 = move-exception
            r1 = r3
        L_0x0040:
            r0.printStackTrace()     // Catch:{ all -> 0x0096 }
            if (r1 == 0) goto L_0x0048
            r1.close()     // Catch:{ IOException -> 0x0072 }
        L_0x0048:
            r0 = r2
            goto L_0x0010
        L_0x004a:
            byte[] r0 = r5.toByteArray()     // Catch:{ IOException -> 0x003e, Exception -> 0x0099 }
            byte[] r0 = defpackage.ay.b(r0)     // Catch:{ IOException -> 0x003e, Exception -> 0x0099 }
            r5.close()     // Catch:{ IOException -> 0x003e, Exception -> 0x0099 }
            boolean r0 = java.util.Arrays.equals(r0, r4)     // Catch:{ IOException -> 0x003e, Exception -> 0x0099 }
            if (r0 == 0) goto L_0x0067
            if (r3 == 0) goto L_0x0060
            r3.close()     // Catch:{ IOException -> 0x0062 }
        L_0x0060:
            r0 = r1
            goto L_0x0010
        L_0x0062:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0060
        L_0x0067:
            if (r3 == 0) goto L_0x0048
            r3.close()     // Catch:{ IOException -> 0x006d }
            goto L_0x0048
        L_0x006d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0048
        L_0x0072:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0048
        L_0x0077:
            r0 = move-exception
            r3 = r4
        L_0x0079:
            r0.printStackTrace()     // Catch:{ all -> 0x0094 }
            if (r3 == 0) goto L_0x0048
            r3.close()     // Catch:{ IOException -> 0x0082 }
            goto L_0x0048
        L_0x0082:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0048
        L_0x0087:
            r0 = move-exception
            r3 = r4
        L_0x0089:
            if (r3 == 0) goto L_0x008e
            r3.close()     // Catch:{ IOException -> 0x008f }
        L_0x008e:
            throw r0
        L_0x008f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x008e
        L_0x0094:
            r0 = move-exception
            goto L_0x0089
        L_0x0096:
            r0 = move-exception
            r3 = r1
            goto L_0x0089
        L_0x0099:
            r0 = move-exception
            goto L_0x0079
        L_0x009b:
            r0 = move-exception
            r1 = r4
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.az.a(android.content.Context, java.lang.String):int");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ad.a(java.lang.String, boolean):int
     arg types: [java.lang.String, int]
     candidates:
      ac.a(int, android.os.Bundle):void
      ac.a(java.lang.String, int):void
      ad.a(java.lang.String, boolean):int */
    public static int a(Context context, String str, boolean z) {
        ad adVar = new ad(context);
        adVar.a(context.getCacheDir().getAbsolutePath());
        adVar.b("qqsecure.info");
        adVar.a((ac.a) null);
        do {
        } while (adVar.a(str, false) == -7);
        String str2 = context.getCacheDir().getAbsolutePath() + "/qqsecure.info";
        int a2 = a(context, str2);
        if (a2 != 0) {
            return a2;
        }
        int b = b(context, str2);
        if (b != 0) {
            return b;
        }
        if (z) {
            new Thread(new ba()).start();
        }
        return 0;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:62:0x00d1 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:47:0x00b2 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:11:0x0053 */
    /* JADX WARN: Type inference failed for: r3v0 */
    /* JADX WARN: Type inference failed for: r3v1 */
    /* JADX WARN: Type inference failed for: r3v2, types: [java.io.FileOutputStream] */
    /* JADX WARN: Type inference failed for: r3v3, types: [java.io.FileOutputStream] */
    /* JADX WARN: Type inference failed for: r3v4, types: [java.io.FileOutputStream] */
    /* JADX WARN: Type inference failed for: r3v5, types: [java.io.FileInputStream] */
    /* JADX WARN: Type inference failed for: r3v6 */
    /* JADX WARN: Type inference failed for: r3v7 */
    /* JADX WARN: Type inference failed for: r3v8 */
    /* JADX WARN: Type inference failed for: r3v9 */
    /* JADX WARN: Type inference failed for: r3v11, types: [java.io.FileOutputStream] */
    /* JADX WARN: Type inference failed for: r3v12 */
    /* JADX WARN: Type inference failed for: r3v13 */
    /* JADX WARN: Type inference failed for: r3v14 */
    /* JADX WARN: Type inference failed for: r3v15 */
    /* JADX WARN: Type inference failed for: r3v16 */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006f A[SYNTHETIC, Splitter:B:21:0x006f] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0074 A[SYNTHETIC, Splitter:B:24:0x0074] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00b7 A[SYNTHETIC, Splitter:B:50:0x00b7] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00bc A[SYNTHETIC, Splitter:B:53:0x00bc] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00d6 A[SYNTHETIC, Splitter:B:65:0x00d6] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00db A[SYNTHETIC, Splitter:B:68:0x00db] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x00f2 A[SYNTHETIC, Splitter:B:78:0x00f2] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x00f7 A[SYNTHETIC, Splitter:B:81:0x00f7] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x010e A[ExcHandler: Exception (e java.lang.Exception), PHI: r3 
      PHI: (r3v9 ?) = (r3v0 ?), (r3v0 ?), (r3v0 ?), (r3v11 ?) binds: [B:5:0x0016, B:27:0x007c, B:28:?, B:11:0x0053] A[DONT_GENERATE, DONT_INLINE], Splitter:B:5:0x0016] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x0110 A[ExcHandler: IOException (e java.io.IOException), PHI: r3 
      PHI: (r3v8 ?) = (r3v0 ?), (r3v0 ?), (r3v0 ?), (r3v11 ?) binds: [B:5:0x0016, B:27:0x007c, B:28:?, B:11:0x0053] A[DONT_GENERATE, DONT_INLINE], Splitter:B:5:0x0016] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:62:0x00d1=Splitter:B:62:0x00d1, B:47:0x00b2=Splitter:B:47:0x00b2} */
    private static int b(android.content.Context r9, java.lang.String r10) {
        /*
            r3 = 0
            r0 = 0
            r2 = -1
            r1 = -2
            java.io.File r6 = new java.io.File
            r6.<init>(r10)
            boolean r4 = r6.exists()
            if (r4 != 0) goto L_0x0011
            r0 = r2
        L_0x0010:
            return r0
        L_0x0011:
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0112, IOException -> 0x00b0, Exception -> 0x00cf, all -> 0x00ee }
            r4.<init>(r6)     // Catch:{ FileNotFoundException -> 0x0112, IOException -> 0x00b0, Exception -> 0x00cf, all -> 0x00ee }
            java.io.File r7 = new java.io.File     // Catch:{ FileNotFoundException -> 0x0116, IOException -> 0x0110, Exception -> 0x010e }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0116, IOException -> 0x0110, Exception -> 0x010e }
            r5.<init>()     // Catch:{ FileNotFoundException -> 0x0116, IOException -> 0x0110, Exception -> 0x010e }
            java.io.File r8 = r9.getFilesDir()     // Catch:{ FileNotFoundException -> 0x0116, IOException -> 0x0110, Exception -> 0x010e }
            java.lang.String r8 = r8.getAbsolutePath()     // Catch:{ FileNotFoundException -> 0x0116, IOException -> 0x0110, Exception -> 0x010e }
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ FileNotFoundException -> 0x0116, IOException -> 0x0110, Exception -> 0x010e }
            java.lang.String r8 = "/lib"
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ FileNotFoundException -> 0x0116, IOException -> 0x0110, Exception -> 0x010e }
            java.lang.String r8 = "sm_mq"
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ FileNotFoundException -> 0x0116, IOException -> 0x0110, Exception -> 0x010e }
            java.lang.String r8 = ".so"
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ FileNotFoundException -> 0x0116, IOException -> 0x0110, Exception -> 0x010e }
            java.lang.String r5 = r5.toString()     // Catch:{ FileNotFoundException -> 0x0116, IOException -> 0x0110, Exception -> 0x010e }
            r7.<init>(r5)     // Catch:{ FileNotFoundException -> 0x0116, IOException -> 0x0110, Exception -> 0x010e }
            boolean r5 = r7.exists()     // Catch:{ FileNotFoundException -> 0x0116, IOException -> 0x0110, Exception -> 0x010e }
            if (r5 == 0) goto L_0x007c
            r7.delete()     // Catch:{ FileNotFoundException -> 0x0116, IOException -> 0x0110, Exception -> 0x010e }
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0116, IOException -> 0x0110, Exception -> 0x010e }
            r5.<init>(r7)     // Catch:{ FileNotFoundException -> 0x0116, IOException -> 0x0110, Exception -> 0x010e }
            r3 = r5
        L_0x0051:
            r5 = 21
            byte[] r5 = new byte[r5]     // Catch:{ FileNotFoundException -> 0x0067, IOException -> 0x0110, Exception -> 0x010e }
            r4.read(r5)     // Catch:{ FileNotFoundException -> 0x0067, IOException -> 0x0110, Exception -> 0x010e }
            r5 = 1024(0x400, float:1.435E-42)
            byte[] r5 = new byte[r5]     // Catch:{ FileNotFoundException -> 0x0067, IOException -> 0x0110, Exception -> 0x010e }
        L_0x005c:
            int r7 = r4.read(r5)     // Catch:{ FileNotFoundException -> 0x0067, IOException -> 0x0110, Exception -> 0x010e }
            if (r7 == r2) goto L_0x008d
            r8 = 0
            r3.write(r5, r8, r7)     // Catch:{ FileNotFoundException -> 0x0067, IOException -> 0x0110, Exception -> 0x010e }
            goto L_0x005c
        L_0x0067:
            r0 = move-exception
            r2 = r3
            r3 = r4
        L_0x006a:
            r0.printStackTrace()     // Catch:{ all -> 0x010a }
            if (r3 == 0) goto L_0x0072
            r3.close()     // Catch:{ IOException -> 0x00a6 }
        L_0x0072:
            if (r2 == 0) goto L_0x0077
            r2.close()     // Catch:{ IOException -> 0x00ab }
        L_0x0077:
            r6.delete()
            r0 = r1
            goto L_0x0010
        L_0x007c:
            java.io.File r5 = r7.getParentFile()     // Catch:{ FileNotFoundException -> 0x0116, IOException -> 0x0110, Exception -> 0x010e }
            r5.mkdirs()     // Catch:{ FileNotFoundException -> 0x0116, IOException -> 0x0110, Exception -> 0x010e }
            r7.createNewFile()     // Catch:{ FileNotFoundException -> 0x0116, IOException -> 0x0110, Exception -> 0x010e }
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0116, IOException -> 0x0110, Exception -> 0x010e }
            r5.<init>(r7)     // Catch:{ FileNotFoundException -> 0x0116, IOException -> 0x0110, Exception -> 0x010e }
            r3 = r5
            goto L_0x0051
        L_0x008d:
            if (r4 == 0) goto L_0x0092
            r4.close()     // Catch:{ IOException -> 0x009c }
        L_0x0092:
            if (r3 == 0) goto L_0x0097
            r3.close()     // Catch:{ IOException -> 0x00a1 }
        L_0x0097:
            r6.delete()
            goto L_0x0010
        L_0x009c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0092
        L_0x00a1:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0097
        L_0x00a6:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0072
        L_0x00ab:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0077
        L_0x00b0:
            r0 = move-exception
            r4 = r3
        L_0x00b2:
            r0.printStackTrace()     // Catch:{ all -> 0x0108 }
            if (r4 == 0) goto L_0x00ba
            r4.close()     // Catch:{ IOException -> 0x00c5 }
        L_0x00ba:
            if (r3 == 0) goto L_0x00bf
            r3.close()     // Catch:{ IOException -> 0x00ca }
        L_0x00bf:
            r6.delete()
            r0 = r1
            goto L_0x0010
        L_0x00c5:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00ba
        L_0x00ca:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00bf
        L_0x00cf:
            r0 = move-exception
            r4 = r3
        L_0x00d1:
            r0.printStackTrace()     // Catch:{ all -> 0x0108 }
            if (r4 == 0) goto L_0x00d9
            r4.close()     // Catch:{ IOException -> 0x00e4 }
        L_0x00d9:
            if (r3 == 0) goto L_0x00de
            r3.close()     // Catch:{ IOException -> 0x00e9 }
        L_0x00de:
            r6.delete()
            r0 = r1
            goto L_0x0010
        L_0x00e4:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00d9
        L_0x00e9:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00de
        L_0x00ee:
            r0 = move-exception
            r4 = r3
        L_0x00f0:
            if (r4 == 0) goto L_0x00f5
            r4.close()     // Catch:{ IOException -> 0x00fe }
        L_0x00f5:
            if (r3 == 0) goto L_0x00fa
            r3.close()     // Catch:{ IOException -> 0x0103 }
        L_0x00fa:
            r6.delete()
            throw r0
        L_0x00fe:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00f5
        L_0x0103:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00fa
        L_0x0108:
            r0 = move-exception
            goto L_0x00f0
        L_0x010a:
            r0 = move-exception
            r4 = r3
            r3 = r2
            goto L_0x00f0
        L_0x010e:
            r0 = move-exception
            goto L_0x00d1
        L_0x0110:
            r0 = move-exception
            goto L_0x00b2
        L_0x0112:
            r0 = move-exception
            r2 = r3
            goto L_0x006a
        L_0x0116:
            r0 = move-exception
            r2 = r3
            r3 = r4
            goto L_0x006a
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.az.b(android.content.Context, java.lang.String):int");
    }
}
