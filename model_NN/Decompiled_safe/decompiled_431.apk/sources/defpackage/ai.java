package defpackage;

import android.content.DialogInterface;

/* renamed from: ai  reason: default package */
class ai implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ah f12a;

    ai(ah ahVar) {
        this.f12a = ahVar;
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.f12a.f11a.finish();
    }
}
