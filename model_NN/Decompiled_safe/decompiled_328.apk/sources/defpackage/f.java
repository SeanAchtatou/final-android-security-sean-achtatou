package defpackage;

import android.webkit.WebView;

/* renamed from: f  reason: default package */
final class f implements Runnable {
    final /* synthetic */ c a;
    private final String b;
    private final String c;
    private final WebView d;

    public f(c cVar, WebView webView, String str, String str2) {
        this.a = cVar;
        this.d = webView;
        this.b = str;
        this.c = str2;
    }

    public final void run() {
        this.d.loadDataWithBaseURL(this.b, this.c, "text/html", "utf-8", null);
    }
}
