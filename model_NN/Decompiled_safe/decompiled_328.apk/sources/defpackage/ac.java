package defpackage;

import com.google.ads.util.b;
import java.lang.ref.WeakReference;

/* renamed from: ac  reason: default package */
public final class ac implements Runnable {
    private WeakReference a;

    public ac(i iVar) {
        this.a = new WeakReference(iVar);
    }

    public final void run() {
        i iVar = (i) this.a.get();
        if (iVar == null) {
            b.a("The ad must be gone, so cancelling the refresh timer.");
        } else {
            iVar.v();
        }
    }
}
