package defpackage;

import android.net.Uri;
import android.webkit.WebView;
import com.google.ads.util.b;
import java.util.HashMap;
import java.util.Locale;

/* renamed from: p  reason: default package */
public final class p implements n {
    public final void a(i iVar, HashMap hashMap, WebView webView) {
        Uri parse;
        String host;
        String str;
        String str2 = (String) hashMap.get("u");
        if (str2 == null) {
            b.e("Could not get URL from click gmsg.");
            return;
        }
        k l = iVar.l();
        if (!(l == null || (host = (parse = Uri.parse(str2)).getHost()) == null || !host.toLowerCase(Locale.US).endsWith(".admob.com"))) {
            String path = parse.getPath();
            if (path != null) {
                String[] split = path.split("/");
                if (split.length >= 4) {
                    str = split[2] + "/" + split[3];
                    l.b(str);
                }
            }
            str = null;
            l.b(str);
        }
        new Thread(new ab(str2, webView.getContext().getApplicationContext())).start();
    }
}
