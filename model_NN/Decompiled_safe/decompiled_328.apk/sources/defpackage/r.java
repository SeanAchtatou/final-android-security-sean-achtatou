package defpackage;

import android.webkit.WebView;
import com.google.ads.AdActivity;
import com.google.ads.util.b;
import java.util.HashMap;

/* renamed from: r  reason: default package */
public final class r implements n {
    public final void a(i iVar, HashMap hashMap, WebView webView) {
        String str = (String) hashMap.get("js");
        if (str == null) {
            b.b("Could not get the JS to evaluate.");
        }
        if (webView instanceof l) {
            AdActivity b = ((l) webView).b();
            if (b == null) {
                b.b("Could not get the AdActivity from the AdWebView.");
                return;
            }
            l b2 = b.b();
            if (b2 == null) {
                b.b("Could not get the opening WebView.");
            } else {
                a.a(b2, str);
            }
        } else {
            b.b("Trying to evaluate JS in a WebView that isn't an AdWebView");
        }
    }
}
