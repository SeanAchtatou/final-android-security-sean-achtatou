package defpackage;

import android.webkit.WebView;
import com.google.ads.d;
import com.google.ads.util.b;
import java.util.HashMap;

/* renamed from: t  reason: default package */
public final class t implements n {
    public final void a(i iVar, HashMap hashMap, WebView webView) {
        b.e("Invalid " + ((String) hashMap.get("type")) + " request error: " + ((String) hashMap.get("errors")));
        c g = iVar.g();
        if (g != null) {
            g.a(d.INVALID_REQUEST);
        }
    }
}
