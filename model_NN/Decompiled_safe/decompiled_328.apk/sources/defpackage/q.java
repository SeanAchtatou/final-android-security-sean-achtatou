package defpackage;

import android.webkit.WebView;
import com.google.ads.util.b;
import java.util.HashMap;

/* renamed from: q  reason: default package */
public final class q implements n {
    public final void a(i iVar, HashMap hashMap, WebView webView) {
        if (webView instanceof l) {
            ((l) webView).a();
        } else {
            b.b("Trying to close WebView that isn't an AdWebView");
        }
    }
}
