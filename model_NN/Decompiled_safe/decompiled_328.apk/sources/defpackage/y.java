package defpackage;

import android.webkit.WebView;
import com.google.ads.util.b;
import java.util.HashMap;

/* renamed from: y  reason: default package */
public final class y implements n {
    public final void a(i iVar, HashMap hashMap, WebView webView) {
        String str = (String) hashMap.get("applicationTimeout");
        if (str != null) {
            try {
                iVar.a((long) (Float.parseFloat(str) * 1000.0f));
            } catch (NumberFormatException e) {
                b.b("Trying to set applicationTimeout to invalid value: " + str, e);
            }
        }
    }
}
