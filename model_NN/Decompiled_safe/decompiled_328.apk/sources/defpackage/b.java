package defpackage;

import android.app.Activity;
import com.google.ads.d;
import com.google.ads.util.AdUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.StringTokenizer;

/* renamed from: b  reason: default package */
public final class b implements Runnable {
    private c a;
    private i b;
    private volatile boolean c;
    private String d;

    b(c cVar, i iVar) {
        this.a = cVar;
        this.b = iVar;
    }

    private void a(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Tracking-Urls");
        if (headerField != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(headerField);
            while (stringTokenizer.hasMoreTokens()) {
                this.b.a(stringTokenizer.nextToken());
            }
        }
        b(httpURLConnection);
        String headerField2 = httpURLConnection.getHeaderField("X-Afma-Refresh-Rate");
        if (headerField2 != null) {
            try {
                this.b.a(Float.parseFloat(headerField2));
                if (!this.b.o()) {
                    this.b.d();
                }
            } catch (NumberFormatException e) {
                com.google.ads.util.b.b("Could not get refresh value: " + headerField2, e);
            }
        }
        String headerField3 = httpURLConnection.getHeaderField("X-Afma-Interstitial-Timeout");
        if (headerField3 != null) {
            try {
                this.b.a((long) (Float.parseFloat(headerField3) * 1000.0f));
            } catch (NumberFormatException e2) {
                com.google.ads.util.b.b("Could not get timeout value: " + headerField3, e2);
            }
        }
        String headerField4 = httpURLConnection.getHeaderField("X-Afma-Orientation");
        if (headerField4 == null) {
            return;
        }
        if (headerField4.equals("portrait")) {
            this.a.a(AdUtil.b());
        } else if (headerField4.equals("landscape")) {
            this.a.a(AdUtil.a());
        }
    }

    private void b(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Click-Tracking-Urls");
        if (headerField != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(headerField);
            while (stringTokenizer.hasMoreTokens()) {
                this.a.a(stringTokenizer.nextToken());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.c = true;
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.d = str;
        this.c = false;
        new Thread(this).start();
    }

    public final void run() {
        HttpURLConnection httpURLConnection;
        String readLine;
        while (!this.c) {
            try {
                httpURLConnection = (HttpURLConnection) new URL(this.d).openConnection();
                Activity e = this.b.e();
                if (e == null) {
                    com.google.ads.util.b.c("activity was null in AdHtmlLoader.");
                    this.a.a(d.INTERNAL_ERROR);
                    httpURLConnection.disconnect();
                    return;
                }
                AdUtil.a(httpURLConnection, e.getApplicationContext());
                httpURLConnection.setInstanceFollowRedirects(false);
                httpURLConnection.connect();
                int responseCode = httpURLConnection.getResponseCode();
                if (300 <= responseCode && responseCode < 400) {
                    String headerField = httpURLConnection.getHeaderField("Location");
                    if (headerField == null) {
                        com.google.ads.util.b.c("Could not get redirect location from a " + responseCode + " redirect.");
                        this.a.a(d.INTERNAL_ERROR);
                        httpURLConnection.disconnect();
                        return;
                    }
                    a(httpURLConnection);
                    this.d = headerField;
                    httpURLConnection.disconnect();
                } else if (responseCode == 200) {
                    a(httpURLConnection);
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()), 4096);
                    StringBuilder sb = new StringBuilder();
                    while (!this.c && (readLine = bufferedReader.readLine()) != null) {
                        sb.append(readLine);
                        sb.append("\n");
                    }
                    String sb2 = sb.toString();
                    com.google.ads.util.b.a("Response content is: " + sb2);
                    if (sb2 == null || sb2.trim().length() <= 0) {
                        com.google.ads.util.b.a("Response message is null or zero length: " + sb2);
                        this.a.a(d.NO_FILL);
                        httpURLConnection.disconnect();
                        return;
                    }
                    this.a.a(sb2, this.d);
                    httpURLConnection.disconnect();
                    return;
                } else if (responseCode == 400) {
                    com.google.ads.util.b.c("Bad request");
                    this.a.a(d.INVALID_REQUEST);
                    httpURLConnection.disconnect();
                    return;
                } else {
                    com.google.ads.util.b.c("Invalid response code: " + responseCode);
                    this.a.a(d.INTERNAL_ERROR);
                    httpURLConnection.disconnect();
                    return;
                }
            } catch (MalformedURLException e2) {
                com.google.ads.util.b.a("Received malformed ad url from javascript.", e2);
                this.a.a(d.INTERNAL_ERROR);
                return;
            } catch (IOException e3) {
                com.google.ads.util.b.b("IOException connecting to ad url.", e3);
                this.a.a(d.NETWORK_ERROR);
                return;
            } catch (Exception e4) {
                com.google.ads.util.b.a("An unknown error occurred in AdHtmlLoader.", e4);
                this.a.a(d.INTERNAL_ERROR);
                return;
            } catch (Throwable th) {
                httpURLConnection.disconnect();
                throw th;
            }
        }
    }
}
