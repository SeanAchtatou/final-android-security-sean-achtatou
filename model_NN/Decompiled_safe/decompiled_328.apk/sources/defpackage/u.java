package defpackage;

import android.webkit.WebView;
import com.google.ads.util.b;
import java.util.HashMap;

/* renamed from: u  reason: default package */
public final class u implements n {
    public final void a(i iVar, HashMap hashMap, WebView webView) {
        String str = (String) hashMap.get("url");
        b.c("Received ad url: <\"url\": \"" + str + "\", \"afmaNotifyDt\": \"" + ((String) hashMap.get("afma_notify_dt")) + "\">");
        c g = iVar.g();
        if (g != null) {
            g.b(str);
        }
    }
}
