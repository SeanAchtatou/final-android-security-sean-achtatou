package defpackage;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Handler;
import com.google.ads.AdActivity;
import com.google.ads.AdView;
import com.google.ads.a;
import com.google.ads.b;
import com.google.ads.c;
import com.google.ads.d;
import com.google.ads.f;
import com.google.ads.g;
import com.google.ads.util.AdUtil;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: i  reason: default package */
public final class i {
    private static final Object a = new Object();
    private WeakReference b;
    private a c;
    private b d = null;
    private c e = null;
    private c f = null;
    private f g;
    private k h = new k();
    private String i;
    private l j;
    private m k;
    private Handler l = new Handler();
    private long m;
    private boolean n = false;
    private boolean o = false;
    private SharedPreferences p;
    private long q = 0;
    private ac r;
    private boolean s = false;
    private LinkedList t;
    private LinkedList u;
    private int v;

    public i(Activity activity, a aVar, f fVar, String str) {
        this.b = new WeakReference(activity);
        this.c = aVar;
        this.g = fVar;
        this.i = str;
        synchronized (a) {
            this.p = activity.getApplicationContext().getSharedPreferences("GoogleAdMobAdsPrefs", 0);
            this.m = 60000;
        }
        this.r = new ac(this);
        this.t = new LinkedList();
        this.u = new LinkedList();
        a();
        AdUtil.g(activity.getApplicationContext());
    }

    private synchronized boolean w() {
        return this.e != null;
    }

    private synchronized void x() {
        Activity activity = (Activity) this.b.get();
        if (activity == null) {
            com.google.ads.util.b.e("activity was null while trying to ping tracking URLs.");
        } else {
            Iterator it = this.t.iterator();
            while (it.hasNext()) {
                new Thread(new ab((String) it.next(), activity.getApplicationContext())).start();
            }
        }
    }

    private synchronized void y() {
        Activity activity = (Activity) this.b.get();
        if (activity == null) {
            com.google.ads.util.b.e("activity was null while trying to ping click tracking URLs.");
        } else {
            Iterator it = this.u.iterator();
            while (it.hasNext()) {
                new Thread(new ab((String) it.next(), activity.getApplicationContext())).start();
            }
        }
    }

    private synchronized void z() {
        if (this.e != null) {
            this.e.a();
            this.e = null;
        }
        this.j.stopLoading();
    }

    public final synchronized void a() {
        Activity e2 = e();
        if (e2 == null) {
            com.google.ads.util.b.a("activity was null while trying to create an AdWebView.");
        } else {
            this.j = new l(e2.getApplicationContext(), this.g);
            this.j.setVisibility(8);
            if (this.c instanceof AdView) {
                this.k = new m(this, a.b, true, false);
            } else {
                this.k = new m(this, a.b, true, true);
            }
            this.j.setWebViewClient(this.k);
        }
    }

    public final synchronized void a(float f2) {
        this.q = (long) (1000.0f * f2);
    }

    public final synchronized void a(int i2) {
        this.v = i2;
    }

    public final void a(long j2) {
        synchronized (a) {
            SharedPreferences.Editor edit = this.p.edit();
            edit.putLong("Timeout" + this.i, j2);
            edit.commit();
            if (this.s) {
                this.m = j2;
            }
        }
    }

    public final synchronized void a(b bVar) {
        this.d = bVar;
    }

    public final synchronized void a(c cVar) {
        if (w()) {
            com.google.ads.util.b.e("loadAd called while the ad is already loading, so aborting.");
        } else if (AdActivity.c()) {
            com.google.ads.util.b.e("loadAd called while an interstitial or landing page is displayed, so aborting");
        } else {
            Activity e2 = e();
            if (e2 == null) {
                com.google.ads.util.b.e("activity is null while trying to load an ad.");
            } else if (AdUtil.c(e2.getApplicationContext()) && AdUtil.b(e2.getApplicationContext())) {
                this.n = false;
                this.t.clear();
                this.f = cVar;
                this.e = new c(this);
                this.e.a(cVar);
            }
        }
    }

    public final synchronized void a(d dVar) {
        this.e = null;
        if (this.c instanceof g) {
            if (dVar == d.NO_FILL) {
                this.h.n();
            } else if (dVar == d.NETWORK_ERROR) {
                this.h.l();
            }
        }
        com.google.ads.util.b.c("onFailedToReceiveAd(" + dVar + ")");
        if (this.d != null) {
            b bVar = this.d;
            a aVar = this.c;
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Runnable runnable) {
        this.l.post(runnable);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str) {
        com.google.ads.util.b.a("Adding a tracking URL: " + str);
        this.t.add(str);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(LinkedList linkedList) {
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            com.google.ads.util.b.a("Adding a click tracking URL: " + ((String) it.next()));
        }
        this.u = linkedList;
    }

    public final synchronized void b() {
        a((b) null);
        z();
        this.j.destroy();
    }

    public final synchronized void c() {
        if (this.o) {
            com.google.ads.util.b.a("Disabling refreshing.");
            this.l.removeCallbacks(this.r);
            this.o = false;
        } else {
            com.google.ads.util.b.a("Refreshing is already disabled.");
        }
    }

    public final synchronized void d() {
        if (!(this.c instanceof AdView)) {
            com.google.ads.util.b.a("Tried to enable refreshing on something other than an AdView.");
        } else if (!this.o) {
            com.google.ads.util.b.a("Enabling refreshing every " + this.q + " milliseconds.");
            this.l.postDelayed(this.r, this.q);
            this.o = true;
        } else {
            com.google.ads.util.b.a("Refreshing is already enabled.");
        }
    }

    public final Activity e() {
        return (Activity) this.b.get();
    }

    public final a f() {
        return this.c;
    }

    public final synchronized c g() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final String h() {
        return this.i;
    }

    public final synchronized l i() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public final synchronized m j() {
        return this.k;
    }

    public final f k() {
        return this.g;
    }

    public final k l() {
        return this.h;
    }

    public final synchronized int m() {
        return this.v;
    }

    public final long n() {
        return this.m;
    }

    public final synchronized boolean o() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void p() {
        this.e = null;
        this.n = true;
        this.j.setVisibility(0);
        this.h.c();
        if (this.c instanceof AdView) {
            x();
        }
        com.google.ads.util.b.c("onReceiveAd()");
        if (this.d != null) {
            b bVar = this.d;
            a aVar = this.c;
            bVar.c();
        }
    }

    public final synchronized void q() {
        this.h.o();
        com.google.ads.util.b.c("onDismissScreen()");
        if (this.d != null) {
            b bVar = this.d;
            a aVar = this.c;
        }
    }

    public final synchronized void r() {
        com.google.ads.util.b.c("onPresentScreen()");
        if (this.d != null) {
            b bVar = this.d;
            a aVar = this.c;
        }
    }

    public final synchronized void s() {
        com.google.ads.util.b.c("onLeaveApplication()");
        if (this.d != null) {
            b bVar = this.d;
            a aVar = this.c;
        }
    }

    public final void t() {
        this.h.b();
        y();
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean u() {
        return !this.u.isEmpty();
    }

    public final synchronized void v() {
        if (this.f == null) {
            com.google.ads.util.b.a("Tried to refresh before calling loadAd().");
        } else if (this.c instanceof AdView) {
            if (!((AdView) this.c).isShown() || !AdUtil.d()) {
                com.google.ads.util.b.a("Not refreshing because the ad is not visible.");
            } else {
                com.google.ads.util.b.c("Refreshing ad.");
                a(this.f);
            }
            this.l.postDelayed(this.r, this.q);
        } else {
            com.google.ads.util.b.a("Tried to refresh an ad that wasn't an AdView.");
        }
    }
}
