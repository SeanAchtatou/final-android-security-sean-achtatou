package defpackage;

import android.webkit.WebView;

/* renamed from: d  reason: default package */
final class d implements Runnable {
    final /* synthetic */ c a;
    private final i b;
    private final WebView c;
    private final b d;
    private final com.google.ads.d e;
    private final boolean f;

    public d(c cVar, i iVar, WebView webView, b bVar, com.google.ads.d dVar, boolean z) {
        this.a = cVar;
        this.b = iVar;
        this.c = webView;
        this.d = bVar;
        this.e = dVar;
        this.f = z;
    }

    public final void run() {
        this.c.stopLoading();
        this.c.destroy();
        this.d.a();
        if (this.f) {
            l i = this.b.i();
            i.stopLoading();
            i.setVisibility(8);
        }
        this.b.a(this.e);
    }
}
