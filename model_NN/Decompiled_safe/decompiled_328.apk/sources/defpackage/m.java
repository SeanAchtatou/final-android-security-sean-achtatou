package defpackage;

import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.ads.AdActivity;
import com.google.ads.util.AdUtil;
import com.google.ads.util.b;
import java.util.HashMap;
import java.util.Map;

/* renamed from: m  reason: default package */
public final class m extends WebViewClient {
    private i a;
    private Map b;
    private boolean c;
    private boolean d;
    private boolean e = false;
    private boolean f = false;

    public m(i iVar, Map map, boolean z, boolean z2) {
        this.a = iVar;
        this.b = map;
        this.c = z;
        this.d = z2;
    }

    public final void a() {
        this.e = true;
    }

    public final void b() {
        this.f = true;
    }

    public final void onPageFinished(WebView webView, String str) {
        if (this.e) {
            c g = this.a.g();
            if (g != null) {
                g.b();
            } else {
                b.a("adLoader was null while trying to setFinishedLoadingHtml().");
            }
            this.e = false;
        }
        if (this.f) {
            a.a(webView);
            this.f = false;
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        b.a("shouldOverrideUrlLoading(\"" + str + "\")");
        Uri parse = Uri.parse(str);
        HashMap b2 = AdUtil.b(parse);
        if (b2 == null) {
            b.e("An error occurred while parsing the url parameters.");
            return true;
        }
        String str2 = (String) b2.get("ai");
        if (str2 != null) {
            this.a.l().a(str2);
        }
        if (a.a(parse)) {
            a.a(this.a, this.b, parse, webView);
            return true;
        } else if (this.d) {
            if (AdUtil.a(parse)) {
                return super.shouldOverrideUrlLoading(webView, str);
            }
            HashMap hashMap = new HashMap();
            hashMap.put("u", str);
            AdActivity.a(this.a, new j("intent", hashMap));
            return true;
        } else if (this.c) {
            String str3 = (!this.a.u() || !AdUtil.a(parse)) ? "intent" : "webapp";
            HashMap hashMap2 = new HashMap();
            hashMap2.put("u", parse.toString());
            AdActivity.a(this.a, new j(str3, hashMap2));
            return true;
        } else {
            b.e("URL is not a GMSG and can't handle URL: " + str);
            return true;
        }
    }
}
