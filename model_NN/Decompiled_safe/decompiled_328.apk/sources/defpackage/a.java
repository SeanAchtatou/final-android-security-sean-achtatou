package defpackage;

import android.net.Uri;
import android.webkit.WebView;
import com.google.ads.util.AdUtil;
import com.google.ads.util.b;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

/* renamed from: a  reason: default package */
public final class a {
    public static final Map a;
    public static final Map b;

    static {
        HashMap hashMap = new HashMap();
        hashMap.put("/invalidRequest", new t());
        hashMap.put("/loadAdURL", new u());
        a = Collections.unmodifiableMap(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("/open", new w());
        hashMap2.put("/canOpenURLs", new o());
        hashMap2.put("/close", new q());
        hashMap2.put("/evalInOpener", new r());
        hashMap2.put("/log", new v());
        hashMap2.put("/click", new p());
        hashMap2.put("/httpTrack", new s());
        hashMap2.put("/reloadRequest", new x());
        hashMap2.put("/settings", new y());
        hashMap2.put("/touch", new z());
        hashMap2.put("/video", new aa());
        b = Collections.unmodifiableMap(hashMap2);
    }

    private a() {
    }

    public static void a(WebView webView) {
        b.d("Calling onshow.");
        a(webView, "onshow", "{'version': 'afma-sdk-a-v4.1.1'}");
    }

    public static void a(WebView webView, String str) {
        webView.loadUrl("javascript:" + str);
    }

    public static void a(WebView webView, String str, String str2) {
        if (str2 != null) {
            a(webView, "AFMA_ReceiveMessage" + "('" + str + "', " + str2 + ");");
        } else {
            a(webView, "AFMA_ReceiveMessage" + "('" + str + "');");
        }
    }

    public static void a(WebView webView, Map map) {
        a(webView, "openableURLs", new JSONObject(map).toString());
    }

    static void a(i iVar, Map map, Uri uri, WebView webView) {
        String str;
        HashMap b2 = AdUtil.b(uri);
        if (b2 == null) {
            b.e("An error occurred while parsing the message parameters.");
            return;
        }
        if (c(uri)) {
            String host = uri.getHost();
            if (host == null) {
                b.e("An error occurred while parsing the AMSG parameters.");
                str = null;
            } else if (host.equals("launch")) {
                b2.put("a", "intent");
                b2.put("u", b2.get("url"));
                b2.remove("url");
                str = "/open";
            } else if (host.equals("closecanvas")) {
                str = "/close";
            } else if (host.equals("log")) {
                str = "/log";
            } else {
                b.e("An error occurred while parsing the AMSG: " + uri.toString());
                str = null;
            }
        } else if (b(uri)) {
            str = uri.getPath();
        } else {
            b.e("Message was neither a GMSG nor an AMSG.");
            str = null;
        }
        if (str == null) {
            b.e("An error occurred while parsing the message.");
            return;
        }
        n nVar = (n) map.get(str);
        if (nVar == null) {
            b.e("No AdResponse found, <message: " + str + ">");
        } else {
            nVar.a(iVar, b2, webView);
        }
    }

    public static boolean a(Uri uri) {
        if (uri == null || !uri.isHierarchical()) {
            return false;
        }
        return b(uri) || c(uri);
    }

    public static void b(WebView webView) {
        b.d("Calling onhide.");
        a(webView, "onhide", null);
    }

    private static boolean b(Uri uri) {
        String scheme = uri.getScheme();
        if (scheme == null || !scheme.equals("gmsg")) {
            return false;
        }
        String authority = uri.getAuthority();
        return authority != null && authority.equals("mobileads.google.com");
    }

    private static boolean c(Uri uri) {
        String scheme = uri.getScheme();
        return scheme != null && scheme.equals("admob");
    }
}
