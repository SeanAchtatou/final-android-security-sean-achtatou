package defpackage;

import android.webkit.WebView;
import com.google.ads.AdActivity;
import com.google.ads.util.b;
import java.util.HashMap;

/* renamed from: w  reason: default package */
public final class w implements n {
    public final void a(i iVar, HashMap hashMap, WebView webView) {
        String str = (String) hashMap.get("a");
        if (str == null) {
            b.a("Could not get the action parameter for open GMSG.");
        } else if (str.equals("webapp")) {
            AdActivity.a(iVar, new j("webapp", hashMap));
        } else {
            AdActivity.a(iVar, new j("intent", hashMap));
        }
    }
}
