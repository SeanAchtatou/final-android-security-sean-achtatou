package defpackage;

import java.util.LinkedList;

/* renamed from: h  reason: default package */
final class h implements Runnable {
    final /* synthetic */ c a;
    private final i b;
    private final LinkedList c;
    private final int d;

    public h(c cVar, i iVar, LinkedList linkedList, int i) {
        this.a = cVar;
        this.b = iVar;
        this.c = linkedList;
        this.d = i;
    }

    public final void run() {
        this.b.a(this.c);
        this.b.a(this.d);
        this.b.p();
    }
}
