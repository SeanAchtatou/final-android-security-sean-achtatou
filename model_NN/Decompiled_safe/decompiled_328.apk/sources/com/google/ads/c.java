package com.google.ads;

import android.content.Context;
import android.location.Location;
import com.google.ads.util.AdUtil;
import com.google.ads.util.b;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class c {
    public static final String a = AdUtil.a("emulator");
    private e b = null;
    private String c = null;
    private Set d = null;
    private Map e = null;
    private Location f = null;
    private boolean g = false;
    private boolean h = false;
    private Set i = null;

    public final Map a(Context context) {
        String a2;
        HashMap hashMap = new HashMap();
        if (this.d != null) {
            hashMap.put("kw", this.d);
        }
        if (this.b != null) {
            hashMap.put("cust_gender", this.b.toString());
        }
        if (this.c != null) {
            hashMap.put("cust_age", this.c);
        }
        if (this.f != null) {
            hashMap.put("uule", AdUtil.a(this.f));
        }
        if (this.g) {
            hashMap.put("testing", 1);
        }
        if ((this.i == null || (a2 = AdUtil.a(context)) == null || !this.i.contains(a2)) ? false : true) {
            hashMap.put("adtest", "on");
        } else if (!this.h) {
            b.c("To get test ads on this device, call adRequest.addTestDevice(" + (AdUtil.c() ? "AdRequest.TEST_EMULATOR" : "\"" + AdUtil.a(context) + "\"") + ");");
            this.h = true;
        }
        if (this.e != null) {
            hashMap.put("extras", this.e);
        }
        return hashMap;
    }

    public final void a(String str) {
        if (this.d == null) {
            this.d = new HashSet();
        }
        this.d.add(str);
    }
}
