package com.google.ads.util;

import java.io.UnsupportedEncodingException;

public class c {
    static final /* synthetic */ boolean a = (!c.class.desiredAssertionStatus());

    private c() {
    }

    public static String a(byte[] bArr) {
        try {
            int length = bArr.length;
            e eVar = new e();
            int i = (length / 3) * 4;
            if (!eVar.d) {
                switch (length % 3) {
                    case 1:
                        i += 2;
                        break;
                    case 2:
                        i += 3;
                        break;
                }
            } else if (length % 3 > 0) {
                i += 4;
            }
            if (eVar.e && length > 0) {
                i += (((length - 1) / 57) + 1) * (eVar.f ? 2 : 1);
            }
            eVar.a = new byte[i];
            eVar.a(bArr, length);
            if (a || eVar.b == i) {
                return new String(eVar.a, "US-ASCII");
            }
            throw new AssertionError();
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }
}
