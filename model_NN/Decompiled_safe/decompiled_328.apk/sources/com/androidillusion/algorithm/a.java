package com.androidillusion.algorithm;

import java.lang.reflect.Array;
import java.util.Random;

public final class a {
    static int[] a = new int[514];
    static float[][] b = ((float[][]) Array.newInstance(Float.TYPE, 514, 3));
    static float[][] c = ((float[][]) Array.newInstance(Float.TYPE, 514, 2));
    static float[] d = new float[514];
    static boolean e = true;
    private static Random f = new Random();

    private static float a(float f2) {
        return f2 * f2 * (3.0f - (2.0f * f2));
    }

    public static float a(float f2, float f3) {
        if (e) {
            e = false;
            for (int i = 0; i < 256; i++) {
                a[i] = i;
                d[i] = ((float) ((a() % 512) - 256)) / 256.0f;
                for (int i2 = 0; i2 < 2; i2++) {
                    c[i][i2] = ((float) ((a() % 512) - 256)) / 256.0f;
                }
                float[] fArr = c[i];
                float sqrt = (float) Math.sqrt((double) ((fArr[0] * fArr[0]) + (fArr[1] * fArr[1])));
                fArr[0] = fArr[0] / sqrt;
                fArr[1] = fArr[1] / sqrt;
                for (int i3 = 0; i3 < 3; i3++) {
                    b[i][i3] = ((float) ((a() % 512) - 256)) / 256.0f;
                }
                float[] fArr2 = b[i];
                float sqrt2 = (float) Math.sqrt((double) ((fArr2[0] * fArr2[0]) + (fArr2[1] * fArr2[1]) + (fArr2[2] * fArr2[2])));
                fArr2[0] = fArr2[0] / sqrt2;
                fArr2[1] = fArr2[1] / sqrt2;
                fArr2[2] = fArr2[2] / sqrt2;
            }
            for (int i4 = 255; i4 >= 0; i4--) {
                int i5 = a[i4];
                int[] iArr = a;
                int[] iArr2 = a;
                int a2 = a() % 256;
                iArr[i4] = iArr2[a2];
                a[a2] = i5;
            }
            for (int i6 = 0; i6 < 258; i6++) {
                a[i6 + 256] = a[i6];
                d[i6 + 256] = d[i6];
                for (int i7 = 0; i7 < 2; i7++) {
                    c[i6 + 256][i7] = c[i6][i7];
                }
                for (int i8 = 0; i8 < 3; i8++) {
                    b[i6 + 256][i8] = b[i6][i8];
                }
            }
        }
        float f4 = 4096.0f + f2;
        int i9 = ((int) f4) & 255;
        float f5 = f4 - ((float) ((int) f4));
        float f6 = f5 - 1.0f;
        float f7 = 4096.0f + f3;
        int i10 = ((int) f7) & 255;
        int i11 = (i10 + 1) & 255;
        float f8 = f7 - ((float) ((int) f7));
        float f9 = f8 - 1.0f;
        int i12 = a[i9];
        int i13 = a[(i9 + 1) & 255];
        int i14 = a[i12 + i10];
        int i15 = a[i10 + i13];
        int i16 = a[i12 + i11];
        int i17 = a[i13 + i11];
        float a3 = a(f5);
        float a4 = a(f8);
        float[] fArr3 = c[i14];
        float f10 = (fArr3[1] * f8) + (fArr3[0] * f5);
        float[] fArr4 = c[i15];
        float f11 = ((((f8 * fArr4[1]) + (fArr4[0] * f6)) - f10) * a3) + f10;
        float[] fArr5 = c[i16];
        float f12 = (f5 * fArr5[0]) + (fArr5[1] * f9);
        float[] fArr6 = c[i17];
        return ((((f12 + ((((fArr6[1] * f9) + (fArr6[0] * f6)) - f12) * a3)) - f11) * a4) + f11) * 1.5f;
    }

    private static int a() {
        return f.nextInt() & Integer.MAX_VALUE;
    }
}
