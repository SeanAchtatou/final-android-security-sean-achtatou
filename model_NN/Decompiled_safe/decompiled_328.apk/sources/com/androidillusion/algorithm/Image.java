package com.androidillusion.algorithm;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import java.lang.reflect.Array;
import java.util.Arrays;

public class Image {
    public static char[] a;
    public static Typeface b;
    public static boolean c;
    public static int[] d;
    public static int[] e;
    public static float f = 4.0f;
    public static float g = 4.0f;
    static float h = 32.0f;
    static float i = 1.0f;
    static float j = 0.0f;
    static float k = 12.0f;
    static float l = 1.0f;
    static float m = 0.0f;
    static float n = 1.0f;
    static float o = 0.0f;
    static float p = 0.0f;
    static float q = 1.0f;

    static {
        System.loadLibrary("android-illusion");
    }

    public static native void GetArray(String str, int[] iArr, byte[] bArr, int i2, int i3, int i4, int i5, int i6, int i7);

    public static native void JpegToRawFile(String str, String str2);

    public static native void JpegToRotatedAngleJpeg(String str, String str2, int i2, int i3, String str3);

    public static native void NDKEffectBoxblurRAW(String str, String str2, String str3, int i2, int i3, int i4);

    public static native void NDKEffectBoxblurRGB(int[] iArr, int[] iArr2, int i2, int i3, int i4);

    public static native void NDKEffectCustomYUV(int[] iArr, int[] iArr2, int[] iArr3, int i2, int i3);

    public static native void NDKEffectFirstFisheyeRGB(int[] iArr, int i2, int i3, int i4, int i5);

    public static native void NDKEffectFirstMosaicRGB(int[] iArr, int i2, int i3);

    public static native void NDKEffectFirstPinchRGB(int[] iArr, int i2, int i3, int i4, int i5);

    public static native void NDKEffectFirstStretchRGB(int[] iArr, int i2, int i3, int i4, int i5, int i6);

    public static native void NDKEffectFirstTunnelRGB(int[] iArr, int i2, int i3, int i4);

    public static native void NDKEffectFirstTwirlRGB(int[] iArr, int i2, int i3, int i4, int i5);

    public static native void NDKEffectGlowRAW(String str, String str2, String str3, int i2, int i3, int i4);

    public static native void NDKEffectGlowRGB(int[] iArr, int[] iArr2, int i2, int i3, int i4);

    public static native void NDKEffectHorizontalStretchRAW(String str, String str2, int i2, int i3, float f2, int i4, int i5);

    public static native void NDKEffectMirrorRAW(String str, String str2, int i2, int i3, boolean z, int i4);

    public static native void NDKEffectMirrorRGB(int[] iArr, int i2, int i3, int i4, int i5);

    public static native void NDKEffectMosaicRAW(String str, String str2, int i2, int i3);

    public static native void NDKEffectPixelationRAW(String str, String str2, int i2, int i3, int i4);

    public static native void NDKEffectPixelationRGB(int[] iArr, int i2, int i3);

    public static native void NDKEffectSharpRAW(String str, String str2, String str3, int i2, int i3, int i4);

    public static native void NDKEffectSharpRGB(int[] iArr, int[] iArr2, int i2, int i3, int i4);

    public static native void NDKEffectVerticalStretchRAW(String str, String str2, int i2, int i3, float f2, int i4, int i5);

    public static native void NDKFilterBWRAW(String str, String str2, int i2, int i3, int i4);

    public static native void NDKFilterBWYUV(int[] iArr, byte[] bArr, int i2, int i3, int i4);

    public static native void NDKFilterColorUVRAW(String str, String str2, int i2, int i3, int i4, int i5, boolean z);

    public static native void NDKFilterColorUVYUV(int[] iArr, byte[] bArr, int i2, int i3, int i4, int i5, boolean z);

    public static native void NDKFilterColorcropRAW(String str, String str2, int i2, int i3, int i4);

    public static native void NDKFilterColorcropYUV(int[] iArr, byte[] bArr, int i2, int i3, int i4);

    public static native void NDKFilterCustomRAW(String str, String str2, int i2, int i3, int i4, int i5, int i6);

    public static native void NDKFilterCustomYUV(int[] iArr, byte[] bArr, int i2, int i3, int i4, int i5, int i6);

    public static native void NDKFilterLomoRAW(String str, String str2, int i2, int i3);

    public static native void NDKFilterLomoYUV(int[] iArr, byte[] bArr, int i2, int i3);

    public static native void NDKFilterMonoRAW(String str, String str2, int i2, int i3);

    public static native void NDKFilterMonoYUV(int[] iArr, byte[] bArr, int i2, int i3);

    public static native void NDKFilterNegativeRAW(String str, String str2, int i2, int i3);

    public static native void NDKFilterNegativeYUV(int[] iArr, byte[] bArr, int i2, int i3);

    public static native void NDKFilterNoneYUV(int[] iArr, byte[] bArr, int i2, int i3);

    public static native void NDKFilterOilRAW(String str, String str2, int i2, int i3);

    public static native void NDKFilterOilYUV(int[] iArr, byte[] bArr, int i2, int i3);

    public static native void NDKFilterOldPhotoRAW(String str, String str2, int i2, int i3);

    public static native void NDKFilterOldPhotoYUV(int[] iArr, byte[] bArr, int i2, int i3);

    public static native void NDKFilterPencilRAW(String str, String str2, int i2, int i3, int i4);

    public static native void NDKFilterPencilYUV(int[] iArr, byte[] bArr, int i2, int i3, int i4);

    public static native void NDKFilterThermalRAW(String str, String str2, int i2, int i3);

    public static native void NDKFilterThermalYUV(int[] iArr, byte[] bArr, int i2, int i3);

    public static native void NDKFilterXrayRAW(String str, String str2, int i2, int i3);

    public static native void NDKFilterXrayYUV(int[] iArr, byte[] bArr, int i2, int i3);

    public static native void NDKFrontCameraMirrorRGB(int[] iArr, int i2, int i3);

    public static native void RawToJpegFile(String str, String str2, int i2, int i3, int i4);

    public static void a(int[] iArr, int i2, int i3) {
        if (d == null) {
            d = new int[(i2 * i3)];
        }
        if (c) {
            Arrays.fill(d, 0);
            NDKEffectFirstMosaicRGB(d, i2, i3);
            c = false;
        }
        if (e == null) {
            e = new int[(i2 * i3)];
        }
        System.arraycopy(iArr, 0, e, 0, iArr.length);
        NDKEffectCustomYUV(iArr, d, e, i2, i3);
    }

    public static void a(int[] iArr, int i2, int i3, int i4) {
        if (d == null) {
            d = new int[(i2 * i3)];
        }
        if (c) {
            Arrays.fill(d, 0);
            NDKEffectFirstTunnelRGB(d, i2, i3, i4);
            c = false;
        }
        if (e == null) {
            e = new int[(i2 * i3)];
        }
        System.arraycopy(iArr, 0, e, 0, iArr.length);
        NDKEffectCustomYUV(iArr, d, e, i2, i3);
    }

    public static void a(int[] iArr, int i2, int i3, int i4, int i5) {
        if (d == null) {
            d = new int[(i2 * i3)];
        }
        if (c) {
            Arrays.fill(d, 0);
            NDKEffectFirstFisheyeRGB(d, i2, i3, i4, i5);
            c = false;
        }
        if (e == null) {
            e = new int[(i2 * i3)];
        }
        System.arraycopy(iArr, 0, e, 0, iArr.length);
        NDKEffectCustomYUV(iArr, d, e, i2, i3);
    }

    public static void a(int[] iArr, int i2, int i3, int i4, int i5, int i6) {
        if (d == null) {
            d = new int[(i2 * i3)];
        }
        if (c) {
            Arrays.fill(d, 0);
            NDKEffectFirstStretchRGB(d, i2, i3, i4, i5, i6);
            c = false;
        }
        if (e == null) {
            e = new int[(i2 * i3)];
        }
        System.arraycopy(iArr, 0, e, 0, iArr.length);
        NDKEffectCustomYUV(iArr, d, e, i2, i3);
    }

    public static char[][] a(Context context, int[] iArr, int i2, int i3) {
        int i4 = i2 / 5;
        int i5 = i3 / 8;
        char[][] cArr = new char[i5][];
        for (int i6 = 0; i6 < cArr.length; i6++) {
            cArr[i6] = new char[i4];
        }
        if (a == null) {
            a = new char[256];
            for (int i7 = 0; i7 < 256; i7++) {
                int i8 = 255 - i7;
                if (i8 >= 230) {
                    a[i7] = ' ';
                } else if (i8 >= 200) {
                    a[i7] = '.';
                } else if (i8 >= 180) {
                    a[i7] = '*';
                } else if (i8 >= 160) {
                    a[i7] = ':';
                } else if (i8 >= 130) {
                    a[i7] = 'o';
                } else if (i8 >= 100) {
                    a[i7] = '&';
                } else if (i8 >= 70) {
                    a[i7] = '8';
                } else if (i8 >= 50) {
                    a[i7] = '#';
                } else {
                    a[i7] = '@';
                }
            }
        }
        for (int i9 = 0; i9 < i5; i9++) {
            for (int i10 = 0; i10 < i4; i10++) {
                cArr[i9][i10] = a[iArr[(i10 * 5) + 2 + (((i9 * 8) + 4) * i2)] & 255];
            }
        }
        if (b == null) {
            b = Typeface.createFromAsset(context.getAssets(), "fonts/VeraMono.ttf");
        }
        Paint paint = new Paint();
        paint.setTypeface(b);
        paint.setColor(-1);
        paint.setTextSize(8.0f);
        paint.setTextScaleX(1.0f);
        paint.setTextAlign(Paint.Align.LEFT);
        Bitmap createBitmap = Bitmap.createBitmap(i2, i3, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas();
        canvas.setBitmap(createBitmap);
        for (int i11 = 0; i11 < i5; i11++) {
            canvas.drawText(new String(cArr[i11]), 0.0f, (float) ((i11 * 8) + 8), paint);
        }
        createBitmap.getPixels(iArr, 0, createBitmap.getWidth(), 0, 0, createBitmap.getWidth(), createBitmap.getHeight());
        return cArr;
    }

    public static void b(int[] iArr, int i2, int i3) {
        int i4;
        if (d == null) {
            d = new int[(i2 * i3)];
        }
        if (c) {
            Arrays.fill(d, 0);
            int[] iArr2 = d;
            int[] iArr3 = new int[256];
            int[] iArr4 = new int[256];
            for (int i5 = 0; i5 < 256; i5++) {
                float f2 = (6.2831855f * ((float) i5)) / 256.0f;
                iArr3[i5] = (int) (((double) (-g)) * Math.sin((double) f2));
                iArr4[i5] = (int) (((double) g) * Math.cos((double) f2));
            }
            int i6 = i2 / 4;
            int i7 = i3 / 4;
            int[][] iArr5 = (int[][]) Array.newInstance(Integer.TYPE, i6, i7);
            for (int i8 = 0; i8 < i3; i8++) {
                for (int i9 = 0; i9 < i2; i9++) {
                    if (i8 >= i7 || i9 >= i6) {
                        i4 = iArr5[i9 % i6][i8 % i7];
                    } else {
                        int[] iArr6 = iArr5[i9];
                        int a2 = (int) (127.0f * (1.0f + a.a(((float) i9) / f, ((float) i8) / f)));
                        if (a2 < 0) {
                            a2 = 0;
                        } else if (a2 > 255) {
                            a2 = 255;
                        }
                        iArr6[i8] = a2;
                        i4 = a2;
                    }
                    int i10 = iArr3[i4] + i9;
                    int i11 = iArr4[i4] + i8;
                    if (i10 < 0 || i10 >= i2 || i11 < 0 || i11 >= i3) {
                        iArr2[(i8 * i2) + i9] = 0;
                    } else {
                        iArr2[(i8 * i2) + i9] = ((i11 * i2) + i10) - ((i8 * i2) + i9);
                    }
                }
            }
            c = false;
        }
        if (e == null) {
            e = new int[(i2 * i3)];
        }
        System.arraycopy(iArr, 0, e, 0, iArr.length);
        NDKEffectCustomYUV(iArr, d, e, i2, i3);
    }

    public static void b(int[] iArr, int i2, int i3, int i4, int i5) {
        if (d == null) {
            d = new int[(i2 * i3)];
        }
        if (c) {
            Arrays.fill(d, 0);
            NDKEffectFirstPinchRGB(d, i2, i3, i4, i5);
            c = false;
        }
        if (e == null) {
            e = new int[(i2 * i3)];
        }
        System.arraycopy(iArr, 0, e, 0, iArr.length);
        NDKEffectCustomYUV(iArr, d, e, i2, i3);
    }

    public static void c(int[] iArr, int i2, int i3) {
        if (d == null) {
            d = new int[(i2 * i3)];
        }
        if (c) {
            Arrays.fill(d, 0);
            int[] iArr2 = d;
            for (int i4 = 0; i4 < i3; i4++) {
                int i5 = 0;
                while (i5 < i2) {
                    try {
                        float f2 = (n * ((float) i5)) + (o * ((float) i4));
                        float f3 = (p * ((float) i5)) + (q * ((float) i4));
                        float f4 = f2 / h;
                        float f5 = f3 / (h * i);
                        int a2 = ((int) (k * a.a(f4 + 0.5f, f5))) + i5;
                        int a3 = ((int) (a.a(f4, f5 + 0.5f) * k)) + i4;
                        if (a2 < 0 || a2 >= i2 || a3 < 0 || a3 >= i3) {
                            iArr2[(i4 * i2) + i5] = 0;
                            i5++;
                        } else {
                            iArr2[(i4 * i2) + i5] = ((a3 * i2) + a2) - ((i4 * i2) + i5);
                            i5++;
                        }
                    } catch (Exception e2) {
                    }
                }
            }
            c = false;
        }
        if (e == null) {
            e = new int[(i2 * i3)];
        }
        System.arraycopy(iArr, 0, e, 0, iArr.length);
        NDKEffectCustomYUV(iArr, d, e, i2, i3);
    }

    public static void c(int[] iArr, int i2, int i3, int i4, int i5) {
        if (d == null) {
            d = new int[(i2 * i3)];
        }
        if (c) {
            Arrays.fill(d, 0);
            NDKEffectFirstTwirlRGB(d, i2, i3, i4, i5);
            c = false;
        }
        if (e == null) {
            e = new int[(i2 * i3)];
        }
        System.arraycopy(iArr, 0, e, 0, iArr.length);
        NDKEffectCustomYUV(iArr, d, e, i2, i3);
    }
}
