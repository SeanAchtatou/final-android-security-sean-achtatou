package com.androidillusion.d;

import com.androidillusion.cameraillusion.C0000R;

public final class b extends a {
    public int h;
    public float[] i;
    public int j;
    public String k;

    public b(int i2, String str, int i3, int i4, float[] fArr, int i5) {
        super(2, i2, str, i4, null);
        this.h = i3;
        this.i = fArr;
        this.j = i5;
    }

    public b(String str, float[] fArr, String str2) {
        this(5, str, 0, C0000R.drawable.thumb_mask_custom, fArr, 1);
        this.k = str2;
    }
}
