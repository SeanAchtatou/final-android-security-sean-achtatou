package com.androidillusion.f;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.androidillusion.cameraillusion.C0000R;

public final class w extends FrameLayout {
    int a;
    int b;
    View c;
    public TextView d;
    public Button e;
    ProgressBar f;
    public v g;
    int h;
    Activity i;
    private Matrix j = new Matrix();
    private Matrix k = new Matrix();
    private Matrix l = new Matrix();
    private float[] m = new float[2];

    public w(Activity activity, Handler handler, int i2, int i3, v vVar) {
        super(activity);
        this.j.invert(this.k);
        this.g = vVar;
        this.a = i2;
        this.b = i3;
        this.i = activity;
        this.c = View.inflate(activity, C0000R.layout.processing, null);
        ((LinearLayout) this.c).setGravity(17);
        this.e = (Button) this.c.findViewById(C0000R.id.cancel);
        this.f = (ProgressBar) this.c.findViewById(C0000R.id.progressbar_Horizontal);
        this.d = (TextView) this.c.findViewById(C0000R.id.processing);
        this.e.setOnClickListener(new x(this, handler));
        addView(this.c);
    }

    public final void a(int i2) {
        this.a = i2;
        this.b = i2;
        b(this.h);
    }

    public final void b(int i2) {
        this.h = i2;
        this.j = new Matrix();
        this.k = new Matrix();
        switch (i2) {
            case 0:
                ((LinearLayout) this.c).setGravity(17);
                break;
            case 90:
                ((LinearLayout) this.c).setGravity(17);
                this.j.preTranslate((float) ((-(this.b + this.a)) / 2), (float) ((this.a - this.b) / 2));
                this.j.postRotate(270.0f);
                break;
            case 180:
                ((LinearLayout) this.c).setGravity(17);
                this.j.preTranslate((float) (-this.a), (float) (-this.b));
                this.j.postRotate(180.0f);
                break;
            case 270:
                ((LinearLayout) this.c).setGravity(17);
                this.j.preTranslate((float) ((-(this.a - this.b)) / 2), (float) ((-(this.b + this.a)) / 2));
                this.j.postRotate(90.0f);
                break;
        }
        this.j.invert(this.k);
    }

    public final void c(int i2) {
        this.f.setProgress(i2);
        postInvalidate();
    }

    /* access modifiers changed from: protected */
    public final void dispatchDraw(Canvas canvas) {
        canvas.save();
        this.l = new Matrix(this.j);
        this.j.postConcat(canvas.getMatrix());
        canvas.setMatrix(this.j);
        this.j = new Matrix(this.l);
        super.dispatchDraw(canvas);
        canvas.restore();
    }

    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        float[] fArr = this.m;
        fArr[0] = motionEvent.getX();
        fArr[1] = motionEvent.getY();
        this.k.mapPoints(fArr);
        motionEvent.setLocation(fArr[0], fArr[1]);
        return super.dispatchTouchEvent(motionEvent);
    }
}
