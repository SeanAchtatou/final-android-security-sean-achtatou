package com.androidillusion.f;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.WindowManager;
import com.androidillusion.cameraillusion.C0000R;

public final class v extends Dialog {
    public int a;
    public w b;
    Handler c;

    public v(Activity activity, Handler handler, int i, int i2) {
        super(activity, C0000R.style.FullHeightDialog);
        this.a = (i2 * 4) / 6;
        this.c = handler;
        this.b = new w(activity, handler, i, i2, this);
        setContentView(this.b);
    }

    public final void a(int i) {
        this.b.c(i);
    }

    public final void onBackPressed() {
        Message message = new Message();
        message.setTarget(this.c);
        message.what = 14;
        message.sendToTarget();
    }

    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.width = this.a;
        attributes.height = this.a;
        this.b.a(this.a - (getWindow().getDecorView().getPaddingTop() * 2));
        getWindow().setAttributes(attributes);
    }
}
