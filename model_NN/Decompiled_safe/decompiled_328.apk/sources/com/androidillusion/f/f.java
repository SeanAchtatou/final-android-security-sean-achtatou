package com.androidillusion.f;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.os.Handler;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.androidillusion.cameraillusion.C0000R;
import com.androidillusion.d.a;

public final class f extends FrameLayout {
    public Button a;
    public Button b;
    public Button c;
    public Button d;
    public Button e;
    public Button f;
    public Button g;
    a h;
    Button i;
    Button j;
    TextView k;
    TextView l;
    TextView m;
    public SeekBar n;
    public SeekBar o;
    public SeekBar p;
    LinearLayout q;
    LinearLayout r;
    int s;
    int t;
    int u;
    public Handler v;
    private Matrix w;
    private Matrix x;
    private float[] y = new float[2];
    private Activity z;

    public f(Activity activity, Handler handler, int i2, int i3) {
        super(activity);
        this.z = activity;
        this.v = handler;
        this.w = new Matrix();
        this.x = new Matrix();
        this.w.invert(this.x);
        this.s = i2;
        this.t = i3;
    }

    public final void a() {
        removeAllViews();
        addView(this.r);
        a(this.u);
    }

    public final void a(int i2) {
        this.w = new Matrix();
        this.x = new Matrix();
        this.u = i2;
        switch (i2) {
            case 0:
                if (getChildCount() > 0) {
                    ((LinearLayout) getChildAt(0)).setGravity(81);
                    this.w.preTranslate((float) ((-this.s) / 12), 0.0f);
                    break;
                }
                break;
            case 90:
                if (getChildCount() > 0) {
                    int height = this.t - getHeight();
                    ((LinearLayout) getChildAt(0)).setGravity(81);
                    this.w.preTranslate((float) ((-(this.t + this.s)) / 2), (float) (height + (((this.s * 5) / 6) - this.t)));
                    this.w.postRotate(270.0f);
                    break;
                }
                break;
            case 180:
                if (getChildCount() > 0) {
                    int height2 = this.t - getHeight();
                    ((LinearLayout) getChildAt(0)).setGravity(81);
                    this.w.preTranslate((float) ((this.s / 12) - this.s), (float) (height2 + (-this.t)));
                    this.w.postRotate(180.0f);
                    break;
                }
                break;
            case 270:
                int i3 = this.t;
                getHeight();
                if (getChildCount() > 0) {
                    ((LinearLayout) getChildAt(0)).setGravity(81);
                    this.w.preTranslate((float) ((-(this.s - this.t)) / 2), (float) (-this.t));
                    this.w.postRotate(90.0f);
                    break;
                }
                break;
        }
        this.w.invert(this.x);
    }

    public final void a(LinearLayout linearLayout, LinearLayout linearLayout2) {
        this.q = linearLayout;
        this.r = linearLayout2;
        this.k = (TextView) this.r.findViewById(C0000R.id.textSetting1);
        this.l = (TextView) this.r.findViewById(C0000R.id.textSetting2);
        this.m = (TextView) this.r.findViewById(C0000R.id.textSetting3);
        this.n = (SeekBar) this.r.findViewById(C0000R.id.seekbarSetting1);
        this.n.setOnSeekBarChangeListener(new g(this));
        this.o = (SeekBar) this.r.findViewById(C0000R.id.seekbarSetting2);
        this.o.setOnSeekBarChangeListener(new k(this));
        this.p = (SeekBar) this.r.findViewById(C0000R.id.seekbarSetting3);
        this.p.setOnSeekBarChangeListener(new l(this));
        this.a = (Button) this.q.findViewById(C0000R.id.resetIllusions);
        this.a.setOnClickListener(new m(this));
        this.j = (Button) this.r.findViewById(C0000R.id.illusionReset);
        this.j.setOnClickListener(new n(this));
        ((TextView) this.q.findViewById(C0000R.id.textFilter)).setText(this.z.getString(C0000R.string.camera_filter));
        ((TextView) this.q.findViewById(C0000R.id.textEffect)).setText(this.z.getString(C0000R.string.camera_effect));
        ((TextView) this.q.findViewById(C0000R.id.textMask)).setText(this.z.getString(C0000R.string.camera_mask));
        this.b = (Button) this.q.findViewById(C0000R.id.selectFilter);
        this.b.setOnClickListener(new o(this));
        this.d = (Button) this.q.findViewById(C0000R.id.selectEffect);
        this.d.setOnClickListener(new p(this));
        this.f = (Button) this.q.findViewById(C0000R.id.selectMask);
        this.f.setOnClickListener(new q(this));
        this.c = (Button) this.q.findViewById(C0000R.id.configFilter);
        this.c.setOnClickListener(new r(this));
        this.e = (Button) this.q.findViewById(C0000R.id.configEffect);
        this.e.setOnClickListener(new h(this));
        this.g = (Button) this.q.findViewById(C0000R.id.configMask);
        this.g.setOnClickListener(new i(this));
        this.i = (Button) this.r.findViewById(C0000R.id.illusionConfigBack);
        this.i.setOnClickListener(new j(this));
        super.addView(this.q);
        a(0);
    }

    public final void a(a aVar) {
        this.h = aVar;
        if (aVar.e != 0) {
            this.k.setVisibility(4);
            this.l.setVisibility(4);
            this.m.setVisibility(4);
            this.n.setVisibility(4);
            this.o.setVisibility(4);
            this.p.setVisibility(4);
            if (aVar.e > 0) {
                this.k.setVisibility(0);
                this.n.setVisibility(0);
                this.k.setText(aVar.f[0]);
                this.n.setProgress(aVar.g[0]);
            }
            if (aVar.e > 1) {
                this.l.setVisibility(0);
                this.o.setVisibility(0);
                this.l.setText(aVar.f[1]);
                this.o.setProgress(aVar.g[1]);
            }
            if (aVar.e > 2) {
                this.m.setVisibility(0);
                this.p.setVisibility(0);
                this.m.setText(aVar.f[2]);
                this.p.setProgress(aVar.g[2]);
            }
        }
    }

    public final void b() {
        removeAllViews();
        addView(this.q);
        a(this.u);
    }

    /* access modifiers changed from: protected */
    public final void dispatchDraw(Canvas canvas) {
        canvas.save();
        canvas.setMatrix(this.w);
        super.dispatchDraw(canvas);
        canvas.restore();
    }

    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        float[] fArr = this.y;
        fArr[0] = motionEvent.getX();
        fArr[1] = motionEvent.getY();
        this.x.mapPoints(fArr);
        motionEvent.setLocation(fArr[0], fArr[1]);
        return super.dispatchTouchEvent(motionEvent);
    }
}
