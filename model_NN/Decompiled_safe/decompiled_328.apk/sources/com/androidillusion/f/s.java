package com.androidillusion.f;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.androidillusion.cameraillusion.C0000R;

public final class s extends FrameLayout {
    int a;
    int b;
    View c;
    public ImageView d;
    public TextView e;
    public TextView f;
    public Button g;
    public Button h;
    public String i;
    public c j;
    int k;
    Activity l;
    private Matrix m = new Matrix();
    private Matrix n = new Matrix();
    private Matrix o = new Matrix();
    private float[] p = new float[2];

    public s(Activity activity, int i2, int i3, c cVar) {
        super(activity);
        this.m.invert(this.n);
        this.j = cVar;
        this.a = i2;
        this.b = i3;
        this.l = activity;
        this.c = View.inflate(activity, C0000R.layout.market_dialog, null);
        ((LinearLayout) this.c).setGravity(17);
        this.e = (TextView) this.c.findViewById(C0000R.id.title);
        this.d = (ImageView) this.c.findViewById(C0000R.id.icon);
        this.f = (TextView) this.c.findViewById(C0000R.id.description);
        if (activity.getResources().getDisplayMetrics().density > 1.0f && (activity.getResources().getConfiguration().screenLayout & 3) == 3) {
            Log.i("camera", "space 1.3");
            this.f.setLineSpacing(0.0f, 1.3f);
        }
        this.g = (Button) this.c.findViewById(C0000R.id.acceptButton);
        this.h = (Button) this.c.findViewById(C0000R.id.cancelButton);
        this.g.setOnClickListener(new t(this));
        this.h.setOnClickListener(new u(this));
        addView(this.c);
    }

    public final void a(int i2) {
        this.a = i2;
        this.b = i2;
        b(this.k);
    }

    public final void b(int i2) {
        this.k = i2;
        this.m = new Matrix();
        this.n = new Matrix();
        switch (i2) {
            case 0:
                ((LinearLayout) this.c).setGravity(17);
                break;
            case 90:
                ((LinearLayout) this.c).setGravity(17);
                this.m.preTranslate((float) ((-(this.b + this.a)) / 2), (float) ((this.a - this.b) / 2));
                this.m.postRotate(270.0f);
                break;
            case 180:
                ((LinearLayout) this.c).setGravity(17);
                this.m.preTranslate((float) (-this.a), (float) (-this.b));
                this.m.postRotate(180.0f);
                break;
            case 270:
                ((LinearLayout) this.c).setGravity(17);
                this.m.preTranslate((float) ((-(this.a - this.b)) / 2), (float) ((-(this.b + this.a)) / 2));
                this.m.postRotate(90.0f);
                break;
        }
        this.m.invert(this.n);
    }

    /* access modifiers changed from: protected */
    public final void dispatchDraw(Canvas canvas) {
        canvas.save();
        this.o = new Matrix(this.m);
        this.m.postConcat(canvas.getMatrix());
        canvas.setMatrix(this.m);
        this.m = new Matrix(this.o);
        super.dispatchDraw(canvas);
        canvas.restore();
    }

    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        float[] fArr = this.p;
        fArr[0] = motionEvent.getX();
        fArr[1] = motionEvent.getY();
        this.n.mapPoints(fArr);
        motionEvent.setLocation(fArr[0], fArr[1]);
        return super.dispatchTouchEvent(motionEvent);
    }
}
