package com.androidillusion.f;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

public final class b extends View {
    public boolean a;
    public int b = 0;
    public int c = 0;
    public int d;
    public int e;
    public Bitmap[] f;
    public Paint g;
    public Bitmap h;
    public int i;
    public int j;
    public int k;
    public int l;
    public int m;
    public int n;
    private Paint o;

    public b(Context context, int i2, int i3, int i4, int i5) {
        super(context);
        this.k = i2;
        this.l = i3;
        this.m = i4;
        this.n = i5;
        this.g = new Paint();
        this.g.setColor(-65536);
        this.g.setStyle(Paint.Style.STROKE);
        this.o = new Paint();
        this.o.setColor(-1);
        this.o.setStyle(Paint.Style.FILL);
    }

    public final void a() {
        if (this.b < 0) {
            this.b = 0;
        } else if (this.b > this.m - this.k) {
            this.b = this.m - this.k;
        }
        if (this.c < 0) {
            this.c = 0;
        } else if (this.c > this.n - this.l) {
            this.c = this.n - this.l;
        }
    }

    public final void a(float f2, float f3) {
        if (((float) (this.b - this.d)) - f2 >= 0.0f && ((float) (this.b - this.d)) - f2 < ((float) (this.m - this.k))) {
            this.d = (int) (((float) this.d) + f2);
        }
        if (((float) (this.c - this.e)) - f3 >= 0.0f && ((float) (this.c - this.e)) - f3 < ((float) (this.n - this.l))) {
            this.e = (int) (((float) this.e) + f3);
        }
    }

    public final void a(Bitmap[] bitmapArr) {
        this.f = bitmapArr;
    }

    public final void b() {
        this.b -= this.d;
        this.c -= this.e;
        this.d = 0;
        this.e = 0;
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        if (this.f != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < 9) {
                    if (this.f[i3] != null) {
                        canvas.drawBitmap(this.f[i3], (float) ((((i3 / 3) - 1) * this.k) + this.d), (float) ((((i3 % 3) - 1) * this.l) + this.e), (Paint) null);
                        if (this.a && this.h != null) {
                            canvas.drawRect((float) this.i, (float) this.j, (float) (this.i + this.h.getWidth() + 4), (float) (this.j + this.h.getHeight() + 4), this.o);
                            canvas.drawBitmap(this.h, (float) (this.i + 2), (float) (this.j + 2), (Paint) null);
                            int width = (((this.b - this.d) * this.h.getWidth()) / this.m) + this.i;
                            int height = this.j + (((this.c - this.e) * this.h.getHeight()) / this.n);
                            Canvas canvas2 = canvas;
                            canvas2.drawRect((float) (width + 2), (float) (height + 2), (float) (width + ((this.h.getWidth() * this.k) / this.m) + 2), (float) (height + ((this.h.getHeight() * this.l) / this.n) + 2), this.g);
                        }
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }
}
