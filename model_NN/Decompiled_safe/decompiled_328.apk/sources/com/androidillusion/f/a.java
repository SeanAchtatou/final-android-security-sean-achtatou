package com.androidillusion.f;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import com.google.ads.AdView;

public final class a extends FrameLayout {
    int a;
    int b;
    private Matrix c = new Matrix();
    private Matrix d = new Matrix();
    private float[] e = new float[2];

    public a(Context context, int i, int i2) {
        super(context);
        this.c.invert(this.d);
        this.a = i;
        this.b = i2;
        setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
    }

    public final void a(int i, boolean z) {
        this.c = new Matrix();
        this.d = new Matrix();
        if (!z) {
            this.c.postRotate(90.0f);
            this.c.invert(this.d);
            return;
        }
        switch (i) {
            case 0:
                if (getChildCount() > 0) {
                    ((AdView) getChildAt(0)).setGravity(49);
                    this.c.preTranslate((float) ((-this.a) / 12), 0.0f);
                    break;
                }
                break;
            case 90:
                if (getChildCount() > 0) {
                    ((AdView) getChildAt(0)).setGravity(49);
                    this.c.preTranslate((float) ((-(this.b + this.a)) / 2), 0.0f);
                    this.c.postRotate(270.0f);
                    break;
                }
                break;
            case 180:
                if (getChildCount() > 0) {
                    int height = this.b - getHeight();
                    ((AdView) getChildAt(0)).setGravity(49);
                    this.c.preTranslate((float) ((this.a / 12) - this.a), (float) (height + (-this.b)));
                    this.c.postRotate(180.0f);
                    break;
                }
                break;
            case 270:
                int height2 = this.b - getHeight();
                if (getChildCount() > 0) {
                    ((AdView) getChildAt(0)).setGravity(81);
                    this.c.preTranslate((float) ((-(this.a - this.b)) / 2), (float) (height2 + (-this.b)));
                    this.c.postRotate(90.0f);
                    break;
                }
                break;
        }
        this.c.invert(this.d);
    }

    public final void a(AdView adView) {
        super.addView(adView);
        a(0, true);
    }

    /* access modifiers changed from: protected */
    public final void dispatchDraw(Canvas canvas) {
        canvas.save();
        canvas.setMatrix(this.c);
        super.dispatchDraw(canvas);
        canvas.restore();
    }

    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        float[] fArr = this.e;
        fArr[0] = motionEvent.getX();
        fArr[1] = motionEvent.getY();
        this.d.mapPoints(fArr);
        motionEvent.setLocation(fArr[0], fArr[1]);
        return super.dispatchTouchEvent(motionEvent);
    }
}
