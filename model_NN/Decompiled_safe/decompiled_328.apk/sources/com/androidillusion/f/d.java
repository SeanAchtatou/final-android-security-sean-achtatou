package com.androidillusion.f;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.androidillusion.a.a;
import com.androidillusion.cameraillusion.C0000R;
import java.util.Vector;

public final class d extends FrameLayout {
    int a;
    int b;
    View c;
    public TextView d;
    public ListView e;
    public Button f;
    public a g;
    int h;
    public Handler i;
    private Matrix j;
    private Matrix k;
    private Matrix l;
    private float[] m = new float[2];

    public d(Context context, Handler handler, int i2, int i3, Vector vector, Vector vector2, Vector vector3) {
        super(context);
        this.i = handler;
        this.j = new Matrix();
        this.k = new Matrix();
        this.l = new Matrix();
        this.j.invert(this.k);
        this.a = i2;
        this.b = i3;
        this.c = View.inflate(context, C0000R.layout.element_dialog, null);
        this.d = (TextView) this.c.findViewById(C0000R.id.selector_title);
        this.e = (ListView) this.c.findViewById(C0000R.id.description);
        this.g = new a(context, this.i, vector, vector2, vector3);
        this.e.setAdapter((ListAdapter) this.g);
        this.f = (Button) this.c.findViewById(C0000R.id.cancelButton);
        this.f.setOnClickListener(new e(this));
        addView(this.c);
    }

    public final void a(int i2) {
        this.a = i2;
        this.b = i2;
        b(this.h);
    }

    public final void b(int i2) {
        this.h = i2;
        this.j = new Matrix();
        this.k = new Matrix();
        switch (i2) {
            case 90:
                this.j.preTranslate((float) ((-(this.b + this.a)) / 2), (float) ((this.a - this.b) / 2));
                this.j.postRotate(270.0f);
                break;
            case 180:
                this.j.preTranslate((float) (-this.a), (float) (-this.b));
                this.j.postRotate(180.0f);
                break;
            case 270:
                this.j.preTranslate((float) ((-(this.a - this.b)) / 2), (float) ((-(this.b + this.a)) / 2));
                this.j.postRotate(90.0f);
                break;
        }
        this.j.invert(this.k);
    }

    /* access modifiers changed from: protected */
    public final void dispatchDraw(Canvas canvas) {
        canvas.save();
        this.l = new Matrix(this.j);
        this.j.postConcat(canvas.getMatrix());
        canvas.setMatrix(this.j);
        this.j = new Matrix(this.l);
        super.dispatchDraw(canvas);
        canvas.restore();
    }

    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        float[] fArr = this.m;
        fArr[0] = motionEvent.getX();
        fArr[1] = motionEvent.getY();
        this.k.mapPoints(fArr);
        motionEvent.setLocation(fArr[0], fArr[1]);
        return super.dispatchTouchEvent(motionEvent);
    }
}
