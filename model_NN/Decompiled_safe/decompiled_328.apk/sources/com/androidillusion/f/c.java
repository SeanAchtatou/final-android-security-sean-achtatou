package com.androidillusion.f;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.WindowManager;
import com.androidillusion.cameraillusion.C0000R;

public final class c extends Dialog {
    public int a;
    public s b;

    public c(Activity activity, int i, int i2) {
        super(activity, C0000R.style.FullHeightDialog);
        this.a = (i2 * 5) / 6;
        this.b = new s(activity, i, i2, this);
        setContentView(this.b);
    }

    public final void a(String str, String str2, String str3, String str4, String str5, Bitmap bitmap) {
        this.b.e.setText(str);
        this.b.f.setText(str2);
        this.b.d.setImageBitmap(bitmap);
        this.b.g.setText(str3);
        this.b.h.setText(str4);
        this.b.i = str5;
    }

    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.width = this.a;
        attributes.height = this.a;
        this.b.a(this.a - (getWindow().getDecorView().getPaddingTop() * 2));
        getWindow().setAttributes(attributes);
    }
}
