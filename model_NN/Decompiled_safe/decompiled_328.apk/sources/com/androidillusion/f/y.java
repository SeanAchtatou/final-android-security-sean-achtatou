package com.androidillusion.f;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import com.androidillusion.cameraillusion.C0000R;
import java.util.Vector;

public final class y extends Dialog {
    public int a;
    public d b;

    public y(Context context, Handler handler, int i, int i2, Vector vector, Vector vector2, Vector vector3) {
        super(context, C0000R.style.FullHeightDialog);
        this.a = (i2 * 5) / 6;
        this.b = new d(context, handler, i, i2, vector, vector2, vector3);
        setContentView(this.b);
    }

    public final void a(int i, int i2, String str) {
        this.b.g.b = i;
        this.b.g.a = i2;
        this.b.d.setText(str);
        this.b.g.notifyDataSetChanged();
        this.b.e.setSelection(i2);
    }

    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.width = this.a;
        attributes.height = this.a;
        this.b.a(this.a - (getWindow().getDecorView().getPaddingTop() * 2));
        getWindow().setAttributes(attributes);
    }
}
