package com.androidillusion.e;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import com.androidillusion.cameraillusion.C0000R;
import com.androidillusion.cameraillusion.CameraActivity;
import com.androidillusion.f.c;
import java.util.List;

public final class e {
    public static void a(Activity activity) {
        activity.requestWindowFeature(1);
        activity.getWindow().setFlags(1024, 1024);
        activity.getWindow().setFlags(512, 512);
    }

    public static void a(Activity activity, c cVar, int i) {
        switch (i) {
            case 0:
                cVar.a(activity.getString(C0000R.string.popup_videocam_title), activity.getString(C0000R.string.popup_videocam_description), activity.getString(17039370), activity.getString(17039360), "market://details?id=com.androidillusion.videocamillusion", BitmapFactory.decodeResource(activity.getResources(), C0000R.drawable.videocam_illusion_192));
                break;
            case 1:
                cVar.a(activity.getString(C0000R.string.popup_photo_title), activity.getString(C0000R.string.popup_photo_description), activity.getString(17039370), activity.getString(17039360), "market://details?id=com.androidillusion.photoillusion", BitmapFactory.decodeResource(activity.getResources(), C0000R.drawable.photo_illusion_192));
                break;
            case 2:
                cVar.a(activity.getString(C0000R.string.popup_camera_pro_title), activity.getString(C0000R.string.popup_camera_pro_description), activity.getString(17039370), activity.getString(17039360), "market://details?id=com.androidillusion.cameraillusionpro", BitmapFactory.decodeResource(activity.getResources(), C0000R.drawable.camera_illusion_pro_192));
                break;
        }
        cVar.show();
    }

    public static void a(Activity activity, String str) {
        try {
            activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        } catch (Exception e) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.androidillusion.e.e.a(android.app.Activity, com.androidillusion.f.c, int):void
     arg types: [com.androidillusion.cameraillusion.CameraActivity, com.androidillusion.f.c, int]
     candidates:
      com.androidillusion.e.e.a(com.androidillusion.cameraillusion.CameraActivity, com.androidillusion.f.c, int):void
      com.androidillusion.e.e.a(android.app.Activity, com.androidillusion.f.c, int):void */
    public static void a(CameraActivity cameraActivity, c cVar, int i) {
        Intent intent = new Intent();
        switch (i) {
            case 0:
                if (b(cameraActivity, "com.androidillusion.videocamillusionpro")) {
                    Log.i("camera", "Launching Videocam illusion Pro...");
                    intent.setClassName("com.androidillusion.videocamillusionpro", "com.androidillusion.videocamillusionpro.VideoillusionActivity");
                    cameraActivity.b();
                    cameraActivity.startActivity(intent);
                    return;
                } else if (b(cameraActivity, "com.androidillusion.videocamillusion")) {
                    Log.i("camera", "Launching Videocam illusion...");
                    intent.setClassName("com.androidillusion.videocamillusion", "com.androidillusion.videocamillusion.VideoillusionActivity");
                    cameraActivity.b();
                    cameraActivity.startActivity(intent);
                    return;
                } else {
                    Log.i("camera", "Videocam illusion not found...");
                    a((Activity) cameraActivity, cVar, 0);
                    return;
                }
            case 1:
                if (b(cameraActivity, "com.androidillusion.photoillusionpro")) {
                    Log.i("camera", "Launching Photo illusion Pro...");
                    intent.setClassName("com.androidillusion.photoillusionpro", "com.androidillusion.photoillusionpro.PhotoActivity");
                    cameraActivity.b();
                    cameraActivity.startActivity(intent);
                    return;
                } else if (b(cameraActivity, "com.androidillusion.photoillusion")) {
                    Log.i("camera", "Launching Photo illusion...");
                    intent.setClassName("com.androidillusion.photoillusion", "com.androidillusion.photoillusion.PhotoActivity");
                    cameraActivity.b();
                    cameraActivity.startActivity(intent);
                    return;
                } else {
                    Log.i("camera", "Photo illusion not found...");
                    a((Activity) cameraActivity, cVar, 1);
                    return;
                }
            default:
                return;
        }
    }

    private static boolean b(Activity activity, String str) {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.LAUNCHER");
        List<ResolveInfo> queryIntentActivities = activity.getPackageManager().queryIntentActivities(intent, 0);
        for (int i = 0; i < queryIntentActivities.size(); i++) {
            if (str.compareTo(queryIntentActivities.get(i).activityInfo.packageName) == 0) {
                return true;
            }
        }
        return false;
    }
}
