package com.androidillusion.e;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import com.androidillusion.c.a;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Vector;

public final class b {
    public static Bitmap a(String str, int i, int i2, boolean z) {
        int i3;
        int i4;
        if (str == null) {
            return null;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(str, options);
        int i5 = options.outWidth;
        int i6 = options.outHeight;
        if (options.outWidth <= 0 || options.outHeight <= 0) {
            return null;
        }
        if (((float) i) / ((float) i2) > ((float) i5) / ((float) i6)) {
            i4 = (int) ((((float) options.outWidth) * ((float) i2)) / ((float) options.outHeight));
            i3 = i2;
        } else {
            i3 = (int) ((((float) options.outHeight) * ((float) i)) / ((float) options.outWidth));
            i4 = i;
        }
        if (z) {
            if (i4 % 2 != 0) {
                i4++;
            }
            if (i3 % 2 != 0) {
                i3++;
            }
        }
        int i7 = 1;
        while (i5 / (i7 + 1) > i4) {
            i7++;
        }
        options.inSampleSize = i7;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        options.inDither = false;
        options.inJustDecodeBounds = false;
        Bitmap decodeFile = BitmapFactory.decodeFile(str, options);
        Bitmap.Config config = Bitmap.Config.ARGB_8888;
        Rect rect = new Rect(0, 0, decodeFile.getWidth(), decodeFile.getHeight());
        Rect rect2 = new Rect(0, 0, i4, i3);
        Bitmap createBitmap = Bitmap.createBitmap(i4, i3, config);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        paint.setFilterBitmap(true);
        canvas.drawBitmap(decodeFile, rect, rect2, paint);
        return createBitmap;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Uri a(Context context, Bitmap bitmap, int i, String str) {
        File file = new File(String.valueOf(b()) + a.b + str);
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            FileOutputStream fileOutputStream = file.canWrite() ? new FileOutputStream(file) : null;
            Matrix matrix = new Matrix();
            switch (i) {
                case 0:
                default:
                    Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true).compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream);
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    break;
                case 90:
                    matrix.preTranslate(0.0f, (float) (-bitmap.getHeight()));
                    matrix.postRotate(90.0f);
                    Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true).compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream);
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    break;
                case 180:
                    matrix.preTranslate((float) (-bitmap.getWidth()), 0.0f);
                    matrix.postRotate(180.0f);
                    Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true).compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream);
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    break;
                case 270:
                    matrix.preTranslate((float) (-bitmap.getWidth()), 0.0f);
                    matrix.postRotate(270.0f);
                    Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true).compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream);
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    break;
            }
        } catch (Exception e) {
        }
        return a(context, String.valueOf(b()) + a.b + str, "", str, System.currentTimeMillis());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static Uri a(Context context, String str, String str2, String str3, long j) {
        long length = new File(str).length();
        ContentValues contentValues = new ContentValues(9);
        contentValues.put("title", str2);
        contentValues.put("_display_name", str3);
        contentValues.put("datetaken", Long.valueOf(j));
        contentValues.put("mime_type", "image/jpeg");
        contentValues.put("orientation", (Integer) 0);
        contentValues.put("_data", str);
        contentValues.put("_size", Long.valueOf(length));
        return context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
    }

    public static String a(Activity activity, Uri uri) {
        Cursor managedQuery = activity.managedQuery(uri, new String[]{"_data"}, null, null, null);
        if (managedQuery.getCount() <= 0) {
            return null;
        }
        int columnIndexOrThrow = managedQuery.getColumnIndexOrThrow("_data");
        managedQuery.moveToFirst();
        return managedQuery.getString(columnIndexOrThrow);
    }

    public static Vector a(String str) {
        Vector vector = new Vector();
        Log.i("camera", "Searching files in " + str);
        File file = new File(str);
        if (file.exists()) {
            File[] listFiles = file.listFiles();
            for (int i = 0; i < listFiles.length; i++) {
                try {
                    Log.i("camera", "File: " + listFiles[i].getName());
                    if (listFiles[i].getName().toLowerCase().endsWith(".png")) {
                        vector.add(listFiles[i].getName());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return a(vector);
    }

    private static Vector a(Vector vector) {
        if (vector.size() == 1) {
            return vector;
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= vector.size()) {
                return vector;
            }
            int i3 = i2 + 1;
            while (true) {
                int i4 = i3;
                if (i4 >= vector.size()) {
                    break;
                }
                if (((String) vector.get(i2)).compareToIgnoreCase((String) vector.get(i4)) > 0) {
                    vector.set(i2, (String) vector.get(i4));
                    vector.set(i4, (String) vector.get(i2));
                }
                i3 = i4 + 1;
            }
            i = i2 + 1;
        }
    }

    public static void a(String str, char[][] cArr) {
        FileOutputStream fileOutputStream = null;
        File file = new File(str);
        Log.i("camera", "saving html " + str);
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            if (file.canWrite()) {
                fileOutputStream = new FileOutputStream(file);
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
            stringBuffer.append("<html lang=en>");
            stringBuffer.append("<body style=\"background: Black;\" lang=en leftmargin=0 topmargin=0>");
            stringBuffer.append("<head>");
            stringBuffer.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
            stringBuffer.append("<style type=\"text/css\">");
            stringBuffer.append("pre {");
            stringBuffer.append("font: 7pt monospace;");
            stringBuffer.append("line-height: 88%;");
            stringBuffer.append("color: #FFFFFF;");
            stringBuffer.append("}");
            stringBuffer.append("code {");
            stringBuffer.append("height: 100%;");
            stringBuffer.append("width: 100%;");
            stringBuffer.append("}");
            stringBuffer.append("</style>");
            stringBuffer.append("</head>");
            stringBuffer.append("<pre><code>");
            for (int i = 0; i < cArr.length; i++) {
                stringBuffer.append("\n" + new String(cArr[i]));
            }
            stringBuffer.append("\n</code></pre></body></html>");
            fileOutputStream.write(stringBuffer.toString().getBytes("UTF-8"));
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (Exception e) {
            Log.i("camera", "saveFile exception " + e);
        }
    }

    public static boolean a() {
        return "mounted".equals(Environment.getExternalStorageState());
    }

    public static String b() {
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        return externalStorageDirectory != null ? externalStorageDirectory.getAbsolutePath() : "/sdcard";
    }

    public static void b(String str) {
        if (a()) {
            Vector vector = new Vector();
            for (File file = new File(str); !file.getAbsolutePath().equalsIgnoreCase(b()); file = new File(file.getParent())) {
                vector.add(file);
            }
            for (int size = vector.size() - 1; size >= 0; size--) {
                if (!(new File(((File) vector.get(size)).getAbsolutePath()).exists())) {
                    new File(((File) vector.get(size)).getAbsolutePath()).mkdir();
                    Log.i("camera", "Creating folder... " + ((File) vector.get(size)).getAbsolutePath());
                }
            }
        }
    }

    public static String c() {
        Date date = new Date(System.currentTimeMillis());
        DecimalFormat decimalFormat = new DecimalFormat();
        decimalFormat.setMinimumIntegerDigits(2);
        String str = new StringBuilder().append(date.getYear() + 1900).toString().toString();
        String format = decimalFormat.format((long) (date.getMonth() + 1));
        String format2 = decimalFormat.format((long) date.getDate());
        String format3 = decimalFormat.format((long) date.getHours());
        String format4 = decimalFormat.format((long) date.getMinutes());
        return String.valueOf(str) + "-" + format + "-" + format2 + " " + format3 + "_" + format4 + "_" + decimalFormat.format((long) date.getSeconds());
    }

    public static void c(String str) {
        try {
            File file = new File(str);
            if (file.isDirectory()) {
                String[] list = file.list();
                for (String file2 : list) {
                    new File(file, file2).delete();
                }
            }
        } catch (Exception e) {
        }
    }
}
