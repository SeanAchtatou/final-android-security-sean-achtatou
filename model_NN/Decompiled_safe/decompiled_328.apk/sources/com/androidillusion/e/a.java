package com.androidillusion.e;

import android.content.Context;
import android.graphics.Point;
import android.hardware.Camera;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import com.androidillusion.b.f;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Vector;

public final class a {
    private static Method a;
    private static Method b;
    private static Method c;
    private static Method d;
    private static Method e;
    private static Method f;
    private static Object g;
    private static Method h;

    static {
        Object a2;
        Method a3;
        try {
            a = Camera.class.getMethod("getNumberOfCameras", new Class[0]);
        } catch (NoSuchMethodException e2) {
        }
        try {
            b = Camera.class.getMethod("open", Integer.TYPE);
        } catch (NoSuchMethodException e3) {
        }
        try {
            c = Camera.Parameters.class.getMethod("isZoomSupported", new Class[0]);
        } catch (NoSuchMethodException e4) {
        }
        try {
            d = Camera.Parameters.class.getMethod("getMaxZoom", new Class[0]);
        } catch (NoSuchMethodException e5) {
        }
        try {
            e = Camera.Parameters.class.getMethod("getZoomRatios", new Class[0]);
        } catch (NoSuchMethodException e6) {
        }
        try {
            f = Camera.Parameters.class.getMethod("setZoom", Integer.TYPE);
        } catch (NoSuchMethodException e7) {
        }
        Class a4 = a("android.os.ServiceManager");
        if (a4 == null) {
            a2 = null;
        } else {
            Method a5 = a(a4, "getService", String.class);
            if (a5 == null) {
                a2 = null;
            } else {
                Object a6 = a(a5, (Object) null, "hardware");
                if (a6 == null) {
                    a2 = null;
                } else {
                    Class a7 = a("android.os.IHardwareService$Stub");
                    if (a7 == null) {
                        a2 = null;
                    } else {
                        Method a8 = a(a7, "asInterface", IBinder.class);
                        if (a8 == null) {
                            a2 = null;
                        } else {
                            a2 = a(a8, (Object) null, a6);
                        }
                    }
                }
            }
        }
        g = a2;
        if (a2 == null) {
            a3 = null;
        } else {
            a3 = a(a2.getClass(), "setFlashlightEnabled", Boolean.TYPE);
        }
        h = a3;
        if (g != null) {
            Log.i("camera", "iHardwareService_setFlashEnabledMethod TRUE");
        } else {
            Log.i("camera", "iHardwareService_setFlashEnabledMethod FALSE");
        }
    }

    private static int a() {
        int i;
        if (a != null) {
            try {
                i = ((Integer) a.invoke(null, new Object[0])).intValue();
            } catch (Exception e2) {
                e2.printStackTrace();
                i = 1;
            }
            Log.i("camera", "API 9+ -> number of cameras:" + i);
            return i;
        }
        Log.i("camera", "API <9 -> number of cameras:" + 1);
        return 1;
    }

    private static int a(Vector vector, int i, int i2, Point point) {
        float f2;
        int i3;
        float f3;
        int i4;
        int i5 = 0;
        float f4 = ((float) point.x) / ((float) point.y);
        int i6 = (i * 5) / 6;
        int i7 = 0;
        float f5 = 0.0f;
        int i8 = 0;
        while (i5 < vector.size()) {
            Point point2 = (Point) vector.elementAt(i5);
            int i9 = point2.x * point2.y;
            try {
                f2 = ((float) point2.x) / ((float) point2.y);
            } catch (Exception e2) {
                f2 = 0.0f;
            }
            if (point2.x > i6 || point2.y > i2 || (i9 <= i7 && (Math.abs(f4 - f5) <= 0.05f || Math.abs(f4 - f5) <= Math.abs(f4 - f2)))) {
                i3 = i7;
                f3 = f5;
                i4 = i8;
            } else {
                i3 = i9;
                f3 = f2;
                i4 = i5;
            }
            i5++;
            i8 = i4;
            f5 = f3;
            i7 = i3;
        }
        return i8;
    }

    public static Camera a(Camera camera, f fVar) {
        Camera camera2;
        if (camera != null) {
            camera.stopPreview();
            camera.release();
            Log.i("camera", "openCamera:cameraStopped");
            camera2 = null;
        } else {
            camera2 = camera;
        }
        if (fVar.b == 0) {
            return Camera.open();
        }
        try {
            if (b == null) {
                return camera2;
            }
            return (Camera) b.invoke(null, Integer.valueOf(fVar.b));
        } catch (Exception e2) {
            return Camera.open();
        }
    }

    public static f a(Context context, int i, int i2) {
        Camera open;
        f fVar = new f();
        fVar.b = 0;
        fVar.e = false;
        fVar.a = a();
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 >= fVar.a) {
                return fVar;
            }
            com.androidillusion.b.a aVar = new com.androidillusion.b.a();
            if (i4 == 0) {
                try {
                    Camera open2 = Camera.open();
                    aVar.j = false;
                    open = open2;
                } catch (Exception e2) {
                    fVar.e = true;
                    return fVar;
                }
            } else {
                try {
                    if (b != null) {
                        Camera camera = (Camera) b.invoke(null, Integer.valueOf(i4));
                        aVar.j = true;
                        open = camera;
                    } else {
                        open = null;
                    }
                } catch (Exception e3) {
                    open = Camera.open();
                }
            }
            aVar.f = c(open);
            if (aVar.f) {
                aVar.h = d(open);
                List e4 = e(open);
                DecimalFormat decimalFormat = new DecimalFormat("###.##");
                aVar.i = new String[(aVar.h + 1)];
                int i5 = 0;
                while (true) {
                    int i6 = i5;
                    if (i6 >= aVar.h + 1) {
                        break;
                    }
                    if (e4 != null) {
                        aVar.i[i6] = String.valueOf(decimalFormat.format(((double) ((Integer) e4.get(i6)).intValue()) / 100.0d)) + "x";
                    } else {
                        aVar.f = false;
                    }
                    i5 = i6 + 1;
                }
            }
            aVar.d = b(open);
            aVar.e = 0;
            aVar.a = a(open);
            aVar.b = a(aVar.a, i, i2, aVar.b());
            aVar.c = aVar.b;
            aVar.k = 0;
            List<String> supportedFlashModes = open.getParameters().getSupportedFlashModes();
            Log.i("camera", "supported flash: " + supportedFlashModes);
            if (supportedFlashModes == null) {
                Log.i("camera", "Feature " + context.getPackageManager().hasSystemFeature("android.hardware.camera.flash"));
                Log.i("camera", "Build " + Build.MODEL);
                boolean z = false;
                if (context.getPackageManager().hasSystemFeature("android.hardware.camera.flash") || Build.MODEL.contains("X10")) {
                    z = true;
                }
                if (!z || h == null || i4 != 0) {
                    aVar.l = false;
                } else {
                    aVar.l = true;
                    aVar.n = true;
                }
                aVar.p = false;
                aVar.o = false;
                aVar.m = false;
            } else {
                aVar.o = a("on", supportedFlashModes);
                aVar.p = a("auto", supportedFlashModes);
                aVar.m = a("torch", supportedFlashModes);
                if ((aVar.o | aVar.p) || aVar.m) {
                    aVar.l = true;
                } else {
                    aVar.l = false;
                }
            }
            fVar.c.add(aVar);
            open.release();
            i3 = i4 + 1;
        }
    }

    private static Class a(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e2) {
            return null;
        } catch (RuntimeException e3) {
            Log.w("camera", "Unexpected error while finding class " + str, e3);
            return null;
        }
    }

    private static Object a(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e2) {
            Log.w("camera", "Unexpected error while invoking " + method, e2);
            return null;
        } catch (InvocationTargetException e3) {
            Log.w("camera", "Unexpected error while invoking " + method, e3.getCause());
            return null;
        } catch (RuntimeException e4) {
            Log.w("camera", "Unexpected error while invoking " + method, e4);
            return null;
        }
    }

    private static Method a(Class cls, String str, Class... clsArr) {
        try {
            return cls.getMethod(str, clsArr);
        } catch (NoSuchMethodException e2) {
            return null;
        } catch (RuntimeException e3) {
            Log.w("camera", "Unexpected error while finding method " + str, e3);
            return null;
        }
    }

    private static Vector a(Camera camera) {
        Vector vector = new Vector();
        List<Camera.Size> supportedPreviewSizes = camera.getParameters().getSupportedPreviewSizes();
        while (!supportedPreviewSizes.isEmpty()) {
            int i = 0;
            int i2 = 0;
            for (int i3 = 0; i3 < supportedPreviewSizes.size(); i3++) {
                Camera.Size size = supportedPreviewSizes.get(i3);
                if (size.width * size.height > i) {
                    i = size.width * size.height;
                    i2 = i3;
                }
            }
            vector.add(new Point(supportedPreviewSizes.get(i2).width, supportedPreviewSizes.get(i2).height));
            supportedPreviewSizes.remove(i2);
        }
        return vector;
    }

    public static void a(Camera camera, int i) {
        try {
            if (f != null) {
                Camera.Parameters parameters = camera.getParameters();
                f.invoke(parameters, Integer.valueOf(i));
                camera.setParameters(parameters);
                Log.i("camera", "Set zoom: " + i);
            }
        } catch (Exception e2) {
        }
    }

    public static void a(com.androidillusion.b.a aVar, Camera camera, boolean z) {
        Camera.Parameters parameters = camera.getParameters();
        if (aVar.m) {
            if (z) {
                parameters.setFlashMode("torch");
            } else {
                parameters.setFlashMode("off");
            }
        } else if (aVar.n) {
            if (z) {
                a(true);
            } else {
                a(false);
            }
        }
        camera.setParameters(parameters);
    }

    private static void a(boolean z) {
        if (g != null) {
            a(h, g, Boolean.valueOf(z));
        }
    }

    private static boolean a(String str, List list) {
        return list != null && list.indexOf(str) >= 0;
    }

    private static Vector b(Camera camera) {
        Vector vector = new Vector();
        List<Camera.Size> supportedPictureSizes = camera.getParameters().getSupportedPictureSizes();
        while (!supportedPictureSizes.isEmpty()) {
            int i = 0;
            int i2 = 0;
            for (int i3 = 0; i3 < supportedPictureSizes.size(); i3++) {
                Camera.Size size = supportedPictureSizes.get(i3);
                if (size.width * size.height > i) {
                    i = size.width * size.height;
                    i2 = i3;
                }
            }
            vector.add(new Point(supportedPictureSizes.get(i2).width, supportedPictureSizes.get(i2).height));
            supportedPictureSizes.remove(i2);
        }
        return vector;
    }

    private static boolean c(Camera camera) {
        boolean z;
        try {
            if (c != null) {
                z = ((Boolean) c.invoke(camera.getParameters(), new Object[0])).booleanValue();
                Log.i("camera", "zoom available: " + z);
                return z;
            }
        } catch (Exception e2) {
        }
        z = false;
        Log.i("camera", "zoom available: " + z);
        return z;
    }

    private static int d(Camera camera) {
        try {
            if (d != null) {
                return ((Integer) d.invoke(camera.getParameters(), new Object[0])).intValue();
            }
        } catch (Exception e2) {
        }
        return 0;
    }

    private static List e(Camera camera) {
        try {
            if (e != null) {
                return (List) e.invoke(camera.getParameters(), new Object[0]);
            }
            return null;
        } catch (Exception e2) {
            return null;
        }
    }
}
