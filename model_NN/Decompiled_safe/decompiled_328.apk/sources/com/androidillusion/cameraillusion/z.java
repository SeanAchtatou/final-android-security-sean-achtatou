package com.androidillusion.cameraillusion;

import android.preference.Preference;
import com.androidillusion.b.a;

final class z implements Preference.OnPreferenceChangeListener {
    final /* synthetic */ SettingsActivity a;

    z(SettingsActivity settingsActivity) {
        this.a = settingsActivity;
    }

    public final boolean onPreferenceChange(Preference preference, Object obj) {
        int parseInt = Integer.parseInt((String) obj);
        ((a) this.a.c.c.elementAt(0)).c = parseInt;
        this.a.d.setSummary(this.a.e[parseInt]);
        return true;
    }
}
