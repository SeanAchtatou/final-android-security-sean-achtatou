package com.androidillusion.cameraillusion;

import android.content.Intent;
import android.view.View;

final class p implements View.OnClickListener {
    final /* synthetic */ CameraActivity a;

    p(CameraActivity cameraActivity) {
        this.a = cameraActivity;
    }

    public final void onClick(View view) {
        this.a.startActivity(new Intent(this.a, HelpActivity.class));
    }
}
