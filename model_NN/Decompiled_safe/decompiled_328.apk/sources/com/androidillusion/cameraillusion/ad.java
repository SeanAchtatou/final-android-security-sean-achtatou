package com.androidillusion.cameraillusion;

import android.content.Intent;
import android.preference.Preference;

final class ad implements Preference.OnPreferenceClickListener {
    final /* synthetic */ SettingsActivity a;

    ad(SettingsActivity settingsActivity) {
        this.a = settingsActivity;
    }

    public final boolean onPreferenceClick(Preference preference) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.putExtra("android.intent.extra.EMAIL", new String[]{this.a.getString(C0000R.string.config_feedback_summary)});
        intent.putExtra("android.intent.extra.SUBJECT", String.valueOf(this.a.getString(C0000R.string.app_name)) + " " + this.a.b);
        intent.setType("message/rfc822");
        this.a.startActivity(Intent.createChooser(intent, this.a.getString(C0000R.string.config_feedback_choose_email)));
        return true;
    }
}
