package com.androidillusion.cameraillusion;

import android.content.Context;
import android.view.OrientationEventListener;

final class s extends OrientationEventListener {
    final /* synthetic */ CameraActivity a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public s(CameraActivity cameraActivity, Context context) {
        super(context);
        this.a = cameraActivity;
    }

    public final void onOrientationChanged(int i) {
        int i2;
        if (i != -1) {
            this.a.z = (((i + 45) / 90) * 90) % 360;
            int b = this.a.z;
            switch (this.a.getWindowManager().getDefaultDisplay().getOrientation()) {
                case 0:
                    i2 = 0;
                    break;
                case 1:
                    i2 = 90;
                    break;
                case 2:
                    i2 = 180;
                    break;
                case 3:
                    i2 = 270;
                    break;
                default:
                    i2 = 0;
                    break;
            }
            int i3 = (b + i2) % 360;
            if (this.a.A != i3) {
                this.a.a(this.a.A, i3);
                this.a.A = i3;
            }
        }
    }
}
