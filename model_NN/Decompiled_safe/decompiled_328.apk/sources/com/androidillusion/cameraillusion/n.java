package com.androidillusion.cameraillusion;

import android.content.Intent;
import android.view.View;

final class n implements View.OnClickListener {
    final /* synthetic */ CameraActivity a;

    n(CameraActivity cameraActivity) {
        this.a = cameraActivity;
    }

    public final void onClick(View view) {
        if (!this.a.v) {
            Intent intent = new Intent(this.a.getApplicationContext(), ImageViewerActivity.class);
            intent.putExtra("uri", this.a.q.G);
            intent.putExtra("raw", this.a.q.H);
            this.a.startActivityForResult(intent, 1);
            return;
        }
        try {
            this.a.startActivity(new Intent("android.intent.action.VIEW", this.a.q.G));
        } catch (Exception e) {
        }
    }
}
