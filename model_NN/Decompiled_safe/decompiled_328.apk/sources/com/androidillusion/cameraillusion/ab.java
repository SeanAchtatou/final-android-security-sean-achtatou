package com.androidillusion.cameraillusion;

import android.preference.Preference;
import com.androidillusion.b.a;

final class ab implements Preference.OnPreferenceChangeListener {
    final /* synthetic */ SettingsActivity a;

    ab(SettingsActivity settingsActivity) {
        this.a = settingsActivity;
    }

    public final boolean onPreferenceChange(Preference preference, Object obj) {
        int parseInt = Integer.parseInt((String) obj);
        ((a) this.a.c.c.elementAt(1)).c = parseInt;
        this.a.h.setSummary(this.a.i[parseInt]);
        return true;
    }
}
