package com.androidillusion.cameraillusion;

import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.util.Log;
import com.androidillusion.b.f;
import com.androidillusion.e.a;

public class SettingsActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    public ListPreference a;
    String b;
    f c;
    ListPreference d;
    String[] e;
    ListPreference f;
    String[] g;
    ListPreference h;
    String[] i;
    ListPreference j;
    String[] k;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().setFlags(1024, 1024);
        addPreferencesFromResource(C0000R.layout.settings);
        try {
            this.b = getPackageManager().getPackageInfo(getPackageName(), 128).versionName;
            Log.i("camera", "Version " + this.b);
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
        }
        this.d = (ListPreference) getPreferenceScreen().getPreferenceManager().findPreference("resolution");
        Bundle extras = getIntent().getExtras();
        this.c = a.a(this, extras != null ? extras.getInt("width") : 0, extras != null ? extras.getInt("height") : 0);
        com.androidillusion.b.a aVar = (com.androidillusion.b.a) this.c.c.elementAt(0);
        this.e = new String[aVar.a.size()];
        String[] strArr = new String[this.e.length];
        for (int i2 = 0; i2 < this.e.length; i2++) {
            Point point = (Point) aVar.a.elementAt(i2);
            this.e[i2] = String.valueOf(point.x) + " x " + point.y;
            if (i2 == aVar.b) {
                this.e[i2] = String.valueOf(this.e[i2]) + " " + getString(C0000R.string.config_resolution_default);
            }
            strArr[i2] = new StringBuilder().append(i2).toString();
        }
        this.d.setEntries(this.e);
        this.d.setEntryValues(strArr);
        int intValue = Integer.valueOf(this.d.getValue()).intValue();
        if (intValue != -1) {
            aVar.c = intValue;
        } else {
            aVar.c = aVar.b;
        }
        this.d.setValueIndex(aVar.c);
        this.d.setSummary(this.e[aVar.c]);
        this.d.setOnPreferenceChangeListener(new z(this));
        this.f = (ListPreference) getPreferenceScreen().getPreferenceManager().findPreference("resolutionhd");
        this.g = new String[aVar.d.size()];
        String[] strArr2 = new String[this.g.length];
        for (int i3 = 0; i3 < this.g.length; i3++) {
            Point point2 = (Point) aVar.d.elementAt(i3);
            this.g[i3] = String.valueOf(point2.x) + " x " + point2.y;
            if (i3 == 0) {
                this.g[i3] = String.valueOf(this.g[i3]) + " " + getString(C0000R.string.config_resolution_default);
            }
            strArr2[i3] = new StringBuilder().append(i3).toString();
        }
        this.f.setEntries(this.g);
        this.f.setEntryValues(strArr2);
        int intValue2 = Integer.valueOf(this.f.getValue()).intValue();
        if (intValue2 != -1) {
            aVar.e = intValue2;
        }
        this.f.setValueIndex(aVar.e);
        this.f.setSummary(this.g[aVar.e]);
        this.f.setOnPreferenceChangeListener(new aa(this));
        if (this.c.a > 1) {
            this.h = (ListPreference) getPreferenceScreen().getPreferenceManager().findPreference("resolution2");
            com.androidillusion.b.a aVar2 = (com.androidillusion.b.a) this.c.c.elementAt(1);
            this.i = new String[aVar2.a.size()];
            String[] strArr3 = new String[this.i.length];
            for (int i4 = 0; i4 < this.i.length; i4++) {
                Point point3 = (Point) aVar2.a.elementAt(i4);
                this.i[i4] = String.valueOf(point3.x) + " x " + point3.y;
                if (i4 == aVar2.b) {
                    this.i[i4] = String.valueOf(this.i[i4]) + " " + getString(C0000R.string.config_resolution_default);
                }
                strArr3[i4] = new StringBuilder().append(i4).toString();
            }
            this.h.setEntries(this.i);
            this.h.setEntryValues(strArr3);
            int intValue3 = Integer.valueOf(this.h.getValue()).intValue();
            if (intValue3 != -1) {
                aVar2.c = intValue3;
            } else {
                aVar2.c = aVar2.b;
            }
            this.h.setValueIndex(aVar2.c);
            this.h.setSummary(this.i[aVar2.c]);
            this.h.setOnPreferenceChangeListener(new ab(this));
            this.j = (ListPreference) getPreferenceScreen().getPreferenceManager().findPreference("resolutionhd2");
            this.k = new String[aVar2.d.size()];
            String[] strArr4 = new String[this.k.length];
            for (int i5 = 0; i5 < this.k.length; i5++) {
                Point point4 = (Point) aVar2.d.elementAt(i5);
                this.k[i5] = String.valueOf(point4.x) + " x " + point4.y;
                if (i5 == 0) {
                    this.k[i5] = String.valueOf(this.k[i5]) + " " + getString(C0000R.string.config_resolution_default);
                }
                strArr4[i5] = new StringBuilder().append(i5).toString();
            }
            this.j.setEntries(this.k);
            this.j.setEntryValues(strArr4);
            int intValue4 = Integer.valueOf(this.j.getValue()).intValue();
            if (intValue4 != -1) {
                aVar2.e = intValue4;
            }
            this.j.setValueIndex(aVar2.e);
            this.j.setSummary(this.k[aVar2.e]);
            this.j.setOnPreferenceChangeListener(new ac(this));
            this.d.setTitle(getString(C0000R.string.config_preview_resolution_rear));
            this.f.setTitle(getString(C0000R.string.config_hd_resolution_rear));
        } else {
            PreferenceCategory preferenceCategory = (PreferenceCategory) findPreference("cam");
            preferenceCategory.removePreference(findPreference("resolution2"));
            preferenceCategory.removePreference(findPreference("resolutionhd2"));
        }
        this.a = (ListPreference) getPreferenceScreen().getPreferenceManager().findPreference("videoformat");
        this.a.setSummary(this.a.getEntry());
        getPreferenceScreen().getPreferenceManager().findPreference("feedback").setOnPreferenceClickListener(new ad(this));
        getPreferenceScreen().getPreferenceManager().findPreference("version").setSummary(String.valueOf(getString(C0000R.string.app_name)) + " " + this.b);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        if (str.equals("videoformat")) {
            this.a.setSummary(this.a.getEntry());
            this.c.d = Integer.valueOf(this.a.getValue().toString()).intValue();
        }
    }
}
