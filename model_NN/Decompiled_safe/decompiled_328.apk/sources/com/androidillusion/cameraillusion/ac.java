package com.androidillusion.cameraillusion;

import android.preference.Preference;
import com.androidillusion.b.a;

final class ac implements Preference.OnPreferenceChangeListener {
    final /* synthetic */ SettingsActivity a;

    ac(SettingsActivity settingsActivity) {
        this.a = settingsActivity;
    }

    public final boolean onPreferenceChange(Preference preference, Object obj) {
        int parseInt = Integer.parseInt((String) obj);
        ((a) this.a.c.c.elementAt(1)).e = parseInt;
        this.a.j.setSummary(this.a.k[parseInt]);
        return true;
    }
}
