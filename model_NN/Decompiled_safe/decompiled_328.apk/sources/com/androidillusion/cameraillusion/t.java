package com.androidillusion.cameraillusion;

import android.view.View;

final class t implements View.OnClickListener {
    final /* synthetic */ ImageViewerActivity a;

    t(ImageViewerActivity imageViewerActivity) {
        this.a = imageViewerActivity;
    }

    public final void onClick(View view) {
        if (this.a.g.getVisibility() == 0) {
            this.a.g.setVisibility(4);
            this.a.i.setVisibility(4);
            return;
        }
        this.a.g.setVisibility(0);
        this.a.i.setVisibility(0);
    }
}
