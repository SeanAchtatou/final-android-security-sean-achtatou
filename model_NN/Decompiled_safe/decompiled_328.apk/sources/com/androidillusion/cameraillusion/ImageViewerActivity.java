package com.androidillusion.cameraillusion;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.androidillusion.e.b;
import com.androidillusion.e.e;
import java.io.File;
import java.text.DecimalFormat;

public class ImageViewerActivity extends Activity {
    Uri a;
    String b;
    String c;
    Bitmap d;
    int e;
    int f;
    /* access modifiers changed from: private */
    public LinearLayout g;
    private FrameLayout h;
    /* access modifiers changed from: private */
    public LinearLayout i;
    private FrameLayout j;
    private FrameLayout k;
    private FrameLayout l;

    static /* synthetic */ void a(ImageViewerActivity imageViewerActivity, Uri uri) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.SEND");
        intent.setType("image/jpeg");
        intent.putExtra("android.intent.extra.STREAM", uri);
        try {
            imageViewerActivity.startActivity(Intent.createChooser(intent, imageViewerActivity.getString(C0000R.string.image_viewer_dialog_share)));
        } catch (ActivityNotFoundException e2) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    static /* synthetic */ void b(ImageViewerActivity imageViewerActivity, Uri uri, String str) {
        File file = new File(str);
        try {
            if (file.exists()) {
                file.delete();
                Log.i("camera", "Delete " + str);
            }
        } catch (Exception e2) {
            Log.i("camera", "Delete error");
        }
        try {
            imageViewerActivity.getContentResolver().delete(uri, null, null);
        } catch (Exception e3) {
            Log.i("camera", "Excepcion ContentResolver " + e3.getMessage());
        }
        Intent intent = new Intent();
        intent.putExtra("delete", true);
        imageViewerActivity.setResult(-1, intent);
        imageViewerActivity.finish();
    }

    public final void a() {
        Intent intent = new Intent(this, FullImageViewerActivity.class);
        intent.putExtra("uri", this.b);
        intent.putExtra("raw", this.c);
        startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.androidillusion.e.b.a(java.lang.String, int, int, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int, int, int]
     candidates:
      com.androidillusion.e.b.a(android.content.Context, android.graphics.Bitmap, int, java.lang.String):android.net.Uri
      com.androidillusion.e.b.a(java.lang.String, int, int, boolean):android.graphics.Bitmap */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        e.a(this);
        Display defaultDisplay = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        if (getResources().getConfiguration().orientation == 1) {
            this.e = Math.min(defaultDisplay.getWidth(), defaultDisplay.getHeight());
            this.f = Math.max(defaultDisplay.getWidth(), defaultDisplay.getHeight());
        } else {
            this.e = Math.max(defaultDisplay.getWidth(), defaultDisplay.getHeight());
            this.f = Math.min(defaultDisplay.getWidth(), defaultDisplay.getHeight());
        }
        Bundle extras = getIntent().getExtras();
        this.a = (Uri) (extras != null ? extras.getParcelable("uri") : null);
        this.b = b.a(this, this.a);
        this.c = extras != null ? extras.getString("raw") : null;
        setContentView((int) C0000R.layout.image_viewer);
        this.g = (LinearLayout) findViewById(C0000R.id.layout1);
        this.h = (FrameLayout) findViewById(C0000R.id.layout1_3);
        this.i = (LinearLayout) findViewById(C0000R.id.layout2);
        this.j = (FrameLayout) findViewById(C0000R.id.layout2_1);
        this.k = (FrameLayout) findViewById(C0000R.id.layout2_2);
        this.l = (FrameLayout) findViewById(C0000R.id.layout2_3);
        ImageView imageView = (ImageView) findViewById(C0000R.id.imagefull);
        try {
            Object lastNonConfigurationInstance = getLastNonConfigurationInstance();
            if (lastNonConfigurationInstance != null) {
                this.d = (Bitmap) lastNonConfigurationInstance;
            } else {
                this.d = b.a(this.b, Math.max(this.e, this.f), Math.max(this.e, this.f), true);
            }
            if (this.d != null) {
                imageView.setImageBitmap(this.d);
            }
            imageView.setOnClickListener(new t(this));
            this.j.setOnClickListener(new u(this));
            this.k.setOnClickListener(new v(this));
            this.l.setOnClickListener(new w(this));
            String str = this.b;
            DecimalFormat decimalFormat = new DecimalFormat("###.#");
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(str, options);
            ((TextView) findViewById(C0000R.id.resolution)).setText(String.valueOf(options.outWidth) + "x" + options.outHeight + "\n(" + decimalFormat.format((((double) options.outWidth) * ((double) options.outHeight)) / 1000000.0d) + " Mpx)");
            this.h.setOnClickListener(new x(this));
            BitmapFactory.Options options2 = new BitmapFactory.Options();
            options2.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(this.b, options2);
            int min = Math.min(options2.outWidth, options2.outHeight);
            int max = Math.max(this.e, this.f);
            if (this.c == null || min < max) {
                this.h.setVisibility(4);
            }
        } catch (Error e2) {
            Log.i("camera", "MEM ERROR VIEW, EXIT");
            finish();
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 84 || this.h.getVisibility() != 0) {
            return super.onKeyDown(i2, keyEvent);
        }
        a();
        return true;
    }

    public Object onRetainNonConfigurationInstance() {
        return this.d;
    }
}
