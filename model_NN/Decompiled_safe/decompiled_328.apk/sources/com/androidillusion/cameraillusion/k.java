package com.androidillusion.cameraillusion;

import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import com.androidillusion.algorithm.Image;
import com.androidillusion.b.b;

final class k extends Handler {
    final /* synthetic */ CameraActivity a;

    k(CameraActivity cameraActivity) {
        this.a = cameraActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.androidillusion.e.b.a(java.lang.String, int, int, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int, int, int]
     candidates:
      com.androidillusion.e.b.a(android.content.Context, android.graphics.Bitmap, int, java.lang.String):android.net.Uri
      com.androidillusion.e.b.a(java.lang.String, int, int, boolean):android.graphics.Bitmap */
    public final void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                switch (message.arg1) {
                    case 0:
                        this.a.k.a(0, this.a.q.z, this.a.getString(C0000R.string.select_filter));
                        break;
                    case 1:
                        this.a.k.a(1, this.a.q.A, this.a.getString(C0000R.string.select_effect));
                        break;
                    case 2:
                        this.a.k.a(2, this.a.q.B, this.a.getString(C0000R.string.select_mask));
                        break;
                }
                this.a.q.P = true;
                this.a.k.show();
                return;
            case 1:
                switch (message.arg1) {
                    case 0:
                        this.a.q.z = message.arg2;
                        break;
                    case 1:
                        this.a.q.A = message.arg2;
                        Image.c = true;
                        break;
                    case 2:
                        this.a.q.B = message.arg2;
                        this.a.q.j = false;
                        break;
                }
                this.a.a();
                this.a.k.cancel();
                return;
            case 2:
                switch (message.arg1) {
                    case 0:
                        this.a.j.a(this.a.q.a(0));
                        break;
                    case 1:
                        this.a.j.a(this.a.q.a(1));
                        break;
                    case 2:
                        this.a.j.a(this.a.q.a(2));
                        break;
                }
                this.a.a(2);
                return;
            case 3:
                this.a.q.z = 0;
                this.a.q.A = 0;
                this.a.q.B = 0;
                this.a.a();
                return;
            case 4:
                this.a.a(1);
                return;
            case 5:
                int i = this.a.a / 7;
                String str = "";
                if (this.a.g) {
                    StringBuilder sb = new StringBuilder(String.valueOf(str));
                    CameraActivity cameraActivity = this.a;
                    b bVar = this.a.q;
                    str = sb.append(CameraActivity.d(cameraActivity, b.a(this.a.q.a(0).b, this.a.q.a(1).b, this.a.q.a(2).b))).toString();
                }
                this.a.a(String.valueOf(str) + this.a.getString(C0000R.string.misc_photo_stored_in) + ' ' + com.androidillusion.e.b.a(this.a, this.a.q.G));
                this.a.F.setImageBitmap(com.androidillusion.e.b.a(com.androidillusion.e.b.a(this.a, this.a.q.G), i, i, false));
                this.a.F.setVisibility(0);
                this.a.a(this.a.A, this.a.A);
                return;
            case 6:
                this.a.getWindow().addFlags(128);
                this.a.m.b.d.setText(this.a.getString(C0000R.string.misc_popup_processing));
                this.a.m.show();
                return;
            case 7:
                this.a.getWindow().clearFlags(128);
                this.a.m.cancel();
                return;
            case 8:
                this.a.m.a(this.a.q.I);
                this.a.m.b.invalidate();
                return;
            case 9:
                this.a.finish();
                return;
            case 10:
            case 11:
            default:
                return;
            case 12:
                this.a.b(this.a.q.v.a().k);
                this.a.R.setText(this.a.q.v.a().c());
                this.a.q.invalidate();
                return;
            case 13:
                this.a.j.n.setProgress(50);
                this.a.j.o.setProgress(50);
                this.a.j.p.setProgress(50);
                return;
            case 14:
                Log.i("camera", "requesting exit");
                this.a.m.b.d.setText(this.a.getString(C0000R.string.misc_popup_cancelling));
                this.a.m.b.postInvalidate();
                this.a.q.i = true;
                return;
            case 15:
                try {
                    this.a.a(message.getData().getString("msg"));
                    return;
                } catch (Exception e) {
                    return;
                }
            case 16:
                this.a.a();
                return;
            case 17:
                Log.i("camera", "Exit ->restore config");
                SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.a);
                if (defaultSharedPreferences != null) {
                    SharedPreferences.Editor edit = defaultSharedPreferences.edit();
                    edit.putString("resolution", "-1");
                    edit.putString("resolutionhd", "-1");
                    edit.putString("resolution2", "-1");
                    edit.putString("resolutionhd2", "-1");
                    edit.commit();
                }
                this.a.d.setDuration(1);
                this.a.a("MEMORY ERROR\nPlease contact camera.illusion@gmail.com to fix the problem");
                this.a.finish();
                return;
        }
    }
}
