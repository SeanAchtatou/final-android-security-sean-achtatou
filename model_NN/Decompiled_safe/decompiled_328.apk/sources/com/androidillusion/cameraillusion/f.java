package com.androidillusion.cameraillusion;

import android.view.View;

final class f implements View.OnClickListener {
    final /* synthetic */ CameraActivity a;

    f(CameraActivity cameraActivity) {
        this.a = cameraActivity;
    }

    public final void onClick(View view) {
        if (!this.a.q.v.a().f) {
            this.a.a(this.a.getString(C0000R.string.error_no_zoom));
            return;
        }
        this.a.h = !this.a.h;
        if (this.a.h) {
            this.a.O.setBackgroundResource(C0000R.drawable.zoom_selected);
            this.a.R.setVisibility(0);
            this.a.P.setVisibility(0);
            this.a.Q.setVisibility(0);
            this.a.a(this.a.A, this.a.A);
            return;
        }
        this.a.O.setBackgroundResource(C0000R.drawable.zoom_unpressed);
        this.a.R.clearAnimation();
        this.a.P.clearAnimation();
        this.a.Q.clearAnimation();
        this.a.R.setVisibility(4);
        this.a.Q.setVisibility(4);
        this.a.P.setVisibility(4);
    }
}
