package com.androidillusion.cameraillusion;

import android.hardware.Camera;

final class a implements Camera.AutoFocusCallback {
    final /* synthetic */ CameraActivity a;

    a(CameraActivity cameraActivity) {
        this.a = cameraActivity;
    }

    public final void onAutoFocus(boolean z, Camera camera) {
        this.a.q.J = this.a.g;
        this.a.q.K = this.a.A;
        this.a.q.h = this.a.s;
        this.a.q.F = true;
        this.a.q.g = false;
    }
}
