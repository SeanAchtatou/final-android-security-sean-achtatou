package com.androidillusion.cameraillusion;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.androidillusion.e.e;
import com.androidillusion.f.a;
import com.androidillusion.f.c;
import com.androidillusion.f.f;
import com.androidillusion.f.v;
import com.androidillusion.f.y;
import com.google.ads.AdView;
import com.google.ads.b;
import java.lang.reflect.Method;
import java.util.Random;

public class CameraActivity extends Activity implements b {
    /* access modifiers changed from: private */
    public int A = 0;
    private View B;
    private View C;
    private Button D;
    private Button E;
    /* access modifiers changed from: private */
    public ImageButton F;
    private View G;
    private Button H;
    private Button I;
    private Button J;
    private Button K;
    private Button L;
    private Button M;
    private Button N;
    /* access modifiers changed from: private */
    public Button O;
    /* access modifiers changed from: private */
    public Button P;
    /* access modifiers changed from: private */
    public Button Q;
    /* access modifiers changed from: private */
    public TextView R;
    private Button S;
    private Button T;
    /* access modifiers changed from: private */
    public Random U = new Random();
    private boolean V;
    int a;
    public int b;
    LayoutInflater c;
    Toast d;
    View e;
    public TextView f;
    boolean g;
    boolean h;
    Handler i;
    f j;
    y k;
    c l;
    v m;
    a n;
    AdView o;
    public int p;
    com.androidillusion.b.b q;
    public boolean r;
    boolean s;
    boolean t;
    boolean u;
    boolean v;
    boolean w;
    Camera.AutoFocusCallback x = new a(this);
    private s y;
    /* access modifiers changed from: private */
    public int z = -1;

    /* access modifiers changed from: private */
    public void a(int i2) {
        this.p = i2;
        switch (this.p) {
            case 0:
                if (com.androidillusion.c.a.a) {
                    this.H.clearAnimation();
                    this.H.setVisibility(4);
                } else {
                    this.H.setVisibility(0);
                    this.n.setVisibility(0);
                }
                this.I.setVisibility(0);
                this.J.setVisibility(0);
                this.K.setVisibility(0);
                this.L.setVisibility(0);
                if (this.q.b() > 1) {
                    this.M.setVisibility(0);
                } else {
                    this.M.clearAnimation();
                    this.M.setVisibility(4);
                }
                this.O.setVisibility(0);
                if (this.h) {
                    this.R.setVisibility(0);
                    this.P.setVisibility(0);
                    this.Q.setVisibility(0);
                } else {
                    this.R.clearAnimation();
                    this.R.setVisibility(4);
                    this.P.clearAnimation();
                    this.P.setVisibility(4);
                    this.Q.clearAnimation();
                    this.Q.setVisibility(4);
                }
                this.N.setVisibility(0);
                this.S.setVisibility(0);
                this.T.setVisibility(0);
                this.j.clearAnimation();
                this.j.setVisibility(4);
                break;
            case 1:
                this.j.b();
                this.H.clearAnimation();
                this.H.setVisibility(4);
                this.I.clearAnimation();
                this.I.setVisibility(4);
                this.J.clearAnimation();
                this.J.setVisibility(4);
                this.K.clearAnimation();
                this.K.setVisibility(4);
                this.L.clearAnimation();
                this.L.setVisibility(4);
                this.M.clearAnimation();
                this.M.setVisibility(4);
                this.O.clearAnimation();
                this.O.setVisibility(4);
                this.R.clearAnimation();
                this.R.setVisibility(4);
                this.P.clearAnimation();
                this.P.setVisibility(4);
                this.Q.clearAnimation();
                this.Q.setVisibility(4);
                this.N.clearAnimation();
                this.N.setVisibility(4);
                this.S.clearAnimation();
                this.S.setVisibility(4);
                this.T.clearAnimation();
                this.T.setVisibility(4);
                this.j.setVisibility(0);
                if (this.n != null) {
                    this.n.setVisibility(4);
                    break;
                }
                break;
            case 2:
                this.j.a();
                break;
        }
        a(this.A, this.A);
    }

    /* access modifiers changed from: private */
    public void a(boolean z2) {
        this.g = z2;
        if (this.g) {
            this.T.setBackgroundResource(C0000R.drawable.hd_selected);
        } else {
            this.T.setBackgroundResource(C0000R.drawable.hd_unselected);
        }
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        this.q.v.a().k = i2;
        switch (i2) {
            case 0:
                this.N.setBackgroundResource(C0000R.drawable.flash_off);
                return;
            case 1:
                this.N.setBackgroundResource(C0000R.drawable.flash_torch);
                return;
            case 2:
                this.N.setBackgroundResource(C0000R.drawable.flash_on);
                return;
            case 3:
                this.N.setBackgroundResource(C0000R.drawable.flash_auto);
                return;
            default:
                return;
        }
    }

    static /* synthetic */ String d(CameraActivity cameraActivity, int i2) {
        switch (i2) {
            case 0:
                return "";
            case 1:
                return String.valueOf(cameraActivity.getString(C0000R.string.error_no_HD_mode)) + ' ' + cameraActivity.getString(C0000R.string.error_no_HD_with_selected_filter) + ' ' + cameraActivity.q.a(0).c + '.' + 10 + 10;
            case 2:
                return String.valueOf(cameraActivity.getString(C0000R.string.error_no_HD_mode)) + ' ' + cameraActivity.getString(C0000R.string.error_no_HD_with_selected_effect) + ' ' + cameraActivity.q.a(1).c + '.' + 10 + 10;
            case 3:
                return String.valueOf(cameraActivity.getString(C0000R.string.error_no_HD_mode)) + ' ' + cameraActivity.getString(C0000R.string.error_no_HD_mask_selected) + 10 + 10;
            default:
                return "";
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        if (!com.androidillusion.e.b.a()) {
            a(getString(C0000R.string.error_SD_not_found));
        } else if (!this.u) {
            this.q.J = this.g;
            this.q.K = this.A;
            this.q.h = this.s;
            this.q.F = true;
        } else if (!this.q.g && !this.q.F) {
            this.q.g = true;
            this.q.r.autoFocus(this.x);
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        this.q.a();
        Intent intent = new Intent(this, SettingsActivity.class);
        intent.putExtra("width", this.a);
        intent.putExtra("height", this.b);
        startActivity(intent);
    }

    public final void a() {
        com.androidillusion.d.a a2 = this.q.a(0);
        this.j.b.setText(a2.c);
        if (a2.e == 0) {
            this.j.c.setVisibility(4);
        } else {
            this.j.c.setVisibility(0);
        }
        com.androidillusion.d.a a3 = this.q.a(1);
        this.j.d.setText(a3.c);
        if (a3.e == 0) {
            this.j.e.setVisibility(4);
        } else {
            this.j.e.setVisibility(0);
        }
        com.androidillusion.d.a a4 = this.q.a(2);
        this.j.f.setText(a4.c);
        if (a4.e == 0) {
            this.j.g.setVisibility(4);
        } else {
            this.j.g.setVisibility(0);
        }
        this.j.postInvalidate();
    }

    public final void a(int i2, int i3) {
        int i4;
        int i5;
        if (this.V) {
            if (this.o != null) {
                this.n.a(i3, this.r);
                this.n.postInvalidate();
            }
            if (this.k.b != null) {
                this.k.b.b(i3);
                this.k.b.postInvalidate();
            }
            if (this.l.b != null) {
                this.l.b.b(i3);
                this.l.b.postInvalidate();
            }
            if (this.m.b != null) {
                this.m.b.b(i3);
                this.m.b.postInvalidate();
            }
            if (this.j != null) {
                this.j.a(i3);
                this.j.postInvalidate();
            }
            if (this.q != null) {
                this.q.b(i3);
            }
            int i6 = i3 - i2;
            if (i6 > 180) {
                i4 = i3 - 360;
                i5 = i2;
            } else if (i6 < -180) {
                i4 = i3;
                i5 = i2 - 360;
            } else {
                i4 = i3;
                i5 = i2;
            }
            RotateAnimation rotateAnimation = new RotateAnimation((float) (-i5), (float) (-i4), 1, 0.5f, 1, 0.5f);
            rotateAnimation.setDuration(300);
            rotateAnimation.setFillAfter(true);
            RotateAnimation rotateAnimation2 = new RotateAnimation((float) (-i5), (float) (-i4), 1, 0.5f, 1, 0.5f);
            rotateAnimation2.setDuration(5);
            rotateAnimation2.setFillAfter(true);
            RotateAnimation rotateAnimation3 = new RotateAnimation((float) (-i5), (float) (-i4), 1, 0.5f, 1, 0.5f);
            rotateAnimation3.setDuration(0);
            rotateAnimation3.setFillAfter(true);
            if (this.D.getVisibility() == 0) {
                this.D.startAnimation(rotateAnimation);
            }
            if (this.E.getVisibility() == 0) {
                this.E.startAnimation(rotateAnimation);
            }
            if (this.F.getVisibility() == 0) {
                this.F.startAnimation(rotateAnimation);
            }
            if ((i4 == 90 || i4 == -90 || i4 == 270) && !com.androidillusion.c.a.a && this.r) {
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.G.getLayoutParams();
                layoutParams.leftMargin = (int) (60.0f * getResources().getDisplayMetrics().density);
                this.G.setLayoutParams(layoutParams);
            } else if (!com.androidillusion.c.a.a) {
                LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) this.G.getLayoutParams();
                layoutParams2.leftMargin = 0;
                this.G.setLayoutParams(layoutParams2);
            }
            if (this.H.getVisibility() == 0) {
                this.H.startAnimation(rotateAnimation3);
            }
            if (this.I.getVisibility() == 0) {
                this.I.startAnimation(rotateAnimation3);
            }
            if (this.J.getVisibility() == 0) {
                this.J.startAnimation(rotateAnimation3);
            }
            if (this.K.getVisibility() == 0) {
                this.K.startAnimation(rotateAnimation3);
            }
            if (this.L.getVisibility() == 0) {
                this.L.startAnimation(rotateAnimation3);
            }
            if (this.M.getVisibility() == 0) {
                this.M.startAnimation(rotateAnimation3);
            }
            if (this.O.getVisibility() == 0) {
                this.O.startAnimation(rotateAnimation3);
            }
            if (this.R.getVisibility() == 0) {
                this.R.startAnimation(rotateAnimation2);
            }
            if (this.P.getVisibility() == 0) {
                this.P.startAnimation(rotateAnimation3);
            }
            if (this.Q.getVisibility() == 0) {
                this.Q.startAnimation(rotateAnimation3);
            }
            if (this.N.getVisibility() == 0) {
                this.N.startAnimation(rotateAnimation3);
            }
            if (this.S.getVisibility() == 0) {
                this.S.startAnimation(rotateAnimation3);
            }
            if (this.T.getVisibility() == 0) {
                this.T.startAnimation(rotateAnimation3);
            }
        }
    }

    public final void a(String str) {
        this.f.setText(str);
        RotateAnimation rotateAnimation = new RotateAnimation(0.0f, (float) (-this.A), 1, 0.5f, 1, 0.5f);
        rotateAnimation.setDuration(0);
        rotateAnimation.setFillAfter(true);
        this.f.setAnimation(rotateAnimation);
        this.d.show();
    }

    public final void b() {
        this.q.a();
    }

    public final void c() {
        this.r = true;
        a(this.A, this.A);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 != -1) {
            Log.i("camera", "onActivityResult:RESULT_NOK");
        } else if (i2 == 1) {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if ((extras != null ? Boolean.valueOf(extras.getBoolean("delete")) : null).booleanValue()) {
                this.F.clearAnimation();
                this.F.setVisibility(4);
            }
        }
    }

    public void onCreate(Bundle bundle) {
        Method method;
        super.onCreate(bundle);
        this.V = false;
        this.r = false;
        e.a(this);
        Display defaultDisplay = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        this.a = Math.max(defaultDisplay.getWidth(), defaultDisplay.getHeight());
        this.b = Math.min(defaultDisplay.getWidth(), defaultDisplay.getHeight());
        this.y = new s(this, this);
        this.i = new k(this);
        this.c = (LayoutInflater) getSystemService("layout_inflater");
        this.d = Toast.makeText(getApplicationContext(), "", 0);
        this.e = this.c.inflate((int) C0000R.layout.toast, (ViewGroup) null);
        this.e.setBackgroundDrawable(getResources().getDrawable(C0000R.drawable.dialog));
        this.d.setView(this.e);
        this.f = (TextView) this.e.findViewById(C0000R.id.textView1);
        this.d.setGravity(17, 0, 0);
        this.q = new com.androidillusion.b.b(getApplicationContext(), this.i, this.a, this.b);
        this.B = this.c.inflate((int) C0000R.layout.camera, (ViewGroup) null);
        this.C = this.c.inflate((int) C0000R.layout.camera_left, (ViewGroup) null);
        this.G = (LinearLayout) this.C.findViewById(C0000R.id.layout1);
        this.D = (Button) this.B.findViewById(C0000R.id.shotButton);
        this.E = (Button) this.B.findViewById(C0000R.id.illusionButton);
        this.F = (ImageButton) this.B.findViewById(C0000R.id.thumbButton);
        this.H = (Button) this.C.findViewById(C0000R.id.proButton);
        this.I = (Button) this.C.findViewById(C0000R.id.settingsButton);
        this.J = (Button) this.C.findViewById(C0000R.id.infoButton);
        this.K = (Button) this.C.findViewById(C0000R.id.photoButton);
        this.L = (Button) this.C.findViewById(C0000R.id.videocamButton);
        this.M = (Button) this.B.findViewById(C0000R.id.changeCameraButton);
        this.O = (Button) this.B.findViewById(C0000R.id.zoomButton);
        this.R = (TextView) this.B.findViewById(C0000R.id.zoomRatio);
        this.P = (Button) this.B.findViewById(C0000R.id.zoomInButton);
        this.Q = (Button) this.B.findViewById(C0000R.id.zoomOutButton);
        this.N = (Button) this.B.findViewById(C0000R.id.flashButton);
        this.S = (Button) this.B.findViewById(C0000R.id.diceButton);
        this.T = (Button) this.B.findViewById(C0000R.id.hdButton);
        this.F.setVisibility(4);
        if (com.androidillusion.c.a.a) {
            this.H.setVisibility(4);
        }
        if (this.q.b() == 1) {
            this.M.setVisibility(4);
        }
        this.R.setVisibility(4);
        this.P.setVisibility(4);
        this.Q.setVisibility(4);
        this.h = false;
        this.g = false;
        this.D.setOnClickListener(new m(this));
        this.F.setOnClickListener(new n(this));
        this.E.setOnClickListener(new o(this));
        this.J.setOnClickListener(new p(this));
        this.I.setOnClickListener(new q(this));
        this.H.setOnClickListener(new r(this));
        this.K.setOnClickListener(new b(this));
        this.L.setOnClickListener(new c(this));
        this.M.setOnClickListener(new d(this));
        this.N.setOnClickListener(new e(this));
        this.O.setOnClickListener(new f(this));
        this.R.setText(this.q.v.a().c());
        this.P.setOnClickListener(new g(this));
        this.Q.setOnClickListener(new h(this));
        this.S.setOnClickListener(new i(this));
        this.T.setOnClickListener(new j(this));
        this.j = new f(this, this.i, this.a, this.b);
        this.j.a((LinearLayout) this.c.inflate((int) C0000R.layout.mode_illusion, (ViewGroup) null), (LinearLayout) this.c.inflate((int) C0000R.layout.mode_illusion_config, (ViewGroup) null));
        this.k = new y(this, this.i, this.a, this.b, this.q.w, this.q.x, this.q.y);
        this.k.setOnDismissListener(new l(this));
        Handler handler = this.i;
        this.l = new c(this, this.a, this.b);
        this.m = new v(this, this.i, this.a, this.b);
        if (!com.androidillusion.c.a.a) {
            if (!((getResources().getConfiguration().screenLayout & 4) == 4)) {
                this.o = new AdView(this, com.google.ads.f.a, "a14b3ecec82f73f");
            } else {
                this.o = new AdView(this, com.google.ads.f.c, "a14b3ecec82f73f");
            }
            this.o.a(this);
            this.o.setGravity(49);
            this.o.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            this.n = new a(this, this.a, this.b);
            this.n.a(this.o);
            this.o.a(new com.google.ads.c());
        }
        try {
            method = Camera.class.getMethod("addCallbackBuffer", new byte[1].getClass());
        } catch (NoSuchMethodException e2) {
            method = null;
        }
        if (method == null) {
            this.d.setDuration(1);
            a("DEVICE NOT COMPATIBLE\nPlease contact camera.illusion@gmail.com to fix the problem");
            finish();
        }
        setContentView(this.q);
        addContentView(this.q.c, new ViewGroup.LayoutParams(-1, -1));
        if (!com.androidillusion.c.a.a) {
            addContentView(this.n, new ViewGroup.LayoutParams(-1, -1));
        }
        addContentView(this.j, new ViewGroup.LayoutParams(-1, -1));
        addContentView(this.C, new ViewGroup.LayoutParams(-1, -1));
        addContentView(this.B, new ViewGroup.LayoutParams(-1, -1));
        a();
        a(0);
        this.V = true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        e();
        return false;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.q != null) {
            this.q.a();
        }
        com.androidillusion.e.b.c(String.valueOf(com.androidillusion.e.b.b()) + com.androidillusion.c.a.d);
        if (this.o != null) {
            this.o.a();
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            switch (this.p) {
                case 1:
                    a(0);
                    return true;
                case 2:
                    a(1);
                    return true;
            }
        } else if ((this.t && (i2 == 25 || i2 == 24)) || i2 == 27) {
            d();
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if ((!this.t || (i2 != 25 && i2 != 24)) && i2 != 27) {
            return super.onKeyUp(i2, keyEvent);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.q.i = true;
        SharedPreferences.Editor edit = getSharedPreferences("CameraIllusion2.0.1", 0).edit();
        edit.putInt("SELECTED_FILTER", this.q.z);
        edit.putInt("SELECTED_EFFECT", this.q.A);
        edit.putInt("SELECTED_MASK", this.q.B);
        edit.putBoolean("SELECTED_HD", this.g);
        edit.putInt("SELECTED_FLASH", this.q.v.a().k);
        edit.commit();
        if (this.y != null) {
            this.y.disable();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.q.F = false;
        this.q.g = false;
        if (this.y != null) {
            this.y.enable();
        }
        SharedPreferences sharedPreferences = getSharedPreferences("CameraIllusion2.0.1", 0);
        if (sharedPreferences.getBoolean("firstTime", true)) {
            Log.i("camera", "Erase settings");
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putBoolean("firstTime", false);
            edit.commit();
            SharedPreferences.Editor edit2 = PreferenceManager.getDefaultSharedPreferences(this).edit();
            edit2.clear();
            edit2.commit();
        }
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (defaultSharedPreferences != null) {
            int intValue = Integer.valueOf(defaultSharedPreferences.getString("resolution", "-1")).intValue();
            if (intValue != -1) {
                ((com.androidillusion.b.a) this.q.v.c.elementAt(0)).c = intValue;
            }
            int intValue2 = Integer.valueOf(defaultSharedPreferences.getString("resolutionhd", "-1")).intValue();
            if (intValue2 != -1) {
                ((com.androidillusion.b.a) this.q.v.c.elementAt(0)).e = intValue2;
            }
            if (this.q.v.a > 1) {
                int intValue3 = Integer.valueOf(defaultSharedPreferences.getString("resolution2", "-1")).intValue();
                if (intValue3 != -1) {
                    ((com.androidillusion.b.a) this.q.v.c.elementAt(1)).c = intValue3;
                }
                int intValue4 = Integer.valueOf(defaultSharedPreferences.getString("resolutionhd2", "-1")).intValue();
                if (intValue4 != -1) {
                    ((com.androidillusion.b.a) this.q.v.c.elementAt(1)).e = intValue4;
                }
            }
            this.q.c.a(Integer.valueOf(defaultSharedPreferences.getString("videoformat", "0")).intValue());
            this.q.M = defaultSharedPreferences.getBoolean("shutter", true);
            this.t = defaultSharedPreferences.getBoolean("volume", true);
            this.u = defaultSharedPreferences.getBoolean("autofocus1", true);
            this.s = defaultSharedPreferences.getBoolean("saveoriginal", false);
            this.v = defaultSharedPreferences.getBoolean("externalviewer", false);
            this.q.L = defaultSharedPreferences.getBoolean("autofocustap", true);
            this.q.N = defaultSharedPreferences.getBoolean("ascii", false);
            defaultSharedPreferences.getBoolean("thread", true);
            Process.setThreadPriority(0);
            this.w = defaultSharedPreferences.getBoolean("remember", false);
            if (this.w) {
                if (sharedPreferences.getInt("SELECTED_FILTER", 0) < this.q.w.size()) {
                    this.q.z = sharedPreferences.getInt("SELECTED_FILTER", 0);
                }
                if (sharedPreferences.getInt("SELECTED_EFFECT", 0) < this.q.x.size()) {
                    this.q.A = sharedPreferences.getInt("SELECTED_EFFECT", 0);
                }
                if (sharedPreferences.getInt("SELECTED_MASK", 0) < this.q.y.size()) {
                    this.q.B = sharedPreferences.getInt("SELECTED_MASK", 0);
                    this.q.j = false;
                }
                a();
            }
            boolean z2 = sharedPreferences.getBoolean("SELECTED_HD", false);
            if (!com.androidillusion.c.a.a) {
                z2 = false;
            }
            a(z2);
            b(sharedPreferences.getInt("SELECTED_FLASH", 0));
        }
    }
}
