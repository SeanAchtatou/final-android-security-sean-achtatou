package com.androidillusion.cameraillusion;

import android.app.Activity;
import android.view.View;
import com.androidillusion.c.a;
import com.androidillusion.e.e;

final class j implements View.OnClickListener {
    final /* synthetic */ CameraActivity a;

    j(CameraActivity cameraActivity) {
        this.a = cameraActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.androidillusion.e.e.a(android.app.Activity, com.androidillusion.f.c, int):void
     arg types: [com.androidillusion.cameraillusion.CameraActivity, com.androidillusion.f.c, int]
     candidates:
      com.androidillusion.e.e.a(com.androidillusion.cameraillusion.CameraActivity, com.androidillusion.f.c, int):void
      com.androidillusion.e.e.a(android.app.Activity, com.androidillusion.f.c, int):void */
    public final void onClick(View view) {
        if (a.a) {
            this.a.a(!this.a.g);
            if (!this.a.g && this.a.q.v.a().k != 1) {
                this.a.b(0);
                return;
            }
            return;
        }
        e.a((Activity) this.a, this.a.l, 2);
    }
}
