package com.androidillusion.cameraillusion;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.WindowManager;
import com.androidillusion.algorithm.Image;
import com.androidillusion.e.e;
import com.androidillusion.f.b;
import java.io.File;

public class FullImageViewerActivity extends Activity {
    float a = 0.0f;
    float b = 0.0f;
    String c;
    String d;
    Bitmap[] e;
    int f;
    int[] g;
    byte[] h;
    public b i;
    int j;
    int k;
    int l;
    int m;

    private void a() {
        for (int i2 = 0; i2 < 9; i2++) {
            Image.GetArray(this.d, this.g, this.h, this.l, this.m, (((i2 / 3) - 1) * this.j) + this.i.b, (((i2 % 3) - 1) * this.k) + this.i.c, this.j, this.k);
            this.e[i2].setPixels(this.g, 0, this.j, 0, 0, this.j, this.k);
        }
        this.i.a(this.e);
        this.i.postInvalidate();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        e.a(this);
        Display defaultDisplay = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        if (getResources().getConfiguration().orientation == 1) {
            this.j = Math.min(defaultDisplay.getWidth(), defaultDisplay.getHeight());
            this.k = Math.max(defaultDisplay.getWidth(), defaultDisplay.getHeight());
        } else {
            this.j = Math.max(defaultDisplay.getWidth(), defaultDisplay.getHeight());
            this.k = Math.min(defaultDisplay.getWidth(), defaultDisplay.getHeight());
        }
        Bundle extras = getIntent().getExtras();
        this.c = extras != null ? extras.getString("uri") : null;
        this.d = extras != null ? extras.getString("raw") : null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(this.c, options);
        this.l = options.outWidth;
        this.m = options.outHeight;
        this.i = new b(this, this.j, this.k, this.l, this.m);
        this.i.a = false;
        setContentView(this.i);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.e != null) {
            for (int i2 = 0; i2 < this.e.length; i2++) {
                if (this.e[i2] != null) {
                    this.e[i2].recycle();
                    this.e[i2] = null;
                }
            }
        }
        this.g = null;
        this.h = null;
        System.gc();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.androidillusion.e.b.a(java.lang.String, int, int, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int, int, int]
     candidates:
      com.androidillusion.e.b.a(android.content.Context, android.graphics.Bitmap, int, java.lang.String):android.net.Uri
      com.androidillusion.e.b.a(java.lang.String, int, int, boolean):android.graphics.Bitmap */
    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (new File(this.c).exists()) {
            this.i.b = 0;
            this.i.c = 0;
            this.i.d = 0;
            this.i.e = 0;
            this.i.a();
            this.e = new Bitmap[9];
            this.f = this.j * this.k;
            this.g = new int[this.f];
            this.h = new byte[(this.j * 3)];
            try {
                System.gc();
                int max = (int) (((float) Math.max(this.j, this.k)) / 3.5f);
                this.i.h = com.androidillusion.e.b.a(this.c, max, max, true);
                for (int i2 = 0; i2 < 9; i2++) {
                    System.gc();
                    this.e[i2] = Bitmap.createBitmap(this.j, this.k, Bitmap.Config.RGB_565);
                }
                a();
            } catch (Error e2) {
                Log.i("camera", "MEM ERROR, EXIT");
                finish();
                return;
            }
        } else {
            finish();
        }
        this.i.i = 0;
        this.i.j = 0;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.a = motionEvent.getRawX();
            this.b = motionEvent.getRawY();
            this.i.a = true;
        } else if (motionEvent.getAction() == 2) {
            this.i.a(motionEvent.getRawX() - this.a, motionEvent.getRawY() - this.b);
            this.a = motionEvent.getRawX();
            this.b = motionEvent.getRawY();
            this.i.postInvalidate();
        } else if (motionEvent.getAction() == 1) {
            this.i.a = false;
            this.i.b();
            a();
        }
        return true;
    }
}
