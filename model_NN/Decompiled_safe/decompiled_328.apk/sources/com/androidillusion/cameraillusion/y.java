package com.androidillusion.cameraillusion;

import android.content.DialogInterface;
import android.net.Uri;

final class y implements DialogInterface.OnClickListener {
    final /* synthetic */ ImageViewerActivity a;
    private final /* synthetic */ Uri b;
    private final /* synthetic */ String c;

    y(ImageViewerActivity imageViewerActivity, Uri uri, String str) {
        this.a = imageViewerActivity;
        this.b = uri;
        this.c = str;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        ImageViewerActivity.b(this.a, this.b, this.c);
    }
}
