package com.androidillusion.cameraillusion;

import android.preference.Preference;
import com.androidillusion.b.a;

final class aa implements Preference.OnPreferenceChangeListener {
    final /* synthetic */ SettingsActivity a;

    aa(SettingsActivity settingsActivity) {
        this.a = settingsActivity;
    }

    public final boolean onPreferenceChange(Preference preference, Object obj) {
        int parseInt = Integer.parseInt((String) obj);
        ((a) this.a.c.c.elementAt(0)).e = parseInt;
        this.a.f.setSummary(this.a.g[parseInt]);
        return true;
    }
}
