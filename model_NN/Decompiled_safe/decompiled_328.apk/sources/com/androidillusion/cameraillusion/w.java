package com.androidillusion.cameraillusion;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;

final class w implements View.OnClickListener {
    final /* synthetic */ ImageViewerActivity a;

    w(ImageViewerActivity imageViewerActivity) {
        this.a = imageViewerActivity;
    }

    public final void onClick(View view) {
        new AlertDialog.Builder(this.a).setMessage(this.a.getString(C0000R.string.image_viewer_dialog_delete_summary)).setTitle(this.a.getString(C0000R.string.image_viewer_dialog_delete_title)).setIcon(17301543).setCancelable(true).setPositiveButton(17039370, new y(this.a, this.a.a, this.a.b)).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).show();
    }
}
