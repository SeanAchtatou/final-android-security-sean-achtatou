package com.androidillusion.a;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.androidillusion.cameraillusion.C0000R;
import java.util.Vector;

public final class a extends BaseAdapter {
    public int a = 0;
    public int b;
    private Context c;
    private Vector d;
    private Vector e;
    private Vector f;
    /* access modifiers changed from: private */
    public Handler g;

    public a(Context context, Handler handler, Vector vector, Vector vector2, Vector vector3) {
        this.c = context;
        this.g = handler;
        this.d = vector;
        this.e = vector2;
        this.f = vector3;
    }

    public final int getCount() {
        switch (this.b) {
            case 0:
                return this.d.size();
            case 1:
                return this.e.size();
            case 2:
                return this.f.size();
            default:
                return 0;
        }
    }

    public final Object getItem(int i) {
        switch (this.b) {
            case 0:
                return this.d.get(i);
            case 1:
                return this.e.get(i);
            case 2:
                return this.f.get(i);
            default:
                return null;
        }
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        com.androidillusion.d.a aVar;
        switch (this.b) {
            case 0:
                aVar = (com.androidillusion.d.a) this.d.get(i);
                break;
            case 1:
                aVar = (com.androidillusion.d.a) this.e.get(i);
                break;
            case 2:
                aVar = (com.androidillusion.d.a) this.f.get(i);
                break;
            default:
                aVar = null;
                break;
        }
        View inflate = view == null ? View.inflate(this.c, C0000R.layout.element_adapter, null) : view;
        ((TextView) inflate.findViewById(C0000R.id.title)).setText(aVar.c);
        ((ImageView) inflate.findViewById(C0000R.id.iconView1)).setImageBitmap(BitmapFactory.decodeResource(this.c.getResources(), aVar.d));
        ImageView imageView = (ImageView) inflate.findViewById(C0000R.id.imageViewCheck);
        if (i == this.a) {
            imageView.setImageBitmap(BitmapFactory.decodeResource(this.c.getResources(), C0000R.drawable.checked));
        } else {
            imageView.setImageBitmap(BitmapFactory.decodeResource(this.c.getResources(), C0000R.drawable.unchecked));
        }
        inflate.setOnClickListener(new b(this, i));
        return inflate;
    }
}
