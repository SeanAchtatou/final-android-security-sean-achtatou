package com.androidillusion.a;

import android.os.Message;
import android.view.View;

final class b implements View.OnClickListener {
    final /* synthetic */ a a;
    private final /* synthetic */ int b;

    b(a aVar, int i) {
        this.a = aVar;
        this.b = i;
    }

    public final void onClick(View view) {
        this.a.a = this.b;
        this.a.notifyDataSetChanged();
        Message message = new Message();
        message.setTarget(this.a.g);
        message.what = 1;
        message.arg1 = this.a.b;
        message.arg2 = this.a.a;
        message.sendToTarget();
    }
}
