package com.androidillusion.b;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.view.View;

public final class g extends View {
    int a = 0;
    int b = 0;
    public Bitmap c;
    long d;
    public int e;
    public int f;
    public boolean g;
    public boolean h;
    public int i;
    public Matrix j;
    private Paint k;
    private Paint l;
    private Paint m;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [int, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    public g(Context context, int i2, int i3) {
        super(context);
        setWillNotCacheDrawing(false);
        this.g = false;
        this.e = i2;
        this.f = i3;
        this.k = new Paint();
        this.k.setColor(-16777216);
        this.k.setStyle(Paint.Style.FILL);
        this.l = new Paint();
        int max = Math.max(i2, i3);
        this.l.setColor(-1);
        this.l.setTextSize((float) (max / 22));
        this.l.setAntiAlias(true);
        this.l.setShadowLayer(3.0f, 3.0f, 3.0f, -16777216);
        this.l.setTextAlign(Paint.Align.RIGHT);
        this.m = new Paint(this.l);
        this.m.setTextSize((float) (max / 33));
        this.m.setTextAlign(Paint.Align.LEFT);
        this.d = System.currentTimeMillis();
    }

    public final void a(int i2) {
        this.i = i2;
        this.g = false;
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        canvas.drawRect(0.0f, 0.0f, (float) this.e, (float) this.f, this.k);
        if (this.c != null) {
            canvas.drawBitmap(this.c, this.j, this.k);
        }
    }
}
