package com.androidillusion.b;

import android.graphics.Point;
import java.text.DecimalFormat;
import java.util.Vector;

public final class a {
    public Vector a;
    public int b;
    public int c;
    public Vector d;
    public int e;
    public boolean f;
    public int g;
    public int h;
    public String[] i;
    public boolean j;
    public int k;
    public boolean l;
    public boolean m;
    public boolean n;
    public boolean o;
    public boolean p;

    public final Point a() {
        try {
            return (Point) this.a.elementAt(this.c);
        } catch (Exception e2) {
            return new Point(0, 0);
        }
    }

    public final Point b() {
        try {
            return (Point) this.d.elementAt(this.e);
        } catch (Exception e2) {
            return new Point(0, 0);
        }
    }

    public final String c() {
        return this.f ? this.i[this.g] : String.valueOf(new DecimalFormat("###.##").format(1.0d)) + "x";
    }
}
