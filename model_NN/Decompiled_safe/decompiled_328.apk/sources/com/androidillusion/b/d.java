package com.androidillusion.b;

import android.hardware.Camera;
import android.media.AudioManager;
import android.os.Message;
import com.androidillusion.e.a;
import com.androidillusion.e.c;

final class d implements Camera.PictureCallback {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ b a;

    d(b bVar) {
        this.a = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.androidillusion.e.a.a(com.androidillusion.b.a, android.hardware.Camera, boolean):void
     arg types: [com.androidillusion.b.a, android.hardware.Camera, int]
     candidates:
      com.androidillusion.e.a.a(android.content.Context, int, int):com.androidillusion.b.f
      com.androidillusion.e.a.a(java.lang.reflect.Method, java.lang.Object, java.lang.Object[]):java.lang.Object
      com.androidillusion.e.a.a(java.lang.Class, java.lang.String, java.lang.Class[]):java.lang.reflect.Method
      com.androidillusion.e.a.a(com.androidillusion.b.a, android.hardware.Camera, boolean):void */
    public final void onPictureTaken(byte[] bArr, Camera camera) {
        Message message = new Message();
        message.setTarget(this.a.u);
        message.what = 6;
        message.sendToTarget();
        b.a(this.a, 0);
        ((AudioManager) this.a.getContext().getSystemService("audio")).setStreamMute(1, false);
        a a2 = this.a.v.a();
        if (a2.k == 1) {
            a.a(a2, this.a.r, false);
        }
        if (this.a.M) {
            c.a(this.a.S);
        }
        if (this.a.r != null) {
            this.a.r.stopPreview();
        }
        new Thread(new e(this, bArr)).start();
    }
}
