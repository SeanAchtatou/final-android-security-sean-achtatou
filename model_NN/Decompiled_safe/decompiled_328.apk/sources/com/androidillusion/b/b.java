package com.androidillusion.b;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.androidillusion.algorithm.Image;
import com.androidillusion.cameraillusion.C0000R;
import com.androidillusion.e.a;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Vector;

public final class b extends SurfaceView implements Camera.PreviewCallback, SurfaceHolder.Callback {
    public static Bitmap l;
    public static Bitmap m;
    public int A;
    public int B;
    int C;
    int D;
    public int E;
    public boolean F = false;
    public Uri G;
    public String H;
    public int I;
    public boolean J;
    public int K;
    public boolean L;
    public boolean M;
    public boolean N;
    char[][] O;
    public boolean P = false;
    Camera.AutoFocusCallback Q = new c(this);
    Camera.PictureCallback R = new d(this);
    /* access modifiers changed from: private */
    public Context S;
    Method a;
    Object[] b;
    public g c;
    Canvas d = new Canvas();
    long e;
    float f;
    public boolean g = false;
    public boolean h;
    public boolean i = false;
    public boolean j;
    int k = 0;
    public Bitmap n;
    public int[] o;
    float[] p;
    SurfaceHolder q;
    public Camera r;
    int s;
    int t;
    Handler u;
    public f v;
    public Vector w;
    public Vector x;
    public Vector y;
    public int z;

    public b(Context context, Handler handler, int i2, int i3) {
        super(context);
        this.v = a.a(context, i2, i3);
        this.u = handler;
        this.q = getHolder();
        this.q.addCallback(this);
        this.q.setType(3);
        this.S = context;
        this.c = new g(context, i2, i3);
        this.e = System.currentTimeMillis();
        this.f = 0.0f;
        this.w = new Vector();
        this.w.add(new com.androidillusion.d.a(0, 0, context.getString(C0000R.string.filter_none), C0000R.drawable.thumb_filter_none, null));
        if (com.androidillusion.c.a.a) {
            this.w.add(new com.androidillusion.d.a(0, 1, context.getString(C0000R.string.filter_customized), C0000R.drawable.thumb_filter_custom, new String[]{context.getString(C0000R.string.filter_config_contrast), context.getString(C0000R.string.filter_config_saturation), context.getString(C0000R.string.filter_config_brightness)}));
        }
        this.w.add(new com.androidillusion.d.a(0, 2, context.getString(C0000R.string.filter_mono), C0000R.drawable.thumb_filter_mono, null));
        this.w.add(new com.androidillusion.d.a(0, 20, context.getString(C0000R.string.filter_color), C0000R.drawable.thumb_filter_color, new String[]{context.getString(C0000R.string.filter_config_hue), context.getString(C0000R.string.filter_config_saturation)}));
        this.w.add(new com.androidillusion.d.a(0, 3, context.getString(C0000R.string.filter_negative), C0000R.drawable.thumb_filter_negative, null));
        this.w.add(new com.androidillusion.d.a(0, 4, context.getString(C0000R.string.filter_sepia), C0000R.drawable.thumb_filter_sepia, null));
        this.w.add(new com.androidillusion.d.a(0, 5, context.getString(C0000R.string.filter_aqua), C0000R.drawable.thumb_filter_aqua, null));
        this.w.add(new com.androidillusion.d.a(0, 6, context.getString(C0000R.string.filter_old_photo), C0000R.drawable.thumb_filter_old_photo, null));
        this.w.add(new com.androidillusion.d.a(0, 7, context.getString(C0000R.string.filter_pencil), C0000R.drawable.thumb_filter_pencil, null));
        this.w.add(new com.androidillusion.d.a(0, 8, context.getString(C0000R.string.filter_chalk), C0000R.drawable.thumb_filter_chalk, null));
        if (com.androidillusion.c.a.a) {
            this.w.add(new com.androidillusion.d.a(0, 9, context.getString(C0000R.string.filter_emboss), C0000R.drawable.thumb_filter_emboss, null));
            this.w.add(new com.androidillusion.d.a(0, 10, context.getString(C0000R.string.filter_lomo), C0000R.drawable.thumb_filter_lomo, null));
        }
        this.w.add(new com.androidillusion.d.a(0, 11, context.getString(C0000R.string.filter_thermal), C0000R.drawable.thumb_filter_thermal, null));
        this.w.add(new com.androidillusion.d.a(0, 12, context.getString(C0000R.string.filter_ascii_art), C0000R.drawable.thumb_filter_ascii, null));
        this.w.add(new com.androidillusion.d.a(0, 13, context.getString(C0000R.string.filter_comic), C0000R.drawable.thumb_filter_comic, null));
        this.w.add(new com.androidillusion.d.a(0, 14, context.getString(C0000R.string.filter_xray), C0000R.drawable.thumb_filter_xray, null));
        this.w.add(new com.androidillusion.d.a(0, 15, context.getString(C0000R.string.filter_oil), C0000R.drawable.thumb_filter_oil, null));
        this.w.add(new com.androidillusion.d.a(0, 16, context.getString(C0000R.string.filter_bw), C0000R.drawable.thumb_filter_bw, new String[]{context.getString(C0000R.string.filter_config_threshold)}));
        this.w.add(new com.androidillusion.d.a(0, 17, context.getString(C0000R.string.filter_red_channel), C0000R.drawable.thumb_filter_red_channel, null));
        this.w.add(new com.androidillusion.d.a(0, 18, context.getString(C0000R.string.filter_green_channel), C0000R.drawable.thumb_filter_green_channel, null));
        this.w.add(new com.androidillusion.d.a(0, 19, context.getString(C0000R.string.filter_blue_channel), C0000R.drawable.thumb_filter_blue_channel, null));
        this.x = new Vector();
        this.x.add(new com.androidillusion.d.a(1, 0, context.getString(C0000R.string.effect_none), C0000R.drawable.thumb_filter_none, null));
        this.x.add(new com.androidillusion.d.a(1, 15, context.getString(C0000R.string.effect_blur), C0000R.drawable.thumb_effect_blur, new String[]{context.getString(C0000R.string.effect_config_amount)}));
        if (com.androidillusion.c.a.a) {
            this.x.add(new com.androidillusion.d.a(1, 16, context.getString(C0000R.string.effect_sharp), C0000R.drawable.thumb_effect_sharp, new String[]{context.getString(C0000R.string.effect_config_amount)}));
            this.x.add(new com.androidillusion.d.a(1, 17, context.getString(C0000R.string.effect_glow), C0000R.drawable.thumb_effect_glow, new String[]{context.getString(C0000R.string.effect_config_amount)}));
        }
        this.x.add(new com.androidillusion.d.a(1, 1, context.getString(C0000R.string.effect_thin), C0000R.drawable.thumb_effect_thin, new String[]{context.getString(C0000R.string.effect_config_amount)}));
        this.x.add(new com.androidillusion.d.a(1, 2, context.getString(C0000R.string.effect_fat), C0000R.drawable.thumb_effect_fat, new String[]{context.getString(C0000R.string.effect_config_amount)}));
        this.x.add(new com.androidillusion.d.a(1, 3, context.getString(C0000R.string.effect_tall), C0000R.drawable.thumb_effect_tall, new String[]{context.getString(C0000R.string.effect_config_amount)}));
        this.x.add(new com.androidillusion.d.a(1, 4, context.getString(C0000R.string.effect_short), C0000R.drawable.thumb_effect_short, new String[]{context.getString(C0000R.string.effect_config_amount)}));
        this.x.add(new com.androidillusion.d.a(1, 5, context.getString(C0000R.string.effect_hor_mirror), C0000R.drawable.thumb_effect_hor_mirror, null));
        this.x.add(new com.androidillusion.d.a(1, 6, context.getString(C0000R.string.effect_ver_mirror), C0000R.drawable.thumb_effect_ver_mirror, null));
        this.x.add(new com.androidillusion.d.a(1, 7, context.getString(C0000R.string.effect_pixelation), C0000R.drawable.thumb_effect_pixelation, null));
        this.x.add(new com.androidillusion.d.a(1, 9, context.getString(C0000R.string.effect_mosaic), C0000R.drawable.thumb_effect_mosaic, null));
        this.x.add(new com.androidillusion.d.a(1, 8, context.getString(C0000R.string.effect_fisheye), C0000R.drawable.thumb_effect_fisheye, new String[]{context.getString(C0000R.string.effect_config_radius), context.getString(C0000R.string.effect_config_amount)}));
        this.x.add(new com.androidillusion.d.a(1, 10, context.getString(C0000R.string.effect_light_tunnel), C0000R.drawable.thumb_effect_lighttunnel, new String[]{context.getString(C0000R.string.effect_config_radius)}));
        this.x.add(new com.androidillusion.d.a(1, 11, context.getString(C0000R.string.effect_pinch), C0000R.drawable.thumb_effect_pinch, new String[]{context.getString(C0000R.string.effect_config_radius), context.getString(C0000R.string.effect_config_amount)}));
        this.x.add(new com.androidillusion.d.a(1, 12, context.getString(C0000R.string.effect_twirl), C0000R.drawable.thumb_effect_twirl, new String[]{context.getString(C0000R.string.effect_config_radius), context.getString(C0000R.string.effect_config_amount)}));
        this.x.add(new com.androidillusion.d.a(1, 13, context.getString(C0000R.string.effect_rough_glass), C0000R.drawable.thumb_effect_roughglass, null));
        if (com.androidillusion.c.a.a) {
            this.x.add(new com.androidillusion.d.a(1, 14, context.getString(C0000R.string.effect_waves), C0000R.drawable.thumb_effect_waves, null));
        }
        this.y = new Vector();
        this.y.add(new com.androidillusion.d.b(0, context.getString(C0000R.string.mask_none), 0, C0000R.drawable.thumb_mask_none, null, 0));
        this.y.add(new com.androidillusion.d.b(1, context.getString(C0000R.string.mask_frame), C0000R.drawable.mask_frame, C0000R.drawable.thumb_mask_frame, new float[]{110.0f, 83.0f, 530.0f, 83.0f, 530.0f, 398.0f, 110.0f, 398.0f}, 0));
        this.y.add(new com.androidillusion.d.b(8, context.getString(C0000R.string.mask_blackboard), C0000R.drawable.mask_blackboard, C0000R.drawable.thumb_mask_blackboard, new float[]{124.0f, 170.0f, 426.0f, 69.0f, 514.0f, 265.0f, 177.0f, 384.0f}, 1));
        this.y.add(new com.androidillusion.d.b(7, context.getString(C0000R.string.mask_poster), C0000R.drawable.mask_poster, C0000R.drawable.thumb_mask_poster, new float[]{183.0f, 122.0f, 509.0f, 122.0f, 509.0f, 367.0f, 183.0f, 367.0f}, 0));
        boolean z2 = com.androidillusion.c.a.a;
        this.y.add(new com.androidillusion.d.b(4, context.getString(C0000R.string.mask_bricks), C0000R.drawable.mask_bricks_png, C0000R.drawable.thumb_mask_bricks, new float[]{0.0f, 0.0f, 640.0f, 0.0f, 640.0f, 480.0f, 0.0f, 480.0f}, 1));
        boolean z3 = com.androidillusion.c.a.a;
        this.y.add(new com.androidillusion.d.b(10, context.getString(C0000R.string.mask_white_border), C0000R.drawable.mask_white_border, C0000R.drawable.thumb_mask_white_border, new float[]{0.0f, 0.0f, 640.0f, 0.0f, 640.0f, 480.0f, 0.0f, 480.0f}, 1));
        String str = String.valueOf(com.androidillusion.e.b.b()) + com.androidillusion.c.a.c;
        Vector a2 = com.androidillusion.e.b.a(str);
        for (int i4 = 0; i4 < a2.size(); i4++) {
            Vector vector = this.y;
            String str2 = (String) a2.elementAt(i4);
            vector.add(new com.androidillusion.d.b(str2.substring(0, str2.length() - 4), new float[]{0.0f, 0.0f, 640.0f, 0.0f, 640.0f, 480.0f, 0.0f, 480.0f}, String.valueOf(str) + ((String) a2.elementAt(i4))));
        }
    }

    public static int a(int i2, int i3, int i4) {
        if (i4 != 0) {
            return 3;
        }
        switch (i3) {
            case 8:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
                return 2;
            case 9:
            default:
                return i2 == 12 ? 1 : 0;
        }
    }

    static /* synthetic */ void a(b bVar, int i2) {
        bVar.I = i2;
        Message message = new Message();
        message.setTarget(bVar.u);
        message.what = 8;
        message.sendToTarget();
    }

    private void a(byte[] bArr) {
        if (this.b == null) {
            h();
        }
        this.b[0] = bArr;
        try {
            this.a.invoke(this.r, this.b);
        } catch (Exception e2) {
            Log.e("camera", "error addCallbackBuffer: " + e2.toString());
        }
    }

    private void g() {
        try {
            this.F = false;
            this.g = false;
            Image.d = null;
            Image.e = null;
            Image.c = true;
            this.j = false;
            this.n = null;
            this.c.g = false;
            a a2 = this.v.a();
            Point a3 = a2.a();
            this.s = a3.x;
            this.t = a3.y;
            Camera.Parameters parameters = this.r.getParameters();
            parameters.setPictureFormat(256);
            parameters.setPreviewFormat(17);
            Point b2 = a2.b();
            Log.i("camera", "camera hd res: " + b2.x + "x" + b2.y);
            parameters.setPreviewSize(this.s, this.t);
            parameters.setPictureSize(b2.x, b2.y);
            this.r.setParameters(parameters);
            PixelFormat pixelFormat = new PixelFormat();
            PixelFormat.getPixelFormatInfo(parameters.getPreviewFormat(), pixelFormat);
            h();
            byte[] bArr = new byte[(((this.s * this.t) * pixelFormat.bitsPerPixel) / 8)];
            if (l != null) {
                l.recycle();
                l = null;
            }
            if (m != null) {
                m.recycle();
                m = null;
            }
            this.o = null;
            System.gc();
            l = Bitmap.createBitmap(this.s, this.t, Bitmap.Config.RGB_565);
            m = Bitmap.createBitmap(this.s, this.t, Bitmap.Config.RGB_565);
            this.o = new int[(this.s * this.t)];
            a(bArr);
            i();
            this.c.g = false;
            this.c.h = a2.j;
            this.r.startPreview();
            Message message = new Message();
            message.setTarget(this.u);
            message.what = 12;
            message.sendToTarget();
            a.a(this.r, this.v.a().g);
            j();
        } catch (Exception e2) {
            Log.i("camera", "Memory exception -> exit");
            Message message2 = new Message();
            message2.setTarget(this.u);
            message2.what = 17;
            message2.sendToTarget();
        } catch (Error e3) {
            Log.i("camera", "Memory error -> exit");
            Message message3 = new Message();
            message3.setTarget(this.u);
            message3.what = 17;
            message3.sendToTarget();
        }
    }

    private void h() {
        try {
            this.a = Class.forName("android.hardware.Camera").getMethod("addCallbackBuffer", new byte[1].getClass());
            this.b = new Object[1];
        } catch (Exception e2) {
            Log.e("camera", "Error addCallbackBuffer: " + e2.toString());
        }
    }

    private void i() {
        Method method;
        int i2 = 0;
        try {
            Method[] methods = Class.forName("android.hardware.Camera").getMethods();
            while (true) {
                if (i2 >= methods.length) {
                    method = null;
                    break;
                } else if (methods[i2].getName().compareTo("setPreviewCallbackWithBuffer") == 0) {
                    method = methods[i2];
                    break;
                } else {
                    i2++;
                }
            }
            if (method != null) {
                new Object[1][0] = this;
                method.invoke(this.r, this);
                return;
            }
            Log.i("camera", "setPreviewCallbackWithBuffer: not found");
        } catch (Exception e2) {
            Log.i("camera", e2.toString());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.androidillusion.e.a.a(com.androidillusion.b.a, android.hardware.Camera, boolean):void
     arg types: [com.androidillusion.b.a, android.hardware.Camera, int]
     candidates:
      com.androidillusion.e.a.a(android.content.Context, int, int):com.androidillusion.b.f
      com.androidillusion.e.a.a(java.lang.reflect.Method, java.lang.Object, java.lang.Object[]):java.lang.Object
      com.androidillusion.e.a.a(java.lang.Class, java.lang.String, java.lang.Class[]):java.lang.reflect.Method
      com.androidillusion.e.a.a(com.androidillusion.b.a, android.hardware.Camera, boolean):void */
    private void j() {
        a a2 = this.v.a();
        if (!a2.l) {
            return;
        }
        if (a2.k == 1) {
            a.a(a2, this.r, true);
            return;
        }
        a.a(a2, this.r, false);
        Camera.Parameters parameters = this.r.getParameters();
        switch (a2.k) {
            case 0:
                parameters.setFlashMode("off");
                break;
            case 1:
            case 2:
                parameters.setFlashMode("on");
                break;
            case 3:
                parameters.setFlashMode("auto");
                break;
            default:
                parameters.setFlashMode("off");
                break;
        }
        this.r.setParameters(parameters);
    }

    private int k() {
        return (this.E == 0 || this.E == 180) ? 2 : 1;
    }

    public final com.androidillusion.d.a a(int i2) {
        switch (i2) {
            case 0:
                return (com.androidillusion.d.a) this.w.elementAt(this.z);
            case 1:
                return (com.androidillusion.d.a) this.x.elementAt(this.A);
            case 2:
                return (com.androidillusion.d.a) this.y.elementAt(this.B);
            default:
                return null;
        }
    }

    public final synchronized void a() {
        if (this.r != null) {
            this.r.stopPreview();
            this.r.release();
            this.r = null;
        }
    }

    public final void a(boolean z2) {
        a a2 = this.v.a();
        if (a2.l) {
            if (z2) {
                boolean z3 = false;
                while (!z3) {
                    a2.k = (a2.k + 1) % 4;
                    switch (a2.k) {
                        case 0:
                            z3 = true;
                            break;
                        case 1:
                            if (!a2.m && !a2.n) {
                                z3 = false;
                                break;
                            } else {
                                z3 = true;
                                break;
                            }
                            break;
                        case 2:
                            z3 = a2.o;
                            break;
                        case 3:
                            z3 = a2.p;
                            break;
                    }
                }
            } else if (a2.k == 1) {
                a2.k = 0;
            } else {
                a2.k = 1;
            }
            j();
        }
    }

    public final int b() {
        return this.v.a;
    }

    public final void b(int i2) {
        this.E = i2;
        switch (a(1).b) {
            case 1:
            case 2:
            case 3:
            case 4:
                Image.c = true;
                return;
            default:
                return;
        }
    }

    public final void c() {
        a a2 = this.v.a();
        if (a2.f && a2.h > a2.g) {
            a2.g++;
            a.a(this.r, a2.g);
        }
    }

    public final void d() {
        a a2 = this.v.a();
        if (a2.f && a2.g > 0) {
            a2.g--;
            a.a(this.r, a2.g);
        }
    }

    public final void e() {
        a();
        this.v.b = (this.v.b + 1) % this.v.a;
        this.r = a.a(this.r, this.v);
        g();
    }

    public final void f() {
        Message message = new Message();
        message.setTarget(this.u);
        message.what = 7;
        message.sendToTarget();
        g();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x01dd, code lost:
        android.util.Log.i("camera", "Memory exception -> exit");
        r0 = new android.os.Message();
        r0.setTarget(r11.u);
        r0.what = 17;
        r0.sendToTarget();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0203, code lost:
        android.util.Log.i("camera", "Memory error -> exit");
        r0 = new android.os.Message();
        r0.setTarget(r11.u);
        r0.what = 17;
        r0.sendToTarget();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0482 A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x04e1 A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x0551 A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x002d A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0041 A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005a A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0074 A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0202 A[ExcHandler: Error (e java.lang.Error), Splitter:B:4:0x000b] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x030f A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0324 A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0339 A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x034e A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0363 A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0373 A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0383 A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x038e A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0399 A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x03ae A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x03be A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x03d3 A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x03e8 A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x03f3 A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x03fe A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x042a A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0456 A[Catch:{ Exception -> 0x01dc, Error -> 0x0202 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onPreviewFrame(byte[] r12, android.hardware.Camera r13) {
        /*
            r11 = this;
            if (r12 == 0) goto L_0x01c0
            boolean r0 = r11.P
            if (r0 != 0) goto L_0x01b9
            r0 = 0
            com.androidillusion.d.a r1 = r11.a(r0)
            int r0 = r1.b     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            switch(r0) {
                case 1: goto L_0x01c1;
                case 2: goto L_0x01f7;
                case 3: goto L_0x0234;
                case 4: goto L_0x023f;
                case 5: goto L_0x0257;
                case 6: goto L_0x0270;
                case 7: goto L_0x027b;
                case 8: goto L_0x0287;
                case 9: goto L_0x0293;
                case 10: goto L_0x029f;
                case 11: goto L_0x02aa;
                case 12: goto L_0x0010;
                case 13: goto L_0x02b5;
                case 14: goto L_0x02c1;
                case 15: goto L_0x02cc;
                case 16: goto L_0x02d7;
                case 17: goto L_0x02e7;
                case 18: goto L_0x02f4;
                case 19: goto L_0x0302;
                case 20: goto L_0x021d;
                default: goto L_0x0010;
            }     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
        L_0x0010:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.NDKFilterNoneYUV(r0, r12, r1, r2)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
        L_0x0019:
            r0 = 1
            com.androidillusion.d.a r5 = r11.a(r0)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r0 = r5.b     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            switch(r0) {
                case 1: goto L_0x030f;
                case 2: goto L_0x0324;
                case 3: goto L_0x0339;
                case 4: goto L_0x034e;
                case 5: goto L_0x0363;
                case 6: goto L_0x0373;
                case 7: goto L_0x0383;
                case 8: goto L_0x0399;
                case 9: goto L_0x038e;
                case 10: goto L_0x03ae;
                case 11: goto L_0x03be;
                case 12: goto L_0x03d3;
                case 13: goto L_0x03e8;
                case 14: goto L_0x03f3;
                case 15: goto L_0x03fe;
                case 16: goto L_0x042a;
                case 17: goto L_0x0456;
                default: goto L_0x0023;
            }     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
        L_0x0023:
            com.androidillusion.b.f r0 = r11.v     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.b.a r0 = r0.a()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            boolean r0 = r0.j     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            if (r0 == 0) goto L_0x0036
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.NDKFrontCameraMirrorRGB(r0, r1, r2)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
        L_0x0036:
            r0 = 0
            com.androidillusion.d.a r0 = r11.a(r0)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r0 = r0.b     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r1 = 12
            if (r0 != r1) goto L_0x004f
            android.content.Context r0 = r11.S     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r1 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r3 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            char[][] r0 = com.androidillusion.algorithm.Image.a(r0, r1, r2, r3)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r11.O = r0     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
        L_0x004f:
            r0 = 2
            com.androidillusion.d.a r13 = r11.a(r0)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.d.b r13 = (com.androidillusion.d.b) r13     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r0 = r13.b     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            if (r0 != 0) goto L_0x0482
            android.graphics.Bitmap r0 = com.androidillusion.b.b.l     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r1 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r2 = 0
            int r3 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r4 = 0
            r5 = 0
            int r6 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r7 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0.setPixels(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
        L_0x006a:
            com.androidillusion.b.g r0 = r11.c     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            android.graphics.Bitmap r1 = com.androidillusion.b.b.l     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0.c = r1     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            boolean r2 = r0.g     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            if (r2 != 0) goto L_0x0082
            int r2 = r0.e     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r2 * 5
            int r2 = r2 / 6
            int r3 = r0.i     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            switch(r3) {
                case 0: goto L_0x05f9;
                case 1: goto L_0x05d9;
                case 2: goto L_0x0650;
                default: goto L_0x007f;
            }     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
        L_0x007f:
            r2 = 1
            r0.g = r2     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
        L_0x0082:
            int r2 = r0.e     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r3 = r1.getWidth()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r2 - r3
            int r2 = r2 / 2
            r0.a = r2     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r0.f     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r1.getHeight()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r2 - r1
            int r1 = r1 / 2
            r0.b = r1     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0.postInvalidate()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
        L_0x009c:
            boolean r0 = r11.F
            if (r0 == 0) goto L_0x0198
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = com.androidillusion.e.b.b()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r0.<init>(r1)
            java.lang.String r1 = com.androidillusion.c.a.d
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.androidillusion.e.b.b(r0)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = com.androidillusion.e.b.b()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r0.<init>(r1)
            java.lang.String r1 = com.androidillusion.c.a.c
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.androidillusion.e.b.b(r0)
            boolean r0 = r11.J
            if (r0 == 0) goto L_0x06c9
            r0 = 0
            com.androidillusion.d.a r0 = r11.a(r0)
            int r0 = r0.b
            r1 = 1
            com.androidillusion.d.a r1 = r11.a(r1)
            int r1 = r1.b
            r2 = 2
            com.androidillusion.d.a r2 = r11.a(r2)
            int r2 = r2.b
            int r0 = a(r0, r1, r2)
            if (r0 != 0) goto L_0x06c9
            android.content.Context r0 = r11.getContext()
            java.lang.String r1 = "audio"
            java.lang.Object r13 = r0.getSystemService(r1)
            android.media.AudioManager r13 = (android.media.AudioManager) r13
            r0 = 1
            r1 = 1
            r13.setStreamMute(r0, r1)
            android.os.StatFs r0 = new android.os.StatFs
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r1 = r1.getAbsolutePath()
            r0.<init>(r1)
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r1 = r1.getAbsolutePath()
            r0.restat(r1)
            int r1 = r0.getAvailableBlocks()
            long r1 = (long) r1
            int r0 = r0.getBlockSize()
            long r3 = (long) r0
            long r0 = r1 * r3
            com.androidillusion.b.f r2 = r11.v
            com.androidillusion.b.a r2 = r2.a()
            android.graphics.Point r2 = r2.b()
            int r3 = r2.x
            int r2 = r2.y
            int r2 = r2 * r3
            int r2 = r2 * 3
            int r2 = r2 * 3
            long r2 = (long) r2
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 >= 0) goto L_0x066e
            android.os.Message r0 = new android.os.Message
            r0.<init>()
            android.os.Handler r1 = r11.u
            r0.setTarget(r1)
            r1 = 15
            r0.what = r1
            android.os.Bundle r1 = new android.os.Bundle
            r1.<init>()
            java.lang.String r4 = "msg"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            android.content.Context r6 = r11.S
            r7 = 2131099767(0x7f060077, float:1.7811897E38)
            java.lang.String r6 = r6.getString(r7)
            java.lang.String r6 = java.lang.String.valueOf(r6)
            r5.<init>(r6)
            java.lang.String r6 = ": "
            java.lang.StringBuilder r5 = r5.append(r6)
            r6 = 1000000(0xf4240, double:4.940656E-318)
            long r2 = r2 / r6
            java.lang.StringBuilder r2 = r5.append(r2)
            java.lang.String r3 = " MB "
            java.lang.StringBuilder r2 = r2.append(r3)
            android.content.Context r3 = r11.S
            r5 = 2131099768(0x7f060078, float:1.7811899E38)
            java.lang.String r3 = r3.getString(r5)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.putString(r4, r2)
            r0.setData(r1)
            r0.sendToTarget()
        L_0x0195:
            r0 = 0
            r11.F = r0
        L_0x0198:
            int r0 = r11.k
            int r0 = r0 + 1
            r11.k = r0
            int r0 = r11.k
            int r0 = r0 % 30
            if (r0 != 0) goto L_0x01b9
            long r0 = java.lang.System.currentTimeMillis()
            long r2 = r11.e
            long r0 = r0 - r2
            long r2 = java.lang.System.currentTimeMillis()
            r11.e = r2
            r2 = 1189765120(0x46ea6000, float:30000.0)
            float r0 = (float) r0
            float r0 = r2 / r0
            r11.f = r0
        L_0x01b9:
            android.hardware.Camera r0 = r11.r
            if (r0 == 0) goto L_0x01c0
            r11.a(r12)
        L_0x01c0:
            return
        L_0x01c1:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r3 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r4 = r1.g     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r5 = 0
            r4 = r4[r5]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r5 = r1.g     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r6 = 1
            r5 = r5[r6]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r1 = r1.g     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r6 = 2
            r6 = r1[r6]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r1 = r12
            com.androidillusion.algorithm.Image.NDKFilterCustomYUV(r0, r1, r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0019
        L_0x01dc:
            r0 = move-exception
            java.lang.String r0 = "camera"
            java.lang.String r1 = "Memory exception -> exit"
            android.util.Log.i(r0, r1)
            android.os.Message r0 = new android.os.Message
            r0.<init>()
            android.os.Handler r1 = r11.u
            r0.setTarget(r1)
            r1 = 17
            r0.what = r1
            r0.sendToTarget()
            goto L_0x009c
        L_0x01f7:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.NDKFilterMonoYUV(r0, r12, r1, r2)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0019
        L_0x0202:
            r0 = move-exception
            java.lang.String r0 = "camera"
            java.lang.String r1 = "Memory error -> exit"
            android.util.Log.i(r0, r1)
            android.os.Message r0 = new android.os.Message
            r0.<init>()
            android.os.Handler r1 = r11.u
            r0.setTarget(r1)
            r1 = 17
            r0.what = r1
            r0.sendToTarget()
            goto L_0x009c
        L_0x021d:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r3 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r4 = r1.g     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r5 = 0
            r4 = r4[r5]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r1 = r1.g     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r5 = 1
            r5 = r1[r5]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r6 = 1
            r1 = r12
            com.androidillusion.algorithm.Image.NDKFilterColorUVYUV(r0, r1, r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0019
        L_0x0234:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.NDKFilterNegativeYUV(r0, r12, r1, r2)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0019
        L_0x023f:
            r0 = -8
            r11.C = r0     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0 = 21
            r11.D = r0     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r3 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r4 = r11.C     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r5 = r11.D     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r6 = 0
            r1 = r12
            com.androidillusion.algorithm.Image.NDKFilterColorUVYUV(r0, r1, r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0019
        L_0x0257:
            r0 = 19
            r11.C = r0     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0 = -73
            r11.D = r0     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r3 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r4 = r11.C     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r5 = r11.D     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r6 = 0
            r1 = r12
            com.androidillusion.algorithm.Image.NDKFilterColorUVYUV(r0, r1, r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0019
        L_0x0270:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.NDKFilterOldPhotoYUV(r0, r12, r1, r2)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0019
        L_0x027b:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r3 = 1
            com.androidillusion.algorithm.Image.NDKFilterPencilYUV(r0, r12, r1, r2, r3)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0019
        L_0x0287:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r3 = 2
            com.androidillusion.algorithm.Image.NDKFilterPencilYUV(r0, r12, r1, r2, r3)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0019
        L_0x0293:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r3 = 3
            com.androidillusion.algorithm.Image.NDKFilterPencilYUV(r0, r12, r1, r2, r3)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0019
        L_0x029f:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.NDKFilterLomoYUV(r0, r12, r1, r2)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0019
        L_0x02aa:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.NDKFilterThermalYUV(r0, r12, r1, r2)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0019
        L_0x02b5:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r3 = 4
            com.androidillusion.algorithm.Image.NDKFilterPencilYUV(r0, r12, r1, r2, r3)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0019
        L_0x02c1:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.NDKFilterXrayYUV(r0, r12, r1, r2)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0019
        L_0x02cc:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.NDKFilterOilYUV(r0, r12, r1, r2)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0019
        L_0x02d7:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r3 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r1 = r1.g     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r4 = 0
            r1 = r1[r4]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.NDKFilterBWYUV(r0, r12, r2, r3, r1)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0019
        L_0x02e7:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r3 = 16711680(0xff0000, float:2.3418052E-38)
            com.androidillusion.algorithm.Image.NDKFilterColorcropYUV(r0, r12, r1, r2, r3)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0019
        L_0x02f4:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r3 = 65280(0xff00, float:9.1477E-41)
            com.androidillusion.algorithm.Image.NDKFilterColorcropYUV(r0, r12, r1, r2, r3)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0019
        L_0x0302:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r3 = 255(0xff, float:3.57E-43)
            com.androidillusion.algorithm.Image.NDKFilterColorcropYUV(r0, r12, r1, r2, r3)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0019
        L_0x030f:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r3 = r11.k()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r4 = 1
            int[] r5 = r5.g     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r6 = 0
            r5 = r5[r6]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.a(r0, r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0023
        L_0x0324:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r3 = r11.k()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r4 = 2
            int[] r5 = r5.g     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r6 = 0
            r5 = r5[r6]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.a(r0, r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0023
        L_0x0339:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r3 = r11.k()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r4 = 3
            int[] r5 = r5.g     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r6 = 0
            r5 = r5[r6]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.a(r0, r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0023
        L_0x034e:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r3 = r11.k()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r4 = 4
            int[] r5 = r5.g     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r6 = 0
            r5 = r5[r6]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.a(r0, r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0023
        L_0x0363:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r3 = r11.k()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r4 = 1
            com.androidillusion.algorithm.Image.NDKEffectMirrorRGB(r0, r1, r2, r3, r4)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0023
        L_0x0373:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r3 = r11.k()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r4 = 2
            com.androidillusion.algorithm.Image.NDKEffectMirrorRGB(r0, r1, r2, r3, r4)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0023
        L_0x0383:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.NDKEffectPixelationRGB(r0, r1, r2)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0023
        L_0x038e:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.a(r0, r1, r2)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0023
        L_0x0399:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r3 = r5.g     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r4 = 0
            r3 = r3[r4]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r4 = r5.g     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r5 = 1
            r4 = r4[r5]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.a(r0, r1, r2, r3, r4)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0023
        L_0x03ae:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r3 = r5.g     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r4 = 0
            r3 = r3[r4]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.a(r0, r1, r2, r3)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0023
        L_0x03be:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r3 = r5.g     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r4 = 0
            r3 = r3[r4]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r4 = r5.g     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r5 = 1
            r4 = r4[r5]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.b(r0, r1, r2, r3, r4)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0023
        L_0x03d3:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r3 = r5.g     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r4 = 0
            r3 = r3[r4]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r4 = r5.g     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r5 = 1
            r4 = r4[r5]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.c(r0, r1, r2, r3, r4)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0023
        L_0x03e8:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.b(r0, r1, r2)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0023
        L_0x03f3:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.c(r0, r1, r2)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0023
        L_0x03fe:
            int[] r0 = com.androidillusion.algorithm.Image.e     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            if (r0 != 0) goto L_0x040b
            int r0 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r0 = r0 * r1
            int[] r0 = new int[r0]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.e = r0     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
        L_0x040b:
            boolean r0 = com.androidillusion.algorithm.Image.c     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            if (r0 == 0) goto L_0x0418
            int[] r0 = com.androidillusion.algorithm.Image.e     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r1 = 0
            java.util.Arrays.fill(r0, r1)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0 = 0
            com.androidillusion.algorithm.Image.c = r0     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
        L_0x0418:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r1 = com.androidillusion.algorithm.Image.e     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r3 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r4 = r5.g     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r5 = 0
            r4 = r4[r5]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.NDKEffectBoxblurRGB(r0, r1, r2, r3, r4)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0023
        L_0x042a:
            int[] r0 = com.androidillusion.algorithm.Image.e     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            if (r0 != 0) goto L_0x0437
            int r0 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r0 = r0 * r1
            int[] r0 = new int[r0]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.e = r0     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
        L_0x0437:
            boolean r0 = com.androidillusion.algorithm.Image.c     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            if (r0 == 0) goto L_0x0444
            int[] r0 = com.androidillusion.algorithm.Image.e     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r1 = 0
            java.util.Arrays.fill(r0, r1)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0 = 0
            com.androidillusion.algorithm.Image.c = r0     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
        L_0x0444:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r1 = com.androidillusion.algorithm.Image.e     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r3 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r4 = r5.g     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r5 = 0
            r4 = r4[r5]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.NDKEffectSharpRGB(r0, r1, r2, r3, r4)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0023
        L_0x0456:
            int[] r0 = com.androidillusion.algorithm.Image.e     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            if (r0 != 0) goto L_0x0463
            int r0 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r0 = r0 * r1
            int[] r0 = new int[r0]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.e = r0     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
        L_0x0463:
            boolean r0 = com.androidillusion.algorithm.Image.c     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            if (r0 == 0) goto L_0x0470
            int[] r0 = com.androidillusion.algorithm.Image.e     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r1 = 0
            java.util.Arrays.fill(r0, r1)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0 = 0
            com.androidillusion.algorithm.Image.c = r0     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
        L_0x0470:
            int[] r0 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r1 = com.androidillusion.algorithm.Image.e     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r3 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r4 = r5.g     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r5 = 0
            r4 = r4[r5]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.algorithm.Image.NDKEffectGlowRGB(r0, r1, r2, r3, r4)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x0023
        L_0x0482:
            boolean r0 = r11.j     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            if (r0 != 0) goto L_0x054f
            java.lang.String r0 = "camera"
            java.lang.String r1 = "RECALCULATE MASK"
            android.util.Log.i(r0, r1)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0 = 8
            float[] r0 = new float[r0]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float[] r1 = r13.i     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r2 = 0
            r3 = 0
            r4 = 8
            java.lang.System.arraycopy(r1, r2, r0, r3, r4)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            android.graphics.BitmapFactory$Options r1 = new android.graphics.BitmapFactory$Options     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r1.<init>()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r2 = 0
            r1.inScaled = r2     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r13.h     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            if (r2 == 0) goto L_0x04f3
            android.content.res.Resources r2 = r11.getResources()     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            int r3 = r13.h     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r2, r3, r1)     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
        L_0x04b0:
            int r2 = r1.getWidth()     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            int r3 = r1.getHeight()     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            int r4 = r11.s     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            float r4 = (float) r4     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            float r2 = (float) r2     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            float r2 = r4 / r2
            int r4 = r11.t     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            float r4 = (float) r4     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            float r3 = (float) r3     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            float r3 = r4 / r3
            r4 = 0
        L_0x04c5:
            r5 = 8
            if (r4 < r5) goto L_0x04fa
            int r2 = r11.s     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            int r3 = r11.t     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            r4 = 1
            android.graphics.Bitmap r2 = android.graphics.Bitmap.createScaledBitmap(r1, r2, r3, r4)     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            r11.n = r2     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            r1.recycle()     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            r11.p = r0     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            r0 = 1
            r11.j = r0     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            r8 = r13
        L_0x04dd:
            int r0 = r8.b     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            if (r0 != 0) goto L_0x0551
            android.graphics.Bitmap r0 = com.androidillusion.b.b.l     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r1 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r2 = 0
            int r3 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r4 = 0
            r5 = 0
            int r6 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r7 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0.setPixels(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x006a
        L_0x04f3:
            java.lang.String r2 = r13.k     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeFile(r2, r1)     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            goto L_0x04b0
        L_0x04fa:
            int r5 = r4 % 2
            if (r5 != 0) goto L_0x0506
            r5 = r0[r4]     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            float r5 = r5 * r2
            r0[r4] = r5     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
        L_0x0503:
            int r4 = r4 + 1
            goto L_0x04c5
        L_0x0506:
            r5 = r0[r4]     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            float r5 = r5 * r3
            r0[r4] = r5     // Catch:{ Exception -> 0x050c, Error -> 0x0202 }
            goto L_0x0503
        L_0x050c:
            r0 = move-exception
            android.os.Message r0 = new android.os.Message     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0.<init>()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            android.os.Handler r1 = r11.u     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0.setTarget(r1)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r1 = 15
            r0.what = r1     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            android.os.Bundle r1 = new android.os.Bundle     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r1.<init>()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            java.lang.String r2 = "msg"
            android.content.Context r3 = r11.S     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r4 = 2131099764(0x7f060074, float:1.781189E38)
            java.lang.String r3 = r3.getString(r4)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r1.putString(r2, r3)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0.setData(r1)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0.sendToTarget()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0 = 0
            r11.B = r0     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0 = 2
            com.androidillusion.d.a r13 = r11.a(r0)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            com.androidillusion.d.b r13 = (com.androidillusion.d.b) r13     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            android.os.Message r0 = new android.os.Message     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0.<init>()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            android.os.Handler r1 = r11.u     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0.setTarget(r1)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r1 = 16
            r0.what = r1     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0.sendToTarget()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
        L_0x054f:
            r8 = r13
            goto L_0x04dd
        L_0x0551:
            r0 = 8
            float[] r9 = new float[r0]     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0 = 0
            r1 = 0
            r9[r0] = r1     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0 = 1
            r1 = 0
            r9[r0] = r1     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0 = 2
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r1 = (float) r1     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r9[r0] = r1     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0 = 3
            r1 = 0
            r9[r0] = r1     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0 = 4
            int r1 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r1 = (float) r1     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r9[r0] = r1     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0 = 5
            int r1 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r1 = (float) r1     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r9[r0] = r1     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0 = 6
            r1 = 0
            r9[r0] = r1     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0 = 7
            int r1 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r1 = (float) r1     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r9[r0] = r1     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            android.graphics.Bitmap r0 = com.androidillusion.b.b.m     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int[] r1 = r11.o     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r2 = 0
            int r3 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r4 = 0
            r5 = 0
            int r6 = r11.s     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r7 = r11.t     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0.setPixels(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            android.graphics.Canvas r0 = r11.d     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            android.graphics.Bitmap r1 = com.androidillusion.b.b.l     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0.setBitmap(r1)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            android.graphics.Matrix r0 = new android.graphics.Matrix     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0.<init>()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r2 = 0
            float[] r3 = r11.p     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r4 = 0
            r5 = 4
            r1 = r9
            r0.setPolyToPoly(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r1 = r8.j     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            if (r1 != 0) goto L_0x05bd
            android.graphics.Canvas r1 = r11.d     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            android.graphics.Bitmap r2 = r11.n     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            android.graphics.Matrix r3 = new android.graphics.Matrix     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r3.<init>()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r4 = 0
            r1.drawBitmap(r2, r3, r4)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            android.graphics.Canvas r1 = r11.d     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            android.graphics.Bitmap r2 = com.androidillusion.b.b.m     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r3 = 0
            r1.drawBitmap(r2, r0, r3)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x006a
        L_0x05bd:
            int r1 = r8.j     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r2 = 1
            if (r1 != r2) goto L_0x006a
            android.graphics.Canvas r1 = r11.d     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            android.graphics.Bitmap r2 = com.androidillusion.b.b.m     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r3 = 0
            r1.drawBitmap(r2, r0, r3)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            android.graphics.Canvas r0 = r11.d     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            android.graphics.Bitmap r1 = r11.n     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            android.graphics.Matrix r2 = new android.graphics.Matrix     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r2.<init>()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r3 = 0
            r0.drawBitmap(r1, r2, r3)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x006a
        L_0x05d9:
            android.graphics.Matrix r3 = new android.graphics.Matrix     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r3.<init>()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0.j = r3     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            android.graphics.Matrix r3 = r0.j     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r4 = r1.getWidth()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r2 = r2 - r4
            int r2 = r2 / 2
            float r2 = (float) r2     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r4 = r0.f     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r5 = r1.getHeight()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r4 = r4 - r5
            int r4 = r4 / 2
            float r4 = (float) r4     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r3.postTranslate(r2, r4)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x007f
        L_0x05f9:
            android.graphics.Matrix r3 = new android.graphics.Matrix     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r3.<init>()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0.j = r3     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r3 = r1.getWidth()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r3 = (float) r3     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r4 = (float) r2     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r3 = r3 / r4
            int r4 = r1.getHeight()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r4 = (float) r4     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r5 = r0.f     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r5 = (float) r5     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r4 = r4 / r5
            int r5 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r5 <= 0) goto L_0x0643
            float r4 = (float) r2     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r5 = r1.getHeight()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r5 = (float) r5     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r3 = r5 / r3
        L_0x061c:
            int r5 = r1.getWidth()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r5 = (float) r5     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r5 = r4 / r5
            int r6 = r1.getHeight()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r6 = (float) r6     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r6 = r3 / r6
            android.graphics.Matrix r7 = r0.j     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r7.preScale(r5, r6)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            android.graphics.Matrix r5 = r0.j     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r2 = (float) r2     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r2 = r2 - r4
            r4 = 1073741824(0x40000000, float:2.0)
            float r2 = r2 / r4
            int r4 = r0.f     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r4 = (float) r4     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r3 = r4 - r3
            r4 = 1073741824(0x40000000, float:2.0)
            float r3 = r3 / r4
            r5.postTranslate(r2, r3)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x007f
        L_0x0643:
            int r3 = r1.getWidth()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r3 = (float) r3     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r3 = r3 / r4
            int r4 = r0.f     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r4 = (float) r4     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r10 = r4
            r4 = r3
            r3 = r10
            goto L_0x061c
        L_0x0650:
            android.graphics.Matrix r3 = new android.graphics.Matrix     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r3.<init>()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r0.j = r3     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r2 = (float) r2     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r3 = r1.getWidth()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r3 = (float) r3     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r2 = r2 / r3
            int r3 = r0.f     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r3 = (float) r3     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            int r4 = r1.getHeight()     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r4 = (float) r4     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            float r3 = r3 / r4
            android.graphics.Matrix r4 = r0.j     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            r4.preScale(r2, r3)     // Catch:{ Exception -> 0x01dc, Error -> 0x0202 }
            goto L_0x007f
        L_0x066e:
            r0 = 0
            r11.i = r0
            android.hardware.Camera r0 = r11.r
            android.hardware.Camera$Parameters r0 = r0.getParameters()
            r1 = 90
            r0.setJpegQuality(r1)
            com.androidillusion.b.f r1 = r11.v
            com.androidillusion.b.a r1 = r1.a()
            boolean r1 = r1.l
            if (r1 == 0) goto L_0x0691
            com.androidillusion.b.f r1 = r11.v
            com.androidillusion.b.a r1 = r1.a()
            int r1 = r1.k
            switch(r1) {
                case 0: goto L_0x06a7;
                case 1: goto L_0x06b9;
                case 2: goto L_0x06ad;
                case 3: goto L_0x06b3;
                default: goto L_0x0691;
            }
        L_0x0691:
            android.hardware.Camera r1 = r11.r
            r1.setParameters(r0)
            android.hardware.Camera r0 = r11.r
            r1 = 0
            r0.setPreviewCallback(r1)
            android.hardware.Camera r0 = r11.r
            r1 = 0
            r2 = 0
            android.hardware.Camera$PictureCallback r3 = r11.R
            r0.takePicture(r1, r2, r3)
            goto L_0x0195
        L_0x06a7:
            java.lang.String r1 = "off"
            r0.setFlashMode(r1)
            goto L_0x0691
        L_0x06ad:
            java.lang.String r1 = "on"
            r0.setFlashMode(r1)
            goto L_0x0691
        L_0x06b3:
            java.lang.String r1 = "auto"
            r0.setFlashMode(r1)
            goto L_0x0691
        L_0x06b9:
            com.androidillusion.b.f r1 = r11.v
            com.androidillusion.b.a r1 = r1.a()
            boolean r1 = r1.o
            if (r1 == 0) goto L_0x0691
            java.lang.String r1 = "on"
            r0.setFlashMode(r1)
            goto L_0x0691
        L_0x06c9:
            boolean r0 = r11.M
            if (r0 == 0) goto L_0x06d2
            android.content.Context r0 = r11.S
            com.androidillusion.e.c.a(r0)
        L_0x06d2:
            java.lang.String r0 = "camera"
            java.lang.String r1 = "SAVE SCREEN IMAGE"
            android.util.Log.i(r0, r1)
            java.lang.String r8 = com.androidillusion.e.b.c()
            boolean r0 = r11.h
            if (r0 == 0) goto L_0x0729
            int[] r0 = r11.o
            int r1 = r11.s
            int r2 = r11.t
            com.androidillusion.algorithm.Image.NDKFilterNoneYUV(r0, r12, r1, r2)
            com.androidillusion.b.f r0 = r11.v
            com.androidillusion.b.a r0 = r0.a()
            boolean r0 = r0.j
            if (r0 == 0) goto L_0x06fd
            int[] r0 = r11.o
            int r1 = r11.s
            int r2 = r11.t
            com.androidillusion.algorithm.Image.NDKFrontCameraMirrorRGB(r0, r1, r2)
        L_0x06fd:
            android.graphics.Bitmap r0 = com.androidillusion.b.b.m
            int[] r1 = r11.o
            r2 = 0
            int r3 = r11.s
            r4 = 0
            r5 = 0
            int r6 = r11.s
            int r7 = r11.t
            r0.setPixels(r1, r2, r3, r4, r5, r6, r7)
            android.content.Context r0 = r11.S
            android.graphics.Bitmap r1 = com.androidillusion.b.b.m
            int r2 = r11.K
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = java.lang.String.valueOf(r8)
            r3.<init>(r4)
            java.lang.String r4 = "-ori.jpg"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            com.androidillusion.e.b.a(r0, r1, r2, r3)
        L_0x0729:
            android.content.Context r0 = r11.S
            android.graphics.Bitmap r1 = com.androidillusion.b.b.l
            int r2 = r11.K
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = java.lang.String.valueOf(r8)
            r3.<init>(r4)
            java.lang.String r4 = ".jpg"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            android.net.Uri r0 = com.androidillusion.e.b.a(r0, r1, r2, r3)
            r11.G = r0
            r0 = 0
            com.androidillusion.d.a r0 = r11.a(r0)
            int r0 = r0.b
            r1 = 12
            if (r0 != r1) goto L_0x077d
            boolean r0 = r11.N
            if (r0 == 0) goto L_0x077d
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = com.androidillusion.e.b.b()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r0.<init>(r1)
            java.lang.String r1 = com.androidillusion.c.a.b
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r8)
            java.lang.String r1 = ".html"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            char[][] r1 = r11.O
            com.androidillusion.e.b.a(r0, r1)
        L_0x077d:
            android.os.Message r0 = new android.os.Message
            r0.<init>()
            android.os.Handler r1 = r11.u
            r0.setTarget(r1)
            r1 = 5
            r0.what = r1
            r0.sendToTarget()
            goto L_0x0195
        */
        throw new UnsupportedOperationException("Method not decompiled: com.androidillusion.b.b.onPreviewFrame(byte[], android.hardware.Camera):void");
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.L && !this.g && this.r != null) {
            this.g = true;
            this.r.autoFocus(this.Q);
        }
        return true;
    }

    public final void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        g();
    }

    public final void surfaceCreated(SurfaceHolder surfaceHolder) {
        try {
            this.r = a.a(this.r, this.v);
            try {
                this.r.setPreviewDisplay(this.q);
            } catch (IOException e2) {
                this.r.release();
                this.r = null;
            }
        } catch (Exception e3) {
            Log.i("camera", "Camera exception -> exit");
            Message message = new Message();
            message.setTarget(this.u);
            message.what = 9;
            message.sendToTarget();
        }
    }

    public final void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        a();
    }
}
