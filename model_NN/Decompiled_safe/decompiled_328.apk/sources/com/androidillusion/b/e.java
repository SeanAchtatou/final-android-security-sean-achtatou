package com.androidillusion.b;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Message;
import android.util.Log;
import com.androidillusion.algorithm.Image;
import com.androidillusion.c.a;
import com.androidillusion.e.b;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

final class e implements Runnable {
    final /* synthetic */ d a;
    private final /* synthetic */ byte[] b;

    e(d dVar, byte[] bArr) {
        this.a = dVar;
        this.b = bArr;
    }

    public final void run() {
        int i;
        int i2;
        String str;
        if (this.b != null) {
            long currentTimeMillis = System.currentTimeMillis();
            String c = b.c();
            String str2 = String.valueOf(b.b()) + a.b + c + ".jpg";
            String str3 = String.valueOf(b.b()) + a.d + "rotated.tmp";
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(str2);
                fileOutputStream.write(this.b);
                fileOutputStream.close();
                Log.d("camera", "onPictureTaken - wrote bytes: " + this.b.length);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            Log.d("camera", "onPictureTaken - jpeg");
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            options.inDither = false;
            options.inJustDecodeBounds = true;
            options.inSampleSize = 1;
            BitmapFactory.decodeFile(str2, options);
            int i3 = options.outWidth;
            int i4 = options.outHeight;
            if (this.a.a.K != 0) {
                Log.i("camera", "Rotate image first");
                if (this.a.a.v.a().j) {
                    if (this.a.a.K == 90) {
                        this.a.a.K = 270;
                    } else if (this.a.a.K == 270) {
                        this.a.a.K = 90;
                    }
                }
                Image.JpegToRotatedAngleJpeg(str2, str3, i3, i4, new StringBuilder().append(this.a.a.K).toString());
                BitmapFactory.Options options2 = new BitmapFactory.Options();
                options2.inPreferredConfig = Bitmap.Config.ARGB_8888;
                options2.inDither = false;
                options2.inJustDecodeBounds = true;
                options2.inSampleSize = 1;
                BitmapFactory.decodeFile(str3, options2);
                i = options2.outWidth;
                i2 = options2.outHeight;
            } else {
                i = i3;
                i2 = i4;
            }
            String str4 = String.valueOf(b.b()) + a.d + "level1.raw";
            String str5 = String.valueOf(b.b()) + a.d + "level2.raw";
            String str6 = String.valueOf(b.b()) + a.d + "level3.raw";
            String str7 = String.valueOf(b.b()) + a.d + "level1Temp.raw";
            if (this.a.a.K == 0) {
                Image.JpegToRawFile(str2, str4);
            } else {
                Image.JpegToRawFile(str3, str4);
            }
            Log.i("camera", "raw ready");
            if (this.a.a.h) {
                if (this.a.a.i) {
                    this.a.a.f();
                    return;
                }
                b.a(this.a.a, 15);
                String str8 = String.valueOf(b.b()) + a.b + c + "-ori.jpg";
                new File(this.a.a.K == 0 ? str2 : str3).renameTo(new File(str8));
                b.a(this.a.a.S, str8, "", str8, System.currentTimeMillis());
            }
            if (this.a.a.i) {
                this.a.a.f();
                return;
            }
            b.a(this.a.a, 25);
            com.androidillusion.d.a a2 = this.a.a.a(0);
            switch (a2.b) {
                case 1:
                    Image.NDKFilterCustomRAW(str4, str5, i, i2, a2.g[0], a2.g[1], a2.g[2]);
                    str = str5;
                    break;
                case 2:
                    Image.NDKFilterMonoRAW(str4, str5, i, i2);
                    str = str5;
                    break;
                case 3:
                    Image.NDKFilterNegativeRAW(str4, str5, i, i2);
                    str = str5;
                    break;
                case 4:
                    this.a.a.C = -8;
                    this.a.a.D = 21;
                    Image.NDKFilterColorUVRAW(str4, str5, i, i2, this.a.a.C, this.a.a.D, false);
                    str = str5;
                    break;
                case 5:
                    this.a.a.C = 19;
                    this.a.a.D = -73;
                    Image.NDKFilterColorUVRAW(str4, str5, i, i2, this.a.a.C, this.a.a.D, false);
                    str = str5;
                    break;
                case 6:
                    Image.NDKFilterOldPhotoRAW(str4, str5, i, i2);
                    str = str5;
                    break;
                case 7:
                    Image.NDKFilterPencilRAW(str4, str5, i, i2, 1);
                    str = str5;
                    break;
                case 8:
                    Image.NDKFilterPencilRAW(str4, str5, i, i2, 2);
                    str = str5;
                    break;
                case 9:
                    Image.NDKFilterPencilRAW(str4, str5, i, i2, 3);
                    str = str5;
                    break;
                case 10:
                    Image.NDKFilterLomoRAW(str4, str5, i, i2);
                    str = str5;
                    break;
                case 11:
                    Image.NDKFilterThermalRAW(str4, str5, i, i2);
                    str = str5;
                    break;
                case 12:
                default:
                    str = str4;
                    break;
                case 13:
                    Image.NDKFilterPencilRAW(str4, str5, i, i2, 4);
                    str = str5;
                    break;
                case 14:
                    Image.NDKFilterXrayRAW(str4, str5, i, i2);
                    str = str5;
                    break;
                case 15:
                    Image.NDKFilterOilRAW(str4, str5, i, i2);
                    str = str5;
                    break;
                case 16:
                    Image.NDKFilterBWRAW(str4, str5, i, i2, a2.g[0]);
                    str = str5;
                    break;
                case 17:
                    Image.NDKFilterColorcropRAW(str4, str5, i, i2, 16711680);
                    str = str5;
                    break;
                case 18:
                    Image.NDKFilterColorcropRAW(str4, str5, i, i2, 65280);
                    str = str5;
                    break;
                case 19:
                    Image.NDKFilterColorcropRAW(str4, str5, i, i2, 255);
                    str = str5;
                    break;
                case 20:
                    Image.NDKFilterColorUVRAW(str4, str5, i, i2, a2.g[0], a2.g[1], true);
                    str = str5;
                    break;
            }
            Log.i("camera", "filter raw ready");
            if (this.a.a.i) {
                this.a.a.f();
                return;
            }
            b.a(this.a.a, 50);
            com.androidillusion.d.a a3 = this.a.a.a(1);
            switch (a3.b) {
                case 1:
                    Image.NDKEffectHorizontalStretchRAW(str, str6, i, i2, ((float) i) / ((float) this.a.a.s), 1, a3.g[0]);
                    str = str6;
                    break;
                case 2:
                    Image.NDKEffectHorizontalStretchRAW(str, str6, i, i2, ((float) i) / ((float) this.a.a.s), 2, a3.g[0]);
                    str = str6;
                    break;
                case 3:
                    Image.NDKEffectVerticalStretchRAW(str, str6, i, i2, ((float) i) / ((float) this.a.a.s), 3, a3.g[0]);
                    str = str6;
                    break;
                case 4:
                    Image.NDKEffectVerticalStretchRAW(str, str6, i, i2, ((float) i) / ((float) this.a.a.s), 4, a3.g[0]);
                    str = str6;
                    break;
                case 5:
                    Image.NDKEffectMirrorRAW(str, str6, i, i2, this.a.a.K == 90 || this.a.a.K == 180, 1);
                    str = str6;
                    break;
                case 6:
                    Image.NDKEffectMirrorRAW(str, str6, i, i2, this.a.a.K == 180 || this.a.a.K == 270, 2);
                    str = str6;
                    break;
                case 7:
                    Image.NDKEffectPixelationRAW(str, str6, i, i2, (i * 4) / this.a.a.s);
                    str = str6;
                    break;
                case 9:
                    Image.NDKEffectMosaicRAW(str, str6, i, i2);
                    str = str6;
                    break;
                case 15:
                    Image.NDKEffectBoxblurRAW(str, str6, str7, i, i2, a3.g[0]);
                    str = str6;
                    break;
                case 16:
                    Image.NDKEffectSharpRAW(str, str6, str7, i, i2, a3.g[0]);
                    str = str6;
                    break;
                case 17:
                    Image.NDKEffectGlowRAW(str, str6, str7, i, i2, a3.g[0]);
                    str = str6;
                    break;
            }
            Log.i("camera", "effect raw ready");
            if (this.a.a.i) {
                this.a.a.f();
                return;
            }
            b.a(this.a.a, 75);
            long currentTimeMillis2 = System.currentTimeMillis();
            Image.RawToJpegFile(str, str2, i, i2, 90);
            long currentTimeMillis3 = System.currentTimeMillis();
            Log.i("camera", "encoding time ready " + (currentTimeMillis3 - currentTimeMillis2));
            Log.i("camera", "image ready " + (currentTimeMillis3 - currentTimeMillis));
            this.a.a.H = str;
            this.a.a.G = b.a(this.a.a.S, str2, "", str2, System.currentTimeMillis());
        }
        if (this.a.a.i) {
            this.a.a.f();
            return;
        }
        b.a(this.a.a, 100);
        this.a.a.f();
        Message message = new Message();
        message.setTarget(this.a.a.u);
        message.what = 5;
        message.sendToTarget();
    }
}
