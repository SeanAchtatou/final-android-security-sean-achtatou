package com.feasy.app.memory.CuteOceanAnimals;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

public class OptionActivity extends Activity {
    /* access modifiers changed from: private */
    public CheckBox cbChangeTheme;
    /* access modifiers changed from: private */
    public CheckBox cbMatchSound;
    /* access modifiers changed from: private */
    public CheckBox cbMusic;
    /* access modifiers changed from: private */
    public CheckBox cbVibrate;
    /* access modifiers changed from: private */
    public Intent mIntent;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.option);
        setTitle("Game Option");
        this.mIntent = getIntent();
        Bundle b = getIntent().getExtras();
        boolean isMusic = b.getBoolean("GameMusic");
        boolean isMatchSound = b.getBoolean("MatchSound");
        boolean isVibrate = b.getBoolean("Vibrate");
        boolean isChangeTheme = b.getBoolean("ChangeTheme");
        this.cbMusic = (CheckBox) findViewById(R.id.cb_music);
        this.cbMusic.setChecked(isMusic);
        this.cbMatchSound = (CheckBox) findViewById(R.id.cb_sound);
        this.cbMatchSound.setChecked(isMatchSound);
        this.cbVibrate = (CheckBox) findViewById(R.id.cb_vibrate);
        this.cbVibrate.setChecked(isVibrate);
        this.cbChangeTheme = (CheckBox) findViewById(R.id.cb_changetheme);
        this.cbChangeTheme.setChecked(isChangeTheme);
        ((Button) findViewById(R.id.btn_save)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                OptionActivity.this.mIntent.putExtra("GameMusic", OptionActivity.this.cbMusic.isChecked());
                OptionActivity.this.mIntent.putExtra("MatchSound", OptionActivity.this.cbMatchSound.isChecked());
                OptionActivity.this.mIntent.putExtra("Vibrate", OptionActivity.this.cbVibrate.isChecked());
                OptionActivity.this.mIntent.putExtra("ChangeTheme", OptionActivity.this.cbChangeTheme.isChecked());
                OptionActivity.this.setResult(-1, OptionActivity.this.mIntent);
                OptionActivity.this.finish();
            }
        });
        ((Button) findViewById(R.id.btn_cancel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                OptionActivity.this.finish();
            }
        });
    }
}
