package com.feasy.app.memory.CuteOceanAnimals;

public final class R {

    public static final class anim {
        public static final int alpha_rotate = 2130968576;
        public static final int alpha_scale = 2130968577;
        public static final int alpha_scale_rotate = 2130968578;
        public static final int alpha_scale_translate = 2130968579;
        public static final int alpha_scale_translate_rotate = 2130968580;
        public static final int alpha_translate = 2130968581;
        public static final int alpha_translate_rotate = 2130968582;
        public static final int decelerate_interpolator = 2130968583;
        public static final int gokou = 2130968584;
        public static final int grow_fade_in_center = 2130968585;
        public static final int my_alpha_action = 2130968586;
        public static final int my_rotate_action = 2130968587;
        public static final int my_scale_action = 2130968588;
        public static final int my_translate_action = 2130968589;
        public static final int myanimation_simple = 2130968590;
        public static final int myown_design = 2130968591;
        public static final int scale_rotate = 2130968592;
        public static final int scale_translate = 2130968593;
        public static final int scale_translate_rotate = 2130968594;
        public static final int slide_in_left = 2130968595;
        public static final int slide_in_right = 2130968596;
        public static final int slide_out_left = 2130968597;
        public static final int slide_out_right = 2130968598;
        public static final int translate_rotate = 2130968599;
        public static final int welcome = 2130968600;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int aa = 2130837504;
        public static final int app1 = 2130837505;
        public static final int app2 = 2130837506;
        public static final int bb = 2130837507;
        public static final int bg2 = 2130837508;
        public static final int bkg = 2130837509;
        public static final int bkg0 = 2130837510;
        public static final int bkg1 = 2130837511;
        public static final int bkg2 = 2130837512;
        public static final int bkg3 = 2130837513;
        public static final int cc = 2130837514;
        public static final int classictetris = 2130837515;
        public static final int dd = 2130837516;
        public static final int ee = 2130837517;
        public static final int ff = 2130837518;
        public static final int gg = 2130837519;
        public static final int hh = 2130837520;
        public static final int ic_backandcancel = 2130837521;
        public static final int ic_bye = 2130837522;
        public static final int ic_menu_block = 2130837523;
        public static final int ic_menu_close_clear_cancel = 2130837524;
        public static final int ic_menu_emoticons = 2130837525;
        public static final int ic_menu_info_details = 2130837526;
        public static final int ic_menu_refresh = 2130837527;
        public static final int ic_menu_revert = 2130837528;
        public static final int ic_menu_star = 2130837529;
        public static final int ic_menu_view = 2130837530;
        public static final int ic_moregame = 2130837531;
        public static final int icon = 2130837532;
        public static final int ii = 2130837533;
        public static final int image0 = 2130837534;
        public static final int image1 = 2130837535;
        public static final int image2 = 2130837536;
        public static final int image3 = 2130837537;
        public static final int jj = 2130837538;
        public static final int kk = 2130837539;
        public static final int ll = 2130837540;
        public static final int menu_quit = 2130837541;
        public static final int mm = 2130837542;
        public static final int other = 2130837543;
        public static final int pause_btn = 2130837544;
        public static final int play_btn = 2130837545;
    }

    public static final class id {
        public static final int ad2 = 2131165210;
        public static final int btn_cancel = 2131165221;
        public static final int btn_save = 2131165222;
        public static final int cb_changetheme = 2131165220;
        public static final int cb_music = 2131165217;
        public static final int cb_sound = 2131165218;
        public static final int cb_vibrate = 2131165219;
        public static final int game_Score = 2131165211;
        public static final int game_icon = 2131165185;
        public static final int game_level = 2131165186;
        public static final int game_score = 2131165187;
        public static final int game_time = 2131165189;
        public static final int game_timebar = 2131165188;
        public static final int img11 = 2131165190;
        public static final int img12 = 2131165191;
        public static final int img13 = 2131165192;
        public static final int img14 = 2131165193;
        public static final int img21 = 2131165194;
        public static final int img22 = 2131165195;
        public static final int img23 = 2131165196;
        public static final int img24 = 2131165197;
        public static final int img31 = 2131165198;
        public static final int img32 = 2131165199;
        public static final int img33 = 2131165200;
        public static final int img34 = 2131165201;
        public static final int img41 = 2131165202;
        public static final int img42 = 2131165203;
        public static final int img43 = 2131165204;
        public static final int img44 = 2131165205;
        public static final int img51 = 2131165206;
        public static final int img52 = 2131165207;
        public static final int img53 = 2131165208;
        public static final int img54 = 2131165209;
        public static final int img61 = 2131165212;
        public static final int img62 = 2131165213;
        public static final int img63 = 2131165214;
        public static final int img64 = 2131165215;
        public static final int img_icon = 2131165224;
        public static final int lv = 2131165223;
        public static final int main = 2131165184;
        public static final int tv_name = 2131165225;
        public static final int tv_sound = 2131165216;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int main_droid = 2130903041;
        public static final int option = 2130903042;
        public static final int popup_lv = 2130903043;
        public static final int popup_lv_item = 2130903044;
    }

    public static final class raw {
        public static final int matchit = 2131034112;
        public static final int music = 2131034113;
    }

    public static final class string {
        public static final int app_name = 2131099649;
        public static final int hello = 2131099648;
    }
}
