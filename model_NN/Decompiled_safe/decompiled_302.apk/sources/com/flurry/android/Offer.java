package com.flurry.android;

public final class Offer {
    private long a;
    private String b;
    private String c;
    private int d;
    private String e;
    private AdImage f;

    Offer(long j, String str, AdImage adImage, String str2, String str3, int i) {
        this.a = j;
        this.b = str2;
        this.e = str;
        this.f = adImage;
        this.c = str3;
        this.d = i;
    }

    public final long getId() {
        return this.a;
    }

    public final String getName() {
        return this.b;
    }

    public final String getDescription() {
        return this.c;
    }

    public final int getPrice() {
        return this.d;
    }

    public final String getUrl() {
        return this.e;
    }

    public final AdImage getImage() {
        return this.f;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[id=" + this.a + ",name=" + this.b + ",price=" + this.d);
        return sb.toString();
    }
}
