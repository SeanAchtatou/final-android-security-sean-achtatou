package com.mobfox.sdk;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class InAppWebView extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String url = (String) getIntent().getExtras().get(Const.REDIRECT_URI);
        if (url != null) {
            WebView webView = new WebView(this);
            webView.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return false;
                }
            });
            webView.loadUrl(url);
            setContentView(webView);
        } else if (Log.isLoggable(Const.TAG, 3)) {
            Log.d(Const.TAG, "url is null so do not load anything");
        }
    }
}
