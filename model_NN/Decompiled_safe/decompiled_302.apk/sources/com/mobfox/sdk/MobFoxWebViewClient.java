package com.mobfox.sdk;

import android.content.Context;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class MobFoxWebViewClient extends WebViewClient {
    private final MobFoxView adServerViewCore;
    private Context context;

    public MobFoxWebViewClient(MobFoxView adServerViewCore2, Context context2) {
        this.adServerViewCore = adServerViewCore2;
        this.context = context2;
    }

    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return true;
    }
}
