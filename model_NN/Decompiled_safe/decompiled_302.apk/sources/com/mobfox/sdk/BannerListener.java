package com.mobfox.sdk;

public interface BannerListener {
    void bannerLoadFailed(RequestException requestException);

    void bannerLoadSucceeded();

    void noAdFound();
}
