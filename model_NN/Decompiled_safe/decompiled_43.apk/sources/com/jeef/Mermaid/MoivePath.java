package com.jeef.Mermaid;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import java.util.Vector;

public class MoivePath {
    private Vector<PathInfo> list = new Vector<>();
    private int offset = 0;

    public void clear() {
        this.list.removeAllElements();
        this.offset = 0;
    }

    public void moveTo(float x, float y, float angle, float rate, float alpha) {
        PathInfo info = new PathInfo();
        info.x = x;
        info.y = y;
        info.rate = rate;
        info.angle = angle;
        info.alpha = alpha;
        this.list.addElement(info);
    }

    public void lineTo(float x, float y, float angle, int step, float rate, float alpha) {
        PathInfo last = this.list.elementAt(this.list.size() - 1);
        float a = last.angle;
        float incx = 1.0f;
        if (last.x == x) {
            incx = 0.0f;
        } else if (last.x > x) {
            incx = -1.0f;
        }
        float incy = 1.0f;
        if (last.y == y) {
            incy = 0.0f;
        } else if (last.y > y) {
            incy = -1.0f;
        }
        int len = (int) Math.abs(last.x - x);
        int len1 = (int) Math.abs(last.y - y);
        int len2 = len;
        float rateseg = rate / (((float) len2) / ((float) step));
        float aseg = (float) ((int) ((alpha / ((float) len2)) / ((float) step)));
        if (len < len1) {
            len2 = len1;
        }
        int ix = 0;
        int iy = 0;
        int sx = (int) last.x;
        int sy = (int) last.y;
        int d = 0;
        float r = last.rate;
        float ac = last.alpha;
        for (int rep = 0; rep < len2; rep++) {
            ix += len;
            iy += len1;
            if (ix >= len2) {
                ix -= len2;
                if (len == len2) {
                    d++;
                }
                sx = (int) (((float) sx) + incx);
            }
            if (iy >= len2) {
                iy -= len2;
                if (len1 == len2) {
                    d++;
                }
                sy = (int) (((float) sy) + incy);
            }
            if (d >= step || rep == len2 - 1) {
                d = 0;
                PathInfo p = new PathInfo();
                p.x = (float) sx;
                p.y = (float) sy;
                p.angle = a + angle;
                p.rate = r + rateseg;
                p.alpha = ac + aseg;
                r += rateseg;
                ac += aseg;
                if (angle > 0.0f) {
                    if (p.angle >= 360.0f) {
                        p.angle -= 360.0f;
                    }
                } else if (angle < 0.0f && p.angle < 0.0f) {
                    p.angle += 360.0f;
                }
                a = p.angle;
                this.list.addElement(p);
            }
        }
    }

    public void reset() {
        this.offset = 0;
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public PathInfo next() {
        if (this.offset >= this.list.size()) {
            return null;
        }
        this.offset++;
        return this.list.elementAt(this.offset - 1);
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public PathInfo getLast() {
        if (this.list.size() < 1) {
            return null;
        }
        return this.list.elementAt(this.list.size() - 1);
    }

    public void draw(Canvas canvas, Paint paint, Bitmap b, float rate_x, float rate_y, PathInfo p) {
        canvas.save();
        int old = paint.getAlpha();
        paint.setAlpha((int) p.alpha);
        int x = (int) (p.x * rate_x);
        int y = (int) (p.y * rate_y);
        int w = (int) ((((float) b.getWidth()) / 2.0f) * rate_x);
        int h = (int) ((((float) b.getHeight()) / 2.0f) * rate_y);
        int w2 = (int) (((float) w) * p.rate);
        int h2 = (int) (((float) h) * p.rate);
        canvas.rotate(p.angle, p.x * rate_x, p.y * rate_y);
        canvas.drawBitmap(b, new Rect(0, 0, b.getWidth(), b.getHeight()), new Rect(x - w2, y - h2, x + w2, y + h2), paint);
        paint.setAlpha(old);
        canvas.restore();
    }
}
