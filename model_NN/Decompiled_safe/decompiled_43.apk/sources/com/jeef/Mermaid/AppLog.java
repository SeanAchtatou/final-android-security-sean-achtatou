package com.jeef.Mermaid;

import android.util.Log;

public class AppLog {
    private static final String APP_TAG = "FantasyWallpaper";

    public static int logString(String message) {
        return Log.i(APP_TAG, message);
    }
}
