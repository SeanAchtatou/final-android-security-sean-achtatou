package com.jeef.Mermaid;

import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import com.jeef.wapsConnection.wapsConnection;

public class FantasyWallpaperSetup extends PreferenceActivity implements Preference.OnPreferenceChangeListener {
    private CheckBoxPreference chekcb = null;
    private wapsConnection waps;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPreferenceManager().setSharedPreferencesName("Mermaid");
        addPreferencesFromResource(R.xml.patternwallpaper_settings);
        this.chekcb = (CheckBoxPreference) findPreference("moive");
        this.chekcb.setOnPreferenceChangeListener(this);
        wapsConnection.Initialize(this, "2091ad55612c2c798cf31f53326bf44f", "goapk", 50, true);
        this.waps = new wapsConnection(this, "Mermaid_data");
    }

    public boolean onPreferenceChange(Preference preference, Object newValue) {
        return true;
    }

    public void onResume() {
        super.onResume();
        this.waps.onResume();
    }

    public void onPause() {
        super.onPause();
        this.waps.onPause();
    }
}
