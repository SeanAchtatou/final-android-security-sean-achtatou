package com.jeef.Mermaid;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int disc = 2130837504;
        public static final int icon = 2130837505;
    }

    public static final class id {
        public static final int tableRow1 = 2131099648;
        public static final int textView1 = 2131099649;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2131034112;
        public static final int wallpaper_description = 2131034117;
        public static final int wallpaper_pattern = 2131034113;
        public static final int wallpaper_settings = 2131034116;
        public static final int wallpaper_settings_summary = 2131034115;
        public static final int wallpaper_settings_title = 2131034114;
    }

    public static final class xml {
        public static final int patternwallpaper = 2130968576;
        public static final int patternwallpaper_settings = 2130968577;
    }
}
