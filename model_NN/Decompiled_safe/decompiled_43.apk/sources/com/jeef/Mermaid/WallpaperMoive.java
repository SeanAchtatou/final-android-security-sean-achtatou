package com.jeef.Mermaid;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.view.SurfaceHolder;
import java.util.Vector;

public class WallpaperMoive extends WallpaperService {
    private static int index = 0;
    private static long nowTime;
    /* access modifiers changed from: private */
    public final Handler handler = new Handler();
    /* access modifiers changed from: private */
    public boolean isVisible = false;
    /* access modifiers changed from: private */
    public Paint paint = null;
    /* access modifiers changed from: private */
    public SharedPreferences preference;

    public WallpaperService.Engine onCreateEngine() {
        return new PatternWallpaperEngine(getBaseContext());
    }

    public void onCreate() {
        super.onCreate();
        this.preference = getSharedPreferences("Mermaid", 0);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    private class PatternWallpaperEngine extends WallpaperService.Engine {
        private static final int DRAW_DELAY = 33;
        /* access modifiers changed from: private */
        public Vector<Bitmap> ImageList = new Vector<>();
        private float angle = 0.0f;
        private Bitmap back;
        protected Context con;
        private Bitmap disc;
        private final Runnable drawrunnable = new Runnable() {
            public void run() {
                PatternWallpaperEngine.this.draw();
            }
        };
        private Bitmap exbitmap = null;
        private int height = 800;
        private int index = 0;
        /* access modifiers changed from: private */
        public boolean isRun = true;
        private Runnable loadImage = new Runnable() {
            public void run() {
                while (PatternWallpaperEngine.this.isRun) {
                    if (PatternWallpaperEngine.this.ImageList.size() < 5) {
                        Bitmap ex = XBitmap.create(PatternWallpaperEngine.this.con, "a" + PatternWallpaperEngine.this.start + ".png");
                        synchronized (PatternWallpaperEngine.this.ImageList) {
                            PatternWallpaperEngine.this.ImageList.addElement(ex);
                        }
                        PatternWallpaperEngine patternWallpaperEngine = PatternWallpaperEngine.this;
                        patternWallpaperEngine.start = patternWallpaperEngine.start + 1;
                        if (PatternWallpaperEngine.this.start > 10045) {
                            PatternWallpaperEngine.this.start = 10001;
                        }
                    }
                    try {
                        Thread.sleep(1);
                    } catch (Exception e) {
                    }
                }
            }
        };
        private boolean loadOK = false;
        private float offx = 0.0f;
        private float rate_x = 1.0f;
        private float rate_y = 1.0f;
        /* access modifiers changed from: private */
        public int start = 10001;
        private int width = 480;

        PatternWallpaperEngine(Context ctx) {
            super(WallpaperMoive.this);
            this.con = ctx;
            this.back = XBitmap.create(ctx, "back.jpg");
            this.disc = XBitmap.create(ctx, "disc.jpg");
            this.loadOK = true;
        }

        public void onCreate(SurfaceHolder surfaceHolder) {
            super.onCreate(surfaceHolder);
        }

        public void onDestroy() {
            super.onDestroy();
            WallpaperMoive.this.handler.removeCallbacks(this.drawrunnable);
        }

        public void onOffsetsChanged(float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset) {
            this.offx = (float) (-xPixelOffset);
            super.onOffsetsChanged(xOffset, yOffset, xOffsetStep, yOffsetStep, xPixelOffset, yPixelOffset);
        }

        public void onSurfaceChanged(SurfaceHolder holder, int format, int width2, int height2) {
            super.onSurfaceChanged(holder, format, width2, height2);
            draw();
        }

        public void onSurfaceCreated(SurfaceHolder holder) {
            super.onSurfaceCreated(holder);
            WallpaperMoive.this.paint = new Paint();
            WallpaperMoive.this.paint.setAntiAlias(true);
            WallpaperMoive.this.paint.setColor(-65536);
            this.isRun = true;
            new Thread(this.loadImage).start();
        }

        public void onSurfaceDestroyed(SurfaceHolder holder) {
            super.onSurfaceDestroyed(holder);
            this.isRun = false;
        }

        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            WallpaperMoive.this.isVisible = visible;
            if (visible) {
                draw();
            } else {
                WallpaperMoive.this.handler.removeCallbacks(this.drawrunnable);
            }
        }

        /* access modifiers changed from: private */
        public void draw() {
            SurfaceHolder holder = getSurfaceHolder();
            Rect frame = holder.getSurfaceFrame();
            this.width = frame.width();
            this.height = frame.height();
            this.rate_x = ((float) this.width) / 480.0f;
            this.rate_y = ((float) this.height) / 800.0f;
            Canvas canvas = null;
            WallpaperMoive.this.handler.removeCallbacks(this.drawrunnable);
            try {
                canvas = holder.lockCanvas();
                drawPattern(canvas);
                if (WallpaperMoive.this.isVisible) {
                    WallpaperMoive.this.handler.postDelayed(this.drawrunnable, 33);
                }
            } finally {
                if (canvas != null) {
                    holder.unlockCanvasAndPost(canvas);
                }
            }
        }

        private void drawPattern(Canvas canvas) {
            if (this.loadOK) {
                boolean moive = WallpaperMoive.this.preference.getBoolean("moive", false);
                if (!moive) {
                    this.isRun = false;
                } else if (!this.isRun) {
                    this.isRun = true;
                    new Thread(this.loadImage).start();
                }
                WallpaperMoive.this.paint.setColor(-16777216);
                WallpaperMoive.this.paint.setStyle(Paint.Style.FILL);
                int i = (int) (this.offx / this.rate_x);
                if (moive) {
                    canvas.drawBitmap(this.back, new Rect(0, 0, 480, 800), new Rect(0, 0, this.width, this.height), WallpaperMoive.this.paint);
                } else {
                    canvas.drawBitmap(this.disc, new Rect(0, 0, 480, 800), new Rect(0, 0, this.width, this.height), WallpaperMoive.this.paint);
                }
                if (this.ImageList.size() > 0) {
                    if (this.exbitmap != null) {
                        this.exbitmap.recycle();
                        this.exbitmap = null;
                    }
                    this.exbitmap = this.ImageList.elementAt(0);
                    synchronized (this.ImageList) {
                        this.ImageList.removeElementAt(0);
                    }
                }
                if (moive && this.exbitmap != null) {
                    canvas.drawBitmap(this.exbitmap, new Rect(0, 0, 480, 800), new Rect(0, 0, this.width, this.height), WallpaperMoive.this.paint);
                }
            }
        }
    }
}
