package com.jeef.Mermaid;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.InputStream;

public class XBitmap {
    public static final Bitmap create(Context context, String file) {
        try {
            InputStream is = context.getAssets().open(file);
            Bitmap ret = BitmapFactory.decodeStream(is);
            is.close();
            return ret;
        } catch (Exception e) {
            return null;
        }
    }
}
