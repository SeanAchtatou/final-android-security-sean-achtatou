package com.jeef.wapsConnection;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Process;
import com.mobclick.android.MobclickAgent;
import com.waps.AppConnect;
import com.waps.UpdatePointsNotifier;

public class wapsConnection implements UpdatePointsNotifier {
    /* access modifiers changed from: private */
    public static String active = "active";
    /* access modifiers changed from: private */
    public static int buyState = 0;
    /* access modifiers changed from: private */
    public static int maxmoney = 50;
    /* access modifiers changed from: private */
    public static int money = 0;
    private static boolean netConfig = false;
    /* access modifiers changed from: private */
    public static boolean valid = false;
    /* access modifiers changed from: private */
    public String dialogText = null;
    final Runnable invalid = new Runnable() {
        public void run() {
            wapsConnection.this.showMessage("提示", "软件ID无效或已被盗版，不能正常使用！");
        }
    };
    final Runnable mActive = new Runnable() {
        public void run() {
            if (!wapsConnection.valid) {
                wapsConnection.this.showMessage("提示", "软件ID无效或已经背盗版，不能正常使用！");
                return;
            }
            String msg = "软件功能未激活，您现在拥有" + wapsConnection.money + "积分,使用" + wapsConnection.maxmoney + "积分，免费获取积分，激活软件所有功能，不需要花费您一分钱，您还等什么呢？马上行动吧。";
            if (wapsConnection.this.dialogText != null) {
                msg = wapsConnection.this.dialogText;
                if (msg.indexOf("m_money") >= 0) {
                    msg.replaceAll("m_money", new StringBuilder().append(wapsConnection.maxmoney).toString());
                }
                if (msg.indexOf("s_money") >= 0) {
                    msg.replaceAll("s_money", new StringBuilder().append(wapsConnection.money).toString());
                }
            }
            AlertDialog.Builder dialog = new AlertDialog.Builder(wapsConnection.this.parent);
            dialog.setTitle("积分系统");
            dialog.setMessage(msg);
            dialog.setCancelable(true);
            dialog.setPositiveButton("激活软件", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (wapsConnection.money < wapsConnection.maxmoney) {
                        wapsConnection.this.mHandler.post(new Runnable() {
                            public void run() {
                                wapsConnection.this.showMessage("激活失败", "您现在只拥有" + wapsConnection.money + "积分，激活软件所有功能" + "需要" + wapsConnection.maxmoney + "积分，无法进行软件激活！");
                            }
                        });
                    } else {
                        wapsConnection.buyState = 2;
                        AppConnect.getInstance(wapsConnection.this.parent).spendPoints(wapsConnection.maxmoney, wapsConnection.this);
                    }
                    dialog.cancel();
                }
            });
            dialog.setNegativeButton("获取" + wapsConnection.maxmoney + "积分", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    wapsConnection.buyState = 1;
                    AppConnect.getInstance(wapsConnection.this.parent).showOffers(wapsConnection.this.parent);
                    dialog.cancel();
                }
            });
            dialog.show();
        }
    };
    final Runnable mActived = new Runnable() {
        public void run() {
            wapsConnection.this.share.edit().putBoolean(wapsConnection.active, true).commit();
            wapsConnection.this.showMessage("激活成功", "该软件所有功能已经成功激活，可以永久免费使用，感谢您的支持！");
            wapsConnection.buyState = 0;
        }
    };
    final Runnable mError = new Runnable() {
        public void run() {
            wapsConnection.this.showMessage("错误提示", "获取积分失败，请检测网络是否正常！");
            wapsConnection.buyState = 0;
        }
    };
    final Runnable mError1 = new Runnable() {
        public void run() {
            wapsConnection.this.showMessage("错误提示", "激活失败，请稍后再试！");
            wapsConnection.buyState = 0;
        }
    };
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public Context parent;
    /* access modifiers changed from: private */
    public ShareData share;

    public static void Initialize(Context context, String ID, int max_money, boolean config) {
        maxmoney = max_money;
        netConfig = config;
        try {
            String ex = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString("WAPS_ID");
            if (ex == null || !ex.equals(ID)) {
                valid = false;
            } else {
                valid = true;
            }
        } catch (PackageManager.NameNotFoundException e) {
            valid = false;
        }
    }

    public static void Initialize(Context context, String ID, String act, int max_money, boolean config) {
        maxmoney = max_money;
        netConfig = config;
        active = act;
        try {
            String ex = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString("WAPS_ID");
            if (ex == null || !ex.equals(ID)) {
                valid = false;
            } else {
                valid = true;
            }
        } catch (PackageManager.NameNotFoundException e) {
            valid = false;
        }
    }

    public wapsConnection(Context context, String buyName) {
        if (valid) {
            this.share = new ShareData(context, buyName);
            this.parent = context;
            MobclickAgent.updateOnlineConfig(context);
            MobclickAgent.update(context);
            MobclickAgent.setUpdateOnlyWifi(false);
            AppConnect.getInstance(context);
            buyState = 0;
            AppConnect.getInstance(context).getPoints(this);
        }
    }

    public void setDialogText(String text) {
        this.dialogText = text;
    }

    public void onResume() {
        MobclickAgent.onResume(this.parent);
        buyState = 0;
        AppConnect.getInstance(this.parent).getPoints(this);
    }

    public void onPause() {
        MobclickAgent.onPause(this.parent);
    }

    public boolean isValid() {
        String key;
        if (netConfig) {
            key = MobclickAgent.getConfigParams(this.parent, active);
            if (key.equals("0")) {
                this.share.edit().putBoolean(active, true).commit();
            }
            if (!key.equals("")) {
                this.share.edit().putString("lastConfig", key).commit();
            } else {
                key = this.share.get().getString("lastConfig", "0");
            }
        } else {
            key = "1";
        }
        if (this.share.get().getBoolean(active, false) || !key.equals("1")) {
            return true;
        }
        this.mHandler.post(this.mActive);
        return false;
    }

    public String getNetConfig() {
        String ret = MobclickAgent.getConfigParams(this.parent, active);
        if (ret.equals("")) {
            return this.share.get().getString("lastConfig", "0");
        }
        return ret;
    }

    public String getNetConfig(String key, String def) {
        String ret = MobclickAgent.getConfigParams(this.parent, key);
        if (ret.equals("")) {
            return this.share.get().getString(key, def);
        }
        this.share.edit().putString(key, ret);
        return ret;
    }

    public boolean getValid() {
        if (valid) {
            return this.share.get().getBoolean(active, false);
        }
        this.mHandler.post(this.invalid);
        return false;
    }

    public void getUpdatePoints(String arg0, int arg1) {
        money = arg1;
        if (buyState == 1) {
            this.mHandler.post(this.mActive);
        } else if (buyState == 2) {
            this.mHandler.post(this.mActived);
        }
    }

    public void getUpdatePointsFailed(String arg0) {
        if (buyState == 1) {
            this.mHandler.post(this.mError);
        } else if (buyState == 2) {
            this.mHandler.post(this.mError1);
        }
    }

    public void ExitDialog() {
        AlertDialog.Builder about = new AlertDialog.Builder(this.parent);
        about.setTitle("退出系统");
        about.setMessage("选择确定将退出系统，您是否确定退出？");
        about.setCancelable(true);
        about.setPositiveButton("退出", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Process.killProcess(Process.myPid());
                System.exit(0);
                dialog.cancel();
            }
        });
        about.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        about.show();
    }

    public void showMessage(String title, String text) {
        AlertDialog.Builder about = new AlertDialog.Builder(this.parent);
        about.setTitle(title);
        about.setMessage(text);
        about.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        about.show();
    }
}
