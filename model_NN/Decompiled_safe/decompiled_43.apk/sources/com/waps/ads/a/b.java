package com.waps.ads.a;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobclick.android.ReportPolicy;
import com.waps.ads.AdGroupLayout;
import com.waps.ads.b.c;
import com.waps.ads.c.a;
import java.util.concurrent.TimeUnit;

public class b extends a {
    public b(AdGroupLayout adGroupLayout, c cVar) {
        super(adGroupLayout, cVar);
    }

    public void a() {
        Activity activity;
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null && (activity = (Activity) adGroupLayout.a.get()) != null) {
            switch (adGroupLayout.e.a) {
                case 1:
                    Log.d("AdGroup_SDK", "Serving custom type: banner");
                    RelativeLayout relativeLayout = new RelativeLayout(activity);
                    if (adGroupLayout.e.c != null) {
                        ImageView imageView = new ImageView(activity);
                        imageView.setImageDrawable(adGroupLayout.e.c);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                        layoutParams.addRule(13);
                        relativeLayout.addView(imageView, layoutParams);
                        adGroupLayout.pushSubView(relativeLayout);
                        break;
                    } else {
                        adGroupLayout.rotateThreadedNow();
                        return;
                    }
                case ReportPolicy.BATCH_AT_TERMINATE:
                    Log.d("AdGroup_SDK", "Serving custom type: icon");
                    RelativeLayout relativeLayout2 = new RelativeLayout(activity);
                    if (adGroupLayout.e.c != null) {
                        double a = a.a(activity);
                        double a2 = (double) a.a(4, a);
                        double a3 = (double) a.a(6, a);
                        relativeLayout2.setLayoutParams(new FrameLayout.LayoutParams((int) ((double) a.a(320, a)), (int) ((double) a.a(50, a))));
                        ImageView imageView2 = new ImageView(activity);
                        int rgb = Color.rgb(adGroupLayout.d.e, adGroupLayout.d.f, adGroupLayout.d.g);
                        imageView2.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-1, rgb, rgb, rgb}));
                        relativeLayout2.addView(imageView2, new RelativeLayout.LayoutParams(-1, -1));
                        ImageView imageView3 = new ImageView(activity);
                        imageView3.setImageDrawable(adGroupLayout.e.c);
                        imageView3.setId(10);
                        imageView3.setPadding((int) a2, 0, (int) a3, 0);
                        imageView3.setScaleType(ImageView.ScaleType.CENTER);
                        relativeLayout2.addView(imageView3, new RelativeLayout.LayoutParams(-2, -1));
                        ImageView imageView4 = new ImageView(activity);
                        imageView4.setImageDrawable(new BitmapDrawable(getClass().getResourceAsStream("/com/waps/ads/assets/ad_frame.gif")));
                        imageView4.setPadding((int) a2, 0, (int) a3, 0);
                        imageView4.setScaleType(ImageView.ScaleType.CENTER);
                        relativeLayout2.addView(imageView4, new RelativeLayout.LayoutParams(-2, -1));
                        TextView textView = new TextView(activity);
                        textView.setText(adGroupLayout.e.f);
                        textView.setTypeface(Typeface.DEFAULT_BOLD, 1);
                        textView.setTextColor(Color.rgb(adGroupLayout.d.a, adGroupLayout.d.b, adGroupLayout.d.c));
                        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
                        layoutParams2.addRule(1, imageView3.getId());
                        layoutParams2.addRule(10);
                        layoutParams2.addRule(12);
                        layoutParams2.addRule(15);
                        layoutParams2.addRule(13);
                        textView.setGravity(16);
                        relativeLayout2.addView(textView, layoutParams2);
                        adGroupLayout.pushSubView(relativeLayout2);
                        break;
                    } else {
                        adGroupLayout.rotateThreadedNow();
                        return;
                    }
                default:
                    Log.w("AdGroup_SDK", "Unknown custom type!");
                    adGroupLayout.rotateThreadedNow();
                    return;
            }
            adGroupLayout.j.resetRollover();
            adGroupLayout.rotateThreadedDelayed();
        }
    }

    public void handle() {
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null) {
            adGroupLayout.c.schedule(new d(this), 0, TimeUnit.SECONDS);
        }
    }
}
