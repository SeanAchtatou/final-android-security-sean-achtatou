package com.waps.ads;

import android.app.ActivityManager;
import android.view.View;
import android.view.ViewGroup;
import com.waps.AnimationType;
import java.lang.ref.WeakReference;
import java.util.List;

public class g implements Runnable {
    private WeakReference a;
    private ViewGroup b;
    private View c;
    private String d;

    public g(AdGroupLayout adGroupLayout, View view) {
        this.a = new WeakReference(adGroupLayout);
        this.c = view;
    }

    public g(AdGroupLayout adGroupLayout, ViewGroup viewGroup) {
        this.a = new WeakReference(adGroupLayout);
        this.b = viewGroup;
    }

    public void run() {
        List<ActivityManager.RunningTaskInfo> runningTasks;
        AnimationType animationType = new AnimationType(AdGroupLayout.y);
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.a.get();
        if (adGroupLayout != null && (runningTasks = ((ActivityManager) AdGroupLayout.x.getSystemService("activity")).getRunningTasks(2)) != null && !runningTasks.isEmpty()) {
            this.d = runningTasks.get(0).topActivity.getShortClassName();
            if (!AdGroupLayout.x.getClass().toString().contains(this.d)) {
                return;
            }
            if (this.c != null) {
                adGroupLayout.updateResultsInUi(this.c);
                animationType.startAnimation(adGroupLayout);
                return;
            }
            adGroupLayout.pushSubView(this.b);
            animationType.startAnimation(adGroupLayout);
        }
    }
}
