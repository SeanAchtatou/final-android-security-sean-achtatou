package com.waps.ads;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import com.waps.AppConnect;
import com.waps.ads.b.b;
import com.waps.ads.b.c;
import com.waps.ads.c.a;
import com.waps.n;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdGroupManager {
    private static long j = -1;
    public String a;
    Iterator b;
    public String c;
    public String d;
    public Location e;
    private b f;
    private List g;
    private double h = 0.0d;
    private WeakReference i;

    public AdGroupManager(WeakReference weakReference, String str) {
        Log.i("AdGroup_SDK", "Creating adManager...");
        this.i = weakReference;
        this.a = str;
        this.c = Locale.getDefault().toString();
        Log.d("AdGroup_SDK", "Locale is: " + this.c);
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            StringBuffer stringBuffer = new StringBuffer("android_id");
            stringBuffer.append("AdGroup");
            this.d = a.a(instance.digest(stringBuffer.toString().getBytes()));
        } catch (NoSuchAlgorithmException e2) {
            this.d = "00000000000000000000000000000000";
        }
        Log.d("AdGroup_SDK", "Hashed device ID is: " + this.d);
        Log.i("AdGroup_SDK", "Finished creating adManager");
    }

    private String convertStreamToString(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine + "\n");
                } else {
                    try {
                        inputStream.close();
                        return sb.toString();
                    } catch (IOException e2) {
                        Log.e("AdGroup_SDK", "Caught IOException in convertStreamToString()", e2);
                        return null;
                    }
                }
            } catch (IOException e3) {
                Log.e("AdGroup_SDK", "Caught IOException in convertStreamToString()", e3);
                try {
                    inputStream.close();
                    return null;
                } catch (IOException e4) {
                    Log.e("AdGroup_SDK", "Caught IOException in convertStreamToString()", e4);
                    return null;
                }
            } catch (Throwable th) {
                try {
                    inputStream.close();
                    throw th;
                } catch (IOException e5) {
                    Log.e("AdGroup_SDK", "Caught IOException in convertStreamToString()", e5);
                    return null;
                }
            }
        }
    }

    private Drawable fetchImage(String str) {
        try {
            return Drawable.createFromStream((InputStream) new URL(str).getContent(), "src");
        } catch (Exception e2) {
            Log.e("AdGroup_SDK", "Unable to fetchImage(): ", e2);
            return null;
        }
    }

    private void parseConfigurationString(String str) {
        Log.d("AdGroup_SDK", "Received jsonString: " + str);
        try {
            JSONObject jSONObject = new JSONObject(str);
            parseExtraJson(jSONObject.getJSONObject("extra"));
            parseRationsJson(jSONObject.getJSONArray("rations"));
        } catch (JSONException e2) {
            Log.e("AdGroup_SDK", "Unable to parse response from JSON. This may or may not be fatal.", e2);
            this.f = new b();
        } catch (NullPointerException e3) {
            Log.e("AdGroup_SDK", "Unable to parse response from JSON. This may or may not be fatal.", e3);
            this.f = new b();
        }
    }

    private com.waps.ads.b.a parseCustomJsonString(String str) {
        Log.d("AdGroup_SDK", "Received custom jsonString: " + str);
        com.waps.ads.b.a aVar = new com.waps.ads.b.a();
        JSONObject jSONObject = new JSONObject(str);
        aVar.a = jSONObject.getInt("ad_type");
        aVar.d = jSONObject.getString("img_url");
        aVar.b = jSONObject.getString("redirect_url");
        aVar.f = jSONObject.getString("ad_text");
        try {
            aVar.e = jSONObject.getString("img_url_480x75");
        } catch (JSONException e2) {
            aVar.e = null;
        }
        try {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) ((Context) this.i.get()).getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            if (((double) displayMetrics.density) != 1.5d || aVar.a != 1 || aVar.e == null || aVar.e.length() == 0) {
                aVar.c = fetchImage(aVar.d);
            } else {
                aVar.c = fetchImage(aVar.e);
            }
            return aVar;
        } catch (JSONException e3) {
            Log.e("AdGroup_SDK", "Caught JSONException in parseCustomJsonString()", e3);
            return null;
        }
    }

    private void parseExtraJson(JSONObject jSONObject) {
        b bVar = new b();
        try {
            bVar.i = jSONObject.getInt("cycle_time");
            bVar.k = jSONObject.getInt("transition");
            JSONObject jSONObject2 = jSONObject.getJSONObject("background_color_rgb");
            bVar.e = jSONObject2.getInt("red");
            bVar.f = jSONObject2.getInt("green");
            bVar.g = jSONObject2.getInt("blue");
            bVar.h = jSONObject2.getInt("alpha") * 255;
            JSONObject jSONObject3 = jSONObject.getJSONObject("text_color_rgb");
            bVar.a = jSONObject3.getInt("red");
            bVar.b = jSONObject3.getInt("green");
            bVar.c = jSONObject3.getInt("blue");
            bVar.d = jSONObject3.getInt("alpha") * 255;
        } catch (JSONException e2) {
            Log.e("AdGroup_SDK", "Exception in parsing config.extra JSON. This may or may not be fatal.", e2);
        }
        this.f = bVar;
    }

    private void parseRationsJson(JSONArray jSONArray) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        this.h = 0.0d;
        int i2 = 0;
        while (i2 < jSONArray.length()) {
            try {
                JSONObject jSONObject = jSONArray.getJSONObject(i2);
                if (jSONObject != null) {
                    c cVar = new c();
                    cVar.a = jSONObject.getString("nid");
                    cVar.b = jSONObject.getInt("type");
                    cVar.c = jSONObject.getString("nname");
                    cVar.d = (double) jSONObject.getInt("weight");
                    cVar.g = jSONObject.getInt("priority");
                    cVar.e = jSONObject.getString("key");
                    cVar.f = jSONObject.getString("key2");
                    this.h += cVar.d;
                    arrayList.add(cVar);
                    if (cVar.g > 0) {
                        arrayList2.add(cVar);
                    }
                    switch (cVar.b) {
                        case 22:
                            cVar.e = jSONObject.getString("nid");
                            cVar.f = jSONObject.getString("key");
                            continue;
                        case 29:
                            cVar.e = jSONObject.getString("nid");
                            cVar.f = jSONObject.getString("key");
                            continue;
                    }
                }
                i2++;
            } catch (JSONException e2) {
            }
        }
        Collections.sort(arrayList);
        this.g = arrayList;
        this.b = this.g.iterator();
    }

    public static void setConfigExpireTimeout(long j2) {
        j = j2;
    }

    public void fetchConfig() {
        String str;
        String str2;
        String str3;
        String str4;
        HttpHost httpHost;
        HttpResponse execute;
        Context context = (Context) this.i.get();
        if (context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(this.a, 0);
            String string = sharedPreferences.getString("config", null);
            Log.i("AdLayout", "jsonString=  " + string);
            long j2 = sharedPreferences.getLong("timestamp", -1);
            Log.d("AdGroup_SDK", "Prefs{" + this.a + "}: {\"" + "config" + "\": \"" + string + "\", \"" + "timestamp" + "\": " + j2 + "}");
            if (string == null || j == -1 || System.currentTimeMillis() >= j2 + j) {
                Log.i("AdGroup_SDK", "Stored config info not present or expired, fetching fresh data");
                try {
                    DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                    n nVar = new n();
                    NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
                    if (!(activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null || !activeNetworkInfo.getExtraInfo().equals("cmwap"))) {
                        nVar.a(true);
                    }
                    if (!nVar.a()) {
                        execute = defaultHttpClient.execute(new HttpGet(a.a + new AppConnect().getParams(context)));
                    } else {
                        String str5 = a.a + new AppConnect().getParams(context);
                        HttpHost httpHost2 = new HttpHost("10.0.0.172", 80, "http");
                        if (str5.startsWith("http://app")) {
                            HttpHost httpHost3 = new HttpHost("ads.wapx.cn", 80, "http");
                            str4 = str5.replaceAll(" ", "%20").replaceFirst("http://app.wapx.cn", "");
                            httpHost = httpHost3;
                        } else if (str5.startsWith("http://ads")) {
                            HttpHost httpHost4 = new HttpHost("ads.wapx.cn", 80, "http");
                            str4 = str5.replaceAll(" ", "%20").replaceFirst("http://ads.wapx.cn", "");
                            httpHost = httpHost4;
                        } else {
                            str4 = str5;
                            httpHost = null;
                        }
                        HttpGet httpGet = new HttpGet(str4);
                        BasicHttpParams basicHttpParams = new BasicHttpParams();
                        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 15000);
                        HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
                        DefaultHttpClient defaultHttpClient2 = new DefaultHttpClient(basicHttpParams);
                        defaultHttpClient2.getParams().setParameter("http.route.default-proxy", httpHost2);
                        execute = defaultHttpClient2.execute(httpHost, httpGet);
                    }
                    Log.d("AdGroup_SDK", execute.getStatusLine().toString());
                    HttpEntity entity = execute.getEntity();
                    if (entity != null) {
                        str = convertStreamToString(entity.getContent());
                        try {
                            SharedPreferences.Editor edit = sharedPreferences.edit();
                            edit.putString("config", str);
                            edit.putLong("timestamp", System.currentTimeMillis());
                            edit.commit();
                        } catch (ClientProtocolException e2) {
                            Throwable th = e2;
                            str3 = str;
                            e = th;
                            Log.e("AdGroup_SDK", "Caught ClientProtocolException in fetchConfig()", e);
                            str = str3;
                            parseConfigurationString(str);
                        } catch (Exception e3) {
                            Exception exc = e3;
                            str2 = str;
                            e = exc;
                            Log.e("AdGroup_SDK", "Caught IOException in fetchConfig()", e);
                            str = str2;
                            parseConfigurationString(str);
                        }
                    }
                    str = string;
                } catch (ClientProtocolException e4) {
                    e = e4;
                    str3 = string;
                    Log.e("AdGroup_SDK", "Caught ClientProtocolException in fetchConfig()", e);
                    str = str3;
                    parseConfigurationString(str);
                } catch (Exception e5) {
                    e = e5;
                    str2 = string;
                    Log.e("AdGroup_SDK", "Caught IOException in fetchConfig()", e);
                    str = str2;
                    parseConfigurationString(str);
                }
            } else {
                Log.i("AdGroup_SDK", "Using stored config data");
                str = string;
            }
            parseConfigurationString(str);
        }
    }

    public com.waps.ads.b.a getCustom(String str) {
        String str2;
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        if (this.f.j == 1) {
            this.e = getLocation();
            if (this.e != null) {
                str2 = String.format("&location=%f,%f&location_timestamp=%d", Double.valueOf(this.e.getLatitude()), Double.valueOf(this.e.getLongitude()), Long.valueOf(this.e.getTime()));
            } else {
                str2 = "";
            }
        } else {
            this.e = null;
            str2 = "";
        }
        try {
            HttpResponse execute = defaultHttpClient.execute(new HttpGet(String.format("http://ads.waps.cn/action/adgroup/ad_custom?", this.a, str, this.d, this.c, str2, 100)));
            Log.d("AdGroup_SDK", execute.getStatusLine().toString());
            HttpEntity entity = execute.getEntity();
            if (entity != null) {
                return parseCustomJsonString(convertStreamToString(entity.getContent()));
            }
        } catch (ClientProtocolException e2) {
            Log.e("AdGroup_SDK", "Caught ClientProtocolException in getCustom()", e2);
        } catch (IOException e3) {
            Log.e("AdGroup_SDK", "Caught IOException in getCustom()", e3);
        }
        return null;
    }

    public b getExtra() {
        if (this.h > 0.0d) {
            return this.f;
        }
        Log.i("AdGroup_SDK", "Sum of ration weights is 0 - no ads to be shown");
        return null;
    }

    public Location getLocation() {
        if (this.i == null) {
            return null;
        }
        Context context = (Context) this.i.get();
        if (context == null) {
            return null;
        }
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
            return ((LocationManager) context.getSystemService("location")).getLastKnownLocation("gps");
        }
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
            return ((LocationManager) context.getSystemService("location")).getLastKnownLocation("network");
        }
        return null;
    }

    public c getRation() {
        double nextDouble = new Random().nextDouble() * this.h;
        Log.d("AdGroup_SDK", "Dart is <" + nextDouble + "> of <" + this.h + ">");
        double d2 = 0.0d;
        c cVar = null;
        for (c cVar2 : this.g) {
            int i2 = cVar2.b;
            double d3 = cVar2.d + d2;
            if (d3 >= nextDouble) {
                return cVar2;
            }
            d2 = d3;
            cVar = cVar2;
        }
        return cVar;
    }

    public c getRollover() {
        if (this.b == null) {
            return null;
        }
        if (this.b.hasNext()) {
            return (c) this.b.next();
        }
        return null;
    }

    public void resetRollover() {
        this.b = this.g.iterator();
    }
}
