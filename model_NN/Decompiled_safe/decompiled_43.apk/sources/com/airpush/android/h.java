package com.airpush.android;

import android.util.Log;

final class h implements Runnable {
    private /* synthetic */ PushService a;

    h(PushService pushService) {
        this.a = pushService;
    }

    public final void run() {
        try {
            Log.i("AirpushSDK", "Notification Expired");
            this.a.q.cancel(999);
        } catch (Exception e) {
            Airpush.a(this.a.getApplicationContext(), 1800000);
        }
    }
}
