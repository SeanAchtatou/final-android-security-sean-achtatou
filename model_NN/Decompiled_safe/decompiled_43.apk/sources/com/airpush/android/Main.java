package com.airpush.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;

public class Main extends Activity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        new Airpush(this, "10256", "airpush", true);
        setContentView(2130903041);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        moveTaskToBack(true);
        return true;
    }
}
