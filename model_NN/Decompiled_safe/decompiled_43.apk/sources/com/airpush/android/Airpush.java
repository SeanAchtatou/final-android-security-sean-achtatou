package com.airpush.android;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Airpush {
    protected static Context a = null;
    private static String b = null;
    private static String c = null;
    private static String d = null;
    private static boolean e = false;
    private static int g = 17301620;
    private String f;
    private boolean h;
    private long i = 0;
    private long j = 0;
    /* access modifiers changed from: private */
    public long k = 0;
    private Runnable l = new a(this);
    private Runnable m = new b(this);

    public Airpush() {
    }

    public Airpush(Context context, String str, String str2) {
        try {
            a = context;
            new SetPreferences().a(a, str, str2, e, false, g, true);
            a();
            a(context, str, str2, e, false, g, true);
        } catch (Exception e2) {
        }
    }

    public Airpush(Context context, String str, String str2, boolean z) {
        try {
            e = z;
            a = context;
            new SetPreferences().a(a, str, str2, z, false, g, true);
            a();
            a(context, str, str2, e, false, g, true);
        } catch (Exception e2) {
        }
    }

    private static void a() {
        try {
            if (!a.getSharedPreferences("dataPrefs", 1).equals(null)) {
                SharedPreferences sharedPreferences = a.getSharedPreferences("dataPrefs", 1);
                b = sharedPreferences.getString("appId", "invalid");
                d = sharedPreferences.getString("apikey", "airpush");
                c = sharedPreferences.getString("imei", "invalid");
                e = sharedPreferences.getBoolean("testMode", false);
                g = sharedPreferences.getInt("icon", 17301620);
            }
        } catch (Exception e2) {
        }
    }

    protected static void a(Context context, long j2) {
        Log.i("AirpushSDK", "SDK will restart in " + j2 + " ms.");
        a = context;
        a();
        try {
            Intent intent = new Intent(context, UserDetailsReceiver.class);
            intent.setAction("SetUserInfo");
            intent.putExtra("appId", b);
            intent.putExtra("imei", c);
            intent.putExtra("apikey", d);
            ((AlarmManager) context.getSystemService("alarm")).set(0, System.currentTimeMillis() + (1000 * j2 * 60), PendingIntent.getBroadcast(context, 0, intent, 0));
            Intent intent2 = new Intent(context, MessageReceiver.class);
            intent2.setAction("SetMessageReceiver");
            intent2.putExtra("appId", b);
            intent2.putExtra("imei", c);
            intent2.putExtra("apikey", d);
            intent2.putExtra("testMode", e);
            intent2.putExtra("icon", g);
            ((AlarmManager) context.getSystemService("alarm")).setInexactRepeating(0, System.currentTimeMillis() + j2 + ((long) c.b.intValue()), c.a, PendingIntent.getBroadcast(context, 0, intent2, 0));
        } catch (Exception e2) {
        }
    }

    static /* synthetic */ void a(Airpush airpush) {
        boolean z;
        try {
            if (a.getSharedPreferences("airpushTimePref", 1) != null) {
                airpush.i = System.currentTimeMillis();
                SharedPreferences sharedPreferences = a.getSharedPreferences("airpushTimePref", 1);
                if (sharedPreferences.contains("startTime")) {
                    airpush.j = sharedPreferences.getLong("startTime", 0);
                    airpush.k = (airpush.i - airpush.j) / 60000;
                    if (airpush.k >= ((long) c.c.intValue())) {
                        z = true;
                    } else {
                        new Handler().post(airpush.m);
                        z = false;
                    }
                } else {
                    SharedPreferences.Editor edit = a.getSharedPreferences("airpushTimePref", 2).edit();
                    airpush.j = System.currentTimeMillis();
                    edit.putLong("startTime", airpush.j);
                    edit.commit();
                    z = true;
                }
            } else {
                z = true;
            }
            if (z) {
                Intent intent = new Intent(a, UserDetailsReceiver.class);
                intent.setAction("SetUserInfo");
                intent.putExtra("appId", b);
                intent.putExtra("imei", c);
                intent.putExtra("apikey", d);
                ((AlarmManager) a.getSystemService("alarm")).set(0, System.currentTimeMillis(), PendingIntent.getBroadcast(a, 0, intent, 0));
                Intent intent2 = new Intent(a, MessageReceiver.class);
                intent2.setAction("SetMessageReceiver");
                intent2.putExtra("appId", b);
                intent2.putExtra("imei", c);
                intent2.putExtra("apikey", d);
                intent2.putExtra("testMode", e);
                intent2.putExtra("icon", g);
                ((AlarmManager) a.getSystemService("alarm")).setInexactRepeating(0, System.currentTimeMillis() + ((long) c.b.intValue()), c.a, PendingIntent.getBroadcast(a, 0, intent2, 0));
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Context context, String str, String str2, boolean z, boolean z2, int i2, boolean z3) {
        try {
            this.h = z3;
            SharedPreferences.Editor edit = a.getSharedPreferences("dialogPref", 2).edit();
            edit.putBoolean("ShowDialog", z2);
            edit.putBoolean("ShowAd", this.h);
            edit.commit();
            if (this.h) {
                Log.i("AirpushSDK", "Initialising.....");
                e = z;
                b = str;
                d = str2;
                g = i2;
                c.a();
                this.f = ((TelephonyManager) a.getSystemService("phone")).getDeviceId();
                try {
                    MessageDigest instance = MessageDigest.getInstance("MD5");
                    instance.update(this.f.getBytes(), 0, this.f.length());
                    c = new BigInteger(1, instance.digest()).toString(16);
                } catch (NoSuchAlgorithmException e2) {
                    e2.printStackTrace();
                }
                new Handler().postDelayed(this.l, 20000);
            }
        } catch (Exception e3) {
        }
    }

    public void disableSdk(Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("sdkPrefs", 2).edit();
        edit.putBoolean("SDKEnabled", false);
        edit.commit();
    }

    public void enableSdk(Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("sdkPrefs", 2).edit();
        edit.putBoolean("SDKEnabled", true);
        edit.commit();
    }

    public boolean isEnabled(Context context) {
        if (context.getSharedPreferences("sdkPrefs", 1).equals(null)) {
            return true;
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences("sdkPrefs", 1);
        if (sharedPreferences.contains("SDKEnabled")) {
            return sharedPreferences.getBoolean("SDKEnabled", false);
        }
        return true;
    }
}
