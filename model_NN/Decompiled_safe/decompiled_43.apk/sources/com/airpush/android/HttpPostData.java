package com.airpush.android;

import android.content.Context;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public final class HttpPostData {
    protected static long a = 1800000;
    private static String b;
    private static Context c;
    private static BasicHttpParams d;
    private static int e;
    private static int f;
    private static DefaultHttpClient g;
    private static HttpEntity h;
    private static BasicHttpResponse i;
    private static HttpPost j;

    protected static String a(String str, Context context) {
        if (c.a(context)) {
            c = context;
            try {
                if (c.a(context)) {
                    HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
                    httpURLConnection.setRequestMethod("GET");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    httpURLConnection.setConnectTimeout(3000);
                    httpURLConnection.connect();
                    if (httpURLConnection.getResponseCode() == 200) {
                        StringBuffer stringBuffer = new StringBuffer();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                        while (true) {
                            String readLine = bufferedReader.readLine();
                            if (readLine == null) {
                                return stringBuffer.toString();
                            }
                            stringBuffer.append(readLine);
                        }
                    }
                }
            } catch (MalformedURLException e2) {
                Airpush.a(c, 1800000);
            } catch (IOException e3) {
                Airpush.a(c, 1800000);
            } catch (Exception e4) {
                Airpush.a(c, 1800000);
            }
            return "";
        }
        Airpush.a(context, a);
        return "";
    }

    protected static HttpEntity a(List list, Context context) {
        if (c.a(context)) {
            c = context;
            try {
                HttpPost httpPost = new HttpPost("http://api.airpush.com/v2/api.php");
                j = httpPost;
                httpPost.setEntity(new UrlEncodedFormEntity(list));
                d = new BasicHttpParams();
                e = 3000;
                HttpConnectionParams.setConnectionTimeout(d, e);
                f = 3000;
                HttpConnectionParams.setSoTimeout(d, f);
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient(d);
                g = defaultHttpClient;
                BasicHttpResponse execute = defaultHttpClient.execute(j);
                i = execute;
                HttpEntity entity = execute.getEntity();
                h = entity;
                return entity;
            } catch (Exception e2) {
                Airpush.a(c, 1800000);
                return null;
            }
        } else {
            Airpush.a(context, a);
            return null;
        }
    }

    protected static HttpEntity a(List list, boolean z, Context context) {
        if (c.a(context)) {
            c = context;
            if (z) {
                try {
                    b = "http://api.airpush.com/testmsg2.php";
                } catch (Exception e2) {
                    Airpush.a(c, 1800000);
                    return null;
                }
            } else {
                b = "http://api.airpush.com/v2/api.php";
            }
            HttpPost httpPost = new HttpPost(b);
            j = httpPost;
            httpPost.setEntity(new UrlEncodedFormEntity(list));
            d = new BasicHttpParams();
            e = 3000;
            HttpConnectionParams.setConnectionTimeout(d, e);
            f = 3000;
            HttpConnectionParams.setSoTimeout(d, f);
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient(d);
            g = defaultHttpClient;
            BasicHttpResponse execute = defaultHttpClient.execute(j);
            i = execute;
            HttpEntity entity = execute.getEntity();
            h = entity;
            return entity;
        }
        Airpush.a(context, a);
        return null;
    }
}
