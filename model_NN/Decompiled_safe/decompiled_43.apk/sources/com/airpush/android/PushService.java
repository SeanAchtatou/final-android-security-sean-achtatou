package com.airpush.android;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;
import org.apache.http.HttpEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class PushService extends Service {
    private static int A = 17301620;
    private static String b = null;
    private static String c = null;
    private static String f = null;
    private static Context m = null;
    private static String p = null;
    private static boolean s = false;
    private Runnable B = new h(this);
    private List a = null;
    private String d = null;
    private String e = null;
    private String g = null;
    private String h = null;
    private String i = null;
    private String j = null;
    private String k = null;
    private String l = "http://api.airpush.com/redirect.php?market=";
    private String n;
    private long o;
    /* access modifiers changed from: private */
    public NotificationManager q;
    private String r = null;
    private String t = null;
    private String u;
    private Long v;
    private String w;
    private long x;
    private String y;
    private String z;

    private static long a(String str, String str2) {
        try {
            return new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse(str).getTime() - new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse(str2).getTime();
        } catch (ParseException e2) {
            Airpush.a(m, 1800000);
            Log.e("AirpushSDK", "Date Diff .....Failed");
            return 0;
        }
    }

    private static String a(JSONObject jSONObject) {
        try {
            return jSONObject.getString("adtype");
        } catch (JSONException e2) {
            return "invalid";
        }
    }

    private void a() {
        Log.i("AirpushSDK", "Receiving.......");
        try {
            this.a = SetPreferences.a(m);
            this.a.add(new BasicNameValuePair("model", "message"));
            this.a.add(new BasicNameValuePair("action", "getmessage"));
            this.a.add(new BasicNameValuePair("APIKEY", p));
            Context context = m;
            String str = b;
            c.a();
            this.t = null;
            HttpEntity a2 = HttpPostData.a(this.a, s, m);
            if (!a2.equals(null)) {
                InputStream content = a2.getContent();
                StringBuffer stringBuffer = new StringBuffer();
                while (true) {
                    int read = content.read();
                    if (read == -1) {
                        this.t = stringBuffer.toString();
                        b(this.t);
                        return;
                    }
                    stringBuffer.append((char) read);
                }
            }
        } catch (Exception e2) {
            Context context2 = m;
            "json" + e2.toString();
            c.a();
            Context context3 = m;
            "Message " + this.t;
            c.a();
            Airpush.a(m, 1800000);
        }
    }

    private static void a(long j2) {
        try {
            if (!m.getSharedPreferences("dataPrefs", 1).equals(null)) {
                SharedPreferences sharedPreferences = m.getSharedPreferences("dataPrefs", 1);
                c = sharedPreferences.getString("appId", "invalid");
                p = sharedPreferences.getString("apikey", "airpush");
                b = sharedPreferences.getString("imei", "invalid");
                s = sharedPreferences.getBoolean("testMode", false);
                A = sharedPreferences.getInt("icon", 17301620);
            }
        } catch (Exception e2) {
        }
        try {
            Intent intent = new Intent(m, MessageReceiver.class);
            intent.setAction("SetMessageReceiver");
            intent.putExtra("appId", c);
            intent.putExtra("apikey", p);
            intent.putExtra("imei", b);
            intent.putExtra("testMode", s);
            ((AlarmManager) m.getSystemService("alarm")).setInexactRepeating(0, System.currentTimeMillis() + j2, c.a, PendingIntent.getBroadcast(m, 0, intent, 0));
        } catch (Exception e3) {
            Airpush.a(m, j2);
        }
    }

    private void a(String str) {
        try {
            this.a = SetPreferences.a(m);
            this.a.add(new BasicNameValuePair("model", "user"));
            this.a.add(new BasicNameValuePair("action", "setuserinfo"));
            this.a.add(new BasicNameValuePair("APIKEY", str));
            this.a.add(new BasicNameValuePair("type", "app"));
            HttpEntity a2 = HttpPostData.a(this.a, m);
            if (!a2.equals(null)) {
                InputStream content = a2.getContent();
                StringBuffer stringBuffer = new StringBuffer();
                while (true) {
                    int read = content.read();
                    if (read == -1) {
                        stringBuffer.toString();
                        Log.i("AirpushSDK", "User Info Sent.");
                        return;
                    }
                    stringBuffer.append((char) read);
                }
            }
        } catch (Exception e2) {
            Log.e("AirpushSDK", "User Info Sending Failed.....");
            Airpush.a(m, 1800000);
        }
    }

    private static String b(JSONObject jSONObject) {
        try {
            return jSONObject.getString("title");
        } catch (JSONException e2) {
            return "New Message";
        }
    }

    private void b() {
        int[] iArr = c.d;
        A = iArr[new Random().nextInt(iArr.length - 1)];
        try {
            if (this.n.equals("W") || this.n.equals("A")) {
                if (this.n.equals("A")) {
                    this.e = String.valueOf(this.l) + this.e;
                } else if (this.n.equals("W") && this.e.contains("?")) {
                    this.e = String.valueOf(this.l) + this.e + "&" + c;
                } else if (this.n.equals("W") && !this.e.contains("?")) {
                    this.e = String.valueOf(this.l) + this.e + "?" + c;
                }
                this.i = "settexttracking";
                this.j = "trayDelivered";
                this.a = SetPreferences.a(m);
                this.a.add(new BasicNameValuePair("model", "log"));
                this.a.add(new BasicNameValuePair("action", this.i));
                this.a.add(new BasicNameValuePair("APIKEY", p));
                this.a.add(new BasicNameValuePair("event", this.j));
                this.a.add(new BasicNameValuePair("campId", this.g));
                this.a.add(new BasicNameValuePair("creativeId", this.h));
                if (!s) {
                    HttpPostData.a(this.a, m);
                }
                this.q = (NotificationManager) m.getSystemService("notification");
                String str = this.d;
                String str2 = this.r;
                String str3 = this.d;
                Notification notification = new Notification(A, str, System.currentTimeMillis());
                if (m.getPackageManager().checkPermission("android.permission.VIBRATE", getApplicationContext().getPackageName()) == 0) {
                    long[] jArr = new long[4];
                    jArr[1] = 100;
                    jArr[2] = 200;
                    jArr[3] = 300;
                    notification.vibrate = jArr;
                }
                notification.ledARGB = -65536;
                notification.ledOffMS = 300;
                notification.ledOnMS = 300;
                Intent intent = new Intent(m, PushAds.class);
                intent.addFlags(268435456);
                intent.setAction("Web And App");
                SharedPreferences.Editor edit = m.getSharedPreferences("airpushNotificationPref", 2).edit();
                edit.putString("appId", c);
                edit.putString("apikey", p);
                edit.putString("url", this.e);
                edit.putString("adType", this.n);
                edit.putString("tray", "trayClicked");
                edit.putString("campId", this.g);
                edit.putString("creativeId", this.h);
                edit.putString("header", this.z);
                edit.commit();
                intent.putExtra("appId", c);
                intent.putExtra("apikey", p);
                intent.putExtra("adType", this.n);
                intent.putExtra("url", this.e);
                intent.putExtra("campId", this.g);
                intent.putExtra("creativeId", this.h);
                intent.putExtra("tray", "trayClicked");
                intent.putExtra("header", this.z);
                PendingIntent activity = PendingIntent.getActivity(m.getApplicationContext(), 0, intent, 268435456);
                notification.defaults |= 4;
                notification.flags |= 16;
                notification.setLatestEventInfo(m, str2, str3, activity);
                notification.contentIntent = activity;
                this.q.notify(999, notification);
                new Handler().postDelayed(this.B, this.o * 1000);
                Log.i("AirpushSDK", "Message Delivered");
            }
            if (this.n.equals("CM")) {
                this.i = "settexttracking";
                this.j = "trayDelivered";
                this.a = SetPreferences.a(m);
                this.a.add(new BasicNameValuePair("model", "log"));
                this.a.add(new BasicNameValuePair("action", this.i));
                this.a.add(new BasicNameValuePair("APIKEY", p));
                this.a.add(new BasicNameValuePair("event", this.j));
                this.a.add(new BasicNameValuePair("campId", this.g));
                this.a.add(new BasicNameValuePair("creativeId", this.h));
                if (!s) {
                    HttpPostData.a(this.a, m);
                }
                this.q = (NotificationManager) m.getSystemService("notification");
                String str4 = this.d;
                String str5 = this.r;
                String str6 = this.d;
                Notification notification2 = new Notification(A, str4, System.currentTimeMillis());
                if (m.getPackageManager().checkPermission("android.permission.VIBRATE", getApplicationContext().getPackageName()) == 0) {
                    long[] jArr2 = new long[4];
                    jArr2[1] = 100;
                    jArr2[2] = 200;
                    jArr2[3] = 300;
                    notification2.vibrate = jArr2;
                }
                notification2.defaults = -1;
                notification2.ledARGB = -65536;
                notification2.ledOffMS = 300;
                notification2.ledOnMS = 300;
                Intent intent2 = new Intent(m, PushAds.class);
                intent2.addFlags(268435456);
                intent2.setAction("CM");
                SharedPreferences.Editor edit2 = m.getSharedPreferences("airpushNotificationPref", 2).edit();
                edit2.putString("appId", c);
                edit2.putString("apikey", p);
                edit2.putString("sms", this.y);
                edit2.putString("number", this.u);
                edit2.putString("adType", this.n);
                edit2.putString("tray", "trayClicked");
                edit2.putString("campId", this.g);
                edit2.putString("creativeId", this.h);
                edit2.commit();
                intent2.putExtra("appId", c);
                intent2.putExtra("apikey", p);
                intent2.putExtra("sms", this.y);
                intent2.putExtra("number", this.u);
                intent2.putExtra("adType", this.n);
                intent2.putExtra("tray", "trayClicked");
                intent2.putExtra("campId", this.g);
                intent2.putExtra("creativeId", this.h);
                PendingIntent activity2 = PendingIntent.getActivity(m.getApplicationContext(), 0, intent2, 268435456);
                notification2.defaults |= 4;
                notification2.flags |= 16;
                notification2.setLatestEventInfo(m, str5, str6, activity2);
                notification2.contentIntent = activity2;
                this.q.notify(999, notification2);
                new Handler().postDelayed(this.B, this.o * 1000);
                Log.i("AirpushSDK", "Notification Delivered");
            }
            if (this.n.equals("CC")) {
                this.i = "settexttracking";
                this.j = "trayDelivered";
                this.a = SetPreferences.a(m);
                this.a.add(new BasicNameValuePair("model", "log"));
                this.a.add(new BasicNameValuePair("action", this.i));
                this.a.add(new BasicNameValuePair("APIKEY", p));
                this.a.add(new BasicNameValuePair("event", this.j));
                this.a.add(new BasicNameValuePair("campId", this.g));
                this.a.add(new BasicNameValuePair("creativeId", this.h));
                if (!s) {
                    HttpPostData.a(this.a, m);
                }
                this.q = (NotificationManager) m.getSystemService("notification");
                String str7 = this.d;
                String str8 = this.r;
                String str9 = this.d;
                Notification notification3 = new Notification(A, str7, System.currentTimeMillis());
                if (m.getPackageManager().checkPermission("android.permission.VIBRATE", getApplicationContext().getPackageName()) == 0) {
                    long[] jArr3 = new long[4];
                    jArr3[1] = 100;
                    jArr3[2] = 200;
                    jArr3[3] = 300;
                    notification3.vibrate = jArr3;
                }
                notification3.defaults = -1;
                notification3.ledARGB = -65536;
                notification3.ledOffMS = 300;
                notification3.ledOnMS = 300;
                Intent intent3 = new Intent(m, PushAds.class);
                intent3.addFlags(268435456);
                intent3.setAction("CC");
                SharedPreferences.Editor edit3 = m.getSharedPreferences("airpushNotificationPref", 2).edit();
                edit3.putString("appId", c);
                edit3.putString("apikey", p);
                edit3.putString("number", this.u);
                edit3.putString("adType", this.n);
                edit3.putString("tray", "trayClicked");
                edit3.putString("campId", this.g);
                edit3.putString("creativeId", this.h);
                edit3.commit();
                intent3.putExtra("appId", c);
                intent3.putExtra("apikey", p);
                intent3.putExtra("number", this.u);
                intent3.putExtra("adType", this.n);
                intent3.putExtra("tray", "trayClicked");
                intent3.putExtra("campId", this.g);
                intent3.putExtra("creativeId", this.h);
                PendingIntent activity3 = PendingIntent.getActivity(m.getApplicationContext(), 0, intent3, 268435456);
                notification3.defaults |= 4;
                notification3.flags |= 16;
                notification3.setLatestEventInfo(m, str8, str9, activity3);
                notification3.contentIntent = activity3;
                this.q.notify(999, notification3);
                new Handler().postDelayed(this.B, this.o * 1000);
                Log.i("AirpushSDK", "Notification Delivered");
            }
        } catch (Exception e2) {
            Airpush.a(m, 1800000);
            Log.i("AirpushSDK", "Message Delivered");
        } finally {
            new Handler().postDelayed(this.B, this.o * 1000);
        }
    }

    /* JADX INFO: finally extract failed */
    private synchronized void b(String str) {
        Context context = m;
        c.a();
        this.v = Long.valueOf(c.a);
        if (str.contains("nextmessagecheck")) {
            try {
                Context context2 = m;
                c.a();
                JSONObject jSONObject = new JSONObject(str);
                this.n = a(jSONObject);
                if (!this.n.equals("invalid")) {
                    if (this.n.equals("W") || this.n.equals("A")) {
                        try {
                            this.r = b(jSONObject);
                            this.d = c(jSONObject);
                            this.e = d(jSONObject);
                            this.g = h(jSONObject);
                            this.z = m(jSONObject);
                            this.h = g(jSONObject);
                            if (!this.g.equals(null) && !this.g.equals("") && !this.h.equals(null) && !this.h.equals("") && !this.e.equals(null) && !this.e.equals("nothing")) {
                                this.v = Long.valueOf(i(jSONObject));
                                if (this.v.longValue() == 0) {
                                    this.v = Long.valueOf(c.a);
                                }
                                this.w = j(jSONObject);
                                this.o = k(jSONObject).longValue();
                                l(jSONObject);
                                if (!this.w.equals(null) && !this.w.equals("0")) {
                                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                                    String format = simpleDateFormat.format(new Date());
                                    Context context3 = m;
                                    this.w.toString();
                                    c.a();
                                    Context context4 = m;
                                    c.a();
                                    this.x = a(this.w.toString(), format);
                                    this.v = Long.valueOf(this.v.longValue() + this.x);
                                } else if (this.w.equals("0")) {
                                    this.x = 0;
                                }
                                b();
                            }
                            a(this.v.longValue());
                        } catch (Exception e2) {
                            Log.e("AirpushSDK", "Web and App Message Parsing.....Failed ");
                            a(this.v.longValue());
                        } catch (Throwable th) {
                            a(this.v.longValue());
                            throw th;
                        }
                    }
                    if (this.n.equals("CC")) {
                        try {
                            this.r = b(jSONObject);
                            this.d = c(jSONObject);
                            this.u = e(jSONObject);
                            this.g = h(jSONObject);
                            this.h = g(jSONObject);
                            if (!this.g.equals(null) && !this.g.equals("") && !this.h.equals(null) && !this.h.equals("")) {
                                this.v = Long.valueOf(i(jSONObject));
                                if (this.v.longValue() == 0) {
                                    this.v = Long.valueOf(c.a);
                                }
                                this.w = j(jSONObject);
                                this.o = k(jSONObject).longValue();
                                l(jSONObject);
                                if (!this.w.equals(null) && !this.w.equals("0")) {
                                    SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    simpleDateFormat2.setTimeZone(TimeZone.getTimeZone("GMT"));
                                    String format2 = simpleDateFormat2.format(new Date());
                                    Context context5 = m;
                                    this.w.toString();
                                    c.a();
                                    Context context6 = m;
                                    c.a();
                                    this.x = a(this.w.toString(), format2);
                                    this.v = Long.valueOf(this.v.longValue() + this.x);
                                } else if (this.w.equals("0")) {
                                    this.x = 0;
                                }
                                if (!this.u.equals(null) && !this.u.equals("0")) {
                                    b();
                                }
                            }
                            a(this.v.longValue());
                        } catch (Exception e3) {
                            Log.e("AirpushSDK", "Click to Call Message Parsing.....Failed ");
                            a(this.v.longValue());
                        } catch (Throwable th2) {
                            a(this.v.longValue());
                            throw th2;
                        }
                    }
                    if (this.n.equals("CM")) {
                        try {
                            this.r = b(jSONObject);
                            this.d = c(jSONObject);
                            this.u = e(jSONObject);
                            this.y = f(jSONObject);
                            this.g = h(jSONObject);
                            this.h = g(jSONObject);
                            if (!this.g.equals(null) && !this.g.equals("") && !this.h.equals(null) && !this.h.equals("")) {
                                this.v = Long.valueOf(i(jSONObject));
                                if (this.v.longValue() == 0) {
                                    this.v = Long.valueOf(c.a);
                                }
                                this.w = j(jSONObject);
                                this.o = k(jSONObject).longValue();
                                l(jSONObject);
                                if (!this.w.equals(null) && !this.w.equals("0")) {
                                    SimpleDateFormat simpleDateFormat3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    simpleDateFormat3.setTimeZone(TimeZone.getTimeZone("GMT"));
                                    String format3 = simpleDateFormat3.format(new Date());
                                    Context context7 = m;
                                    this.w.toString();
                                    c.a();
                                    Context context8 = m;
                                    c.a();
                                    this.x = a(this.w.toString(), format3);
                                    this.v = Long.valueOf(this.v.longValue() + this.x);
                                } else if (this.w.equals("0")) {
                                    this.x = 0;
                                }
                                if (!this.u.equals(null) && !this.u.equals("0")) {
                                    b();
                                }
                            }
                            a(this.v.longValue());
                        } catch (Exception e4) {
                            Log.e("AirpushSDK", "Click to SMS Message Parsing.....Failed ");
                            a(this.v.longValue());
                        } catch (Throwable th3) {
                            a(this.v.longValue());
                            throw th3;
                        }
                    }
                }
                a(this.v.longValue());
            } catch (JSONException e5) {
                Log.e("AirpushSDK", "Message Parsing.....Failed : " + e5.toString());
                a(this.v.longValue());
            } catch (Exception e6) {
                Log.e("AirpushSDK", "Message Parsing.....Error : " + e6.toString());
                a(this.v.longValue());
            } catch (Throwable th4) {
                a(this.v.longValue());
                throw th4;
            }
        }
        return;
    }

    private static String c(JSONObject jSONObject) {
        try {
            return jSONObject.getString("text");
        } catch (JSONException e2) {
            return "Click here for details!";
        }
    }

    private static String d(JSONObject jSONObject) {
        try {
            return jSONObject.getString("url");
        } catch (JSONException e2) {
            return "nothing";
        }
    }

    private static String e(JSONObject jSONObject) {
        try {
            return jSONObject.getString("number");
        } catch (JSONException e2) {
            return "0";
        }
    }

    private static String f(JSONObject jSONObject) {
        try {
            return jSONObject.getString("sms");
        } catch (JSONException e2) {
            return "";
        }
    }

    private static String g(JSONObject jSONObject) {
        try {
            return jSONObject.getString("creativeid");
        } catch (JSONException e2) {
            return "";
        }
    }

    private static String h(JSONObject jSONObject) {
        try {
            return jSONObject.getString("campaignid");
        } catch (JSONException e2) {
            return "";
        }
    }

    private static long i(JSONObject jSONObject) {
        try {
            return Long.valueOf(Long.parseLong(jSONObject.get("nextmessagecheck").toString()) * 1000).longValue();
        } catch (JSONException e2) {
            return c.a;
        }
    }

    private static String j(JSONObject jSONObject) {
        try {
            return jSONObject.getString("delivery_time");
        } catch (JSONException e2) {
            return "0";
        }
    }

    private static Long k(JSONObject jSONObject) {
        try {
            return Long.valueOf(jSONObject.getLong("expirytime"));
        } catch (JSONException e2) {
            return Long.valueOf(Long.parseLong("86400000"));
        }
    }

    private static String l(JSONObject jSONObject) {
        try {
            return jSONObject.getString("adimage");
        } catch (JSONException e2) {
            return "http://beta.airpush.com/images/adsthumbnail/48.png";
        }
    }

    private static String m(JSONObject jSONObject) {
        try {
            return jSONObject.getString("header");
        } catch (JSONException e2) {
            return "Advertisment";
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onDestroy() {
        super.onDestroy();
        Log.i("AirpushSDK", "Service Finished");
    }

    public void onLowMemory() {
        super.onLowMemory();
        Log.i("AirpushSDK", "Low On Memory");
    }

    public void onStart(Intent intent, int i2) {
        Integer valueOf = Integer.valueOf(i2);
        try {
            c = intent.getStringExtra("appId");
            f = intent.getStringExtra("type");
            p = intent.getStringExtra("apikey");
            if (f.equals("PostAdValues")) {
                this.n = intent.getStringExtra("adType");
                if (this.n.equals("CC")) {
                    if (getApplicationContext().getSharedPreferences("airpushNotificationPref", 1) != null) {
                        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("airpushNotificationPref", 1);
                        c = sharedPreferences.getString("appId", intent.getStringExtra("appId"));
                        p = sharedPreferences.getString("apikey", intent.getStringExtra("apikey"));
                        sharedPreferences.getString("number", intent.getStringExtra("number"));
                        this.g = sharedPreferences.getString("campId", intent.getStringExtra("campId"));
                        this.h = sharedPreferences.getString("creativeId", intent.getStringExtra("creativeId"));
                    } else {
                        c = intent.getStringExtra("appId");
                        p = intent.getStringExtra("apikey");
                        this.g = intent.getStringExtra("campId");
                        this.h = intent.getStringExtra("creativeId");
                        intent.getStringExtra("number");
                    }
                    this.a = SetPreferences.a(getApplicationContext());
                    this.a.add(new BasicNameValuePair("model", "log"));
                    this.a.add(new BasicNameValuePair("action", "settexttracking"));
                    this.a.add(new BasicNameValuePair("APIKEY", p));
                    this.a.add(new BasicNameValuePair("event", "TrayClicked"));
                    this.a.add(new BasicNameValuePair("campId", this.g));
                    this.a.add(new BasicNameValuePair("creativeId", this.h));
                    if (!s) {
                        HttpPostData.a(this.a, getApplicationContext());
                    }
                }
                if (this.n.equals("CM")) {
                    if (getApplicationContext().getSharedPreferences("airpushNotificationPref", 1) != null) {
                        SharedPreferences sharedPreferences2 = getApplicationContext().getSharedPreferences("airpushNotificationPref", 1);
                        c = sharedPreferences2.getString("appId", intent.getStringExtra("appId"));
                        p = sharedPreferences2.getString("apikey", intent.getStringExtra("apikey"));
                        sharedPreferences2.getString("sms", intent.getStringExtra("sms"));
                        this.g = sharedPreferences2.getString("campId", intent.getStringExtra("campId"));
                        this.h = sharedPreferences2.getString("creativeId", intent.getStringExtra("creativeId"));
                        sharedPreferences2.getString("number", intent.getStringExtra("number"));
                    } else {
                        c = intent.getStringExtra("appId");
                        p = intent.getStringExtra("apikey");
                        this.g = intent.getStringExtra("campId");
                        this.h = intent.getStringExtra("creativeId");
                        intent.getStringExtra("sms");
                        intent.getStringExtra("number");
                    }
                    this.a = SetPreferences.a(getApplicationContext());
                    this.a.add(new BasicNameValuePair("model", "log"));
                    this.a.add(new BasicNameValuePair("action", "settexttracking"));
                    this.a.add(new BasicNameValuePair("APIKEY", p));
                    this.a.add(new BasicNameValuePair("event", "TrayClicked"));
                    this.a.add(new BasicNameValuePair("campId", this.g));
                    this.a.add(new BasicNameValuePair("creativeId", this.h));
                    if (!s) {
                        HttpPostData.a(this.a, getApplicationContext());
                    }
                }
                if (this.n.equals("W") || this.n.equals("A")) {
                    if (getApplicationContext().getSharedPreferences("airpushNotificationPref", 1) != null) {
                        SharedPreferences sharedPreferences3 = getApplicationContext().getSharedPreferences("airpushNotificationPref", 1);
                        c = sharedPreferences3.getString("appId", intent.getStringExtra("appId"));
                        p = sharedPreferences3.getString("apikey", intent.getStringExtra("apikey"));
                        sharedPreferences3.getString("url", intent.getStringExtra("url"));
                        this.g = sharedPreferences3.getString("campId", intent.getStringExtra("campId"));
                        this.h = sharedPreferences3.getString("creativeId", intent.getStringExtra("creativeId"));
                        this.z = sharedPreferences3.getString("header", intent.getStringExtra("header"));
                    } else {
                        c = intent.getStringExtra("appId");
                        p = intent.getStringExtra("apikey");
                        this.g = intent.getStringExtra("campId");
                        this.h = intent.getStringExtra("creativeId");
                        intent.getStringExtra("url");
                        this.z = intent.getStringExtra("header");
                    }
                    this.a = SetPreferences.a(getApplicationContext());
                    this.a.add(new BasicNameValuePair("model", "log"));
                    this.a.add(new BasicNameValuePair("action", "settexttracking"));
                    this.a.add(new BasicNameValuePair("APIKEY", p));
                    this.a.add(new BasicNameValuePair("event", "TrayClicked"));
                    this.a.add(new BasicNameValuePair("campId", this.g));
                    this.a.add(new BasicNameValuePair("creativeId", this.h));
                    if (!s) {
                        HttpPostData.a(this.a, getApplicationContext());
                    }
                }
            } else if (f.equals("userInfo")) {
                Context context = UserDetailsReceiver.a;
                m = context;
                if (!context.getSharedPreferences("dataPrefs", 1).equals(null)) {
                    b = m.getSharedPreferences("dataPrefs", 1).getString("imei", "invalid");
                }
                Context context2 = m;
                String str = c;
                a(p);
            } else if (f.equals("message")) {
                Context context3 = MessageReceiver.a;
                m = context3;
                if (!context3.getSharedPreferences("dataPrefs", 1).equals(null)) {
                    b = m.getSharedPreferences("dataPrefs", 1).getString("imei", "invalid");
                }
                s = intent.getBooleanExtra("testMode", false);
                A = intent.getIntExtra("icon", 17301620);
                a();
            } else if (f.equals("delivery")) {
                m = DeliveryReceiver.a;
                this.n = intent.getStringExtra("adType");
                if (this.n.equals("W")) {
                    c = intent.getStringExtra("appId");
                    this.e = intent.getStringExtra("link");
                    this.d = intent.getStringExtra("text");
                    this.r = intent.getStringExtra("title");
                    intent.getStringExtra("imageurl");
                    this.o = intent.getLongExtra("expiry_time", 60);
                    this.z = intent.getStringExtra("header");
                    this.g = intent.getStringExtra("campId");
                    this.h = intent.getStringExtra("creativeId");
                    Context context4 = m;
                    c.a();
                    b();
                }
                if (this.n.equals("A")) {
                    c = intent.getStringExtra("appId");
                    this.e = intent.getStringExtra("link");
                    this.d = intent.getStringExtra("text");
                    this.r = intent.getStringExtra("title");
                    intent.getStringExtra("imageurl");
                    this.o = intent.getLongExtra("expiry_time", 60);
                    this.g = intent.getStringExtra("campId");
                    this.h = intent.getStringExtra("creativeId");
                    Context context5 = m;
                    c.a();
                    b();
                }
                if (this.n.equals("CC")) {
                    c = intent.getStringExtra("appId");
                    this.u = intent.getStringExtra("number");
                    this.d = intent.getStringExtra("text");
                    this.r = intent.getStringExtra("title");
                    intent.getStringExtra("imageurl");
                    this.o = intent.getLongExtra("expiry_time", 60);
                    this.g = intent.getStringExtra("campId");
                    this.h = intent.getStringExtra("creativeId");
                    Context context6 = m;
                    c.a();
                    b();
                }
                if (this.n.equals("CM")) {
                    c = intent.getStringExtra("appId");
                    this.u = intent.getStringExtra("number");
                    this.y = intent.getStringExtra("sms");
                    this.d = intent.getStringExtra("text");
                    this.r = intent.getStringExtra("title");
                    intent.getStringExtra("imageurl");
                    this.o = intent.getLongExtra("expiry_time", 60);
                    this.g = intent.getStringExtra("campId");
                    this.h = intent.getStringExtra("creativeId");
                    Context context7 = m;
                    c.a();
                    b();
                }
            }
            if (valueOf != null) {
                stopSelf(i2);
            }
        } catch (Exception e2) {
            new Airpush(getApplicationContext(), c, "airpush");
            Log.e("AirpushSDK", "Service Error");
            if (valueOf != null) {
                stopSelf(i2);
            }
        } catch (Throwable th) {
            if (valueOf != null) {
                stopSelf(i2);
            }
            throw th;
        }
    }

    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }
}
