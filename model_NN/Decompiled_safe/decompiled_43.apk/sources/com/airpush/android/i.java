package com.airpush.android;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

public final class i implements LocationListener {
    public i(SetPreferences setPreferences) {
    }

    public final void onLocationChanged(Location location) {
        try {
            SetPreferences.u = String.valueOf(location.getLongitude());
            SetPreferences.v = String.valueOf(location.getLatitude());
        } catch (Exception e) {
        }
    }

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
