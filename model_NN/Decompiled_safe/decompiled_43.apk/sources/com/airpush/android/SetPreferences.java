package com.airpush.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.webkit.WebView;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class SetPreferences {
    private static JSONObject a = null;
    private static int d;
    private static String f;
    private static String g;
    private static boolean h;
    private static String i = "3.4";
    private static String j = "0";
    private static String k = "00";
    private static String l = "invalid";
    private static String m = "0";
    private static String n = "0";
    private static String o = "0";
    private static String p = "0";
    private static String q = "0";
    private static String r = "0";
    private static String s = "0";
    private static String t = "0";
    /* access modifiers changed from: private */
    public static String u = "0";
    /* access modifiers changed from: private */
    public static String v = "0";
    private static Context w;
    private static List x;
    private static String y;
    private String b = "0";
    private boolean c;
    private boolean e;

    protected static List a(Context context) {
        try {
            if (!context.getSharedPreferences("dataPrefs", 1).equals(null)) {
                SharedPreferences sharedPreferences = context.getSharedPreferences("dataPrefs", 1);
                r = sharedPreferences.getString("appId", "invalid");
                s = sharedPreferences.getString("apikey", "airpush");
                j = sharedPreferences.getString("imei", "invalid");
                t = sharedPreferences.getString("token", "invalid");
                k = new Date().toString();
                l = sharedPreferences.getString("packageName", "invalid");
                m = sharedPreferences.getString("version", "invalid");
                n = sharedPreferences.getString("carrier", "invalid");
                o = sharedPreferences.getString("networkOperator", "invalid");
                p = sharedPreferences.getString("phoneModel", "invalid");
                q = sharedPreferences.getString("manufacturer", "invalid");
                u = sharedPreferences.getString("longitude", "invalid");
                v = sharedPreferences.getString("latitude", "invalid");
                i = sharedPreferences.getString("sdkversion", "3.4");
                g = sharedPreferences.getString("connectionType", "0");
                h = sharedPreferences.getBoolean("testMode", false);
                f = sharedPreferences.getString("useragent", "Default");
                d = sharedPreferences.getInt("icon", 17301514);
            } else {
                l = w.getPackageName();
                String a2 = HttpPostData.a("http://api.airpush.com/model/user/getappinfo.php?packageName=" + l, w);
                y = a2;
                r = c(a2);
                s = d(y);
            }
        } catch (Exception e2) {
        }
        try {
            ArrayList arrayList = new ArrayList();
            x = arrayList;
            arrayList.add(new BasicNameValuePair("apikey", s));
            x.add(new BasicNameValuePair("appId", r));
            x.add(new BasicNameValuePair("imei", j));
            x.add(new BasicNameValuePair("token", t));
            x.add(new BasicNameValuePair("request_timestamp", k));
            x.add(new BasicNameValuePair("packageName", l));
            x.add(new BasicNameValuePair("version", m));
            x.add(new BasicNameValuePair("carrier", n));
            x.add(new BasicNameValuePair("networkOperator", o));
            x.add(new BasicNameValuePair("phoneModel", p));
            x.add(new BasicNameValuePair("manufacturer", q));
            x.add(new BasicNameValuePair("longitude", u));
            x.add(new BasicNameValuePair("latitude", v));
            x.add(new BasicNameValuePair("sdkversion", i));
            x.add(new BasicNameValuePair("wifi", g));
            x.add(new BasicNameValuePair("useragent", f));
        } catch (Exception e3) {
        }
        return x;
    }

    private static String c(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            a = jSONObject;
            return jSONObject.getString("appid");
        } catch (JSONException e2) {
            return "invalid Id";
        }
    }

    private static String d(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            a = jSONObject;
            return jSONObject.getString("authkey");
        } catch (JSONException e2) {
            return "invalid key";
        }
    }

    public static boolean isEnabled(Context context) {
        if (context.getSharedPreferences("sdkPrefs", 1).equals(null)) {
            return true;
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences("sdkPrefs", 1);
        if (sharedPreferences.contains("SDKEnabled")) {
            return sharedPreferences.getBoolean("SDKEnabled", false);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a(Context context, String str, String str2, boolean z, boolean z2, int i2, boolean z3) {
        w = context;
        r = str;
        s = str2;
        this.c = false;
        d = i2;
        this.e = true;
        h = z;
        f = new WebView(w).getSettings().getUserAgentString();
        g = ((ConnectivityManager) w.getSystemService("connectivity")).getActiveNetworkInfo().getTypeName().equals("WIFI") ? "1" : "0";
        Context context2 = w;
        if (context2.getPackageManager().checkPermission("android.permission.ACCESS_COARSE_LOCATION", context2.getPackageName()) == 0 && context2.getPackageManager().checkPermission("android.permission.ACCESS_FINE_LOCATION", context2.getPackageName()) == 0) {
            LocationManager locationManager = (LocationManager) context2.getSystemService("location");
            Location lastKnownLocation = locationManager.getLastKnownLocation("network");
            if (lastKnownLocation == null) {
                locationManager.requestLocationUpdates("network", 0, 0.0f, new i(this));
            } else {
                u = String.valueOf(lastKnownLocation.getLongitude());
                v = String.valueOf(lastKnownLocation.getLatitude());
            }
        }
        TelephonyManager telephonyManager = (TelephonyManager) w.getSystemService("phone");
        this.b = telephonyManager.getDeviceId();
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(this.b.getBytes(), 0, this.b.length());
            j = new BigInteger(1, instance.digest()).toString(16);
            k = new Date().toString();
            p = Build.MODEL;
            q = Build.MANUFACTURER;
            o = telephonyManager.getNetworkOperatorName();
            n = telephonyManager.getSimOperatorName();
            m = Build.VERSION.SDK;
            l = w.getPackageName();
            t = String.valueOf(j) + r + k;
            MessageDigest instance2 = MessageDigest.getInstance("MD5");
            instance2.update(t.getBytes(), 0, t.length());
            t = new BigInteger(1, instance2.digest()).toString(16);
            try {
                k = new Date().toString();
                SharedPreferences.Editor edit = w.getSharedPreferences("dataPrefs", 2).edit();
                edit.putString("apikey", s);
                edit.putString("appId", r);
                edit.putString("imei", j);
                edit.putString("connectionType", g);
                edit.putString("token", t);
                edit.putString("request_timestamp", k);
                edit.putString("packageName", l);
                edit.putString("version", m);
                edit.putString("carrier", n);
                edit.putString("networkOperator", o);
                edit.putString("phoneModel", p);
                edit.putString("manufacturer", q);
                edit.putString("longitude", u);
                edit.putString("latitude", v);
                edit.putString("sdkversion", "3.4");
                edit.putBoolean("showDialog", this.c);
                edit.putBoolean("showAd", this.e);
                edit.putBoolean("testMode", h);
                edit.putInt("icon", d);
                edit.putString("useragent", f);
                edit.commit();
            } catch (Exception e2) {
            }
        } catch (NoSuchAlgorithmException e3) {
            Log.i("AirpushSDK", "IMEI conversion Error ");
        }
    }
}
