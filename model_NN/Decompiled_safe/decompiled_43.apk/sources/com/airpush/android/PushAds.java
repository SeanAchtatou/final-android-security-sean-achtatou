package com.airpush.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import java.util.List;

public class PushAds extends Activity {
    private String a = null;
    private String b = null;
    private List c = null;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h = null;
    private String i;
    private Context j;
    /* access modifiers changed from: private */
    public boolean k = true;
    private boolean l = false;
    private int m = 17301620;
    /* access modifiers changed from: private */
    public boolean n = true;
    private Intent o;
    private String p;

    public PushAds() {
        new e(this);
    }

    static /* synthetic */ void a(PushAds pushAds) {
        Log.i("AirpushSDK", "Posting data");
        HttpPostData.a((List) null, pushAds.getApplicationContext());
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.j = getApplicationContext();
        this.o = getIntent();
        this.p = this.o.getAction();
        this.d = this.o.getStringExtra("adType");
        if (this.d.equals("ShoWDialog")) {
            this.b = this.o.getStringExtra("appId");
            this.h = this.o.getStringExtra("apikey");
            this.l = this.o.getBooleanExtra("test", false);
            this.m = this.o.getIntExtra("icon", 17301620);
            try {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setCancelable(true);
                builder.setMessage("Support the App developer by enabling ads in the notification tray, limited to 1 per day.");
                builder.setPositiveButton("I Agree", new f(this));
                builder.setNegativeButton("No", new g(this));
                builder.create();
                builder.show();
            } catch (Exception e2) {
                Log.i("AirpushSDK", "Error : " + e2.toString());
            }
        }
        if (this.p.equals("CC")) {
            if (this.d.equals("CC")) {
                Log.i("AirpushSDK", "Pushing Ads.....");
                if (this.j.getSharedPreferences("airpushNotificationPref", 1) != null) {
                    SharedPreferences sharedPreferences = this.j.getSharedPreferences("airpushNotificationPref", 1);
                    this.b = sharedPreferences.getString("appId", this.o.getStringExtra("appId"));
                    this.h = sharedPreferences.getString("apikey", this.o.getStringExtra("apikey"));
                    this.g = sharedPreferences.getString("number", this.o.getStringExtra("number"));
                    sharedPreferences.getString("campId", this.o.getStringExtra("campId"));
                    sharedPreferences.getString("creativeId", this.o.getStringExtra("creativeId"));
                } else {
                    this.b = this.o.getStringExtra("appId");
                    this.h = this.o.getStringExtra("apikey");
                    this.o.getStringExtra("campId");
                    this.o.getStringExtra("creativeId");
                    this.g = this.o.getStringExtra("number");
                }
                Intent intent = new Intent();
                intent.setAction("com.airpush.android.PushServiceStart" + this.b);
                intent.putExtra("type", "PostAdValues");
                intent.putExtras(this.o);
                startService(intent);
                Log.i("AirpushSDK", "Pushing CC Ads.....");
                startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + this.g)));
            }
        } else if (this.p.equals("CM")) {
            if (this.d.equals("CM")) {
                Log.i("AirpushSDK", "Pushing Ads.....");
                if (this.j.getSharedPreferences("airpushNotificationPref", 1) != null) {
                    SharedPreferences sharedPreferences2 = this.j.getSharedPreferences("airpushNotificationPref", 1);
                    this.b = sharedPreferences2.getString("appId", this.o.getStringExtra("appId"));
                    this.h = sharedPreferences2.getString("apikey", this.o.getStringExtra("apikey"));
                    this.e = sharedPreferences2.getString("sms", this.o.getStringExtra("sms"));
                    sharedPreferences2.getString("campId", this.o.getStringExtra("campId"));
                    sharedPreferences2.getString("creativeId", this.o.getStringExtra("creativeId"));
                    this.f = sharedPreferences2.getString("number", this.o.getStringExtra("number"));
                } else {
                    this.b = this.o.getStringExtra("appId");
                    this.h = this.o.getStringExtra("apikey");
                    this.o.getStringExtra("campId");
                    this.o.getStringExtra("creativeId");
                    this.e = this.o.getStringExtra("sms");
                    this.f = this.o.getStringExtra("number");
                }
                Intent intent2 = new Intent();
                intent2.setAction("com.airpush.android.PushServiceStart" + this.b);
                intent2.putExtra("type", "PostAdValues");
                intent2.putExtras(this.o);
                startService(intent2);
                Log.i("AirpushSDK", "Pushing CM Ads.....");
                Intent intent3 = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + this.f));
                intent3.putExtra("sms_body", this.e);
                startActivity(intent3);
            }
        } else if (!this.p.equals("Web And App")) {
        } else {
            if (this.d.equals("W") || this.d.equals("A")) {
                Log.i("AirpushSDK", "Pushing Ads.....");
                if (this.j.getSharedPreferences("airpushNotificationPref", 1) != null) {
                    SharedPreferences sharedPreferences3 = this.j.getSharedPreferences("airpushNotificationPref", 1);
                    this.b = sharedPreferences3.getString("appId", this.o.getStringExtra("appId"));
                    this.h = sharedPreferences3.getString("apikey", this.o.getStringExtra("apikey"));
                    this.a = sharedPreferences3.getString("url", this.o.getStringExtra("url"));
                    sharedPreferences3.getString("campId", this.o.getStringExtra("campId"));
                    sharedPreferences3.getString("creativeId", this.o.getStringExtra("creativeId"));
                    this.i = sharedPreferences3.getString("header", this.o.getStringExtra("header"));
                } else {
                    this.b = this.o.getStringExtra("appId");
                    this.h = this.o.getStringExtra("apikey");
                    this.o.getStringExtra("campId");
                    this.o.getStringExtra("creativeId");
                    this.a = this.o.getStringExtra("url");
                    this.i = this.o.getStringExtra("header");
                }
                Intent intent4 = new Intent();
                intent4.setAction("com.airpush.android.PushServiceStart" + this.b);
                intent4.putExtra("type", "PostAdValues");
                intent4.putExtras(this.o);
                startService(intent4);
                setTitle(this.i);
                String str = this.a;
                Log.i("AirpushSDK", "Pushing Web and App Ads.....");
                CustomWebView customWebView = new CustomWebView(this);
                customWebView.loadUrl(str);
                setContentView(customWebView);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        finish();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        finish();
    }
}
