package com.airpush.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class DeliveryReceiver extends BroadcastReceiver {
    protected static Context a = null;
    private String b = null;
    private String c = null;
    private String d = null;
    private String e = null;
    private String f = null;
    private String g = null;
    private String h;
    private Long i;
    private String j;
    private String k;
    private String l;
    private String m;

    public void onReceive(Context context, Intent intent) {
        a = context;
        try {
            Intent intent2 = new Intent();
            if (c.a(a)) {
                Log.i("AirpushSDK", "Delivering Message");
                if (intent.getAction().equals("setDeliveryReceiverPhone")) {
                    this.g = intent.getStringExtra("apikey");
                    this.b = new String(intent.getStringExtra("appId"));
                    intent.getStringExtra("imei");
                    this.k = new String(intent.getStringExtra("number"));
                    this.j = new String(intent.getStringExtra("title"));
                    this.c = new String(intent.getStringExtra("text"));
                    this.h = new String(intent.getStringExtra("imageurl"));
                    this.i = Long.valueOf(intent.getLongExtra("expiry_time", 60));
                    this.e = intent.getStringExtra("campId");
                    this.f = intent.getStringExtra("creativeId");
                    intent2.setAction("com.airpush.android.PushServiceStart" + this.b);
                    intent2.putExtra("adType", "CC");
                    intent2.putExtra("appId", this.b);
                    intent2.putExtra("type", "delivery");
                    intent2.putExtra("number", this.k);
                    intent2.putExtra("title", this.j);
                    intent2.putExtra("text", this.c);
                    intent2.putExtra("apikey", this.g);
                    intent2.putExtra("imageurl", this.h);
                    intent2.putExtra("expiry_time", this.i);
                } else if (intent.getAction().equals("setDeliveryReceiverSMS")) {
                    this.g = intent.getStringExtra("apikey");
                    this.b = new String(intent.getStringExtra("appId"));
                    intent.getStringExtra("imei");
                    this.k = new String(intent.getStringExtra("number"));
                    this.l = new String(intent.getStringExtra("sms"));
                    this.j = new String(intent.getStringExtra("title"));
                    this.c = new String(intent.getStringExtra("text"));
                    this.h = new String(intent.getStringExtra("imageurl"));
                    this.i = Long.valueOf(intent.getLongExtra("expiry_time", 60));
                    this.e = intent.getStringExtra("campId");
                    this.f = intent.getStringExtra("creativeId");
                    intent2.setAction("com.airpush.android.PushServiceStart" + this.b);
                    intent2.putExtra("adType", "CM");
                    intent2.putExtra("appId", this.b);
                    intent2.putExtra("type", "delivery");
                    intent2.putExtra("number", this.k);
                    intent2.putExtra("title", this.j);
                    intent2.putExtra("text", this.c);
                    intent2.putExtra("sms", this.l);
                    intent2.putExtra("apikey", this.g);
                    intent2.putExtra("imageurl", this.h);
                    intent2.putExtra("expiry_time", this.i);
                    intent2.putExtra("campId", this.e);
                    intent2.putExtra("creativeId", this.f);
                } else if (intent.getAction().equals("setDeliveryReceiverWEB")) {
                    this.g = intent.getStringExtra("apikey");
                    this.b = new String(intent.getStringExtra("appId"));
                    intent.getStringExtra("imei");
                    this.d = new String(intent.getStringExtra("url"));
                    this.j = new String(intent.getStringExtra("title"));
                    this.c = new String(intent.getStringExtra("text"));
                    this.h = new String(intent.getStringExtra("imageurl"));
                    this.m = new String(intent.getStringExtra("header"));
                    this.i = Long.valueOf(intent.getLongExtra("expiry_time", 60));
                    this.e = intent.getStringExtra("campId");
                    this.f = intent.getStringExtra("creativeId");
                    intent2.setAction("com.airpush.android.PushServiceStart" + this.b);
                    intent2.putExtra("adType", "W");
                    intent2.putExtra("appId", this.b);
                    intent2.putExtra("type", "delivery");
                    intent2.putExtra("link", this.d);
                    intent2.putExtra("header", this.m);
                    intent2.putExtra("title", this.j);
                    intent2.putExtra("text", this.c);
                    intent2.putExtra("apikey", this.g);
                    intent2.putExtra("imageurl", this.h);
                    intent2.putExtra("expiry_time", this.i);
                    intent2.putExtra("campId", this.e);
                    intent2.putExtra("creativeId", this.f);
                } else if (intent.getAction().equals("setDeliveryReceiverMARKET")) {
                    this.g = intent.getStringExtra("apikey");
                    this.b = new String(intent.getStringExtra("appId"));
                    intent.getStringExtra("imei");
                    this.d = new String(intent.getStringExtra("url"));
                    this.j = new String(intent.getStringExtra("title"));
                    this.c = new String(intent.getStringExtra("text"));
                    this.h = new String(intent.getStringExtra("imageurl"));
                    this.i = Long.valueOf(intent.getLongExtra("expiry_time", 60));
                    this.e = intent.getStringExtra("campId");
                    this.f = intent.getStringExtra("creativeId");
                    intent2.setAction("com.airpush.android.PushServiceStart" + this.b);
                    intent2.putExtra("adType", "A");
                    intent2.putExtra("appId", this.b);
                    intent2.putExtra("type", "delivery");
                    intent2.putExtra("link", this.d);
                    intent2.putExtra("title", this.j);
                    intent2.putExtra("text", this.c);
                    intent2.putExtra("apikey", this.g);
                    intent2.putExtra("imageurl", this.h);
                    intent2.putExtra("expiry_time", this.i);
                    intent2.putExtra("campId", this.e);
                    intent2.putExtra("creativeId", this.f);
                }
                context.startService(intent2);
            }
        } catch (Exception e2) {
            Log.e("AirpushSDK", "Delivering Message Failed");
        }
    }
}
