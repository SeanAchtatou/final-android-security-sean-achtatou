package com.tencent.util;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.View;

public final class f {
    private final Context a;
    private final Resources b = this.a.getResources();
    private final String c;

    public f(Context context, String str) {
        this.a = context.createPackageContext(str, 3);
        this.c = str;
    }

    public final View a(int i) {
        return View.inflate(this.a, i, null);
    }

    public final String a(String str) {
        int identifier = this.b.getIdentifier(str, "string", this.c);
        if (identifier == 0) {
            return null;
        }
        return this.b.getString(identifier);
    }

    public final Drawable b(int i) {
        if (i == 0) {
            return null;
        }
        return this.b.getDrawable(i);
    }

    public final Drawable b(String str) {
        int identifier = this.b.getIdentifier(str, "drawable", this.c);
        if (identifier == 0) {
            return null;
        }
        return this.b.getDrawable(identifier);
    }

    public final XmlResourceParser c(int i) {
        return this.b.getXml(i);
    }

    public final Bitmap c(String str) {
        int identifier = this.b.getIdentifier(str, "drawable", this.c);
        if (identifier == 0) {
            return null;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inDither = false;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        try {
            return BitmapFactory.decodeResource(this.b, identifier, options);
        } catch (OutOfMemoryError e) {
            return null;
        }
    }
}
