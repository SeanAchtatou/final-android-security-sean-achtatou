package com.tencent.util;

import java.lang.ref.ReferenceQueue;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public final class l extends AbstractMap {
    private static final Object a = new Object();
    /* access modifiers changed from: private */
    public e[] b = new e[16];
    private int c;
    private int d = 16;
    private final float e = 0.75f;
    private final ReferenceQueue f = new ReferenceQueue();
    /* access modifiers changed from: private */
    public volatile int g;
    private transient Set h = null;
    private volatile transient Set i = null;
    private volatile transient Collection j = null;

    static /* synthetic */ Object a(Object obj) {
        if (obj == a) {
            return null;
        }
        return obj;
    }

    private void a() {
        while (true) {
            e eVar = (e) this.f.poll();
            if (eVar != null) {
                int a2 = eVar.b & (this.b.length - 1);
                e eVar2 = this.b[a2];
                e eVar3 = eVar2;
                while (true) {
                    if (eVar2 == null) {
                        break;
                    }
                    e c2 = eVar2.c;
                    if (eVar2 == eVar) {
                        if (eVar3 == eVar) {
                            this.b[a2] = c2;
                        } else {
                            e unused = eVar3.c = c2;
                        }
                        e unused2 = eVar.c = (e) null;
                        Object unused3 = eVar.a = (Object) null;
                        this.c--;
                    } else {
                        eVar3 = eVar2;
                        eVar2 = c2;
                    }
                }
            } else {
                return;
            }
        }
    }

    private void a(int i2) {
        a();
        e[] eVarArr = this.b;
        if (eVarArr.length == 1073741824) {
            this.d = Integer.MAX_VALUE;
            return;
        }
        e[] eVarArr2 = new e[i2];
        a(eVarArr, eVarArr2);
        this.b = eVarArr2;
        if (this.c >= this.d / 2) {
            this.d = (int) (((float) i2) * this.e);
            return;
        }
        a();
        a(eVarArr2, eVarArr);
        this.b = eVarArr;
    }

    private void a(e[] eVarArr, e[] eVarArr2) {
        for (int i2 = 0; i2 < eVarArr.length; i2++) {
            e eVar = eVarArr[i2];
            eVarArr[i2] = null;
            while (eVar != null) {
                e c2 = eVar.c;
                if (eVar.get() == null) {
                    e unused = eVar.c = (e) null;
                    Object unused2 = eVar.a = (Object) null;
                    this.c--;
                } else {
                    int a2 = eVar.b & (eVarArr2.length - 1);
                    e unused3 = eVar.c = eVarArr2[a2];
                    eVarArr2[a2] = eVar;
                }
                eVar = c2;
            }
        }
    }

    static /* synthetic */ e b(l lVar, Object obj) {
        if (obj instanceof Map.Entry) {
            lVar.a();
            e[] eVarArr = lVar.b;
            Map.Entry entry = (Map.Entry) obj;
            Object key = entry.getKey();
            if (key == null) {
                key = a;
            }
            int hashCode = key.hashCode();
            int i2 = hashCode ^ ((hashCode >>> 20) ^ (hashCode >>> 12));
            int i3 = (i2 >>> 4) ^ ((i2 >>> 7) ^ i2);
            int length = (eVarArr.length - 1) & i3;
            e eVar = eVarArr[length];
            e eVar2 = eVar;
            while (eVar != null) {
                e c2 = eVar.c;
                if (i3 != eVar.b || !eVar.equals(entry)) {
                    eVar2 = eVar;
                    eVar = c2;
                } else {
                    lVar.g++;
                    lVar.c--;
                    if (eVar2 == eVar) {
                        eVarArr[length] = c2;
                    } else {
                        e unused = eVar2.c = c2;
                    }
                    return eVar;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public e b(Object obj) {
        Object obj2 = obj == null ? a : obj;
        int hashCode = obj2.hashCode();
        int i2 = hashCode ^ ((hashCode >>> 20) ^ (hashCode >>> 12));
        int i3 = (i2 >>> 4) ^ ((i2 >>> 7) ^ i2);
        a();
        e[] eVarArr = this.b;
        e eVar = eVarArr[(eVarArr.length - 1) & i3];
        while (eVar != null) {
            if (eVar.b == i3) {
                Object obj3 = eVar.get();
                if (obj2 == obj3 || obj2.equals(obj3)) {
                    break;
                }
            }
            eVar = eVar.c;
        }
        return eVar;
    }

    public final void clear() {
        do {
        } while (this.f.poll() != null);
        this.g++;
        e[] eVarArr = this.b;
        for (int i2 = 0; i2 < eVarArr.length; i2++) {
            eVarArr[i2] = null;
        }
        this.c = 0;
        do {
        } while (this.f.poll() != null);
    }

    public final boolean containsKey(Object obj) {
        return b(obj) != null;
    }

    public final boolean containsValue(Object obj) {
        if (obj == null) {
            a();
            e[] eVarArr = this.b;
            int length = eVarArr.length;
            while (true) {
                int i2 = length - 1;
                if (length <= 0) {
                    return false;
                }
                for (e eVar = eVarArr[i2]; eVar != null; eVar = eVar.c) {
                    if (eVar.a == null) {
                        return true;
                    }
                }
                length = i2;
            }
        } else {
            a();
            e[] eVarArr2 = this.b;
            int length2 = eVarArr2.length;
            while (true) {
                int i3 = length2 - 1;
                if (length2 <= 0) {
                    return false;
                }
                for (e eVar2 = eVarArr2[i3]; eVar2 != null; eVar2 = eVar2.c) {
                    if (obj.equals(eVar2.a)) {
                        return true;
                    }
                }
                length2 = i3;
            }
        }
    }

    public final Set entrySet() {
        Set set = this.h;
        if (set != null) {
            return set;
        }
        h hVar = new h(this);
        this.h = hVar;
        return hVar;
    }

    public final Object get(Object obj) {
        Object obj2 = obj == null ? a : obj;
        int hashCode = obj2.hashCode();
        int i2 = hashCode ^ ((hashCode >>> 20) ^ (hashCode >>> 12));
        int i3 = (i2 >>> 4) ^ ((i2 >>> 7) ^ i2);
        a();
        e[] eVarArr = this.b;
        for (e eVar = eVarArr[(eVarArr.length - 1) & i3]; eVar != null; eVar = eVar.c) {
            if (eVar.b == i3) {
                Object obj3 = eVar.get();
                if (obj2 == obj3 || obj2.equals(obj3)) {
                    return eVar.a;
                }
            }
        }
        return null;
    }

    public final boolean isEmpty() {
        return size() == 0;
    }

    public final Set keySet() {
        Set set = this.i;
        if (set != null) {
            return set;
        }
        b bVar = new b(this);
        this.i = bVar;
        return bVar;
    }

    public final Object put(Object obj, Object obj2) {
        Object obj3 = obj == null ? a : obj;
        int hashCode = obj3.hashCode();
        int i2 = hashCode ^ ((hashCode >>> 20) ^ (hashCode >>> 12));
        int i3 = ((i2 >>> 7) ^ i2) ^ (i2 >>> 4);
        a();
        e[] eVarArr = this.b;
        int length = i3 & (eVarArr.length - 1);
        for (e eVar = eVarArr[length]; eVar != null; eVar = eVar.c) {
            if (i3 == eVar.b) {
                Object obj4 = eVar.get();
                if (obj3 == obj4 || obj3.equals(obj4)) {
                    Object b2 = eVar.a;
                    if (obj2 != b2) {
                        Object unused = eVar.a = obj2;
                    }
                    return b2;
                }
            }
        }
        this.g++;
        eVarArr[length] = new e(obj3, obj2, this.f, i3, eVarArr[length]);
        int i4 = this.c + 1;
        this.c = i4;
        if (i4 >= this.d) {
            a(eVarArr.length * 2);
        }
        return null;
    }

    public final void putAll(Map map) {
        int size = map.size();
        if (size != 0) {
            if (size > this.d) {
                int i2 = (int) ((((float) size) / this.e) + 1.0f);
                if (i2 > 1073741824) {
                    i2 = 1073741824;
                }
                int length = this.b.length;
                while (length < i2) {
                    length <<= 1;
                }
                if (length > this.b.length) {
                    a(length);
                }
            }
            for (Map.Entry entry : map.entrySet()) {
                put(entry.getKey(), entry.getValue());
            }
        }
    }

    public final Object remove(Object obj) {
        Object obj2 = obj == null ? a : obj;
        int hashCode = obj2.hashCode();
        int i2 = hashCode ^ ((hashCode >>> 20) ^ (hashCode >>> 12));
        int i3 = (i2 >>> 4) ^ ((i2 >>> 7) ^ i2);
        a();
        e[] eVarArr = this.b;
        int length = (eVarArr.length - 1) & i3;
        e eVar = eVarArr[length];
        e eVar2 = eVar;
        while (eVar != null) {
            e c2 = eVar.c;
            if (i3 == eVar.b) {
                Object obj3 = eVar.get();
                if (obj2 == obj3 || obj2.equals(obj3)) {
                    this.g++;
                    this.c--;
                    if (eVar2 == eVar) {
                        eVarArr[length] = c2;
                    } else {
                        e unused = eVar2.c = c2;
                    }
                    return eVar.a;
                }
            }
            eVar2 = eVar;
            eVar = c2;
        }
        return null;
    }

    public final int size() {
        if (this.c == 0) {
            return 0;
        }
        a();
        return this.c;
    }

    public final Collection values() {
        Collection collection = this.j;
        if (collection != null) {
            return collection;
        }
        k kVar = new k(this);
        this.j = kVar;
        return kVar;
    }
}
