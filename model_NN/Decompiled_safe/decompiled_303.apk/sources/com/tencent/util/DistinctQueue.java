package com.tencent.util;

import java.util.LinkedList;

public class DistinctQueue extends LinkedList {
    public boolean offer(Object obj) {
        if (contains(obj)) {
            return false;
        }
        return super.offer(obj);
    }
}
