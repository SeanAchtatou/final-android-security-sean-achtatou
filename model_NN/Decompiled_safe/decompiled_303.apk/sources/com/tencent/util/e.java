package com.tencent.util;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.Map;

final class e extends SoftReference implements Map.Entry {
    /* access modifiers changed from: private */
    public Object a;
    /* access modifiers changed from: private */
    public final int b;
    /* access modifiers changed from: private */
    public e c;

    e(Object obj, Object obj2, ReferenceQueue referenceQueue, int i, e eVar) {
        super(obj, referenceQueue);
        this.a = obj2;
        this.b = i;
        this.c = eVar;
    }

    public final boolean equals(Object obj) {
        Object value;
        Object value2;
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        Object key = getKey();
        Object key2 = entry.getKey();
        return (key == key2 || (key != null && key.equals(key2))) && ((value = getValue()) == (value2 = entry.getValue()) || (value != null && value.equals(value2)));
    }

    public final Object getKey() {
        return l.a(get());
    }

    public final Object getValue() {
        return this.a;
    }

    public final int hashCode() {
        Object key = getKey();
        Object value = getValue();
        return (key == null ? 0 : key.hashCode()) ^ (value == null ? 0 : value.hashCode());
    }

    public final Object setValue(Object obj) {
        Object obj2 = this.a;
        this.a = obj;
        return obj2;
    }

    public final String toString() {
        return getKey() + "=" + getValue();
    }
}
