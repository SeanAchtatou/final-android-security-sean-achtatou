package com.tencent.util;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

abstract class m implements Iterator {
    private int a;
    private e b = null;
    private e c = null;
    private int d = this.g.g;
    private Object e = null;
    private Object f = null;
    private /* synthetic */ l g;

    m(l lVar) {
        this.g = lVar;
        this.a = lVar.size() != 0 ? lVar.b.length : 0;
    }

    /* access modifiers changed from: protected */
    public final e a() {
        if (this.g.g != this.d) {
            throw new ConcurrentModificationException();
        } else if (this.e != null || hasNext()) {
            this.c = this.b;
            this.b = this.b.c;
            this.f = this.e;
            this.e = null;
            return this.c;
        } else {
            throw new NoSuchElementException();
        }
    }

    public boolean hasNext() {
        e[] b2 = this.g.b;
        while (this.e == null) {
            e eVar = this.b;
            int i = this.a;
            while (eVar == null && i > 0) {
                i--;
                eVar = b2[i];
            }
            this.b = eVar;
            this.a = i;
            if (eVar == null) {
                this.f = null;
                return false;
            }
            this.e = eVar.get();
            if (this.e == null) {
                this.b = this.b.c;
            }
        }
        return true;
    }

    public void remove() {
        if (this.c == null) {
            throw new IllegalStateException();
        } else if (this.g.g != this.d) {
            throw new ConcurrentModificationException();
        } else {
            this.g.remove(this.f);
            this.d = this.g.g;
            this.c = null;
            this.f = null;
        }
    }
}
