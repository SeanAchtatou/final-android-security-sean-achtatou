package com.tencent.util;

import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Iterator;

final class b extends AbstractSet {
    private /* synthetic */ l a;

    /* synthetic */ b(l lVar) {
        this(lVar, (byte) 0);
    }

    private b(l lVar, byte b) {
        this.a = lVar;
    }

    public final void clear() {
        this.a.clear();
    }

    public final boolean contains(Object obj) {
        return this.a.containsKey(obj);
    }

    public final Iterator iterator() {
        return new g(this.a);
    }

    public final boolean remove(Object obj) {
        if (!this.a.containsKey(obj)) {
            return false;
        }
        this.a.remove(obj);
        return true;
    }

    public final int size() {
        return this.a.size();
    }

    public final Object[] toArray() {
        ArrayList arrayList = new ArrayList(size());
        Iterator it = iterator();
        while (it.hasNext()) {
            arrayList.add(it.next());
        }
        return arrayList.toArray();
    }

    public final Object[] toArray(Object[] objArr) {
        ArrayList arrayList = new ArrayList(size());
        Iterator it = iterator();
        while (it.hasNext()) {
            arrayList.add(it.next());
        }
        return arrayList.toArray(objArr);
    }
}
