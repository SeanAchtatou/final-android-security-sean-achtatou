package com.tencent.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.DisplayMetrics;
import com.tencent.launcher.base.BaseApp;
import com.tencent.launcher.base.b;
import com.tencent.qqlauncher.R;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public final class p {
    private static long[] a = {1073741824, 1048576, 1024, 1};
    private static String[] b = {"GB", "MB", "KB", "B"};
    private static DecimalFormat c = new DecimalFormat("##.#");
    private static String[] d = null;

    public static int a(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static Intent a(String str) {
        Intent intent = new Intent();
        if (b.a >= 9) {
            intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent.setData(Uri.fromParts("package", str, null));
        } else {
            intent.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
        }
        intent.putExtra("com.android.settings.ApplicationPkgName", str);
        intent.putExtra("pkg", str);
        return intent;
    }

    public static String a() {
        if (d == null || d.length <= 0) {
            return "";
        }
        return d[Math.abs(new Random().nextInt()) % d.length];
    }

    public static String a(int i) {
        return i <= 10000 ? i + BaseApp.b().getString(R.string.ManypeoDL) : (i <= 10000 || i > 50000) ? (i <= 50000 || i > 100000) ? BaseApp.b().getString(R.string.TenpeoDL) : BaseApp.b().getString(R.string.FivepeoDL) : BaseApp.b().getString(R.string.OnepeoDL);
    }

    public static String a(long j) {
        if (j < 1) {
            return "0B";
        }
        for (int i = 0; i < a.length; i++) {
            long j2 = a[i];
            if (j >= j2) {
                return c.format(j2 > 1 ? ((double) j) / ((double) j2) : (double) j) + " " + b[i];
            }
        }
        return null;
    }

    public static Field a(Class cls, String str) {
        try {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (field.getName().compareTo(str) == 0) {
                    field.setAccessible(true);
                    return field;
                }
            }
        } catch (Exception e) {
        }
        return null;
    }

    public static boolean a(Context context) {
        String packageName = context.getPackageName();
        PackageManager packageManager = context.getPackageManager();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        packageManager.getPreferredActivities(arrayList2, arrayList, packageName);
        for (int i = 0; i < arrayList2.size(); i++) {
            if (((IntentFilter) arrayList2.get(i)).hasCategory("android.intent.category.HOME")) {
                return true;
            }
        }
        return false;
    }

    public static boolean a(String str, String str2) {
        try {
            if (!new File(str).exists()) {
                return true;
            }
            FileInputStream fileInputStream = new FileInputStream(str);
            FileOutputStream fileOutputStream = new FileOutputStream(str2);
            byte[] bArr = new byte[1444];
            while (true) {
                int read = fileInputStream.read(bArr);
                if (read != -1) {
                    fileOutputStream.write(bArr, 0, read);
                } else {
                    fileInputStream.close();
                    return true;
                }
            }
        } catch (IOException e) {
            throw e;
        }
    }

    public static int b(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    public static long b(String str) {
        try {
            String[] split = str.split(" ");
            String[] split2 = split[0].split("-");
            String[] split3 = split[1].split(":");
            Calendar instance = Calendar.getInstance();
            instance.set(Integer.parseInt(split2[0]), Integer.parseInt(split2[1]) - 1, Integer.parseInt(split2[2]), Integer.parseInt(split3[0]), Integer.parseInt(split3[1]), Integer.parseInt(split3[2]));
            return instance.getTimeInMillis();
        } catch (Exception e) {
            return 0;
        }
    }

    public static boolean b() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) BaseApp.b().getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isAvailable()) {
            return true;
        }
        BaseApp.a((int) R.string.theme_network_disable);
        return false;
    }

    public static boolean b(Context context) {
        NetworkInfo[] allNetworkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (!(connectivityManager == null || (allNetworkInfo = connectivityManager.getAllNetworkInfo()) == null)) {
            for (NetworkInfo state : allNetworkInfo) {
                if (state.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }

    public static ResolveInfo c(Context context) {
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 0);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (ResolveInfo next : queryIntentActivities) {
            arrayList.clear();
            arrayList2.clear();
            packageManager.getPreferredActivities(arrayList, arrayList2, next.activityInfo.packageName);
            Iterator it = arrayList.iterator();
            while (true) {
                if (it.hasNext()) {
                    IntentFilter intentFilter = (IntentFilter) it.next();
                    if (intentFilter.hasAction("android.intent.action.MAIN") && intentFilter.hasCategory("android.intent.category.HOME")) {
                        return next;
                    }
                }
            }
        }
        return null;
    }
}
