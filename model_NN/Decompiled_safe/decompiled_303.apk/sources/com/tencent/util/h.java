package com.tencent.util;

import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

final class h extends AbstractSet {
    private /* synthetic */ l a;

    /* synthetic */ h(l lVar) {
        this(lVar, (byte) 0);
    }

    private h(l lVar, byte b) {
        this.a = lVar;
    }

    public final void clear() {
        this.a.clear();
    }

    public final boolean contains(Object obj) {
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        e a2 = this.a.b(entry.getKey());
        return a2 != null && a2.equals(entry);
    }

    public final Iterator iterator() {
        return new i(this.a);
    }

    public final boolean remove(Object obj) {
        return l.b(this.a, obj) != null;
    }

    public final int size() {
        return this.a.size();
    }

    public final Object[] toArray() {
        ArrayList arrayList = new ArrayList(size());
        Iterator it = iterator();
        while (it.hasNext()) {
            arrayList.add(new j((Map.Entry) it.next()));
        }
        return arrayList.toArray();
    }

    public final Object[] toArray(Object[] objArr) {
        ArrayList arrayList = new ArrayList(size());
        Iterator it = iterator();
        while (it.hasNext()) {
            arrayList.add(new j((Map.Entry) it.next()));
        }
        return arrayList.toArray(objArr);
    }
}
