package com.tencent.util;

import java.util.Map;

final class j implements Map.Entry {
    private final Object a;
    private Object b;

    public j(Map.Entry entry) {
        this.a = entry.getKey();
        this.b = entry.getValue();
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        Object obj2 = this.a;
        Object key = entry.getKey();
        if (obj2 == null ? key == null : obj2.equals(key)) {
            Object obj3 = this.b;
            Object value = entry.getValue();
            if (obj3 == null ? value == null : obj3.equals(value)) {
                return true;
            }
        }
        return false;
    }

    public final Object getKey() {
        return this.a;
    }

    public final Object getValue() {
        return this.b;
    }

    public final int hashCode() {
        return (this.a == null ? 0 : this.a.hashCode()) ^ (this.b == null ? 0 : this.b.hashCode());
    }

    public final Object setValue(Object obj) {
        Object obj2 = this.b;
        this.b = obj;
        return obj2;
    }

    public final String toString() {
        return this.a + "=" + this.b;
    }
}
