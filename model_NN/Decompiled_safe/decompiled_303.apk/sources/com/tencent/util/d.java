package com.tencent.util;

import android.graphics.Color;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import com.tencent.launcher.home.JNI;
import com.tencent.launcher.home.c;
import java.util.Vector;

public final class d {
    private c a = new c();

    private static int a(Vector vector, String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= vector.size()) {
                return -1;
            }
            if (str.compareTo((String) vector.elementAt(i2)) == 0) {
                return i2;
            }
            i = i2 + 1;
        }
    }

    private void a(Vector vector, int i, String[] strArr, String str) {
        if (i >= vector.size()) {
            String str2 = "";
            String str3 = "";
            for (String str4 : strArr) {
                StringBuffer stringBuffer = new StringBuffer();
                c.a(str4, stringBuffer);
                str2 = str2 + str4;
                str3 = str3 + ((Object) stringBuffer);
            }
            b(str2, str);
            for (int i2 = 0; i2 < str3.length(); i2++) {
                b(new Character(str3.charAt(i2)).toString(), str);
            }
            if (str3 != null && str3.length() > 0) {
                b(str3, str);
                return;
            }
            return;
        }
        String[] strArr2 = (String[]) vector.elementAt(i);
        int i3 = 0;
        while (true) {
            String[] strArr3 = strArr;
            if (i3 < strArr2.length) {
                strArr3[i] = strArr2[i3];
                a(vector, i + 1, strArr3, str);
                b(strArr2[i3], str);
                b(strArr3[i], str);
                i3++;
            } else {
                return;
            }
        }
    }

    private void b(String str, String str2) {
        c cVar;
        boolean z;
        int length = str.length();
        c cVar2 = this.a;
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (str.charAt(i) >= 'A' && str.charAt(i) <= 'Z') {
                charAt = (char) ((charAt - 'A') + 97);
            }
            if (charAt < 'a' || charAt > 'z') {
                cVar = cVar2.e[26];
                z = true;
            } else {
                cVar = cVar2.e[charAt - 'a'];
                z = false;
            }
            if (cVar != null) {
                cVar2 = cVar;
            } else if (!z) {
                c cVar3 = new c();
                cVar2.e[charAt - 'a'] = cVar3;
                cVar3.c = new Vector();
                cVar3.a = true;
                cVar3.f = charAt - 'a';
                cVar2 = cVar3;
            } else {
                c[] cVarArr = cVar2.e;
                c cVar4 = new c();
                cVarArr[26] = cVar4;
                cVar4.b = true;
                cVar4.d = new Vector();
                String ch = new Character(charAt).toString();
                if (!cVar4.d.contains(ch)) {
                    cVar4.d.add(ch);
                }
                cVar4.f = charAt;
                cVar2 = cVar4;
            }
            if (!cVar2.a) {
                cVar2.c = new Vector();
                cVar2.a = true;
                if (!cVar2.c.contains(str2)) {
                    cVar2.c.add(str2);
                }
            } else if (!cVar2.c.contains(str2)) {
                cVar2.c.add(str2);
            }
        }
    }

    public final SpannableString a(String str, String str2) {
        Vector c;
        String str3;
        int i;
        boolean[] zArr = new boolean[str.length()];
        int length = str2.length();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < length; i2++) {
            String GetUcs2Pinyin = JNI.GetUcs2Pinyin(str2.charAt(i2));
            if (GetUcs2Pinyin == null || GetUcs2Pinyin.length() == 0) {
                stringBuffer.append(str2.charAt(i2));
            } else {
                stringBuffer.append(GetUcs2Pinyin);
            }
        }
        String stringBuffer2 = stringBuffer.toString();
        SpannableString spannableString = new SpannableString(str);
        String str4 = stringBuffer2;
        String str5 = "";
        while (str4 != "") {
            if (str4 != "") {
                Vector c2 = c(str4);
                if (c2 != null) {
                    int i3 = 0;
                    int i4 = -1;
                    while (true) {
                        if (i3 < str.length()) {
                            if (!zArr[i3] && (i4 = a(c2, new Character(str.charAt(i3)).toString())) >= 0) {
                                spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#1a8ed8")), i3, i3 + 1, 17);
                                zArr[i3] = true;
                                str3 = "";
                                i = -1;
                                break;
                            }
                            i3++;
                        } else {
                            str3 = str4;
                            i = i4;
                            break;
                        }
                    }
                    if (i >= 0 || str3.length() <= 0) {
                        str4 = str3;
                    } else {
                        str5 = str3.charAt(str3.length() - 1) + str5;
                        str4 = str3.length() > 1 ? str3.substring(0, str3.length() - 1) : "";
                    }
                } else if (str4.length() > 0) {
                    str5 = str4.charAt(str4.length() - 1) + str5;
                    str4 = str4.length() > 1 ? str4.substring(0, str4.length() - 1) : "";
                }
            }
            if (str5 != "" && (c = c(str5)) != null) {
                int i5 = 0;
                while (true) {
                    if (i5 < str.length()) {
                        if (!zArr[i5] && a(c, new Character(str.charAt(i5)).toString()) >= 0) {
                            spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#1a8ed8")), i5, i5 + 1, 17);
                            zArr[i5] = true;
                            str5 = "";
                            break;
                        }
                        i5++;
                    } else {
                        break;
                    }
                }
            }
        }
        return spannableString;
    }

    public final c a() {
        return this.a;
    }

    public final void a(c cVar) {
        if (cVar != null) {
            Log.i("TreeUtil", ((char) (cVar.f + 97)) + ":" + cVar.c + " ");
            for (int i = 0; i < 26; i++) {
                a(cVar.e[i]);
            }
        }
    }

    public final void a(String str) {
        c.a(str);
        Vector vector = new Vector();
        for (int i = 0; i < str.length(); i++) {
            String[] a2 = c.a(str.charAt(i));
            if (a2 == null) {
                vector.add(new String[]{new Character(str.charAt(i)).toString()});
            } else {
                vector.add(a2);
            }
        }
        a(vector, 0, new String[str.length()], str);
    }

    public final void b(String str) {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            String ch = new Character(charAt).toString();
            String[] a2 = c.a(charAt);
            if (a2 != null) {
                for (String b : a2) {
                    b(b, ch);
                }
            } else if (ch.length() > 0) {
                b(c.a(ch)[0], ch);
            }
        }
    }

    public final Vector c(String str) {
        c cVar;
        boolean z;
        int length = str.length();
        c cVar2 = this.a;
        int i = 0;
        while (i < length) {
            char charAt = str.charAt(i);
            if (str.charAt(i) >= 'A' && str.charAt(i) <= 'Z') {
                charAt = (char) ((charAt - 'A') + 97);
            }
            if (charAt < 'a' || charAt > 'z') {
                cVar = cVar2.e[26];
                z = true;
            } else {
                cVar = cVar2.e[charAt - 'a'];
                z = false;
            }
            if (cVar == null) {
                return null;
            }
            if (i != length - 1 || !cVar.a) {
                i++;
                cVar2 = cVar;
            } else if (!z) {
                return cVar.c;
            } else {
                if (cVar.d.contains(new Character(charAt).toString())) {
                    return cVar.c;
                }
                return null;
            }
        }
        return null;
    }
}
