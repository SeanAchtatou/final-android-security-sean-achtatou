package com.tencent.util;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Iterator;

final class k extends AbstractCollection {
    private /* synthetic */ l a;

    /* synthetic */ k(l lVar) {
        this(lVar, (byte) 0);
    }

    private k(l lVar, byte b) {
        this.a = lVar;
    }

    public final void clear() {
        this.a.clear();
    }

    public final boolean contains(Object obj) {
        return this.a.containsValue(obj);
    }

    public final Iterator iterator() {
        return new n(this.a);
    }

    public final int size() {
        return this.a.size();
    }

    public final Object[] toArray() {
        ArrayList arrayList = new ArrayList(size());
        Iterator it = iterator();
        while (it.hasNext()) {
            arrayList.add(it.next());
        }
        return arrayList.toArray();
    }

    public final Object[] toArray(Object[] objArr) {
        ArrayList arrayList = new ArrayList(size());
        Iterator it = iterator();
        while (it.hasNext()) {
            arrayList.add(it.next());
        }
        return arrayList.toArray(objArr);
    }
}
