package com.tencent.a;

import com.tencent.qqlauncher.R;

public final class a {
    public static final int[] a = {16843028, 16843029};
    public static final int[] b = {R.attr.cellWidth, R.attr.cellHeight, R.attr.longAxisStartPadding, R.attr.longAxisEndPadding, R.attr.shortAxisStartPadding, R.attr.shortAxisEndPadding, R.attr.shortAxisCells, R.attr.longAxisCells};
    public static final int[] c = {R.attr.customAlertDialogStyle};
    public static final int[] d = {R.attr.direction};
    public static final int[] e = {R.attr.direction, R.attr.cellWidth, R.attr.cellHeight, R.attr.cellNum, R.attr.startPadding, R.attr.endPadding};
    public static final int[] f = {R.attr.handle, R.attr.content, R.attr.position};
    public static final int[] g = {R.attr.className, R.attr.packageName, R.attr.screen, R.attr.x, R.attr.y, R.attr.icon, R.attr.title, R.attr.uri, R.attr.container, R.attr.action, R.attr.type, R.attr.datatype, R.attr.extra};
    public static final int[] h = {R.attr.folder_position, R.attr.folder_title, R.attr.folder_id, R.attr.app_package, R.attr.app_class, R.attr.app_type};
    public static final int[] i = {R.attr.direction};
    public static final int[] j = {R.attr.textColor, R.attr.textSize, R.attr.startPadding, R.attr.endPadding, R.attr.rightPadding};
    public static final int[] k = {R.attr.cellWidth, R.attr.cellHeight, R.attr.orientation, R.attr.cells};
    public static final int[] l = {R.attr.slideDirections, R.attr.targetDistance};
    public static final int[] m = {R.attr.defaultScreen};
    private static int[] n = {R.attr.texture};
    private static int[] o = {R.attr.completionHint, R.attr.completionHintView, R.attr.completionThreshold, R.attr.dropDownSelector, R.attr.dropDownVerticalOffset, R.attr.dropDownHorizontalOffset, R.attr.dropDownAnchor, R.attr.dropDownWidth, R.attr.dropDownHeight};
    private static int[] p = {R.attr.name, R.attr.spanX, R.attr.spanY, R.attr.layoutId, R.attr.maxInstance};
}
