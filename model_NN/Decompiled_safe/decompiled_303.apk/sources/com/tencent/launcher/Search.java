package com.tencent.launcher;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.qqlauncher.R;

public class Search extends LinearLayout implements View.OnClickListener, View.OnKeyListener, View.OnLongClickListener {
    private final String a = "SearchWidget";
    private Launcher b;
    private TextView c;
    private ImageButton d;
    /* access modifiers changed from: private */
    public Animation e;
    private Animation f;
    private String g;
    private boolean h;
    private Bundle i;
    private boolean j;
    private Intent k;
    private int l;

    public Search(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.l = Math.round(context.getResources().getDisplayMetrics().density * 9.0f);
        AccelerateDecelerateInterpolator accelerateDecelerateInterpolator = new AccelerateDecelerateInterpolator();
        this.e = new dk(this);
        this.e.setFillBefore(false);
        this.e.setFillAfter(true);
        this.e.setInterpolator(accelerateDecelerateInterpolator);
        this.e.setAnimationListener(new fs(this));
        this.f = new ii(this);
        this.f.setFillBefore(true);
        this.f.setFillAfter(false);
        this.f.setInterpolator(accelerateDecelerateInterpolator);
        this.f.setAnimationListener(new fr(this));
        this.k = new Intent("android.speech.action.WEB_SEARCH");
        this.k.putExtra("android.speech.extra.LANGUAGE_MODEL", "web_search");
    }

    static /* synthetic */ int c(Search search) {
        return search.getTop() + search.getChildAt(0).getTop() + search.l;
    }

    public final void a() {
        this.c.setText("", TextView.BufferType.NORMAL);
        if (getAnimation() == this.e) {
            clearAnimation();
        }
    }

    public final void a(Launcher launcher) {
        this.b = launcher;
    }

    public final void a(String str) {
        this.c.setText(str, TextView.BufferType.NORMAL);
    }

    public final void a(String str, boolean z, Bundle bundle, boolean z2) {
        this.g = str;
        this.h = z;
        this.i = bundle;
        this.j = z2;
        if ((getTop() + getChildAt(0).getTop()) + this.l == 0) {
            this.b.showSearchDialog(this.g, this.h, this.i, this.j);
            return;
        }
        if (getContext().getResources().getConfiguration().hardKeyboardHidden == 2) {
            ((InputMethodManager) getContext().getSystemService("input_method")).showSoftInput(this, 0, null);
        }
        if (getAnimation() != this.e) {
            this.e.setDuration((long) ((int) (((float) ((getTop() + getChildAt(0).getTop()) + this.l)) / 1.0f)));
            startAnimation(this.e);
        }
    }

    public void clearAnimation() {
        Animation animation = getAnimation();
        if (animation != null) {
            super.clearAnimation();
            if (!animation.hasEnded() || !animation.getFillAfter() || !animation.willChangeBounds()) {
                invalidate();
            } else {
                ((View) getParent()).invalidate();
            }
        }
    }

    public void onClick(View view) {
        if (view == this.d) {
            try {
                getContext().startActivity(this.k);
            } catch (ActivityNotFoundException e2) {
                Log.w("SearchWidget", "Could not find voice search activity");
            }
        } else {
            this.b.onSearchRequested();
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.c = (TextView) findViewById(R.id.search_src_text);
        this.d = (ImageButton) findViewById(R.id.search_voice_btn);
        this.c.setOnKeyListener(this);
        this.c.setOnClickListener(this);
        this.d.setOnClickListener(this);
        setOnClickListener(this);
        this.c.setOnLongClickListener(this);
        this.d.setOnLongClickListener(this);
        this.c.setCompoundDrawablesWithIntrinsicBounds(getContext().getResources().getDrawable(R.drawable.placeholder_google), (Drawable) null, (Drawable) null, (Drawable) null);
        this.d.setVisibility(getContext().getPackageManager().resolveActivity(this.k, 65536) != null ? 0 : 8);
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (!(keyEvent.isSystem() || i2 == 19 || i2 == 20 || i2 == 21 || i2 == 22 || i2 == 23)) {
            switch (keyEvent.getAction()) {
                case 0:
                    return this.b.onKeyDown(i2, keyEvent);
                case 1:
                    return this.b.onKeyUp(i2, keyEvent);
                case 2:
                    return this.b.onKeyMultiple(i2, keyEvent.getRepeatCount(), keyEvent);
            }
        }
        return false;
    }

    public boolean onLongClick(View view) {
        return performLongClick();
    }
}
