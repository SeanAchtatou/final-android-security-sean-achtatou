package com.tencent.launcher;

import android.database.ContentObserver;
import android.os.Handler;

final class aq extends ContentObserver {
    private /* synthetic */ Launcher a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public aq(Launcher launcher) {
        super(new Handler());
        this.a = launcher;
    }

    public final void onChange(boolean z) {
        Launcher.access$4400(this.a);
    }
}
