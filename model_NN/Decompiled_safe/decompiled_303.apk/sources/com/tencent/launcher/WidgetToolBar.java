package com.tencent.launcher;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.Scroller;
import com.tencent.a.a;
import com.tencent.module.switcher.v;
import java.util.List;

public class WidgetToolBar extends ViewGroup {
    private int a;
    private int b;
    private int c;
    private int d;
    private Context e;
    private Handler f;
    private Scroller g;
    private float h;
    private float i;
    private int j;
    private int k;
    private int l;
    private boolean m;

    public WidgetToolBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public WidgetToolBar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.a = 1;
        this.j = 0;
        this.l = 600;
        this.m = false;
        this.e = context;
        this.f = new Handler();
        setHapticFeedbackEnabled(true);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.k, i2, 0);
        this.a = obtainStyledAttributes.getInt(2, this.a);
        this.b = obtainStyledAttributes.getInt(3, this.b);
        this.c = obtainStyledAttributes.getDimensionPixelSize(0, this.c);
        this.d = obtainStyledAttributes.getDimensionPixelSize(1, this.d);
        obtainStyledAttributes.recycle();
        this.g = new Scroller(getContext());
        this.k = ViewConfiguration.get(context).getScaledTouchSlop();
        List b2 = v.a().b();
        for (int i3 = 0; i3 < b2.size(); i3++) {
            addView((View) b2.get(i3));
        }
    }

    public void computeScroll() {
        if (this.g.computeScrollOffset()) {
            scrollTo(this.g.getCurrX(), this.g.getCurrY());
            postInvalidate();
        }
    }

    /* access modifiers changed from: protected */
    public final void onFinishInflate() {
        super.onFinishInflate();
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (!this.m) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action == 2 && this.j != 0) {
            return true;
        }
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        switch (action) {
            case 0:
                this.h = x;
                this.i = y;
                this.j = this.g.isFinished() ? 0 : 1;
                break;
            case 1:
            case 3:
                this.j = 0;
                break;
            case 2:
                int abs = (int) Math.abs(x - this.h);
                int abs2 = (int) Math.abs(y - this.i);
                int i2 = this.k;
                boolean z = abs > i2;
                boolean z2 = abs2 > i2;
                if (z || z2) {
                    if (!z || this.a != 1) {
                        if (z2 && this.a == 0) {
                            this.j = 1;
                            break;
                        }
                    } else {
                        this.j = 1;
                        break;
                    }
                }
                break;
        }
        return this.j != 0;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        getMeasuredWidth();
        getMeasuredHeight();
        if (getChildCount() > this.b) {
            if (this.a == 1) {
                this.c = getMeasuredWidth() / this.b;
            } else {
                this.d = getMeasuredHeight() / this.b;
            }
            this.m = true;
        } else {
            this.m = false;
            scrollTo(0, 0);
        }
        int i6 = 0;
        int i7 = 0;
        for (int i8 = 0; i8 < childCount; i8++) {
            View childAt = getChildAt(i8);
            if (childAt.getVisibility() != 8) {
                if (this.a != 1) {
                    i7 = 0;
                }
                if (this.a != 0) {
                    i6 = 0;
                }
                int i9 = this.c + i7;
                int i10 = this.d + i6;
                childAt.layout(i7, i6, i9, i10);
                i6 = i10;
                i7 = i9;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        if (this.a == 1) {
            super.onMeasure(i2, View.MeasureSpec.makeMeasureSpec(this.d, Integer.MIN_VALUE));
        } else {
            super.onMeasure(View.MeasureSpec.makeMeasureSpec(this.c, Integer.MIN_VALUE), i3);
        }
        if (this.a == 1) {
            this.c = View.MeasureSpec.getSize(i2) / this.b;
        } else {
            this.d = View.MeasureSpec.getSize(i3) / this.b;
        }
        int childCount = getChildCount();
        for (int i4 = 0; i4 < childCount; i4++) {
            getChildAt(i4).measure(this.c, this.d);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.m) {
            return false;
        }
        int action = motionEvent.getAction();
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        switch (action) {
            case 0:
                if (!this.g.isFinished()) {
                    this.g.abortAnimation();
                }
                this.j = 2;
                this.h = x;
                this.i = y;
                break;
            case 1:
                if (this.j == 1) {
                    int scrollX = this.a == 1 ? getScrollX() : getScrollY();
                    int i2 = this.a == 1 ? this.c : this.d;
                    int width = this.a == 1 ? getWidth() : getHeight();
                    int i3 = scrollX - (scrollX % i2);
                    if (i3 < 0) {
                        i3 = 0;
                    }
                    int childCount = getChildCount();
                    int i4 = (i3 > (childCount * i2) - width ? (i2 * childCount) - width : i3) - scrollX;
                    if (this.a == 1) {
                        this.g.startScroll(scrollX, 0, i4, 0, this.l);
                    } else {
                        this.g.startScroll(0, scrollX, 0, i4, this.l);
                    }
                    invalidate();
                }
                this.j = 0;
                invalidate();
                break;
            case 2:
                if (this.j == 1) {
                    int i5 = this.a == 1 ? (int) (this.h - x) : (int) (this.i - y);
                    if (Math.abs(i5) > this.k || this.j == 1) {
                        this.j = 1;
                        this.h = x;
                        this.i = y;
                        if (i5 >= 0) {
                            if (i5 > 0) {
                                if (this.a != 1) {
                                    scrollBy(0, i5);
                                    break;
                                } else {
                                    scrollBy(i5, 0);
                                    break;
                                }
                            }
                        } else if (this.a != 1) {
                            scrollBy(0, i5);
                            break;
                        } else {
                            scrollBy(i5, 0);
                            break;
                        }
                    }
                }
                break;
            case 3:
                this.j = 0;
                break;
        }
        return true;
    }
}
