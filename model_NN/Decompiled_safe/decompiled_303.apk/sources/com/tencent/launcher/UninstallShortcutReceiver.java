package com.tencent.launcher;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.widget.Toast;
import com.tencent.qqlauncher.R;
import java.net.URISyntaxException;

public class UninstallShortcutReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        boolean z;
        if ("com.android.launcher.action.UNINSTALL_SHORTCUT".equals(intent.getAction())) {
            Intent intent2 = (Intent) intent.getParcelableExtra("android.intent.extra.shortcut.INTENT");
            String stringExtra = intent.getStringExtra("android.intent.extra.shortcut.NAME");
            boolean booleanExtra = intent.getBooleanExtra("duplicate", true);
            if (intent2 != null && stringExtra != null) {
                ContentResolver contentResolver = context.getContentResolver();
                Cursor query = contentResolver.query(ba.a, new String[]{"_id", "intent"}, "title=?", new String[]{stringExtra}, null);
                int columnIndexOrThrow = query.getColumnIndexOrThrow("intent");
                int columnIndexOrThrow2 = query.getColumnIndexOrThrow("_id");
                boolean z2 = false;
                while (true) {
                    try {
                        if (!query.moveToNext()) {
                            z = z2;
                            break;
                        }
                        try {
                            if (intent2.filterEquals(Intent.parseUri(query.getString(columnIndexOrThrow), 0))) {
                                contentResolver.delete(ba.a(query.getLong(columnIndexOrThrow2)), null, null);
                                if (!booleanExtra) {
                                    z = true;
                                    break;
                                }
                                z2 = true;
                            } else {
                                continue;
                            }
                        } catch (URISyntaxException e) {
                        }
                    } finally {
                        query.close();
                    }
                }
                if (z) {
                    contentResolver.notifyChange(ba.a, null);
                    Toast.makeText(context, context.getString(R.string.shortcut_uninstalled, stringExtra), 0).show();
                }
            }
        }
    }
}
