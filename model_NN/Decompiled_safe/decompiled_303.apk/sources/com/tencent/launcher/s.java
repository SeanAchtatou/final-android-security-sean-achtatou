package com.tencent.launcher;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

final class s extends ArrayAdapter {
    public String a;
    private Context b;
    private final LayoutInflater c = ((LayoutInflater) this.b.getSystemService("layout_inflater"));
    private /* synthetic */ SearchAppTestActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public s(SearchAppTestActivity searchAppTestActivity, Context context, ArrayList arrayList) {
        super(context, 0, arrayList);
        this.d = searchAppTestActivity;
        this.b = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.a.a(android.content.Context, android.graphics.drawable.Drawable, com.tencent.launcher.ha, boolean):android.graphics.drawable.Drawable
     arg types: [android.content.Context, android.graphics.drawable.Drawable, com.tencent.launcher.am, int]
     candidates:
      com.tencent.launcher.a.a(android.graphics.Bitmap, int, int, android.content.Context):android.graphics.Bitmap
      com.tencent.launcher.a.a(android.content.Context, android.graphics.drawable.Drawable, android.graphics.drawable.Drawable, com.tencent.launcher.ha):android.graphics.drawable.Drawable
      com.tencent.launcher.a.a(android.content.Context, android.graphics.drawable.Drawable, com.tencent.launcher.ha, boolean):android.graphics.drawable.Drawable */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        am amVar = (am) getItem(i);
        View inflate = (view == null || view.getTag() == null) ? this.c.inflate((int) R.layout.search_list_item, (ViewGroup) null) : view;
        TextView textView = (TextView) inflate.findViewById(R.id.softname);
        textView.setCompoundDrawablesWithIntrinsicBounds(a.a(getContext(), amVar.d, (ha) amVar, true), (Drawable) null, (Drawable) null, (Drawable) null);
        textView.setText(this.d.tree.a(amVar.a.toString(), this.d.mCurKeyWord));
        inflate.setTag(amVar);
        return inflate;
    }
}
