package com.tencent.launcher;

import android.view.View;
import android.view.animation.Animation;

final class bp extends fl {
    View a;
    boolean b;

    /* synthetic */ bp() {
        this((byte) 0);
    }

    private bp(byte b2) {
    }

    public final void onAnimationEnd(Animation animation) {
        View view;
        this.c--;
        if (this.c <= 0 && (view = this.a) != null) {
            this.c = 0;
            if (this.b) {
                if (view.getParent() != null) {
                    if (this.e != null) {
                        for (int i = 0; i < this.e.size(); i++) {
                            ((View) this.e.get(i)).clearAnimation();
                        }
                    }
                    QGridLayout qGridLayout = (QGridLayout) view.getParent();
                    int unused = qGridLayout.am = 3;
                    qGridLayout.removeViewInLayout(view);
                    qGridLayout.addView(view, ((ha) this.a.getTag()).t, view.getLayoutParams());
                    view.clearAnimation();
                    view.setVisibility(0);
                    qGridLayout.requestLayout();
                    if (qGridLayout.aE) {
                        qGridLayout.n();
                    }
                }
            } else if (view.getParent() != null) {
                QGridLayout qGridLayout2 = (QGridLayout) view.getParent();
                int unused2 = qGridLayout2.am = 3;
                qGridLayout2.requestLayout();
                qGridLayout2.invalidate();
            }
            if (this.e != null) {
                this.e.clear();
                this.e = null;
            }
        }
    }
}
