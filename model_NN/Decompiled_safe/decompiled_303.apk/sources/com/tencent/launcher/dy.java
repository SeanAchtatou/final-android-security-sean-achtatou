package com.tencent.launcher;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import com.tencent.launcher.home.a;

final class dy implements DialogInterface.OnClickListener {
    private /* synthetic */ Launcher a;

    dy(Launcher launcher) {
        this.a = launcher;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        a.a(this.a.getApplicationContext(), "fn_appstore");
        String str = dg.a[i];
        try {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
            this.a.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            this.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        }
    }
}
