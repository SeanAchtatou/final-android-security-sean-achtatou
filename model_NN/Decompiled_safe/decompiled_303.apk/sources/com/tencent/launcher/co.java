package com.tencent.launcher;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

public final class co extends BaseAdapter {
    private final LayoutInflater a;
    private final ArrayList b = new ArrayList();

    public co(Launcher launcher, boolean z) {
        this.a = (LayoutInflater) launcher.getSystemService("layout_inflater");
        Resources resources = launcher.getResources();
        this.b.add(new ck(this, resources, R.string.group_applications, R.drawable.ic_launcher_application, 0));
        this.b.add(new ck(this, resources, R.string.group_shortcuts, R.drawable.ic_launcher_shortcut, 1));
        if (!z) {
            this.b.add(new ck(this, resources, R.string.item_widget, R.drawable.ic_launcher_appwidget, 2));
        }
        this.b.add(new ck(this, resources, R.string.group_folder, R.drawable.ic_launcher_add_folder, 3));
        if (!z) {
            this.b.add(new ck(this, resources, R.string.group_wallpapers, R.drawable.ic_launcher_wallpaper, 4));
        }
    }

    public final int getCount() {
        return this.b.size();
    }

    public final Object getItem(int i) {
        return this.b.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        ck ckVar = (ck) getItem(i);
        View inflate = view == null ? this.a.inflate((int) R.layout.add_list_item, viewGroup, false) : view;
        TextView textView = (TextView) inflate;
        textView.setTag(ckVar);
        textView.setText(ckVar.a);
        textView.setCompoundDrawablesWithIntrinsicBounds(ckVar.b, (Drawable) null, (Drawable) null, (Drawable) null);
        return inflate;
    }
}
