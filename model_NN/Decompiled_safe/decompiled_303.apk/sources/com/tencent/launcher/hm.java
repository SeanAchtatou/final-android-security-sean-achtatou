package com.tencent.launcher;

import android.view.animation.Animation;

final class hm implements Animation.AnimationListener {
    final /* synthetic */ ThumbnailWorkspace a;

    hm(ThumbnailWorkspace thumbnailWorkspace) {
        this.a = thumbnailWorkspace;
    }

    public final void onAnimationEnd(Animation animation) {
        ThumbnailWorkspace.l(this.a);
        if (this.a.N >= this.a.M) {
            this.a.m.postDelayed(new gv(this), 200);
        }
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
