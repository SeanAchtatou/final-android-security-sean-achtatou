package com.tencent.launcher;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

public class AllAppsListView extends ListView implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener, cm {
    private Launcher a;
    private ArrayAdapter b;
    private cl c;
    private ArrayList d = new ArrayList();
    private dt e;
    private be f;
    private View g;
    private boolean h;
    private int i;
    private int j;

    public AllAppsListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AllAppsListView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
    }

    /* access modifiers changed from: private */
    public void a() {
        int i2;
        int[] iArr;
        String[] strArr;
        int i3;
        String str;
        int i4;
        int count = this.b.getCount();
        ArrayList arrayList = new ArrayList();
        for (int i5 = 0; i5 < count; i5++) {
            ha haVar = (ha) this.b.getItem(i5);
            if (haVar instanceof am) {
                arrayList.add(new am((am) haVar));
            } else if (haVar instanceof bq) {
                Iterator it = ((bq) haVar).b.iterator();
                while (it.hasNext()) {
                    am amVar = new am((am) it.next());
                    amVar.n = -300;
                    arrayList.add(amVar);
                }
            }
        }
        Collections.sort(arrayList, new ag());
        int size = arrayList.size();
        String[] strArr2 = new String[size];
        int[] iArr2 = new int[size];
        int i6 = 0;
        String str2 = null;
        int i7 = 0;
        int i8 = 0;
        while (i8 < size) {
            am amVar2 = (am) arrayList.get(i8);
            if (amVar2.b == null || amVar2.b.length() <= 0) {
                i3 = i6;
                str = str2;
                i4 = i7;
            } else {
                char charAt = amVar2.b.charAt(0);
                if (ff.a(charAt)) {
                    if (Character.isLowerCase(charAt)) {
                        charAt = Character.toUpperCase(charAt);
                    }
                    String ch = Character.toString(charAt);
                    if (i7 == 0 || !TextUtils.equals(ch, str2)) {
                        strArr2[i7] = ch;
                        iArr2[i7] = 1;
                        i4 = i7 + 1;
                        int i9 = i6;
                        str = ch;
                        i3 = i9;
                    } else {
                        int i10 = i7 - 1;
                        iArr2[i10] = iArr2[i10] + 1;
                        i3 = i6;
                        str = str2;
                        i4 = i7;
                    }
                } else {
                    i3 = i6 + 1;
                    str = str2;
                    i4 = i7;
                }
            }
            i8++;
            i7 = i4;
            str2 = str;
            i6 = i3;
        }
        if (i6 > 0) {
            strArr2[i7] = "#";
            iArr2[i7] = i6;
            i2 = i7 + 1;
        } else {
            i2 = i7;
        }
        if (i2 < size) {
            strArr = new String[i2];
            System.arraycopy(strArr2, 0, strArr, 0, i2);
            int[] iArr3 = new int[i2];
            System.arraycopy(iArr2, 0, iArr3, 0, i2);
            iArr = iArr3;
        } else {
            iArr = iArr2;
            strArr = strArr2;
        }
        this.d.clear();
        this.d.addAll(arrayList);
        this.f.a(strArr, iArr);
        this.f.notifyDataSetChanged();
    }

    public final void a(int i2) {
        int i3;
        int i4;
        if (this.g != null) {
            switch (this.f.a(i2)) {
                case 0:
                    this.h = false;
                    return;
                case 1:
                    this.f.a(this.g, i2, BrightnessActivity.MAXIMUM_BACKLIGHT);
                    if (this.g.getTop() != 0) {
                        this.g.layout(0, 0, this.i, this.j);
                    }
                    this.h = true;
                    return;
                case 2:
                    View childAt = getChildAt(0);
                    if (childAt != null) {
                        int bottom = childAt.getBottom();
                        childAt.getHeight();
                        int height = this.g.getHeight();
                        if (bottom < height) {
                            int i5 = bottom - height;
                            int i6 = i5;
                            i4 = ((height + i5) * BrightnessActivity.MAXIMUM_BACKLIGHT) / height;
                            i3 = i6;
                        } else {
                            i3 = 0;
                            i4 = 255;
                        }
                        this.f.a(this.g, i2, i4);
                        if (this.g.getTop() != i3) {
                            this.g.layout(0, i3, this.i, this.j + i3);
                        }
                        this.h = true;
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public final void a(View view, boolean z) {
    }

    /* access modifiers changed from: package-private */
    public final void a(Launcher launcher) {
        this.a = launcher;
    }

    public final void a(dt dtVar) {
        this.e = dtVar;
    }

    public final void a(String str) {
        String[] strArr;
        int binarySearch;
        if (this.f != null && (strArr = (String[]) this.f.getSections()) != null) {
            if ("#".equals(str)) {
                binarySearch = strArr.length - 1;
            } else {
                binarySearch = Arrays.binarySearch(strArr, str);
                if (binarySearch < 0) {
                    binarySearch = (-binarySearch) - 2;
                }
            }
            setSelection(this.f.getPositionForSection(binarySearch));
        }
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (this.h) {
            drawChild(canvas, this.g, getDrawingTime());
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        setOnItemClickListener(this);
        setOnItemLongClickListener(this);
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        this.a.startActivitySafely(((am) adapterView.getItemAtPosition(i2)).c);
    }

    public boolean onItemLongClick(AdapterView adapterView, View view, int i2, long j2) {
        if (view == null || this.e == null) {
            return false;
        }
        if (!view.isInTouchMode()) {
            return true;
        }
        am amVar = (am) view.getTag();
        TextView b2 = ((VerticalAppLayout) getParent()).b();
        if (amVar == null || b2 == null) {
            return false;
        }
        b2.setText(amVar.a != null ? amVar.a : "");
        b2.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, amVar.d != null ? amVar.d : null, (Drawable) null, (Drawable) null);
        b2.setDrawingCacheEnabled(true);
        b2.buildDrawingCache();
        this.e.a(view, b2.getDrawingCache(), this, amVar, 1);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        if (this.g != null) {
            this.g.layout(0, 0, this.i, this.j);
            a(getFirstVisiblePosition());
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        if (this.g != null) {
            measureChild(this.g, i2, i3);
            this.i = this.g.getMeasuredWidth();
            this.j = this.g.getMeasuredHeight();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.tencent.launcher.AllAppsListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void setAdapter(ListAdapter listAdapter) {
        if (listAdapter != null) {
            if (this.f == null) {
                this.f = new be(this.a, this.d);
                super.setAdapter((ListAdapter) this.f);
                setOnScrollListener(this.f);
                this.f.setNotifyOnChange(false);
            }
            this.b = (ArrayAdapter) listAdapter;
            a();
            if (this.c != null) {
                this.b.unregisterDataSetObserver(this.c);
            }
            this.c = new cl(this);
            this.b.registerDataSetObserver(this.c);
            if (this.g == null) {
                this.g = this.a.getLayoutInflater().inflate((int) R.layout.list_section, (ViewGroup) this, false);
                requestLayout();
            }
        }
    }
}
