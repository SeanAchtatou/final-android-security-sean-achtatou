package com.tencent.launcher;

final class bo implements Runnable {
    private int a;
    private /* synthetic */ PageTurningZone b;

    bo(PageTurningZone pageTurningZone) {
        this.b = pageTurningZone;
    }

    /* access modifiers changed from: package-private */
    public final void a(int i) {
        this.a = i;
    }

    public final void run() {
        if (this.b.b != null) {
            if (this.a == 0) {
                this.b.b.l();
            } else {
                this.b.b.m();
            }
        }
        this.b.postDelayed(this.b.a, 600);
    }
}
