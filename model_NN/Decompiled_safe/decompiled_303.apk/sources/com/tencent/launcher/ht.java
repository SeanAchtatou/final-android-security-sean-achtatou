package com.tencent.launcher;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;

final class ht extends Thread {
    private /* synthetic */ Launcher a;

    ht(Launcher launcher) {
        this.a = launcher;
    }

    public final void run() {
        AlarmManager unused = this.a.mAlarmManager = (AlarmManager) this.a.getSystemService("alarm");
        Intent intent = new Intent();
        intent.setAction(Launcher.ACTION_CHECK_STAT);
        PendingIntent unused2 = this.a.mStatPendingIntent = PendingIntent.getBroadcast(this.a, 0, intent, 0);
        this.a.mAlarmManager.cancel(this.a.mStatPendingIntent);
        this.a.mAlarmManager.setRepeating(1, System.currentTimeMillis() + 10000, Launcher.STAT_CHECK_TIME, this.a.mStatPendingIntent);
    }
}
