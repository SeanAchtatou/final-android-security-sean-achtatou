package com.tencent.launcher;

import android.view.animation.TranslateAnimation;

final class cu extends TranslateAnimation {
    public cu(int i, int i2, float f, int i3, int i4, float f2) {
        super(i, 0.0f, i2, f, i3, 0.0f, i4, f2);
    }

    public final boolean willChangeBounds() {
        return false;
    }

    public final boolean willChangeTransformationMatrix() {
        return true;
    }
}
