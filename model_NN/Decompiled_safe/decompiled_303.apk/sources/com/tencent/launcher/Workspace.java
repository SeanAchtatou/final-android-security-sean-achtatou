package com.tencent.launcher;

import android.app.Activity;
import android.app.WallpaperManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.Scroller;
import android.widget.TextView;
import com.tencent.launcher.CellLayout;
import com.tencent.launcher.base.BaseApp;
import com.tencent.launcher.base.b;
import com.tencent.module.a.a;
import com.tencent.module.a.e;
import com.tencent.module.a.h;
import com.tencent.qqlauncher.R;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import mobi.intuitit.android.widget.WidgetSpace;

public class Workspace extends WidgetSpace implements cm, e, gp, gw, hc, a {
    private static final double d = (1.0d / Math.log(1.25d));
    private int A;
    private Rect B;
    private Rect C;
    private boolean D;
    private boolean E;
    private QNavigation F;
    private fx G;
    private Animation H;
    private int I;
    private e J;
    private h K;
    private boolean L;
    private int M;
    private int N;
    private boolean O;
    private Paint P;
    private TextView Q;
    private boolean R;
    private bs S;
    private boolean T;
    private String U;
    private ha V;
    private int W;
    private int[] X;
    private int Y;
    private int Z;
    private int aa;
    private ha ab;
    private String ac;
    private View ad;
    private boolean ae;
    private boolean af;
    private Paint ag;
    private Bitmap ah;
    private int ai;
    private int aj;
    private float ak;
    private boolean al;
    private boolean am;
    private int e;
    private final WallpaperManager f;
    private boolean g;
    private int h;
    private int i;
    private int j;
    private Scroller k;
    private VelocityTracker l;
    private bs m;
    private int[] n;
    private float o;
    private float p;
    private int q;
    private View.OnLongClickListener r;
    /* access modifiers changed from: private */
    public Launcher s;
    private dt t;
    private bs u;
    private int[] v;
    private int[] w;
    private boolean x;
    private boolean y;
    private int z;

    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new ij();
        int currentScreen;

        private SavedState(Parcel parcel) {
            super(parcel);
            this.currentScreen = -1;
            this.currentScreen = parcel.readInt();
        }

        /* synthetic */ SavedState(Parcel parcel, et etVar) {
            this(parcel);
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
            this.currentScreen = -1;
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.currentScreen);
        }
    }

    public Workspace(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public Workspace(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.g = true;
        this.h = -1;
        this.j = -1;
        this.n = null;
        this.q = 0;
        this.u = null;
        this.v = new int[2];
        this.w = new int[2];
        this.B = new Rect();
        this.C = new Rect();
        this.D = true;
        this.I = 450;
        this.M = 2000;
        this.N = 0;
        this.T = false;
        this.X = new int[1];
        this.Y = -1;
        this.Z = -1;
        this.aa = -1;
        this.ac = "";
        this.f = WallpaperManager.getInstance(context);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, com.tencent.a.a.m, i2, 0);
        this.e = obtainStyledAttributes.getInt(0, 1);
        obtainStyledAttributes.recycle();
        this.k = new Scroller(getContext(), new g());
        this.i = this.e;
        Launcher.setScreen(this.i);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        this.z = viewConfiguration.getScaledTouchSlop();
        this.A = viewConfiguration.getScaledMaximumFlingVelocity();
        this.G = new fx(this);
        this.H = AnimationUtils.loadAnimation(getContext(), R.anim.indicator_zoom_in);
        this.J = e.a(this);
        e.b(this);
        this.P = new Paint();
        this.P.setColor(-1);
        this.P.setStyle(Paint.Style.FILL);
        this.P.setTextSize(25.0f);
    }

    private void F() {
        e eVar = this.J;
        if (!this.ae && eVar.c()) {
            int childCount = getChildCount();
            int i2 = this.N;
            int i3 = this.i;
            for (int i4 = 0; i4 < childCount; i4++) {
                if (i4 >= i3 - 1 && i4 <= i3 + 1) {
                    CellLayout cellLayout = (CellLayout) getChildAt(i4);
                    cellLayout.setChildrenDrawnWithCacheEnabled(true);
                    int childCount2 = cellLayout.getChildCount();
                    for (int i5 = 0; i5 < childCount2; i5++) {
                        View childAt = cellLayout.getChildAt(i5);
                        childAt.setDrawingCacheQuality(i2);
                        childAt.setDrawingCacheEnabled(true);
                        childAt.buildDrawingCache();
                    }
                }
            }
            this.ae = true;
        }
    }

    private void G() {
        int i2 = this.i;
        int childCount = this.F.getChildCount();
        if (this.F != null && childCount > i2) {
            this.F.b(i2);
        }
    }

    /* JADX WARN: Type inference failed for: r5v29 */
    /* JADX WARN: Type inference failed for: r5v36 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.tencent.launcher.cm r19, int r20, int r21, java.lang.Object r22, com.tencent.launcher.CellLayout r23, boolean r24) {
        /*
            r18 = this;
            com.tencent.launcher.ha r22 = (com.tencent.launcher.ha) r22
            r8 = 0
            if (r23 == 0) goto L_0x0035
            r0 = r18
            r1 = r23
            int r5 = r0.indexOfChild(r1)
            r16 = r5
        L_0x000f:
            r0 = r22
            int r0 = r0.m
            r5 = r0
            switch(r5) {
                case 0: goto L_0x0039;
                case 1: goto L_0x0039;
                case 2: goto L_0x00da;
                case 3: goto L_0x016f;
                default: goto L_0x0017;
            }
        L_0x0017:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "Unknown item type: "
            java.lang.StringBuilder r6 = r6.append(r7)
            r0 = r22
            int r0 = r0.m
            r7 = r0
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            r5.<init>(r6)
            throw r5
        L_0x0035:
            r5 = 0
            r16 = r5
            goto L_0x000f
        L_0x0039:
            r0 = r19
            boolean r0 = r0 instanceof com.tencent.launcher.AllAppsListView
            r5 = r0
            if (r5 != 0) goto L_0x006c
            r0 = r22
            long r0 = r0.n
            r5 = r0
            r9 = -1
            int r5 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r5 == 0) goto L_0x006c
            r0 = r22
            long r0 = r0.n
            r5 = r0
            r9 = -300(0xfffffffffffffed4, double:NaN)
            int r5 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r5 == 0) goto L_0x006c
            r0 = r19
            boolean r0 = r0 instanceof com.tencent.launcher.UserFolder
            r5 = r0
            if (r5 == 0) goto L_0x022b
            r0 = r19
            com.tencent.launcher.UserFolder r0 = (com.tencent.launcher.UserFolder) r0
            r5 = r0
            com.tencent.launcher.ex r5 = r5.f
            long r5 = r5.n
            r9 = -300(0xfffffffffffffed4, double:NaN)
            int r5 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r5 != 0) goto L_0x022b
        L_0x006c:
            com.tencent.launcher.am r5 = new com.tencent.launcher.am
            com.tencent.launcher.am r22 = (com.tencent.launcher.am) r22
            r0 = r5
            r1 = r22
            r0.<init>(r1)
            r0 = r5
            com.tencent.launcher.am r0 = (com.tencent.launcher.am) r0
            r22 = r0
            r6 = -1
            r5.l = r6
            r6 = -1
            r5.n = r6
            r6 = 0
            r0 = r6
            r1 = r22
            r1.f = r0
            r0 = r22
            android.content.Intent r0 = r0.c
            r6 = r0
            java.lang.String r7 = "android.intent.action.MAIN"
            r6.setAction(r7)
        L_0x0093:
            r0 = r18
            com.tencent.launcher.Launcher r0 = r0.s
            r6 = r0
            r7 = 2130903055(0x7f03000f, float:1.7412917E38)
            r0 = r5
            com.tencent.launcher.am r0 = (com.tencent.launcher.am) r0
            r22 = r0
            android.graphics.drawable.Drawable r9 = com.tencent.launcher.Launcher.sIconBackgroundDrawable
            r0 = r6
            r1 = r7
            r2 = r23
            r3 = r22
            r4 = r9
            android.view.View r6 = r0.createShortcut(r1, r2, r3, r4)
            r10 = r6
            r17 = r8
            r15 = r5
        L_0x00b1:
            r8 = 1
            r9 = 1
            r0 = r18
            int[] r0 = r0.n
            r12 = r0
            r5 = r18
            r6 = r20
            r7 = r21
            r11 = r23
            int[] r5 = r5.a(r6, r7, r8, r9, r10, r11, r12)
            r0 = r5
            r1 = r18
            r1.n = r0
            r0 = r18
            int[] r0 = r0.n
            r5 = r0
            if (r5 != 0) goto L_0x0188
            r5 = 0
            r0 = r19
            r1 = r18
            r2 = r5
            r0.a(r1, r2)
        L_0x00d9:
            return
        L_0x00da:
            r0 = r22
            long r0 = r0.n
            r5 = r0
            r9 = -1
            int r5 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r5 == 0) goto L_0x00f0
            r0 = r22
            long r0 = r0.n
            r5 = r0
            r9 = -300(0xfffffffffffffed4, double:NaN)
            int r5 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r5 != 0) goto L_0x014b
        L_0x00f0:
            r6 = 1
            com.tencent.launcher.bq r7 = new com.tencent.launcher.bq
            r0 = r22
            com.tencent.launcher.bq r0 = (com.tencent.launcher.bq) r0
            r5 = r0
            r7.<init>(r5)
            com.tencent.launcher.bq r22 = (com.tencent.launcher.bq) r22
            r0 = r22
            java.util.ArrayList r0 = r0.b
            r5 = r0
            java.util.Iterator r5 = r5.iterator()
        L_0x0106:
            boolean r8 = r5.hasNext()
            if (r8 == 0) goto L_0x0127
            java.lang.Object r22 = r5.next()
            com.tencent.launcher.am r22 = (com.tencent.launcher.am) r22
            com.tencent.launcher.am r8 = new com.tencent.launcher.am
            r0 = r8
            r1 = r22
            r0.<init>(r1)
            android.content.Intent r9 = r8.c
            java.lang.String r10 = "android.intent.action.MAIN"
            r9.setAction(r10)
            java.util.ArrayList r9 = r7.b
            r9.add(r8)
            goto L_0x0106
        L_0x0127:
            r0 = r18
            com.tencent.launcher.Launcher r0 = r0.s
            r5 = r0
            r0 = r5
            r1 = r23
            r2 = r7
            com.tencent.launcher.FolderIcon r5 = com.tencent.launcher.FolderIcon.a(r0, r1, r2)
            r8 = 0
            r7.j = r8
            r0 = r5
            com.tencent.launcher.FolderIcon r0 = (com.tencent.launcher.FolderIcon) r0
            r22 = r0
            r0 = r22
            r1 = r7
            r1.i = r0
            r8 = -1
            r7.n = r8
            r10 = r5
            r17 = r6
            r15 = r7
            goto L_0x00b1
        L_0x014b:
            r0 = r18
            com.tencent.launcher.Launcher r0 = r0.s
            r6 = r0
            r0 = r22
            com.tencent.launcher.bq r0 = (com.tencent.launcher.bq) r0
            r5 = r0
            r0 = r6
            r1 = r23
            r2 = r5
            com.tencent.launcher.FolderIcon r7 = com.tencent.launcher.FolderIcon.a(r0, r1, r2)
            r0 = r22
            com.tencent.launcher.bq r0 = (com.tencent.launcher.bq) r0
            r5 = r0
            r0 = r7
            com.tencent.launcher.FolderIcon r0 = (com.tencent.launcher.FolderIcon) r0
            r6 = r0
            r5.i = r6
            r10 = r7
            r17 = r8
            r15 = r22
            goto L_0x00b1
        L_0x016f:
            r0 = r18
            com.tencent.launcher.Launcher r0 = r0.s
            r6 = r0
            r0 = r22
            com.tencent.launcher.dq r0 = (com.tencent.launcher.dq) r0
            r5 = r0
            r0 = r6
            r1 = r23
            r2 = r5
            com.tencent.launcher.LiveFolderIcon r5 = com.tencent.launcher.LiveFolderIcon.a(r0, r1, r2)
            r10 = r5
            r17 = r8
            r15 = r22
            goto L_0x00b1
        L_0x0188:
            if (r24 == 0) goto L_0x0209
            r5 = 0
        L_0x018b:
            r0 = r23
            r1 = r10
            r2 = r5
            r0.addView(r1, r2)
            r0 = r18
            android.view.View$OnLongClickListener r0 = r0.r
            r5 = r0
            r10.setOnLongClickListener(r5)
            r0 = r18
            int[] r0 = r0.n
            r5 = r0
            r0 = r23
            r1 = r10
            r2 = r5
            r0.a(r1, r2)
            r0 = r18
            int[] r0 = r0.v
            r5 = r0
            r0 = r18
            int[] r0 = r0.n
            r6 = r0
            r7 = 0
            r6 = r6[r7]
            r0 = r18
            int[] r0 = r0.n
            r7 = r0
            r8 = 1
            r7 = r7[r8]
            r0 = r23
            r1 = r6
            r2 = r7
            r3 = r5
            r0.a(r1, r2, r3)
            r0 = r18
            int r0 = r0.W
            r6 = r0
            int r12 = r21 - r6
            r6 = 0
            r13 = r5[r6]
            r6 = 1
            r14 = r5[r6]
            r9 = r18
            r11 = r20
            r9.b(r10, r11, r12, r13, r14)
            android.view.ViewGroup$LayoutParams r19 = r10.getLayoutParams()
            com.tencent.launcher.CellLayout$LayoutParams r19 = (com.tencent.launcher.CellLayout.LayoutParams) r19
            com.tencent.launcher.ff r5 = com.tencent.launcher.Launcher.getModel()
            r5.a(r15)
            if (r17 == 0) goto L_0x020b
            com.tencent.launcher.bu r5 = new com.tencent.launcher.bu
            r0 = r18
            com.tencent.launcher.Launcher r0 = r0.s
            r6 = r0
            r0 = r15
            com.tencent.launcher.bq r0 = (com.tencent.launcher.bq) r0
            r7 = r0
            r0 = r19
            int r0 = r0.a
            r9 = r0
            r0 = r19
            int r0 = r0.b
            r10 = r0
            r8 = r16
            r5.<init>(r6, r7, r8, r9, r10)
            android.os.Handler r6 = com.tencent.launcher.base.BaseApp.d()
            r6.post(r5)
            goto L_0x00d9
        L_0x0209:
            r5 = -1
            goto L_0x018b
        L_0x020b:
            com.tencent.launcher.fy r5 = new com.tencent.launcher.fy
            r0 = r18
            com.tencent.launcher.Launcher r0 = r0.s
            r6 = r0
            r0 = r19
            int r0 = r0.a
            r9 = r0
            r0 = r19
            int r0 = r0.b
            r10 = r0
            r7 = r15
            r8 = r16
            r5.<init>(r6, r7, r8, r9, r10)
            android.os.Handler r6 = com.tencent.launcher.base.BaseApp.d()
            r6.post(r5)
            goto L_0x00d9
        L_0x022b:
            r5 = r22
            goto L_0x0093
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.launcher.Workspace.a(com.tencent.launcher.cm, int, int, java.lang.Object, com.tencent.launcher.CellLayout, boolean):void");
    }

    private int[] a(int i2, int i3, int i4, int i5, View view, CellLayout cellLayout, int[] iArr) {
        if (this.u == null) {
            this.u = cellLayout.a((boolean[]) null, view);
        }
        bs bsVar = this.u;
        int[] iArr2 = iArr != null ? iArr : new int[2];
        int[] iArr3 = cellLayout.a;
        double d2 = Double.MAX_VALUE;
        if (bsVar.g) {
            int size = bsVar.h.size();
            for (int i6 = 0; i6 < size; i6++) {
                bk bkVar = (bk) bsVar.h.get(i6);
                if (bkVar.c == i4 && bkVar.d == i5) {
                    cellLayout.a(bkVar.a, bkVar.b, iArr3);
                    double sqrt = Math.sqrt(Math.pow((double) (iArr3[0] - i2), 2.0d) + Math.pow((double) (iArr3[1] - i3), 2.0d));
                    if (sqrt <= d2) {
                        iArr2[0] = bkVar.a;
                        iArr2[1] = bkVar.b;
                        d2 = sqrt;
                    }
                }
            }
            if (d2 < Double.MAX_VALUE) {
                return iArr2;
            }
        }
        return null;
    }

    private void b(View view, int i2, int i3, int i4, int i5) {
        if (view != null) {
            this.ad = view;
            TranslateAnimation translateAnimation = new TranslateAnimation((float) (i2 - i4), 0.0f, (float) (i3 - i5), 0.0f);
            translateAnimation.setDuration(180);
            translateAnimation.setZAdjustment(1);
            view.bringToFront();
            view.startAnimation(translateAnimation);
            view.setVisibility(0);
        }
    }

    private void i(int i2) {
        this.f.setWallpaperOffsetSteps(1.0f / ((float) (getChildCount() - 1)), 0.0f);
        this.f.setWallpaperOffsets(getWindowToken(), ((float) (getScrollX() + (getWidth() / 2))) / ((float) i2), 0.0f);
    }

    private void j(int i2) {
        if (this.Q.getVisibility() != 0) {
            this.Q.setVisibility(0);
        }
        this.Q.setText("" + i2);
        this.Q.requestLayout();
        int i3 = i2 - 1;
        if (i3 < 0) {
            i3 = 0;
        }
        View childAt = this.F.getChildAt(i3);
        if (childAt != null) {
            int[] iArr = new int[2];
            childAt.getLocationInWindow(iArr);
            int height = this.Q.getHeight();
            int width = this.Q.getWidth();
            int height2 = childAt.getHeight();
            int width2 = childAt.getWidth();
            int paddingTop = getContext() instanceof Activity ? ((ViewGroup) ((Activity) getContext()).getWindow().getDecorView()).getChildAt(0).getPaddingTop() : 0;
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.Q.getLayoutParams();
            layoutParams.leftMargin = ((width2 / 2) + iArr[0]) - (width / 2);
            layoutParams.topMargin = ((iArr[1] + height2) - height) - paddingTop;
            this.Q.setLayoutParams(layoutParams);
        }
    }

    private void k(int i2) {
        int abs = Math.abs(i2 / getWidth());
        if (Math.abs(i2 % getWidth()) > getWidth() / 2) {
            abs++;
        }
        QNavigation qNavigation = this.F;
        int childCount = qNavigation.getChildCount();
        if (qNavigation != null && childCount > abs) {
            qNavigation.b(abs);
        }
    }

    public final void A() {
        this.y = true;
        this.D = false;
    }

    public final void B() {
        CellLayout cellLayout = (CellLayout) getChildAt(this.k.isFinished() ? this.i : this.j);
        int childCount = cellLayout.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = cellLayout.getChildAt(i2);
            childAt.destroyDrawingCache();
            childAt.setDrawingCacheEnabled(false);
        }
    }

    public final boolean C() {
        return ((CellLayout) getChildAt(this.i)).a();
    }

    public final int a(View view) {
        if (view != null) {
            ViewParent parent = view.getParent();
            int childCount = getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                if (parent == getChildAt(i2)) {
                    return i2;
                }
            }
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public final View a(CellLayout cellLayout) {
        bs bsVar = this.m;
        if (bsVar == null || cellLayout == null || bsVar.f != indexOfChild(cellLayout)) {
            return null;
        }
        return bsVar.a;
    }

    public final Folder a(Object obj) {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            CellLayout cellLayout = (CellLayout) getChildAt(i2);
            int childCount2 = cellLayout.getChildCount();
            for (int i3 = 0; i3 < childCount2; i3++) {
                View childAt = cellLayout.getChildAt(i3);
                CellLayout.LayoutParams layoutParams = (CellLayout.LayoutParams) childAt.getLayoutParams();
                if (layoutParams.c == 4 && layoutParams.d == 4 && (childAt instanceof Folder)) {
                    Folder folder = (Folder) childAt;
                    if (folder.e() == obj) {
                        return folder;
                    }
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final bs a(boolean[] zArr) {
        CellLayout cellLayout = (CellLayout) getChildAt(this.i);
        if (cellLayout != null) {
            return cellLayout.a(zArr, (View) null);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2) {
        if (this.u != null) {
            this.u.a();
            this.u = null;
        }
        if (this.i >= getChildCount()) {
            this.i = this.e;
        }
        this.i = Math.max(0, Math.min(i2, getChildCount() - 1));
        scrollTo(this.i * getWidth(), 0);
        this.F.b(this.i);
        invalidate();
    }

    public final void a(int i2, float f2) {
        int i3;
        int width = getWidth();
        if (!this.R) {
            this.R = true;
        }
        float f3 = f2 > 1.0f ? 1.0f : f2;
        if (i2 >= getChildCount() - 1) {
            i3 = getChildCount() - 1;
            f3 = 0.0f;
        } else {
            i3 = i2;
        }
        scrollTo(((int) (((float) width) * f3)) + (i3 * width), 0);
        j(((f3 <= 0.5f || i3 >= getChildCount() - 1) ? i3 : i3 + 1) + 1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, int, boolean):void
     arg types: [android.view.View, int, int, int, int, int, int]
     candidates:
      com.tencent.launcher.Workspace.a(int, int, int, int, android.view.View, com.tencent.launcher.CellLayout, int[]):int[]
      com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a(View view, int i2, int i3, int i4, int i5) {
        a(view, this.i, i2, i3, i4, i5, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, int, boolean):void
     arg types: [android.view.View, int, int, int, int, int, int]
     candidates:
      com.tencent.launcher.Workspace.a(int, int, int, int, android.view.View, com.tencent.launcher.CellLayout, int[]):int[]
      com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a(View view, int i2, int i3, int i4, int i5, int i6) {
        a(view, i2, i3, i4, i5, i6, false);
    }

    /* access modifiers changed from: package-private */
    public final void a(View view, int i2, int i3, int i4, int i5, int i6, boolean z2) {
        if (i2 >= 0 && i2 < getChildCount()) {
            if (this.u != null) {
                this.u.a();
                this.u = null;
            }
            CellLayout cellLayout = (CellLayout) getChildAt(i2);
            CellLayout.LayoutParams layoutParams = (CellLayout.LayoutParams) view.getLayoutParams();
            if (layoutParams == null) {
                layoutParams = new CellLayout.LayoutParams(i3, i4, i5, i6);
            } else {
                layoutParams.a = i3;
                layoutParams.b = i4;
                layoutParams.c = i5;
                layoutParams.d = i6;
            }
            if (layoutParams.a >= cellLayout.d() || layoutParams.a + layoutParams.c > cellLayout.d() || layoutParams.b >= cellLayout.e() || layoutParams.b + layoutParams.d > cellLayout.e()) {
                Log.e("Workspace", "over screen");
                return;
            }
            cellLayout.addView(view, z2 ? 0 : -1, layoutParams);
            if (!(view instanceof Folder)) {
                view.setOnLongClickListener(this.r);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(View view, int i2, int i3, int i4, int i5, boolean z2) {
        a(view, this.i, i2, i3, i4, i5, z2);
    }

    /* access modifiers changed from: package-private */
    public final void a(View view, bd bdVar, boolean z2) {
        a(view, bdVar.o, bdVar.p, bdVar.q, bdVar.r, bdVar.s, z2);
    }

    public final void a(View view, boolean z2) {
        View a;
        this.S = this.m;
        this.T = false;
        if (this.u != null) {
            this.u.a();
            this.u = null;
        }
        if (z2) {
            if (!(view == this || this.m == null || (view instanceof QNavigation))) {
                if (view instanceof DeleteZone) {
                    this.T = true;
                    if (!((DeleteZone) view).a) {
                        return;
                    }
                }
                if (!(view instanceof DockBar) || ((DockBar) view).b) {
                    CellLayout cellLayout = (CellLayout) getChildAt(this.m.f);
                    cellLayout.removeView(this.m.a);
                    Launcher.getModel().b((ha) this.m.a.getTag());
                    if (view instanceof DockBar) {
                        DockBar dockBar = (DockBar) view;
                        ha haVar = dockBar.a;
                        if (haVar != null) {
                            switch (haVar.m) {
                                case 0:
                                case 1:
                                    ha amVar = haVar.n == -1 ? new am((am) haVar) : haVar;
                                    haVar = amVar;
                                    a = this.s.createShortcut(R.layout.application, cellLayout, (am) amVar, Launcher.sIconBackgroundDrawable);
                                    break;
                                case 2:
                                    a = FolderIcon.a(this.s, cellLayout, (bq) haVar);
                                    break;
                                case 3:
                                    a = LiveFolderIcon.a(this.s, cellLayout, (dq) haVar);
                                    break;
                                default:
                                    throw new IllegalStateException("Unknown item type: " + haVar.m);
                            }
                            cellLayout.addView(a);
                            a.setOnLongClickListener(this.r);
                            this.n = new int[2];
                            this.n[0] = this.m.b;
                            this.n[1] = this.m.c;
                            cellLayout.a(a, this.n);
                            CellLayout.LayoutParams layoutParams = (CellLayout.LayoutParams) a.getLayoutParams();
                            Launcher.getModel().a(haVar);
                            ff.a(this.s, haVar, -100, this.m.f, layoutParams.a, layoutParams.b);
                            dockBar.a = null;
                        } else {
                            return;
                        }
                    }
                } else {
                    this.m = null;
                    return;
                }
            }
        } else if (this.m != null) {
            ((CellLayout) getChildAt(this.m.f)).a(this.m.a);
        }
        this.m = null;
    }

    public final void a(TextView textView) {
        this.Q = textView;
    }

    /* access modifiers changed from: package-private */
    public final void a(Launcher launcher) {
        this.s = launcher;
        D();
    }

    public final void a(QNavigation qNavigation) {
        this.F = qNavigation;
        this.F.a(this);
        G();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, int, boolean):void
     arg types: [android.view.View, int, int, int, int, int, int]
     candidates:
      com.tencent.launcher.Workspace.a(int, int, int, int, android.view.View, com.tencent.launcher.CellLayout, int[]):int[]
      com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a(am amVar) {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            CellLayout cellLayout = (CellLayout) getChildAt(i2);
            int childCount2 = cellLayout.getChildCount();
            int i3 = 0;
            while (true) {
                if (i3 >= childCount2) {
                    break;
                }
                View childAt = cellLayout.getChildAt(i3);
                Object tag = childAt.getTag();
                if (tag instanceof am) {
                    am amVar2 = (am) tag;
                    if (amVar2.l == amVar.l) {
                        if (amVar instanceof am) {
                            am amVar3 = amVar;
                            amVar2.a = amVar3.a.toString();
                            amVar2.c = new Intent(amVar3.c);
                            if (amVar3.h != null) {
                                amVar2.h = new Intent.ShortcutIconResource();
                                amVar2.h.packageName = amVar3.h.packageName;
                                amVar2.h.resourceName = amVar3.h.resourceName;
                            }
                            amVar2.d = amVar3.d;
                            amVar2.f = amVar3.f;
                            amVar2.g = amVar3.g;
                        }
                        View createShortcut = this.s.createShortcut(R.layout.application, cellLayout, amVar2);
                        cellLayout.removeView(childAt);
                        a(createShortcut, amVar.o, amVar.p, amVar.q, amVar.r, amVar.s, false);
                    }
                }
                i3++;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar, bs bsVar, boolean z2) {
        CellLayout cellLayout = (CellLayout) getChildAt(bsVar.f);
        int[] iArr = new int[2];
        cellLayout.a(bsVar.b, bsVar.c, iArr);
        a((cm) null, iArr[0], iArr[1], amVar, cellLayout, z2);
    }

    /* access modifiers changed from: package-private */
    public final void a(bs bsVar) {
        View view = bsVar.a;
        if (view.isInTouchMode() || (view instanceof Search)) {
            this.m = bsVar;
            this.m.f = this.i;
            this.aa = ((CellLayout) getChildAt(this.i)).b(view);
            this.t.a(view, this, view.getTag(), 0);
            invalidate();
        }
    }

    public final void a(cm cmVar, boolean z2) {
        if (this.u != null) {
            this.u.a();
            this.u = null;
        }
        if (this.Q.getVisibility() != 8) {
            this.Q.setVisibility(8);
        }
        this.Y = -1;
        CellLayout cellLayout = (CellLayout) getChildAt(this.k.isFinished() ? this.i : this.j);
        if (z2) {
            cellLayout.k();
            cellLayout.m();
            if (cellLayout.h()) {
                cellLayout.j();
            }
        }
    }

    public final void a(cy cyVar) {
        cyVar.a();
    }

    public final void a(dt dtVar) {
        this.t = dtVar;
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        boolean z2;
        ArrayList arrayList = new ArrayList();
        ff model = Launcher.getModel();
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            CellLayout cellLayout = (CellLayout) getChildAt(i2);
            int childCount2 = cellLayout.getChildCount();
            arrayList.clear();
            for (int i3 = 0; i3 < childCount2; i3++) {
                View childAt = cellLayout.getChildAt(i3);
                Object tag = childAt.getTag();
                if (tag instanceof am) {
                    am amVar = (am) tag;
                    ComponentName component = amVar.c.getComponent();
                    if (component != null && str.equals(component.getPackageName())) {
                        model.b(amVar);
                        ff.g(this.s, amVar);
                        arrayList.add(childAt);
                    }
                } else if (tag instanceof bq) {
                    ArrayList arrayList2 = ((bq) tag).b;
                    ArrayList arrayList3 = new ArrayList(1);
                    int size = arrayList2.size();
                    int i4 = 0;
                    boolean z3 = false;
                    while (i4 < size) {
                        am amVar2 = (am) arrayList2.get(i4);
                        ComponentName component2 = amVar2.c.getComponent();
                        if (component2 == null || !str.equals(component2.getPackageName())) {
                            z2 = z3;
                        } else {
                            arrayList3.add(amVar2);
                            ff.g(this.s, amVar2);
                            z2 = true;
                        }
                        i4++;
                        z3 = z2;
                    }
                    arrayList2.removeAll(arrayList3);
                    if (z3) {
                        Folder openFolder = this.s.getOpenFolder();
                        if (openFolder != null) {
                            openFolder.d();
                        }
                        if (childAt instanceof FolderIcon) {
                            ((FolderIcon) childAt).a();
                        }
                    }
                }
            }
            int size2 = arrayList.size();
            for (int i5 = 0; i5 < size2; i5++) {
                cellLayout.removeViewInLayout((View) arrayList.get(i5));
            }
            if (size2 > 0) {
                cellLayout.requestLayout();
                cellLayout.invalidate();
            }
        }
    }

    public final void a(String str, ha haVar) {
        this.U = str;
        this.V = haVar;
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z2) {
        this.L = z2;
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z2, int i2) {
        for (int i3 = 0; i3 < getChildCount(); i3++) {
            ((CellLayout) getChildAt(i3)).a(z2, i2);
        }
        this.W = z2 ? 0 : i2;
        requestLayout();
    }

    public final boolean a() {
        return this.O;
    }

    public final boolean a(int i2, Object obj) {
        ha haVar = (ha) obj;
        return haVar.m != 4 || (haVar.r <= 1 && haVar.s <= 1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.Workspace.a(com.tencent.launcher.cm, int, int, java.lang.Object, com.tencent.launcher.CellLayout, boolean):void
     arg types: [com.tencent.launcher.cm, int, int, java.lang.Object, com.tencent.launcher.CellLayout, int]
     candidates:
      com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, int):void
      com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, boolean):void
      com.tencent.launcher.Workspace.a(com.tencent.launcher.cm, int, int, int, int, java.lang.Object):boolean
      com.tencent.launcher.e.a(com.tencent.launcher.cm, int, int, int, int, java.lang.Object):boolean
      com.tencent.launcher.gw.a(com.tencent.launcher.cm, int, int, int, int, java.lang.Object):boolean
      com.tencent.launcher.Workspace.a(com.tencent.launcher.cm, int, int, java.lang.Object, com.tencent.launcher.CellLayout, boolean):void */
    public final boolean a(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
        if (this.Q.getVisibility() != 8) {
            this.Q.setVisibility(8);
        }
        CellLayout cellLayout = (CellLayout) getChildAt(this.k.isFinished() ? this.i : this.j);
        int indexOfChild = indexOfChild(cellLayout);
        if (cellLayout.a(cellLayout.a(i2, i3, i4, i5, this.aa, this.X), (ha) obj, this.m == null ? null : this.m.a, cmVar, i2 - i4, i3 - i5, this.t.b())) {
            return true;
        }
        if (cmVar != this) {
            a(cmVar, i2 - i4, i3 - i5, obj, cellLayout, false);
        } else if (this.m != null) {
            View view = this.m.a;
            this.n = a(i2 - i4, i3 - i5, this.m.d, this.m.e, view, cellLayout, this.n);
            if (this.n == null) {
                cmVar.a(this, false);
                return true;
            }
            if (indexOfChild != this.m.f) {
                ((CellLayout) getChildAt(this.m.f)).removeView(view);
                cellLayout.addView(view);
            }
            if (this.u != null) {
                this.u.a();
                this.u = null;
            }
            cellLayout.a(view, this.n);
            int[] iArr = this.v;
            cellLayout.a(this.n[0], this.n[1], iArr);
            b(view, i2 - i4, (i3 - i5) - this.W, iArr[0], iArr[1]);
            BaseApp.d().post(new eu(this, view, indexOfChild));
        }
        return true;
    }

    public final boolean a(cm cmVar, Object obj) {
        if (this.s.isDrawerOpen()) {
            return false;
        }
        CellLayout cellLayout = (CellLayout) getChildAt(this.k.isFinished() ? this.i : this.j);
        bs bsVar = this.m;
        int i2 = bsVar == null ? 1 : bsVar.d;
        int i3 = bsVar == null ? 1 : bsVar.e;
        if (this.u == null) {
            this.u = cellLayout.a((boolean[]) null, bsVar == null ? null : bsVar.a);
        }
        return this.u.a(this.w, i2, i3, false) || cellLayout.l();
    }

    public void addFocusables(ArrayList arrayList, int i2, int i3) {
        if (this.s.isDrawerClose()) {
            Folder openFolder = this.s.getOpenFolder();
            if (openFolder == null) {
                getChildAt(this.i).addFocusables(arrayList, i2);
                if (i2 == 17) {
                    if (this.i > 0) {
                        getChildAt(this.i - 1).addFocusables(arrayList, i2);
                    }
                } else if (i2 == 66 && this.i < getChildCount() - 1) {
                    getChildAt(this.i + 1).addFocusables(arrayList, i2);
                }
            } else {
                openFolder.addFocusables(arrayList, i2);
            }
        }
    }

    public void addView(View view) {
        if (!(view instanceof CellLayout)) {
            throw new IllegalArgumentException("A Workspace can only have CellLayout children.");
        }
        super.addView(view);
    }

    public void addView(View view, int i2) {
        if (!(view instanceof CellLayout)) {
            throw new IllegalArgumentException("A Workspace can only have CellLayout children.");
        }
        super.addView(view, i2);
    }

    public void addView(View view, int i2, int i3) {
        if (!(view instanceof CellLayout)) {
            throw new IllegalArgumentException("A Workspace can only have CellLayout children.");
        }
        super.addView(view, i2, i3);
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        if (!(view instanceof CellLayout)) {
            throw new IllegalArgumentException("A Workspace can only have CellLayout children.");
        }
        super.addView(view, i2, layoutParams);
    }

    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        if (!(view instanceof CellLayout)) {
            throw new IllegalArgumentException("A Workspace can only have CellLayout and FixedScreen children.");
        }
        super.addView(view, layoutParams);
    }

    /* access modifiers changed from: package-private */
    public final View b(CellLayout cellLayout) {
        if (cellLayout == null || this.i != indexOfChild(cellLayout)) {
            return null;
        }
        return this.ad;
    }

    public final View b(Object obj) {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            CellLayout cellLayout = (CellLayout) getChildAt(i2);
            int childCount2 = cellLayout.getChildCount();
            for (int i3 = 0; i3 < childCount2; i3++) {
                View childAt = cellLayout.getChildAt(i3);
                if (childAt.getTag() == obj) {
                    return childAt;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final ArrayList b() {
        int childCount = getChildCount();
        ArrayList arrayList = new ArrayList(childCount);
        for (int i2 = 0; i2 < childCount; i2++) {
            CellLayout cellLayout = (CellLayout) getChildAt(i2);
            int childCount2 = cellLayout.getChildCount();
            int i3 = 0;
            while (true) {
                if (i3 >= childCount2) {
                    break;
                }
                View childAt = cellLayout.getChildAt(i3);
                CellLayout.LayoutParams layoutParams = (CellLayout.LayoutParams) childAt.getLayoutParams();
                if (layoutParams.c == 4 && layoutParams.d == 4 && (childAt instanceof Folder)) {
                    arrayList.add((Folder) childAt);
                    break;
                }
                i3++;
            }
        }
        return arrayList;
    }

    public final void b(int i2) {
        if (this.u != null) {
            this.u.a();
            this.u = null;
        }
        F();
        int max = Math.max(0, Math.min(i2, getChildCount() - 1));
        boolean z2 = max != this.i;
        this.j = max;
        View focusedChild = getFocusedChild();
        if (focusedChild != null && z2 && focusedChild == getChildAt(this.i)) {
            focusedChild.clearFocus();
        }
        int width = (max * getWidth()) - getScrollX();
        int abs = (!z2 ? 200 : 1) + Math.abs(width * 2);
        int i3 = abs > 650 ? 650 : abs;
        this.k.startScroll(getScrollX(), 0, width, 0, i3);
        this.H.setDuration((long) i3);
        this.F.a(this.j, this.H);
        invalidate();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void
     arg types: [com.tencent.launcher.Launcher, com.tencent.launcher.bq, int, int, int, int, int]
     candidates:
      com.tencent.launcher.ff.a(android.database.Cursor, android.content.Context, int, int, int, int, android.content.Intent):com.tencent.launcher.am
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void */
    public final void b(View view) {
        ha haVar = this.ab;
        if (haVar instanceof am) {
            am amVar = (am) haVar;
            am amVar2 = (am) view.getTag();
            CellLayout cellLayout = (CellLayout) getChildAt(this.k.isFinished() ? this.i : this.j);
            if (this.m != null) {
                View view2 = this.m.a;
                ((CellLayout) getChildAt(this.m.f)).removeView(view2);
                Launcher.getModel().b((ha) view2.getTag());
            }
            cellLayout.removeView(view);
            if (this.u != null) {
                this.u.a();
                this.u = null;
            }
            CellLayout.LayoutParams layoutParams = (CellLayout.LayoutParams) view.getLayoutParams();
            bq bqVar = new bq();
            bqVar.h = getContext().getResources().getString(R.string.folder_name);
            ff.a((Context) this.s, (ha) bqVar, -100L, this.i, layoutParams.a, layoutParams.b, false);
            this.s.addDesktopItem(bqVar);
            this.s.addFolder(bqVar);
            bqVar.a(amVar2);
            bqVar.a(amVar);
            FolderIcon a = FolderIcon.a(this.s, cellLayout, bqVar);
            a.setOnLongClickListener(this.s);
            cellLayout.addView(a);
            cellLayout.a(a, new int[]{layoutParams.a, layoutParams.b});
            ff.a(this.s, amVar2, bqVar.l, 0, 0, 0);
            ff.a(this.s, amVar, bqVar.l, 0, 0, 0);
        }
    }

    public final void b(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
        int i6 = this.k.isFinished() ? this.i : this.j;
        if (this.Y == -1 || this.Y != i6) {
            this.Y = i6;
            this.Z = -1;
            ((CellLayout) getChildAt(this.k.isFinished() ? this.i : this.j)).g();
        }
        if (cmVar != this) {
            this.aa = -1;
        }
        ((CellLayout) getChildAt(this.k.isFinished() ? this.i : this.j)).n();
        this.ab = (ha) obj;
        if (this.u != null) {
            this.u.a();
            this.u = null;
        }
        if (this.Q.getVisibility() != 8) {
            this.Q.setVisibility(8);
        }
        c(cmVar, i2, i3, i4, i5, obj);
    }

    /* access modifiers changed from: package-private */
    public final void b(String str) {
        boolean z2;
        int childCount = getChildCount();
        Context context = getContext();
        for (int i2 = 0; i2 < childCount; i2++) {
            CellLayout cellLayout = (CellLayout) getChildAt(i2);
            int childCount2 = cellLayout.getChildCount();
            for (int i3 = 0; i3 < childCount2; i3++) {
                View childAt = cellLayout.getChildAt(i3);
                Object tag = childAt.getTag();
                if (tag instanceof am) {
                    am amVar = (am) tag;
                    Intent intent = amVar.c;
                    ComponentName component = intent.getComponent();
                    if ((amVar.m == 0 || amVar.m == 1) && "android.intent.action.MAIN".equals(intent.getAction()) && component != null && str.equals(component.getPackageName())) {
                        Drawable a = Launcher.getModel().a(this.s.getPackageManager(), amVar);
                        if (a == null || a == amVar.d) {
                            amVar.d = a.a(context, this.s.getPackageManager().getDefaultActivityIcon(), Launcher.sIconBackgroundDrawable, amVar);
                        } else {
                            amVar.d.setCallback(null);
                            amVar.d = a.a(context, a, Launcher.sIconBackgroundDrawable, amVar);
                        }
                        amVar.f = true;
                        amVar.e = true;
                        ((TextView) childAt).setCompoundDrawablesWithIntrinsicBounds((Drawable) null, amVar.d, (Drawable) null, (Drawable) null);
                    }
                } else if (tag instanceof bq) {
                    bq bqVar = (bq) tag;
                    boolean z3 = false;
                    Iterator it = bqVar.b.iterator();
                    while (it.hasNext()) {
                        am amVar2 = (am) it.next();
                        if (amVar2.c == null || amVar2.c.getComponent() == null || !amVar2.c.getComponent().getPackageName().equals(str)) {
                            z2 = z3;
                        } else {
                            Drawable a2 = Launcher.getModel().a(this.s.getPackageManager(), amVar2);
                            if (a2 == null || a2 == amVar2.d) {
                                amVar2.d = a.a(context, this.s.getPackageManager().getDefaultActivityIcon(), amVar2);
                            } else {
                                amVar2.d.setCallback(null);
                                amVar2.d = a.a(context, a2, amVar2);
                            }
                            z2 = true;
                        }
                        if (z2) {
                            if (childAt instanceof FolderIcon) {
                                ((FolderIcon) childAt).a();
                            } else if (bqVar.i != null) {
                                bqVar.i.a();
                            }
                        }
                        z3 = false;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(boolean z2) {
        if (this.f.getWallpaperInfo() != null) {
            this.am = true;
            this.ah = null;
            this.al = false;
            this.s.setWindowBackground(true);
        } else {
            if (!(this.f.getWallpaperInfo() != null) || this.ah == null) {
                this.am = false;
                Drawable drawable = this.f.getDrawable();
                if (drawable != null && (drawable instanceof BitmapDrawable)) {
                    this.ah = ((BitmapDrawable) drawable).getBitmap();
                }
                this.al = true;
                this.s.setWindowBackground(false);
                String str = "" + this.ah;
                if (str.equals(this.ac)) {
                    this.ac = str;
                    if (!z2) {
                        postDelayed(new ev(this), 200);
                    }
                } else {
                    this.ac = str;
                }
            }
        }
        requestLayout();
        invalidate();
    }

    public final boolean b(cy cyVar) {
        if (((int) Math.round(Math.log((double) cyVar.b()) * d)) >= 0) {
            return false;
        }
        this.s.showThumbnailManager();
        invalidate();
        return true;
    }

    /* access modifiers changed from: package-private */
    public final void c(int i2) {
        this.aa = i2;
    }

    public final void c(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
        int a;
        boolean z2;
        int a2;
        if (this.Q.getVisibility() != 8) {
            this.Q.setVisibility(8);
        }
        int i6 = this.k.isFinished() ? this.i : this.j;
        CellLayout cellLayout = (CellLayout) getChildAt(i6);
        ha haVar = (ha) obj;
        if (this.Y != i6) {
            cellLayout.g();
            CellLayout cellLayout2 = (CellLayout) getChildAt(this.Y);
            cellLayout2.m();
            cellLayout2.k();
            cellLayout2.i();
            this.Y = i6;
        }
        CellLayout cellLayout3 = (CellLayout) getChildAt(i6);
        int a3 = cellLayout3.a(i2, i3, i4, i5, this.aa, this.X);
        if (a3 == -1) {
            if (this.Z >= 0) {
                cellLayout3.k();
                cellLayout3.m();
                cellLayout3.n();
            }
            if (cellLayout3.h() && (a = cellLayout3.a(i2, i3, i4, i5)) != this.aa && cellLayout3.c(a) == null) {
                ((CellLayout) getChildAt(this.Y)).j();
            }
        } else if (a3 != this.Z) {
            if (this.Z >= 0) {
                cellLayout3.k();
                cellLayout3.m();
                cellLayout3.n();
            }
            if (a3 >= 0) {
                if (this.X[0] <= 0) {
                    z2 = cellLayout3.a(a3);
                } else if (haVar.m == 0 || haVar.m == 1) {
                    z2 = cellLayout3.b(a3);
                }
                if (!z2 && cellLayout3.h() && (a2 = cellLayout3.a(i2, i3, i4, i5)) != this.aa && cellLayout3.c(a2) == null) {
                    ((CellLayout) getChildAt(this.Y)).j();
                }
            }
            z2 = false;
            ((CellLayout) getChildAt(this.Y)).j();
        } else if (a3 >= 0) {
            if (this.X[0] <= 0) {
                cellLayout3.m();
                cellLayout3.a(a3);
            } else if (haVar.m == 0) {
                cellLayout3.k();
                cellLayout3.b(a3);
            }
        }
        this.Z = a3;
    }

    public final void c(boolean z2) {
        this.O = z2;
    }

    /* access modifiers changed from: package-private */
    public final boolean c() {
        return this.i == this.e;
    }

    public void computeScroll() {
        if (this.k.computeScrollOffset()) {
            scrollTo(this.k.getCurrX(), this.k.getCurrY());
            if (b.g && this.L) {
                i(getChildAt(getChildCount() - 1).getRight());
            }
            k(this.k.getCurrX());
            postInvalidate();
        } else if (this.j != -1) {
            this.i = Math.max(0, Math.min(this.j, getChildCount() - 1));
            Launcher.setScreen(this.i);
            this.j = -1;
            if (this.af) {
                int childCount = getChildCount();
                for (int i2 = 0; i2 < childCount; i2++) {
                    ((CellLayout) getChildAt(i2)).setChildrenDrawnWithCacheEnabled(false);
                }
                this.ae = false;
            }
            G();
        }
    }

    /* access modifiers changed from: package-private */
    public final int d() {
        return this.i;
    }

    public final void d(int i2) {
        if (this.k.isFinished()) {
            Folder openFolder = this.s.getOpenFolder();
            if (openFolder != null) {
                this.s.shadeViewsReserve((UserFolder) openFolder);
                this.y = false;
            }
            if (this.u != null) {
                this.u.a();
                this.u = null;
            }
            F();
            int max = Math.max(0, Math.min(i2, getChildCount() - 1));
            boolean z2 = max != this.i;
            this.j = max;
            View focusedChild = getFocusedChild();
            if (focusedChild != null && z2 && focusedChild == getChildAt(this.i)) {
                focusedChild.clearFocus();
            }
            int width = (max * getWidth()) - getScrollX();
            int abs = Math.abs(width) * 2;
            this.k.startScroll(getScrollX(), 0, width, 0, abs > 300 ? 300 : abs);
            this.H.setDuration(abs > 300 ? 300 : (long) abs);
            BaseApp.c();
            this.F.a(this.j, this.H);
            j(this.j + 1);
            invalidate();
        }
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        if (!b.g) {
            int scrollX = getScrollX();
            Bitmap bitmap = this.ah;
            float f2 = ((float) scrollX) * this.ak;
            if (((float) this.ai) + f2 < ((float) (getRight() - getLeft()))) {
                f2 = (float) ((getRight() - getLeft()) - this.ai);
            }
            if (getChildCount() <= 1) {
                f2 = (float) scrollX;
            } else if (scrollX < 0) {
                f2 = (float) scrollX;
            } else if (scrollX > (getChildCount() - 1) * getWidth()) {
                f2 = (float) ((getWidth() + scrollX) - this.ai);
            }
            float f3 = this.L ? f2 : (float) scrollX;
            if (!this.s.isDrawerOpen() && !this.s.mThumbnailManagerShowed) {
                if (bitmap != null && !bitmap.isRecycled()) {
                    canvas.drawBitmap(bitmap, f3, (float) ((((getBottom() - getTop()) - this.W) - this.aj) / 2), this.ag);
                }
                boolean z2 = this.q != 1 && this.j == -1 && !this.R;
                long drawingTime = getDrawingTime();
                if (z2) {
                    drawChild(canvas, getChildAt(this.i), drawingTime);
                } else if (this.j < 0 || this.j >= getChildCount() || Math.abs(this.i - this.j) != 1) {
                    this.K.a(canvas, drawingTime);
                } else {
                    this.K.b(canvas, drawingTime);
                }
            } else if (bitmap != null && !bitmap.isRecycled()) {
                canvas.drawBitmap(this.ah, f3, (float) ((((getBottom() - getTop()) - this.W) - this.aj) / 2), this.ag);
            }
        } else if (!this.s.isDrawerOpen() && !this.s.mThumbnailManagerShowed) {
            if (this.q != 1 && this.j == -1 && !this.R) {
                drawChild(canvas, getChildAt(this.i), getDrawingTime());
                return;
            }
            int i2 = this.j;
            long drawingTime2 = getDrawingTime();
            if (i2 < 0 || i2 >= getChildCount() || Math.abs(this.i - i2) != 1) {
                this.K.a(canvas, drawingTime2);
            } else {
                this.K.b(canvas, drawingTime2);
            }
        }
    }

    public boolean dispatchUnhandledMove(View view, int i2) {
        if (this.s.isDrawerOpen()) {
            return super.dispatchUnhandledMove(view, i2);
        }
        if (i2 == 17) {
            if (this.i > 0) {
                b(this.i - 1);
                return true;
            }
        } else if (i2 == 66 && this.i < getChildCount() - 1) {
            b(this.i + 1);
            return true;
        }
        return super.dispatchUnhandledMove(view, i2);
    }

    public boolean drawChild(Canvas canvas, View view, long j2) {
        return super.drawChild(canvas, view, j2);
    }

    /* access modifiers changed from: package-private */
    public final bs e() {
        int i2 = this.k.isFinished() ? this.i : this.j;
        CellLayout cellLayout = (CellLayout) getChildAt(i2);
        if (cellLayout == null) {
            return null;
        }
        int d2 = cellLayout.d();
        int e2 = cellLayout.e();
        boolean[][] zArr = (boolean[][]) Array.newInstance(Boolean.TYPE, d2, e2);
        Launcher.getModel().a(zArr, d2, e2, i2);
        return cellLayout.a(zArr, d2, e2);
    }

    /* access modifiers changed from: package-private */
    public final void e(int i2) {
        if (i2 == 0) {
            this.k = new Scroller(getContext(), new g());
        } else {
            this.k = new Scroller(getContext());
        }
        this.K = this.J.a(i2);
    }

    /* access modifiers changed from: package-private */
    public final int f() {
        int i2 = this.i;
        CellLayout cellLayout = (CellLayout) getChildAt(i2);
        if (cellLayout == null) {
            return 0;
        }
        int d2 = cellLayout.d();
        int e2 = cellLayout.e();
        boolean[][] zArr = (boolean[][]) Array.newInstance(Boolean.TYPE, d2, e2);
        Launcher.getModel().a(zArr, d2, e2, i2);
        int i3 = 0;
        int i4 = 0;
        while (i4 < d2) {
            int i5 = i3;
            for (int i6 = 0; i6 < e2; i6++) {
                if (!zArr[i4][i6]) {
                    i5++;
                }
            }
            i4++;
            i3 = i5;
        }
        return i3;
    }

    /* access modifiers changed from: package-private */
    public final void f(int i2) {
        this.W = i2;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
    }

    public final void g() {
        int width = getWidth();
        b((getScrollX() + (width / 2)) / width);
    }

    public final void g(int i2) {
        this.e = i2 >= getChildCount() ? getChildCount() - 1 : i2;
        this.i = this.e;
    }

    /* access modifiers changed from: package-private */
    public final void h() {
        this.F.d();
        this.E = true;
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < getChildCount()) {
                ((CellLayout) getChildAt(i3)).g();
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void i() {
        this.F.e();
        this.E = false;
        if (this.R) {
            this.R = false;
        }
        if (this.Q.getVisibility() != 8) {
            this.Q.setVisibility(8);
        }
        ((CellLayout) getChildAt(this.k.isFinished() ? this.i : this.j)).m();
        BaseApp.d().post(new et(this));
    }

    public void invalidate() {
        if (this.k == null || this.ah != null || this.J.b()) {
            super.invalidate();
            return;
        }
        CellLayout cellLayout = (CellLayout) getChildAt(this.k.isFinished() ? this.i : this.j);
        if (cellLayout == null) {
            super.invalidate();
        } else {
            super.invalidate(getScrollX(), cellLayout.b(), getScrollX() + getWidth(), getHeight() - cellLayout.c());
        }
    }

    /* access modifiers changed from: package-private */
    public final int j() {
        return this.aa;
    }

    public final void k() {
        if (this.R) {
            this.R = false;
        }
        if (this.Q.getVisibility() != 8) {
            this.Q.setVisibility(8);
        }
    }

    public final void l() {
        if (this.D) {
            if (this.u != null) {
                this.u.a();
                this.u = null;
            }
            if (this.j == -1 && this.i > 0 && this.k.isFinished()) {
                b(this.i - 1);
            }
        }
    }

    public final void m() {
        if (this.D) {
            if (this.u != null) {
                this.u.a();
                this.u = null;
            }
            if (this.j == -1 && this.i < getChildCount() - 1 && this.k.isFinished()) {
                b(this.i + 1);
            }
        }
    }

    public final Search n() {
        CellLayout cellLayout = (CellLayout) getChildAt(this.i);
        int childCount = cellLayout.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = cellLayout.getChildAt(i2);
            if (childAt instanceof Search) {
                return (Search) childAt;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final void o() {
        CellLayout cellLayout = (CellLayout) getChildAt(this.i);
        for (int i2 = 0; i2 < cellLayout.getChildCount(); i2++) {
            CellLayout.LayoutParams layoutParams = (CellLayout.LayoutParams) cellLayout.getChildAt(i2).getLayoutParams();
            if (!(layoutParams.j == layoutParams.a && layoutParams.k == layoutParams.b)) {
                ha haVar = (ha) cellLayout.getChildAt(i2).getTag();
                haVar.p = layoutParams.a;
                haVar.q = layoutParams.b;
                ff.e(getContext(), haVar);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.y || !this.s.isDrawerClose()) {
            return true;
        }
        if (this.G.a(motionEvent)) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action == 2 && this.q != 0) {
            return true;
        }
        float x2 = motionEvent.getX();
        float y2 = motionEvent.getY();
        switch (action) {
            case 0:
                this.o = x2;
                this.p = y2;
                this.x = true;
                this.q = this.k.isFinished() ? 0 : 1;
                break;
            case 1:
            case 3:
                if (this.q != 1 && !((CellLayout) getChildAt(this.i)).p()) {
                    getLocationOnScreen(this.v);
                    this.f.sendWallpaperCommand(getWindowToken(), "android.wallpaper.tap", this.v[0] + ((int) motionEvent.getX()), this.v[1] + ((int) motionEvent.getY()), 0, null);
                }
                this.af = true;
                this.q = 0;
                this.x = false;
                break;
            case 2:
                int abs = (int) Math.abs(x2 - this.o);
                int abs2 = (int) Math.abs(y2 - this.p);
                int i2 = this.z;
                boolean z2 = abs > i2;
                boolean z3 = abs2 > i2;
                if (z2 || z3) {
                    if (z2) {
                        this.q = 1;
                        F();
                    }
                    if (this.x) {
                        this.x = false;
                        getChildAt(this.i).cancelLongPress();
                        break;
                    }
                }
                break;
        }
        return this.q != 0;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        int i6 = 0;
        for (int i7 = 0; i7 < childCount; i7++) {
            View childAt = getChildAt(i7);
            if (childAt.getVisibility() != 8) {
                int measuredWidth = childAt.getMeasuredWidth();
                childAt.layout(i6, 0, i6 + measuredWidth, childAt.getMeasuredHeight());
                i6 += measuredWidth;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int size = View.MeasureSpec.getSize(i2);
        if (View.MeasureSpec.getMode(i2) != 1073741824) {
            throw new IllegalStateException("Workspace can only be used in EXACTLY mode.");
        } else if (View.MeasureSpec.getMode(i3) != 1073741824) {
            throw new IllegalStateException("Workspace can only be used in EXACTLY mode.");
        } else {
            int childCount = getChildCount();
            for (int i4 = 0; i4 < childCount; i4++) {
                getChildAt(i4).measure(i2, i3);
            }
            if (!b.g && this.al) {
                this.al = false;
                Bitmap a = a.a(this.ah, size, View.MeasureSpec.getSize(i3), getContext());
                this.ai = a.getWidth();
                this.aj = a.getHeight();
                this.ah = a;
            }
            if (!b.g) {
                int i5 = this.ai;
                this.ak = i5 > size ? ((float) ((childCount * size) - i5)) / (((float) (childCount - 1)) * ((float) size)) : 1.0f;
            }
            if (this.g) {
                this.J.a(getMeasuredWidth(), getMeasuredHeight());
                this.K.a(getMeasuredWidth());
                this.K.b(getMeasuredHeight());
                scrollTo(this.i * size, 0);
                G();
                i(size * (getChildCount() - 1));
                this.g = false;
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i2, Rect rect) {
        if (this.s.isDrawerClose()) {
            Folder openFolder = this.s.getOpenFolder();
            if (openFolder != null) {
                return openFolder.requestFocus(i2, rect);
            }
            int i3 = this.j != -1 ? this.j : this.i;
            if (getChildAt(i3) != null) {
                getChildAt(i3).requestFocus(i2, rect);
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (savedState.currentScreen != -1) {
            this.i = savedState.currentScreen;
            Launcher.setScreen(this.i);
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.currentScreen = this.i;
        return savedState;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.y || !this.s.isDrawerClose()) {
            return true;
        }
        if (this.l == null) {
            this.l = VelocityTracker.obtain();
        }
        this.l.addMovement(motionEvent);
        int action = motionEvent.getAction();
        float x2 = motionEvent.getX();
        switch (action) {
            case 0:
                if (!this.k.isFinished()) {
                    this.af = false;
                    this.k.abortAnimation();
                } else {
                    this.af = true;
                }
                this.o = x2;
                break;
            case 1:
                if (this.q == 1) {
                    VelocityTracker velocityTracker = this.l;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.A);
                    int xVelocity = (int) velocityTracker.getXVelocity();
                    int i2 = this.i;
                    if (xVelocity > 500 && i2 > 0) {
                        b(i2 - 1);
                    } else if (xVelocity >= -500 || i2 >= getChildCount() - 1) {
                        int width = getWidth();
                        b((getScrollX() + (width / 2)) / width);
                    } else {
                        b(i2 + 1);
                    }
                    if (this.l != null) {
                        this.l.recycle();
                        this.l = null;
                    }
                }
                this.q = 0;
                this.af = true;
                break;
            case 2:
                if (this.q == 1) {
                    int i3 = (int) (this.o - x2);
                    int width2 = getWidth() / 2;
                    int scrollX = getScrollX();
                    this.o = x2;
                    if (i3 < 0) {
                        if (scrollX >= 0 || this.O) {
                            scrollBy(Math.max((-width2) - scrollX, i3), 0);
                        } else {
                            scrollBy(Math.max((-width2) - scrollX, i3 / 2), 0);
                        }
                    } else if (i3 > 0) {
                        int right = (getChildAt(getChildCount() - 1).getRight() - scrollX) - getWidth();
                        if (scrollX <= (getChildCount() - 1) * getWidth() || this.O) {
                            scrollBy(Math.min(right + width2, i3), 0);
                        } else {
                            scrollBy(Math.min(right + width2, i3 / 2), 0);
                        }
                    }
                    if (b.g && this.L) {
                        i(getChildAt(getChildCount() - 1).getRight());
                    }
                    k(getScrollX());
                    break;
                }
                break;
            case 3:
                this.q = 0;
                break;
        }
        return true;
    }

    public final void p() {
        this.y = false;
    }

    public final void q() {
        this.y = true;
    }

    public final boolean r() {
        return this.x;
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z2) {
        if (this.s.isDrawerOpen()) {
            return false;
        }
        int indexOfChild = indexOfChild(view);
        if (indexOfChild == this.i && this.k.isFinished()) {
            return false;
        }
        if (!this.s.isWorkspaceLocked()) {
            b(indexOfChild);
        }
        return true;
    }

    public final void s() {
        this.x = false;
    }

    public void scrollTo(int i2, int i3) {
        int i4;
        if (this.O) {
            int width = getWidth();
            int childCount = getChildCount();
            if (i2 <= (-width) / 2) {
                i4 = (width * childCount) + i2;
            } else if (i2 >= (width * childCount) - (width / 2)) {
                i4 = i2 - (width * childCount);
            }
            this.K.a(i4, i3);
            super.scrollTo(i4, i3);
        }
        i4 = i2;
        this.K.a(i4, i3);
        super.scrollTo(i4, i3);
    }

    public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.r = onLongClickListener;
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            getChildAt(i2).setOnLongClickListener(onLongClickListener);
        }
    }

    /* access modifiers changed from: package-private */
    public final void t() {
        b(this.e);
        getChildAt(this.e).requestFocus();
    }

    /* access modifiers changed from: package-private */
    public final void u() {
        if (this.f.getWallpaperInfo() != null) {
            this.am = true;
            this.ah = null;
            this.al = false;
            this.s.setWindowBackground(true);
        }
    }

    public final int v() {
        return this.e;
    }

    public final Object w() {
        return this;
    }

    public final void x() {
        this.x = false;
    }

    public final void y() {
        this.k.forceFinished(true);
    }

    public final void z() {
        this.y = false;
        this.D = true;
    }
}
