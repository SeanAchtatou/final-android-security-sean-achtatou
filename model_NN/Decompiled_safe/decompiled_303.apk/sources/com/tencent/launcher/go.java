package com.tencent.launcher;

import android.widget.SectionIndexer;
import java.util.Arrays;

public final class go implements SectionIndexer {
    private final String[] a;
    private final int[] b;
    private final int c;

    public go(String[] strArr, int[] iArr) {
        if (strArr == null || iArr == null) {
            throw new NullPointerException();
        } else if (strArr.length != iArr.length) {
            throw new IllegalArgumentException("The sections and counts arrays must have the same length");
        } else {
            this.a = strArr;
            this.b = new int[iArr.length];
            int i = 0;
            for (int i2 = 0; i2 < iArr.length; i2++) {
                if (this.a[i2] == null) {
                    this.a[i2] = " ";
                } else {
                    this.a[i2] = this.a[i2].trim();
                }
                this.b[i2] = i;
                i += iArr[i2];
            }
            this.c = i;
        }
    }

    public final int getPositionForSection(int i) {
        if (i < 0 || i >= this.a.length) {
            return -1;
        }
        return this.b[i];
    }

    public final int getSectionForPosition(int i) {
        if (i < 0 || i >= this.c) {
            return -1;
        }
        int binarySearch = Arrays.binarySearch(this.b, i);
        return binarySearch < 0 ? (-binarySearch) - 2 : binarySearch;
    }

    public final Object[] getSections() {
        return this.a;
    }
}
