package com.tencent.launcher;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;

public class LiveFolder extends Folder {
    private AsyncTask a;

    public LiveFolder(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        super.a();
        requestFocus();
    }

    /* access modifiers changed from: package-private */
    public final void a(ex exVar) {
        super.a(exVar);
        if (this.a != null && this.a.getStatus() == AsyncTask.Status.RUNNING) {
            this.a.cancel(true);
        }
        this.a = new an(this).execute((dq) exVar);
    }

    /* access modifiers changed from: package-private */
    public final void a_() {
        super.a_();
        if (this.a != null && this.a.getStatus() == AsyncTask.Status.RUNNING) {
            this.a.cancel(true);
        }
        gq gqVar = (gq) this.c.getAdapter();
        if (gqVar != null) {
            gqVar.a();
        }
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        es esVar = (es) view.getTag();
        if (esVar.f) {
            Intent intent = ((dq) this.f).a;
            if (intent != null) {
                Intent intent2 = new Intent(intent);
                intent2.setData(intent.getData().buildUpon().appendPath(Long.toString(esVar.e)).build());
                this.d.startActivitySafely(intent2);
            }
        } else if (esVar.d != null) {
            this.d.startActivitySafely(esVar.d);
        }
    }

    public boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
        return false;
    }
}
