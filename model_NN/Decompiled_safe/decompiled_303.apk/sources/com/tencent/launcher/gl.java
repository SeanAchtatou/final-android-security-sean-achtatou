package com.tencent.launcher;

import android.view.View;
import android.view.animation.Animation;

final class gl implements Animation.AnimationListener {
    private /* synthetic */ CellLayout a;

    gl(CellLayout cellLayout) {
        this.a = cellLayout;
    }

    public final void onAnimationEnd(Animation animation) {
        CellLayout.a(this.a);
        if (this.a.y <= 0) {
            View[] c = this.a.x;
            for (int i = 0; i < c.length; i++) {
                if (c[i] != null) {
                    c[i].clearAnimation();
                }
            }
            boolean unused = this.a.B = true;
            if (this.a.C != null) {
                this.a.C.a();
            }
            this.a.requestLayout();
            this.a.n();
        }
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
