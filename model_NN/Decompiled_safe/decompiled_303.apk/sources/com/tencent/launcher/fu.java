package com.tencent.launcher;

import android.content.DialogInterface;

final class fu implements DialogInterface.OnClickListener {
    private /* synthetic */ cm a;
    private /* synthetic */ ha b;
    private /* synthetic */ DeleteZone c;

    fu(DeleteZone deleteZone, cm cmVar, ha haVar) {
        this.c = deleteZone;
        this.a = cmVar;
        this.b = haVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.c.a(this.a, this.b);
        this.c.a = true;
        this.a.a(this.c, true);
    }
}
