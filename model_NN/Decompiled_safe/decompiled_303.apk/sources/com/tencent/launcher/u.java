package com.tencent.launcher;

import android.appwidget.AppWidgetHost;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import com.tencent.launcher.base.BaseApp;
import com.tencent.launcher.base.b;
import com.tencent.module.qqwidget.a;
import com.tencent.module.qqwidget.h;
import com.tencent.qqlauncher.R;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.xmlpull.v1.XmlPullParserException;

final class u extends SQLiteOpenHelper {
    private final Context a;
    private final AppWidgetHost b;

    u(Context context) {
        super(context, "launcher.db", (SQLiteDatabase.CursorFactory) null, 6);
        this.a = context;
        this.b = new AppWidgetHost(context, Launcher.APPWIDGET_HOST_ID);
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private static int a(SQLiteDatabase sQLiteDatabase, Cursor cursor) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_id");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("intent");
        int columnIndexOrThrow3 = cursor.getColumnIndexOrThrow("title");
        int columnIndexOrThrow4 = cursor.getColumnIndexOrThrow("iconType");
        int columnIndexOrThrow5 = cursor.getColumnIndexOrThrow("icon");
        int columnIndexOrThrow6 = cursor.getColumnIndexOrThrow("iconPackage");
        int columnIndexOrThrow7 = cursor.getColumnIndexOrThrow("iconResource");
        int columnIndexOrThrow8 = cursor.getColumnIndexOrThrow("container");
        int columnIndexOrThrow9 = cursor.getColumnIndexOrThrow("itemType");
        int columnIndexOrThrow10 = cursor.getColumnIndexOrThrow("screen");
        int columnIndexOrThrow11 = cursor.getColumnIndexOrThrow("cellX");
        int columnIndexOrThrow12 = cursor.getColumnIndexOrThrow("cellY");
        int columnIndexOrThrow13 = cursor.getColumnIndexOrThrow("uri");
        int columnIndexOrThrow14 = cursor.getColumnIndexOrThrow("displayMode");
        int columnIndexOrThrow15 = cursor.getColumnIndexOrThrow("packageName");
        int columnIndexOrThrow16 = cursor.getColumnIndexOrThrow("layoutId");
        ContentValues[] contentValuesArr = new ContentValues[cursor.getCount()];
        int i = 0;
        while (cursor.moveToNext()) {
            ContentValues contentValues = new ContentValues(cursor.getColumnCount());
            contentValues.put("_id", Long.valueOf(cursor.getLong(columnIndexOrThrow)));
            contentValues.put("intent", cursor.getString(columnIndexOrThrow2));
            contentValues.put("title", cursor.getString(columnIndexOrThrow3));
            contentValues.put("iconType", Integer.valueOf(cursor.getInt(columnIndexOrThrow4)));
            contentValues.put("icon", cursor.getBlob(columnIndexOrThrow5));
            contentValues.put("iconPackage", cursor.getString(columnIndexOrThrow6));
            contentValues.put("iconResource", cursor.getString(columnIndexOrThrow7));
            contentValues.put("container", Integer.valueOf(cursor.getInt(columnIndexOrThrow8)));
            contentValues.put("itemType", Integer.valueOf(cursor.getInt(columnIndexOrThrow9)));
            contentValues.put("appWidgetId", (Integer) -1);
            contentValues.put("screen", Integer.valueOf(cursor.getInt(columnIndexOrThrow10)));
            contentValues.put("cellX", Integer.valueOf(cursor.getInt(columnIndexOrThrow11)));
            contentValues.put("cellY", Integer.valueOf(cursor.getInt(columnIndexOrThrow12)));
            contentValues.put("uri", cursor.getString(columnIndexOrThrow13));
            contentValues.put("displayMode", Integer.valueOf(cursor.getInt(columnIndexOrThrow14)));
            contentValues.put("packageName", cursor.getString(columnIndexOrThrow15));
            contentValues.put("layoutId", Integer.valueOf(cursor.getInt(columnIndexOrThrow16)));
            contentValuesArr[i] = contentValues;
            i++;
        }
        sQLiteDatabase.beginTransaction();
        int i2 = 0;
        try {
            for (ContentValues insert : contentValuesArr) {
                if (sQLiteDatabase.insert("favorites", null, insert) < 0) {
                    sQLiteDatabase.endTransaction();
                    return 0;
                }
                i2++;
            }
            sQLiteDatabase.setTransactionSuccessful();
            sQLiteDatabase.endTransaction();
            return i2;
        } catch (Throwable th) {
            sQLiteDatabase.endTransaction();
            throw th;
        }
    }

    private void a(int[] iArr, ArrayList arrayList) {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.LauncherAppWidgetBinder"));
        intent.setFlags(268435456);
        Bundle bundle = new Bundle();
        bundle.putIntArray("com.tencent.qqlauncher.settings.bindsources", iArr);
        bundle.putParcelableArrayList("com.tencent.qqlauncher.settings.bindtargets", arrayList);
        intent.putExtras(bundle);
        this.a.startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private boolean a(SQLiteDatabase sQLiteDatabase, ContentValues contentValues) {
        boolean z;
        int[] iArr = {1000};
        ArrayList arrayList = new ArrayList();
        arrayList.add(new ComponentName("com.android.alarmclock", "com.android.alarmclock.AnalogAppWidgetProvider"));
        try {
            int allocateAppWidgetId = this.b.allocateAppWidgetId();
            contentValues.put("itemType", (Integer) 1000);
            contentValues.put("spanX", (Integer) 2);
            contentValues.put("spanY", (Integer) 2);
            contentValues.put("appWidgetId", Integer.valueOf(allocateAppWidgetId));
            sQLiteDatabase.insert("favorites", null, contentValues);
            z = true;
        } catch (RuntimeException e) {
            Log.e("LauncherProvider", "Problem allocating appWidgetId", e);
            z = false;
        }
        if (z) {
            a(iArr, arrayList);
        }
        return z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private boolean a(SQLiteDatabase sQLiteDatabase, ContentValues contentValues, TypedArray typedArray) {
        String str;
        String str2;
        Resources resources = this.a.getResources();
        int resourceId = typedArray.getResourceId(5, 0);
        int resourceId2 = typedArray.getResourceId(6, 0);
        try {
            try {
                Intent intent = Intent.getIntent(typedArray.getString(7));
                if (resourceId == 0 || resourceId2 == 0) {
                    Log.w("HomeLoaders", "Shortcut is missing title or icon resource ID");
                    return false;
                }
                intent.setFlags(268435456);
                contentValues.put("intent", intent.toURI());
                contentValues.put("title", resources.getString(resourceId2));
                contentValues.put("itemType", (Integer) 1);
                contentValues.put("spanX", (Integer) 1);
                contentValues.put("spanY", (Integer) 1);
                contentValues.put("iconType", (Integer) 0);
                contentValues.put("iconPackage", this.a.getPackageName());
                contentValues.put("iconResource", this.a.getResources().getResourceName(resourceId));
                sQLiteDatabase.insert("favorites", null, contentValues);
                return true;
            } catch (URISyntaxException e) {
                str = str2;
            }
        } catch (URISyntaxException e2) {
            str = null;
        }
        Log.w("HomeLoaders", "Shortcut has malformed uri: " + str);
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private static boolean a(SQLiteDatabase sQLiteDatabase, ContentValues contentValues, TypedArray typedArray, PackageManager packageManager, Intent intent) {
        String string;
        String string2;
        String str;
        String str2 = null;
        if ("action".equals(typedArray.getString(10))) {
            String string3 = typedArray.getString(9);
            String string4 = typedArray.getString(7);
            String string5 = typedArray.getString(11);
            String string6 = typedArray.getString(12);
            if (string6 != null) {
                String[] split = string6.split("/");
                str2 = split[0];
                str = split[1];
                try {
                    ComponentName componentName = new ComponentName(str2, str);
                    ActivityInfo activityInfo = packageManager.getActivityInfo(componentName, 0);
                    intent.setComponent(componentName);
                    intent.setFlags(270532608);
                    if (intent.resolveActivityInfo(packageManager, 0) != null) {
                        contentValues.put("intent", intent.toURI());
                        contentValues.put("title", activityInfo.loadLabel(packageManager).toString());
                        contentValues.put("itemType", (Integer) 0);
                        contentValues.put("spanX", (Integer) 1);
                        contentValues.put("spanY", (Integer) 1);
                        sQLiteDatabase.insert("favorites", null, contentValues);
                        return true;
                    }
                } catch (PackageManager.NameNotFoundException e) {
                }
            } else {
                str = null;
            }
            Intent intent2 = new Intent(string3);
            if (string4 != null) {
                intent2.setData(Uri.parse(string4));
            }
            if (string5 != null) {
                intent2.setType(string5);
            }
            List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent2, 65536);
            if (queryIntentActivities.size() > 0) {
                Iterator<ResolveInfo> it = queryIntentActivities.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        string2 = str;
                        string = str2;
                        break;
                    }
                    ResolveInfo next = it.next();
                    if ((next.activityInfo.applicationInfo.flags & 1) != 0) {
                        ActivityInfo activityInfo2 = next.activityInfo;
                        string = activityInfo2.packageName;
                        string2 = activityInfo2.name;
                        break;
                    }
                }
            } else {
                return false;
            }
        } else {
            string = typedArray.getString(1);
            string2 = typedArray.getString(0);
        }
        if (string == null || string2 == null) {
            return false;
        }
        try {
            if (!b.h && string.equals(BaseApp.a) && string2.equals("com.tencent.android.ui.LocalSoftManageActivity")) {
                return false;
            }
            ComponentName componentName2 = new ComponentName(string, string2);
            ActivityInfo activityInfo3 = packageManager.getActivityInfo(componentName2, 0);
            intent.setComponent(componentName2);
            intent.setFlags(270532608);
            if (intent.resolveActivityInfo(packageManager, 0) == null) {
                return false;
            }
            contentValues.put("intent", intent.toURI());
            contentValues.put("title", activityInfo3.loadLabel(packageManager).toString());
            contentValues.put("itemType", (Integer) 0);
            contentValues.put("spanX", (Integer) 1);
            contentValues.put("spanY", (Integer) 1);
            sQLiteDatabase.insert("favorites", null, contentValues);
            return true;
        } catch (PackageManager.NameNotFoundException e2) {
            Log.w("LauncherProvider", "Unable to add favorite: " + string + "/" + string2, e2);
            return false;
        }
    }

    private boolean b(SQLiteDatabase sQLiteDatabase) {
        Cursor cursor;
        boolean z;
        Uri parse = Uri.parse("content://settings/old_favorites?notify=true");
        ContentResolver contentResolver = this.a.getContentResolver();
        try {
            cursor = contentResolver.query(parse, null, null, null, null);
        } catch (Exception e) {
            cursor = null;
        }
        if (cursor == null || cursor.getCount() <= 0) {
            z = false;
        } else {
            try {
                boolean z2 = a(sQLiteDatabase, cursor) > 0;
                if (z2) {
                    contentResolver.delete(parse, null, null);
                }
                z = z2;
            } finally {
                cursor.close();
            }
        }
        if (z) {
            d(sQLiteDatabase);
        }
        return z;
    }

    private boolean b(SQLiteDatabase sQLiteDatabase, ContentValues contentValues, TypedArray typedArray) {
        PackageInfo packageInfo;
        String string = typedArray.getString(1);
        String string2 = typedArray.getString(0);
        if (string == null || string2 == null) {
            return false;
        }
        if (string.equals("com.tencent.sc") && string2.equals("com.tencent.sc.qqwidget.QQQzoneWidgetService")) {
            try {
                packageInfo = this.a.getPackageManager().getPackageInfo("com.tencent.sc", 1);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                packageInfo = null;
            }
            if (packageInfo == null) {
                a a2 = h.a().a("com.tencent.qqlauncher", "com.tencent.qqwidget.QzoneWidgetDemoServices");
                if (a2 == null) {
                    return false;
                }
                contentValues.put("itemType", Integer.valueOf(a2.m));
                contentValues.put("spanX", Integer.valueOf(a2.r));
                contentValues.put("spanY", Integer.valueOf(a2.s));
                contentValues.put("title", a2.b);
                contentValues.put("packageName", a2.e);
                contentValues.put("className", a2.f);
                contentValues.put("layoutId", Integer.valueOf(a2.h));
                contentValues.put("appWidgetId", Integer.valueOf(a2.d));
                sQLiteDatabase.insert("favorites", null, contentValues);
                return true;
            }
        }
        a a3 = h.a().a(string, string2);
        if (a3 == null) {
            return false;
        }
        contentValues.put("itemType", Integer.valueOf(a3.m));
        contentValues.put("spanX", Integer.valueOf(a3.r));
        contentValues.put("spanY", Integer.valueOf(a3.s));
        contentValues.put("title", a3.b);
        contentValues.put("packageName", a3.e);
        contentValues.put("className", a3.f);
        contentValues.put("layoutId", Integer.valueOf(a3.h));
        contentValues.put("appWidgetId", Integer.valueOf(a3.d));
        sQLiteDatabase.insert("favorites", null, contentValues);
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x00f0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean c(android.database.sqlite.SQLiteDatabase r13) {
        /*
            r11 = 0
            r10 = 1
            r8 = 0
            java.lang.String r0 = "itemType"
            int[] r1 = new int[r10]
            r1[r11] = r10
            java.lang.String r3 = com.tencent.launcher.LauncherProvider.a(r0, r1)
            r13.beginTransaction()
            java.lang.String r1 = "favorites"
            r0 = 2
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ SQLException -> 0x0106, all -> 0x0101 }
            r0 = 0
            java.lang.String r4 = "_id"
            r2[r0] = r4     // Catch:{ SQLException -> 0x0106, all -> 0x0101 }
            r0 = 1
            java.lang.String r4 = "intent"
            r2[r0] = r4     // Catch:{ SQLException -> 0x0106, all -> 0x0101 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r13
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLException -> 0x0106, all -> 0x0101 }
            android.content.ContentValues r1 = new android.content.ContentValues     // Catch:{ SQLException -> 0x00c8, all -> 0x00e7 }
            r1.<init>()     // Catch:{ SQLException -> 0x00c8, all -> 0x00e7 }
            java.lang.String r2 = "_id"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ SQLException -> 0x00c8, all -> 0x00e7 }
            java.lang.String r3 = "intent"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ SQLException -> 0x00c8, all -> 0x00e7 }
        L_0x0039:
            if (r0 == 0) goto L_0x00f4
            boolean r4 = r0.moveToNext()     // Catch:{ SQLException -> 0x00c8, all -> 0x00e7 }
            if (r4 == 0) goto L_0x00f4
            long r4 = r0.getLong(r2)     // Catch:{ SQLException -> 0x00c8, all -> 0x00e7 }
            java.lang.String r6 = r0.getString(r3)     // Catch:{ SQLException -> 0x00c8, all -> 0x00e7 }
            if (r6 == 0) goto L_0x0039
            android.content.Intent r6 = android.content.Intent.getIntent(r6)     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            java.lang.String r7 = "Home"
            java.lang.String r8 = r6.toString()     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            android.util.Log.d(r7, r8)     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            android.net.Uri r7 = r6.getData()     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            java.lang.String r8 = r7.toString()     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            java.lang.String r9 = "android.intent.action.VIEW"
            java.lang.String r6 = r6.getAction()     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            boolean r6 = r9.equals(r6)     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            if (r6 == 0) goto L_0x0039
            java.lang.String r6 = "content://contacts/people/"
            boolean r6 = r8.startsWith(r6)     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            if (r6 != 0) goto L_0x007c
            java.lang.String r6 = "content://com.android.contacts/contacts/lookup/"
            boolean r6 = r8.startsWith(r6)     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            if (r6 == 0) goto L_0x0039
        L_0x007c:
            android.content.Intent r6 = new android.content.Intent     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            java.lang.String r8 = "com.android.contacts.action.QUICK_CONTACT"
            r6.<init>(r8)     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            r8 = 337641472(0x14200000, float:8.077936E-27)
            r6.setFlags(r8)     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            r6.setData(r7)     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            java.lang.String r7 = "mode"
            r8 = 3
            r6.putExtra(r7, r8)     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            java.lang.String r7 = "exclude_mimes"
            r8 = 0
            r6.putExtra(r7, r8)     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            r1.clear()     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            java.lang.String r7 = "intent"
            java.lang.String r6 = r6.toURI()     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            r1.put(r7, r6)     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            r6.<init>()     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            java.lang.String r7 = "_id="
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            java.lang.StringBuilder r4 = r6.append(r4)     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            java.lang.String r4 = r4.toString()     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            java.lang.String r5 = "favorites"
            r6 = 0
            r13.update(r5, r1, r4, r6)     // Catch:{ RuntimeException -> 0x00be, URISyntaxException -> 0x00dd }
            goto L_0x0039
        L_0x00be:
            r4 = move-exception
            java.lang.String r5 = "LauncherProvider"
            java.lang.String r6 = "Problem upgrading shortcut"
            android.util.Log.e(r5, r6, r4)     // Catch:{ SQLException -> 0x00c8, all -> 0x00e7 }
            goto L_0x0039
        L_0x00c8:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x00cc:
            java.lang.String r2 = "LauncherProvider"
            java.lang.String r3 = "Problem while upgrading contacts"
            android.util.Log.w(r2, r3, r0)     // Catch:{ all -> 0x0104 }
            r13.endTransaction()
            if (r1 == 0) goto L_0x00db
            r1.close()
        L_0x00db:
            r0 = r11
        L_0x00dc:
            return r0
        L_0x00dd:
            r4 = move-exception
            java.lang.String r5 = "LauncherProvider"
            java.lang.String r6 = "Problem upgrading shortcut"
            android.util.Log.e(r5, r6, r4)     // Catch:{ SQLException -> 0x00c8, all -> 0x00e7 }
            goto L_0x0039
        L_0x00e7:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x00eb:
            r13.endTransaction()
            if (r1 == 0) goto L_0x00f3
            r1.close()
        L_0x00f3:
            throw r0
        L_0x00f4:
            r13.setTransactionSuccessful()     // Catch:{ SQLException -> 0x00c8, all -> 0x00e7 }
            r13.endTransaction()
            if (r0 == 0) goto L_0x00ff
            r0.close()
        L_0x00ff:
            r0 = r10
            goto L_0x00dc
        L_0x0101:
            r0 = move-exception
            r1 = r8
            goto L_0x00eb
        L_0x0104:
            r0 = move-exception
            goto L_0x00eb
        L_0x0106:
            r0 = move-exception
            r1 = r8
            goto L_0x00cc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.launcher.u.c(android.database.sqlite.SQLiteDatabase):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00d2, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00d3, code lost:
        r13 = r1;
        r1 = r0;
        r0 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00de, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00df, code lost:
        r2 = r0;
        r0 = r1;
        r1 = false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00d2 A[ExcHandler: all (r1v10 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:3:0x0042] */
    /* JADX WARNING: Removed duplicated region for block: B:53:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void d(android.database.sqlite.SQLiteDatabase r15) {
        /*
            r14 = this;
            r12 = 1
            r0 = 2
            r11 = 0
            r10 = 0
            int[] r8 = new int[r0]
            r8 = {1000, 1002} // fill-array
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            android.content.ComponentName r0 = new android.content.ComponentName
            java.lang.String r1 = "com.android.alarmclock"
            java.lang.String r2 = "com.android.alarmclock.AnalogAppWidgetProvider"
            r0.<init>(r1, r2)
            r9.add(r0)
            android.content.ComponentName r0 = new android.content.ComponentName
            java.lang.String r1 = "com.android.camera"
            java.lang.String r2 = "com.android.camera.PhotoAppWidgetProvider"
            r0.<init>(r1, r2)
            r9.add(r0)
            java.lang.String r0 = "itemType"
            java.lang.String r3 = com.tencent.launcher.LauncherProvider.a(r0, r8)
            r15.beginTransaction()
            java.lang.String r1 = "favorites"
            r0 = 1
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ SQLException -> 0x00da, all -> 0x00c7 }
            r0 = 0
            java.lang.String r4 = "_id"
            r2[r0] = r4     // Catch:{ SQLException -> 0x00da, all -> 0x00c7 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r15
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLException -> 0x00da, all -> 0x00c7 }
            android.content.ContentValues r1 = new android.content.ContentValues     // Catch:{ SQLException -> 0x00de, all -> 0x00d2 }
            r1.<init>()     // Catch:{ SQLException -> 0x00de, all -> 0x00d2 }
            r2 = r11
        L_0x0048:
            if (r0 == 0) goto L_0x00ba
            boolean r3 = r0.moveToNext()     // Catch:{ SQLException -> 0x009f, all -> 0x00d2 }
            if (r3 == 0) goto L_0x00ba
            r3 = 0
            long r3 = r0.getLong(r3)     // Catch:{ SQLException -> 0x009f, all -> 0x00d2 }
            android.appwidget.AppWidgetHost r5 = r14.b     // Catch:{ RuntimeException -> 0x0096 }
            int r5 = r5.allocateAppWidgetId()     // Catch:{ RuntimeException -> 0x0096 }
            r1.clear()     // Catch:{ RuntimeException -> 0x0096 }
            java.lang.String r6 = "appWidgetId"
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ RuntimeException -> 0x0096 }
            r1.put(r6, r5)     // Catch:{ RuntimeException -> 0x0096 }
            java.lang.String r5 = "spanX"
            r6 = 2
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ RuntimeException -> 0x0096 }
            r1.put(r5, r6)     // Catch:{ RuntimeException -> 0x0096 }
            java.lang.String r5 = "spanY"
            r6 = 2
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ RuntimeException -> 0x0096 }
            r1.put(r5, r6)     // Catch:{ RuntimeException -> 0x0096 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0096 }
            r5.<init>()     // Catch:{ RuntimeException -> 0x0096 }
            java.lang.String r6 = "_id="
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ RuntimeException -> 0x0096 }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ RuntimeException -> 0x0096 }
            java.lang.String r3 = r3.toString()     // Catch:{ RuntimeException -> 0x0096 }
            java.lang.String r4 = "favorites"
            r5 = 0
            r15.update(r4, r1, r3, r5)     // Catch:{ RuntimeException -> 0x0096 }
            r2 = r12
            goto L_0x0048
        L_0x0096:
            r3 = move-exception
            java.lang.String r4 = "LauncherProvider"
            java.lang.String r5 = "Problem allocating appWidgetId"
            android.util.Log.e(r4, r5, r3)     // Catch:{ SQLException -> 0x009f, all -> 0x00d2 }
            goto L_0x0048
        L_0x009f:
            r1 = move-exception
            r13 = r1
            r1 = r2
            r2 = r0
            r0 = r13
        L_0x00a4:
            java.lang.String r3 = "LauncherProvider"
            java.lang.String r4 = "Problem while allocating appWidgetIds for existing widgets"
            android.util.Log.w(r3, r4, r0)     // Catch:{ all -> 0x00d7 }
            r15.endTransaction()
            if (r2 == 0) goto L_0x00e3
            r2.close()
            r0 = r1
        L_0x00b4:
            if (r0 == 0) goto L_0x00b9
            r14.a(r8, r9)
        L_0x00b9:
            return
        L_0x00ba:
            r15.setTransactionSuccessful()     // Catch:{ SQLException -> 0x009f, all -> 0x00d2 }
            r15.endTransaction()
            if (r0 == 0) goto L_0x00e5
            r0.close()
            r0 = r2
            goto L_0x00b4
        L_0x00c7:
            r0 = move-exception
            r1 = r10
        L_0x00c9:
            r15.endTransaction()
            if (r1 == 0) goto L_0x00d1
            r1.close()
        L_0x00d1:
            throw r0
        L_0x00d2:
            r1 = move-exception
            r13 = r1
            r1 = r0
            r0 = r13
            goto L_0x00c9
        L_0x00d7:
            r0 = move-exception
            r1 = r2
            goto L_0x00c9
        L_0x00da:
            r0 = move-exception
            r1 = r11
            r2 = r10
            goto L_0x00a4
        L_0x00de:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r11
            goto L_0x00a4
        L_0x00e3:
            r0 = r1
            goto L_0x00b4
        L_0x00e5:
            r0 = r2
            goto L_0x00b4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.launcher.u.d(android.database.sqlite.SQLiteDatabase):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0162 A[LOOP:2: B:37:0x0160->B:38:0x0162, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void e(android.database.sqlite.SQLiteDatabase r15) {
        /*
            r14 = this;
            r13 = 2
            r10 = 1
            r12 = 0
            r11 = 0
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r1 = "android.intent.action.MAIN"
            r0.<init>(r1)
            java.lang.String r1 = "android.intent.category.LAUNCHER"
            r0.addCategory(r1)
            android.content.Context r0 = r14.a
            android.content.pm.PackageManager r0 = r0.getPackageManager()
            android.content.ContentValues r1 = new android.content.ContentValues
            r1.<init>()
            android.content.Context r2 = r14.a     // Catch:{ XmlPullParserException -> 0x01bd, IOException -> 0x0147 }
            android.content.res.Resources r2 = r2.getResources()     // Catch:{ XmlPullParserException -> 0x01bd, IOException -> 0x0147 }
            r3 = 2131034112(0x7f050000, float:1.7678732E38)
            android.content.res.XmlResourceParser r2 = r2.getXml(r3)     // Catch:{ XmlPullParserException -> 0x01bd, IOException -> 0x0147 }
            android.util.AttributeSet r3 = android.util.Xml.asAttributeSet(r2)     // Catch:{ XmlPullParserException -> 0x01bd, IOException -> 0x0147 }
            java.lang.String r4 = "folders"
            com.tencent.util.a.a(r2, r4)     // Catch:{ XmlPullParserException -> 0x01bd, IOException -> 0x0147 }
            int r4 = r2.getDepth()     // Catch:{ XmlPullParserException -> 0x01bd, IOException -> 0x0147 }
            r5 = r11
        L_0x0035:
            int r6 = r2.next()     // Catch:{ XmlPullParserException -> 0x0092, IOException -> 0x01ba }
            r7 = 3
            if (r6 != r7) goto L_0x0042
            int r7 = r2.getDepth()     // Catch:{ XmlPullParserException -> 0x0092, IOException -> 0x01ba }
            if (r7 <= r4) goto L_0x0144
        L_0x0042:
            if (r6 == r10) goto L_0x0144
            if (r6 != r13) goto L_0x0035
            java.lang.String r6 = r2.getName()     // Catch:{ XmlPullParserException -> 0x0092, IOException -> 0x01ba }
            android.content.Context r7 = r14.a     // Catch:{ XmlPullParserException -> 0x0092, IOException -> 0x01ba }
            int[] r8 = com.tencent.a.a.h     // Catch:{ XmlPullParserException -> 0x0092, IOException -> 0x01ba }
            android.content.res.TypedArray r7 = r7.obtainStyledAttributes(r3, r8)     // Catch:{ XmlPullParserException -> 0x0092, IOException -> 0x01ba }
            r1.clear()     // Catch:{ XmlPullParserException -> 0x0092, IOException -> 0x01ba }
            java.lang.String r8 = "folder"
            boolean r6 = r8.equals(r6)     // Catch:{ XmlPullParserException -> 0x0092, IOException -> 0x01ba }
            if (r6 == 0) goto L_0x008e
            r6 = 0
            java.lang.String r6 = r7.getString(r6)     // Catch:{ XmlPullParserException -> 0x0092, IOException -> 0x01ba }
            r8 = 1
            java.lang.String r8 = r7.getString(r8)     // Catch:{ XmlPullParserException -> 0x0092, IOException -> 0x01ba }
            java.lang.String r9 = "position"
            r1.put(r9, r6)     // Catch:{ XmlPullParserException -> 0x0092, IOException -> 0x01ba }
            java.lang.String r6 = "container"
            r9 = -300(0xfffffffffffffed4, float:NaN)
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ XmlPullParserException -> 0x0092, IOException -> 0x01ba }
            r1.put(r6, r9)     // Catch:{ XmlPullParserException -> 0x0092, IOException -> 0x01ba }
            java.lang.String r6 = "title"
            r1.put(r6, r8)     // Catch:{ XmlPullParserException -> 0x0092, IOException -> 0x01ba }
            java.lang.String r6 = "itemType"
            r8 = 2
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ XmlPullParserException -> 0x0092, IOException -> 0x01ba }
            r1.put(r6, r8)     // Catch:{ XmlPullParserException -> 0x0092, IOException -> 0x01ba }
            java.lang.String r6 = "applications"
            r8 = 0
            r15.insert(r6, r8, r1)     // Catch:{ XmlPullParserException -> 0x0092, IOException -> 0x01ba }
            int r5 = r5 + 1
        L_0x008e:
            r7.recycle()     // Catch:{ XmlPullParserException -> 0x0092, IOException -> 0x01ba }
            goto L_0x0035
        L_0x0092:
            r2 = move-exception
            r3 = r5
        L_0x0094:
            java.lang.String r4 = "LauncherProvider"
            java.lang.String r5 = "Got exception parsing favorites."
            android.util.Log.w(r4, r5, r2)
            r2 = r3
        L_0x009c:
            android.content.Intent r3 = new android.content.Intent
            java.lang.String r4 = "android.intent.action.MAIN"
            r3.<init>(r4, r12)
            java.lang.String r4 = "android.intent.category.LAUNCHER"
            r3.addCategory(r4)
            java.util.List r3 = r0.queryIntentActivities(r3, r11)
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            int r5 = r3.size()
            r6 = r11
        L_0x00b6:
            if (r6 >= r5) goto L_0x0153
            java.lang.Object r14 = r3.get(r6)
            android.content.pm.ResolveInfo r14 = (android.content.pm.ResolveInfo) r14
            android.content.ComponentName r7 = new android.content.ComponentName
            android.content.pm.ActivityInfo r8 = r14.activityInfo
            android.content.pm.ApplicationInfo r8 = r8.applicationInfo
            java.lang.String r8 = r8.packageName
            android.content.pm.ActivityInfo r9 = r14.activityInfo
            java.lang.String r9 = r9.name
            r7.<init>(r8, r9)
            java.lang.String r8 = r7.getPackageName()
            java.lang.String r9 = "com.tencent.qqlauncher.theme"
            boolean r8 = r8.startsWith(r9)
            if (r8 != 0) goto L_0x0140
            com.tencent.launcher.am r8 = new com.tencent.launcher.am
            r8.<init>()
            r9 = -300(0xfffffffffffffed4, double:NaN)
            r8.n = r9
            r8.m = r11
            java.lang.CharSequence r9 = r14.loadLabel(r0)
            r8.a = r9
            java.lang.CharSequence r9 = r8.a
            if (r9 != 0) goto L_0x00f4
            android.content.pm.ActivityInfo r9 = r14.activityInfo
            java.lang.String r9 = r9.name
            r8.a = r9
        L_0x00f4:
            java.lang.CharSequence r9 = r8.a
            java.lang.String r9 = r9.toString()
            java.lang.String r9 = com.tencent.launcher.home.c.b(r9)
            r8.b = r9
            java.lang.String r9 = r8.b
            if (r9 == 0) goto L_0x010e
            java.lang.String r9 = ""
            java.lang.String r10 = r8.b
            boolean r9 = r9.equals(r10)
            if (r9 == 0) goto L_0x0116
        L_0x010e:
            java.lang.CharSequence r9 = r8.a
            java.lang.String r9 = r9.toString()
            r8.b = r9
        L_0x0116:
            android.content.Intent r9 = new android.content.Intent
            java.lang.String r10 = "android.intent.action.MAIN"
            r9.<init>(r10, r12)
            java.lang.String r10 = "android.intent.category.LAUNCHER"
            r9.addCategory(r10)
            r9.setComponent(r7)
            r7 = 270532608(0x10200000, float:3.1554436E-29)
            r9.setFlags(r7)
            r8.c = r9
            android.content.pm.ActivityInfo r7 = r14.activityInfo
            android.content.pm.ApplicationInfo r7 = r7.applicationInfo
            java.lang.String r7 = r7.sourceDir
            java.io.File r9 = new java.io.File
            r9.<init>(r7)
            long r9 = r9.lastModified()
            r8.j = r9
            r4.add(r8)
        L_0x0140:
            int r6 = r6 + 1
            goto L_0x00b6
        L_0x0144:
            r2 = r5
            goto L_0x009c
        L_0x0147:
            r2 = move-exception
            r3 = r11
        L_0x0149:
            java.lang.String r4 = "LauncherProvider"
            java.lang.String r5 = "Got exception parsing favorites."
            android.util.Log.w(r4, r5, r2)
            r2 = r3
            goto L_0x009c
        L_0x0153:
            com.tencent.launcher.fd r0 = new com.tencent.launcher.fd
            r0.<init>()
            java.util.Collections.sort(r4, r0)
            int r0 = r4.size()
            r3 = r11
        L_0x0160:
            if (r3 >= r0) goto L_0x01b9
            java.lang.Object r14 = r4.get(r3)
            com.tencent.launcher.am r14 = (com.tencent.launcher.am) r14
            int r5 = r2 + r3
            r14.t = r5
            r1.clear()
            java.lang.String r5 = "container"
            long r6 = r14.n
            java.lang.Long r6 = java.lang.Long.valueOf(r6)
            r1.put(r5, r6)
            java.lang.String r5 = "position"
            int r6 = r14.t
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            r1.put(r5, r6)
            java.lang.String r5 = "itemType"
            int r6 = r14.m
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            r1.put(r5, r6)
            java.lang.String r5 = "title"
            java.lang.CharSequence r6 = r14.a
            java.lang.String r6 = r6.toString()
            r1.put(r5, r6)
            java.lang.String r5 = "intent"
            android.content.Intent r6 = r14.c
            java.lang.String r6 = r6.toUri(r11)
            r1.put(r5, r6)
            java.lang.String r5 = "install"
            long r6 = r14.j
            java.lang.Long r6 = java.lang.Long.valueOf(r6)
            r1.put(r5, r6)
            java.lang.String r5 = "applications"
            r15.insert(r5, r12, r1)
            int r3 = r3 + 1
            goto L_0x0160
        L_0x01b9:
            return
        L_0x01ba:
            r2 = move-exception
            r3 = r5
            goto L_0x0149
        L_0x01bd:
            r2 = move-exception
            r3 = r11
            goto L_0x0094
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.launcher.u.e(android.database.sqlite.SQLiteDatabase):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public final int a(SQLiteDatabase sQLiteDatabase) {
        int i;
        int i2;
        boolean a2;
        Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
        intent.addCategory("android.intent.category.LAUNCHER");
        ContentValues contentValues = new ContentValues();
        PackageManager packageManager = this.a.getPackageManager();
        try {
            XmlResourceParser xml = this.a.getResources().getXml(R.xml.default_workspace);
            AttributeSet asAttributeSet = Xml.asAttributeSet(xml);
            com.tencent.util.a.a(xml, "favorites");
            int depth = xml.getDepth();
            int i3 = 0;
            while (true) {
                try {
                    int next = xml.next();
                    if ((next != 3 || xml.getDepth() > depth) && next != 1) {
                        if (next == 2) {
                            String name = xml.getName();
                            TypedArray obtainStyledAttributes = this.a.obtainStyledAttributes(asAttributeSet, com.tencent.a.a.g);
                            contentValues.clear();
                            String string = obtainStyledAttributes.getString(8);
                            if (string == null) {
                                contentValues.put("container", (Integer) -100);
                            } else {
                                contentValues.put("container", string);
                            }
                            contentValues.put("screen", obtainStyledAttributes.getString(2));
                            contentValues.put("cellX", obtainStyledAttributes.getString(3));
                            contentValues.put("cellY", obtainStyledAttributes.getString(4));
                            if ("favorite".equals(name)) {
                                a2 = a(sQLiteDatabase, contentValues, obtainStyledAttributes, packageManager, intent);
                            } else if ("search".equals(name) && b.a < 8) {
                                contentValues.put("itemType", (Integer) 1001);
                                contentValues.put("spanX", (Integer) 4);
                                contentValues.put("spanY", (Integer) 1);
                                sQLiteDatabase.insert("favorites", null, contentValues);
                                a2 = true;
                            } else if ("switcher".equals(name)) {
                                contentValues.put("itemType", (Integer) 2001);
                                contentValues.put("spanX", (Integer) 4);
                                contentValues.put("spanY", (Integer) 1);
                                sQLiteDatabase.insert("favorites", null, contentValues);
                                a2 = true;
                            } else if ("task_manager".equals(name)) {
                                contentValues.put("itemType", (Integer) 2002);
                                contentValues.put("spanX", (Integer) 4);
                                contentValues.put("spanY", (Integer) 1);
                                sQLiteDatabase.insert("favorites", null, contentValues);
                                a2 = true;
                            } else if ("person_center".equals(name)) {
                                contentValues.put("itemType", (Integer) 2003);
                                contentValues.put("spanX", (Integer) 4);
                                contentValues.put("spanY", (Integer) 2);
                                sQLiteDatabase.insert("favorites", null, contentValues);
                                a2 = true;
                            } else if (!"soso".equals(name) || !b.a()) {
                                a2 = "clock".equals(name) ? a(sQLiteDatabase, contentValues) : "shortcut".equals(name) ? a(sQLiteDatabase, contentValues, obtainStyledAttributes) : "qqwidget".equals(name) ? b(sQLiteDatabase, contentValues, obtainStyledAttributes) : false;
                            } else {
                                contentValues.put("itemType", (Integer) 2004);
                                contentValues.put("spanX", (Integer) 4);
                                contentValues.put("spanY", (Integer) 1);
                                sQLiteDatabase.insert("favorites", null, contentValues);
                                a2 = true;
                            }
                            if (a2) {
                                i3++;
                            }
                            obtainStyledAttributes.recycle();
                        }
                    }
                } catch (XmlPullParserException e) {
                    e = e;
                    i = i3;
                } catch (IOException e2) {
                    e = e2;
                    i2 = i3;
                    Log.w("LauncherProvider", "Got exception parsing favorites.", e);
                    return i2;
                }
            }
            return i3;
        } catch (XmlPullParserException e3) {
            e = e3;
            i = 0;
            Log.w("LauncherProvider", "Got exception parsing favorites.", e);
            return i;
        } catch (IOException e4) {
            e = e4;
            i2 = 0;
            Log.w("LauncherProvider", "Got exception parsing favorites.", e);
            return i2;
        }
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        System.currentTimeMillis();
        sQLiteDatabase.execSQL("CREATE TABLE favorites (_id INTEGER PRIMARY KEY,title TEXT,intent TEXT,container INTEGER,screen INTEGER,cellX INTEGER,cellY INTEGER,spanX INTEGER,spanY INTEGER,orderId INTEGER,itemType INTEGER,appWidgetId INTEGER NOT NULL DEFAULT -1,isShortcut INTEGER,iconType INTEGER,iconPackage TEXT,iconResource TEXT,icon BLOB,uri TEXT,displayMode INTEGER,packageName TEXT,className TEXT,layoutId INTEGER);");
        sQLiteDatabase.execSQL("CREATE TABLE applications (_id INTEGER PRIMARY KEY,title TEXT,intent TEXT,container INTEGER,position INTEGER,orderId INTEGER,itemType INTEGER,install INTEGER,callednum INTEGER);");
        sQLiteDatabase.execSQL("CREATE TABLE gestures (_id INTEGER PRIMARY KEY,title TEXT,intent TEXT,itemType INTEGER,iconType INTEGER,iconPackage TEXT,iconResource TEXT,icon BLOB);");
        sQLiteDatabase.execSQL("CREATE TABLE newAppsNotify (_id INTEGER PRIMARY KEY,packageName TEXT);");
        if (this.b != null) {
            this.b.deleteHost();
            this.a.getContentResolver().notifyChange(LauncherProvider.a, null);
        }
        if (!b(sQLiteDatabase)) {
            a(sQLiteDatabase);
        }
        e(sQLiteDatabase);
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        int i3;
        if (i < 3) {
            sQLiteDatabase.beginTransaction();
            try {
                sQLiteDatabase.execSQL("ALTER TABLE favorites ADD COLUMN appWidgetId INTEGER NOT NULL DEFAULT -1;");
                sQLiteDatabase.setTransactionSuccessful();
                sQLiteDatabase.endTransaction();
                i3 = 3;
            } catch (SQLException e) {
                Log.e("LauncherProvider", e.getMessage(), e);
                sQLiteDatabase.endTransaction();
                i3 = i;
            } catch (Throwable th) {
                sQLiteDatabase.endTransaction();
                throw th;
            }
            if (i3 == 3) {
                d(sQLiteDatabase);
            }
        } else {
            i3 = i;
        }
        if (i3 < 4) {
            i3 = 4;
        }
        if (i3 < 5 && c(sQLiteDatabase)) {
            i3 = 5;
        }
        if (i3 < 6) {
            sQLiteDatabase.beginTransaction();
            try {
                sQLiteDatabase.execSQL("CREATE TABLE applications (_id INTEGER PRIMARY KEY,title TEXT,intent TEXT,container INTEGER,position INTEGER,orderId INTEGER,itemType INTEGER,install INTEGER,callednum INTEGER);");
                e(sQLiteDatabase);
                sQLiteDatabase.setTransactionSuccessful();
                sQLiteDatabase.endTransaction();
                i3 = 6;
            } catch (SQLException e2) {
                Log.e("LauncherProvider", e2.getMessage(), e2);
                sQLiteDatabase.endTransaction();
            } catch (Throwable th2) {
                sQLiteDatabase.endTransaction();
                throw th2;
            }
        }
        if (i3 != 6) {
            Log.w("LauncherProvider", "Destroying all old data.");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS favorites");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS applications");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS gestures");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS newAppsNotify");
            onCreate(sQLiteDatabase);
        }
    }
}
