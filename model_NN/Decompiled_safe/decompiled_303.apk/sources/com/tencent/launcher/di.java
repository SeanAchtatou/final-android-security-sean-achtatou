package com.tencent.launcher;

import android.os.Handler;
import android.view.animation.Animation;

final class di implements Animation.AnimationListener {
    final /* synthetic */ UserFolder a;

    di(UserFolder userFolder) {
        this.a = userFolder;
    }

    public final void onAnimationEnd(Animation animation) {
        new Handler().postAtTime(new fh(this), 100);
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
