package com.tencent.launcher;

import android.view.View;
import android.view.animation.Animation;
import com.tencent.qqlauncher.R;

final class hi implements Animation.AnimationListener {
    private /* synthetic */ View a;
    private /* synthetic */ ThumbnailWorkspace b;

    hi(ThumbnailWorkspace thumbnailWorkspace, View view) {
        this.b = thumbnailWorkspace;
        this.a = view;
    }

    public final void onAnimationEnd(Animation animation) {
        int childCount = this.b.getChildCount();
        if (childCount - 1 == this.b.A) {
            this.b.removeViewAt(this.b.A);
            if (this.b.getChildCount() % this.b.w == 0 && this.b.getChildCount() != 0) {
                ThumbnailWorkspace.b(this.b, ThumbnailWorkspace.i(this.b) - 1);
            }
        }
        this.b.b(childCount - 1, this.b.A);
        if (this.a.findViewById(R.id.thumbnail_imageview).isSelected()) {
            this.b.b(0);
        }
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
