package com.tencent.launcher;

import android.view.View;
import com.tencent.qqlauncher.R;

final class j implements View.OnClickListener {
    private /* synthetic */ View a;
    private /* synthetic */ x b;

    j(x xVar, View view) {
        this.b = xVar;
        this.a = view;
    }

    public final void onClick(View view) {
        this.a.findViewById(R.id.button1).setEnabled(false);
        switch (this.b.a.mRenameFolderType) {
            case 0:
                x.a(this.b);
                break;
            case 1:
                this.b.c();
                break;
            case 2:
                this.b.b();
                break;
            case 3:
                this.b.a();
                break;
        }
        int unused = this.b.a.mRenameFolderType = -1;
    }
}
