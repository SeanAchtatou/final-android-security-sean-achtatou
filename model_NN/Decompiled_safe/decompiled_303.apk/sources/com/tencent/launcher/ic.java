package com.tencent.launcher;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

public final class ic extends BaseAdapter {
    private final LayoutInflater a;
    private ArrayList b;
    private Context c;
    private GridListView d;

    public ic(Context context, ArrayList arrayList) {
        this.a = LayoutInflater.from(context);
        this.c = context;
        this.b = arrayList;
    }

    public final void a(GridListView gridListView) {
        this.d = gridListView;
    }

    public final void a(am amVar) {
        this.b.remove(amVar);
    }

    public final int getCount() {
        return GridListView.a ? this.b.size() : Math.min(this.b.size() + 1, 12);
    }

    public final Object getItem(int i) {
        if (i >= this.b.size()) {
            return null;
        }
        return this.b.get(i);
    }

    public final long getItemId(int i) {
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.a.a(android.content.Context, android.graphics.drawable.Drawable, com.tencent.launcher.ha, boolean):android.graphics.drawable.Drawable
     arg types: [android.content.Context, android.graphics.drawable.Drawable, com.tencent.launcher.am, int]
     candidates:
      com.tencent.launcher.a.a(android.graphics.Bitmap, int, int, android.content.Context):android.graphics.Bitmap
      com.tencent.launcher.a.a(android.content.Context, android.graphics.drawable.Drawable, android.graphics.drawable.Drawable, com.tencent.launcher.ha):android.graphics.drawable.Drawable
      com.tencent.launcher.a.a(android.content.Context, android.graphics.drawable.Drawable, com.tencent.launcher.ha, boolean):android.graphics.drawable.Drawable */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        am amVar = (am) getItem(i);
        View inflate = view == null ? this.a.inflate((int) R.layout.application_folder, viewGroup, false) : view;
        if (this.d != null) {
            this.d.a(inflate, i);
        }
        if (amVar == null) {
            TextView textView = (TextView) inflate;
            textView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, this.c.getResources().getDrawable(R.drawable.folder_add_icon), (Drawable) null, (Drawable) null);
            textView.setText("");
            return inflate;
        }
        if (amVar.e) {
            Context context = this.c;
            if (amVar.e) {
                amVar.d = a.a(context, amVar.d, (ha) amVar, true);
                amVar.f = true;
                amVar.e = false;
            }
        } else if (!amVar.f) {
            amVar.d = a.a(this.c, amVar.d, amVar);
            amVar.f = true;
        }
        TextView textView2 = (TextView) inflate;
        textView2.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, amVar.d, (Drawable) null, (Drawable) null);
        textView2.setText(amVar.a);
        if (i == Folder.b) {
            textView2.setText("");
            textView2.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
        }
        inflate.setTag(new Integer(i));
        return inflate;
    }
}
