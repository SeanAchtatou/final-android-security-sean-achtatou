package com.tencent.launcher;

import android.view.animation.Animation;

final class fz implements Animation.AnimationListener {
    private /* synthetic */ GridListView a;

    fz(GridListView gridListView) {
        this.a = gridListView;
    }

    public final void onAnimationEnd(Animation animation) {
        GridListView.a(this.a);
        if (this.a.g == 0) {
            this.a.h.clear();
            int childCount = this.a.getChildCount();
            for (int i = 0; i < childCount; i++) {
                this.a.getChildAt(i).clearAnimation();
            }
            if (this.a.c != null) {
                this.a.c.a(this.a.d, this.a.e);
            }
            int unused = this.a.d = this.a.e;
            Folder.b = this.a.d;
            this.a.c().notifyDataSetChanged();
            boolean unused2 = this.a.f = false;
            if (!GridListView.a) {
                int unused3 = this.a.d = 99;
                int unused4 = this.a.e = 99;
            }
        }
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
