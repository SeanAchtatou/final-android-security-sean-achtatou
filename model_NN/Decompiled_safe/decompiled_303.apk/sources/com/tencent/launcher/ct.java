package com.tencent.launcher;

import android.app.SearchManager;

final class ct implements SearchManager.OnCancelListener {
    private /* synthetic */ SearchManager a;
    private /* synthetic */ Launcher b;

    ct(Launcher launcher, SearchManager searchManager) {
        this.b = launcher;
        this.a = searchManager;
    }

    public final void onCancel() {
        this.a.setOnCancelListener(null);
        this.b.stopSearch();
    }
}
