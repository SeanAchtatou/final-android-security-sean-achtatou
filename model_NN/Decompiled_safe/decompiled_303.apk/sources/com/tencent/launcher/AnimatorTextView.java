package com.tencent.launcher;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.DrawFilter;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;
import com.tencent.launcher.CellLayout;
import com.tencent.qqlauncher.R;
import com.tencent.util.o;
import java.util.Random;

public class AnimatorTextView extends TextView implements e {
    private static Drawable c;
    private static final Random d = new Random();
    private Context a;
    private Drawable b;
    private int e;
    private int f;
    private int g = 2;
    private int h;
    private int i;
    private int j;
    private DrawFilter k;
    private boolean l = true;
    private boolean m = false;
    private Bitmap n;
    private Paint o = new Paint();
    private Runnable p;
    private TranslateAnimation q;

    public AnimatorTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = context;
        this.k = new PaintFlagsDrawFilter(0, 3);
        if (c == null) {
            c = context.getResources().getDrawable(R.drawable.kill);
        }
        this.h = d.nextInt(6) - 3;
        this.e = d.nextInt(50);
        this.f = d.nextInt(50);
        this.g = d.nextBoolean() ? -2 : 2;
        this.p = new o(this);
        this.q = new TranslateAnimation(getContext(), null);
        this.q.setFillAfter(true);
        this.q.setInterpolator(new AccelerateDecelerateInterpolator());
    }

    public final void a(cm cmVar, boolean z) {
        removeCallbacks(this.p);
    }

    public final boolean a(int i2, Object obj) {
        return false;
    }

    public final boolean a(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
        return true;
    }

    public final boolean a(cm cmVar, Object obj) {
        return getLayoutParams() != null && ((CellLayout.LayoutParams) getLayoutParams()).c <= 1 && ((CellLayout.LayoutParams) getLayoutParams()).d <= 1;
    }

    public final void b(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
        if (fn.a && getVisibility() != 8) {
            postDelayed(this.p, 250);
        }
    }

    public final void c(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00e3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDraw(android.graphics.Canvas r13) {
        /*
            r12 = this;
            r11 = 255(0xff, float:3.57E-43)
            r10 = 100
            r9 = 0
            int r1 = r12.getCompoundPaddingLeft()
            int r0 = r12.getCompoundPaddingRight()
            int r2 = r12.getCompoundPaddingTop()
            int r3 = r12.getScrollX()
            int r4 = r12.getScrollY()
            int r5 = r12.getRight()
            int r6 = r12.getLeft()
            int r5 = r5 - r6
            int r0 = r5 - r0
            int r0 = r0 - r1
            int r3 = r3 + r1
            android.graphics.drawable.Drawable r5 = r12.b
            android.graphics.Rect r5 = r5.getBounds()
            int r5 = r5.right
            int r0 = r0 - r5
            int r0 = r0 / 2
            int r3 = r3 + r0
            java.lang.Object r0 = r12.getTag()
            com.tencent.launcher.am r0 = (com.tencent.launcher.am) r0
            r13.save()
            boolean r5 = com.tencent.launcher.fn.a
            if (r5 == 0) goto L_0x00e9
            android.graphics.DrawFilter r5 = r12.k
            r13.setDrawFilter(r5)
            int r5 = r12.h
            float r5 = (float) r5
            int r6 = r12.e
            float r6 = (float) r6
            int r7 = r12.f
            float r7 = (float) r7
            r13.rotate(r5, r6, r7)
            int r5 = r12.h
            r6 = 3
            if (r5 <= r6) goto L_0x00d9
            r5 = -2
            r12.g = r5
        L_0x0058:
            int r5 = r12.h
            int r6 = r12.g
            int r5 = r5 + r6
            r12.h = r5
            boolean r5 = r0.i
            if (r5 == 0) goto L_0x00e9
            r5 = r10
        L_0x0064:
            float r6 = (float) r3
            int r7 = r12.getPaddingTop()
            int r4 = r4 + r7
            float r4 = (float) r4
            r13.translate(r6, r4)
            android.graphics.Paint r4 = r12.o
            r4.setAlpha(r5)
            android.graphics.Bitmap r4 = r12.n
            android.graphics.Paint r5 = r12.o
            r13.drawBitmap(r4, r9, r9, r5)
            boolean r4 = com.tencent.launcher.fn.a
            if (r4 == 0) goto L_0x00c7
            boolean r4 = r12.m
            if (r4 != 0) goto L_0x00c7
            android.graphics.drawable.Drawable r4 = com.tencent.launcher.AnimatorTextView.c
            android.graphics.Rect r4 = r4.getBounds()
            boolean r4 = r4.isEmpty()
            if (r4 == 0) goto L_0x00b4
            android.graphics.drawable.Drawable r4 = com.tencent.launcher.AnimatorTextView.c
            android.content.res.Resources r5 = r12.getResources()
            android.util.DisplayMetrics r5 = r5.getDisplayMetrics()
            float r5 = r5.density
            r6 = -1063256064(0xffffffffc0a00000, float:-5.0)
            float r5 = r5 * r6
            int r5 = (int) r5
            int r6 = r4.getIntrinsicWidth()
            int r7 = r4.getIntrinsicHeight()
            int r8 = r12.getWidth()
            int r6 = r8 - r6
            int r8 = r12.getWidth()
            int r7 = r7 + r5
            r4.setBounds(r6, r5, r8, r7)
        L_0x00b4:
            int r3 = -r3
            float r3 = (float) r3
            r13.translate(r3, r9)
            boolean r0 = r0.i
            if (r0 == 0) goto L_0x00e3
            android.graphics.drawable.Drawable r0 = com.tencent.launcher.AnimatorTextView.c
            r0.setAlpha(r10)
        L_0x00c2:
            android.graphics.drawable.Drawable r0 = com.tencent.launcher.AnimatorTextView.c
            r0.draw(r13)
        L_0x00c7:
            r13.restore()
            float r0 = (float) r1
            r1 = 4
            int r1 = r2 - r1
            int r2 = r12.j
            int r1 = r1 + r2
            float r1 = (float) r1
            r13.translate(r0, r1)
            super.onDraw(r13)
            return
        L_0x00d9:
            int r5 = r12.h
            r6 = -3
            if (r5 >= r6) goto L_0x0058
            r5 = 2
            r12.g = r5
            goto L_0x0058
        L_0x00e3:
            android.graphics.drawable.Drawable r0 = com.tencent.launcher.AnimatorTextView.c
            r0.setAlpha(r11)
            goto L_0x00c2
        L_0x00e9:
            r5 = r11
            goto L_0x0064
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.launcher.AnimatorTextView.onDraw(android.graphics.Canvas):void");
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i2, int i3, int i4, int i5) {
        super.onScrollChanged(i2, i3, i4, i5);
        this.l = true;
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        this.l = true;
    }

    public void setCompoundDrawables(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (!((drawable == null && drawable2 == null && drawable3 == null && drawable4 == null) ? false : true)) {
            this.i = 0;
            this.j = 0;
            return;
        }
        this.b = drawable2;
        Rect bounds = this.b.getBounds();
        this.i = bounds.width();
        this.j = bounds.height();
        if (this.b instanceof BitmapDrawable) {
            this.n = ((BitmapDrawable) this.b).getBitmap();
        } else if (this.b instanceof hp) {
            this.n = ((hp) this.b).a();
        } else {
            this.n = o.a(this.b);
        }
    }

    public void setCompoundDrawablesWithIntrinsicBounds(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (drawable != null) {
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        }
        if (drawable3 != null) {
            drawable3.setBounds(0, 0, drawable3.getIntrinsicWidth(), drawable3.getIntrinsicHeight());
        }
        if (drawable2 != null) {
            drawable2.setBounds(0, 0, drawable2.getIntrinsicWidth(), drawable2.getIntrinsicHeight());
        }
        if (drawable4 != null) {
            drawable4.setBounds(0, 0, drawable4.getIntrinsicWidth(), drawable4.getIntrinsicHeight());
        }
        setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
    }

    public void setTag(Object obj) {
        super.setTag(obj);
    }
}
