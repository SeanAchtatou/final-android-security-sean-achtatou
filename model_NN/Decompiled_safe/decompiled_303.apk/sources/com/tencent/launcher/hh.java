package com.tencent.launcher;

import android.view.View;
import android.widget.Toast;
import com.tencent.qqlauncher.R;

final class hh implements View.OnClickListener {
    private /* synthetic */ ThumbnailWorkspace a;

    hh(ThumbnailWorkspace thumbnailWorkspace) {
        this.a = thumbnailWorkspace;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ThumbnailWorkspace.a(com.tencent.launcher.ThumbnailWorkspace, boolean):boolean
     arg types: [com.tencent.launcher.ThumbnailWorkspace, int]
     candidates:
      com.tencent.launcher.ThumbnailWorkspace.a(int, int):int
      com.tencent.launcher.ThumbnailWorkspace.a(com.tencent.launcher.ThumbnailWorkspace, int):int
      com.tencent.launcher.ThumbnailWorkspace.a(com.tencent.launcher.ThumbnailWorkspace, android.view.View):void
      com.tencent.launcher.ThumbnailWorkspace.a(int, boolean):void
      com.tencent.launcher.ThumbnailWorkspace.a(android.view.View, int):void
      com.tencent.launcher.ThumbnailWorkspace.a(com.tencent.launcher.ThumbnailWorkspace, boolean):boolean */
    public final void onClick(View view) {
        if (!this.a.e()) {
            boolean unused = this.a.v = true;
            if (this.a.getChildCount() > 9) {
                Toast.makeText(this.a.b, (int) R.string.notify_addscreen_failed, 0).show();
            } else {
                this.a.a((View) null);
                this.a.a(true);
            }
            boolean unused2 = this.a.v = false;
        }
    }
}
