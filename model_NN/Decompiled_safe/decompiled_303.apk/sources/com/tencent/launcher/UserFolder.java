package com.tencent.launcher;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.qqlauncher.R;

public class UserFolder extends Folder implements ae, e {
    private static int h = -1;
    public int a = 0;
    private int i = 0;
    private int j = 0;
    private int k = 0;
    private int l = 0;
    /* access modifiers changed from: private */
    public GridListView m;
    /* access modifiers changed from: private */
    public ViewGroup n;
    private View o;
    private View p;
    private View q;
    private View r;
    private View s;
    private boolean t = false;
    private int u = 0;

    public UserFolder(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    static UserFolder a(Context context) {
        return (UserFolder) LayoutInflater.from(context).inflate((int) R.layout.user_folder, (ViewGroup) null);
    }

    private static void a(Rect rect, View view) {
        View view2 = view;
        Rect rect2 = rect;
        while (true) {
            rect2.offset(view2.getLeft(), view2.getTop());
            View view3 = (View) view2.getParent();
            if (!(view3 instanceof DragLayer)) {
                view2 = view3;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        super.a();
        requestFocus();
    }

    public final void a(int i2, int i3) {
        Object remove = ((bq) this.f).b.remove(i2);
        ((bq) this.f).b.add(Math.min(i3, ((bq) this.f).b.size()), (am) remove);
    }

    public final void a(int i2, int i3, int i4, int i5, int i6) {
        this.a = i2;
        this.i = i3;
        this.j = i4;
        this.k = i5;
        this.l = i6;
    }

    public final void a(View view, boolean z) {
        clearAnimation();
        this.n.clearAnimation();
        if (!z || this.g == null) {
            if (this.f.j != null) {
                ((QGridLayout) this.f.j.getParent()).e();
            }
            FolderIcon folderIcon = this.f.i;
            if (folderIcon != null) {
                folderIcon.a();
                folderIcon.setVisibility(0);
            } else {
                DrawerTextView drawerTextView = this.f.j;
                if (drawerTextView != null) {
                    drawerTextView.f();
                    drawerTextView.setVisibility(0);
                }
            }
            this.d.getWorkspace().p();
            this.f.i = null;
            this.f.j = null;
            this.f.k = null;
            this.g = null;
            if (this.t) {
                this.t = false;
                return;
            }
            return;
        }
        if (view != this) {
            if (this.f.n != -300 || (view instanceof QGridLayout)) {
                this.d.closeUserFolderFast(this);
                ((ic) this.c.getAdapter()).a(this.g);
                if (((bq) this.f).b.size() <= 1 && this.f.j != null) {
                    this.f.j.getParent();
                }
            } else {
                DrawerTextView drawerTextView2 = this.f.j;
                if (drawerTextView2 != null) {
                    drawerTextView2.f();
                    return;
                }
                return;
            }
        } else if (this.m.d()) {
            new Handler().postDelayed(new dh(this), 250);
        } else {
            setVisibility(0);
            this.m.b();
            d();
        }
        new dl(this).start();
        if (this.f.n == -200) {
            DockView dockView = this.f.k;
            dockView.a(this.f);
            dockView.setVisibility(0);
        } else {
            FolderIcon folderIcon2 = this.f.i;
            if (folderIcon2 != null) {
                folderIcon2.a();
                folderIcon2.setVisibility(0);
            } else {
                DrawerTextView drawerTextView3 = this.f.j;
                if (drawerTextView3 != null) {
                    drawerTextView3.f();
                    drawerTextView3.setVisibility(0);
                }
            }
        }
        this.g = null;
    }

    public final void a(Launcher launcher) {
        int i2;
        this.u = 0;
        this.n.setDrawingCacheQuality(524288);
        int i3 = this.f.n != -300 ? 230 : 250;
        switch ((((bq) this.f).b.size() + 1) / 4) {
            case 0:
                i2 = i3;
                break;
            case 1:
                i2 = (int) (((double) i3) * 1.15d);
                break;
            case 2:
                i2 = (int) (((double) i3) * 1.3d);
                break;
            default:
                i2 = i3;
                break;
        }
        AccelerateInterpolator accelerateInterpolator = new AccelerateInterpolator();
        dn dnVar = new dn(this, launcher);
        float measuredWidth = ((float) (this.k + (this.i / 2))) / ((float) this.n.getMeasuredWidth());
        ScaleAnimation scaleAnimation = this.f.n != -200 ? new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, measuredWidth, 1, 0.0f) : new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, measuredWidth, 1, 1.0f);
        AnimationSet animationSet = new AnimationSet(false);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration((long) i2);
        alphaAnimation.setInterpolator(accelerateInterpolator);
        alphaAnimation.setFillAfter(true);
        scaleAnimation.setDuration((long) i2);
        scaleAnimation.setInterpolator(accelerateInterpolator);
        scaleAnimation.setFillAfter(true);
        animationSet.addAnimation(alphaAnimation);
        animationSet.addAnimation(scaleAnimation);
        animationSet.setFillAfter(true);
        if (this.a == 0) {
            animationSet.setAnimationListener(dnVar);
            this.n.startAnimation(animationSet);
        }
        launcher.shadeViewsReserve((bq) this.f, this);
        if (this.a != 0) {
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) (-this.a));
            translateAnimation.setInterpolator(accelerateInterpolator);
            translateAnimation.setDuration((long) i2);
            translateAnimation.setFillAfter(true);
            translateAnimation.setAnimationListener(dnVar);
            this.n.startAnimation(animationSet);
            startAnimation(translateAnimation);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(bq bqVar) {
        int i2;
        ScaleAnimation scaleAnimation;
        TranslateAnimation translateAnimation;
        super.a();
        requestFocus();
        b = 99;
        this.d.setUserFolderOpenAndCloseFocus(false);
        this.u = 0;
        switch (bqVar.b.size() / 4) {
            case 0:
                i2 = 200;
                break;
            case 1:
                i2 = 229;
                break;
            case 2:
                i2 = 260;
                break;
            case 3:
                i2 = 260;
                break;
            default:
                i2 = 200;
                break;
        }
        LinearInterpolator linearInterpolator = new LinearInterpolator();
        FolderIcon folderIcon = bqVar.i;
        DockView dockView = bqVar.k;
        DrawerTextView drawerTextView = bqVar.j;
        int i3 = this.l;
        int i4 = this.k;
        GridListView.a = false;
        this.n.setAnimationCacheEnabled(true);
        Bitmap createBitmap = Bitmap.createBitmap(this.i, this.j, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas();
        canvas.setBitmap(createBitmap);
        if (bqVar.n == -100) {
            boolean isDrawingCacheEnabled = folderIcon.isDrawingCacheEnabled();
            folderIcon.setPressed(false);
            folderIcon.destroyDrawingCache();
            folderIcon.setDrawingCacheEnabled(true);
            canvas.drawBitmap(folderIcon.getDrawingCache(), 0.0f, 0.0f, (Paint) null);
            folderIcon.setDrawingCacheEnabled(isDrawingCacheEnabled);
        } else if (bqVar.n != -300) {
            DockView dockView2 = dockView;
            canvas.save();
            canvas.translate((float) dockView2.getPaddingLeft(), (float) dockView2.getPaddingTop());
            canvas.concat(dockView2.getImageMatrix());
            dockView2.getDrawable().draw(canvas);
            canvas.restore();
        } else if (drawerTextView != null) {
            boolean isDrawingCacheEnabled2 = drawerTextView.isDrawingCacheEnabled();
            drawerTextView.setPressed(false);
            drawerTextView.destroyDrawingCache();
            drawerTextView.setDrawingCacheEnabled(true);
            canvas.drawBitmap(drawerTextView.getDrawingCache(), 0.0f, 0.0f, (Paint) null);
            drawerTextView.setDrawingCacheEnabled(isDrawingCacheEnabled2);
        }
        ImageView imageView = (ImageView) findViewById(R.id.folder_icon);
        imageView.setImageBitmap(createBitmap);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
        layoutParams.width = this.i;
        layoutParams.height = this.j;
        layoutParams.leftMargin = this.k;
        measure(View.MeasureSpec.makeMeasureSpec(((View) getParent()).getWidth(), Integer.MIN_VALUE), View.MeasureSpec.makeMeasureSpec(((View) getParent()).getHeight(), Integer.MIN_VALUE));
        requestLayout();
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.n.getLayoutParams();
        LinearLayout.LayoutParams layoutParams3 = (LinearLayout.LayoutParams) this.q.getLayoutParams();
        if (bqVar.n == -200) {
            layoutParams2.topMargin = i3 - this.n.getMeasuredHeight();
            layoutParams3.width = (((((this.i / 2) + i4) - this.o.getMeasuredWidth()) - (this.s.getMeasuredWidth() / 2)) - layoutParams2.leftMargin) - this.n.getPaddingLeft();
        } else {
            layoutParams2.topMargin = i3 + this.j + this.a;
            layoutParams3.width = (((((this.i / 2) + i4) - this.o.getMeasuredWidth()) - (this.s.getMeasuredWidth() / 2)) - layoutParams2.leftMargin) - this.n.getPaddingLeft();
        }
        View findViewById = findViewById(R.id.folder_body);
        float measuredWidth = (((float) i4) + (((float) this.i) / 2.0f)) / ((float) this.n.getMeasuredWidth());
        AnimationSet animationSet = new AnimationSet(false);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration((long) i2);
        alphaAnimation.setInterpolator(linearInterpolator);
        alphaAnimation.setFillAfter(true);
        if (this.a == 0) {
            layoutParams.topMargin = this.l;
            if (bqVar.n != -200) {
                TranslateAnimation translateAnimation2 = new TranslateAnimation(0.0f, 0.0f, (float) ((-findViewById.getMeasuredHeight()) + 20), 0.0f);
                scaleAnimation = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, measuredWidth, 1, 0.0f);
                translateAnimation = translateAnimation2;
            } else {
                TranslateAnimation translateAnimation3 = new TranslateAnimation(0.0f, 0.0f, (float) (findViewById.getMeasuredHeight() - 20), 0.0f);
                scaleAnimation = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, measuredWidth, 1, 1.0f);
                translateAnimation = translateAnimation3;
            }
            translateAnimation.setDuration((long) i2);
            translateAnimation.setInterpolator(linearInterpolator);
            scaleAnimation.setDuration((long) i2);
            scaleAnimation.setInterpolator(linearInterpolator);
            animationSet.addAnimation(alphaAnimation);
            animationSet.addAnimation(scaleAnimation);
            this.n.startAnimation(animationSet);
            return;
        }
        layoutParams.topMargin = this.l + this.a;
        TranslateAnimation translateAnimation4 = new TranslateAnimation(0.0f, 0.0f, (float) (-this.a), 0.0f);
        translateAnimation4.setDuration((long) i2);
        TranslateAnimation translateAnimation5 = new TranslateAnimation(0.0f, 0.0f, (float) ((-findViewById.getMeasuredHeight()) + 20), 0.0f);
        translateAnimation5.setDuration((long) i2);
        translateAnimation4.setInterpolator(linearInterpolator);
        translateAnimation5.setInterpolator(linearInterpolator);
        ((View) imageView.getParent()).startAnimation(translateAnimation4);
        ScaleAnimation scaleAnimation2 = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, measuredWidth, 1, 0.0f);
        scaleAnimation2.setDuration((long) i2);
        scaleAnimation2.setInterpolator(linearInterpolator);
        animationSet.addAnimation(alphaAnimation);
        animationSet.addAnimation(scaleAnimation2);
        this.n.startAnimation(animationSet);
    }

    public final void a(cm cmVar, boolean z) {
    }

    /* access modifiers changed from: package-private */
    public final void a(ex exVar) {
        removeAllViews();
        if (exVar.n == -200) {
            addView(LayoutInflater.from(getContext()).inflate((int) R.layout.user_folder_bottom, (ViewGroup) null), -1, -1);
        } else {
            addView(LayoutInflater.from(getContext()).inflate((int) R.layout.user_folder_up, (ViewGroup) null), -1, -1);
        }
        this.c = (AbsListView) findViewById(R.id.folder_grid);
        if (this.c != null) {
            this.c.setOnItemClickListener(this);
            this.c.setOnItemLongClickListener(this);
        }
        this.e = (TextView) findViewById(R.id.folder_name);
        View findViewById = findViewById(R.id.folder_name_edit);
        if (findViewById != null) {
            findViewById.setOnClickListener(this);
        }
        View findViewById2 = findViewById(R.id.folder_edit);
        if (findViewById2 != null) {
            findViewById2.setOnClickListener(this);
        }
        this.n = (ViewGroup) findViewById(R.id.folder_content);
        this.o = findViewById(R.id.folder_left);
        this.p = findViewById(R.id.folder_right);
        this.q = findViewById(R.id.folder_left_line);
        this.r = findViewById(R.id.folder_right_line);
        this.s = findViewById(R.id.folder_arrow);
        super.a(exVar);
        ic icVar = new ic(getContext(), ((bq) exVar).b);
        a(icVar);
        if (this.c instanceof GridListView) {
            this.m = (GridListView) this.c;
            this.m.a(this);
        }
        icVar.a(this.m);
        setOnClickListener(new dm(this));
    }

    public final boolean a(int i2, Object obj) {
        return true;
    }

    public final boolean a(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
        if (cmVar == this) {
            if (this.m.d()) {
                new Handler().postDelayed(new dj(this), 250);
            } else {
                b = 99;
                return true;
            }
        }
        return true;
    }

    public final boolean a(cm cmVar, Object obj) {
        if (obj == null) {
            return false;
        }
        int i2 = ((ha) obj).m;
        return i2 == 0 || i2 == 1;
    }

    public final int b(int i2, int i3) {
        measure(View.MeasureSpec.makeMeasureSpec(i2, Integer.MIN_VALUE), View.MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE));
        requestLayout();
        return this.n.getMeasuredHeight();
    }

    public final void b() {
        if (this.f != null) {
            if (this.f.i != null) {
                this.f.i.setVisibility(4);
            }
            if (this.f.k != null) {
                this.f.k.setVisibility(4);
            }
            if (this.f.j != null) {
                this.f.j.setVisibility(4);
            }
        }
    }

    public final void b(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
    }

    public final void c() {
        if (this.f != null) {
            if (this.f.i != null) {
                this.f.i.setVisibility(0);
            }
            if (this.f.k != null) {
                this.f.k.setVisibility(0);
            }
            if (this.f.j != null) {
                this.f.j.setVisibility(0);
            }
        }
    }

    public final void c(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
        int i6;
        Rect rect = new Rect();
        this.n.getHitRect(rect);
        if (!rect.contains(i2, i3)) {
            setVisibility(8);
            b = 99;
            this.d.setUserFolderOpenAndCloseFocus(true);
            switch ((((bq) this.f).b.size() + 1) / 4) {
                case 0:
                    i6 = 250;
                    break;
                case 1:
                    i6 = 300;
                    break;
                case 2:
                    i6 = 375;
                    break;
                default:
                    i6 = 250;
                    break;
            }
            AccelerateDecelerateInterpolator accelerateDecelerateInterpolator = new AccelerateDecelerateInterpolator();
            di diVar = new di(this);
            float measuredWidth = ((float) (this.k + (this.i / 2))) / ((float) this.n.getMeasuredWidth());
            ScaleAnimation scaleAnimation = this.f.n != -200 ? new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, measuredWidth, 1, 0.0f) : new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, measuredWidth, 1, 1.0f);
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) (-this.a));
            translateAnimation.setInterpolator(accelerateDecelerateInterpolator);
            translateAnimation.setDuration((long) i6);
            translateAnimation.setFillAfter(true);
            translateAnimation.setAnimationListener(diVar);
            AnimationSet animationSet = new AnimationSet(false);
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration((long) i6);
            alphaAnimation.setInterpolator(accelerateDecelerateInterpolator);
            alphaAnimation.setFillAfter(true);
            scaleAnimation.setDuration((long) i6);
            scaleAnimation.setInterpolator(accelerateDecelerateInterpolator);
            scaleAnimation.setFillAfter(true);
            animationSet.addAnimation(alphaAnimation);
            animationSet.addAnimation(scaleAnimation);
            animationSet.setFillAfter(true);
            this.n.startAnimation(animationSet);
            startAnimation(translateAnimation);
            this.d.shadeViewsReserve((bq) this.f, this);
            this.f.a(this);
        } else if (this.c instanceof GridListView) {
            GridListView gridListView = (GridListView) this.c;
            Rect rect2 = new Rect(0, 0, gridListView.getWidth(), gridListView.getHeight());
            a(rect2, gridListView);
            if (GridListView.a()) {
                gridListView.a(i2 - rect2.left, i3 - rect2.top);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.u++;
        super.onDraw(canvas);
    }
}
