package com.tencent.launcher;

import android.content.SharedPreferences;
import com.tencent.launcher.home.i;

final class du implements SharedPreferences.OnSharedPreferenceChangeListener {
    private /* synthetic */ Launcher a;

    /* synthetic */ du(Launcher launcher) {
        this(launcher, (byte) 0);
    }

    private du(Launcher launcher, byte b) {
        this.a = launcher;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.home.i.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.launcher.home.i.b(java.lang.String, int):int
      com.tencent.launcher.home.i.b(java.lang.String, java.lang.String):java.lang.String
      com.tencent.launcher.home.i.b(java.lang.String, boolean):boolean */
    public final void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        if (!this.a.mNeedRestart) {
            i unused = this.a.mMSFConfig;
            if (i.a(str)) {
                this.a.requestRestartHome();
                return;
            }
            if ("setting_switchdesktop_specialeffects".equals(str)) {
                this.a.mWorkspace.e(Integer.parseInt(this.a.mMSFConfig.b("setting_switchdesktop_specialeffects", "0")));
            }
            if ("setting_switchdesktop_wallpaperscroll".equals(str)) {
                this.a.mWorkspace.a(this.a.mMSFConfig.b("setting_switchdesktop_wallpaperscroll", true));
            }
            if ("setting_switchdesktop_screenrecycleswitch".equals(str)) {
                this.a.mWorkspace.c(this.a.mMSFConfig.b("setting_switchdesktop_screenrecycleswitch", false));
            }
            if ("support_land_port_switch".equals(str)) {
                if (this.a.mMSFConfig.b("support_land_port_switch", false)) {
                    this.a.setRequestedOrientation(4);
                } else {
                    this.a.setRequestedOrientation(1);
                }
            }
            if ("setting_normal_functionlist".equals(str)) {
                this.a.resetDrawerMode();
            }
        }
    }
}
