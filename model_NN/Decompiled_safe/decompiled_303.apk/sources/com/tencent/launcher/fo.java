package com.tencent.launcher;

import android.graphics.drawable.AnimationDrawable;
import java.util.ArrayList;
import java.util.Iterator;

final class fo implements Runnable {
    public ArrayList a;
    private /* synthetic */ SliderView b;

    fo(SliderView sliderView) {
        this.b = sliderView;
    }

    public final void run() {
        Iterator it = this.a.iterator();
        while (it.hasNext()) {
            ((AnimationDrawable) it.next()).start();
        }
    }
}
