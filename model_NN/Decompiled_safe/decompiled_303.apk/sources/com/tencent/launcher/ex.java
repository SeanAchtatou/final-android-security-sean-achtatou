package com.tencent.launcher;

import android.graphics.drawable.Drawable;

class ex extends ha {
    boolean g;
    CharSequence h;
    FolderIcon i;
    DrawerTextView j;
    DockView k;

    ex() {
    }

    public void a() {
        if (this.i != null) {
            Drawable i2 = this.i.i();
            if (i2 != null) {
                i2.setCallback(null);
            }
            Drawable j2 = this.i.j();
            if (j2 != null) {
                j2.setCallback(null);
            }
        }
    }

    public void a(UserFolder userFolder) {
        if (this.n == -100 && this.i != null) {
            this.i.a(userFolder);
        } else if (this.n == -300 && this.j != null && (this.j.getTag() instanceof bq)) {
            this.j.a(userFolder);
        } else if (this.n == -200 && this.k != null) {
            this.k.a((bq) this);
        }
    }
}
