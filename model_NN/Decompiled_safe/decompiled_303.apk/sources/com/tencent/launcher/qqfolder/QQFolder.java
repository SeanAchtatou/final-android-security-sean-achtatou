package com.tencent.launcher.qqfolder;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import com.tencent.launcher.cm;
import com.tencent.launcher.e;

public class QQFolder extends RelativeLayout implements cm, e {
    public QQFolder(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public QQFolder(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public final void a(View view, boolean z) {
    }

    public final void a(cm cmVar, boolean z) {
    }

    public final boolean a(int i, Object obj) {
        return false;
    }

    public final boolean a(cm cmVar, int i, int i2, int i3, int i4, Object obj) {
        return false;
    }

    public final boolean a(cm cmVar, Object obj) {
        return false;
    }

    public final void b(cm cmVar, int i, int i2, int i3, int i4, Object obj) {
    }

    public final void c(cm cmVar, int i, int i2, int i3, int i4, Object obj) {
    }
}
