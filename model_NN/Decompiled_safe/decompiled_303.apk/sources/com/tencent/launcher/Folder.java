package com.tencent.launcher;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.qqlauncher.R;

public class Folder extends LinearLayout implements View.OnClickListener, View.OnLongClickListener, AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener, cm {
    public static int b = 99;
    private dt a;
    protected AbsListView c;
    protected Launcher d;
    protected TextView e;
    protected ex f;
    protected am g;
    private Button h;
    private View i;
    private long j;
    private boolean k;

    public Folder(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setAlwaysDrawnWithCacheEnabled(false);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.c.requestLayout();
    }

    public void a(View view, boolean z) {
    }

    /* access modifiers changed from: package-private */
    public final void a(BaseAdapter baseAdapter) {
        this.c.setAdapter((Adapter) baseAdapter);
    }

    public final void a(dt dtVar) {
        this.a = dtVar;
    }

    /* access modifiers changed from: package-private */
    public void a(ex exVar) {
        this.f = exVar;
        if (this.e != null) {
            this.e.setText(exVar.h);
        }
        if (this.h != null) {
            this.h.setText(exVar.h);
        }
    }

    /* access modifiers changed from: package-private */
    public void a_() {
        if (this.f.n != -300) {
            Workspace workspace = this.d.getWorkspace();
            workspace.getChildAt(workspace.d()).requestFocus();
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(Launcher launcher) {
        this.d = launcher;
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        ((BaseAdapter) this.c.getAdapter()).notifyDataSetChanged();
    }

    /* access modifiers changed from: package-private */
    public final ex e() {
        return this.f;
    }

    public void onClick(View view) {
        if (this instanceof UserFolder) {
            this.d.closeFolder(this);
            this.d.showRenameDialog(this.f, 0);
            return;
        }
        this.d.closeFolder(this);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.c = (AbsListView) findViewById(R.id.folder_grid);
        if (this.c != null) {
            this.c.setOnItemClickListener(this);
            this.c.setOnItemLongClickListener(this);
        } else {
            this.c = (AbsListView) findViewById(R.id.folder_content);
            if (this.c != null) {
                this.c.setOnItemClickListener(this);
            }
        }
        this.h = (Button) findViewById(R.id.folder_close);
        if (this.h != null) {
            this.h.setOnClickListener(this);
            this.h.setOnLongClickListener(this);
        }
        this.e = (TextView) findViewById(R.id.folder_name);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        Rect rect = new Rect();
        View findViewById = findViewById(R.id.folder_content);
        if (findViewById != null) {
            findViewById.getHitRect(rect);
        }
        switch (motionEvent.getAction()) {
            case 0:
                this.j = System.currentTimeMillis();
                if (!rect.contains((int) motionEvent.getX(), (int) motionEvent.getY())) {
                    this.k = true;
                    break;
                } else {
                    this.k = false;
                    return false;
                }
            case 1:
                if (this.k && System.currentTimeMillis() - this.j <= 1000 && !rect.contains((int) motionEvent.getX(), (int) motionEvent.getY())) {
                    this.d.closeFolder(this);
                    return true;
                }
            case 2:
                if (rect.contains((int) motionEvent.getX(), (int) motionEvent.getY())) {
                    this.k = false;
                    break;
                }
                break;
        }
        return super.onInterceptTouchEvent(motionEvent);
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        am amVar = (am) adapterView.getItemAtPosition(i2);
        if (amVar == null) {
            if (this.f.i != null) {
                this.d.showAddAppDialog(((bq) this.f).b, this.f.i, -100, (bq) this.f);
            } else if (this.f.j != null) {
                this.d.showAddAppDialog(((bq) this.f).b, this.f.j, -300, (bq) this.f);
            } else if (this.f.k != null) {
                this.d.showAddAppDialog(((bq) this.f).b, this.f.k, -200, (bq) this.f);
            }
            this.d.closeFolder(this.d.getOpenFolder());
            return;
        }
        Intent intent = amVar.c;
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        try {
            Intent.class.getDeclaredMethod("setSourceBounds", Rect.class).invoke(intent, new Rect(iArr[0], iArr[1], iArr[0] + view.getWidth(), iArr[1] + view.getHeight()));
        } catch (Exception e2) {
        }
        this.d.startActivitySafely(amVar.c);
    }

    public boolean onItemLongClick(AdapterView adapterView, View view, int i2, long j2) {
        if (!view.isInTouchMode()) {
            return false;
        }
        am amVar = (am) adapterView.getItemAtPosition(i2);
        if (amVar == null) {
            return true;
        }
        b = i2;
        this.i = view;
        if (!(this instanceof UserFolder)) {
            this.a.a(view, this, amVar, 1);
            this.d.closeFolder(this);
        } else {
            this.a.a(view, this, amVar, 0);
        }
        this.g = amVar;
        if (this.c instanceof GridListView) {
            ((GridListView) this.c).a(i2);
            view.setVisibility(0);
            ((ic) this.c.getAdapter()).notifyDataSetChanged();
        }
        FolderIcon folderIcon = this.f.i;
        if (folderIcon == null) {
            DrawerTextView drawerTextView = this.f.j;
            if (drawerTextView != null) {
                drawerTextView.f();
            }
        } else {
            folderIcon.a();
        }
        return true;
    }

    public boolean onLongClick(View view) {
        this.d.closeFolder(this);
        this.d.showRenameDialog(this.f, 0);
        return true;
    }
}
