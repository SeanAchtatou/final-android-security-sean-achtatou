package com.tencent.launcher;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

public final class he {
    public final CharSequence a;
    public final Drawable b;
    private /* synthetic */ dg c;

    public he(dg dgVar, Resources resources, int i, int i2) {
        this.c = dgVar;
        this.a = resources.getString(i);
        if (i2 != -1) {
            this.b = resources.getDrawable(i2);
        } else {
            this.b = null;
        }
    }
}
