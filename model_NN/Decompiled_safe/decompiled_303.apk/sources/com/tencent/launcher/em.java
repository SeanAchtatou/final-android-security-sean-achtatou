package com.tencent.launcher;

import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

final class em implements DialogInterface.OnClickListener {
    private /* synthetic */ ArrayList a;
    private /* synthetic */ Context b;

    em(ArrayList arrayList, Context context) {
        this.a = arrayList;
        this.b = context;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (i < this.a.size()) {
            bc bcVar = (bc) this.a.get(i);
            dialogInterface.dismiss();
            if (bcVar.d == 0) {
                Toast.makeText(this.b, (int) R.string.import_no_data, 0).show();
            } else {
                Launcher.showSecondSureDialogForActivity(bcVar.b, this.b, true);
            }
        }
    }
}
