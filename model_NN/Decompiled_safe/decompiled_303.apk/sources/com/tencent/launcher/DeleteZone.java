package com.tencent.launcher;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.RectF;
import android.graphics.drawable.TransitionDrawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.Toast;
import com.tencent.a.a;
import com.tencent.module.qqwidget.h;
import com.tencent.module.setting.e;
import com.tencent.qqlauncher.R;

public class DeleteZone extends ImageView implements e, hs {
    boolean a;
    private final int[] b;
    /* access modifiers changed from: private */
    public Launcher c;
    private boolean d;
    private AnimationSet e;
    private AnimationSet f;
    private int g;
    private DragLayer h;
    private final RectF i;
    private TransitionDrawable j;
    /* access modifiers changed from: private */
    public boolean k;
    private boolean l;
    /* access modifiers changed from: private */
    public boolean m;
    private String n;
    private ha o;
    private String p;
    private cm q;
    /* access modifiers changed from: private */
    public boolean r;
    private boolean s;
    private final Runnable t;

    public DeleteZone(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public DeleteZone(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.b = new int[2];
        this.i = new RectF();
        this.a = false;
        this.k = false;
        this.l = false;
        this.m = false;
        this.n = null;
        this.r = false;
        this.t = new ft(this);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.d, i2, 0);
        this.g = obtainStyledAttributes.getInt(0, 1);
        obtainStyledAttributes.recycle();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.bq):void
     arg types: [com.tencent.launcher.Launcher, com.tencent.launcher.bq]
     candidates:
      com.tencent.launcher.ff.a(java.lang.String, boolean):char
      com.tencent.launcher.ff.a(android.content.Context, long):com.tencent.launcher.am
      com.tencent.launcher.ff.a(android.content.pm.PackageManager, android.content.Intent):com.tencent.launcher.am
      com.tencent.launcher.ff.a(java.util.HashMap, long):com.tencent.launcher.bq
      com.tencent.launcher.ff.a(com.tencent.launcher.ff, java.util.ArrayList):java.util.ArrayList
      com.tencent.launcher.ff.a(com.tencent.launcher.ff, java.util.HashMap):java.util.HashMap
      com.tencent.launcher.ff.a(android.content.pm.PackageManager, java.lang.String):java.util.List
      com.tencent.launcher.ff.a(android.content.ContentResolver, android.content.pm.PackageManager):void
      com.tencent.launcher.ff.a(android.content.Context, int):void
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha):void
      com.tencent.launcher.ff.a(com.tencent.launcher.bq, com.tencent.launcher.ha):void
      com.tencent.launcher.ff.a(com.tencent.launcher.ff, com.tencent.launcher.Launcher):void
      com.tencent.launcher.ff.a(android.content.Context, java.lang.String):boolean
      com.tencent.launcher.ff.a(java.util.HashMap, android.content.ComponentName):boolean
      com.tencent.launcher.ff.a(java.util.List, android.content.ComponentName):boolean
      com.tencent.launcher.ff.a(android.content.pm.PackageManager, com.tencent.launcher.am):android.graphics.drawable.Drawable
      com.tencent.launcher.ff.a(com.tencent.launcher.Launcher, android.content.ComponentName):void
      com.tencent.launcher.ff.a(com.tencent.launcher.Launcher, java.lang.String):void
      com.tencent.launcher.ff.a(com.tencent.launcher.Launcher, boolean):boolean
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.bq):void */
    /* access modifiers changed from: private */
    public void a(cm cmVar, ha haVar) {
        if (haVar instanceof am) {
            this.p = ((am) haVar).a == null ? "" : ((am) haVar).a.toString();
            this.o = haVar;
            this.q = cmVar;
        }
        ff model = Launcher.getModel();
        if (haVar.n == -100) {
            if (haVar instanceof c) {
                model.b((c) haVar);
            } else if (haVar instanceof com.tencent.module.qqwidget.a) {
                model.b((com.tencent.module.qqwidget.a) haVar);
            } else {
                model.b(haVar);
            }
        } else if (cmVar instanceof UserFolder) {
            ff.a((bq) ((UserFolder) cmVar).e(), haVar);
        }
        if (haVar instanceof bq) {
            bq bqVar = (bq) haVar;
            ff.a((Context) this.c, bqVar);
            model.b(bqVar);
        } else if (haVar instanceof c) {
            c cVar = (c) haVar;
            cv appWidgetHost = this.c.getAppWidgetHost();
            if (appWidgetHost != null) {
                appWidgetHost.deleteAppWidgetId(cVar.a);
            }
        } else if (haVar instanceof com.tencent.module.qqwidget.a) {
            h.a().b((com.tencent.module.qqwidget.a) haVar);
        }
        ff.g(this.c, haVar);
    }

    private static boolean a(Context context, am amVar) {
        ApplicationInfo applicationInfo;
        if (amVar.c.getComponent() == null) {
            return true;
        }
        try {
            applicationInfo = context.getPackageManager().getApplicationInfo(amVar.c.getComponent().getPackageName(), 8192);
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            applicationInfo = null;
        }
        return (applicationInfo.flags & 1) != 0;
    }

    private void b(cm cmVar, ha haVar) {
        new e(this.c).a((int) R.string.userfolder_delete_folder).a().a((int) R.string.thumbnail_close_ok, new fu(this, cmVar, haVar)).b((int) R.string.thumbnail_close_canel, new fw(this)).c().show();
    }

    public final void a() {
        this.k = false;
        this.m = false;
        if (this.d) {
            this.d = false;
            this.h.a((RectF) null);
            this.f.setAnimationListener(new fv(this));
            View view = (View) getParent();
            view.startAnimation(this.f);
            view.setVisibility(8);
        }
    }

    public final void a(View view, cm cmVar, Object obj, int i2) {
        ha haVar = (ha) obj;
        if (haVar != null) {
            this.c.requestFullScreen(true);
            this.d = true;
            if (this.e == null) {
                this.e = new l();
                AnimationSet animationSet = this.e;
                animationSet.setInterpolator(new AccelerateInterpolator());
                animationSet.addAnimation(new AlphaAnimation(0.0f, 1.0f));
                if (this.g == 1) {
                    animationSet.addAnimation(new TranslateAnimation(0, 0.0f, 0, 0.0f, 1, -1.0f, 1, 0.0f));
                } else {
                    animationSet.addAnimation(new TranslateAnimation(1, 1.0f, 1, 0.0f, 0, 0.0f, 0, 0.0f));
                }
                animationSet.setDuration(300);
            }
            if (this.f == null) {
                this.f = new l();
                AnimationSet animationSet2 = this.f;
                animationSet2.setInterpolator(new AccelerateInterpolator());
                animationSet2.addAnimation(new AlphaAnimation(1.0f, 0.0f));
                if (this.g == 1) {
                    animationSet2.addAnimation(new cu(0, 0, 0.0f, 1, 1, -1.0f));
                } else {
                    animationSet2.addAnimation(new cu(1, 1, 1.0f, 0, 0, 0.0f));
                }
                animationSet2.setDuration(300);
            }
            int[] iArr = this.b;
            getLocationOnScreen(iArr);
            this.i.set((float) iArr[0], (float) (iArr[1] - this.c.notiHeight), (float) ((iArr[0] + getRight()) - getLeft()), (float) (((iArr[1] + getBottom()) - getTop()) - this.c.notiHeight));
            this.h.a(this.i);
            if (this.j == null) {
                this.j = (TransitionDrawable) ((View) getParent()).getBackground();
            }
            this.j.resetTransition();
            View view2 = (View) getParent();
            view2.startAnimation(this.e);
            this.a = false;
            view2.setVisibility(0);
            this.n = null;
            if (haVar instanceof am) {
                this.r = true;
                try {
                    am amVar = (am) haVar;
                    this.m = a(this.c, amVar);
                    if (amVar.h != null) {
                        this.n = amVar.h.packageName;
                    } else {
                        this.n = getContext().getPackageManager().resolveActivity(amVar.c, 0).activityInfo.packageName;
                    }
                } catch (Exception e2) {
                    this.n = null;
                }
            } else {
                this.r = false;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(DragLayer dragLayer) {
        this.h = dragLayer;
    }

    /* access modifiers changed from: package-private */
    public final void a(Launcher launcher) {
        this.c = launcher;
    }

    public final void a(cm cmVar, boolean z) {
        if (z) {
            this.k = false;
        }
        this.j.reverseTransition(250);
        removeCallbacks(this.t);
    }

    public final boolean a(int i2, Object obj) {
        return true;
    }

    public final boolean a(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
        if (this.s && ((((cmVar instanceof QGridLayout) || (cmVar instanceof AllAppsListView)) && ((ha) obj).m == 0) || ((cmVar instanceof UserFolder) && ((UserFolder) cmVar).f.n == -300))) {
            ha haVar = (ha) obj;
            boolean z = false;
            if (haVar instanceof am) {
                try {
                    am amVar = (am) haVar;
                    boolean a2 = a(this.c, amVar);
                    String str = amVar.h != null ? amVar.h.packageName : getContext().getPackageManager().resolveActivity(amVar.c, 0).activityInfo.packageName;
                    if (!a2 && str != null) {
                        z = true;
                        getContext().startActivity(new Intent("android.intent.action.DELETE", Uri.parse("package:" + str)));
                    }
                } catch (Exception e2) {
                }
            }
            if (!z) {
                Toast.makeText(getContext(), (int) R.string.delete_application_failed, 0).show();
            }
            return true;
        } else if (this.s && (cmVar instanceof QGridLayout) && ((ha) obj).m == 2) {
            bq bqVar = (bq) obj;
            if (bqVar.b != null) {
                if (bqVar.b.size() > 0) {
                    b(cmVar, bqVar);
                } else {
                    this.a = true;
                    return false;
                }
            }
            return true;
        } else if (!this.k || this.n == null) {
            ha haVar2 = (ha) obj;
            if (haVar2.n == -1) {
                return true;
            }
            if (!(cmVar instanceof UserFolder) || ((UserFolder) cmVar).f.j == null) {
                if (haVar2 instanceof bq) {
                    bq bqVar2 = (bq) haVar2;
                    if (bqVar2.n == -300) {
                        return true;
                    }
                    if (bqVar2.b != null && bqVar2.b.size() > 0) {
                        b(cmVar, haVar2);
                        return true;
                    }
                }
                a(cmVar, haVar2);
                this.a = true;
                if (cmVar instanceof Workspace) {
                    ((Workspace) cmVar).a(this.p, this.o);
                }
                return true;
            }
            this.a = false;
            haVar2.v = false;
            cmVar.a(this, false);
            Toast.makeText(getContext(), (int) R.string.delete_application_failed, 0).show();
            return true;
        } else {
            if (!this.m) {
                getContext().startActivity(new Intent("android.intent.action.DELETE", Uri.parse("package:" + this.n)));
            }
            ((ha) obj).v = false;
            return true;
        }
    }

    public final boolean a(cm cmVar, Object obj) {
        return true;
    }

    public final void b(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
        this.j.reverseTransition(250);
        this.s = this.c.isDrawerOpen();
        if (!this.s || (!(cmVar instanceof QGridLayout) && !(cmVar instanceof AllAppsListView) && (!(cmVar instanceof UserFolder) || ((UserFolder) cmVar).f.n != -300))) {
            removeCallbacks(this.t);
            postDelayed(this.t, 1000);
            return;
        }
        this.k = true;
        post(this.t);
    }

    public final void c(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
    }
}
