package com.tencent.launcher;

import android.content.DialogInterface;

final class hn implements DialogInterface.OnCancelListener {
    private /* synthetic */ ThumbnailWorkspace a;

    hn(ThumbnailWorkspace thumbnailWorkspace) {
        this.a = thumbnailWorkspace;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        boolean unused = this.a.t = false;
        boolean unused2 = this.a.s = false;
    }
}
