package com.tencent.launcher;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import com.tencent.a.a;

public class DrawerBar extends LinearLayout implements View.OnClickListener {
    public boolean a = false;
    private final int b = 15;
    private int c;
    private int d;
    private View e;
    private int f = 1;
    /* access modifiers changed from: private */
    public int g = 0;
    private boolean h = true;
    private GestureDetector i;
    private gf j;

    public DrawerBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.f);
        this.c = obtainStyledAttributes.getResourceId(0, 0);
        this.d = obtainStyledAttributes.getResourceId(1, 0);
        if (this.d == 0) {
            new IllegalArgumentException(obtainStyledAttributes.getPositionDescription() + ": The content attribute is required and must refer to a valid child.");
        }
        this.g = obtainStyledAttributes.getInt(2, this.g);
        obtainStyledAttributes.recycle();
        this.j = new gf(this);
        this.i = new GestureDetector(context, this.j);
    }

    public final void a() {
        int height;
        int i2 = 0;
        this.f = 1;
        switch (this.g) {
            case 0:
                height = -getHeight();
                break;
            case 1:
                height = getHeight();
                break;
            case 2:
                i2 = -getWidth();
                height = 0;
                break;
            case 3:
                i2 = getWidth();
                height = 0;
                break;
            default:
                height = 0;
                break;
        }
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, (float) i2, 0.0f, (float) height);
        translateAnimation.setDuration(250);
        translateAnimation.setAnimationListener(new fk(this));
        startAnimation(translateAnimation);
    }

    public void onClick(View view) {
        if (view == this.e) {
            a();
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        if (this.c != 0) {
            this.e = findViewById(this.c);
            if (this.e != null) {
                this.e.setOnClickListener(this);
            }
        }
        setVisibility(0);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.i.onTouchEvent(motionEvent)) {
            return true;
        }
        if (!this.a) {
            return super.onInterceptTouchEvent(motionEvent);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        if (this.h) {
            setVisibility(8);
            this.h = false;
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.i.onTouchEvent(motionEvent)) {
            return true;
        }
        if (!this.a) {
            return super.onTouchEvent(motionEvent);
        }
        return false;
    }
}
