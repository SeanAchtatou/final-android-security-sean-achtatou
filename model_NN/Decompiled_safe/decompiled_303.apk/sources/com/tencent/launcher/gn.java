package com.tencent.launcher;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.tencent.module.qqwidget.a;
import com.tencent.qqlauncher.R;
import com.tencent.util.f;
import java.util.ArrayList;

public final class gn extends BaseAdapter {
    private ArrayList a = new ArrayList();
    private Context b;
    private final LayoutInflater c;
    private Resources d;

    public gn(ArrayList arrayList, Context context) {
        this.b = context;
        this.d = context.getResources();
        this.c = (LayoutInflater) this.b.getSystemService("layout_inflater");
        this.a = arrayList;
    }

    public final int getCount() {
        return this.a.size();
    }

    public final Object getItem(int i) {
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        Drawable drawable;
        a aVar = (a) getItem(i);
        View inflate = view == null ? this.c.inflate((int) R.layout.add_list_item, viewGroup, false) : view;
        TextView textView = (TextView) inflate;
        textView.setTag(aVar);
        textView.setText(aVar.b);
        if (aVar.c == 0) {
            drawable = this.d.getDrawable(R.drawable.ic_launcher_appwidget);
        } else {
            try {
                drawable = new f(this.b, aVar.e).b(aVar.c);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                drawable = null;
            }
        }
        textView.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
        return inflate;
    }
}
