package com.tencent.launcher;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class DockButton extends ImageView {
    private int a = -1;
    private int b = -1;

    public DockButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        int i;
        Drawable current;
        Bitmap bitmap;
        Drawable drawable = getDrawable();
        if (drawable instanceof BitmapDrawable) {
            Bitmap bitmap2 = ((BitmapDrawable) drawable).getBitmap();
            if (bitmap2 != null) {
                i = a.a(bitmap2);
            }
            i = 0;
        } else {
            if ((drawable instanceof StateListDrawable) && (current = drawable.getCurrent()) != null && (current instanceof BitmapDrawable) && (bitmap = ((BitmapDrawable) current).getBitmap()) != null) {
                i = a.a(bitmap);
            }
            i = 0;
        }
        float[] fArr = new float[9];
        getImageMatrix().getValues(fArr);
        int i2 = (int) fArr[5];
        if (i2 > 0) {
            this.b = i2;
        }
        this.a = i;
    }

    public final int a() {
        if (this.b < 0) {
            float[] fArr = new float[9];
            getImageMatrix().getValues(fArr);
            this.b = (int) fArr[5];
        }
        return this.a + this.b;
    }
}
