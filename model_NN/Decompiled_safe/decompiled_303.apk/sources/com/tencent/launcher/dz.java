package com.tencent.launcher;

import android.content.DialogInterface;
import com.tencent.module.setting.CustomAlertDialog;
import java.util.ArrayList;
import java.util.Iterator;

final class dz implements DialogInterface.OnClickListener {
    private /* synthetic */ ArrayList a;
    private /* synthetic */ Launcher b;

    dz(Launcher launcher, ArrayList arrayList) {
        this.b = launcher;
        this.a = arrayList;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (dialogInterface != null && (dialogInterface instanceof CustomAlertDialog)) {
            ((CustomAlertDialog) dialogInterface).a().setEnabled(false);
        }
        ArrayList arrayList = new ArrayList();
        Iterator it = this.a.iterator();
        while (it.hasNext()) {
            gy gyVar = (gy) it.next();
            if (gyVar.d) {
                am amVar = new am(gyVar.a);
                amVar.n = -1;
                amVar.c.setAction("android.intent.action.MAIN");
                arrayList.add(amVar);
            }
        }
        this.b.completeAddApplicationBatch(arrayList, !this.b.mDesktopLocked);
        dialogInterface.dismiss();
        this.a.clear();
    }
}
