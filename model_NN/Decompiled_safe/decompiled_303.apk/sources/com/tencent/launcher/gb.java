package com.tencent.launcher;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.tencent.launcher.base.b;

public final class gb extends Drawable {
    private static final Paint i = new Paint();
    private static final Paint j;
    private Bitmap a;
    private Bitmap b;
    private Bitmap c;
    private Rect d;
    private Rect e;
    private Rect f;
    private int g;
    private int h;
    private int k = -1;
    private boolean l = false;
    private int m = 0;

    static {
        Paint paint = new Paint();
        j = paint;
        paint.setAlpha(30);
        i.setFilterBitmap(true);
    }

    private void a(int i2, int i3, int i4, int i5) {
        Rect rect = this.f;
        if (rect == null) {
            rect = new Rect();
            this.f = rect;
        }
        rect.set(i2, i3, i4, i5);
    }

    public final Bitmap a() {
        return this.c;
    }

    public final void a(int i2) {
        this.k = i2;
    }

    public final void a(int i2, int i3) {
        int i4;
        int i5;
        int i6;
        int i7;
        Bitmap bitmap = this.c;
        this.g = i2;
        if (bitmap == null || i2 <= 0) {
            a(0, 0, i3, i3);
            return;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (i3 < width || i3 < height) {
            float f2 = ((float) width) / ((float) height);
            if (width > height) {
                i6 = (int) (((float) i3) / f2);
                i7 = i3;
            } else if (height > width) {
                i7 = (int) (((float) i3) * f2);
                i6 = i3;
            } else {
                i6 = i3;
                i7 = i3;
            }
            this.h = (this.h * i6) / height;
            i5 = i6;
            i4 = i7;
        } else {
            i5 = i3;
            i4 = i3;
        }
        int i8 = (i2 - i4) / 2;
        int i9 = (i2 - i5) / 2;
        a(i8, i9, i4 + i8, i5 + i9);
        Rect rect = this.d;
        if (rect == null) {
            rect = new Rect();
            this.d = rect;
        }
        rect.set(0, 0, i2, i2);
        Rect rect2 = this.e;
        if (rect2 == null) {
            rect2 = new Rect();
            this.e = rect2;
        }
        rect2.set(0, 0, i2, i2);
    }

    public final void a(Bitmap bitmap) {
        this.a = bitmap;
    }

    public final void a(Rect rect) {
        if (rect != null) {
            Rect rect2 = this.f;
            if (rect2 == null) {
                rect2 = new Rect();
                this.f = rect2;
            }
            rect2.set(rect);
        }
    }

    public final void a(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            this.b = ((BitmapDrawable) drawable).getBitmap();
        }
    }

    public final void a(boolean z) {
        this.l = z;
    }

    public final Rect b() {
        return this.f;
    }

    public final void b(int i2) {
        this.m = i2;
    }

    public final void b(Bitmap bitmap) {
        this.b = bitmap;
    }

    public final void b(Rect rect) {
        if (rect != null) {
            Rect rect2 = this.d;
            if (rect2 == null) {
                rect2 = new Rect();
                this.d = rect2;
            }
            rect2.set(rect);
        }
    }

    public final void b(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            this.c = bitmap;
            this.l = false;
            this.h = a.a(bitmap);
        }
    }

    public final void c(Bitmap bitmap) {
        this.c = bitmap;
        this.l = false;
        this.h = a.a(bitmap);
    }

    public final void c(Rect rect) {
        if (rect != null) {
            Rect rect2 = this.e;
            if (rect2 == null) {
                rect2 = new Rect();
                this.e = rect2;
            }
            rect2.set(rect);
        }
    }

    public final void c(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            this.c = bitmap;
            this.l = true;
            this.h = a.a(bitmap);
        }
    }

    public final boolean c() {
        return this.l;
    }

    public final int d() {
        return this.m;
    }

    public final void draw(Canvas canvas) {
        Bitmap bitmap = this.a;
        Bitmap bitmap2 = this.c;
        Bitmap bitmap3 = this.b;
        Rect rect = this.f;
        Rect rect2 = this.e;
        Rect rect3 = this.d;
        int i2 = this.g;
        if (bitmap != null) {
            if (rect3 == null) {
                canvas.drawBitmap(bitmap, 0.0f, 0.0f, i);
            } else {
                canvas.drawBitmap(bitmap, (Rect) null, rect3, i);
            }
        }
        if (bitmap2 != null) {
            boolean z = rect == null;
            if (this.k == 0) {
                int i3 = (z ? 0 : rect.top) + this.h + 1;
                canvas.save();
                canvas.clipRect(0.0f, (float) i3, (float) i2, ((float) i2) - (5.0f * b.b));
                canvas.scale(1.0f, -1.0f, (float) (i2 / 2), (float) i3);
                if (z) {
                    canvas.drawBitmap(bitmap2, 0.0f, 0.0f, i);
                } else {
                    canvas.drawBitmap(bitmap2, (Rect) null, rect, j);
                }
                canvas.restore();
            }
            if (z) {
                canvas.drawBitmap(bitmap2, 0.0f, 0.0f, i);
            } else {
                canvas.drawBitmap(bitmap2, (Rect) null, rect, i);
            }
        }
        if (bitmap3 == null) {
            return;
        }
        if (rect2 != null) {
            canvas.drawBitmap(bitmap3, (Rect) null, rect2, i);
        } else if (rect2 != null || rect3 == null) {
            canvas.drawBitmap(bitmap3, 0.0f, 0.0f, i);
        } else {
            canvas.drawBitmap(bitmap3, (Rect) null, rect3, i);
        }
    }

    public final Rect e() {
        return this.d;
    }

    public final int getIntrinsicHeight() {
        return this.g;
    }

    public final int getIntrinsicWidth() {
        return this.g;
    }

    public final int getMinimumHeight() {
        if (this.c == null) {
            return 0;
        }
        return this.c.getHeight();
    }

    public final int getMinimumWidth() {
        if (this.c == null) {
            return 0;
        }
        return this.c.getWidth();
    }

    public final int getOpacity() {
        return -3;
    }

    public final void setAlpha(int i2) {
        i.setAlpha(i2);
    }

    public final void setColorFilter(ColorFilter colorFilter) {
        i.setColorFilter(colorFilter);
    }
}
