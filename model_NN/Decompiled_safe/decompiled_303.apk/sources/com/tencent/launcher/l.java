package com.tencent.launcher;

import android.view.animation.AnimationSet;

final class l extends AnimationSet {
    l() {
        super(false);
    }

    public final boolean willChangeBounds() {
        return false;
    }

    public final boolean willChangeTransformationMatrix() {
        return true;
    }
}
