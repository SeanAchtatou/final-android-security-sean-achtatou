package com.tencent.launcher;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.launcher.home.i;
import com.tencent.module.theme.l;
import com.tencent.qqlauncher.R;

public class ApplicationsDrawer extends RelativeLayout implements e {
    private boolean a;
    private Animation b;
    private Animation c;
    private hr d;
    /* access modifiers changed from: private */
    public QGridLayout e;
    /* access modifiers changed from: private */
    public boolean f;
    private boolean g = true;
    private float h;
    private float i;
    private int j = -1;
    private Paint k = new Paint();
    private Shader l;
    private QNavigation m;
    private boolean n;

    public ApplicationsDrawer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setWillNotDraw(false);
        this.c = AnimationUtils.loadAnimation(context, R.anim.drop_down);
        this.b = AnimationUtils.loadAnimation(context, R.anim.drop_up);
        this.k.setColor(-16777216);
        this.c.setAnimationListener(new ei(this));
        this.b.setAnimationListener(new ek(this));
    }

    public final void a(Animation animation) {
        ViewGroup viewGroup = (ViewGroup) this.e.getParent();
        this.n = viewGroup.isDrawingCacheEnabled();
        viewGroup.setDrawingCacheQuality(524288);
        viewGroup.startAnimation(animation);
        findViewById(R.id.grid_navigation).startAnimation(animation);
    }

    public final void a(TextView textView) {
        if (this.e != null) {
            this.e.a(textView);
        }
    }

    public final void a(cm cmVar, boolean z) {
    }

    public final void a(fn fnVar) {
        boolean z = !i.a().b("setting_normal_functionlist", "").equals("1");
        findViewById(R.id.all_apps_h).setVisibility(z ? 0 : 8);
        findViewById(R.id.grid_navigation).setVisibility(z ? 0 : 8);
        findViewById(R.id.all_apps_v).setVisibility(z ? 8 : 0);
        if (z) {
            fnVar.a(this.e);
        } else {
            fnVar.a((VerticalAppLayout) findViewById(R.id.all_apps_v));
        }
    }

    public final void a(hr hrVar) {
        this.d = hrVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, float, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    public final void a(String str) {
        if (str == null || str.equals("")) {
            this.j = -1728053248;
        } else {
            try {
                this.j = Color.parseColor(str);
            } catch (Exception e2) {
                this.j = -1728053248;
            }
        }
        this.l = new LinearGradient(0.0f, this.h, 0.0f, this.i + this.h, this.j, 0, Shader.TileMode.CLAMP);
        this.k.setShader(this.l);
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        return this.f;
    }

    public final boolean a(int i2, Object obj) {
        return true;
    }

    public final boolean a(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
        return this.e.a(cmVar, i2 - i4, i3 - i5);
    }

    public final boolean a(cm cmVar, Object obj) {
        return true;
    }

    public final void b(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        clearAnimation();
        if (this.d != null) {
            this.d.onDrawerOpen();
        }
        this.m.setVisibility(4);
        startAnimation(this.c);
    }

    public final void c(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        setVisibility(0);
        if (this.e.getVisibility() == 0) {
            this.m.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.fade_in));
            this.m.setVisibility(0);
        }
        this.f = true;
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        setVisibility(0);
        if (this.d != null) {
            this.d.onDrawerOpen();
        }
        startAnimation(this.c);
        QGridLayout qGridLayout = this.e;
        if (qGridLayout != null && qGridLayout.getVisibility() == 0) {
            new Handler().postDelayed(new el(this), 200);
        }
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        this.f = false;
        this.g = true;
        if (this.d != null) {
            this.d.onDrawerClose();
        }
        setVisibility(8);
    }

    /* access modifiers changed from: package-private */
    public final void g() {
        clearAnimation();
        this.f = false;
        this.m.setVisibility(4);
        startAnimation(this.b);
        this.g = false;
    }

    /* access modifiers changed from: package-private */
    public final void h() {
        this.a = true;
    }

    /* access modifiers changed from: package-private */
    public final void i() {
        this.a = false;
    }

    public final void j() {
        ViewGroup viewGroup = (ViewGroup) this.e.getParent();
        viewGroup.clearAnimation();
        viewGroup.setDrawingCacheQuality(0);
        findViewById(R.id.grid_navigation).clearAnimation();
    }

    public final void k() {
        QGridLayout qGridLayout = this.e;
        if (qGridLayout != null && qGridLayout.getVisibility() == 0) {
            qGridLayout.o();
        }
    }

    public final void l() {
        QGridLayout qGridLayout = this.e;
        if (qGridLayout != null && qGridLayout.getVisibility() == 0 && qGridLayout.p()) {
            qGridLayout.q();
        }
    }

    public final void m() {
        QGridLayout qGridLayout = this.e;
        if (qGridLayout != null && qGridLayout.getVisibility() == 0) {
            qGridLayout.b(qGridLayout.getChildCount() - 1);
        }
    }

    public final void n() {
        this.e.r();
    }

    public final QGridLayout o() {
        return this.e;
    }

    public void onDraw(Canvas canvas) {
        canvas.drawPaint(this.k);
        super.onDraw(canvas);
    }

    public void onFinishInflate() {
        super.onFinishInflate();
        this.e = (QGridLayout) findViewById(R.id.all_apps_h);
        this.m = (QNavigation) findViewById(R.id.grid_navigation);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, float, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        this.h = ((float) i3) * 0.81f;
        this.i = ((float) i3) * 0.12f;
        float f2 = this.h;
        float f3 = this.i;
        if (this.j == -1) {
            String h2 = l.a().h();
            if (h2 == null || h2.equals("")) {
                this.j = -1728053248;
            } else {
                try {
                    this.j = Color.parseColor(h2);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
        this.l = new LinearGradient(0.0f, f2, 0.0f, f2 + f3, this.j, 0, Shader.TileMode.CLAMP);
        this.k.setShader(this.l);
    }
}
