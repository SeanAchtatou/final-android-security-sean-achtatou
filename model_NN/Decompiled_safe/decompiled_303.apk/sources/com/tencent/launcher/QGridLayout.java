package com.tencent.launcher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.a.a;
import com.tencent.launcher.base.BaseApp;
import com.tencent.launcher.home.c;
import com.tencent.module.theme.l;
import com.tencent.qqlauncher.R;
import com.tencent.util.DistinctQueue;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Queue;

public class QGridLayout extends ViewGroup implements View.OnClickListener, cm, e, gw, hc, hs {
    private boolean A;
    private int B;
    private int C;
    private int D;
    private int E;
    private int F;
    private int G;
    private int H;
    private int I;
    /* access modifiers changed from: private */
    public int J;
    private int K;
    private int L;
    /* access modifiers changed from: private */
    public View M;
    private View N;
    /* access modifiers changed from: private */
    public View O;
    private boolean P;
    private boolean Q;
    private Rect R;
    private Rect S;
    /* access modifiers changed from: private */
    public boolean T;
    private boolean U;
    private boolean V;
    /* access modifiers changed from: private */
    public boolean W;
    private Runnable X;
    private Runnable Y;
    private int[] Z;
    private int a;
    /* access modifiers changed from: private */
    public int aA;
    /* access modifiers changed from: private */
    public int aB;
    private ArrayList aC;
    private Bitmap aD;
    /* access modifiers changed from: private */
    public boolean aE;
    /* access modifiers changed from: private */
    public Queue aF;
    /* access modifiers changed from: private */
    public boolean aG;
    private PaintFlagsDrawFilter aH;
    /* access modifiers changed from: private */
    public View aa;
    /* access modifiers changed from: private */
    public Drawable ab;
    /* access modifiers changed from: private */
    public Drawable ac;
    private Rect ad;
    private Bitmap ae;
    private float af;
    private float ag;
    private int ah;
    private int ai;
    private DrawerTextView aj;
    private boolean ak;
    private int al;
    /* access modifiers changed from: private */
    public int am;
    /* access modifiers changed from: private */
    public int an;
    private int ao;
    private boolean ap;
    private Bitmap aq;
    private int ar;
    /* access modifiers changed from: private */
    public int as;
    /* access modifiers changed from: private */
    public int at;
    private int au;
    private int av;
    private long aw;
    private int ax;
    /* access modifiers changed from: private */
    public int ay;
    /* access modifiers changed from: private */
    public int az;
    private boolean b;
    private int c;
    private TextView d;
    /* access modifiers changed from: private */
    public int e;
    private int f;
    private Scroller g;
    private VelocityTracker h;
    private float i;
    private float j;
    private int k;
    /* access modifiers changed from: private */
    public Launcher l;
    private dt m;
    private boolean n;
    private boolean o;
    private int p;
    private int q;
    private Rect r;
    private Rect s;
    /* access modifiers changed from: private */
    public ArrayAdapter t;
    private id u;
    private QNavigation v;
    private View.OnLongClickListener w;
    private boolean x;
    private Animation y;
    private boolean z;

    public class LayoutParams extends ViewGroup.MarginLayoutParams {
        int a;
        int b;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }
    }

    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new y();
        int currentScreen;

        private SavedState(Parcel parcel) {
            super(parcel);
            this.currentScreen = -1;
            this.currentScreen = parcel.readInt();
        }

        /* synthetic */ SavedState(Parcel parcel, ci ciVar) {
            this(parcel);
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
            this.currentScreen = -1;
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.currentScreen);
        }
    }

    public QGridLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public QGridLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.b = true;
        this.f = -1;
        this.k = 0;
        this.r = new Rect();
        this.s = new Rect();
        this.x = true;
        this.R = new Rect();
        this.S = new Rect();
        this.Z = new int[1];
        this.ad = new Rect();
        this.af = 1.0f;
        this.ag = 1.0f;
        this.al = 120;
        this.am = 3;
        this.an = 3;
        this.ao = 3;
        this.ar = 3;
        this.ax = 110;
        this.aE = true;
        this.aF = new DistinctQueue();
        this.aG = false;
        this.aH = new PaintFlagsDrawFilter(0, 3);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.b, i2, 0);
        this.B = obtainStyledAttributes.getDimensionPixelSize(0, 10);
        this.C = obtainStyledAttributes.getDimensionPixelSize(1, 10);
        this.D = obtainStyledAttributes.getDimensionPixelSize(2, 10);
        this.E = obtainStyledAttributes.getDimensionPixelSize(3, 10);
        this.F = obtainStyledAttributes.getDimensionPixelSize(4, 10);
        this.G = obtainStyledAttributes.getDimensionPixelSize(5, 10);
        this.H = obtainStyledAttributes.getInt(6, 4);
        this.I = obtainStyledAttributes.getInt(7, 4);
        this.J = this.H * this.I;
        obtainStyledAttributes.recycle();
        TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, a.m, i2, 0);
        this.a = obtainStyledAttributes2.getInt(0, 0);
        obtainStyledAttributes2.recycle();
        Context context2 = getContext();
        this.g = new Scroller(context2);
        this.e = this.a;
        setClickable(true);
        Resources resources = context2.getResources();
        this.ab = a.a(context2, resources.getDrawable(R.drawable.folder_bg_adding_default), Launcher.sIconBackgroundDrawable, l.a().c(resources), null);
        this.y = AnimationUtils.loadAnimation(context2, R.anim.indicator_zoom_in);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context2);
        this.p = viewConfiguration.getScaledTouchSlop();
        this.q = viewConfiguration.getScaledMaximumFlingVelocity();
        this.w = new ci(this);
        this.X = new cg(this);
        this.Y = new cf(this);
    }

    private static int a(Rect rect, Rect rect2) {
        return (Math.min(rect.right, rect2.right) - Math.max(rect.left, rect2.left)) * (Math.min(rect.bottom, rect2.bottom) - Math.max(rect.top, rect2.top));
    }

    private View a(int i2, int i3, int i4, int i5) {
        int a2;
        int i6 = this.J;
        int i7 = this.e;
        int i8 = i7 * i6;
        int i9 = i6 * (i7 + 1);
        if (i9 >= getChildCount()) {
            i9 = getChildCount();
        }
        Rect rect = this.R;
        Rect rect2 = this.S;
        rect2.set(0, 0, this.B, this.C);
        rect2.offset(i2 - i4, i3 - i5);
        int width = i7 * getWidth();
        View view = null;
        int i10 = 0;
        for (int i11 = i8; i11 < i9; i11++) {
            View childAt = getChildAt(i11);
            if (childAt.getVisibility() != 8) {
                childAt.getHitRect(rect);
                rect.offset(-width, 0);
                if (Rect.intersects(rect2, rect) && (a2 = a(rect2, rect)) > 0 && a2 > i10) {
                    view = childAt;
                    i10 = a2;
                }
            }
        }
        return view;
    }

    private View a(int i2, int i3, int i4, int i5, ha haVar, int[] iArr) {
        int a2;
        int i6 = this.J;
        int i7 = this.e;
        int i8 = i7 * i6;
        int i9 = i6 * (i7 + 1);
        if (i9 >= getChildCount()) {
            i9 = getChildCount();
        }
        Rect rect = this.R;
        Rect rect2 = this.S;
        View view = null;
        rect2.set(0, 0, this.B, this.C);
        rect2.offset(i2 - i4, i3 - i5);
        int width = i7 * getWidth();
        View view2 = null;
        int i10 = 0;
        int i11 = 0;
        for (int i12 = i8; i12 < i9; i12++) {
            View childAt = getChildAt(i12);
            if (childAt.getVisibility() != 8) {
                childAt.getHitRect(rect);
                rect.offset(-width, 0);
                if (Rect.intersects(rect2, rect) && (a2 = a(rect2, rect)) > 0) {
                    if (haVar.t > ((ha) childAt.getTag()).t) {
                        if (haVar.m == 2) {
                            if (Math.min(Math.abs(rect.top - rect2.bottom), Math.abs(rect.bottom - rect2.top)) > (rect.height() >> 1) && rect2.right - rect.left < rect.width()) {
                                iArr[0] = 0;
                                return childAt;
                            }
                        } else {
                            if (Math.min(Math.abs(rect.top - rect2.bottom), Math.abs(rect.bottom - rect2.top)) > (rect.height() >> 1) && rect2.right - rect.left < (rect.width() * 3) / 4) {
                                if (a2 > i10) {
                                    view2 = childAt;
                                    i10 = a2;
                                }
                            } else if (a2 > i11) {
                                view = childAt;
                                i11 = a2;
                            }
                        }
                    } else if (haVar.m == 2) {
                        if (Math.min(Math.abs(rect.top - rect2.bottom), Math.abs(rect.bottom - rect2.top)) > (rect.height() >> 1) && rect.right - rect2.left < rect.width()) {
                            iArr[0] = 0;
                            return childAt;
                        }
                    } else {
                        if (Math.min(Math.abs(rect.top - rect2.bottom), Math.abs(rect.bottom - rect2.top)) > (rect.height() >> 1) && rect.right - rect2.left < (rect.width() * 3) / 4) {
                            if (a2 > i10) {
                                view2 = childAt;
                                i10 = a2;
                            }
                        } else if (a2 > i11) {
                            view = childAt;
                            i11 = a2;
                        }
                    }
                }
            }
        }
        if (haVar.m == 2) {
            return null;
        }
        if (view2 != null) {
            iArr[0] = 0;
            return view2;
        } else if (view == null || i11 <= ((view.getWidth() * view.getHeight()) * 2) / 3) {
            return null;
        } else {
            iArr[0] = i11;
            return view;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private View a(ha haVar) {
        View inflate = LayoutInflater.from(getContext()).inflate((int) R.layout.application_boxed, (ViewGroup) null, false);
        am amVar = (am) haVar;
        amVar.n = -300;
        if (!amVar.f) {
            amVar.d = a.a(getContext(), amVar.d, amVar);
            amVar.f = true;
        }
        TextView textView = (TextView) inflate;
        textView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, amVar.d, (Drawable) null, (Drawable) null);
        textView.setText(amVar.a);
        textView.setTag(amVar);
        textView.setFocusable(true);
        textView.setOnClickListener(this);
        textView.setOnLongClickListener(this.w);
        return textView;
    }

    private void a(int i2) {
        int width = getWidth();
        int abs = Math.abs(i2 / width);
        this.v.b(Math.abs(i2 % width) > width / 2 ? abs + 1 : abs);
    }

    private void a(int i2, int i3) {
        View view = this.N;
        if (view != null) {
            a(getChildAt(getChildCount() - 1), i2, i3);
            ff.a(getContext(), (ha) view.getTag());
            if (getChildAt(getChildCount() - 1) != view) {
                c(view);
            }
        }
    }

    private void a(Canvas canvas, int i2, long j2) {
        int i3 = (this.J * i2) - 1;
        int i4 = (i2 + 1) * this.J;
        if (i3 < 0) {
            i3 = 0;
        }
        if (i4 >= getChildCount()) {
            i4 = getChildCount() - 1;
        }
        while (i3 <= i4) {
            drawChild(canvas, getChildAt(i3), j2);
            i3++;
        }
    }

    private void a(View view, int i2, int i3) {
        this.O = this.N;
        View view2 = this.N;
        this.Q = true;
        View view3 = view == null ? view2 : view;
        this.ap = view2 != view3;
        if (this.ap) {
            this.T = true;
        }
        int left = view3.getLeft();
        int top = view3.getTop();
        this.as = (this.e * getWidth()) + i2;
        this.au = left;
        this.at = i3;
        this.av = top;
        this.ar = 1;
        this.aq = b(view2);
        invalidate();
    }

    static /* synthetic */ boolean a(QGridLayout qGridLayout, int i2, ArrayList arrayList) {
        bw bwVar;
        Object obj = null;
        switch (i2) {
            case 0:
                obj = new fd();
                bwVar = new bw();
                break;
            case 1:
                obj = new gs();
                bwVar = null;
                break;
            case 2:
                obj = new k();
                bwVar = null;
                break;
            default:
                bwVar = null;
                break;
        }
        ArrayAdapter arrayAdapter = qGridLayout.t;
        int count = arrayAdapter.getCount();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        for (int i3 = 0; i3 < count; i3++) {
            ha haVar = (ha) arrayAdapter.getItem(i3);
            if (haVar instanceof am) {
                arrayList3.add((am) haVar);
            } else if (haVar instanceof bq) {
                arrayList2.add((bq) haVar);
            }
        }
        if (bwVar != null) {
            Collections.sort(arrayList2, bwVar);
        }
        if (obj != null) {
            Collections.sort(arrayList3, obj);
            Iterator it = arrayList2.iterator();
            while (it.hasNext()) {
                Collections.sort(((bq) it.next()).b, obj);
            }
        }
        Iterator it2 = arrayList2.iterator();
        boolean z2 = false;
        int i4 = 0;
        while (it2.hasNext()) {
            bq bqVar = (bq) it2.next();
            arrayList.add(bqVar);
            if (bqVar.t != i4) {
                z2 = true;
            }
            bqVar.t = i4;
            i4++;
        }
        Iterator it3 = arrayList3.iterator();
        while (it3.hasNext()) {
            am amVar = (am) it3.next();
            arrayList.add(amVar);
            if (amVar.t != i4) {
                z2 = true;
            }
            amVar.t = i4;
            i4++;
        }
        return z2;
    }

    private boolean a(Object obj, DrawerTextView drawerTextView, int i2, int i3) {
        if (this.N == null) {
            return false;
        }
        am amVar = (am) obj;
        bq bqVar = (bq) drawerTextView.getTag();
        if (!bqVar.a(amVar)) {
            Toast.makeText(getContext(), (int) R.string.folder_is_full, 0).show();
            return false;
        }
        Rect d2 = drawerTextView.d();
        Rect rect = this.ad;
        rect.set(0, 0, this.B, this.C);
        rect.offset(drawerTextView.getLeft(), drawerTextView.getTop());
        Bitmap b2 = this.m.b();
        try {
            this.ae = Bitmap.createBitmap(b2);
        } catch (OutOfMemoryError e2) {
            this.ae = b2;
        }
        this.af = 1.0f;
        this.ag = ((float) d2.width()) / ((float) drawerTextView.getWidth());
        this.as = i2;
        this.at = i3;
        this.ah = d2.centerX();
        this.ai = d2.centerY();
        this.ao = 1;
        this.aj = drawerTextView;
        this.ak = true;
        invalidate();
        amVar.n = bqVar.l;
        this.aF.offer(amVar);
        this.T = true;
        c(this.N);
        if (this.T) {
            this.T = false;
            BaseApp.d().post(new ch(this));
        }
        return true;
    }

    private static Bitmap b(View view) {
        if (view == null) {
            return null;
        }
        boolean willNotCacheDrawing = view.willNotCacheDrawing();
        view.setWillNotCacheDrawing(false);
        int drawingCacheBackgroundColor = view.getDrawingCacheBackgroundColor();
        view.setDrawingCacheBackgroundColor(0);
        if (drawingCacheBackgroundColor != 0) {
            view.destroyDrawingCache();
        }
        view.buildDrawingCache();
        Bitmap drawingCache = view.getDrawingCache();
        if (drawingCache != null) {
            drawingCache = Bitmap.createBitmap(drawingCache);
        }
        view.setWillNotCacheDrawing(willNotCacheDrawing);
        return drawingCache;
    }

    private void b(int i2, int i3) {
        this.O = this.N;
        this.Q = false;
        this.ap = false;
        View view = this.N;
        int left = view.getLeft();
        int top = view.getTop();
        this.as = (this.e * getWidth()) + i2;
        this.au = left;
        this.at = i3;
        this.av = top;
        this.ar = 1;
        this.aq = b(view);
        invalidate();
    }

    private void c(int i2) {
        if (this.d.getVisibility() != 0) {
            this.d.setVisibility(0);
        }
        this.d.setText("" + i2);
        int i3 = i2 - 1;
        if (i3 < 0) {
            i3 = 0;
        }
        View childAt = this.v.getChildAt(i3);
        if (childAt != null) {
            int[] iArr = new int[2];
            childAt.getLocationInWindow(iArr);
            int height = this.d.getHeight();
            int width = this.d.getWidth();
            int height2 = childAt.getHeight();
            int width2 = childAt.getWidth();
            int paddingTop = getContext() instanceof Activity ? ((ViewGroup) ((Activity) getContext()).getWindow().getDecorView()).getChildAt(0).getPaddingTop() : 0;
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.d.getLayoutParams();
            layoutParams.leftMargin = ((width2 / 2) + iArr[0]) - (width / 2);
            layoutParams.topMargin = ((iArr[1] + height2) - height) - paddingTop;
            this.d.setLayoutParams(layoutParams);
        }
    }

    private void c(int i2, int i3) {
        this.O = this.N;
        this.Q = false;
        this.ap = false;
        View view = this.N;
        int i4 = this.ay;
        int i5 = this.az;
        this.as = (this.e * getWidth()) + i2;
        this.au = i4;
        this.at = i3;
        this.av = i5;
        this.ar = 1;
        this.aq = b(view);
        invalidate();
    }

    private void c(View view) {
        int i2 = this.J;
        int i3 = this.e;
        int indexOfChild = indexOfChild(view);
        int i4 = indexOfChild / i2;
        ArrayAdapter arrayAdapter = this.t;
        arrayAdapter.setNotifyOnChange(false);
        Queue queue = this.aF;
        if (i3 == i4 || i3 == i4 + 1 || i3 == (((getChildCount() - 1) / this.J) + 1) - 1) {
            int i5 = ((i3 + 1) * i2) + 1;
            if (i5 >= getChildCount()) {
                i5 = getChildCount();
            }
            view.setVisibility(4);
            int i6 = i3 != i4 ? i2 * i3 : indexOfChild + 1;
            if (getChildAt(i6) != null) {
                fl flVar = new fl();
                flVar.d = view;
                if (flVar.e == null) {
                    flVar.e = new ArrayList();
                }
                this.am = 2;
                for (int i7 = i6; i7 < i5; i7++) {
                    View childAt = getChildAt(i7);
                    View childAt2 = getChildAt(i7 - 1);
                    if (!(childAt == null || childAt2 == null)) {
                        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, (float) (childAt2.getLeft() - childAt.getLeft()), 0.0f, (float) (childAt2.getTop() - childAt.getTop()));
                        translateAnimation.setDuration(200);
                        translateAnimation.setFillAfter(true);
                        translateAnimation.setAnimationListener(flVar);
                        flVar.c++;
                        flVar.e.add(childAt);
                        ha haVar = (ha) childAt.getTag();
                        haVar.t = i7 - 1;
                        queue.offer(haVar);
                        childAt.startAnimation(translateAnimation);
                    }
                }
                for (int i8 = i5; i8 < getChildCount(); i8++) {
                    ha haVar2 = (ha) getChildAt(i8).getTag();
                    haVar2.t = i8 - 1;
                    queue.offer(haVar2);
                }
                this.T = true;
                ha haVar3 = (ha) view.getTag();
                arrayAdapter.remove(haVar3);
                if (haVar3.n == -300) {
                    ff.f(getContext(), haVar3);
                }
            } else if (view.getParent() != null) {
                this.am = 3;
                ha haVar4 = (ha) view.getTag();
                arrayAdapter.remove(haVar4);
                if (haVar4.n == -300) {
                    ff.f(getContext(), haVar4);
                }
                ((ViewGroup) view.getParent()).removeView(view);
                this.aG = true;
            }
        } else {
            this.aG = true;
            ha haVar5 = (ha) view.getTag();
            arrayAdapter.remove(haVar5);
            ((ViewGroup) view.getParent()).removeView(view);
            if (haVar5.n == -300) {
                ff.f(getContext(), haVar5);
            }
        }
    }

    private void s() {
        setChildrenDrawnWithCacheEnabled(true);
        int childCount = getChildCount();
        int childCount2 = ((getChildCount() - 1) / this.J) + 1;
        int i2 = this.J;
        int i3 = this.e - 1;
        int i4 = this.e + 1;
        if (i3 < 0) {
            i3 = 0;
        }
        int i5 = ((i4 < childCount2 ? i4 : childCount2 - 1) + 1) * i2;
        if (i5 < childCount) {
            childCount = i5;
        }
        for (int i6 = i3 * i2; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            if (childAt.getTag() instanceof bq) {
                childAt.setDrawingCacheQuality(0);
            } else {
                childAt.setDrawingCacheQuality(524288);
            }
            childAt.setDrawingCacheEnabled(true);
            childAt.buildDrawingCache();
        }
        setAlwaysDrawnWithCacheEnabled(true);
    }

    private void t() {
        int i2 = (this.e + 1) * this.J;
        int childCount = getChildCount();
        if (i2 > childCount) {
            i2 = childCount;
        }
        for (int i3 = this.J * this.e; i3 < i2; i3++) {
            getChildAt(i3).cancelLongPress();
        }
    }

    private void u() {
        if (!this.U && this.am != 2) {
            postDelayed(this.X, 220);
            this.W = true;
            this.U = true;
            this.am = 1;
            this.O = this.N;
        }
    }

    private void v() {
        if (this.U) {
            removeCallbacks(this.X);
            this.U = false;
            this.W = false;
            if (this.am != 2) {
                this.am = 3;
            }
        }
    }

    private void w() {
        if (this.V) {
            if (this.aa != null) {
                ((DrawerTextView) this.aa).b();
            }
            removeCallbacks(this.Y);
            this.V = false;
            this.ac = null;
            if (this.an != 2) {
                this.an = 3;
            }
            invalidate();
        }
    }

    public final void a() {
        this.v.e();
        this.aE = true;
        this.N = null;
        this.an = 3;
        this.ac = null;
        if (this.d.getVisibility() != 8) {
            this.d.setVisibility(8);
        }
    }

    public final void a(int i2, float f2) {
        int i3;
        int width = getWidth();
        if (!this.z) {
            this.z = true;
        }
        float f3 = f2 > 1.0f ? 1.0f : f2;
        if (i2 >= (((getChildCount() - 1) / this.J) + 1) - 1) {
            i3 = (((getChildCount() - 1) / this.J) + 1) - 1;
            f3 = 0.0f;
        } else {
            i3 = i2;
        }
        scrollTo(((int) (((float) width) * f3)) + (i3 * width), 0);
        c(((f3 <= 0.5f || i3 >= getChildCount() - 1) ? i3 : i3 + 1) + 1);
    }

    /* access modifiers changed from: package-private */
    public final void a(View view) {
        if (view != null) {
            if (this.m == null) {
                this.m = this.l.getDragLayer();
            }
            if (this.m != null) {
                this.aE = false;
                this.N = view;
                this.O = view;
                this.P = false;
                this.m.a(view, this, view.getTag(), 0);
                this.ay = view.getLeft();
                this.az = view.getTop();
                view.setVisibility(4);
                invalidate();
            }
        }
    }

    public final void a(View view, cm cmVar, Object obj, int i2) {
        this.v.d();
    }

    public final void a(View view, boolean z2) {
        this.t.setNotifyOnChange(false);
        if (this.N != null) {
            this.O = this.N;
        }
        if (z2 && this.O != null && (view instanceof DeleteZone)) {
            View view2 = this.O;
            if (((DeleteZone) view).a && (view2.getTag() instanceof bq)) {
                DrawerTextView drawerTextView = (DrawerTextView) view2;
                ArrayAdapter arrayAdapter = this.t;
                arrayAdapter.setNotifyOnChange(false);
                bq bqVar = (bq) drawerTextView.getTag();
                if (bqVar.b.size() <= 0) {
                    Launcher.getModel().c(bqVar);
                    c(drawerTextView);
                    return;
                }
                ArrayList arrayList = bqVar.b;
                am amVar = (am) arrayList.get(0);
                Context context = getContext();
                if (amVar != null) {
                    arrayAdapter.remove(bqVar);
                    Launcher.getModel().c(bqVar);
                    ff.f(context, bqVar);
                    removeViewInLayout(drawerTextView);
                    View a2 = a(amVar);
                    int i2 = bqVar.t;
                    amVar.t = i2;
                    arrayAdapter.insert(amVar, i2);
                    addViewInLayout(a2, i2, drawerTextView.getLayoutParams());
                    ff.a(context, amVar);
                }
                int size = arrayList.size();
                for (int i3 = 1; i3 < size; i3++) {
                    am amVar2 = (am) arrayList.get(i3);
                    View a3 = a(amVar2);
                    amVar2.t = arrayAdapter.getCount();
                    ff.a(context, amVar2);
                    arrayAdapter.add(amVar2);
                    addViewInLayout(a3, -1, new ViewGroup.LayoutParams(-2, -2));
                }
                this.v.removeAllViews();
                this.v.a(((getChildCount() - 1) / this.J) + 1);
                this.v.b(this.e);
                requestLayout();
                invalidate();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(ListAdapter listAdapter) {
        if (listAdapter != null) {
            if (this.u != null) {
                listAdapter.unregisterDataSetObserver(this.u);
            }
            this.t = (ArrayAdapter) listAdapter;
            c();
            this.u = new id(this);
            this.t.registerDataSetObserver(this.u);
        }
    }

    public final void a(TextView textView) {
        this.d = textView;
    }

    /* access modifiers changed from: package-private */
    public final void a(Launcher launcher) {
        this.l = launcher;
    }

    public final void a(QNavigation qNavigation) {
        this.v = qNavigation;
        this.v.f();
        this.v.a(this);
        this.v.b(this.e);
    }

    public final void a(cm cmVar, boolean z2) {
        if (this.d.getVisibility() != 8) {
            this.d.setVisibility(8);
        }
        this.aq = null;
        if (z2) {
            v();
            w();
        }
    }

    public final void a(dt dtVar) {
        this.m = dtVar;
    }

    public final void a(String str, int i2) {
        int i3 = this.J;
        ArrayAdapter arrayAdapter = this.t;
        arrayAdapter.setNotifyOnChange(false);
        int min = Math.min(i3 * i2, getChildCount());
        bq bqVar = new bq();
        bqVar.h = str;
        bqVar.a = c.d(bqVar.h.toString());
        if (bqVar.a == null || "".equals(bqVar.a)) {
            bqVar.a = bqVar.h.toString();
        }
        bqVar.t = min;
        bqVar.n = -300;
        BaseApp.d().post(new ce(this, bqVar));
        DrawerTextView a2 = DrawerTextView.a(getContext(), this, bqVar, null);
        a2.setOnClickListener(this);
        a2.setOnLongClickListener(this.w);
        addView(a2, min);
        arrayAdapter.insert(bqVar, min);
    }

    public final boolean a(int i2, Object obj) {
        return true;
    }

    public final boolean a(cm cmVar, int i2, int i3) {
        DrawerTextView drawerTextView;
        if (this.O == null) {
            return false;
        }
        if (this.P) {
            c(this.O);
            if ((cmVar instanceof UserFolder) && (drawerTextView = ((UserFolder) cmVar).f.j) != null) {
                drawerTextView.f();
            }
        } else {
            this.O.setVisibility(4);
            a(this.O, i2, i3);
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x01ad  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01e3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(com.tencent.launcher.cm r9, int r10, int r11, int r12, int r13, java.lang.Object r14) {
        /*
            r8 = this;
            r0 = r14
            com.tencent.launcher.ha r0 = (com.tencent.launcher.ha) r0
            r6 = r0
            int[] r7 = r8.Z
            r1 = r8
            r2 = r10
            r3 = r11
            r4 = r12
            r5 = r13
            android.view.View r1 = r1.a(r2, r3, r4, r5, r6, r7)
            r8.c(r9, r10, r11, r12, r13, r14)
            android.widget.TextView r2 = r8.d
            int r2 = r2.getVisibility()
            r3 = 8
            if (r2 == r3) goto L_0x0023
            android.widget.TextView r2 = r8.d
            r3 = 8
            r2.setVisibility(r3)
        L_0x0023:
            r2 = 1
            r8.aE = r2
            android.view.View r2 = r8.N
            if (r2 != 0) goto L_0x002c
            r1 = 1
        L_0x002b:
            return r1
        L_0x002c:
            boolean r2 = r8.V
            if (r2 == 0) goto L_0x0131
            int r2 = r8.an
            r3 = 1
            if (r2 != r3) goto L_0x0044
            r8.w()
            r8.v()
            int r1 = r10 - r12
            int r2 = r11 - r13
            r8.b(r1, r2)
            r1 = 0
            goto L_0x002b
        L_0x0044:
            if (r1 == 0) goto L_0x012e
            r8.w()
            r8.v()
            java.lang.Object r2 = r1.getTag()
            boolean r2 = r2 instanceof com.tencent.launcher.bq
            if (r2 == 0) goto L_0x006b
            r0 = r1
            com.tencent.launcher.DrawerTextView r0 = (com.tencent.launcher.DrawerTextView) r0
            r9 = r0
            int r1 = r10 - r12
            int r2 = r11 - r13
            boolean r1 = r8.a(r14, r9, r1, r2)
            if (r1 != 0) goto L_0x0069
            int r1 = r10 - r12
            int r2 = r11 - r13
            r8.b(r1, r2)
        L_0x0069:
            r1 = 0
            goto L_0x002b
        L_0x006b:
            java.lang.Object r2 = r1.getTag()
            boolean r2 = r2 instanceof com.tencent.launcher.am
            if (r2 == 0) goto L_0x0069
            com.tencent.launcher.am r14 = (com.tencent.launcher.am) r14
            java.lang.Object r9 = r1.getTag()
            com.tencent.launcher.am r9 = (com.tencent.launcher.am) r9
            android.widget.ArrayAdapter r2 = r8.t
            r3 = 0
            r2.setNotifyOnChange(r3)
            android.view.View r3 = r8.N
            if (r3 == 0) goto L_0x0069
            r2.remove(r9)
            com.tencent.launcher.bq r3 = new com.tencent.launcher.bq
            r3.<init>()
            android.content.Context r4 = r8.getContext()
            android.content.res.Resources r4 = r4.getResources()
            r5 = 2131297010(0x7f0902f2, float:1.8211953E38)
            java.lang.String r4 = r4.getString(r5)
            r3.h = r4
            java.lang.CharSequence r4 = r3.h
            java.lang.String r4 = r4.toString()
            java.lang.String r4 = com.tencent.launcher.home.c.d(r4)
            r3.a = r4
            java.lang.String r4 = r3.a
            if (r4 == 0) goto L_0x00b8
            java.lang.String r4 = ""
            java.lang.String r5 = r3.a
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x00c0
        L_0x00b8:
            java.lang.CharSequence r4 = r3.h
            java.lang.String r4 = r4.toString()
            r3.a = r4
        L_0x00c0:
            int r4 = r9.t
            r3.t = r4
            r4 = -300(0xfffffffffffffed4, double:NaN)
            r3.n = r4
            android.content.Context r4 = r8.getContext()
            com.tencent.launcher.ff.b(r4, r3)
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            android.graphics.drawable.Drawable r5 = r9.d
            r4.add(r5)
            android.graphics.drawable.Drawable r5 = r14.d
            r4.add(r5)
            long r4 = r3.l
            r9.n = r4
            long r4 = r3.l
            r14.n = r4
            r3.a(r9)
            r3.a(r14)
            com.tencent.launcher.DrawerTextView r1 = (com.tencent.launcher.DrawerTextView) r1
            android.content.Context r4 = r8.getContext()
            com.tencent.launcher.DrawerTextView.a(r1, r4, r8, r3, r8)
            r4 = 1
            r1.setFocusable(r4)
            r3.j = r1
            int r1 = r3.t
            r2.insert(r3, r1)
            com.tencent.launcher.ff r1 = com.tencent.launcher.Launcher.getModel()
            r1.a(r3)
            java.util.Queue r1 = r8.aF
            r1.add(r9)
            java.util.Queue r1 = r8.aF
            r1.add(r14)
            r1 = 1
            r8.T = r1
            android.view.View r1 = r8.N
            r8.c(r1)
            boolean r1 = r8.T
            if (r1 == 0) goto L_0x0069
            r1 = 0
            r8.T = r1
            android.os.Handler r1 = com.tencent.launcher.base.BaseApp.d()
            com.tencent.launcher.ch r2 = new com.tencent.launcher.ch
            r2.<init>(r8)
            r1.post(r2)
            goto L_0x0069
        L_0x012e:
            r8.w()
        L_0x0131:
            int r2 = r8.am
            r3 = 1
            if (r2 != r3) goto L_0x0157
            r8.v()
            android.view.View r1 = r8.N
            r8.O = r1
            int r1 = r8.e
            int r2 = r8.getWidth()
            int r1 = r1 * r2
            int r1 = r1 + r10
            int r1 = r1 - r12
            r8.aA = r1
            int r1 = r11 - r13
            r8.aB = r1
            r1 = 0
            r8.W = r1
            java.lang.Runnable r1 = r8.X
            r1.run()
            r1 = 0
            goto L_0x002b
        L_0x0157:
            int r2 = r8.am
            r3 = 2
            if (r2 != r3) goto L_0x016d
            r1 = 3
            r8.am = r1
            android.view.View r1 = r8.N
            r8.O = r1
            int r1 = r10 - r12
            int r2 = r11 - r13
            r8.c(r1, r2)
            r1 = 0
            goto L_0x002b
        L_0x016d:
            if (r1 != 0) goto L_0x0228
            android.widget.Scroller r1 = r8.g
            boolean r1 = r1.isFinished()
            if (r1 == 0) goto L_0x01b7
            int r1 = r8.e
        L_0x0179:
            int r2 = r8.getChildCount()
            r3 = 1
            int r2 = r2 - r3
            int r3 = r8.J
            int r2 = r2 / r3
            int r2 = r2 + 1
            r3 = 1
            int r2 = r2 - r3
            if (r1 != r2) goto L_0x01e1
            int r2 = r8.getChildCount()
            r3 = 1
            int r2 = r2 - r3
            android.view.View r2 = r8.getChildAt(r2)
            android.graphics.Rect r3 = r8.R
            r4 = 0
            int r5 = r2.getBottom()
            int r6 = r8.getWidth()
            int r7 = r8.getBottom()
            r3.set(r4, r5, r6, r7)
            boolean r4 = r3.contains(r10, r11)
            if (r4 == 0) goto L_0x01ba
            r1 = 1
        L_0x01ab:
            if (r1 == 0) goto L_0x01e3
            int r1 = r10 - r12
            int r2 = r11 - r13
            r8.a(r1, r2)
        L_0x01b4:
            r1 = 0
            goto L_0x002b
        L_0x01b7:
            int r1 = r8.f
            goto L_0x0179
        L_0x01ba:
            r4 = 0
            int r5 = r2.getTop()
            int r6 = r8.getWidth()
            int r7 = r2.getBottom()
            r3.set(r4, r5, r6, r7)
            int r2 = r2.getRight()
            int r4 = r8.getWidth()
            int r1 = r1 * r4
            int r1 = r2 - r1
            r2 = 0
            r3.offset(r1, r2)
            boolean r1 = r3.contains(r10, r11)
            if (r1 == 0) goto L_0x01e1
            r1 = 1
            goto L_0x01ab
        L_0x01e1:
            r1 = 0
            goto L_0x01ab
        L_0x01e3:
            int r1 = r8.getChildCount()
            r2 = 1
            int r1 = r1 - r2
            android.view.View r1 = r8.getChildAt(r1)
            android.view.View r2 = r8.N
            if (r1 != r2) goto L_0x0220
            android.view.View r1 = r8.a(r10, r11, r12, r13)
            if (r1 == 0) goto L_0x0218
            android.view.View r2 = r8.N
            r8.O = r2
            r8.M = r1
            int r1 = r8.e
            int r2 = r8.getWidth()
            int r1 = r1 * r2
            int r1 = r1 + r10
            int r1 = r1 - r12
            r8.aA = r1
            int r1 = r11 - r13
            r8.aB = r1
            r1 = 0
            r8.W = r1
            r1 = 1
            r8.am = r1
            java.lang.Runnable r1 = r8.X
            r1.run()
            goto L_0x01b4
        L_0x0218:
            int r1 = r10 - r12
            int r2 = r11 - r13
            r8.c(r1, r2)
            goto L_0x01b4
        L_0x0220:
            int r1 = r10 - r12
            int r2 = r11 - r13
            r8.c(r1, r2)
            goto L_0x01b4
        L_0x0228:
            if (r1 == 0) goto L_0x0234
            int r1 = r10 - r12
            int r2 = r11 - r13
            r8.c(r1, r2)
            r1 = 0
            goto L_0x002b
        L_0x0234:
            android.view.View r1 = r8.N
            r2 = 0
            r1.setVisibility(r2)
            r1 = 1
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.launcher.QGridLayout.a(com.tencent.launcher.cm, int, int, int, int, java.lang.Object):boolean");
    }

    public final boolean a(cm cmVar, Object obj) {
        return this.N != null && (cmVar == this || (cmVar instanceof UserFolder));
    }

    public void addFocusables(ArrayList arrayList, int i2, int i3) {
        if (this.l.isDrawerOpen()) {
            Folder openFolder = this.l.getOpenFolder();
            if (openFolder == null) {
                try {
                    int i4 = this.J;
                    for (int i5 = this.e * i4; i5 < (this.e + 1) * i4; i5++) {
                        getChildAt(i5).addFocusables(arrayList, i2);
                    }
                } catch (Exception e2) {
                }
            } else {
                openFolder.addFocusables(arrayList, i2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final int b() {
        return this.e;
    }

    public final void b(int i2) {
        if (this.g.isFinished()) {
            s();
            int max = Math.max(0, Math.min(i2, (((getChildCount() - 1) / this.J) + 1) - 1));
            boolean z2 = max != this.e;
            this.f = max;
            int i3 = !z2 ? 200 : 1;
            int width = (max * getWidth()) - getScrollX();
            int abs = (Math.abs(width) * 2) + i3;
            int i4 = abs > 650 ? 650 : abs;
            this.g.startScroll(getScrollX(), 0, width, 0, i4);
            this.y.setDuration((long) i4);
            this.v.a(this.f, this.y);
            "snapToScreen, delta:" + width + ",duration:" + i4;
            invalidate();
        }
    }

    public final void b(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
        if (cmVar instanceof UserFolder) {
            this.o = false;
        }
        if (this.d.getVisibility() != 8) {
            this.d.setVisibility(8);
        }
        c(cmVar, i2, i3, i4, i5, obj);
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        ArrayAdapter arrayAdapter = this.t;
        int count = arrayAdapter.getCount();
        int i2 = this.J;
        int i3 = -1;
        int i4 = 0;
        while (i4 < count) {
            int i5 = i4 / i2;
            if (i3 != i5) {
                this.v.a();
            }
            View view = arrayAdapter.getView(i4, getChildAt(i4), this);
            view.destroyDrawingCache();
            view.setOnClickListener(this);
            view.setOnLongClickListener(this.w);
            if (view.getParent() == null) {
                addView(view, i4);
            }
            i4++;
            i3 = i5;
        }
        if (getChildCount() > count) {
            removeViewsInLayout(count, getChildCount() - count);
        }
        int childCount = ((getChildCount() - 1) / this.J) + 1;
        if (this.e > childCount - 1) {
            this.e = childCount - 1;
            scrollTo(this.e * getWidth(), 0);
        }
        this.v.b(this.e);
        int childCount2 = getChildCount();
        for (int i6 = 0; i6 < childCount2; i6++) {
            View childAt = getChildAt(i6);
            Object tag = childAt.getTag();
            if (tag != null && (tag instanceof bq)) {
                ((DrawerTextView) childAt).f();
            }
        }
        this.aG = true;
        requestLayout();
        invalidate();
    }

    public final void c(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
        ha haVar = (ha) obj;
        View a2 = a(i2, i3, i4, i5, haVar, this.Z);
        if (this.d.getVisibility() != 8) {
            this.d.setVisibility(8);
        }
        if (this.N == null) {
            View a3 = a(haVar);
            haVar.t = getChildCount();
            haVar.n = -300;
            a3.setVisibility(4);
            this.t.setNotifyOnChange(false);
            this.t.add(haVar);
            addView(a3);
            this.N = a3;
            this.O = a3;
            this.P = true;
        }
        if (a2 == null || a2 == this.N) {
            v();
            w();
        } else if (a2 != this.M) {
            int i6 = this.Z[0];
            v();
            w();
            if (i6 <= 0) {
                u();
            } else if (!this.V) {
                postDelayed(this.Y, 300);
                this.V = true;
                this.an = 1;
            }
        } else if (this.Z[0] <= 0) {
            w();
            u();
        } else {
            v();
            if (!this.V) {
                postDelayed(this.Y, 300);
                this.V = true;
                this.an = 1;
            }
        }
        this.M = a2;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public void computeScroll() {
        if (this.g.computeScrollOffset()) {
            "computeScroll,cx:" + this.g.getCurrX() + ",timePassed:" + this.g.timePassed() + ",duration" + this.g.getDuration();
            scrollTo(this.g.getCurrX(), this.g.getCurrY());
            a(this.g.getCurrX());
        } else if (this.f != -1) {
            "computeScroll, nextScreen:" + this.f;
            this.e = Math.max(0, Math.min(this.f, (((getChildCount() - 1) / this.J) + 1) - 1));
            this.f = -1;
            setChildrenDrawnWithCacheEnabled(false);
            setAlwaysDrawnWithCacheEnabled(false);
            this.v.b(this.e);
            invalidate();
        } else {
            "computeScroll, nextScreen:" + this.f;
        }
    }

    public final ArrayAdapter d() {
        return this.t;
    }

    public final void d(int i2) {
        if (this.g.isFinished()) {
            s();
            int max = Math.max(0, Math.min(i2, (((getChildCount() - 1) / this.J) + 1) - 1));
            boolean z2 = max != this.e;
            this.f = max;
            View focusedChild = getFocusedChild();
            if (focusedChild != null && z2 && focusedChild == getChildAt(this.e)) {
                focusedChild.clearFocus();
            }
            int width = (max * getWidth()) - getScrollX();
            int abs = Math.abs(width) * 2;
            this.g.startScroll(getScrollX(), 0, width, 0, abs > 300 ? 300 : abs);
            this.y.setDuration(abs > 300 ? 300 : (long) abs);
            BaseApp.c();
            this.v.a(this.f, this.y);
            c(this.f + 1);
            invalidate();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public void dispatchDraw(Canvas canvas) {
        int indexOfChild;
        if (this.aG) {
            this.aG = false;
            try {
                super.dispatchDraw(canvas);
            } catch (Exception e2) {
            }
        } else {
            int i2 = this.e;
            boolean z2 = this.k != 1 && this.f == -1 && !this.z;
            long drawingTime = getDrawingTime();
            if (z2) {
                a(canvas, i2, drawingTime);
                View view = this.O;
                if (!(view == null || (indexOfChild = indexOfChild(view)) < 0 || indexOfChild / this.J == i2)) {
                    drawChild(canvas, view, drawingTime);
                }
            } else {
                int i3 = this.f;
                if (i3 >= 0 && i3 < ((getChildCount() - 1) / this.J) + 1 && Math.abs(i2 - i3) == 1) {
                    a(canvas, i2, drawingTime);
                    a(canvas, i3, drawingTime);
                } else if (!this.z) {
                    int width = getWidth();
                    int scrollX = (getScrollX() + (width / 2)) / width;
                    if (scrollX - 1 >= 0) {
                        a(canvas, scrollX - 1, drawingTime);
                    }
                    a(canvas, scrollX, drawingTime);
                    if (scrollX + 1 < ((getChildCount() - 1) / this.J) + 1) {
                        a(canvas, scrollX + 1, drawingTime);
                    }
                } else {
                    int childCount = getChildCount();
                    for (int i4 = 0; i4 < childCount; i4++) {
                        drawChild(canvas, getChildAt(i4), drawingTime);
                    }
                }
            }
        }
        Bitmap bitmap = this.ae;
        if (bitmap != null && !bitmap.isRecycled()) {
            if (this.ao == 1) {
                this.aw = SystemClock.uptimeMillis();
                this.ao = 2;
            }
            if (this.ao == 2) {
                float uptimeMillis = ((float) (SystemClock.uptimeMillis() - this.aw)) / ((float) this.al);
                if (uptimeMillis >= 1.0f) {
                    this.ao = 3;
                }
                float min = (Math.min(uptimeMillis, 1.0f) * (this.ag - this.af)) + this.af;
                canvas.save();
                canvas.scale(min, min, (float) this.ah, (float) this.ai);
                canvas.drawBitmap(bitmap, (float) this.as, (float) this.at, (Paint) null);
                canvas.restore();
                invalidate(this.ad);
            } else {
                if (this.ak && this.aj != null) {
                    this.aj.f();
                    this.aj = null;
                }
                bitmap.recycle();
                this.ae = null;
            }
        } else if (this.ao != 3) {
            this.ao = 3;
        }
        Drawable drawable = this.ac;
        if (drawable != null) {
            canvas.save();
            canvas.translate((float) this.as, (float) this.at);
            drawable.draw(canvas);
            canvas.restore();
        }
        if (this.aq != null) {
            if (this.ar == 1) {
                this.aw = SystemClock.uptimeMillis();
                this.ar = 2;
            }
            if (this.ar == 2) {
                float uptimeMillis2 = ((float) (SystemClock.uptimeMillis() - this.aw)) / 200.0f;
                if (uptimeMillis2 >= 1.0f) {
                    this.ar = 3;
                }
                float min2 = Math.min(uptimeMillis2, 1.0f);
                float f2 = ((float) this.as) + (((float) (this.au - this.as)) * min2);
                float f3 = (min2 * ((float) (this.av - this.at))) + ((float) this.at);
                canvas.save();
                canvas.drawBitmap(this.aq, f2, f3, (Paint) null);
                canvas.restore();
                invalidate();
                return;
            }
            canvas.drawBitmap(this.aq, (float) this.au, (float) this.av, (Paint) null);
            View view2 = this.O;
            this.aq.recycle();
            this.aq = null;
            if (view2 != null) {
                if (this.Q) {
                    if (!this.ap) {
                        view2.clearAnimation();
                        view2.setVisibility(0);
                    } else {
                        ha haVar = (ha) view2.getTag();
                        ArrayAdapter arrayAdapter = this.t;
                        arrayAdapter.setNotifyOnChange(false);
                        arrayAdapter.remove(haVar);
                        arrayAdapter.add(haVar);
                        view2.clearAnimation();
                        haVar.t = getChildCount();
                        if (view2.getParent() != null) {
                            haVar.t = getChildCount() - 1;
                            removeViewInLayout(view2);
                        }
                        addView(view2);
                        view2.setVisibility(0);
                    }
                } else if (!this.ap) {
                    view2.clearAnimation();
                    view2.setVisibility(0);
                } else {
                    ha haVar2 = (ha) view2.getTag();
                    if (view2.getParent() != null) {
                        removeViewInLayout(view2);
                    }
                    addView(view2, haVar2.t, view2.getLayoutParams());
                    view2.setVisibility(0);
                }
                requestLayout();
                invalidate();
                if (this.T) {
                    this.T = false;
                    BaseApp.d().post(new ch(this));
                }
            }
        }
    }

    public boolean dispatchUnhandledMove(View view, int i2) {
        if (i2 == 17) {
            if (this.e > 0) {
                b(this.e - 1);
                return true;
            }
        } else if (i2 == 66 && this.e < (((getChildCount() - 1) / this.J) + 1) - 1) {
            b(this.e + 1);
            return true;
        }
        return super.dispatchUnhandledMove(view, i2);
    }

    public boolean drawChild(Canvas canvas, View view, long j2) {
        if (view == null) {
            return true;
        }
        if (view.getVisibility() != 0 && view.getAnimation() == null) {
            return true;
        }
        boolean drawChild = super.drawChild(canvas, view, j2);
        if (this.aC == null || !this.aC.contains(view)) {
            return drawChild;
        }
        if (this.aD == null) {
            this.aD = BitmapFactory.decodeResource(getResources(), R.drawable.new_app_notify);
        }
        canvas.drawBitmap(this.aD, (float) (view.getRight() - this.aD.getWidth()), (float) view.getTop(), (Paint) null);
        return drawChild;
    }

    public final void e() {
        if (this.N != null) {
            removeView(this.N);
            int i2 = this.e;
            if (!this.g.isFinished()) {
                i2 = this.f;
                this.g.forceFinished(true);
            }
            if (i2 >= ((getChildCount() - 1) / this.J) + 1) {
                this.e = i2 - 1;
                if (this.e >= ((getChildCount() - 1) / this.J) + 1) {
                    this.e = this.a;
                }
                scrollTo(this.e * getWidth(), 0);
                invalidate();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final int f() {
        return ((getChildCount() - 1) / this.J) + 1;
    }

    public final void g() {
        int width = getWidth();
        b((getScrollX() + (width / 2)) / width);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new LayoutParams(layoutParams);
    }

    public final void h() {
        if (this.P && this.O != null) {
            ArrayAdapter arrayAdapter = this.t;
            arrayAdapter.setNotifyOnChange(false);
            removeView(this.O);
            arrayAdapter.remove((ha) this.O.getTag());
            requestLayout();
        }
    }

    public final void i() {
        this.o = false;
        this.x = true;
    }

    public void invalidate() {
        boolean z2 = this.A;
        super.invalidate(getScrollX(), z2 ? this.D : this.F, getScrollX() + getWidth(), getHeight() - (z2 ? this.E : this.G));
    }

    public final void j() {
        this.o = true;
        this.x = false;
    }

    public final void k() {
        if (this.z) {
            this.z = false;
        }
        if (this.d.getVisibility() == 0) {
            this.d.setVisibility(4);
        }
    }

    public final void l() {
        if (this.x) {
            w();
            v();
            if (this.f == -1 && this.e > 0 && this.g.isFinished()) {
                b(this.e - 1);
            }
        }
    }

    public final void m() {
        if (this.x) {
            w();
            v();
            if (this.f == -1 && this.e < (((getChildCount() - 1) / this.J) + 1) - 1 && this.g.isFinished()) {
                b(this.e + 1);
            }
        }
    }

    public final void n() {
        if (this.T) {
            this.T = false;
            BaseApp.d().post(new ch(this));
        }
    }

    public final void o() {
        Intent intent;
        boolean z2;
        ArrayList h2 = ff.h(getContext());
        if (h2 != null && h2.size() != 0) {
            for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
                Object tag = getChildAt(childCount).getTag();
                if (!(tag == null || !(tag instanceof am) || (intent = ((am) tag).c) == null)) {
                    String packageName = intent.getComponent().getPackageName();
                    Iterator it = h2.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            z2 = true;
                            break;
                        } else if (((String) it.next()).equals(packageName)) {
                            z2 = false;
                            if (this.aC == null) {
                                this.aC = new ArrayList();
                            }
                            this.aC.add(getChildAt(childCount));
                        }
                    }
                    if (z2) {
                        return;
                    }
                }
            }
        }
    }

    public void onClick(View view) {
        this.l.onClick(view);
        if (this.aC != null) {
            this.aC.clear();
            this.aC = null;
        }
        if (this.aD != null) {
            this.aD = null;
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.o) {
            return true;
        }
        int action = motionEvent.getAction();
        if (action == 2 && this.k != 0) {
            return true;
        }
        float x2 = motionEvent.getX();
        float y2 = motionEvent.getY();
        switch (action) {
            case 0:
                this.i = x2;
                this.j = y2;
                this.n = true;
                this.k = this.g.isFinished() ? 0 : 1;
                break;
            case 1:
            case 3:
                setChildrenDrawnWithCacheEnabled(false);
                setAlwaysDrawnWithCacheEnabled(false);
                this.k = 0;
                this.n = false;
                break;
            case 2:
                int abs = (int) Math.abs(x2 - this.i);
                int abs2 = (int) Math.abs(y2 - this.j);
                int i2 = this.p;
                boolean z2 = abs > i2;
                boolean z3 = abs2 > i2;
                if (z2 || z3) {
                    if (z2) {
                        this.k = 1;
                        s();
                    }
                    if (this.n) {
                        this.n = false;
                        t();
                        cancelLongPress();
                        break;
                    }
                }
                break;
        }
        return this.k != 0;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        if (this.aG) {
            this.aG = false;
            this.am = 3;
        } else if (this.am == 2) {
            return;
        } else {
            if (!(this.aq == null || this.ar == 3)) {
                return;
            }
        }
        int childCount = getChildCount();
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                int i7 = layoutParams.a;
                int i8 = layoutParams.b;
                childAt.layout(i7, i8, layoutParams.width + i7, layoutParams.height + i8);
                if (childAt.getAnimation() != null) {
                    childAt.clearAnimation();
                }
                ha haVar = (ha) childAt.getTag();
                if (haVar.t != i6) {
                    haVar.t = i6;
                }
            }
        }
        int childCount2 = ((getChildCount() - 1) / this.J) + 1;
        QNavigation qNavigation = this.v;
        if (qNavigation.getChildCount() != childCount2) {
            qNavigation.removeAllViews();
            qNavigation.a(childCount2);
            qNavigation.b(this.e);
            qNavigation.postInvalidate();
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        if (mode == 0 || mode2 == 0) {
            throw new RuntimeException("CellLayout cannot have UNSPECIFIED dimensions");
        }
        int size3 = View.MeasureSpec.getSize(i2);
        int i4 = this.H;
        int i5 = this.I;
        int i6 = this.D;
        int i7 = this.E;
        int i8 = this.F;
        int i9 = this.G;
        int i10 = this.B;
        int i11 = this.C;
        this.A = size2 > size;
        int i12 = i4 - 1;
        int i13 = i5 - 1;
        if (this.A) {
            this.L = (((size2 - i6) - i7) - (i11 * i5)) / i13;
            int i14 = ((size - i8) - i9) - (i10 * i4);
            if (i12 > 0) {
                this.K = i14 / i12;
            } else {
                this.K = 0;
            }
        } else {
            this.K = (((size - i6) - i7) - (i10 * i5)) / i13;
            int i15 = ((size2 - i8) - i9) - (i11 * i4);
            if (i12 > 0) {
                this.L = i15 / i12;
            } else {
                this.L = 0;
            }
        }
        int childCount = getChildCount();
        int i16 = i4 * i5;
        int i17 = 0;
        int i18 = 0;
        while (i18 < childCount) {
            View childAt = getChildAt(i18);
            int i19 = i18 / i16;
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            layoutParams.width = (i10 - layoutParams.leftMargin) - layoutParams.rightMargin;
            layoutParams.height = (i11 - layoutParams.topMargin) - layoutParams.bottomMargin;
            if (this.A) {
                int i20 = i18 % i16;
                layoutParams.a = ((i20 % i4) * (this.K + i10)) + (size3 * i19) + i8 + layoutParams.leftMargin;
                layoutParams.b = ((i20 / i4) * (this.L + i11)) + i6 + layoutParams.topMargin;
            } else {
                int i21 = i18 % i16;
                layoutParams.a = ((i21 % i5) * (this.K + i10)) + (size3 * i19) + i6 + layoutParams.leftMargin;
                layoutParams.b = ((i21 / i5) * (this.L + i11)) + i8 + layoutParams.topMargin;
            }
            childAt.measure(View.MeasureSpec.makeMeasureSpec(layoutParams.width, 1073741824), View.MeasureSpec.makeMeasureSpec(layoutParams.height, 1073741824));
            i18++;
            i17 = i19;
        }
        this.c = (i17 + 1) * size3;
        if (this.b) {
            scrollTo(size3 * this.e, 0);
            this.b = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (savedState.currentScreen != -1) {
            this.e = savedState.currentScreen;
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.currentScreen = this.e;
        return savedState;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.o) {
            return true;
        }
        if (this.h == null) {
            this.h = VelocityTracker.obtain();
        }
        this.h.addMovement(motionEvent);
        int action = motionEvent.getAction();
        float x2 = motionEvent.getX();
        float y2 = motionEvent.getY();
        switch (action) {
            case 0:
                if (!this.g.isFinished()) {
                    this.g.abortAnimation();
                }
                this.i = x2;
                break;
            case 1:
                if (this.k == 1) {
                    VelocityTracker velocityTracker = this.h;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.q);
                    int xVelocity = (int) velocityTracker.getXVelocity();
                    if (xVelocity > 500 && this.e > 0) {
                        b(this.e - 1);
                    } else if (xVelocity >= -500 || this.e >= (((getChildCount() - 1) / this.J) + 1) - 1) {
                        int width = getWidth();
                        b((getScrollX() + (width / 2)) / width);
                    } else {
                        b(this.e + 1);
                    }
                    if (this.h != null) {
                        this.h.recycle();
                        this.h = null;
                    }
                }
                this.k = 0;
                break;
            case 2:
                if (this.k != 1) {
                    int abs = (int) Math.abs(x2 - this.i);
                    int abs2 = (int) Math.abs(y2 - this.j);
                    int i2 = this.p;
                    boolean z2 = abs > i2;
                    boolean z3 = abs2 > i2;
                    if (z2 || z3) {
                        if (z2) {
                            this.k = 1;
                            s();
                        }
                        if (this.n) {
                            this.n = false;
                            t();
                            cancelLongPress();
                            break;
                        }
                    }
                } else {
                    int i3 = (int) (this.i - x2);
                    int width2 = getWidth() / 2;
                    int scrollX = getScrollX();
                    this.i = x2;
                    if (i3 < 0) {
                        if (scrollX < 0) {
                            scrollBy(Math.max((-width2) - scrollX, i3 / 2), 0);
                        } else {
                            scrollBy(Math.max((-width2) - scrollX, i3), 0);
                        }
                    } else if (i3 > 0) {
                        int i4 = this.c;
                        int width3 = (i4 - scrollX) - getWidth();
                        if (scrollX > i4) {
                            scrollBy(Math.min(width2 + width3, i3 / 2), 0);
                        } else {
                            scrollBy(Math.min(width2 + width3, i3), 0);
                        }
                    }
                    a(scrollX);
                    break;
                }
                break;
            case 3:
                this.k = 0;
                break;
        }
        return true;
    }

    public final boolean p() {
        return this.aC != null;
    }

    public final void q() {
        if (this.aC != null) {
            this.aC.clear();
            this.aC = null;
        }
        if (this.aD != null) {
            this.aD = null;
        }
    }

    public final void r() {
        this.t.notifyDataSetChanged();
        c();
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z2) {
        int indexOfChild = indexOfChild(view) / this.J;
        if (indexOfChild == this.e && this.g.isFinished()) {
            return false;
        }
        b(indexOfChild);
        return true;
    }

    public void setVisibility(int i2) {
        super.setVisibility(i2);
        if (i2 != 0) {
            if (this.aC != null) {
                this.aC.clear();
                this.aC = null;
            }
            if (this.aD != null) {
                this.aD = null;
            }
        }
    }
}
