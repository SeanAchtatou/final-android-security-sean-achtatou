package com.tencent.launcher;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.qqlauncher.R;

public class VerticalAppLayout extends RelativeLayout implements m {
    private AllAppsListView a;
    private ArrayAdapter b;
    private LetterListView c;
    private TextView d;
    private DrawerTextView e;

    public VerticalAppLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public final void a() {
        this.d.setVisibility(8);
    }

    public final void a(ArrayAdapter arrayAdapter) {
        this.b = arrayAdapter;
        this.a.setAdapter((ListAdapter) arrayAdapter);
    }

    public final void a(Launcher launcher) {
        if (this.a != null) {
            this.a.a(launcher);
        }
    }

    public final void a(dt dtVar) {
        this.a.a(dtVar);
    }

    public final void a(String str) {
        this.d.setVisibility(0);
        this.d.setText(str);
        this.a.a(str);
    }

    public final TextView b() {
        return this.e;
    }

    public final void b(String str) {
        this.d.setText(str);
        this.a.a(str);
    }

    public final ArrayAdapter c() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
    }

    /* access modifiers changed from: protected */
    public final void onFinishInflate() {
        super.onFinishInflate();
        this.a = (AllAppsListView) findViewById(R.id.applistview);
        this.c = (LetterListView) findViewById(R.id.MyLetterListView01);
        this.d = (TextView) findViewById(R.id.TextView_CenterChar);
        this.c.a(this);
        this.e = (DrawerTextView) findViewById(R.id.drag_cache_view);
    }
}
