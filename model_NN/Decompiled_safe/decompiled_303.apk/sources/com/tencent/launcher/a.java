package com.tencent.launcher;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PaintDrawable;
import com.tencent.launcher.base.b;
import com.tencent.module.theme.l;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

public final class a {
    private static int a = -1;
    private static int b = -1;
    private static int c = -1;
    private static int d = -1;
    private static int e = -1;
    private static int f = -1;
    private static int g = -1;
    private static int h = -1;
    private static int i = -1;
    private static int j = -1;
    private static int k = -1;
    private static final Paint l = new Paint();
    private static final Rect m = new Rect();
    private static final Rect n = new Rect();
    private static Canvas o = new Canvas();
    private static final Matrix p = new Matrix();
    private static final Paint q = new Paint();

    static {
        o.setDrawFilter(new PaintFlagsDrawFilter(4, 2));
        q.setAlpha(30);
    }

    public static int a(Bitmap bitmap) {
        if (bitmap == null) {
            return 0;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int[] iArr = new int[(width * height)];
        bitmap.getPixels(iArr, 0, width, 0, 0, width, height);
        int i2 = -1;
        for (int i3 = 0; i3 < height; i3++) {
            int i4 = 0;
            while (true) {
                if (i4 < width) {
                    if (((iArr[(i3 * width) + i4] >> 24) & BrightnessActivity.MAXIMUM_BACKLIGHT) != 0) {
                        i2 = -1;
                        break;
                    }
                    i4++;
                } else if (i2 == -1) {
                    i2 = i3;
                }
            }
        }
        return i2 == -1 ? height : i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.a.a(android.content.Context, android.graphics.drawable.Drawable, com.tencent.launcher.ha, boolean):android.graphics.drawable.Drawable
     arg types: [android.content.Context, android.graphics.drawable.Drawable, ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.launcher.a.a(android.graphics.Bitmap, int, int, android.content.Context):android.graphics.Bitmap
      com.tencent.launcher.a.a(android.content.Context, android.graphics.drawable.Drawable, android.graphics.drawable.Drawable, com.tencent.launcher.ha):android.graphics.drawable.Drawable
      com.tencent.launcher.a.a(android.content.Context, android.graphics.drawable.Drawable, com.tencent.launcher.ha, boolean):android.graphics.drawable.Drawable */
    public static Bitmap a(Context context, Drawable drawable) {
        int intrinsicWidth;
        int intrinsicHeight;
        gb gbVar = (gb) a(context, drawable, (ha) null, false);
        Rect e2 = gbVar.e();
        if (e2 != null) {
            intrinsicWidth = e2.width();
            intrinsicHeight = e2.height();
        } else {
            Rect b2 = gbVar.b();
            if (b2 != null) {
                intrinsicWidth = b2.width();
                intrinsicHeight = b2.height();
            } else {
                intrinsicWidth = gbVar.getIntrinsicWidth();
                intrinsicHeight = gbVar.getIntrinsicHeight();
            }
        }
        Bitmap createBitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, gbVar.getOpacity() != -1 ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(createBitmap);
        gbVar.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
        gbVar.draw(canvas);
        return createBitmap;
    }

    static Bitmap a(Bitmap bitmap, int i2, int i3, Context context) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width >= i2 && height >= i3) {
            return bitmap;
        }
        int color = context.getResources().getColor(R.color.window_background);
        Bitmap createBitmap = Bitmap.createBitmap(width < i2 ? i2 : width, height < i3 ? i3 : height, Bitmap.Config.RGB_565);
        createBitmap.setDensity(bitmap.getDensity());
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawColor(color);
        canvas.drawBitmap(bitmap, ((float) (i2 - width)) / 2.0f, ((float) (i3 - height)) / 2.0f, (Paint) null);
        return createBitmap;
    }

    static Bitmap a(Bitmap bitmap, Context context) {
        int i2;
        int i3;
        if (e == -1) {
            int dimension = (int) context.getResources().getDimension(R.dimen.icon_drawer_size);
            f = dimension;
            e = dimension;
        }
        int i4 = e;
        int i5 = f;
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (i4 > 0 && i5 > 0) {
            if (i4 < width || i5 < height) {
                float f2 = ((float) width) / ((float) height);
                if (width > height) {
                    i2 = i4;
                    i3 = (int) (((float) i4) / f2);
                } else if (height > width) {
                    int i6 = i5;
                    i2 = (int) (((float) i5) * f2);
                    i3 = i6;
                } else {
                    int i7 = i5;
                    i2 = i4;
                    i3 = i7;
                }
                Bitmap.Config config = (i2 == e && i3 == f) ? bitmap.getConfig() : Bitmap.Config.ARGB_8888;
                if (config == null) {
                    config = Bitmap.Config.ARGB_8888;
                }
                Bitmap createBitmap = Bitmap.createBitmap(e, f, config);
                Canvas canvas = o;
                Paint paint = l;
                canvas.setBitmap(createBitmap);
                paint.setDither(false);
                paint.setFilterBitmap(true);
                m.set((e - i2) / 2, (f - i3) / 2, i2, i3);
                n.set(0, 0, width, height);
                canvas.drawBitmap(bitmap, n, m, paint);
                return createBitmap;
            } else if (width < i4 || height < i5) {
                Bitmap createBitmap2 = Bitmap.createBitmap(e, f, Bitmap.Config.ARGB_8888);
                Canvas canvas2 = o;
                Paint paint2 = l;
                canvas2.setBitmap(createBitmap2);
                paint2.setDither(false);
                paint2.setFilterBitmap(true);
                canvas2.drawBitmap(bitmap, (float) ((e - width) / 2), (float) ((f - height) / 2), paint2);
                return createBitmap2;
            }
        }
        return bitmap;
    }

    public static Bitmap a(Drawable drawable) {
        if (drawable == null) {
            return null;
        }
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }
        if (drawable instanceof hp) {
            return ((hp) drawable).a();
        }
        if (drawable instanceof gm) {
            return ((gm) drawable).a();
        }
        if (drawable instanceof gb) {
            return ((gb) drawable).a();
        }
        return null;
    }

    public static Rect a(Context context, int i2) {
        int dimension = (int) context.getResources().getDimension(R.dimen.folder_icon_paddingleft);
        int dimension2 = (int) context.getResources().getDimension(R.dimen.folder_icon_paddingtop);
        int dimension3 = (int) context.getResources().getDimension(R.dimen.folder_icon_space_h);
        int dimension4 = (int) context.getResources().getDimension(R.dimen.folder_icon_space_v);
        int dimension5 = (int) context.getResources().getDimension(R.dimen.icon_folder_size);
        int dimension6 = ((dimension5 - dimension) - ((int) context.getResources().getDimension(R.dimen.folder_icon_paddingright))) / 2;
        int dimension7 = ((dimension5 - dimension2) - ((int) context.getResources().getDimension(R.dimen.folder_icon_paddingbottom))) / 2;
        Rect rect = new Rect();
        int i3 = dimension + ((i2 % 2) * dimension6) + dimension3;
        int i4 = dimension2 + ((i2 / 2) * dimension7) + dimension4;
        rect.set(i3, i4, (dimension6 + i3) - dimension3, (dimension7 + i4) - dimension4);
        return rect;
    }

    public static Drawable a(Context context, Drawable drawable, Drawable drawable2, Drawable drawable3, ha haVar) {
        if (j == -1) {
            Resources resources = context.getResources();
            j = (int) resources.getDimension(R.dimen.icon_folder_size);
            g = (int) resources.getDimension(R.dimen.icon_out_size);
            h = (int) resources.getDimension(R.dimen.icon_in_size);
        }
        if (!(drawable instanceof gb) || ((gb) drawable).d() != 4) {
            gb b2 = new dr().a(drawable2).b(drawable3).c(drawable).b(j).a(g).c(4).a().b();
            if (!l.a().d()) {
                b2.a((Bitmap) null);
                b2.a(drawable3);
                b2.a(b.k, b.k);
                b2.a(-1);
                return b2;
            }
            Rect b3 = b2.b();
            b3.offsetTo(b3.left, (h - j) / 2);
            b2.c(b2.b());
            return b2;
        }
        if ((drawable instanceof gb) && (haVar instanceof am) && !l.a().d()) {
            gb gbVar = (gb) drawable;
            gbVar.a(l.a().a(haVar));
            gbVar.b(l.a().e());
            Drawable a2 = l.a().a((am) haVar);
            if (a2 != null) {
                gbVar.c(a2);
                gbVar.a((Bitmap) null);
                gbVar.b((Bitmap) null);
            }
        }
        return drawable;
    }

    public static Drawable a(Context context, Drawable drawable, Drawable drawable2, ha haVar) {
        if (h == -1) {
            Resources resources = context.getResources();
            h = (int) resources.getDimension(R.dimen.icon_in_size);
            g = (int) resources.getDimension(R.dimen.icon_out_size);
        }
        if (!(drawable instanceof gb) || ((gb) drawable).d() != 1) {
            gb b2 = new dr().a(drawable2).c(drawable).b(h).a(g).a().c(1).b();
            if (!l.a().d()) {
                b2.a(l.a().a(haVar));
                b2.b(l.a().e());
                Drawable a2 = haVar instanceof am ? l.a().a((am) haVar) : null;
                if (a2 != null) {
                    b2.c(a2);
                    b2.a((Bitmap) null);
                    b2.b((Bitmap) null);
                    b2.a(b.k, b.k);
                } else {
                    b2.a(b.k, b.l);
                }
                b2.a(-1);
                return b2;
            }
            Rect b3 = b2.b();
            b3.offsetTo(b3.left, 0);
            return b2;
        }
        if ((drawable instanceof gb) && (haVar instanceof am) && !l.a().d()) {
            gb gbVar = (gb) drawable;
            gbVar.a(l.a().a(haVar));
            gbVar.b(l.a().e());
            Drawable a3 = l.a().a((am) haVar);
            if (a3 != null) {
                gbVar.c(a3);
                gbVar.a((Bitmap) null);
                gbVar.b((Bitmap) null);
                gbVar.a(b.k, b.k);
                return drawable;
            }
            gbVar.a(b.k, b.l);
        }
        return drawable;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.a.a(android.content.Context, android.graphics.drawable.Drawable, com.tencent.launcher.ha, boolean):android.graphics.drawable.Drawable
     arg types: [android.content.Context, android.graphics.drawable.Drawable, com.tencent.launcher.ha, int]
     candidates:
      com.tencent.launcher.a.a(android.graphics.Bitmap, int, int, android.content.Context):android.graphics.Bitmap
      com.tencent.launcher.a.a(android.content.Context, android.graphics.drawable.Drawable, android.graphics.drawable.Drawable, com.tencent.launcher.ha):android.graphics.drawable.Drawable
      com.tencent.launcher.a.a(android.content.Context, android.graphics.drawable.Drawable, com.tencent.launcher.ha, boolean):android.graphics.drawable.Drawable */
    public static Drawable a(Context context, Drawable drawable, ha haVar) {
        return a(context, drawable, haVar, false);
    }

    public static Drawable a(Context context, Drawable drawable, ha haVar, boolean z) {
        if (h == -1) {
            Resources resources = context.getResources();
            h = (int) resources.getDimension(R.dimen.icon_in_size);
            g = (int) resources.getDimension(R.dimen.icon_out_size);
        }
        if (z || !(drawable instanceof gb) || ((gb) drawable).d() != 0) {
            gb b2 = new dr().c(drawable).b(h).a(g).c(0).b();
            gb gbVar = b2;
            if (l.a().d()) {
                return b2;
            }
            gbVar.a(l.a().a(haVar));
            gbVar.b(l.a().e());
            Drawable a2 = l.a().a((am) haVar);
            if (a2 != null) {
                gbVar.c(a2);
                gbVar.a((Bitmap) null);
                gbVar.b((Bitmap) null);
                gbVar.a(b.k, b.k);
                return b2;
            }
            gbVar.a(b.k, b.l);
            return b2;
        }
        if ((drawable instanceof gb) && (haVar instanceof am) && !l.a().d()) {
            gb gbVar2 = (gb) drawable;
            gbVar2.a(l.a().a(haVar));
            gbVar2.b(l.a().e());
            Drawable a3 = l.a().a((am) haVar);
            if (a3 != null) {
                gbVar2.c(a3);
                gbVar2.a((Bitmap) null);
                gbVar2.b((Bitmap) null);
                gbVar2.a(b.k, b.k);
                return drawable;
            }
            gbVar2.a(b.k, b.l);
        }
        return drawable;
    }

    public static Drawable a(Drawable drawable, Context context) {
        int i2;
        if (e == -1) {
            int dimension = (int) context.getResources().getDimension(R.dimen.icon_drawer_size);
            f = dimension;
            e = dimension;
        }
        int i3 = e;
        int i4 = f;
        if (drawable instanceof PaintDrawable) {
            PaintDrawable paintDrawable = (PaintDrawable) drawable;
            paintDrawable.setIntrinsicWidth(i3);
            paintDrawable.setIntrinsicHeight(i4);
        } else if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap().getDensity() == 0) {
                bitmapDrawable.setTargetDensity(context.getResources().getDisplayMetrics());
            }
        }
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = drawable.getIntrinsicHeight();
        if (i3 > 0 && i4 > 0) {
            if (i3 < intrinsicWidth || i4 < intrinsicHeight) {
                float f2 = ((float) intrinsicWidth) / ((float) intrinsicHeight);
                if (intrinsicWidth > intrinsicHeight) {
                    i2 = (int) (((float) i3) / f2);
                } else if (intrinsicHeight > intrinsicWidth) {
                    i3 = (int) (((float) i4) * f2);
                    i2 = i4;
                } else {
                    i2 = i4;
                }
                Bitmap createBitmap = Bitmap.createBitmap(e, f, drawable.getOpacity() != -1 ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
                Canvas canvas = o;
                canvas.setBitmap(createBitmap);
                n.set(drawable.getBounds());
                int i5 = (e - i3) / 2;
                int i6 = (f - i2) / 2;
                drawable.setBounds(i5, i6, i3 + i5, i2 + i6);
                drawable.draw(canvas);
                drawable.setBounds(n);
                return new hp(createBitmap);
            } else if (intrinsicWidth <= i3 && intrinsicHeight <= i4) {
                try {
                    Bitmap createBitmap2 = Bitmap.createBitmap(e, f, Bitmap.Config.ARGB_8888);
                    Canvas canvas2 = o;
                    canvas2.setBitmap(createBitmap2);
                    n.set(drawable.getBounds());
                    int i7 = (i3 - intrinsicWidth) / 2;
                    int i8 = (i4 - intrinsicHeight) / 2;
                    drawable.setBounds(i7, i8, intrinsicWidth + i7, intrinsicHeight + i8);
                    drawable.draw(canvas2);
                    drawable.setBounds(n);
                    return new hp(createBitmap2);
                } catch (OutOfMemoryError e2) {
                    return null;
                }
            }
        }
        return drawable;
    }

    public static Drawable a(Drawable drawable, Context context, int i2) {
        int i3;
        int i4;
        if (drawable instanceof PaintDrawable) {
            PaintDrawable paintDrawable = (PaintDrawable) drawable;
            paintDrawable.setIntrinsicWidth(i2);
            paintDrawable.setIntrinsicHeight(i2);
        } else if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap().getDensity() == 0) {
                bitmapDrawable.setTargetDensity(context.getResources().getDisplayMetrics());
            }
        }
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = drawable.getIntrinsicHeight();
        if (i2 > 0 && i2 > 0) {
            if (i2 < intrinsicWidth || i2 < intrinsicHeight) {
                float f2 = ((float) intrinsicWidth) / ((float) intrinsicHeight);
                if (intrinsicWidth > intrinsicHeight) {
                    i3 = (int) (((float) i2) / f2);
                    i4 = i2;
                } else if (intrinsicHeight > intrinsicWidth) {
                    i4 = (int) (((float) i2) * f2);
                    i3 = i2;
                } else {
                    i3 = i2;
                    i4 = i2;
                }
                Bitmap createBitmap = Bitmap.createBitmap(i2, i2, drawable.getOpacity() != -1 ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
                Canvas canvas = o;
                canvas.setBitmap(createBitmap);
                n.set(drawable.getBounds());
                drawable.setBounds(0, 0, i4, i3);
                drawable.draw(canvas);
                drawable.setBounds(n);
                return new hp(createBitmap);
            } else if (intrinsicWidth < i2 && intrinsicHeight < i2) {
                Bitmap createBitmap2 = Bitmap.createBitmap(i2, i2, Bitmap.Config.ARGB_8888);
                Canvas canvas2 = o;
                canvas2.setBitmap(createBitmap2);
                n.set(drawable.getBounds());
                int i5 = (i2 - intrinsicWidth) / 2;
                int i6 = (i2 - intrinsicHeight) / 2;
                drawable.setBounds(i5, i6, intrinsicWidth + i5, intrinsicHeight + i6);
                drawable.draw(canvas2);
                drawable.setBounds(n);
                return new hp(createBitmap2);
            }
        }
        return drawable;
    }

    public static Drawable a(ArrayList arrayList, Drawable drawable, Context context) {
        int dimension = (int) context.getResources().getDimension(R.dimen.folder_icon_paddingleft);
        int dimension2 = (int) context.getResources().getDimension(R.dimen.folder_icon_paddingtop);
        int dimension3 = (int) context.getResources().getDimension(R.dimen.folder_icon_paddingright);
        int dimension4 = (int) context.getResources().getDimension(R.dimen.folder_icon_paddingbottom);
        int dimension5 = (int) context.getResources().getDimension(R.dimen.folder_icon_space_h);
        int dimension6 = (int) context.getResources().getDimension(R.dimen.folder_icon_space_v);
        int size = arrayList.size();
        int i2 = size >= 4 ? 3 : size - 1;
        Bitmap a2 = a(drawable);
        int dimension7 = (int) context.getResources().getDimension(R.dimen.icon_dock_folder_size);
        int i3 = ((dimension7 - dimension) - dimension3) / 2;
        int i4 = ((dimension7 - dimension2) - dimension4) / 2;
        Bitmap createBitmap = Bitmap.createBitmap(dimension7, dimension7, Bitmap.Config.ARGB_8888);
        Canvas canvas = o;
        canvas.setBitmap(createBitmap);
        canvas.setDrawFilter(new PaintFlagsDrawFilter(0, 3));
        Rect rect = new Rect();
        rect.set(0, 0, dimension7, dimension7);
        canvas.drawBitmap(a2, (Rect) null, rect, (Paint) null);
        int i5 = 0;
        while (true) {
            int i6 = i5;
            if (i6 > i2) {
                return new hp(createBitmap);
            }
            Bitmap a3 = a((Drawable) arrayList.get(i6));
            int i7 = i6 - 0;
            int i8 = ((i7 % 2) * i3) + dimension + dimension5;
            int i9 = ((i7 / 2) * i4) + dimension2 + dimension6;
            rect.set(i8, i9, (i8 + i3) - dimension5, (i9 + i4) - dimension6);
            canvas.drawBitmap(a3, (Rect) null, rect, (Paint) null);
            i5 = i6 + 1;
        }
    }

    public static Bitmap b(Drawable drawable) {
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = drawable.getIntrinsicHeight();
        Bitmap createBitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, drawable.getOpacity() != -1 ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(createBitmap);
        drawable.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
        drawable.draw(canvas);
        return createBitmap;
    }

    public static Drawable b(Context context, Drawable drawable) {
        if (h == -1) {
            Resources resources = context.getResources();
            h = (int) resources.getDimension(R.dimen.icon_in_size);
            g = (int) resources.getDimension(R.dimen.icon_out_size);
        }
        if (!(drawable instanceof gb) || ((gb) drawable).d() != 0) {
            return new dr().c(drawable).b(h).a(g).c(0).b();
        }
        if (drawable instanceof gb) {
            gb gbVar = (gb) drawable;
            gbVar.a(l.a().a((String) null));
            gbVar.b(l.a().e());
        }
        return drawable;
    }

    public static Drawable b(Context context, Drawable drawable, Drawable drawable2, ha haVar) {
        if (k == -1) {
            k = (int) context.getResources().getDimension(R.dimen.icon_dock_folder_size);
        }
        if (!(drawable instanceof gb) || ((gb) drawable).d() != 5) {
            gb b2 = new dr().b(drawable2).c(drawable).b(k).a(k).c(5).b();
            if (l.a().d()) {
                return b2;
            }
            b2.a((Bitmap) null);
            b2.a(drawable2);
            b2.a(b.j, b.j);
            return b2;
        }
        if ((drawable instanceof gb) && (haVar instanceof am) && !l.a().d()) {
            gb gbVar = (gb) drawable;
            gbVar.a((Bitmap) null);
            gbVar.a(drawable2);
            gbVar.a(b.j, b.j);
        }
        return drawable;
    }

    public static Drawable b(Context context, Drawable drawable, ha haVar) {
        if (i == -1) {
            i = (int) context.getResources().getDimension(R.dimen.dock_icon_size);
        }
        gb b2 = (!(drawable instanceof gb) || ((gb) drawable).d() != 2) ? new dr().c(drawable).b(i).a(i).c(2).b() : drawable;
        gb gbVar = b2;
        if (!l.a().d()) {
            gbVar.a(l.a().a(haVar));
            gbVar.b(l.a().e());
            Drawable a2 = l.a().a(haVar instanceof am ? (am) haVar : null);
            if (a2 != null) {
                gbVar.c(a2);
                gbVar.a((Bitmap) null);
                gbVar.b((Bitmap) null);
                gbVar.a(b.m, b.m);
            } else {
                gbVar.a(b.m, b.n);
            }
        }
        return b2;
    }

    public static Drawable b(Drawable drawable, Context context) {
        boolean z;
        int i2;
        int i3;
        if (e == -1 || a == -1) {
            Resources resources = context.getResources();
            int dimension = (int) resources.getDimension(R.dimen.icon_out_size);
            b = dimension;
            a = dimension;
            int dimension2 = (int) resources.getDimension(R.dimen.icon_drawer_size);
            f = dimension2;
            e = dimension2;
        }
        int i4 = e;
        int i5 = f;
        if (drawable instanceof PaintDrawable) {
            PaintDrawable paintDrawable = (PaintDrawable) drawable;
            paintDrawable.setIntrinsicWidth(i4);
            paintDrawable.setIntrinsicHeight(i5);
            z = false;
        } else if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap().getDensity() == 0) {
                bitmapDrawable.setTargetDensity(context.getResources().getDisplayMetrics());
            }
            z = false;
        } else {
            z = drawable instanceof hp;
        }
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = drawable.getIntrinsicHeight();
        if (i4 > 0 && i5 > 0) {
            if (i4 < intrinsicWidth || i5 < intrinsicHeight) {
                float f2 = ((float) intrinsicWidth) / ((float) intrinsicHeight);
                if (intrinsicWidth > intrinsicHeight) {
                    i2 = i4;
                    i3 = (int) (((float) i4) / f2);
                } else if (intrinsicHeight > intrinsicWidth) {
                    int i6 = i5;
                    i2 = (int) (((float) i5) * f2);
                    i3 = i6;
                } else {
                    int i7 = i5;
                    i2 = i4;
                    i3 = i7;
                }
                Bitmap createBitmap = Bitmap.createBitmap(a, b, drawable.getOpacity() != -1 ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
                Canvas canvas = o;
                canvas.setBitmap(createBitmap);
                int i8 = (a - i2) / 2;
                int i9 = (b - i3) / 2;
                if (z) {
                    Rect rect = m;
                    rect.set(i8, i9, i2 + i8, i3 + i9);
                    canvas.drawBitmap(((hp) drawable).a(), (Rect) null, rect, (Paint) null);
                } else {
                    n.set(drawable.getBounds());
                    drawable.setBounds(i8, i9, i8 + i2, i3 + i9);
                    drawable.draw(canvas);
                    drawable.setBounds(n);
                }
                return new hp(createBitmap);
            } else if (intrinsicWidth <= i4 && intrinsicHeight <= i5) {
                Bitmap createBitmap2 = Bitmap.createBitmap(a, b, Bitmap.Config.ARGB_8888);
                Canvas canvas2 = o;
                canvas2.setBitmap(createBitmap2);
                int i10 = (a - intrinsicWidth) / 2;
                int i11 = (b - intrinsicHeight) / 2;
                if (z) {
                    Rect rect2 = m;
                    rect2.set(i10, i11, intrinsicWidth + i10, intrinsicHeight + i11);
                    canvas2.drawBitmap(((hp) drawable).a(), (Rect) null, rect2, (Paint) null);
                } else {
                    n.set(drawable.getBounds());
                    drawable.setBounds(i10, i11, i10 + intrinsicWidth, i11 + intrinsicHeight);
                    drawable.draw(canvas2);
                    drawable.setBounds(n);
                }
                return new hp(createBitmap2);
            }
        }
        return drawable;
    }

    public static Drawable b(ArrayList arrayList, Drawable drawable, Context context) {
        int dimension = (int) context.getResources().getDimension(R.dimen.folder_icon_paddingleft);
        int dimension2 = (int) context.getResources().getDimension(R.dimen.folder_icon_paddingtop);
        int dimension3 = (int) context.getResources().getDimension(R.dimen.folder_icon_paddingright);
        int dimension4 = (int) context.getResources().getDimension(R.dimen.folder_icon_paddingbottom);
        int dimension5 = (int) context.getResources().getDimension(R.dimen.folder_icon_space_h);
        int dimension6 = (int) context.getResources().getDimension(R.dimen.folder_icon_space_v);
        int size = arrayList.size();
        int i2 = size >= 4 ? 3 : size - 1;
        Bitmap a2 = a(drawable);
        int dimension7 = (int) context.getResources().getDimension(R.dimen.icon_folder_size);
        int i3 = ((dimension7 - dimension) - dimension3) / 2;
        int i4 = ((dimension7 - dimension2) - dimension4) / 2;
        Bitmap createBitmap = Bitmap.createBitmap(dimension7, dimension7, Bitmap.Config.ARGB_8888);
        Canvas canvas = o;
        canvas.setBitmap(createBitmap);
        canvas.setDrawFilter(new PaintFlagsDrawFilter(0, 3));
        Rect rect = new Rect();
        rect.set(0, 0, dimension7, dimension7);
        canvas.drawBitmap(a2, (Rect) null, rect, (Paint) null);
        int i5 = 0;
        while (true) {
            int i6 = i5;
            if (i6 > i2) {
                return new hp(createBitmap);
            }
            Bitmap a3 = a((Drawable) arrayList.get(i6));
            int i7 = i6 - 0;
            int i8 = ((i7 % 2) * i3) + dimension + dimension5;
            int i9 = ((i7 / 2) * i4) + dimension2 + dimension6;
            rect.set(i8, i9, (i8 + i3) - dimension5, (i9 + i4) - dimension6);
            canvas.drawBitmap(a3, (Rect) null, rect, (Paint) null);
            i5 = i6 + 1;
        }
    }
}
