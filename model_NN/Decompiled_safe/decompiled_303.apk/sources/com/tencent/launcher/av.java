package com.tencent.launcher;

import android.os.Handler;
import android.os.Message;
import android.text.SpannableStringBuilder;
import android.text.style.BackgroundColorSpan;
import java.util.List;

final class av extends Handler {
    private /* synthetic */ SearchAppActivityReserved a;

    av(SearchAppActivityReserved searchAppActivityReserved) {
        this.a = searchAppActivityReserved;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case 900:
                this.a.dismissProgressDialog();
                return;
            case 1003:
                this.a.onReceiveHotWords((List) message.obj);
                break;
            case 1005:
                Object[] objArr = (Object[]) message.obj;
                List list = (List) objArr[0];
                int unused = this.a.totalCount = ((Integer) objArr[2]).intValue();
                if (list.size() == 0) {
                    this.a.mNetSearchListView.removeFooterView(this.a.mWaittingTips);
                    boolean unused2 = this.a.bAllHttpMsgReceived = true;
                }
                for (int i = 0; i < this.a.totalCount; i++) {
                    this.a.mNetList.add(list.get(i));
                }
                this.a.netsearchListAdapter.notifyDataSetChanged();
                SearchAppActivityReserved.access$1912(this.a, list.size());
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(this.a.nAppCount + "条关于" + this.a.mSearchEditText.getText().toString() + "的搜索结果");
                String valueOf = String.valueOf(this.a.nAppCount);
                spannableStringBuilder.setSpan(new BackgroundColorSpan(-65536), valueOf.length() + 3, valueOf.length() + 3 + this.a.mSearchEditText.getText().toString().length(), 33);
                this.a.netsearchTips.setText(spannableStringBuilder);
                break;
            case 2200:
                break;
            default:
                return;
        }
        this.a.dismissProgressDialog();
    }
}
