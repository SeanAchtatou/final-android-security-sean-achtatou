package com.tencent.launcher;

import android.view.animation.Animation;
import android.view.animation.Transformation;

final class dk extends Animation {
    private /* synthetic */ Search a;

    /* synthetic */ dk(Search search) {
        this(search, (byte) 0);
    }

    private dk(Search search, byte b) {
        this.a = search;
    }

    /* access modifiers changed from: protected */
    public final void applyTransformation(float f, Transformation transformation) {
        transformation.getMatrix().setTranslate(((float) (-this.a.getLeft())) * f, ((float) (-Search.c(this.a))) * f);
    }
}
