package com.tencent.launcher;

import android.view.View;
import android.widget.AdapterView;

final class fp implements AdapterView.OnItemClickListener {
    private /* synthetic */ SearchAppActivity a;

    fp(SearchAppActivity searchAppActivity) {
        this.a = searchAppActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (this.a.listAdapter.getCount() - 1 >= i) {
            Launcher.getLauncher().startActivitySafely(((am) this.a.listAdapter.getItem(i)).c);
        }
    }
}
