package com.tencent.launcher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import com.tencent.qqlauncher.R;

public class InstallShortcutReceiver extends BroadcastReceiver {
    private final int[] a = new int[2];

    private boolean a(Context context, Intent intent, int i) {
        String stringExtra = intent.getStringExtra("android.intent.extra.shortcut.NAME");
        if (a(context, this.a, i)) {
            bs bsVar = new bs();
            bsVar.b = this.a[0];
            bsVar.c = this.a[1];
            bsVar.f = i;
            Intent intent2 = (Intent) intent.getParcelableExtra("android.intent.extra.shortcut.INTENT");
            if (intent2.getAction() == null) {
                intent2.setAction("android.intent.action.VIEW");
            }
            if (intent.getBooleanExtra("duplicate", true) || !ff.a(context, stringExtra, intent2)) {
                Launcher.addShortcut(context, intent, bsVar, true);
                Toast.makeText(context, context.getString(R.string.shortcut_installed, stringExtra), 0).show();
            } else {
                Toast.makeText(context, context.getString(R.string.shortcut_duplicate, stringExtra), 0).show();
            }
            return true;
        }
        Toast.makeText(context, context.getString(R.string.out_of_space), 0).show();
        return false;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: boolean[][]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean a(android.content.Context r13, int[] r14, int r15) {
        /*
            r1 = 4
            r2 = 4
            int[] r1 = new int[]{r1, r2}
            java.lang.Class r2 = java.lang.Boolean.TYPE
            java.lang.Object r1 = java.lang.reflect.Array.newInstance(r2, r1)
            r0 = r1
            boolean[][] r0 = (boolean[][]) r0
            r7 = r0
            android.content.ContentResolver r1 = r13.getContentResolver()
            android.net.Uri r2 = com.tencent.launcher.ba.a
            r13 = 4
            java.lang.String[] r3 = new java.lang.String[r13]
            r13 = 0
            java.lang.String r4 = "cellX"
            r3[r13] = r4
            r13 = 1
            java.lang.String r4 = "cellY"
            r3[r13] = r4
            r13 = 2
            java.lang.String r4 = "spanX"
            r3[r13] = r4
            r13 = 3
            java.lang.String r4 = "spanY"
            r3[r13] = r4
            java.lang.String r4 = "screen=?"
            r13 = 1
            java.lang.String[] r5 = new java.lang.String[r13]
            r13 = 0
            java.lang.String r15 = java.lang.String.valueOf(r15)
            r5[r13] = r15
            r6 = 0
            android.database.Cursor r13 = r1.query(r2, r3, r4, r5, r6)
            java.lang.String r15 = "cellX"
            int r15 = r13.getColumnIndexOrThrow(r15)
            java.lang.String r1 = "cellY"
            int r1 = r13.getColumnIndexOrThrow(r1)
            java.lang.String r2 = "spanX"
            int r2 = r13.getColumnIndexOrThrow(r2)
            java.lang.String r3 = "spanY"
            int r3 = r13.getColumnIndexOrThrow(r3)
        L_0x0056:
            boolean r4 = r13.moveToNext()     // Catch:{ Exception -> 0x00bd, all -> 0x00c3 }
            if (r4 == 0) goto L_0x0087
            int r4 = r13.getInt(r15)     // Catch:{ Exception -> 0x00bd, all -> 0x00c3 }
            int r5 = r13.getInt(r1)     // Catch:{ Exception -> 0x00bd, all -> 0x00c3 }
            int r6 = r13.getInt(r2)     // Catch:{ Exception -> 0x00bd, all -> 0x00c3 }
            int r8 = r13.getInt(r3)     // Catch:{ Exception -> 0x00bd, all -> 0x00c3 }
            r9 = r4
        L_0x006d:
            int r10 = r4 + r6
            if (r9 >= r10) goto L_0x0056
            r10 = 4
            if (r9 >= r10) goto L_0x0056
            r10 = r5
        L_0x0075:
            int r11 = r5 + r8
            if (r10 >= r11) goto L_0x0084
            r11 = 4
            if (r10 >= r11) goto L_0x0084
            r11 = r7[r9]     // Catch:{ Exception -> 0x00bd, all -> 0x00c3 }
            r12 = 1
            r11[r10] = r12     // Catch:{ Exception -> 0x00bd, all -> 0x00c3 }
            int r10 = r10 + 1
            goto L_0x0075
        L_0x0084:
            int r9 = r9 + 1
            goto L_0x006d
        L_0x0087:
            r13.close()
            r13 = 0
        L_0x008b:
            r15 = 4
            if (r13 >= r15) goto L_0x00e1
            r15 = 0
        L_0x008f:
            r1 = 4
            if (r15 >= r1) goto L_0x00de
            r1 = r7[r13]
            boolean r1 = r1[r15]
            if (r1 != 0) goto L_0x00c8
            r1 = 1
        L_0x0099:
            r2 = r1
            r1 = r13
        L_0x009b:
            int r3 = r13 + 1
            r4 = 1
            int r3 = r3 - r4
            if (r1 >= r3) goto L_0x00e3
            r3 = 4
            if (r13 >= r3) goto L_0x00e3
            r3 = r2
            r2 = r15
        L_0x00a6:
            int r4 = r15 + 1
            r5 = 1
            int r4 = r4 - r5
            if (r2 >= r4) goto L_0x00cc
            r4 = 4
            if (r15 >= r4) goto L_0x00cc
            if (r3 == 0) goto L_0x00ca
            r3 = r7[r1]
            boolean r3 = r3[r2]
            if (r3 != 0) goto L_0x00ca
            r3 = 1
        L_0x00b8:
            if (r3 == 0) goto L_0x00d0
            int r2 = r2 + 1
            goto L_0x00a6
        L_0x00bd:
            r14 = move-exception
            r13.close()
            r13 = 0
        L_0x00c2:
            return r13
        L_0x00c3:
            r14 = move-exception
            r13.close()
            throw r14
        L_0x00c8:
            r1 = 0
            goto L_0x0099
        L_0x00ca:
            r3 = 0
            goto L_0x00b8
        L_0x00cc:
            int r1 = r1 + 1
            r2 = r3
            goto L_0x009b
        L_0x00d0:
            r1 = r3
        L_0x00d1:
            if (r1 == 0) goto L_0x00db
            r1 = 0
            r14[r1] = r13
            r13 = 1
            r14[r13] = r15
            r13 = 1
            goto L_0x00c2
        L_0x00db:
            int r15 = r15 + 1
            goto L_0x008f
        L_0x00de:
            int r13 = r13 + 1
            goto L_0x008b
        L_0x00e1:
            r13 = 0
            goto L_0x00c2
        L_0x00e3:
            r1 = r2
            goto L_0x00d1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.launcher.InstallShortcutReceiver.a(android.content.Context, int[], int):boolean");
    }

    public void onReceive(Context context, Intent intent) {
        if ("com.android.launcher.action.INSTALL_SHORTCUT".equals(intent.getAction())) {
            int screen = Launcher.getScreen();
            if (!a(context, intent, screen)) {
                int i = 0;
                while (i < 3) {
                    if (i == screen || !a(context, intent, i)) {
                        i++;
                    } else {
                        return;
                    }
                }
            }
        }
    }
}
