package com.tencent.launcher;

import android.content.ContentValues;

public class ha {
    private boolean a = false;
    public long l = -1;
    public int m;
    public long n = -1;
    public int o = -1;
    public int p = -1;
    public int q = -1;
    public int r = 1;
    public int s = 1;
    public int t = -1;
    public int u;
    public boolean v = true;

    public ha() {
    }

    public ha(ha haVar) {
        this.l = haVar.l;
        this.p = haVar.p;
        this.q = haVar.q;
        this.r = haVar.r;
        this.s = haVar.s;
        this.o = haVar.o;
        this.m = haVar.m;
        this.n = haVar.n;
    }

    public void a() {
    }

    public void a(ContentValues contentValues) {
        contentValues.put("itemType", Integer.valueOf(this.m));
        if (!this.a) {
            contentValues.put("container", Long.valueOf(this.n));
            contentValues.put("screen", Integer.valueOf(this.o));
            contentValues.put("cellX", Integer.valueOf(this.p));
            contentValues.put("cellY", Integer.valueOf(this.q));
            contentValues.put("spanX", Integer.valueOf(this.r));
            contentValues.put("spanY", Integer.valueOf(this.s));
            contentValues.put("orderId", Integer.valueOf(this.u));
        }
    }
}
