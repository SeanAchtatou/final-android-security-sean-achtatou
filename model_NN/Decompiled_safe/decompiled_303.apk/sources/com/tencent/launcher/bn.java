package com.tencent.launcher;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import com.tencent.launcher.Launcher;
import com.tencent.module.appcenter.c;
import com.tencent.module.theme.ThemeSettingActivity;
import java.io.File;
import java.io.IOException;

final class bn implements DialogInterface.OnClickListener {
    private /* synthetic */ String a;
    private /* synthetic */ Launcher.ThemeDownLoadReceiver b;

    bn(Launcher.ThemeDownLoadReceiver themeDownLoadReceiver, String str) {
        this.b = themeDownLoadReceiver;
        this.a = str;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        File file = new File(c.a + "/" + (ThemeSettingActivity.decodeUrl2FileName(this.a) + ".apk"));
        try {
            Runtime.getRuntime().exec("chmod 777 " + file.getAbsolutePath());
            Runtime.getRuntime().exec("chmod 777 " + file.getParent());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent();
        intent.addFlags(268435456);
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        Launcher.this.mContext.startActivity(intent);
        dialogInterface.dismiss();
    }
}
