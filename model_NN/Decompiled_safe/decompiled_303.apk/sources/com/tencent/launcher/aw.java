package com.tencent.launcher;

import android.view.View;
import android.widget.AdapterView;

final class aw implements AdapterView.OnItemClickListener {
    private /* synthetic */ SearchAppActivityReserved a;

    aw(SearchAppActivityReserved searchAppActivityReserved) {
        this.a = searchAppActivityReserved;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (this.a.listAdapter.getCount() - 1 >= i) {
            Launcher.getLauncher().startActivitySafely(((am) this.a.listAdapter.getItem(i)).c);
        }
    }
}
