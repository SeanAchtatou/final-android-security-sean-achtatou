package com.tencent.launcher;

import android.view.View;
import com.tencent.launcher.CellLayout;

final class eu implements Runnable {
    private /* synthetic */ View a;
    private /* synthetic */ int b;
    private /* synthetic */ Workspace c;

    eu(Workspace workspace, View view, int i) {
        this.c = workspace;
        this.a = view;
        this.b = i;
    }

    public final void run() {
        CellLayout.LayoutParams layoutParams = (CellLayout.LayoutParams) this.a.getLayoutParams();
        ff.b(this.c.s, (ha) this.a.getTag(), -100, this.b, layoutParams.a, layoutParams.b);
    }
}
