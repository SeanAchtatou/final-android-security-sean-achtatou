package com.tencent.launcher;

final class cs implements hc {
    private /* synthetic */ Workspace a;
    private /* synthetic */ Launcher b;

    cs(Launcher launcher, Workspace workspace) {
        this.b = launcher;
        this.a = workspace;
    }

    public final void l() {
        if (!this.b.mDrawer.a() || !this.b.mGrid.c()) {
            this.a.l();
        } else {
            this.b.mGrid.a().l();
        }
    }

    public final void m() {
        if (!this.b.mDrawer.a() || !this.b.mGrid.c()) {
            this.a.m();
        } else {
            this.b.mGrid.a().m();
        }
    }
}
