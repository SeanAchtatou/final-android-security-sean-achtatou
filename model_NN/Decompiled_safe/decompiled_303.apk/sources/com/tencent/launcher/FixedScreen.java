package com.tencent.launcher;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.tencent.launcher.CellLayout;

public class FixedScreen extends CellLayout {
    public FixedScreen(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i, new CellLayout.LayoutParams(layoutParams));
    }

    public void onFinishInflate() {
        super.onFinishInflate();
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int childCount = getChildCount();
        for (int i5 = 0; i5 < childCount; i5++) {
            View childAt = getChildAt(i5);
            if (childAt.getVisibility() != 8) {
                childAt.layout(0, 0, childAt.getMeasuredWidth() + 0, childAt.getMeasuredHeight() + 0);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        setMeasuredDimension(getDefaultSize(getSuggestedMinimumWidth(), i), getDefaultSize(getSuggestedMinimumHeight(), i2));
        if (View.MeasureSpec.getMode(i) != 1073741824) {
            throw new IllegalStateException("can only be used in EXACTLY mode.");
        } else if (View.MeasureSpec.getMode(i2) != 1073741824) {
            throw new IllegalStateException("can only be used in EXACTLY mode.");
        } else {
            int childCount = getChildCount();
            for (int i3 = 0; i3 < childCount; i3++) {
                getChildAt(i3).measure(i, i2);
            }
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return true;
    }
}
