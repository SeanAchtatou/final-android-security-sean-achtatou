package com.tencent.launcher;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

public class LauncherProvider extends ContentProvider {
    static final Uri a = Uri.parse("content://com.tencent.qqlauncher.settings/appWidgetReset");
    private SQLiteOpenHelper b;

    static String a(String str, int[] iArr) {
        StringBuilder sb = new StringBuilder();
        for (int length = iArr.length - 1; length >= 0; length--) {
            sb.append(str).append("=").append(iArr[length]);
            if (length > 0) {
                sb.append(" OR ");
            }
        }
        return sb.toString();
    }

    private void a(Uri uri) {
        String queryParameter = uri.getQueryParameter("notify");
        if (queryParameter == null || "true".equals(queryParameter)) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
    }

    /* JADX INFO: finally extract failed */
    public int bulkInsert(Uri uri, ContentValues[] contentValuesArr) {
        gh ghVar = new gh(uri);
        SQLiteDatabase writableDatabase = this.b.getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            for (ContentValues insert : contentValuesArr) {
                if (writableDatabase.insert(ghVar.a, null, insert) < 0) {
                    writableDatabase.endTransaction();
                    return 0;
                }
            }
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            a(uri);
            return contentValuesArr.length;
        } catch (Throwable th) {
            writableDatabase.endTransaction();
            throw th;
        }
    }

    public int delete(Uri uri, String str, String[] strArr) {
        if (str == null || !str.equals("initData")) {
            gh ghVar = new gh(uri, str, strArr);
            int delete = this.b.getWritableDatabase().delete(ghVar.a, ghVar.b, ghVar.c);
            if (delete <= 0) {
                return delete;
            }
            a(uri);
            return delete;
        }
        if (this.b instanceof u) {
            u uVar = (u) this.b;
            uVar.a(uVar.getWritableDatabase());
        }
        return 0;
    }

    public String getType(Uri uri) {
        gh ghVar = new gh(uri, null, null);
        return TextUtils.isEmpty(ghVar.b) ? "vnd.android.cursor.dir/" + ghVar.a : "vnd.android.cursor.item/" + ghVar.a;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        long insert = this.b.getWritableDatabase().insert(new gh(uri).a, null, contentValues);
        if (insert <= 0) {
            return null;
        }
        Uri withAppendedId = ContentUris.withAppendedId(uri, insert);
        a(withAppendedId);
        return withAppendedId;
    }

    public boolean onCreate() {
        this.b = new u(getContext());
        return true;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        gh ghVar = new gh(uri, str, strArr2);
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setTables(ghVar.a);
        Cursor query = sQLiteQueryBuilder.query(this.b.getWritableDatabase(), strArr, ghVar.b, ghVar.c, null, null, str2);
        query.setNotificationUri(getContext().getContentResolver(), uri);
        return query;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        gh ghVar = new gh(uri, str, strArr);
        int update = this.b.getWritableDatabase().update(ghVar.a, contentValues, ghVar.b, ghVar.c);
        if (update > 0) {
            a(uri);
        }
        return update;
    }
}
