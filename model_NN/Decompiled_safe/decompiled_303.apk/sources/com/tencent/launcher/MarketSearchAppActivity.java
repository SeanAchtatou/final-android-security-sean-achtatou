package com.tencent.launcher;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.module.appcenter.d;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MarketSearchAppActivity extends Activity implements TextWatcher, View.OnClickListener, View.OnFocusChangeListener, View.OnTouchListener, TextView.OnEditorActionListener {
    private static final float Font_PADDING = 20.0f;
    private static final float HOTWORD_TEXTSIZE = 16.0f;
    private static final int MAX_FONT_HEIGHT = 50;
    private static final int RADIO_LINE_NAMEBER = 12;
    private static final int RECT_MATRIC = 6;
    private static final int TAB_LOCAL = 0;
    private static final int TAB_NET = 1;
    private static final String TAG = "MarketSearchAppActivity";
    private AnimationSet[] asets;
    /* access modifiers changed from: private */
    public boolean bAllHttpMsgReceived = false;
    private ImageView centerIcon;
    /* access modifiers changed from: private */
    public int[] hotWordX;
    /* access modifiers changed from: private */
    public int[] hotWordY;
    private int[] hotwordColor = {R.color.tag_color_1, R.color.tag_color_2, R.color.tag_color_3, R.color.tag_color_4, R.color.tag_color_5};
    private int[] hotwordSize = {14, 16, 17, 19, 20, 21, 22, 23, 24};
    private boolean isFinishHotwords = false;
    private ImageView mClearButton;
    private Context mContext;
    private String mCurKeyWord;
    private int mCurTab;
    /* access modifiers changed from: private */
    public EditText mEditText;
    /* access modifiers changed from: private */
    public RelativeLayout mFootLoadingView;
    /* access modifiers changed from: private */
    public Handler mHandler = new ca(this);
    private TextView mHotWordTitleView;
    private TextView[] mHotWordViews;
    private ArrayList mHotwordsCache;
    private RelativeLayout mHotwordsLayout;
    private LinearLayout mHotwordsLinearLayout;
    private LayoutInflater mInflater;
    /* access modifiers changed from: private */
    public ListView mListView;
    private ProgressBar mLoadingView;
    /* access modifiers changed from: private */
    public cz mNetAdapter;
    /* access modifiers changed from: private */
    public ArrayList mNetAppList;
    private TextView mNetTabView;
    /* access modifiers changed from: private */
    public int mPageNumber = 1;
    private Button mSearchButton;
    /* access modifiers changed from: private */
    public TextView mSearchCountView;
    private Button mSwitchHotwords;
    /* access modifiers changed from: private */
    public int nAppCount = 0;
    private int parentHeight;
    private int parentWidth;
    private Rect[][] regions;
    private int requestId = -1;
    /* access modifiers changed from: private */
    public int totalCount = 0;
    private int unitH;
    private int unitW;

    static /* synthetic */ int access$308(MarketSearchAppActivity marketSearchAppActivity) {
        int i = marketSearchAppActivity.mPageNumber;
        marketSearchAppActivity.mPageNumber = i + 1;
        return i;
    }

    static /* synthetic */ int access$912(MarketSearchAppActivity marketSearchAppActivity, int i) {
        int i2 = marketSearchAppActivity.nAppCount + i;
        marketSearchAppActivity.nAppCount = i2;
        return i2;
    }

    private Rect[] calculateRegin() {
        this.regions = new Rect[6][];
        for (int i = 0; i < this.regions.length; i++) {
            this.regions[i] = new Rect[6];
            for (int i2 = 0; i2 < this.regions[i].length; i2++) {
                int i3 = (i2 - 3) * this.unitW;
                int i4 = (i - 3) * this.unitH;
                int i5 = ((i2 + 1) - 3) * this.unitW;
                int i6 = ((i + 1) - 3) * this.unitH;
                if (i3 == 0) {
                    i3 = this.unitW / 2;
                }
                if (i4 == 0) {
                    i4 = this.unitH / 2;
                }
                if (i5 == this.unitW * 6) {
                    i5 -= this.unitW / 2;
                }
                if (i6 == this.unitH * 6) {
                    i6 -= this.unitH / 2;
                }
                this.regions[i][i2] = new Rect(i3, i4, i5, i6);
            }
        }
        Rect[] rectArr = new Rect[RADIO_LINE_NAMEBER];
        rectArr[0] = new Rect(this.regions[2][0].left, this.regions[2][0].top, this.regions[2][2].right, this.regions[2][2].bottom);
        rectArr[1] = new Rect(this.regions[0][0].left, this.regions[0][0].top, this.regions[1][1].right, this.regions[1][1].bottom);
        rectArr[2] = new Rect(this.regions[0][2].left, this.regions[0][2].top, this.regions[1][2].right, this.regions[1][2].bottom);
        rectArr[3] = new Rect(this.regions[0][3].left, this.regions[0][3].top, this.regions[1][3].right, this.regions[1][3].bottom);
        rectArr[4] = new Rect(this.regions[0][4].left, this.regions[0][4].top, this.regions[1][5].right, this.regions[1][5].bottom);
        rectArr[5] = new Rect(this.regions[2][3].left, this.regions[2][3].top, this.regions[2][5].right, this.regions[2][5].bottom);
        rectArr[6] = new Rect(this.regions[3][4].left, this.regions[3][4].top, this.regions[3][5].right, this.regions[3][5].bottom);
        rectArr[7] = new Rect(this.regions[4][4].left, this.regions[4][4].top, this.regions[5][5].right, this.regions[5][5].bottom);
        rectArr[8] = new Rect(this.regions[3][3].left, this.regions[3][3].top, this.regions[5][3].right, this.regions[5][3].bottom);
        rectArr[9] = new Rect(this.regions[3][2].left, this.regions[3][2].top, this.regions[5][2].right, this.regions[5][2].bottom);
        rectArr[10] = new Rect(this.regions[4][0].left, this.regions[4][0].top, this.regions[5][1].right, this.regions[5][1].bottom);
        rectArr[11] = new Rect(this.regions[3][0].left, this.regions[3][0].top, this.regions[3][2].right, this.regions[3][2].bottom);
        return rectArr;
    }

    private Rect[] getCenterRect() {
        Rect[] rectArr = new Rect[16];
        rectArr[0] = this.regions[2][2];
        rectArr[1] = this.regions[2][3];
        rectArr[2] = this.regions[3][3];
        rectArr[3] = this.regions[3][2];
        rectArr[4] = this.regions[3][1];
        rectArr[5] = this.regions[2][1];
        rectArr[6] = this.regions[1][1];
        rectArr[7] = this.regions[1][2];
        rectArr[8] = this.regions[1][3];
        rectArr[9] = this.regions[1][4];
        rectArr[10] = this.regions[2][4];
        rectArr[11] = this.regions[3][4];
        rectArr[RADIO_LINE_NAMEBER] = this.regions[4][4];
        rectArr[13] = this.regions[4][3];
        rectArr[14] = this.regions[4][2];
        rectArr[15] = this.regions[4][1];
        return rectArr;
    }

    /* access modifiers changed from: private */
    public String getTextFilter() {
        if (this.mEditText == null) {
            return null;
        }
        this.mCurKeyWord = this.mEditText.getText().toString();
        return this.mEditText.getText().toString();
    }

    private void hideSoftKeyboard() {
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.mListView.getWindowToken(), 0);
    }

    private void initialNetSearch() {
        this.mListView.setFooterDividersEnabled(true);
        if (this.mListView.getFooterViewsCount() > 0) {
            this.mListView.removeFooterView(this.mFootLoadingView);
        }
        this.mListView.addFooterView(this.mFootLoadingView);
        this.mListView.setAdapter((ListAdapter) this.mNetAdapter);
        this.mListView.setOnItemClickListener(new cc(this));
        this.mListView.setOnScrollListener(new cd(this));
    }

    private boolean isCachedHotwards(List list) {
        for (int i = 0; i < this.mHotwordsCache.size(); i++) {
            ArrayList arrayList = (ArrayList) this.mHotwordsCache.get(i);
            for (int i2 = 0; i2 < list.size(); i2++) {
                if (arrayList.contains(list.get(i))) {
                    this.isFinishHotwords = true;
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isCollision(int i, int i2, boolean z) {
        if (this.mHotWordViews[i].getVisibility() != 0 || this.mHotWordViews[i2].getVisibility() != 0) {
            return false;
        }
        Log.v("Search", "检测  " + z + " " + ((Object) this.mHotWordViews[i].getText()) + " " + ((Object) this.mHotWordViews[i2].getText()));
        int abs = Math.abs(this.hotWordY[i] - this.hotWordY[i2]);
        Paint paint = new Paint();
        paint.setTextSize(this.mHotWordViews[i].getTextSize());
        float measureText = paint.measureText(this.mHotWordViews[i].getText().toString());
        paint.setTextSize(this.mHotWordViews[i2].getTextSize());
        float measureText2 = paint.measureText(this.mHotWordViews[i2].getText().toString());
        int i3 = (int) (((float) this.hotWordX[i]) - (measureText / 2.0f));
        int i4 = (int) (((float) this.hotWordX[i2]) - (measureText2 / 2.0f));
        if (abs < MAX_FONT_HEIGHT) {
            Log.v("Search", "两者之高度差比字体高小 Yabs:" + abs + "fontHeight:" + ((int) MAX_FONT_HEIGHT));
            if (i3 < i4) {
                Log.v("Search", "自己在左边");
                if (((float) (i4 - i3)) < measureText) {
                    Log.v("Search", "有冲突:hotWordX[other]:" + this.hotWordX[i2] + "hotWordX[self]:" + this.hotWordX[i] + "strWidths:" + measureText);
                    if (z) {
                        Log.v("Search", "解决ing");
                        Log.v("Search", ((Object) this.mHotWordViews[i].getText()) + "Ori X:" + this.hotWordX[i] + "Ori Y:" + this.hotWordY[i]);
                        solveCollision(i, i2);
                        Log.v("Search", ((Object) this.mHotWordViews[i].getText()) + "Solved X:" + this.hotWordX[i] + "Solved Y:" + this.hotWordY[i]);
                        Log.v("Search", "解决ed");
                    }
                    return true;
                }
                Log.v("Search", "没有冲突");
            } else {
                Log.v("Search", "自己在右边");
                if (((float) (i3 - i4)) < measureText2) {
                    Log.v("Search", "有冲突:hotWordX[other]:" + this.hotWordX[i2] + "hotWordX[self]:" + this.hotWordX[i] + "strWidths:" + measureText2);
                    if (z) {
                        Log.v("Search", "解决ing");
                        Log.v("Search", ((Object) this.mHotWordViews[i].getText()) + "Ori X:" + this.hotWordX[i] + "Ori Y:" + this.hotWordY[i]);
                        solveCollision(i, i2);
                        Log.v("Search", ((Object) this.mHotWordViews[i].getText()) + "Solved X:" + this.hotWordX[i] + "Solved Y:" + this.hotWordY[i]);
                        Log.v("Search", "解决ed");
                    }
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isOutScreen(int i) {
        Paint paint = new Paint();
        paint.setTextSize(this.mHotWordViews[i].getTextSize());
        float measureText = paint.measureText(this.mHotWordViews[i].getText().toString());
        if (((float) this.hotWordX[i]) + measureText > ((float) (this.parentWidth / 2))) {
            Log.v("Search", "超出右边  " + this.hotWordX[i]);
            return true;
        } else if (((float) (this.hotWordX[i] + (this.parentWidth / 2))) - measureText < 0.0f) {
            Log.v("Search", "超出左边  " + this.hotWordX[i]);
            return true;
        } else if (this.hotWordY[i] + MAX_FONT_HEIGHT > this.parentHeight / 2) {
            Log.v("Search", "超出下边  " + this.hotWordY[i]);
            return true;
        } else if (this.hotWordY[i] - 25 >= (-this.parentHeight) / 2) {
            return false;
        } else {
            Log.v("Search", "超出上边  " + this.hotWordY[i]);
            return true;
        }
    }

    /* access modifiers changed from: private */
    public void searchKeyWord() {
        this.mListView.setVisibility(0);
        String textFilter = getTextFilter();
        if (textFilter == null || textFilter.length() == 0) {
            this.mListView.setVisibility(8);
            this.mHotwordsLinearLayout.setVisibility(0);
            this.requestId = d.a().b(this.mHandler);
            this.mLoadingView.setVisibility(0);
            return;
        }
        this.mHotwordsLinearLayout.setVisibility(4);
        if (!(this.mListView.getAdapter() instanceof cz)) {
            initialNetSearch();
        }
        if (this.mNetAdapter.a == null || !this.mNetAdapter.a.equals(textFilter)) {
            this.mNetAppList.clear();
            this.mNetAdapter.a = textFilter;
            this.mPageNumber = 1;
            this.nAppCount = 0;
            this.bAllHttpMsgReceived = false;
            this.mHotwordsLinearLayout.setVisibility(4);
            d.a().a(this.mHandler, textFilter, this.mPageNumber);
            this.mPageNumber++;
            return;
        }
        setCountNotify(this.mNetAdapter.getCount(), textFilter);
        this.mNetAdapter.notifyDataSetChanged();
    }

    private void setCountNotify(int i, String str) {
        this.mSearchCountView.setText(i + "条关于" + str + "的搜索结果");
    }

    private synchronized void setHotwords(List list) {
        if (list != null) {
            this.mHotwordsLayout.removeAllViews();
            this.mHotwordsLayout.clearDisappearingChildren();
            this.mHotwordsLayout.requestLayout();
            if (this.mHotWordViews != null) {
                for (int length = this.mHotWordViews.length - 1; length >= 0; length--) {
                    this.mHotWordViews[length].clearAnimation();
                    this.mHotWordViews[length].setText("");
                    this.mHotWordViews[length] = null;
                }
            }
            this.mHotWordViews = new TextView[10];
            this.hotWordX = new int[10];
            this.hotWordY = new int[10];
            this.parentHeight = this.mHotwordsLayout.getHeight();
            this.parentWidth = this.mHotwordsLayout.getWidth();
            Random random = new Random();
            for (int i = 0; i < 10; i++) {
                this.mHotWordViews[i] = new TextView(this.mContext);
                this.mHotWordViews[i].setText((CharSequence) list.get(i));
                this.mHotWordViews[i].setBackgroundColor(Color.parseColor("#00000000"));
                this.mHotWordViews[i].setBackgroundColor(getResources().getColor(R.color.background_for_all));
                this.mHotWordViews[i].setTextColor(getResources().getColor(this.hotwordColor[i % this.hotwordColor.length]));
                this.mHotWordViews[i].setDrawingCacheBackgroundColor(getResources().getColor(R.color.background_for_all));
                this.mHotWordViews[i].setTextSize((float) this.hotwordSize[random.nextInt(this.hotwordSize.length)]);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams.addRule(13);
                this.mHotWordViews[i].setOnClickListener(new cb(this));
                this.mHotwordsLayout.addView(this.mHotWordViews[i], layoutParams);
            }
            if (getResources().getConfiguration().orientation == 2) {
                for (int length2 = this.mHotWordViews.length - 1; length2 >= 6; length2--) {
                    this.mHotWordViews[length2].setVisibility(8);
                }
            }
            setOurSearchTextViewAnimationPos();
            startAnimation();
        }
    }

    private void setToOrigin(int i) {
        Log.v("Search", "从中心开始========================" + ((Object) this.mHotWordViews[i].getText()));
        Rect[] centerRect = getCenterRect();
        for (int i2 = 0; i2 < centerRect.length; i2++) {
            Log.v("Search", "current rect:" + i2 + "left:" + centerRect[i2].left + "top:" + centerRect[i2].top);
            this.hotWordX[i] = centerRect[i2].left;
            this.hotWordY[i] = centerRect[i2].top;
            if (!isOutScreen(i)) {
                int i3 = 0;
                while (i3 < this.mHotWordViews.length && (i3 == i || !isCollision(i, i3, false))) {
                    i3++;
                }
                if (i3 == this.mHotWordViews.length) {
                    return;
                }
            }
        }
        Log.v("Search", ((Object) this.mHotWordViews[i].getText()) + " location error!!!!!!!!!!!!!!!!!!!!!");
        this.mHotWordViews[i].setText("");
        this.mHotWordViews[i].setVisibility(8);
    }

    private void setupView() {
        this.mSearchButton = (Button) findViewById(R.id.search_app_go_search);
        this.mClearButton = (ImageView) findViewById(R.id.search_app_clear);
        this.mLoadingView = (ProgressBar) findViewById(R.id.search_app_loading);
        this.mListView = (ListView) findViewById(R.id.search_app_listview);
        this.mEditText = (EditText) findViewById(R.id.search_app_key_word);
        this.mNetTabView = (TextView) findViewById(R.id.search_app_netbox);
        this.mHotwordsLayout = (RelativeLayout) findViewById(R.id.search_app_hotwords_layout);
        this.mHotwordsLinearLayout = (LinearLayout) findViewById(R.id.search_app_hotwords_linearlayout);
        this.mSwitchHotwords = (Button) findViewById(R.id.search_app_switch_hotwords);
        this.mHotWordTitleView = (TextView) findViewById(R.id.search_app_hotwords_title);
        this.mSearchCountView = (TextView) this.mInflater.inflate((int) R.layout.search_app_count, (ViewGroup) null);
        this.mFootLoadingView = (RelativeLayout) this.mInflater.inflate((int) R.layout.search_app_foot_loading, (ViewGroup) null);
        this.mSearchButton.setOnClickListener(this);
        this.mClearButton.setOnClickListener(this);
        this.mNetTabView.setOnClickListener(this);
        this.mSwitchHotwords.setOnClickListener(this);
        this.mEditText.setOnEditorActionListener(this);
        this.mEditText.addTextChangedListener(this);
        this.mListView.setOnTouchListener(this);
        this.mListView.setOnFocusChangeListener(this);
        this.mListView.addHeaderView(this.mSearchCountView);
        this.mListView.setFooterDividersEnabled(false);
        this.mListView.setHeaderDividersEnabled(false);
        this.mNetTabView.setSelected(false);
        switchHotwords();
    }

    private void showHotWords() {
        this.mHotwordsLinearLayout.setVisibility(0);
        this.requestId = d.a().b(this.mHandler);
    }

    private void solveCollision(int i, int i2) {
        int abs = Math.abs(this.hotWordY[i] - this.hotWordY[i2]);
        if (this.hotWordY[i] < this.hotWordY[i2]) {
            Log.v("Search", "hotWordY[self] < hotWordY[other]");
            Log.v("Search", "Old Y:" + this.hotWordY[i]);
            int[] iArr = this.hotWordY;
            iArr[i] = iArr[i] - (MAX_FONT_HEIGHT - abs);
            Log.v("Search", "new Y:" + this.hotWordY[i]);
        } else {
            Log.v("Search", "hotWordY[self] > hotWordY[other]");
            Log.v("Search", "Old Y:" + this.hotWordY[i]);
            int[] iArr2 = this.hotWordY;
            iArr2[i] = (MAX_FONT_HEIGHT - abs) + iArr2[i];
            Log.v("Search", "new Y:" + this.hotWordY[i]);
        }
        if (isOutScreen(i)) {
            Log.v("Search", "超出了屏幕");
            setToOrigin(i);
            return;
        }
        Log.v("Search", "未超出屏幕,调整后再比较");
        int i3 = 0;
        while (i3 < this.mHotWordViews.length) {
            if (i3 == i || !isCollision(i, i3, false)) {
                i3++;
            } else {
                setToOrigin(i);
                return;
            }
        }
    }

    private synchronized void startAnimation() {
        int i = 0;
        synchronized (this) {
            while (true) {
                int i2 = i;
                if (i2 < this.mHotWordViews.length) {
                    AnimationSet animationSet = new AnimationSet(false);
                    AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                    alphaAnimation.setInterpolator(new DecelerateInterpolator(1.0f));
                    alphaAnimation.setDuration(1000);
                    TranslateAnimation translateAnimation = new TranslateAnimation(0, 0.0f, 0, (float) this.hotWordX[i2], 0, 0.0f, 0, (float) this.hotWordY[i2]);
                    translateAnimation.setInterpolator(new DecelerateInterpolator(1.0f));
                    translateAnimation.setDuration(800);
                    translateAnimation.setFillEnabled(true);
                    translateAnimation.setFillAfter(true);
                    alphaAnimation.setFillEnabled(true);
                    alphaAnimation.setFillAfter(true);
                    animationSet.addAnimation(translateAnimation);
                    animationSet.addAnimation(alphaAnimation);
                    animationSet.setAnimationListener(new bz(this, this.mHotWordViews[i2], i2));
                    this.mHotWordViews[i2].startAnimation(animationSet);
                    i = i2 + 1;
                }
            }
        }
    }

    private void switchHotwords() {
        if (this.mLoadingView.getVisibility() != 0) {
            if (!this.isFinishHotwords) {
                this.requestId = d.a().b(this.mHandler);
                this.mLoadingView.setVisibility(0);
                this.mHotwordsLayout.removeAllViews();
                this.mHotwordsLayout.clearDisappearingChildren();
                this.mHotwordsLayout.requestLayout();
                return;
            }
            onReceiveHotWords((List) this.mHotwordsCache.get(new Random().nextInt(this.mHotwordsCache.size())));
        }
    }

    public void afterTextChanged(Editable editable) {
        editable.length();
        if (this.mCurTab == 0) {
            searchKeyWord();
        }
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void dismissProgressDialog() {
        this.mLoadingView.setVisibility(4);
        this.mSwitchHotwords.setEnabled(true);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.search_app_go_search /*2131493118*/:
                searchKeyWord();
                return;
            case R.id.search_app_switch_hotwords /*2131493125*/:
                switchHotwords();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.market_search_application);
        this.mContext = this;
        this.mInflater = LayoutInflater.from(this.mContext);
        setupView();
        this.mNetAppList = new ArrayList();
        this.mNetAdapter = new cz(this, this.mContext, this.mNetAppList);
        this.mListView.setAdapter((ListAdapter) this.mNetAdapter);
    }

    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i != 6) {
            return false;
        }
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.mListView.getWindowToken(), 0);
        if (TextUtils.isEmpty(getTextFilter())) {
            finish();
        }
        return true;
    }

    public void onFocusChange(View view, boolean z) {
        if (view == this.mListView && z) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.mListView.getWindowToken(), 0);
        }
    }

    public void onReceiveHotWords(List list) {
        if (this.mListView != null) {
            this.mListView.setVisibility(8);
        }
        this.mHotwordsLayout.setVisibility(0);
        if (this.mHotwordsCache == null) {
            this.mHotwordsCache = new ArrayList();
        }
        if (!isCachedHotwards(list)) {
            this.mHotwordsCache.add((ArrayList) list);
        }
        setHotwords(list);
        setHotwordResult();
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (view == this.mListView) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.mListView.getWindowToken(), 0);
        }
        return false;
    }

    public void setHotwordResult() {
        this.mHotwordsLayout.setVisibility(0);
        this.mSwitchHotwords.setVisibility(0);
    }

    /* access modifiers changed from: package-private */
    public void setOurSearchTextViewAnimationPos() {
        this.parentHeight = this.mHotwordsLayout.getHeight();
        this.parentWidth = this.mHotwordsLayout.getWidth();
        this.unitH = this.parentHeight / 6;
        this.unitW = this.parentWidth / 6;
        Rect[] calculateRegin = calculateRegin();
        ArrayList arrayList = new ArrayList();
        for (Integer num = 0; num.intValue() < RADIO_LINE_NAMEBER; num = Integer.valueOf(num.intValue() + 1)) {
            arrayList.add(num);
        }
        for (int i = 0; i < this.mHotWordViews.length; i++) {
            Random random = new Random();
            int nextInt = random.nextInt(arrayList.size());
            int intValue = ((Integer) arrayList.get(nextInt)).intValue();
            arrayList.remove(nextInt);
            this.hotWordX[i] = random.nextInt(this.unitW) + calculateRegin[intValue].left;
            this.hotWordY[i] = calculateRegin[intValue].top + random.nextInt(this.unitH);
            this.hotWordY[i] = this.hotWordY[i] + (this.unitH / 2);
            this.hotWordX[i] = this.hotWordX[i] + (this.unitW / 2);
        }
        for (int i2 = 0; i2 < this.hotWordX.length; i2++) {
            Paint paint = new Paint();
            paint.setTextSize(this.mHotWordViews[i2].getTextSize());
            float measureText = paint.measureText(this.mHotWordViews[i2].getText().toString());
            if (((float) this.hotWordX[i2]) + measureText > ((float) (this.parentWidth / 2))) {
                this.hotWordX[i2] = (int) (((double) (this.parentWidth / 2)) - (((double) measureText) * 1.5d));
            }
            if (((float) this.hotWordX[i2]) - measureText < ((float) ((-this.parentWidth) / 2))) {
                this.hotWordX[i2] = (int) (((float) ((-this.parentWidth) / 2)) + measureText);
            }
            if (this.hotWordY[i2] + MAX_FONT_HEIGHT > this.parentHeight / 2) {
                this.hotWordY[i2] = this.hotWordY[i2] - MAX_FONT_HEIGHT;
            }
            if (this.hotWordY[i2] - 25 < (-this.parentHeight) / 2) {
                this.hotWordY[i2] = this.hotWordY[i2] + 25;
            }
            Log.v(TAG, ((Object) this.mHotWordViews[i2].getText()) + " width:" + measureText + "X: " + this.hotWordX[i2]);
            Log.v(TAG, ((Object) this.mHotWordViews[i2].getText()) + " height:" + ((int) MAX_FONT_HEIGHT) + "Y: " + this.hotWordY[i2]);
            Log.v("Search", ((Object) this.mHotWordViews[i2].getText()) + " 开始检测");
            for (int i3 = 0; i3 < this.hotWordX.length; i3++) {
                if (i2 != i3) {
                    isCollision(i2, i3, true);
                }
            }
            Log.v("Search", ((Object) this.mHotWordViews[i2].getText()) + " 结束检测");
            Log.v(TAG, ((Object) this.mHotWordViews[i2].getText()) + "final width:" + measureText + "X: " + this.hotWordX[i2]);
            Log.v(TAG, ((Object) this.mHotWordViews[i2].getText()) + "final height:" + ((int) MAX_FONT_HEIGHT) + "Y: " + this.hotWordY[i2]);
        }
    }
}
