package com.tencent.launcher;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import com.tencent.launcher.base.d;
import com.tencent.qqlauncher.R;
import java.io.File;

final class hy implements DialogInterface.OnClickListener {
    private /* synthetic */ Launcher a;

    hy(Launcher launcher) {
        this.a = launcher;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        File file = new File(d.a);
        Intent intent = new Intent("android.intent.action.SEND");
        intent.putExtra("android.intent.extra.EMAIL", new String[]{"qqlauncher@tencent.com"});
        intent.putExtra("subject", this.a.getString(R.string.crash_mail_title));
        intent.putExtra("body", this.a.getString(R.string.crash_mail_msg));
        if (file.exists()) {
            intent.putExtra("android.intent.extra.STREAM", Uri.fromFile(file));
            intent.setType("text/plain");
        }
        intent.setType("message/rfc882");
        intent.addFlags(268435456);
        this.a.startActivity(intent);
        this.a.stopHome();
    }
}
