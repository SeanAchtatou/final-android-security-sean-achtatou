package com.tencent.launcher;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.TextView;
import com.tencent.launcher.base.b;
import com.tencent.qqlauncher.R;

public class BubbleTextView extends TextView {
    private static Drawable d;
    private static boolean e = true;
    private static final Interpolator f = new DecelerateInterpolator();
    private final Rect a = new Rect();
    private boolean b;
    private Drawable c;

    public BubbleTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public BubbleTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a();
    }

    private void a() {
        setFocusable(true);
        this.c = getBackground();
        setBackgroundDrawable(null);
        if (d == null) {
            d = getResources().getDrawable(R.drawable.bg_bubble_text);
        }
    }

    public static void a(boolean z) {
        e = z;
    }

    public final void a(float f2, float f3, Animation.AnimationListener animationListener) {
        TranslateAnimation translateAnimation = new TranslateAnimation(getContext(), null);
        translateAnimation.setFillAfter(true);
        translateAnimation.setInterpolator(f);
        translateAnimation.a(f2, f3);
        translateAnimation.setDuration(180);
        translateAnimation.setAnimationListener(animationListener);
        startAnimation(translateAnimation);
    }

    public final boolean b() {
        if (getParent() != null) {
            return ((Workspace) getParent().getParent()).C();
        }
        return false;
    }

    public final int c() {
        Layout layout = getLayout();
        if (layout == null) {
            return 0;
        }
        return (int) (((float) (layout.getLineBottom(0) - layout.getLineTop(0))) + (12.0f * b.b));
    }

    public void draw(Canvas canvas) {
        Drawable drawable = this.c;
        if (drawable != null) {
            int scrollX = getScrollX();
            int scrollY = getScrollY();
            if (this.b) {
                drawable.setBounds(0, 0, getRight() - getLeft(), getBottom() - getTop());
                this.b = false;
            }
            if ((scrollX | scrollY) == 0) {
                drawable.draw(canvas);
            } else {
                canvas.translate((float) scrollX, (float) scrollY);
                drawable.draw(canvas);
                canvas.translate((float) (-scrollX), (float) (-scrollY));
            }
        }
        Rect rect = this.a;
        if (e) {
            Drawable drawable2 = d;
            rect.set(0, 0, drawable2.getIntrinsicWidth(), drawable2.getIntrinsicHeight());
            rect.offset(getScrollX() + ((getWidth() - drawable2.getIntrinsicWidth()) / 2), getCompoundPaddingTop() - getCompoundDrawablePadding());
            drawable2.setBounds(rect);
            drawable2.draw(canvas);
        }
        super.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        Drawable drawable = this.c;
        if (drawable != null && drawable.isStateful()) {
            drawable.setState(getDrawableState());
        }
        super.drawableStateChanged();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        if (this.c != null) {
            this.c.setCallback(this);
        }
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        if (this.c != null) {
            this.c.setCallback(null);
        }
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public boolean setFrame(int i, int i2, int i3, int i4) {
        if (!(getLeft() == i && getRight() == i3 && getTop() == i2 && getBottom() == i4)) {
            this.b = true;
        }
        return super.setFrame(i, i2, i3, i4);
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return drawable == this.c || super.verifyDrawable(drawable);
    }
}
