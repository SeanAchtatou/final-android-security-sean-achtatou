package com.tencent.launcher;

import android.content.DialogInterface;
import android.widget.Toast;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

final class eq implements DialogInterface.OnClickListener {
    private /* synthetic */ ArrayList a;
    private /* synthetic */ Launcher b;

    eq(Launcher launcher, ArrayList arrayList) {
        this.b = launcher;
        this.a = arrayList;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (i < this.a.size()) {
            bc bcVar = (bc) this.a.get(i);
            dialogInterface.dismiss();
            if (bcVar.d == 0) {
                Toast.makeText(this.b.mContext, (int) R.string.import_no_data, 0).show();
            } else {
                Launcher.showImportingDialog(bcVar.b, this.b.mContext, false);
            }
        }
    }
}
