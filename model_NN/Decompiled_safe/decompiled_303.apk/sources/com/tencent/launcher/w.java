package com.tencent.launcher;

import java.lang.ref.WeakReference;

final class w implements Runnable {
    private volatile boolean a;
    private volatile boolean b = true;
    private final WeakReference c;
    private final boolean d;
    /* access modifiers changed from: private */
    public final boolean e;
    private final boolean f;
    private final int g = ff.z.getAndIncrement();
    private /* synthetic */ ff h;

    w(ff ffVar, Launcher launcher, boolean z, boolean z2, boolean z3) {
        this.h = ffVar;
        this.e = z2;
        this.f = z3;
        this.c = new WeakReference(launcher);
        this.d = z;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.a = true;
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        return this.b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.home.i.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.launcher.home.i.b(java.lang.String, int):int
      com.tencent.launcher.home.i.b(java.lang.String, java.lang.String):java.lang.String
      com.tencent.launcher.home.i.b(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.home.i.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.launcher.home.i.a(java.lang.String, int):void
      com.tencent.launcher.home.i.a(java.lang.String, long):void
      com.tencent.launcher.home.i.a(java.lang.String, java.lang.String):void
      com.tencent.launcher.home.i.a(java.lang.String, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x027f A[Catch:{ Exception -> 0x01a5, all -> 0x01af }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0281 A[Catch:{ Exception -> 0x01a5, all -> 0x01af }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r38 = this;
            r3 = 0
            android.os.Process.setThreadPriority(r3)
            r0 = r38
            java.lang.ref.WeakReference r0 = r0.c
            r3 = r0
            java.lang.Object r3 = r3.get()
            r0 = r3
            com.tencent.launcher.Launcher r0 = (com.tencent.launcher.Launcher) r0
            r15 = r0
            android.content.ContentResolver r3 = r15.getContentResolver()
            android.content.pm.PackageManager r16 = r15.getPackageManager()
            com.tencent.launcher.home.i r4 = com.tencent.launcher.home.i.a()
            java.lang.String r5 = "setting_config_reset"
            r6 = 0
            boolean r4 = r4.b(r5, r6)
            if (r4 == 0) goto L_0x0039
            com.tencent.launcher.ff.f(r15)
            r0 = r3
            r1 = r16
            com.tencent.launcher.ff.a(r0, r1)
            com.tencent.launcher.home.i r4 = com.tencent.launcher.home.i.a()
            java.lang.String r5 = "setting_config_reset"
            r6 = 0
            r4.a(r5, r6)
        L_0x0039:
            r0 = r38
            boolean r0 = r0.d
            r4 = r0
            if (r4 == 0) goto L_0x0046
            r0 = r3
            r1 = r16
            com.tencent.launcher.ff.a(r0, r1)
        L_0x0046:
            r0 = r38
            com.tencent.launcher.ff r0 = r0.h
            r4 = r0
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.util.ArrayList unused = r4.e = r5
            r0 = r38
            com.tencent.launcher.ff r0 = r0.h
            r4 = r0
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.util.ArrayList unused = r4.f = r5
            r0 = r38
            com.tencent.launcher.ff r0 = r0.h
            r4 = r0
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.util.ArrayList unused = r4.g = r5
            r0 = r38
            com.tencent.launcher.ff r0 = r0.h
            r4 = r0
            java.util.HashMap r5 = new java.util.HashMap
            r5.<init>()
            java.util.HashMap unused = r4.h = r5
            r0 = r38
            com.tencent.launcher.ff r0 = r0.h
            r4 = r0
            java.util.ArrayList r17 = r4.e
            r0 = r38
            com.tencent.launcher.ff r0 = r0.h
            r4 = r0
            java.util.ArrayList r18 = r4.f
            r0 = r38
            com.tencent.launcher.ff r0 = r0.h
            r4 = r0
            java.util.ArrayList r19 = r4.g
            android.net.Uri r4 = com.tencent.launcher.ba.a
            r5 = 0
            r6 = 0
            r7 = 0
            java.lang.String r8 = "container,orderId"
            android.database.Cursor r3 = r3.query(r4, r5, r6, r7, r8)
            java.lang.String r4 = "_id"
            int r20 = r3.getColumnIndexOrThrow(r4)     // Catch:{ all -> 0x01af }
            java.lang.String r4 = "intent"
            int r21 = r3.getColumnIndexOrThrow(r4)     // Catch:{ all -> 0x01af }
            java.lang.String r4 = "title"
            int r22 = r3.getColumnIndexOrThrow(r4)     // Catch:{ all -> 0x01af }
            java.lang.String r4 = "iconType"
            int r5 = r3.getColumnIndexOrThrow(r4)     // Catch:{ all -> 0x01af }
            java.lang.String r4 = "icon"
            int r8 = r3.getColumnIndexOrThrow(r4)     // Catch:{ all -> 0x01af }
            java.lang.String r4 = "iconPackage"
            int r6 = r3.getColumnIndexOrThrow(r4)     // Catch:{ all -> 0x01af }
            java.lang.String r4 = "iconResource"
            int r7 = r3.getColumnIndexOrThrow(r4)     // Catch:{ all -> 0x01af }
            java.lang.String r4 = "container"
            int r23 = r3.getColumnIndexOrThrow(r4)     // Catch:{ all -> 0x01af }
            java.lang.String r4 = "itemType"
            int r24 = r3.getColumnIndexOrThrow(r4)     // Catch:{ all -> 0x01af }
            java.lang.String r4 = "appWidgetId"
            int r25 = r3.getColumnIndexOrThrow(r4)     // Catch:{ all -> 0x01af }
            java.lang.String r4 = "screen"
            int r26 = r3.getColumnIndexOrThrow(r4)     // Catch:{ all -> 0x01af }
            java.lang.String r4 = "cellX"
            int r27 = r3.getColumnIndexOrThrow(r4)     // Catch:{ all -> 0x01af }
            java.lang.String r4 = "cellY"
            int r28 = r3.getColumnIndexOrThrow(r4)     // Catch:{ all -> 0x01af }
            java.lang.String r4 = "spanX"
            int r29 = r3.getColumnIndexOrThrow(r4)     // Catch:{ all -> 0x01af }
            java.lang.String r4 = "spanY"
            int r30 = r3.getColumnIndexOrThrow(r4)     // Catch:{ all -> 0x01af }
            java.lang.String r4 = "uri"
            int r31 = r3.getColumnIndexOrThrow(r4)     // Catch:{ all -> 0x01af }
            java.lang.String r4 = "displayMode"
            int r32 = r3.getColumnIndexOrThrow(r4)     // Catch:{ all -> 0x01af }
            java.lang.String r4 = "packageName"
            int r33 = r3.getColumnIndexOrThrow(r4)     // Catch:{ all -> 0x01af }
            java.lang.String r4 = "className"
            int r34 = r3.getColumnIndexOrThrow(r4)     // Catch:{ all -> 0x01af }
            java.lang.String r4 = "layoutId"
            int r35 = r3.getColumnIndexOrThrow(r4)     // Catch:{ all -> 0x01af }
            r0 = r38
            com.tencent.launcher.ff r0 = r0.h     // Catch:{ all -> 0x01af }
            r4 = r0
            java.util.HashMap r36 = r4.h     // Catch:{ all -> 0x01af }
        L_0x0121:
            r0 = r38
            boolean r0 = r0.a     // Catch:{ all -> 0x01af }
            r4 = r0
            if (r4 != 0) goto L_0x0408
            boolean r4 = r3.moveToNext()     // Catch:{ all -> 0x01af }
            if (r4 == 0) goto L_0x0408
            r0 = r3
            r1 = r24
            int r4 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            switch(r4) {
                case 0: goto L_0x0139;
                case 1: goto L_0x0139;
                case 2: goto L_0x01c2;
                case 3: goto L_0x020d;
                case 4: goto L_0x0329;
                case 1001: goto L_0x028c;
                case 2001: goto L_0x02d3;
                case 2002: goto L_0x02d3;
                case 2004: goto L_0x02d3;
                case 10000: goto L_0x038a;
                default: goto L_0x0138;
            }     // Catch:{ Exception -> 0x01a5 }
        L_0x0138:
            goto L_0x0121
        L_0x0139:
            r0 = r3
            r1 = r21
            java.lang.String r9 = r0.getString(r1)     // Catch:{ Exception -> 0x01a5 }
            android.content.Intent r9 = android.content.Intent.getIntent(r9)     // Catch:{ URISyntaxException -> 0x0470 }
            if (r4 != 0) goto L_0x01b4
            r0 = r16
            r1 = r9
            com.tencent.launcher.am r4 = com.tencent.launcher.ff.a(r0, r1)     // Catch:{ Exception -> 0x01a5 }
        L_0x014d:
            if (r4 != 0) goto L_0x015a
            com.tencent.launcher.am r4 = new com.tencent.launcher.am     // Catch:{ Exception -> 0x01a5 }
            r4.<init>()     // Catch:{ Exception -> 0x01a5 }
            android.graphics.drawable.Drawable r10 = r16.getDefaultActivityIcon()     // Catch:{ Exception -> 0x01a5 }
            r4.d = r10     // Catch:{ Exception -> 0x01a5 }
        L_0x015a:
            if (r4 == 0) goto L_0x0121
            r0 = r3
            r1 = r22
            java.lang.String r10 = r0.getString(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.a = r10     // Catch:{ Exception -> 0x01a5 }
            r4.c = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r20
            long r9 = r0.getLong(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.l = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r23
            int r9 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            long r10 = (long) r9     // Catch:{ Exception -> 0x01a5 }
            r4.n = r10     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r26
            int r10 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.o = r10     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r27
            int r10 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.p = r10     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r28
            int r10 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.q = r10     // Catch:{ Exception -> 0x01a5 }
            switch(r9) {
                case -200: goto L_0x01ba;
                case -100: goto L_0x01ba;
                default: goto L_0x0198;
            }     // Catch:{ Exception -> 0x01a5 }
        L_0x0198:
            long r9 = (long) r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r36
            r1 = r9
            com.tencent.launcher.bq r9 = com.tencent.launcher.ff.c(r0, r1)     // Catch:{ Exception -> 0x01a5 }
            r9.a(r4)     // Catch:{ Exception -> 0x01a5 }
            goto L_0x0121
        L_0x01a5:
            r4 = move-exception
            java.lang.String r9 = "Launcher"
            java.lang.String r10 = "Desktop items loading interrupted:"
            android.util.Log.w(r9, r10, r4)     // Catch:{ all -> 0x01af }
            goto L_0x0121
        L_0x01af:
            r4 = move-exception
            r3.close()
            throw r4
        L_0x01b4:
            r4 = r15
            com.tencent.launcher.am r4 = com.tencent.launcher.ff.b(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x01a5 }
            goto L_0x014d
        L_0x01ba:
            r0 = r17
            r1 = r4
            r0.add(r1)     // Catch:{ Exception -> 0x01a5 }
            goto L_0x0121
        L_0x01c2:
            r0 = r3
            r1 = r20
            long r9 = r0.getLong(r1)     // Catch:{ Exception -> 0x01a5 }
            r0 = r36
            r1 = r9
            com.tencent.launcher.bq r4 = com.tencent.launcher.ff.c(r0, r1)     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r22
            java.lang.String r11 = r0.getString(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.h = r11     // Catch:{ Exception -> 0x01a5 }
            r4.l = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r23
            int r9 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            long r10 = (long) r9     // Catch:{ Exception -> 0x01a5 }
            r4.n = r10     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r26
            int r10 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.o = r10     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r27
            int r10 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.p = r10     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r28
            int r10 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.q = r10     // Catch:{ Exception -> 0x01a5 }
            switch(r9) {
                case -200: goto L_0x0205;
                case -100: goto L_0x0205;
                default: goto L_0x0203;
            }     // Catch:{ Exception -> 0x01a5 }
        L_0x0203:
            goto L_0x0121
        L_0x0205:
            r0 = r17
            r1 = r4
            r0.add(r1)     // Catch:{ Exception -> 0x01a5 }
            goto L_0x0121
        L_0x020d:
            r0 = r3
            r1 = r20
            long r9 = r0.getLong(r1)     // Catch:{ Exception -> 0x01a5 }
            r0 = r36
            r1 = r9
            com.tencent.launcher.dq r14 = com.tencent.launcher.ff.d(r0, r1)     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r21
            java.lang.String r4 = r0.getString(r1)     // Catch:{ Exception -> 0x01a5 }
            r11 = 0
            if (r4 == 0) goto L_0x028a
            android.content.Intent r4 = android.content.Intent.getIntent(r4)     // Catch:{ URISyntaxException -> 0x0289 }
        L_0x0229:
            r0 = r3
            r1 = r22
            java.lang.String r11 = r0.getString(r1)     // Catch:{ Exception -> 0x01a5 }
            r14.h = r11     // Catch:{ Exception -> 0x01a5 }
            r14.l = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r23
            int r37 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r0 = r37
            long r0 = (long) r0     // Catch:{ Exception -> 0x01a5 }
            r9 = r0
            r14.n = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r26
            int r9 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r14.o = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r27
            int r9 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r14.p = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r28
            int r9 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r14.q = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r31
            java.lang.String r9 = r0.getString(r1)     // Catch:{ Exception -> 0x01a5 }
            android.net.Uri r9 = android.net.Uri.parse(r9)     // Catch:{ Exception -> 0x01a5 }
            r14.b = r9     // Catch:{ Exception -> 0x01a5 }
            r14.a = r4     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r32
            int r4 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r14.c = r4     // Catch:{ Exception -> 0x01a5 }
            r9 = r15
            r10 = r3
            r11 = r5
            r12 = r6
            r13 = r7
            com.tencent.launcher.ff.a(r9, r10, r11, r12, r13, r14)     // Catch:{ Exception -> 0x01a5 }
            switch(r37) {
                case -200: goto L_0x0281;
                case -100: goto L_0x0281;
                default: goto L_0x027f;
            }     // Catch:{ Exception -> 0x01a5 }
        L_0x027f:
            goto L_0x0121
        L_0x0281:
            r0 = r17
            r1 = r14
            r0.add(r1)     // Catch:{ Exception -> 0x01a5 }
            goto L_0x0121
        L_0x0289:
            r4 = move-exception
        L_0x028a:
            r4 = r11
            goto L_0x0229
        L_0x028c:
            com.tencent.launcher.bd r4 = com.tencent.launcher.bd.b()     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r23
            int r9 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r10 = -100
            if (r9 == r10) goto L_0x02a4
            java.lang.String r4 = "Launcher"
            java.lang.String r9 = "Widget found where container != CONTAINER_DESKTOP  ignoring!"
            android.util.Log.e(r4, r9)     // Catch:{ Exception -> 0x01a5 }
            goto L_0x0121
        L_0x02a4:
            r0 = r3
            r1 = r20
            long r10 = r0.getLong(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.l = r10     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r26
            int r10 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.o = r10     // Catch:{ Exception -> 0x01a5 }
            long r9 = (long) r9     // Catch:{ Exception -> 0x01a5 }
            r4.n = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r27
            int r9 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.p = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r28
            int r9 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.q = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r17
            r1 = r4
            r0.add(r1)     // Catch:{ Exception -> 0x01a5 }
            goto L_0x0121
        L_0x02d3:
            switch(r4) {
                case 2001: goto L_0x02eb;
                case 2002: goto L_0x02f0;
                case 2003: goto L_0x02d6;
                case 2004: goto L_0x02f5;
                default: goto L_0x02d6;
            }     // Catch:{ Exception -> 0x01a5 }
        L_0x02d6:
            r4 = 0
        L_0x02d7:
            r0 = r3
            r1 = r23
            int r9 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r10 = -100
            if (r9 == r10) goto L_0x02fa
            java.lang.String r4 = "Launcher"
            java.lang.String r9 = "Widget found where container != CONTAINER_DESKTOP  ignoring!"
            android.util.Log.e(r4, r9)     // Catch:{ Exception -> 0x01a5 }
            goto L_0x0121
        L_0x02eb:
            com.tencent.launcher.bd r4 = com.tencent.launcher.bd.c()     // Catch:{ Exception -> 0x01a5 }
            goto L_0x02d7
        L_0x02f0:
            com.tencent.launcher.bd r4 = com.tencent.launcher.bd.d()     // Catch:{ Exception -> 0x01a5 }
            goto L_0x02d7
        L_0x02f5:
            com.tencent.launcher.bd r4 = com.tencent.launcher.bd.e()     // Catch:{ Exception -> 0x01a5 }
            goto L_0x02d7
        L_0x02fa:
            r0 = r3
            r1 = r20
            long r10 = r0.getLong(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.l = r10     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r26
            int r10 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.o = r10     // Catch:{ Exception -> 0x01a5 }
            long r9 = (long) r9     // Catch:{ Exception -> 0x01a5 }
            r4.n = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r27
            int r9 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.p = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r28
            int r9 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.q = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r17
            r1 = r4
            r0.add(r1)     // Catch:{ Exception -> 0x01a5 }
            goto L_0x0121
        L_0x0329:
            r0 = r3
            r1 = r25
            int r4 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            com.tencent.launcher.c r9 = new com.tencent.launcher.c     // Catch:{ Exception -> 0x01a5 }
            r9.<init>(r4)     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r20
            long r10 = r0.getLong(r1)     // Catch:{ Exception -> 0x01a5 }
            r9.l = r10     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r26
            int r4 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r9.o = r4     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r27
            int r4 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r9.p = r4     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r28
            int r4 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r9.q = r4     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r29
            int r4 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r9.r = r4     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r30
            int r4 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r9.s = r4     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r23
            int r4 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r10 = -100
            if (r4 == r10) goto L_0x037f
            java.lang.String r4 = "Launcher"
            java.lang.String r9 = "Widget found where container != CONTAINER_DESKTOP -- ignoring!"
            android.util.Log.e(r4, r9)     // Catch:{ Exception -> 0x01a5 }
            goto L_0x0121
        L_0x037f:
            long r10 = (long) r4     // Catch:{ Exception -> 0x01a5 }
            r9.n = r10     // Catch:{ Exception -> 0x01a5 }
            r0 = r18
            r1 = r9
            r0.add(r1)     // Catch:{ Exception -> 0x01a5 }
            goto L_0x0121
        L_0x038a:
            com.tencent.module.qqwidget.a r4 = new com.tencent.module.qqwidget.a     // Catch:{ Exception -> 0x01a5 }
            r4.<init>()     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r25
            int r9 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.d = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r20
            long r9 = r0.getLong(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.l = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r26
            int r9 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.o = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r27
            int r9 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.p = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r28
            int r9 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.q = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r29
            int r9 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.r = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r30
            int r9 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.s = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r33
            java.lang.String r9 = r0.getString(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.e = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r34
            java.lang.String r9 = r0.getString(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.f = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r35
            int r9 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r4.h = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r3
            r1 = r23
            int r9 = r0.getInt(r1)     // Catch:{ Exception -> 0x01a5 }
            r10 = -100
            if (r9 == r10) goto L_0x03fd
            java.lang.String r4 = "Launcher"
            java.lang.String r9 = "QQWidget found where container != CONTAINER_DESKTOP -- ignoring!"
            android.util.Log.e(r4, r9)     // Catch:{ Exception -> 0x01a5 }
            goto L_0x0121
        L_0x03fd:
            long r9 = (long) r9     // Catch:{ Exception -> 0x01a5 }
            r4.n = r9     // Catch:{ Exception -> 0x01a5 }
            r0 = r19
            r1 = r4
            r0.add(r1)     // Catch:{ Exception -> 0x01a5 }
            goto L_0x0121
        L_0x0408:
            r3.close()
            r0 = r38
            com.tencent.launcher.ff r0 = r0.h
            r9 = r0
            monitor-enter(r9)
            r0 = r38
            boolean r0 = r0.a     // Catch:{ all -> 0x046d }
            r3 = r0
            if (r3 != 0) goto L_0x0465
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch:{ all -> 0x046d }
            r0 = r6
            r1 = r17
            r0.<init>(r1)     // Catch:{ all -> 0x046d }
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ all -> 0x046d }
            r0 = r7
            r1 = r18
            r0.<init>(r1)     // Catch:{ all -> 0x046d }
            java.util.ArrayList r8 = new java.util.ArrayList     // Catch:{ all -> 0x046d }
            r0 = r8
            r1 = r19
            r0.<init>(r1)     // Catch:{ all -> 0x046d }
            r0 = r38
            boolean r0 = r0.a     // Catch:{ all -> 0x046d }
            r3 = r0
            if (r3 != 0) goto L_0x0449
            java.lang.String r3 = "HomeLoaders"
            java.lang.String r4 = "  ----> items cloned, ready to refresh UI"
            android.util.Log.d(r3, r4)     // Catch:{ all -> 0x046d }
            com.tencent.launcher.gc r3 = new com.tencent.launcher.gc     // Catch:{ all -> 0x046d }
            r4 = r38
            r5 = r15
            r3.<init>(r4, r5, r6, r7, r8)     // Catch:{ all -> 0x046d }
            r15.runOnUiThread(r3)     // Catch:{ all -> 0x046d }
        L_0x0449:
            r0 = r38
            boolean r0 = r0.e     // Catch:{ all -> 0x046d }
            r3 = r0
            if (r3 == 0) goto L_0x045d
            r0 = r38
            com.tencent.launcher.ff r0 = r0.h     // Catch:{ all -> 0x046d }
            r3 = r0
            r0 = r38
            boolean r0 = r0.f     // Catch:{ all -> 0x046d }
            r4 = r0
            r3.b(r15, r4)     // Catch:{ all -> 0x046d }
        L_0x045d:
            r0 = r38
            com.tencent.launcher.ff r0 = r0.h     // Catch:{ all -> 0x046d }
            r3 = r0
            boolean unused = r3.d = true     // Catch:{ all -> 0x046d }
        L_0x0465:
            monitor-exit(r9)     // Catch:{ all -> 0x046d }
            r3 = 0
            r0 = r3
            r1 = r38
            r1.b = r0
            return
        L_0x046d:
            r3 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x046d }
            throw r3
        L_0x0470:
            r4 = move-exception
            goto L_0x0121
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.launcher.w.run():void");
    }
}
