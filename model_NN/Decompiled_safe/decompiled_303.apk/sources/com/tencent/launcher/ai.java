package com.tencent.launcher;

import android.os.Handler;
import android.os.Message;
import com.tencent.qqlauncher.R;
import java.util.List;

final class ai extends Handler {
    private /* synthetic */ SearchAppTestActivity a;

    ai(SearchAppTestActivity searchAppTestActivity) {
        this.a = searchAppTestActivity;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case 900:
                this.a.dismissProgressDialog();
                return;
            case 1003:
                this.a.onReceiveHotWords((List) message.obj);
                break;
            case 1005:
                Object[] objArr = (Object[]) message.obj;
                List list = (List) objArr[0];
                int unused = this.a.totalCount = ((Integer) objArr[2]).intValue();
                if (list.size() == 0) {
                    this.a.mFootLoadingView.setVisibility(4);
                    boolean unused2 = this.a.bAllHttpMsgReceived = true;
                }
                if (list.size() < 10) {
                    this.a.mFootLoadingView.setVisibility(4);
                }
                for (int i = 0; i < this.a.totalCount; i++) {
                    this.a.mNetAppList.add(list.get(i));
                }
                this.a.mNetAdapter.notifyDataSetChanged();
                SearchAppTestActivity.access$1112(this.a, list.size());
                this.a.mSearchCountView.setText(this.a.getResources().getString(R.string.search_app_result, Integer.valueOf(this.a.nAppCount), this.a.getTextFilter()));
                break;
            case 2200:
                break;
            default:
                return;
        }
        this.a.dismissProgressDialog();
    }
}
