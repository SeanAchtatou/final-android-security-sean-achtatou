package com.tencent.launcher;

import android.app.ListActivity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.tencent.launcher.home.c;
import com.tencent.qqlauncher.R;
import com.tencent.util.d;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import java.util.regex.Pattern;

public class SearchAppActivity extends ListActivity implements TextWatcher, View.OnClickListener, View.OnFocusChangeListener, View.OnKeyListener, View.OnTouchListener, TextView.OnEditorActionListener {
    private static final String soundMotherReg = "^[b|p|m|f|d|t|n|l|g|k|h|j|q|x|r|z|c|s|y|w].*";
    private static String[] specialTable = {"*", "$", "?", ".", "#", "^", "&", "+", "}", "{", "}", "(", ")", "%", "|", ","};
    private ImageView delSearchBtn;
    private d dic;
    /* access modifiers changed from: private */
    public gu listAdapter;
    private ArrayList mAppList = new ArrayList();
    private ListView mListView;
    private ArrayList mMacheList = new ArrayList();
    private EditText mSearchEditText;
    private View marketSearch;
    String ori = "";
    String preStr = "";
    private ff sModel;
    private TextView textView;
    /* access modifiers changed from: private */
    public d tree = new d();

    private String escapeQuery(String str) {
        String replaceAll = str.replaceAll("/", "");
        for (int i = 0; i < specialTable.length; i++) {
            replaceAll = replaceAll.replaceAll("\\" + specialTable[i], "\\\\" + specialTable[i]);
        }
        return replaceAll;
    }

    private String getTextFilter() {
        if (this.mSearchEditText == null) {
            return null;
        }
        this.ori = this.mSearchEditText.getText().toString();
        return this.mSearchEditText.getText().toString();
    }

    private void hideSoftKeyboard() {
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.mListView.getWindowToken(), 0);
    }

    private boolean isHas(Vector vector, String str) {
        for (int i = 0; i < vector.size(); i++) {
            if (((String) vector.elementAt(i)).indexOf(str) != -1) {
                return true;
            }
        }
        return false;
    }

    private boolean isValid(Vector vector, String str) {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if ((charAt < 'a' || charAt > 'z') && (charAt < 'A' || charAt > 'Z')) {
                if (!isHas(vector, new String(new char[]{charAt}))) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean matcheApp(String str, ArrayList arrayList) {
        boolean z;
        ArrayList arrayList2 = this.mAppList;
        String escapeQuery = escapeQuery(str);
        if (escapeQuery.length() == 1 && escapeQuery.equalsIgnoreCase("\\")) {
            return false;
        }
        if (escapeQuery.length() == 0) {
            return false;
        }
        Pattern compile = Pattern.compile(".*" + escapeQuery + ".*", 2);
        Iterator it = arrayList2.iterator();
        boolean z2 = false;
        while (it.hasNext()) {
            am amVar = (am) ((ha) it.next());
            if (compile.matcher(amVar.a).find()) {
                arrayList.add(amVar);
                if (this.tree == null) {
                    this.tree = new d();
                }
                this.tree.b(amVar.a.toString());
                z2 = true;
            } else {
                String c = c.c(escapeQuery);
                if (Pattern.compile(".*" + c + ".*", 2).matcher(amVar.b).find()) {
                    c = new Character(c.charAt(0)).toString();
                }
                Vector c2 = this.dic.c(c);
                if (c2 == null || !isValid(c2, escapeQuery) || !c2.contains(amVar.a)) {
                    z = z2;
                } else {
                    arrayList.add(amVar);
                    if (this.tree == null) {
                        this.tree = new d();
                    }
                    this.tree.b(amVar.a.toString());
                    z = true;
                }
                z2 = z;
            }
        }
        this.tree.a(this.tree.a());
        return z2;
    }

    private boolean showResults(String str) {
        if (str == null || "".equals(str)) {
            return false;
        }
        ArrayList arrayList = this.mMacheList;
        arrayList.clear();
        boolean matcheApp = matcheApp(str, arrayList);
        this.listAdapter.notifyDataSetChanged();
        if (!matcheApp) {
            this.textView.setVisibility(0);
        } else {
            this.textView.setVisibility(8);
        }
        return true;
    }

    public void afterTextChanged(Editable editable) {
        if (editable.length() == 0) {
            this.textView.setVisibility(8);
            this.marketSearch.setVisibility(8);
            this.mMacheList.clear();
            this.listAdapter.notifyDataSetChanged();
            return;
        }
        this.marketSearch.setVisibility(0);
        showResults(getTextFilter());
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void onClick(View view) {
        if (view.getId() == R.id.search_market) {
            String textFilter = getTextFilter();
            if (textFilter != null && !"".equals(textFilter) && TextUtils.getTrimmedLength(textFilter) != 0) {
                try {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=" + textFilter)));
                } catch (ActivityNotFoundException e) {
                }
            }
        } else if (view.getId() == R.id.search_app_center) {
            String textFilter2 = getTextFilter();
            if (textFilter2 != null && !"".equals(textFilter2) && TextUtils.getTrimmedLength(textFilter2) != 0) {
                try {
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("http://a.app.qq.com/g/s?aid=searchsoft_a&g_f=990233&softname=" + URLEncoder.encode(textFilter2, "utf-8")));
                    intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
                    startActivity(intent);
                } catch (ActivityNotFoundException e2) {
                } catch (UnsupportedEncodingException e3) {
                    e3.printStackTrace();
                }
            }
        } else if (view == this.delSearchBtn && this.mSearchEditText != null) {
            this.mSearchEditText.setText("");
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.search_application);
        this.mSearchEditText = (EditText) findViewById(R.id.EditText_Search);
        this.mSearchEditText.addTextChangedListener(this);
        this.mSearchEditText.setOnEditorActionListener(this);
        this.textView = (TextView) findViewById(R.id.nofindapp);
        this.delSearchBtn = (ImageView) findViewById(R.id.btn_del_search);
        this.delSearchBtn.setOnClickListener(this);
        this.mListView = getListView();
        this.marketSearch = LayoutInflater.from(this).inflate((int) R.layout.search_list_market_item, (ViewGroup) null);
        this.marketSearch.findViewById(R.id.search_market).setOnClickListener(this);
        this.mListView.setFooterDividersEnabled(false);
        this.mListView.addFooterView(this.marketSearch);
        this.marketSearch.setVisibility(8);
        this.listAdapter = new gu(this, this, this.mMacheList);
        this.listAdapter.setNotifyOnChange(false);
        this.mListView.setAdapter((ListAdapter) this.listAdapter);
        this.mListView.setOnItemClickListener(new fp(this));
        this.mListView.setOnFocusChangeListener(this);
        this.mListView.setOnTouchListener(this);
        this.sModel = Launcher.getModel();
        bl e = this.sModel.e();
        if (e == null) {
            finish();
            return;
        }
        int count = e.getCount();
        ArrayList arrayList = this.mAppList;
        arrayList.clear();
        for (int i = 0; i < count; i++) {
            ha haVar = (ha) e.getItem(i);
            if (haVar instanceof am) {
                arrayList.add((am) haVar);
            } else if (haVar instanceof bq) {
                arrayList.addAll(((bq) haVar).b);
            }
        }
        this.dic = new d();
        Iterator it = this.mAppList.iterator();
        while (it.hasNext()) {
            this.dic.a(((am) ((ha) it.next())).a.toString());
        }
    }

    public boolean onEditorAction(TextView textView2, int i, KeyEvent keyEvent) {
        if (i != 6) {
            return false;
        }
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.mListView.getWindowToken(), 0);
        if (TextUtils.isEmpty(getTextFilter())) {
            finish();
        }
        return true;
    }

    public void onFocusChange(View view, boolean z) {
        if (view == this.mListView && z) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.mListView.getWindowToken(), 0);
        }
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        return false;
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (view == getListView()) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.mListView.getWindowToken(), 0);
        }
        return false;
    }
}
