package com.tencent.launcher;

import android.content.DialogInterface;
import android.view.View;
import android.widget.ArrayAdapter;
import com.tencent.module.setting.CustomAlertDialog;
import java.util.ArrayList;
import java.util.Iterator;

final class ed implements DialogInterface.OnClickListener {
    private /* synthetic */ ArrayList a;
    private /* synthetic */ int b;
    private /* synthetic */ ArrayList c;
    private /* synthetic */ bq d;
    private /* synthetic */ View e;
    private /* synthetic */ Launcher f;

    ed(Launcher launcher, ArrayList arrayList, int i, ArrayList arrayList2, bq bqVar, View view) {
        this.f = launcher;
        this.a = arrayList;
        this.b = i;
        this.c = arrayList2;
        this.d = bqVar;
        this.e = view;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        int i2;
        if (dialogInterface != null && (dialogInterface instanceof CustomAlertDialog)) {
            ((CustomAlertDialog) dialogInterface).a().setEnabled(false);
        }
        ArrayAdapter b2 = this.f.mGrid.b();
        bl e2 = b2 == null ? Launcher.sModel.e() : b2;
        e2.setNotifyOnChange(false);
        while (this.a.size() > 0) {
            am amVar = (am) this.a.get(0);
            this.a.remove(amVar);
            if (this.b != -300) {
                ff.g(this.f.mContext, amVar);
            } else {
                amVar.t = e2.getCount();
                amVar.n = -300;
                e2.add(amVar);
                ff.a(this.f.mContext, amVar);
            }
        }
        Iterator it = this.c.iterator();
        int i3 = 0;
        while (it.hasNext()) {
            gy gyVar = (gy) it.next();
            if (gyVar.d) {
                if (this.b != -300) {
                    am amVar2 = new am(gyVar.a);
                    amVar2.n = -1;
                    amVar2.u = i3;
                    amVar2.c.setAction("android.intent.action.MAIN");
                    this.a.add(amVar2);
                    ff.a(this.f.mContext, amVar2, this.d.l, 0, 0, 0);
                } else {
                    if (gyVar.a.n != this.d.l) {
                        e2.remove(gyVar.a);
                    }
                    gyVar.a.n = this.d.l;
                    gyVar.a.u = i3;
                    this.a.add(gyVar.a);
                    e2.remove(gyVar.a);
                    ff.a(this.f.mContext, gyVar.a);
                }
                i2 = i3 + 1;
            } else {
                i2 = i3;
            }
            i3 = i2;
        }
        dialogInterface.dismiss();
        this.c.clear();
        if (this.b == -100) {
            ((FolderIcon) this.e).a();
        } else if (this.b == -300) {
            this.f.mDrawer.n();
            ((DrawerTextView) this.e).f();
        } else if (this.b == -200) {
            ((DockView) this.e).a(this.d);
        }
    }
}
