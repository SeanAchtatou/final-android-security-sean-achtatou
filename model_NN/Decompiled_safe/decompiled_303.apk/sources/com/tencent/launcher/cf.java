package com.tencent.launcher;

import android.graphics.drawable.Drawable;
import android.view.View;
import com.tencent.launcher.QGridLayout;

final class cf implements Runnable {
    private /* synthetic */ QGridLayout a;

    cf(QGridLayout qGridLayout) {
        this.a = qGridLayout;
    }

    public final void run() {
        if (this.a.M != null) {
            int unused = this.a.an = 2;
            View a2 = this.a.M;
            View unused2 = this.a.aa = a2;
            if (a2.getTag() instanceof bq) {
                ((DrawerTextView) a2).a();
            } else if (a2.getTag() instanceof am) {
                Drawable unused3 = this.a.ac = this.a.ab;
                QGridLayout.LayoutParams layoutParams = (QGridLayout.LayoutParams) a2.getLayoutParams();
                int unused4 = this.a.as = a2.getLeft() + a2.getPaddingLeft() + layoutParams.leftMargin;
                int unused5 = this.a.at = a2.getPaddingTop() + a2.getTop() + layoutParams.topMargin;
            }
            int unused6 = this.a.an = 3;
            this.a.invalidate();
        }
    }
}
