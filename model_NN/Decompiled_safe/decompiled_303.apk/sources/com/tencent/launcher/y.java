package com.tencent.launcher;

import android.os.Parcel;
import android.os.Parcelable;
import com.tencent.launcher.QGridLayout;

final class y implements Parcelable.Creator {
    y() {
    }

    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new QGridLayout.SavedState(parcel, null);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new QGridLayout.SavedState[i];
    }
}
