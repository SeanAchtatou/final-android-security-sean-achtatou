package com.tencent.launcher;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.tencent.launcher.base.b;
import java.lang.ref.WeakReference;

final class ig extends BroadcastReceiver {
    private final Application a;
    private WeakReference b;

    ig(Application application, Launcher launcher) {
        this.a = application;
        this.b = new WeakReference(launcher);
    }

    /* access modifiers changed from: package-private */
    public final void a(Launcher launcher) {
        this.b = new WeakReference(launcher);
    }

    public final void onReceive(Context context, Intent intent) {
        Launcher launcher;
        Workspace workspace;
        Drawable wallpaper = this.a.getWallpaper();
        if (wallpaper instanceof BitmapDrawable) {
            Bitmap unused = Launcher.sWallpaper = ((BitmapDrawable) wallpaper).getBitmap();
            System.out.println("-----set wallpaper");
            if (this.b != null && (launcher = (Launcher) this.b.get()) != null && !b.g && (workspace = launcher.getWorkspace()) != null) {
                workspace.b(false);
                return;
            }
            return;
        }
        throw new IllegalStateException("The wallpaper must be a BitmapDrawable.");
    }
}
