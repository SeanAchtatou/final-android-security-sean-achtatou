package com.tencent.launcher;

import AndroidDLoader.Software;
import AndroidDLoader.a;
import android.view.View;
import android.widget.AdapterView;
import com.tencent.module.appcenter.SoftWareActivity;

final class ak implements AdapterView.OnItemClickListener {
    private /* synthetic */ SearchAppTestActivity a;

    ak(SearchAppTestActivity searchAppTestActivity) {
        this.a = searchAppTestActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (this.a.mCurTab == 1) {
            Software software = (Software) adapterView.getAdapter().getItem(i);
            if (software != null) {
                SoftWareActivity.openSoftwareActivity(this.a, software.a(), software.c(), software.g(), a.f);
                return;
            }
            return;
        }
        am amVar = (am) adapterView.getAdapter().getItem(i);
        Launcher launcher = Launcher.getLauncher();
        if (launcher != null && amVar != null) {
            launcher.startActivitySafely(amVar.c);
        }
    }
}
