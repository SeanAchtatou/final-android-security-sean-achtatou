package com.tencent.launcher;

import android.view.View;

final class hf implements View.OnLongClickListener {
    private /* synthetic */ ThumbnailWorkspace a;

    hf(ThumbnailWorkspace thumbnailWorkspace) {
        this.a = thumbnailWorkspace;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ThumbnailWorkspace.b(com.tencent.launcher.ThumbnailWorkspace, boolean):boolean
     arg types: [com.tencent.launcher.ThumbnailWorkspace, int]
     candidates:
      com.tencent.launcher.ThumbnailWorkspace.b(int, boolean):android.view.animation.Animation
      com.tencent.launcher.ThumbnailWorkspace.b(int, int):void
      com.tencent.launcher.ThumbnailWorkspace.b(com.tencent.launcher.ThumbnailWorkspace, int):void
      com.tencent.launcher.ThumbnailWorkspace.b(com.tencent.launcher.ThumbnailWorkspace, android.view.View):void
      com.tencent.launcher.ThumbnailWorkspace.b(com.tencent.launcher.ThumbnailWorkspace, boolean):boolean */
    public final boolean onLongClick(View view) {
        if (!this.a.r) {
            int unused = this.a.z = this.a.a((view.getLeft() + view.getRight()) / 2, (view.getTop() + view.getBottom()) / 2);
            boolean unused2 = this.a.r = true;
            this.a.o.setAlpha(0);
            this.a.requestLayout();
        }
        return true;
    }
}
