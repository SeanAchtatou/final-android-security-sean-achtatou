package com.tencent.launcher;

import android.view.View;
import com.tencent.module.appcenter.d;
import java.util.List;
import java.util.Random;

final class as implements View.OnClickListener {
    private /* synthetic */ SearchAppActivityReserved a;

    as(SearchAppActivityReserved searchAppActivityReserved) {
        this.a = searchAppActivityReserved;
    }

    public final void onClick(View view) {
        if (!this.a.isFinishHotwords) {
            int unused = this.a.requestId = d.a().b(this.a.handler);
            this.a.setWaitScreen();
            this.a.mLinearLayout.removeAllViews();
            this.a.mLinearLayout.requestLayout();
            return;
        }
        this.a.onReceiveHotWords((List) this.a.hotwordsCache.get(new Random().nextInt(this.a.hotwordsCache.size())));
    }
}
