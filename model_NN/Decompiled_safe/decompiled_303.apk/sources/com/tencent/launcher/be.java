package com.tencent.launcher;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

public final class be extends ArrayAdapter implements AbsListView.OnScrollListener, SectionIndexer {
    private SectionIndexer a;
    private boolean b = true;
    private boolean c = false;
    private boolean d = true;
    private boolean e = false;
    private final LayoutInflater f;
    private Context g;
    private int h;

    public be(Context context, ArrayList arrayList) {
        super(context, 0, arrayList);
        this.g = context;
        this.f = LayoutInflater.from(context);
        this.h = context.getResources().getColor(R.color.pinned_header_background);
    }

    public final int a(int i) {
        if (this.a == null || super.getCount() == 0) {
            return 0;
        }
        int i2 = this.e ? i - 1 : i;
        if (i2 < 0) {
            return 0;
        }
        int positionForSection = getPositionForSection(getSectionForPosition(i2) + 1);
        return (positionForSection == -1 || i2 != positionForSection - 1) ? 1 : 2;
    }

    public final void a(View view, int i, int i2) {
        bb bbVar;
        bb bbVar2 = (bb) view.getTag();
        if (bbVar2 == null) {
            bbVar = new bb();
            bbVar.a = (TextView) view.findViewById(R.id.header_text);
            bbVar.b = bbVar.a.getTextColors();
            bbVar.c = view.getBackground();
            view.setTag(bbVar);
        } else {
            bbVar = bbVar2;
        }
        bbVar.a.setText((String) this.a.getSections()[getSectionForPosition(this.e ? i - 1 : i)]);
        if (i2 == 255) {
            view.setBackgroundDrawable(bbVar.c);
            bbVar.a.setTextColor(bbVar.b);
            return;
        }
        view.setBackgroundColor(Color.rgb((Color.red(this.h) * i2) / BrightnessActivity.MAXIMUM_BACKLIGHT, (Color.green(this.h) * i2) / BrightnessActivity.MAXIMUM_BACKLIGHT, (Color.blue(this.h) * i2) / BrightnessActivity.MAXIMUM_BACKLIGHT));
        int defaultColor = bbVar.b.getDefaultColor();
        bbVar.a.setTextColor(Color.argb(i2, Color.red(defaultColor), Color.green(defaultColor), Color.blue(defaultColor)));
    }

    public final void a(String[] strArr, int[] iArr) {
        this.a = new go(strArr, iArr);
    }

    public final int getCount() {
        int count = super.getCount();
        return (!this.e || count <= 0) ? count : count + 1;
    }

    public final /* bridge */ /* synthetic */ Object getItem(int i) {
        int i2 = this.e ? i - 1 : i;
        if (i2 < 0) {
            return null;
        }
        return (am) super.getItem(i2);
    }

    public final long getItemId(int i) {
        int i2 = this.e ? i - 1 : i;
        if (i2 < 0) {
            return 0;
        }
        return super.getItemId(i2);
    }

    public final int getPositionForSection(int i) {
        if (this.a == null) {
            return -1;
        }
        return this.a.getPositionForSection(i);
    }

    public final int getSectionForPosition(int i) {
        if (this.a == null) {
            return -1;
        }
        return this.a.getSectionForPosition(i);
    }

    public final Object[] getSections() {
        if (this.a != null) {
            return this.a.getSections();
        }
        return new String[]{" "};
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (i != 0 || !this.e) {
            int i2 = this.e ? i - 1 : i;
            am amVar = i2 < 0 ? null : (am) super.getItem(i2);
            int i3 = this.e ? i - 1 : i;
            View inflate = (view == null || view.getTag() == null) ? this.f.inflate((int) R.layout.app_list_item, (ViewGroup) null) : view;
            if (!amVar.f) {
                amVar.d = a.a(getContext(), amVar.d, amVar);
                amVar.f = true;
            }
            TextView textView = (TextView) inflate.findViewById(R.id.softname);
            textView.setCompoundDrawablesWithIntrinsicBounds(amVar.d, (Drawable) null, (Drawable) null, (Drawable) null);
            textView.setText(amVar.a);
            boolean z = this.d;
            TextView textView2 = (TextView) inflate.findViewById(R.id.sectionheader);
            if (z) {
                int sectionForPosition = getSectionForPosition(i3);
                if (getPositionForSection(sectionForPosition) == i3) {
                    textView2.setText((String) this.a.getSections()[sectionForPosition]);
                    textView2.setVisibility(0);
                    inflate.setTag(amVar);
                    return inflate;
                }
            }
            textView2.setVisibility(8);
            inflate.setTag(amVar);
            return inflate;
        }
        View inflate2 = this.f.inflate((int) R.layout.total_apps, viewGroup, false);
        TextView textView3 = (TextView) inflate2.findViewById(R.id.totalAppsText);
        int count = super.getCount();
        textView3.setText(count == 0 ? this.g.getString(R.string.listTotalAllAppZero) : String.format(this.g.getResources().getQuantityText(R.plurals.listTotalAllApps, count).toString(), Integer.valueOf(count)));
        return inflate2;
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (absListView instanceof AllAppsListView) {
            ((AllAppsListView) absListView).a(i);
        }
    }

    public final void onScrollStateChanged(AbsListView absListView, int i) {
    }
}
