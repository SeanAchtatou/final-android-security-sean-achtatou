package com.tencent.launcher;

import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ArrayAdapter;
import java.util.ArrayList;
import java.util.Queue;

final class cg implements Runnable {
    private /* synthetic */ QGridLayout a;

    cg(QGridLayout qGridLayout) {
        this.a = qGridLayout;
    }

    public final void run() {
        if (this.a.M != null && this.a.O != null) {
            int unused = this.a.am = 2;
            View a2 = this.a.M;
            int unused2 = this.a.ay = a2.getLeft();
            int unused3 = this.a.az = a2.getTop();
            ha haVar = (ha) this.a.O.getTag();
            int i = haVar.t;
            int i2 = ((ha) a2.getTag()).t;
            int c = this.a.e;
            Queue d = this.a.aF;
            bp bpVar = new bp();
            bpVar.d = a2;
            bpVar.a = this.a.O;
            bpVar.b = false;
            if (bpVar.e == null) {
                bpVar.e = new ArrayList();
            }
            AccelerateDecelerateInterpolator accelerateDecelerateInterpolator = new AccelerateDecelerateInterpolator();
            if (i2 < i) {
                int e = ((c + 1) * this.a.J) - 1;
                if (e >= this.a.getChildCount()) {
                    e = this.a.getChildCount() - 1;
                }
                int i3 = e >= i ? i - 1 : e;
                for (int i4 = i2; i4 <= i3; i4++) {
                    View childAt = this.a.getChildAt(i4);
                    View childAt2 = this.a.getChildAt(i4 + 1);
                    if (!(childAt == null || childAt2 == null)) {
                        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, (float) (childAt2.getLeft() - childAt.getLeft()), 0.0f, (float) (childAt2.getTop() - childAt.getTop()));
                        translateAnimation.setInterpolator(accelerateDecelerateInterpolator);
                        translateAnimation.setDuration(200);
                        translateAnimation.setFillAfter(true);
                        translateAnimation.setAnimationListener(bpVar);
                        childAt.startAnimation(translateAnimation);
                        ha haVar2 = (ha) childAt.getTag();
                        haVar2.t = i4 + 1;
                        d.offer(haVar2);
                        bpVar.e.add(childAt);
                        bpVar.c++;
                    }
                }
                for (int i5 = i3 + 1; i5 < i; i5++) {
                    ha haVar3 = (ha) this.a.getChildAt(i5).getTag();
                    haVar3.t = i5 + 1;
                    d.offer(haVar3);
                }
            } else {
                int e2 = c * this.a.J;
                if (e2 < 0) {
                    e2 = 0;
                }
                int i6 = e2 <= i ? i + 1 : e2;
                for (int i7 = i2; i7 >= i6; i7--) {
                    View childAt3 = this.a.getChildAt(i7);
                    View childAt4 = this.a.getChildAt(i7 - 1);
                    if (!(childAt3 == null || childAt4 == null)) {
                        TranslateAnimation translateAnimation2 = new TranslateAnimation(0.0f, (float) (childAt4.getLeft() - childAt3.getLeft()), 0.0f, (float) (childAt4.getTop() - childAt3.getTop()));
                        translateAnimation2.setInterpolator(accelerateDecelerateInterpolator);
                        translateAnimation2.setDuration(200);
                        translateAnimation2.setFillAfter(true);
                        translateAnimation2.setAnimationListener(bpVar);
                        childAt3.startAnimation(translateAnimation2);
                        ha haVar4 = (ha) childAt3.getTag();
                        haVar4.t = i7 - 1;
                        d.offer(haVar4);
                        bpVar.e.add(childAt3);
                        bpVar.c++;
                    }
                }
                for (int i8 = i6 - 1; i8 > i; i8--) {
                    ha haVar5 = (ha) this.a.getChildAt(i8).getTag();
                    haVar5.t = i8 - 1;
                    d.offer(haVar5);
                }
            }
            boolean unused4 = this.a.T = true;
            haVar.t = i2;
            d.offer(haVar);
            if (this.a.W) {
                this.a.removeViewInLayout(this.a.O);
                boolean unused5 = this.a.addViewInLayout(this.a.O, i2, this.a.O.getLayoutParams());
                ArrayAdapter h = this.a.t;
                h.setNotifyOnChange(false);
                h.remove(haVar);
                h.insert(haVar, i2);
                return;
            }
            View b = this.a.O;
            TranslateAnimation translateAnimation3 = new TranslateAnimation((float) (this.a.aA - b.getLeft()), (float) (a2.getLeft() - b.getLeft()), (float) (this.a.aB - b.getTop()), (float) (a2.getTop() - b.getTop()));
            translateAnimation3.setInterpolator(accelerateDecelerateInterpolator);
            translateAnimation3.setDuration(200);
            translateAnimation3.setFillAfter(true);
            translateAnimation3.setAnimationListener(bpVar);
            b.startAnimation(translateAnimation3);
            bpVar.b = true;
            bpVar.e.add(b);
            bpVar.c++;
        }
    }
}
