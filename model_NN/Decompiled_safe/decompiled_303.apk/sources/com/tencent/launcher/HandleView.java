package com.tencent.launcher;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import com.tencent.a.a;

public class HandleView extends ImageView {
    private Launcher a;
    private int b;

    public HandleView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public HandleView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.b = 1;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.i, i, 0);
        this.b = obtainStyledAttributes.getInt(0, 1);
        obtainStyledAttributes.recycle();
    }

    public View focusSearch(int i) {
        View focusSearch = super.focusSearch(i);
        if (focusSearch != null || !this.a.isDrawerClose()) {
            return focusSearch;
        }
        Workspace workspace = this.a.getWorkspace();
        workspace.dispatchUnhandledMove(null, i);
        return (this.b == 1 && i == 130) ? this : workspace;
    }
}
