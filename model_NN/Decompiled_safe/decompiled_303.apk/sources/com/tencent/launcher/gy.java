package com.tencent.launcher;

import android.graphics.drawable.Drawable;

public final class gy {
    public am a;
    public CharSequence b;
    public Drawable c;
    public boolean d;
    private boolean e;
    private /* synthetic */ Launcher f;

    public gy(Launcher launcher, am amVar, boolean z, boolean z2) {
        this.f = launcher;
        this.a = amVar;
        this.b = amVar.a;
        if (!amVar.f) {
            amVar.d = a.a(launcher.mContext, amVar.d, amVar);
            amVar.f = true;
        }
        this.c = amVar.d;
        this.d = z;
        this.e = z2;
    }
}
