package com.tencent.launcher;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import com.tencent.launcher.home.i;
import com.tencent.module.setting.AboutSettingActivity;
import com.tencent.qqlauncher.R;

final class hz extends Handler {
    private /* synthetic */ Launcher a;

    hz(Launcher launcher) {
        this.a = launcher;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case AboutSettingActivity.UPDATE_READY /*800*/:
                String str = AboutSettingActivity.updateUrl;
                if (str != null && !"".equals(str)) {
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                    Notification notification = new Notification();
                    PendingIntent activity = PendingIntent.getActivity(this.a, 0, intent, 0);
                    notification.icon = R.drawable.notice;
                    notification.tickerText = this.a.getString(R.string.tipupdate_title);
                    notification.defaults |= 2;
                    notification.flags |= 16;
                    notification.setLatestEventInfo(this.a, this.a.getString(R.string.tipupdate_title), this.a.getString(R.string.tipupdate_content), activity);
                    ((NotificationManager) this.a.getSystemService("notification")).notify(2515401, notification);
                    i.a().a("setting_updatetime", System.currentTimeMillis());
                    break;
                } else {
                    return;
                }
        }
        super.handleMessage(message);
    }
}
