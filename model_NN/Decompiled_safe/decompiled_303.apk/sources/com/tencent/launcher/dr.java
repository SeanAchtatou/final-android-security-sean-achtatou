package com.tencent.launcher;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.tencent.util.o;

public final class dr {
    private Bitmap a;
    private Bitmap b;
    private Bitmap c;
    private int d;
    private int e;
    private Rect f;
    private Rect g;
    private Rect h;
    private int i = -1;
    private boolean j;
    private int k = 0;

    public final dr a() {
        this.i = 0;
        return this;
    }

    public final dr a(int i2) {
        this.d = i2;
        return this;
    }

    public final dr a(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            this.a = ((BitmapDrawable) drawable).getBitmap();
        } else if (drawable instanceof hp) {
            this.a = ((hp) drawable).a();
        } else if (drawable instanceof gm) {
            this.a = ((gm) drawable).a();
        } else if (drawable instanceof gb) {
            this.a = ((gb) drawable).a();
        }
        return this;
    }

    public final dr b(int i2) {
        this.e = i2;
        return this;
    }

    public final dr b(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            this.c = ((BitmapDrawable) drawable).getBitmap();
        } else if (drawable instanceof hp) {
            this.c = ((hp) drawable).a();
        } else if (drawable instanceof gm) {
            this.c = ((gm) drawable).a();
        } else if (drawable instanceof gb) {
            this.c = ((gb) drawable).a();
        }
        return this;
    }

    public final gb b() {
        gb gbVar = new gb();
        gbVar.c(this.b);
        gbVar.a(this.d, this.e);
        gbVar.a(this.a);
        gbVar.b(this.c);
        gbVar.b(this.f);
        gbVar.a(this.g);
        gbVar.c(this.h);
        gbVar.a(this.j);
        gbVar.b(this.k);
        if (this.i != -1) {
            gbVar.a(this.i);
        }
        return gbVar;
    }

    public final dr c(int i2) {
        this.k = i2;
        return this;
    }

    public final dr c(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            this.b = ((BitmapDrawable) drawable).getBitmap();
        } else if (drawable instanceof hp) {
            this.b = ((hp) drawable).a();
        } else if (drawable instanceof gm) {
            this.b = ((gm) drawable).a();
        } else if (drawable instanceof gb) {
            this.b = ((gb) drawable).a();
        } else {
            this.b = o.a(drawable);
        }
        return this;
    }
}
