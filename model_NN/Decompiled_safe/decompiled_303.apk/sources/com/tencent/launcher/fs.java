package com.tencent.launcher;

import android.view.animation.Animation;

final class fs implements Animation.AnimationListener {
    final /* synthetic */ Search a;
    private final Runnable b = new ap(this);

    fs(Search search) {
        this.a = search;
    }

    public final void onAnimationEnd(Animation animation) {
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public final void onAnimationStart(Animation animation) {
        this.a.getHandler().postDelayed(this.b, Math.max(this.a.e.getDuration() - 80, 0L));
    }
}
