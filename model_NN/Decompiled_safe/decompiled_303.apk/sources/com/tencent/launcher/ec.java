package com.tencent.launcher;

import android.content.DialogInterface;
import java.util.ArrayList;

final class ec implements DialogInterface.OnClickListener {
    private /* synthetic */ ArrayList a;
    private /* synthetic */ Launcher b;

    ec(Launcher launcher, ArrayList arrayList) {
        this.b = launcher;
        this.a = arrayList;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        this.a.clear();
    }
}
