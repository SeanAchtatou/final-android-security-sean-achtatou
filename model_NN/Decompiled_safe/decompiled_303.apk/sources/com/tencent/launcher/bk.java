package com.tencent.launcher;

final class bk {
    private static final Object e = new Object();
    private static int f = 0;
    private static bk g;
    int a;
    int b;
    int c;
    int d;
    private bk h;

    bk() {
    }

    static bk a() {
        synchronized (e) {
            if (g == null) {
                bk bkVar = new bk();
                return bkVar;
            }
            bk bkVar2 = g;
            g = bkVar2.h;
            f--;
            return bkVar2;
        }
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        synchronized (e) {
            if (f < 100) {
                f++;
                this.h = g;
                g = this;
            }
        }
    }

    public final String toString() {
        return "VacantCell[x=" + this.a + ", y=" + this.b + ", spanX=" + this.c + ", spanY=" + this.d + "]";
    }
}
