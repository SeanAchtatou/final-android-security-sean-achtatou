package com.tencent.launcher;

import android.view.GestureDetector;
import android.view.MotionEvent;

final class gf implements GestureDetector.OnGestureListener {
    private /* synthetic */ DrawerBar a;

    gf(DrawerBar drawerBar) {
        this.a = drawerBar;
    }

    public final boolean onDown(MotionEvent motionEvent) {
        this.a.a = false;
        return false;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        float abs;
        switch (this.a.g) {
            case 0:
                if (f2 < 0.0f && Math.abs(f2) > Math.abs(f)) {
                    abs = Math.abs(f2);
                    break;
                }
                abs = 0.0f;
                break;
            case 1:
                if (f2 > 0.0f && Math.abs(f2) > Math.abs(f)) {
                    abs = Math.abs(f2);
                    break;
                }
                abs = 0.0f;
                break;
            case 2:
                if (f < 0.0f && Math.abs(f2) < Math.abs(f)) {
                    abs = Math.abs(f);
                    break;
                }
                abs = 0.0f;
                break;
            case 3:
                if (f > 0.0f && Math.abs(f2) < Math.abs(f)) {
                    abs = Math.abs(f);
                    break;
                }
                abs = 0.0f;
                break;
            default:
                abs = 0.0f;
                break;
        }
        if (abs <= 15.0f) {
            return false;
        }
        this.a.a();
        this.a.a = true;
        return true;
    }

    public final void onLongPress(MotionEvent motionEvent) {
    }

    public final boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return false;
    }

    public final void onShowPress(MotionEvent motionEvent) {
    }

    public final boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }
}
