package com.tencent.launcher;

import android.view.View;

final class cq implements f {
    private /* synthetic */ Launcher a;

    cq(Launcher launcher) {
        this.a = launcher;
    }

    public final void a(View view) {
        if (view == this.a.mDockBar) {
            this.a.mDockButtonGroup.a();
        } else if (view == this.a.mDockButtonGroup) {
            this.a.mDockBar.e();
        }
    }
}
