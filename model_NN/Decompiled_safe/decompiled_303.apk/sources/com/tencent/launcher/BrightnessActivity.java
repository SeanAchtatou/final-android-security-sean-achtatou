package com.tencent.launcher;

import android.app.Activity;
import android.content.ContentResolver;
import android.os.Bundle;
import android.provider.Settings;
import android.view.WindowManager;
import java.util.Timer;

public class BrightnessActivity extends Activity {
    public static final String BRIGHT_MODE = "screen_brightness_mode";
    public static final int BRIGHT_MODE_AUTO = 1;
    public static final int BRIGHT_MODE_MANUAL = 0;
    public static final int DEFAULT_BACKLIGHT = 128;
    public static final int MAXIMUM_BACKLIGHT = 255;
    public static final int MINIMUM_BACKLIGHT = 30;

    public void finish() {
        super.finish();
    }

    public void onCreate(Bundle bundle) {
        int i;
        int i2;
        super.onCreate(bundle);
        try {
            ContentResolver contentResolver = getContentResolver();
            int i3 = Settings.System.getInt(contentResolver, "screen_brightness");
            int i4 = Settings.System.getInt(contentResolver, BRIGHT_MODE);
            if (i4 == 1) {
                i = 30;
                i2 = 0;
            } else if (i3 < 128) {
                i2 = i4;
                i = 128;
            } else if (i3 < 255) {
                i2 = i4;
                i = 255;
            } else {
                i = i3;
                i2 = 1;
            }
            Settings.System.putInt(getContentResolver(), BRIGHT_MODE, i2);
            if (i2 == 0) {
                Settings.System.putInt(contentResolver, "screen_brightness", i);
                setBrightness(i);
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        new Timer().schedule(new r(this), 500);
    }

    public void setBrightness(int i) {
        Settings.System.putInt(getContentResolver(), "screen_brightness", i);
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.screenBrightness = ((float) i) / 255.0f;
        getWindow().setAttributes(attributes);
    }
}
