package com.tencent.launcher;

import android.view.View;

final class ci implements View.OnLongClickListener {
    private /* synthetic */ QGridLayout a;

    ci(QGridLayout qGridLayout) {
        this.a = qGridLayout;
    }

    public final boolean onLongClick(View view) {
        if (!view.isInTouchMode()) {
            return true;
        }
        this.a.q();
        this.a.a(view);
        return true;
    }
}
