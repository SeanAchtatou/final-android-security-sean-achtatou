package com.tencent.launcher;

import android.view.View;
import com.tencent.launcher.DockBar;

final class ez implements Runnable {
    private /* synthetic */ DockBar a;

    ez(DockBar dockBar) {
        this.a = dockBar;
    }

    public final void run() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.a.getChildCount()) {
                View childAt = this.a.getChildAt(i2);
                ha haVar = (ha) childAt.getTag();
                if (haVar != null) {
                    ff.a(this.a.d, haVar, -200, -1, ((DockBar.LayoutParams) childAt.getLayoutParams()).a, -1);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }
}
