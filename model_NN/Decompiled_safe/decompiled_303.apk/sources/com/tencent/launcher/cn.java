package com.tencent.launcher;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.tencent.launcher.CustomMenuActivity;
import com.tencent.qqlauncher.R;

final class cn extends BaseAdapter {
    private /* synthetic */ CustomMenuActivity.MenuDialog a;

    /* synthetic */ cn(CustomMenuActivity.MenuDialog menuDialog) {
        this(menuDialog, (byte) 0);
    }

    private cn(CustomMenuActivity.MenuDialog menuDialog, byte b) {
        this.a = menuDialog;
    }

    public final int getCount() {
        return CustomMenuActivity.this.mMenuItem.size();
    }

    public final Object getItem(int i) {
        return null;
    }

    public final long getItemId(int i) {
        return 0;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? CustomMenuActivity.this.getLayoutInflater().inflate((int) R.layout.item_menu, (ViewGroup) null) : view;
        TextView textView = (TextView) inflate.findViewById(R.id.item_text);
        textView.setText(((gg) CustomMenuActivity.this.mMenuItem.get(i)).a());
        textView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, ((gg) CustomMenuActivity.this.mMenuItem.get(i)).b(), (Drawable) null, (Drawable) null);
        if (!((gg) CustomMenuActivity.this.mMenuItem.get(i)).c().isEnabled()) {
            textView.setTextColor(Color.parseColor("#775e697b"));
            ((gg) CustomMenuActivity.this.mMenuItem.get(i)).b().setAlpha(77);
        }
        inflate.setTag(CustomMenuActivity.this.mMenuItem.get(i));
        inflate.setOnClickListener(this.a);
        return inflate;
    }
}
