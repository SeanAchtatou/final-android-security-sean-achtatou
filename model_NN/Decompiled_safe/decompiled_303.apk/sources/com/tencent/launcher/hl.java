package com.tencent.launcher;

import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import com.tencent.qqlauncher.R;

final class hl implements View.OnClickListener {
    private /* synthetic */ ThumbnailWorkspace a;

    hl(ThumbnailWorkspace thumbnailWorkspace) {
        this.a = thumbnailWorkspace;
    }

    public final void onClick(View view) {
        if (this.a.e() || this.a.B) {
            return;
        }
        if (this.a.getChildCount() <= 2) {
            Toast.makeText(this.a.b, (int) R.string.notify_deletescreen_failed, 0).show();
            return;
        }
        ImageView imageView = (ImageView) ((View) view.getParent()).findViewById(R.id.home_light);
        if (imageView == null || imageView.getVisibility() != 0) {
            boolean unused = this.a.s = true;
            if (view.getTag() != null) {
                ThumbnailWorkspace.a(this.a, view);
            } else {
                ThumbnailWorkspace.b(this.a, view);
            }
        } else {
            Toast.makeText(this.a.b, (int) R.string.notify_delete_homescreen_failed, 0).show();
        }
    }
}
