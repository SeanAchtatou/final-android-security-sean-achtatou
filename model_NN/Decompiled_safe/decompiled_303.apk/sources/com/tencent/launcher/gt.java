package com.tencent.launcher;

import android.appwidget.AppWidgetHostView;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import com.tencent.qqlauncher.R;

public final class gt extends AppWidgetHostView {
    /* access modifiers changed from: private */
    public boolean a;
    private cj b;
    private LayoutInflater c;

    public gt(Context context) {
        super(context);
        this.c = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    public final void cancelLongPress() {
        super.cancelLongPress();
        this.a = false;
        if (this.b != null) {
            removeCallbacks(this.b);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.tencent.launcher.gt, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public final View getErrorView() {
        return this.c.inflate((int) R.layout.appwidget_error, (ViewGroup) this, false);
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.a) {
            this.a = false;
            return true;
        }
        switch (motionEvent.getAction()) {
            case 0:
                this.a = false;
                if (this.b == null) {
                    this.b = new cj(this);
                }
                this.b.a();
                postDelayed(this.b, (long) ViewConfiguration.getLongPressTimeout());
                break;
            case 1:
            case 3:
                this.a = false;
                if (this.b != null) {
                    removeCallbacks(this.b);
                    break;
                }
                break;
        }
        return false;
    }
}
