package com.tencent.launcher;

import android.view.animation.Animation;

final class fk implements Animation.AnimationListener {
    private /* synthetic */ DrawerBar a;

    fk(DrawerBar drawerBar) {
        this.a = drawerBar;
    }

    public final void onAnimationEnd(Animation animation) {
        this.a.setVisibility(8);
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
