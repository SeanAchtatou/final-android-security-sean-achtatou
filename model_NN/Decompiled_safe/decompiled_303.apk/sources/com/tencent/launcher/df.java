package com.tencent.launcher;

import AndroidDLoader.Software;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import com.tencent.module.appcenter.d;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

final class df extends ArrayAdapter {
    public String a;
    private Context b;
    private final LayoutInflater c;
    private Handler d = new gx(this);
    private /* synthetic */ SearchAppTestActivity e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public df(SearchAppTestActivity searchAppTestActivity, Context context, ArrayList arrayList) {
        super(context, 0, arrayList);
        this.e = searchAppTestActivity;
        this.b = context;
        this.c = (LayoutInflater) this.b.getSystemService("layout_inflater");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.module.appcenter.d.a(java.lang.String, android.os.Handler, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, android.os.Handler, int]
     candidates:
      com.tencent.module.appcenter.d.a(android.os.Handler, java.lang.String, int):int
      com.tencent.module.appcenter.d.a(android.os.Handler, int, int):void
      com.tencent.module.appcenter.d.a(android.os.Handler, java.util.ArrayList, com.tencent.android.ui.a.ae):void
      com.tencent.android.net.d.a(android.os.Handler, int, int):void
      com.tencent.module.appcenter.d.a(java.lang.String, android.os.Handler, boolean):android.graphics.Bitmap */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = (view == null || view.getTag() == null) ? this.c.inflate((int) R.layout.searchnetwork_list_item, (ViewGroup) null) : view;
        Software software = (Software) getItem(i);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.SearchAppIcon);
        Bitmap a2 = d.a().a(software.c, this.d, true);
        if (a2 != null) {
            imageView.setImageBitmap(a2);
        } else {
            imageView.setImageResource(R.drawable.sw_default_icon);
        }
        ((TextView) inflate.findViewById(R.id.SearchAppName)).setText(software.b);
        ((TextView) inflate.findViewById(R.id.SearchAppDownloadTimes)).setText(software.n + "人下载");
        ((RatingBar) inflate.findViewById(R.id.SearchAppStar)).setRating((float) (software.m / 2));
        inflate.setTag(software);
        return inflate;
    }
}
