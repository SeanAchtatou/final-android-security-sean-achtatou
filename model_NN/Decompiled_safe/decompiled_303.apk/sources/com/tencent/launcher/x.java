package com.tencent.launcher;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import com.tencent.launcher.DockBar;
import com.tencent.qqlauncher.R;

final class x extends AlertDialog {
    final /* synthetic */ Launcher a;
    private EditText b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    protected x(Launcher launcher, Context context) {
        super(context);
        this.a = launcher;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private void a(bq bqVar, String str) {
        String replace = str.trim().replace(10, ' ');
        if (replace.length() == 0 || replace.length() > 20) {
            bqVar.h = this.a.getText(R.string.folder_name);
        } else {
            bqVar.h = replace;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(boolean, com.tencent.launcher.Launcher, boolean, boolean):void
     arg types: [int, com.tencent.launcher.Launcher, int, int]
     candidates:
      com.tencent.launcher.ff.a(boolean[][], int, int, int):void
      com.tencent.launcher.ff.a(boolean, com.tencent.launcher.Launcher, boolean, boolean):void */
    static /* synthetic */ void a(x xVar) {
        String trim = xVar.b.getText().toString().replace(10, ' ').trim();
        if (!TextUtils.isEmpty(trim) && trim.length() <= 20) {
            if (Launcher.sModel.a(xVar.a.mFolderInfo.l) == null) {
                ex unused = xVar.a.mFolderInfo = Launcher.sModel.b(xVar.a.mFolderInfo.l);
                xVar.a.mFolderInfo.h = trim;
                ff.d(xVar.a.getBaseContext(), xVar.a.mFolderInfo);
            } else {
                ex unused2 = xVar.a.mFolderInfo = Launcher.sModel.a(xVar.a.mFolderInfo.l);
                xVar.a.mFolderInfo.h = trim;
                ff.e(xVar.a, xVar.a.mFolderInfo);
            }
            if (xVar.a.mDesktopLocked) {
                xVar.a.mDrawer.h();
                Launcher.sModel.a(false, xVar.a, false, false);
            } else {
                FolderIcon folderIcon = (FolderIcon) xVar.a.mWorkspace.b(xVar.a.mFolderInfo);
                if (folderIcon != null) {
                    folderIcon.setText(trim);
                    folderIcon.invalidate();
                    xVar.a.getWorkspace().requestLayout();
                } else {
                    boolean unused3 = xVar.a.mDesktopLocked = true;
                    xVar.a.mDrawer.h();
                    Launcher.sModel.a(false, xVar.a, false, false);
                }
            }
        }
        xVar.a.mWorkspace.invalidate();
        xVar.d();
    }

    /* access modifiers changed from: private */
    public void d() {
        this.a.mWorkspace.z();
        boolean unused = this.a.flagDockEnable = true;
        if (this.a.mDrawer.a() && this.a.mGrid.c()) {
            this.a.mGrid.a().i();
        }
        this.a.dismissDialog(2);
        boolean unused2 = this.a.mWaitingForResult = false;
        ex unused3 = this.a.mFolderInfo = null;
        this.b.setText("");
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        bq bqVar = new bq();
        a(bqVar, this.b.getText().toString());
        QGridLayout a2 = this.a.mGrid.a();
        if (this.a.mGrid.c() && a2 != null) {
            a2.a(bqVar.h.toString(), a2.b());
        }
        d();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void
     arg types: [com.tencent.launcher.Launcher, com.tencent.launcher.bq, int, int, int, int, int]
     candidates:
      com.tencent.launcher.ff.a(android.database.Cursor, android.content.Context, int, int, int, int, android.content.Intent):com.tencent.launcher.am
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public final void b() {
        bq bqVar = new bq();
        a(bqVar, this.b.getText().toString());
        DockView access$2800 = this.a.mAddDockItem;
        if (access$2800 != null) {
            ff.a((Context) this.a, (ha) bqVar, -200L, -1, ((DockBar.LayoutParams) access$2800.getLayoutParams()).a, -1, false);
            Launcher.sModel.a((ha) bqVar);
            Launcher.sModel.a((ex) bqVar);
            access$2800.a(bqVar);
            DockView unused = this.a.mAddDockItem = null;
            d();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void
     arg types: [android.content.Context, com.tencent.launcher.bq, int, int, int, int, int]
     candidates:
      com.tencent.launcher.ff.a(android.database.Cursor, android.content.Context, int, int, int, int, android.content.Intent):com.tencent.launcher.am
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, boolean):void
     arg types: [com.tencent.launcher.FolderIcon, int, int, int, int, boolean]
     candidates:
      com.tencent.launcher.Workspace.a(com.tencent.launcher.cm, int, int, java.lang.Object, com.tencent.launcher.CellLayout, boolean):void
      com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, int):void
      com.tencent.launcher.Workspace.a(com.tencent.launcher.cm, int, int, int, int, java.lang.Object):boolean
      com.tencent.launcher.e.a(com.tencent.launcher.cm, int, int, int, int, java.lang.Object):boolean
      com.tencent.launcher.gw.a(com.tencent.launcher.cm, int, int, int, int, java.lang.Object):boolean
      com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public final void c() {
        bq bqVar = new bq();
        a(bqVar, this.b.getText().toString());
        bs access$3000 = this.a.mAddItemCellInfo;
        access$3000.f = this.a.mWorkspace.d();
        if (this.a.findSingleSlot(access$3000)) {
            ff.a(this.a.mContext, (ha) bqVar, -100L, this.a.mWorkspace.d(), access$3000.b, access$3000.c, false);
            Launcher.sModel.a((ha) bqVar);
            Launcher.sModel.a((ex) bqVar);
            this.a.mWorkspace.a((View) FolderIcon.a(this.a, (ViewGroup) this.a.mWorkspace.getChildAt(this.a.mWorkspace.d()), bqVar), access$3000.b, access$3000.c, 1, 1, !this.a.mDesktopLocked);
            d();
        }
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        View inflate = View.inflate(this.a, R.layout.rename_folder, null);
        getWindow().setContentView(inflate);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(-2, -2, 0, -60, 2, 2, -3);
        layoutParams.dimAmount = 0.5f;
        getWindow().setAttributes(layoutParams);
        getWindow().getAttributes().windowAnimations = 16973826;
        getWindow().setSoftInputMode(37);
        this.b = (EditText) inflate.findViewById(R.id.folder_name);
        inflate.findViewById(R.id.button1).setOnClickListener(new j(this, inflate));
        inflate.findViewById(R.id.button1).requestFocus();
        inflate.findViewById(R.id.button2).setOnClickListener(new i(this));
        setOnCancelListener(new h(this));
    }

    public final void show() {
        findViewById(R.id.button1).setEnabled(true);
        super.show();
    }
}
