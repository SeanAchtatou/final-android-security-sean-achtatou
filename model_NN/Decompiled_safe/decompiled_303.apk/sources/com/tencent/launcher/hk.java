package com.tencent.launcher;

import android.view.View;
import android.widget.ImageView;
import com.tencent.qqlauncher.R;

final class hk implements View.OnClickListener {
    private /* synthetic */ ThumbnailWorkspace a;

    hk(ThumbnailWorkspace thumbnailWorkspace) {
        this.a = thumbnailWorkspace;
    }

    public final void onClick(View view) {
        if (!this.a.e()) {
            int childCount = this.a.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = this.a.getChildAt(i);
                ImageView imageView = (ImageView) childAt.findViewById(R.id.home_light);
                View findViewById = childAt.findViewById(R.id.home_button);
                if (imageView != null) {
                    imageView.setVisibility(4);
                }
                if (findViewById == view) {
                    imageView.setVisibility(0);
                }
            }
            this.a.a(true);
        }
    }
}
