package com.tencent.launcher;

public final class cw {
    private int a;
    private float[] b = new float[10];
    private float[] c = new float[10];
    private float[] d = new float[10];
    private int[] e = new int[10];
    private float f;
    private float g;
    private float h;
    private float i;
    private float j;
    private float k;
    private float l;
    private float m;
    private boolean n;
    private boolean o;
    private boolean p;
    private boolean q;
    private boolean r;
    private int s;
    /* access modifiers changed from: private */
    public long t;

    static /* synthetic */ void a(cw cwVar, int i2, float[] fArr, float[] fArr2, float[] fArr3, int[] iArr, int i3, boolean z, long j2) {
        cwVar.t = j2;
        cwVar.s = i3;
        cwVar.a = i2;
        for (int i4 = 0; i4 < i2; i4++) {
            cwVar.b[i4] = fArr[i4];
            cwVar.c[i4] = fArr2[i4];
            cwVar.d[i4] = fArr3[i4];
            cwVar.e[i4] = iArr[i4];
        }
        cwVar.n = z;
        cwVar.o = i2 >= 2;
        if (cwVar.o) {
            cwVar.f = (fArr[0] + fArr[1]) * 0.5f;
            cwVar.g = (fArr2[0] + fArr2[1]) * 0.5f;
            cwVar.h = (fArr3[0] + fArr3[1]) * 0.5f;
            cwVar.i = Math.abs(fArr[1] - fArr[0]);
            cwVar.j = Math.abs(fArr2[1] - fArr2[0]);
        } else {
            cwVar.f = fArr[0];
            cwVar.g = fArr2[0];
            cwVar.h = fArr3[0];
            cwVar.j = 0.0f;
            cwVar.i = 0.0f;
        }
        cwVar.r = false;
        cwVar.q = false;
        cwVar.p = false;
    }

    public final boolean a() {
        return this.o;
    }

    public final float b() {
        if (this.o) {
            return this.i;
        }
        return 0.0f;
    }

    public final float c() {
        if (this.o) {
            return this.j;
        }
        return 0.0f;
    }

    public final float d() {
        int i2;
        int i3;
        float f2;
        if (!this.q) {
            if (!this.o) {
                this.k = 0.0f;
            } else {
                if (!this.p) {
                    this.l = this.o ? (this.i * this.i) + (this.j * this.j) : 0.0f;
                    this.p = true;
                }
                float f3 = this.l;
                if (f3 == 0.0f) {
                    f2 = 0.0f;
                } else {
                    int i4 = (int) (f3 * 256.0f);
                    int i5 = 15;
                    int i6 = 32768;
                    int i7 = 0;
                    while (true) {
                        int i8 = i5 - 1;
                        int i9 = ((i7 << 1) + i6) << i5;
                        if (i4 >= i9) {
                            int i10 = i7 + i6;
                            i3 = i4 - i9;
                            i2 = i10;
                        } else {
                            i2 = i7;
                            i3 = i4;
                        }
                        i6 >>= 1;
                        if (i6 <= 0) {
                            break;
                        }
                        i4 = i3;
                        i7 = i2;
                        i5 = i8;
                    }
                    f2 = ((float) i2) / 16.0f;
                }
                this.k = f2;
                if (this.k < this.i) {
                    this.k = this.i;
                }
                if (this.k < this.j) {
                    this.k = this.j;
                }
            }
            this.q = true;
        }
        return this.k;
    }

    public final float e() {
        if (!this.r) {
            if (!this.o) {
                this.m = 0.0f;
            } else {
                this.m = (float) Math.atan2((double) (this.c[1] - this.c[0]), (double) (this.b[1] - this.b[0]));
            }
            this.r = true;
        }
        return this.m;
    }

    public final float f() {
        return this.f;
    }

    public final float g() {
        return this.g;
    }

    public final boolean h() {
        return this.n;
    }

    public final long i() {
        return this.t;
    }
}
