package com.tencent.launcher;

import android.os.Parcel;
import android.os.Parcelable;
import com.tencent.launcher.Workspace;

final class ij implements Parcelable.Creator {
    ij() {
    }

    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new Workspace.SavedState(parcel, null);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new Workspace.SavedState[i];
    }
}
