package com.tencent.launcher.home;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class c {
    public static String a(String str, StringBuffer stringBuffer) {
        Matcher matcher = Pattern.compile("([b|p|m|f|d|t|n|l|g|k|h|j|q|x|r|z|c|s|y|w|a|e|o])").matcher(str);
        if (matcher.find()) {
            MatchResult matchResult = matcher.toMatchResult();
            if (matchResult.group(1) == null) {
                return null;
            }
            stringBuffer.append(matchResult.group(1));
            return null;
        }
        stringBuffer.append(str);
        return null;
    }

    public static String[] a(char c) {
        int GetUcs2PinyinNum = JNI.GetUcs2PinyinNum(c);
        if (GetUcs2PinyinNum <= 0) {
            return null;
        }
        String[] strArr = new String[GetUcs2PinyinNum];
        for (int i = 0; i < GetUcs2PinyinNum; i++) {
            strArr[i] = JNI.GetUcs2Pinyin(c, i);
        }
        return strArr;
    }

    public static String[] a(String str) {
        int length = str.length();
        String[] strArr = new String[length];
        for (int i = 0; i < length; i++) {
            String GetUcs2Pinyin = JNI.GetUcs2Pinyin(str.charAt(i));
            if (GetUcs2Pinyin == null || GetUcs2Pinyin.length() == 0) {
                strArr[i] = "" + str.charAt(i);
            } else {
                strArr[i] = GetUcs2Pinyin;
            }
        }
        return strArr;
    }

    public static String b(String str) {
        String trim = str.trim();
        int length = trim.length();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < length; i++) {
            String GetUcs2Pinyin = JNI.GetUcs2Pinyin(trim.charAt(i));
            if (GetUcs2Pinyin == null || GetUcs2Pinyin.length() == 0) {
                stringBuffer.append(trim.charAt(i));
            } else {
                stringBuffer.append(GetUcs2Pinyin);
            }
        }
        return stringBuffer.toString();
    }

    public static String c(String str) {
        int length = str.length();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < length; i++) {
            String GetUcs2Pinyin = JNI.GetUcs2Pinyin(str.charAt(i));
            if (GetUcs2Pinyin == null || GetUcs2Pinyin.length() == 0) {
                stringBuffer.append(str.charAt(i));
            } else {
                stringBuffer.append(GetUcs2Pinyin);
            }
        }
        return stringBuffer.toString();
    }

    public static String d(String str) {
        String trim = str.trim();
        trim.length();
        StringBuffer stringBuffer = new StringBuffer();
        StringBuffer stringBuffer2 = new StringBuffer();
        int length = trim.length();
        for (int i = 0; i < length; i++) {
            String GetUcs2Pinyin = JNI.GetUcs2Pinyin(trim.charAt(i));
            if (GetUcs2Pinyin == null || GetUcs2Pinyin.length() == 0) {
                stringBuffer.append(trim.charAt(i));
                stringBuffer2.append(trim.charAt(i));
            } else {
                a(GetUcs2Pinyin, stringBuffer);
                stringBuffer2.append(GetUcs2Pinyin);
            }
        }
        return stringBuffer2.toString() + stringBuffer.toString();
    }
}
