package com.tencent.launcher.home;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.tencent.launcher.Launcher;
import com.tencent.launcher.ff;
import com.tencent.module.setting.AboutSettingActivity;
import com.tencent.util.p;

public final class a {
    private static String a = "";

    public static String a(Context context) {
        a = "";
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        if (TextUtils.isEmpty(deviceId) || deviceId.length() <= 5 || deviceId.contains("000000")) {
            SharedPreferences sharedPreferences = context.getSharedPreferences("xml_stats", 0);
            String string = sharedPreferences.getString("imei", null);
            if (TextUtils.isEmpty(string)) {
                string = "imr" + ((int) (Math.random() * 2.147483647E9d));
                sharedPreferences.edit().putString("imei", string).commit();
            }
            deviceId = string;
        }
        a += "10005,";
        a += ((int) AboutSettingActivity.launcherAPPID) + ",";
        a += "QQPro,";
        a += ",";
        a += deviceId + ",";
        a += ",";
        a += "|,";
        a += "default_desktop=" + (p.a(context) ? 1 : 0) + "|";
        a += "platform=" + "Android" + "|";
        a += "subplatform=" + Build.VERSION.SDK + "|";
        a += "version=" + "0" + "|";
        StringBuilder append = new StringBuilder().append(a).append("AppCount=");
        Launcher.getModel();
        a = append.append(ff.a(context)).append("|").toString();
        StringBuilder append2 = new StringBuilder().append(a).append("ShortCutCount=");
        Launcher.getModel();
        a = append2.append(ff.c(context)).append("|").toString();
        StringBuilder append3 = new StringBuilder().append(a).append("WidgetCount=");
        Launcher.getModel();
        a = append3.append(ff.b(context)).append("|").toString();
        StringBuilder append4 = new StringBuilder().append(a).append("ScreenCount=");
        Launcher.getModel();
        a = append4.append(ff.c()).append("|").toString();
        StringBuilder append5 = new StringBuilder().append(a).append("FolderCountInAppList=");
        Launcher.getModel();
        a = append5.append(ff.d(context)).append("|").toString();
        StringBuilder append6 = new StringBuilder().append(a).append("FolderCountInDesktop=");
        Launcher.getModel();
        a = append6.append(ff.e(context)).append("|").toString();
        StringBuilder append7 = new StringBuilder().append(a).append("QQMoveFunc=");
        SharedPreferences sharedPreferences2 = context.getSharedPreferences("xml_stats", 0);
        boolean z = sharedPreferences2.getBoolean("use_qqmove_func", false);
        if (z) {
            sharedPreferences2.edit().putBoolean("use_qqmove_func", false);
        }
        a = append7.append(z ? 1 : 0).append("|").toString();
        a += "AppStoreVisitCount=" + c(context, "fn_appstore") + "|";
        String str = a + "SearchCount=" + c(context, "fn_appstore");
        a = str;
        return str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.home.a.a(android.content.Context, java.lang.String, boolean):void
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.tencent.launcher.home.a.a(android.content.Context, java.lang.String, long):void
      com.tencent.launcher.home.a.a(android.content.Context, java.lang.String, boolean):void */
    public static void a(Context context, String str) {
        long currentTimeMillis = System.currentTimeMillis() + 28800000;
        long j = currentTimeMillis % 86400000 == 0 ? currentTimeMillis / 86400000 : (currentTimeMillis / 86400000) + 1;
        long d = d(context, str);
        if (d == 0) {
            a(context, str, false);
            a(context, str, j);
        } else if (j - d <= 0) {
            a(context, str, false);
        } else if (j - d <= 1) {
            b(context, str, false);
            a(context, str, true);
            a(context, str, j);
        } else {
            b(context, str, true);
            a(context, str, true);
            a(context, str, j);
        }
    }

    private static void a(Context context, String str, long j) {
        context.getSharedPreferences("xml_stats", 0).edit().putLong(str + "_time", j).commit();
    }

    private static void a(Context context, String str, boolean z) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("xml_stats", 0);
        if (!z) {
            sharedPreferences.edit().putInt(str, sharedPreferences.getInt(str, 0) + 1).commit();
            return;
        }
        sharedPreferences.edit().putInt(str, 1).commit();
    }

    public static void b(Context context) {
        context.getSharedPreferences("xml_stats", 0).edit().putBoolean("use_qqmove_func", true);
    }

    public static void b(Context context, String str) {
        context.getSharedPreferences("xml_stats", 0).edit().remove(str).remove(str + "_time").remove(str + "_predix").commit();
    }

    private static void b(Context context, String str, boolean z) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("xml_stats", 0);
        if (!z) {
            sharedPreferences.edit().putInt(str + "_predix", sharedPreferences.getInt(str, 0)).commit();
            sharedPreferences.edit().putInt(str, 0).commit();
            return;
        }
        sharedPreferences.edit().putInt(str + "_predix", 0).commit();
    }

    private static int c(Context context, String str) {
        long currentTimeMillis = System.currentTimeMillis() + 28800000;
        long j = currentTimeMillis % 86400000 == 0 ? currentTimeMillis / 86400000 : (currentTimeMillis / 86400000) + 1;
        long d = d(context, str);
        if (d == 0) {
            return 0;
        }
        if (j - d <= 0) {
            return context.getSharedPreferences("xml_stats", 0).getInt(str + "_predix", 0);
        }
        if (j - d > 1) {
            return 0;
        }
        b(context, str, false);
        a(context, str, j);
        return context.getSharedPreferences("xml_stats", 0).getInt(str + "_predix", 0);
    }

    private static long d(Context context, String str) {
        return context.getSharedPreferences("xml_stats", 0).getLong(str + "_time", 0);
    }
}
