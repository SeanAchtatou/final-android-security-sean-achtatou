package com.tencent.launcher.home;

import com.tencent.launcher.base.b;

public class JNI {
    static {
        try {
            System.loadLibrary("launcher");
        } catch (Throwable th) {
            th.printStackTrace();
            System.load("/data/data/" + b.e + "/lib/liblauncher.so");
        }
    }

    public static native String GetUcs2Pinyin(char c);

    public static native String GetUcs2Pinyin(char c, int i);

    public static native int GetUcs2PinyinNum(char c);
}
