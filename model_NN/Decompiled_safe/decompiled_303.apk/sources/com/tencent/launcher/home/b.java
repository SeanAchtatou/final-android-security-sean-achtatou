package com.tencent.launcher.home;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.MenuItem;
import com.tencent.module.theme.c;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

public final class b {
    private static c a;
    private static ArrayList b = new ArrayList();

    public static void a(Menu menu, Context context) {
        if (a != null) {
            int size = menu.size();
            for (int i = 0; i < size; i++) {
                MenuItem item = menu.getItem(i);
                switch (item.getItemId()) {
                    case 2:
                        Drawable a2 = a.a("ic_menu_add");
                        if (a2 == null) {
                            Drawable drawable = context.getResources().getDrawable(R.drawable.ic_menu_add);
                            if (drawable == null) {
                                break;
                            } else {
                                item.setIcon(drawable);
                                break;
                            }
                        } else {
                            item.setIcon(a2);
                            break;
                        }
                    case 3:
                        Drawable a3 = a.a("ic_menu_gallery");
                        if (a3 == null) {
                            Drawable drawable2 = context.getResources().getDrawable(R.drawable.ic_menu_gallery);
                            if (drawable2 == null) {
                                break;
                            } else {
                                item.setIcon(drawable2);
                                break;
                            }
                        } else {
                            item.setIcon(a3);
                            break;
                        }
                    case 4:
                        Drawable a4 = a.a("ic_menu_search");
                        if (a4 == null) {
                            Drawable drawable3 = context.getResources().getDrawable(R.drawable.ic_menu_search);
                            if (drawable3 == null) {
                                break;
                            } else {
                                item.setIcon(drawable3);
                                break;
                            }
                        } else {
                            item.setIcon(a4);
                            break;
                        }
                    case 5:
                        Drawable a5 = a.a("ic_menu_notifications");
                        if (a5 == null) {
                            Drawable drawable4 = context.getResources().getDrawable(R.drawable.ic_menu_gallery);
                            if (drawable4 == null) {
                                break;
                            } else {
                                item.setIcon(drawable4);
                                break;
                            }
                        } else {
                            item.setIcon(a5);
                            break;
                        }
                    case 6:
                        Drawable a6 = a.a("ic_menu_preferences");
                        if (a6 == null) {
                            Drawable drawable5 = context.getResources().getDrawable(R.drawable.ic_menu_preferences);
                            if (drawable5 == null) {
                                break;
                            } else {
                                item.setIcon(drawable5);
                                break;
                            }
                        } else {
                            item.setIcon(a6);
                            break;
                        }
                }
            }
        }
    }

    public static void a(Object obj) {
        b.add(new h(obj));
    }
}
