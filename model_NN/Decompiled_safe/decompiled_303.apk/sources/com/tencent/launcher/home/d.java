package com.tencent.launcher.home;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

final class d implements DialogInterface.OnClickListener {
    private /* synthetic */ Context a;

    d(Context context) {
        this.a = context;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Context context = this.a;
        File file = new File(context.getCacheDir() + "/center.apk");
        if (file.exists()) {
            file.delete();
        }
        try {
            InputStream open = context.getAssets().open("center.db");
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            byte[] bArr = new byte[8192];
            for (int read = open.read(bArr); read > 0; read = open.read(bArr)) {
                fileOutputStream.write(bArr, 0, read);
            }
            fileOutputStream.flush();
            fileOutputStream.close();
            open.close();
            Runtime.getRuntime().exec("chmod 777 " + file.getAbsolutePath());
            new Handler().postDelayed(new e(file, context), 500);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
