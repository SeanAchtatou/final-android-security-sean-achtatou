package com.tencent.launcher.home;

import android.content.Context;
import com.tencent.module.setting.AboutSettingActivity;
import com.tencent.module.setting.ac;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

public final class g extends Thread {
    private static final char[] b = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private WeakReference a;

    public g(Context context) {
        this.a = new WeakReference(context);
    }

    private static boolean a(String str) {
        String str2;
        try {
            byte[] b2 = new ac().b(str.getBytes(), AboutSettingActivity.TEAkey);
            if (b2 == null || b2.length == 0) {
                str2 = null;
            } else {
                char[] cArr = new char[(b2.length * 2)];
                for (int i = 0; i < b2.length; i++) {
                    byte b3 = b2[i];
                    cArr[(i * 2) + 1] = b[b3 & 15];
                    cArr[(i * 2) + 0] = b[((byte) (b3 >>> 4)) & 15];
                }
                str2 = new String(cArr);
            }
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL("http://kiss.3g.qq.com/activeQQ/report/login/?LOG=" + str2).openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setInstanceFollowRedirects(true);
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpURLConnection.connect();
            new DataOutputStream(httpURLConnection.getOutputStream()).close();
            InputStream inputStream = httpURLConnection.getInputStream();
            if (inputStream.read() == 48) {
                inputStream.close();
                httpURLConnection.disconnect();
                return true;
            }
            inputStream.close();
            httpURLConnection.disconnect();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r4 = this;
            java.lang.System.currentTimeMillis()
            r0 = 10
            android.os.Process.setThreadPriority(r0)     // Catch:{ all -> 0x005f }
            java.lang.ref.WeakReference r0 = r4.a     // Catch:{ all -> 0x005f }
            java.lang.Object r4 = r0.get()     // Catch:{ all -> 0x005f }
            android.content.Context r4 = (android.content.Context) r4     // Catch:{ all -> 0x005f }
            if (r4 != 0) goto L_0x0013
        L_0x0012:
            return
        L_0x0013:
            java.lang.String r0 = "xml_stats"
            r1 = 0
            android.content.SharedPreferences r0 = r4.getSharedPreferences(r0, r1)     // Catch:{ all -> 0x005f }
            java.lang.String r1 = "stats_date"
            java.lang.String r2 = ""
            java.lang.String r1 = r0.getString(r1, r2)     // Catch:{ all -> 0x005f }
            java.text.SimpleDateFormat r2 = new java.text.SimpleDateFormat     // Catch:{ all -> 0x005f }
            java.lang.String r3 = "yyyy-MM-dd"
            r2.<init>(r3)     // Catch:{ all -> 0x005f }
            java.util.Date r3 = new java.util.Date     // Catch:{ all -> 0x005f }
            r3.<init>()     // Catch:{ all -> 0x005f }
            java.lang.String r2 = r2.format(r3)     // Catch:{ all -> 0x005f }
            boolean r1 = r2.equals(r1)     // Catch:{ all -> 0x005f }
            if (r1 != 0) goto L_0x0012
            java.lang.String r1 = com.tencent.launcher.home.a.a(r4)     // Catch:{ Throwable -> 0x005a }
            boolean r1 = a(r1)     // Catch:{ Throwable -> 0x005a }
            if (r1 == 0) goto L_0x0061
            java.lang.String r1 = "fn_search"
            com.tencent.launcher.home.a.b(r4, r1)     // Catch:{ Throwable -> 0x005a }
            java.lang.String r1 = "fn_appstore"
            com.tencent.launcher.home.a.b(r4, r1)     // Catch:{ Throwable -> 0x005a }
            android.content.SharedPreferences$Editor r0 = r0.edit()     // Catch:{ Throwable -> 0x005a }
            java.lang.String r1 = "stats_date"
            android.content.SharedPreferences$Editor r0 = r0.putString(r1, r2)     // Catch:{ Throwable -> 0x005a }
            r0.commit()     // Catch:{ Throwable -> 0x005a }
            goto L_0x0012
        L_0x005a:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x005f }
            goto L_0x0012
        L_0x005f:
            r0 = move-exception
            throw r0
        L_0x0061:
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Throwable -> 0x005a }
            java.lang.String r1 = "---bRes=false;"
            r0.println(r1)     // Catch:{ Throwable -> 0x005a }
            goto L_0x0012
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.launcher.home.g.run():void");
    }
}
