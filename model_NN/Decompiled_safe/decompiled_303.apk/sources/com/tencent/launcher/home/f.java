package com.tencent.launcher.home;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import com.tencent.qqlauncher.R;
import java.io.File;

public final class f {
    public static void a(Activity activity) {
        try {
            Intent intent = new Intent();
            intent.setClassName("com.tencent.sc", "com.tencent.sc.activity.SplashActivity");
            intent.addFlags(268435456);
            activity.startActivity(intent);
            File file = new File(activity.getCacheDir() + "/center.apk");
            if (file.exists()) {
                file.delete();
            }
        } catch (ActivityNotFoundException e) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle((int) R.string.percenter_install_notify);
            builder.setMessage((int) R.string.percenter_install_msg);
            builder.setPositiveButton((int) R.string.percenter_install_button_continue, new d(activity));
            builder.setNegativeButton((int) R.string.cancel, (DialogInterface.OnClickListener) null);
            AlertDialog create = builder.create();
            create.setCancelable(false);
            create.show();
        }
    }
}
