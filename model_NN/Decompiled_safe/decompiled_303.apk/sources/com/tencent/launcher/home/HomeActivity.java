package com.tencent.launcher.home;

import android.app.Activity;
import android.os.Bundle;
import com.tencent.qqlauncher.R;

public class HomeActivity extends Activity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.main);
    }
}
