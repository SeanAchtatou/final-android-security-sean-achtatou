package com.tencent.launcher.home;

import android.content.Context;
import com.tencent.launcher.base.BaseApp;

public final class i {
    private static final i a = new i();
    private static final String[] b = {"test", "type_wifi", "type_bluetooth", "type_gps", "type_sync", "type_brightness", "type_vibrate", "type_gravity", "type_airplane"};
    private final Context c = BaseApp.b();

    private i() {
    }

    public static i a() {
        return a;
    }

    public static boolean a(String str) {
        for (String equals : b) {
            if (equals.equals(str)) {
                return true;
            }
        }
        return false;
    }

    public final void a(String str, int i) {
        this.c.getSharedPreferences("home_config", 0).edit().putInt(str, i).commit();
    }

    public final void a(String str, long j) {
        this.c.getSharedPreferences("home_config", 0).edit().putLong(str, j).commit();
    }

    public final void a(String str, String str2) {
        this.c.getSharedPreferences("home_config", 0).edit().putString(str, str2).commit();
    }

    public final void a(String str, boolean z) {
        this.c.getSharedPreferences("home_config", 0).edit().putBoolean(str, z).commit();
    }

    public final int b(String str, int i) {
        return this.c.getSharedPreferences("home_config", 0).getInt(str, i);
    }

    public final long b(String str) {
        return this.c.getSharedPreferences("home_config", 0).getLong(str, 0);
    }

    public final String b(String str, String str2) {
        return this.c.getSharedPreferences("home_config", 0).getString(str, str2);
    }

    public final boolean b(String str, boolean z) {
        return this.c.getSharedPreferences("home_config", 0).getBoolean(str, z);
    }

    public final void c(String str) {
        if (str != null) {
            this.c.getSharedPreferences("home_config", 0).edit().remove(str).commit();
        }
    }
}
