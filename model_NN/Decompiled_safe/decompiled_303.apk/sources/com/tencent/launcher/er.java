package com.tencent.launcher;

import android.content.Context;

final class er extends Thread {
    private /* synthetic */ String a;
    private /* synthetic */ Context b;
    private /* synthetic */ boolean c;

    er(String str, Context context, boolean z) {
        this.a = str;
        this.b = context;
        this.c = z;
    }

    public final void run() {
        Launcher launcher = Launcher.getLauncher();
        if (launcher != null) {
            launcher.stopLoaders();
        }
        Launcher.importItemsFormHomeAndDeleteEmptyScreen(this.a, this.b, this.c);
        if (launcher != null) {
            Launcher.access$6100(launcher);
        }
    }
}
