package com.tencent.launcher;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.tencent.module.appcenter.AppCenterActivity;
import com.tencent.qqlauncher.R;

final class ib implements View.OnClickListener {
    private /* synthetic */ Button a;
    private /* synthetic */ Launcher b;

    ib(Launcher launcher, Button button) {
        this.b = launcher;
        this.a = button;
    }

    public final void onClick(View view) {
        if (view == this.a) {
            if (this.b.flagDockEnable && this.b.mDrawer.b() && this.b.mDockBar.d() && this.b.mDockButtonGroup.c()) {
                this.b.mHomeFlipper.setDisplayedChild(1);
                Launcher.access$1400(this.b);
                Workspace unused = this.b.mWorkspace;
                this.b.mDockBar.c();
                if (this.b.hasNewAppsNotify()) {
                    this.b.mDrawer.e();
                    this.b.mDrawer.k();
                    this.b.mNewAppNofifyView.setVisibility(8);
                    return;
                }
                this.b.mDrawer.c();
                this.b.mDrawer.l();
            }
        } else if (view == this.b.mHomeIconButton) {
            if (this.b.mDrawer.a()) {
                this.b.mHomeFlipper.setDisplayedChild(0);
                this.b.mDrawer.g();
                Workspace unused2 = this.b.mWorkspace;
                this.b.mDockButtonGroup.b();
                this.b.clearNewAppsNotify();
            }
        } else if (view == this.b.mMarketEntry) {
            if (this.b.mContext.getResources().getConfiguration().locale.getLanguage().toLowerCase().contains("zh")) {
                Intent intent = new Intent();
                intent.setClass(Launcher.launcher, AppCenterActivity.class);
                this.b.startActivity(intent);
                return;
            }
            try {
                Intent intent2 = new Intent();
                intent2.setClassName("com.android.vending", "com.android.vending.AssetBrowserActivity");
                this.b.startActivity(intent2);
            } catch (Exception e) {
                Toast.makeText(this.b.mContext, (int) R.string.market_not_installed, 0).show();
            }
        } else if (view == this.b.mSearchApp) {
            this.b.onSearchRequested();
        }
    }
}
