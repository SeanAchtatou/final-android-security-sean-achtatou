package com.tencent.launcher;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.qqlauncher.R;
import java.lang.ref.SoftReference;
import java.net.URISyntaxException;
import java.util.HashMap;

final class gq extends CursorAdapter {
    private boolean a;
    private LayoutInflater b;
    private final HashMap c = new HashMap();
    private final HashMap d = new HashMap();
    private final Launcher e;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, boolean):void}
     arg types: [com.tencent.launcher.Launcher, android.database.Cursor, int]
     candidates:
      ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, int):void}
      ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, boolean):void} */
    gq(Launcher launcher, dq dqVar, Cursor cursor) {
        super((Context) launcher, cursor, true);
        this.a = dqVar.c == 2;
        this.b = LayoutInflater.from(launcher);
        this.e = launcher;
        this.e.startManagingCursor(getCursor());
    }

    private Drawable a(Context context, Cursor cursor, es esVar) {
        byte[] blob = esVar.k != -1 ? cursor.getBlob(esVar.k) : null;
        if (blob != null) {
            SoftReference softReference = (SoftReference) this.d.get(Long.valueOf(esVar.e));
            Drawable drawable = softReference != null ? (Drawable) softReference.get() : null;
            if (drawable != null) {
                return drawable;
            }
            hp hpVar = new hp(a.a(BitmapFactory.decodeByteArray(blob, 0, blob.length), context));
            this.d.put(Long.valueOf(esVar.e), new SoftReference(hpVar));
            return hpVar;
        } else if (esVar.l == -1 || esVar.m == -1) {
            return null;
        } else {
            String string = cursor.getString(esVar.l);
            Drawable drawable2 = (Drawable) this.c.get(string);
            if (drawable2 != null) {
                return drawable2;
            }
            try {
                Resources resourcesForApplication = context.getPackageManager().getResourcesForApplication(cursor.getString(esVar.m));
                drawable2 = a.a(resourcesForApplication.getDrawable(resourcesForApplication.getIdentifier(string, null, null)), context);
                this.c.put(string, drawable2);
                return drawable2;
            } catch (Exception e2) {
                return drawable2;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        for (Drawable callback : this.c.values()) {
            callback.setCallback(null);
        }
        this.c.clear();
        for (SoftReference softReference : this.d.values()) {
            Drawable drawable = (Drawable) softReference.get();
            if (drawable != null) {
                drawable.setCallback(null);
            }
        }
        this.d.clear();
        Cursor cursor = getCursor();
        if (cursor != null) {
            try {
                cursor.close();
            } finally {
                this.e.stopManagingCursor(cursor);
            }
        }
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        es esVar = (es) view.getTag();
        esVar.e = cursor.getLong(esVar.g);
        Drawable a2 = a(context, cursor, esVar);
        esVar.a.setText(cursor.getString(esVar.h));
        if (!this.a) {
            esVar.a.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, a2, (Drawable) null, (Drawable) null);
        } else {
            boolean z = a2 != null;
            esVar.c.setVisibility(z ? 0 : 8);
            if (z) {
                esVar.c.setImageDrawable(a2);
            }
            if (esVar.i != -1) {
                String string = cursor.getString(esVar.i);
                if (string != null) {
                    esVar.b.setText(string);
                    esVar.b.setVisibility(0);
                } else {
                    esVar.b.setVisibility(8);
                }
            } else {
                esVar.b.setVisibility(8);
            }
        }
        if (esVar.j != -1) {
            try {
                esVar.d = Intent.parseUri(cursor.getString(esVar.j), 0);
            } catch (URISyntaxException e2) {
            }
        } else {
            esVar.f = true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        View inflate;
        es esVar = new es();
        if (!this.a) {
            inflate = this.b.inflate((int) R.layout.application_boxed, viewGroup, false);
        } else {
            inflate = this.b.inflate((int) R.layout.application_list, viewGroup, false);
            esVar.b = (TextView) inflate.findViewById(R.id.description);
            esVar.c = (ImageView) inflate.findViewById(R.id.icon);
        }
        esVar.a = (TextView) inflate.findViewById(R.id.name);
        esVar.g = cursor.getColumnIndexOrThrow("_id");
        esVar.h = cursor.getColumnIndexOrThrow("name");
        esVar.i = cursor.getColumnIndex("description");
        esVar.j = cursor.getColumnIndex("intent");
        esVar.k = cursor.getColumnIndex("icon_bitmap");
        esVar.l = cursor.getColumnIndex("icon_resource");
        esVar.m = cursor.getColumnIndex("icon_package");
        inflate.setTag(esVar);
        return inflate;
    }
}
