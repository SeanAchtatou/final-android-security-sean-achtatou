package com.tencent.launcher;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.tencent.a.a;
import com.tencent.launcher.base.b;

public class DockButtonGroup extends ViewGroup {
    private static final int h = ((int) (0.0f * b.b));
    private static final int i = ((int) (4.0f * b.b));
    private boolean a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private boolean g;
    private int j;
    private int k;
    private long l;
    private boolean m;
    private f n;
    private int o;

    public class LayoutParams extends ViewGroup.MarginLayoutParams {
        public int a;
        int b;
        int c;

        public LayoutParams(int i) {
            super(-1, -1);
            this.a = i;
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }
    }

    public DockButtonGroup(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public DockButtonGroup(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.b = 5;
        this.c = 56;
        this.d = 56;
        this.e = 5;
        this.f = 5;
        this.g = true;
        this.j = 3;
        this.k = 3;
        this.m = false;
        this.o = 1;
        setHapticFeedbackEnabled(true);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.e, i2, 0);
        this.a = obtainStyledAttributes.getInt(0, 1) == 1;
        this.b = obtainStyledAttributes.getInt(3, this.b);
        this.e = obtainStyledAttributes.getDimensionPixelSize(4, this.e);
        this.f = obtainStyledAttributes.getDimensionPixelSize(5, this.f);
        obtainStyledAttributes.recycle();
    }

    public final void a() {
        if (!(this.k == 3 || this.n == null)) {
            this.n.a(this);
        }
        this.j = 1;
        this.k = 3;
        this.m = false;
        setVisibility(0);
    }

    public final void a(f fVar) {
        this.n = fVar;
    }

    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        view.setFocusable(true);
        super.addView(view, layoutParams);
    }

    public final void b() {
        if (getVisibility() == 0) {
            this.k = 1;
            this.j = 3;
            invalidate();
        }
    }

    public final boolean c() {
        return this.k == 3 && this.j == 3;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public boolean drawChild(Canvas canvas, View view, long j2) {
        if (this.m) {
            return true;
        }
        DockButton dockButton = (DockButton) view;
        int paddingLeft = ((LayoutParams) dockButton.getLayoutParams()).b + dockButton.getPaddingLeft();
        int top = dockButton.getTop() + dockButton.a() + 1;
        if (this.k != 3 && this.j == 3) {
            if (this.k == 1) {
                this.l = SystemClock.uptimeMillis();
                this.k = 2;
            }
            float uptimeMillis = ((float) (SystemClock.uptimeMillis() - this.l)) / 300.0f;
            if (uptimeMillis >= 1.0f) {
                this.k = 3;
                this.m = true;
                setVisibility(4);
                if (this.n != null) {
                    this.n.a(this);
                }
                return true;
            }
            float height = ((float) dockButton.getHeight()) * Math.min(uptimeMillis, 1.0f);
            canvas.save();
            Canvas canvas2 = canvas;
            canvas2.saveLayerAlpha((float) dockButton.getLeft(), (float) top, (float) dockButton.getRight(), (float) (getHeight() - i), 40, 20);
            canvas.scale(1.0f, -1.0f, (float) (dockButton.getLeft() + (dockButton.getWidth() / 2)), (float) top);
            canvas.translate(0.0f, height);
            super.drawChild(canvas, dockButton, j2);
            canvas.restore();
            canvas.restore();
            canvas.save();
            canvas.clipRect(paddingLeft, dockButton.getTop(), dockButton.getRight(), top);
            canvas.translate(0.0f, height);
            super.drawChild(canvas, dockButton, j2);
            canvas.restore();
            invalidate();
            return true;
        } else if (this.j == 3 || this.k != 3) {
            canvas.save();
            Canvas canvas3 = canvas;
            canvas3.saveLayerAlpha((float) dockButton.getLeft(), (float) top, (float) dockButton.getRight(), (float) (getHeight() - i), 40, 20);
            canvas.scale(1.0f, -1.0f, (float) (dockButton.getLeft() + (dockButton.getWidth() / 2)), (float) top);
            super.drawChild(canvas, dockButton, j2);
            canvas.restore();
            canvas.restore();
            super.drawChild(canvas, dockButton, j2);
            return true;
        } else {
            if (this.j == 1) {
                this.l = SystemClock.uptimeMillis();
                this.j = 2;
            }
            float uptimeMillis2 = ((float) (SystemClock.uptimeMillis() - this.l)) / 300.0f;
            if (uptimeMillis2 >= 1.0f) {
                this.j = 3;
                invalidate();
                return super.drawChild(canvas, dockButton, j2);
            }
            float height2 = ((float) dockButton.getHeight()) * (1.0f - Math.min(uptimeMillis2, 1.0f));
            canvas.save();
            Canvas canvas4 = canvas;
            canvas4.saveLayerAlpha((float) dockButton.getLeft(), (float) top, (float) dockButton.getRight(), (float) (getHeight() - i), 40, 20);
            canvas.scale(1.0f, -1.0f, (float) (dockButton.getLeft() + (dockButton.getWidth() / 2)), (float) top);
            canvas.translate(0.0f, height2);
            super.drawChild(canvas, dockButton, j2);
            canvas.restore();
            canvas.restore();
            canvas.save();
            canvas.clipRect(paddingLeft, dockButton.getTop(), dockButton.getRight(), top);
            canvas.translate(0.0f, height2);
            super.drawChild(canvas, dockButton, j2);
            canvas.restore();
            invalidate();
            return true;
        }
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new LayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                int i7 = layoutParams.b;
                int i8 = layoutParams.c;
                childAt.layout(i7, i8, layoutParams.width + i7, layoutParams.height + i8);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        if (mode == 0 || mode2 == 0) {
            throw new RuntimeException("dockbar cannot have UNSPECIFIED dimensions");
        }
        if (this.g) {
            int i4 = this.b;
            if (this.a) {
                this.c = ((size - this.e) - this.f) / i4;
                this.d = size2 - h;
            } else {
                this.c = size - h;
                this.d = ((size2 - this.e) - this.f) / i4;
            }
            this.g = false;
        }
        int i5 = this.c;
        int i6 = this.d;
        int childCount = getChildCount();
        for (int i7 = 0; i7 < childCount; i7++) {
            View childAt = getChildAt(i7);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            boolean z = this.a;
            int i8 = this.e;
            int i9 = layoutParams.a;
            layoutParams.width = (i5 - layoutParams.leftMargin) - layoutParams.rightMargin;
            layoutParams.height = (i6 - layoutParams.topMargin) - layoutParams.bottomMargin;
            layoutParams.b = z ? (i9 * i5) + i8 + layoutParams.leftMargin : layoutParams.leftMargin;
            layoutParams.c = z ? layoutParams.topMargin : (i9 * i6) + i8 + layoutParams.topMargin;
            childAt.measure(View.MeasureSpec.makeMeasureSpec(layoutParams.width, 1073741824), View.MeasureSpec.makeMeasureSpec(layoutParams.height, 1073741824));
        }
        setMeasuredDimension(size, size2);
    }
}
