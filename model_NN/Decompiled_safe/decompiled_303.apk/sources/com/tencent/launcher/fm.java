package com.tencent.launcher;

import android.view.animation.Animation;
import android.widget.Button;
import android.widget.RelativeLayout;

final class fm implements Animation.AnimationListener {
    private /* synthetic */ Button a;
    private /* synthetic */ int b;
    private /* synthetic */ SearchAppActivityReserved c;

    fm(SearchAppActivityReserved searchAppActivityReserved, Button button, int i) {
        this.c = searchAppActivityReserved;
        this.a = button;
        this.b = i;
    }

    public final void onAnimationEnd(Animation animation) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.leftMargin = this.a.getLeft() + this.c.hotWordX[this.b];
        layoutParams.topMargin = this.c.hotWordY[this.b] + this.a.getTop();
        layoutParams.addRule(10);
        layoutParams.addRule(9);
        this.a.setLayoutParams(layoutParams);
        this.a.clearAnimation();
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
