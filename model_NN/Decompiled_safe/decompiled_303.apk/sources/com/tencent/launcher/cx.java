package com.tencent.launcher;

final class cx implements Runnable {
    private int a;
    private /* synthetic */ WidgetContainer b;

    cx(WidgetContainer widgetContainer) {
        this.b = widgetContainer;
    }

    public final void a() {
        this.a = this.b.getWindowAttachCount();
    }

    public final void run() {
        if (this.b.getParent() != null && this.b.hasWindowFocus() && this.a == this.b.getWindowAttachCount() && !this.b.a && this.b.performLongClick()) {
            this.b.a = true;
        }
    }
}
