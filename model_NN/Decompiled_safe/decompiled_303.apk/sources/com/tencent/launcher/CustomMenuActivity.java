package com.tencent.launcher;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.tencent.qqlauncher.R;
import java.util.Vector;

public class CustomMenuActivity extends Activity {
    private static final int DEFAULT_PER_ROW_COUNT = 3;
    private static final int MAX_PER_ROW_COUNT = 4;
    /* access modifiers changed from: private */
    public Vector mMenuItem = new Vector();
    protected boolean mNeedCreateMenu = true;
    private MenuDialog menudialog = null;

    public class MenuDialog extends AlertDialog implements View.OnClickListener {
        GridView a;
        private cn c = null;

        protected MenuDialog(Context context) {
            super(context);
        }

        public final void a() {
            this.a.setAdapter((ListAdapter) this.c);
        }

        public void onClick(View view) {
            cancel();
            gg ggVar = (gg) view.getTag();
            if (ggVar.c().isEnabled()) {
                CustomMenuActivity.this.onOptionsItemSelected(ggVar.c());
            }
        }

        /* access modifiers changed from: protected */
        public void onCreate(Bundle bundle) {
            super.onCreate(bundle);
            this.a = (GridView) LayoutInflater.from(CustomMenuActivity.this).inflate((int) R.layout.gridview_menu, (ViewGroup) null);
            getWindow().setContentView(this.a);
            getWindow().setLayout(-1, -2);
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(-1, -2, 0, 0, 2, 2, -3);
            layoutParams.gravity = 81;
            layoutParams.dimAmount = 0.5f;
            getWindow().setAttributes(layoutParams);
            getWindow().getAttributes().windowAnimations = R.style.custommenudialog;
            setCanceledOnTouchOutside(true);
            this.c = new cn(this);
            this.a.setSelector((int) R.drawable.trans);
        }
    }

    private void CreateMenuDlg() {
        this.menudialog = new MenuDialog(this);
        this.menudialog.setOnKeyListener(new bh(this));
    }

    private void showQqMenuDlg() {
        if (this.menudialog != null) {
            this.menudialog.show();
        }
    }

    public boolean GetIsNeedCreateMenu() {
        return this.mNeedCreateMenu;
    }

    public void SetIsNeedCreateMenu(boolean z) {
        this.mNeedCreateMenu = z;
    }

    /* access modifiers changed from: package-private */
    public void closeMenuDialog() {
        if (this.menudialog != null) {
            this.menudialog.dismiss();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.menudialog != null && this.menudialog.isShowing()) {
            this.menudialog.dismiss();
            this.menudialog.show();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (this.menudialog != null) {
            return true;
        }
        this.menudialog = new MenuDialog(this);
        this.menudialog.setOnKeyListener(new bh(this));
        return true;
    }

    public boolean onMenuOpened(int i, Menu menu) {
        if (!this.mNeedCreateMenu || this.menudialog == null) {
            return true;
        }
        this.mMenuItem.clear();
        for (int i2 = 0; i2 < menu.size(); i2++) {
            if (menu.getItem(i2).getTitle() != null) {
                MenuItem item = menu.getItem(i2);
                if (item.isVisible()) {
                    this.mMenuItem.add(new gg(this, item));
                }
            }
        }
        if (this.mMenuItem.size() == 0) {
            this.menudialog = null;
            return true;
        }
        if (this.menudialog != null) {
            this.menudialog.show();
        }
        this.menudialog.a();
        if (this.mMenuItem.size() <= 4) {
            this.menudialog.a.setNumColumns(this.mMenuItem.size());
        } else {
            this.menudialog.a.setNumColumns(3);
        }
        return false;
    }

    public void onOptionsMenuClosed(Menu menu) {
        super.onOptionsMenuClosed(menu);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        if (this.menudialog == null) {
            return true;
        }
        MenuDialog menuDialog = this.menudialog;
        if (menuDialog.a == null) {
            return true;
        }
        menuDialog.a.setPressed(false);
        menuDialog.a.setSelected(false);
        return true;
    }
}
