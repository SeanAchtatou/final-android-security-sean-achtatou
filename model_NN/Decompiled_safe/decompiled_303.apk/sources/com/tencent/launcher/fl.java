package com.tencent.launcher;

import android.view.View;
import android.view.animation.Animation;
import java.util.ArrayList;

class fl implements Animation.AnimationListener {
    private boolean a;
    int c;
    View d;
    ArrayList e;

    /* synthetic */ fl() {
        this((byte) 0);
    }

    private fl(byte b) {
        this.a = false;
    }

    public void onAnimationEnd(Animation animation) {
        this.c--;
        if (this.c <= 0 && !this.a) {
            this.a = true;
            if (!(this.d == null || this.d.getParent() == null)) {
                QGridLayout qGridLayout = (QGridLayout) this.d.getParent();
                boolean unused = qGridLayout.aG = true;
                int unused2 = qGridLayout.am = 3;
                qGridLayout.removeView(this.d);
            }
            if (this.e != null) {
                int size = this.e.size();
                for (int i = 0; i < size; i++) {
                    View view = (View) this.e.get(i);
                    if (view != null) {
                        view.clearAnimation();
                    }
                }
                this.e.clear();
                this.e = null;
            }
            this.c = 0;
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
