package com.tencent.launcher;

import android.app.Activity;
import android.app.WallpaperManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import com.tencent.qqlauncher.R;
import java.io.IOException;
import java.util.ArrayList;

public class WallpaperChooser extends Activity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    /* access modifiers changed from: private */
    public Bitmap mBitmap;
    private Gallery mGallery;
    /* access modifiers changed from: private */
    public ImageView mImageView;
    /* access modifiers changed from: private */
    public ArrayList mImages;
    private boolean mIsWallpaperSet;
    /* access modifiers changed from: private */
    public Cif mLoader;
    /* access modifiers changed from: private */
    public ArrayList mThumbs;

    private void addWallpapers(Resources resources, String str, int i) {
        int identifier;
        for (String str2 : resources.getStringArray(i)) {
            int identifier2 = resources.getIdentifier(str2, "drawable", str);
            if (!(identifier2 == 0 || (identifier = resources.getIdentifier(str2 + "_small", "drawable", str)) == 0)) {
                this.mThumbs.add(Integer.valueOf(identifier));
                this.mImages.add(Integer.valueOf(identifier2));
            }
        }
    }

    private void findWallpapers() {
        this.mThumbs = new ArrayList(24);
        this.mImages = new ArrayList(24);
        Resources resources = getResources();
        String packageName = getApplication().getPackageName();
        addWallpapers(resources, packageName, R.array.wallpapers);
        addWallpapers(resources, packageName, R.array.extra_wallpapers);
    }

    private void selectWallpaper(int i) {
        if (!this.mIsWallpaperSet) {
            if (this.mImages == null || this.mImages.size() == 0 || this.mImages.size() <= i) {
                setResult(0);
                finish();
            }
            this.mIsWallpaperSet = true;
            try {
                ((WallpaperManager) getSystemService("wallpaper")).setResource(((Integer) this.mImages.get(i)).intValue());
                setResult(-1);
                finish();
            } catch (IOException e) {
                Log.e("Launcher", "Failed to set wallpaper: " + e);
            }
        }
    }

    public void onClick(View view) {
        selectWallpaper(this.mGallery.getSelectedItemPosition());
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        findWallpapers();
        setContentView((int) R.layout.wallpaper_chooser);
        this.mGallery = (Gallery) findViewById(R.id.gallery);
        this.mGallery.setAdapter((SpinnerAdapter) new q(this, this));
        this.mGallery.setOnItemSelectedListener(this);
        this.mGallery.setCallbackDuringFling(false);
        findViewById(R.id.set).setOnClickListener(this);
        this.mImageView = (ImageView) findViewById(R.id.wallpaper);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.mLoader != null && this.mLoader.getStatus() != AsyncTask.Status.FINISHED) {
            this.mLoader.cancel(true);
            this.mLoader = null;
        }
    }

    public void onItemSelected(AdapterView adapterView, View view, int i, long j) {
        if (!(this.mLoader == null || this.mLoader.getStatus() == AsyncTask.Status.FINISHED)) {
            this.mLoader.a();
        }
        this.mLoader = (Cif) new Cif(this).execute(Integer.valueOf(i));
    }

    public void onNothingSelected(AdapterView adapterView) {
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mIsWallpaperSet = false;
    }
}
