package com.tencent.launcher;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.SystemClock;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.tencent.launcher.base.b;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

public class DragLayer extends FrameLayout implements dt {
    private static final int a = ((int) (20.0f * b.b));
    private e A;
    private final Paint B = new Paint();
    private final Paint C = new Paint();
    private Paint D;
    private float E;
    private float F;
    private int G;
    private long H;
    private int I;
    private int J = 3;
    private InputMethodManager K;
    private float L;
    private float M;
    private int N;
    private int O = 0;
    private Paint P = new Paint();
    private boolean b = false;
    private boolean c;
    private float d;
    private float e;
    private float f;
    private boolean g = true;
    private Bitmap h = null;
    private View i;
    private int j;
    private int k;
    private float l;
    private float m;
    private Rect n = new Rect();
    private cm o;
    private Object p;
    private final Rect q = new Rect();
    private final int[] r = new int[2];
    private final Vibrator s;
    private hs t;
    /* access modifiers changed from: private */
    public hc u;
    /* access modifiers changed from: private */
    public int v = 0;
    private p w = new p(this);
    private ArrayList x;
    private RectF y;
    private boolean z;

    public DragLayer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.B.setColorFilter(new PorterDuffColorFilter(context.getResources().getColor(R.color.delete_color_filter), PorterDuff.Mode.SRC_ATOP));
        this.C.setAlpha(200);
        this.P.setColor(-65536);
        this.P.setTextSize(20.0f);
        int color = context.getResources().getColor(R.color.snag_callout_color);
        Paint paint = new Paint();
        paint.setColor(color);
        paint.setStrokeWidth(3.0f);
        paint.setAntiAlias(true);
        this.s = (Vibrator) context.getSystemService("vibrator");
    }

    private e a(ViewGroup viewGroup, int i2, int i3, int[] iArr, boolean z2) {
        int i4;
        int i5;
        e eVar;
        Rect rect = this.n;
        int childCount = viewGroup.getChildCount();
        int scrollX = i2 + viewGroup.getScrollX();
        int scrollY = i3 + viewGroup.getScrollY();
        ArrayList arrayList = this.x;
        int i6 = i2;
        int i7 = i3;
        for (int i8 = childCount - 1; i8 >= 0; i8--) {
            View childAt = viewGroup.getChildAt(i8);
            if (childAt.getVisibility() == 0 && (arrayList == null || !arrayList.contains(childAt))) {
                childAt.getHitRect(rect);
                if (!rect.contains(scrollX, scrollY)) {
                    continue;
                } else {
                    if (childAt instanceof ViewGroup) {
                        i4 = scrollX - childAt.getLeft();
                        int top = scrollY - childAt.getTop();
                        eVar = a((ViewGroup) childAt, i4, top, iArr, z2);
                        i5 = top;
                    } else {
                        i4 = i6;
                        i5 = i7;
                        eVar = null;
                    }
                    if (eVar != null) {
                        return eVar;
                    }
                    if (childAt instanceof e) {
                        e eVar2 = (e) childAt;
                        if ((!z2 || !eVar2.a(this.o, this.p)) && (z2 || !eVar2.a(i5, this.p))) {
                            return null;
                        }
                        iArr[0] = i4;
                        iArr[1] = i5;
                        return (e) childAt;
                    }
                    i7 = i5;
                    i6 = i4;
                }
            }
        }
        return null;
    }

    private void a(e eVar) {
        if ((eVar instanceof BubbleTextView) && !(eVar instanceof FolderIcon) && ((BubbleTextView) eVar).b()) {
            Toast.makeText(getContext(), getContext().getResources().getString(R.string.desktop_is_full), 0).show();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.DragLayer.a(android.view.ViewGroup, int, int, int[], boolean):com.tencent.launcher.e
     arg types: [com.tencent.launcher.DragLayer, int, int, int[], int]
     candidates:
      com.tencent.launcher.DragLayer.a(android.view.View, android.graphics.Bitmap, com.tencent.launcher.cm, java.lang.Object, int):void
      com.tencent.launcher.dt.a(android.view.View, android.graphics.Bitmap, com.tencent.launcher.cm, java.lang.Object, int):void
      com.tencent.launcher.DragLayer.a(android.view.ViewGroup, int, int, int[], boolean):com.tencent.launcher.e */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.e.a(com.tencent.launcher.cm, boolean):void
     arg types: [com.tencent.launcher.cm, int]
     candidates:
      com.tencent.launcher.e.a(int, java.lang.Object):boolean
      com.tencent.launcher.e.a(com.tencent.launcher.cm, java.lang.Object):boolean
      com.tencent.launcher.e.a(com.tencent.launcher.cm, boolean):void */
    private boolean a(float f2, float f3) {
        invalidate();
        this.g = true;
        int[] iArr = this.r;
        e a2 = a((ViewGroup) this, (int) (f2 < 0.0f ? 1.0f : f2 > ((float) b.c) ? (float) (b.c - 1) : f2), (int) (f3 < 0.0f ? 1.0f : f3 > ((float) b.d) ? (float) (b.d - 1) : f3), iArr, true);
        if (!(this.A == null || this.A == a2)) {
            this.A.a(this.o, false);
        }
        if (a2 != null) {
            a2.a(this.o, false);
            if (a2.a(this.o, this.p)) {
                this.g = a2.a(this.o, iArr[0], iArr[1], (int) this.l, (int) this.m, this.p);
                if (this.p instanceof ha) {
                    ha haVar = (ha) this.p;
                    if (!haVar.v) {
                        a(a2);
                        this.o.a((View) a2, false);
                        haVar.v = true;
                        return true;
                    }
                }
                a(a2);
                this.o.a((View) a2, true);
                return true;
            }
            a(a2);
            this.o.a((View) a2, false);
            return true;
        }
        this.o.a(null, false);
        return false;
    }

    private void c() {
        if (this.b) {
            this.b = false;
            if (this.h != null) {
                this.h.recycle();
            }
            if (this.i != null && this.g) {
                this.i.setVisibility(0);
            }
            if (this.t != null) {
                this.t.a();
            }
        }
    }

    public final hc a() {
        return this.u;
    }

    /* access modifiers changed from: package-private */
    public final void a(RectF rectF) {
        this.y = rectF;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public final void a(View view, Bitmap bitmap, cm cmVar, Object obj, int i2) {
        Bitmap bitmap2;
        if (view != null) {
            if (this.N == 0) {
                this.N = getTop();
            }
            if (this.K == null) {
                this.K = (InputMethodManager) getContext().getSystemService("input_method");
            }
            this.K.hideSoftInputFromWindow(getWindowToken(), 0);
            if (this.t != null) {
                this.t.a(view, cmVar, obj, i2);
            }
            Rect rect = this.n;
            rect.set(view.getScrollX(), view.getScrollY(), 0, 0);
            this.L = this.d;
            this.M = this.e;
            offsetDescendantRectToMyCoords(view, rect);
            this.l = this.d - ((float) rect.left);
            this.m = this.e - ((float) rect.top);
            view.clearFocus();
            view.setPressed(false);
            boolean willNotCacheDrawing = view.willNotCacheDrawing();
            int drawingCacheBackgroundColor = view.getDrawingCacheBackgroundColor();
            if (bitmap == null) {
                view.setWillNotCacheDrawing(false);
                view.setDrawingCacheBackgroundColor(0);
                if (drawingCacheBackgroundColor != 0) {
                    view.destroyDrawingCache();
                }
                view.buildDrawingCache();
                bitmap2 = view.getDrawingCache();
                if (bitmap2 == null) {
                    bitmap2 = BitmapFactory.decodeResource(getResources(), R.drawable.icon);
                }
            } else {
                this.l = (float) (bitmap.getWidth() / 2);
                this.m = (float) (bitmap.getHeight() / 2);
                bitmap2 = bitmap;
            }
            int width = bitmap2.getWidth();
            int height = bitmap2.getHeight();
            Matrix matrix = new Matrix();
            float f2 = (float) width;
            float f3 = (24.0f + f2) / f2;
            matrix.setScale(f3, f3);
            this.F = 1.0f;
            this.E = 1.0f / f3;
            this.G = 110;
            this.J = 1;
            this.I = 1;
            try {
                this.h = Bitmap.createBitmap(bitmap2, 0, 0, width, height, matrix, true);
            } catch (OutOfMemoryError e2) {
                if (this.h == null || this.h.isRecycled()) {
                    this.h = bitmap2;
                }
            }
            if (bitmap == null) {
                view.destroyDrawingCache();
                view.setWillNotCacheDrawing(willNotCacheDrawing);
                view.setDrawingCacheBackgroundColor(drawingCacheBackgroundColor);
            }
            Bitmap bitmap3 = this.h;
            this.j = (bitmap3.getWidth() - width) / 2;
            this.k = (bitmap3.getHeight() - height) / 2;
            if (i2 == 0) {
                view.setVisibility(8);
            }
            this.D = this.C;
            this.b = true;
            this.c = true;
            this.i = view;
            this.o = cmVar;
            this.p = obj;
            this.s.vibrate(35);
            this.z = false;
            invalidate();
        }
    }

    public final void a(View view, cm cmVar, Object obj, int i2) {
        a(view, (Bitmap) null, cmVar, obj, i2);
    }

    public final void a(hc hcVar) {
        this.u = hcVar;
    }

    public final void a(hs hsVar) {
        this.t = hsVar;
    }

    public final Bitmap b() {
        return this.h;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        float f2 = this.e != this.f ? this.f : this.e;
        if (this.b && this.h != null && !this.h.isRecycled()) {
            if (this.J == 1) {
                this.H = SystemClock.uptimeMillis();
                this.J = 2;
            }
            if (this.J == 2) {
                float uptimeMillis = ((float) (SystemClock.uptimeMillis() - this.H)) / ((float) this.G);
                if (uptimeMillis >= 1.0f) {
                    this.J = 3;
                }
                float min = (Math.min(uptimeMillis, 1.0f) * (this.F - this.E)) + this.E;
                switch (this.I) {
                    case 1:
                        Bitmap bitmap = this.h;
                        canvas.save();
                        canvas.translate(((((float) getScrollX()) + this.d) - this.l) - ((float) this.j), ((f2 + ((float) getScrollY())) - this.m) - ((float) this.k));
                        canvas.translate((((float) bitmap.getWidth()) * (1.0f - min)) / 2.0f, (((float) bitmap.getHeight()) * (1.0f - min)) / 2.0f);
                        canvas.scale(min, min);
                        canvas.drawBitmap(bitmap, 0.0f, 0.0f, this.D);
                        canvas.restore();
                        break;
                }
                invalidate();
                return;
            }
            canvas.drawBitmap(this.h, ((((float) getScrollX()) + this.d) - this.l) - ((float) this.j), ((f2 + ((float) getScrollY())) - this.m) - ((float) this.k), this.D);
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return this.b || super.dispatchKeyEvent(keyEvent);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        float x2 = motionEvent.getX();
        float y2 = motionEvent.getY();
        switch (action) {
            case 0:
                this.d = x2;
                this.e = y2;
                this.f = motionEvent.getRawY();
                this.A = null;
                break;
            case 1:
            case 3:
                if (this.c && a(x2, y2)) {
                    this.c = false;
                }
                c();
                break;
        }
        return this.b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.DragLayer.a(android.view.ViewGroup, int, int, int[], boolean):com.tencent.launcher.e
     arg types: [com.tencent.launcher.DragLayer, int, int, int[], int]
     candidates:
      com.tencent.launcher.DragLayer.a(android.view.View, android.graphics.Bitmap, com.tencent.launcher.cm, java.lang.Object, int):void
      com.tencent.launcher.dt.a(android.view.View, android.graphics.Bitmap, com.tencent.launcher.cm, java.lang.Object, int):void
      com.tencent.launcher.DragLayer.a(android.view.ViewGroup, int, int, int[], boolean):com.tencent.launcher.e */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.e.a(com.tencent.launcher.cm, boolean):void
     arg types: [com.tencent.launcher.cm, int]
     candidates:
      com.tencent.launcher.e.a(int, java.lang.Object):boolean
      com.tencent.launcher.e.a(com.tencent.launcher.cm, java.lang.Object):boolean
      com.tencent.launcher.e.a(com.tencent.launcher.cm, boolean):void */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.b) {
            return false;
        }
        int action = motionEvent.getAction();
        float x2 = motionEvent.getX();
        float y2 = motionEvent.getY();
        this.f = motionEvent.getRawY();
        switch (action) {
            case 0:
                this.d = x2;
                this.e = y2;
                if (x2 >= ((float) a) && x2 <= ((float) (getWidth() - a))) {
                    this.v = 0;
                    break;
                } else {
                    this.v = 1;
                    postDelayed(this.w, 600);
                    break;
                }
                break;
            case 1:
                removeCallbacks(this.w);
                if (this.c) {
                    a(x2, y2);
                    this.c = false;
                }
                c();
                break;
            case 2:
                int scrollX = getScrollX();
                int scrollY = getScrollY();
                float f2 = this.l;
                float f3 = this.m;
                int i2 = this.j;
                int i3 = this.k;
                int i4 = (int) (((((float) scrollX) + this.d) - f2) - ((float) i2));
                int i5 = (int) (((((float) scrollY) + this.e) - f3) - ((float) i3));
                Bitmap bitmap = this.h;
                int width = bitmap.getWidth();
                int height = bitmap.getHeight();
                Rect rect = this.q;
                rect.set(i4 - 1, i5 - 1, i4 + width + 1, i5 + height + 1);
                this.d = x2;
                this.e = y2;
                int i6 = (int) (((((float) scrollX) + x2) - f2) - ((float) i2));
                int i7 = (int) (((((float) scrollY) + y2) - f3) - ((float) i3));
                rect.union(i6 - 1, i7 - 1, i6 + width + 1, i7 + height + 1);
                int[] iArr = this.r;
                float f4 = x2 < 0.0f ? 1.0f : x2 > ((float) b.c) ? (float) (b.c - 1) : x2;
                if (y2 < 0.0f) {
                    y2 = 1.0f;
                } else if (y2 > ((float) b.d)) {
                    y2 = (float) (b.d - 1);
                }
                e a2 = a((ViewGroup) this, (int) f4, (int) y2, iArr, false);
                if (a2 != null) {
                    if (this.A == a2) {
                        a2.c(this.o, iArr[0], iArr[1], (int) this.l, (int) this.m, this.p);
                    } else {
                        if (this.A != null) {
                            this.A.a(this.o, true);
                        }
                        a2.b(this.o, iArr[0], iArr[1], (int) this.l, (int) this.m, this.p);
                    }
                } else if (this.A != null) {
                    this.A.a(this.o, true);
                }
                invalidate(rect);
                this.A = a2;
                boolean z2 = false;
                if (this.y != null) {
                    boolean contains = this.y.contains(motionEvent.getX(), motionEvent.getY());
                    if (!this.z && contains) {
                        this.D = this.B;
                        this.z = true;
                        z2 = true;
                    } else if (this.z && !contains) {
                        this.D = this.C;
                        this.z = false;
                    }
                }
                if (!z2 && x2 < ((float) a)) {
                    if (this.v == 0) {
                        this.v = 1;
                        this.w.a(0);
                        postDelayed(this.w, 600);
                        break;
                    }
                } else if (!z2 && x2 > ((float) (getWidth() - a))) {
                    if (this.v == 0) {
                        this.v = 1;
                        this.w.a(1);
                        postDelayed(this.w, 600);
                        break;
                    }
                } else if (this.v == 1) {
                    this.v = 0;
                    this.w.a(1);
                    removeCallbacks(this.w);
                    break;
                }
                break;
            case 3:
                c();
                break;
        }
        return true;
    }
}
