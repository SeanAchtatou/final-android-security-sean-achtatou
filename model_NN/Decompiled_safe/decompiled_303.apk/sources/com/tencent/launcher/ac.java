package com.tencent.launcher;

import android.view.View;
import android.view.animation.Animation;

final class ac implements Animation.AnimationListener {
    private /* synthetic */ QNavigation a;

    ac(QNavigation qNavigation) {
        this.a = qNavigation;
    }

    public final void onAnimationEnd(Animation animation) {
        QNavigation.a(this.a);
        if (this.a.i <= 0) {
            for (int i = 0; i < this.a.getChildCount(); i++) {
                View childAt = this.a.getChildAt(i);
                childAt.setAnimation(null);
                childAt.getLayoutParams().width = QNavigation.h;
            }
            boolean unused = this.a.j = true;
            this.a.requestLayout();
        }
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
