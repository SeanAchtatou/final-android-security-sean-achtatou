package com.tencent.launcher;

import android.widget.Toast;
import com.tencent.launcher.base.BaseApp;
import com.tencent.qqlauncher.R;

final class ft implements Runnable {
    private /* synthetic */ DeleteZone a;

    ft(DeleteZone deleteZone) {
        this.a = deleteZone;
    }

    public final void run() {
        boolean unused = this.a.k = true;
        BaseApp.c();
        if (this.a.m) {
            Toast.makeText(this.a.c, (int) R.string.drop_to_uninstall_systemapp, 500).show();
        } else if (this.a.r) {
            Toast.makeText(this.a.c, (int) R.string.drop_to_uninstall, 500).show();
        }
    }
}
