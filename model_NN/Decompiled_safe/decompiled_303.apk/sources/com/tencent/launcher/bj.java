package com.tencent.launcher;

import android.os.Handler;
import android.os.Message;
import android.os.MessageQueue;
import com.tencent.module.qqwidget.a;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.LinkedList;

final class bj extends Handler implements MessageQueue.IdleHandler {
    public boolean a = false;
    private final ArrayList b;
    private final LinkedList c;
    private final LinkedList d;
    private final bl e;
    private final WeakReference f;

    bj(Launcher launcher, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3, bl blVar) {
        this.f = new WeakReference(launcher);
        this.b = arrayList;
        this.e = blVar;
        int d2 = launcher.mWorkspace.d();
        int size = arrayList2.size();
        this.c = new LinkedList();
        this.d = new LinkedList();
        for (int i = 0; i < size; i++) {
            c cVar = (c) arrayList2.get(i);
            if (cVar.o == d2) {
                this.c.addFirst(cVar);
            } else {
                this.c.addLast(cVar);
            }
        }
        int size2 = arrayList3.size();
        for (int i2 = 0; i2 < size2; i2++) {
            a aVar = (a) arrayList3.get(i2);
            if (aVar.o == d2) {
                this.d.addFirst(aVar);
            } else {
                this.d.addLast(aVar);
            }
        }
    }

    public final void a() {
        obtainMessage(1, 0, this.b.size()).sendToTarget();
    }

    public final void handleMessage(Message message) {
        Launcher launcher = (Launcher) this.f.get();
        if (launcher != null && !this.a) {
            switch (message.what) {
                case 1:
                    launcher.bindItems(this, this.b, message.arg1, message.arg2);
                    return;
                case 2:
                    launcher.bindAppWidgets(this, this.c);
                    return;
                case 3:
                    Launcher.access$4700(launcher, this, this.e);
                    return;
                case 4:
                    launcher.bindQQWidgets(this, this.d);
                    return;
                default:
                    return;
            }
        }
    }

    public final boolean queueIdle() {
        obtainMessage(2).sendToTarget();
        obtainMessage(4).sendToTarget();
        return false;
    }
}
