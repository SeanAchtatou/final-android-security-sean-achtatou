package com.tencent.launcher;

import android.graphics.Rect;
import android.view.ContextMenu;
import android.view.View;
import java.util.ArrayList;

final class bs implements ContextMenu.ContextMenuInfo {
    View a;
    int b;
    int c;
    int d;
    int e;
    int f;
    boolean g;
    final ArrayList h = new ArrayList(100);
    int i;
    int j;
    int k;
    int l;
    final Rect m = new Rect();

    bs() {
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        ArrayList arrayList = this.h;
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            ((bk) arrayList.get(i2)).b();
        }
        arrayList.clear();
    }

    /* access modifiers changed from: package-private */
    public final boolean a(int[] iArr, int i2, int i3, boolean z) {
        boolean z2;
        boolean z3;
        ArrayList arrayList = this.h;
        int size = arrayList.size();
        if (this.d < i2 || this.e < i3) {
            z2 = false;
        } else {
            iArr[0] = this.b;
            iArr[1] = this.c;
            z2 = true;
        }
        int i4 = 0;
        while (true) {
            if (i4 >= size) {
                break;
            }
            bk bkVar = (bk) arrayList.get(i4);
            if (bkVar.c == i2 && bkVar.d == i3) {
                iArr[0] = bkVar.a;
                iArr[1] = bkVar.b;
                z2 = true;
                break;
            }
            i4++;
        }
        int i5 = 0;
        while (true) {
            if (i5 >= size) {
                z3 = z2;
                break;
            }
            bk bkVar2 = (bk) arrayList.get(i5);
            if (bkVar2.c >= i2 && bkVar2.d >= i3) {
                iArr[0] = bkVar2.a;
                iArr[1] = bkVar2.b;
                z3 = true;
                break;
            }
            i5++;
        }
        if (z) {
            a();
        }
        return z3;
    }

    public final String toString() {
        return "Cell[view=" + (this.a == null ? "null" : this.a.getClass()) + ", x=" + this.b + ", y=" + this.c + "]";
    }
}
