package com.tencent.launcher;

import android.content.Context;
import java.util.Queue;

final class ch implements Runnable {
    private /* synthetic */ QGridLayout a;

    ch(QGridLayout qGridLayout) {
        this.a = qGridLayout;
    }

    public final void run() {
        Queue d = this.a.aF;
        Context context = this.a.getContext();
        while (true) {
            ha haVar = (ha) d.poll();
            if (haVar != null) {
                ff.a(context, haVar);
            } else {
                return;
            }
        }
    }
}
