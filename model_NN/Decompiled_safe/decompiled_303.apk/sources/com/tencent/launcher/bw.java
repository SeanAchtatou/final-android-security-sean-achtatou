package com.tencent.launcher;

import java.util.Comparator;

final class bw implements Comparator {
    bw() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(java.lang.String, boolean):char
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.launcher.ff.a(android.content.Context, long):com.tencent.launcher.am
      com.tencent.launcher.ff.a(android.content.pm.PackageManager, android.content.Intent):com.tencent.launcher.am
      com.tencent.launcher.ff.a(java.util.HashMap, long):com.tencent.launcher.bq
      com.tencent.launcher.ff.a(com.tencent.launcher.ff, java.util.ArrayList):java.util.ArrayList
      com.tencent.launcher.ff.a(com.tencent.launcher.ff, java.util.HashMap):java.util.HashMap
      com.tencent.launcher.ff.a(android.content.pm.PackageManager, java.lang.String):java.util.List
      com.tencent.launcher.ff.a(android.content.ContentResolver, android.content.pm.PackageManager):void
      com.tencent.launcher.ff.a(android.content.Context, int):void
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.bq):void
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha):void
      com.tencent.launcher.ff.a(com.tencent.launcher.bq, com.tencent.launcher.ha):void
      com.tencent.launcher.ff.a(com.tencent.launcher.ff, com.tencent.launcher.Launcher):void
      com.tencent.launcher.ff.a(android.content.Context, java.lang.String):boolean
      com.tencent.launcher.ff.a(java.util.HashMap, android.content.ComponentName):boolean
      com.tencent.launcher.ff.a(java.util.List, android.content.ComponentName):boolean
      com.tencent.launcher.ff.a(android.content.pm.PackageManager, com.tencent.launcher.am):android.graphics.drawable.Drawable
      com.tencent.launcher.ff.a(com.tencent.launcher.Launcher, android.content.ComponentName):void
      com.tencent.launcher.ff.a(com.tencent.launcher.Launcher, java.lang.String):void
      com.tencent.launcher.ff.a(com.tencent.launcher.Launcher, boolean):boolean
      com.tencent.launcher.ff.a(java.lang.String, boolean):char */
    public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        bq bqVar = (bq) obj;
        bq bqVar2 = (bq) obj2;
        if (bqVar.h != null && bqVar.h.length() > 0 && bqVar2.h != null && bqVar2.h.length() > 0) {
            char a = ff.a(bqVar.h.toString(), false);
            char a2 = ff.a(bqVar2.h.toString(), false);
            if (ff.b(a)) {
                if (!ff.b(a2)) {
                    return -1;
                }
            } else if (ff.b(a2) && !ff.b(a)) {
                return 1;
            }
        }
        if (bqVar.a == null || bqVar2.a == null) {
            return 0;
        }
        return ff.b.compare(bqVar.a.toString(), bqVar2.a.toString());
    }
}
