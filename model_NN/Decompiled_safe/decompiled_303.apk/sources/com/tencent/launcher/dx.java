package com.tencent.launcher;

import java.util.Comparator;

final class dx implements Comparator {
    dx() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(java.lang.String, boolean):char
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.launcher.ff.a(android.content.Context, long):com.tencent.launcher.am
      com.tencent.launcher.ff.a(android.content.pm.PackageManager, android.content.Intent):com.tencent.launcher.am
      com.tencent.launcher.ff.a(java.util.HashMap, long):com.tencent.launcher.bq
      com.tencent.launcher.ff.a(com.tencent.launcher.ff, java.util.ArrayList):java.util.ArrayList
      com.tencent.launcher.ff.a(com.tencent.launcher.ff, java.util.HashMap):java.util.HashMap
      com.tencent.launcher.ff.a(android.content.pm.PackageManager, java.lang.String):java.util.List
      com.tencent.launcher.ff.a(android.content.ContentResolver, android.content.pm.PackageManager):void
      com.tencent.launcher.ff.a(android.content.Context, int):void
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.bq):void
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha):void
      com.tencent.launcher.ff.a(com.tencent.launcher.bq, com.tencent.launcher.ha):void
      com.tencent.launcher.ff.a(com.tencent.launcher.ff, com.tencent.launcher.Launcher):void
      com.tencent.launcher.ff.a(android.content.Context, java.lang.String):boolean
      com.tencent.launcher.ff.a(java.util.HashMap, android.content.ComponentName):boolean
      com.tencent.launcher.ff.a(java.util.List, android.content.ComponentName):boolean
      com.tencent.launcher.ff.a(android.content.pm.PackageManager, com.tencent.launcher.am):android.graphics.drawable.Drawable
      com.tencent.launcher.ff.a(com.tencent.launcher.Launcher, android.content.ComponentName):void
      com.tencent.launcher.ff.a(com.tencent.launcher.Launcher, java.lang.String):void
      com.tencent.launcher.ff.a(com.tencent.launcher.Launcher, boolean):boolean
      com.tencent.launcher.ff.a(java.lang.String, boolean):char */
    public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        gy gyVar = (gy) obj;
        gy gyVar2 = (gy) obj2;
        char a = ff.a(gyVar.a.b, false);
        char a2 = ff.a(gyVar2.a.b, false);
        if (ff.a(a) && ff.a(a2)) {
            if (Character.isLowerCase(a)) {
                a = Character.toUpperCase(a);
            }
            if (Character.isLowerCase(a2)) {
                a2 = Character.toUpperCase(a2);
            }
            if (a == a2) {
                char a3 = ff.a(gyVar.a.a.toString(), true);
                char a4 = ff.a(gyVar2.a.a.toString(), true);
                if (ff.a(a3) && !ff.a(a4)) {
                    return -1;
                }
                if (!ff.a(a3) && ff.a(a4)) {
                    return 1;
                }
            }
        } else if (ff.a(a) && !ff.a(a2)) {
            return -1;
        } else {
            if (!ff.a(a) && ff.a(a2)) {
                return 1;
            }
        }
        return ff.b.compare(gyVar.a.b.toString(), gyVar2.a.b.toString());
    }
}
