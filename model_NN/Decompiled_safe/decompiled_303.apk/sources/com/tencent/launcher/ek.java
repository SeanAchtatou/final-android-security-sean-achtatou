package com.tencent.launcher;

import android.view.animation.Animation;

final class ek implements Animation.AnimationListener {
    private /* synthetic */ ApplicationsDrawer a;

    ek(ApplicationsDrawer applicationsDrawer) {
        this.a = applicationsDrawer;
    }

    public final void onAnimationEnd(Animation animation) {
        this.a.f();
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
        boolean unused = this.a.f = false;
    }
}
