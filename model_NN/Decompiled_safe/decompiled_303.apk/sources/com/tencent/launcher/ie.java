package com.tencent.launcher;

import android.database.ContentObserver;
import android.os.Handler;

final class ie extends ContentObserver {
    private /* synthetic */ Launcher a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ie(Launcher launcher) {
        super(new Handler());
        this.a = launcher;
    }

    public final void onChange(boolean z) {
        this.a.mAppWidgetHost.startListening();
    }
}
