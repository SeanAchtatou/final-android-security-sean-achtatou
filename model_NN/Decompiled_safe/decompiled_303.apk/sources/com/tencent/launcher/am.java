package com.tencent.launcher;

import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public final class am extends ha {
    public CharSequence a;
    String b;
    public Intent c;
    public Drawable d;
    boolean e;
    boolean f;
    boolean g;
    public Intent.ShortcutIconResource h;
    boolean i = false;
    long j;
    int k;

    am() {
        this.m = 1;
    }

    public am(am amVar) {
        super(amVar);
        this.a = amVar.a.toString();
        this.c = new Intent(amVar.c);
        if (amVar.h != null) {
            this.h = new Intent.ShortcutIconResource();
            this.h.packageName = amVar.h.packageName;
            this.h.resourceName = amVar.h.resourceName;
        }
        this.d = amVar.d;
        this.f = amVar.f;
        this.g = amVar.g;
        this.i = amVar.i;
        this.j = amVar.j;
        this.e = amVar.e;
        this.b = amVar.b;
        this.k = amVar.k;
    }

    public final void a() {
        if (this.d != null) {
            this.d.setCallback(null);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(ComponentName componentName) {
        this.c = new Intent("android.intent.action.MAIN");
        this.c.addCategory("android.intent.category.LAUNCHER");
        this.c.setComponent(componentName);
        this.c.setFlags(270532608);
        this.m = 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public final void a(ContentValues contentValues) {
        super.a(contentValues);
        contentValues.put("title", this.a != null ? this.a.toString() : null);
        contentValues.put("intent", this.c != null ? this.c.toURI() : null);
        if (this.g) {
            contentValues.put("iconType", (Integer) 1);
            Bitmap a2 = a.a(this.d);
            if (a2 != null) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(a2.getWidth() * a2.getHeight() * 4);
                try {
                    a2.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byteArrayOutputStream.flush();
                    byteArrayOutputStream.close();
                    contentValues.put("icon", byteArrayOutputStream.toByteArray());
                } catch (IOException e2) {
                    Log.w("Favorite", "Could not write icon");
                }
            }
        } else {
            contentValues.put("iconType", (Integer) 0);
            if (this.h != null) {
                contentValues.put("iconPackage", this.h.packageName);
                contentValues.put("iconResource", this.h.resourceName);
            }
        }
    }

    public final String toString() {
        if (this.a == null) {
            return null;
        }
        return this.a.toString();
    }
}
