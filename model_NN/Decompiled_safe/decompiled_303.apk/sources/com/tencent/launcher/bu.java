package com.tencent.launcher;

import android.content.Context;
import java.util.Iterator;

final class bu implements Runnable {
    private Context a;
    private bq b;
    private int c;
    private int d;
    private int e;

    public bu(Context context, bq bqVar, int i, int i2, int i3) {
        this.a = context;
        this.b = bqVar;
        this.c = i;
        this.d = i2;
        this.e = i3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void
     arg types: [android.content.Context, com.tencent.launcher.bq, int, int, int, int, int]
     candidates:
      com.tencent.launcher.ff.a(android.database.Cursor, android.content.Context, int, int, int, int, android.content.Intent):com.tencent.launcher.am
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void
     arg types: [android.content.Context, com.tencent.launcher.am, long, int, int, int, int]
     candidates:
      com.tencent.launcher.ff.a(android.database.Cursor, android.content.Context, int, int, int, int, android.content.Intent):com.tencent.launcher.am
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void */
    public final void run() {
        ff.a(this.a, (ha) this.b, -100L, this.c, this.d, this.e, false);
        Iterator it = this.b.b.iterator();
        while (it.hasNext()) {
            ff.a(this.a, (ha) ((am) it.next()), this.b.l, this.c, 0, 0, false);
        }
    }
}
