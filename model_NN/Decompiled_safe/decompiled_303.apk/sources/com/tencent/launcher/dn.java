package com.tencent.launcher;

import android.os.Handler;
import android.view.animation.Animation;

final class dn implements Animation.AnimationListener {
    final /* synthetic */ Launcher a;
    final /* synthetic */ UserFolder b;

    dn(UserFolder userFolder, Launcher launcher) {
        this.b = userFolder;
        this.a = launcher;
    }

    public final void onAnimationEnd(Animation animation) {
        new Handler().postAtTime(new hb(this), 100);
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
