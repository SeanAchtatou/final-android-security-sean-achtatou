package com.tencent.launcher;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DrawFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import com.tencent.module.theme.l;
import com.tencent.qqlauncher.R;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class DockView extends ImageView {
    private static int a = -1;
    private static final Random c = new Random();
    private Context b;
    private int d;
    private int e;
    private int f = 2;
    private int g;
    private DrawFilter h;
    private TranslateAnimation i;
    private Matrix j;
    private Matrix k = new Matrix();
    private ha l;
    private Drawable m;
    private int n;
    private Paint o = new Paint();
    private Bitmap p;
    private int q = -1;
    private int r = -1;
    private boolean s;
    private boolean t;

    public DockView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = context;
        a = (int) context.getResources().getDimension(R.dimen.dock_icon_size);
        this.n = BrightnessActivity.MAXIMUM_BACKLIGHT;
        this.h = new PaintFlagsDrawFilter(0, 3);
        this.g = c.nextInt(6) - 3;
        this.d = c.nextInt(50);
        this.e = c.nextInt(50);
        this.f = c.nextBoolean() ? -2 : 2;
        this.i = new TranslateAnimation(getContext(), null);
        this.i.setFillAfter(true);
        this.i.setInterpolator(new AccelerateDecelerateInterpolator());
        setDrawingCacheEnabled(true);
    }

    private void a(Drawable drawable) {
        if (this.m != null) {
            this.m.setCallback(null);
            this.m = null;
        }
        this.m = drawable;
        setImageDrawable(this.m);
    }

    private Matrix i() {
        try {
            Field declaredField = Class.forName("android.widget.ImageView").getDeclaredField("mDrawMatrix");
            declaredField.setAccessible(true);
            return (Matrix) declaredField.get(this);
        } catch (IllegalArgumentException e2) {
            e2.printStackTrace();
            return null;
        } catch (SecurityException e3) {
            e3.printStackTrace();
            return null;
        } catch (IllegalAccessException e4) {
            e4.printStackTrace();
            return null;
        } catch (NoSuchFieldException e5) {
            e5.printStackTrace();
            return null;
        } catch (ClassNotFoundException e6) {
            e6.printStackTrace();
            return null;
        }
    }

    private void j() {
        int height = getHeight();
        if (this.p != null) {
            Rect bounds = getDrawable().getBounds();
            height = (bounds.height() * a.a(this.p)) / this.p.getHeight();
        }
        float[] fArr = new float[9];
        getImageMatrix().getValues(fArr);
        this.r = (int) fArr[5];
        this.q = height;
    }

    public final int a() {
        if (this.r < 0) {
            float[] fArr = new float[9];
            getImageMatrix().getValues(fArr);
            this.r = (int) fArr[5];
        }
        return this.q + this.r;
    }

    /* access modifiers changed from: protected */
    public final void a(ha haVar) {
        this.l = haVar;
        c();
    }

    public final void a(boolean z) {
        if (z) {
            this.n = 50;
        } else {
            this.n = BrightnessActivity.MAXIMUM_BACKLIGHT;
        }
        invalidate();
    }

    public final boolean b() {
        return this.n == 50;
    }

    public final void c() {
        Drawable drawable;
        ha haVar = this.l;
        Resources resources = getResources();
        Context context = getContext();
        if (haVar != null) {
            if (haVar instanceof am) {
                am amVar = (am) haVar;
                amVar.d = a.b(context, amVar.d, amVar);
                Drawable drawable2 = amVar.d;
                amVar.e = false;
                drawable = drawable2;
            } else if (haVar instanceof dq) {
                dq dqVar = (dq) haVar;
                Drawable b2 = a.b(getContext(), getContext().getResources().getDrawable(R.drawable.ic_launcher_folder), (ha) null);
                if (b2 != null) {
                    dqVar.d = b2;
                }
                drawable = dqVar.d;
            } else if (haVar instanceof bq) {
                bq bqVar = (bq) haVar;
                if (bqVar.b == null || bqVar.b.size() == 0) {
                    drawable = a.b(context, l.a().a(context.getResources()), l.a().b(context.getResources()), bqVar);
                } else {
                    ArrayList arrayList = new ArrayList();
                    Iterator it = bqVar.b.iterator();
                    while (it.hasNext()) {
                        am amVar2 = (am) it.next();
                        if ((amVar2.d instanceof BitmapDrawable) || (amVar2.d instanceof hp) || (amVar2.d instanceof gb) || (amVar2.d instanceof gm)) {
                            arrayList.add(amVar2.d);
                        }
                    }
                    drawable = a.b(context, a.a(arrayList, l.a().a(context.getResources()), context), l.a().b(context.getResources()), bqVar);
                }
            }
            a(drawable);
            ((ViewGroup) getParent()).postInvalidate();
        }
        drawable = resources.getDrawable(R.drawable.dock_empty);
        a(drawable);
        ((ViewGroup) getParent()).postInvalidate();
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:36:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void d() {
        /*
            r7 = this;
            boolean r0 = r7.s
            if (r0 == 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            com.tencent.launcher.ha r0 = r7.l
            if (r0 == 0) goto L_0x0004
            com.tencent.launcher.ha r0 = r7.l
            boolean r0 = r0 instanceof com.tencent.launcher.bq
            if (r0 == 0) goto L_0x0004
            r0 = 1
            r7.s = r0
            r2 = 0
            com.tencent.launcher.ha r0 = r7.l     // Catch:{ Exception -> 0x0063 }
            com.tencent.launcher.bq r0 = (com.tencent.launcher.bq) r0     // Catch:{ Exception -> 0x0063 }
            android.content.Context r3 = r7.getContext()     // Catch:{ Exception -> 0x0063 }
            java.util.ArrayList r1 = r0.b     // Catch:{ Exception -> 0x0063 }
            if (r1 == 0) goto L_0x0027
            java.util.ArrayList r1 = r0.b     // Catch:{ Exception -> 0x0063 }
            int r1 = r1.size()     // Catch:{ Exception -> 0x0063 }
            if (r1 != 0) goto L_0x0034
        L_0x0027:
            android.graphics.drawable.Drawable r0 = com.tencent.launcher.Launcher.getDefaultFloderIconOpenedInDock(r3, r0)     // Catch:{ Exception -> 0x0063 }
        L_0x002b:
            if (r0 == 0) goto L_0x0004
            r7.a(r0)
            r7.invalidate()
            goto L_0x0004
        L_0x0034:
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ Exception -> 0x0063 }
            r4.<init>()     // Catch:{ Exception -> 0x0063 }
            java.util.ArrayList r1 = r0.b     // Catch:{ Exception -> 0x0063 }
            java.util.Iterator r5 = r1.iterator()     // Catch:{ Exception -> 0x0063 }
        L_0x003f:
            boolean r1 = r5.hasNext()     // Catch:{ Exception -> 0x0063 }
            if (r1 == 0) goto L_0x006d
            java.lang.Object r1 = r5.next()     // Catch:{ Exception -> 0x0063 }
            com.tencent.launcher.am r1 = (com.tencent.launcher.am) r1     // Catch:{ Exception -> 0x0063 }
            android.graphics.drawable.Drawable r6 = r1.d     // Catch:{ Exception -> 0x0063 }
            boolean r6 = r6 instanceof android.graphics.drawable.BitmapDrawable     // Catch:{ Exception -> 0x0063 }
            if (r6 != 0) goto L_0x005d
            android.graphics.drawable.Drawable r6 = r1.d     // Catch:{ Exception -> 0x0063 }
            boolean r6 = r6 instanceof com.tencent.launcher.hp     // Catch:{ Exception -> 0x0063 }
            if (r6 != 0) goto L_0x005d
            android.graphics.drawable.Drawable r6 = r1.d     // Catch:{ Exception -> 0x0063 }
            boolean r6 = r6 instanceof com.tencent.launcher.gb     // Catch:{ Exception -> 0x0063 }
            if (r6 == 0) goto L_0x003f
        L_0x005d:
            android.graphics.drawable.Drawable r1 = r1.d     // Catch:{ Exception -> 0x0063 }
            r4.add(r1)     // Catch:{ Exception -> 0x0063 }
            goto L_0x003f
        L_0x0063:
            r0 = move-exception
            java.lang.String r0 = ""
            java.lang.String r1 = "create bitmap failed..."
            android.util.Log.e(r0, r1)
            r0 = r2
            goto L_0x002b
        L_0x006d:
            com.tencent.module.theme.l r1 = com.tencent.module.theme.l.a()     // Catch:{ Exception -> 0x0063 }
            android.content.res.Resources r5 = r3.getResources()     // Catch:{ Exception -> 0x0063 }
            android.graphics.drawable.Drawable r1 = r1.a(r5)     // Catch:{ Exception -> 0x0063 }
            android.graphics.drawable.Drawable r1 = com.tencent.launcher.a.a(r4, r1, r3)     // Catch:{ Exception -> 0x0063 }
            com.tencent.module.theme.l r4 = com.tencent.module.theme.l.a()     // Catch:{ Exception -> 0x0063 }
            android.content.res.Resources r5 = r3.getResources()     // Catch:{ Exception -> 0x0063 }
            android.graphics.drawable.Drawable r4 = r4.c(r5)     // Catch:{ Exception -> 0x0063 }
            android.graphics.drawable.Drawable r0 = com.tencent.launcher.a.b(r3, r1, r4, r0)     // Catch:{ Exception -> 0x0063 }
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.launcher.DockView.d():void");
    }

    public final void e() {
        if (this.s) {
            c();
            this.s = false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:36:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void f() {
        /*
            r7 = this;
            boolean r0 = r7.t
            if (r0 == 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            com.tencent.launcher.ha r0 = r7.l
            if (r0 == 0) goto L_0x0004
            com.tencent.launcher.ha r0 = r7.l
            boolean r0 = r0 instanceof com.tencent.launcher.bq
            if (r0 == 0) goto L_0x0004
            r0 = 1
            r7.t = r0
            r2 = 0
            com.tencent.launcher.ha r0 = r7.l     // Catch:{ Exception -> 0x0063 }
            com.tencent.launcher.bq r0 = (com.tencent.launcher.bq) r0     // Catch:{ Exception -> 0x0063 }
            android.content.Context r3 = r7.getContext()     // Catch:{ Exception -> 0x0063 }
            java.util.ArrayList r1 = r0.b     // Catch:{ Exception -> 0x0063 }
            if (r1 == 0) goto L_0x0027
            java.util.ArrayList r1 = r0.b     // Catch:{ Exception -> 0x0063 }
            int r1 = r1.size()     // Catch:{ Exception -> 0x0063 }
            if (r1 != 0) goto L_0x0034
        L_0x0027:
            android.graphics.drawable.Drawable r0 = com.tencent.launcher.Launcher.getDefaultFloderIconAddingInDock(r3, r0)     // Catch:{ Exception -> 0x0063 }
        L_0x002b:
            if (r0 == 0) goto L_0x0004
            r7.a(r0)
            r7.invalidate()
            goto L_0x0004
        L_0x0034:
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ Exception -> 0x0063 }
            r4.<init>()     // Catch:{ Exception -> 0x0063 }
            java.util.ArrayList r1 = r0.b     // Catch:{ Exception -> 0x0063 }
            java.util.Iterator r5 = r1.iterator()     // Catch:{ Exception -> 0x0063 }
        L_0x003f:
            boolean r1 = r5.hasNext()     // Catch:{ Exception -> 0x0063 }
            if (r1 == 0) goto L_0x006d
            java.lang.Object r1 = r5.next()     // Catch:{ Exception -> 0x0063 }
            com.tencent.launcher.am r1 = (com.tencent.launcher.am) r1     // Catch:{ Exception -> 0x0063 }
            android.graphics.drawable.Drawable r6 = r1.d     // Catch:{ Exception -> 0x0063 }
            boolean r6 = r6 instanceof android.graphics.drawable.BitmapDrawable     // Catch:{ Exception -> 0x0063 }
            if (r6 != 0) goto L_0x005d
            android.graphics.drawable.Drawable r6 = r1.d     // Catch:{ Exception -> 0x0063 }
            boolean r6 = r6 instanceof com.tencent.launcher.hp     // Catch:{ Exception -> 0x0063 }
            if (r6 != 0) goto L_0x005d
            android.graphics.drawable.Drawable r6 = r1.d     // Catch:{ Exception -> 0x0063 }
            boolean r6 = r6 instanceof com.tencent.launcher.gb     // Catch:{ Exception -> 0x0063 }
            if (r6 == 0) goto L_0x003f
        L_0x005d:
            android.graphics.drawable.Drawable r1 = r1.d     // Catch:{ Exception -> 0x0063 }
            r4.add(r1)     // Catch:{ Exception -> 0x0063 }
            goto L_0x003f
        L_0x0063:
            r0 = move-exception
            java.lang.String r0 = ""
            java.lang.String r1 = "create bitmap failed..."
            android.util.Log.e(r0, r1)
            r0 = r2
            goto L_0x002b
        L_0x006d:
            android.content.res.Resources r1 = r3.getResources()     // Catch:{ Exception -> 0x0063 }
            r5 = 2130837638(0x7f020086, float:1.7280236E38)
            android.graphics.drawable.Drawable r1 = r1.getDrawable(r5)     // Catch:{ Exception -> 0x0063 }
            android.graphics.drawable.Drawable r1 = com.tencent.launcher.a.a(r4, r1, r3)     // Catch:{ Exception -> 0x0063 }
            com.tencent.module.theme.l r4 = com.tencent.module.theme.l.a()     // Catch:{ Exception -> 0x0063 }
            android.content.res.Resources r5 = r3.getResources()     // Catch:{ Exception -> 0x0063 }
            android.graphics.drawable.Drawable r4 = r4.c(r5)     // Catch:{ Exception -> 0x0063 }
            android.graphics.drawable.Drawable r0 = com.tencent.launcher.a.b(r3, r1, r4, r0)     // Catch:{ Exception -> 0x0063 }
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.launcher.DockView.f():void");
    }

    public View focusSearch(int i2) {
        View focusSearch = super.focusSearch(i2);
        if (focusSearch != null || !Launcher.getLauncher().isDrawerClose()) {
            return focusSearch;
        }
        Workspace workspace = Launcher.getLauncher().getWorkspace();
        workspace.dispatchUnhandledMove(null, i2);
        return workspace;
    }

    public final void g() {
        if (this.t) {
            c();
            this.t = false;
        }
    }

    public Object getTag() {
        return this.l;
    }

    public final boolean h() {
        if (this.l == null) {
            return false;
        }
        return this.l instanceof ex;
    }

    public void onDraw(Canvas canvas) {
        if (this.n != 255) {
            canvas.save();
            canvas.saveLayerAlpha(0.0f, 0.0f, (float) getWidth(), (float) getHeight(), this.n, 20);
            super.onDraw(canvas);
            canvas.save();
            canvas.save();
            return;
        }
        super.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        this.j = i();
    }

    /* access modifiers changed from: protected */
    public boolean setFrame(int i2, int i3, int i4, int i5) {
        boolean frame = super.setFrame(i2, i3, i4, i5);
        this.j = i();
        return frame;
    }

    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        this.j = i();
        this.p = a.a(getDrawable());
        j();
    }

    public void setImageMatrix(Matrix matrix) {
        super.setImageMatrix(matrix);
        this.j = i();
    }

    public void setImageResource(int i2) {
        super.setImageResource(i2);
        this.j = i();
        this.p = a.a(getDrawable());
        j();
    }

    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        this.j = i();
    }
}
