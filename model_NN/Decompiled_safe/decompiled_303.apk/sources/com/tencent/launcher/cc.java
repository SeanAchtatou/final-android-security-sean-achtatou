package com.tencent.launcher;

import AndroidDLoader.Software;
import AndroidDLoader.a;
import android.view.View;
import android.widget.AdapterView;
import com.tencent.module.appcenter.SoftWareActivity;

final class cc implements AdapterView.OnItemClickListener {
    private /* synthetic */ MarketSearchAppActivity a;

    cc(MarketSearchAppActivity marketSearchAppActivity) {
        this.a = marketSearchAppActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Software software = (Software) adapterView.getAdapter().getItem(i);
        if (software != null) {
            SoftWareActivity.openSoftwareActivity(this.a, software.a(), software.c(), software.g(), a.f);
        }
    }
}
