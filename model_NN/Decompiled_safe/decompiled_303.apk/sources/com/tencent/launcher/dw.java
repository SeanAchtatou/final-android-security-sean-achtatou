package com.tencent.launcher;

import java.lang.ref.WeakReference;

final class dw implements Runnable {
    private final WeakReference a;
    /* access modifiers changed from: private */
    public volatile boolean b;
    private volatile boolean c = true;
    private final boolean d;
    private final int e = ff.y.getAndIncrement();
    private /* synthetic */ ff f;

    dw(ff ffVar, Launcher launcher, boolean z) {
        this.f = ffVar;
        this.d = z;
        this.a = new WeakReference(launcher);
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.b = true;
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        return this.c;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0131, code lost:
        if ("".equals(r3.a) != false) goto L_0x0133;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0228, code lost:
        if ("".equals(r28.b) != false) goto L_0x022a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r33 = this;
            r0 = r33
            boolean r0 = r0.d
            r3 = r0
            if (r3 == 0) goto L_0x00f1
            r3 = 0
        L_0x0008:
            android.os.Process.setThreadPriority(r3)
            r0 = r33
            java.lang.ref.WeakReference r0 = r0.a
            r3 = r0
            java.lang.Object r3 = r3.get()
            r0 = r3
            com.tencent.launcher.Launcher r0 = (com.tencent.launcher.Launcher) r0
            r11 = r0
            android.content.pm.PackageManager r12 = r11.getPackageManager()
            java.lang.String r3 = android.os.Environment.getExternalStorageState()
            java.lang.String r4 = "mounted"
            boolean r13 = r3.equals(r4)
            r0 = r33
            com.tencent.launcher.ff r0 = r0.f
            r3 = r0
            com.tencent.launcher.bl r9 = r3.k
            com.tencent.launcher.fj r10 = new com.tencent.launcher.fj
            r3 = 1
            r10.<init>(r9, r3)
            r0 = r33
            com.tencent.launcher.ff r0 = r0.f
            r3 = r0
            java.util.HashMap r14 = r3.p
            r0 = r33
            com.tencent.launcher.ff r0 = r0.f
            r3 = r0
            java.util.HashMap r15 = r3.j
            android.content.ContentResolver r3 = r11.getContentResolver()
            android.net.Uri r4 = com.tencent.launcher.fg.a
            r5 = 0
            r6 = 0
            r7 = 0
            java.lang.String r8 = "position,orderId"
            android.database.Cursor r16 = r3.query(r4, r5, r6, r7, r8)
            r3 = 0
            r4 = -1
            r5 = 0
            java.lang.String r6 = "_id"
            r0 = r16
            r1 = r6
            int r6 = r0.getColumnIndexOrThrow(r1)
            java.lang.String r7 = "title"
            r0 = r16
            r1 = r7
            int r7 = r0.getColumnIndexOrThrow(r1)
            java.lang.String r8 = "container"
            r0 = r16
            r1 = r8
            int r17 = r0.getColumnIndexOrThrow(r1)
            java.lang.String r8 = "position"
            r0 = r16
            r1 = r8
            int r18 = r0.getColumnIndexOrThrow(r1)
            java.lang.String r8 = "intent"
            r0 = r16
            r1 = r8
            int r19 = r0.getColumnIndexOrThrow(r1)
            java.lang.String r8 = "itemType"
            r0 = r16
            r1 = r8
            int r20 = r0.getColumnIndexOrThrow(r1)
            java.lang.String r8 = "install"
            r0 = r16
            r1 = r8
            int r21 = r0.getColumnIndexOrThrow(r1)
            java.lang.String r8 = "callednum"
            r0 = r16
            r1 = r8
            int r22 = r0.getColumnIndexOrThrow(r1)
            r8 = r10
            r10 = r5
            r5 = r3
        L_0x00a4:
            boolean r3 = r16.moveToNext()
            if (r3 == 0) goto L_0x02d0
            r0 = r33
            boolean r0 = r0.b
            r3 = r0
            if (r3 != 0) goto L_0x02d0
            r0 = r16
            r1 = r6
            long r23 = r0.getLong(r1)
            long r25 = r16.getLong(r17)
            r0 = r16
            r1 = r18
            int r27 = r0.getInt(r1)
            r0 = r16
            r1 = r7
            java.lang.String r28 = r0.getString(r1)
            r0 = r16
            r1 = r19
            java.lang.String r3 = r0.getString(r1)
            r0 = r16
            r1 = r20
            int r29 = r0.getInt(r1)
            r0 = r16
            r1 = r21
            long r30 = r0.getLong(r1)
            r0 = r16
            r1 = r22
            int r32 = r0.getInt(r1)
            switch(r29) {
                case 0: goto L_0x0175;
                case 1: goto L_0x00ee;
                case 2: goto L_0x00f5;
                default: goto L_0x00ee;
            }
        L_0x00ee:
            r3 = r10
        L_0x00ef:
            r10 = r3
            goto L_0x00a4
        L_0x00f1:
            r3 = 10
            goto L_0x0008
        L_0x00f5:
            java.lang.Long r3 = java.lang.Long.valueOf(r23)
            boolean r3 = r15.containsKey(r3)
            if (r3 != 0) goto L_0x016a
            com.tencent.launcher.bq r3 = new com.tencent.launcher.bq
            r3.<init>()
        L_0x0104:
            r0 = r23
            r2 = r3
            r2.l = r0
            r0 = r25
            r2 = r3
            r2.n = r0
            r0 = r28
            r1 = r3
            r1.h = r0
            java.lang.CharSequence r10 = r3.h
            java.lang.String r10 = r10.toString()
            java.lang.String r10 = com.tencent.launcher.home.c.d(r10)
            r3.a = r10
            java.lang.String r10 = r3.a
            if (r10 == 0) goto L_0x0133
            java.lang.String r10 = ""
            r0 = r3
            java.lang.String r0 = r0.a
            r28 = r0
            r0 = r10
            r1 = r28
            boolean r10 = r0.equals(r1)
            if (r10 == 0) goto L_0x013b
        L_0x0133:
            java.lang.CharSequence r10 = r3.h
            java.lang.String r10 = r10.toString()
            r3.a = r10
        L_0x013b:
            r0 = r25
            r2 = r3
            r2.n = r0
            int r10 = r27 - r5
            r3.t = r10
            int r10 = r3.t
            java.lang.Long r23 = java.lang.Long.valueOf(r23)
            r0 = r15
            r1 = r23
            r2 = r3
            r0.put(r1, r2)
            boolean r3 = r8.a(r3)
            if (r3 == 0) goto L_0x00ee
            r0 = r33
            boolean r0 = r0.b
            r3 = r0
            if (r3 != 0) goto L_0x00ee
            r11.runOnUiThread(r8)
            com.tencent.launcher.fj r3 = new com.tencent.launcher.fj
            r8 = 0
            r3.<init>(r9, r8)
            r8 = r3
            goto L_0x00a4
        L_0x016a:
            java.lang.Long r3 = java.lang.Long.valueOf(r23)
            java.lang.Object r3 = r15.get(r3)
            com.tencent.launcher.bq r3 = (com.tencent.launcher.bq) r3
            goto L_0x0104
        L_0x0175:
            com.tencent.launcher.am r28 = new com.tencent.launcher.am
            r28.<init>()
            r0 = r23
            r2 = r28
            r2.l = r0
            r23 = 0
            r0 = r3
            r1 = r23
            android.content.Intent r3 = android.content.Intent.parseUri(r0, r1)     // Catch:{ URISyntaxException -> 0x01b6 }
            r0 = r3
            r1 = r28
            r1.c = r0     // Catch:{ URISyntaxException -> 0x01b6 }
        L_0x018e:
            r0 = r28
            android.content.Intent r0 = r0.c
            r3 = r0
            r23 = 0
            r0 = r3
            r1 = r12
            r2 = r23
            android.content.pm.ActivityInfo r3 = r0.resolveActivityInfo(r1, r2)
            if (r3 != 0) goto L_0x01bb
            if (r4 >= 0) goto L_0x0324
            r3 = r10
        L_0x01a2:
            int r4 = r5 + 1
            if (r13 != 0) goto L_0x0320
            r23 = -300(0xfffffffffffffed4, double:NaN)
            int r5 = (r25 > r23 ? 1 : (r25 == r23 ? 0 : -1))
            if (r5 != 0) goto L_0x0320
            r0 = r11
            r1 = r28
            com.tencent.launcher.ff.f(r0, r1)
            r5 = r4
            r4 = r3
            goto L_0x00a4
        L_0x01b6:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x018e
        L_0x01bb:
            r0 = r3
            android.content.pm.ApplicationInfo r0 = r0.applicationInfo
            r23 = r0
            r0 = r23
            java.lang.String r0 = r0.packageName
            r23 = r0
            if (r23 == 0) goto L_0x01db
            r0 = r3
            android.content.pm.ApplicationInfo r0 = r0.applicationInfo
            r23 = r0
            r0 = r23
            java.lang.String r0 = r0.packageName
            r23 = r0
            java.lang.String r24 = "com.tencent.qqlauncher.theme"
            boolean r23 = r23.startsWith(r24)
            if (r23 != 0) goto L_0x00a4
        L_0x01db:
            android.graphics.drawable.Drawable r23 = r3.loadIcon(r12)
            r0 = r23
            r1 = r28
            r1.d = r0
            java.lang.CharSequence r3 = r3.loadLabel(r12)
            r0 = r3
            r1 = r28
            r1.a = r0
            r0 = r25
            r2 = r28
            r2.n = r0
            r0 = r30
            r2 = r28
            r2.j = r0
            r0 = r32
            r1 = r28
            r1.k = r0
            r0 = r28
            java.lang.CharSequence r0 = r0.a
            r3 = r0
            java.lang.String r3 = r3.toString()
            java.lang.String r3 = com.tencent.launcher.home.c.d(r3)
            r0 = r3
            r1 = r28
            r1.b = r0
            r0 = r28
            java.lang.String r0 = r0.b
            r3 = r0
            if (r3 == 0) goto L_0x022a
            java.lang.String r3 = ""
            r0 = r28
            java.lang.String r0 = r0.b
            r23 = r0
            r0 = r3
            r1 = r23
            boolean r3 = r0.equals(r1)
            if (r3 == 0) goto L_0x0238
        L_0x022a:
            r0 = r28
            java.lang.CharSequence r0 = r0.a
            r3 = r0
            java.lang.String r3 = r3.toString()
            r0 = r3
            r1 = r28
            r1.b = r0
        L_0x0238:
            r0 = r29
            r1 = r28
            r1.m = r0
            r0 = r28
            android.content.Intent r0 = r0.c
            r3 = r0
            android.content.ComponentName r3 = r3.getComponent()
            java.lang.Object r3 = r14.get(r3)
            if (r3 != 0) goto L_0x00a4
            r0 = r28
            android.content.Intent r0 = r0.c
            r3 = r0
            android.content.ComponentName r3 = r3.getComponent()
            r0 = r14
            r1 = r3
            r2 = r28
            r0.put(r1, r2)
            r0 = r33
            boolean r0 = r0.b
            r3 = r0
            if (r3 != 0) goto L_0x00ee
            r23 = -300(0xfffffffffffffed4, double:NaN)
            int r3 = (r25 > r23 ? 1 : (r25 == r23 ? 0 : -1))
            if (r3 == 0) goto L_0x02b0
            java.lang.Long r3 = java.lang.Long.valueOf(r25)
            boolean r3 = r15.containsKey(r3)
            if (r3 != 0) goto L_0x0289
            com.tencent.launcher.bq r3 = new com.tencent.launcher.bq
            r3.<init>()
            r0 = r25
            r2 = r3
            r2.l = r0
            java.lang.Long r23 = java.lang.Long.valueOf(r25)
            r0 = r15
            r1 = r23
            r2 = r3
            r0.put(r1, r2)
        L_0x0289:
            java.lang.Long r3 = java.lang.Long.valueOf(r25)
            java.lang.Object r3 = r15.get(r3)
            com.tencent.launcher.bq r3 = (com.tencent.launcher.bq) r3
            r0 = r3
            long r0 = r0.l
            r23 = r0
            r0 = r23
            r2 = r28
            r2.n = r0
            java.lang.Long r3 = java.lang.Long.valueOf(r25)
            java.lang.Object r3 = r15.get(r3)
            com.tencent.launcher.bq r3 = (com.tencent.launcher.bq) r3
            r0 = r3
            r1 = r28
            r0.a(r1)
            goto L_0x00a4
        L_0x02b0:
            int r3 = r27 - r5
            r0 = r3
            r1 = r28
            r1.t = r0
            r0 = r28
            int r0 = r0.t
            r3 = r0
            r0 = r8
            r1 = r28
            boolean r10 = r0.a(r1)
            if (r10 == 0) goto L_0x00ef
            r11.runOnUiThread(r8)
            com.tencent.launcher.fj r8 = new com.tencent.launcher.fj
            r10 = 0
            r8.<init>(r9, r10)
            goto L_0x00ef
        L_0x02d0:
            if (r5 <= 0) goto L_0x02dd
            r0 = r33
            boolean r0 = r0.b
            r3 = r0
            if (r3 != 0) goto L_0x02dd
            long r3 = (long) r4
            com.tencent.launcher.ff.a(r11, r3, r5)
        L_0x02dd:
            r3 = r33
            r4 = r11
            r5 = r12
            r6 = r14
            r7 = r15
            com.tencent.launcher.ff.a(r3, r4, r5, r6, r7, r8, r9, r10)
            r0 = r33
            com.tencent.launcher.ff r0 = r0.f
            r3 = r0
            com.tencent.launcher.ff.a(r3, r11)
            r11.runOnUiThread(r8)
            r16.close()
            r0 = r33
            com.tencent.launcher.ff r0 = r0.f
            r3 = r0
            monitor-enter(r3)
            r0 = r33
            boolean r0 = r0.b     // Catch:{ all -> 0x031d }
            r4 = r0
            if (r4 != 0) goto L_0x0309
            r0 = r33
            com.tencent.launcher.ff r0 = r0.f     // Catch:{ all -> 0x031d }
            r4 = r0
            boolean unused = r4.c = true     // Catch:{ all -> 0x031d }
        L_0x0309:
            monitor-exit(r3)     // Catch:{ all -> 0x031d }
            r3 = 0
            r0 = r3
            r1 = r33
            r1.c = r0
            com.tencent.launcher.bi r3 = new com.tencent.launcher.bi
            r0 = r3
            r1 = r33
            r2 = r11
            r0.<init>(r1, r2)
            r11.runOnUiThread(r3)
            return
        L_0x031d:
            r4 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x031d }
            throw r4
        L_0x0320:
            r5 = r4
            r4 = r3
            goto L_0x00a4
        L_0x0324:
            r3 = r4
            goto L_0x01a2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.launcher.dw.run():void");
    }
}
