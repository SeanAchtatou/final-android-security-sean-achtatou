package com.tencent.launcher;

import android.app.Dialog;
import android.appwidget.AppWidgetProviderInfo;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.tencent.launcher.base.b;
import com.tencent.module.setting.CustomAlertDialog;
import com.tencent.module.setting.e;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

final class bf implements DialogInterface.OnCancelListener, DialogInterface.OnClickListener, DialogInterface.OnDismissListener {
    private co a;
    private boolean b;
    private /* synthetic */ Launcher c;

    /* synthetic */ bf(Launcher launcher) {
        this(launcher, (byte) 0);
    }

    private bf(Launcher launcher, byte b2) {
        this.c = launcher;
    }

    /* access modifiers changed from: package-private */
    public final Dialog a(boolean z) {
        boolean unused = this.c.mWaitingForResult = true;
        this.b = z;
        this.a = new co(this.c, z);
        e eVar = new e(this.c);
        eVar.a(this.b ? this.c.getString(R.string.menu_add_shortcut) : this.c.getString(R.string.menu_add_to_desktop));
        eVar.a(this.a, this);
        eVar.b();
        CustomAlertDialog c2 = eVar.c();
        c2.setOnCancelListener(this);
        c2.setOnDismissListener(this);
        return c2;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        boolean unused = this.c.mWaitingForResult = false;
        this.c.mWorkspace.p();
        this.c.dismissDialog(this.b ? 3 : 1);
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.c.getResources();
        this.c.mWorkspace.p();
        this.c.dismissDialog(this.b ? 3 : 1);
        switch ((!this.b || i != 2) ? i : 3) {
            case 0:
                if (this.b) {
                    Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
                    intent.addCategory("android.intent.category.LAUNCHER");
                    Intent intent2 = new Intent();
                    intent2.setAction("com.android.launcher.action.CUSTOM_ACTIVITYPICKER");
                    intent2.putExtra("android.intent.extra.TITLE", this.c.getString(R.string.title_select_application));
                    intent2.putExtra("android.intent.extra.INTENT", intent);
                    this.c.startActivityForResult(intent2, this.b ? 13 : 6);
                    return;
                }
                this.c.showAddAppPicker();
                return;
            case 1:
                this.c.pickShortcut(this.b ? 11 : 7, R.string.title_select_shortcut);
                return;
            case 2:
                int allocateAppWidgetId = this.c.mAppWidgetHost.allocateAppWidgetId();
                Intent intent3 = new Intent("android.appwidget.action.APPWIDGET_PICK");
                intent3.putExtra("appWidgetId", allocateAppWidgetId);
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                new AppWidgetProviderInfo();
                new Bundle();
                if (b.a < 8) {
                    AppWidgetProviderInfo appWidgetProviderInfo = new AppWidgetProviderInfo();
                    appWidgetProviderInfo.provider = new ComponentName(this.c.getPackageName(), "XXX.YYY");
                    appWidgetProviderInfo.label = this.c.getString(R.string.group_search);
                    appWidgetProviderInfo.icon = R.drawable.ic_search_widget;
                    arrayList.add(appWidgetProviderInfo);
                    Bundle bundle = new Bundle();
                    bundle.putString("custom_widget", "search_widget");
                    arrayList2.add(bundle);
                }
                AppWidgetProviderInfo appWidgetProviderInfo2 = new AppWidgetProviderInfo();
                appWidgetProviderInfo2.provider = new ComponentName(this.c.getPackageName(), "XXX.YYY");
                appWidgetProviderInfo2.label = this.c.getString(R.string.group_switcher);
                appWidgetProviderInfo2.icon = R.drawable.ic_switcher_widget;
                arrayList.add(appWidgetProviderInfo2);
                Bundle bundle2 = new Bundle();
                bundle2.putString("custom_widget", "switcher_widget");
                arrayList2.add(bundle2);
                AppWidgetProviderInfo appWidgetProviderInfo3 = new AppWidgetProviderInfo();
                appWidgetProviderInfo3.provider = new ComponentName(this.c.getPackageName(), "XXX.YYY");
                appWidgetProviderInfo3.label = this.c.getString(R.string.group_task_manager);
                appWidgetProviderInfo3.icon = R.drawable.ic_task_widget;
                arrayList.add(appWidgetProviderInfo3);
                Bundle bundle3 = new Bundle();
                bundle3.putString("custom_widget", "task_widget");
                arrayList2.add(bundle3);
                if (b.a()) {
                    AppWidgetProviderInfo appWidgetProviderInfo4 = new AppWidgetProviderInfo();
                    appWidgetProviderInfo4.provider = new ComponentName(this.c.getPackageName(), "XXX.YYY");
                    appWidgetProviderInfo4.label = this.c.getString(R.string.soso_name);
                    appWidgetProviderInfo4.icon = R.drawable.soso_widget;
                    arrayList.add(appWidgetProviderInfo4);
                    Bundle bundle4 = new Bundle();
                    bundle4.putString("custom_widget", "soso_widget");
                    arrayList2.add(bundle4);
                }
                intent3.putParcelableArrayListExtra("customInfo", arrayList);
                intent3.putParcelableArrayListExtra("customExtras", arrayList2);
                this.c.startActivityForResult(intent3, 9);
                return;
            case 3:
                if (this.b) {
                    this.c.showRenameDialog(null, 2);
                    return;
                } else {
                    this.c.showRenameDialog(null, 1);
                    return;
                }
            case 4:
                this.c.startWallpaper();
                return;
            default:
                return;
        }
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.c.mWorkspace.p();
    }
}
