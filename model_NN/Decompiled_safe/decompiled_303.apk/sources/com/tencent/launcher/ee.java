package com.tencent.launcher;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

final class ee extends ArrayAdapter {
    private Context a;
    private final LayoutInflater b = ((LayoutInflater) this.a.getSystemService("layout_inflater"));
    private /* synthetic */ SearchAppActivityReserved c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ee(SearchAppActivityReserved searchAppActivityReserved, Context context, ArrayList arrayList) {
        super(context, 0, arrayList);
        this.c = searchAppActivityReserved;
        this.a = context;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        am amVar = (am) getItem(i);
        View inflate = (view == null || view.getTag() == null) ? this.b.inflate((int) R.layout.search_list_item, (ViewGroup) null) : view;
        if (!amVar.f) {
            amVar.f = true;
        }
        TextView textView = (TextView) inflate.findViewById(R.id.softname);
        textView.setCompoundDrawablesWithIntrinsicBounds(amVar.d, (Drawable) null, (Drawable) null, (Drawable) null);
        textView.setText(amVar.a);
        inflate.setTag(amVar);
        return inflate;
    }
}
