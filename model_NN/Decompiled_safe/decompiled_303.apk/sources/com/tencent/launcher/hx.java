package com.tencent.launcher;

import android.content.DialogInterface;
import android.content.Intent;
import android.widget.Toast;
import com.tencent.qqlauncher.R;
import com.tencent.util.p;

final class hx implements DialogInterface.OnClickListener {
    final /* synthetic */ Launcher a;

    hx(Launcher launcher) {
        this.a = launcher;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Intent a2 = p.a(this.a.getPackageName());
        a2.addFlags(268435456);
        this.a.startActivity(a2);
        Toast.makeText(this.a, (int) R.string.clear_data, 1).show();
        this.a.handler.postDelayed(new bv(this), 1000);
    }
}
