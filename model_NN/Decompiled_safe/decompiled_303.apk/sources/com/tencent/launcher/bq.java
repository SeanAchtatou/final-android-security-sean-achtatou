package com.tencent.launcher;

import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.tencent.module.theme.l;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;
import java.util.Iterator;

public final class bq extends ex {
    String a;
    public ArrayList b = new ArrayList();

    bq() {
        this.m = 2;
    }

    bq(bq bqVar) {
        this.m = 2;
        this.h = bqVar.h;
        this.i = bqVar.i;
        this.j = bqVar.j;
        this.m = bqVar.m;
    }

    public final Drawable a(Context context, boolean z) {
        if (this.b == null || this.b.size() == 0) {
            return Launcher.getDefaultFloderIconAdding(context, z, this);
        }
        ArrayList arrayList = new ArrayList();
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            am amVar = (am) it.next();
            if ((amVar.d instanceof BitmapDrawable) || (amVar.d instanceof hp) || (amVar.d instanceof gb)) {
                arrayList.add(amVar.d);
            }
        }
        return a.a(context, a.b(arrayList, context.getResources().getDrawable(R.drawable.folder_bg_adding_default), context), z ? Launcher.sIconBackgroundDrawable : null, l.a().c(context.getResources()), this);
    }

    public final Drawable a(Context context, boolean z, UserFolder userFolder) {
        if (this.b == null || this.b.size() == 0) {
            return Launcher.getDefaultFloderIcon(context, z, this);
        }
        ArrayList arrayList = new ArrayList();
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            am amVar = (am) it.next();
            if ((userFolder == null || userFolder.g == null || userFolder.g != amVar) && ((amVar.d instanceof BitmapDrawable) || (amVar.d instanceof hp) || (amVar.d instanceof gb) || (amVar.d instanceof gm))) {
                arrayList.add(amVar.d);
            }
        }
        return a.a(context, a.b(arrayList, l.a().a(context.getResources()), context), z ? Launcher.sIconBackgroundDrawable : null, l.a().b(context.getResources()), this);
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public final void a(ContentValues contentValues) {
        super.a(contentValues);
        contentValues.put("title", this.h.toString());
    }

    public final /* bridge */ /* synthetic */ void a(UserFolder userFolder) {
        super.a(userFolder);
    }

    public final boolean a(am amVar) {
        if (this.b.size() >= 12 && !this.b.contains(amVar)) {
            return false;
        }
        amVar.u = this.b.size();
        this.b.add(amVar);
        return true;
    }

    public final Drawable b(Context context, boolean z) {
        if (this.b == null || this.b.size() == 0) {
            return Launcher.getDefaultFloderIconOpened(context, z, this);
        }
        ArrayList arrayList = new ArrayList();
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            am amVar = (am) it.next();
            if ((amVar.d instanceof BitmapDrawable) || (amVar.d instanceof hp) || (amVar.d instanceof gb)) {
                arrayList.add(amVar.d);
            }
        }
        return a.a(context, a.b(arrayList, l.a().a(context.getResources()), context), z ? Launcher.sIconBackgroundDrawable : null, l.a().c(context.getResources()), this);
    }

    public final boolean b(am amVar) {
        if (this.b.size() >= 12) {
            return false;
        }
        if (amVar.m == 1) {
            return false;
        }
        ComponentName component = amVar.c.getComponent();
        if (component == null) {
            return false;
        }
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            ComponentName component2 = ((am) it.next()).c.getComponent();
            if (component2 != null && component.equals(component2)) {
                return true;
            }
        }
        return false;
    }

    public final Drawable c(Context context, boolean z) {
        if (this.b == null || this.b.size() == 0) {
            return Launcher.getDefaultFloderIcon(context, z, this);
        }
        ArrayList arrayList = new ArrayList();
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            am amVar = (am) it.next();
            if ((amVar.d instanceof BitmapDrawable) || (amVar.d instanceof hp) || (amVar.d instanceof gb) || (amVar.d instanceof gm)) {
                arrayList.add(amVar.d);
            }
        }
        return a.a(context, a.b(arrayList, l.a().a(context.getResources()), context), z ? Launcher.sIconBackgroundDrawable : null, l.a().b(context.getResources()), this);
    }
}
