package com.tencent.launcher;

import android.appwidget.AppWidgetHost;
import android.appwidget.AppWidgetHostView;
import android.appwidget.AppWidgetProviderInfo;
import android.content.Context;

public final class cv extends AppWidgetHost {
    public cv(Context context) {
        super(context, Launcher.APPWIDGET_HOST_ID);
    }

    /* access modifiers changed from: protected */
    public final AppWidgetHostView onCreateView(Context context, int i, AppWidgetProviderInfo appWidgetProviderInfo) {
        return new gt(context);
    }
}
