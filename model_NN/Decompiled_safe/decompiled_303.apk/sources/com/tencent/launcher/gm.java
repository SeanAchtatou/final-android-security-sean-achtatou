package com.tencent.launcher;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public final class gm extends Drawable {
    private static final Paint d;
    private Bitmap a;
    private Rect b;
    private int c;

    static {
        Paint paint = new Paint();
        d = paint;
        paint.setFilterBitmap(true);
    }

    public final Bitmap a() {
        return this.a;
    }

    public final void draw(Canvas canvas) {
        Bitmap bitmap = this.a;
        Rect rect = this.b;
        canvas.save();
        canvas.clipRect(getBounds());
        if (bitmap != null) {
            if (rect == null) {
                canvas.drawBitmap(bitmap, 0.0f, 0.0f, d);
            } else {
                canvas.drawBitmap(bitmap, (Rect) null, rect, d);
            }
        }
        canvas.restore();
    }

    public final int getIntrinsicHeight() {
        return this.c;
    }

    public final int getIntrinsicWidth() {
        return this.c;
    }

    public final int getMinimumHeight() {
        return this.c;
    }

    public final int getMinimumWidth() {
        return this.c;
    }

    public final int getOpacity() {
        return -3;
    }

    public final void setAlpha(int i) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
