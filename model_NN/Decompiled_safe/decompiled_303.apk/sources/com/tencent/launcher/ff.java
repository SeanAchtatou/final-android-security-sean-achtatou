package com.tencent.launcher;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import com.tencent.launcher.base.BaseApp;
import com.tencent.launcher.home.c;
import com.tencent.launcher.home.i;
import com.tencent.module.qqwidget.a;
import com.tencent.qqlauncher.R;
import java.io.File;
import java.net.URISyntaxException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public final class ff {
    public static boolean a;
    /* access modifiers changed from: private */
    public static final Collator b = Collator.getInstance();
    /* access modifiers changed from: private */
    public static final AtomicInteger y = new AtomicInteger(1);
    /* access modifiers changed from: private */
    public static final AtomicInteger z = new AtomicInteger(1);
    /* access modifiers changed from: private */
    public boolean c;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public ArrayList e;
    /* access modifiers changed from: private */
    public ArrayList f;
    /* access modifiers changed from: private */
    public ArrayList g;
    /* access modifiers changed from: private */
    public HashMap h;
    private ArrayList i;
    /* access modifiers changed from: private */
    public HashMap j;
    /* access modifiers changed from: private */
    public bl k;
    private dw l;
    private w m;
    private Thread n;
    private Thread o;
    /* access modifiers changed from: private */
    public final HashMap p = new HashMap(50);
    private int q = 0;
    private int r = 0;
    private int s = 0;
    private int t = 0;
    private int u = 0;
    private int v = 0;
    private int w = 0;
    private int x = 0;

    static /* synthetic */ char a(String str, boolean z2) {
        boolean z3 = z2;
        String str2 = str;
        while (true) {
            if (str2 == null) {
                break;
            } else if (z3) {
                String trim = str2.trim();
                if (str2.trim().length() > 0) {
                    return trim.charAt(0);
                }
                str2 = trim;
                z3 = false;
            } else if (str2.length() > 0) {
                return str2.charAt(0);
            }
        }
        return 0;
    }

    public static int a(Context context) {
        return context.getContentResolver().query(fg.a, null, "itemType = 0", null, "position,orderId").getCount();
    }

    /* JADX INFO: finally extract failed */
    static am a(Context context, long j2) {
        ContentResolver contentResolver = context.getContentResolver();
        PackageManager packageManager = context.getPackageManager();
        Cursor query = contentResolver.query(ba.a, null, null, null, null);
        if (query == null) {
            return null;
        }
        try {
            query.moveToFirst();
            while (!query.isAfterLast()) {
                long j3 = query.getLong(query.getColumnIndexOrThrow("_id"));
                if (j3 == j2) {
                    int columnIndexOrThrow = query.getColumnIndexOrThrow("intent");
                    int columnIndexOrThrow2 = query.getColumnIndexOrThrow("title");
                    int columnIndexOrThrow3 = query.getColumnIndexOrThrow("iconType");
                    int columnIndexOrThrow4 = query.getColumnIndexOrThrow("icon");
                    int columnIndexOrThrow5 = query.getColumnIndexOrThrow("iconPackage");
                    int columnIndexOrThrow6 = query.getColumnIndexOrThrow("iconResource");
                    int columnIndexOrThrow7 = query.getColumnIndexOrThrow("container");
                    int columnIndexOrThrow8 = query.getColumnIndexOrThrow("itemType");
                    int columnIndexOrThrow9 = query.getColumnIndexOrThrow("screen");
                    int columnIndexOrThrow10 = query.getColumnIndexOrThrow("cellX");
                    int columnIndexOrThrow11 = query.getColumnIndexOrThrow("cellY");
                    int i2 = query.getInt(columnIndexOrThrow8);
                    if (i2 == 0 || i2 == 1) {
                        try {
                            Intent parseUri = Intent.parseUri(query.getString(columnIndexOrThrow), 0);
                            am a2 = i2 == 0 ? a(packageManager, parseUri) : b(query, context, columnIndexOrThrow3, columnIndexOrThrow5, columnIndexOrThrow6, columnIndexOrThrow4, parseUri);
                            if (a2 == null) {
                                a2 = new am();
                                a2.d = packageManager.getDefaultActivityIcon();
                            }
                            if (a2 != null) {
                                a2.a = query.getString(columnIndexOrThrow2);
                                a2.c = parseUri;
                                a2.l = j3;
                                a2.n = (long) query.getInt(columnIndexOrThrow7);
                                a2.o = query.getInt(columnIndexOrThrow9);
                                a2.p = query.getInt(columnIndexOrThrow10);
                                a2.q = query.getInt(columnIndexOrThrow11);
                            }
                            query.close();
                            return a2;
                        } catch (URISyntaxException e2) {
                            query.close();
                            return null;
                        }
                    }
                }
                query.moveToNext();
            }
            query.close();
            return null;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    public static am a(PackageManager packageManager, Intent intent) {
        ResolveInfo resolveActivity = packageManager.resolveActivity(intent, 0);
        if (resolveActivity == null) {
            return null;
        }
        am amVar = new am();
        ActivityInfo activityInfo = resolveActivity.activityInfo;
        amVar.d = activityInfo.loadIcon(packageManager);
        if (amVar.a == null || amVar.a.length() == 0) {
            amVar.a = activityInfo.loadLabel(packageManager);
        }
        if (amVar.a == null) {
            amVar.a = "";
        }
        amVar.m = 0;
        return amVar;
    }

    private static am a(PackageManager packageManager, HashMap hashMap, ResolveInfo resolveInfo) {
        ComponentName componentName = new ComponentName(resolveInfo.activityInfo.applicationInfo.packageName, resolveInfo.activityInfo.name);
        am amVar = (am) hashMap.get(componentName);
        if (amVar != null) {
            return amVar;
        }
        am amVar2 = new am();
        amVar2.n = -300;
        amVar2.t = hashMap.size();
        amVar2.j = System.currentTimeMillis();
        b(packageManager, resolveInfo, amVar2);
        amVar2.a(componentName);
        hashMap.put(componentName, amVar2);
        return amVar2;
    }

    private static am a(am amVar, String str, String str2) {
        ComponentName component = amVar.c.getComponent();
        if (!str.equals(component.getPackageName()) || !str2.equals(component.getClassName())) {
            return null;
        }
        return amVar;
    }

    private static am a(bl blVar, String str, String str2) {
        if (str != null && str.startsWith("com.tencent.qqlauncher.theme")) {
            return null;
        }
        int count = blVar.getCount();
        for (int i2 = 0; i2 < count; i2++) {
            if (blVar.getItem(i2) instanceof am) {
                am a2 = a((am) blVar.getItem(i2), str, str2);
                if (a2 != null) {
                    return a2;
                }
            } else if (blVar.getItem(i2) instanceof bq) {
                Iterator it = ((bq) blVar.getItem(i2)).b.iterator();
                while (it.hasNext()) {
                    am a3 = a((am) it.next(), str, str2);
                    if (a3 != null) {
                        return a3;
                    }
                }
                continue;
            } else {
                continue;
            }
        }
        return null;
    }

    private static List a(PackageManager packageManager, String str) {
        Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
        intent.addCategory("android.intent.category.LAUNCHER");
        List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 0);
        ArrayList arrayList = new ArrayList();
        if (queryIntentActivities != null) {
            int size = queryIntentActivities.size();
            for (int i2 = 0; i2 < size; i2++) {
                ResolveInfo resolveInfo = queryIntentActivities.get(i2);
                if (str.equals(resolveInfo.activityInfo.packageName)) {
                    arrayList.add(resolveInfo);
                }
            }
        }
        return arrayList;
    }

    static void a(int i2) {
        i.a().a("ScreenCount", i2);
    }

    static /* synthetic */ void a(ContentResolver contentResolver, PackageManager packageManager) {
        String string;
        ComponentName component;
        Cursor query = contentResolver.query(ba.a, new String[]{"_id", "title", "intent", "itemType"}, null, null, null);
        int columnIndexOrThrow = query.getColumnIndexOrThrow("_id");
        int columnIndexOrThrow2 = query.getColumnIndexOrThrow("intent");
        int columnIndexOrThrow3 = query.getColumnIndexOrThrow("itemType");
        int columnIndexOrThrow4 = query.getColumnIndexOrThrow("title");
        while (query.moveToNext()) {
            try {
                try {
                    if (query.getInt(columnIndexOrThrow3) == 0 && (string = query.getString(columnIndexOrThrow2)) != null) {
                        Intent intent = Intent.getIntent(string);
                        if ("android.intent.action.MAIN".equals(intent.getAction()) && (component = intent.getComponent()) != null) {
                            ActivityInfo activityInfo = packageManager.getActivityInfo(component, 0);
                            String string2 = query.getString(columnIndexOrThrow4);
                            String obj = activityInfo.loadLabel(packageManager).toString();
                            String str = (obj == null && (obj = packageManager.getApplicationLabel(activityInfo.applicationInfo).toString()) == null) ? activityInfo.name : obj;
                            if (string2 == null || !string2.equals(str)) {
                                ContentValues contentValues = new ContentValues();
                                contentValues.put("title", str);
                                contentResolver.update(ba.b, contentValues, "_id=?", new String[]{String.valueOf(query.getLong(columnIndexOrThrow))});
                            }
                        }
                    }
                } catch (PackageManager.NameNotFoundException | URISyntaxException e2) {
                }
            } finally {
                query.close();
            }
        }
    }

    public static void a(Context context, int i2) {
        SQLiteDatabase openOrCreateDatabase = context.openOrCreateDatabase("launcher.db", 0, null);
        try {
            openOrCreateDatabase.execSQL(String.format("update %s set position=position+1 where position>=%d", "applications", Integer.valueOf(i2)));
        } catch (SQLException e2) {
            e2.printStackTrace();
        }
        openOrCreateDatabase.close();
    }

    static void a(Context context, long j2, int i2) {
        SQLiteDatabase openOrCreateDatabase = context.openOrCreateDatabase("launcher.db", 2, null);
        openOrCreateDatabase.execSQL(String.format("update %s set %s=%s-%d where %s>%d", "applications", "position", "position", Integer.valueOf(i2), "_id", Long.valueOf(j2)));
        openOrCreateDatabase.close();
    }

    static void a(Context context, bq bqVar) {
        ContentResolver contentResolver = context.getContentResolver();
        contentResolver.delete(ba.a(bqVar.l), null, null);
        contentResolver.delete(ba.b, "container=" + bqVar.l, null);
        a = true;
    }

    static void a(Context context, ha haVar) {
        ContentValues contentValues = new ContentValues();
        ContentResolver contentResolver = context.getContentResolver();
        contentValues.put("container", Long.valueOf(haVar.n));
        contentValues.put("orderId", Integer.valueOf(haVar.u));
        contentValues.put("position", Integer.valueOf(haVar.t));
        contentResolver.update(fg.a(haVar.l), contentValues, null, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void
     arg types: [android.content.Context, com.tencent.launcher.ha, long, int, int, int, int]
     candidates:
      com.tencent.launcher.ff.a(android.database.Cursor, android.content.Context, int, int, int, int, android.content.Intent):com.tencent.launcher.am
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void */
    static void a(Context context, ha haVar, long j2, int i2, int i3, int i4) {
        if (haVar.n == -1 || haVar.n == -300) {
            a(context, haVar, j2, i2, i3, i4, false);
        } else {
            b(context, haVar, j2, i2, i3, i4);
        }
    }

    public static void a(Context context, ha haVar, long j2, int i2, int i3, int i4, boolean z2) {
        haVar.n = j2;
        haVar.o = i2;
        haVar.p = i3;
        haVar.q = i4;
        ContentValues contentValues = new ContentValues();
        ContentResolver contentResolver = context.getContentResolver();
        haVar.a(contentValues);
        Uri insert = contentResolver.insert(z2 ? ba.a : ba.b, contentValues);
        if (insert != null) {
            haVar.l = (long) Integer.parseInt(insert.getPathSegments().get(1));
        }
        a = true;
    }

    private void a(PackageManager packageManager, ResolveInfo resolveInfo, am amVar) {
        b(packageManager, resolveInfo, amVar);
        this.p.put(new ComponentName(resolveInfo.activityInfo.applicationInfo.packageName, resolveInfo.activityInfo.name), amVar);
    }

    static /* synthetic */ void a(Launcher launcher, Cursor cursor, int i2, int i3, int i4, dq dqVar) {
        switch (cursor.getInt(i2)) {
            case 0:
                String string = cursor.getString(i3);
                String string2 = cursor.getString(i4);
                try {
                    Resources resourcesForApplication = launcher.getPackageManager().getResourcesForApplication(string);
                    dqVar.d = resourcesForApplication.getDrawable(resourcesForApplication.getIdentifier(string2, null, null));
                } catch (Exception e2) {
                    dqVar.d = launcher.getResources().getDrawable(R.drawable.ic_launcher_folder);
                }
                dqVar.f = new Intent.ShortcutIconResource();
                dqVar.f.packageName = string;
                dqVar.f.resourceName = string2;
                return;
            default:
                dqVar.d = launcher.getResources().getDrawable(R.drawable.ic_launcher_folder);
                return;
        }
    }

    static void a(bq bqVar, ha haVar) {
        bqVar.b.remove(haVar);
    }

    static /* synthetic */ void a(dw dwVar, Launcher launcher, PackageManager packageManager, HashMap hashMap, HashMap hashMap2, fj fjVar, bl blVar, int i2) {
        int i3;
        fj fjVar2;
        if (!dwVar.b) {
            ContentResolver contentResolver = launcher.getContentResolver();
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            intent.addCategory("android.intent.category.LAUNCHER");
            List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 0);
            int size = queryIntentActivities.size();
            fj fjVar3 = fjVar;
            int i4 = i2;
            int i5 = 0;
            int i6 = i4;
            while (i5 < size && !dwVar.b) {
                ResolveInfo resolveInfo = queryIntentActivities.get(i5);
                ComponentName componentName = new ComponentName(resolveInfo.activityInfo.applicationInfo.packageName, resolveInfo.activityInfo.name);
                if (!hashMap.containsKey(componentName) && !a(hashMap2, componentName) && !componentName.getPackageName().startsWith("com.tencent.qqlauncher.theme")) {
                    am amVar = new am();
                    amVar.d = resolveInfo.loadIcon(packageManager);
                    amVar.n = -300;
                    amVar.m = 0;
                    amVar.a = resolveInfo.loadLabel(packageManager);
                    if (amVar.a == null) {
                        amVar.a = resolveInfo.activityInfo.name;
                    }
                    amVar.b = c.d(amVar.a.toString());
                    if (amVar.b == null || "".equals(amVar.b)) {
                        amVar.b = amVar.a.toString();
                    }
                    Intent intent2 = new Intent("android.intent.action.MAIN", (Uri) null);
                    intent2.addCategory("android.intent.category.LAUNCHER");
                    intent2.setComponent(componentName);
                    intent2.setFlags(270532608);
                    amVar.c = intent2;
                    i6++;
                    amVar.t = i6;
                    amVar.j = new File(resolveInfo.activityInfo.applicationInfo.sourceDir).lastModified();
                    hashMap.put(componentName, amVar);
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("container", (Integer) -300);
                    contentValues.put("position", Integer.valueOf(amVar.t));
                    contentValues.put("itemType", Integer.valueOf(amVar.m));
                    contentValues.put("intent", amVar.c.toUri(0));
                    contentValues.put("install", Long.valueOf(amVar.j));
                    if (!dwVar.b) {
                        contentResolver.insert(fg.a, contentValues);
                        if (fjVar3.a(amVar)) {
                            launcher.runOnUiThread(fjVar3);
                            int i7 = i6;
                            fjVar2 = new fj(blVar, false);
                            i3 = i7;
                            i5++;
                            fjVar3 = fjVar2;
                            i6 = i3;
                        }
                    } else {
                        return;
                    }
                }
                i3 = i6;
                fjVar2 = fjVar3;
                i5++;
                fjVar3 = fjVar2;
                i6 = i3;
            }
        }
    }

    static /* synthetic */ void a(ff ffVar, Launcher launcher) {
        bl blVar = ffVar.k;
        int count = blVar.getCount();
        new fq().a = blVar;
        boolean z2 = true;
        for (int i2 = 0; i2 < count; i2++) {
            ha haVar = (ha) blVar.getItem(i2);
            if (!(haVar == null || haVar.t == i2)) {
                if (z2) {
                    z2 = false;
                }
                haVar.t = i2;
                c(launcher, haVar);
            }
        }
        if (!z2) {
            Log.e("HomeLoaders", "recover position in application list!");
        }
    }

    private static void a(boolean[][] zArr, int i2, ha haVar, int i3, int i4) {
        if (haVar.o == i2) {
            int i5 = haVar.p;
            while (i5 < haVar.p + haVar.r && i5 >= 0 && i5 < i3) {
                int i6 = haVar.q;
                while (i6 < haVar.q + haVar.s && i6 >= 0 && i6 < i4) {
                    zArr[i5][i6] = true;
                    i6++;
                }
                i5++;
            }
        }
    }

    public static boolean a(char c2) {
        return Character.isUpperCase(c2) || Character.isLowerCase(c2);
    }

    public static boolean a(Context context, String str) {
        ContentResolver contentResolver = context.getContentResolver();
        ContentValues contentValues = new ContentValues();
        contentValues.put("packageName", str);
        contentResolver.insert(Uri.parse("content://com.tencent.qqlauncher.settings/newAppsNotify"), contentValues);
        return true;
    }

    static boolean a(Context context, String str, Intent intent) {
        Cursor query = context.getContentResolver().query(ba.a, new String[]{"title", "intent"}, "title=? and intent=?", new String[]{str, intent.toURI()}, null);
        try {
            return query.moveToFirst();
        } finally {
            query.close();
        }
    }

    private static boolean a(HashMap hashMap, ComponentName componentName) {
        for (bq bqVar : hashMap.values()) {
            Iterator it = bqVar.b.iterator();
            while (true) {
                if (it.hasNext()) {
                    am amVar = (am) it.next();
                    if (componentName != null && componentName.equals(amVar.c.getComponent())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private static boolean a(List list, ComponentName componentName) {
        String className = componentName.getClassName();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            if (((ResolveInfo) it.next()).activityInfo.name.equals(className)) {
                return true;
            }
        }
        return false;
    }

    private boolean a(List list, bl blVar, Launcher launcher) {
        ArrayList<am> arrayList = new ArrayList<>();
        int size = list.size();
        boolean z2 = false;
        for (int i2 = 0; i2 < size; i2++) {
            ResolveInfo resolveInfo = (ResolveInfo) list.get(i2);
            am a2 = a(blVar, resolveInfo.activityInfo.applicationInfo.packageName, resolveInfo.activityInfo.name);
            if (a2 == null) {
                am a3 = a(launcher.getPackageManager(), this.p, resolveInfo);
                arrayList.add(a3);
                b(launcher, a3);
            } else {
                a(launcher.getPackageManager(), resolveInfo, a2);
                d(launcher, a2);
            }
            z2 = true;
        }
        for (am add : arrayList) {
            blVar.setNotifyOnChange(false);
            blVar.add(add);
        }
        return z2;
    }

    public static int b(Context context) {
        return context.getContentResolver().query(ba.a, null, "(itemType = 4 or itemType=1001 or itemType=1002 or itemType=2001 or itemType=2002) and container = -100", null, "container,orderId").getCount();
    }

    /* access modifiers changed from: private */
    public static am b(Cursor cursor, Context context, int i2, int i3, int i4, int i5, Intent intent) {
        am amVar = new am();
        amVar.m = 1;
        switch (cursor.getInt(i2)) {
            case 0:
                String string = cursor.getString(i3);
                String string2 = cursor.getString(i4);
                PackageManager packageManager = context.getPackageManager();
                try {
                    Resources resourcesForApplication = packageManager.getResourcesForApplication(string);
                    amVar.d = resourcesForApplication.getDrawable(resourcesForApplication.getIdentifier(string2, null, null));
                    amVar.d = a.a(amVar.d, context);
                } catch (Exception e2) {
                    ResolveInfo resolveActivity = packageManager.resolveActivity(intent, 0);
                    if (resolveActivity == null) {
                        throw new Exception();
                    }
                    amVar.d = resolveActivity.activityInfo.loadIcon(packageManager);
                } catch (Exception e3) {
                    amVar.d = packageManager.getDefaultActivityIcon();
                }
                amVar.h = new Intent.ShortcutIconResource();
                amVar.h.packageName = string;
                amVar.h.resourceName = string2;
                amVar.g = false;
                break;
            case 1:
                byte[] blob = cursor.getBlob(i5);
                try {
                    amVar.d = new hp(BitmapFactory.decodeByteArray(blob, 0, blob.length));
                    amVar.d = a.a(context, amVar.d, Launcher.sIconBackgroundDrawable, amVar);
                    amVar.e = true;
                } catch (Exception e4) {
                    amVar.d = context.getPackageManager().getDefaultActivityIcon();
                    amVar.d = a.a(context, amVar.d, Launcher.sIconBackgroundDrawable, amVar);
                    amVar.e = true;
                }
                amVar.f = true;
                amVar.g = true;
                break;
            default:
                amVar.d = context.getPackageManager().getDefaultActivityIcon();
                amVar.g = false;
                break;
        }
        return amVar;
    }

    static void b(int i2) {
        i.a().a("DefaultScreenIndex", i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    static void b(Context context, int i2) {
        ContentResolver contentResolver = context.getContentResolver();
        contentResolver.delete(ba.a(), "screen != ? and screen != ? and screen != ?", new String[]{"2", "-1", "1"});
        ContentValues contentValues = new ContentValues();
        contentValues.put("screen", (Integer) 99);
        contentResolver.update(ba.a(), contentValues, "screen = ?", new String[]{"2"});
        contentValues.put("screen", (Integer) 98);
        contentResolver.update(ba.a(), contentValues, "screen = ?", new String[]{"1"});
        contentValues.put("screen", Integer.valueOf(i2));
        contentResolver.update(ba.a(), contentValues, "screen = ?", new String[]{"99"});
        contentValues.put("screen", Integer.valueOf(i2 - 1));
        contentResolver.update(ba.a(), contentValues, "screen = ?", new String[]{"98"});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static void b(Context context, ha haVar) {
        ContentValues contentValues = new ContentValues();
        ContentResolver contentResolver = context.getContentResolver();
        contentValues.put("container", Long.valueOf(haVar.n));
        contentValues.put("orderId", Integer.valueOf(haVar.u));
        contentValues.put("position", Integer.valueOf(haVar.t));
        if (haVar instanceof bq) {
            contentValues.put("title", ((bq) haVar).h.toString());
            contentValues.put("itemType", (Integer) 2);
        } else {
            contentValues.put("install", Long.valueOf(((am) haVar).j));
            contentValues.put("intent", ((am) haVar).c.toUri(0));
            contentValues.put("itemType", (Integer) 0);
        }
        Uri insert = contentResolver.insert(fg.b, contentValues);
        if (insert != null) {
            haVar.l = (long) Integer.parseInt(insert.getPathSegments().get(1));
        }
        a = true;
    }

    static void b(Context context, ha haVar, long j2, int i2, int i3, int i4) {
        haVar.n = j2;
        haVar.o = i2;
        haVar.p = i3;
        haVar.q = i4;
        ContentValues contentValues = new ContentValues();
        ContentResolver contentResolver = context.getContentResolver();
        contentValues.put("container", Long.valueOf(haVar.n));
        contentValues.put("cellX", Integer.valueOf(haVar.p));
        contentValues.put("cellY", Integer.valueOf(haVar.q));
        contentValues.put("screen", Integer.valueOf(haVar.o));
        contentValues.put("orderId", Integer.valueOf(haVar.u));
        contentResolver.update(ba.a(haVar.l), contentValues, null, null);
        a = true;
    }

    private static void b(PackageManager packageManager, ResolveInfo resolveInfo, am amVar) {
        amVar.a = resolveInfo.loadLabel(packageManager);
        if (amVar.a == null) {
            amVar.a = resolveInfo.activityInfo.name;
        }
        amVar.b = c.d(amVar.a.toString());
        if (amVar.b == null || "".equals(amVar.b)) {
            amVar.b = amVar.a.toString();
        }
        amVar.d = resolveInfo.activityInfo.loadIcon(packageManager);
        amVar.f = false;
    }

    /* access modifiers changed from: private */
    public synchronized void b(Launcher launcher, boolean z2) {
        c(launcher, z2);
    }

    static /* synthetic */ boolean b(char c2) {
        return Character.isDigit(c2) || Character.isUpperCase(c2) || Character.isLowerCase(c2);
    }

    public static boolean b(Context context, String str) {
        context.getContentResolver().delete(Uri.parse("content://com.tencent.qqlauncher.settings/newAppsNotify?packageName=" + str), null, null);
        return true;
    }

    public static int c() {
        return i.a().b("ScreenCount", 5);
    }

    public static int c(Context context) {
        return context.getContentResolver().query(ba.a, null, "itemType = 1 and container = -100", null, "container,orderId").getCount();
    }

    /* access modifiers changed from: private */
    public static bq c(HashMap hashMap, long j2) {
        bq bqVar;
        ex exVar = (ex) hashMap.get(Long.valueOf(j2));
        if (exVar == null || !(exVar instanceof bq)) {
            bq bqVar2 = new bq();
            hashMap.put(Long.valueOf(j2), bqVar2);
            bqVar = bqVar2;
        } else {
            bqVar = exVar;
        }
        return bqVar;
    }

    static void c(Context context, ha haVar) {
        ContentValues contentValues = new ContentValues();
        ContentResolver contentResolver = context.getContentResolver();
        contentValues.put("position", Integer.valueOf(haVar.t));
        contentResolver.update(fg.a(haVar.l), contentValues, null, null);
        a = true;
    }

    private void c(Launcher launcher, boolean z2) {
        n();
        this.l = new dw(this, launcher, z2);
        this.n = new Thread(this.l, "Applications Loader");
        this.n.start();
    }

    public static int d(Context context) {
        return context.getContentResolver().query(fg.a, null, "itemType = 2 or itemType = 3", null, "position,orderId").getCount();
    }

    /* access modifiers changed from: private */
    public static dq d(HashMap hashMap, long j2) {
        dq dqVar;
        ex exVar = (ex) hashMap.get(Long.valueOf(j2));
        if (exVar == null || !(exVar instanceof dq)) {
            dq dqVar2 = new dq();
            hashMap.put(Long.valueOf(j2), dqVar2);
            dqVar = dqVar2;
        } else {
            dqVar = exVar;
        }
        return dqVar;
    }

    static void d(Context context, ha haVar) {
        ContentValues contentValues = new ContentValues();
        ContentResolver contentResolver = context.getContentResolver();
        contentValues.put("orderId", Integer.valueOf(haVar.u));
        if (haVar instanceof bq) {
            contentValues.put("title", ((bq) haVar).h.toString());
        } else if (haVar instanceof am) {
            contentValues.put("intent", ((am) haVar).c.toUri(0));
            contentValues.put("install", Long.valueOf(((am) haVar).j));
            contentValues.put("callednum", Integer.valueOf(((am) haVar).k));
        }
        contentResolver.update(fg.a(haVar.l), contentValues, null, null);
        a = true;
    }

    public static int e(Context context) {
        return context.getContentResolver().query(ba.a, null, "(itemType = 2 or itemType = 3) and container = -100", null, "container,orderId").getCount();
    }

    static void e(Context context, ha haVar) {
        ContentValues contentValues = new ContentValues();
        ContentResolver contentResolver = context.getContentResolver();
        haVar.a(contentValues);
        contentResolver.update(ba.a(haVar.l), contentValues, null, null);
        a = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.c(com.tencent.launcher.Launcher, boolean):void
     arg types: [com.tencent.launcher.Launcher, int]
     candidates:
      com.tencent.launcher.ff.c(java.util.HashMap, long):com.tencent.launcher.bq
      com.tencent.launcher.ff.c(com.tencent.launcher.ff, java.util.ArrayList):java.util.ArrayList
      com.tencent.launcher.ff.c(android.content.Context, com.tencent.launcher.ha):void
      com.tencent.launcher.ff.c(com.tencent.launcher.Launcher, java.lang.String):void
      com.tencent.launcher.ff.c(com.tencent.launcher.Launcher, boolean):void */
    private synchronized void e(Launcher launcher, String str) {
        synchronized (this) {
            if (this.l != null && this.l.b()) {
                c(launcher, false);
            } else if (str != null) {
                if (str.length() > 0) {
                    bl blVar = this.k;
                    ArrayList<am> arrayList = new ArrayList<>();
                    int count = blVar == null ? 0 : blVar.getCount();
                    for (int i2 = 0; i2 < count; i2++) {
                        ha haVar = (ha) blVar.getItem(i2);
                        if ((haVar instanceof am) || !(haVar instanceof bq)) {
                            am amVar = (am) haVar;
                            if (str.equals(amVar.c.getComponent().getPackageName())) {
                                arrayList.add(amVar);
                            }
                        } else {
                            Iterator it = ((bq) haVar).b.iterator();
                            while (it.hasNext()) {
                                am amVar2 = (am) it.next();
                                if (str.equals(amVar2.c.getComponent().getPackageName())) {
                                    arrayList.add(amVar2);
                                }
                            }
                        }
                    }
                    HashMap hashMap = this.p;
                    for (am amVar3 : arrayList) {
                        if (amVar3.n != -300) {
                            bq bqVar = (bq) this.j.get(Long.valueOf(amVar3.n));
                            if (bqVar != null) {
                                bqVar.b.remove(amVar3);
                                if (bqVar.j != null) {
                                    bqVar.j.f();
                                }
                            }
                        }
                        blVar.setNotifyOnChange(false);
                        blVar.remove(amVar3);
                        hashMap.remove(amVar3.c.getComponent());
                        launcher.getContentResolver().delete(fg.a(amVar3.l), null, null);
                        a = true;
                    }
                    if (arrayList.size() > 0) {
                        j(launcher);
                        blVar.notifyDataSetChanged();
                    }
                }
            }
        }
    }

    static int f() {
        return i.a().b("ScreenCount", 5);
    }

    public static void f(Context context) {
        PackageManager packageManager = context.getPackageManager();
        ContentResolver contentResolver = context.getContentResolver();
        Cursor query = contentResolver.query(ba.a, null, "itemType = 0 or itemType = 1 or itemType = 4", null, null);
        try {
            int columnIndexOrThrow = query.getColumnIndexOrThrow("_id");
            int columnIndexOrThrow2 = query.getColumnIndexOrThrow("intent");
            int columnIndexOrThrow3 = query.getColumnIndexOrThrow("itemType");
            int columnIndexOrThrow4 = query.getColumnIndexOrThrow("appWidgetId");
            while (query.moveToNext()) {
                switch (query.getInt(columnIndexOrThrow3)) {
                    case 0:
                    case 1:
                        try {
                            if (packageManager.resolveActivity(Intent.getIntent(query.getString(columnIndexOrThrow2)), 0) == null) {
                                contentResolver.delete(ba.a(query.getLong(columnIndexOrThrow)), null, null);
                            } else {
                                continue;
                            }
                        } catch (URISyntaxException e2) {
                        }
                    case 2:
                    case 3:
                    default:
                        continue;
                    case 4:
                        if (AppWidgetManager.getInstance(context).getAppWidgetInfo(query.getInt(columnIndexOrThrow4)) == null) {
                            contentResolver.delete(ba.a(query.getLong(columnIndexOrThrow)), null, null);
                        } else {
                            continue;
                        }
                }
                e.printStackTrace();
            }
            query.close();
        } catch (Exception e3) {
            e3.printStackTrace();
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    static void f(Context context, ha haVar) {
        context.getContentResolver().delete(fg.a(haVar.l), null, null);
        a = true;
    }

    private boolean f(Launcher launcher, String str) {
        boolean z2;
        boolean z3;
        List a2 = a(launcher.getPackageManager(), str);
        if (a2.size() <= 0) {
            return false;
        }
        bl blVar = this.k;
        ArrayList<am> arrayList = new ArrayList<>();
        int count = blVar.getCount();
        int i2 = 0;
        boolean z4 = false;
        while (i2 < count) {
            if (blVar.getItem(i2) instanceof am) {
                am amVar = (am) blVar.getItem(i2);
                ComponentName component = amVar.c.getComponent();
                if (str.equals(component.getPackageName()) && !a(a2, component)) {
                    arrayList.add(amVar);
                    z2 = true;
                }
                z2 = z4;
            } else {
                if (blVar.getItem(i2) instanceof bq) {
                    Iterator it = ((bq) blVar.getItem(i2)).b.iterator();
                    while (it.hasNext()) {
                        am amVar2 = (am) it.next();
                        ComponentName component2 = amVar2.c.getComponent();
                        if (!str.equals(component2.getPackageName()) || a(a2, component2)) {
                            z3 = z4;
                        } else {
                            arrayList.add(amVar2);
                            z3 = true;
                        }
                        z4 = z3;
                    }
                }
                z2 = z4;
            }
            i2++;
            z4 = z2;
        }
        HashMap hashMap = this.p;
        for (am amVar3 : arrayList) {
            blVar.setNotifyOnChange(false);
            blVar.remove(amVar3);
            hashMap.remove(amVar3.c.getComponent());
            launcher.getContentResolver().delete(fg.a(amVar3.l), null, null);
            a = true;
        }
        return a(a2, blVar, launcher) || z4;
    }

    static int g() {
        return i.a().b("DefaultScreenIndex", 2);
    }

    static void g(Context context, ha haVar) {
        context.getContentResolver().delete(ba.a(haVar.l), null, null);
        a = true;
    }

    public static boolean g(Context context) {
        context.getContentResolver().delete(Uri.parse("content://com.tencent.qqlauncher.settings/newAppsNotify"), null, null);
        return true;
    }

    public static ArrayList h(Context context) {
        ArrayList arrayList = new ArrayList();
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.qqlauncher.settings/newAppsNotify"), new String[]{"packageName"}, null, new String[0], null);
        while (query.moveToNext()) {
            try {
                String string = query.getString(0);
                if (string != null) {
                    arrayList.add(string);
                }
            } finally {
                query.close();
            }
        }
        return arrayList;
    }

    static void h(Context context, ha haVar) {
        e(context, haVar);
        a = false;
    }

    public static void i(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        contentResolver.delete(ba.a, null, null);
        contentResolver.delete(ba.a, "initData", null);
    }

    static void i(Context context, ha haVar) {
        context.getContentResolver().delete(ba.a(haVar.l), null, null);
        a = true;
        a = false;
    }

    private void j(Context context) {
        bl blVar = this.k;
        int count = blVar.getCount();
        boolean z2 = false;
        for (int i2 = 0; i2 < count; i2++) {
            ha haVar = (ha) blVar.getItem(i2);
            if (haVar.t != i2) {
                z2 = true;
            }
            haVar.t = i2;
        }
        if (z2) {
            BaseApp.d().post(new ih(this, count, blVar, context));
        }
    }

    private synchronized void m() {
        this.p.clear();
    }

    private synchronized void n() {
        if (this.l != null && this.l.b()) {
            this.l.a();
            try {
                this.n.join(5000);
            } catch (InterruptedException e2) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final Drawable a(PackageManager packageManager, am amVar) {
        ResolveInfo resolveActivity = packageManager.resolveActivity(amVar.c, 0);
        if (resolveActivity == null) {
            return null;
        }
        am amVar2 = (am) this.p.get(new ComponentName(resolveActivity.activityInfo.applicationInfo.packageName, resolveActivity.activityInfo.name));
        return amVar2 == null ? resolveActivity.activityInfo.loadIcon(packageManager) : amVar2.d;
    }

    /* access modifiers changed from: package-private */
    public final ex a(long j2) {
        return (ex) this.h.get(Long.valueOf(j2));
    }

    public final a a(String str) {
        Iterator it = this.g.iterator();
        while (it.hasNext()) {
            a aVar = (a) it.next();
            if (aVar.e.endsWith(str)) {
                return aVar;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a() {
        if (this.l != null && this.l.b()) {
            this.l.a();
            this.c = false;
        }
        if (this.m != null && this.m.b()) {
            this.m.a();
            this.d = false;
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Launcher launcher, ComponentName componentName) {
        am amVar = (am) this.p.get(componentName);
        if (amVar != null) {
            amVar.k++;
            d(launcher, amVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(com.tencent.launcher.Launcher, java.lang.String, boolean):void
     arg types: [com.tencent.launcher.Launcher, java.lang.String, int]
     candidates:
      com.tencent.launcher.ff.a(android.content.pm.PackageManager, java.util.HashMap, android.content.pm.ResolveInfo):com.tencent.launcher.am
      com.tencent.launcher.ff.a(com.tencent.launcher.am, java.lang.String, java.lang.String):com.tencent.launcher.am
      com.tencent.launcher.ff.a(com.tencent.launcher.bl, java.lang.String, java.lang.String):com.tencent.launcher.am
      com.tencent.launcher.ff.a(android.content.Context, long, int):void
      com.tencent.launcher.ff.a(android.content.pm.PackageManager, android.content.pm.ResolveInfo, com.tencent.launcher.am):void
      com.tencent.launcher.ff.a(com.tencent.launcher.ff, com.tencent.launcher.Launcher, boolean):void
      com.tencent.launcher.ff.a(android.content.Context, java.lang.String, android.content.Intent):boolean
      com.tencent.launcher.ff.a(java.util.List, com.tencent.launcher.bl, com.tencent.launcher.Launcher):boolean
      com.tencent.launcher.ff.a(com.tencent.launcher.Launcher, java.lang.String, boolean):void */
    /* access modifiers changed from: package-private */
    public final synchronized void a(Launcher launcher, String str) {
        a(launcher, str, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.c(com.tencent.launcher.Launcher, boolean):void
     arg types: [com.tencent.launcher.Launcher, int]
     candidates:
      com.tencent.launcher.ff.c(java.util.HashMap, long):com.tencent.launcher.bq
      com.tencent.launcher.ff.c(com.tencent.launcher.ff, java.util.ArrayList):java.util.ArrayList
      com.tencent.launcher.ff.c(android.content.Context, com.tencent.launcher.ha):void
      com.tencent.launcher.ff.c(com.tencent.launcher.Launcher, java.lang.String):void
      com.tencent.launcher.ff.c(com.tencent.launcher.Launcher, boolean):void */
    /* access modifiers changed from: package-private */
    public final synchronized void a(Launcher launcher, String str, boolean z2) {
        boolean z3;
        if (!str.startsWith("com.tencent.qqlauncher.theme")) {
            if (this.l != null && this.l.b()) {
                c(launcher, false);
            } else if (str != null) {
                if (str.length() > 0) {
                    PackageManager packageManager = launcher.getPackageManager();
                    List<ResolveInfo> a2 = a(packageManager, str);
                    if (a2.size() > 0) {
                        bl blVar = this.k;
                        HashMap hashMap = this.p;
                        for (ResolveInfo a3 : a2) {
                            blVar.setNotifyOnChange(false);
                            am a4 = a(packageManager, hashMap, a3);
                            if (a4 instanceof am) {
                                a4.t = blVar.getCount();
                            }
                            if (z2) {
                                if (blVar.getPosition(a4) == -1) {
                                    int count = blVar.getCount();
                                    int i2 = 0;
                                    while (true) {
                                        if (i2 >= count) {
                                            z3 = false;
                                            break;
                                        }
                                        ha haVar = (ha) blVar.getItem(i2);
                                        if ((haVar instanceof bq) && ((bq) haVar).b.contains(a4)) {
                                            z3 = true;
                                            break;
                                        }
                                        i2++;
                                    }
                                    if (z3) {
                                    }
                                }
                            }
                            blVar.add(a4);
                            b(launcher, a4);
                        }
                        blVar.notifyDataSetChanged();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(bq bqVar) {
        this.j.put(Long.valueOf(bqVar.l), bqVar);
    }

    /* access modifiers changed from: package-private */
    public final void a(c cVar) {
        this.f.add(cVar);
    }

    /* access modifiers changed from: package-private */
    public final void a(ex exVar) {
        this.h.put(Long.valueOf(exVar.l), exVar);
    }

    /* access modifiers changed from: package-private */
    public final void a(ha haVar) {
        this.e.add(haVar);
    }

    /* access modifiers changed from: package-private */
    public final void a(a aVar) {
        this.g.add(aVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.b(com.tencent.launcher.Launcher, boolean):void
     arg types: [com.tencent.launcher.Launcher, int]
     candidates:
      com.tencent.launcher.ff.b(java.util.HashMap, long):com.tencent.launcher.dq
      com.tencent.launcher.ff.b(com.tencent.launcher.ff, java.util.ArrayList):java.util.ArrayList
      com.tencent.launcher.ff.b(android.content.Context, int):void
      com.tencent.launcher.ff.b(android.content.Context, com.tencent.launcher.ha):void
      com.tencent.launcher.ff.b(android.content.Context, java.lang.String):boolean
      com.tencent.launcher.ff.b(android.content.Context, long):com.tencent.launcher.ex
      com.tencent.launcher.ff.b(com.tencent.launcher.Launcher, java.lang.String):void
      com.tencent.launcher.ff.b(com.tencent.launcher.Launcher, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a(boolean z2, Launcher launcher, boolean z3, boolean z4) {
        boolean z5;
        if (z2) {
            if ((this.e == null || this.f == null || !this.d) ? false : true) {
                if (z4) {
                    b(launcher, true);
                }
                launcher.onDesktopItemsLoaded(this.e, this.f, this.g);
                return;
            }
        }
        if (this.m == null || !this.m.b()) {
            z5 = z4;
        } else {
            this.m.a();
            try {
                this.o.join(5000);
            } catch (InterruptedException e2) {
            }
            z5 = this.m.e;
        }
        this.d = false;
        this.m = new w(this, launcher, z3, z5, z2);
        this.o = new Thread(this.m, "Desktop Items Loader");
        this.o.start();
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean[][] zArr, int i2, int i3, int i4) {
        ArrayList arrayList = this.e;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i5 = 0; i5 < size; i5++) {
                a(zArr, i4, (ha) arrayList.get(i5), i2, i3);
            }
        }
        ArrayList arrayList2 = this.g;
        if (arrayList2 != null) {
            int size2 = arrayList2.size();
            for (int i6 = 0; i6 < size2; i6++) {
                a(zArr, i4, (ha) arrayList2.get(i6), i2, i3);
            }
        }
        ArrayList arrayList3 = this.f;
        if (arrayList3 != null) {
            int size3 = arrayList3.size();
            for (int i7 = 0; i7 < size3; i7++) {
                a(zArr, i4, (ha) arrayList3.get(i7), i2, i3);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean a(Launcher launcher, boolean z2) {
        boolean z3;
        if (!this.c || z2) {
            n();
            if (z2) {
                m();
            }
            this.i = new ArrayList(42);
            this.j = new HashMap();
            this.k = new bl(launcher, this.i);
            this.c = false;
            z3 = true;
        } else {
            this.k = new bl(launcher, this.i);
            z3 = false;
        }
        return z3;
    }

    /* access modifiers changed from: package-private */
    public final bq b(long j2) {
        return (bq) this.j.get(Long.valueOf(j2));
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    public final ex b(Context context, long j2) {
        ex d2;
        Cursor query = context.getContentResolver().query(ba.a, null, "_id=? and (itemType=? or itemType=?)", new String[]{String.valueOf(j2), String.valueOf(2), String.valueOf(3)}, null);
        try {
            if (query.moveToFirst()) {
                int columnIndexOrThrow = query.getColumnIndexOrThrow("itemType");
                int columnIndexOrThrow2 = query.getColumnIndexOrThrow("title");
                int columnIndexOrThrow3 = query.getColumnIndexOrThrow("container");
                int columnIndexOrThrow4 = query.getColumnIndexOrThrow("screen");
                int columnIndexOrThrow5 = query.getColumnIndexOrThrow("cellX");
                int columnIndexOrThrow6 = query.getColumnIndexOrThrow("cellY");
                switch (query.getInt(columnIndexOrThrow)) {
                    case 2:
                        d2 = c(this.h, j2);
                        break;
                    case 3:
                        d2 = d(this.h, j2);
                        break;
                    default:
                        d2 = null;
                        break;
                }
                d2.h = query.getString(columnIndexOrThrow2);
                d2.l = j2;
                d2.n = (long) query.getInt(columnIndexOrThrow3);
                d2.o = query.getInt(columnIndexOrThrow4);
                d2.p = query.getInt(columnIndexOrThrow5);
                d2.q = query.getInt(columnIndexOrThrow6);
                query.close();
                return d2;
            }
            query.close();
            return null;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void b(Launcher launcher, String str) {
        e(launcher, str);
    }

    /* access modifiers changed from: package-private */
    public final void b(bq bqVar) {
        this.h.remove(Long.valueOf(bqVar.l));
    }

    /* access modifiers changed from: package-private */
    public final void b(c cVar) {
        this.f.remove(cVar);
    }

    /* access modifiers changed from: package-private */
    public final void b(ha haVar) {
        this.e.remove(haVar);
    }

    /* access modifiers changed from: package-private */
    public final void b(a aVar) {
        this.g.remove(aVar);
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        return (this.e == null || this.f == null || !this.d) ? false : true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.c(com.tencent.launcher.Launcher, boolean):void
     arg types: [com.tencent.launcher.Launcher, int]
     candidates:
      com.tencent.launcher.ff.c(java.util.HashMap, long):com.tencent.launcher.bq
      com.tencent.launcher.ff.c(com.tencent.launcher.ff, java.util.ArrayList):java.util.ArrayList
      com.tencent.launcher.ff.c(android.content.Context, com.tencent.launcher.ha):void
      com.tencent.launcher.ff.c(com.tencent.launcher.Launcher, java.lang.String):void
      com.tencent.launcher.ff.c(com.tencent.launcher.Launcher, boolean):void */
    /* access modifiers changed from: package-private */
    public final synchronized void c(Launcher launcher, String str) {
        boolean z2;
        int i2 = 0;
        synchronized (this) {
            if (this.l != null && this.l.b()) {
                c(launcher, false);
            } else if (str != null) {
                if (str.length() > 0) {
                    PackageManager packageManager = launcher.getPackageManager();
                    bl blVar = this.k;
                    List a2 = a(packageManager, str);
                    int size = a2.size();
                    boolean z3 = false;
                    while (i2 < size) {
                        ResolveInfo resolveInfo = (ResolveInfo) a2.get(i2);
                        am a3 = a(blVar, resolveInfo.activityInfo.applicationInfo.packageName, resolveInfo.activityInfo.name);
                        if (a3 != null) {
                            a(packageManager, resolveInfo, a3);
                            a3.j = System.currentTimeMillis();
                            d(launcher, a3);
                            z2 = true;
                        } else {
                            z2 = z3;
                        }
                        i2++;
                        z3 = z2;
                    }
                    if (f(launcher, str) ? true : z3) {
                        j(launcher);
                        blVar.notifyDataSetChanged();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void c(bq bqVar) {
        this.j.remove(Long.valueOf(bqVar.l));
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        n();
        this.k = null;
        ArrayList arrayList = this.i;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (arrayList.get(i2) != null) {
                    ((ha) arrayList.get(i2)).a();
                }
            }
        }
        ArrayList arrayList2 = this.e;
        if (arrayList2 != null) {
            int size2 = arrayList2.size();
            for (int i3 = 0; i3 < size2; i3++) {
                ha haVar = (ha) arrayList2.get(i3);
                switch (haVar.m) {
                    case 0:
                    case 1:
                        ((am) haVar).d.setCallback(null);
                        break;
                }
            }
        }
        ArrayList arrayList3 = this.f;
        if (arrayList3 != null) {
            int size3 = arrayList3.size();
            for (int i4 = 0; i4 < size3; i4++) {
                ((c) arrayList3.get(i4)).b = null;
            }
        }
        for (am amVar : this.p.values()) {
            amVar.d.setCallback(null);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.c(com.tencent.launcher.Launcher, boolean):void
     arg types: [com.tencent.launcher.Launcher, int]
     candidates:
      com.tencent.launcher.ff.c(java.util.HashMap, long):com.tencent.launcher.bq
      com.tencent.launcher.ff.c(com.tencent.launcher.ff, java.util.ArrayList):java.util.ArrayList
      com.tencent.launcher.ff.c(android.content.Context, com.tencent.launcher.ha):void
      com.tencent.launcher.ff.c(com.tencent.launcher.Launcher, java.lang.String):void
      com.tencent.launcher.ff.c(com.tencent.launcher.Launcher, boolean):void */
    /* access modifiers changed from: package-private */
    public final synchronized void d(Launcher launcher, String str) {
        if (this.l != null && this.l.b()) {
            c(launcher, false);
        } else if (str != null) {
            if (str.length() > 0 && f(launcher, str)) {
                j(launcher);
                this.k.notifyDataSetChanged();
            }
        }
    }

    public final bl e() {
        return this.k;
    }

    public final ArrayList h() {
        return this.i;
    }

    public final ArrayList i() {
        return this.e;
    }
}
