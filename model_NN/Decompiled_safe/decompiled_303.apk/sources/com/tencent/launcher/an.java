package com.tencent.launcher;

import android.database.Cursor;
import android.os.AsyncTask;
import java.lang.ref.WeakReference;

final class an extends AsyncTask {
    private final WeakReference a;
    private dq b;

    an(LiveFolder liveFolder) {
        this.a = new WeakReference(liveFolder);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        dq[] dqVarArr = (dq[]) objArr;
        LiveFolder liveFolder = (LiveFolder) this.a.get();
        if (liveFolder == null) {
            return null;
        }
        this.b = dqVarArr[0];
        return liveFolder.d.getContentResolver().query(this.b.b, null, null, null, "name ASC");
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        LiveFolder liveFolder;
        Cursor cursor = (Cursor) obj;
        if (!isCancelled()) {
            if (cursor != null && (liveFolder = (LiveFolder) this.a.get()) != null) {
                liveFolder.a(new gq(liveFolder.d, this.b, cursor));
            }
        } else if (cursor != null) {
            cursor.close();
        }
    }
}
