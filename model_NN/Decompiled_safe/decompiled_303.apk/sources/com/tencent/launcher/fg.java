package com.tencent.launcher;

import android.net.Uri;

public final class fg implements ge {
    public static final Uri a = Uri.parse("content://com.tencent.qqlauncher.settings/applications?notify=true");
    public static final Uri b = Uri.parse("content://com.tencent.qqlauncher.settings/applications?notify=false");

    public static Uri a(long j) {
        return Uri.parse("content://com.tencent.qqlauncher.settings/applications/" + j + "?" + "notify" + "=" + false);
    }
}
