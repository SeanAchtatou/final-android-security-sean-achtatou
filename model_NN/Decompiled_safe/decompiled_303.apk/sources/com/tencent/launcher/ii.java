package com.tencent.launcher;

import android.view.animation.Animation;
import android.view.animation.Transformation;

final class ii extends Animation {
    private /* synthetic */ Search a;

    /* synthetic */ ii(Search search) {
        this(search, (byte) 0);
    }

    private ii(Search search, byte b) {
        this.a = search;
    }

    /* access modifiers changed from: protected */
    public final void applyTransformation(float f, Transformation transformation) {
        transformation.getMatrix().setTranslate(((float) (-this.a.getLeft())) * (1.0f - f), ((float) (-Search.c(this.a))) * (1.0f - f));
    }
}
