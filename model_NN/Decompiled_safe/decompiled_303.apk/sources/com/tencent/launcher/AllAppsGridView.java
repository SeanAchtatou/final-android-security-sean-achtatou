package com.tencent.launcher;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import com.tencent.widget.QGridView;

public class AllAppsGridView extends QGridView implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener, cm {
    private dt a;
    private Launcher b;
    private Runnable c;

    public AllAppsGridView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842865);
    }

    public AllAppsGridView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setScrollingCacheEnabled(false);
        setVerticalScrollBarEnabled(false);
        this.c = new af(this);
    }

    public final void a(View view, boolean z) {
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        setOnItemClickListener(this);
        setOnItemLongClickListener(this);
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.b.startActivitySafely(((am) adapterView.getItemAtPosition(i)).c);
    }

    public boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
        if (!view.isInTouchMode()) {
            return false;
        }
        this.a.a(view, this, new am((am) adapterView.getItemAtPosition(i)), 1);
        this.b.closeAllApplications();
        return true;
    }
}
