package com.tencent.launcher;

import android.content.DialogInterface;
import com.tencent.module.qqwidget.a;
import com.tencent.module.qqwidget.h;

final class db implements DialogInterface.OnClickListener {
    private /* synthetic */ Launcher a;

    db(Launcher launcher) {
        this.a = launcher;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        a a2 = h.a().a(i);
        if (a2 != null) {
            this.a.addQQWidget(a2);
        }
    }
}
