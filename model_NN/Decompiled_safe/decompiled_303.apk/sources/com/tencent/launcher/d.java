package com.tencent.launcher;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.ArrayAdapter;
import com.tencent.qqlauncher.R;

public final class d implements DialogInterface.OnCancelListener, DialogInterface.OnClickListener, DialogInterface.OnDismissListener {
    private ArrayAdapter a;
    private /* synthetic */ ShirtcutEditActivity b;

    protected d(ShirtcutEditActivity shirtcutEditActivity) {
        this.b = shirtcutEditActivity;
    }

    /* access modifiers changed from: package-private */
    public final Dialog a() {
        this.a = new ArrayAdapter(this.b, R.layout.add_list_item);
        this.a.add(this.b.getString(R.string.shirtcuts_select_picture));
        this.a.add(this.b.getString(R.string.shirtcuts_crop_picture));
        AlertDialog.Builder builder = new AlertDialog.Builder(this.b);
        builder.setTitle(this.b.getString(R.string.shirtcuts_select_icon_type));
        builder.setAdapter(this.a, this);
        builder.setInverseBackgroundForced(false);
        AlertDialog create = builder.create();
        create.setOnCancelListener(this);
        create.setOnDismissListener(this);
        return create;
    }

    public final void onCancel(DialogInterface dialogInterface) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case 0:
                Intent intent = new Intent("android.intent.action.PICK");
                intent.setType("image/*");
                this.b.startActivityForResult(Intent.createChooser(intent, "Select icon"), 1);
                return;
            case 1:
                Intent intent2 = new Intent("android.intent.action.GET_CONTENT");
                intent2.setType("image/*");
                int access$000 = this.b.mIconSize;
                intent2.putExtra("crop", "true");
                intent2.putExtra("outputX", access$000);
                intent2.putExtra("outputY", access$000);
                intent2.putExtra("aspectX", access$000);
                intent2.putExtra("aspectY", access$000);
                intent2.putExtra("noFaceDetection", true);
                intent2.putExtra("return-data", true);
                this.b.startActivityForResult(intent2, 5);
                return;
            default:
                return;
        }
    }

    public final void onDismiss(DialogInterface dialogInterface) {
    }
}
