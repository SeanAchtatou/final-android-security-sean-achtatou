package com.tencent.launcher;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.tencent.qqlauncher.R;
import java.io.InputStream;

public class ShirtcutEditActivity extends Activity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    private static final int DIALOG_ICON_TYPE = 1;
    public static final String EXTRA_APPLICATIONINFO = "EXTRA_APPLICATIONINFO";
    private static final int PICK_CUSTOM_ICON = 1;
    private static final int PICK_CUSTOM_PICTURE = 5;
    private static final int PICK_FROM_ICON_PACK = 6;
    private static final int PICK_STANDARD_APPLICATION = 4;
    private static final int PICK_STANDARD_MENU = 2;
    private static final int PICK_STANDARD_SHORTCUT = 3;
    public static final String USE_DEFAULT_SHIRTCUT = "USE_DEFAULT_SHIRTCUT";
    private Button btCanel;
    private Button btOk;
    private ImageButton btPickIcon;
    private EditText edLabel;
    private Bitmap mBitmap;
    private RadioButton mCustomRadioButton;
    private RadioButton mDefaultRadioButton;
    private Intent.ShortcutIconResource mIconResource;
    /* access modifiers changed from: private */
    public int mIconSize;
    private Intent mIntent;
    PackageManager mPackageManager;
    private RadioGroup mRadioGroup;

    private am getAppInfo() {
        Intent intent = getIntent();
        if (intent == null || intent.getAction() == null || !intent.getAction().equals("android.intent.action.EDIT") || !intent.hasExtra(EXTRA_APPLICATIONINFO)) {
            return null;
        }
        return ff.a(this, intent.getLongExtra(EXTRA_APPLICATIONINFO, 0));
    }

    private void loadFromAppInfo(am amVar) {
        ActivityInfo activityInfo;
        if (amVar != null) {
            this.edLabel.setText(amVar.a);
            this.mIntent = amVar.c;
            this.btPickIcon.setImageDrawable(amVar.d);
            this.btPickIcon.setEnabled(true);
            this.btOk.setEnabled(true);
            ComponentName component = this.mIntent.getComponent();
            if (component != null) {
                try {
                    activityInfo = this.mPackageManager.getActivityInfo(component, 0);
                } catch (PackageManager.NameNotFoundException e) {
                    activityInfo = null;
                }
                if (activityInfo != null) {
                    activityInfo.loadLabel(this.mPackageManager).toString();
                    this.mIconResource = new Intent.ShortcutIconResource();
                    this.mIconResource.packageName = activityInfo.packageName;
                    try {
                        this.mIconResource.resourceName = this.mPackageManager.getResourcesForApplication(this.mIconResource.packageName).getResourceName(activityInfo.getIconResource());
                    } catch (PackageManager.NameNotFoundException e2) {
                        this.mIconResource = null;
                    } catch (Resources.NotFoundException e3) {
                        this.mIconResource = null;
                    }
                }
            }
        }
    }

    private void setEditMode(boolean z) {
        this.btPickIcon.setEnabled(z);
        this.edLabel.setEnabled(z);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == -1) {
            switch (i) {
                case 1:
                    Uri data = intent.getData();
                    try {
                        InputStream openInputStream = getContentResolver().openInputStream(data);
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inJustDecodeBounds = true;
                        BitmapFactory.decodeStream(openInputStream, null, options);
                        BitmapFactory.Options options2 = new BitmapFactory.Options();
                        options2.inSampleSize = (int) (((float) options.outWidth) / ((float) this.mIconSize));
                        this.mBitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(data), null, options2);
                        if (this.mBitmap != null) {
                            if (this.mBitmap.getWidth() > this.mIconSize) {
                                this.mBitmap = a.a(this.mBitmap, this);
                            }
                            this.btPickIcon.setImageBitmap(this.mBitmap);
                            return;
                        }
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                case 5:
                    this.mBitmap = (Bitmap) intent.getParcelableExtra("data");
                    if (this.mBitmap != null) {
                        if (this.mBitmap.getWidth() > this.mIconSize) {
                            this.mBitmap = a.a(this.mBitmap, this);
                        }
                        this.btPickIcon.setImageBitmap(this.mBitmap);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        if (i == this.mCustomRadioButton.getId()) {
            this.btPickIcon.setEnabled(true);
            this.edLabel.setEnabled(true);
        } else if (i == this.mDefaultRadioButton.getId()) {
            this.btPickIcon.setEnabled(false);
            this.edLabel.setEnabled(false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        if (view.equals(this.btPickIcon)) {
            showDialog(1);
        } else if (view.equals(this.btOk)) {
            Intent intent = new Intent();
            intent.putExtra("android.intent.extra.shortcut.INTENT", this.mIntent);
            Intent intent2 = getIntent();
            if (intent2 != null && intent2.getAction() != null && intent2.getAction().equals("android.intent.action.EDIT") && intent2.hasExtra(EXTRA_APPLICATIONINFO)) {
                intent.putExtra(EXTRA_APPLICATIONINFO, intent2.getLongExtra(EXTRA_APPLICATIONINFO, 0));
            }
            if (this.mDefaultRadioButton.isChecked()) {
                intent.putExtra(USE_DEFAULT_SHIRTCUT, true);
                setResult(-1, intent);
                finish();
                return;
            }
            intent.putExtra("android.intent.extra.shortcut.NAME", this.edLabel.getText().toString());
            if (this.mBitmap != null) {
                intent.putExtra("android.intent.extra.shortcut.ICON", this.mBitmap);
            } else if (this.mIconResource != null) {
                intent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", this.mIconResource);
            }
            setResult(-1, intent);
            finish();
        } else if (view.equals(this.btCanel)) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.custom_shirtcuts);
        this.mRadioGroup = (RadioGroup) findViewById(R.id.custom_shirtcut);
        this.mRadioGroup.setOnCheckedChangeListener(this);
        this.mCustomRadioButton = (RadioButton) findViewById(R.id.custom_radio);
        this.mDefaultRadioButton = (RadioButton) findViewById(R.id.default_radio);
        this.btPickIcon = (ImageButton) findViewById(R.id.pick_icon);
        this.btPickIcon.setOnClickListener(this);
        this.btPickIcon.setEnabled(false);
        this.btOk = (Button) findViewById(R.id.shirtcut_ok);
        this.btOk.setEnabled(false);
        this.btOk.setOnClickListener(this);
        this.btCanel = (Button) findViewById(R.id.shirtcut_cancel);
        this.btCanel.setOnClickListener(this);
        this.edLabel = (EditText) findViewById(R.id.shirtcut_label);
        this.mPackageManager = getPackageManager();
        this.mIconSize = (int) getResources().getDimension(17104896);
        loadFromAppInfo(getAppInfo());
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 1:
                return new d(this).a();
            default:
                return super.onCreateDialog(i);
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        if (bundle != null && bundle.size() >= 7) {
            this.mBitmap = (Bitmap) bundle.getParcelable("mBitmap");
            this.mIntent = (Intent) bundle.getParcelable("mIntent");
            this.mIconResource = (Intent.ShortcutIconResource) bundle.getParcelable("mIconResource");
            this.mIconSize = bundle.getInt("mIconResource");
            if (this.mBitmap != null) {
                this.btPickIcon.setImageBitmap(this.mBitmap);
            } else if (this.mIconResource != null) {
                try {
                    Resources resourcesForApplication = this.mPackageManager.getResourcesForApplication(this.mIconResource.packageName);
                    this.btPickIcon.setImageDrawable(resourcesForApplication.getDrawable(resourcesForApplication.getIdentifier(this.mIconResource.resourceName, null, null)));
                } catch (PackageManager.NameNotFoundException e) {
                }
            }
            this.btPickIcon.setEnabled(bundle.getBoolean("btPickIcon_enabled"));
            this.btOk.setEnabled(bundle.getBoolean("btOk_enabled"));
            boolean z = bundle.getBoolean("mCustomRadio_checked");
            this.mCustomRadioButton.setChecked(z);
            this.mDefaultRadioButton.setChecked(!z);
        }
        super.onRestoreInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putParcelable("mBitmap", this.mBitmap);
        bundle.putParcelable("mIntent", this.mIntent);
        bundle.putParcelable("mIconResource", this.mIconResource);
        bundle.putInt("mIconSize", this.mIconSize);
        bundle.putBoolean("btOk_enabled", this.btOk.isEnabled());
        bundle.putBoolean("btPickIcon_enabled", this.btPickIcon.isEnabled());
        bundle.putBoolean("mCustomRadio_checked", this.mCustomRadioButton.isChecked());
    }
}
