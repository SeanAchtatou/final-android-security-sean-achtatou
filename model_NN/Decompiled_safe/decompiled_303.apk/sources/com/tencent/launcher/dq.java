package com.tencent.launcher;

import android.content.ContentValues;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;

final class dq extends ex {
    Intent a;
    Uri b;
    int c;
    Drawable d;
    boolean e;
    Intent.ShortcutIconResource f;

    dq() {
        this.m = 3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public final void a(ContentValues contentValues) {
        super.a(contentValues);
        contentValues.put("title", this.h.toString());
        contentValues.put("uri", this.b.toString());
        if (this.a != null) {
            contentValues.put("intent", this.a.toUri(0));
        }
        contentValues.put("iconType", (Integer) 0);
        contentValues.put("displayMode", Integer.valueOf(this.c));
        if (this.f != null) {
            contentValues.put("iconPackage", this.f.packageName);
            contentValues.put("iconResource", this.f.resourceName);
        }
    }
}
