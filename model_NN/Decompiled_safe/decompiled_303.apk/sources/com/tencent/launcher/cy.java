package com.tencent.launcher;

public final class cy {
    /* access modifiers changed from: private */
    public float a;
    /* access modifiers changed from: private */
    public float b;
    /* access modifiers changed from: private */
    public float c;
    /* access modifiers changed from: private */
    public float d;
    /* access modifiers changed from: private */
    public float e;
    /* access modifiers changed from: private */
    public float f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public boolean h;
    /* access modifiers changed from: private */
    public boolean i;

    public final void a() {
        this.a = 0.0f;
        this.b = 0.0f;
        this.g = true;
        this.c = 1.0f;
        this.h = false;
        this.d = 1.0f;
        this.e = 1.0f;
        this.i = false;
        this.f = 0.0f;
    }

    /* access modifiers changed from: protected */
    public final void a(float f2, float f3, float f4, float f5, float f6, float f7) {
        this.a = f2;
        this.b = f3;
        this.c = f4 == 0.0f ? 1.0f : f4;
        this.d = f5 == 0.0f ? 1.0f : f5;
        this.e = f6 == 0.0f ? 1.0f : f6;
        this.f = f7;
    }

    public final float b() {
        if (!this.g) {
            return 1.0f;
        }
        return this.c;
    }
}
