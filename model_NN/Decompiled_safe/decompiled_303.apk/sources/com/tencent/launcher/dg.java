package com.tencent.launcher;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

public final class dg extends BaseAdapter {
    public static final String[] a = {"http://fwd.3g.qq.com:8080/forward.jsp?bid=476&sid=0", "http://fwd.3g.qq.com:8080/forward.jsp?bid=478&sid=0", "http://fwd.3g.qq.com:8080/forward.jsp?bid=477&sid=0", "http://fwd.3g.qq.com:8080/forward.jsp?bid=479&sid=0"};
    private final ArrayList b = new ArrayList();
    private final LayoutInflater c;

    public dg(Launcher launcher) {
        this.c = (LayoutInflater) launcher.getSystemService("layout_inflater");
        Resources resources = launcher.getResources();
        this.b.add(new he(this, resources, R.string.market_common, R.drawable.ic_launcher_market_common));
        this.b.add(new he(this, resources, R.string.market_best, R.drawable.ic_launcher_market_best));
        this.b.add(new he(this, resources, R.string.market_new, R.drawable.ic_launcher_market_new));
        this.b.add(new he(this, resources, R.string.market_class, R.drawable.ic_launcher_market_class));
    }

    public final int getCount() {
        return this.b.size();
    }

    public final Object getItem(int i) {
        return this.b.get(i);
    }

    public final long getItemId(int i) {
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        he heVar = (he) getItem(i);
        View inflate = view == null ? this.c.inflate((int) R.layout.add_list_item, viewGroup, false) : view;
        TextView textView = (TextView) inflate;
        textView.setText(heVar.a);
        textView.setCompoundDrawablesWithIntrinsicBounds(heVar.b, (Drawable) null, (Drawable) null, (Drawable) null);
        return inflate;
    }
}
