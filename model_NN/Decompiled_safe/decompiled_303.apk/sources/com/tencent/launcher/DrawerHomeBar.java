package com.tencent.launcher;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.tencent.qqlauncher.R;

public class DrawerHomeBar extends FrameLayout implements e {
    private Launcher a;
    private TextView b;
    private boolean c;

    public DrawerHomeBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public final void a() {
        this.b.setVisibility(0);
        setVisibility(0);
    }

    public final void a(Launcher launcher) {
        this.a = launcher;
    }

    public final void a(cm cmVar, boolean z) {
        this.c = false;
    }

    public final boolean a(int i, Object obj) {
        return !this.c && i > getHeight() / 4;
    }

    public final boolean a(cm cmVar, int i, int i2, int i3, int i4, Object obj) {
        ((ha) obj).v = false;
        return true;
    }

    public final boolean a(cm cmVar, Object obj) {
        return false;
    }

    public final void b() {
        this.b.setVisibility(8);
        setVisibility(8);
    }

    public final void b(cm cmVar, int i, int i2, int i3, int i4, Object obj) {
        this.c = false;
        if (this.a != null && i2 > getHeight() / 4) {
            this.c = true;
            this.a.onDragFromDrawer();
        }
    }

    public final void c(cm cmVar, int i, int i2, int i3, int i4, Object obj) {
        if (!this.c && i2 > getHeight() / 4) {
            this.c = true;
            this.a.onDragFromDrawer();
            setVisibility(4);
        }
    }

    public void onFinishInflate() {
        super.onFinishInflate();
        this.b = (TextView) findViewById(R.id.back_home_tips);
    }
}
