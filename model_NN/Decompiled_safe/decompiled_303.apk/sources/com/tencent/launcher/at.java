package com.tencent.launcher;

import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import com.tencent.module.appcenter.d;

final class at implements View.OnClickListener {
    private /* synthetic */ SearchAppActivityReserved a;

    at(SearchAppActivityReserved searchAppActivityReserved) {
        this.a = searchAppActivityReserved;
    }

    public final void onClick(View view) {
        int unused = this.a.nSearchedAppCount = 0;
        this.a.netsearchTips.setText("正在努力搜索中");
        this.a.setSearchResult();
        ((InputMethodManager) this.a.getSystemService("input_method")).hideSoftInputFromWindow(this.a.mListView.getWindowToken(), 0);
        this.a.mSearchEditText.setText(((Button) view).getText().toString());
        this.a.InitialNetSearch();
        d.a().a(this.a.handler, this.a.mSearchEditText.getText().toString(), this.a.nPageNumber);
        SearchAppActivityReserved.access$508(this.a);
    }
}
