package com.tencent.launcher;

import android.widget.ArrayAdapter;

public final class fn {
    public static boolean a = false;
    private Launcher b;
    private QGridLayout c;
    private VerticalAppLayout d;

    public final QGridLayout a() {
        return this.c;
    }

    public final void a(int i) {
        if (this.c != null) {
            QGridLayout qGridLayout = this.c;
            qGridLayout.getClass();
            new dv(qGridLayout).execute(Integer.valueOf(i));
        }
    }

    public final void a(ArrayAdapter arrayAdapter) {
        if (this.c != null) {
            this.c.a(arrayAdapter);
        } else if (this.d != null) {
            this.d.a(arrayAdapter);
        }
    }

    public final void a(Launcher launcher) {
        this.b = launcher;
        if (this.c != null) {
            this.c.a(launcher);
        } else if (this.d != null) {
            this.d.a(launcher);
        }
    }

    public final void a(QGridLayout qGridLayout) {
        this.c = qGridLayout;
        this.d = null;
    }

    public final void a(VerticalAppLayout verticalAppLayout) {
        this.d = verticalAppLayout;
        this.c = null;
    }

    public final void a(dt dtVar) {
        if (this.c != null) {
            this.c.a(dtVar);
        } else if (this.d != null) {
            this.d.a(dtVar);
        }
    }

    public final ArrayAdapter b() {
        if (this.c != null) {
            return this.c.d();
        }
        if (this.d != null) {
            return this.d.c();
        }
        return null;
    }

    public final boolean c() {
        return this.c != null && (this.c instanceof QGridLayout);
    }
}
