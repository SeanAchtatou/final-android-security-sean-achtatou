package com.tencent.launcher;

import android.graphics.drawable.Drawable;
import android.view.View;
import com.tencent.launcher.CellLayout;

final class gj implements Runnable {
    private /* synthetic */ CellLayout a;

    gj(CellLayout cellLayout) {
        this.a = cellLayout;
    }

    public final void run() {
        View f = this.a.D;
        if (f instanceof FolderIcon) {
            FolderIcon unused = this.a.H = (FolderIcon) f;
            this.a.H.d();
        } else if (f instanceof BubbleTextView) {
            Drawable unused2 = this.a.M = this.a.I;
            CellLayout.LayoutParams layoutParams = (CellLayout.LayoutParams) f.getLayoutParams();
            int unused3 = this.a.S = f.getLeft() + f.getPaddingLeft() + layoutParams.leftMargin;
            int unused4 = this.a.T = f.getPaddingTop() + f.getTop() + layoutParams.topMargin;
        } else {
            return;
        }
        int unused5 = this.a.W = 3;
        this.a.invalidate();
    }
}
