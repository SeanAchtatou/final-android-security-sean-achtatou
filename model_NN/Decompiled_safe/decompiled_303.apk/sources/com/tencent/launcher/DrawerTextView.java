package com.tencent.launcher;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.tencent.qqlauncher.R;

public class DrawerTextView extends TextView {
    private bq a;
    private Drawable b;
    private Drawable c;
    private boolean d = false;
    private boolean e;
    private boolean f;
    private Drawable g;

    public DrawerTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setFocusable(true);
        this.g = getBackground();
        setBackgroundDrawable(null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    static DrawerTextView a(Context context, ViewGroup viewGroup, bq bqVar, View.OnClickListener onClickListener) {
        Drawable drawable;
        DrawerTextView drawerTextView = (DrawerTextView) LayoutInflater.from(context).inflate((int) R.layout.application_boxed, viewGroup, false);
        Resources resources = context.getResources();
        drawerTextView.setText(bqVar.h);
        drawerTextView.setTag(bqVar);
        drawerTextView.setFocusable(true);
        drawerTextView.setOnClickListener(onClickListener);
        drawerTextView.a = bqVar;
        try {
            drawable = bqVar.c(context, false);
        } catch (Exception e2) {
            Log.e("", "create bitmap failed...");
            drawable = null;
        }
        if (drawable != null) {
            drawerTextView.b = drawable;
            drawerTextView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, drawable, (Drawable) null, (Drawable) null);
        } else {
            Drawable a2 = a.a(resources.getDrawable(R.drawable.folder_icon_default), context);
            drawerTextView.b = a2;
            drawerTextView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, a2, (Drawable) null, (Drawable) null);
        }
        drawerTextView.c = drawerTextView.b;
        return drawerTextView;
    }

    static DrawerTextView a(DrawerTextView drawerTextView, Context context, ViewGroup viewGroup, bq bqVar, View.OnClickListener onClickListener) {
        Drawable drawable;
        if (drawerTextView == null) {
            return a(context, viewGroup, bqVar, onClickListener);
        }
        Resources resources = context.getResources();
        drawerTextView.setText(bqVar.h);
        drawerTextView.setTag(bqVar);
        drawerTextView.setOnClickListener(onClickListener);
        drawerTextView.a = bqVar;
        try {
            drawable = bqVar.c(context, false);
        } catch (Exception e2) {
            Log.e("", "create bitmap failed...");
            drawable = null;
        }
        if (drawable != null) {
            drawerTextView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, drawable, (Drawable) null, (Drawable) null);
        } else {
            drawerTextView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, a.a(resources.getDrawable(R.drawable.folder_icon_default), context), (Drawable) null, (Drawable) null);
        }
        return drawerTextView;
    }

    public final void a() {
        Drawable drawable;
        if (!this.e && getTag() != null && (getTag() instanceof bq)) {
            this.e = true;
            try {
                drawable = this.a.a(getContext(), false);
            } catch (Exception e2) {
                Log.e("", "create bitmap failed...");
                drawable = null;
            }
            if (drawable != null) {
                this.b = drawable;
                setCompoundDrawablesWithIntrinsicBounds((Drawable) null, this.b, (Drawable) null, (Drawable) null);
            }
            destroyDrawingCache();
            setWillNotCacheDrawing(false);
            postInvalidate();
        }
    }

    public final void a(UserFolder userFolder) {
        Drawable drawable;
        if (getTag() != null && (getTag() instanceof bq)) {
            try {
                drawable = this.a.a(getContext(), false, userFolder);
            } catch (Exception e2) {
                Log.e("", "create bitmap failed...");
                drawable = null;
            }
            if (drawable != null) {
                this.b = drawable;
                setCompoundDrawablesWithIntrinsicBounds((Drawable) null, this.b, (Drawable) null, (Drawable) null);
            }
            destroyDrawingCache();
            setWillNotCacheDrawing(false);
            postInvalidate();
        }
    }

    public final void b() {
        if (this.e) {
            f();
            this.e = false;
        }
    }

    public final void c() {
        Drawable drawable;
        if (!this.d && getTag() != null && (getTag() instanceof bq)) {
            this.d = true;
            try {
                drawable = this.a.b(getContext(), false);
            } catch (Exception e2) {
                Log.e("", "create bitmap failed...");
                drawable = null;
            }
            if (drawable != null) {
                this.b = drawable;
                setCompoundDrawablesWithIntrinsicBounds((Drawable) null, this.b, (Drawable) null, (Drawable) null);
            }
            destroyDrawingCache();
            setWillNotCacheDrawing(false);
            postInvalidate();
        }
    }

    public final Rect d() {
        int i;
        int i2;
        int size = this.a.b.size();
        int left = getLeft() + getPaddingLeft();
        int top = getTop() + getPaddingTop();
        if (size <= 4) {
            Rect a2 = a.a(getContext(), size - 1);
            a2.offset(left, top);
            return a2;
        }
        Rect rect = new Rect();
        if (this.b != null) {
            Drawable drawable = this.b;
            int centerY = top + drawable.getBounds().centerY();
            i = left + drawable.getBounds().centerX();
            i2 = centerY;
        } else {
            int i3 = top;
            i = left;
            i2 = i3;
        }
        rect.set(0, 0, 1, 1);
        rect.offset(i, i2);
        return rect;
    }

    public void draw(Canvas canvas) {
        Drawable drawable = this.g;
        if (drawable != null) {
            int scrollX = getScrollX();
            int scrollY = getScrollY();
            if (this.f) {
                drawable.setBounds(0, 0, getRight() - getLeft(), getBottom() - getTop());
                this.f = false;
            }
            if ((scrollX | scrollY) == 0) {
                drawable.draw(canvas);
            } else {
                canvas.translate((float) scrollX, (float) scrollY);
                drawable.draw(canvas);
                canvas.translate((float) (-scrollX), (float) (-scrollY));
            }
        }
        super.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        Drawable drawable = this.g;
        if (drawable != null && drawable.isStateful()) {
            drawable.setState(getDrawableState());
        }
        super.drawableStateChanged();
    }

    public final void e() {
        if (this.d) {
            f();
            this.d = false;
        }
    }

    public final void f() {
        Drawable drawable;
        if (getTag() != null && (getTag() instanceof bq)) {
            try {
                drawable = this.a.c(getContext(), false);
            } catch (Exception e2) {
                Log.e("", "create bitmap failed...");
                drawable = null;
            }
            if (drawable != null) {
                this.b = drawable;
                setCompoundDrawablesWithIntrinsicBounds((Drawable) null, this.b, (Drawable) null, (Drawable) null);
            }
            destroyDrawingCache();
        }
    }

    public final boolean g() {
        if (this.a == null) {
            return false;
        }
        return this.a instanceof bq;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        if (this.g != null) {
            this.g.setCallback(this);
        }
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        if (this.g != null) {
            this.g.setCallback(null);
        }
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public boolean setFrame(int i, int i2, int i3, int i4) {
        if (!(getLeft() == i && getRight() == i3 && getTop() == i2 && getBottom() == i4)) {
            this.f = true;
        }
        return super.setFrame(i, i2, i3, i4);
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return drawable == this.g || super.verifyDrawable(drawable);
    }
}
