package com.tencent.launcher;

final class hg implements Runnable {
    final /* synthetic */ ThumbnailWorkspace a;

    hg(ThumbnailWorkspace thumbnailWorkspace) {
        this.a = thumbnailWorkspace;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ThumbnailWorkspace.b(com.tencent.launcher.ThumbnailWorkspace, boolean):boolean
     arg types: [com.tencent.launcher.ThumbnailWorkspace, int]
     candidates:
      com.tencent.launcher.ThumbnailWorkspace.b(int, boolean):android.view.animation.Animation
      com.tencent.launcher.ThumbnailWorkspace.b(int, int):void
      com.tencent.launcher.ThumbnailWorkspace.b(com.tencent.launcher.ThumbnailWorkspace, int):void
      com.tencent.launcher.ThumbnailWorkspace.b(com.tencent.launcher.ThumbnailWorkspace, android.view.View):void
      com.tencent.launcher.ThumbnailWorkspace.b(com.tencent.launcher.ThumbnailWorkspace, boolean):boolean */
    public final void run() {
        if (this.a.u) {
            this.a.m.postDelayed(new bg(this), 100);
            return;
        }
        this.a.o.setAlpha(BrightnessActivity.MAXIMUM_BACKLIGHT);
        this.a.requestLayout();
        boolean unused = this.a.r = false;
    }
}
