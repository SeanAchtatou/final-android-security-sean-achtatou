package com.tencent.launcher;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

final class fe extends BaseAdapter {
    private ArrayList a;
    private Context b;

    public fe(ArrayList arrayList, Context context) {
        this.a = arrayList;
        this.b = context;
    }

    public final int getCount() {
        return this.a.size();
    }

    public final Object getItem(int i) {
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return 0;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? LayoutInflater.from(this.b).inflate((int) R.layout.launcher_pick_item, (ViewGroup) null) : view;
        bc bcVar = (bc) getItem(i);
        ((TextView) inflate.findViewById(R.id.tv_title)).setText(bcVar.a);
        ((TextView) inflate.findViewById(R.id.tv_num)).setText(this.b.getString(R.string.import_available, Integer.valueOf(bcVar.d)));
        ((ImageView) inflate.findViewById(R.id.launcher_img)).setImageDrawable(bcVar.c);
        return inflate;
    }
}
