package com.tencent.launcher;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.Scroller;
import com.tencent.qqlauncher.R;

public class ThumbnailWorkspace extends ViewGroup {
    private static int C;
    private static int D;
    private static int E;
    private static int F;
    /* access modifiers changed from: private */
    public int A;
    /* access modifiers changed from: private */
    public boolean B;
    private int G;
    private int H;
    private View.OnLongClickListener I = new hf(this);
    private View.OnClickListener J = new hk(this);
    private View.OnClickListener K = new hl(this);
    private View.OnClickListener L = new hj(this);
    /* access modifiers changed from: private */
    public int M;
    /* access modifiers changed from: private */
    public int N;
    private Animation.AnimationListener O = new hm(this);
    private int P;
    private int Q;
    private int a;
    /* access modifiers changed from: private */
    public Context b;
    private LayoutInflater c;
    private Scroller d;
    /* access modifiers changed from: private */
    public ds e;
    private int f;
    private int g;
    private VelocityTracker h;
    private int i;
    private int j;
    private float k;
    private float l;
    /* access modifiers changed from: private */
    public Handler m;
    private View n;
    /* access modifiers changed from: private */
    public ImageView o;
    private boolean p;
    private int q;
    /* access modifiers changed from: private */
    public boolean r;
    /* access modifiers changed from: private */
    public boolean s = false;
    /* access modifiers changed from: private */
    public boolean t = false;
    /* access modifiers changed from: private */
    public boolean u = false;
    /* access modifiers changed from: private */
    public boolean v = false;
    /* access modifiers changed from: private */
    public int w;
    private int x;
    private int y;
    /* access modifiers changed from: private */
    public int z;

    public ThumbnailWorkspace(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public ThumbnailWorkspace(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context);
    }

    /* access modifiers changed from: private */
    public int a(int i2, int i3) {
        int measuredHeight = (this.i * this.w) + (((i3 - E) / (((getMeasuredHeight() - E) - F) / this.y)) * this.x) + (((i2 - C) % getWidth()) / (((getMeasuredWidth() - C) - D) / this.x));
        return measuredHeight > getChildCount() - 1 ? getChildCount() - 1 : measuredHeight;
    }

    private void a(Context context) {
        this.b = context;
        C = (int) context.getResources().getDimension(R.dimen.thumbnail_width_padding_start);
        D = (int) context.getResources().getDimension(R.dimen.thumbnail_width_padding_end);
        E = (int) context.getResources().getDimension(R.dimen.thumbnail_height_padding_start);
        F = (int) context.getResources().getDimension(R.dimen.thumbnail_height_padding_end);
        this.d = new Scroller(this.b);
        this.c = LayoutInflater.from(this.b);
        this.w = 9;
        this.y = 3;
        this.x = 3;
        this.q = 0;
        ViewConfiguration viewConfiguration = ViewConfiguration.get(this.b);
        this.f = viewConfiguration.getScaledTouchSlop();
        this.g = viewConfiguration.getScaledMinimumFlingVelocity();
        this.j = -1;
        this.i = 0;
        this.m = new Handler();
        this.n = this.c.inflate((int) R.layout.thumbnail_new_screen, (ViewGroup) null);
        this.o = (ImageView) this.n.findViewById(R.id.new_screen);
        this.o.setOnClickListener(new hh(this));
        addView(this.n);
    }

    static /* synthetic */ void a(ThumbnailWorkspace thumbnailWorkspace, View view) {
        thumbnailWorkspace.t = true;
        new AlertDialog.Builder(thumbnailWorkspace.b).setTitle((int) R.string.thumbnail_close_title).setPositiveButton((int) R.string.thumbnail_close_ok, new ho(thumbnailWorkspace, view)).setNegativeButton((int) R.string.thumbnail_close_canel, new hq(thumbnailWorkspace)).setOnCancelListener(new hn(thumbnailWorkspace)).create().show();
    }

    private Bitmap b(View view) {
        int i2;
        int i3;
        if (view == null) {
            return null;
        }
        if (((ViewGroup) view).getChildCount() == 0) {
            return null;
        }
        int width = view.getWidth();
        int height = view.getHeight();
        int measuredWidth = ((getMeasuredWidth() - C) - D) / this.x;
        int measuredHeight = ((getMeasuredHeight() - E) - F) / this.y;
        if (measuredWidth == 0 || measuredHeight == 0) {
            int i4 = this.P / this.x;
            i2 = i4;
            i3 = this.Q / this.y;
        } else {
            int i5 = measuredHeight;
            i2 = measuredWidth;
            i3 = i5;
        }
        float f2 = ((float) i2) / ((float) width);
        float f3 = ((float) i3) / ((float) height);
        Bitmap createBitmap = Bitmap.createBitmap(i2, i3, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        canvas.scale(f2, f3);
        view.draw(canvas);
        return createBitmap;
    }

    private Animation b(int i2, boolean z2) {
        TranslateAnimation translateAnimation;
        int i3 = z2 ? i2 - 1 : i2 + 1;
        int d2 = d(i3) - d(i2);
        int c2 = c(i3) - c(i2);
        if (!z2 || i2 != 9) {
            TranslateAnimation translateAnimation2 = new TranslateAnimation(0.0f, (float) d2, 0.0f, (float) c2);
            translateAnimation2.setDuration(200);
            translateAnimation = translateAnimation2;
        } else {
            TranslateAnimation translateAnimation3 = new TranslateAnimation(0.0f, (float) d2, (float) c2, (float) c2);
            translateAnimation3.setDuration(200);
            translateAnimation = translateAnimation3;
        }
        translateAnimation.setInterpolator(new DecelerateInterpolator());
        translateAnimation.setAnimationListener(this.O);
        translateAnimation.setFillAfter(true);
        return translateAnimation;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ThumbnailWorkspace.b(int, boolean):android.view.animation.Animation
     arg types: [int, int]
     candidates:
      com.tencent.launcher.ThumbnailWorkspace.b(int, int):void
      com.tencent.launcher.ThumbnailWorkspace.b(com.tencent.launcher.ThumbnailWorkspace, int):void
      com.tencent.launcher.ThumbnailWorkspace.b(com.tencent.launcher.ThumbnailWorkspace, android.view.View):void
      com.tencent.launcher.ThumbnailWorkspace.b(com.tencent.launcher.ThumbnailWorkspace, boolean):boolean
      com.tencent.launcher.ThumbnailWorkspace.b(int, boolean):android.view.animation.Animation */
    /* access modifiers changed from: private */
    public void b(int i2, int i3) {
        if (this.u) {
            g();
        }
        if (i2 != i3) {
            boolean z2 = i2 > i3;
            this.u = true;
            this.M = Math.abs(i2 - i3);
            this.N = 0;
            if (z2) {
                for (int i4 = i3 + 1; i4 <= i2; i4++) {
                    View childAt = getChildAt(i4);
                    Animation b2 = b(i4, true);
                    b2.setStartOffset((long) (((i4 - i3) - 1) * 20));
                    childAt.startAnimation(b2);
                }
                return;
            }
            for (int i5 = i3 - 1; i5 >= i2; i5--) {
                View childAt2 = getChildAt(i5);
                Animation b3 = b(i5, false);
                b3.setStartOffset((long) (((i3 - 1) - i5) * 20));
                childAt2.startAnimation(b3);
            }
        }
    }

    static /* synthetic */ void b(ThumbnailWorkspace thumbnailWorkspace, int i2) {
        if (thumbnailWorkspace.d.isFinished()) {
            int max = Math.max(0, Math.min(i2, (((thumbnailWorkspace.getChildCount() - 1) / thumbnailWorkspace.w) + 1) - 1));
            thumbnailWorkspace.j = max;
            int width = (max * thumbnailWorkspace.getWidth()) - thumbnailWorkspace.getScrollX();
            thumbnailWorkspace.d.startScroll(thumbnailWorkspace.getScrollX(), 0, width, 0, Math.abs(width) * 2);
            thumbnailWorkspace.invalidate();
        }
    }

    static /* synthetic */ void b(ThumbnailWorkspace thumbnailWorkspace, View view) {
        thumbnailWorkspace.p = true;
        View view2 = (View) view.getParent();
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setDuration(200);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setAnimationListener(new hi(thumbnailWorkspace, view2));
        view2.startAnimation(scaleAnimation);
    }

    private int c(int i2) {
        return (((i2 % this.w) / this.x) * (((getMeasuredHeight() - E) - F) / this.y)) + E;
    }

    private int d(int i2) {
        int i3 = (i2 % this.w) % this.x;
        int measuredWidth = ((getMeasuredWidth() - C) - D) / this.x;
        return (getMeasuredWidth() * (i2 / this.w)) + C + (i3 * measuredWidth);
    }

    private void f() {
        View childAt = getChildAt(this.z);
        int measuredWidth = ((getMeasuredWidth() - C) - D) / this.x;
        int measuredHeight = ((getMeasuredHeight() - E) - F) / this.y;
        int width = ((int) this.k) + (this.i * getWidth());
        int i2 = (int) this.l;
        childAt.layout(width - (measuredWidth / 2), i2 - (measuredHeight / 2), (measuredWidth / 2) + width, (measuredHeight / 2) + i2);
        childAt.invalidate();
    }

    /* access modifiers changed from: private */
    public void g() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            getChildAt(i2).clearAnimation();
        }
        int i3 = this.z;
        int a2 = a((int) this.k, (int) this.l);
        View childAt = getChildAt(i3);
        removeViewAt(i3);
        addView(childAt, a2);
        this.z = a2;
        this.u = false;
        this.N = 0;
        requestLayout();
        invalidate();
    }

    static /* synthetic */ int i(ThumbnailWorkspace thumbnailWorkspace) {
        return ((thumbnailWorkspace.getChildCount() - 1) / thumbnailWorkspace.w) + 1;
    }

    static /* synthetic */ int l(ThumbnailWorkspace thumbnailWorkspace) {
        int i2 = thumbnailWorkspace.N;
        thumbnailWorkspace.N = i2 + 1;
        return i2;
    }

    static /* synthetic */ void p(ThumbnailWorkspace thumbnailWorkspace) {
        int childCount = thumbnailWorkspace.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            thumbnailWorkspace.getChildAt(i2).clearAnimation();
        }
        thumbnailWorkspace.u = false;
        thumbnailWorkspace.s = false;
        thumbnailWorkspace.N = 0;
        thumbnailWorkspace.removeViewAt(thumbnailWorkspace.A);
        thumbnailWorkspace.requestLayout();
        thumbnailWorkspace.invalidate();
    }

    public final int a() {
        int childCount = getChildCount();
        int i2 = 0;
        while (i2 < childCount) {
            ImageView imageView = (ImageView) getChildAt(i2).findViewById(R.id.home_light);
            if (imageView == null || imageView.getVisibility() != 0) {
                i2++;
            } else {
                this.a = i2;
                return this.a;
            }
        }
        return this.a;
    }

    public final void a(int i2) {
        this.a = i2;
        if (i2 <= getChildCount() - 1) {
            ((ImageView) getChildAt(i2).findViewById(R.id.home_light)).setVisibility(0);
        }
    }

    public final void a(int i2, boolean z2) {
        View findViewById = getChildAt(i2).findViewById(R.id.delete_screen);
        if (findViewById == null) {
            return;
        }
        if (z2) {
            findViewById.setTag(null);
        } else {
            findViewById.setTag(1);
        }
    }

    public final void a(View view) {
        View inflate = this.c.inflate((int) R.layout.thumbnail_item, (ViewGroup) null);
        inflate.setOnLongClickListener(this.I);
        inflate.setOnClickListener(this.L);
        View findViewById = inflate.findViewById(R.id.delete_screen);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.thumbnail_imageview);
        ImageView imageView2 = (ImageView) inflate.findViewById(R.id.home_button);
        if (!(imageView == null || view == null)) {
            imageView.setImageBitmap(b(view));
        }
        if (findViewById != null) {
            findViewById.setOnClickListener(this.K);
        }
        if (imageView2 != null) {
            imageView2.setOnClickListener(this.J);
        }
        if (view != null) {
            inflate.setTag(view);
        }
        addView(inflate, getChildCount() - 1);
    }

    public final void a(View view, int i2) {
        ImageView imageView = (ImageView) getChildAt(i2).findViewById(R.id.thumbnail_imageview);
        if (imageView == null) {
            return;
        }
        if (view == null) {
            imageView.setImageBitmap(null);
            return;
        }
        try {
            imageView.setImageBitmap(b(view));
        } catch (OutOfMemoryError e2) {
        }
    }

    public final void a(ds dsVar) {
        this.e = dsVar;
    }

    public final void a(boolean z2) {
        this.p = z2;
    }

    public final void b(int i2) {
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount - 1; i3++) {
            getChildAt(i3).findViewById(R.id.thumbnail_imageview).setSelected(false);
        }
        getChildAt(i2).findViewById(R.id.thumbnail_imageview).setSelected(true);
    }

    public final boolean b() {
        return this.p;
    }

    public final void c() {
        this.B = false;
    }

    public void computeScroll() {
        if (this.d.computeScrollOffset()) {
            scrollTo(this.d.getCurrX(), this.d.getCurrY());
            postInvalidate();
        } else if (this.j != -1) {
            this.i = Math.max(0, Math.min(this.j, (((getChildCount() - 1) / this.w) + 1) - 1));
            this.j = -1;
        }
    }

    public final boolean d() {
        if (e()) {
            return false;
        }
        return !this.t;
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        getScrollX();
        long drawingTime = getDrawingTime();
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            if (getChildAt(i2) != null) {
                drawChild(canvas, getChildAt(i2), drawingTime);
            }
        }
    }

    public final boolean e() {
        if (this.s) {
            return true;
        }
        if (this.u) {
            return true;
        }
        if (this.v) {
            return true;
        }
        return this.B;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (this.r) {
            return true;
        }
        if (this.s) {
            return true;
        }
        if (action == 2 && this.q != 0) {
            return true;
        }
        float x2 = motionEvent.getX();
        float y2 = motionEvent.getY();
        switch (action) {
            case 0:
                this.k = x2;
                this.l = y2;
                this.q = this.d.isFinished() ? 0 : 1;
                break;
            case 1:
                this.q = 0;
                this.A = a((int) x2, (int) y2);
                break;
            case 2:
                if (((int) Math.abs(x2 - this.k)) > this.f) {
                    this.q = 1;
                    break;
                }
                break;
        }
        return this.q != 0;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        if (!this.r) {
            removeView(this.n);
            addView(this.n);
        }
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            if (childAt.getVisibility() == 0) {
                int d2 = d(i6);
                int c2 = c(i6);
                childAt.layout(d2, c2, (((getMeasuredWidth() - C) - D) / this.x) + d2, (((getMeasuredHeight() - E) - F) / this.y) + c2);
            }
        }
        if (this.r) {
            f();
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int size = View.MeasureSpec.getSize(i2);
        int mode = View.MeasureSpec.getMode(i2);
        int size2 = View.MeasureSpec.getSize(i3);
        int mode2 = View.MeasureSpec.getMode(i3);
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(((size - C) - D) / this.y, 1073741824);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(((size2 - E) - F) / this.x, 1073741824);
        if (mode != 1073741824) {
            throw new IllegalStateException("Workspace can only be used in EXACTLY mode.");
        } else if (mode2 != 1073741824) {
            throw new IllegalStateException("Workspace can only be used in EXACTLY mode.");
        } else {
            int childCount = getChildCount();
            for (int i4 = 0; i4 < childCount; i4++) {
                getChildAt(i4).measure(makeMeasureSpec, makeMeasureSpec2);
            }
            setMeasuredDimension(size, size2);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int a2;
        if (this.s) {
            return true;
        }
        if (this.h == null) {
            this.h = VelocityTracker.obtain();
        }
        this.h.addMovement(motionEvent);
        int action = motionEvent.getAction();
        float x2 = motionEvent.getX();
        float y2 = motionEvent.getY();
        switch (action) {
            case 0:
                if (!this.d.isFinished()) {
                    this.d.abortAnimation();
                }
                this.k = x2;
                this.l = y2;
                this.q = 1;
                this.H = 0;
                this.G = this.z;
                break;
            case 1:
                this.A = a((int) x2, (int) y2);
                if (!this.r) {
                    this.h.getXVelocity();
                    if (this.h != null) {
                        this.h.recycle();
                        this.h = null;
                    }
                    this.q = 0;
                    break;
                } else {
                    if (this.u) {
                        this.m.postDelayed(new hg(this), 350);
                    } else {
                        this.o.setAlpha(BrightnessActivity.MAXIMUM_BACKLIGHT);
                        requestLayout();
                        this.r = false;
                    }
                    return true;
                }
            case 2:
                int i2 = (int) (this.k - x2);
                this.k = x2;
                this.l = y2;
                if (this.r) {
                    f();
                    int a3 = a((int) x2, (int) y2);
                    if (a3 == this.G) {
                        this.H++;
                    } else {
                        this.G = a3;
                        this.H = 0;
                    }
                    if (this.H >= 5) {
                        int i3 = (int) x2;
                        int i4 = (int) y2;
                        if (!this.u && (a2 = a(i3, i4)) != this.z) {
                            b(a2, this.z);
                            this.p = true;
                            break;
                        }
                    }
                } else {
                    int scrollX = getScrollX();
                    if (i2 >= 0) {
                        if (i2 > 0) {
                            getChildCount();
                            getWidth();
                            getWidth();
                            break;
                        }
                    } else if (scrollX > 0) {
                    }
                }
                break;
            case 3:
                this.q = 0;
                break;
        }
        return true;
    }
}
