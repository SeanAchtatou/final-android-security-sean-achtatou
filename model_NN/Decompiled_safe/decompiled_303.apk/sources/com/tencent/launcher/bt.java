package com.tencent.launcher;

import android.view.animation.Animation;

final class bt implements Animation.AnimationListener {
    private /* synthetic */ SliderView a;

    bt(SliderView sliderView) {
        this.a = sliderView;
    }

    public final void onAnimationEnd(Animation animation) {
        SliderView.a(this.a);
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
