package com.tencent.launcher;

import android.content.Context;
import android.content.DialogInterface;

final class en implements DialogInterface.OnClickListener {
    private /* synthetic */ String a;
    private /* synthetic */ Context b;
    private /* synthetic */ boolean c;

    en(String str, Context context, boolean z) {
        this.a = str;
        this.b = context;
        this.c = z;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        Launcher.showImportingDialog(this.a, this.b, this.c);
    }
}
