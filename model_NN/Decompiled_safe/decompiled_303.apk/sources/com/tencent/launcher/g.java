package com.tencent.launcher;

import android.view.animation.Interpolator;

public final class g implements Interpolator {
    private final float a = 4.0f;

    public final float getInterpolation(float f) {
        float f2 = f - 1.0f;
        return (((f2 * (this.a + 1.0f)) + this.a) * f2 * f2 * f2 * f2) + 1.0f;
    }
}
