package com.tencent.launcher;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

final class hp extends Drawable {
    private Bitmap a;

    hp(Bitmap bitmap) {
        this.a = bitmap;
    }

    public final Bitmap a() {
        return this.a;
    }

    public final void draw(Canvas canvas) {
        canvas.drawBitmap(this.a, 0.0f, 0.0f, (Paint) null);
    }

    public final int getIntrinsicHeight() {
        return this.a.getHeight();
    }

    public final int getIntrinsicWidth() {
        return this.a.getWidth();
    }

    public final int getMinimumHeight() {
        return this.a.getHeight();
    }

    public final int getMinimumWidth() {
        return this.a.getWidth();
    }

    public final int getOpacity() {
        return -3;
    }

    public final void setAlpha(int i) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
