package com.tencent.launcher;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import java.io.File;

final class eo implements Runnable {
    private /* synthetic */ File a;
    private /* synthetic */ Context b;

    eo(File file, Context context) {
        this.a = file;
        this.b = context;
    }

    public final void run() {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(this.a), "application/vnd.android.package-archive");
        this.b.startActivity(intent);
    }
}
