package com.tencent.launcher;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

final class dv extends AsyncTask {
    private ProgressDialog a;
    private ArrayList b;
    private /* synthetic */ QGridLayout c;

    dv(QGridLayout qGridLayout) {
        this.c = qGridLayout;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        int intValue = ((Integer[]) objArr)[0].intValue();
        this.b = new ArrayList();
        boolean a2 = QGridLayout.a(this.c, intValue, this.b);
        publishProgress(Boolean.valueOf(a2));
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            ha haVar = (ha) this.b.get(i);
            if (a2) {
                ff.a(this.c.l, haVar);
            }
            if (haVar instanceof bq) {
                bq bqVar = (bq) haVar;
                int size2 = bqVar.b.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    am amVar = (am) bqVar.b.get(i2);
                    amVar.u = i2;
                    ff.a(this.c.l, amVar);
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        this.a = new ProgressDialog(this.c.l);
        this.a.setMessage(this.c.l.getString(R.string.order_app_runing));
        this.a.show();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onProgressUpdate(Object[] objArr) {
        Boolean[] boolArr = (Boolean[]) objArr;
        if (boolArr.length == 1) {
            this.a.dismiss();
            if (boolArr[0].booleanValue()) {
                this.c.t.setNotifyOnChange(false);
                this.c.t.clear();
                int size = this.b.size();
                for (int i = 0; i < size; i++) {
                    this.c.t.add((ha) this.b.get(i));
                }
                this.c.t.setNotifyOnChange(true);
                this.c.c();
            }
        }
    }
}
