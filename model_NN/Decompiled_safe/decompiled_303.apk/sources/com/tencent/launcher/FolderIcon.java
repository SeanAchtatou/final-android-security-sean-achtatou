package com.tencent.launcher;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.tencent.qqlauncher.R;

public class FolderIcon extends BubbleTextView {
    private bq a;
    private Drawable b;
    private Drawable c;
    private boolean d = false;
    private boolean e;
    private boolean f;
    private Drawable g = null;

    public FolderIcon(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    static FolderIcon a(Launcher launcher, ViewGroup viewGroup, bq bqVar) {
        Drawable drawable;
        FolderIcon folderIcon = (FolderIcon) LayoutInflater.from(launcher).inflate((int) R.layout.folder_icon, viewGroup, false);
        Resources resources = launcher.getResources();
        folderIcon.setText(bqVar.h);
        folderIcon.setTag(bqVar);
        folderIcon.setOnClickListener(launcher);
        folderIcon.a = bqVar;
        try {
            drawable = bqVar.c(launcher, true);
        } catch (Exception e2) {
            Log.e("", "create bitmap failed...");
            drawable = null;
        }
        if (drawable != null) {
            folderIcon.b = drawable;
            folderIcon.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, drawable, (Drawable) null, (Drawable) null);
        } else {
            Drawable a2 = a.a(resources.getDrawable(R.drawable.folder_icon_default), launcher);
            folderIcon.b = a2;
            folderIcon.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, a2, (Drawable) null, (Drawable) null);
        }
        folderIcon.c = folderIcon.b;
        return folderIcon;
    }

    public void a() {
        Drawable drawable;
        destroyDrawingCache();
        try {
            drawable = this.a.c(getContext(), true);
        } catch (Exception e2) {
            Log.e("", "create bitmap failed...");
            drawable = null;
        }
        if (drawable != null) {
            this.b = drawable;
            setCompoundDrawablesWithIntrinsicBounds((Drawable) null, drawable, (Drawable) null, (Drawable) null);
        }
        postInvalidate();
    }

    public final void a(UserFolder userFolder) {
        Drawable drawable;
        try {
            drawable = this.a.a(getContext(), true, userFolder);
        } catch (Exception e2) {
            Log.e("", "create bitmap failed...");
            drawable = null;
        }
        if (drawable != null) {
            this.b = drawable;
            setCompoundDrawablesWithIntrinsicBounds((Drawable) null, drawable, (Drawable) null, (Drawable) null);
        }
        destroyDrawingCache();
        postInvalidate();
    }

    public final void d() {
        Drawable drawable;
        if (!this.f && getTag() != null && (getTag() instanceof bq)) {
            this.f = true;
            try {
                drawable = this.a.a(getContext(), true);
            } catch (Exception e2) {
                Log.e("", "create bitmap failed...");
                drawable = null;
            }
            if (drawable != null) {
                this.b = drawable;
                setCompoundDrawablesWithIntrinsicBounds((Drawable) null, this.b, (Drawable) null, (Drawable) null);
            }
            destroyDrawingCache();
            setWillNotCacheDrawing(false);
            postInvalidate();
        }
    }

    public final void e() {
        if (this.f) {
            a();
            this.f = false;
        }
    }

    public final void f() {
        Drawable drawable;
        if (!this.e && getTag() != null && (getTag() instanceof bq)) {
            this.e = true;
            try {
                drawable = this.a.b(getContext(), true);
            } catch (Exception e2) {
                Log.e("", "create bitmap failed...");
                drawable = null;
            }
            if (drawable != null) {
                this.b = drawable;
                setCompoundDrawablesWithIntrinsicBounds((Drawable) null, this.b, (Drawable) null, (Drawable) null);
            }
            destroyDrawingCache();
            setWillNotCacheDrawing(false);
            postInvalidate();
        }
    }

    public final Rect g() {
        int i;
        int i2;
        int size = this.a.b.size();
        int left = getLeft() + getPaddingLeft();
        int top = getTop() + getPaddingTop();
        if (size <= 4) {
            Rect a2 = a.a(getContext(), size - 1);
            a2.offset(left, top);
            return a2;
        }
        Rect rect = new Rect();
        if (this.b != null) {
            int centerX = left + this.b.getBounds().centerX();
            int centerY = top + this.b.getBounds().centerY();
            i = centerX;
            i2 = centerY;
        } else {
            int i3 = top;
            i = left;
            i2 = i3;
        }
        rect.set(0, 0, 1, 1);
        rect.offset(i, i2);
        return rect;
    }

    public final void h() {
        if (this.e) {
            a();
            this.e = false;
        }
    }

    public final Drawable i() {
        return this.c;
    }

    public final Drawable j() {
        return this.b;
    }
}
