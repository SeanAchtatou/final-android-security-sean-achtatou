package com.tencent.launcher;

import android.content.DialogInterface;
import android.content.Intent;

final class hw implements DialogInterface.OnClickListener {
    private /* synthetic */ Launcher a;

    hw(Launcher launcher) {
        this.a = launcher;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.getPackageManager().clearPackagePreferredActivities(this.a.getPackageName());
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        intent.addFlags(268435456);
        this.a.startActivity(intent);
        this.a.stopHome();
    }
}
