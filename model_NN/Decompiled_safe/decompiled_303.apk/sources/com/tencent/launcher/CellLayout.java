package com.tencent.launcher;

import android.app.WallpaperManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;
import com.tencent.a.a;
import com.tencent.launcher.base.BaseApp;
import com.tencent.module.theme.l;
import com.tencent.qqlauncher.R;
import java.lang.reflect.Array;

public class CellLayout extends ViewGroup {
    private boolean A;
    /* access modifiers changed from: private */
    public boolean B;
    /* access modifiers changed from: private */
    public gd C;
    /* access modifiers changed from: private */
    public View D;
    private int E;
    private Runnable F;
    private Runnable G;
    /* access modifiers changed from: private */
    public FolderIcon H;
    /* access modifiers changed from: private */
    public Drawable I;
    private boolean J;
    private boolean K;
    private Bitmap L;
    /* access modifiers changed from: private */
    public Drawable M;
    private Rect N;
    private float O;
    private float P;
    private int Q;
    private int R;
    /* access modifiers changed from: private */
    public int S;
    /* access modifiers changed from: private */
    public int T;
    private int U;
    private int V;
    /* access modifiers changed from: private */
    public int W;
    private int X;
    private FolderIcon Y;
    private boolean Z;
    int[] a;
    private long aa;
    private int ab;
    private int ac;
    private int ad;
    private int ae;
    private boolean b;
    private int c;
    private int d;
    private boolean e;
    private int f;
    private int g;
    private int h;
    private int i;
    private int j;
    private int k;
    private int l;
    private int m;
    private final Rect n;
    private final Rect o;
    private final bs p;
    private boolean[][] q;
    private RectF r;
    private boolean s;
    private boolean t;
    private final WallpaperManager u;
    private int[] v;
    private int[] w;
    /* access modifiers changed from: private */
    public View[] x;
    /* access modifiers changed from: private */
    public int y;
    private Animation.AnimationListener z;

    public class LayoutParams extends ViewGroup.MarginLayoutParams {
        public int a;
        public int b;
        public int c;
        public int d;
        public boolean e;
        int f;
        int g;
        boolean h;
        boolean i;
        int j;
        int k;
        boolean l;

        public LayoutParams(int i2, int i3, int i4, int i5) {
            super(-1, -1);
            this.a = i2;
            this.b = i3;
            this.c = i4;
            this.d = i5;
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.c = 1;
            this.d = 1;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.c = 1;
            this.d = 1;
        }

        public final void a(int i2, int i3, int i4, int i5, int i6, int i7) {
            int i8 = this.c;
            int i9 = this.d;
            int i10 = this.a;
            int i11 = this.b;
            this.width = ((((i8 - 1) * i4) + (i8 * i2)) - this.leftMargin) - this.rightMargin;
            this.height = (((i9 * i3) + ((i9 - 1) * i5)) - this.topMargin) - this.bottomMargin;
            this.f = ((i2 + i4) * i10) + i6 + this.leftMargin;
            this.g = ((i3 + i5) * i11) + i7 + this.topMargin;
        }
    }

    public CellLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CellLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.n = new Rect();
        this.o = new Rect();
        this.p = new bs();
        this.a = new int[2];
        this.r = new RectF();
        this.t = false;
        this.v = new int[2];
        this.w = new int[2];
        this.B = true;
        this.N = new Rect();
        this.O = 1.0f;
        this.P = 1.0f;
        this.U = 120;
        this.V = 3;
        this.W = 3;
        this.X = 3;
        this.ab = -1;
        this.ac = -1;
        this.ad = -1;
        this.ae = -1;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.b, i2, 0);
        this.c = obtainStyledAttributes.getDimensionPixelSize(0, 10);
        this.d = obtainStyledAttributes.getDimensionPixelSize(1, 10);
        this.f = obtainStyledAttributes.getDimensionPixelSize(2, 10);
        this.g = obtainStyledAttributes.getDimensionPixelSize(3, 10);
        this.h = obtainStyledAttributes.getDimensionPixelSize(4, 10);
        this.i = obtainStyledAttributes.getDimensionPixelSize(5, 10);
        this.j = obtainStyledAttributes.getInt(6, 4);
        this.k = obtainStyledAttributes.getInt(7, 4);
        obtainStyledAttributes.recycle();
        if (this.q == null) {
            if (this.b) {
                this.q = (boolean[][]) Array.newInstance(Boolean.TYPE, this.j, this.k);
            } else {
                this.q = (boolean[][]) Array.newInstance(Boolean.TYPE, this.k, this.j);
            }
        }
        Resources resources = context.getResources();
        this.I = a.a(context, resources.getDrawable(R.drawable.folder_bg_adding_default), Launcher.sIconBackgroundDrawable, l.a().c(resources), null);
        this.u = WallpaperManager.getInstance(getContext());
        this.x = new View[(this.j * this.k)];
        this.z = new gl(this);
        this.F = new gk(this);
        this.G = new gj(this);
    }

    private static int a(Rect rect, Rect rect2) {
        return (Math.min(rect.right, rect2.right) - Math.max(rect.left, rect2.left)) * (Math.min(rect.bottom, rect2.bottom) - Math.max(rect.top, rect2.top));
    }

    static /* synthetic */ int a(CellLayout cellLayout) {
        int i2 = cellLayout.y;
        cellLayout.y = i2 - 1;
        return i2;
    }

    private void a(int i2, int i3, boolean[][] zArr, View view) {
        for (int i4 = 0; i4 < i2; i4++) {
            for (int i5 = 0; i5 < i3; i5++) {
                zArr[i4][i5] = false;
            }
        }
        int childCount = getChildCount();
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            if (!(childAt instanceof Folder) && !childAt.equals(view)) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                int i7 = layoutParams.a;
                while (i7 < layoutParams.a + layoutParams.c && i7 < i2 && i7 >= 0) {
                    int i8 = layoutParams.b;
                    while (i8 < layoutParams.b + layoutParams.d && i8 < i3 && i8 >= 0) {
                        zArr[i7][i8] = true;
                        i8++;
                    }
                    i7++;
                }
            }
        }
    }

    private static void a(Rect rect, int i2, int i3, boolean[][] zArr, bs bsVar) {
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        bk a2 = bk.a();
        a2.a = rect.left;
        a2.b = rect.top;
        a2.c = (rect.right - rect.left) + 1;
        a2.d = (rect.bottom - rect.top) + 1;
        if (a2.c > bsVar.i) {
            bsVar.i = a2.c;
            bsVar.j = a2.d;
        }
        if (a2.d > bsVar.k) {
            bsVar.k = a2.d;
            bsVar.l = a2.c;
        }
        bsVar.h.add(a2);
        if (rect.left > 0) {
            int i4 = rect.left - 1;
            int i5 = rect.top;
            int i6 = rect.bottom;
            while (true) {
                if (i5 > i6) {
                    z5 = true;
                    break;
                } else if (zArr[i4][i5]) {
                    z5 = false;
                    break;
                } else {
                    i5++;
                }
            }
            if (z5) {
                rect.left--;
                a(rect, i2, i3, zArr, bsVar);
                rect.left++;
            }
        }
        if (rect.right < i2 - 1) {
            int i7 = rect.right + 1;
            int i8 = rect.top;
            int i9 = rect.bottom;
            while (true) {
                if (i8 > i9) {
                    z4 = true;
                    break;
                } else if (zArr[i7][i8]) {
                    z4 = false;
                    break;
                } else {
                    i8++;
                }
            }
            if (z4) {
                rect.right++;
                a(rect, i2, i3, zArr, bsVar);
                rect.right--;
            }
        }
        if (rect.top > 0) {
            int i10 = rect.top - 1;
            int i11 = rect.left;
            int i12 = rect.right;
            while (true) {
                if (i11 > i12) {
                    z3 = true;
                    break;
                } else if (zArr[i11][i10]) {
                    z3 = false;
                    break;
                } else {
                    i11++;
                }
            }
            if (z3) {
                rect.top--;
                a(rect, i2, i3, zArr, bsVar);
                rect.top++;
            }
        }
        if (rect.bottom < i3 - 1) {
            int i13 = rect.bottom + 1;
            int i14 = rect.left;
            int i15 = rect.right;
            while (true) {
                if (i14 > i15) {
                    z2 = true;
                    break;
                } else if (zArr[i14][i13]) {
                    z2 = false;
                    break;
                } else {
                    i14++;
                }
            }
            if (z2) {
                rect.bottom++;
                a(rect, i2, i3, zArr, bsVar);
                rect.bottom--;
            }
        }
    }

    /* access modifiers changed from: private */
    public static void b(bs bsVar, int i2, int i3, int i4, int i5, boolean[][] zArr) {
        bsVar.i = Integer.MIN_VALUE;
        bsVar.j = Integer.MIN_VALUE;
        bsVar.k = Integer.MIN_VALUE;
        bsVar.l = Integer.MIN_VALUE;
        bsVar.a();
        if (!zArr[i2][i3]) {
            bsVar.m.set(i2, i3, i2, i3);
            a(bsVar.m, i4, i5, zArr, bsVar);
        }
    }

    private int d(View view) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        View[] viewArr = this.x;
        for (int i2 = layoutParams.a + ((this.b ? this.j : this.k) * layoutParams.b); i2 >= 0; i2--) {
            if (viewArr[i2] == null) {
                return i2 + 1;
            }
            LayoutParams layoutParams2 = (LayoutParams) viewArr[i2].getLayoutParams();
            if (layoutParams2.c > 1 || layoutParams2.d > 1) {
                return -1;
            }
        }
        return -1;
    }

    private void e(View view) {
        int i2;
        int i3 = this.b ? this.j : this.k;
        int i4 = this.b ? this.k : this.j;
        View[] viewArr = this.x;
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        LayoutParams layoutParams2 = (LayoutParams) view.getLayoutParams();
        View[] viewArr2 = this.x;
        int i5 = this.b ? this.j : this.k;
        int i6 = (i5 * (this.b ? this.k : this.j)) - 1;
        int i7 = layoutParams2.a + (layoutParams2.b * i5);
        while (true) {
            if (i7 > i6) {
                i2 = -1;
                break;
            } else if (viewArr2[i7] == null) {
                i2 = i7 - 1;
                break;
            } else {
                LayoutParams layoutParams3 = (LayoutParams) viewArr2[i7].getLayoutParams();
                if (layoutParams3.c > 1 || layoutParams3.d > 1) {
                    i2 = -1;
                } else {
                    i7++;
                }
            }
        }
        i2 = -1;
        AnimationUtils.currentAnimationTimeMillis();
        if (i2 < 0) {
            int d2 = d(view);
            if (d2 >= 0) {
                for (int i8 = layoutParams.a + (layoutParams.b * i3); i8 >= d2; i8--) {
                    if (this.B) {
                        this.B = false;
                    }
                    LayoutParams layoutParams4 = (LayoutParams) viewArr[i8].getLayoutParams();
                    layoutParams4.b = (layoutParams4.a > 0 || layoutParams4.b <= 0) ? layoutParams4.b : layoutParams4.b - 1;
                    layoutParams4.a = layoutParams4.a <= 0 ? i3 - 1 : layoutParams4.a - 1;
                    layoutParams4.l = true;
                    if (viewArr[i8] instanceof BubbleTextView) {
                        if (i8 == d2) {
                            ((BubbleTextView) viewArr[i8]).a((float) (((((this.c + this.l) * layoutParams4.a) + this.h) + layoutParams4.leftMargin) - layoutParams4.f), (float) (((((this.d + this.m) * layoutParams4.b) + this.f) + layoutParams4.topMargin) - layoutParams4.g), this.z);
                        } else {
                            ((BubbleTextView) viewArr[i8]).a((float) (viewArr[i8 - 1].getLeft() - viewArr[i8].getLeft()), (float) (viewArr[i8 - 1].getTop() - viewArr[i8].getTop()), this.z);
                        }
                    } else if (viewArr[i8] instanceof WidgetContainer) {
                        if (i8 == d2) {
                            ((WidgetContainer) viewArr[i8]).a((float) (((((this.c + this.l) * layoutParams4.a) + this.h) + layoutParams4.leftMargin) - layoutParams4.f), (float) (((((this.d + this.m) * layoutParams4.b) + this.f) + layoutParams4.topMargin) - layoutParams4.g), this.z);
                        } else {
                            ((WidgetContainer) viewArr[i8]).a((float) (viewArr[i8 - 1].getLeft() - viewArr[i8].getLeft()), (float) (viewArr[i8 - 1].getTop() - viewArr[i8].getTop()), this.z);
                        }
                    }
                    this.y++;
                }
                return;
            }
            return;
        }
        for (int i9 = layoutParams.a + (layoutParams.b * i3); i9 <= i2; i9++) {
            if (this.B) {
                this.B = false;
            }
            LayoutParams layoutParams5 = (LayoutParams) viewArr[i9].getLayoutParams();
            layoutParams5.b = (layoutParams5.a < i3 - 1 || layoutParams5.b >= i4 - 1) ? layoutParams5.b : layoutParams5.b + 1;
            layoutParams5.a = layoutParams5.a >= i3 - 1 ? 0 : layoutParams5.a + 1;
            layoutParams5.l = true;
            if (viewArr[i9] instanceof BubbleTextView) {
                if (i9 == i2) {
                    ((BubbleTextView) viewArr[i9]).a((float) (((((this.c + this.l) * layoutParams5.a) + this.h) + layoutParams5.leftMargin) - layoutParams5.f), (float) (((((this.d + this.m) * layoutParams5.b) + this.f) + layoutParams5.topMargin) - layoutParams5.g), this.z);
                } else {
                    ((BubbleTextView) viewArr[i9]).a((float) (viewArr[i9 + 1].getLeft() - viewArr[i9].getLeft()), (float) (viewArr[i9 + 1].getTop() - viewArr[i9].getTop()), this.z);
                }
            } else if (viewArr[i9] instanceof WidgetContainer) {
                if (i9 == i2) {
                    ((WidgetContainer) viewArr[i9]).a((float) (((((this.c + this.l) * layoutParams5.a) + this.h) + layoutParams5.leftMargin) - layoutParams5.f), (float) (((((this.d + this.m) * layoutParams5.b) + this.f) + layoutParams5.topMargin) - layoutParams5.g), this.z);
                } else {
                    ((WidgetContainer) viewArr[i9]).a((float) (viewArr[i9 + 1].getLeft() - viewArr[i9].getLeft()), (float) (viewArr[i9 + 1].getTop() - viewArr[i9].getTop()), this.z);
                }
            }
            this.y++;
        }
    }

    /* access modifiers changed from: package-private */
    public final int a(int i2, int i3, int i4, int i5) {
        int a2;
        int length = this.x.length;
        Rect rect = this.n;
        Rect rect2 = this.o;
        int i6 = this.ab;
        int i7 = this.ac;
        int i8 = this.ad;
        int i9 = this.ae;
        int i10 = this.b ? this.j : this.k;
        rect2.set(0, 0, i8, i9);
        rect2.offset(i2 - i4, i3 - i5);
        int i11 = 0;
        int i12 = -1;
        for (int i13 = 0; i13 < length; i13++) {
            rect.set(i6, i7, i8, i9);
            int[] iArr = this.a;
            a(i13 % i10, i13 / i10, iArr);
            rect.offset(iArr[0], iArr[1]);
            if (Rect.intersects(rect2, rect) && (a2 = a(rect2, rect)) > 0 && a2 > i11) {
                i12 = i13;
                i11 = a2;
            }
        }
        if (i12 != -1) {
            return i12;
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public final int a(int i2, int i3, int i4, int i5, int i6, int[] iArr) {
        int a2;
        int length = this.x.length;
        Rect rect = this.n;
        Rect rect2 = this.o;
        int i7 = this.ab;
        int i8 = this.ac;
        int i9 = this.ad;
        int i10 = this.ae;
        rect2.set(0, 0, i9, i10);
        rect2.offset(i2 - i4, i3 - i5);
        int i11 = this.b ? this.j : this.k;
        int i12 = i6 % i11;
        int i13 = i6 / i11;
        int i14 = 0;
        int i15 = -1;
        int i16 = 0;
        int i17 = -1;
        for (int i18 = 0; i18 < length; i18++) {
            rect.set(i7, i8, i9, i10);
            int[] iArr2 = this.a;
            a(i18 % i11, i18 / i11, iArr2);
            rect.offset(iArr2[0], iArr2[1]);
            if (Rect.intersects(rect2, rect) && (a2 = a(rect2, rect)) > 0) {
                int i19 = i18 / i11;
                if (!(i13 < i19 || (i13 == i19 && i12 < i18 % i11))) {
                    if (Math.min(Math.abs(rect.top - rect2.bottom), Math.abs(rect.bottom - rect2.top)) > (rect.height() >> 1) && rect2.right - rect.left < (rect.width() * 3) / 4) {
                        if (a2 > i16) {
                            i15 = i18;
                            i16 = a2;
                        }
                    } else if (a2 > i14) {
                        i17 = i18;
                        i14 = a2;
                    }
                } else {
                    if (Math.min(Math.abs(rect.top - rect2.bottom), Math.abs(rect.bottom - rect2.top)) > (rect.height() >> 1) && rect.right - rect2.left < (rect.width() * 3) / 4) {
                        if (a2 > i16) {
                            i15 = i18;
                            i16 = a2;
                        }
                    } else if (a2 > i14) {
                        i17 = i18;
                        i14 = a2;
                    }
                }
            }
        }
        if (i15 != -1) {
            iArr[0] = 0;
            return i15;
        } else if (i17 == -1 || i14 <= ((i9 * i10) * 2) / 3) {
            return -1;
        } else {
            iArr[0] = i14;
            return i17;
        }
    }

    /* access modifiers changed from: package-private */
    public final bs a(boolean[] zArr, View view) {
        boolean z2 = this.b;
        int i2 = z2 ? this.j : this.k;
        int i3 = z2 ? this.k : this.j;
        boolean[][] zArr2 = this.q;
        if (zArr != null) {
            for (int i4 = 0; i4 < i3; i4++) {
                for (int i5 = 0; i5 < i2; i5++) {
                    zArr2[i5][i4] = zArr[(i4 * i2) + i5];
                }
            }
        } else {
            a(i2, i3, zArr2, view);
        }
        return a(zArr2, i2, i3);
    }

    /* access modifiers changed from: package-private */
    public final bs a(boolean[][] zArr, int i2, int i3) {
        bs bsVar = new bs();
        bsVar.b = -1;
        bsVar.c = -1;
        bsVar.e = 0;
        bsVar.d = 0;
        bsVar.i = Integer.MIN_VALUE;
        bsVar.j = Integer.MIN_VALUE;
        bsVar.k = Integer.MIN_VALUE;
        bsVar.l = Integer.MIN_VALUE;
        bsVar.f = this.p.f;
        Rect rect = bsVar.m;
        for (int i4 = 0; i4 < i2; i4++) {
            for (int i5 = 0; i5 < i3; i5++) {
                if (!zArr[i4][i5]) {
                    rect.set(i4, i5, i4, i5);
                    a(rect, i2, i3, zArr, bsVar);
                    zArr[i4][i5] = true;
                }
            }
        }
        bsVar.g = bsVar.h.size() > 0;
        return bsVar;
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2, int i3, int[] iArr) {
        boolean z2 = this.b;
        int i4 = z2 ? this.h : this.f;
        int i5 = z2 ? this.f : this.h;
        iArr[0] = i4 + ((this.c + this.l) * i2);
        iArr[1] = i5 + ((this.d + this.m) * i3);
    }

    /* access modifiers changed from: package-private */
    public final void a(View view) {
        if (view != null) {
            ((LayoutParams) view.getLayoutParams()).e = false;
            invalidate();
        }
        this.r.setEmpty();
    }

    /* access modifiers changed from: package-private */
    public final void a(View view, int[] iArr) {
        if (view != null) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            layoutParams.a = iArr[0];
            layoutParams.b = iArr[1];
            layoutParams.e = false;
            layoutParams.i = true;
            this.r.setEmpty();
            view.requestLayout();
            invalidate();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z2, int i2) {
        if (this.e != z2) {
            if (z2) {
                if (this.b) {
                    this.f += i2;
                } else {
                    this.h += i2;
                }
            } else if (this.b) {
                this.f -= i2;
            } else {
                this.h -= i2;
            }
            this.e = z2;
        }
    }

    public final boolean a() {
        int i2 = 0;
        for (View view : this.x) {
            if (view != null) {
                i2++;
            }
        }
        return i2 >= this.x.length;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(int i2) {
        if (!this.J && this.B) {
            this.D = this.x[i2];
            if (this.D == null) {
                return false;
            }
            if (i2 < 0) {
                return false;
            }
            this.E = i2;
            postDelayed(this.F, 200);
            this.J = true;
            this.V = 1;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(int i2, ha haVar, View view, cm cmVar, int i3, int i4, Bitmap bitmap) {
        boolean z2;
        am amVar;
        int i5;
        Bitmap bitmap2;
        if (this.J) {
            if (this.V == 1) {
                removeCallbacks(this.F);
            } else {
                j();
            }
        }
        if (this.K) {
            if (this.W != 1) {
                View view2 = this.x[i2];
                if (view2 != null && (haVar.m == 0 || haVar.m == 1)) {
                    m();
                    k();
                    ha haVar2 = (ha) view2.getTag();
                    switch (haVar2.m) {
                        case 0:
                        case 1:
                            boolean z3 = false;
                            if ((haVar2 instanceof am) && (haVar instanceof am)) {
                                ComponentName component = ((am) haVar2).c.getComponent();
                                ComponentName component2 = ((am) haVar).c.getComponent();
                                if (component != null && component.equals(component2)) {
                                    z3 = true;
                                }
                            }
                            if (z3) {
                                z2 = false;
                                BaseApp.a(getContext().getResources().getText(R.string.same_item_no_create_folder));
                                break;
                            } else {
                                if ((cmVar instanceof AllAppsListView) || haVar2.n == -1 || haVar2.n == -300 || ((cmVar instanceof UserFolder) && ((UserFolder) cmVar).f.n == -300)) {
                                    ((Workspace) getParent()).b(view2);
                                } else {
                                    ((Workspace) getParent()).b(view2);
                                }
                                z2 = true;
                                break;
                            }
                            break;
                        case 2:
                            bq bqVar = (bq) haVar2;
                            if (bqVar.b((am) haVar) && (!(cmVar instanceof UserFolder) || ((UserFolder) cmVar).f != bqVar)) {
                                Toast.makeText(getContext(), (int) R.string.folder_contain_item, 0).show();
                                z2 = false;
                                break;
                            } else {
                                if ((cmVar instanceof AllAppsListView) || haVar2.n == -1 || haVar2.n == -300 || ((cmVar instanceof UserFolder) && ((UserFolder) cmVar).f.n == -300)) {
                                    amVar = new am((am) haVar);
                                    am amVar2 = amVar;
                                    amVar2.l = -1;
                                    amVar2.n = -1;
                                    amVar2.f = false;
                                } else {
                                    amVar = haVar;
                                }
                                if (bqVar.a(amVar)) {
                                    int width = view2.getWidth();
                                    view2.getHeight();
                                    if (view != null) {
                                        int width2 = view.getWidth();
                                        view.getHeight();
                                        if (view.getParent() != null) {
                                            ((ViewGroup) view.getParent()).removeView(view);
                                        }
                                        ff.a(getContext(), (ha) view.getTag(), haVar2.l, 0, 0, 0);
                                        Launcher.getModel().b((ha) view.getTag());
                                        if (view == null) {
                                            bitmap2 = null;
                                        } else {
                                            boolean willNotCacheDrawing = view.willNotCacheDrawing();
                                            view.setWillNotCacheDrawing(false);
                                            int drawingCacheBackgroundColor = view.getDrawingCacheBackgroundColor();
                                            view.setDrawingCacheBackgroundColor(0);
                                            if (drawingCacheBackgroundColor != 0) {
                                                view.destroyDrawingCache();
                                            }
                                            view.buildDrawingCache();
                                            Bitmap drawingCache = view.getDrawingCache();
                                            if (drawingCache != null) {
                                                drawingCache = Bitmap.createBitmap(drawingCache);
                                            }
                                            view.setWillNotCacheDrawing(willNotCacheDrawing);
                                            bitmap2 = drawingCache;
                                        }
                                        this.L = bitmap2;
                                        i5 = width2;
                                    } else {
                                        amVar.c.setAction("android.intent.action.MAIN");
                                        if (cmVar instanceof QGridLayout) {
                                            amVar.n = -300;
                                        } else if (!(cmVar instanceof DockBar) && (cmVar instanceof UserFolder)) {
                                            bq bqVar2 = (bq) ((UserFolder) cmVar).f;
                                            if (bqVar2.n != -200 && bqVar2.n == -300) {
                                                amVar.n = -300;
                                            }
                                        }
                                        ff.a(getContext(), amVar, haVar2.l, 0, 0, 0);
                                        try {
                                            this.L = Bitmap.createBitmap(bitmap);
                                            i5 = width;
                                        } catch (OutOfMemoryError e2) {
                                            this.L = bitmap;
                                            i5 = width;
                                        }
                                    }
                                    FolderIcon folderIcon = (FolderIcon) view2;
                                    Rect g2 = folderIcon.g();
                                    LayoutParams layoutParams = (LayoutParams) folderIcon.getLayoutParams();
                                    int[] iArr = this.a;
                                    Rect rect = this.N;
                                    a(layoutParams.a, layoutParams.b, iArr);
                                    rect.set(0, 0, this.c, this.d);
                                    rect.offset(iArr[0], iArr[1]);
                                    this.O = 1.0f;
                                    this.P = ((float) g2.width()) / ((float) i5);
                                    this.S = i3;
                                    this.T = i4;
                                    this.Q = g2.centerX();
                                    this.R = g2.centerY();
                                    this.X = 1;
                                    this.Y = folderIcon;
                                    this.Z = true;
                                    invalidate();
                                    z2 = true;
                                    break;
                                } else {
                                    Toast.makeText(getContext(), (int) R.string.folder_is_full, 0).show();
                                }
                            }
                            break;
                        default:
                            z2 = false;
                            break;
                    }
                }
            } else {
                m();
                k();
                z2 = false;
            }
            m();
            return z2;
        }
        z2 = false;
        m();
        return z2;
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        ((LayoutParams) layoutParams).h = true;
        super.addView(view, i2, layoutParams);
    }

    /* access modifiers changed from: package-private */
    public final int b() {
        return this.b ? this.f : this.h;
    }

    /* access modifiers changed from: package-private */
    public final int b(View view) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        layoutParams.e = true;
        this.r.setEmpty();
        this.w[0] = layoutParams.a;
        this.w[1] = layoutParams.b;
        return layoutParams.a + ((this.b ? this.j : this.k) * layoutParams.b);
    }

    /* access modifiers changed from: package-private */
    public final boolean b(int i2) {
        if (!this.K) {
            this.D = this.x[i2];
            if (this.D == null || (((ha) this.D.getTag()).m != 0 && ((ha) this.D.getTag()).m != 1 && ((ha) this.D.getTag()).m != 2)) {
                return false;
            }
            this.E = i2;
            postDelayed(this.G, 300);
            this.K = true;
            this.W = 1;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public final int c() {
        return this.b ? this.g : this.i;
    }

    /* access modifiers changed from: package-private */
    public final View c(int i2) {
        if (i2 < 0 || i2 >= this.x.length) {
            return null;
        }
        return this.x[i2];
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x007c, code lost:
        if (r1 == false) goto L_0x008e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c(android.view.View r15) {
        /*
            r14 = this;
            r13 = 0
            r12 = 1
            android.view.ViewParent r1 = r14.getParent()
            com.tencent.launcher.Workspace r1 = (com.tencent.launcher.Workspace) r1
            int r2 = r1.j()
            android.view.ViewParent r1 = r14.getParent()
            com.tencent.launcher.Workspace r1 = (com.tencent.launcher.Workspace) r1
            int r3 = r14.E
            r1.c(r3)
            boolean r1 = r14.b
            if (r1 == 0) goto L_0x0045
            int r1 = r14.j
            r3 = r1
        L_0x001e:
            boolean r1 = r14.b
            if (r1 == 0) goto L_0x0049
            int r1 = r14.k
            r4 = r1
        L_0x0025:
            android.view.ViewGroup$LayoutParams r1 = r15.getLayoutParams()
            com.tencent.launcher.CellLayout$LayoutParams r1 = (com.tencent.launcher.CellLayout.LayoutParams) r1
            int[] r5 = r14.w
            int r6 = r1.a
            r5[r13] = r6
            int[] r5 = r14.w
            int r6 = r1.b
            r5[r12] = r6
            int r5 = r1.b
            int r5 = r5 * r3
            int r6 = r1.a
            int r5 = r5 + r6
            if (r2 <= r5) goto L_0x004d
            r14.e(r15)
        L_0x0042:
            r14.A = r12
            return
        L_0x0045:
            int r1 = r14.k
            r3 = r1
            goto L_0x001e
        L_0x0049:
            int r1 = r14.j
            r4 = r1
            goto L_0x0025
        L_0x004d:
            if (r2 < r5) goto L_0x008e
            int r2 = r1.j
            int r5 = r1.a
            if (r2 != r5) goto L_0x0082
            int r2 = r1.k
            int r5 = r1.b
            if (r2 != r5) goto L_0x0082
            r2 = r12
        L_0x005c:
            if (r2 != 0) goto L_0x007e
            int r2 = r1.k
            int r5 = r1.a
            int r6 = r3 - r12
            if (r5 < r6) goto L_0x0084
            int r5 = r1.b
            int r4 = r4 - r12
            if (r5 >= r4) goto L_0x0084
            int r4 = r1.b
            int r4 = r4 + 1
        L_0x006f:
            if (r2 != r4) goto L_0x008c
            int r2 = r1.j
            int r4 = r1.a
            int r3 = r3 - r12
            if (r4 < r3) goto L_0x0087
            r1 = r13
        L_0x0079:
            if (r2 != r1) goto L_0x008c
            r1 = r12
        L_0x007c:
            if (r1 == 0) goto L_0x008e
        L_0x007e:
            r14.e(r15)
            goto L_0x0042
        L_0x0082:
            r2 = r13
            goto L_0x005c
        L_0x0084:
            int r4 = r1.b
            goto L_0x006f
        L_0x0087:
            int r1 = r1.a
            int r1 = r1 + 1
            goto L_0x0079
        L_0x008c:
            r1 = r13
            goto L_0x007c
        L_0x008e:
            boolean r1 = r14.b
            if (r1 == 0) goto L_0x0116
            int r1 = r14.j
            r3 = r1
        L_0x0095:
            android.view.ViewGroup$LayoutParams r1 = r15.getLayoutParams()
            com.tencent.launcher.CellLayout$LayoutParams r1 = (com.tencent.launcher.CellLayout.LayoutParams) r1
            android.view.View[] r4 = r14.x
            int r5 = r14.d(r15)
            int r2 = r1.b
            int r2 = r2 * r3
            int r1 = r1.a
            int r1 = r1 + r2
            if (r5 < 0) goto L_0x0042
            android.view.animation.AnimationUtils.currentAnimationTimeMillis()
            r6 = r1
        L_0x00ad:
            if (r6 < r5) goto L_0x0042
            boolean r1 = r14.B
            if (r1 == 0) goto L_0x00b5
            r14.B = r13
        L_0x00b5:
            r1 = r4[r6]
            android.view.ViewGroup$LayoutParams r1 = r1.getLayoutParams()
            r0 = r1
            com.tencent.launcher.CellLayout$LayoutParams r0 = (com.tencent.launcher.CellLayout.LayoutParams) r0
            r2 = r0
            int r1 = r2.a
            if (r1 > 0) goto L_0x011b
            int r1 = r2.b
            if (r1 <= 0) goto L_0x011b
            int r1 = r2.b
            int r1 = r1 - r12
        L_0x00ca:
            r2.b = r1
            int r1 = r2.a
            if (r1 > 0) goto L_0x011e
            int r1 = r3 - r12
        L_0x00d2:
            r2.a = r1
            r2.l = r12
            r1 = r4[r6]
            boolean r1 = r1 instanceof com.tencent.launcher.BubbleTextView
            if (r1 == 0) goto L_0x014c
            if (r6 != r5) goto L_0x0122
            r1 = r4[r6]
            com.tencent.launcher.BubbleTextView r1 = (com.tencent.launcher.BubbleTextView) r1
            int r7 = r14.c
            int r8 = r14.l
            int r9 = r14.h
            int r10 = r2.a
            int r7 = r7 + r8
            int r7 = r7 * r10
            int r7 = r7 + r9
            int r8 = r2.leftMargin
            int r7 = r7 + r8
            int r8 = r2.f
            int r7 = r7 - r8
            float r7 = (float) r7
            int r8 = r14.d
            int r9 = r14.m
            int r10 = r14.f
            int r11 = r2.b
            int r8 = r8 + r9
            int r8 = r8 * r11
            int r8 = r8 + r10
            int r9 = r2.topMargin
            int r8 = r8 + r9
            int r2 = r2.g
            int r2 = r8 - r2
            float r2 = (float) r2
            android.view.animation.Animation$AnimationListener r8 = r14.z
            r1.a(r7, r2, r8)
        L_0x010c:
            int r1 = r14.y
            int r1 = r1 + 1
            r14.y = r1
            int r1 = r6 + -1
            r6 = r1
            goto L_0x00ad
        L_0x0116:
            int r1 = r14.k
            r3 = r1
            goto L_0x0095
        L_0x011b:
            int r1 = r2.b
            goto L_0x00ca
        L_0x011e:
            int r1 = r2.a
            int r1 = r1 - r12
            goto L_0x00d2
        L_0x0122:
            r1 = r4[r6]
            com.tencent.launcher.BubbleTextView r1 = (com.tencent.launcher.BubbleTextView) r1
            int r2 = r6 - r12
            r2 = r4[r2]
            int r2 = r2.getLeft()
            r7 = r4[r6]
            int r7 = r7.getLeft()
            int r2 = r2 - r7
            float r2 = (float) r2
            int r7 = r6 - r12
            r7 = r4[r7]
            int r7 = r7.getTop()
            r8 = r4[r6]
            int r8 = r8.getTop()
            int r7 = r7 - r8
            float r7 = (float) r7
            android.view.animation.Animation$AnimationListener r8 = r14.z
            r1.a(r2, r7, r8)
            goto L_0x010c
        L_0x014c:
            r1 = r4[r6]
            boolean r1 = r1 instanceof com.tencent.launcher.WidgetContainer
            if (r1 == 0) goto L_0x010c
            if (r6 != r5) goto L_0x0183
            r1 = r4[r6]
            com.tencent.launcher.WidgetContainer r1 = (com.tencent.launcher.WidgetContainer) r1
            int r7 = r14.c
            int r8 = r14.l
            int r9 = r14.h
            int r10 = r2.a
            int r7 = r7 + r8
            int r7 = r7 * r10
            int r7 = r7 + r9
            int r8 = r2.leftMargin
            int r7 = r7 + r8
            int r8 = r2.f
            int r7 = r7 - r8
            float r7 = (float) r7
            int r8 = r14.d
            int r9 = r14.m
            int r10 = r14.f
            int r11 = r2.b
            int r8 = r8 + r9
            int r8 = r8 * r11
            int r8 = r8 + r10
            int r9 = r2.topMargin
            int r8 = r8 + r9
            int r2 = r2.g
            int r2 = r8 - r2
            float r2 = (float) r2
            android.view.animation.Animation$AnimationListener r8 = r14.z
            r1.a(r7, r2, r8)
            goto L_0x010c
        L_0x0183:
            r1 = r4[r6]
            com.tencent.launcher.WidgetContainer r1 = (com.tencent.launcher.WidgetContainer) r1
            int r2 = r6 - r12
            r2 = r4[r2]
            int r2 = r2.getLeft()
            r7 = r4[r6]
            int r7 = r7.getLeft()
            int r2 = r2 - r7
            float r2 = (float) r2
            int r7 = r6 - r12
            r7 = r4[r7]
            int r7 = r7.getTop()
            r8 = r4[r6]
            int r8 = r8.getTop()
            int r7 = r7 - r8
            float r7 = (float) r7
            android.view.animation.Animation$AnimationListener r8 = r14.z
            r1.a(r2, r7, r8)
            goto L_0x010c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.launcher.CellLayout.c(android.view.View):void");
    }

    public void cancelLongPress() {
        super.cancelLongPress();
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            getChildAt(i2).cancelLongPress();
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    /* access modifiers changed from: package-private */
    public final int d() {
        return this.b ? this.j : this.k;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        Bitmap bitmap = this.L;
        if (bitmap != null && !bitmap.isRecycled()) {
            if (this.X == 1) {
                this.aa = SystemClock.uptimeMillis();
                this.X = 2;
            }
            if (this.X == 2) {
                float uptimeMillis = ((float) (SystemClock.uptimeMillis() - this.aa)) / ((float) this.U);
                if (uptimeMillis >= 1.0f) {
                    this.X = 3;
                }
                float min = Math.min(uptimeMillis, 1.0f);
                float f2 = (min * (this.P - this.O)) + this.O;
                canvas.save();
                canvas.scale(f2, f2, (float) this.Q, (float) this.R);
                canvas.drawBitmap(bitmap, (float) this.S, (float) this.T, (Paint) null);
                canvas.restore();
                invalidate(this.N);
            } else {
                if (this.Z && this.Y != null) {
                    this.Y.a();
                    this.Y = null;
                }
                bitmap.recycle();
                this.L = null;
                invalidate();
            }
        } else if (this.X != 3) {
            this.X = 3;
        }
        Drawable drawable = this.M;
        if (drawable != null) {
            canvas.save();
            canvas.translate((float) this.S, (float) this.T);
            drawable.draw(canvas);
            canvas.restore();
        }
    }

    /* access modifiers changed from: protected */
    public void dispatchSetPressed(boolean z2) {
        if (!z2) {
            super.dispatchSetPressed(z2);
        }
    }

    /* access modifiers changed from: package-private */
    public final int e() {
        return this.b ? this.k : this.j;
    }

    /* renamed from: f */
    public final bs getTag() {
        bs bsVar = (bs) super.getTag();
        if (this.s && bsVar.g) {
            boolean z2 = this.b;
            int i2 = z2 ? this.j : this.k;
            int i3 = z2 ? this.k : this.j;
            boolean[][] zArr = this.q;
            a(i2, i3, zArr, (View) null);
            b(bsVar, bsVar.b, bsVar.c, i2, i3, zArr);
            this.s = false;
        }
        return bsVar;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
    }

    /* access modifiers changed from: package-private */
    public final void g() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            LayoutParams layoutParams = (LayoutParams) getChildAt(i2).getLayoutParams();
            layoutParams.j = layoutParams.a;
            layoutParams.k = layoutParams.b;
        }
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new LayoutParams(layoutParams);
    }

    /* access modifiers changed from: package-private */
    public final boolean h() {
        return this.A;
    }

    /* access modifiers changed from: package-private */
    public final void i() {
        if (this.A) {
            int childCount = getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                LayoutParams layoutParams = (LayoutParams) getChildAt(i2).getLayoutParams();
                layoutParams.a = layoutParams.j;
                layoutParams.b = layoutParams.k;
            }
            this.B = true;
            if (this.C != null) {
                this.C.a();
            }
            n();
            requestLayout();
        }
    }

    public void invalidate() {
        boolean z2 = this.b;
        super.invalidate(getScrollX(), z2 ? this.f : this.h, getScrollX() + getWidth(), getHeight() - (z2 ? this.g : this.i));
    }

    /* access modifiers changed from: package-private */
    public final void j() {
        if (this.A) {
            if (!this.B) {
                this.C = new gi(this);
                return;
            }
            int childCount = getChildCount();
            AnimationUtils.currentAnimationTimeMillis();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = getChildAt(i2);
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (!(layoutParams.j == layoutParams.a && layoutParams.k == layoutParams.b)) {
                    if (this.B) {
                        this.B = false;
                    }
                    layoutParams.a = layoutParams.j;
                    layoutParams.b = layoutParams.k;
                    layoutParams.l = true;
                    if (childAt instanceof BubbleTextView) {
                        ((BubbleTextView) childAt).a((float) (((((this.c + this.l) * layoutParams.a) + this.h) + layoutParams.leftMargin) - layoutParams.f), (float) (((((this.d + this.m) * layoutParams.b) + this.f) + layoutParams.topMargin) - layoutParams.g), this.z);
                    } else if (childAt instanceof WidgetContainer) {
                        ((WidgetContainer) childAt).a((float) (((((this.c + this.l) * layoutParams.a) + this.h) + layoutParams.leftMargin) - layoutParams.f), (float) (((((this.d + this.m) * layoutParams.b) + this.f) + layoutParams.topMargin) - layoutParams.g), this.z);
                    }
                    this.y++;
                }
            }
            this.A = false;
        }
    }

    /* access modifiers changed from: package-private */
    public final void k() {
        if (this.J) {
            removeCallbacks(this.F);
            this.J = false;
            if (this.V != 2) {
                this.V = 3;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean l() {
        return this.K;
    }

    /* access modifiers changed from: package-private */
    public final void m() {
        if (this.K) {
            if (this.H != null) {
                this.H.e();
            }
            removeCallbacks(this.G);
            this.K = false;
            this.M = null;
            if (this.W != 2) {
                this.W = 3;
            }
            invalidate();
        }
    }

    /* access modifiers changed from: package-private */
    public final void n() {
        View[] viewArr = this.x;
        int childCount = getChildCount();
        boolean z2 = this.b;
        int i2 = z2 ? this.j : this.k;
        int i3 = z2 ? this.k : this.j;
        for (int i4 = 0; i4 < viewArr.length; i4++) {
            viewArr[i4] = null;
        }
        View a2 = ((Workspace) getParent()).a(this);
        for (int i5 = 0; i5 < childCount; i5++) {
            View childAt = getChildAt(i5);
            if (childAt != a2) {
                LayoutParams layoutParams = (LayoutParams) getChildAt(i5).getLayoutParams();
                int i6 = layoutParams.a;
                while (i6 <= (layoutParams.a + layoutParams.c) - 1 && i6 < i2) {
                    int i7 = layoutParams.b;
                    while (i7 <= (layoutParams.b + layoutParams.d) - 1 && i7 < i3) {
                        viewArr[(i2 * i7) + i6] = childAt;
                        i7++;
                    }
                    i6++;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean[] o() {
        boolean z2 = this.b;
        int i2 = z2 ? this.j : this.k;
        int i3 = z2 ? this.k : this.j;
        boolean[][] zArr = this.q;
        a(i2, i3, zArr, (View) null);
        boolean[] zArr2 = new boolean[(i2 * i3)];
        for (int i4 = 0; i4 < i3; i4++) {
            for (int i5 = 0; i5 < i2; i5++) {
                zArr2[(i4 * i2) + i5] = zArr[i5][i4];
            }
        }
        return zArr2;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.p.f = ((ViewGroup) getParent()).indexOfChild(this);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z2;
        int action = motionEvent.getAction();
        bs bsVar = this.p;
        if (action == 0) {
            Rect rect = this.n;
            int x2 = ((int) motionEvent.getX()) + getScrollX();
            int y2 = ((int) motionEvent.getY()) + getScrollY();
            int childCount = getChildCount() - 1;
            while (true) {
                if (childCount < 0) {
                    z2 = false;
                    break;
                }
                View childAt = getChildAt(childCount);
                if (childAt.getVisibility() == 0 || childAt.getAnimation() != null) {
                    childAt.getHitRect(rect);
                    if (rect.contains(x2, y2)) {
                        LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                        bsVar.a = childAt;
                        bsVar.b = layoutParams.a;
                        bsVar.c = layoutParams.b;
                        bsVar.d = layoutParams.c;
                        bsVar.e = layoutParams.d;
                        bsVar.g = true;
                        this.s = false;
                        z2 = true;
                        break;
                    }
                }
                childCount--;
            }
            this.t = z2;
            if (!z2) {
                int[] iArr = this.a;
                boolean z3 = this.b;
                int i2 = z3 ? this.h : this.f;
                int i3 = z3 ? this.f : this.h;
                iArr[0] = (x2 - i2) / (this.c + this.l);
                iArr[1] = (y2 - i3) / (this.d + this.m);
                int i4 = z3 ? this.j : this.k;
                int i5 = z3 ? this.k : this.j;
                if (iArr[0] < 0) {
                    iArr[0] = 0;
                }
                if (iArr[0] >= i4) {
                    iArr[0] = i4 - 1;
                }
                if (iArr[1] < 0) {
                    iArr[1] = 0;
                }
                if (iArr[1] >= i5) {
                    iArr[1] = i5 - 1;
                }
                boolean z4 = this.b;
                int i6 = z4 ? this.j : this.k;
                int i7 = z4 ? this.k : this.j;
                boolean[][] zArr = this.q;
                a(i6, i7, zArr, (View) null);
                bsVar.a = null;
                bsVar.b = iArr[0];
                bsVar.c = iArr[1];
                bsVar.d = 1;
                bsVar.e = 1;
                bsVar.g = iArr[0] >= 0 && iArr[1] >= 0 && iArr[0] < i6 && iArr[1] < i7 && !zArr[iArr[0]][iArr[1]];
                this.s = true;
            }
            setTag(bsVar);
        } else if (action == 1) {
            bsVar.a = null;
            bsVar.b = -1;
            bsVar.c = -1;
            bsVar.d = 0;
            bsVar.e = 0;
            bsVar.g = false;
            this.s = false;
            setTag(bsVar);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        if (!this.B) {
            View b2 = ((Workspace) getParent()).b(this);
            if (b2 != null && indexOfChild(b2) != -1) {
                LayoutParams layoutParams = (LayoutParams) b2.getLayoutParams();
                int i6 = layoutParams.f;
                int i7 = layoutParams.g;
                b2.layout(i6, i7, layoutParams.width + i6, layoutParams.height + i7);
                return;
            }
            return;
        }
        int childCount = getChildCount();
        for (int i8 = 0; i8 < childCount; i8++) {
            View childAt = getChildAt(i8);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams2 = (LayoutParams) childAt.getLayoutParams();
                int i9 = layoutParams2.f;
                int i10 = layoutParams2.g;
                childAt.layout(i9, i10, layoutParams2.width + i9, layoutParams2.height + i10);
                if (this.ab < 0 && layoutParams2.c <= 1 && layoutParams2.d <= 1) {
                    this.ab = layoutParams2.leftMargin;
                    this.ac = layoutParams2.topMargin;
                    this.ad = layoutParams2.width;
                    this.ae = layoutParams2.height;
                }
                if (layoutParams2.i) {
                    layoutParams2.i = false;
                    int[] iArr = this.a;
                    getLocationOnScreen(iArr);
                    this.u.sendWallpaperCommand(getWindowToken(), "android.home.drop", i9 + iArr[0] + (layoutParams2.width / 2), i10 + iArr[1] + (layoutParams2.height / 2), 0, null);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        if (mode == 0 || mode2 == 0) {
            throw new RuntimeException("CellLayout cannot have UNSPECIFIED dimensions");
        }
        int i4 = this.j;
        int i5 = this.k;
        int i6 = this.f;
        int i7 = this.g;
        int i8 = this.h;
        int i9 = this.i;
        int i10 = this.c;
        int i11 = this.d;
        this.b = size2 > size;
        int i12 = i4 - 1;
        int i13 = i5 - 1;
        if (this.b) {
            this.m = (((size2 - i6) - i7) - (i5 * i11)) / i13;
            int i14 = ((size - i8) - i9) - (i4 * i10);
            if (i12 > 0) {
                this.l = i14 / i12;
            } else {
                this.l = 0;
            }
        } else {
            this.l = (((size - i6) - i7) - (i5 * i10)) / i13;
            int i15 = ((size2 - i8) - i9) - (i4 * i11);
            if (i12 > 0) {
                this.m = i15 / i12;
            } else {
                this.m = 0;
            }
        }
        int childCount = getChildCount();
        int i16 = 0;
        while (true) {
            int i17 = i16;
            if (i17 < childCount) {
                View childAt = getChildAt(i17);
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (this.b) {
                    layoutParams.a(i10, i11, this.l, this.m, i8, i6);
                } else {
                    layoutParams.a(i10, i11, this.l, this.m, i6, i8);
                }
                if (layoutParams.h) {
                    childAt.setId(((getId() & BrightnessActivity.MAXIMUM_BACKLIGHT) << 16) | ((layoutParams.a & BrightnessActivity.MAXIMUM_BACKLIGHT) << 8) | (layoutParams.b & BrightnessActivity.MAXIMUM_BACKLIGHT));
                    layoutParams.h = false;
                }
                childAt.measure(View.MeasureSpec.makeMeasureSpec(layoutParams.width, 1073741824), View.MeasureSpec.makeMeasureSpec(layoutParams.height, 1073741824));
                i16 = i17 + 1;
            } else {
                setMeasuredDimension(size, size2);
                return;
            }
        }
    }

    public final boolean p() {
        return this.t;
    }

    public void requestChildFocus(View view, View view2) {
        super.requestChildFocus(view, view2);
        if (view != null) {
            Rect rect = new Rect();
            view.getDrawingRect(rect);
            requestRectangleOnScreen(rect);
        }
    }

    /* access modifiers changed from: protected */
    public void setChildrenDrawingCacheEnabled(boolean z2) {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            childAt.setDrawingCacheEnabled(z2);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            if (layoutParams.c > 1 || layoutParams.d > 1) {
                childAt.destroyDrawingCache();
            }
            childAt.buildDrawingCache();
        }
    }

    /* access modifiers changed from: protected */
    public void setChildrenDrawnWithCacheEnabled(boolean z2) {
        super.setChildrenDrawnWithCacheEnabled(z2);
    }
}
