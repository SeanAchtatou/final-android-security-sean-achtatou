package com.tencent.launcher;

import android.view.View;
import android.widget.TextView;

final class cb implements View.OnClickListener {
    private /* synthetic */ MarketSearchAppActivity a;

    cb(MarketSearchAppActivity marketSearchAppActivity) {
        this.a = marketSearchAppActivity;
    }

    public final void onClick(View view) {
        this.a.mEditText.setText(((TextView) view).getText());
        this.a.searchKeyWord();
    }
}
