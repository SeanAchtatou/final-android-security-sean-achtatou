package com.tencent.launcher;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.widget.GridView;
import java.util.HashMap;

public class GridListView extends GridView {
    public static boolean a = false;
    private static int b = 250;
    /* access modifiers changed from: private */
    public ae c;
    /* access modifiers changed from: private */
    public int d = 99;
    /* access modifiers changed from: private */
    public int e = 99;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public int g;
    /* access modifiers changed from: private */
    public HashMap h = new HashMap();
    private Handler i = new Handler();
    private long j = 0;
    private long k = 99;
    private long l;
    private Animation.AnimationListener m = new fz(this);

    public GridListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Launcher launcher = Launcher.getLauncher();
        if (launcher != null) {
            launcher.clearStatusBarTapScrollTopViews();
        }
    }

    public GridListView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        Launcher launcher = Launcher.getLauncher();
        if (launcher != null) {
            launcher.clearStatusBarTapScrollTopViews();
        }
    }

    static /* synthetic */ int a(GridListView gridListView) {
        int i2 = gridListView.g;
        gridListView.g = i2 - 1;
        return i2;
    }

    public static boolean a() {
        return a;
    }

    private static boolean b(View view, int i2) {
        if (view.getTag() == null || !(view.getTag() instanceof Integer)) {
            return false;
        }
        return ((Integer) view.getTag()).intValue() == i2;
    }

    public final void a(int i2) {
        this.d = i2;
        this.e = i2;
        a = true;
    }

    public final void a(int i2, int i3) {
        int i4;
        if (a) {
            int childCount = getChildCount();
            if (childCount == 0) {
                i4 = -1;
            } else {
                int height = ((i3 / (getHeight() / (((childCount - 1) / 4) + 1))) * 4) + (i2 / (getWidth() / 4));
                i4 = height >= childCount ? -1 : height;
            }
            if (i4 == -1) {
                i4 = (getFirstVisiblePosition() + getChildCount()) - 1;
            }
            if (i4 != this.d && !this.f) {
                if (this.k != ((long) i4)) {
                    this.k = (long) i4;
                    this.l = System.currentTimeMillis();
                    return;
                }
                this.j += System.currentTimeMillis() - this.l;
                if (this.j < 500) {
                    this.l = System.currentTimeMillis();
                    return;
                }
                this.e = i4;
                int firstVisiblePosition = this.d - getFirstVisiblePosition();
                int firstVisiblePosition2 = i4 - getFirstVisiblePosition();
                ga gaVar = new ga(this);
                this.g = Math.abs(i4 - this.d);
                if (firstVisiblePosition > firstVisiblePosition2) {
                    for (int i5 = firstVisiblePosition2; i5 < firstVisiblePosition; i5++) {
                        View childAt = getChildAt(i5);
                        View childAt2 = getChildAt(i5 + 1);
                        if (!b(childAt, i5)) {
                            this.g--;
                        } else {
                            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, (float) (childAt2.getLeft() - childAt.getLeft()), 0.0f, (float) (childAt2.getTop() - childAt.getTop()));
                            translateAnimation.setDuration((long) b);
                            translateAnimation.setFillAfter(true);
                            translateAnimation.setInterpolator(gaVar);
                            translateAnimation.setAnimationListener(this.m);
                            childAt.startAnimation(translateAnimation);
                            this.h.put("" + i5, translateAnimation);
                        }
                    }
                } else {
                    for (int i6 = firstVisiblePosition + 1; i6 <= firstVisiblePosition2; i6++) {
                        View childAt3 = getChildAt(i6);
                        View childAt4 = getChildAt(i6 - 1);
                        if (!b(childAt3, i6)) {
                            this.g--;
                        } else {
                            TranslateAnimation translateAnimation2 = new TranslateAnimation(0.0f, (float) (childAt4.getLeft() - childAt3.getLeft()), 0.0f, (float) (childAt4.getTop() - childAt3.getTop()));
                            translateAnimation2.setDuration((long) b);
                            translateAnimation2.setFillAfter(true);
                            translateAnimation2.setInterpolator(gaVar);
                            translateAnimation2.setAnimationListener(this.m);
                            childAt3.startAnimation(translateAnimation2);
                            this.h.put("" + i6, translateAnimation2);
                        }
                    }
                }
                this.f = true;
            }
        }
    }

    public final void a(View view, int i2) {
        if (this.h.size() != 0 && view != null) {
            Animation animation = (Animation) this.h.get("" + i2);
            if (animation == null) {
                view.setAnimation(null);
            } else {
                view.setAnimation(animation);
            }
        }
    }

    public final void a(ae aeVar) {
        this.c = aeVar;
    }

    public final void b() {
        a = false;
        if (!this.f) {
            this.d = 99;
            this.e = 99;
            (getAdapter() instanceof ic ? (ic) getAdapter() : null).notifyDataSetChanged();
        }
    }

    public final ic c() {
        if (getAdapter() instanceof ic) {
            return (ic) getAdapter();
        }
        return null;
    }

    public final boolean d() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        System.currentTimeMillis();
        super.dispatchDraw(canvas);
    }
}
