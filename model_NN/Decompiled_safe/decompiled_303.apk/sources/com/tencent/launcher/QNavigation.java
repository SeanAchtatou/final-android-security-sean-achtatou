package com.tencent.launcher;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.tencent.launcher.base.b;
import com.tencent.qqlauncher.R;

public class QNavigation extends LinearLayout implements e {
    /* access modifiers changed from: private */
    public static final int h = ((int) (30.0f * b.b));
    private LayoutInflater a = LayoutInflater.from(getContext());
    private Rect b = new Rect();
    private gw c;
    private boolean d;
    private int e;
    private Runnable f = new ad(this);
    private Runnable g = new ab(this);
    /* access modifiers changed from: private */
    public int i;
    /* access modifiers changed from: private */
    public boolean j;
    private int k;
    private int l;
    private float m;
    private Animation.AnimationListener n = new ac(this);
    private Animation.AnimationListener o = new z(this);
    private int p = R.layout.launcher_navigation_item;
    private boolean q = true;

    public QNavigation(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    static /* synthetic */ int a(QNavigation qNavigation) {
        int i2 = qNavigation.i;
        qNavigation.i = i2 - 1;
        return i2;
    }

    private int c(int i2) {
        int childCount = getChildCount();
        Rect rect = this.b;
        for (int i3 = 0; i3 < childCount; i3++) {
            if (getChildAt(i3).getVisibility() == 0) {
                getChildAt(i3).getHitRect(rect);
                if (i2 >= rect.left && i2 < rect.right) {
                    this.m = ((float) (i2 - rect.left)) / ((float) h);
                    return i3;
                } else if (i3 == 0 && i2 < rect.left) {
                    this.m = 0.0f;
                    return i3;
                } else if (i3 == childCount - 1 && i2 >= rect.right) {
                    this.m = 0.0f;
                    return i3;
                }
            }
        }
        return -1;
    }

    static /* synthetic */ void d(QNavigation qNavigation) {
        int childCount = qNavigation.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = qNavigation.getChildAt(i2);
            cp cpVar = childAt.getLayoutParams() != null ? (cp) childAt.getLayoutParams() : new cp(qNavigation, -1);
            cpVar.b = childAt.getLeft();
            cpVar.a = childAt.getWidth();
            cpVar.width = h;
        }
        qNavigation.requestLayout();
    }

    /* access modifiers changed from: private */
    public void h() {
        int childCount = getChildCount();
        this.i = 0;
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            cp cpVar = childAt.getLayoutParams() != null ? (cp) childAt.getLayoutParams() : new cp(this, -1);
            if (childAt.getVisibility() == 0) {
                TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, (float) ((cpVar.b - childAt.getLeft()) - ((childAt.getWidth() - cpVar.a) / 2)), 0.0f, 0.0f);
                translateAnimation.setDuration(150);
                translateAnimation.setAnimationListener(this.o);
                childAt.startAnimation(translateAnimation);
                this.i++;
            }
        }
    }

    public final void a() {
        addView((ImageView) this.a.inflate(this.p, (ViewGroup) null), new cp(this, -2));
    }

    public final void a(int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            addView((ImageView) this.a.inflate(this.p, (ViewGroup) null), new cp(this, -2));
        }
    }

    public final void a(int i2, Animation animation) {
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            getChildAt(i3).clearAnimation();
        }
        if (!this.d && getChildAt(i2) != null) {
            getChildAt(i2).startAnimation(animation);
        }
    }

    public final void a(cm cmVar, boolean z) {
        if (this.c != null) {
            this.c.k();
            this.c.g();
        }
        removeCallbacks(this.g);
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            getChildAt(i2).setBackgroundColor(0);
        }
        this.d = false;
    }

    public final void a(gw gwVar) {
        this.c = gwVar;
    }

    public final boolean a(int i2, Object obj) {
        return true;
    }

    public final boolean a(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
        if (this.c.a(cmVar, obj)) {
            this.c.a(cmVar, getLeft() + i2, getTop() + i3, i4, i5, obj);
        } else {
            ((ha) obj).v = false;
            Toast.makeText(getContext(), (int) R.string.no_enough_space, 0).show();
        }
        this.d = false;
        return true;
    }

    public final boolean a(cm cmVar, Object obj) {
        return this.q;
    }

    public final void b() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            getChildAt(i2).setBackgroundColor(0);
        }
        if (this.e >= 0 && this.e < childCount) {
            b(this.e);
        }
        if (this.c != null) {
            this.c.d(this.e);
        }
    }

    public final void b(int i2) {
        int childCount = getChildCount();
        if (i2 < childCount) {
            for (int i3 = 0; i3 < childCount; i3++) {
                getChildAt(i3).setSelected(false);
            }
            getChildAt(i2).setSelected(true);
        }
    }

    public final void b(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
        this.d = true;
        this.k = i2;
        this.l = i3;
        removeCallbacks(this.g);
        int c2 = c(i2);
        if (c2 >= 0) {
            this.e = c2;
            postDelayed(this.g, 200);
        }
    }

    public final void c() {
        int childCount = getChildCount();
        int i2 = this.e;
        for (int i3 = 0; i3 < childCount; i3++) {
            getChildAt(i3).setBackgroundColor(0);
        }
        if (i2 >= 0 && i2 < childCount) {
            b((this.m <= 0.5f || i2 >= childCount - 1) ? i2 : i2 + 1);
            if (this.c != null) {
                this.c.a(i2, this.m);
            }
        }
    }

    public final void c(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
        this.d = true;
        this.k = i2;
        this.l = i3;
        int c2 = c(i2);
        if (c2 >= 0 && this.e != c2) {
            removeCallbacks(this.g);
            this.e = c2;
            postDelayed(this.g, 200);
        }
    }

    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof cp;
    }

    public final void d() {
        post(new aa(this));
    }

    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
    }

    public final void e() {
        post(new ao(this));
    }

    public final void f() {
        this.q = false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int x = (int) motionEvent.getX();
        this.k = x;
        this.l = (int) motionEvent.getY();
        switch (motionEvent.getAction()) {
            case 0:
                this.d = true;
                int childCount = getChildCount();
                int i2 = h;
                int width = (getWidth() - (childCount * i2)) / 2;
                this.j = false;
                this.i = 0;
                for (int i3 = 0; i3 < childCount; i3++) {
                    View childAt = getChildAt(i3);
                    cp cpVar = childAt.getLayoutParams() != null ? (cp) childAt.getLayoutParams() : new cp(this, -1);
                    cpVar.b = childAt.getLeft();
                    cpVar.a = childAt.getWidth();
                    TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, (float) ((((i3 * i2) + width) - childAt.getLeft()) + ((i2 - childAt.getWidth()) / 2)), 0.0f, 0.0f);
                    translateAnimation.setDuration(150);
                    translateAnimation.setAnimationListener(this.n);
                    childAt.startAnimation(translateAnimation);
                    this.i++;
                }
                break;
            case 1:
                if (this.c != null) {
                    this.c.k();
                }
                removeCallbacks(this.f);
                int c2 = c(x);
                if (c2 >= 0) {
                    this.e = c2;
                    if (this.c != null) {
                        gw gwVar = this.c;
                        if (this.m > 0.5f && c2 < getChildCount() - 1) {
                            c2++;
                        }
                        gwVar.b(c2);
                    }
                    h();
                    this.d = false;
                    break;
                }
                break;
            case 2:
                int c3 = c(x);
                if (c3 >= 0) {
                    this.e = c3;
                    removeCallbacks(this.f);
                    post(this.f);
                    break;
                }
                break;
        }
        return true;
    }
}
