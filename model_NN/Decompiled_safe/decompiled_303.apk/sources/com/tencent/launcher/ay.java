package com.tencent.launcher;

import android.view.View;
import com.tencent.module.appcenter.d;
import com.tencent.module.setting.e;
import com.tencent.qqlauncher.R;

final class ay implements View.OnClickListener {
    private /* synthetic */ SearchAppActivityReserved a;

    ay(SearchAppActivityReserved searchAppActivityReserved) {
        this.a = searchAppActivityReserved;
    }

    public final void onClick(View view) {
        if (this.a.mSearchEditText.getText().toString().length() == 0) {
            e eVar = new e(this.a);
            eVar.a((int) R.string.network_tips_title);
            eVar.b(this.a.getResources().getString(R.string.network_tips_no_input_content));
            eVar.b();
            eVar.a((int) R.string.confirm, new b(this));
            eVar.c().show();
            return;
        }
        int unused = this.a.nSearchedAppCount = 0;
        this.a.netsearchTips.setText("正在努力搜索中");
        this.a.setSearchResult();
        this.a.InitialNetSearch();
        d.a().a(this.a.handler, this.a.mSearchEditText.getText().toString(), this.a.nPageNumber);
        SearchAppActivityReserved.access$508(this.a);
    }
}
