package com.tencent.launcher;

import android.appwidget.AppWidgetHostView;
import android.content.ContentValues;

final class c extends ha {
    int a;
    AppWidgetHostView b = null;

    c(int i) {
        this.m = 4;
        this.a = i;
    }

    public final void a(ContentValues contentValues) {
        super.a(contentValues);
        contentValues.put("appWidgetId", Integer.valueOf(this.a));
    }

    public final String toString() {
        return Integer.toString(this.a);
    }
}
