package com.tencent.launcher;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;

public class WidgetContainer extends LinearLayout {
    boolean a;
    private TranslateAnimation b;
    private cx c;
    private boolean d;
    private boolean e;

    public WidgetContainer(Context context) {
        this(context, null);
    }

    public WidgetContainer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = false;
        this.c = null;
        this.d = false;
        this.e = false;
        this.b = new TranslateAnimation(getContext(), null);
        this.b.setFillAfter(true);
        this.b.setInterpolator(new DecelerateInterpolator());
    }

    public final void a(float f, float f2, Animation.AnimationListener animationListener) {
        this.b.a(f, f2);
        this.b.setDuration(180);
        this.b.setAnimationListener(animationListener);
        startAnimation(this.b);
    }

    public void cancelLongPress() {
        super.cancelLongPress();
        this.a = false;
        if (this.c != null) {
            removeCallbacks(this.c);
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.a) {
            this.a = false;
            return true;
        }
        switch (motionEvent.getAction()) {
            case 0:
                this.a = false;
                if (this.c == null) {
                    this.c = new cx(this);
                }
                this.c.a();
                postDelayed(this.c, (long) ViewConfiguration.getLongPressTimeout());
                break;
            case 1:
            case 3:
                this.a = false;
                if (this.c != null) {
                    removeCallbacks(this.c);
                    break;
                }
                break;
        }
        return false;
    }
}
