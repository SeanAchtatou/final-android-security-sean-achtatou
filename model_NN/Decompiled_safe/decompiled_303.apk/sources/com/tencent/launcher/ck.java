package com.tencent.launcher;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

public final class ck {
    public final CharSequence a;
    public final Drawable b;
    private int c;
    private /* synthetic */ co d;

    public ck(co coVar, Resources resources, int i, int i2, int i3) {
        this.d = coVar;
        this.a = resources.getString(i);
        if (i2 != -1) {
            this.b = resources.getDrawable(i2);
        } else {
            this.b = null;
        }
        this.c = i3;
    }
}
