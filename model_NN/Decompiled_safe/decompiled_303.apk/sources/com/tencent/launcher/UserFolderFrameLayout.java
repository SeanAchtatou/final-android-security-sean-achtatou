package com.tencent.launcher;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import java.lang.ref.SoftReference;

public class UserFolderFrameLayout extends FrameLayout {
    private SoftReference a;

    public UserFolderFrameLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public UserFolderFrameLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void clearAnimation() {
        if (this.a != null) {
            if (this.a.get() != null) {
                ((Bitmap) this.a.get()).recycle();
            }
            this.a = null;
        }
        super.clearAnimation();
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        if (this.a == null || this.a.get() == null) {
            super.dispatchDraw(canvas);
        } else {
            canvas.drawBitmap((Bitmap) this.a.get(), 0.0f, 0.0f, new Paint());
        }
    }

    public void startAnimation(Animation animation) {
        setDrawingCacheEnabled(true);
        Bitmap drawingCache = getDrawingCache(true);
        if (drawingCache != null) {
            try {
                Bitmap createBitmap = Bitmap.createBitmap(drawingCache.getWidth(), drawingCache.getHeight(), Bitmap.Config.ARGB_8888);
                createBitmap.setDensity(getResources().getDisplayMetrics().densityDpi);
                new Canvas(createBitmap).drawBitmap(drawingCache, 0.0f, 0.0f, new Paint());
                this.a = new SoftReference(createBitmap);
            } catch (OutOfMemoryError e) {
                return;
            }
        }
        super.startAnimation(animation);
    }
}
