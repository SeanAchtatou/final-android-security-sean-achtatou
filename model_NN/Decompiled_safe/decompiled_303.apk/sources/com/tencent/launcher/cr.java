package com.tencent.launcher;

import android.view.View;

final class cr implements hs {
    private /* synthetic */ QNavigation a;
    private /* synthetic */ Workspace b;
    private /* synthetic */ DeleteZone c;
    private /* synthetic */ Launcher d;

    cr(Launcher launcher, QNavigation qNavigation, Workspace workspace, DeleteZone deleteZone) {
        this.d = launcher;
        this.a = qNavigation;
        this.b = workspace;
        this.c = deleteZone;
    }

    public final void a() {
        if (this.d.mGrid.c()) {
            this.d.mGrid.a().a();
        }
        if (this.d.mDrawer.a()) {
            this.d.showDockButtonGroup();
        } else {
            this.b.i();
        }
        this.d.mHomeButton.b();
        this.d.showDockBar();
        this.d.showHomeFlipper();
        this.c.a();
        this.d.mDockBar.a();
    }

    public final void a(View view, cm cmVar, Object obj, int i) {
        if (cmVar instanceof AllAppsListView) {
            this.d.disappearPreNextZone();
        } else {
            this.d.showPreNextZone();
        }
        if (this.d.mDrawer.a()) {
            if (this.d.mGrid.c()) {
                this.d.mGrid.a().a(this.a, cmVar, obj, i);
            }
            this.d.hideDockBar();
            this.d.hideDockButtonGroup();
            this.d.hideHomeFlipper();
            this.d.mHomeButton.a();
        } else {
            this.b.h();
        }
        this.c.a(view, cmVar, obj, i);
        this.d.mDockBar.a(view, cmVar, obj, i);
    }
}
