package com.tencent.launcher;

import android.content.Context;
import java.util.Iterator;

/* renamed from: com.tencent.launcher.do  reason: invalid class name */
final class Cdo implements Runnable {
    private bq a;
    private /* synthetic */ DockBar b;

    public Cdo(DockBar dockBar, bq bqVar) {
        this.b = dockBar;
        this.a = bqVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void
     arg types: [com.tencent.launcher.Launcher, com.tencent.launcher.am, long, int, int, int, int]
     candidates:
      com.tencent.launcher.ff.a(android.database.Cursor, android.content.Context, int, int, int, int, android.content.Intent):com.tencent.launcher.am
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void */
    public final void run() {
        Iterator it = this.a.b.iterator();
        while (it.hasNext()) {
            ff.a((Context) this.b.d, (ha) ((am) it.next()), this.a.l, 0, 0, 0, false);
        }
    }
}
