package com.tencent.launcher.base;

import com.tencent.util.l;
import java.util.HashMap;
import java.util.WeakHashMap;

public final class c {
    private static c a = new c();
    private HashMap b = new HashMap();
    private WeakHashMap c = new WeakHashMap();
    private l d = new l();

    private c() {
    }

    public static c a() {
        return a;
    }

    public final void a(String str, Object obj) {
        this.b.put(str, obj);
    }
}
