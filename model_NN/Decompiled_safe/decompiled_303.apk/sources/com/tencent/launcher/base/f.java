package com.tencent.launcher.base;

import android.widget.Toast;

final class f implements Runnable {
    private /* synthetic */ CharSequence a;

    f(CharSequence charSequence) {
        this.a = charSequence;
    }

    public final void run() {
        if (BaseApp.e == null) {
            Toast unused = BaseApp.e = Toast.makeText(BaseApp.b, this.a, 0);
        } else {
            BaseApp.e.cancel();
            BaseApp.e.setText(this.a);
        }
        BaseApp.e.show();
    }
}
