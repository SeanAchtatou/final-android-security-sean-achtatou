package com.tencent.launcher.base;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.tencent.qqlauncher.R;
import com.tencent.util.p;
import java.lang.reflect.Field;
import java.util.Locale;

public final class b {
    public static final int a = Integer.parseInt(Build.VERSION.SDK);
    public static float b = 1.0f;
    public static int c;
    public static int d;
    public static String e;
    public static Field f;
    public static boolean g;
    public static boolean h = false;
    public static int i;
    public static int j;
    public static int k;
    public static int l;
    public static int m;
    public static int n;
    private static String o = Build.MODEL;
    private static DisplayMetrics p = new DisplayMetrics();
    private static int q;

    public static void a(Context context) {
        e = context.getPackageName();
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        defaultDisplay.getMetrics(p);
        b = p.density;
        g = defaultDisplay.getPixelFormat() != 4;
        c = defaultDisplay.getWidth();
        d = defaultDisplay.getHeight();
        if (c > d) {
            int i2 = c;
            c = d;
            d = i2;
        }
        f = p.a(Activity.class, "mStatusBarTapScrollTopViews");
        Resources resources = context.getResources();
        i = (int) resources.getDimension(R.dimen.icon_out_size);
        j = (int) resources.getDimension(R.dimen.icon_in_size);
        q = (int) resources.getDimension(R.dimen.icon_drawer_size);
        k = (int) resources.getDimension(R.dimen.icon_out_size_theme);
        l = (int) resources.getDimension(R.dimen.icon_in_size_theme);
        m = (int) resources.getDimension(R.dimen.dockicon_out_size_theme);
        n = (int) resources.getDimension(R.dimen.dockicon_in_size_theme);
        b(context);
    }

    public static boolean a() {
        return h;
    }

    public static void b(Context context) {
        Locale locale = context.getResources().getConfiguration().locale;
        h = false;
        if (locale != null && "zh_CN".equals(locale.toString())) {
            h = true;
        }
    }
}
