package com.tencent.launcher.base;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import com.tencent.qqlauncher.R;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.Thread;
import java.util.ArrayList;

public final class d implements Thread.UncaughtExceptionHandler {
    public static String a = (e + "错误信息.txt");
    private static String b = "QQLauncher";
    private static final String c = System.getProperty("line.separator");
    private static final String d = Environment.getExternalStorageDirectory().getAbsolutePath();
    private static String e = (d + "/Tencent/QQLauncher/log/");
    private static final Thread.UncaughtExceptionHandler f = Thread.getDefaultUncaughtExceptionHandler();
    private static boolean k = false;
    private static String l;
    private String g;
    private int h;
    private String i;
    private String j;

    public d(Context context) {
        a = e + context.getResources().getString(R.string.title_crash_log) + ".txt";
        l = "/data/data/" + BaseApp.a;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            this.g = packageInfo.versionName;
            this.h = packageInfo.versionCode;
            this.i = Build.MODEL;
            this.j = Build.VERSION.SDK;
            new h(this).start();
            File file = new File(l + "/crashed");
            if (file.exists() && "true".equals(new BufferedReader(new FileReader(file)).readLine())) {
                k = true;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void a(d dVar) {
        try {
            File[] listFiles = new File(e).listFiles(new g(dVar, System.currentTimeMillis()));
            if (listFiles != null) {
                for (File delete : listFiles) {
                    delete.delete();
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static boolean a() {
        return k;
    }

    public static void b() {
        new File(l + "/crashed").delete();
        k = false;
    }

    private static StringBuilder c() {
        StringBuilder sb = new StringBuilder();
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add("logcat");
            arrayList.add("-d");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec((String[]) arrayList.toArray(new String[0])).getInputStream()));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
                sb.append(c);
            }
        } catch (IOException e2) {
            Log.e("TAG", "getLog failed", e2);
        }
        return sb;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x01a3, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x01a3 A[ExcHandler: Throwable (r0v9 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:11:0x0129] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void uncaughtException(java.lang.Thread r7, java.lang.Throwable r8) {
        /*
            r6 = this;
            java.io.StringWriter r0 = new java.io.StringWriter
            r0.<init>()
            java.io.PrintWriter r1 = new java.io.PrintWriter
            r1.<init>(r0)
            r8.printStackTrace(r1)
            java.io.PrintStream r1 = java.lang.System.out
            java.lang.String r2 = "The home has been crash!"
            r1.println(r2)
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r2 = com.tencent.launcher.base.d.e     // Catch:{ Exception -> 0x01a8 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x01a8 }
            boolean r2 = r1.isDirectory()     // Catch:{ Exception -> 0x01a8 }
            if (r2 != 0) goto L_0x002a
            boolean r2 = r1.exists()     // Catch:{ Exception -> 0x01a8 }
            if (r2 != 0) goto L_0x002a
            r1.mkdirs()     // Catch:{ Exception -> 0x01a8 }
        L_0x002a:
            android.text.format.Time r1 = new android.text.format.Time     // Catch:{ Exception -> 0x01a8 }
            r1.<init>()     // Catch:{ Exception -> 0x01a8 }
            r1.setToNow()     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r2 = "%Y-%m-%d %H:%M:%S"
            java.lang.String r1 = r1.format(r2)     // Catch:{ Exception -> 0x01a8 }
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x01a8 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a8 }
            r3.<init>()     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r4 = com.tencent.launcher.base.d.e     // Catch:{ Exception -> 0x01a8 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x01a8 }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x01a8 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r4 = ".log"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x01a8 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x01a8 }
            r2.createNewFile()     // Catch:{ Exception -> 0x01a8 }
            java.io.BufferedWriter r3 = new java.io.BufferedWriter     // Catch:{ Exception -> 0x01a8 }
            java.io.FileWriter r4 = new java.io.FileWriter     // Catch:{ Exception -> 0x01a8 }
            r4.<init>(r2)     // Catch:{ Exception -> 0x01a8 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r4 = "\t\n==================LOG=================\t\n"
            r3.write(r4)     // Catch:{ Exception -> 0x01a8 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a8 }
            r4.<init>()     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r5 = "APP_VERSION:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r5 = r6.g     // Catch:{ Exception -> 0x01a8 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r5 = "|"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01a8 }
            int r5 = r6.h     // Catch:{ Exception -> 0x01a8 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r5 = "\t\n"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01a8 }
            r3.write(r4)     // Catch:{ Exception -> 0x01a8 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a8 }
            r4.<init>()     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r5 = "PHONE_MODEL:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r5 = r6.i     // Catch:{ Exception -> 0x01a8 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r5 = "\t\n"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01a8 }
            r3.write(r4)     // Catch:{ Exception -> 0x01a8 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a8 }
            r4.<init>()     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r5 = "ANDROID_SDK:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r5 = r6.j     // Catch:{ Exception -> 0x01a8 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r5 = "\t\n"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01a8 }
            r3.write(r4)     // Catch:{ Exception -> 0x01a8 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a8 }
            r4.<init>()     // Catch:{ Exception -> 0x01a8 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r4 = "\t\n"
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01a8 }
            r3.write(r1)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01a8 }
            r3.write(r0)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r0 = "\t\n--------------------------------------\t\n"
            r3.write(r0)     // Catch:{ Exception -> 0x01a8 }
            r3.flush()     // Catch:{ Exception -> 0x01a8 }
            java.lang.StringBuilder r0 = c()     // Catch:{ Exception -> 0x01a8 }
            int r1 = r0.length()     // Catch:{ Exception -> 0x01a8 }
            r4 = 200000(0x30d40, float:2.8026E-40)
            int r1 = r1 - r4
            r4 = 0
            int r1 = java.lang.Math.max(r1, r4)     // Catch:{ Exception -> 0x01a8 }
            if (r1 <= 0) goto L_0x0114
            r4 = 0
            r0.delete(r4, r1)     // Catch:{ Exception -> 0x01a8 }
        L_0x0114:
            java.lang.StringBuilder r0 = c()     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01a8 }
            r3.write(r0)     // Catch:{ Exception -> 0x01a8 }
            r3.flush()     // Catch:{ Exception -> 0x01a8 }
            r3.close()     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r0 = r2.getAbsolutePath()     // Catch:{ Exception -> 0x01a8 }
            java.io.File r1 = new java.io.File     // Catch:{ Throwable -> 0x01a3 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01a3 }
            r2.<init>()     // Catch:{ Throwable -> 0x01a3 }
            java.lang.String r3 = com.tencent.launcher.base.d.l     // Catch:{ Throwable -> 0x01a3 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x01a3 }
            java.lang.String r3 = "/crashed"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x01a3 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x01a3 }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x01a3 }
            r2 = -1
            boolean r4 = r1.exists()     // Catch:{ Throwable -> 0x01a3 }
            if (r4 == 0) goto L_0x015d
            java.io.FileReader r4 = new java.io.FileReader     // Catch:{ Throwable -> 0x01a3 }
            r4.<init>(r1)     // Catch:{ Throwable -> 0x01a3 }
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x01a3 }
            r5.<init>(r4)     // Catch:{ Throwable -> 0x01a3 }
            java.lang.String r4 = r5.readLine()     // Catch:{ Throwable -> 0x01a3 }
            long r2 = java.lang.Long.parseLong(r4)     // Catch:{ Exception -> 0x01ad, Throwable -> 0x01a3 }
        L_0x015d:
            r4 = 0
            int r4 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x018b
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x01a3 }
            long r2 = r4 - r2
            r4 = 15000(0x3a98, double:7.411E-320)
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 >= 0) goto L_0x018b
            java.lang.String r2 = "true"
            java.lang.String r3 = com.tencent.launcher.base.d.a     // Catch:{ Throwable -> 0x01a3 }
            com.tencent.util.p.a(r0, r3)     // Catch:{ Throwable -> 0x01a3 }
            r0 = r2
        L_0x0177:
            java.io.FileWriter r2 = new java.io.FileWriter     // Catch:{ Throwable -> 0x01a3 }
            r2.<init>(r1)     // Catch:{ Throwable -> 0x01a3 }
            r2.write(r0)     // Catch:{ Throwable -> 0x01a3 }
            r2.flush()     // Catch:{ Throwable -> 0x01a3 }
            r2.close()     // Catch:{ Throwable -> 0x01a3 }
        L_0x0185:
            java.lang.Thread$UncaughtExceptionHandler r0 = com.tencent.launcher.base.d.f
            r0.uncaughtException(r7, r8)
            return
        L_0x018b:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01a3 }
            r0.<init>()     // Catch:{ Throwable -> 0x01a3 }
            java.lang.String r2 = ""
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x01a3 }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x01a3 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x01a3 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x01a3 }
            goto L_0x0177
        L_0x01a3:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x01a8 }
            goto L_0x0185
        L_0x01a8:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0185
        L_0x01ad:
            r4 = move-exception
            goto L_0x015d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.launcher.base.d.uncaughtException(java.lang.Thread, java.lang.Throwable):void");
    }
}
