package com.tencent.launcher.base;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Vibrator;
import android.widget.Toast;
import com.tencent.module.download.DownloadService;

public class BaseApp extends Application {
    public static String a;
    /* access modifiers changed from: private */
    public static Context b;
    private static Handler c;
    private static Handler d;
    /* access modifiers changed from: private */
    public static Toast e;
    private static Vibrator f;
    private ServiceConnection g;

    public static void a(int i) {
        String string = b.getResources().getString(i);
        if (string != null) {
            c.post(new f(string));
        }
    }

    public static void a(CharSequence charSequence) {
        c.post(new f(charSequence));
    }

    public static Context b() {
        return b;
    }

    public static void c() {
        if (f == null) {
            f = (Vibrator) b.getSystemService("vibrator");
        }
        f.vibrate(20);
    }

    public static Handler d() {
        return d;
    }

    public final void a() {
        unbindService(this.g);
        sendBroadcast(new Intent("com.tencent.qqlauncher.QUIT"));
    }

    public void onConfigurationChanged(Configuration configuration) {
        b.b(getBaseContext());
        super.onConfigurationChanged(configuration);
    }

    public void onCreate() {
        System.currentTimeMillis();
        a = getPackageName();
        super.onCreate();
        b = getApplicationContext();
        c = new Handler();
        HandlerThread handlerThread = new HandlerThread("base-handler-thread");
        handlerThread.start();
        d = new Handler(handlerThread.getLooper());
        Thread.setDefaultUncaughtExceptionHandler(new d(this));
        b.a(getBaseContext());
        this.g = new e(this);
        bindService(new Intent(this, DownloadService.class), this.g, 1);
    }

    public void onLowMemory() {
        super.onLowMemory();
    }

    public void onTerminate() {
        super.onTerminate();
    }
}
