package com.tencent.launcher.base;

import java.io.File;
import java.io.FilenameFilter;

final class g implements FilenameFilter {
    private /* synthetic */ long a;
    private /* synthetic */ d b;

    g(d dVar, long j) {
        this.b = dVar;
        this.a = j;
    }

    public final boolean accept(File file, String str) {
        return this.a - new File(new StringBuilder().append(file.getAbsolutePath()).append("/").append(str).toString()).lastModified() > 1209600000;
    }
}
