package com.tencent.launcher;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;

/* renamed from: com.tencent.launcher.if  reason: invalid class name */
final class Cif extends AsyncTask {
    private BitmapFactory.Options a = new BitmapFactory.Options();
    private /* synthetic */ WallpaperChooser b;

    Cif(WallpaperChooser wallpaperChooser) {
        this.b = wallpaperChooser;
        this.a.inDither = false;
        this.a.inPreferredConfig = Bitmap.Config.ARGB_8888;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public Bitmap doInBackground(Integer... numArr) {
        if (isCancelled()) {
            return null;
        }
        try {
            return BitmapFactory.decodeResource(this.b.getResources(), ((Integer) this.b.mImages.get(numArr[0].intValue())).intValue(), this.a);
        } catch (OutOfMemoryError e) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.a.requestCancelDecode();
        super.cancel(true);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        if (bitmap == null) {
            return;
        }
        if (isCancelled() || this.a.mCancel) {
            bitmap.recycle();
            return;
        }
        if (this.b.mBitmap != null) {
            this.b.mBitmap.recycle();
        }
        ImageView access$300 = this.b.mImageView;
        access$300.setImageBitmap(bitmap);
        Bitmap unused = this.b.mBitmap = bitmap;
        Drawable drawable = access$300.getDrawable();
        drawable.setFilterBitmap(true);
        drawable.setDither(true);
        access$300.postInvalidate();
        Cif unused2 = this.b.mLoader = null;
    }
}
