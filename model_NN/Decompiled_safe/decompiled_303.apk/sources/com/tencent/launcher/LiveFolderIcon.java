package com.tencent.launcher;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.tencent.qqlauncher.R;

public class LiveFolderIcon extends FolderIcon {
    private dq a;

    public LiveFolderIcon(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    static LiveFolderIcon a(Launcher launcher, ViewGroup viewGroup, dq dqVar) {
        LiveFolderIcon liveFolderIcon = (LiveFolderIcon) LayoutInflater.from(launcher).inflate((int) R.layout.live_folder_icon, viewGroup, false);
        liveFolderIcon.a = dqVar;
        Drawable a2 = dqVar.d == null ? a.a(launcher, launcher.getResources().getDrawable(R.drawable.ic_launcher_folder), Launcher.sIconBackgroundDrawable, dqVar) : a.a(launcher, dqVar.d, Launcher.sIconBackgroundDrawable, dqVar);
        dqVar.d = a2;
        liveFolderIcon.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, a2, (Drawable) null, (Drawable) null);
        liveFolderIcon.setText(dqVar.h);
        liveFolderIcon.setTag(dqVar);
        liveFolderIcon.setOnClickListener(launcher);
        return liveFolderIcon;
    }

    public final void a() {
        destroyDrawingCache();
        Drawable a2 = a.a(getContext(), getContext().getResources().getDrawable(R.drawable.ic_launcher_folder), Launcher.sIconBackgroundDrawable, (ha) null);
        if (a2 != null) {
            setCompoundDrawablesWithIntrinsicBounds((Drawable) null, a2, (Drawable) null, (Drawable) null);
            this.a.d = a2;
        }
        postInvalidate();
    }
}
