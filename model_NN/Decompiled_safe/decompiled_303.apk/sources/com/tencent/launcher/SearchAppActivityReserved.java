package com.tencent.launcher;

import android.app.ListActivity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.qqlauncher.R;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchAppActivityReserved extends ListActivity implements TextWatcher, View.OnClickListener, View.OnFocusChangeListener, View.OnKeyListener, View.OnTouchListener, TextView.OnEditorActionListener {
    private static final float Font_PADDING = 20.0f;
    private static final float HOTWORD_TEXTSIZE = 16.0f;
    private static final int MAX_FONT_HEIGHT = 50;
    private static final int RADIO_LINE_NAMEBER = 12;
    private static final int RECT_MATRIC = 6;
    private static final String TAG = "SearchAppActivityReserved";
    private static final String soundMotherReg = "^[b|p|m|f|d|t|n|l|g|k|h|j|q|x|r|z|c|s|y|w].*";
    private View appCenterSearch;
    private AnimationSet[] asets;
    /* access modifiers changed from: private */
    public boolean bAllHttpMsgReceived = false;
    /* access modifiers changed from: private */
    public boolean bSearchType = false;
    private ImageView centerIcon;
    private ImageView closeBt;
    private ImageView delSearchBtn;
    private ImageButton doSearchButton;
    /* access modifiers changed from: private */
    public Handler handler = new av(this);
    /* access modifiers changed from: private */
    public int[] hotWordX;
    /* access modifiers changed from: private */
    public int[] hotWordY;
    private int[] hotwordColor = {R.color.tag_color_1, R.color.tag_color_2, R.color.tag_color_3, R.color.tag_color_4, R.color.tag_color_5};
    private int[] hotwordSize = {14, 16, 17, 19, 20, 21, 22, 23, 24};
    /* access modifiers changed from: private */
    public ArrayList hotwordsCache;
    private View hotwordsView;
    /* access modifiers changed from: private */
    public boolean isFinishHotwords = false;
    /* access modifiers changed from: private */
    public ee listAdapter;
    /* access modifiers changed from: private */
    public TextView localSearch;
    private ArrayList mAppList = new ArrayList();
    private TextView mBtn = null;
    /* access modifiers changed from: private */
    public RelativeLayout mLinearLayout;
    /* access modifiers changed from: private */
    public ListView mListView;
    private ArrayList mMacheList = new ArrayList();
    /* access modifiers changed from: private */
    public ArrayList mNetList = new ArrayList();
    /* access modifiers changed from: private */
    public ListView mNetSearchListView;
    /* access modifiers changed from: private */
    public EditText mSearchEditText;
    private Button[] mTextView;
    /* access modifiers changed from: private */
    public View mWaittingTips;
    private View marketSearch;
    /* access modifiers changed from: private */
    public int nAppCount = 0;
    /* access modifiers changed from: private */
    public int nPageNumber = 1;
    /* access modifiers changed from: private */
    public int nSearchedAppCount = 0;
    private LinearLayout netSearchTipsLayout;
    /* access modifiers changed from: private */
    public gz netsearchListAdapter;
    /* access modifiers changed from: private */
    public TextView netsearchTips;
    /* access modifiers changed from: private */
    public TextView networkSearch;
    private int parentHeight;
    private int parentWidth;
    private ProgressBar pbar = null;
    private TextView reTryBtn = null;
    private Rect[][] regions;
    /* access modifiers changed from: private */
    public int requestId = -1;
    private ff sModel;
    private TextView textView;
    /* access modifiers changed from: private */
    public int totalCount = 0;
    private TextView tvLoading = null;
    private int unitH;
    private int unitW;

    private void InitialCommonUI() {
        this.mSearchEditText = (EditText) findViewById(R.id.EditText_Search);
        this.mSearchEditText.addTextChangedListener(this);
        this.mSearchEditText.setOnEditorActionListener(this);
        this.textView = (TextView) findViewById(R.id.nofindapp);
        this.delSearchBtn = (ImageView) findViewById(R.id.btn_del_search);
        this.delSearchBtn.setOnClickListener(this);
        this.doSearchButton = (ImageButton) findViewById(R.id.ImageButton_Search);
        this.doSearchButton.setOnClickListener(new ay(this));
        this.appCenterSearch = LayoutInflater.from(this).inflate((int) R.layout.search_list_appcenter_item, (ViewGroup) null);
        this.appCenterSearch.findViewById(R.id.search_app_center).setOnClickListener(this);
        this.appCenterSearch.setVisibility(8);
        this.marketSearch = LayoutInflater.from(this).inflate((int) R.layout.search_list_market_item, (ViewGroup) null);
        this.marketSearch.findViewById(R.id.search_market).setOnClickListener(this);
        this.marketSearch.setVisibility(8);
        this.localSearch = (TextView) findViewById(R.id.local_search);
        this.localSearch.setBackgroundColor(-65536);
        this.netSearchTipsLayout = (LinearLayout) findViewById(R.id.SearchTipsLayout);
        this.netSearchTipsLayout.setVisibility(8);
        this.netsearchTips = (TextView) findViewById(R.id.search_tips);
        this.netsearchTips.setText(" ");
        this.networkSearch = (TextView) findViewById(R.id.network_search);
        this.networkSearch.setBackgroundColor(-1);
        this.localSearch.setOnClickListener(new az(this));
        this.networkSearch.setOnClickListener(new au(this));
        initFrame();
        setSearchResult();
    }

    /* access modifiers changed from: private */
    public void InitialLocalSearch() {
        setSearchResult();
        this.bSearchType = false;
        this.localSearch.setBackgroundColor(-65536);
        this.networkSearch.setBackgroundColor(-1);
        this.netSearchTipsLayout.setVisibility(8);
        if (this.mNetSearchListView != null && this.mNetSearchListView.getFooterViewsCount() > 0) {
            this.mNetSearchListView.removeFooterView(this.mWaittingTips);
        }
        this.mListView = getListView();
        this.mListView.setFooterDividersEnabled(false);
        this.mListView.addFooterView(this.appCenterSearch);
        this.mListView.addFooterView(this.marketSearch);
        this.listAdapter = new ee(this, this, this.mMacheList);
        this.listAdapter.setNotifyOnChange(false);
        this.mListView.setAdapter((ListAdapter) this.listAdapter);
        this.mListView.setOnItemClickListener(new aw(this));
        this.mListView.setOnFocusChangeListener(this);
        this.mListView.setOnTouchListener(this);
    }

    /* access modifiers changed from: private */
    public void InitialNetSearch() {
        this.mNetSearchListView = getListView();
        this.mNetSearchListView.setFooterDividersEnabled(true);
        this.mNetList.clear();
        this.nPageNumber = 1;
        this.nAppCount = 0;
        this.bAllHttpMsgReceived = false;
        this.mNetSearchListView.removeFooterView(this.appCenterSearch);
        this.mNetSearchListView.removeFooterView(this.marketSearch);
        if (this.mNetSearchListView.getFooterViewsCount() > 0) {
            this.mNetSearchListView.removeFooterView(this.mWaittingTips);
        }
        this.netSearchTipsLayout.setVisibility(0);
        this.nSearchedAppCount = 0;
        this.netsearchTips.setText("正在努力搜索中");
        this.mWaittingTips = getLayoutInflater().inflate((int) R.layout.list_waiting, (ViewGroup) null);
        this.mNetSearchListView.addFooterView(this.mWaittingTips);
        this.tvLoading = (TextView) this.mWaittingTips.findViewById(R.id.TextView01);
        this.pbar = (ProgressBar) this.mWaittingTips.findViewById(R.id.ProgressBar01);
        this.reTryBtn = (TextView) this.mWaittingTips.findViewById(R.id.WaitingBtn);
        this.tvLoading.setVisibility(0);
        this.pbar.setVisibility(0);
        this.reTryBtn.setVisibility(8);
        this.netsearchListAdapter = new gz(this, this, this.mNetList);
        this.netsearchListAdapter.setNotifyOnChange(false);
        this.mNetSearchListView.setAdapter((ListAdapter) this.netsearchListAdapter);
        this.mNetSearchListView.setOnItemClickListener(new ax(this));
        this.mNetSearchListView.setOnScrollListener(new ar(this));
    }

    static /* synthetic */ int access$1912(SearchAppActivityReserved searchAppActivityReserved, int i) {
        int i2 = searchAppActivityReserved.nAppCount + i;
        searchAppActivityReserved.nAppCount = i2;
        return i2;
    }

    static /* synthetic */ int access$508(SearchAppActivityReserved searchAppActivityReserved) {
        int i = searchAppActivityReserved.nPageNumber;
        searchAppActivityReserved.nPageNumber = i + 1;
        return i;
    }

    private Rect[] calculateRegin() {
        this.regions = new Rect[6][];
        for (int i = 0; i < this.regions.length; i++) {
            this.regions[i] = new Rect[6];
            for (int i2 = 0; i2 < this.regions[i].length; i2++) {
                int i3 = (i2 - 3) * this.unitW;
                int i4 = (i - 3) * this.unitH;
                int i5 = ((i2 + 1) - 3) * this.unitW;
                int i6 = ((i + 1) - 3) * this.unitH;
                if (i3 == 0) {
                    i3 = this.unitW / 2;
                }
                if (i4 == 0) {
                    i4 = this.unitH / 2;
                }
                if (i5 == this.unitW * 6) {
                    i5 -= this.unitW / 2;
                }
                if (i6 == this.unitH * 6) {
                    i6 -= this.unitH / 2;
                }
                this.regions[i][i2] = new Rect(i3, i4, i5, i6);
            }
        }
        Rect[] rectArr = new Rect[RADIO_LINE_NAMEBER];
        rectArr[0] = new Rect(this.regions[2][0].left, this.regions[2][0].top, this.regions[2][2].right, this.regions[2][2].bottom);
        rectArr[1] = new Rect(this.regions[0][0].left, this.regions[0][0].top, this.regions[1][1].right, this.regions[1][1].bottom);
        rectArr[2] = new Rect(this.regions[0][2].left, this.regions[0][2].top, this.regions[1][2].right, this.regions[1][2].bottom);
        rectArr[3] = new Rect(this.regions[0][3].left, this.regions[0][3].top, this.regions[1][3].right, this.regions[1][3].bottom);
        rectArr[4] = new Rect(this.regions[0][4].left, this.regions[0][4].top, this.regions[1][5].right, this.regions[1][5].bottom);
        rectArr[5] = new Rect(this.regions[2][3].left, this.regions[2][3].top, this.regions[2][5].right, this.regions[2][5].bottom);
        rectArr[6] = new Rect(this.regions[3][4].left, this.regions[3][4].top, this.regions[3][5].right, this.regions[3][5].bottom);
        rectArr[7] = new Rect(this.regions[4][4].left, this.regions[4][4].top, this.regions[5][5].right, this.regions[5][5].bottom);
        rectArr[8] = new Rect(this.regions[3][3].left, this.regions[3][3].top, this.regions[5][3].right, this.regions[5][3].bottom);
        rectArr[9] = new Rect(this.regions[3][2].left, this.regions[3][2].top, this.regions[5][2].right, this.regions[5][2].bottom);
        rectArr[10] = new Rect(this.regions[4][0].left, this.regions[4][0].top, this.regions[5][1].right, this.regions[5][1].bottom);
        rectArr[11] = new Rect(this.regions[3][0].left, this.regions[3][0].top, this.regions[3][2].right, this.regions[3][2].bottom);
        return rectArr;
    }

    private void getAllApp() {
        this.sModel = Launcher.getModel();
        bl e = this.sModel.e();
        if (e == null) {
            finish();
            return;
        }
        int count = e.getCount();
        this.mAppList.clear();
        this.mNetList.clear();
        for (int i = 0; i < count; i++) {
            ha haVar = (ha) e.getItem(i);
            if (haVar instanceof am) {
                this.mAppList.add((am) haVar);
            } else if (haVar instanceof bq) {
                this.mAppList.addAll(((bq) haVar).b);
            }
        }
    }

    private Rect[] getCenterRect() {
        Rect[] rectArr = new Rect[16];
        rectArr[0] = this.regions[2][2];
        rectArr[1] = this.regions[2][3];
        rectArr[2] = this.regions[3][3];
        rectArr[3] = this.regions[3][2];
        rectArr[4] = this.regions[3][1];
        rectArr[5] = this.regions[2][1];
        rectArr[6] = this.regions[1][1];
        rectArr[7] = this.regions[1][2];
        rectArr[8] = this.regions[1][3];
        rectArr[9] = this.regions[1][4];
        rectArr[10] = this.regions[2][4];
        rectArr[11] = this.regions[3][4];
        rectArr[RADIO_LINE_NAMEBER] = this.regions[4][4];
        rectArr[13] = this.regions[4][3];
        rectArr[14] = this.regions[4][2];
        rectArr[15] = this.regions[4][1];
        return rectArr;
    }

    private String getTextFilter() {
        if (this.mSearchEditText != null) {
            return this.mSearchEditText.getText().toString();
        }
        return null;
    }

    private void hideSoftKeyboard() {
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.mListView.getWindowToken(), 0);
    }

    private boolean isCachedHotwards(List list) {
        for (int i = 0; i < this.hotwordsCache.size(); i++) {
            ArrayList arrayList = (ArrayList) this.hotwordsCache.get(i);
            for (int i2 = 0; i2 < list.size(); i2++) {
                if (arrayList.contains(list.get(i))) {
                    this.isFinishHotwords = true;
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isCollision(int i, int i2, boolean z) {
        if (this.mTextView[i].getVisibility() != 0 || this.mTextView[i2].getVisibility() != 0) {
            return false;
        }
        Log.v("Search", "检测  " + z + " " + ((Object) this.mTextView[i].getText()) + " " + ((Object) this.mTextView[i2].getText()));
        int abs = Math.abs(this.hotWordY[i] - this.hotWordY[i2]);
        Paint paint = new Paint();
        paint.setTextSize(this.mTextView[i].getTextSize());
        float measureText = paint.measureText(this.mTextView[i].getText().toString());
        paint.setTextSize(this.mTextView[i2].getTextSize());
        float measureText2 = paint.measureText(this.mTextView[i2].getText().toString());
        int i3 = (int) (((float) this.hotWordX[i]) - (measureText / 2.0f));
        int i4 = (int) (((float) this.hotWordX[i2]) - (measureText2 / 2.0f));
        if (abs < MAX_FONT_HEIGHT) {
            Log.v("Search", "两者之高度差比字体高小 Yabs:" + abs + "fontHeight:" + ((int) MAX_FONT_HEIGHT));
            if (i3 < i4) {
                Log.v("Search", "自己在左边");
                if (((float) (i4 - i3)) < measureText) {
                    Log.v("Search", "有冲突:hotWordX[other]:" + this.hotWordX[i2] + "hotWordX[self]:" + this.hotWordX[i] + "strWidths:" + measureText);
                    if (z) {
                        Log.v("Search", "解决ing");
                        Log.v("Search", ((Object) this.mTextView[i].getText()) + "Ori X:" + this.hotWordX[i] + "Ori Y:" + this.hotWordY[i]);
                        solveCollision(i, i2);
                        Log.v("Search", ((Object) this.mTextView[i].getText()) + "Solved X:" + this.hotWordX[i] + "Solved Y:" + this.hotWordY[i]);
                        Log.v("Search", "解决ed");
                    }
                    return true;
                }
                Log.v("Search", "没有冲突");
            } else {
                Log.v("Search", "自己在右边");
                if (((float) (i3 - i4)) < measureText2) {
                    Log.v("Search", "有冲突:hotWordX[other]:" + this.hotWordX[i2] + "hotWordX[self]:" + this.hotWordX[i] + "strWidths:" + measureText2);
                    if (z) {
                        Log.v("Search", "解决ing");
                        Log.v("Search", ((Object) this.mTextView[i].getText()) + "Ori X:" + this.hotWordX[i] + "Ori Y:" + this.hotWordY[i]);
                        solveCollision(i, i2);
                        Log.v("Search", ((Object) this.mTextView[i].getText()) + "Solved X:" + this.hotWordX[i] + "Solved Y:" + this.hotWordY[i]);
                        Log.v("Search", "解决ed");
                    }
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isOutScreen(int i) {
        Paint paint = new Paint();
        paint.setTextSize(this.mTextView[i].getTextSize());
        float measureText = paint.measureText(this.mTextView[i].getText().toString());
        if (((float) this.hotWordX[i]) + measureText > ((float) (this.parentWidth / 2))) {
            Log.v("Search", "超出右边  " + this.hotWordX[i]);
            return true;
        } else if (((float) (this.hotWordX[i] + (this.parentWidth / 2))) - measureText < 0.0f) {
            Log.v("Search", "超出左边  " + this.hotWordX[i]);
            return true;
        } else if (this.hotWordY[i] + MAX_FONT_HEIGHT > this.parentHeight / 2) {
            Log.v("Search", "超出下边  " + this.hotWordY[i]);
            return true;
        } else if (this.hotWordY[i] - 25 >= (-this.parentHeight) / 2) {
            return false;
        } else {
            Log.v("Search", "超出上边  " + this.hotWordY[i]);
            return true;
        }
    }

    private boolean matcheApp(String str, ArrayList arrayList) {
        boolean z = false;
        ArrayList arrayList2 = this.mAppList;
        String str2 = ".*" + str + ".*";
        String str3 = str.matches(soundMotherReg) ? ".*" + str + ".*" : "^" + str + ".*";
        Pattern compile = Pattern.compile(str2, 2);
        Pattern compile2 = Pattern.compile(str3, 2);
        Iterator it = arrayList2.iterator();
        while (it.hasNext()) {
            am amVar = (am) ((ha) it.next());
            Matcher matcher = compile.matcher(amVar.a);
            Matcher matcher2 = compile2.matcher(amVar.b);
            if (matcher.find() || matcher2.find()) {
                z = true;
                arrayList.add(amVar);
            }
        }
        return z;
    }

    private synchronized void setHotwords(List list) {
        if (list != null) {
            this.mLinearLayout.removeAllViews();
            this.mLinearLayout.requestLayout();
            if (this.mTextView != null) {
                for (int length = this.mTextView.length - 1; length >= 0; length--) {
                    this.mTextView[length].clearAnimation();
                    this.mTextView[length].setText("");
                    this.mTextView[length] = null;
                }
            }
            this.mTextView = new Button[10];
            this.hotWordX = new int[10];
            this.hotWordY = new int[10];
            this.parentHeight = this.mLinearLayout.getHeight();
            this.parentWidth = this.mLinearLayout.getWidth();
            Random random = new Random();
            for (int i = 0; i < 10; i++) {
                this.mTextView[i] = new Button(this);
                this.mTextView[i].setText((CharSequence) list.get(i));
                this.mTextView[i].setBackgroundColor(getResources().getColor(R.color.background_for_all));
                this.mTextView[i].setTextColor(getResources().getColor(this.hotwordColor[i % this.hotwordColor.length]));
                this.mTextView[i].setDrawingCacheBackgroundColor(getResources().getColor(R.color.background_for_all));
                this.mTextView[i].setTextSize((float) this.hotwordSize[random.nextInt(this.hotwordSize.length)]);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams.addRule(13);
                this.mTextView[i].setOnClickListener(new at(this));
                this.mLinearLayout.addView(this.mTextView[i], layoutParams);
            }
            if (getResources().getConfiguration().orientation == 2) {
                for (int length2 = this.mTextView.length - 1; length2 >= 6; length2--) {
                    this.mTextView[length2].setVisibility(8);
                }
            }
            setOurSearchTextViewAnimationPos();
            startAnimation();
        }
    }

    private void setToOrigin(int i) {
        Log.v("Search", "从中心开始========================" + ((Object) this.mTextView[i].getText()));
        Rect[] centerRect = getCenterRect();
        for (int i2 = 0; i2 < centerRect.length; i2++) {
            Log.v("Search", "current rect:" + i2 + "left:" + centerRect[i2].left + "top:" + centerRect[i2].top);
            this.hotWordX[i] = centerRect[i2].left;
            this.hotWordY[i] = centerRect[i2].top;
            if (!isOutScreen(i)) {
                int i3 = 0;
                while (i3 < this.mTextView.length && (i3 == i || !isCollision(i, i3, false))) {
                    i3++;
                }
                if (i3 == this.mTextView.length) {
                    return;
                }
            }
        }
        Log.v("Search", ((Object) this.mTextView[i].getText()) + " location error!!!!!!!!!!!!!!!!!!!!!");
        this.mTextView[i].setText("");
        this.mTextView[i].setVisibility(8);
    }

    private boolean showResults(String str) {
        if (str == null || "".equals(str)) {
            return false;
        }
        if (TextUtils.getTrimmedLength(str) == 0) {
            return false;
        }
        String replaceAll = str.replaceAll(" ", "");
        ArrayList arrayList = this.mMacheList;
        arrayList.clear();
        boolean matcheApp = matcheApp(replaceAll, arrayList);
        this.listAdapter.notifyDataSetChanged();
        if (!matcheApp) {
            this.textView.setVisibility(0);
        } else {
            this.textView.setVisibility(8);
        }
        return true;
    }

    private void solveCollision(int i, int i2) {
        int abs = Math.abs(this.hotWordY[i] - this.hotWordY[i2]);
        if (this.hotWordY[i] < this.hotWordY[i2]) {
            Log.v("Search", "hotWordY[self] < hotWordY[other]");
            Log.v("Search", "Old Y:" + this.hotWordY[i]);
            int[] iArr = this.hotWordY;
            iArr[i] = iArr[i] - (MAX_FONT_HEIGHT - abs);
            Log.v("Search", "new Y:" + this.hotWordY[i]);
        } else {
            Log.v("Search", "hotWordY[self] > hotWordY[other]");
            Log.v("Search", "Old Y:" + this.hotWordY[i]);
            int[] iArr2 = this.hotWordY;
            iArr2[i] = (MAX_FONT_HEIGHT - abs) + iArr2[i];
            Log.v("Search", "new Y:" + this.hotWordY[i]);
        }
        if (isOutScreen(i)) {
            Log.v("Search", "超出了屏幕");
            setToOrigin(i);
            return;
        }
        Log.v("Search", "未超出屏幕,调整后再比较");
        int i3 = 0;
        while (i3 < this.mTextView.length) {
            if (i3 == i || !isCollision(i, i3, false)) {
                i3++;
            } else {
                setToOrigin(i);
                return;
            }
        }
    }

    private synchronized void startAnimation() {
        for (int i = 0; i < this.mTextView.length; i++) {
            AnimationSet animationSet = new AnimationSet(false);
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setInterpolator(new DecelerateInterpolator(1.0f));
            alphaAnimation.setDuration(1000);
            TranslateAnimation translateAnimation = new TranslateAnimation(0, 0.0f, 0, (float) this.hotWordX[i], 0, 0.0f, 0, (float) this.hotWordY[i]);
            translateAnimation.setInterpolator(new DecelerateInterpolator(1.0f));
            translateAnimation.setDuration(800);
            translateAnimation.setFillEnabled(true);
            translateAnimation.setFillAfter(true);
            alphaAnimation.setFillEnabled(true);
            alphaAnimation.setFillAfter(true);
            animationSet.addAnimation(translateAnimation);
            animationSet.addAnimation(alphaAnimation);
            animationSet.setAnimationListener(new fm(this, this.mTextView[i], i));
            this.mTextView[i].startAnimation(animationSet);
        }
        RotateAnimation rotateAnimation = new RotateAnimation(0.0f, 1080.0f, (float) (this.centerIcon.getWidth() / 2), (float) (this.centerIcon.getHeight() / 2));
        rotateAnimation.setDuration(1000);
        rotateAnimation.setInterpolator(new DecelerateInterpolator(1.2f));
        this.centerIcon.startAnimation(rotateAnimation);
    }

    public void afterTextChanged(Editable editable) {
        if (!this.bSearchType) {
            if (editable.length() == 0) {
                this.textView.setVisibility(8);
                this.marketSearch.setVisibility(8);
                this.appCenterSearch.setVisibility(8);
                this.mMacheList.clear();
                this.listAdapter.notifyDataSetChanged();
                return;
            }
            this.marketSearch.setVisibility(0);
            this.appCenterSearch.setVisibility(0);
            showResults(getTextFilter());
        }
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void dismissProgressDialog() {
        ((ProgressBar) findViewById(R.id.loading)).setVisibility(8);
        this.mBtn.setEnabled(true);
    }

    /* access modifiers changed from: package-private */
    public void initFrame() {
        getApplicationContext().getSystemService("layout_inflater");
        this.hotwordsView = findViewById(R.id.hotwords_view);
        this.mBtn = (TextView) findViewById(R.id.WaitingBtn);
        this.mBtn.setOnClickListener(new as(this));
        this.mLinearLayout = (RelativeLayout) findViewById(R.id.SearchResultLayout);
        this.centerIcon = (ImageView) findViewById(R.id.center_icon);
        this.centerIcon.setAlpha(44);
    }

    public void onClick(View view) {
        if (view.getId() == R.id.search_market) {
            String textFilter = getTextFilter();
            if (textFilter != null && !"".equals(textFilter) && TextUtils.getTrimmedLength(textFilter) != 0) {
                try {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=" + textFilter)));
                } catch (ActivityNotFoundException e) {
                }
            }
        } else if (view.getId() == R.id.search_app_center) {
            String textFilter2 = getTextFilter();
            if (textFilter2 != null && !"".equals(textFilter2) && TextUtils.getTrimmedLength(textFilter2) != 0) {
                try {
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("http://a.app.qq.com/g/s?aid=searchsoft_a&g_f=990233&softname=" + URLEncoder.encode(textFilter2, "utf-8")));
                    intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
                    startActivity(intent);
                } catch (ActivityNotFoundException e2) {
                } catch (UnsupportedEncodingException e3) {
                    e3.printStackTrace();
                }
            }
        } else if (view == this.delSearchBtn && this.mSearchEditText != null) {
            this.mSearchEditText.setText("");
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.search_application_reserved);
        InitialCommonUI();
        InitialLocalSearch();
        getAllApp();
    }

    public boolean onEditorAction(TextView textView2, int i, KeyEvent keyEvent) {
        if (i != 6) {
            return false;
        }
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.mListView.getWindowToken(), 0);
        if (TextUtils.isEmpty(getTextFilter())) {
            finish();
        }
        return true;
    }

    public void onFocusChange(View view, boolean z) {
        if (view == this.mListView && z) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.mListView.getWindowToken(), 0);
        }
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        return false;
    }

    public void onReceiveHotWords(List list) {
        if (this.mNetSearchListView != null) {
            this.mNetSearchListView.setVisibility(8);
        }
        this.mLinearLayout.setVisibility(0);
        this.mBtn.setVisibility(0);
        if (this.hotwordsCache == null) {
            this.hotwordsCache = new ArrayList();
        }
        if (!isCachedHotwards(list)) {
            this.hotwordsCache.add((ArrayList) list);
        }
        setHotwords(list);
        setHotwordResult();
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (view == getListView()) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.mListView.getWindowToken(), 0);
        }
        return false;
    }

    public void setHotwordResult() {
        this.mLinearLayout.setVisibility(0);
        this.mBtn.setVisibility(0);
        this.hotwordsView.setVisibility(0);
    }

    /* access modifiers changed from: package-private */
    public void setOurSearchTextViewAnimationPos() {
        this.parentHeight = this.mLinearLayout.getHeight();
        this.parentWidth = this.mLinearLayout.getWidth();
        this.unitH = this.parentHeight / 6;
        this.unitW = this.parentWidth / 6;
        Rect[] calculateRegin = calculateRegin();
        ArrayList arrayList = new ArrayList();
        for (Integer num = 0; num.intValue() < RADIO_LINE_NAMEBER; num = Integer.valueOf(num.intValue() + 1)) {
            arrayList.add(num);
        }
        for (int i = 0; i < this.mTextView.length; i++) {
            Random random = new Random();
            int nextInt = random.nextInt(arrayList.size());
            int intValue = ((Integer) arrayList.get(nextInt)).intValue();
            arrayList.remove(nextInt);
            this.hotWordX[i] = random.nextInt(this.unitW) + calculateRegin[intValue].left;
            this.hotWordY[i] = calculateRegin[intValue].top + random.nextInt(this.unitH);
            this.hotWordY[i] = this.hotWordY[i] + (this.unitH / 2);
            this.hotWordX[i] = this.hotWordX[i] + (this.unitW / 2);
        }
        for (int i2 = 0; i2 < this.hotWordX.length; i2++) {
            Paint paint = new Paint();
            paint.setTextSize(this.mTextView[i2].getTextSize());
            float measureText = paint.measureText(this.mTextView[i2].getText().toString());
            if (((float) this.hotWordX[i2]) + measureText > ((float) (this.parentWidth / 2))) {
                this.hotWordX[i2] = (int) (((double) (this.parentWidth / 2)) - (((double) measureText) * 1.5d));
            }
            if (((float) this.hotWordX[i2]) - measureText < ((float) ((-this.parentWidth) / 2))) {
                this.hotWordX[i2] = (int) (((float) ((-this.parentWidth) / 2)) + measureText);
            }
            if (this.hotWordY[i2] + MAX_FONT_HEIGHT > this.parentHeight / 2) {
                this.hotWordY[i2] = this.hotWordY[i2] - MAX_FONT_HEIGHT;
            }
            if (this.hotWordY[i2] - 25 < (-this.parentHeight) / 2) {
                this.hotWordY[i2] = this.hotWordY[i2] + 25;
            }
            Log.v(TAG, ((Object) this.mTextView[i2].getText()) + " width:" + measureText + "X: " + this.hotWordX[i2]);
            Log.v(TAG, ((Object) this.mTextView[i2].getText()) + " height:" + ((int) MAX_FONT_HEIGHT) + "Y: " + this.hotWordY[i2]);
            Log.v("Search", ((Object) this.mTextView[i2].getText()) + " 开始检测");
            for (int i3 = 0; i3 < this.hotWordX.length; i3++) {
                if (i2 != i3) {
                    isCollision(i2, i3, true);
                }
            }
            Log.v("Search", ((Object) this.mTextView[i2].getText()) + " 结束检测");
            Log.v(TAG, ((Object) this.mTextView[i2].getText()) + "final width:" + measureText + "X: " + this.hotWordX[i2]);
            Log.v(TAG, ((Object) this.mTextView[i2].getText()) + "final height:" + ((int) MAX_FONT_HEIGHT) + "Y: " + this.hotWordY[i2]);
        }
    }

    public void setSearchResult() {
        this.mLinearLayout.setVisibility(8);
        this.mBtn.setVisibility(8);
        this.hotwordsView.setVisibility(8);
    }

    public void setWaitScreen() {
        ((ProgressBar) findViewById(R.id.loading)).setVisibility(0);
        this.mBtn.setEnabled(false);
    }
}
