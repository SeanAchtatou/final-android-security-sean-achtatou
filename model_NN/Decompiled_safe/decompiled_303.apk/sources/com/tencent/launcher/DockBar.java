package com.tencent.launcher;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Toast;
import com.tencent.a.a;
import com.tencent.launcher.base.BaseApp;
import com.tencent.launcher.base.b;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;
import java.util.Iterator;

public class DockBar extends ViewGroup implements View.OnLongClickListener, cm, e, hs {
    private static final int s = ((int) (15.0f * b.b));
    private static final int t = ((int) (4.0f * b.b));
    private f A;
    private int B;
    private Runnable C;
    private Runnable D;
    private Animation.AnimationListener E;
    private boolean F;
    public ha a;
    boolean b;
    private boolean c;
    /* access modifiers changed from: private */
    public Launcher d;
    private Workspace e;
    private boolean f;
    private dt g;
    private int h;
    private int i;
    private int j;
    private int k;
    private int l;
    private ha m;
    private View n;
    private DecelerateInterpolator o;
    /* access modifiers changed from: private */
    public int p;
    /* access modifiers changed from: private */
    public int q;
    private boolean r;
    private Paint u;
    private Matrix v;
    private long w;
    private int x;
    private int y;
    private boolean z;

    public class LayoutParams extends ViewGroup.MarginLayoutParams {
        public int a;
        public int b;
        int c;
        int d;

        public LayoutParams(int i) {
            super(-1, -1);
            this.a = i;
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }
    }

    public DockBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public DockBar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f = true;
        this.h = 5;
        this.i = 56;
        this.j = 56;
        this.k = 5;
        this.l = 5;
        this.o = new DecelerateInterpolator();
        this.u = new Paint();
        this.v = new Matrix();
        this.x = 3;
        this.y = 3;
        this.z = false;
        this.b = false;
        this.C = new ey(this);
        this.D = new fb(this);
        this.E = new fa(this);
        this.F = false;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.e, i2, 0);
        this.c = obtainStyledAttributes.getInt(0, 1) == 1;
        this.h = obtainStyledAttributes.getInt(3, this.h);
        this.k = obtainStyledAttributes.getDimensionPixelSize(4, this.k);
        this.l = obtainStyledAttributes.getDimensionPixelSize(5, this.l);
        obtainStyledAttributes.recycle();
        this.u.setAlpha(30);
    }

    private View a(int i2, int i3) {
        boolean z2 = this.c;
        int childCount = getChildCount();
        int scrollX = z2 ? getScrollX() + i2 : getScrollY() + i3;
        for (int i4 = 0; i4 < childCount; i4++) {
            View childAt = getChildAt(i4);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            if (childAt.getVisibility() != 8) {
                int i5 = this.k + (layoutParams.a * (z2 ? this.i : this.j));
                int i6 = (z2 ? this.i : this.j) + i5;
                if (scrollX < i5 && layoutParams.a == 0) {
                    return childAt;
                }
                if (scrollX >= i6 && layoutParams.a == this.h - 1) {
                    return childAt;
                }
                if (scrollX >= i5 && scrollX < i6) {
                    return childAt;
                }
            }
        }
        return null;
    }

    private void a(View view) {
        View a2;
        View a3;
        if (view.getTag() == null) {
            this.n = view;
            return;
        }
        int i2 = ((LayoutParams) view.getLayoutParams()).a;
        int i3 = this.h;
        if (i2 <= 2) {
            int i4 = 0;
            while (true) {
                if (i4 < i3) {
                    if (i4 != 2 && (a2 = a(i4)) != null && a2.getTag() == null) {
                        this.n = a2;
                        break;
                    }
                    i4++;
                } else {
                    break;
                }
            }
        } else {
            int i5 = i3 - 1;
            while (true) {
                if (i5 >= 0) {
                    if (i5 != 2 && (a3 = a(i5)) != null && a3.getTag() == null) {
                        this.n = a3;
                        break;
                    }
                    i5--;
                } else {
                    break;
                }
            }
        }
        this.B = i2;
        removeCallbacks(this.C);
        postDelayed(this.C, 50);
    }

    private void a(cm cmVar, boolean z2, Object obj) {
        ff model = Launcher.getModel();
        ha haVar = (ha) obj;
        switch (haVar.m) {
            case 0:
            case 1:
                if (z2 || haVar.n == -1 || haVar.n == -300 || ((cmVar instanceof UserFolder) && ((UserFolder) cmVar).f.n == -300)) {
                    this.m = new am((am) haVar);
                    this.m.n = -1;
                }
                if (this.m != null) {
                    ((am) this.m).f = false;
                    break;
                }
                break;
            case 2:
                if (z2 || haVar.n == -1 || haVar.n == -300) {
                    this.m = new bq((bq) haVar);
                    Iterator it = ((bq) haVar).b.iterator();
                    while (it.hasNext()) {
                        am amVar = new am((am) it.next());
                        amVar.c.setAction("android.intent.action.MAIN");
                        ((bq) this.m).b.add(amVar);
                    }
                    ((bq) this.m).j = null;
                    this.m.n = -1;
                    break;
                }
            case 3:
                break;
            case 4:
            default:
                return;
        }
        if (this.n != null) {
            if (this.a == null || this.a.m != 2 || (this.m.m != 0 && this.m.m != 1)) {
                ((DockView) this.n).a(this.m);
                model.a(this.m);
                BaseApp.d().post(new ez(this));
            } else if (((bq) this.a).b.size() >= 12) {
                Toast.makeText(getContext(), (int) R.string.folder_is_full, 0).show();
                haVar.v = false;
                return;
            } else if (!(this.m instanceof am) || !((bq) this.a).b((am) this.m)) {
                ff.a(this.d, this.m, this.a.l, 0, 0, 0);
                ((bq) this.a).a((am) this.m);
                ((DockView) this.n).a(this.a);
                this.a = null;
            } else {
                Toast.makeText(getContext(), (int) R.string.folder_contain_item, 0).show();
                haVar.v = false;
                return;
            }
            if (haVar.n == -300) {
                if (haVar.m == 2) {
                    BaseApp.d().post(new Cdo(this, (bq) this.m));
                    model.a((ex) this.m);
                }
                if (this.a != null) {
                    ff.g(this.d, this.a);
                    this.a = null;
                }
            }
            this.m = null;
            this.n = null;
            this.b = true;
            return;
        }
        haVar.v = false;
    }

    private Animation b(int i2, int i3) {
        int i4;
        int i5;
        if (i3 == i2) {
            return null;
        }
        if (this.c) {
            i5 = (i3 - i2) * this.i;
            i4 = 0;
        } else {
            i4 = (i3 - i2) * this.j;
            i5 = 0;
        }
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, (float) i5, 0.0f, (float) i4);
        translateAnimation.setInterpolator(this.o);
        translateAnimation.setDuration(200);
        translateAnimation.setAnimationListener(this.E);
        translateAnimation.setFillBefore(false);
        translateAnimation.setFillAfter(true);
        return translateAnimation;
    }

    static /* synthetic */ void b(DockBar dockBar) {
        boolean z2;
        View view = dockBar.n;
        if (dockBar.n != null) {
            int i2 = dockBar.B;
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            dockBar.p = 0;
            for (int i3 = 0; i3 < dockBar.getChildCount(); i3++) {
                View childAt = dockBar.getChildAt(i3);
                if (childAt != view) {
                    LayoutParams layoutParams2 = (LayoutParams) childAt.getLayoutParams();
                    int i4 = layoutParams2.a;
                    if (layoutParams.a > i2) {
                        if (layoutParams2.a >= i2 && layoutParams2.a <= layoutParams.a) {
                            layoutParams2.a++;
                            if (layoutParams2.a == 2) {
                                layoutParams2.a++;
                            }
                            dockBar.p++;
                            z2 = true;
                        }
                        z2 = false;
                    } else {
                        if (layoutParams2.a <= i2 && layoutParams2.a >= layoutParams.a) {
                            layoutParams2.a--;
                            if (layoutParams2.a == 2) {
                                layoutParams2.a--;
                            }
                            dockBar.p++;
                            z2 = true;
                        }
                        z2 = false;
                    }
                    if (z2) {
                        Animation b2 = dockBar.b(i4, layoutParams2.a);
                        b2.setStartOffset(((long) (Math.abs(layoutParams.a - i4) - 1)) * 20);
                        childAt.startAnimation(b2);
                    }
                }
            }
            if (dockBar.p > 0) {
                dockBar.r = true;
            }
            layoutParams.a = i2;
        }
    }

    static /* synthetic */ void c(DockBar dockBar) {
        int i2 = 0;
        View view = dockBar.n;
        if (dockBar.n != null) {
            dockBar.p = 0;
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            while (true) {
                int i3 = i2;
                if (i3 >= dockBar.getChildCount()) {
                    break;
                }
                View childAt = dockBar.getChildAt(i3);
                LayoutParams layoutParams2 = (LayoutParams) childAt.getLayoutParams();
                if (!(childAt == view || layoutParams2.a == layoutParams2.b)) {
                    int i4 = layoutParams2.a;
                    layoutParams2.a = layoutParams2.b;
                    if (childAt.getTag() != null) {
                        dockBar.p++;
                        Animation b2 = dockBar.b(i4, layoutParams2.b);
                        b2.setStartOffset(((long) (Math.abs(layoutParams.a - i4) - 1)) * 20);
                        childAt.startAnimation(b2);
                    }
                }
                i2 = i3 + 1;
            }
            if (dockBar.p > 0) {
                dockBar.r = true;
            }
            if (layoutParams.a != layoutParams.b) {
                layoutParams.a = layoutParams.b;
            }
        }
    }

    static /* synthetic */ int d(DockBar dockBar) {
        int i2 = dockBar.q;
        dockBar.q = i2 + 1;
        return i2;
    }

    private int f() {
        int childCount = getChildCount();
        int i2 = 0;
        int i3 = 0;
        while (i2 < childCount) {
            int i4 = ((DockView) getChildAt(i2)).getTag() == null ? i3 + 1 : i3;
            i2++;
            i3 = i4;
        }
        return i3;
    }

    private void g() {
        if (this.z) {
            this.z = false;
            for (int i2 = 0; i2 < getChildCount(); i2++) {
                getChildAt(i2).setVisibility(0);
            }
        }
    }

    static /* synthetic */ void g(DockBar dockBar) {
        dockBar.requestLayout();
        dockBar.r = false;
        dockBar.q = 0;
    }

    public final View a(int i2) {
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            if (((LayoutParams) childAt.getLayoutParams()).a == i2) {
                return childAt;
            }
        }
        return null;
    }

    public final void a() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            DockView dockView = (DockView) getChildAt(i2);
            if (!dockView.isShown()) {
                dockView.setVisibility(0);
            }
        }
    }

    public final void a(View view, cm cmVar, Object obj, int i2) {
        b();
    }

    public final void a(View view, boolean z2) {
        if (!(!z2 || view == this || this.n == null)) {
            if (!(view instanceof DeleteZone) || ((DeleteZone) view).a) {
                ((DockView) this.n).a((ha) null);
                Launcher.getModel().b((ha) this.n.getTag());
            } else {
                return;
            }
        }
        this.n = null;
    }

    /* access modifiers changed from: package-private */
    public final void a(Launcher launcher) {
        this.d = launcher;
    }

    /* access modifiers changed from: package-private */
    public final void a(Workspace workspace) {
        this.e = workspace;
    }

    public final void a(cm cmVar, boolean z2) {
        View view = this.n;
        if (cmVar != this) {
            if (view != null && ((DockView) view).b()) {
                ((DockView) view).a(false);
            } else if (view != null) {
                ((DockView) view).g();
            }
        }
        if (view != null && z2) {
            removeCallbacks(this.C);
            removeCallbacks(this.D);
            postDelayed(this.D, 100);
        }
    }

    public final void a(dt dtVar) {
        this.g = dtVar;
    }

    public final void a(f fVar) {
        this.A = fVar;
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        boolean z2;
        ff model = Launcher.getModel();
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            DockView dockView = (DockView) getChildAt(i2);
            Object tag = dockView.getTag();
            if (tag instanceof am) {
                am amVar = (am) tag;
                ComponentName component = amVar.c.getComponent();
                if (component != null && str.equals(component.getPackageName())) {
                    model.b(amVar);
                    ff.g(this.d, amVar);
                    dockView.a((ha) null);
                }
            } else if (tag instanceof bq) {
                bq bqVar = (bq) tag;
                ArrayList arrayList = bqVar.b;
                ArrayList arrayList2 = new ArrayList(1);
                int size = arrayList.size();
                int i3 = 0;
                boolean z3 = false;
                while (i3 < size) {
                    am amVar2 = (am) arrayList.get(i3);
                    ComponentName component2 = amVar2.c.getComponent();
                    if (component2 == null || !str.equals(component2.getPackageName())) {
                        z2 = z3;
                    } else {
                        arrayList2.add(amVar2);
                        ff.g(this.d, amVar2);
                        z2 = true;
                    }
                    i3++;
                    z3 = z2;
                }
                arrayList.removeAll(arrayList2);
                if (z3) {
                    dockView.a(bqVar);
                }
            }
        }
    }

    public final boolean a(int i2, Object obj) {
        return true;
    }

    public final boolean a(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
        if (cmVar == this) {
            BaseApp.d().post(new ez(this));
        } else if ((cmVar instanceof AllAppsListView) || ((cmVar instanceof UserFolder) && ((UserFolder) cmVar).f.j != null)) {
            a(cmVar, true, obj);
        } else {
            a(cmVar, false, obj);
        }
        return true;
    }

    public final boolean a(cm cmVar, Object obj) {
        return true;
    }

    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        view.setFocusable(true);
        view.setLongClickable(true);
        view.setOnLongClickListener(this);
        super.addView(view, layoutParams);
    }

    public final void b() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            DockView dockView = (DockView) getChildAt(i2);
            if (dockView.getTag() == null) {
                dockView.setVisibility(4);
            }
            int childCount2 = getChildCount();
            for (int i3 = 0; i3 < childCount2; i3++) {
                LayoutParams layoutParams = (LayoutParams) getChildAt(i3).getLayoutParams();
                layoutParams.b = layoutParams.a;
            }
        }
    }

    public final void b(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
        if (cmVar != this) {
            if (cmVar instanceof UserFolder) {
                this.d.shadeViewsReserve((UserFolder) cmVar);
                this.e.p();
            }
            ha haVar = (ha) obj;
            this.b = false;
            switch (haVar.m) {
                case 0:
                case 1:
                case 2:
                case 3:
                    View a2 = a(i2, i3);
                    if (a2 != null) {
                        if (f() > 0) {
                            a(a2);
                            this.a = null;
                        } else {
                            this.n = a2;
                            this.a = (ha) this.n.getTag();
                            if (this.a.m != 2 || haVar.m == 2 || haVar.m == 3) {
                                ((DockView) this.n).a(true);
                            } else {
                                ((DockView) this.n).f();
                            }
                        }
                        this.m = haVar;
                        return;
                    }
                    return;
                case 4:
                default:
                    return;
            }
        } else {
            LayoutParams layoutParams = (LayoutParams) this.n.getLayoutParams();
            View a3 = a(i2, i3);
            if (!(a3 == null || this.n == null || this.n == a3 || a3.getTag() != null)) {
                LayoutParams layoutParams2 = (LayoutParams) a3.getLayoutParams();
                int i6 = layoutParams.a;
                int i7 = layoutParams.b;
                layoutParams.a = layoutParams2.a;
                layoutParams.b = layoutParams2.b;
                layoutParams2.a = i6;
                layoutParams2.b = i7;
                requestLayout();
            }
            this.B = layoutParams.a;
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(String str) {
        boolean z2;
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            DockView dockView = (DockView) getChildAt(i2);
            Object tag = dockView.getTag();
            if (tag != null && (tag instanceof am)) {
                am amVar = (am) tag;
                Intent intent = amVar.c;
                ComponentName component = intent == null ? null : intent.getComponent();
                if ((amVar.m == 0 || amVar.m == 1) && "android.intent.action.MAIN".equals(intent.getAction()) && component != null && str.equals(component.getPackageName())) {
                    Drawable a2 = Launcher.getModel().a(this.d.getPackageManager(), amVar);
                    if (a2 == null || a2 == amVar.d) {
                        amVar.d = a.b(getContext(), this.d.getPackageManager().getDefaultActivityIcon(), amVar);
                    } else {
                        if (amVar.d != null) {
                            amVar.d.setCallback(null);
                        }
                        amVar.d = a.b(getContext(), a2, amVar);
                    }
                    dockView.a(amVar);
                }
            } else if (tag != null && (tag instanceof bq)) {
                bq bqVar = (bq) tag;
                Iterator it = bqVar.b.iterator();
                while (it.hasNext()) {
                    am amVar2 = (am) it.next();
                    if (amVar2 == null || amVar2.c == null || amVar2.c.getComponent() == null || !amVar2.c.getComponent().getPackageName().equals(str)) {
                        z2 = false;
                    } else {
                        Drawable a3 = Launcher.getModel().a(this.d.getPackageManager(), amVar2);
                        if (a3 == null || a3 == amVar2.d) {
                            amVar2.d = a.b(getContext(), this.d.getPackageManager().getDefaultActivityIcon(), bqVar);
                        } else {
                            if (amVar2.d != null) {
                                amVar2.d.setCallback(null);
                            }
                            amVar2.d = a.b(getContext(), a3, bqVar);
                        }
                        z2 = true;
                    }
                    if (z2) {
                        dockView.c();
                    }
                }
            }
        }
    }

    public final void c() {
        this.F = true;
        if (getVisibility() == 0) {
            this.x = 1;
            this.y = 3;
            g();
            invalidate();
        }
    }

    public final void c(cm cmVar, int i2, int i3, int i4, int i5, Object obj) {
        int i6;
        View view = this.n;
        int f2 = f();
        if (cmVar == this || f2 > 0) {
            View a2 = a(i2, i3);
            if (a2 != null) {
                if (view == null) {
                    a(a2);
                    this.m = (ha) obj;
                    this.a = null;
                } else if (view == a2 || a2.getTag() != null) {
                    LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
                    LayoutParams layoutParams2 = (LayoutParams) ((DockView) view).getLayoutParams();
                    boolean z2 = this.c;
                    int childCount = getChildCount();
                    int scrollX = z2 ? getScrollX() + i2 : getScrollY() + i3;
                    int i7 = 0;
                    while (true) {
                        if (i7 >= childCount) {
                            i6 = 2;
                            break;
                        }
                        View childAt = getChildAt(i7);
                        LayoutParams layoutParams3 = (LayoutParams) childAt.getLayoutParams();
                        if (childAt.getVisibility() != 8) {
                            int i8 = this.k + (layoutParams3.a * (z2 ? this.i : this.j));
                            int i9 = (z2 ? this.i : this.j) + i8;
                            if (scrollX >= i8 || layoutParams3.a != 0) {
                                if (scrollX >= i9 && layoutParams3.a == this.h - 1) {
                                    i6 = this.h - 1;
                                    break;
                                } else if (scrollX >= i8 && scrollX < i9) {
                                    int i10 = layoutParams3.a;
                                    if (childAt != view) {
                                        int i11 = ((z2 ? this.i : this.j) / 2) + i8;
                                        if (scrollX >= i11 && layoutParams2.a > layoutParams3.a) {
                                            i6 = i10 + 1 > this.h - 1 ? this.h - 1 : i10 + 1;
                                        } else if (scrollX <= i11 && layoutParams2.a < layoutParams3.a) {
                                            i6 = i10 - 1 < 0 ? 0 : i10 - 1;
                                        }
                                    }
                                    i6 = i10 > this.h - 1 ? this.h - 1 : i10;
                                }
                            } else {
                                i6 = 0;
                                break;
                            }
                        }
                        i7++;
                    }
                    if (layoutParams != null && layoutParams.a != i6 && i6 != 2 && this.B != i6) {
                        this.B = i6;
                        removeCallbacks(this.D);
                        removeCallbacks(this.C);
                        postDelayed(this.C, 250);
                    }
                } else {
                    removeCallbacks(this.C);
                    LayoutParams layoutParams4 = (LayoutParams) a2.getLayoutParams();
                    LayoutParams layoutParams5 = (LayoutParams) view.getLayoutParams();
                    int i12 = layoutParams5.a;
                    int i13 = layoutParams5.b;
                    layoutParams5.a = layoutParams4.a;
                    layoutParams5.b = layoutParams4.b;
                    layoutParams4.a = i12;
                    layoutParams4.b = i13;
                }
            }
        } else {
            View a3 = a(i2, i3);
            if (a3 != view) {
                if (view != null && ((DockView) view).b()) {
                    ((DockView) view).a(false);
                } else if (view != null) {
                    ((DockView) view).g();
                }
                if (a3 != null) {
                    this.a = (ha) a3.getTag();
                    if (this.m == null) {
                        this.m = (ha) obj;
                    }
                    if ((this.a != null && this.a.m != 2) || this.m.m == 2 || this.m.m == 3) {
                        ((DockView) a3).a(true);
                    } else {
                        ((DockView) a3).f();
                    }
                }
                this.n = a3;
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public final boolean d() {
        return this.x == 3 && this.y == 3;
    }

    /* access modifiers changed from: protected */
    public void dispatchSetPressed(boolean z2) {
        if (!z2) {
            super.dispatchSetPressed(z2);
        }
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (this.F) {
            return true;
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public boolean drawChild(Canvas canvas, View view, long j2) {
        if (this.z) {
            return true;
        }
        int paddingLeft = ((LayoutParams) view.getLayoutParams()).c + view.getPaddingLeft();
        int a2 = ((DockView) view).a() + view.getTop() + 1;
        if (this.x != 3 && this.y == 3) {
            if (this.x == 1) {
                this.w = SystemClock.uptimeMillis();
                this.x = 2;
            }
            float uptimeMillis = ((float) (SystemClock.uptimeMillis() - this.w)) / 200.0f;
            if (uptimeMillis >= 1.0f) {
                this.x = 3;
                if (!this.z) {
                    this.z = true;
                    for (int i2 = 0; i2 < getChildCount(); i2++) {
                        getChildAt(i2).setVisibility(4);
                    }
                }
                if (this.A != null) {
                    this.A.a(this);
                }
                return true;
            }
            float height = ((float) view.getHeight()) * Math.min(uptimeMillis, 1.0f);
            canvas.save();
            Canvas canvas2 = canvas;
            canvas2.saveLayerAlpha((float) view.getLeft(), (float) a2, (float) view.getRight(), (float) (getHeight() - t), 30, 20);
            canvas.scale(1.0f, -1.0f, (float) (view.getLeft() + (view.getWidth() / 2)), (float) a2);
            canvas.translate(0.0f, height);
            super.drawChild(canvas, view, j2);
            canvas.restore();
            canvas.restore();
            canvas.save();
            canvas.clipRect(paddingLeft, view.getTop(), view.getRight(), a2);
            canvas.translate(0.0f, height);
            super.drawChild(canvas, view, j2);
            canvas.restore();
            invalidate();
            return true;
        } else if (this.y == 3 || this.x != 3) {
            canvas.save();
            Canvas canvas3 = canvas;
            canvas3.saveLayerAlpha((float) view.getLeft(), (float) a2, (float) view.getRight(), (float) (getHeight() - t), 30, 20);
            canvas.scale(1.0f, -1.0f, (float) (view.getLeft() + (view.getWidth() / 2)), (float) a2);
            super.drawChild(canvas, view, j2);
            canvas.restore();
            canvas.restore();
            return super.drawChild(canvas, view, j2);
        } else {
            if (this.y == 1) {
                this.w = SystemClock.uptimeMillis();
                this.y = 2;
            }
            float uptimeMillis2 = ((float) (SystemClock.uptimeMillis() - this.w)) / 200.0f;
            if (uptimeMillis2 >= 1.0f) {
                this.y = 3;
                invalidate();
                return super.drawChild(canvas, view, j2);
            }
            float height2 = ((float) view.getHeight()) * (1.0f - Math.min(uptimeMillis2, 1.0f));
            canvas.save();
            Canvas canvas4 = canvas;
            canvas4.saveLayerAlpha((float) view.getLeft(), (float) a2, (float) view.getRight(), (float) (getHeight() - t), 30, 20);
            canvas.scale(1.0f, -1.0f, (float) (view.getLeft() + (view.getWidth() / 2)), (float) a2);
            canvas.translate(0.0f, height2);
            super.drawChild(canvas, view, j2);
            canvas.restore();
            canvas.restore();
            canvas.save();
            canvas.clipRect(paddingLeft, view.getTop(), view.getRight(), a2);
            canvas.translate(0.0f, height2);
            super.drawChild(canvas, view, j2);
            canvas.restore();
            invalidate();
            return true;
        }
    }

    public final void e() {
        this.F = false;
        if (this.x == 3) {
            g();
        } else if (this.A != null) {
            this.A.a(this);
        }
        this.y = 1;
        this.x = 3;
        invalidate();
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new LayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            childAt.clearAnimation();
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                int i7 = layoutParams.c;
                int i8 = layoutParams.d;
                childAt.layout(i7, i8, layoutParams.width + i7, layoutParams.height + i8);
            }
        }
    }

    public boolean onLongClick(View view) {
        if (!view.isInTouchMode()) {
            return false;
        }
        ha haVar = (ha) view.getTag();
        if (haVar != null) {
            this.n = view;
            this.g.a(view, this, haVar, 0);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        if (mode == 0 || mode2 == 0) {
            throw new RuntimeException("dockbar cannot have UNSPECIFIED dimensions");
        }
        if (this.f) {
            int i4 = this.h;
            if (this.c) {
                this.i = ((size - this.k) - this.l) / i4;
                this.j = size2 - s;
            } else {
                this.i = size - s;
                this.j = ((size2 - this.k) - this.l) / i4;
            }
            this.f = false;
        }
        int i5 = this.i;
        int i6 = this.j;
        int childCount = getChildCount();
        for (int i7 = 0; i7 < childCount; i7++) {
            View childAt = getChildAt(i7);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            boolean z2 = this.c;
            int i8 = this.k;
            int i9 = layoutParams.a;
            layoutParams.width = (i5 - layoutParams.leftMargin) - layoutParams.rightMargin;
            layoutParams.height = (i6 - layoutParams.topMargin) - layoutParams.bottomMargin;
            layoutParams.c = z2 ? (i9 * i5) + i8 + layoutParams.leftMargin : layoutParams.leftMargin;
            layoutParams.d = z2 ? layoutParams.topMargin : (i9 * i6) + i8 + layoutParams.topMargin;
            childAt.measure(View.MeasureSpec.makeMeasureSpec(layoutParams.width, 1073741824), View.MeasureSpec.makeMeasureSpec(layoutParams.height, 1073741824));
        }
        setMeasuredDimension(size, size2);
    }

    /* access modifiers changed from: protected */
    public void setChildrenDrawnWithCacheEnabled(boolean z2) {
        super.setChildrenDrawnWithCacheEnabled(z2);
    }
}
