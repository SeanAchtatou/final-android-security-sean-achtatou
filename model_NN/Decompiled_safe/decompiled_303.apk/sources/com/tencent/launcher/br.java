package com.tencent.launcher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.tencent.launcher.home.i;
import com.tencent.module.theme.l;

final class br extends BroadcastReceiver {
    private /* synthetic */ Launcher a;

    /* synthetic */ br(Launcher launcher) {
        this(launcher, (byte) 0);
    }

    private br(Launcher launcher, byte b) {
        this.a = launcher;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.b(android.content.Context, java.lang.String):boolean
     arg types: [com.tencent.launcher.Launcher, java.lang.String]
     candidates:
      com.tencent.launcher.ff.b(java.util.HashMap, long):com.tencent.launcher.dq
      com.tencent.launcher.ff.b(com.tencent.launcher.ff, java.util.ArrayList):java.util.ArrayList
      com.tencent.launcher.ff.b(android.content.Context, int):void
      com.tencent.launcher.ff.b(android.content.Context, com.tencent.launcher.ha):void
      com.tencent.launcher.ff.b(com.tencent.launcher.Launcher, boolean):void
      com.tencent.launcher.ff.b(android.content.Context, long):com.tencent.launcher.ex
      com.tencent.launcher.ff.b(com.tencent.launcher.Launcher, java.lang.String):void
      com.tencent.launcher.ff.b(android.content.Context, java.lang.String):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(android.content.Context, java.lang.String):boolean
     arg types: [com.tencent.launcher.Launcher, java.lang.String]
     candidates:
      com.tencent.launcher.ff.a(java.lang.String, boolean):char
      com.tencent.launcher.ff.a(android.content.Context, long):com.tencent.launcher.am
      com.tencent.launcher.ff.a(android.content.pm.PackageManager, android.content.Intent):com.tencent.launcher.am
      com.tencent.launcher.ff.a(java.util.HashMap, long):com.tencent.launcher.bq
      com.tencent.launcher.ff.a(com.tencent.launcher.ff, java.util.ArrayList):java.util.ArrayList
      com.tencent.launcher.ff.a(com.tencent.launcher.ff, java.util.HashMap):java.util.HashMap
      com.tencent.launcher.ff.a(android.content.pm.PackageManager, java.lang.String):java.util.List
      com.tencent.launcher.ff.a(android.content.ContentResolver, android.content.pm.PackageManager):void
      com.tencent.launcher.ff.a(android.content.Context, int):void
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.bq):void
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha):void
      com.tencent.launcher.ff.a(com.tencent.launcher.bq, com.tencent.launcher.ha):void
      com.tencent.launcher.ff.a(com.tencent.launcher.ff, com.tencent.launcher.Launcher):void
      com.tencent.launcher.ff.a(java.util.HashMap, android.content.ComponentName):boolean
      com.tencent.launcher.ff.a(java.util.List, android.content.ComponentName):boolean
      com.tencent.launcher.ff.a(android.content.pm.PackageManager, com.tencent.launcher.am):android.graphics.drawable.Drawable
      com.tencent.launcher.ff.a(com.tencent.launcher.Launcher, android.content.ComponentName):void
      com.tencent.launcher.ff.a(com.tencent.launcher.Launcher, java.lang.String):void
      com.tencent.launcher.ff.a(com.tencent.launcher.Launcher, boolean):boolean
      com.tencent.launcher.ff.a(android.content.Context, java.lang.String):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(com.tencent.launcher.Launcher, java.lang.String, boolean):void
     arg types: [com.tencent.launcher.Launcher, java.lang.String, int]
     candidates:
      com.tencent.launcher.ff.a(android.content.pm.PackageManager, java.util.HashMap, android.content.pm.ResolveInfo):com.tencent.launcher.am
      com.tencent.launcher.ff.a(com.tencent.launcher.am, java.lang.String, java.lang.String):com.tencent.launcher.am
      com.tencent.launcher.ff.a(com.tencent.launcher.bl, java.lang.String, java.lang.String):com.tencent.launcher.am
      com.tencent.launcher.ff.a(android.content.Context, long, int):void
      com.tencent.launcher.ff.a(android.content.pm.PackageManager, android.content.pm.ResolveInfo, com.tencent.launcher.am):void
      com.tencent.launcher.ff.a(com.tencent.launcher.ff, com.tencent.launcher.Launcher, boolean):void
      com.tencent.launcher.ff.a(android.content.Context, java.lang.String, android.content.Intent):boolean
      com.tencent.launcher.ff.a(java.util.List, com.tencent.launcher.bl, com.tencent.launcher.Launcher):boolean
      com.tencent.launcher.ff.a(com.tencent.launcher.Launcher, java.lang.String, boolean):void */
    public final void onReceive(Context context, Intent intent) {
        String[] stringArrayExtra;
        String action = intent.getAction();
        if ("android.intent.action.PACKAGE_CHANGED".equals(action) || "android.intent.action.PACKAGE_REMOVED".equals(action) || "android.intent.action.PACKAGE_ADDED".equals(action)) {
            String schemeSpecificPart = intent.getData().getSchemeSpecificPart();
            boolean booleanExtra = intent.getBooleanExtra("android.intent.extra.REPLACING", false);
            if (!"android.intent.action.PACKAGE_CHANGED".equals(action)) {
                if ("android.intent.action.PACKAGE_REMOVED".equals(action)) {
                    if (!booleanExtra) {
                        Launcher.access$4000(this.a, schemeSpecificPart);
                        Launcher.access$4100(this.a, schemeSpecificPart);
                        Launcher.sModel.b(this.a, schemeSpecificPart);
                        ff.b((Context) this.a, schemeSpecificPart);
                        this.a.updateNewAppsNofity();
                        if (schemeSpecificPart.equals(this.a.mThemeManager.g().a)) {
                            this.a.mThemeManager.c("com.tencent.qqlauncher");
                        }
                    }
                } else if (!booleanExtra) {
                    Launcher.sModel.a(this.a, schemeSpecificPart);
                    if (!schemeSpecificPart.startsWith("com.tencent.qqlauncher.theme")) {
                        ff unused = Launcher.sModel;
                        ff.a((Context) this.a, schemeSpecificPart);
                        this.a.mHasNewAppsNotify = true;
                        this.a.updateNewAppsNofity();
                    }
                } else {
                    Launcher.sModel.c(this.a, schemeSpecificPart);
                    Launcher.access$4200(this.a, schemeSpecificPart);
                }
                this.a.removeDialog(1);
                return;
            }
            Launcher.sModel.d(this.a, schemeSpecificPart);
        } else if ("android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE".equals(action)) {
            String[] stringArrayExtra2 = intent.getStringArrayExtra("android.intent.extra.changed_package_list");
            if (stringArrayExtra2 != null && stringArrayExtra2.length != 0) {
                for (int i = 0; i < stringArrayExtra2.length; i++) {
                    Launcher.sModel.a(this.a, stringArrayExtra2[i], true);
                    Launcher.access$4200(this.a, stringArrayExtra2[i]);
                    Launcher.access$4300(this.a, stringArrayExtra2[i]);
                }
                String b = i.a().b("curr_theme", (String) null);
                if (b != null && !b.equals("com.tencent.qqlauncher")) {
                    for (String equals : stringArrayExtra2) {
                        if (b.equals(equals)) {
                            l.a().b(b);
                        }
                    }
                }
            }
        } else if ("android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE".equals(action) && (stringArrayExtra = intent.getStringArrayExtra("android.intent.extra.changed_package_list")) != null && stringArrayExtra.length != 0) {
            for (int i2 = 0; i2 < stringArrayExtra.length; i2++) {
                Launcher.access$4200(this.a, stringArrayExtra[i2]);
                Launcher.access$4300(this.a, stringArrayExtra[i2]);
            }
        }
    }
}
