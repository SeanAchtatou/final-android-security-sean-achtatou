package com.tencent.launcher;

import android.app.SearchManager;
import android.util.Log;

final class ia implements Runnable {
    private /* synthetic */ Launcher a;

    ia(Launcher launcher) {
        this.a = launcher;
    }

    public final void run() {
        try {
            ((SearchManager) this.a.getSystemService("search")).stopSearch();
        } catch (Exception e) {
            Log.e("Launcher", "error stopping search", e);
        }
    }
}
