package com.tencent.launcher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.launcher.base.b;
import com.tencent.launcher.home.c;
import com.tencent.module.appcenter.AppCenterActivity;
import com.tencent.module.download.DownloadInfo;
import com.tencent.qqlauncher.R;
import com.tencent.util.d;
import com.tencent.util.p;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Vector;
import java.util.regex.Pattern;

public class SearchAppTestActivity extends Activity implements TextWatcher, View.OnClickListener, View.OnFocusChangeListener, View.OnTouchListener, TextView.OnEditorActionListener {
    private static final float Font_PADDING = 20.0f;
    private static final float HOTWORD_TEXTSIZE = 16.0f;
    private static final int MAX_FONT_HEIGHT = 50;
    private static final int RADIO_LINE_NAMEBER = 12;
    private static final int RECT_MATRIC = 6;
    private static final String SOUND_MOTHER_REG = "^[b|p|m|f|d|t|n|l|g|k|h|j|q|x|r|z|c|s|y|w].*";
    private static final int TAB_LOCAL = 0;
    private static final int TAB_NET = 1;
    public static final String TAB_TYPE = "tab_type";
    public static final int TAB_TYPE_NET = 100;
    private static final String TAG = "SearchAppActivityReserved";
    private static String[] specialTable;
    private AnimationSet[] asets;
    /* access modifiers changed from: private */
    public boolean bAllHttpMsgReceived = false;
    private boolean bFromAppCenter = false;
    private ImageView centerIcon;
    private d dic;
    /* access modifiers changed from: private */
    public int[] hotWordX;
    /* access modifiers changed from: private */
    public int[] hotWordY;
    private int[] hotwordColor = {R.color.tag_color_1, R.color.tag_color_2, R.color.tag_color_3, R.color.tag_color_4, R.color.tag_color_5};
    private int[] hotwordSize = {14, 16, 17, 19, 20, 21, 22, 23, 24};
    private boolean isFinishHotwords = false;
    boolean isFisrtStart = true;
    private ArrayList mAppList;
    private ImageView mClearButton;
    private Context mContext;
    /* access modifiers changed from: private */
    public String mCurKeyWord;
    /* access modifiers changed from: private */
    public int mCurTab;
    /* access modifiers changed from: private */
    public EditText mEditText;
    /* access modifiers changed from: private */
    public RelativeLayout mFootLoadingView;
    /* access modifiers changed from: private */
    public Handler mHandler = new ai(this);
    private TextView mHotWordTitleView;
    private TextView[] mHotWordViews;
    private ArrayList mHotwordsCache;
    private RelativeLayout mHotwordsLayout;
    private LinearLayout mHotwordsLinearLayout;
    private LayoutInflater mInflater;
    private ListView mListView;
    private ProgressBar mLoadingView;
    private s mLocalAdapter;
    private ImageView mLocalTabIndicator;
    private TextView mLocalTabView;
    private ArrayList mMacheList;
    /* access modifiers changed from: private */
    public df mNetAdapter;
    /* access modifiers changed from: private */
    public ArrayList mNetAppList;
    private ImageView mNetTabIndicator;
    private TextView mNetTabView;
    /* access modifiers changed from: private */
    public int mPageNumber = 1;
    private Button mSearchButton;
    /* access modifiers changed from: private */
    public TextView mSearchCountView;
    private Button mSwitchHotwords;
    /* access modifiers changed from: private */
    public int nAppCount = 0;
    private int parentHeight;
    private int parentWidth;
    private Rect[][] regions;
    private int requestId = -1;
    /* access modifiers changed from: private */
    public int totalCount = 0;
    /* access modifiers changed from: private */
    public d tree = new d();
    private int unitH;
    private int unitW;
    int visible;

    static {
        String[] strArr = new String[16];
        strArr[0] = "*";
        strArr[1] = "$";
        strArr[2] = "?";
        strArr[3] = ".";
        strArr[4] = "#";
        strArr[5] = "^";
        strArr[6] = "&";
        strArr[7] = "+";
        strArr[8] = "}";
        strArr[9] = "{";
        strArr[10] = "}";
        strArr[11] = "(";
        strArr[RADIO_LINE_NAMEBER] = ")";
        strArr[13] = "%";
        strArr[14] = "|";
        strArr[15] = ",";
        specialTable = strArr;
    }

    static /* synthetic */ int access$1112(SearchAppTestActivity searchAppTestActivity, int i) {
        int i2 = searchAppTestActivity.nAppCount + i;
        searchAppTestActivity.nAppCount = i2;
        return i2;
    }

    static /* synthetic */ int access$408(SearchAppTestActivity searchAppTestActivity) {
        int i = searchAppTestActivity.mPageNumber;
        searchAppTestActivity.mPageNumber = i + 1;
        return i;
    }

    private Rect[] calculateRegin() {
        this.regions = new Rect[6][];
        for (int i = 0; i < this.regions.length; i++) {
            this.regions[i] = new Rect[6];
            for (int i2 = 0; i2 < this.regions[i].length; i2++) {
                int i3 = (i2 - 3) * this.unitW;
                int i4 = (i - 3) * this.unitH;
                int i5 = ((i2 + 1) - 3) * this.unitW;
                int i6 = ((i + 1) - 3) * this.unitH;
                if (i3 == 0) {
                    i3 = this.unitW / 2;
                }
                if (i4 == 0) {
                    i4 = this.unitH / 2;
                }
                if (i5 == this.unitW * 6) {
                    i5 -= this.unitW / 2;
                }
                if (i6 == this.unitH * 6) {
                    i6 -= this.unitH / 2;
                }
                this.regions[i][i2] = new Rect(i3, i4, i5, i6);
            }
        }
        Rect[] rectArr = new Rect[RADIO_LINE_NAMEBER];
        rectArr[0] = new Rect(this.regions[2][0].left, this.regions[2][0].top, this.regions[2][2].right, this.regions[2][2].bottom);
        rectArr[1] = new Rect(this.regions[0][0].left, this.regions[0][0].top, this.regions[1][1].right, this.regions[1][1].bottom);
        rectArr[2] = new Rect(this.regions[0][2].left, this.regions[0][2].top, this.regions[1][2].right, this.regions[1][2].bottom);
        rectArr[3] = new Rect(this.regions[0][3].left, this.regions[0][3].top, this.regions[1][3].right, this.regions[1][3].bottom);
        rectArr[4] = new Rect(this.regions[0][4].left, this.regions[0][4].top, this.regions[1][5].right, this.regions[1][5].bottom);
        rectArr[5] = new Rect(this.regions[2][3].left, this.regions[2][3].top, this.regions[2][5].right, this.regions[2][5].bottom);
        rectArr[6] = new Rect(this.regions[3][4].left, this.regions[3][4].top, this.regions[3][5].right, this.regions[3][5].bottom);
        rectArr[7] = new Rect(this.regions[4][4].left, this.regions[4][4].top, this.regions[5][5].right, this.regions[5][5].bottom);
        rectArr[8] = new Rect(this.regions[3][3].left, this.regions[3][3].top, this.regions[5][3].right, this.regions[5][3].bottom);
        rectArr[9] = new Rect(this.regions[3][2].left, this.regions[3][2].top, this.regions[5][2].right, this.regions[5][2].bottom);
        rectArr[10] = new Rect(this.regions[4][0].left, this.regions[4][0].top, this.regions[5][1].right, this.regions[5][1].bottom);
        rectArr[11] = new Rect(this.regions[3][0].left, this.regions[3][0].top, this.regions[3][2].right, this.regions[3][2].bottom);
        return rectArr;
    }

    private void clearKeyWord() {
        String textFilter = getTextFilter();
        if (!(textFilter == null || textFilter.length() == 0)) {
            this.mEditText.setText("");
        }
        this.mListView.setVisibility(8);
    }

    private String escapeQuery(String str) {
        String replaceAll = str.replaceAll("/", "");
        for (int i = 0; i < specialTable.length; i++) {
            replaceAll = replaceAll.replaceAll("\\" + specialTable[i], "\\\\" + specialTable[i]);
        }
        return replaceAll;
    }

    private Rect[] getCenterRect() {
        Rect[] rectArr = new Rect[16];
        rectArr[0] = this.regions[2][2];
        rectArr[1] = this.regions[2][3];
        rectArr[2] = this.regions[3][3];
        rectArr[3] = this.regions[3][2];
        rectArr[4] = this.regions[3][1];
        rectArr[5] = this.regions[2][1];
        rectArr[6] = this.regions[1][1];
        rectArr[7] = this.regions[1][2];
        rectArr[8] = this.regions[1][3];
        rectArr[9] = this.regions[1][4];
        rectArr[10] = this.regions[2][4];
        rectArr[11] = this.regions[3][4];
        rectArr[RADIO_LINE_NAMEBER] = this.regions[4][4];
        rectArr[13] = this.regions[4][3];
        rectArr[14] = this.regions[4][2];
        rectArr[15] = this.regions[4][1];
        return rectArr;
    }

    /* access modifiers changed from: private */
    public String getTextFilter() {
        if (this.mEditText == null) {
            return null;
        }
        this.mCurKeyWord = this.mEditText.getText().toString();
        return this.mEditText.getText().toString();
    }

    private void hideSoftKeyboard() {
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.mListView.getWindowToken(), 0);
    }

    private void initialLocalSearch() {
        this.mListView.setAdapter((ListAdapter) this.mLocalAdapter);
        this.mFootLoadingView.setVisibility(4);
        this.mLoadingView.setVisibility(4);
        this.mHotwordsLinearLayout.setVisibility(4);
    }

    private void initialNetSearch() {
        this.mFootLoadingView.setVisibility(0);
        this.mListView.setAdapter((ListAdapter) this.mNetAdapter);
        Log.e("", "---------------mListView.setAdapter(mNetAdapter) = " + this.mListView.getAdapter());
        this.mListView.setOnScrollListener(new aj(this));
    }

    private boolean isCachedHotwards(List list) {
        for (int i = 0; i < this.mHotwordsCache.size(); i++) {
            ArrayList arrayList = (ArrayList) this.mHotwordsCache.get(i);
            for (int i2 = 0; i2 < list.size(); i2++) {
                if (arrayList.contains(list.get(i))) {
                    this.isFinishHotwords = true;
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isCollision(int i, int i2, boolean z) {
        if (this.mHotWordViews[i].getVisibility() != 0 || this.mHotWordViews[i2].getVisibility() != 0) {
            return false;
        }
        int abs = Math.abs(this.hotWordY[i] - this.hotWordY[i2]);
        Paint paint = new Paint();
        paint.setTextSize(this.mHotWordViews[i].getTextSize());
        float measureText = paint.measureText(this.mHotWordViews[i].getText().toString());
        paint.setTextSize(this.mHotWordViews[i2].getTextSize());
        float measureText2 = paint.measureText(this.mHotWordViews[i2].getText().toString());
        int i3 = (int) (((float) this.hotWordX[i]) - (measureText / 2.0f));
        int i4 = (int) (((float) this.hotWordX[i2]) - (measureText2 / 2.0f));
        if (abs < MAX_FONT_HEIGHT) {
            if (i3 < i4) {
                if (((float) (i4 - i3)) < measureText) {
                    if (z) {
                        solveCollision(i, i2);
                    }
                    return true;
                }
            } else if (((float) (i3 - i4)) < measureText2) {
                if (z) {
                    solveCollision(i, i2);
                }
                return true;
            }
        }
        return false;
    }

    private boolean isHas(Vector vector, String str) {
        for (int i = 0; i < vector.size(); i++) {
            if (((String) vector.elementAt(i)).indexOf(str) != -1) {
                return true;
            }
        }
        return false;
    }

    private boolean isOutScreen(int i) {
        Paint paint = new Paint();
        paint.setTextSize(this.mHotWordViews[i].getTextSize());
        float measureText = paint.measureText(this.mHotWordViews[i].getText().toString());
        if (((float) this.hotWordX[i]) + measureText > ((float) (this.parentWidth / 2))) {
            return true;
        }
        if (((float) (this.hotWordX[i] + (this.parentWidth / 2))) - measureText < 0.0f) {
            return true;
        }
        if (this.hotWordY[i] + MAX_FONT_HEIGHT > this.parentHeight / 2) {
            return true;
        }
        return this.hotWordY[i] - 25 < (-this.parentHeight) / 2;
    }

    private boolean isValid(Vector vector, String str) {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if ((charAt < 'a' || charAt > 'z') && (charAt < 'A' || charAt > 'Z')) {
                if (!isHas(vector, new String(new char[]{charAt}))) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean matcheApp(String str, ArrayList arrayList) {
        boolean z;
        ArrayList arrayList2 = this.mAppList;
        String escapeQuery = escapeQuery(str);
        if (escapeQuery.length() == 1 && escapeQuery.equalsIgnoreCase("\\")) {
            return false;
        }
        if (escapeQuery.length() == 0) {
            return false;
        }
        Pattern compile = Pattern.compile(".*" + escapeQuery + ".*", 2);
        Iterator it = arrayList2.iterator();
        boolean z2 = false;
        while (it.hasNext()) {
            am amVar = (am) ((ha) it.next());
            if (compile.matcher(amVar.a).find()) {
                arrayList.add(amVar);
                if (this.tree == null) {
                    this.tree = new d();
                }
                this.tree.b(amVar.a.toString());
                z2 = true;
            } else {
                String c = c.c(escapeQuery);
                if (Pattern.compile(".*" + c + ".*", 2).matcher(amVar.b).find()) {
                    c = new Character(c.charAt(0)).toString();
                }
                Vector c2 = this.dic.c(c);
                if (c2 == null || !isValid(c2, escapeQuery) || !c2.contains(amVar.a)) {
                    z = z2;
                } else {
                    arrayList.add(amVar);
                    if (this.tree == null) {
                        this.tree = new d();
                    }
                    this.tree.b(amVar.a.toString());
                    z = true;
                }
                z2 = z;
            }
        }
        this.tree.a(this.tree.a());
        return z2;
    }

    /* access modifiers changed from: private */
    public void searchKeyWord() {
        this.mListView.setVisibility(0);
        String textFilter = getTextFilter();
        if (this.mCurTab == 0) {
            this.mLoadingView.setVisibility(4);
            if (textFilter == null || textFilter.length() == 0) {
                this.mListView.setVisibility(8);
                this.mHotwordsLinearLayout.setVisibility(4);
                return;
            }
            ListAdapter adapter = this.mListView.getAdapter();
            if (adapter instanceof HeaderViewListAdapter) {
                adapter = ((HeaderViewListAdapter) adapter).getWrappedAdapter();
            }
            Log.e("", "------------adapter = " + adapter);
            if (!(adapter instanceof s)) {
                initialLocalSearch();
            }
            if (this.mLocalAdapter.a == null || !this.mLocalAdapter.a.equals(textFilter)) {
                ArrayList arrayList = this.mMacheList;
                arrayList.clear();
                matcheApp(textFilter, arrayList);
                this.mLocalAdapter.a = textFilter;
                this.mSearchCountView.setText(getResources().getString(R.string.search_app_result, Integer.valueOf(arrayList.size()), textFilter));
                this.mLocalAdapter.notifyDataSetChanged();
                return;
            }
            this.mLocalAdapter.notifyDataSetChanged();
            setCountNotify(this.mLocalAdapter.getCount(), textFilter);
        } else if (this.mCurTab == 1 && p.b()) {
            if (textFilter == null || textFilter.length() == 0) {
                this.mListView.setVisibility(8);
                showHotWords();
                this.mLoadingView.setVisibility(0);
                return;
            }
            this.mHotwordsLinearLayout.setVisibility(4);
            if (!(this.mListView.getAdapter() instanceof df)) {
                initialNetSearch();
            }
            if (this.mNetAdapter.a == null || !this.mNetAdapter.a.equals(textFilter)) {
                this.mSearchCountView.setText((int) R.string.search_app_isloading);
                this.mNetAppList.clear();
                this.mNetAdapter.a = textFilter;
                this.mPageNumber = 1;
                this.nAppCount = 0;
                this.bAllHttpMsgReceived = false;
                this.mHotwordsLinearLayout.setVisibility(4);
                com.tencent.module.appcenter.d.a().a(this.mHandler, textFilter, this.mPageNumber);
                this.mPageNumber++;
                return;
            }
            setCountNotify(this.mNetAdapter.getCount(), textFilter);
            this.mNetAdapter.notifyDataSetChanged();
        }
    }

    private void setCountNotify(int i, String str) {
        this.mSearchCountView.setText(getResources().getString(R.string.search_app_result, Integer.valueOf(i), str));
    }

    private synchronized void setHotwords(List list) {
        if (list != null) {
            if (list.size() > 0) {
                this.mHotwordsLayout.removeAllViews();
                this.mHotwordsLayout.clearDisappearingChildren();
                this.mHotwordsLayout.requestLayout();
                if (this.mHotWordViews != null) {
                    for (int length = this.mHotWordViews.length - 1; length >= 0; length--) {
                        this.mHotWordViews[length].clearAnimation();
                        this.mHotWordViews[length].setText("");
                        this.mHotWordViews[length] = null;
                    }
                }
                this.mHotWordViews = new TextView[10];
                this.hotWordX = new int[10];
                this.hotWordY = new int[10];
                this.parentHeight = this.mHotwordsLayout.getHeight();
                this.parentWidth = this.mHotwordsLayout.getWidth();
                Random random = new Random();
                for (int i = 0; i < 10; i++) {
                    this.mHotWordViews[i] = new TextView(this.mContext);
                    this.mHotWordViews[i].setText((CharSequence) list.get(i));
                    this.mHotWordViews[i].setBackgroundColor(Color.parseColor("#00000000"));
                    this.mHotWordViews[i].setBackgroundColor(getResources().getColor(R.color.background_for_all));
                    this.mHotWordViews[i].setTextColor(getResources().getColor(this.hotwordColor[i % this.hotwordColor.length]));
                    this.mHotWordViews[i].setDrawingCacheBackgroundColor(getResources().getColor(R.color.background_for_all));
                    this.mHotWordViews[i].setTextSize((float) this.hotwordSize[random.nextInt(this.hotwordSize.length)]);
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                    layoutParams.addRule(13);
                    this.mHotWordViews[i].setOnClickListener(new ah(this));
                    this.mHotwordsLayout.addView(this.mHotWordViews[i], layoutParams);
                }
                if (getResources().getConfiguration().orientation == 2) {
                    for (int length2 = this.mHotWordViews.length - 1; length2 >= 6; length2--) {
                        this.mHotWordViews[length2].setVisibility(8);
                    }
                }
                setOurSearchTextViewAnimationPos();
                startAnimation();
            }
        }
    }

    private void setToOrigin(int i) {
        Rect[] centerRect = getCenterRect();
        for (int i2 = 0; i2 < centerRect.length; i2++) {
            this.hotWordX[i] = centerRect[i2].left;
            this.hotWordY[i] = centerRect[i2].top;
            if (!isOutScreen(i)) {
                int i3 = 0;
                while (i3 < this.mHotWordViews.length && (i3 == i || !isCollision(i, i3, false))) {
                    i3++;
                }
                if (i3 == this.mHotWordViews.length) {
                    return;
                }
            }
        }
        this.mHotWordViews[i].setText("");
        this.mHotWordViews[i].setVisibility(8);
    }

    private void setupView() {
        this.mSearchButton = (Button) findViewById(R.id.search_app_go_search);
        this.mClearButton = (ImageView) findViewById(R.id.search_app_clear);
        this.mLoadingView = (ProgressBar) findViewById(R.id.search_app_loading);
        this.mListView = (ListView) findViewById(R.id.search_app_listview);
        this.mEditText = (EditText) findViewById(R.id.search_app_key_word);
        this.mLocalTabView = (TextView) findViewById(R.id.search_app_localbox);
        this.mLocalTabIndicator = (ImageView) findViewById(R.id.search_app_localbox_indicater);
        this.mNetTabView = (TextView) findViewById(R.id.search_app_netbox);
        this.mNetTabIndicator = (ImageView) findViewById(R.id.search_app_netbox_indicater);
        this.mHotwordsLayout = (RelativeLayout) findViewById(R.id.search_app_hotwords_layout);
        this.mHotwordsLinearLayout = (LinearLayout) findViewById(R.id.search_app_hotwords_linearlayout);
        this.mSwitchHotwords = (Button) findViewById(R.id.search_app_switch_hotwords);
        this.mHotWordTitleView = (TextView) findViewById(R.id.search_app_hotwords_title);
        this.mSearchCountView = (TextView) this.mInflater.inflate((int) R.layout.search_app_count, (ViewGroup) null);
        this.mFootLoadingView = (RelativeLayout) this.mInflater.inflate((int) R.layout.search_app_foot_loading, (ViewGroup) null);
        this.mSearchButton.setOnClickListener(this);
        this.mClearButton.setOnClickListener(this);
        this.mLocalTabView.setOnClickListener(this);
        this.mNetTabView.setOnClickListener(this);
        this.mSwitchHotwords.setOnClickListener(this);
        this.mEditText.setOnEditorActionListener(this);
        this.mEditText.addTextChangedListener(this);
        this.mListView.setOnTouchListener(this);
        this.mListView.setOnFocusChangeListener(this);
        this.mListView.addHeaderView(this.mSearchCountView);
        this.mListView.addFooterView(this.mFootLoadingView);
        this.mFootLoadingView.setVisibility(4);
        this.mListView.setFooterDividersEnabled(false);
        this.mListView.setHeaderDividersEnabled(false);
        this.mListView.setOnItemClickListener(new ak(this));
        this.mLocalTabView.setSelected(true);
        this.mNetTabView.setSelected(false);
    }

    private void showHotWords() {
        this.mHotwordsLinearLayout.setVisibility(0);
        this.requestId = com.tencent.module.appcenter.d.a().b(this.mHandler);
    }

    private void solveCollision(int i, int i2) {
        int abs = Math.abs(this.hotWordY[i] - this.hotWordY[i2]);
        if (this.hotWordY[i] < this.hotWordY[i2]) {
            int[] iArr = this.hotWordY;
            iArr[i] = iArr[i] - (MAX_FONT_HEIGHT - abs);
        } else {
            int[] iArr2 = this.hotWordY;
            iArr2[i] = (MAX_FONT_HEIGHT - abs) + iArr2[i];
        }
        if (isOutScreen(i)) {
            setToOrigin(i);
            return;
        }
        int i3 = 0;
        while (i3 < this.mHotWordViews.length) {
            if (i3 == i || !isCollision(i, i3, false)) {
                i3++;
            } else {
                setToOrigin(i);
                return;
            }
        }
    }

    private synchronized void startAnimation() {
        int i = 0;
        synchronized (this) {
            while (true) {
                int i2 = i;
                if (i2 < this.mHotWordViews.length) {
                    AnimationSet animationSet = new AnimationSet(false);
                    AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                    alphaAnimation.setInterpolator(new DecelerateInterpolator(1.0f));
                    alphaAnimation.setDuration(1000);
                    TranslateAnimation translateAnimation = new TranslateAnimation(0, 0.0f, 0, (float) this.hotWordX[i2], 0, 0.0f, 0, (float) this.hotWordY[i2]);
                    translateAnimation.setInterpolator(new DecelerateInterpolator(1.0f));
                    translateAnimation.setDuration(800);
                    translateAnimation.setFillEnabled(true);
                    translateAnimation.setFillAfter(true);
                    alphaAnimation.setFillEnabled(true);
                    alphaAnimation.setFillAfter(true);
                    animationSet.addAnimation(translateAnimation);
                    animationSet.addAnimation(alphaAnimation);
                    animationSet.setAnimationListener(new al(this, this.mHotWordViews[i2], i2));
                    this.mHotWordViews[i2].startAnimation(animationSet);
                    i = i2 + 1;
                }
            }
        }
    }

    private void startDownLoad(String str) {
        new DownloadInfo().a = str;
    }

    private void switchHotwords() {
        if (this.mLoadingView.getVisibility() != 0) {
            if (!this.isFinishHotwords) {
                this.requestId = com.tencent.module.appcenter.d.a().b(this.mHandler);
                this.mLoadingView.setVisibility(0);
                this.mHotwordsLayout.removeAllViews();
                this.mHotwordsLayout.clearDisappearingChildren();
                this.mHotwordsLayout.requestLayout();
                return;
            }
            onReceiveHotWords((List) this.mHotwordsCache.get(new Random().nextInt(this.mHotwordsCache.size())));
        }
    }

    private void switchTab(int i) {
        switchTab(i, true);
    }

    private void switchTab(int i, boolean z) {
        if (i == 0) {
            this.mLocalTabView.setSelected(true);
            this.mLocalTabIndicator.setVisibility(0);
            this.mLocalTabView.setTextColor(this.mContext.getResources().getColor(R.color.ac_text_tab_select));
            this.mLocalTabView.setTextSize(((float) ((int) this.mContext.getResources().getDimension(R.dimen.home_tab_text_size))) / b.b);
            this.mNetTabView.setSelected(false);
            this.mNetTabIndicator.setVisibility(4);
            this.mNetTabView.setTextColor(this.mContext.getResources().getColor(R.color.ac_text_tab_nor));
            this.mNetTabView.setTextSize(((float) ((int) this.mContext.getResources().getDimension(R.dimen.home_tab_nor_size))) / b.b);
            this.mSearchButton.setVisibility(8);
        } else if (i == 1) {
            this.mLocalTabView.setSelected(false);
            this.mLocalTabIndicator.setVisibility(4);
            this.mLocalTabView.setTextColor(this.mContext.getResources().getColor(R.color.ac_text_tab_nor));
            this.mLocalTabView.setTextSize(((float) ((int) this.mContext.getResources().getDimension(R.dimen.home_tab_nor_size))) / b.b);
            this.mNetTabView.setSelected(true);
            this.mNetTabIndicator.setVisibility(0);
            this.mNetTabView.setTextColor(this.mContext.getResources().getColor(R.color.ac_text_tab_select));
            this.mNetTabView.setTextSize(((float) ((int) this.mContext.getResources().getDimension(R.dimen.home_tab_text_size))) / b.b);
            this.mSearchButton.setVisibility(0);
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.mListView.getWindowToken(), 0);
        }
        this.mCurTab = i;
        if (z) {
            searchKeyWord();
        }
    }

    public void afterTextChanged(Editable editable) {
        editable.length();
        if (this.mCurTab == 0) {
            searchKeyWord();
        }
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void dismissProgressDialog() {
        this.mLoadingView.setVisibility(4);
        this.mSwitchHotwords.setEnabled(true);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.search_app_go_search /*2131493118*/:
                searchKeyWord();
                return;
            case R.id.search_app_clear /*2131493120*/:
                clearKeyWord();
                if (this.mCurTab == 1) {
                    this.mHotwordsLinearLayout.setVisibility(0);
                    this.requestId = com.tencent.module.appcenter.d.a().b(this.mHandler);
                    return;
                }
                return;
            case R.id.search_app_switch_hotwords /*2131493125*/:
                switchHotwords();
                return;
            case R.id.search_app_localbox /*2131493173*/:
                switchTab(0, true);
                return;
            case R.id.search_app_netbox /*2131493175*/:
                switchTab(1, true);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.search_application_test);
        new Bundle();
        Bundle extras = bundle == null ? getIntent().getExtras() : bundle;
        if (extras != null) {
            this.bFromAppCenter = extras.getBoolean("fromAppcenter", false);
        }
        this.mContext = this;
        this.mInflater = LayoutInflater.from(this.mContext);
        setupView();
        this.mAppList = new ArrayList();
        this.mMacheList = new ArrayList();
        this.mLocalAdapter = new s(this, this.mContext, this.mMacheList);
        this.mNetAppList = new ArrayList();
        this.mNetAdapter = new df(this, this.mContext, this.mNetAppList);
        this.mListView.setAdapter((ListAdapter) this.mLocalAdapter);
        bl e = Launcher.getModel().e();
        if (e == null) {
            finish();
            return;
        }
        int count = e.getCount();
        ArrayList arrayList = this.mAppList;
        arrayList.clear();
        for (int i = 0; i < count; i++) {
            ha haVar = (ha) e.getItem(i);
            if (haVar instanceof am) {
                arrayList.add((am) haVar);
            } else if (haVar instanceof bq) {
                arrayList.addAll(((bq) haVar).b);
            }
        }
        this.dic = new d();
        Iterator it = this.mAppList.iterator();
        while (it.hasNext()) {
            this.dic.a(((am) ((ha) it.next())).a.toString());
        }
        if (this.bFromAppCenter) {
            switchTab(1, true);
        } else {
            switchTab(0, false);
        }
        if (!b.a()) {
            findViewById(R.id.search_app_box_bar).setVisibility(8);
        }
    }

    public void onDestroy() {
        Log.e("", "----------onDestroy");
        super.onDestroy();
    }

    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i != 6) {
            return false;
        }
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.mListView.getWindowToken(), 0);
        if (TextUtils.isEmpty(getTextFilter())) {
            finish();
        }
        return true;
    }

    public void onFocusChange(View view, boolean z) {
        if (view == this.mListView && z) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.mListView.getWindowToken(), 0);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (!this.bFromAppCenter || i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        Intent intent = new Intent();
        intent.setClass(this, AppCenterActivity.class);
        startActivity(intent);
        finish();
        return false;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.visible = this.mFootLoadingView.getVisibility();
        this.mFootLoadingView.setVisibility(4);
        super.onPause();
    }

    public void onReceiveHotWords(List list) {
        if (this.mListView != null) {
            this.mListView.setVisibility(8);
        }
        this.mHotwordsLayout.setVisibility(0);
        if (this.mHotwordsCache == null) {
            this.mHotwordsCache = new ArrayList();
        }
        if (!isCachedHotwards(list)) {
            this.mHotwordsCache.add((ArrayList) list);
        }
        setHotwords(list);
        setHotwordResult();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.isFisrtStart) {
            this.isFisrtStart = false;
        } else {
            this.mFootLoadingView.setVisibility(this.visible);
        }
        super.onResume();
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (view == this.mListView) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.mListView.getWindowToken(), 0);
        }
        return false;
    }

    public void setHotwordResult() {
        this.mHotwordsLayout.setVisibility(0);
        this.mSwitchHotwords.setVisibility(0);
    }

    /* access modifiers changed from: package-private */
    public void setOurSearchTextViewAnimationPos() {
        this.parentHeight = this.mHotwordsLayout.getHeight();
        this.parentWidth = this.mHotwordsLayout.getWidth();
        this.unitH = this.parentHeight / 6;
        this.unitW = this.parentWidth / 6;
        Rect[] calculateRegin = calculateRegin();
        ArrayList arrayList = new ArrayList();
        for (Integer num = 0; num.intValue() < RADIO_LINE_NAMEBER; num = Integer.valueOf(num.intValue() + 1)) {
            arrayList.add(num);
        }
        for (int i = 0; i < this.mHotWordViews.length; i++) {
            Random random = new Random();
            int nextInt = random.nextInt(arrayList.size());
            int intValue = ((Integer) arrayList.get(nextInt)).intValue();
            arrayList.remove(nextInt);
            this.hotWordX[i] = random.nextInt(this.unitW) + calculateRegin[intValue].left;
            this.hotWordY[i] = calculateRegin[intValue].top + random.nextInt(this.unitH);
            this.hotWordY[i] = this.hotWordY[i] + (this.unitH / 2);
            this.hotWordX[i] = this.hotWordX[i] + (this.unitW / 2);
        }
        for (int i2 = 0; i2 < this.hotWordX.length; i2++) {
            Paint paint = new Paint();
            paint.setTextSize(this.mHotWordViews[i2].getTextSize());
            float measureText = paint.measureText(this.mHotWordViews[i2].getText().toString());
            if (((float) this.hotWordX[i2]) + measureText > ((float) (this.parentWidth / 2))) {
                this.hotWordX[i2] = (int) (((double) (this.parentWidth / 2)) - (((double) measureText) * 1.5d));
            }
            if (((float) this.hotWordX[i2]) - measureText < ((float) ((-this.parentWidth) / 2))) {
                this.hotWordX[i2] = (int) (measureText + ((float) ((-this.parentWidth) / 2)));
            }
            if (this.hotWordY[i2] + MAX_FONT_HEIGHT > this.parentHeight / 2) {
                this.hotWordY[i2] = this.hotWordY[i2] - MAX_FONT_HEIGHT;
            }
            if (this.hotWordY[i2] - 25 < (-this.parentHeight) / 2) {
                this.hotWordY[i2] = this.hotWordY[i2] + 25;
            }
            for (int i3 = 0; i3 < this.hotWordX.length; i3++) {
                if (i2 != i3) {
                    isCollision(i2, i3, true);
                }
            }
        }
    }
}
