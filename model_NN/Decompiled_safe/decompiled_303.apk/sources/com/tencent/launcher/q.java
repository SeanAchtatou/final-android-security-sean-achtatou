package com.tencent.launcher;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.tencent.qqlauncher.R;

final class q extends BaseAdapter {
    private LayoutInflater a;
    private /* synthetic */ WallpaperChooser b;

    q(WallpaperChooser wallpaperChooser, WallpaperChooser wallpaperChooser2) {
        this.b = wallpaperChooser;
        this.a = wallpaperChooser2.getLayoutInflater();
    }

    public final int getCount() {
        return this.b.mThumbs.size();
    }

    public final Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        ImageView imageView = view == null ? (ImageView) this.a.inflate((int) R.layout.wallpaper_item, viewGroup, false) : (ImageView) view;
        int intValue = ((Integer) this.b.mThumbs.get(i)).intValue();
        imageView.setImageResource(intValue);
        Drawable drawable = imageView.getDrawable();
        if (drawable != null) {
            drawable.setDither(true);
        } else {
            Log.e("Launcher", String.format("Error decoding thumbnail resId=%d for wallpaper #%d", Integer.valueOf(intValue), Integer.valueOf(i)));
        }
        return imageView;
    }
}
