package com.tencent.launcher;

import android.view.View;
import android.widget.TextView;

final class ah implements View.OnClickListener {
    private /* synthetic */ SearchAppTestActivity a;

    ah(SearchAppTestActivity searchAppTestActivity) {
        this.a = searchAppTestActivity;
    }

    public final void onClick(View view) {
        String obj = ((TextView) view).getText().toString();
        this.a.mEditText.setText(obj);
        this.a.mEditText.setSelection(obj.length());
        this.a.searchKeyWord();
    }
}
