package com.tencent.launcher;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;

final class cp extends LinearLayout.LayoutParams {
    int a;
    int b;
    private /* synthetic */ QNavigation c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cp(QNavigation qNavigation, int i) {
        super(-2, i);
        this.c = qNavigation;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cp(QNavigation qNavigation, Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = qNavigation;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cp(QNavigation qNavigation, ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
        this.c = qNavigation;
    }
}
