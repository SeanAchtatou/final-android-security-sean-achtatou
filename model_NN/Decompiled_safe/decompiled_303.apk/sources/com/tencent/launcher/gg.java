package com.tencent.launcher;

import android.graphics.drawable.Drawable;
import android.view.MenuItem;

final class gg {
    private MenuItem a;
    private /* synthetic */ CustomMenuActivity b;

    public gg(CustomMenuActivity customMenuActivity, MenuItem menuItem) {
        this.b = customMenuActivity;
        this.a = menuItem;
    }

    public final String a() {
        return this.a.getTitle().toString();
    }

    public final Drawable b() {
        return this.a.getIcon();
    }

    public final MenuItem c() {
        return this.a;
    }
}
