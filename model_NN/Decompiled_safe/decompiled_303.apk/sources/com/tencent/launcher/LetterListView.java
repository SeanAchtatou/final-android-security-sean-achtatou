package com.tencent.launcher;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.tencent.a.a;
import com.tencent.launcher.base.b;
import com.tencent.qqlauncher.R;

public class LetterListView extends View {
    private int a;
    private float b;
    private int c;
    private int d;
    private int e;
    private Paint f;
    private String[] g;
    private Bitmap h;
    private Drawable i;
    private float j;
    private float k;
    private boolean l;
    private String m;
    private m n;

    public LetterListView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public LetterListView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.a = -3815995;
        this.b = 14.0f;
        this.l = false;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.j, i2, 0);
        this.a = obtainStyledAttributes.getColor(0, this.a);
        this.b = obtainStyledAttributes.getDimension(1, this.b);
        this.c = obtainStyledAttributes.getDimensionPixelSize(2, 1);
        this.d = obtainStyledAttributes.getDimensionPixelSize(3, 1);
        this.e = obtainStyledAttributes.getDimensionPixelSize(4, 2);
        obtainStyledAttributes.recycle();
        this.f = new Paint();
        this.f.setColor(this.a);
        this.f.setTextSize(this.b);
        this.f.setAntiAlias(true);
        this.f.setTextAlign(Paint.Align.CENTER);
        this.g = getResources().getStringArray(R.array.letter_list);
        this.h = ((BitmapDrawable) getResources().getDrawable(R.drawable.ic_hit_point)).getBitmap();
        this.i = getResources().getDrawable(R.drawable.bg_indexbar);
    }

    private String a(float f2) {
        String[] strArr = this.g;
        int height = (int) ((f2 - ((float) this.c)) / (((float) ((getHeight() - this.c) - this.d)) / ((float) this.g.length)));
        if (height < 0 || height >= this.g.length) {
            return null;
        }
        return strArr[height];
    }

    public final void a(m mVar) {
        this.n = mVar;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Drawable drawable = this.i;
        if (drawable != null && this.l) {
            drawable.setBounds(0, (int) (6.0f * b.b), (getRight() - getLeft()) - this.e, getBottom() - getTop());
            drawable.draw(canvas);
        }
        float width = (float) getWidth();
        int length = this.g.length;
        float height = ((float) ((getHeight() - this.c) - this.d)) / ((float) length);
        Paint paint = this.f;
        String[] strArr = this.g;
        for (int i2 = 1; i2 <= length; i2++) {
            canvas.drawText(strArr[i2 - 1], (width - ((float) this.e)) / 2.0f, ((float) this.c) + (((float) i2) * height), paint);
        }
        if (this.l) {
            Bitmap bitmap = this.h;
            canvas.drawBitmap(bitmap, ((width - ((float) this.e)) / 2.0f) - ((float) (bitmap.getWidth() / 2)), this.k - ((float) (bitmap.getHeight() / 2)), paint);
        }
        super.onDraw(canvas);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        this.j = x;
        this.k = y;
        switch (motionEvent.getAction()) {
            case 0:
                this.l = true;
                String a2 = a(y);
                if (a2 != null) {
                    this.m = a2;
                    if (this.n != null) {
                        this.n.a(a2);
                        break;
                    }
                }
                break;
            case 1:
                this.l = false;
                setBackgroundColor(0);
                if (this.n != null) {
                    this.n.a();
                    break;
                }
                break;
            case 2:
                String a3 = a(y);
                if (a3 != null && !a3.equals(this.m)) {
                    this.m = a3;
                    if (this.n != null) {
                        this.n.b(a3);
                    }
                    invalidate();
                    break;
                }
        }
        return true;
    }
}
