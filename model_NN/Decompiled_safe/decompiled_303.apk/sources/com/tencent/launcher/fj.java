package com.tencent.launcher;

import java.util.ArrayList;

final class fj implements Runnable {
    private final bl a;
    private final ArrayList b;
    private boolean c = true;

    fj(bl blVar, boolean z) {
        this.a = blVar;
        this.c = z;
        this.b = new ArrayList(4);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(ha haVar) {
        ArrayList arrayList = this.b;
        arrayList.add(haVar);
        return arrayList.size() >= 4;
    }

    public final void run() {
        bl blVar = this.a;
        if (blVar != null) {
            if (this.c) {
                blVar.setNotifyOnChange(false);
                blVar.clear();
                this.c = false;
            }
            ArrayList arrayList = this.b;
            if (arrayList != null) {
                int size = arrayList.size();
                for (int i = 0; i < size; i++) {
                    blVar.setNotifyOnChange(false);
                    blVar.add(arrayList.get(i));
                }
                arrayList.clear();
                blVar.notifyDataSetChanged();
            }
        }
    }
}
