package com.tencent.launcher;

import android.view.animation.Animation;

final class fv implements Animation.AnimationListener {
    private /* synthetic */ DeleteZone a;

    fv(DeleteZone deleteZone) {
        this.a = deleteZone;
    }

    public final void onAnimationEnd(Animation animation) {
        this.a.c.requestFullScreen(false);
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
