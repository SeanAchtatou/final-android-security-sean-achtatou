package com.tencent.launcher;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.tencent.a.a;
import com.tencent.module.setting.DesktopSwitchSpecialEffectSettingActivity;
import java.util.ArrayList;
import java.util.Iterator;

public class SliderView extends ImageView {
    private boolean a;
    private boolean b;
    private int c;
    private int d;
    private boolean e;
    private Rect f;
    private boolean g;
    private ArrayList h;
    private int i;
    private Rect j;
    private Rect k;
    private Point l;
    private boolean m;
    private int n;
    private int o;
    private int p;
    private long q;
    private boolean r;
    private final Animation.AnimationListener s;

    public SliderView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        a();
    }

    public SliderView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.b = false;
        this.c = 2;
        this.d = 1;
        this.e = false;
        this.i = 50;
        this.p = 15;
        this.r = true;
        this.s = new bt(this);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.l, i2, 0);
        this.d = obtainStyledAttributes.getInt(0, this.d);
        this.i = obtainStyledAttributes.getDimensionPixelSize(1, this.i);
        obtainStyledAttributes.recycle();
        a();
    }

    private void a() {
        this.k = new Rect();
        this.f = new Rect();
        this.j = new Rect();
        getHitRect(this.f);
    }

    static /* synthetic */ void a(SliderView sliderView) {
        sliderView.a(false);
        sliderView.e = false;
    }

    private void a(boolean z) {
        setPressed(false);
        int left = this.l.x - getLeft();
        int top = this.l.y - getTop();
        if (!z || getVisibility() != 0) {
            offsetLeftAndRight(left);
            offsetTopAndBottom(top);
            clearAnimation();
            invalidate();
            return;
        }
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, (float) left, 0.0f, (float) top);
        translateAnimation.setDuration(250);
        translateAnimation.setAnimationListener(this.s);
        this.e = true;
        startAnimation(translateAnimation);
    }

    private void b() {
        if (this.h != null) {
            Iterator it = this.h.iterator();
            while (it.hasNext()) {
                ImageView imageView = (ImageView) it.next();
                imageView.clearAnimation();
                imageView.setVisibility(4);
            }
        }
    }

    private void c() {
        Iterator it = this.h.iterator();
        while (it.hasNext()) {
            ImageView imageView = (ImageView) it.next();
            AnimationDrawable animationDrawable = (AnimationDrawable) imageView.getBackground();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) imageView.getLayoutParams();
            switch (((Integer) imageView.getTag()).intValue()) {
                case 1:
                    layoutParams.topMargin = (getTop() - this.i) - animationDrawable.getIntrinsicHeight();
                    this.k.top = ((getTop() - this.i) - animationDrawable.getIntrinsicHeight()) - this.p;
                    break;
                case 2:
                    layoutParams.topMargin = getBottom() + this.i;
                    this.k.bottom = animationDrawable.getIntrinsicHeight() + getBottom() + this.i + this.p;
                    break;
                case 4:
                    layoutParams.leftMargin = (getLeft() - this.i) - animationDrawable.getIntrinsicWidth();
                    this.k.left = ((getLeft() - this.i) - animationDrawable.getIntrinsicWidth()) - this.p;
                    break;
                case DesktopSwitchSpecialEffectSettingActivity.nCubeSetting /*8*/:
                    layoutParams.leftMargin = getRight() + this.i;
                    this.k.right = animationDrawable.getIntrinsicWidth() + getRight() + this.i + this.p;
                    break;
            }
            imageView.setLayoutParams(layoutParams);
        }
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        switch (i2) {
            case 23:
            case 66:
                if (isPressed()) {
                    return true;
                }
                break;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        if (!this.e && !this.g) {
            super.onLayout(z, i2, i3, i4, i5);
            getHitRect(this.f);
            this.l = new Point(getLeft(), getTop());
            this.k.set(this.f);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x0343  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r15) {
        /*
            r14 = this;
            r13 = 4
            r12 = 2
            r11 = -2
            r10 = 0
            r9 = 1
            int r0 = r15.getAction()
            float r1 = r15.getX()
            float r2 = r15.getY()
            switch(r0) {
                case 0: goto L_0x0020;
                case 1: goto L_0x032e;
                case 2: goto L_0x0203;
                case 3: goto L_0x0331;
                default: goto L_0x0014;
            }
        L_0x0014:
            boolean r0 = super.onTouchEvent(r15)
            if (r0 != 0) goto L_0x001e
            boolean r0 = r14.g
            if (r0 == 0) goto L_0x0347
        L_0x001e:
            r0 = r9
        L_0x001f:
            return r0
        L_0x0020:
            boolean r0 = r14.r
            if (r0 == 0) goto L_0x01f8
            r14.g = r9
            r14.b = r10
            boolean r0 = r14.a
            if (r9 == r0) goto L_0x002e
            r14.a = r9
        L_0x002e:
            int r0 = (int) r1
            r14.n = r0
            int r0 = (int) r2
            r14.o = r0
            java.util.ArrayList r0 = r14.h
            if (r0 != 0) goto L_0x01f4
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r14.h = r0
            android.view.ViewParent r0 = r14.getParent()
            android.widget.FrameLayout r0 = (android.widget.FrameLayout) r0
            android.view.ViewGroup$LayoutParams r1 = r14.getLayoutParams()
            android.widget.FrameLayout$LayoutParams r1 = (android.widget.FrameLayout.LayoutParams) r1
            int r2 = r1.gravity
            r2 = r2 & 7
            int r1 = r1.gravity
            r3 = r1 & 112(0x70, float:1.57E-43)
            com.tencent.launcher.fo r4 = new com.tencent.launcher.fo
            r4.<init>(r14)
            android.content.Context r1 = r14.getContext()
            r1.getPackageManager()
            int r1 = r14.d
            r1 = r1 & 1
            if (r1 != r9) goto L_0x00bc
            android.widget.ImageView r5 = new android.widget.ImageView
            android.content.Context r1 = r14.getContext()
            r5.<init>(r1)
            android.graphics.drawable.Drawable r1 = r5.getBackground()
            android.graphics.drawable.AnimationDrawable r1 = (android.graphics.drawable.AnimationDrawable) r1
            java.util.ArrayList r6 = r4.a
            if (r6 != 0) goto L_0x007f
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            r4.a = r6
        L_0x007f:
            java.util.ArrayList r6 = r4.a
            r6.add(r1)
            java.lang.Integer r6 = java.lang.Integer.valueOf(r9)
            r5.setTag(r6)
            java.util.ArrayList r6 = r14.h
            r6.add(r5)
            android.widget.FrameLayout$LayoutParams r6 = new android.widget.FrameLayout$LayoutParams
            r6.<init>(r11, r11)
            r6.gravity = r2
            int r7 = r14.getTop()
            int r8 = r14.i
            int r7 = r7 - r8
            int r8 = r1.getIntrinsicHeight()
            int r7 = r7 - r8
            r6.topMargin = r7
            r0.addView(r5, r6)
            android.graphics.Rect r5 = r14.k
            int r6 = r14.getTop()
            int r7 = r14.i
            int r6 = r6 - r7
            int r1 = r1.getIntrinsicHeight()
            int r1 = r6 - r1
            int r6 = r14.p
            int r1 = r1 - r6
            r5.top = r1
        L_0x00bc:
            int r1 = r14.d
            r1 = r1 & 2
            if (r1 != r12) goto L_0x0113
            android.widget.ImageView r5 = new android.widget.ImageView
            android.content.Context r1 = r14.getContext()
            r5.<init>(r1)
            android.graphics.drawable.Drawable r1 = r5.getBackground()
            android.graphics.drawable.AnimationDrawable r1 = (android.graphics.drawable.AnimationDrawable) r1
            java.util.ArrayList r6 = r4.a
            if (r6 != 0) goto L_0x00dc
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            r4.a = r6
        L_0x00dc:
            java.util.ArrayList r6 = r4.a
            r6.add(r1)
            java.lang.Integer r6 = java.lang.Integer.valueOf(r12)
            r5.setTag(r6)
            java.util.ArrayList r6 = r14.h
            r6.add(r5)
            android.widget.FrameLayout$LayoutParams r6 = new android.widget.FrameLayout$LayoutParams
            r6.<init>(r11, r11)
            r6.gravity = r2
            int r2 = r14.getBottom()
            int r7 = r14.i
            int r2 = r2 + r7
            r6.topMargin = r2
            r0.addView(r5, r6)
            android.graphics.Rect r2 = r14.k
            int r5 = r14.getBottom()
            int r6 = r14.i
            int r5 = r5 + r6
            int r1 = r1.getIntrinsicHeight()
            int r1 = r1 + r5
            int r5 = r14.p
            int r1 = r1 + r5
            r2.bottom = r1
        L_0x0113:
            int r1 = r14.d
            r1 = r1 & 4
            if (r1 != r13) goto L_0x0170
            android.widget.ImageView r2 = new android.widget.ImageView
            android.content.Context r1 = r14.getContext()
            r2.<init>(r1)
            android.graphics.drawable.Drawable r1 = r2.getBackground()
            android.graphics.drawable.AnimationDrawable r1 = (android.graphics.drawable.AnimationDrawable) r1
            java.util.ArrayList r5 = r4.a
            if (r5 != 0) goto L_0x0133
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            r4.a = r5
        L_0x0133:
            java.util.ArrayList r5 = r4.a
            r5.add(r1)
            java.lang.Integer r5 = java.lang.Integer.valueOf(r13)
            r2.setTag(r5)
            java.util.ArrayList r5 = r14.h
            r5.add(r2)
            android.widget.FrameLayout$LayoutParams r5 = new android.widget.FrameLayout$LayoutParams
            r5.<init>(r11, r11)
            r5.gravity = r3
            int r6 = r14.getLeft()
            int r7 = r14.i
            int r6 = r6 - r7
            int r7 = r1.getIntrinsicWidth()
            int r6 = r6 - r7
            r5.leftMargin = r6
            r0.addView(r2, r5)
            android.graphics.Rect r2 = r14.k
            int r5 = r14.getLeft()
            int r6 = r14.i
            int r5 = r5 - r6
            int r1 = r1.getIntrinsicWidth()
            int r1 = r5 - r1
            int r5 = r14.p
            int r1 = r1 - r5
            r2.left = r1
        L_0x0170:
            int r1 = r14.d
            r1 = r1 & 8
            r2 = 8
            if (r1 != r2) goto L_0x01cb
            android.widget.ImageView r2 = new android.widget.ImageView
            android.content.Context r1 = r14.getContext()
            r2.<init>(r1)
            android.graphics.drawable.Drawable r1 = r2.getBackground()
            android.graphics.drawable.AnimationDrawable r1 = (android.graphics.drawable.AnimationDrawable) r1
            java.util.ArrayList r5 = r4.a
            if (r5 != 0) goto L_0x0192
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            r4.a = r5
        L_0x0192:
            java.util.ArrayList r5 = r4.a
            r5.add(r1)
            r5 = 8
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r2.setTag(r5)
            java.util.ArrayList r5 = r14.h
            r5.add(r2)
            android.widget.FrameLayout$LayoutParams r5 = new android.widget.FrameLayout$LayoutParams
            r5.<init>(r11, r11)
            r5.gravity = r3
            int r3 = r14.getRight()
            int r6 = r14.i
            int r3 = r3 + r6
            r5.leftMargin = r3
            r0.addView(r2, r5)
            android.graphics.Rect r0 = r14.k
            int r2 = r14.getRight()
            int r3 = r14.i
            int r2 = r2 + r3
            int r1 = r1.getIntrinsicWidth()
            int r1 = r1 + r2
            int r2 = r14.p
            int r1 = r1 + r2
            r0.right = r1
        L_0x01cb:
            r14.post(r4)
        L_0x01ce:
            android.view.animation.AlphaAnimation r1 = new android.view.animation.AlphaAnimation
            r0 = 0
            r2 = 1065353216(0x3f800000, float:1.0)
            r1.<init>(r0, r2)
            r2 = 500(0x1f4, double:2.47E-321)
            r1.setDuration(r2)
            java.util.ArrayList r0 = r14.h
            java.util.Iterator r2 = r0.iterator()
        L_0x01e1:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x01f8
            java.lang.Object r0 = r2.next()
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r0.startAnimation(r1)
            r0.setVisibility(r10)
            goto L_0x01e1
        L_0x01f4:
            r14.c()
            goto L_0x01ce
        L_0x01f8:
            r14.setPressed(r9)
            long r0 = java.lang.System.currentTimeMillis()
            r14.q = r0
            goto L_0x0014
        L_0x0203:
            boolean r0 = r14.g
            if (r0 == 0) goto L_0x0014
            int r0 = r14.c
            if (r0 != r12) goto L_0x0230
            int r0 = r14.d
            r0 = r0 & 1
            if (r0 == r9) goto L_0x0217
            int r0 = r14.d
            r0 = r0 & 2
            if (r0 != r12) goto L_0x02bc
        L_0x0217:
            int r0 = r14.n
            float r0 = (float) r0
            float r0 = r0 - r1
            float r0 = java.lang.Math.abs(r0)
            r3 = 1073741824(0x40000000, float:2.0)
            float r0 = r0 * r3
            int r3 = r14.o
            float r3 = (float) r3
            float r3 = r3 - r2
            float r3 = java.lang.Math.abs(r3)
            int r0 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r0 >= 0) goto L_0x02bc
            r14.c = r9
        L_0x0230:
            int r0 = r14.c
            if (r0 == r12) goto L_0x0014
            r14.m = r9
            int r0 = r14.c
            if (r0 != 0) goto L_0x02e5
            r0 = r9
        L_0x023b:
            if (r0 == 0) goto L_0x02e8
            int r0 = (int) r1
            int r1 = r14.getWidth()
            int r1 = r1 / 2
            int r0 = r0 - r1
            if (r0 >= 0) goto L_0x0251
            int r1 = r14.getLeft()
            android.graphics.Rect r2 = r14.k
            int r2 = r2.left
            if (r1 > r2) goto L_0x025d
        L_0x0251:
            if (r0 <= 0) goto L_0x0352
            int r1 = r14.getRight()
            android.graphics.Rect r2 = r14.k
            int r2 = r2.right
            if (r1 >= r2) goto L_0x0352
        L_0x025d:
            r14.offsetLeftAndRight(r0)
            r1 = r10
            r2 = r0
            r0 = r9
        L_0x0263:
            if (r0 == 0) goto L_0x0289
            android.graphics.Rect r3 = new android.graphics.Rect
            int r0 = r14.getLeft()
            int r0 = r0 - r2
            int r4 = r14.getTop()
            int r4 = r4 - r1
            int r5 = r14.getRight()
            int r2 = r5 - r2
            int r5 = r14.getBottom()
            int r1 = r5 - r1
            r3.<init>(r0, r4, r2, r1)
            android.view.ViewParent r0 = r14.getParent()
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            r0.invalidate(r3)
        L_0x0289:
            android.graphics.Rect r0 = r14.f
            r14.getHitRect(r0)
            java.util.ArrayList r0 = r14.h
            java.util.Iterator r1 = r0.iterator()
            r2 = r10
        L_0x0295:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0310
            java.lang.Object r0 = r1.next()
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            android.graphics.Rect r3 = r14.j
            r0.getHitRect(r3)
            android.graphics.Rect r3 = r14.j
            android.graphics.Rect r4 = r14.f
            boolean r3 = r3.intersect(r4)
            if (r3 == 0) goto L_0x034a
            java.lang.Object r0 = r0.getTag()
            java.lang.Integer r0 = (java.lang.Integer) r0
            r0.intValue()
            r0 = r9
        L_0x02ba:
            r2 = r0
            goto L_0x0295
        L_0x02bc:
            int r0 = r14.d
            r0 = r0 & 4
            if (r0 == r13) goto L_0x02ca
            int r0 = r14.d
            r0 = r0 & 8
            r3 = 8
            if (r0 != r3) goto L_0x0230
        L_0x02ca:
            int r0 = r14.n
            float r0 = (float) r0
            float r0 = r0 - r1
            float r0 = java.lang.Math.abs(r0)
            int r3 = r14.o
            float r3 = (float) r3
            float r3 = r3 - r2
            float r3 = java.lang.Math.abs(r3)
            r4 = 1073741824(0x40000000, float:2.0)
            float r3 = r3 * r4
            int r0 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x0230
            r14.c = r10
            goto L_0x0230
        L_0x02e5:
            r0 = r10
            goto L_0x023b
        L_0x02e8:
            int r0 = (int) r2
            int r1 = r14.getHeight()
            int r1 = r1 / 2
            int r0 = r0 - r1
            if (r0 >= 0) goto L_0x02fc
            int r1 = r14.getTop()
            android.graphics.Rect r2 = r14.k
            int r2 = r2.top
            if (r1 > r2) goto L_0x0308
        L_0x02fc:
            if (r0 <= 0) goto L_0x034d
            int r1 = r14.getBottom()
            android.graphics.Rect r2 = r14.k
            int r2 = r2.bottom
            if (r1 >= r2) goto L_0x034d
        L_0x0308:
            r14.offsetTopAndBottom(r0)
            r1 = r0
            r2 = r10
            r0 = r9
            goto L_0x0263
        L_0x0310:
            boolean r0 = r14.b
            if (r0 != 0) goto L_0x0014
            if (r2 == 0) goto L_0x0014
            r14.b = r9
            r14.g = r10
            r14.setPressed(r10)
            r14.performHapticFeedback(r10, r9)
            r14.b()
            r14.a(r9)
            boolean r0 = r14.a
            if (r0 == 0) goto L_0x0014
            r14.a = r10
            goto L_0x0014
        L_0x032e:
            java.lang.System.currentTimeMillis()
        L_0x0331:
            r14.g = r10
            r14.b = r10
            r14.m = r10
            r14.c = r12
            r14.a(r9)
            r14.b()
            boolean r0 = r14.a
            if (r0 == 0) goto L_0x0014
            r14.a = r10
            goto L_0x0014
        L_0x0347:
            r0 = r10
            goto L_0x001f
        L_0x034a:
            r0 = r2
            goto L_0x02ba
        L_0x034d:
            r1 = r0
            r2 = r10
            r0 = r10
            goto L_0x0263
        L_0x0352:
            r1 = r10
            r2 = r0
            r0 = r10
            goto L_0x0263
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.launcher.SliderView.onTouchEvent(android.view.MotionEvent):boolean");
    }
}
