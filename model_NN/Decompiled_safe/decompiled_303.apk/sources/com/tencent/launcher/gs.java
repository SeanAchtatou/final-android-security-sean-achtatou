package com.tencent.launcher;

import java.util.Comparator;

final class gs implements Comparator {
    gs() {
    }

    public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        am amVar = (am) obj;
        am amVar2 = (am) obj2;
        if (amVar.k == 0 && amVar2.k == 0) {
            return 0;
        }
        return amVar.k == amVar2.k ? ff.b.compare(String.valueOf(amVar2.j), String.valueOf(amVar.j)) : amVar2.k - amVar.k;
    }
}
