package com.tencent.launcher;

import android.content.DialogInterface;

final class da implements DialogInterface.OnClickListener {
    private /* synthetic */ Launcher a;

    da(Launcher launcher) {
        this.a = launcher;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (this.a.curOrder != -1) {
            this.a.clearNewAppsNotify();
            this.a.mMSFConfig.a("application_order", this.a.curOrder);
            this.a.mGrid.a(this.a.curOrder);
            int unused = this.a.curOrder = -1;
        }
    }
}
