package com.tencent.launcher;

import android.view.View;
import android.view.inputmethod.InputMethodManager;
import com.tencent.module.appcenter.d;

final class au implements View.OnClickListener {
    private /* synthetic */ SearchAppActivityReserved a;

    au(SearchAppActivityReserved searchAppActivityReserved) {
        this.a = searchAppActivityReserved;
    }

    public final void onClick(View view) {
        boolean unused = this.a.bSearchType = true;
        this.a.localSearch.setBackgroundColor(-1);
        this.a.networkSearch.setBackgroundColor(-65536);
        ((InputMethodManager) this.a.getSystemService("input_method")).hideSoftInputFromWindow(this.a.mListView.getWindowToken(), 0);
        if (this.a.mSearchEditText.getText().toString().length() == 0) {
            if (!this.a.bSearchType) {
                if (this.a.mListView != null) {
                    this.a.mListView.setVisibility(8);
                }
            } else if (this.a.mNetSearchListView != null) {
                this.a.mNetSearchListView.setVisibility(8);
            }
            this.a.setHotwordResult();
            int unused2 = this.a.requestId = d.a().b(this.a.handler);
            this.a.setWaitScreen();
            return;
        }
        this.a.setSearchResult();
        this.a.InitialNetSearch();
        d.a().a(this.a.handler, this.a.mSearchEditText.getText().toString(), this.a.nPageNumber);
        SearchAppActivityReserved.access$508(this.a);
    }
}
