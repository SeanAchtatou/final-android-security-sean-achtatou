package com.tencent.launcher;

import android.view.animation.Animation;

final class fa implements Animation.AnimationListener {
    private /* synthetic */ DockBar a;

    fa(DockBar dockBar) {
        this.a = dockBar;
    }

    public final void onAnimationEnd(Animation animation) {
        DockBar.d(this.a);
        if (this.a.q >= this.a.p) {
            DockBar.g(this.a);
        }
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
