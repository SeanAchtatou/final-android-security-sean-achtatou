package com.tencent.launcher;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

public final class bl extends ArrayAdapter {
    private final LayoutInflater a;

    public bl(Context context, ArrayList arrayList) {
        super(context, 0, arrayList);
        this.a = LayoutInflater.from(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        ha haVar = (ha) getItem(i);
        View inflate = view == null ? this.a.inflate((int) R.layout.application_boxed, viewGroup, false) : view;
        if (haVar instanceof bq) {
            bq bqVar = (bq) haVar;
            DrawerTextView drawerTextView = (DrawerTextView) inflate;
            DrawerTextView.a(drawerTextView, getContext(), viewGroup, bqVar, null);
            drawerTextView.setFocusable(true);
            bqVar.j = drawerTextView;
        } else {
            am amVar = (am) haVar;
            if (!amVar.f) {
                amVar.d = a.a(getContext(), amVar.d, amVar);
                amVar.f = true;
            }
            TextView textView = (TextView) inflate;
            textView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, amVar.d, (Drawable) null, (Drawable) null);
            textView.setText(amVar.a);
            textView.setTag(amVar);
            textView.setFocusable(true);
        }
        if (haVar.t != i) {
            haVar.t = i;
        }
        return inflate;
    }
}
