package com.tencent.launcher;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.tencent.qqlauncher.R;

public class PageTurningZone extends ImageView implements e {
    /* access modifiers changed from: private */
    public bo a = new bo(this);
    /* access modifiers changed from: private */
    public hc b;

    public PageTurningZone(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public PageTurningZone(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public final void a(cm cmVar, boolean z) {
        removeCallbacks(this.a);
        setSelected(false);
    }

    public final void a(hc hcVar) {
        this.b = hcVar;
    }

    public final boolean a(int i, Object obj) {
        return true;
    }

    public final boolean a(cm cmVar, int i, int i2, int i3, int i4, Object obj) {
        setSelected(false);
        if ((!(cmVar instanceof QGridLayout) && (!(cmVar instanceof UserFolder) || ((UserFolder) cmVar).e().j == null)) || obj == null) {
            return true;
        }
        ((ha) obj).v = false;
        return true;
    }

    public final boolean a(cm cmVar, Object obj) {
        return (cmVar instanceof QGridLayout) || ((cmVar instanceof UserFolder) && ((UserFolder) cmVar).e().j != null);
    }

    public final void b(cm cmVar, int i, int i2, int i3, int i4, Object obj) {
        removeCallbacks(this.a);
        int id = getId();
        if (id == R.id.pre_zone) {
            this.a.a(0);
            postDelayed(this.a, 600);
        } else if (id == R.id.next_zone) {
            this.a.a(1);
            postDelayed(this.a, 600);
        }
        setSelected(true);
    }

    public final void c(cm cmVar, int i, int i2, int i3, int i4, Object obj) {
    }
}
