package com.tencent.launcher;

import android.net.Uri;

public final class ba implements ge {
    public static final Uri a = Uri.parse("content://com.tencent.qqlauncher.settings/favorites?notify=true");
    public static final Uri b = Uri.parse("content://com.tencent.qqlauncher.settings/favorites?notify=false");

    public static Uri a() {
        return Uri.parse("content://com.tencent.qqlauncher.settings/favorites?notify=" + false);
    }

    public static Uri a(long j) {
        return Uri.parse("content://com.tencent.qqlauncher.settings/favorites/" + j + "?" + "notify" + "=" + false);
    }
}
