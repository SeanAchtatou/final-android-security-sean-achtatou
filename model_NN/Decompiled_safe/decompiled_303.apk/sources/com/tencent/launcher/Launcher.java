package com.tencent.launcher;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.SearchManager;
import android.app.WallpaperManager;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProviderInfo;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;
import android.text.Layout;
import android.text.Selection;
import android.text.SpannableStringBuilder;
import android.text.method.TextKeyListener;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import com.tencent.android.ui.LocalSoftManageActivity;
import com.tencent.launcher.DockBar;
import com.tencent.launcher.DockButtonGroup;
import com.tencent.launcher.base.BaseApp;
import com.tencent.launcher.base.b;
import com.tencent.launcher.base.d;
import com.tencent.launcher.home.f;
import com.tencent.launcher.home.i;
import com.tencent.module.component.ag;
import com.tencent.module.qqwidget.QQWidgetFormatExcepiton;
import com.tencent.module.qqwidget.a;
import com.tencent.module.qqwidget.e;
import com.tencent.module.qqwidget.h;
import com.tencent.module.setting.CustomAlertDialog;
import com.tencent.module.setting.DesktopSwitchSpecialEffectSettingActivity;
import com.tencent.module.setting.SettingActivity;
import com.tencent.module.switcher.af;
import com.tencent.module.switcher.v;
import com.tencent.module.theme.ThemeSettingActivity;
import com.tencent.module.theme.l;
import com.tencent.qqlauncher.R;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public final class Launcher extends CustomMenuActivity implements View.OnClickListener, View.OnLongClickListener, hr {
    /* access modifiers changed from: private */
    public static String ACTION_CHECK_STAT = "com.tencent.qqlauncher.ACTION_CHECK_STAT";
    public static final int APPWIDGET_HOST_ID = 1024;
    static final int APP_ORDER_FREQUENT = 1;
    static final int APP_ORDER_NAME = 0;
    static final int APP_ORDER_TIME = 2;
    static final String AUTHORITY_LAUNCHER1 = "com.android.launcher.settings";
    static final String AUTHORITY_LAUNCHER2 = "com.android.launcher2.settings";
    static final String AUTHORITY_LAUNCHER_ADW = "org.adw.launcher.settings";
    static final String AUTHORITY_LAUNCHER_PRO = "com.fede.launcher.settings";
    private static final boolean DEBUG_USER_INTERFACE = false;
    static final int DEFAULT_SCREN = 1;
    private static final int DIALOG_CREATE_APP_CENTER = 4;
    private static final int DIALOG_CREATE_APP_ORDER = 5;
    private static final int DIALOG_CREATE_SHORTCUT = 1;
    private static final int DIALOG_CREATE_SHORTCUT_DOCK = 3;
    private static final int DIALOG_RENAME_FOLDER = 2;
    static final String EXTRA_CUSTOM_WIDGET = "custom_widget";
    static final String EXTRA_SHORTCUT_DUPLICATE = "duplicate";
    static final String FMT_URI = "content://%s/favorites?notify=true";
    static final boolean LOGD = false;
    static final String LOG_TAG = "Launcher";
    public static final int MENU_ADD = 2;
    private static final int MENU_ADD_FOLDER = 13;
    private static final int MENU_CHANGE_DRAWERMODE = 10;
    private static final int MENU_DESKTOP_SETTINGS = 9;
    private static final int MENU_GROUP_ADD = 1;
    private static final int MENU_GROUP_DRAWER = 2;
    private static final int MENU_GROUP_DRAWER_ORDER = 3;
    private static final int MENU_GROUP_NORMAL = 0;
    public static final int MENU_NOTIFICATIONS = 5;
    private static final int MENU_ORDER_APP = 12;
    private static final int MENU_PERSON_CENTER = 7;
    private static final int MENU_SCREEN_MANAGER = 14;
    public static final int MENU_SEARCH = 4;
    public static final int MENU_SETTINGS = 6;
    private static final int MENU_SOFTWARE_MANAGER = 11;
    private static final int MENU_THEME = 8;
    public static final int MENU_WALLPAPER_SETTINGS = 3;
    static final int NUMBER_CELLS_X = 4;
    static final int NUMBER_CELLS_Y = 4;
    static final String PARAMETER_NOTIFY = "notify";
    private static final String PERSONCENTER_DEMO_PACKAGE_NAME = "com.tencent.qqlauncher";
    private static final String PERSONCENTER_PACKAGE_NAME = "com.tencent.sc";
    private static final int PICK_DOCK_APPLICATION = 13;
    private static final int PICK_DOCK_MENU = 11;
    private static final int PICK_DOCK_SHORTCUT = 12;
    private static final String PREFERENCES = "launcher.preferences";
    private static final boolean PROFILE_DRAWER = false;
    private static final boolean PROFILE_ROTATE = false;
    private static final boolean PROFILE_STARTUP = false;
    static final String QQ_WIDGET = "qq_widget";
    private static final int REQUEST_CREATE_APPWIDGET = 5;
    private static final int REQUEST_CREATE_LIVE_FOLDER = 4;
    private static final int REQUEST_CREATE_SHORTCUT = 1;
    private static final int REQUEST_EDIT_SHIRTCUT = 10;
    private static final int REQUEST_PICK_APPLICATION = 6;
    private static final int REQUEST_PICK_APPWIDGET = 9;
    private static final int REQUEST_PICK_LIVE_FOLDER = 8;
    private static final int REQUEST_PICK_SHORTCUT = 7;
    private static final String RUNTIME_STATE_ALL_APPS_FOLDER = "launcher.all_apps_folder";
    private static final String RUNTIME_STATE_CURRENT_SCREEN = "launcher.current_screen";
    private static final String RUNTIME_STATE_DOCKBAR = "launcher.dockbar";
    private static final String RUNTIME_STATE_PENDING_ADD_CELL_X = "launcher.add_cellX";
    private static final String RUNTIME_STATE_PENDING_ADD_CELL_Y = "launcher.add_cellY";
    private static final String RUNTIME_STATE_PENDING_ADD_COUNT_X = "launcher.add_countX";
    private static final String RUNTIME_STATE_PENDING_ADD_COUNT_Y = "launcher.add_countY";
    private static final String RUNTIME_STATE_PENDING_ADD_OCCUPIED_CELLS = "launcher.add_occupied_cells";
    private static final String RUNTIME_STATE_PENDING_ADD_SCREEN = "launcher.add_screen";
    private static final String RUNTIME_STATE_PENDING_ADD_SPAN_X = "launcher.add_spanX";
    private static final String RUNTIME_STATE_PENDING_ADD_SPAN_Y = "launcher.add_spanY";
    private static final String RUNTIME_STATE_PENDING_FOLDER_RENAME = "launcher.rename_folder";
    private static final String RUNTIME_STATE_PENDING_FOLDER_RENAME_ID = "launcher.rename_folder_id";
    private static final String RUNTIME_STATE_USER_FOLDERS = "launcher.user_folder";
    static final int SCREEN_COUNT = 3;
    static final String SEARCH_WIDGET = "search_widget";
    static final String SOSO_WIDGET = "soso_widget";
    /* access modifiers changed from: private */
    public static long STAT_CHECK_TIME = 18000000;
    static final String SWITCHER_WIDGET = "switcher_widget";
    static final String TABLE_FAVORITES = "favorites";
    static final String TASK_WIDGET = "task_widget";
    static final int TYPE_ADD_DESKTOP_FOLDER = 1;
    static final int TYPE_ADD_DOCKBAR_FOLDER = 2;
    static final int TYPE_ADD_DRAWER_FOLDER = 3;
    static final int TYPE_RENAME_FOLDER = 0;
    static final int WALLPAPER_SCREENS_SPAN = 2;
    /* access modifiers changed from: private */
    public static Launcher launcher;
    static Drawable sIconBackgroundDrawable;
    private static final Object sLock = new Object();
    /* access modifiers changed from: private */
    public static final ff sModel = new ff();
    private static int sScreen = 1;
    /* access modifiers changed from: private */
    public static Bitmap sWallpaper;
    private static ig sWallpaperReceiver;
    private Thread appThread = new ht(this);
    /* access modifiers changed from: private */
    public int curOrder = -1;
    private long downTime = 0;
    /* access modifiers changed from: private */
    public boolean flagDockEnable = true;
    Handler handler;
    boolean isFirstRun;
    boolean isFirstRunForImportData;
    boolean isLoadHome = false;
    private long lastUnMountedTime;
    /* access modifiers changed from: private */
    public DockView mAddDockItem;
    /* access modifiers changed from: private */
    public bs mAddItemCellInfo;
    /* access modifiers changed from: private */
    public AlarmManager mAlarmManager;
    /* access modifiers changed from: private */
    public cv mAppWidgetHost;
    private AppWidgetManager mAppWidgetManager;
    private final BroadcastReceiver mApplicationsReceiver = new br(this);
    private bj mBinder;
    private final int[] mCellCoordinates = new int[2];
    private final BroadcastReceiver mCloseSystemDialogsReceiver = new hd(this);
    /* access modifiers changed from: private */
    public Context mContext;
    private SpannableStringBuilder mDefaultKeySsb = null;
    /* access modifiers changed from: private */
    public boolean mDesktopLocked = true;
    private boolean mDestroyed;
    /* access modifiers changed from: private */
    public DockBar mDockBar;
    /* access modifiers changed from: private */
    public DockButtonGroup mDockButtonGroup;
    /* access modifiers changed from: private */
    public DragLayer mDragLayer;
    /* access modifiers changed from: private */
    public ApplicationsDrawer mDrawer;
    private final ContentObserver mFavoritesChangeObserver = new aq(this);
    private boolean mFirstManager = true;
    /* access modifiers changed from: private */
    public ex mFolderInfo;
    /* access modifiers changed from: private */
    public fn mGrid;
    private QNavigation mGridNavigation;
    /* access modifiers changed from: private */
    public Button mHandleButton;
    boolean mHasNewAppsNotify;
    /* access modifiers changed from: private */
    public DrawerHomeBar mHomeButton;
    /* access modifiers changed from: private */
    public ViewFlipper mHomeFlipper;
    /* access modifiers changed from: private */
    public ImageView mHomeIconButton;
    public boolean mImportDataBySettingActivity = false;
    private LayoutInflater mInflater;
    private boolean mIsNewIntent;
    private boolean mLocaleChanged;
    /* access modifiers changed from: private */
    public final i mMSFConfig = i.a();
    private final du mMSFConfigChangeListener = new du(this);
    /* access modifiers changed from: private */
    public ImageView mMarketEntry;
    private bs mMenuAddInfo;
    private QNavigation mNavigation;
    /* access modifiers changed from: private */
    public boolean mNeedRestart = false;
    /* access modifiers changed from: private */
    public ImageView mNewAppNofifyView;
    private PageTurningZone mNextZone;
    private PageTurningZone mPreZone;
    /* access modifiers changed from: private */
    public int mRenameFolderType = -1;
    private boolean mRestoring;
    private Bundle mSavedInstanceState;
    private Bundle mSavedState;
    /* access modifiers changed from: private */
    public ImageView mSearchApp;
    /* access modifiers changed from: private */
    public PendingIntent mStatPendingIntent;
    private Rect mTempRect = new Rect();
    private final BroadcastReceiver mThemeDownReceiver = new ThemeDownLoadReceiver();
    l mThemeManager;
    private final BroadcastReceiver mThemeReceiver = new fi(this);
    private ds mThumbnailListener = new de(this);
    public boolean mThumbnailManagerShowed;
    /* access modifiers changed from: private */
    public ThumbnailWorkspace mThumbnailWorkspace;
    private ArrayList mViewList = new ArrayList();
    /* access modifiers changed from: private */
    public boolean mWaitingForResult;
    private final ContentObserver mWidgetObserver = new ie(this);
    /* access modifiers changed from: private */
    public Workspace mWorkspace;
    Handler messageHandler = new hz(this);
    boolean needCheckUpdate = false;
    private boolean needRequestWindow = true;
    public int notiHeight = -1;
    private BroadcastReceiver statsReceiver = new hu(this);

    public class ThemeDownLoadReceiver extends BroadcastReceiver {
        public ThemeDownLoadReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            String stringExtra = intent.getStringExtra("url");
            AlertDialog.Builder builder = new AlertDialog.Builder(Launcher.this.mContext);
            builder.setTitle((int) R.string.theme_download_complete);
            builder.setMessage((int) R.string.theme_download_complete_msg);
            builder.setPositiveButton((int) R.string.comfirm, new bn(this, stringExtra));
            builder.setNegativeButton((int) R.string.cancel, new bm(this));
            builder.create().show();
        }
    }

    private boolean acceptFilter() {
        return !((InputMethodManager) getSystemService("input_method")).isFullscreenMode();
    }

    static /* synthetic */ void access$1400(Launcher launcher2) {
        launcher2.mNavigation.startAnimation(AnimationUtils.loadAnimation(launcher2, R.anim.fade_out));
        launcher2.mNavigation.setVisibility(4);
    }

    static /* synthetic */ void access$4000(Launcher launcher2, String str) {
        if (str != null && str.length() > 0) {
            launcher2.mWorkspace.a(str);
        }
    }

    static /* synthetic */ void access$4100(Launcher launcher2, String str) {
        if (str != null && str.length() > 0) {
            launcher2.mDockBar.a(str);
        }
    }

    static /* synthetic */ void access$4200(Launcher launcher2, String str) {
        if (str != null && str.length() > 0) {
            launcher2.mWorkspace.b(str);
        }
    }

    static /* synthetic */ void access$4300(Launcher launcher2, String str) {
        if (str != null && str.length() > 0) {
            launcher2.mDockBar.b(str);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(boolean, com.tencent.launcher.Launcher, boolean, boolean):void
     arg types: [int, com.tencent.launcher.Launcher, int, int]
     candidates:
      com.tencent.launcher.ff.a(boolean[][], int, int, int):void
      com.tencent.launcher.ff.a(boolean, com.tencent.launcher.Launcher, boolean, boolean):void */
    static /* synthetic */ void access$4400(Launcher launcher2) {
        launcher2.mDesktopLocked = true;
        launcher2.mDrawer.h();
        sModel.a(false, launcher2, false, false);
    }

    static /* synthetic */ void access$4700(Launcher launcher2, bj bjVar, bl blVar) {
        launcher2.mGrid.a(blVar);
        Looper.myQueue().addIdleHandler(bjVar);
    }

    static /* synthetic */ void access$5500(Launcher launcher2) {
        launcher2.mNavigation.startAnimation(AnimationUtils.loadAnimation(launcher2, R.anim.fade_in));
        launcher2.mNavigation.setVisibility(0);
    }

    static /* synthetic */ void access$6100(Launcher launcher2) {
        launcher2.getWindow().closeAllPanels();
        try {
            Process.killProcess(Process.myPid());
            launcher2.finish();
            launcher2.startActivity(launcher2.getIntent());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addFolderInDrayLayer(Folder folder) {
        this.mDragLayer.addView(folder);
    }

    private void addItems() {
        bs bsVar = this.mMenuAddInfo;
        this.mWorkspace.q();
        this.mAddItemCellInfo = bsVar;
        this.mWaitingForResult = true;
        showDialog(1);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0097  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static com.tencent.launcher.dq addLiveFolder(android.content.Context r11, android.content.Intent r12, com.tencent.launcher.bs r13, boolean r14) {
        /*
            r9 = 0
            java.lang.String r1 = "android.intent.extra.livefolder.BASE_INTENT"
            android.os.Parcelable r1 = r12.getParcelableExtra(r1)
            android.content.Intent r1 = (android.content.Intent) r1
            java.lang.String r2 = "android.intent.extra.livefolder.NAME"
            java.lang.String r4 = r12.getStringExtra(r2)
            java.lang.String r2 = "android.intent.extra.livefolder.ICON"
            android.os.Parcelable r3 = r12.getParcelableExtra(r2)
            if (r3 == 0) goto L_0x0099
            boolean r2 = r3 instanceof android.content.Intent.ShortcutIconResource
            if (r2 == 0) goto L_0x0099
            r0 = r3
            android.content.Intent$ShortcutIconResource r0 = (android.content.Intent.ShortcutIconResource) r0     // Catch:{ Exception -> 0x0078 }
            r2 = r0
            android.content.pm.PackageManager r5 = r11.getPackageManager()     // Catch:{ Exception -> 0x0095 }
            java.lang.String r6 = r2.packageName     // Catch:{ Exception -> 0x0095 }
            android.content.res.Resources r5 = r5.getResourcesForApplication(r6)     // Catch:{ Exception -> 0x0095 }
            java.lang.String r6 = r2.resourceName     // Catch:{ Exception -> 0x0095 }
            r7 = 0
            r8 = 0
            int r6 = r5.getIdentifier(r6, r7, r8)     // Catch:{ Exception -> 0x0095 }
            android.graphics.drawable.Drawable r3 = r5.getDrawable(r6)     // Catch:{ Exception -> 0x0095 }
            r10 = r2
            r2 = r3
            r3 = r10
        L_0x0038:
            if (r2 != 0) goto L_0x0097
            android.content.res.Resources r2 = r11.getResources()
            r5 = 2130837744(0x7f0200f0, float:1.728045E38)
            android.graphics.drawable.Drawable r2 = r2.getDrawable(r5)
            r5 = r2
        L_0x0046:
            com.tencent.launcher.dq r2 = new com.tencent.launcher.dq
            r2.<init>()
            r2.d = r5
            r5 = 0
            r2.e = r5
            r2.h = r4
            r2.f = r3
            android.net.Uri r3 = r12.getData()
            r2.b = r3
            r2.a = r1
            java.lang.String r1 = "android.intent.extra.livefolder.DISPLAY_MODE"
            r3 = 1
            int r1 = r12.getIntExtra(r1, r3)
            r2.c = r1
            r3 = -100
            int r5 = r13.f
            int r6 = r13.b
            int r7 = r13.c
            r1 = r11
            r8 = r14
            com.tencent.launcher.ff.a(r1, r2, r3, r5, r6, r7, r8)
            com.tencent.launcher.ff r1 = com.tencent.launcher.Launcher.sModel
            r1.a(r2)
            return r2
        L_0x0078:
            r2 = move-exception
            r2 = r9
        L_0x007a:
            java.lang.String r5 = "Launcher"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "Could not load live folder icon: "
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r3 = r6.append(r3)
            java.lang.String r3 = r3.toString()
            android.util.Log.w(r5, r3)
            r3 = r2
            r2 = r9
            goto L_0x0038
        L_0x0095:
            r5 = move-exception
            goto L_0x007a
        L_0x0097:
            r5 = r2
            goto L_0x0046
        L_0x0099:
            r3 = r9
            r2 = r9
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.launcher.Launcher.addLiveFolder(android.content.Context, android.content.Intent, com.tencent.launcher.bs, boolean):com.tencent.launcher.dq");
    }

    static am addShortcut(Context context, Intent intent, bs bsVar, boolean z) {
        am infoFromShortcutIntent = infoFromShortcutIntent(context, intent);
        ff.a(context, infoFromShortcutIntent, -100, bsVar.f, bsVar.b, bsVar.c, z);
        return infoFromShortcutIntent;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void appwidgetReadyBroadcast(int i, ComponentName componentName, int[] iArr) {
        Intent intent = new Intent("com.motorola.blur.home.ACTION_SET_WIDGET_SIZE");
        intent.setComponent(componentName);
        intent.putExtra("appWidgetId", i);
        intent.putExtra("spanX", iArr[0]);
        intent.putExtra("spanY", iArr[1]);
        intent.putExtra("com.motorola.blur.home.EXTRA_NEW_WIDGET", true);
        sendBroadcast(intent);
        sendBroadcast(new Intent("mobi.intuitit.android.hpp.ACTION_READY").putExtra("mobi.intuitit.android.hpp.EXTRA_APPWIDGET_ID", i).putExtra("appWidgetId", i).putExtra("mobi.intuitit.android.hpp.EXTRA_API_VERSION", 2).setComponent(componentName));
    }

    /* access modifiers changed from: private */
    public void bindAppWidgets(bj bjVar, LinkedList linkedList) {
        Workspace workspace = this.mWorkspace;
        boolean z = this.mDesktopLocked;
        if (!linkedList.isEmpty()) {
            c cVar = (c) linkedList.removeFirst();
            int i = cVar.a;
            AppWidgetProviderInfo appWidgetInfo = this.mAppWidgetManager.getAppWidgetInfo(i);
            cVar.b = this.mAppWidgetHost.createView(this, i, appWidgetInfo);
            cVar.b.setAppWidget(i, appWidgetInfo);
            cVar.b.setTag(cVar);
            if (cVar.r == 1 && cVar.s == 1) {
                WidgetContainer widgetContainer = new WidgetContainer(getApplicationContext());
                widgetContainer.setTag(cVar);
                widgetContainer.addView(cVar.b, new ViewGroup.LayoutParams(-1, -1));
                workspace.a(widgetContainer, cVar.o, cVar.p, cVar.q, cVar.r, cVar.s, !z);
            } else {
                workspace.a(cVar.b, cVar.o, cVar.p, cVar.q, cVar.r, cVar.s, !z);
            }
            workspace.requestLayout();
            if (appWidgetInfo != null) {
                appwidgetReadyBroadcast(i, appWidgetInfo.provider, new int[]{cVar.r, cVar.s});
            }
        }
        if (!linkedList.isEmpty()) {
            bjVar.obtainMessage(2).sendToTarget();
        }
    }

    private void bindDesktopItems(ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3) {
        bl e = sModel.e();
        if (arrayList != null && arrayList2 != null && e != null) {
            Workspace workspace = this.mWorkspace;
            int childCount = workspace.getChildCount();
            for (int i = 0; i < childCount; i++) {
                ((ViewGroup) workspace.getChildAt(i)).removeAllViewsInLayout();
            }
            if (this.mBinder != null) {
                this.mBinder.a = true;
            }
            this.mBinder = new bj(this, arrayList, arrayList2, arrayList3, e);
            this.mBinder.a();
        }
    }

    private void bindDrawer(bj bjVar, bl blVar) {
        this.mGrid.a(blVar);
        Looper.myQueue().addIdleHandler(bjVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: private */
    public void bindItems(bj bjVar, ArrayList arrayList, int i, int i2) {
        Workspace workspace = this.mWorkspace;
        boolean z = this.mDesktopLocked;
        Drawable drawable = sIconBackgroundDrawable;
        int min = Math.min(i + 6, i2);
        int i3 = i;
        while (i3 < min) {
            ha haVar = (ha) arrayList.get(i3);
            switch ((int) haVar.n) {
                case -200:
                    DockView dockView = (DockView) this.mDockBar.a(haVar.p);
                    if (dockView == null) {
                        break;
                    } else {
                        dockView.a(haVar);
                        break;
                    }
                default:
                    switch (haVar.m) {
                        case 0:
                        case 1:
                            workspace.a(createShortcut((am) haVar, drawable), haVar.o, haVar.p, haVar.q, 1, 1, !z);
                            continue;
                        case 2:
                            workspace.a(FolderIcon.a(this, (ViewGroup) workspace.getChildAt(workspace.d()), (bq) haVar), haVar.o, haVar.p, haVar.q, 1, 1, !z);
                            continue;
                        case 3:
                            workspace.a(LiveFolderIcon.a(this, (ViewGroup) workspace.getChildAt(workspace.d()), (dq) haVar), haVar.o, haVar.p, haVar.q, 1, 1, !z);
                            continue;
                        case 1001:
                            View inflate = this.mInflater.inflate((int) R.layout.widget_search, (ViewGroup) workspace.getChildAt(workspace.d()), false);
                            ((Search) inflate.findViewById(R.id.widget_search)).a(this);
                            bd bdVar = (bd) haVar;
                            inflate.setTag(bdVar);
                            workspace.a(inflate, bdVar, !z);
                            continue;
                        case 2001:
                            View inflate2 = this.mInflater.inflate((int) R.layout.widget_switcher, (ViewGroup) workspace.getChildAt(workspace.d()), false);
                            bd bdVar2 = (bd) haVar;
                            inflate2.setTag(bdVar2);
                            workspace.a(inflate2, bdVar2, !z);
                            continue;
                        case 2002:
                            View inflate3 = this.mInflater.inflate((int) R.layout.widget_task_manager, (ViewGroup) workspace.getChildAt(workspace.d()), false);
                            bd bdVar3 = (bd) haVar;
                            inflate3.setTag(bdVar3);
                            workspace.a(inflate3, bdVar3, !z);
                            continue;
                        case 2004:
                            View inflate4 = this.mInflater.inflate((int) R.layout.widget_soso, (ViewGroup) workspace.getChildAt(workspace.d()), false);
                            bd bdVar4 = (bd) haVar;
                            inflate4.setTag(bdVar4);
                            workspace.a(inflate4, bdVar4, !z);
                            continue;
                    }
            }
            i3++;
        }
        workspace.requestLayout();
        if (min >= i2) {
            finishBindDesktopItems();
            bjVar.obtainMessage(3).sendToTarget();
            return;
        }
        bjVar.obtainMessage(1, i3, i2).sendToTarget();
    }

    /* access modifiers changed from: private */
    public void bindQQWidgets(bj bjVar, LinkedList linkedList) {
        Workspace workspace = this.mWorkspace;
        boolean z = this.mDesktopLocked;
        if (!linkedList.isEmpty()) {
            a aVar = (a) linkedList.removeFirst();
            try {
                e a = h.a().a(this, aVar);
                a.setTag(aVar);
                workspace.a(a, aVar, !z);
                a.a();
                workspace.requestLayout();
            } catch (QQWidgetFormatExcepiton e) {
                ff.g(this, aVar);
                e.printStackTrace();
            }
        }
        if (!linkedList.isEmpty()) {
            bjVar.obtainMessage(4).sendToTarget();
        }
    }

    private void changeDrawerMode() {
        clearNewAppsNotify();
        i.a().a("grid_orientation", !i.a().b("grid_orientation", true));
        if (i.a().b("setting_normal_functionlist", "").equals("1")) {
            i.a().a("setting_normal_functionlist", "0");
        } else {
            i.a().a("setting_normal_functionlist", "1");
        }
        this.mDrawer.a(this.mGrid);
        this.mGrid.a(this);
        this.mGrid.a(this.mDragLayer);
        initGridNavigation();
        this.mGrid.a(sModel.e());
    }

    private void checkForLocaleChange() {
        gr grVar = new gr();
        readConfiguration(this, grVar);
        Configuration configuration = getResources().getConfiguration();
        String str = grVar.a;
        String locale = configuration.locale.toString();
        int i = grVar.b;
        int i2 = configuration.mcc;
        int i3 = grVar.c;
        int i4 = configuration.mnc;
        this.mLocaleChanged = (locale.equals(str) && i2 == i && i4 == i3) ? false : true;
        if (this.mLocaleChanged) {
            grVar.a = locale;
            grVar.b = i2;
            grVar.c = i4;
            writeConfiguration(this, grVar);
        }
    }

    private boolean checkUpdate() {
        long b = i.a().b("setting_updatetime");
        long currentTimeMillis = System.currentTimeMillis();
        return currentTimeMillis - b < 0 || currentTimeMillis - b >= 604800000;
    }

    private void clearTypedText() {
        this.mDefaultKeySsb.clear();
        this.mDefaultKeySsb.clearSpans();
        Selection.setSelection(this.mDefaultKeySsb, 0);
    }

    private void closeDrawer() {
        closeDrawer(true);
    }

    private void closeDrawer(boolean z) {
        Folder openFolder = getOpenFolder();
        if (openFolder != null && (openFolder instanceof UserFolder)) {
            closeUserFolderFast(openFolder);
        }
        if (this.mDockButtonGroup.getVisibility() == 0) {
            this.mDockButtonGroup.b();
        }
        this.mHomeFlipper.setDisplayedChild(0);
        if (this.mDrawer.a()) {
            clearNewAppsNotify();
            if (z) {
                this.mDrawer.g();
            } else {
                this.mDrawer.f();
            }
            if (this.mDrawer.hasFocus()) {
                this.mWorkspace.getChildAt(this.mWorkspace.d()).requestFocus();
            }
        }
    }

    private boolean closeFolder() {
        Folder openFolder = getOpenFolder();
        if (openFolder == null) {
            return false;
        }
        closeFolder(openFolder);
        return true;
    }

    private boolean closeFolderInDrawer() {
        Folder openFolderInDrawer = getOpenFolderInDrawer();
        if (openFolderInDrawer == null) {
            return false;
        }
        closeFolder(openFolderInDrawer);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void
     arg types: [com.tencent.launcher.Launcher, com.tencent.launcher.c, int, int, int, int, int]
     candidates:
      com.tencent.launcher.ff.a(android.database.Cursor, android.content.Context, int, int, int, int, android.content.Intent):com.tencent.launcher.am
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, boolean):void
     arg types: [com.tencent.launcher.WidgetContainer, int, int, int, int, boolean]
     candidates:
      com.tencent.launcher.Workspace.a(com.tencent.launcher.cm, int, int, java.lang.Object, com.tencent.launcher.CellLayout, boolean):void
      com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, int):void
      com.tencent.launcher.Workspace.a(com.tencent.launcher.cm, int, int, int, int, java.lang.Object):boolean
      com.tencent.launcher.e.a(com.tencent.launcher.cm, int, int, int, int, java.lang.Object):boolean
      com.tencent.launcher.gw.a(com.tencent.launcher.cm, int, int, int, int, java.lang.Object):boolean
      com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, boolean):void
     arg types: [android.appwidget.AppWidgetHostView, int, int, int, int, boolean]
     candidates:
      com.tencent.launcher.Workspace.a(com.tencent.launcher.cm, int, int, java.lang.Object, com.tencent.launcher.CellLayout, boolean):void
      com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, int):void
      com.tencent.launcher.Workspace.a(com.tencent.launcher.cm, int, int, int, int, java.lang.Object):boolean
      com.tencent.launcher.e.a(com.tencent.launcher.cm, int, int, int, int, java.lang.Object):boolean
      com.tencent.launcher.gw.a(com.tencent.launcher.cm, int, int, int, int, java.lang.Object):boolean
      com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, boolean):void */
    private void completeAddAppWidget(Intent intent, bs bsVar, boolean z) {
        int i = intent.getExtras().getInt("appWidgetId", -1);
        AppWidgetProviderInfo appWidgetInfo = this.mAppWidgetManager.getAppWidgetInfo(i);
        int i2 = appWidgetInfo.minWidth;
        int i3 = appWidgetInfo.minHeight;
        Resources resources = ((CellLayout) this.mWorkspace.getChildAt(bsVar.f)).getResources();
        int min = Math.min(resources.getDimensionPixelSize(R.dimen.cell_width), resources.getDimensionPixelSize(R.dimen.cell_height));
        int[] iArr = {(i2 + min) / min, (i3 + min) / min};
        int[] iArr2 = this.mCellCoordinates;
        if (findSlot(bsVar, iArr2, iArr[0], iArr[1])) {
            c cVar = new c(i);
            cVar.r = iArr[0];
            cVar.s = iArr[1];
            ff.a((Context) this, (ha) cVar, -100L, this.mWorkspace.d(), iArr2[0], iArr2[1], false);
            if (appWidgetInfo != null) {
                appwidgetReadyBroadcast(i, appWidgetInfo.provider, iArr);
            }
            if (!this.mRestoring) {
                sModel.a(cVar);
                cVar.b = this.mAppWidgetHost.createView(this, i, appWidgetInfo);
                cVar.b.setAppWidget(i, appWidgetInfo);
                cVar.b.setTag(cVar);
                if (cVar.r == 1 && cVar.s == 1) {
                    WidgetContainer widgetContainer = new WidgetContainer(getApplicationContext());
                    widgetContainer.setTag(cVar);
                    widgetContainer.addView(cVar.b, new ViewGroup.LayoutParams(-1, -1));
                    this.mWorkspace.a((View) widgetContainer, iArr2[0], iArr2[1], cVar.r, cVar.s, z);
                    return;
                }
                this.mWorkspace.a((View) cVar.b, iArr2[0], iArr2[1], cVar.r, cVar.s, z);
            } else if (sModel.b()) {
                sModel.a(cVar);
            }
        } else if (i != -1) {
            this.mAppWidgetHost.deleteAppWidgetId(i);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, boolean):void
     arg types: [com.tencent.launcher.LiveFolderIcon, int, int, int, int, boolean]
     candidates:
      com.tencent.launcher.Workspace.a(com.tencent.launcher.cm, int, int, java.lang.Object, com.tencent.launcher.CellLayout, boolean):void
      com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, int):void
      com.tencent.launcher.Workspace.a(com.tencent.launcher.cm, int, int, int, int, java.lang.Object):boolean
      com.tencent.launcher.e.a(com.tencent.launcher.cm, int, int, int, int, java.lang.Object):boolean
      com.tencent.launcher.gw.a(com.tencent.launcher.cm, int, int, int, int, java.lang.Object):boolean
      com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, boolean):void */
    private void completeAddLiveFolder(Intent intent, bs bsVar, boolean z) {
        bsVar.f = this.mWorkspace.d();
        if (findSingleSlot(bsVar)) {
            dq addLiveFolder = addLiveFolder(this, intent, bsVar, false);
            if (!this.mRestoring) {
                sModel.a((ha) addLiveFolder);
                this.mWorkspace.a((View) LiveFolderIcon.a(this, (ViewGroup) this.mWorkspace.getChildAt(this.mWorkspace.d()), addLiveFolder), bsVar.b, bsVar.c, 1, 1, z);
            } else if (sModel.b()) {
                sModel.a((ha) addLiveFolder);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void
     arg types: [com.tencent.launcher.Launcher, com.tencent.launcher.am, int, int, int, int, int]
     candidates:
      com.tencent.launcher.ff.a(android.database.Cursor, android.content.Context, int, int, int, int, android.content.Intent):com.tencent.launcher.am
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void */
    private void completeAddShirtDockBar(am amVar) {
        DockView dockView = this.mAddDockItem;
        if (dockView != null) {
            ff.a((Context) this, (ha) amVar, -200L, -1, ((DockBar.LayoutParams) dockView.getLayoutParams()).a, -1, false);
            sModel.a(amVar);
            dockView.a(amVar);
            this.mAddDockItem = null;
        }
    }

    private void completeAddShortcut(Intent intent, bs bsVar, boolean z) {
        bsVar.f = this.mWorkspace.d();
        if (findSingleSlot(bsVar)) {
            am addShortcut = addShortcut(this, intent, bsVar, false);
            Drawable drawable = sIconBackgroundDrawable;
            if (!this.mRestoring) {
                sModel.a(addShortcut);
                this.mWorkspace.a(createShortcut(addShortcut, drawable), bsVar.b, bsVar.c, 1, 1, z);
            } else if (sModel.b()) {
                sModel.a(addShortcut);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x003f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void completeEditShortcut(android.content.Intent r13) {
        /*
            r12 = this;
            r11 = 1
            r10 = 0
            r9 = 0
            java.lang.String r1 = "EXTRA_APPLICATIONINFO"
            boolean r1 = r13.hasExtra(r1)
            if (r1 != 0) goto L_0x000c
        L_0x000b:
            return
        L_0x000c:
            java.lang.String r1 = "EXTRA_APPLICATIONINFO"
            r2 = 0
            long r1 = r13.getLongExtra(r1, r2)
            com.tencent.launcher.am r4 = com.tencent.launcher.ff.a(r12, r1)
            if (r4 == 0) goto L_0x000b
            java.lang.String r1 = "USE_DEFAULT_SHIRTCUT"
            boolean r2 = r13.getBooleanExtra(r1, r10)
            java.lang.String r1 = "android.intent.extra.shortcut.INTENT"
            android.os.Parcelable r1 = r13.getParcelableExtra(r1)
            android.content.Intent r1 = (android.content.Intent) r1
            if (r2 == 0) goto L_0x0050
            android.content.pm.PackageManager r2 = r12.getPackageManager()
            com.tencent.launcher.am r2 = com.tencent.launcher.ff.a(r2, r1)
            android.graphics.drawable.Drawable r3 = r2.d
            r4.m = r10
            java.lang.CharSequence r2 = r2.a
            r4.a = r2
            r2 = r9
            r5 = r3
            r3 = r10
        L_0x003d:
            if (r5 == 0) goto L_0x0045
            r4.d = r5
            r4.g = r3
            r4.h = r2
        L_0x0045:
            r4.c = r1
            com.tencent.launcher.ff.e(r12, r4)
            com.tencent.launcher.Workspace r1 = r12.mWorkspace
            r1.a(r4)
            goto L_0x000b
        L_0x0050:
            java.lang.String r2 = "android.intent.extra.shortcut.ICON"
            android.os.Parcelable r2 = r13.getParcelableExtra(r2)
            android.graphics.Bitmap r2 = (android.graphics.Bitmap) r2
            if (r2 == 0) goto L_0x0071
            com.tencent.launcher.hp r3 = new com.tencent.launcher.hp
            android.graphics.Bitmap r2 = com.tencent.launcher.a.a(r2, r12)
            r3.<init>(r2)
            r2 = r9
            r5 = r3
            r3 = r11
        L_0x0066:
            r4.m = r11
            java.lang.String r6 = "android.intent.extra.shortcut.NAME"
            java.lang.String r6 = r13.getStringExtra(r6)
            r4.a = r6
            goto L_0x003d
        L_0x0071:
            java.lang.String r2 = "android.intent.extra.shortcut.ICON_RESOURCE"
            android.os.Parcelable r3 = r13.getParcelableExtra(r2)
            if (r3 == 0) goto L_0x00b9
            boolean r2 = r3 instanceof android.content.Intent.ShortcutIconResource
            if (r2 == 0) goto L_0x00b9
            r0 = r3
            android.content.Intent$ShortcutIconResource r0 = (android.content.Intent.ShortcutIconResource) r0     // Catch:{ Exception -> 0x009a }
            r2 = r0
            android.content.pm.PackageManager r5 = r12.getPackageManager()     // Catch:{ Exception -> 0x00b7 }
            java.lang.String r6 = r2.packageName     // Catch:{ Exception -> 0x00b7 }
            android.content.res.Resources r5 = r5.getResourcesForApplication(r6)     // Catch:{ Exception -> 0x00b7 }
            java.lang.String r6 = r2.resourceName     // Catch:{ Exception -> 0x00b7 }
            r7 = 0
            r8 = 0
            int r6 = r5.getIdentifier(r6, r7, r8)     // Catch:{ Exception -> 0x00b7 }
            android.graphics.drawable.Drawable r3 = r5.getDrawable(r6)     // Catch:{ Exception -> 0x00b7 }
            r5 = r3
            r3 = r10
            goto L_0x0066
        L_0x009a:
            r2 = move-exception
            r2 = r9
        L_0x009c:
            java.lang.String r5 = "Launcher"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "Could not load shortcut icon: "
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r3 = r6.append(r3)
            java.lang.String r3 = r3.toString()
            android.util.Log.w(r5, r3)
            r3 = r10
            r5 = r9
            goto L_0x0066
        L_0x00b7:
            r5 = move-exception
            goto L_0x009c
        L_0x00b9:
            r2 = r9
            r3 = r10
            r5 = r9
            goto L_0x0066
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.launcher.Launcher.completeEditShortcut(android.content.Intent):void");
    }

    private Intent createAppSearchIntent() {
        Intent intent = new Intent("android.search.action.GLOBAL_SEARCH");
        intent.setPackage("com.android.quicksearchbox");
        intent.setFlags(337641472);
        Bundle bundle = new Bundle();
        bundle.putString("source", "launcher-search");
        intent.putExtra("app_data", bundle);
        intent.setData(new Uri.Builder().scheme("qsb.corpus").authority("apps").build());
        return intent;
    }

    /* access modifiers changed from: private */
    public void dissmissThumbnailManager(int i) {
        if (this.mThumbnailWorkspace.b()) {
            ArrayList removeAllItemInfo = removeAllItemInfo();
            int a = this.mThumbnailWorkspace.a();
            this.mWorkspace.g(a);
            ff.b(a);
            this.mWorkspace.removeAllViews();
            int childCount = this.mThumbnailWorkspace.getChildCount() - 1;
            this.mNavigation.removeAllViews();
            for (int i2 = 0; i2 < childCount; i2++) {
                CellLayout cellLayout = (CellLayout) this.mThumbnailWorkspace.getChildAt(i2).getTag();
                if (cellLayout == null) {
                    cellLayout = (CellLayout) this.mInflater.inflate((int) R.layout.workspace_screen, (ViewGroup) null);
                    cellLayout.setOnLongClickListener(this);
                    this.mThumbnailWorkspace.getChildAt(i2).setTag(cellLayout);
                }
                this.mWorkspace.addView(cellLayout);
            }
            int childCount2 = this.mWorkspace.getChildCount();
            this.mNavigation.a(childCount2);
            this.mWorkspace.a(i);
            if (this.mWorkspace.d() >= childCount2) {
                this.mWorkspace.a(childCount2 - 1);
            }
            try {
                new fc(this, removeAllItemInfo).start();
            } catch (Exception e) {
            }
        }
        Animation loadAnimation = AnimationUtils.loadAnimation(this.mContext, R.anim.zoom_out);
        loadAnimation.setAnimationListener(new dd(this, i));
        this.mThumbnailWorkspace.startAnimation(loadAnimation);
    }

    private void dissmissThumbnailManagerToHome() {
        dissmissThumbnailManager(this.mThumbnailWorkspace.a());
    }

    private void doCheckUpdate() {
        new hv(this).start();
    }

    /* access modifiers changed from: private */
    public boolean findSingleSlot(bs bsVar) {
        int[] iArr = new int[2];
        if (!findSlot(bsVar, iArr, 1, 1)) {
            return false;
        }
        bsVar.b = iArr[0];
        bsVar.c = iArr[1];
        return true;
    }

    private boolean findSlot(bs bsVar, int[] iArr, int i, int i2) {
        if (!bsVar.a(iArr, i, i2, true)) {
            if (!this.mWorkspace.a(this.mSavedState != null ? this.mSavedState.getBooleanArray(RUNTIME_STATE_PENDING_ADD_OCCUPIED_CELLS) : null).a(iArr, i, i2, true)) {
                Toast.makeText(this, getString(R.string.out_of_space), 0).show();
                return false;
            }
        }
        return true;
    }

    private void finishBindDesktopItems() {
        Bundle bundle = this.mSavedState;
        Workspace workspace = this.mWorkspace;
        ApplicationsDrawer applicationsDrawer = this.mDrawer;
        DockBar dockBar = this.mDockBar;
        int childCount = dockBar.getChildCount();
        for (int i = 0; i < childCount; i++) {
            DockView dockView = (DockView) dockBar.getChildAt(i);
            if (dockView.getTag() == null) {
                dockView.a((ha) null);
            }
        }
        if (bundle != null) {
            if (!workspace.hasFocus()) {
                workspace.getChildAt(workspace.d()).requestFocus();
            }
            long[] longArray = this.mSavedState.getLongArray(RUNTIME_STATE_USER_FOLDERS);
            if (longArray != null) {
                for (long a : longArray) {
                    ex a2 = sModel.a(a);
                    if (a2 != null) {
                        openFolder(a2);
                    }
                }
                Folder openFolder = getOpenFolder();
                if (openFolder != null) {
                    openFolder.requestFocus();
                }
            }
            if (bundle.getBoolean(RUNTIME_STATE_ALL_APPS_FOLDER, false)) {
                this.mNavigation.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_out));
                this.mNavigation.setVisibility(4);
                applicationsDrawer.c();
            }
            this.mSavedState = null;
        }
        if (this.mSavedInstanceState != null) {
            this.mSavedInstanceState = null;
        }
        if (applicationsDrawer.a() && !applicationsDrawer.hasFocus()) {
            applicationsDrawer.requestFocus();
        }
        this.mDesktopLocked = false;
        applicationsDrawer.i();
        int paddingTop = ((ViewGroup) getWindow().getDecorView()).getChildAt(0).getPaddingTop();
        if (paddingTop > 0) {
            this.notiHeight = paddingTop;
            workspace.f(this.notiHeight);
        }
        initThumbWorkspace();
    }

    public static Drawable getDefaultFloderIcon(Context context, boolean z, ha haVar) {
        Resources resources = context.getResources();
        Drawable a = a.a(context, l.a().a(resources), z ? sIconBackgroundDrawable : null, l.a().b(resources), haVar);
        return a == null ? l.a().a(resources) : a;
    }

    public static Drawable getDefaultFloderIconAdding(Context context, boolean z, ha haVar) {
        Resources resources = context.getResources();
        return a.a(context, resources.getDrawable(R.drawable.folder_bg_adding_default), z ? sIconBackgroundDrawable : null, l.a().b(resources), haVar);
    }

    public static Drawable getDefaultFloderIconAddingInDock(Context context, ha haVar) {
        Resources resources = context.getResources();
        return a.b(context, resources.getDrawable(R.drawable.folder_bg_adding_default), l.a().c(resources), haVar);
    }

    public static Drawable getDefaultFloderIconOpened(Context context, boolean z, ha haVar) {
        Resources resources = context.getResources();
        return a.a(context, l.a().a(resources), z ? sIconBackgroundDrawable : null, l.a().c(resources), haVar);
    }

    public static Drawable getDefaultFloderIconOpenedInDock(Context context, ha haVar) {
        Resources resources = context.getResources();
        return a.b(context, l.a().a(resources), l.a().c(resources), haVar);
    }

    private int getItemsCount() {
        ContentResolver contentResolver = getContentResolver();
        Uri parse = Uri.parse(String.format(FMT_URI, AUTHORITY_LAUNCHER2));
        String type = contentResolver.getType(parse);
        if (type == null) {
            parse = Uri.parse(String.format(FMT_URI, AUTHORITY_LAUNCHER1));
            type = contentResolver.getType(parse);
        }
        if (type == null) {
            return 0;
        }
        try {
            Cursor query = contentResolver.query(parse, null, "itemType = ? or itemType = ? or itemType = ? or itemType = ?", new String[]{"0", "2", "3", "1"}, "screen");
            if (query == null) {
                return 0;
            }
            return query.getCount();
        } catch (SecurityException e) {
            return 0;
        }
    }

    public static Launcher getLauncher() {
        return launcher;
    }

    public static ff getModel() {
        return sModel;
    }

    private static ArrayList getMultiLauncherInfos(Context context) {
        ArrayList arrayList = new ArrayList();
        ContentResolver contentResolver = context.getContentResolver();
        PackageManager packageManager = context.getPackageManager();
        Uri parse = Uri.parse(String.format(FMT_URI, AUTHORITY_LAUNCHER2));
        if (contentResolver.getType(parse) != null) {
            try {
                Cursor query = contentResolver.query(parse, null, "(itemType = ? or itemType = ? or itemType = ? or itemType = ?) and container = ?", new String[]{"0", "2", "3", "1", "-100"}, "screen");
                bc bcVar = new bc();
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo("com.android.launcher2", 0);
                if (applicationInfo != null) {
                    bcVar.a = packageManager.getApplicationLabel(applicationInfo).toString();
                    bcVar.c = a.b(packageManager.getApplicationIcon(applicationInfo), context);
                    bcVar.b = AUTHORITY_LAUNCHER2;
                    bcVar.d = query.getCount();
                    arrayList.add(bcVar);
                }
                query.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Uri parse2 = Uri.parse(String.format(FMT_URI, AUTHORITY_LAUNCHER1));
            if (contentResolver.getType(parse2) != null) {
                try {
                    Cursor query2 = contentResolver.query(parse2, null, "(itemType = ? or itemType = ? or itemType = ? or itemType = ?) and container = ?", new String[]{"0", "2", "3", "1", "-100"}, "screen");
                    bc bcVar2 = new bc();
                    ApplicationInfo applicationInfo2 = packageManager.getApplicationInfo("com.android.launcher", 0);
                    if (applicationInfo2 != null) {
                        bcVar2.a = packageManager.getApplicationLabel(applicationInfo2).toString();
                        bcVar2.c = packageManager.getApplicationIcon(applicationInfo2);
                        bcVar2.b = AUTHORITY_LAUNCHER1;
                        bcVar2.d = query2.getCount();
                        arrayList.add(bcVar2);
                    }
                    query2.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
        if (arrayList.size() == 0) {
            Uri parse3 = Uri.parse(String.format(FMT_URI, AUTHORITY_LAUNCHER2));
            if (contentResolver.getType(parse3) != null) {
                try {
                    Cursor query3 = contentResolver.query(parse3, null, "(itemType = ? or itemType = ? or itemType = ? or itemType = ?) and container = ?", new String[]{"0", "2", "3", "1", "-100"}, "screen");
                    bc bcVar3 = new bc();
                    ApplicationInfo applicationInfo3 = packageManager.getApplicationInfo("com.android.launcher", 0);
                    if (applicationInfo3 != null) {
                        bcVar3.a = packageManager.getApplicationLabel(applicationInfo3).toString();
                        bcVar3.c = packageManager.getApplicationIcon(applicationInfo3);
                        bcVar3.b = AUTHORITY_LAUNCHER2;
                        bcVar3.d = query3.getCount();
                        arrayList.add(bcVar3);
                    }
                    query3.close();
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            }
        }
        Uri parse4 = Uri.parse(String.format(FMT_URI, AUTHORITY_LAUNCHER_PRO));
        if (contentResolver.getType(parse4) != null) {
            try {
                Cursor query4 = contentResolver.query(parse4, null, "(itemType = ? or itemType = ? or itemType = ? or itemType = ?) and container = ?", new String[]{"0", "2", "3", "1", "-100"}, "screen");
                bc bcVar4 = new bc();
                ApplicationInfo applicationInfo4 = packageManager.getApplicationInfo("com.fede.launcher", 0);
                if (applicationInfo4 != null) {
                    bcVar4.a = packageManager.getApplicationLabel(applicationInfo4).toString();
                    bcVar4.c = packageManager.getApplicationIcon(applicationInfo4);
                    bcVar4.b = AUTHORITY_LAUNCHER_PRO;
                    bcVar4.d = query4.getCount();
                    arrayList.add(bcVar4);
                }
                query4.close();
            } catch (Exception e4) {
                e4.printStackTrace();
            }
        }
        Uri parse5 = Uri.parse(String.format(FMT_URI, AUTHORITY_LAUNCHER_ADW));
        if (contentResolver.getType(parse5) != null) {
            try {
                Cursor query5 = contentResolver.query(parse5, null, "(itemType = ? or itemType = ? or itemType = ? or itemType = ?) and container = ?", new String[]{"0", "2", "3", "1", "-100"}, "screen");
                bc bcVar5 = new bc();
                ApplicationInfo applicationInfo5 = packageManager.getApplicationInfo("org.adw.launcher", 0);
                if (applicationInfo5 != null) {
                    bcVar5.a = packageManager.getApplicationLabel(applicationInfo5).toString();
                    bcVar5.c = packageManager.getApplicationIcon(applicationInfo5);
                    bcVar5.b = AUTHORITY_LAUNCHER_ADW;
                    arrayList.add(bcVar5);
                    bcVar5.d = query5.getCount();
                }
                query5.close();
            } catch (Exception e5) {
                e5.printStackTrace();
            }
        }
        return arrayList;
    }

    static int getScreen() {
        int i;
        synchronized (sLock) {
            i = sScreen;
        }
        return i;
    }

    private String getTypedText() {
        return this.mDefaultKeySsb.toString();
    }

    private void handleCrash() {
        d.b();
        AlertDialog create = new AlertDialog.Builder(this).setPositiveButton((int) R.string.crash_report, new hy(this)).setNeutralButton((int) R.string.crash_continue, new hx(this)).setNegativeButton((int) R.string.crash_other, new hw(this)).create();
        create.setTitle((int) R.string.crash_title);
        create.setMessage(getString(R.string.crash_msg));
        create.show();
        create.setCancelable(false);
    }

    private void handleFolderClick(ex exVar) {
        if (!exVar.g) {
            closeFolder();
            openFolder(exVar);
            return;
        }
        Folder a = this.mWorkspace.a(exVar);
        if (a != null) {
            int a2 = this.mWorkspace.a((View) a);
            closeFolder(a);
            if (a2 != this.mWorkspace.d()) {
                closeFolder();
                openFolder(exVar);
            }
        }
    }

    /* access modifiers changed from: private */
    public static void handleInstall(Context context) {
        File file = new File(context.getCacheDir() + "/qqlite.apk");
        if (file.exists()) {
            file.delete();
        }
        try {
            InputStream open = context.getAssets().open("qqlite.db");
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            byte[] bArr = new byte[8192];
            for (int read = open.read(bArr); read > 0; read = open.read(bArr)) {
                fileOutputStream.write(bArr, 0, read);
            }
            fileOutputStream.flush();
            fileOutputStream.close();
            open.close();
            Runtime.getRuntime().exec("chmod 777 " + file.getAbsolutePath());
            new Handler().postDelayed(new eo(file, context), 500);
        } catch (Exception e) {
            e.printStackTrace();
        }
        File file2 = new File(context.getCacheDir() + "/personalcenter.apk");
        if (file2.exists()) {
            file2.delete();
        }
        try {
            InputStream open2 = context.getAssets().open("personalcenter.db");
            FileOutputStream fileOutputStream2 = new FileOutputStream(file2);
            byte[] bArr2 = new byte[8192];
            for (int read2 = open2.read(bArr2); read2 > 0; read2 = open2.read(bArr2)) {
                fileOutputStream2.write(bArr2, 0, read2);
            }
            fileOutputStream2.flush();
            fileOutputStream2.close();
            open2.close();
            Runtime.getRuntime().exec("chmod 777 " + file2.getAbsolutePath());
            new Handler().postDelayed(new ew(file2, context), 500);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00d0  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x011d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int hasMultiLauncher(android.content.Context r10) {
        /*
            r9 = 1
            r8 = 0
            android.content.ContentResolver r0 = r10.getContentResolver()
            java.lang.String r1 = "content://%s/favorites?notify=true"
            java.lang.Object[] r2 = new java.lang.Object[r9]
            java.lang.String r3 = "com.fede.launcher.settings"
            r2[r8] = r3
            java.lang.String r1 = java.lang.String.format(r1, r2)
            android.net.Uri r1 = android.net.Uri.parse(r1)
            java.lang.String r2 = r0.getType(r1)
            if (r2 == 0) goto L_0x011f
            r2 = 0
            java.lang.String r3 = "itemType = ? or itemType = ? or itemType = ? or itemType = ?"
            r4 = 4
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x00f7 }
            r5 = 0
            java.lang.String r6 = "0"
            r4[r5] = r6     // Catch:{ Exception -> 0x00f7 }
            r5 = 1
            java.lang.String r6 = "2"
            r4[r5] = r6     // Catch:{ Exception -> 0x00f7 }
            r5 = 2
            java.lang.String r6 = "3"
            r4[r5] = r6     // Catch:{ Exception -> 0x00f7 }
            r5 = 3
            java.lang.String r6 = "1"
            r4[r5] = r6     // Catch:{ Exception -> 0x00f7 }
            java.lang.String r5 = "screen"
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x00f7 }
            int r2 = r8 + 1
            r1.close()     // Catch:{ Exception -> 0x011b }
            r6 = r2
        L_0x0042:
            java.lang.String r1 = "content://%s/favorites?notify=true"
            java.lang.Object[] r2 = new java.lang.Object[r9]
            java.lang.String r3 = "org.adw.launcher.settings"
            r2[r8] = r3
            java.lang.String r1 = java.lang.String.format(r1, r2)
            android.net.Uri r1 = android.net.Uri.parse(r1)
            java.lang.String r2 = r0.getType(r1)
            if (r2 == 0) goto L_0x007e
            r2 = 0
            java.lang.String r3 = "itemType = ? or itemType = ? or itemType = ? or itemType = ?"
            r4 = 4
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x00ff }
            r5 = 0
            java.lang.String r7 = "0"
            r4[r5] = r7     // Catch:{ Exception -> 0x00ff }
            r5 = 1
            java.lang.String r7 = "2"
            r4[r5] = r7     // Catch:{ Exception -> 0x00ff }
            r5 = 2
            java.lang.String r7 = "3"
            r4[r5] = r7     // Catch:{ Exception -> 0x00ff }
            r5 = 3
            java.lang.String r7 = "1"
            r4[r5] = r7     // Catch:{ Exception -> 0x00ff }
            java.lang.String r5 = "screen"
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x00ff }
            int r2 = r6 + 1
            r1.close()     // Catch:{ Exception -> 0x0119 }
            r6 = r2
        L_0x007e:
            java.lang.String r1 = "content://%s/favorites?notify=true"
            java.lang.Object[] r2 = new java.lang.Object[r9]
            java.lang.String r3 = "com.android.launcher2.settings"
            r2[r8] = r3
            java.lang.String r1 = java.lang.String.format(r1, r2)
            android.net.Uri r1 = android.net.Uri.parse(r1)
            java.lang.String r2 = r0.getType(r1)
            if (r2 == 0) goto L_0x00ba
            r2 = 0
            java.lang.String r3 = "itemType = ? or itemType = ? or itemType = ? or itemType = ?"
            r4 = 4
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0107 }
            r5 = 0
            java.lang.String r7 = "0"
            r4[r5] = r7     // Catch:{ Exception -> 0x0107 }
            r5 = 1
            java.lang.String r7 = "2"
            r4[r5] = r7     // Catch:{ Exception -> 0x0107 }
            r5 = 2
            java.lang.String r7 = "3"
            r4[r5] = r7     // Catch:{ Exception -> 0x0107 }
            r5 = 3
            java.lang.String r7 = "1"
            r4[r5] = r7     // Catch:{ Exception -> 0x0107 }
            java.lang.String r5 = "screen"
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0107 }
            int r2 = r6 + 1
            r1.close()     // Catch:{ Exception -> 0x0117 }
            r6 = r2
        L_0x00ba:
            java.lang.String r1 = "content://%s/favorites?notify=true"
            java.lang.Object[] r2 = new java.lang.Object[r9]
            java.lang.String r3 = "com.android.launcher.settings"
            r2[r8] = r3
            java.lang.String r1 = java.lang.String.format(r1, r2)
            android.net.Uri r1 = android.net.Uri.parse(r1)
            java.lang.String r2 = r0.getType(r1)
            if (r2 == 0) goto L_0x011d
            r2 = 0
            java.lang.String r3 = "itemType = ? or itemType = ? or itemType = ? or itemType = ?"
            r4 = 4
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x010e }
            r5 = 0
            java.lang.String r7 = "0"
            r4[r5] = r7     // Catch:{ Exception -> 0x010e }
            r5 = 1
            java.lang.String r7 = "2"
            r4[r5] = r7     // Catch:{ Exception -> 0x010e }
            r5 = 2
            java.lang.String r7 = "3"
            r4[r5] = r7     // Catch:{ Exception -> 0x010e }
            r5 = 3
            java.lang.String r7 = "1"
            r4[r5] = r7     // Catch:{ Exception -> 0x010e }
            java.lang.String r5 = "screen"
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x010e }
            int r1 = r6 + 1
            r0.close()     // Catch:{ Exception -> 0x0115 }
            r0 = r1
        L_0x00f6:
            return r0
        L_0x00f7:
            r1 = move-exception
            r2 = r8
        L_0x00f9:
            r1.printStackTrace()
            r6 = r2
            goto L_0x0042
        L_0x00ff:
            r1 = move-exception
            r2 = r6
        L_0x0101:
            r1.printStackTrace()
            r6 = r2
            goto L_0x007e
        L_0x0107:
            r1 = move-exception
            r2 = r6
        L_0x0109:
            r1.printStackTrace()
            r6 = r2
            goto L_0x00ba
        L_0x010e:
            r0 = move-exception
            r1 = r6
        L_0x0110:
            r0.printStackTrace()
            r0 = r1
            goto L_0x00f6
        L_0x0115:
            r0 = move-exception
            goto L_0x0110
        L_0x0117:
            r1 = move-exception
            goto L_0x0109
        L_0x0119:
            r1 = move-exception
            goto L_0x0101
        L_0x011b:
            r1 = move-exception
            goto L_0x00f9
        L_0x011d:
            r0 = r6
            goto L_0x00f6
        L_0x011f:
            r6 = r8
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.launcher.Launcher.hasMultiLauncher(android.content.Context):int");
    }

    private void hideNavigation() {
        this.mNavigation.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_out));
        this.mNavigation.setVisibility(4);
    }

    public static void importItemsFormHomeAndDeleteEmptyScreen(String str, Context context, boolean z) {
        Uri uri;
        String str2;
        String str3;
        int i;
        if (z) {
            ff.i(context);
        }
        HashMap hashMap = new HashMap();
        int[] iArr = {99, 99};
        ContentResolver contentResolver = context.getContentResolver();
        if (str != null) {
            Uri parse = Uri.parse(String.format(FMT_URI, str));
            if (contentResolver.getType(parse) != null) {
                uri = parse;
            } else {
                return;
            }
        } else {
            Uri parse2 = Uri.parse(String.format(FMT_URI, AUTHORITY_LAUNCHER2));
            String type = contentResolver.getType(parse2);
            if (type == null) {
                Uri parse3 = Uri.parse(String.format(FMT_URI, AUTHORITY_LAUNCHER1));
                uri = parse3;
                str2 = contentResolver.getType(parse3);
            } else {
                String str4 = type;
                uri = parse2;
                str2 = str4;
            }
            if (str2 == null) {
                return;
            }
        }
        Cursor query = contentResolver.query(uri, null, null, null, "screen");
        if (query == null) {
            System.out.println("----null---");
            return;
        }
        ArrayList arrayList = new ArrayList();
        boolean moveToFirst = query.moveToFirst();
        int[] iArr2 = iArr;
        int i2 = 2;
        while (moveToFirst) {
            int columnIndexOrThrow = query.getColumnIndexOrThrow("_id");
            int columnIndexOrThrow2 = query.getColumnIndexOrThrow("intent");
            int columnIndexOrThrow3 = query.getColumnIndexOrThrow("title");
            int columnIndexOrThrow4 = query.getColumnIndexOrThrow("iconType");
            int columnIndexOrThrow5 = query.getColumnIndexOrThrow("icon");
            int columnIndexOrThrow6 = query.getColumnIndexOrThrow("iconPackage");
            int columnIndexOrThrow7 = query.getColumnIndexOrThrow("iconResource");
            int columnIndexOrThrow8 = query.getColumnIndexOrThrow("container");
            int columnIndexOrThrow9 = query.getColumnIndexOrThrow("itemType");
            int columnIndexOrThrow10 = query.getColumnIndexOrThrow("appWidgetId");
            int columnIndexOrThrow11 = query.getColumnIndexOrThrow("screen");
            int columnIndexOrThrow12 = query.getColumnIndexOrThrow("cellX");
            int columnIndexOrThrow13 = query.getColumnIndexOrThrow("cellY");
            int columnIndexOrThrow14 = query.getColumnIndexOrThrow("spanX");
            int columnIndexOrThrow15 = query.getColumnIndexOrThrow("spanY");
            int columnIndexOrThrow16 = query.getColumnIndexOrThrow("uri");
            int columnIndexOrThrow17 = query.getColumnIndexOrThrow("displayMode");
            int i3 = query.getInt(columnIndexOrThrow9);
            int i4 = query.getInt(columnIndexOrThrow8);
            int i5 = query.getInt(columnIndexOrThrow);
            if (i3 == 0 || i3 == 1 || i3 == 2 || i3 == 3) {
                int i6 = query.getInt(columnIndexOrThrow11);
                if (i6 < 0) {
                    moveToFirst = query.moveToNext();
                } else {
                    int i7 = i6 + 2;
                    if (i7 >= i2) {
                        i2 = i7 + 1;
                        int[] iArr3 = new int[i2];
                        for (int i8 = 0; i8 < iArr2.length; i8++) {
                            iArr3[i8] = iArr2[i8];
                        }
                        iArr2 = iArr3;
                    }
                    if (i4 == -100) {
                        iArr2[i7] = iArr2[i7] + 1;
                    }
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("_id", Integer.valueOf(i5));
                    String string = query.getString(columnIndexOrThrow2);
                    if (string == null || !string.contains("org.adw.launcher")) {
                        if (i4 == -100 && ((i3 == 0 || i3 == 1) && string != null && (string.contains("com.tencent") || string.contains("com.qzone") || string.contains("com.qq")))) {
                            try {
                                String className = Intent.parseUri(string, 0).getComponent().getClassName();
                                if (className != null) {
                                    if (hashMap.get(className) == null) {
                                        hashMap.put(className, className);
                                    } else {
                                        moveToFirst = query.moveToNext();
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        contentValues.put("intent", string);
                        String string2 = query.getString(columnIndexOrThrow3);
                        if (string2 != null && string2.length() > 20) {
                            string2 = string2.substring(0, 19);
                        }
                        contentValues.put("title", string2);
                        contentValues.put("iconType", Integer.valueOf(query.getInt(columnIndexOrThrow4)));
                        contentValues.put("icon", query.getBlob(columnIndexOrThrow5));
                        contentValues.put("iconPackage", query.getString(columnIndexOrThrow6));
                        contentValues.put("iconResource", query.getString(columnIndexOrThrow7));
                        contentValues.put("container", Integer.valueOf(i4));
                        contentValues.put("itemType", Integer.valueOf(i3));
                        contentValues.put("appWidgetId", Integer.valueOf(query.getInt(columnIndexOrThrow10)));
                        contentValues.put("screen", Integer.valueOf(i7));
                        contentValues.put("cellX", Integer.valueOf(query.getInt(columnIndexOrThrow12)));
                        contentValues.put("cellY", Integer.valueOf(query.getInt(columnIndexOrThrow13)));
                        contentValues.put("spanX", Integer.valueOf(query.getInt(columnIndexOrThrow14)));
                        contentValues.put("spanY", Integer.valueOf(query.getInt(columnIndexOrThrow15)));
                        contentValues.put("uri", query.getString(columnIndexOrThrow16));
                        contentValues.put("displayMode", Integer.valueOf(query.getInt(columnIndexOrThrow17)));
                        arrayList.add(contentValues);
                        moveToFirst = query.moveToNext();
                    } else {
                        moveToFirst = query.moveToNext();
                    }
                }
            } else {
                moveToFirst = query.moveToNext();
            }
        }
        query.close();
        int i9 = 0;
        int i10 = 0;
        for (int i11 = 0; i11 < i2; i11++) {
            if (iArr2[i11] != 0) {
                i10++;
                iArr2[i11] = i9;
                i9++;
            }
        }
        if (i10 <= 2) {
            Toast.makeText(context, R.string.import_no_data, 0).show();
            return;
        }
        int min = (Math.min(i10, 9) - 1) / 2;
        int[] iArr4 = new int[i2];
        iArr4[0] = min;
        iArr4[1] = min - 1;
        for (int i12 = 1; i12 < i2; i12++) {
            if (iArr2[i12] == 0) {
                iArr4[i12] = 0;
            } else if (iArr2[i12] <= min) {
                iArr4[i12] = iArr2[i12] - 2;
            } else {
                iArr4[i12] = iArr2[i12];
            }
        }
        ff.b(min);
        ff.b(context, min);
        HashMap hashMap2 = new HashMap();
        HashMap hashMap3 = new HashMap();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            ContentValues contentValues2 = (ContentValues) it.next();
            Integer asInteger = contentValues2.getAsInteger("_id");
            contentValues2.remove("_id");
            Integer asInteger2 = contentValues2.getAsInteger("screen");
            if (contentValues2.getAsInteger("itemType").intValue() == 2 && (i = iArr4[asInteger2.intValue()]) < 9) {
                contentValues2.put("screen", Integer.valueOf(i));
                String uri2 = contentResolver.insert(ba.a, contentValues2).toString();
                try {
                    hashMap2.put("" + asInteger, uri2.substring(uri2.indexOf("/favorites/") + "/favorites/".length(), uri2.indexOf("?notify")));
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            ContentValues contentValues3 = (ContentValues) it2.next();
            contentValues3.getAsInteger("_id");
            Integer asInteger3 = contentValues3.getAsInteger("screen");
            Integer asInteger4 = contentValues3.getAsInteger("itemType");
            Integer asInteger5 = contentValues3.getAsInteger("container");
            if (asInteger4.intValue() != 2) {
                int i13 = iArr4[asInteger3.intValue()];
                if (i13 < 9) {
                    contentValues3.put("screen", Integer.valueOf(i13));
                    if (!(asInteger5.intValue() == -100 || (str3 = (String) hashMap2.get("" + asInteger5)) == null || str3.equals("null"))) {
                        try {
                            int intValue = Integer.valueOf(str3).intValue();
                            Integer num = (Integer) hashMap3.get(str3);
                            int intValue2 = num != null ? num.intValue() : 0;
                            if (intValue2 < 12) {
                                contentValues3.put("container", Integer.valueOf(intValue));
                                hashMap3.put(str3, Integer.valueOf(intValue2 + 1));
                            }
                        } catch (Exception e3) {
                        }
                    }
                    contentResolver.insert(ba.a, contentValues3);
                }
            }
        }
        ff.a(Math.min(i10, 9));
    }

    private static am infoFromApplicationIntent(Context context, Intent intent) {
        ActivityInfo activityInfo;
        ComponentName component = intent.getComponent();
        PackageManager packageManager = context.getPackageManager();
        try {
            activityInfo = packageManager.getActivityInfo(component, 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(LOG_TAG, "Couldn't find ActivityInfo for selected application", e);
            activityInfo = null;
        }
        if (activityInfo == null) {
            return null;
        }
        am amVar = new am();
        amVar.a = activityInfo.loadLabel(packageManager);
        if (amVar.a == null) {
            amVar.a = activityInfo.name;
        }
        amVar.a(component);
        amVar.d = activityInfo.loadIcon(packageManager);
        amVar.n = -1;
        return amVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:4:0x002b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.tencent.launcher.am infoFromShortcutIntent(android.content.Context r11, android.content.Intent r12) {
        /*
            r5 = 1
            r10 = 0
            r9 = 0
            java.lang.String r1 = "android.intent.extra.shortcut.INTENT"
            android.os.Parcelable r1 = r12.getParcelableExtra(r1)
            android.content.Intent r1 = (android.content.Intent) r1
            java.lang.String r2 = "android.intent.extra.shortcut.NAME"
            java.lang.String r4 = r12.getStringExtra(r2)
            java.lang.String r2 = "android.intent.extra.shortcut.ICON"
            android.os.Parcelable r2 = r12.getParcelableExtra(r2)
            android.graphics.Bitmap r2 = (android.graphics.Bitmap) r2
            if (r2 == 0) goto L_0x0047
            com.tencent.launcher.hp r3 = new com.tencent.launcher.hp
            r3.<init>(r2)
            android.graphics.drawable.Drawable r2 = com.tencent.launcher.Launcher.sIconBackgroundDrawable
            android.graphics.drawable.Drawable r2 = com.tencent.launcher.a.a(r11, r3, r2, r9)
            r3 = r5
            r6 = r2
            r2 = r9
        L_0x0029:
            if (r6 != 0) goto L_0x0033
            android.content.pm.PackageManager r6 = r11.getPackageManager()
            android.graphics.drawable.Drawable r6 = r6.getDefaultActivityIcon()
        L_0x0033:
            com.tencent.launcher.am r7 = new com.tencent.launcher.am
            r7.<init>()
            r7.d = r6
            r7.f = r5
            r7.e = r5
            r7.a = r4
            r7.c = r1
            r7.g = r3
            r7.h = r2
            return r7
        L_0x0047:
            java.lang.String r2 = "android.intent.extra.shortcut.ICON_RESOURCE"
            android.os.Parcelable r3 = r12.getParcelableExtra(r2)
            if (r3 == 0) goto L_0x0091
            boolean r2 = r3 instanceof android.content.Intent.ShortcutIconResource
            if (r2 == 0) goto L_0x0091
            r0 = r3
            android.content.Intent$ShortcutIconResource r0 = (android.content.Intent.ShortcutIconResource) r0     // Catch:{ Exception -> 0x0071 }
            r2 = r0
            android.content.pm.PackageManager r5 = r11.getPackageManager()     // Catch:{ Exception -> 0x008f }
            java.lang.String r6 = r2.packageName     // Catch:{ Exception -> 0x008f }
            android.content.res.Resources r5 = r5.getResourcesForApplication(r6)     // Catch:{ Exception -> 0x008f }
            java.lang.String r6 = r2.resourceName     // Catch:{ Exception -> 0x008f }
            r7 = 0
            r8 = 0
            int r6 = r5.getIdentifier(r6, r7, r8)     // Catch:{ Exception -> 0x008f }
            android.graphics.drawable.Drawable r3 = r5.getDrawable(r6)     // Catch:{ Exception -> 0x008f }
            r5 = r10
            r6 = r3
            r3 = r10
            goto L_0x0029
        L_0x0071:
            r2 = move-exception
            r2 = r9
        L_0x0073:
            java.lang.String r5 = "Launcher"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "Could not load shortcut icon: "
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r3 = r6.append(r3)
            java.lang.String r3 = r3.toString()
            android.util.Log.w(r5, r3)
            r3 = r10
            r5 = r10
            r6 = r9
            goto L_0x0029
        L_0x008f:
            r5 = move-exception
            goto L_0x0073
        L_0x0091:
            r2 = r9
            r3 = r10
            r5 = r10
            r6 = r9
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.launcher.Launcher.infoFromShortcutIntent(android.content.Context, android.content.Intent):com.tencent.launcher.am");
    }

    private void initDockBar() {
        DockBar dockBar = this.mDockBar;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < 5) {
                if (i2 != 2) {
                    DockView createDockItem = createDockItem(dockBar);
                    DockBar.LayoutParams layoutParams = (DockBar.LayoutParams) createDockItem.getLayoutParams();
                    if (layoutParams == null) {
                        layoutParams = new DockBar.LayoutParams(i2);
                    } else {
                        layoutParams.a = i2;
                    }
                    dockBar.addView(createDockItem, layoutParams);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private void initDockButtonGroup() {
        DockButtonGroup dockButtonGroup = this.mDockButtonGroup;
        for (int i = 0; i < 3; i++) {
            if (i != 1) {
                View childAt = dockButtonGroup.getChildAt(i);
                if (childAt == null) {
                    childAt = dockButtonGroup.getChildAt(i - 1);
                }
                DockButtonGroup.LayoutParams layoutParams = (DockButtonGroup.LayoutParams) childAt.getLayoutParams();
                if (layoutParams == null) {
                    childAt.setLayoutParams(new DockButtonGroup.LayoutParams(i));
                } else {
                    layoutParams.a = i;
                }
            }
        }
    }

    private void initGridNavigation() {
        if (this.mGrid.c()) {
            QGridLayout a = this.mGrid.a();
            int childCount = a.getChildCount();
            this.mGridNavigation.removeAllViews();
            this.mGridNavigation.a(childCount);
            a.a(this.mGridNavigation);
        }
    }

    private void initNavigation(QNavigation qNavigation) {
        Workspace workspace = this.mWorkspace;
        qNavigation.a(workspace.getChildCount());
        workspace.a(qNavigation);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.home.i.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.launcher.home.i.a(java.lang.String, int):void
      com.tencent.launcher.home.i.a(java.lang.String, long):void
      com.tencent.launcher.home.i.a(java.lang.String, java.lang.String):void
      com.tencent.launcher.home.i.a(java.lang.String, boolean):void */
    private void loadDefaultWallpaper() {
        if (this.isFirstRun) {
            this.isFirstRun = false;
            try {
                Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), R.drawable.wallpaper_default);
                i.a().a("application_first_run", false);
                setWallpaper(decodeResource);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (OutOfMemoryError e2) {
            }
        }
        if (!b.g) {
            this.mWorkspace.b(false);
        }
    }

    private void onAppWidgetReset() {
        this.mAppWidgetHost.startListening();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(boolean, com.tencent.launcher.Launcher, boolean, boolean):void
     arg types: [int, com.tencent.launcher.Launcher, int, int]
     candidates:
      com.tencent.launcher.ff.a(boolean[][], int, int, int):void
      com.tencent.launcher.ff.a(boolean, com.tencent.launcher.Launcher, boolean, boolean):void */
    private void onFavoritesChanged() {
        this.mDesktopLocked = true;
        this.mDrawer.h();
        sModel.a(false, this, false, false);
    }

    private void openFolder(ex exVar) {
        Folder folder;
        if (exVar instanceof bq) {
            Folder a = UserFolder.a((Context) this);
            this.mDragLayer.clearDisappearingChildren();
            folder = a;
        } else if (exVar instanceof dq) {
            folder = (LiveFolder) LayoutInflater.from(this).inflate(((dq) exVar).c == 2 ? R.layout.live_folder_list : R.layout.live_folder_grid, (ViewGroup) null);
        } else {
            return;
        }
        folder.a(this.mDragLayer);
        folder.b(this);
        GridListView.a = false;
        folder.a(exVar);
        exVar.g = true;
        this.mDragLayer.addView(folder);
        this.mWorkspace.A();
        this.flagDockEnable = false;
        if (this.mDrawer.a() && this.mGrid.c()) {
            this.mGrid.a().j();
        }
        if (exVar instanceof bq) {
            shadeViews((bq) exVar, (UserFolder) folder);
            ((UserFolder) folder).a((bq) exVar);
            ((UserFolder) folder).b();
            return;
        }
        folder.a();
    }

    /* access modifiers changed from: private */
    public void pickShortcut(int i, int i2) {
        Bundle bundle = new Bundle();
        ArrayList arrayList = new ArrayList();
        arrayList.add(getString(R.string.system_folder));
        bundle.putStringArrayList("android.intent.extra.shortcut.NAME", arrayList);
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(Intent.ShortcutIconResource.fromContext(this, R.drawable.ic_launcher_folder));
        bundle.putParcelableArrayList("android.intent.extra.shortcut.ICON_RESOURCE", arrayList2);
        Intent intent = new Intent();
        intent.setAction("com.android.launcher.action.CUSTOM_ACTIVITYPICKER");
        intent.putExtra("android.intent.extra.INTENT", new Intent("android.intent.action.CREATE_SHORTCUT"));
        intent.putExtra("android.intent.extra.TITLE", getText(i2));
        if (i == 7) {
            intent.putExtras(bundle);
        }
        startActivityForResult(intent, i);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002e A[SYNTHETIC, Splitter:B:14:0x002e] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003a A[SYNTHETIC, Splitter:B:20:0x003a] */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:32:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0025 A[SYNTHETIC, Splitter:B:9:0x0025] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void readConfiguration(android.content.Context r4, com.tencent.launcher.gr r5) {
        /*
            r0 = 0
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ FileNotFoundException -> 0x0022, IOException -> 0x002b, all -> 0x0034 }
            java.lang.String r2 = "launcher.preferences"
            java.io.FileInputStream r2 = r4.openFileInput(r2)     // Catch:{ FileNotFoundException -> 0x0022, IOException -> 0x002b, all -> 0x0034 }
            r1.<init>(r2)     // Catch:{ FileNotFoundException -> 0x0022, IOException -> 0x002b, all -> 0x0034 }
            java.lang.String r0 = r1.readUTF()     // Catch:{ FileNotFoundException -> 0x0047, IOException -> 0x0044, all -> 0x0042 }
            r5.a = r0     // Catch:{ FileNotFoundException -> 0x0047, IOException -> 0x0044, all -> 0x0042 }
            int r0 = r1.readInt()     // Catch:{ FileNotFoundException -> 0x0047, IOException -> 0x0044, all -> 0x0042 }
            r5.b = r0     // Catch:{ FileNotFoundException -> 0x0047, IOException -> 0x0044, all -> 0x0042 }
            int r0 = r1.readInt()     // Catch:{ FileNotFoundException -> 0x0047, IOException -> 0x0044, all -> 0x0042 }
            r5.c = r0     // Catch:{ FileNotFoundException -> 0x0047, IOException -> 0x0044, all -> 0x0042 }
            r1.close()     // Catch:{ IOException -> 0x003e }
        L_0x0021:
            return
        L_0x0022:
            r1 = move-exception
        L_0x0023:
            if (r0 == 0) goto L_0x0021
            r0.close()     // Catch:{ IOException -> 0x0029 }
            goto L_0x0021
        L_0x0029:
            r0 = move-exception
            goto L_0x0021
        L_0x002b:
            r1 = move-exception
        L_0x002c:
            if (r0 == 0) goto L_0x0021
            r0.close()     // Catch:{ IOException -> 0x0032 }
            goto L_0x0021
        L_0x0032:
            r0 = move-exception
            goto L_0x0021
        L_0x0034:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
        L_0x0038:
            if (r1 == 0) goto L_0x003d
            r1.close()     // Catch:{ IOException -> 0x0040 }
        L_0x003d:
            throw r0
        L_0x003e:
            r0 = move-exception
            goto L_0x0021
        L_0x0040:
            r1 = move-exception
            goto L_0x003d
        L_0x0042:
            r0 = move-exception
            goto L_0x0038
        L_0x0044:
            r0 = move-exception
            r0 = r1
            goto L_0x002c
        L_0x0047:
            r0 = move-exception
            r0 = r1
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.launcher.Launcher.readConfiguration(android.content.Context, com.tencent.launcher.gr):void");
    }

    private void registerContentObservers() {
        ContentResolver contentResolver = getContentResolver();
        contentResolver.registerContentObserver(ba.a, true, this.mFavoritesChangeObserver);
        contentResolver.registerContentObserver(LauncherProvider.a, true, this.mWidgetObserver);
    }

    private void registerIntentReceivers() {
        if (!b.g) {
            if (sWallpaperReceiver == null) {
                Application application = getApplication();
                sWallpaperReceiver = new ig(application, this);
                application.registerReceiver(sWallpaperReceiver, new IntentFilter("android.intent.action.WALLPAPER_CHANGED"));
            } else {
                sWallpaperReceiver.a(this);
            }
        }
        IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
        intentFilter.addAction("android.intent.action.PACKAGE_CHANGED");
        intentFilter.addDataScheme("package");
        registerReceiver(this.mApplicationsReceiver, intentFilter);
        IntentFilter intentFilter2 = new IntentFilter();
        intentFilter2.addAction("android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE");
        intentFilter2.addAction("android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE");
        registerReceiver(this.mApplicationsReceiver, intentFilter2);
        registerReceiver(this.mCloseSystemDialogsReceiver, new IntentFilter("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
        IntentFilter intentFilter3 = new IntentFilter();
        intentFilter3.addAction("com.tencent.launcher.applytheme");
        intentFilter3.setPriority(1000);
        registerReceiver(this.mThemeReceiver, intentFilter3);
        IntentFilter intentFilter4 = new IntentFilter();
        intentFilter4.addAction("com.tencent.launcher.theme.download.complete");
        intentFilter4.setPriority(400);
        registerReceiver(this.mThemeDownReceiver, intentFilter4);
    }

    private ArrayList removeAllItemInfo() {
        Workspace workspace = this.mWorkspace;
        int childCount = workspace.getChildCount();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < childCount; i++) {
            ViewGroup viewGroup = (ViewGroup) workspace.getChildAt(i);
            int childCount2 = viewGroup.getChildCount();
            for (int i2 = 0; i2 < childCount2; i2++) {
                arrayList.add((ha) viewGroup.getChildAt(i2).getTag());
            }
        }
        return arrayList;
    }

    private void removeDockItemForPackage(String str) {
        if (str != null && str.length() > 0) {
            this.mDockBar.a(str);
        }
    }

    private void removeShortcutsForPackage(String str) {
        if (str != null && str.length() > 0) {
            this.mWorkspace.a(str);
        }
    }

    /* access modifiers changed from: private */
    public void resetDrawerMode() {
        this.mDrawer.a(this.mGrid);
        this.mGrid.a(this);
        initGridNavigation();
        this.mGrid.a(sModel.e());
    }

    private void restartHome() {
        getWindow().closeAllPanels();
        try {
            Process.killProcess(Process.myPid());
            finish();
            startActivity(getIntent());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void restoreState(Bundle bundle) {
        if (bundle != null) {
            int i = bundle.getInt(RUNTIME_STATE_CURRENT_SCREEN, -1);
            if (i >= 0) {
                this.mWorkspace.a(i);
            }
            int i2 = bundle.getInt(RUNTIME_STATE_PENDING_ADD_SCREEN, -1);
            if (i2 >= 0) {
                this.mAddItemCellInfo = new bs();
                bs bsVar = this.mAddItemCellInfo;
                bsVar.g = true;
                bsVar.f = i2;
                bsVar.b = bundle.getInt(RUNTIME_STATE_PENDING_ADD_CELL_X);
                bsVar.c = bundle.getInt(RUNTIME_STATE_PENDING_ADD_CELL_Y);
                bsVar.d = bundle.getInt(RUNTIME_STATE_PENDING_ADD_SPAN_X);
                bsVar.e = bundle.getInt(RUNTIME_STATE_PENDING_ADD_SPAN_Y);
                boolean[] booleanArray = bundle.getBooleanArray(RUNTIME_STATE_PENDING_ADD_OCCUPIED_CELLS);
                int i3 = bundle.getInt(RUNTIME_STATE_PENDING_ADD_COUNT_X);
                int i4 = bundle.getInt(RUNTIME_STATE_PENDING_ADD_COUNT_Y);
                if (bsVar.b < 0 || bsVar.c < 0) {
                    bsVar.j = Integer.MIN_VALUE;
                    bsVar.i = Integer.MIN_VALUE;
                    bsVar.l = Integer.MIN_VALUE;
                    bsVar.k = Integer.MIN_VALUE;
                    bsVar.a();
                } else {
                    boolean[][] zArr = (boolean[][]) Array.newInstance(Boolean.TYPE, i3, i4);
                    for (int i5 = 0; i5 < i4; i5++) {
                        for (int i6 = 0; i6 < i3; i6++) {
                            zArr[i6][i5] = booleanArray[(i5 * i3) + i6];
                        }
                    }
                    CellLayout.b(bsVar, bsVar.b, bsVar.c, i3, i4, zArr);
                }
                this.mRestoring = true;
            }
            if (bundle.getBoolean(RUNTIME_STATE_PENDING_FOLDER_RENAME, false)) {
                this.mFolderInfo = sModel.b(this, bundle.getLong(RUNTIME_STATE_PENDING_FOLDER_RENAME_ID));
                this.mRestoring = true;
            }
        }
    }

    private void sendQuitBroadcast() {
        BaseApp.b().sendBroadcast(new Intent("com.tencent.qqlauncher.QUIT"));
    }

    static void setScreen(int i) {
        synchronized (sLock) {
            sScreen = i;
        }
    }

    private void setWallpaperDimension() {
        WallpaperManager wallpaperManager = (WallpaperManager) getSystemService("wallpaper");
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        int width = defaultDisplay.getWidth();
        int height = defaultDisplay.getHeight();
        boolean z = width < height;
        int i = z ? width : height;
        if (!z) {
            height = width;
        }
        wallpaperManager.suggestDesiredDimensions(i * 2, height);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.home.i.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.launcher.home.i.b(java.lang.String, int):int
      com.tencent.launcher.home.i.b(java.lang.String, java.lang.String):java.lang.String
      com.tencent.launcher.home.i.b(java.lang.String, boolean):boolean */
    private void setupViews() {
        this.mDragLayer = (DragLayer) findViewById(R.id.drag_layer);
        DragLayer dragLayer = this.mDragLayer;
        this.mThumbnailWorkspace = (ThumbnailWorkspace) findViewById(R.id.thumbnail_workspace);
        this.mThumbnailWorkspace.a(this.mThumbnailListener);
        this.mWorkspace = (Workspace) dragLayer.findViewById(R.id.workspace);
        Workspace workspace = this.mWorkspace;
        this.mNavigation = (QNavigation) dragLayer.findViewById(R.id.navigation);
        QNavigation qNavigation = this.mNavigation;
        this.mGrid = new fn();
        fn fnVar = this.mGrid;
        this.mDrawer = (ApplicationsDrawer) findViewById(R.id.grid_container);
        ApplicationsDrawer applicationsDrawer = this.mDrawer;
        applicationsDrawer.a(this);
        applicationsDrawer.a(fnVar);
        fnVar.a(this);
        fnVar.a(dragLayer);
        applicationsDrawer.a((TextView) findViewById(R.id.indicate_layer));
        l lVar = this.mThemeManager;
        this.mHandleButton = (Button) findViewById(R.id.btn_all_apps);
        Button button = this.mHandleButton;
        lVar.a = new WeakReference(button);
        ib ibVar = new ib(this, button);
        button.setOnClickListener(ibVar);
        button.setOnLongClickListener(this);
        this.mNewAppNofifyView = (ImageView) findViewById(R.id.new_app_notify);
        cq cqVar = new cq(this);
        this.mDockBar = (DockBar) dragLayer.findViewById(R.id.dock_bar);
        lVar.f = new WeakReference(this.mDockBar);
        DockBar dockBar = this.mDockBar;
        dockBar.a(this);
        dockBar.a(workspace);
        dockBar.setOnLongClickListener(this);
        dockBar.a((dt) dragLayer);
        dockBar.a(cqVar);
        this.mDockButtonGroup = (DockButtonGroup) findViewById(R.id.dock_button_group);
        this.mDockButtonGroup.a(cqVar);
        initDockButtonGroup();
        initDockBar();
        this.mHomeFlipper = (ViewFlipper) findViewById(R.id.home_flip);
        this.mHomeButton = (DrawerHomeBar) findViewById(R.id.back_home);
        this.mHomeButton.a(this);
        this.mHomeIconButton = (ImageView) findViewById(R.id.back_home_image);
        lVar.b = new WeakReference(this.mHomeIconButton);
        this.mHomeIconButton.setOnClickListener(ibVar);
        this.mMarketEntry = (ImageView) findViewById(R.id.market_entry);
        lVar.d = new WeakReference(this.mMarketEntry);
        this.mMarketEntry.setOnClickListener(ibVar);
        this.mSearchApp = (ImageView) findViewById(R.id.search_app);
        lVar.e = new WeakReference(this.mSearchApp);
        this.mSearchApp.setOnClickListener(ibVar);
        this.mGridNavigation = (QNavigation) applicationsDrawer.findViewById(R.id.grid_navigation);
        DeleteZone deleteZone = (DeleteZone) dragLayer.findViewById(R.id.delete_zone);
        lVar.c = new WeakReference(deleteZone);
        int f = ff.f();
        int g = ff.g();
        for (int i = 0; i < f; i++) {
            workspace.addView((CellLayout) this.mInflater.inflate((int) R.layout.workspace_screen, (ViewGroup) null));
        }
        workspace.g(g);
        workspace.setOnLongClickListener(this);
        workspace.a((dt) dragLayer);
        workspace.a(this);
        workspace.a((TextView) findViewById(R.id.indicate_layer));
        initNavigation(qNavigation);
        initGridNavigation();
        cr crVar = new cr(this, qNavigation, workspace, deleteZone);
        dragLayer.a(new cs(this, workspace));
        this.mPreZone = (PageTurningZone) findViewById(R.id.pre_zone);
        this.mNextZone = (PageTurningZone) findViewById(R.id.next_zone);
        this.mPreZone.a(this.mDragLayer.a());
        this.mNextZone.a(this.mDragLayer.a());
        deleteZone.a(this);
        deleteZone.a(dragLayer);
        dragLayer.a(crVar);
        workspace.e(Integer.parseInt(this.mMSFConfig.b("setting_switchdesktop_specialeffects", "0")));
        workspace.a(this.mMSFConfig.b("setting_switchdesktop_wallpaperscroll", true));
        workspace.c(this.mMSFConfig.b("setting_switchdesktop_screenrecycleswitch", false));
        initHasNewAppsNofify();
        if (!this.mThemeManager.d()) {
            lVar.c();
        }
    }

    private void shadeViews(bq bqVar, UserFolder userFolder) {
        int i;
        int i2;
        int i3;
        int i4;
        switch ((bqVar.b.size() + 1) / 4) {
            case 0:
                i = 200;
                break;
            case 1:
                i = 229;
                break;
            case 2:
                i = 260;
                break;
            default:
                i = 200;
                break;
        }
        AccelerateInterpolator accelerateInterpolator = new AccelerateInterpolator();
        Animation loadAnimation = AnimationUtils.loadAnimation(this.mContext, R.anim.shade_anim);
        loadAnimation.setFillAfter(true);
        loadAnimation.setInterpolator(accelerateInterpolator);
        loadAnimation.setDuration((long) i);
        Workspace workspace = this.mWorkspace;
        CellLayout cellLayout = (CellLayout) workspace.getChildAt(workspace.d());
        int b = userFolder.b(workspace.getWidth(), workspace.getHeight());
        int height = workspace.getHeight();
        FolderIcon folderIcon = bqVar.i;
        DockView dockView = bqVar.k;
        DrawerTextView drawerTextView = bqVar.j;
        int i5 = 0;
        if (bqVar.n == -100) {
            folderIcon.f();
            int top = folderIcon.getTop();
            i4 = folderIcon.getLeft();
            int width = folderIcon.getWidth();
            int height2 = folderIcon.getHeight() - folderIcon.c();
            i3 = width;
            i5 = top;
            i2 = height2;
        } else if (bqVar.n == -200) {
            dockView.d();
            Rect rect = this.mTempRect;
            rect.setEmpty();
            findDrayLayerRect(rect, dockView);
            int i6 = rect.top;
            int i7 = rect.left;
            int width2 = dockView.getWidth();
            i2 = dockView.getHeight();
            int i8 = width2;
            i5 = i6;
            i4 = i7;
            i3 = i8;
        } else if (bqVar.n == -300) {
            drawerTextView.c();
            Rect rect2 = this.mTempRect;
            rect2.setEmpty();
            findDrayLayerRect(rect2, drawerTextView);
            int i9 = rect2.top;
            int left = drawerTextView.getLeft() % this.mDragLayer.getWidth();
            int width3 = drawerTextView.getWidth();
            int height3 = drawerTextView.getHeight();
            Layout layout = drawerTextView.getLayout();
            int lineBottom = height3 - (((int) ((float) (layout.getLineBottom(0) - layout.getLineTop(0)))) + 7);
            i4 = left;
            i2 = lineBottom;
            int i10 = width3;
            i5 = i9;
            i3 = i10;
        } else {
            i2 = 0;
            i3 = 0;
            i4 = 0;
        }
        int i11 = (bqVar.n == -200 || (height - i5) - i2 > b) ? 0 : -((b - ((height - i5) - i2)) + 3);
        userFolder.a(i11, i3, i2, i4, i5);
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.setDuration((long) i);
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) i11);
        translateAnimation.setInterpolator(accelerateInterpolator);
        translateAnimation.setDuration((long) i);
        animationSet.addAnimation(translateAnimation);
        animationSet.addAnimation(loadAnimation);
        animationSet.setFillAfter(true);
        animationSet.setInterpolator(accelerateInterpolator);
        if (bqVar.n != -300) {
            cellLayout.setDrawingCacheQuality(524288);
            cellLayout.startAnimation(animationSet);
            this.mNavigation.startAnimation(loadAnimation);
            this.mHandleButton.startAnimation(loadAnimation);
            this.mDockBar.startAnimation(loadAnimation);
        } else if (i11 == 0) {
            this.mDrawer.a(loadAnimation);
        } else {
            this.mDrawer.a(animationSet);
        }
    }

    private void showAddDialog(bs bsVar) {
        this.mWorkspace.q();
        this.mAddItemCellInfo = bsVar;
        this.mWaitingForResult = true;
        showDialog(1);
    }

    public static void showImportDataDialogForSettingActivity(Context context) {
        if (hasMultiLauncher(context) == 0) {
            Toast.makeText(context, (int) R.string.no_launcher_to_import, 0).show();
        } else {
            showMultiLauncherImportSelectDialogForSettingActivity(context);
        }
    }

    public static void showImportingDialog(String str, Context context, boolean z) {
        Dialog dialog = new Dialog(context, R.style.FullHeightDialog);
        dialog.setContentView((int) R.layout.importing_data);
        dialog.show();
        dialog.setCancelable(false);
        com.tencent.launcher.home.a.b(context);
        new er(str, context, z).start();
    }

    private static void showInstallDlg(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("安装个人中心、QQlite");
        builder.setMessage("安装个人中心、QQlite?");
        builder.setPositiveButton((int) R.string.percenter_install_button_continue, new ep(context));
        builder.setNegativeButton((int) R.string.cancel, (DialogInterface.OnClickListener) null);
        AlertDialog create = builder.create();
        create.setCancelable(false);
        create.show();
    }

    /* access modifiers changed from: private */
    public void showMultiLauncherImportSelectDialog() {
        ArrayList multiLauncherInfos = getMultiLauncherInfos(this.mContext);
        com.tencent.module.setting.e eVar = new com.tencent.module.setting.e(this);
        eVar.a((int) R.string.import_data_multi_select);
        eVar.b();
        eVar.b(new fe(multiLauncherInfos, this.mContext), new eq(this, multiLauncherInfos));
        eVar.c().show();
    }

    private static void showMultiLauncherImportSelectDialogForSettingActivity(Context context) {
        ArrayList multiLauncherInfos = getMultiLauncherInfos(context);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle((int) R.string.import_data_multi_select);
        builder.setInverseBackgroundForced(true);
        builder.setSingleChoiceItems(new fe(multiLauncherInfos, context), -1, new em(multiLauncherInfos, context));
        builder.create().show();
    }

    private void showNavigation() {
        this.mNavigation.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_in));
        this.mNavigation.setVisibility(0);
    }

    private void showNotifications() {
        try {
            Object systemService = getSystemService("statusbar");
            if (systemService != null) {
                systemService.getClass().getMethod("expand", new Class[0]).invoke(systemService, new Object[0]);
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    public static void showSecondSureDialogForActivity(String str, Context context, boolean z) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle((int) R.string.import_data);
        builder.setMessage((int) R.string.import_data_second_sure);
        builder.setInverseBackgroundForced(true);
        builder.setPositiveButton((int) R.string.import_btn, new en(str, context, z)).setNegativeButton((int) R.string.cancel, new ej());
        builder.create().show();
    }

    private void startLoaders() {
        sIconBackgroundDrawable = getResources().getDrawable(R.drawable.iconbg);
        sModel.a(!this.mLocaleChanged, this, this.mLocaleChanged, sModel.a(this, this.mLocaleChanged));
        this.mRestoring = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void startWallpaper() {
        Intent intent = new Intent("android.intent.action.SET_WALLPAPER");
        Intent intent2 = new Intent();
        intent2.setAction("com.android.launcher.action.CUSTOM_ACTIVITYPICKER");
        intent2.putExtra("android.intent.extra.TITLE", getString(R.string.title_select_wallpaper));
        intent2.putExtra("android.intent.extra.INTENT", intent);
        intent2.putExtra("wallpaper", true);
        startActivity(intent2);
    }

    /* access modifiers changed from: private */
    public void updateAllItemInfo(ArrayList arrayList) {
        ha haVar;
        if (arrayList != null && arrayList.size() > 0 && this.mWorkspace != null) {
            Workspace workspace = this.mWorkspace;
            int childCount = workspace.getChildCount();
            for (int i = 0; i < childCount; i++) {
                ViewGroup viewGroup = (ViewGroup) workspace.getChildAt(i);
                if (viewGroup != null) {
                    int childCount2 = viewGroup.getChildCount();
                    for (int i2 = 0; i2 < childCount2; i2++) {
                        View childAt = viewGroup.getChildAt(i2);
                        if (!(childAt == null || (haVar = (ha) childAt.getTag()) == null)) {
                            haVar.o = i;
                            ff.h(this.mContext, haVar);
                            arrayList.remove(haVar);
                        }
                    }
                }
            }
            int size = arrayList.size();
            for (int i3 = 0; i3 < size; i3++) {
                ha haVar2 = (ha) arrayList.get(i3);
                if (haVar2 != null) {
                    ff.i(this.mContext, haVar2);
                }
            }
            ff.a(childCount);
        }
    }

    private void updateDockItemForPackage(String str) {
        if (str != null && str.length() > 0) {
            this.mDockBar.b(str);
        }
    }

    private void updateShortcutsForPackage(String str) {
        if (str != null && str.length() > 0) {
            this.mWorkspace.b(str);
        }
    }

    private void viewVisibleManager(View view) {
        Iterator it = this.mViewList.iterator();
        while (it.hasNext()) {
            View view2 = (View) it.next();
            if (view2 == view) {
                view2.setVisibility(0);
            } else {
                view2.setVisibility(4);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0038 A[SYNTHETIC, Splitter:B:17:0x0038] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0044 A[SYNTHETIC, Splitter:B:23:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:36:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:37:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0026 A[SYNTHETIC, Splitter:B:9:0x0026] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void writeConfiguration(android.content.Context r5, com.tencent.launcher.gr r6) {
        /*
            r0 = 0
            java.io.DataOutputStream r1 = new java.io.DataOutputStream     // Catch:{ FileNotFoundException -> 0x0023, IOException -> 0x002c, all -> 0x003e }
            java.lang.String r2 = "launcher.preferences"
            r3 = 0
            java.io.FileOutputStream r2 = r5.openFileOutput(r2, r3)     // Catch:{ FileNotFoundException -> 0x0023, IOException -> 0x002c, all -> 0x003e }
            r1.<init>(r2)     // Catch:{ FileNotFoundException -> 0x0023, IOException -> 0x002c, all -> 0x003e }
            java.lang.String r0 = r6.a     // Catch:{ FileNotFoundException -> 0x0056, IOException -> 0x0053, all -> 0x004c }
            r1.writeUTF(r0)     // Catch:{ FileNotFoundException -> 0x0056, IOException -> 0x0053, all -> 0x004c }
            int r0 = r6.b     // Catch:{ FileNotFoundException -> 0x0056, IOException -> 0x0053, all -> 0x004c }
            r1.writeInt(r0)     // Catch:{ FileNotFoundException -> 0x0056, IOException -> 0x0053, all -> 0x004c }
            int r0 = r6.c     // Catch:{ FileNotFoundException -> 0x0056, IOException -> 0x0053, all -> 0x004c }
            r1.writeInt(r0)     // Catch:{ FileNotFoundException -> 0x0056, IOException -> 0x0053, all -> 0x004c }
            r1.flush()     // Catch:{ FileNotFoundException -> 0x0056, IOException -> 0x0053, all -> 0x004c }
            r1.close()     // Catch:{ IOException -> 0x0048 }
        L_0x0022:
            return
        L_0x0023:
            r1 = move-exception
        L_0x0024:
            if (r0 == 0) goto L_0x0022
            r0.close()     // Catch:{ IOException -> 0x002a }
            goto L_0x0022
        L_0x002a:
            r0 = move-exception
            goto L_0x0022
        L_0x002c:
            r1 = move-exception
        L_0x002d:
            java.lang.String r1 = "launcher.preferences"
            java.io.File r1 = r5.getFileStreamPath(r1)     // Catch:{ all -> 0x004e }
            r1.delete()     // Catch:{ all -> 0x004e }
            if (r0 == 0) goto L_0x0022
            r0.close()     // Catch:{ IOException -> 0x003c }
            goto L_0x0022
        L_0x003c:
            r0 = move-exception
            goto L_0x0022
        L_0x003e:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0042:
            if (r1 == 0) goto L_0x0047
            r1.close()     // Catch:{ IOException -> 0x004a }
        L_0x0047:
            throw r0
        L_0x0048:
            r0 = move-exception
            goto L_0x0022
        L_0x004a:
            r1 = move-exception
            goto L_0x0047
        L_0x004c:
            r0 = move-exception
            goto L_0x0042
        L_0x004e:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0042
        L_0x0053:
            r0 = move-exception
            r0 = r1
            goto L_0x002d
        L_0x0056:
            r0 = move-exception
            r0 = r1
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.launcher.Launcher.writeConfiguration(android.content.Context, com.tencent.launcher.gr):void");
    }

    /* access modifiers changed from: package-private */
    public final void addAppWidget(Intent intent) {
        int intExtra = intent.getIntExtra("appWidgetId", -1);
        String stringExtra = intent.getStringExtra(EXTRA_CUSTOM_WIDGET);
        if (SEARCH_WIDGET.equals(stringExtra)) {
            this.mAppWidgetHost.deleteAppWidgetId(intExtra);
            addSearch();
        } else if (SWITCHER_WIDGET.equals(stringExtra)) {
            this.mAppWidgetHost.deleteAppWidgetId(intExtra);
            addSwitcher();
        } else if (TASK_WIDGET.equals(stringExtra)) {
            this.mAppWidgetHost.deleteAppWidgetId(intExtra);
            addTaskManager();
        } else if (SOSO_WIDGET.equals(stringExtra) && b.h) {
            this.mAppWidgetHost.deleteAppWidgetId(intExtra);
            addTSosoWiget();
        } else if (QQ_WIDGET.equals(stringExtra)) {
            this.mAppWidgetHost.deleteAppWidgetId(intExtra);
            showQQWidgetDialog();
        } else {
            AppWidgetProviderInfo appWidgetInfo = this.mAppWidgetManager.getAppWidgetInfo(intExtra);
            if (appWidgetInfo.configure != null) {
                Intent intent2 = new Intent("android.appwidget.action.APPWIDGET_CONFIGURE");
                intent2.setComponent(appWidgetInfo.configure);
                intent2.putExtra("appWidgetId", intExtra);
                startActivityForResult(intent2, 5);
                return;
            }
            onActivityResult(5, -1, intent);
        }
    }

    public final void addDesktopItem(bq bqVar) {
        sModel.a((ha) bqVar);
    }

    public final void addFolder(bq bqVar) {
        sModel.a((ex) bqVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void
     arg types: [android.content.Context, com.tencent.launcher.bq, int, int, int, int, int]
     candidates:
      com.tencent.launcher.ff.a(android.database.Cursor, android.content.Context, int, int, int, int, android.content.Intent):com.tencent.launcher.am
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, boolean):void
     arg types: [com.tencent.launcher.FolderIcon, int, int, int, int, boolean]
     candidates:
      com.tencent.launcher.Workspace.a(com.tencent.launcher.cm, int, int, java.lang.Object, com.tencent.launcher.CellLayout, boolean):void
      com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, int):void
      com.tencent.launcher.Workspace.a(com.tencent.launcher.cm, int, int, int, int, java.lang.Object):boolean
      com.tencent.launcher.e.a(com.tencent.launcher.cm, int, int, int, int, java.lang.Object):boolean
      com.tencent.launcher.gw.a(com.tencent.launcher.cm, int, int, int, int, java.lang.Object):boolean
      com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public final void addFolder(boolean z) {
        bq bqVar = new bq();
        bqVar.h = getText(R.string.folder_name);
        bs bsVar = this.mAddItemCellInfo;
        bsVar.f = this.mWorkspace.d();
        if (findSingleSlot(bsVar)) {
            ff.a(this.mContext, (ha) bqVar, -100L, this.mWorkspace.d(), bsVar.b, bsVar.c, false);
            sModel.a((ha) bqVar);
            sModel.a((ex) bqVar);
            this.mWorkspace.a((View) FolderIcon.a(this, (ViewGroup) this.mWorkspace.getChildAt(this.mWorkspace.d()), bqVar), bsVar.b, bsVar.c, 1, 1, z);
        }
    }

    /* access modifiers changed from: package-private */
    public final void addLiveFolder(Intent intent) {
        String string = getResources().getString(R.string.group_folder);
        String stringExtra = intent.getStringExtra("android.intent.extra.shortcut.NAME");
        if (string == null || !string.equals(stringExtra)) {
            this.mWaitingForResult = true;
            super.startActivityForResult(intent, 4);
            return;
        }
        this.mWorkspace.q();
        this.mFolderInfo = null;
        this.mRenameFolderType = 1;
        this.mWaitingForResult = true;
        showDialog(2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, boolean):void
     arg types: [com.tencent.module.qqwidget.e, int, int, int, int, boolean]
     candidates:
      com.tencent.launcher.Workspace.a(com.tencent.launcher.cm, int, int, java.lang.Object, com.tencent.launcher.CellLayout, boolean):void
      com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, int):void
      com.tencent.launcher.Workspace.a(com.tencent.launcher.cm, int, int, int, int, java.lang.Object):boolean
      com.tencent.launcher.e.a(com.tencent.launcher.cm, int, int, int, int, java.lang.Object):boolean
      com.tencent.launcher.gw.a(com.tencent.launcher.cm, int, int, int, int, java.lang.Object):boolean
      com.tencent.launcher.Workspace.a(android.view.View, int, int, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void
     arg types: [com.tencent.launcher.Launcher, com.tencent.module.qqwidget.a, int, int, int, int, int]
     candidates:
      com.tencent.launcher.ff.a(android.database.Cursor, android.content.Context, int, int, int, int, android.content.Intent):com.tencent.launcher.am
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public final void addQQWidget(a aVar) {
        bs bsVar = this.mAddItemCellInfo;
        int[] iArr = this.mCellCoordinates;
        int i = aVar.r;
        int i2 = aVar.s;
        if (findSlot(bsVar, iArr, i, i2)) {
            try {
                e a = h.a().a(this, aVar);
                if (a != null) {
                    a.setTag(aVar);
                    this.mWorkspace.a((View) a, iArr[0], iArr[1], aVar.r, i2, !this.mDesktopLocked);
                    this.mWorkspace.requestLayout();
                    a.a();
                    sModel.a(aVar);
                    ff.a((Context) this, (ha) aVar, -100L, this.mWorkspace.d(), iArr[0], iArr[1], false);
                }
            } catch (QQWidgetFormatExcepiton e) {
                e.printStackTrace();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void
     arg types: [com.tencent.launcher.Launcher, com.tencent.launcher.bd, int, int, int, int, int]
     candidates:
      com.tencent.launcher.ff.a(android.database.Cursor, android.content.Context, int, int, int, int, android.content.Intent):com.tencent.launcher.am
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public final void addSearch() {
        bd b = bd.b();
        bs bsVar = this.mAddItemCellInfo;
        int[] iArr = this.mCellCoordinates;
        int i = b.r;
        int i2 = b.s;
        if (findSlot(bsVar, iArr, i, i2)) {
            sModel.a(b);
            ff.a((Context) this, (ha) b, -100L, this.mWorkspace.d(), iArr[0], iArr[1], false);
            View inflate = this.mInflater.inflate(b.h, (ViewGroup) null);
            inflate.setTag(b);
            ((Search) inflate.findViewById(R.id.widget_search)).a(this);
            this.mWorkspace.a(inflate, iArr[0], iArr[1], b.r, i2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void
     arg types: [com.tencent.launcher.Launcher, com.tencent.launcher.bd, int, int, int, int, int]
     candidates:
      com.tencent.launcher.ff.a(android.database.Cursor, android.content.Context, int, int, int, int, android.content.Intent):com.tencent.launcher.am
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public final void addSwitcher() {
        bd c = bd.c();
        bs bsVar = this.mAddItemCellInfo;
        int[] iArr = this.mCellCoordinates;
        int i = c.r;
        int i2 = c.s;
        if (findSlot(bsVar, iArr, i, i2)) {
            sModel.a(c);
            ff.a((Context) this, (ha) c, -100L, this.mWorkspace.d(), iArr[0], iArr[1], false);
            View inflate = this.mInflater.inflate(c.h, (ViewGroup) null);
            inflate.setTag(c);
            this.mWorkspace.a(inflate, iArr[0], iArr[1], c.r, i2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void
     arg types: [com.tencent.launcher.Launcher, com.tencent.launcher.bd, int, int, int, int, int]
     candidates:
      com.tencent.launcher.ff.a(android.database.Cursor, android.content.Context, int, int, int, int, android.content.Intent):com.tencent.launcher.am
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public final void addTSosoWiget() {
        bd e = bd.e();
        bs bsVar = this.mAddItemCellInfo;
        int[] iArr = this.mCellCoordinates;
        int i = e.r;
        int i2 = e.s;
        if (findSlot(bsVar, iArr, i, i2)) {
            sModel.a(e);
            ff.a((Context) this, (ha) e, -100L, this.mWorkspace.d(), iArr[0], iArr[1], false);
            View inflate = this.mInflater.inflate(e.h, (ViewGroup) null);
            inflate.setTag(e);
            this.mWorkspace.a(inflate, iArr[0], iArr[1], e.r, i2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void
     arg types: [com.tencent.launcher.Launcher, com.tencent.launcher.bd, int, int, int, int, int]
     candidates:
      com.tencent.launcher.ff.a(android.database.Cursor, android.content.Context, int, int, int, int, android.content.Intent):com.tencent.launcher.am
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public final void addTaskManager() {
        bd d = bd.d();
        bs bsVar = this.mAddItemCellInfo;
        int[] iArr = this.mCellCoordinates;
        int i = d.r;
        int i2 = d.s;
        if (findSlot(bsVar, iArr, i, i2)) {
            sModel.a(d);
            ff.a((Context) this, (ha) d, -100L, this.mWorkspace.d(), iArr[0], iArr[1], false);
            View inflate = this.mInflater.inflate(d.h, (ViewGroup) null);
            inflate.setTag(d);
            this.mWorkspace.a(inflate, iArr[0], iArr[1], d.r, i2);
        }
    }

    public final void checkedPerCenterRemoved(String str) {
        a a;
        if (str.equals(PERSONCENTER_PACKAGE_NAME) && (a = sModel.a((String) PERSONCENTER_PACKAGE_NAME)) != null) {
            CellLayout cellLayout = (CellLayout) this.mWorkspace.getChildAt(a.o);
            int childCount = cellLayout.getChildCount();
            int i = 0;
            while (true) {
                if (i >= childCount) {
                    break;
                }
                View childAt = cellLayout.getChildAt(i);
                if ((childAt instanceof e) && childAt.getTag() != null && (childAt.getTag() instanceof a) && ((a) childAt.getTag()) == a) {
                    cellLayout.removeView(childAt);
                    this.mAddItemCellInfo = cellLayout.getTag();
                    break;
                }
                i++;
            }
            sModel.b(a);
            h.a().b(a);
            ff.g(this.mContext, a);
        }
    }

    public final void clearNewAppsNotify() {
        if (hasNewAppsNotify()) {
            ff.g(this);
            this.mNewAppNofifyView.setVisibility(8);
            this.mDrawer.l();
        }
        this.mHasNewAppsNotify = false;
    }

    public final void clearStatusBarTapScrollTopViews() {
        if (b.f != null) {
            try {
                ((ArrayList) b.f.get(launcher)).clear();
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void closeAllApplications() {
        this.mNavigation.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_in));
        this.mNavigation.setVisibility(0);
        this.mDrawer.f();
    }

    /* access modifiers changed from: package-private */
    public final void closeFolder(Folder folder) {
        if (folder != null && folder.e().g) {
            setUserFolderOpenAndCloseFocus(true);
            if (folder instanceof UserFolder) {
                ((UserFolder) folder).a(this);
                return;
            }
            ex exVar = folder.f;
            if (exVar.i != null) {
                exVar.i.requestFocus();
                exVar.i.h();
            } else if (exVar.j != null) {
                exVar.j.requestFocus();
                exVar.j.e();
            } else if (exVar.k != null) {
                exVar.k.requestFocus();
                exVar.k.e();
            }
            folder.e().g = false;
            ViewGroup viewGroup = (ViewGroup) folder.getParent();
            if (viewGroup != null) {
                viewGroup.removeView(folder);
            }
            folder.a_();
            this.mWorkspace.z();
            this.flagDockEnable = true;
            if (this.mDrawer.a() && this.mGrid.c()) {
                this.mGrid.a().i();
            }
            View childAt = this.mWorkspace.getChildAt(this.mWorkspace.d());
            childAt.clearAnimation();
            childAt.setDrawingCacheQuality(0);
            this.mWorkspace.B();
        }
    }

    public final void closeFolderByAnim(UserFolder userFolder) {
        ex exVar = userFolder.f;
        if (exVar.i != null) {
            exVar.i.requestFocus();
            exVar.i.h();
        } else if (exVar.j != null) {
            exVar.j.requestFocus();
            exVar.j.e();
        } else if (exVar.k != null) {
            exVar.k.requestFocus();
            exVar.k.e();
        }
        userFolder.e().g = false;
        ViewGroup viewGroup = (ViewGroup) userFolder.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(userFolder);
        }
        userFolder.a_();
        this.mWorkspace.z();
        this.flagDockEnable = true;
        if (this.mDrawer.a() && this.mGrid.c()) {
            this.mGrid.a().i();
        }
        userFolder.c();
        this.mHandleButton.clearAnimation();
        this.mDockBar.clearAnimation();
        this.mDrawer.j();
        this.mNavigation.clearAnimation();
        this.mDragLayer.requestFocus();
        View childAt = this.mWorkspace.getChildAt(this.mWorkspace.d());
        childAt.clearAnimation();
        childAt.setDrawingCacheQuality(0);
        this.mWorkspace.B();
        setUserFolderOpenAndCloseFocus(true);
    }

    /* access modifiers changed from: package-private */
    public final void closeSystemDialogs() {
        getWindow().closeAllPanels();
        closeMenuDialog();
        try {
            dismissDialog(1);
            this.mWorkspace.p();
        } catch (Exception e) {
        }
        try {
            dismissDialog(3);
            this.mWorkspace.p();
        } catch (Exception e2) {
        }
        try {
            dismissDialog(2);
            this.mWorkspace.p();
        } catch (Exception e3) {
        }
        try {
            dismissDialog(4);
            dismissDialog(5);
        } catch (Exception e4) {
        }
    }

    /* access modifiers changed from: package-private */
    public final void closeUserFolderFast(Folder folder) {
        if (folder.e().g) {
            ex exVar = folder.f;
            if (exVar.i != null) {
                exVar.i.requestFocus();
                exVar.i.h();
            } else if (exVar.j != null) {
                exVar.j.requestFocus();
                exVar.j.e();
            } else if (exVar.k != null) {
                exVar.k.requestFocus();
                exVar.k.e();
            }
            folder.e().g = false;
            ViewGroup viewGroup = (ViewGroup) folder.getParent();
            if (viewGroup != null) {
                viewGroup.removeView(folder);
            }
            folder.a_();
            ((UserFolder) folder).c();
            this.mWorkspace.z();
            this.flagDockEnable = true;
            if (this.mDrawer.a() && this.mGrid.c()) {
                this.mGrid.a().i();
            }
            this.mHandleButton.clearAnimation();
            this.mDockBar.clearAnimation();
            this.mDrawer.j();
            this.mNavigation.clearAnimation();
            View childAt = this.mWorkspace.getChildAt(this.mWorkspace.d());
            childAt.clearAnimation();
            childAt.setDrawingCacheQuality(0);
            this.mWorkspace.B();
            this.mDragLayer.clearDisappearingChildren();
            setUserFolderOpenAndCloseFocus(true);
        }
    }

    /* access modifiers changed from: package-private */
    public final void completeAddApplication(Context context, Intent intent, bs bsVar, boolean z) {
        am infoFromApplicationIntent;
        bsVar.f = this.mWorkspace.d();
        if (findSingleSlot(bsVar) && (infoFromApplicationIntent = infoFromApplicationIntent(context, intent)) != null) {
            this.mWorkspace.a(infoFromApplicationIntent, bsVar, z);
        }
    }

    /* access modifiers changed from: package-private */
    public final void completeAddApplicationBatch(ArrayList arrayList, boolean z) {
        if (arrayList.size() > 0) {
            bs e = arrayList.size() == 1 ? this.mAddItemCellInfo : this.mWorkspace.e();
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                am amVar = (am) it.next();
                if (!findSingleSlot(e)) {
                    return;
                }
                if (amVar != null) {
                    this.mWorkspace.a(amVar, e, z);
                }
            }
        }
    }

    public final boolean containApp(ArrayList arrayList, am amVar) {
        String uri = amVar.c.toURI();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            if (uri.equals(((am) it.next()).c.toURI())) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final boolean containDefualtDestop() {
        ContentResolver contentResolver = getContentResolver();
        String type = contentResolver.getType(Uri.parse(String.format(FMT_URI, AUTHORITY_LAUNCHER2)));
        return (type == null ? contentResolver.getType(Uri.parse(String.format(FMT_URI, new Object[]{AUTHORITY_LAUNCHER1}))) : type) != null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: package-private */
    public final DockView createDockItem(ViewGroup viewGroup) {
        DockView dockView = (DockView) this.mInflater.inflate((int) R.layout.dock_item, viewGroup, false);
        dockView.setOnClickListener(this);
        return dockView;
    }

    public final Dialog createMarketListDialog() {
        dg dgVar = new dg(this);
        com.tencent.module.setting.e eVar = new com.tencent.module.setting.e(this);
        eVar.a((int) R.string.market_list_title);
        eVar.a(dgVar, new dy(this));
        eVar.b();
        return eVar.c();
    }

    /* access modifiers changed from: package-private */
    public final CustomAlertDialog createOrderAppDialog() {
        com.tencent.module.setting.e eVar = new com.tencent.module.setting.e(this);
        eVar.a((int) R.string.appicon_order);
        eVar.b();
        eVar.a(new dc(this)).a((int) R.string.confirm, new da(this));
        return eVar.c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: package-private */
    public final View createShortcut(int i, ViewGroup viewGroup, am amVar) {
        TextView textView = (TextView) this.mInflater.inflate(i, viewGroup, false);
        if (!amVar.f) {
            amVar.d = a.a(amVar.d, this);
            amVar.f = true;
        }
        textView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, amVar.d, (Drawable) null, (Drawable) null);
        textView.setText(amVar.a);
        textView.setTag(amVar);
        textView.setOnClickListener(this);
        return textView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: package-private */
    public final View createShortcut(int i, ViewGroup viewGroup, am amVar, Drawable drawable) {
        TextView textView = (TextView) this.mInflater.inflate(i, viewGroup, false);
        if (!amVar.e) {
            if (amVar.d != null) {
                amVar.d = a.a(this, amVar.d, drawable, amVar);
            }
            amVar.e = true;
            amVar.f = true;
        }
        textView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, amVar.d, (Drawable) null, (Drawable) null);
        textView.setText(amVar.a);
        textView.setTag(amVar);
        textView.setOnClickListener(this);
        return textView;
    }

    /* access modifiers changed from: package-private */
    public final View createShortcut(am amVar) {
        return createShortcut(R.layout.application, (ViewGroup) this.mWorkspace.getChildAt(this.mWorkspace.d()), amVar);
    }

    /* access modifiers changed from: package-private */
    public final View createShortcut(am amVar, Drawable drawable) {
        return createShortcut(R.layout.application, (ViewGroup) this.mWorkspace.getChildAt(this.mWorkspace.d()), amVar, drawable);
    }

    public final void disappearPreNextZone() {
        this.mPreZone.setVisibility(4);
        this.mNextZone.setVisibility(4);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (!this.isLoadHome) {
            return super.dispatchKeyEvent(keyEvent);
        }
        if (keyEvent.getAction() != 0) {
            if (keyEvent.getAction() == 1 && keyEvent.getDownTime() == this.downTime) {
                switch (keyEvent.getKeyCode()) {
                    case 3:
                        return true;
                    case 4:
                        if (this.mThumbnailManagerShowed) {
                            if (this.mThumbnailWorkspace.d()) {
                                dissmissThumbnailManager(this.mWorkspace.d());
                            }
                            return true;
                        }
                        if (!((keyEvent.getFlags() & 32) != 0)) {
                            if (this.mDrawer.a()) {
                                if (!closeFolder()) {
                                    closeDrawer(true);
                                }
                            } else if (!closeFolder()) {
                                return this.mWorkspace.dispatchKeyEvent(keyEvent);
                            }
                        }
                        return true;
                    case 82:
                        if (this.mThumbnailManagerShowed) {
                            return true;
                        }
                        break;
                }
            }
        } else {
            switch (keyEvent.getKeyCode()) {
                case 3:
                    return true;
                case 4:
                    this.downTime = keyEvent.getDownTime();
                    return true;
                case 82:
                    if (!this.mThumbnailManagerShowed) {
                        closeFolder();
                        break;
                    } else {
                        return true;
                    }
            }
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    /* access modifiers changed from: package-private */
    public final void editShirtcut(am amVar) {
        Intent intent = new Intent("android.intent.action.EDIT");
        intent.setClass(this, ShirtcutEditActivity.class);
        intent.putExtra(ShirtcutEditActivity.EXTRA_APPLICATIONINFO, amVar.l);
        startActivityForResult(intent, 10);
    }

    /* access modifiers changed from: protected */
    public final void finalize() {
        super.finalize();
    }

    /* access modifiers changed from: package-private */
    public final void findDrayLayerRect(Rect rect, View view) {
        Rect rect2 = rect;
        for (View view2 = view; !(view2 instanceof DragLayer); view2 = (View) view2.getParent()) {
            rect2.set(rect2.left + view2.getLeft(), rect2.top + view2.getTop(), 0, 0);
        }
    }

    public final ArrayList getAllFolders() {
        ArrayList arrayList = new ArrayList();
        QGridLayout o = this.mDrawer.o();
        int childCount = o.getChildCount();
        for (int i = 0; i < childCount; i++) {
            DrawerTextView drawerTextView = (DrawerTextView) o.getChildAt(i);
            if (drawerTextView.g()) {
                arrayList.add(drawerTextView);
            }
        }
        int childCount2 = this.mWorkspace.getChildCount();
        for (int i2 = 0; i2 < childCount2; i2++) {
            CellLayout cellLayout = (CellLayout) this.mWorkspace.getChildAt(i2);
            int childCount3 = cellLayout.getChildCount();
            for (int i3 = 0; i3 < childCount3; i3++) {
                View childAt = cellLayout.getChildAt(i3);
                if (childAt instanceof FolderIcon) {
                    arrayList.add(childAt);
                }
            }
        }
        int childCount4 = this.mDockBar.getChildCount();
        for (int i4 = 0; i4 < childCount4; i4++) {
            DockView dockView = (DockView) this.mDockBar.getChildAt(i4);
            if (dockView.h()) {
                arrayList.add(dockView);
            }
        }
        return arrayList;
    }

    public final cv getAppWidgetHost() {
        return this.mAppWidgetHost;
    }

    public final DragLayer getDragLayer() {
        return this.mDragLayer;
    }

    public final Drawable getFolderBackground(View view) {
        int width = this.mDragLayer.getWidth();
        int height = this.mDragLayer.getHeight();
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas();
        canvas.setDrawFilter(new PaintFlagsDrawFilter(4, 2));
        canvas.setBitmap(createBitmap);
        this.mDragLayer.draw(canvas);
        Bitmap createBitmap2 = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        canvas.setBitmap(createBitmap2);
        Paint paint = new Paint();
        paint.setAlpha(51);
        canvas.drawBitmap(createBitmap, 0.0f, 0.0f, paint);
        return new BitmapDrawable(createBitmap2);
    }

    public final Folder getOpenFolder() {
        DragLayer dragLayer = this.mDragLayer;
        int childCount = dragLayer.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = dragLayer.getChildAt(i);
            if (childAt instanceof Folder) {
                return (Folder) childAt;
            }
        }
        return null;
    }

    public final Folder getOpenFolderInDrawer() {
        int childCount = this.mDrawer.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = this.mDrawer.getChildAt(i);
            if (childAt instanceof Folder) {
                return (Folder) childAt;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final Workspace getWorkspace() {
        return this.mWorkspace;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void
     arg types: [com.tencent.launcher.Launcher, com.tencent.module.qqwidget.a, int, int, int, int, int]
     candidates:
      com.tencent.launcher.ff.a(android.database.Cursor, android.content.Context, int, int, int, int, android.content.Intent):com.tencent.launcher.am
      com.tencent.launcher.ff.a(android.content.Context, com.tencent.launcher.ha, long, int, int, int, boolean):void */
    public final void handlePersonCenterWidget(String str) {
        a a;
        a aVar;
        int i;
        if (str.equals(PERSONCENTER_PACKAGE_NAME) && (a = sModel.a((String) PERSONCENTER_DEMO_PACKAGE_NAME)) != null) {
            CellLayout cellLayout = (CellLayout) this.mWorkspace.getChildAt(a.o);
            int childCount = cellLayout.getChildCount();
            int i2 = 0;
            while (true) {
                if (i2 >= childCount) {
                    break;
                }
                View childAt = cellLayout.getChildAt(i2);
                if ((childAt instanceof e) && childAt.getTag() != null && (childAt.getTag() instanceof a) && ((a) childAt.getTag()) == a) {
                    cellLayout.removeView(childAt);
                    this.mAddItemCellInfo = cellLayout.getTag();
                    break;
                }
                i2++;
            }
            sModel.b(a);
            h.a().b(a);
            ff.g(this.mContext, a);
            List<ResolveInfo> queryIntentServices = this.mContext.getPackageManager().queryIntentServices(new Intent("com.tencent.qqwidget.service"), BrightnessActivity.DEFAULT_BACKLIGHT);
            a aVar2 = null;
            int i3 = 0;
            while (i3 < queryIntentServices.size()) {
                ServiceInfo serviceInfo = queryIntentServices.get(i3).serviceInfo;
                if (!serviceInfo.packageName.equalsIgnoreCase(PERSONCENTER_PACKAGE_NAME) || (i = serviceInfo.metaData.getInt("tencent.qqwidget.service", 0)) == 0) {
                    aVar = aVar2;
                } else {
                    a a2 = h.a(this.mContext, serviceInfo.packageName, i);
                    if (a2 != null) {
                        a2.e = serviceInfo.packageName;
                        a2.f = serviceInfo.name;
                        h.a().a(a2);
                    }
                    aVar = a2;
                }
                i3++;
                aVar2 = aVar;
            }
            if (aVar2 != null) {
                aVar2.p = a.p;
                aVar2.q = a.q;
                aVar2.o = a.o;
                int[] iArr = this.mCellCoordinates;
                try {
                    e a3 = h.a().a(this, aVar2);
                    if (a3 != null) {
                        a3.setTag(a);
                        this.mWorkspace.a(a3, aVar2.o, aVar2.p, aVar2.q, aVar2.r, aVar2.s);
                        this.mWorkspace.requestLayout();
                        a3.a();
                        sModel.a(a);
                        ff.a((Context) this, (ha) a, -100L, this.mWorkspace.d(), iArr[0], iArr[1], false);
                    }
                } catch (QQWidgetFormatExcepiton e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public final boolean hasNewAppsNotify() {
        return this.mHasNewAppsNotify;
    }

    /* access modifiers changed from: package-private */
    public final void hideDockBar() {
        this.mDockBar.setVisibility(8);
    }

    /* access modifiers changed from: package-private */
    public final void hideDockButtonGroup() {
        this.mDockButtonGroup.setVisibility(8);
    }

    /* access modifiers changed from: package-private */
    public final void hideHomeFlipper() {
        this.mHomeFlipper.setVisibility(8);
    }

    public final void initHasNewAppsNofify() {
        ArrayList h = ff.h(this);
        if (h == null || h.size() <= 0) {
            this.mHasNewAppsNotify = false;
        } else {
            this.mHasNewAppsNotify = true;
        }
    }

    public final void initThumbWorkspace() {
        if (!this.mFirstManager) {
            Workspace workspace = this.mWorkspace;
            ThumbnailWorkspace thumbnailWorkspace = this.mThumbnailWorkspace;
            int childCount = workspace.getChildCount();
            for (int i = 0; i < childCount; i++) {
                ViewGroup viewGroup = (ViewGroup) workspace.getChildAt(i);
                boolean z = true;
                if (viewGroup.getChildCount() > 0) {
                    z = false;
                }
                thumbnailWorkspace.a(viewGroup, i);
                thumbnailWorkspace.a(i, z);
            }
        }
    }

    public final void invalidateForTheme() {
        this.mWorkspace.destroyDrawingCache();
        int childCount = this.mWorkspace.getChildCount();
        for (int i = 0; i < childCount; i++) {
            ViewGroup viewGroup = (ViewGroup) this.mWorkspace.getChildAt(i);
            int childCount2 = viewGroup.getChildCount();
            for (int i2 = 0; i2 < childCount2; i2++) {
                View childAt = viewGroup.getChildAt(i2);
                childAt.destroyDrawingCache();
                if (childAt instanceof TextView) {
                    ((TextView) childAt).setCompoundDrawables(null, ((TextView) childAt).getCompoundDrawables()[1], null, null);
                }
            }
            viewGroup.destroyDrawingCache();
        }
        this.mWorkspace.invalidate();
        QGridLayout o = this.mDrawer.o();
        int childCount3 = o.getChildCount();
        for (int i3 = 0; i3 < childCount3; i3++) {
            o.getChildAt(i3).destroyDrawingCache();
        }
        o.destroyDrawingCache();
    }

    /* access modifiers changed from: package-private */
    public final boolean isDrawerClose() {
        return !this.mDrawer.a();
    }

    /* access modifiers changed from: package-private */
    public final boolean isDrawerOpen() {
        return this.mDrawer.a();
    }

    /* access modifiers changed from: package-private */
    public final boolean isWorkspaceLocked() {
        return this.mDesktopLocked;
    }

    /* access modifiers changed from: protected */
    public final void onActivityResult(int i, int i2, Intent intent) {
        int intExtra;
        if (this.isLoadHome) {
            this.mWaitingForResult = false;
            if (i2 == -1 && this.mAddItemCellInfo != null) {
                switch (i) {
                    case 1:
                        completeAddShortcut(intent, this.mAddItemCellInfo, !this.mDesktopLocked);
                        break;
                    case 4:
                        completeAddLiveFolder(intent, this.mAddItemCellInfo, !this.mDesktopLocked);
                        break;
                    case 5:
                        completeAddAppWidget(intent, this.mAddItemCellInfo, !this.mDesktopLocked);
                        break;
                    case 6:
                        completeAddApplication(this, intent, this.mAddItemCellInfo, !this.mDesktopLocked);
                        break;
                    case DesktopSwitchSpecialEffectSettingActivity.nRotationSetting /*7*/:
                        processShortcut(intent, 1);
                        return;
                    case DesktopSwitchSpecialEffectSettingActivity.nCubeSetting /*8*/:
                        addLiveFolder(intent);
                        return;
                    case 9:
                        addAppWidget(intent);
                        return;
                    case 10:
                        completeEditShortcut(intent);
                        break;
                }
                this.mAddItemCellInfo = null;
            } else if ((i == 9 || i == 5) && i2 == 0 && intent != null && (intExtra = intent.getIntExtra("appWidgetId", -1)) != -1) {
                this.mAppWidgetHost.deleteAppWidgetId(intExtra);
            }
            if (i2 == -1 && this.mAddDockItem != null) {
                switch (i) {
                    case 10:
                        completeEditShortcut(intent);
                        return;
                    case 11:
                        processShortcut(intent, 12);
                        return;
                    case 12:
                        completeAddShirtDockBar(infoFromShortcutIntent(this, intent));
                        return;
                    case 13:
                        completeAddShirtDockBar(infoFromApplicationIntent(this, intent));
                        return;
                    default:
                        return;
                }
            }
        }
    }

    public final void onClick(View view) {
        Object tag = view.getTag();
        view.clearFocus();
        if ((view instanceof DockView) && !this.flagDockEnable) {
            return;
        }
        if (tag != null || !(view instanceof DockView)) {
            if ((tag instanceof ha) && ((ha) tag).n == -300) {
                this.mDrawer.l();
            }
            if (tag instanceof am) {
                Intent intent = ((am) tag).c;
                if (!(view instanceof AnimatorTextView) || !fn.a) {
                    int[] iArr = new int[2];
                    view.getLocationOnScreen(iArr);
                    try {
                        Method declaredMethod = Intent.class.getDeclaredMethod("setSourceBounds", Rect.class);
                        Rect rect = this.mTempRect;
                        rect.set(iArr[0], iArr[1], iArr[0] + view.getWidth(), iArr[1] + view.getHeight());
                        declaredMethod.invoke(intent, rect);
                    } catch (Exception e) {
                    }
                    startActivitySafely(intent);
                    return;
                }
                Intent intent2 = new Intent("android.intent.action.DELETE", Uri.parse("package:" + intent.getComponent().getPackageName()));
                intent2.addFlags(268435456);
                startActivity(intent2);
            } else if (tag instanceof ex) {
                if (view instanceof FolderIcon) {
                    ((ex) tag).i = (FolderIcon) view;
                } else if (view instanceof DockView) {
                    ((ex) tag).k = (DockView) view;
                } else if (((ex) tag).n == -300) {
                    ((ex) tag).j = (DrawerTextView) view;
                }
                handleFolderClick((ex) tag);
            }
        } else {
            this.mWorkspace.q();
            this.mAddDockItem = (DockView) view;
            showDialog(3);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.home.i.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.launcher.home.i.b(java.lang.String, int):int
      com.tencent.launcher.home.i.b(java.lang.String, java.lang.String):java.lang.String
      com.tencent.launcher.home.i.b(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.home.i.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.launcher.home.i.a(java.lang.String, int):void
      com.tencent.launcher.home.i.a(java.lang.String, long):void
      com.tencent.launcher.home.i.a(java.lang.String, java.lang.String):void
      com.tencent.launcher.home.i.a(java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        System.currentTimeMillis();
        super.onCreate(bundle);
        this.handler = new Handler();
        if (d.a()) {
            handleCrash();
            return;
        }
        launcher = this;
        this.mContext = this;
        this.isFirstRun = i.a().b("application_first_run", true);
        this.isFirstRunForImportData = i.a().b("is_first_run_for_import_data", true);
        if (this.isFirstRunForImportData) {
            i.a().a("is_first_run_for_import_data", false);
        }
        this.mThemeManager = l.a();
        this.mInflater = getLayoutInflater();
        this.mAppWidgetManager = AppWidgetManager.getInstance(this);
        this.mAppWidgetHost = new cv(this);
        this.mAppWidgetHost.startListening();
        checkForLocaleChange();
        setWallpaperDimension();
        setContentView((int) R.layout.launcher);
        setupViews();
        loadDefaultWallpaper();
        registerIntentReceivers();
        registerContentObservers();
        getSharedPreferences("home_config", 0).registerOnSharedPreferenceChangeListener(this.mMSFConfigChangeListener);
        this.needCheckUpdate = checkUpdate();
        this.mSavedState = bundle;
        restoreState(this.mSavedState);
        if (!this.mRestoring) {
            startLoaders();
        }
        this.mDefaultKeySsb = new SpannableStringBuilder();
        Selection.setSelection(this.mDefaultKeySsb, 0);
        this.isLoadHome = true;
        if (!this.mThemeManager.d()) {
            this.mThemeManager.a(BaseApp.b());
        }
        this.appThread.start();
        registerReceiver(this.statsReceiver, new IntentFilter(ACTION_CHECK_STAT));
    }

    /* access modifiers changed from: protected */
    public final Dialog onCreateDialog(int i) {
        switch (i) {
            case 1:
                return new bf(this).a(false);
            case 2:
                return new x(this, this);
            case 3:
                return new bf(this).a(true);
            case 4:
                return createMarketListDialog();
            case 5:
                return createOrderAppDialog();
            default:
                return super.onCreateDialog(i);
        }
    }

    public final boolean onCreateOptionsMenu(Menu menu) {
        if (!this.isLoadHome) {
            return super.onCreateOptionsMenu(menu);
        }
        if (this.mDesktopLocked && this.mSavedInstanceState == null) {
            return false;
        }
        super.onCreateOptionsMenu(menu);
        menu.add(1, 2, 0, (int) R.string.menu_add).setIcon((int) R.drawable.ic_menu_add).setAlphabeticShortcut('A');
        menu.add(0, (int) MENU_SCREEN_MANAGER, 0, (int) R.string.menu_screen_manager).setIcon((int) R.drawable.ic_menu_screen_manager).setAlphabeticShortcut('P');
        menu.add(0, 8, 0, (int) R.string.menu_theme).setIcon((int) R.drawable.ic_menu_theme).setAlphabeticShortcut('T');
        menu.add(0, 4, 0, (int) R.string.menu_search).setIcon((int) R.drawable.ic_menu_search).setAlphabeticShortcut('s');
        menu.add(0, 9, 0, (int) R.string.menu_qqlauncher_settings).setIcon((int) R.drawable.ic_menu_desktop);
        menu.add(0, 6, 0, (int) R.string.menu_settings).setIcon((int) R.drawable.ic_menu_preferences).setAlphabeticShortcut('P');
        menu.add(2, 11, 0, (int) R.string.menu_software_manager).setIcon((int) R.drawable.ic_menu_softmanager);
        menu.add(2, 10, 0, (int) R.string.menu_change_drawer_listmode).setIcon((int) R.drawable.ic_menu_list_mode);
        menu.add(3, 12, 0, (int) R.string.menu_order_appicon).setIcon((int) R.drawable.ic_menu_order);
        menu.add(3, 13, 0, (int) R.string.menu_add_folder).setIcon((int) R.drawable.ic_drawer_add_folder);
        com.tencent.launcher.home.b.a(menu);
        com.tencent.launcher.home.b.a(menu, this.mContext);
        return true;
    }

    /* access modifiers changed from: package-private */
    public final void onDesktopItemsLoaded(ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3) {
        if (!this.mDestroyed) {
            bindDesktopItems(arrayList, arrayList2, arrayList3);
        }
    }

    public final void onDestroy() {
        launcher = null;
        super.onDestroy();
        if (this.isLoadHome) {
            sIconBackgroundDrawable = null;
            this.mDestroyed = true;
            h.a().c();
            v.a().c();
            ag.a().b();
            getSharedPreferences("home_config", 0).unregisterOnSharedPreferenceChangeListener(this.mMSFConfigChangeListener);
            try {
                this.mAppWidgetHost.stopListening();
            } catch (NullPointerException e) {
                Log.w(LOG_TAG, "problem while stopping AppWidgetHost during Launcher destruction", e);
            }
            TextKeyListener.getInstance().release();
            this.mGrid.a((ArrayAdapter) null);
            sModel.d();
            sModel.a();
            getContentResolver().unregisterContentObserver(this.mFavoritesChangeObserver);
            getContentResolver().unregisterContentObserver(this.mWidgetObserver);
            unregisterReceiver(this.mApplicationsReceiver);
            unregisterReceiver(this.mCloseSystemDialogsReceiver);
            unregisterReceiver(this.statsReceiver);
            unregisterReceiver(this.mThemeReceiver);
            unregisterReceiver(this.mThemeDownReceiver);
            this.mWorkspace.E();
            if (this.mAlarmManager != null && this.mStatPendingIntent != null) {
                this.mAlarmManager.cancel(this.mStatPendingIntent);
            }
        }
    }

    public final void onDragFromDrawer() {
        if (this.mGrid.c()) {
            this.mGrid.a().h();
        }
        this.mDockBar.e();
        this.mDockBar.b();
        closeDrawer(true);
        this.mDockBar.setVisibility(0);
        this.mHomeFlipper.setVisibility(0);
        this.mWorkspace.h();
        this.mPreZone.setVisibility(0);
        this.mNextZone.setVisibility(0);
    }

    public final void onDrawerClose() {
        this.mHandleButton.setVisibility(0);
        this.mDockBar.setVisibility(0);
        this.mNavigation.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_in));
        this.mNavigation.setVisibility(0);
        this.mWorkspace.setFocusable(true);
    }

    public final void onDrawerOpen() {
        if (getCurrentFocus() != null) {
            getCurrentFocus().clearFocus();
        }
        if (this.mGrid.a() != null) {
            this.mGrid.a().i();
        }
        this.mHandleButton.setVisibility(8);
        this.mWorkspace.setFocusable(false);
    }

    public final boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (!this.isLoadHome) {
            return super.onKeyDown(i, keyEvent);
        }
        boolean onKeyDown = super.onKeyDown(i, keyEvent);
        return (onKeyDown || !acceptFilter() || i == 66 || !TextKeyListener.getInstance().onKeyDown(this.mWorkspace, this.mDefaultKeySsb, i, keyEvent) || this.mDefaultKeySsb == null || this.mDefaultKeySsb.length() <= 0) ? onKeyDown : onSearchRequested();
    }

    public final boolean onLongClick(View view) {
        if (!view.isInTouchMode()) {
            return true;
        }
        if (view == this.mHandleButton) {
            showThumbnailManager();
            return true;
        } else if (this.mDesktopLocked) {
            return false;
        } else {
            bs bsVar = (bs) (!(view instanceof CellLayout) ? (View) view.getParent() : view).getTag();
            if (bsVar == null) {
                return true;
            }
            if (this.mWorkspace.r()) {
                if (bsVar.a == null) {
                    if (bsVar.g) {
                        this.mWorkspace.s();
                        this.mWorkspace.q();
                        this.mAddItemCellInfo = bsVar;
                        this.mWaitingForResult = true;
                        showDialog(1);
                    }
                } else if (!(bsVar.a instanceof Folder)) {
                    this.mWorkspace.a(bsVar);
                }
            }
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public final void onNewIntent(Intent intent) {
        if (intent.getIntExtra("flag", 0) == 1) {
            ((BaseApp) getApplication()).a();
            finish();
            Process.killProcess(Process.myPid());
            return;
        }
        super.onNewIntent(intent);
        if (this.isLoadHome && "android.intent.action.MAIN".equals(intent.getAction())) {
            closeSystemDialogs();
            this.mIsNewIntent = true;
            if (this.mThumbnailManagerShowed) {
                if (this.mThumbnailWorkspace.d()) {
                    dissmissThumbnailManager(this.mThumbnailWorkspace.a());
                } else {
                    return;
                }
            }
            if ((intent.getFlags() & 4194304) != 4194304) {
                if (!this.mWorkspace.c()) {
                    this.mWorkspace.t();
                }
                closeDrawer(true);
                View peekDecorView = getWindow().peekDecorView();
                if (peekDecorView != null && peekDecorView.getWindowToken() != null) {
                    ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(peekDecorView.getWindowToken(), 0);
                    return;
                }
                return;
            }
            closeDrawer(false);
            if (this.mGrid.c()) {
                this.mGrid.a().i();
            }
        }
    }

    public final boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 2:
                bs bsVar = this.mMenuAddInfo;
                this.mWorkspace.q();
                this.mAddItemCellInfo = bsVar;
                this.mWaitingForResult = true;
                showDialog(1);
                return true;
            case 3:
                startWallpaper();
                return true;
            case 4:
                onSearchRequested();
                return true;
            case 5:
                showNotifications();
                return true;
            case 6:
                Intent intent = new Intent("android.settings.SETTINGS");
                intent.setFlags(270532608);
                startActivity(intent);
                return true;
            case DesktopSwitchSpecialEffectSettingActivity.nRotationSetting /*7*/:
                f.a(this);
                break;
            case DesktopSwitchSpecialEffectSettingActivity.nCubeSetting /*8*/:
                startActivity(new Intent(this, ThemeSettingActivity.class));
                break;
            case 9:
                startActivity(new Intent(this, SettingActivity.class));
                break;
            case 10:
                changeDrawerMode();
                break;
            case 11:
                startActivity(new Intent(this, LocalSoftManageActivity.class));
                break;
            case 12:
                showDialog(5);
                break;
            case 13:
                showRenameDialog(null, 3);
                break;
            case MENU_SCREEN_MANAGER /*14*/:
                showThumbnailManager();
                break;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final void onPause() {
        super.onPause();
        if (!this.isLoadHome) {
        }
    }

    /* access modifiers changed from: protected */
    public final void onPrepareDialog(int i, Dialog dialog) {
        switch (i) {
            case 1:
            case 3:
            case 4:
            default:
                return;
            case 2:
                EditText editText = (EditText) dialog.findViewById(R.id.folder_name);
                CharSequence string = this.mFolderInfo != null ? this.mFolderInfo.h : getString(R.string.folder_name);
                editText.setText(string);
                editText.setSelection(string.length());
                return;
            case 5:
                ((CustomAlertDialog) dialog).c();
                ListView b = ((CustomAlertDialog) dialog).b();
                if (b != null) {
                    int count = b.getCount();
                    for (int i2 = 0; i2 < count; i2++) {
                        b.setItemChecked(i2, false);
                    }
                    return;
                }
                return;
        }
    }

    public final boolean onPrepareOptionsMenu(Menu menu) {
        if (!this.isLoadHome) {
            return super.onPrepareOptionsMenu(menu);
        }
        boolean a = this.mDrawer.a();
        this.mMenuAddInfo = this.mWorkspace.e();
        MenuItem findItem = menu.findItem(2);
        findItem.setVisible(!a);
        findItem.setEnabled(this.mMenuAddInfo != null && this.mMenuAddInfo.g);
        menu.setGroupVisible(0, !a);
        menu.setGroupVisible(2, a);
        if (a) {
            MenuItem findItem2 = menu.findItem(11);
            if (b.a()) {
                findItem2.setVisible(true);
            } else {
                findItem2.setVisible(false);
            }
        }
        menu.setGroupVisible(3, a && this.mGrid.c());
        String b = i.a().b("setting_normal_functionlist", "");
        if (b == "") {
            i.a().a("setting_normal_functionlist", "0");
            b = i.a().b("setting_normal_functionlist", "");
        } else if (b.equals("0")) {
            i.a().a("setting_normal_functionlist", b);
        } else if (b.equals("1")) {
            i.a().a("setting_normal_functionlist", b);
        }
        if (b.equals("1") || b == "") {
            menu.findItem(10).setTitle((int) R.string.menu_change_drawer_matrixmode).setIcon((int) R.drawable.ic_menu_drawer_mode);
        } else {
            menu.findItem(10).setTitle((int) R.string.menu_change_drawer_listmode).setIcon((int) R.drawable.ic_menu_list_mode);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final void onRestoreInstanceState(Bundle bundle) {
        SparseArray sparseArray;
        int i;
        if (this.isLoadHome) {
            Bundle bundle2 = bundle.getBundle("android:viewHierarchyState");
            if (bundle2 != null) {
                SparseArray sparseParcelableArray = bundle2.getSparseParcelableArray("android:views");
                bundle2.remove("android:views");
                int i2 = bundle2.getInt("android:focusedViewId", -1);
                bundle2.remove("android:focusedViewId");
                int i3 = i2;
                sparseArray = sparseParcelableArray;
                i = i3;
            } else {
                sparseArray = null;
                i = -1;
            }
            super.onRestoreInstanceState(bundle);
            if (bundle2 != null) {
                bundle2.putSparseParcelableArray("android:views", sparseArray);
                bundle2.putInt("android:focusedViewId", i);
                bundle2.remove("android:Panels");
            }
            this.mSavedInstanceState = bundle;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.home.i.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.launcher.home.i.a(java.lang.String, int):void
      com.tencent.launcher.home.i.a(java.lang.String, long):void
      com.tencent.launcher.home.i.a(java.lang.String, java.lang.String):void
      com.tencent.launcher.home.i.a(java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    public final void onResume() {
        if (af.c) {
            af.a(this.mContext);
        }
        System.currentTimeMillis();
        super.onResume();
        if (this.isLoadHome) {
            if (this.mNeedRestart) {
                getWindow().closeAllPanels();
                try {
                    Process.killProcess(Process.myPid());
                    finish();
                    startActivity(getIntent());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                if (this.mRestoring) {
                    startLoaders();
                }
                if (!b.g) {
                    this.mWorkspace.u();
                }
                if (this.isFirstRunForImportData) {
                    this.isFirstRunForImportData = false;
                    i.a().a("first_run_for_import_data", false);
                    if (hasMultiLauncher(this.mContext) > 0) {
                        showImportDataDialog();
                    }
                }
                if (this.mIsNewIntent) {
                    this.mWorkspace.post(new ia(this));
                }
                this.mIsNewIntent = false;
                if (this.needCheckUpdate) {
                    this.needCheckUpdate = false;
                    new hv(this).start();
                }
            }
        }
    }

    public final Object onRetainNonConfigurationInstance() {
        if (!this.isLoadHome) {
            return null;
        }
        if (this.mBinder != null) {
            this.mBinder.a = true;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.isLoadHome) {
            bundle.putInt(RUNTIME_STATE_CURRENT_SCREEN, this.mWorkspace.d());
            ArrayList b = this.mWorkspace.b();
            if (b.size() > 0) {
                int size = b.size();
                long[] jArr = new long[size];
                for (int i = 0; i < size; i++) {
                    jArr[i] = ((Folder) b.get(i)).e().l;
                }
                bundle.putLongArray(RUNTIME_STATE_USER_FOLDERS, jArr);
            }
            boolean z = getChangingConfigurations() != 0;
            if (this.mDrawer.a() && z) {
                bundle.putBoolean(RUNTIME_STATE_ALL_APPS_FOLDER, true);
            }
            if (this.mAddItemCellInfo != null && this.mAddItemCellInfo.g && this.mWaitingForResult) {
                bs bsVar = this.mAddItemCellInfo;
                CellLayout cellLayout = (CellLayout) this.mWorkspace.getChildAt(bsVar.f);
                bundle.putInt(RUNTIME_STATE_PENDING_ADD_SCREEN, bsVar.f);
                bundle.putInt(RUNTIME_STATE_PENDING_ADD_CELL_X, bsVar.b);
                bundle.putInt(RUNTIME_STATE_PENDING_ADD_CELL_Y, bsVar.c);
                bundle.putInt(RUNTIME_STATE_PENDING_ADD_SPAN_X, bsVar.d);
                bundle.putInt(RUNTIME_STATE_PENDING_ADD_SPAN_Y, bsVar.e);
                if (cellLayout != null) {
                    bundle.putInt(RUNTIME_STATE_PENDING_ADD_COUNT_X, cellLayout.d());
                    bundle.putInt(RUNTIME_STATE_PENDING_ADD_COUNT_Y, cellLayout.e());
                    bundle.putBooleanArray(RUNTIME_STATE_PENDING_ADD_OCCUPIED_CELLS, cellLayout.o());
                }
            }
            if (this.mFolderInfo != null && this.mWaitingForResult) {
                bundle.putBoolean(RUNTIME_STATE_PENDING_FOLDER_RENAME, true);
                bundle.putLong(RUNTIME_STATE_PENDING_FOLDER_RENAME_ID, this.mFolderInfo.l);
            }
        }
    }

    public final boolean onSearchRequested() {
        if (!this.isLoadHome) {
            return super.onSearchRequested();
        }
        if (this.mDrawer.a()) {
            com.tencent.launcher.home.a.a(getApplicationContext(), "fn_search");
            startActivity(new Intent(this, SearchAppTestActivity.class));
        } else {
            startSearch(null, false, null, true);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final void onStart() {
        super.onStart();
        if (this.isLoadHome && getIntent().getIntExtra("flag", 0) == 1) {
            finish();
            Process.killProcess(Process.myPid());
        }
    }

    /* access modifiers changed from: protected */
    public final void onStop() {
        super.onStop();
    }

    public final boolean onTrackballEvent(MotionEvent motionEvent) {
        if (!this.isLoadHome) {
            return super.onTrackballEvent(motionEvent);
        }
        if (this.mThumbnailManagerShowed) {
            return true;
        }
        return super.onTrackballEvent(motionEvent);
    }

    /* access modifiers changed from: package-private */
    public final void processShortcut(Intent intent, int i) {
        String string = getString(R.string.system_folder);
        String stringExtra = intent.getStringExtra("android.intent.extra.shortcut.NAME");
        if (string == null || !string.equals(stringExtra)) {
            startActivityForResult(intent, i);
            return;
        }
        Intent intent2 = new Intent("android.intent.action.PICK_ACTIVITY");
        intent2.putExtra("android.intent.extra.INTENT", new Intent("android.intent.action.CREATE_LIVE_FOLDER"));
        intent2.putExtra("android.intent.extra.TITLE", string);
        startActivityForResult(intent2, 8);
    }

    /* access modifiers changed from: package-private */
    public final void requestFullScreen(boolean z) {
        if (this.notiHeight < 0) {
            this.notiHeight = ((ViewGroup) getWindow().getDecorView()).getChildAt(0).getPaddingTop();
        }
        this.mWorkspace.a(z, this.notiHeight);
        if (z) {
            this.mDrawer.setPadding(this.mDrawer.getPaddingLeft(), this.mDrawer.getPaddingTop() + this.notiHeight, this.mDrawer.getPaddingRight(), this.mDrawer.getPaddingBottom());
            if (getOpenFolder() != null) {
                getOpenFolder().setPadding(0, this.notiHeight, 0, 0);
            }
            WindowManager.LayoutParams attributes = getWindow().getAttributes();
            attributes.flags |= APPWIDGET_HOST_ID;
            getWindow().setAttributes(attributes);
            getWindow().addFlags(512);
            return;
        }
        this.mDrawer.setPadding(this.mDrawer.getPaddingLeft(), this.mDrawer.getPaddingTop() - this.notiHeight, this.mDrawer.getPaddingRight(), this.mDrawer.getPaddingBottom());
        if (getOpenFolder() != null) {
            getOpenFolder().setPadding(0, 0, 0, 0);
        }
        WindowManager.LayoutParams attributes2 = getWindow().getAttributes();
        attributes2.flags &= -1025;
        getWindow().setAttributes(attributes2);
        getWindow().clearFlags(512);
    }

    public final void requestRestartHome() {
        this.mNeedRestart = true;
    }

    /* access modifiers changed from: package-private */
    public final void setDrawFast() {
    }

    /* access modifiers changed from: package-private */
    public final void setDrawSlow() {
    }

    public final void setUserFolderOpenAndCloseFocus(boolean z) {
        if (isDrawerOpen()) {
            this.mHomeIconButton.setFocusable(z);
            this.mMarketEntry.setFocusable(z);
            this.mSearchApp.setFocusable(z);
            return;
        }
        this.mWorkspace.setFocusable(z);
        DockBar dockBar = this.mDockBar;
        int childCount = dockBar.getChildCount();
        for (int i = 0; i < childCount; i++) {
            dockBar.getChildAt(i).setFocusable(z);
        }
        this.mHandleButton.setFocusable(z);
    }

    public final void setWindowBackground(boolean z) {
        Window window = getWindow();
        if (!z) {
            window.setBackgroundDrawable(null);
            window.setFormat(2);
            window.clearFlags(1048576);
            return;
        }
        window.setBackgroundDrawable(new ColorDrawable(0));
        window.setFormat(-2);
        window.addFlags(1048576);
    }

    public final void shadeViewsReserve(UserFolder userFolder) {
        this.mHandleButton.clearAnimation();
        this.mDockBar.clearAnimation();
        this.mNavigation.clearAnimation();
        this.mDrawer.j();
        Workspace workspace = this.mWorkspace;
        CellLayout cellLayout = (CellLayout) workspace.getChildAt(workspace.d());
        cellLayout.clearAnimation();
        cellLayout.setAlwaysDrawnWithCacheEnabled(false);
        cellLayout.setChildrenDrawnWithCacheEnabled(false);
        cellLayout.setDrawingCacheQuality(0);
        closeUserFolderFast(userFolder);
        workspace.p();
    }

    public final void shadeViewsReserve(bq bqVar, UserFolder userFolder) {
        int i = 250;
        if (bqVar.n != -300) {
            i = 230;
        }
        switch ((bqVar.b.size() + 1) / 4) {
            case 1:
                i = (int) (((double) i) * 1.15d);
                break;
            case 2:
                i = (int) (((double) i) * 1.3d);
                break;
        }
        LinearInterpolator linearInterpolator = new LinearInterpolator();
        Animation loadAnimation = AnimationUtils.loadAnimation(this.mContext, R.anim.shade_anim_reserve);
        loadAnimation.setFillAfter(true);
        loadAnimation.setDuration((long) i);
        loadAnimation.setInterpolator(linearInterpolator);
        if (bqVar.n != -300) {
            View childAt = this.mWorkspace.getChildAt(this.mWorkspace.d());
            this.mNavigation.startAnimation(loadAnimation);
            this.mHandleButton.startAnimation(loadAnimation);
            this.mDockBar.startAnimation(loadAnimation);
            if (userFolder.a == 0) {
                childAt.startAnimation(loadAnimation);
                return;
            }
            AnimationSet animationSet = new AnimationSet(true);
            animationSet.setInterpolator(linearInterpolator);
            animationSet.setDuration((long) i);
            animationSet.addAnimation(new TranslateAnimation(0.0f, 0.0f, (float) userFolder.a, 0.0f));
            animationSet.addAnimation(loadAnimation);
            animationSet.setFillAfter(true);
            animationSet.setDuration((long) i);
            childAt.startAnimation(animationSet);
        } else if (userFolder.a == 0) {
            this.mDrawer.a(loadAnimation);
        } else {
            AnimationSet animationSet2 = new AnimationSet(true);
            animationSet2.setInterpolator(linearInterpolator);
            animationSet2.setDuration((long) i);
            animationSet2.addAnimation(new TranslateAnimation(0.0f, 0.0f, (float) userFolder.a, 0.0f));
            animationSet2.addAnimation(loadAnimation);
            animationSet2.setFillAfter(true);
            this.mDrawer.a(animationSet2);
        }
    }

    public final void shadeViewsReserveDragOut(UserFolder userFolder) {
        this.mHandleButton.clearAnimation();
        this.mDockBar.clearAnimation();
        this.mNavigation.clearAnimation();
        this.mDrawer.j();
        Workspace workspace = this.mWorkspace;
        View childAt = workspace.getChildAt(workspace.d());
        childAt.clearAnimation();
        childAt.setDrawingCacheQuality(0);
        closeUserFolderFast(userFolder);
        workspace.p();
    }

    public final void showAddAppDialog(ArrayList arrayList, View view, int i, bq bqVar) {
        int i2;
        ArrayList h = sModel.h();
        ArrayList arrayList2 = new ArrayList();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            am amVar = (am) it.next();
            String uri = amVar.c.toURI();
            if (uri == null || !uri.startsWith("com.tencent")) {
                arrayList2.add(new gy(this, amVar, true, false));
            } else {
                arrayList2.add(new gy(this, amVar, true, true));
            }
        }
        ArrayList arrayList3 = new ArrayList();
        Iterator it2 = h.iterator();
        while (it2.hasNext()) {
            ha haVar = (ha) it2.next();
            if (haVar instanceof am) {
                am amVar2 = (am) haVar;
                if (!containApp(arrayList, amVar2)) {
                    String uri2 = amVar2.c.toURI();
                    if (uri2 == null || !uri2.startsWith("com.tencent")) {
                        arrayList3.add(new gy(this, amVar2, false, false));
                    } else {
                        arrayList3.add(0, new gy(this, amVar2, false, true));
                    }
                }
            } else if ((haVar instanceof bq) && i != -300) {
                Iterator it3 = ((bq) haVar).b.iterator();
                while (it3.hasNext()) {
                    am amVar3 = (am) it3.next();
                    if (!containApp(arrayList, amVar3)) {
                        String uri3 = amVar3.c.toURI();
                        if (uri3 == null || !uri3.startsWith("com.tencent")) {
                            arrayList3.add(new gy(this, amVar3, false, false));
                        } else {
                            arrayList3.add(0, new gy(this, amVar3, false, true));
                        }
                    }
                }
            }
        }
        Collections.sort(arrayList3, new dx());
        arrayList2.addAll(arrayList3);
        arrayList3.clear();
        boolean[] zArr = new boolean[arrayList2.size()];
        int size = arrayList2.size();
        int i3 = 0;
        int i4 = 0;
        while (i3 < size) {
            if (((gy) arrayList2.get(i3)).d) {
                zArr[i3] = true;
                i2 = i4 + 1;
            } else {
                zArr[i3] = false;
                i2 = i4;
            }
            i3++;
            i4 = i2;
        }
        com.tencent.module.setting.e eVar = new com.tencent.module.setting.e(this.mContext);
        eVar.a(getString(R.string.add_application, new Object[]{Integer.valueOf(i4), 12}));
        eVar.a(arrayList2, zArr, 12, new eb(this, arrayList2));
        ed edVar = new ed(this, arrayList, i, arrayList2, bqVar, view);
        ef efVar = new ef(this, arrayList2);
        eVar.a((int) R.string.confirm, edVar);
        eVar.b((int) R.string.cancel, efVar);
        eVar.c().show();
    }

    public final void showAddAppPicker() {
        int f = this.mWorkspace.f();
        ArrayList h = sModel.h();
        ArrayList arrayList = new ArrayList();
        int size = h.size();
        for (int i = 0; i < size; i++) {
            ha haVar = (ha) h.get(i);
            if (haVar instanceof am) {
                arrayList.add(new gy(this, (am) haVar, false, false));
            } else if (haVar instanceof bq) {
                Iterator it = ((bq) haVar).b.iterator();
                while (it.hasNext()) {
                    arrayList.add(new gy(this, (am) it.next(), false, false));
                }
            }
        }
        Collections.sort(arrayList, new dx());
        com.tencent.module.setting.e eVar = new com.tencent.module.setting.e(this.mContext);
        eVar.a(getString(R.string.add_application, new Object[]{0, Integer.valueOf(f)}));
        eVar.a(arrayList, new boolean[arrayList.size()], f, new ea(this, arrayList));
        dz dzVar = new dz(this, arrayList);
        ec ecVar = new ec(this, arrayList);
        eVar.a((int) R.string.confirm, dzVar);
        eVar.b((int) R.string.cancel, ecVar);
        eVar.c().show();
    }

    /* access modifiers changed from: package-private */
    public final void showDockBar() {
        this.mDockBar.setVisibility(0);
    }

    /* access modifiers changed from: package-private */
    public final void showDockButtonGroup() {
        this.mDockButtonGroup.setVisibility(0);
    }

    /* access modifiers changed from: package-private */
    public final void showHomeFlipper() {
        this.mHomeFlipper.setVisibility(0);
    }

    public final void showImportDataDialog() {
        if (hasMultiLauncher(this.mContext) != 0) {
            getItemsCount();
            String string = getResources().getString(R.string.import_default_launcher_data);
            com.tencent.module.setting.e eVar = new com.tencent.module.setting.e(this);
            eVar.a((int) R.string.import_data);
            eVar.b(string);
            eVar.b();
            eVar.a((int) R.string.import_btn, new eh(this)).b((int) R.string.cancel, new eg(this));
            eVar.c().show();
        }
    }

    public final void showPreNextZone() {
        this.mPreZone.setVisibility(0);
        this.mNextZone.setVisibility(0);
    }

    public final void showQQWidgetDialog() {
        gn gnVar = new gn(h.a().b(), this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.qqwidgetdilog_title);
        builder.setAdapter(gnVar, new db(this));
        builder.setInverseBackgroundForced(true);
        builder.create().show();
    }

    /* access modifiers changed from: package-private */
    public final void showRenameDialog(ex exVar, int i) {
        this.mWorkspace.q();
        this.mFolderInfo = exVar;
        this.mRenameFolderType = i;
        this.mWaitingForResult = true;
        showDialog(2);
    }

    /* access modifiers changed from: package-private */
    public final void showSearchDialog(String str, boolean z, Bundle bundle, boolean z2) {
        String str2;
        Bundle bundle2;
        if (str == null) {
            String typedText = getTypedText();
            this.mDefaultKeySsb.clear();
            this.mDefaultKeySsb.clearSpans();
            Selection.setSelection(this.mDefaultKeySsb, 0);
            str2 = typedText;
        } else {
            str2 = str;
        }
        if (bundle == null) {
            Bundle bundle3 = new Bundle();
            bundle3.putString("source", "launcher-search");
            bundle2 = bundle3;
        } else {
            bundle2 = bundle;
        }
        SearchManager searchManager = (SearchManager) getSystemService("search");
        if (this.mWorkspace.n() != null) {
            searchManager.setOnCancelListener(new ct(this, searchManager));
        }
        searchManager.startSearch(str2, z, getComponentName(), bundle2, z2);
    }

    public final void showThumbnailManager() {
        if (!this.mThumbnailManagerShowed) {
            this.mThumbnailManagerShowed = true;
            System.currentTimeMillis();
            this.mHandleButton.setVisibility(8);
            this.mDockBar.setVisibility(8);
            this.mNavigation.setVisibility(8);
            this.mThumbnailWorkspace.a(false);
            int v = this.mWorkspace.v();
            int childCount = this.mWorkspace.getChildCount();
            if (this.mFirstManager) {
                for (int i = 0; i < childCount; i++) {
                    this.mThumbnailWorkspace.a((ViewGroup) this.mWorkspace.getChildAt(i));
                }
                this.mFirstManager = false;
                ff.a = false;
                this.mThumbnailWorkspace.a(v);
            }
            boolean z = ff.a;
            for (int i2 = 0; i2 < childCount; i2++) {
                ViewGroup viewGroup = (ViewGroup) this.mWorkspace.getChildAt(i2);
                boolean z2 = viewGroup.getChildCount() <= 0;
                if (z) {
                    this.mThumbnailWorkspace.a(viewGroup, i2);
                }
                this.mThumbnailWorkspace.a(i2, z2);
            }
            this.mThumbnailWorkspace.b(this.mWorkspace.d());
            this.mThumbnailWorkspace.setVisibility(0);
            this.mThumbnailWorkspace.c();
            this.mThumbnailWorkspace.startAnimation(AnimationUtils.loadAnimation(this.mContext, R.anim.zoom_in));
            this.mWorkspace.y();
            this.mWorkspace.clearFocus();
        }
    }

    public final void startActivityForResult(Intent intent, int i) {
        if (i >= 0) {
            this.mWaitingForResult = true;
        }
        super.startActivityForResult(intent, i);
    }

    /* access modifiers changed from: package-private */
    public final void startActivitySafely(Intent intent) {
        intent.addFlags(268435456);
        try {
            startActivity(intent);
            sModel.a(this, intent.getComponent());
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, (int) R.string.activity_not_found, 0).show();
        } catch (SecurityException e2) {
            Toast.makeText(this, (int) R.string.activity_not_found, 0).show();
            Log.e(LOG_TAG, "Launcher does not have the permission to launch " + intent + ". Make sure to create a MAIN intent-filter for the corresponding activity " + "or use the exported attribute for this activity.", e2);
        } catch (Exception e3) {
            Toast.makeText(this, (int) R.string.start_activity_failed, 0).show();
            Log.e(LOG_TAG, "fail to launch " + intent, e3);
        }
    }

    public final void startSearch(String str, boolean z, Bundle bundle, boolean z2) {
        if (this.isLoadHome) {
            closeDrawer(false);
            Search n = this.mWorkspace.n();
            if (n == null) {
                showSearchDialog(str, z, bundle, z2);
                return;
            }
            n.a(str, z, bundle, z2);
            n.a(getTypedText());
        }
    }

    /* access modifiers changed from: package-private */
    public final void stopHome() {
        finish();
        Process.killProcess(Process.myPid());
    }

    public final void stopLoaders() {
        getModel().a();
    }

    /* access modifiers changed from: package-private */
    public final void stopSearch() {
        ((SearchManager) getSystemService("search")).stopSearch();
        Search n = this.mWorkspace.n();
        if (n != null) {
            n.a();
        }
    }

    public final void updateDrawerBackGroundColor(String str) {
        this.mDrawer.a(str);
    }

    public final void updateNewAppsNofity() {
        ArrayList h = ff.h(this);
        String b = i.a().b("setting_normal_functionlist", "");
        if (!(!b.equals(getResources().getString(R.string.setting_qqlauncher_functionlist_listmode)) && !b.equals("1"))) {
            this.mNewAppNofifyView.setVisibility(8);
        } else if (h == null || h.size() <= 0) {
            this.mNewAppNofifyView.setVisibility(8);
            this.mDrawer.l();
            this.mHasNewAppsNotify = false;
        } else if (this.mHandleButton.getVisibility() == 0) {
            this.mNewAppNofifyView.setVisibility(0);
        } else {
            this.mDrawer.m();
            this.mDrawer.k();
        }
    }
}
