package com.tencent.launcher;

import android.view.animation.Animation;

final class dd implements Animation.AnimationListener {
    private /* synthetic */ int a;
    private /* synthetic */ Launcher b;

    dd(Launcher launcher, int i) {
        this.b = launcher;
        this.a = i;
    }

    public final void onAnimationEnd(Animation animation) {
        this.b.mThumbnailWorkspace.setVisibility(8);
        this.b.mDragLayer.setVisibility(0);
        this.b.mWorkspace.y();
        this.b.mWorkspace.b(this.a);
        this.b.mThumbnailManagerShowed = false;
        this.b.mHandleButton.setVisibility(0);
        this.b.mDockBar.setVisibility(0);
        Launcher.access$5500(this.b);
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
