package com.tencent.launcher;

import android.content.Context;
import android.os.Message;
import android.os.Process;
import com.tencent.module.setting.AboutSettingActivity;
import com.tencent.util.p;

final class hv extends Thread {
    private /* synthetic */ Launcher a;

    hv(Launcher launcher) {
        this.a = launcher;
    }

    public final void run() {
        Process.setThreadPriority(10);
        if (p.b((Context) this.a) && AboutSettingActivity.getUpdateUrl()) {
            Message message = new Message();
            if (AboutSettingActivity.updateType != 0) {
                message.what = AboutSettingActivity.UPDATE_READY;
            }
            this.a.messageHandler.sendMessage(message);
        }
    }
}
