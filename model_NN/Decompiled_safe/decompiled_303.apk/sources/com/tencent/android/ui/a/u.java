package com.tencent.android.ui.a;

import AndroidDLoader.Software;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Environment;
import android.os.Handler;
import android.os.RemoteException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.android.a.a;
import com.tencent.android.a.c;
import com.tencent.android.ui.LocalSoftManageActivity;
import com.tencent.launcher.Launcher;
import com.tencent.launcher.base.BaseApp;
import com.tencent.module.download.DownloadInfo;
import com.tencent.module.download.DownloadService;
import com.tencent.module.download.b;
import com.tencent.module.download.r;
import com.tencent.module.download.y;
import com.tencent.qqlauncher.R;
import com.tencent.widget.QProgressBar;
import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

public final class u extends BaseExpandableListAdapter implements ai, f {
    private static int j = 0;
    private static int k = 1;
    private static DecimalFormat s = new DecimalFormat("###.00");
    private View.OnClickListener A = new t(this);
    private boolean B = false;
    List a = new LinkedList();
    Map b = new HashMap();
    Map c = new ConcurrentHashMap();
    List d = new ArrayList();
    public boolean e = true;
    Handler f = new m(this);
    private LayoutInflater g = null;
    /* access modifiers changed from: private */
    public LocalSoftManageActivity h = null;
    private c i = null;
    private Map l = new HashMap();
    private List m = new LinkedList();
    private Map n = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    public Handler o = null;
    /* access modifiers changed from: private */
    public i p = null;
    private Map q = new ConcurrentHashMap();
    private boolean r = true;
    /* access modifiers changed from: private */
    public b t = null;
    private r u = null;
    /* access modifiers changed from: private */
    public y v = null;
    private ServiceConnection w = null;
    private boolean x = false;
    private View.OnClickListener y = new r(this);
    private View.OnClickListener z = new s(this);

    public u(LocalSoftManageActivity localSoftManageActivity, Handler handler) {
        this.h = localSoftManageActivity;
        this.o = handler;
        try {
            this.i = new c(localSoftManageActivity);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        this.u = new o(this);
        this.t = new p(this);
        a(false);
        this.w = new q(this);
        Launcher launcher = Launcher.getLauncher();
        if (launcher != null) {
            launcher.clearStatusBarTapScrollTopViews();
        }
    }

    private static j a(com.tencent.android.b.a.c cVar) {
        j jVar = new j();
        jVar.a = new Software();
        jVar.a.f = cVar.c;
        jVar.a.l = cVar.b;
        Software software = jVar.a;
        Software software2 = jVar.a;
        String str = cVar.a;
        software2.b = str;
        software.e = str;
        jVar.a.j = (int) cVar.h;
        jVar.g = (long) ((int) cVar.h);
        jVar.a.h = a.c(cVar.g);
        jVar.c = 0;
        jVar.e = cVar.g;
        jVar.b = cVar.c;
        jVar.d = cVar.f;
        try {
            jVar.f = cVar.e != null ? com.tencent.launcher.a.a(BaseApp.b(), cVar.e) : com.tencent.launcher.a.a(BaseApp.b(), BaseApp.b().getResources().getDrawable(R.drawable.sw_default_icon));
        } catch (OutOfMemoryError e2) {
            jVar.f = null;
        }
        jVar.i = true;
        jVar.g = (long) ((int) cVar.h);
        return jVar;
    }

    private static j a(DownloadInfo downloadInfo) {
        j jVar = new j();
        jVar.a = new Software();
        jVar.a.k = downloadInfo.a;
        jVar.a.l = downloadInfo.c;
        jVar.a.j = (int) downloadInfo.h;
        jVar.d = downloadInfo.b;
        jVar.c = 2;
        try {
            jVar.f = downloadInfo.i != null ? com.tencent.launcher.a.a(BaseApp.b(), new BitmapDrawable(BaseApp.b().getResources(), downloadInfo.i).getCurrent()) : com.tencent.launcher.a.a(BaseApp.b(), BaseApp.b().getResources().getDrawable(R.drawable.sw_default_icon));
        } catch (OutOfMemoryError e2) {
            jVar.f = downloadInfo.i != null ? downloadInfo.i : null;
        }
        jVar.i = true;
        Software software = jVar.a;
        Software software2 = jVar.a;
        String str = downloadInfo.d;
        software2.b = str;
        software.e = str;
        jVar.g = (long) ((int) downloadInfo.h);
        return jVar;
    }

    static /* synthetic */ void a(u uVar, String str) {
        j jVar = (j) uVar.l.get(str);
        if (jVar != null) {
            uVar.c.remove(jVar.a.k);
            uVar.m.remove(jVar);
            uVar.l.remove(str);
            super.notifyDataSetChanged();
        }
    }

    static /* synthetic */ void a(u uVar, String str, int i2, int i3) {
        w wVar;
        if (str != null && str.length() != 0 && (wVar = (w) uVar.q.get(str)) != null && str.equals(wVar.e)) {
            wVar.d.setProgress((int) ((double) (((float) (((long) i3) * 100)) / ((float) i2))));
        }
    }

    static /* synthetic */ void a(u uVar, String str, String str2) {
        if (uVar.c.containsKey(str)) {
            j jVar = (j) uVar.c.get(str);
            jVar.d = str2;
            uVar.m.remove(jVar);
            if (!uVar.b.containsKey(jVar.a.l)) {
                uVar.a.add(0, jVar);
                if (uVar.i != null) {
                    File file = new File(str2);
                    com.tencent.android.b.a.c cVar = null;
                    try {
                        cVar = uVar.i.a(file.getAbsolutePath());
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    if (cVar != null) {
                        jVar.b = cVar.c;
                        jVar.a.f = cVar.c;
                    }
                    jVar.e = file.lastModified();
                    jVar.g = file.length();
                }
                uVar.b.put(jVar.a.l, jVar);
            }
            jVar.c = 0;
            uVar.p.b(jVar.a.l, jVar);
            uVar.l.remove(jVar.a.l);
            uVar.o.sendEmptyMessage(LocalSoftManageActivity.MSG_downloadlist);
            uVar.o.sendEmptyMessage(LocalSoftManageActivity.MSG_update);
            return;
        }
        uVar.o.sendEmptyMessage(LocalSoftManageActivity.MSG_update);
    }

    private void h() {
        j[] jVarArr = new j[this.a.size()];
        for (int i2 = 0; i2 < this.a.size(); i2++) {
            jVarArr[i2] = (j) this.a.get(i2);
        }
        Arrays.sort(jVarArr, 0, this.a.size(), new k());
        this.a.clear();
        for (j add : jVarArr) {
            this.a.add(add);
        }
    }

    private void i() {
        a(true);
        if (this.p != null) {
            for (int i2 = 0; i2 < this.a.size(); i2++) {
                j jVar = (j) this.a.get(i2);
                this.p.b(jVar.a.l, jVar);
            }
            for (int i3 = 0; i3 < this.m.size(); i3++) {
                j jVar2 = (j) this.m.get(i3);
                this.p.a(jVar2.a.l, jVar2);
            }
            this.p.a();
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        if (this.d != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.d.size()) {
                    DownloadInfo downloadInfo = (DownloadInfo) this.d.get(i3);
                    if (!this.l.containsKey(downloadInfo.c)) {
                        j a2 = a(downloadInfo);
                        this.l.put(downloadInfo.c, a2);
                        this.m.add(a2);
                        this.c.put(downloadInfo.a, a2);
                        if (this.p != null) {
                            this.p.a(downloadInfo.c, (j) this.l.get(downloadInfo.c));
                        }
                    }
                    try {
                        this.v.a(downloadInfo.a, "LocalSoftDownloadAdapter", this.t);
                    } catch (RemoteException e2) {
                        e2.printStackTrace();
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public final void a() {
        i();
    }

    public final void a(i iVar) {
        this.p = iVar;
    }

    public final void a(j jVar) {
        this.m.add(jVar);
        this.c.put(jVar.a.k, jVar);
        jVar.c = 2;
        this.l.put(jVar.a.l, jVar);
        if (this.p != null) {
            this.p.a(jVar.a.l, jVar);
        }
        try {
            if (this.v != null) {
                y yVar = this.v;
                DownloadInfo downloadInfo = new DownloadInfo();
                downloadInfo.d = jVar.a.b;
                downloadInfo.i = jVar.f;
                downloadInfo.c = jVar.a.l;
                downloadInfo.b = jVar.d;
                downloadInfo.h = (long) jVar.a.j;
                downloadInfo.a = jVar.a.k;
                downloadInfo.k = jVar.a.a;
                downloadInfo.m = jVar.a.i;
                downloadInfo.l = jVar.a.d;
                yVar.a(downloadInfo, "LocalSoftDownloadAdapter", this.t);
            }
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.ui.a.i.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.android.ui.a.i.a(java.lang.String, com.tencent.android.ui.a.j):void
      com.tencent.android.ui.a.i.a(java.lang.String, boolean):void */
    public final void a(Vector vector) {
        for (int i2 = 0; i2 < vector.size(); i2++) {
            String str = ((com.tencent.android.b.a.b) vector.elementAt(i2)).a;
            String valueOf = String.valueOf(((com.tencent.android.b.a.b) vector.elementAt(i2)).c);
            j jVar = (j) this.b.get(str);
            if (jVar == null) {
                this.p.a(str, true);
            } else if (jVar.a.l.equalsIgnoreCase(str) && jVar.b.equalsIgnoreCase(valueOf)) {
                com.tencent.android.a.b.a(jVar.d);
                this.p.a(str, true);
                this.a.remove(jVar);
                this.r = false;
                this.b.remove(str);
                this.o.sendEmptyMessage(LocalSoftManageActivity.MSG_download);
            }
        }
    }

    public final void a(boolean z2) {
        if (this.a.size() == 0 || z2) {
            if (Environment.getExternalStorageState().equals("mounted")) {
                File externalStorageDirectory = Environment.getExternalStorageDirectory();
                if (Environment.getExternalStorageState() != "unmounted") {
                    File[] listFiles = new File(externalStorageDirectory.getAbsolutePath() + File.separator + "Tencent" + File.separator + "QQDownload" + File.separator + "Apk").listFiles(new n(this));
                    this.a.clear();
                    this.b.clear();
                    if (this.i != null) {
                        if (listFiles != null) {
                            for (File file : listFiles) {
                                try {
                                    com.tencent.android.b.a.c a2 = this.i.a(file.getAbsolutePath());
                                    if (!(a2 == null || file == null)) {
                                        a2.h = file.length();
                                        a2.g = file.lastModified();
                                        a2.f = file.getAbsolutePath();
                                        j a3 = a(a2);
                                        this.a.add(a3);
                                        this.b.put(a2.b, a3);
                                        j jVar = new j();
                                        jVar.c = 0;
                                        jVar.a = new Software();
                                        jVar.a.l = a2.b;
                                        jVar.a.b = a2.a;
                                        jVar.a.f = a2.c;
                                        jVar.b = a2.c;
                                        jVar.a.h = a.b(a2.g);
                                        jVar.d = a2.f;
                                        jVar.c = 0;
                                        j jVar2 = (j) this.l.get(jVar.a.l);
                                        if (jVar2 != null) {
                                            this.m.remove(jVar2);
                                            this.l.remove(jVar2.a.l);
                                            this.c.remove(jVar2.a.k);
                                            jVar2.c = 0;
                                            if (this.p != null) {
                                                this.p.b(jVar2.a.l, jVar2);
                                            }
                                            this.o.sendEmptyMessage(LocalSoftManageActivity.MSG_update);
                                        }
                                        if (this.p != null) {
                                            this.p.b(a2.b, jVar);
                                        }
                                        this.x = false;
                                    }
                                } catch (Exception e2) {
                                    e2.printStackTrace();
                                }
                            }
                            h();
                        } else if (this.p != null) {
                            this.p.a();
                        }
                    }
                }
            } else if (!this.x) {
                BaseApp.a((int) R.string.sdcard_remove);
                this.x = true;
            }
            notifyDataSetChanged();
        }
    }

    public final boolean a(View view) {
        if (!Environment.getExternalStorageState().equals("mounted")) {
            BaseApp.a((int) R.string.sdcard_remove_install);
            return true;
        }
        Object tag = view.getTag();
        if (tag != null && (tag instanceof l) && view.getId() == 10) {
            return true;
        }
        if (tag == null || !(tag instanceof l) || view.getId() != 20) {
            return false;
        }
        com.tencent.android.a.b.a(((j) ((l) tag).g).d, this.h);
        return true;
    }

    public final void b() {
        if (this.v != null) {
            try {
                this.v.c();
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
    }

    public final int c() {
        return this.m.size();
    }

    public final void d() {
        if (!this.h.bindService(new Intent(this.h, DownloadService.class), this.w, 1)) {
            this.o.sendEmptyMessage(LocalSoftManageActivity.MSG_downloadFail);
        } else if (this.v != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.m.size()) {
                    try {
                        this.v.a(((j) this.m.get(i3)).a.k, "LocalSoftDownloadAdapter", this.t);
                    } catch (RemoteException e2) {
                        e2.printStackTrace();
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public final void e() {
        if (this.w != null && this.v != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.m.size()) {
                    try {
                        this.v.a(((j) this.m.get(i3)).a.k, "LocalSoftDownloadAdapter");
                    } catch (RemoteException e2) {
                        e2.printStackTrace();
                    }
                    i2 = i3 + 1;
                } else {
                    this.h.unbindService(this.w);
                    return;
                }
            }
        }
    }

    public final void f() {
        i();
    }

    public final void g() {
        this.B = false;
    }

    public final Object getChild(int i2, int i3) {
        return null;
    }

    public final long getChildId(int i2, int i3) {
        return 0;
    }

    public final View getChildView(int i2, int i3, boolean z2, View view, ViewGroup viewGroup) {
        View view2;
        View inflate;
        l lVar;
        View view3;
        l lVar2 = null;
        System.currentTimeMillis();
        if (i2 == j) {
            if (view != null && view.getId() == 10) {
                view3 = view;
                lVar2 = (l) view.getTag();
            } else if (view == null || view.getId() != 10) {
                if (this.g == null) {
                    this.g = (LayoutInflater) this.h.getSystemService("layout_inflater");
                }
                View inflate2 = this.g.inflate((int) R.layout.local_soft_download_list_item_loading, (ViewGroup) null);
                inflate2.setId(10);
                l lVar3 = new l(this);
                lVar3.a = (ImageView) inflate2.findViewById(R.id.loading_software_icon);
                lVar3.b = (ImageView) inflate2.findViewById(R.id.loading_software_state_icon);
                lVar3.c = (TextView) inflate2.findViewById(R.id.loading_software_item_name);
                lVar3.d = (TextView) inflate2.findViewById(R.id.TextView_loading_data);
                lVar3.e = (QProgressBar) inflate2.findViewById(R.id.ProgressBar_loading);
                lVar3.f = (TextView) inflate2.findViewById(R.id.cancel_button);
                inflate2.setTag(lVar3);
                lVar2 = lVar3;
                view3 = inflate2;
            } else {
                view3 = null;
            }
            if (this.m != null && this.m.size() > 0 && i3 < this.m.size()) {
                j jVar = (j) this.m.get(i3);
                lVar2.g = jVar;
                lVar2.f.setTag(jVar);
                lVar2.f.setOnClickListener(this.z);
                Bitmap bitmap = jVar.f;
                if (bitmap != null) {
                    lVar2.a.setImageBitmap(bitmap);
                } else {
                    lVar2.a.setImageResource(R.drawable.sw_default_icon);
                }
                String str = jVar.a.b;
                if (jVar.a.b.length() > 6) {
                    str = str.substring(0, 6) + "...";
                }
                lVar2.c.setText(str);
                lVar2.d.setText(a.a((long) jVar.a.j));
                lVar2.d.setVisibility(0);
                double d2 = 0.0d;
                if (jVar.g != 0) {
                    d2 = (double) ((jVar.h * 100) / jVar.g);
                }
                lVar2.e.setProgress((int) d2);
                w wVar = (w) this.q.get(jVar.a.k);
                if (wVar == null) {
                    wVar = new w(this);
                }
                wVar.a = lVar2.a;
                wVar.b = lVar2.b;
                wVar.c = lVar2.d;
                wVar.d = lVar2.e;
                wVar.e = jVar.a.k;
                this.q.put(jVar.a.k, wVar);
            }
            view2 = view3;
        } else if (i2 != k) {
            view2 = null;
        } else if (this.a.size() == 0) {
            return null;
        } else {
            if (view != null && view.getId() == 20) {
                inflate = view;
                lVar = (l) view.getTag();
            } else if (view == null || view.getId() != 20) {
                if (this.g == null) {
                    this.g = (LayoutInflater) this.h.getSystemService("layout_inflater");
                }
                inflate = this.g.inflate((int) R.layout.local_soft_download_list_item_loaded, (ViewGroup) null);
                inflate.setId(20);
                lVar = new l(this);
                lVar.a = (ImageView) inflate.findViewById(R.id.loaded_software_icon);
                lVar.c = (TextView) inflate.findViewById(R.id.loaded_software_item_name);
                lVar.d = (TextView) inflate.findViewById(R.id.TextView_install_size);
                lVar.f = (TextView) inflate.findViewById(R.id.delete_button);
                inflate.setTag(lVar);
            } else {
                inflate = null;
                lVar = null;
            }
            if (this.a != null && this.a.size() > 0 && i3 < this.a.size()) {
                j jVar2 = (j) this.a.get(i3);
                if (jVar2 == null) {
                    return null;
                }
                lVar.g = jVar2;
                lVar.f.setTag(jVar2);
                lVar.f.setOnClickListener(this.y);
                Bitmap bitmap2 = jVar2.f;
                if (bitmap2 != null) {
                    lVar.a.setImageBitmap(bitmap2);
                } else {
                    lVar.a.setImageResource(R.drawable.sw_default_icon);
                }
                lVar.c.setText(jVar2.a.b);
                String str2 = jVar2.a.f;
                if (str2 == null) {
                    str2 = "未知版本";
                }
                lVar.d.setText(str2 + " | " + a.a(jVar2.g));
                inflate.setOnClickListener(this.A);
            }
            view2 = inflate;
        }
        System.currentTimeMillis();
        return view2;
    }

    public final int getChildrenCount(int i2) {
        if (i2 == j) {
            if (this.m != null) {
                return this.m.size();
            }
            return 0;
        } else if (i2 != k || this.a == null) {
            return 0;
        } else {
            return this.a.size();
        }
    }

    public final Object getGroup(int i2) {
        return null;
    }

    public final int getGroupCount() {
        try {
            if (this.v != null && !this.B) {
                this.d = this.v.b();
                if (!(this.d == null || this.d.size() == 0)) {
                    this.B = true;
                }
                j();
            }
        } catch (RemoteException e2) {
        }
        boolean z2 = this.m != null && this.m.size() > 0;
        boolean z3 = this.a != null && this.a.size() > 0;
        if (z2 && z3) {
            j = 0;
            k = 1;
            return 2;
        } else if (z2) {
            j = 0;
            k = -1;
            return 1;
        } else if (z3) {
            j = -1;
            k = 0;
            return 1;
        } else {
            j = -1;
            k = -1;
            return 0;
        }
    }

    public final long getGroupId(int i2) {
        return (long) i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getGroupView(int i2, boolean z2, View view, ViewGroup viewGroup) {
        View view2;
        if (view == null) {
            if (this.g == null) {
                this.g = (LayoutInflater) this.h.getSystemService("layout_inflater");
            }
            view2 = this.g.inflate((int) R.layout.local_manager_group, viewGroup, false);
        } else {
            view2 = view;
        }
        TextView textView = (TextView) view2.findViewById(R.id.LocalGroundName);
        if (i2 == j) {
            textView.setText((int) R.string.local_soft_loading);
        } else if (i2 == k) {
            textView.setText((int) R.string.local_soft_loaded);
        }
        return view2;
    }

    public final boolean hasStableIds() {
        return true;
    }

    public final boolean isChildSelectable(int i2, int i3) {
        return true;
    }

    public final void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
