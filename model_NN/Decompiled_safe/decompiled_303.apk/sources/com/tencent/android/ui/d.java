package com.tencent.android.ui;

import android.widget.ExpandableListView;

final class d implements ExpandableListView.OnGroupCollapseListener {
    private /* synthetic */ LocalSoftManageActivity a;

    d(LocalSoftManageActivity localSoftManageActivity) {
        this.a = localSoftManageActivity;
    }

    public final void onGroupCollapse(int i) {
        this.a.manageListView.expandGroup(i);
    }
}
