package com.tencent.android.ui.a;

import AndroidDLoader.SoftUpdate;
import AndroidDLoader.Software;
import AndroidDLoader.a;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.android.b.a.b;
import com.tencent.android.ui.LocalSoftManageActivity;
import com.tencent.launcher.Launcher;
import com.tencent.launcher.base.BaseApp;
import com.tencent.module.appcenter.SoftWareActivity;
import com.tencent.module.appcenter.d;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

public final class af extends BaseAdapter implements ae, i, v {
    /* access modifiers changed from: private */
    public LocalSoftManageActivity a = null;
    private String b = "";
    private HashMap c = new HashMap();
    private Vector d = new Vector();
    /* access modifiers changed from: private */
    public Handler e = null;
    /* access modifiers changed from: private */
    public Map f = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    public ai g = null;
    private int h = 0;
    private String i = "";
    private View.OnClickListener j = new b(this);
    private View.OnClickListener k = new d(this);
    private View.OnClickListener l = new c(this);
    private View.OnClickListener m = new e(this);

    public af(LocalSoftManageActivity localSoftManageActivity, Handler handler, ai aiVar) {
        this.a = localSoftManageActivity;
        this.g = aiVar;
        this.e = handler;
        this.i = BaseApp.b().getResources().getString(R.string.local_soft_manager_detail_unknown);
        Launcher launcher = Launcher.getLauncher();
        if (launcher != null) {
            launcher.clearStatusBarTapScrollTopViews();
        }
    }

    private void c() {
        d a2 = d.a();
        Handler handler = this.e;
        ArrayList arrayList = new ArrayList();
        if (this.f.size() != 0) {
            for (Map.Entry value : this.f.entrySet()) {
                b bVar = (b) value.getValue();
                SoftUpdate softUpdate = new SoftUpdate();
                softUpdate.a = -1;
                softUpdate.b = bVar.a;
                softUpdate.c = String.valueOf(bVar.b);
                softUpdate.d = bVar.d;
                softUpdate.e = bVar.c;
                arrayList.add(softUpdate);
            }
        }
        a2.a(handler, arrayList, this);
    }

    private void d() {
        j[] jVarArr = new j[this.d.size()];
        for (int i2 = 0; i2 < this.d.size(); i2++) {
            jVarArr[i2] = (j) this.d.get(i2);
        }
        Arrays.sort(jVarArr, 0, this.d.size(), new aa());
        this.d.clear();
        for (j add : jVarArr) {
            this.d.add(add);
        }
    }

    public final void a() {
        c();
    }

    public final void a(b bVar) {
        this.f.put(bVar.a, bVar);
        c();
        super.notifyDataSetChanged();
    }

    public final void a(String str, j jVar) {
        if (this.f.containsKey(str)) {
            if (this.c != null) {
                this.d.remove((j) this.c.put(str, jVar));
                jVar.c = 2;
                this.d.add(jVar);
            } else {
                this.c = new HashMap();
                this.d.remove((j) this.c.put(str, jVar));
                this.d.add(jVar);
            }
            d();
            this.e.sendEmptyMessage(LocalSoftManageActivity.MSG_download);
        }
    }

    public final void a(String str, boolean z) {
        j jVar = (j) this.c.get(str);
        if (!z) {
            if (jVar != null) {
                jVar.c = 1;
            }
            if (jVar != null) {
                this.d.remove(jVar);
                this.d.add(jVar);
            }
            d();
            return;
        }
        if (jVar != null) {
            this.d.remove(jVar);
        }
        this.c.remove(str);
        d();
        this.e.sendEmptyMessage(LocalSoftManageActivity.MSG_download);
    }

    public final void a(Map map) {
        for (Map.Entry value : map.entrySet()) {
            Software software = (Software) value.getValue();
            if (!this.f.containsKey(software.l)) {
                j jVar = (j) this.c.get(software.l);
                if (jVar != null) {
                    this.d.remove(jVar);
                }
                this.c.remove(software.l);
            } else if (!this.c.containsKey(software.l)) {
                j jVar2 = new j();
                jVar2.a = software;
                jVar2.a.j *= Launcher.APPWIDGET_HOST_ID;
                this.c.put(software.l, jVar2);
                this.d.add(jVar2);
            } else if (this.f.containsKey(software.l)) {
                j jVar3 = (j) this.c.get(software.l);
                this.d.remove(jVar3);
                jVar3.a = software;
                jVar3.a.j *= Launcher.APPWIDGET_HOST_ID;
                this.d.add(jVar3);
                this.c.put(software.l, jVar3);
            }
        }
        d();
        this.e.sendEmptyMessage(LocalSoftManageActivity.MSG_update);
    }

    public final void a(Vector vector) {
        if (vector != null) {
            this.f.clear();
            for (int i2 = 0; i2 < vector.size(); i2++) {
                b bVar = (b) vector.elementAt(i2);
                this.f.put(bVar.a, bVar);
            }
            for (int i3 = 0; i3 < this.d.size(); i3++) {
                j jVar = (j) this.d.elementAt(i3);
                if (!this.f.containsKey(jVar.a.l)) {
                    this.c.remove(jVar.a.l);
                    this.d.remove(i3);
                }
            }
        }
    }

    public final boolean a(View view) {
        int i2;
        Object tag = view.getTag();
        if (tag != null && (tag instanceof ah)) {
            Object obj = ((ah) tag).e;
            if (obj instanceof j) {
                j jVar = (j) obj;
                if (jVar.a.a == 0 || jVar.a.d == 0 || jVar.a.i == 0) {
                    Toast.makeText(BaseApp.b(), this.i, 0).show();
                } else {
                    switch (jVar.c) {
                        case 0:
                            i2 = 2;
                            break;
                        case 1:
                            i2 = 0;
                            break;
                        case 2:
                            i2 = 1;
                            break;
                        default:
                            i2 = 0;
                            break;
                    }
                    SoftWareActivity.openSoftwareActivityWithStatus(this.a, jVar.a.a, jVar.a.d, jVar.a.i, a.f, i2);
                }
                return true;
            }
        }
        return false;
    }

    public final int b() {
        int i2 = 0;
        if (this.d == null) {
            return 0;
        }
        int i3 = 0;
        while (i2 < this.d.size()) {
            j jVar = (j) this.d.elementAt(i2);
            i2++;
            i3 = (jVar.c == 1 || jVar.c == 0) ? i3 + 1 : i3;
        }
        return i3;
    }

    public final void b(String str, j jVar) {
        if (this.f.containsKey(str) && this.c.containsKey(str)) {
            j jVar2 = (j) this.c.get(str);
            if (jVar2 != null) {
                this.d.remove(jVar2);
            }
            if (jVar2 == null) {
                this.c.put(str, jVar);
                this.d.add(jVar);
            } else if (jVar.a == null || jVar.a.f == null || !((b) this.f.get(str)).c.equals(jVar.a.f)) {
                this.d.remove(jVar2);
                jVar2.d = jVar.d;
                jVar2.g = jVar.g;
                jVar2.c = 0;
                this.c.put(str, jVar2);
                this.d.add(jVar2);
            } else {
                this.e.sendEmptyMessage(LocalSoftManageActivity.MSG_reload);
            }
            d();
            this.e.sendEmptyMessage(LocalSoftManageActivity.MSG_update);
        }
    }

    public final void c(String str, j jVar) {
        if (this.f.containsKey(str)) {
            j jVar2 = (j) this.c.get(str);
            if (jVar2 != null) {
                this.d.remove(jVar2);
            }
            this.c.put(str, jVar);
            this.d.add(jVar);
            d();
            this.e.sendEmptyMessage(LocalSoftManageActivity.MSG_update);
        }
    }

    public final int getCount() {
        if (this.c == null) {
            return 0;
        }
        return this.c.size();
    }

    public final Object getItem(int i2) {
        return null;
    }

    public final long getItemId(int i2) {
        return (long) i2;
    }

    public final View getView(int i2, View view, ViewGroup viewGroup) {
        ah ahVar;
        View view2;
        System.currentTimeMillis();
        if (view == null) {
            View inflate = LayoutInflater.from(this.a).inflate((int) R.layout.local_soft_local_manage_item, (ViewGroup) null);
            ahVar = new ah(this);
            ahVar.a = (ImageView) inflate.findViewById(R.id.software_icon);
            ahVar.b = (TextView) inflate.findViewById(R.id.software_item_name);
            ahVar.c = (TextView) inflate.findViewById(R.id.TextView_versionName);
            ahVar.d = (TextView) inflate.findViewById(R.id.update_button);
            inflate.setTag(ahVar);
            view2 = inflate;
        } else {
            ahVar = (ah) view.getTag();
            view2 = view;
        }
        if (this.d == null || this.d.size() == 0 || i2 >= this.d.size()) {
            return view2;
        }
        j jVar = (j) this.d.get(i2);
        b bVar = (b) this.f.get(jVar.a.l);
        ahVar.e = jVar;
        if (bVar.e != null) {
            ahVar.a.setImageDrawable(bVar.e);
        } else {
            ahVar.a.setImageResource(R.drawable.sw_default_icon);
        }
        ahVar.b.setText(bVar.d == null ? "" : bVar.d);
        String str = jVar.a.h;
        int indexOf = str.indexOf(" ");
        if (indexOf >= 0) {
            str = str.substring(0, indexOf);
        }
        TextView textView = ahVar.c;
        StringBuilder append = new StringBuilder().append(this.a.getString(R.string.local_soft_manager_updatetime)).append(":");
        if (str.length() == 0) {
            str = this.a.getString(R.string.local_soft_manager_unknown);
        }
        textView.setText(append.append(str).toString());
        ahVar.d.setClickable(true);
        switch (jVar.c) {
            case 0:
                ahVar.d.setBackgroundResource(R.drawable.highlight_button_bg);
                ahVar.d.setTextColor(-1);
                ahVar.d.setText((int) R.string.isCanInstall);
                ahVar.d.setOnClickListener(this.l);
                break;
            case 1:
                ahVar.d.setBackgroundResource(R.drawable.strong_button_bg);
                ahVar.d.setTextColor(-11972523);
                ahVar.d.setText((int) R.string.isUpdate);
                ahVar.d.setOnClickListener(this.j);
                break;
            case 2:
                ahVar.d.setBackgroundResource(R.drawable.strong_button_bg);
                ahVar.d.setTextColor(-11972523);
                ahVar.d.setText((int) R.string.local_soft_loading);
                ahVar.d.setOnClickListener(this.k);
                break;
        }
        view2.setTag(ahVar);
        ahVar.d.setVisibility(0);
        ahVar.d.setTag(ahVar);
        view2.setOnClickListener(this.m);
        view2.setFocusable(true);
        return view2;
    }

    public final void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
