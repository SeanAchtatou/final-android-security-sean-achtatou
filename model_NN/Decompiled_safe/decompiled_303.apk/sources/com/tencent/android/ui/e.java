package com.tencent.android.ui;

import android.widget.AbsListView;

final class e implements AbsListView.OnScrollListener {
    private /* synthetic */ LocalSoftManageActivity a;

    e(LocalSoftManageActivity localSoftManageActivity) {
        this.a = localSoftManageActivity;
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
    }

    public final void onScrollStateChanged(AbsListView absListView, int i) {
        this.a.managerListAdapter.a = i == 0;
        if (this.a.downloadListAdapter.e) {
            this.a.downloadListAdapter.notifyDataSetChanged();
        }
    }
}
