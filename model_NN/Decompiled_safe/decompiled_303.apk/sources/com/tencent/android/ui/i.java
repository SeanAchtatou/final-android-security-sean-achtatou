package com.tencent.android.ui;

import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.tencent.launcher.base.BaseApp;
import com.tencent.qqlauncher.R;

final class i extends Handler {
    private /* synthetic */ LocalSoftManageActivity a;

    i(LocalSoftManageActivity localSoftManageActivity) {
        this.a = localSoftManageActivity;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case 900:
                Toast.makeText(BaseApp.b(), "访问服务器出现异常", 0).show();
                return;
            case 1010:
            case LocalSoftManageActivity.MSG_downloadFail:
            case LocalSoftManageActivity.MSG_updatelist:
            default:
                return;
            case 1501:
            case 1503:
                this.a.managerListAdapter.notifyDataSetChanged();
                return;
            case 1502:
                this.a.managerListAdapter.notifyDataSetChanged();
                return;
            case LocalSoftManageActivity.MSG_loaded:
                this.a.setSelectTabIndex(this.a.lastTabIndex);
                this.a.managerListAdapter.notifyDataSetChanged();
                this.a.expandGroupHandler.sendEmptyMessage(0);
                this.a.downloadListAdapter.notifyDataSetChanged();
                this.a.updateListAdapter.notifyDataSetChanged();
                return;
            case LocalSoftManageActivity.MSG_update:
                String unused = this.a.defaultUpdateTitle = this.a.getString(R.string.local_soft_manager_update);
                if (this.a.updateListAdapter.b() != 0) {
                    LocalSoftManageActivity.access$684(this.a, "(" + this.a.updateListAdapter.b() + ")");
                }
                this.a.setSelectTabIndex(this.a.lastTabIndex);
                if (this.a.lastTabIndex != 1) {
                    this.a.updateListAdapter.notifyDataSetChanged();
                    return;
                }
                return;
            case LocalSoftManageActivity.MSG_download:
                String unused2 = this.a.defaultDowloadListTitle = this.a.getString(R.string.local_soft_manager_download);
                if (this.a.downloadListAdapter.c() != 0) {
                    LocalSoftManageActivity.access$784(this.a, "(" + this.a.downloadListAdapter.c() + ")");
                }
                this.a.setSelectTabIndex(this.a.lastTabIndex);
                return;
            case LocalSoftManageActivity.MSG_all:
                String unused3 = this.a.defaultUpdateTitle = this.a.getString(R.string.local_soft_manager_update);
                if (this.a.updateListAdapter.getCount() != 0) {
                    LocalSoftManageActivity.access$684(this.a, "(" + this.a.updateListAdapter.getCount() + ")");
                }
                String unused4 = this.a.defaultDowloadListTitle = this.a.getString(R.string.local_soft_manager_download);
                if (this.a.downloadListAdapter.c() != 0) {
                    LocalSoftManageActivity.access$784(this.a, "(" + this.a.downloadListAdapter.c() + ")");
                }
                this.a.setSelectTabIndex(this.a.lastTabIndex);
                return;
            case LocalSoftManageActivity.MSG_settab:
                this.a.setSelectTabIndex(message.arg1);
                return;
            case LocalSoftManageActivity.MSG_downloadlist:
                this.a.expandGroupHandler.sendEmptyMessage(0);
                this.a.downloadListAdapter.notifyDataSetChanged();
                return;
            case LocalSoftManageActivity.MSG_httpErr:
                Toast.makeText(BaseApp.b(), "访问服务器出现异常", 0).show();
                return;
            case LocalSoftManageActivity.MSG_reload:
                this.a.managerListAdapter.a();
                return;
        }
    }
}
