package com.tencent.android.ui.a;

import com.tencent.android.b.a.b;
import java.util.Comparator;

public final class ag implements Comparator {
    public final int compare(Object obj, Object obj2) {
        if (!(obj instanceof b) || !(obj2 instanceof b)) {
            return 0;
        }
        String str = ((b) obj).d;
        String str2 = ((b) obj2).d;
        if (str.length() == str2.length()) {
            for (int i = 0; i < str.length(); i++) {
                if (str.charAt(i) != str2.charAt(i)) {
                    return str.charAt(i) - str2.charAt(i);
                }
            }
            return 0;
        }
        int length = str.length() > str2.length() ? str2.length() : str.length();
        for (int i2 = 0; i2 < length; i2++) {
            if (str.charAt(i2) != str2.charAt(i2)) {
                return str.charAt(i2) - str2.charAt(i2);
            }
        }
        return length == str.length() ? -1 : 1;
    }
}
