package com.tencent.android.ui;

import android.content.Context;
import android.os.Process;
import com.tencent.module.appcenter.d;
import java.text.SimpleDateFormat;
import java.util.Date;

final class c extends Thread {
    private /* synthetic */ LocalSoftManageActivity a;

    c(LocalSoftManageActivity localSoftManageActivity) {
        this.a = localSoftManageActivity;
    }

    public final void run() {
        Process.setThreadPriority(10);
        Context baseContext = this.a.getBaseContext();
        if (baseContext != null) {
            if (!new SimpleDateFormat("yyyy-MM-dd").format(new Date()).equals(baseContext.getSharedPreferences("reportStats", 0).getString("stats_date", ""))) {
                d.a().c();
            }
        }
    }
}
