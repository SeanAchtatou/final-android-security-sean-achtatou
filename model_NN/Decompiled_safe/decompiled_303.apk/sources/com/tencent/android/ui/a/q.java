package com.tencent.android.ui.a;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Message;
import com.tencent.android.ui.LocalSoftManageActivity;
import com.tencent.module.download.s;
import com.tencent.module.download.y;

final class q implements ServiceConnection {
    private /* synthetic */ u a;

    q(u uVar) {
        this.a = uVar;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        y unused = this.a.v = s.a(iBinder);
        Message.obtain(this.a.f, (int) LocalSoftManageActivity.MSG_downloadlist).sendToTarget();
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        y unused = this.a.v = (y) null;
    }
}
