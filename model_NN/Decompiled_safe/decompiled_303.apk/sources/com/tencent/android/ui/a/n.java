package com.tencent.android.ui.a;

import java.io.File;
import java.io.FileFilter;

final class n implements FileFilter {
    private /* synthetic */ u a;

    n(u uVar) {
        this.a = uVar;
    }

    public final boolean accept(File file) {
        if (file.isFile()) {
            String name = file.getName();
            if (name.substring(name.length() - 4).equalsIgnoreCase(".apk")) {
                return true;
            }
        }
        return false;
    }
}
