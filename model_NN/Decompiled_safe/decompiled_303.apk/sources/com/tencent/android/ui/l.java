package com.tencent.android.ui;

import android.os.Handler;
import android.os.Message;

final class l extends Handler {
    private /* synthetic */ LocalSoftManageActivity a;

    l(LocalSoftManageActivity localSoftManageActivity) {
        this.a = localSoftManageActivity;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        if (this.a.downloadListAdapter != null) {
            for (int i = 0; i < this.a.downloadListAdapter.getGroupCount(); i++) {
                this.a.downloadListView.expandGroup(i);
            }
        }
    }
}
