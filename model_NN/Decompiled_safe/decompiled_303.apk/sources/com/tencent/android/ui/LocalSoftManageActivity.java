package com.tencent.android.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.tencent.android.ui.a.af;
import com.tencent.android.ui.a.u;
import com.tencent.android.ui.a.x;
import com.tencent.launcher.base.b;
import com.tencent.qqlauncher.R;
import java.util.List;

public class LocalSoftManageActivity extends Activity {
    public static final int DOWNLOAD_LIST = 2;
    public static final int LOCAL_SOFT_MANAGE = 0;
    public static final int LOCAL_SOFT_UPDATE = 1;
    public static final int MSG_all = 2011;
    public static final int MSG_download = 2010;
    public static final int MSG_downloadFail = 2012;
    public static final int MSG_downloadlist = 2016;
    public static final int MSG_getListFail = 2013;
    public static final int MSG_httpErr = 2017;
    public static final int MSG_loaded = 2008;
    public static final int MSG_reload = 2018;
    public static final int MSG_settab = 2014;
    public static final int MSG_update = 2009;
    public static final int MSG_updatelist = 2015;
    private static final String TAG = "LocalSoftManageActivity";
    private static boolean needClear = false;
    public static int statusBarIntentTabIndex = -1;
    private boolean bOnResumeCauseByOnCreate = false;
    /* access modifiers changed from: private */
    public String defaultDowloadListTitle = "";
    /* access modifiers changed from: private */
    public String defaultUpdateTitle = "";
    /* access modifiers changed from: private */
    public u downloadListAdapter = null;
    /* access modifiers changed from: private */
    public ExpandableListView downloadListView = null;
    /* access modifiers changed from: private */
    public Handler expandGroupHandler = new l(this);
    private boolean hasMiss = false;
    private ImageView ivDownloadList = null;
    private ImageView ivLocalManage = null;
    private ImageView ivUpdateList = null;
    /* access modifiers changed from: private */
    public int lastTabIndex = -1;
    private Handler locGroupHandler = new a(this);
    private BroadcastReceiver mApplicationsReceiver = new j(this);
    private LayoutInflater mLayoutInflater = null;
    /* access modifiers changed from: private */
    public ExpandableListView manageListView = null;
    /* access modifiers changed from: private */
    public x managerListAdapter = null;
    private Handler netHandler = new k(this);
    private Handler refreshDownloadListHandler = new b(this);
    private View.OnClickListener tabOnClickListener = new h(this);
    private TextView tvDownloadList = null;
    private TextView tvDownloadListShadow = null;
    private TextView tvLocalManage = null;
    private TextView tvLocalManageShadow = null;
    private TextView tvUpdateList = null;
    private TextView tvUpdateListShadow = null;
    private Handler uiHandler = new i(this);
    private ListView updateList = null;
    /* access modifiers changed from: private */
    public af updateListAdapter = null;
    private List updates = null;
    private View viewDownloadList = null;
    private ViewFlipper viewFlipper = null;
    private View viewLocalManage = null;
    private View viewUpdateList = null;

    static /* synthetic */ void access$1000(LocalSoftManageActivity localSoftManageActivity) {
        localSoftManageActivity.setSelectTabIndex(localSoftManageActivity.lastTabIndex);
        localSoftManageActivity.downloadListAdapter.notifyDataSetChanged();
    }

    static /* synthetic */ String access$684(LocalSoftManageActivity localSoftManageActivity, Object obj) {
        String str = localSoftManageActivity.defaultUpdateTitle + obj;
        localSoftManageActivity.defaultUpdateTitle = str;
        return str;
    }

    static /* synthetic */ String access$784(LocalSoftManageActivity localSoftManageActivity, Object obj) {
        String str = localSoftManageActivity.defaultDowloadListTitle + obj;
        localSoftManageActivity.defaultDowloadListTitle = str;
        return str;
    }

    public static boolean getNeed() {
        return needClear;
    }

    private boolean hasTask() {
        return false;
    }

    private void initAdapter() {
        this.updateList = (ListView) findViewById(R.id.ListView_local_soft_update);
        this.manageListView = (ExpandableListView) findViewById(R.id.ListView_local_soft_manage);
        this.downloadListAdapter = new u(this, this.uiHandler);
        this.updateListAdapter = new af(this, this.uiHandler, this.downloadListAdapter);
        this.downloadListAdapter.a(this.updateListAdapter);
        this.updateList.setAdapter((ListAdapter) this.updateListAdapter);
        this.managerListAdapter = new x(this, this.uiHandler, this.updateListAdapter, this.downloadListAdapter);
        this.manageListView.setAdapter(this.managerListAdapter);
        for (int i = 0; i < this.managerListAdapter.getGroupCount(); i++) {
            this.manageListView.expandGroup(i);
        }
        this.manageListView.setOnGroupCollapseListener(new d(this));
        this.downloadListAdapter.f();
        this.manageListView.setOnScrollListener(new e(this));
        this.downloadListView = (ExpandableListView) findViewById(R.id.ExpandableListView_local_soft_download_list);
        this.downloadListView.setAdapter(this.downloadListAdapter);
        for (int i2 = 0; i2 < this.downloadListAdapter.getGroupCount(); i2++) {
            this.downloadListView.expandGroup(i2);
        }
        this.downloadListView.setOnGroupExpandListener(new f(this));
        this.downloadListView.setOnGroupCollapseListener(new g(this));
    }

    private void initTopBarTab() {
        this.viewFlipper = (ViewFlipper) findViewById(R.id.ViewFlipper_local_soft_manage);
        this.viewLocalManage = findViewById(R.id.RelativeLayout_local_soft);
        this.viewDownloadList = findViewById(R.id.RelativeLayout_download_list);
        this.viewUpdateList = findViewById(R.id.RelativeLayout_local_soft_update);
        this.ivLocalManage = (ImageView) findViewById(R.id.ImageView_local_soft);
        this.ivDownloadList = (ImageView) findViewById(R.id.ImageView_download_list);
        this.ivUpdateList = (ImageView) findViewById(R.id.ImageView_local_update);
        this.tvLocalManage = (TextView) findViewById(R.id.TextView_local_soft);
        this.tvDownloadList = (TextView) findViewById(R.id.TextView_download_list);
        this.tvUpdateList = (TextView) findViewById(R.id.TextView_local_update);
        this.viewLocalManage.setOnClickListener(this.tabOnClickListener);
        this.viewDownloadList.setOnClickListener(this.tabOnClickListener);
        this.viewUpdateList.setOnClickListener(this.tabOnClickListener);
        setSelectTabIndex(0);
    }

    private void initUI() {
        this.mLayoutInflater = (LayoutInflater) getApplicationContext().getSystemService("layout_inflater");
        initTopBarTab();
    }

    private void refreshDownloadList() {
        setSelectTabIndex(this.lastTabIndex);
        this.downloadListAdapter.notifyDataSetChanged();
    }

    private void registerApkChange() {
        IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
        intentFilter.addAction("android.intent.action.PACKAGE_CHANGED");
        intentFilter.addDataScheme("package");
        registerReceiver(this.mApplicationsReceiver, intentFilter);
        if (this.hasMiss) {
            this.managerListAdapter.a();
        }
        this.hasMiss = false;
    }

    public static void reset() {
        needClear = false;
    }

    private void sendReport() {
        new c(this).start();
    }

    /* access modifiers changed from: private */
    public void setSelectTabIndex(int i) {
        this.defaultUpdateTitle = getString(R.string.local_soft_manager_update);
        if (!(this.updateListAdapter == null || this.updateListAdapter.b() == 0)) {
            this.defaultUpdateTitle += "(" + this.updateListAdapter.b() + ")";
        }
        this.defaultDowloadListTitle = getString(R.string.local_soft_manager_download);
        if (!(this.downloadListAdapter == null || this.downloadListAdapter.c() == 0)) {
            this.defaultDowloadListTitle += "(" + this.downloadListAdapter.c() + ")";
        }
        this.ivLocalManage.setVisibility(4);
        this.ivDownloadList.setVisibility(4);
        this.ivUpdateList.setVisibility(4);
        this.tvLocalManage.setTextColor(getResources().getColor(R.color.local_soft_text_not_select));
        this.tvDownloadList.setTextColor(getResources().getColor(R.color.local_soft_text_not_select));
        this.tvUpdateList.setTextColor(getResources().getColor(R.color.local_soft_text_not_select));
        this.tvLocalManage.setTextSize(getResources().getDimension(R.dimen.home_tab_nor_size) / b.b);
        this.tvDownloadList.setTextSize(getResources().getDimension(R.dimen.home_tab_nor_size) / b.b);
        this.tvUpdateList.setTextSize(getResources().getDimension(R.dimen.home_tab_nor_size) / b.b);
        this.tvDownloadList.setText(this.defaultDowloadListTitle);
        this.tvUpdateList.setText(this.defaultUpdateTitle);
        switch (i) {
            case 0:
                this.tvLocalManage.setTextColor(getResources().getColor(R.color.text_shadow));
                this.tvLocalManage.setTextSize(getResources().getDimension(R.dimen.home_tab_text_size) / b.b);
                this.ivLocalManage.setVisibility(0);
                if (this.managerListAdapter != null) {
                    this.managerListAdapter.notifyDataSetChanged();
                    break;
                }
                break;
            case 1:
                this.ivUpdateList.setVisibility(0);
                this.tvUpdateList.setTextColor(getResources().getColor(R.color.text_shadow));
                this.tvUpdateList.setTextSize(getResources().getDimension(R.dimen.home_tab_text_size) / b.b);
                if (this.updateListAdapter != null) {
                    this.updateListAdapter.notifyDataSetChanged();
                    break;
                }
                break;
            case 2:
                this.ivDownloadList.setVisibility(0);
                this.tvDownloadList.setTextColor(getResources().getColor(R.color.text_shadow));
                this.tvDownloadList.setTextSize(getResources().getDimension(R.dimen.home_tab_text_size) / b.b);
                this.ivDownloadList.setVisibility(0);
                if (this.downloadListAdapter != null) {
                    for (int i2 = 0; i2 < this.downloadListAdapter.getGroupCount(); i2++) {
                        if (!this.downloadListView.isGroupExpanded(i2)) {
                            this.downloadListView.expandGroup(i2);
                        }
                    }
                    this.downloadListAdapter.notifyDataSetChanged();
                    break;
                }
                break;
        }
        this.viewFlipper.setDisplayedChild(i);
        this.lastTabIndex = i;
    }

    private void unRegisterApkChange() {
        unregisterReceiver(this.mApplicationsReceiver);
        this.hasMiss = true;
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        motionEvent.getAction();
        return super.dispatchTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        setContentView((int) R.layout.local_soft_manage);
        this.defaultDowloadListTitle = getString(R.string.local_soft_manager_download);
        this.defaultUpdateTitle = getString(R.string.local_soft_manager_update);
        this.mLayoutInflater = (LayoutInflater) getApplicationContext().getSystemService("layout_inflater");
        initTopBarTab();
        initAdapter();
        if (intent != null) {
            if (this.downloadListAdapter != null) {
                this.downloadListAdapter.b();
            }
            int intExtra = intent.getIntExtra("android.intent.extra.TITLE", 0);
            if (intExtra == 0 || intExtra == 1 || intExtra == 2) {
                setSelectTabIndex(intExtra);
            }
            if (intExtra == 2) {
                needClear = true;
            }
        }
        this.bOnResumeCauseByOnCreate = true;
        new c(this).start();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {
            if (this.downloadListAdapter != null) {
                this.downloadListAdapter.b();
            }
            int intExtra = intent.getIntExtra("android.intent.extra.TITLE", 0);
            if (intExtra == 0 || intExtra == 1 || intExtra == 2) {
                setSelectTabIndex(intExtra);
                if (this.downloadListAdapter != null) {
                    this.downloadListAdapter.a(true);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        unregisterReceiver(this.mApplicationsReceiver);
        this.hasMiss = true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        registerApkChange();
        this.managerListAdapter.notifyDataSetChanged();
        this.updateListAdapter.notifyDataSetChanged();
        setSelectTabIndex(this.lastTabIndex);
        this.downloadListAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.downloadListAdapter.d();
        this.downloadListAdapter.g();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.downloadListAdapter.e();
    }
}
