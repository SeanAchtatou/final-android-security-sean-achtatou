package com.tencent.android.ui.a;

import android.content.DialogInterface;
import android.os.RemoteException;
import com.tencent.android.ui.LocalSoftManageActivity;

final class h implements DialogInterface.OnClickListener {
    private /* synthetic */ Object a;
    private /* synthetic */ s b;

    h(s sVar, Object obj) {
        this.b = sVar;
        this.a = obj;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        if (this.b.a.p != null) {
            j jVar = (j) this.a;
            try {
                if (this.b.a.v != null) {
                    this.b.a.v.a(jVar.a.k, this.b.a.t);
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            u.a(this.b.a, jVar.a.l);
            this.b.a.o.sendEmptyMessage(LocalSoftManageActivity.MSG_download);
            if (!this.b.a.b.containsKey(jVar.a.l)) {
                jVar.c = 1;
                this.b.a.p.c(jVar.a.l, (j) this.a);
                return;
            }
            jVar.c = 0;
            jVar.d = ((j) this.b.a.b.get(jVar.a.l)).d;
            this.b.a.p.b(jVar.a.l, (j) this.a);
        }
    }
}
