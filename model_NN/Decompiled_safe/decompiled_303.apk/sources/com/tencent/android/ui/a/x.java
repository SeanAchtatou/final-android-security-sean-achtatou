package com.tencent.android.ui.a;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.android.b.a.b;
import com.tencent.android.ui.LocalSoftManageActivity;
import com.tencent.launcher.Launcher;
import com.tencent.launcher.a;
import com.tencent.launcher.am;
import com.tencent.launcher.bl;
import com.tencent.launcher.bq;
import com.tencent.launcher.ff;
import com.tencent.launcher.ha;
import com.tencent.qqlauncher.R;
import com.tencent.util.p;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Vector;

public final class x extends BaseExpandableListAdapter {
    private static int k = 0;
    private static int l = 1;
    public boolean a = true;
    /* access modifiers changed from: private */
    public LocalSoftManageActivity b = null;
    private Vector c = new Vector();
    private Vector d = new Vector();
    private String e = "";
    private Handler f = null;
    private ff g;
    private v h = null;
    private f i = null;
    private boolean j = false;
    private LayoutInflater m = null;
    private View.OnClickListener n = new y(this);
    private View.OnClickListener o = new z(this);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.ui.a.x.a(java.util.Vector, boolean):void
     arg types: [java.util.Vector, int]
     candidates:
      com.tencent.android.ui.a.x.a(android.content.Context, com.tencent.launcher.am):boolean
      com.tencent.android.ui.a.x.a(java.util.Vector, boolean):void */
    public x(LocalSoftManageActivity localSoftManageActivity, Handler handler, v vVar, f fVar) {
        this.b = localSoftManageActivity;
        this.h = vVar;
        this.i = fVar;
        this.f = handler;
        this.g = Launcher.getModel();
        bl e2 = this.g.e();
        this.e = localSoftManageActivity.getString(R.string.local_soft_installed_version);
        if (e2 == null) {
            localSoftManageActivity.finish();
            return;
        }
        this.c = a(e2);
        if (this.c == null) {
            localSoftManageActivity.finish();
            return;
        }
        a(this.c, true);
        this.h.a(this.c);
        d();
        Launcher launcher = Launcher.getLauncher();
        if (launcher != null) {
            launcher.clearStatusBarTapScrollTopViews();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.a.a(android.content.Context, android.graphics.drawable.Drawable, com.tencent.launcher.ha, boolean):android.graphics.drawable.Drawable
     arg types: [com.tencent.android.ui.LocalSoftManageActivity, android.graphics.drawable.Drawable, com.tencent.launcher.am, int]
     candidates:
      com.tencent.launcher.a.a(android.graphics.Bitmap, int, int, android.content.Context):android.graphics.Bitmap
      com.tencent.launcher.a.a(android.content.Context, android.graphics.drawable.Drawable, android.graphics.drawable.Drawable, com.tencent.launcher.ha):android.graphics.drawable.Drawable
      com.tencent.launcher.a.a(android.content.Context, android.graphics.drawable.Drawable, com.tencent.launcher.ha, boolean):android.graphics.drawable.Drawable */
    private b a(am amVar) {
        b bVar = new b();
        bVar.e = a.a((Context) this.b, amVar.d, (ha) amVar, true);
        bVar.d = amVar.a.toString();
        bVar.a = amVar.c.getComponent().getPackageName();
        return bVar;
    }

    private Vector a(bl blVar) {
        PackageInfo packageInfo;
        if (blVar == null) {
            return null;
        }
        int count = blVar.getCount();
        Vector vector = new Vector();
        vector.clear();
        for (int i2 = 0; i2 < count; i2++) {
            ha haVar = (ha) blVar.getItem(i2);
            if (haVar instanceof am) {
                if (!a(this.b, (am) haVar)) {
                    b a2 = a((am) haVar);
                    if (a2.a != null) {
                        vector.add(a2);
                    }
                } else if (!this.j) {
                    ((am) haVar).c.getComponent();
                    String packageName = ((am) haVar).c.getComponent().getPackageName();
                    b a3 = a((am) haVar);
                    try {
                        this.b.getPackageManager().getApplicationInfo(packageName, 8192);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    try {
                        packageInfo = this.b.getPackageManager().getPackageInfo(a3.a, 0);
                    } catch (Exception e3) {
                        e3.printStackTrace();
                        packageInfo = null;
                    }
                    if (packageInfo != null) {
                        a3.b = packageInfo.versionCode;
                        a3.c = packageInfo.versionName;
                    }
                    if (a3.a != null) {
                        this.d.add(a3);
                    }
                }
            } else if (haVar instanceof bq) {
                Iterator it = ((bq) haVar).b.iterator();
                while (it.hasNext()) {
                    am amVar = (am) it.next();
                    if (!a(this.b, amVar)) {
                        vector.add(a(amVar));
                    }
                }
            }
        }
        if (!this.j) {
            c();
            this.j = true;
        }
        return vector;
    }

    private void a(Vector vector, boolean z) {
        PackageInfo packageInfo;
        int i2 = 0;
        if (vector != null) {
            while (true) {
                int i3 = i2;
                if (i3 >= vector.size()) {
                    break;
                }
                b bVar = (b) vector.get(i3);
                if (bVar != null) {
                    try {
                        packageInfo = this.b.getPackageManager().getPackageInfo(bVar.a, 0);
                    } catch (PackageManager.NameNotFoundException e2) {
                        e2.printStackTrace();
                        packageInfo = null;
                    }
                    if (packageInfo != null) {
                        bVar.b = packageInfo.versionCode;
                        bVar.c = packageInfo.versionName;
                    }
                }
                i2 = i3 + 1;
            }
            if (z) {
                d();
                if (this.f != null) {
                    this.f.sendEmptyMessage(1503);
                }
            }
        }
    }

    private static boolean a(Context context, am amVar) {
        ApplicationInfo applicationInfo;
        if (amVar.c.getComponent() == null) {
            return true;
        }
        try {
            applicationInfo = context.getPackageManager().getApplicationInfo(amVar.c.getComponent().getPackageName(), 8192);
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            applicationInfo = null;
        }
        return (applicationInfo.flags & 1) != 0;
    }

    static /* synthetic */ Intent b() {
        String str = null;
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.LAUNCHER");
        intent.setComponent(new ComponentName(str.substring(0, str.lastIndexOf(".") - 1), str.substring(str.lastIndexOf(".") + 1)));
        intent.setFlags(270532608);
        return intent;
    }

    private void c() {
        b[] bVarArr = new b[this.d.size()];
        for (int i2 = 0; i2 < this.d.size(); i2++) {
            bVarArr[i2] = (b) this.d.get(i2);
        }
        Arrays.sort(bVarArr, 0, this.d.size(), new ag());
        this.d.clear();
        for (b add : bVarArr) {
            this.d.add(add);
        }
        super.notifyDataSetChanged();
    }

    private void d() {
        if (this.c != null) {
            b[] bVarArr = new b[this.c.size()];
            for (int i2 = 0; i2 < this.c.size(); i2++) {
                bVarArr[i2] = (b) this.c.get(i2);
            }
            Arrays.sort(bVarArr, 0, this.c.size(), new ag());
            this.c.clear();
            for (b add : bVarArr) {
                this.c.add(add);
            }
            super.notifyDataSetChanged();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.ui.a.x.a(java.util.Vector, boolean):void
     arg types: [java.util.Vector, int]
     candidates:
      com.tencent.android.ui.a.x.a(android.content.Context, com.tencent.launcher.am):boolean
      com.tencent.android.ui.a.x.a(java.util.Vector, boolean):void */
    public final void a() {
        boolean z;
        boolean z2;
        bl e2 = this.g.e();
        if (e2 == null) {
            this.b.finish();
            return;
        }
        Vector a2 = a(e2);
        a(a2, false);
        if (a2 != null) {
            if (this.c.size() == a2.size()) {
                int i2 = 0;
                while (true) {
                    if (i2 >= a2.size()) {
                        z = true;
                        break;
                    }
                    b bVar = (b) a2.elementAt(i2);
                    int i3 = 0;
                    while (true) {
                        if (i3 >= this.c.size()) {
                            z2 = false;
                            break;
                        }
                        b bVar2 = (b) this.c.elementAt(i3);
                        if (bVar.a != null && bVar2.c != null && bVar2.a.equalsIgnoreCase(bVar.a) && bVar2.c.equalsIgnoreCase(bVar.c)) {
                            z2 = true;
                            break;
                        }
                        i3++;
                    }
                    if (!z2) {
                        z = false;
                        break;
                    }
                    i2++;
                }
            } else {
                z = false;
            }
        } else {
            z = false;
        }
        if (!z) {
            this.c.clear();
            this.c = a2;
            if (this.c == null) {
                this.b.finish();
                return;
            }
            a(this.c, true);
            this.h.a(this.c);
            this.i.a();
            this.i.a(this.c);
            d();
            this.f.sendEmptyMessage(1501);
        }
        if (this.c == null) {
            this.b.finish();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.a.a(android.content.Context, android.graphics.drawable.Drawable, com.tencent.launcher.ha, boolean):android.graphics.drawable.Drawable
     arg types: [com.tencent.android.ui.LocalSoftManageActivity, android.graphics.drawable.Drawable, ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.launcher.a.a(android.graphics.Bitmap, int, int, android.content.Context):android.graphics.Bitmap
      com.tencent.launcher.a.a(android.content.Context, android.graphics.drawable.Drawable, android.graphics.drawable.Drawable, com.tencent.launcher.ha):android.graphics.drawable.Drawable
      com.tencent.launcher.a.a(android.content.Context, android.graphics.drawable.Drawable, com.tencent.launcher.ha, boolean):android.graphics.drawable.Drawable */
    public final void a(String str) {
        PackageInfo packageInfo;
        b bVar = new b();
        PackageManager packageManager = this.b.getPackageManager();
        try {
            bVar.e = a.a((Context) this.b, packageManager.getApplicationIcon(str), (ha) null, true);
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
        }
        bVar.a = str;
        try {
            bVar.d = packageManager.getApplicationLabel(packageManager.getApplicationInfo(str, 0)).toString();
        } catch (PackageManager.NameNotFoundException e3) {
            e3.printStackTrace();
        }
        try {
            packageInfo = packageManager.getPackageInfo(str, 0);
        } catch (PackageManager.NameNotFoundException e4) {
            e4.printStackTrace();
            packageInfo = null;
        }
        if (packageInfo != null) {
            bVar.c = packageInfo.versionName;
            bVar.b = packageInfo.versionCode;
        }
        this.c.add(bVar);
        this.h.a(bVar);
        d();
        this.f.sendEmptyMessage(1501);
    }

    public final boolean a(View view) {
        Object tag = view.getTag();
        if (tag == null || !(tag instanceof ab)) {
            return false;
        }
        ab abVar = (ab) tag;
        if (!(abVar.e instanceof b)) {
            return false;
        }
        this.b.startActivity(p.a(((b) abVar.e).a));
        return true;
    }

    public final void b(String str) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.c.size()) {
                return;
            }
            if (((b) this.c.get(i3)).a.equalsIgnoreCase(str)) {
                this.c.remove(i3);
                this.f.sendEmptyMessage(1501);
                return;
            }
            i2 = i3 + 1;
        }
    }

    /* access modifiers changed from: protected */
    public final void finalize() {
        super.finalize();
    }

    public final Object getChild(int i2, int i3) {
        return null;
    }

    public final long getChildId(int i2, int i3) {
        return 0;
    }

    public final View getChildView(int i2, int i3, boolean z, View view, ViewGroup viewGroup) {
        View view2;
        View inflate;
        ab abVar;
        View inflate2;
        ab abVar2;
        if (i2 == k) {
            if (view == null || view.getId() != 10) {
                inflate2 = LayoutInflater.from(this.b).inflate((int) R.layout.local_soft_local_manage_item, (ViewGroup) null);
                inflate2.setId(10);
                abVar2 = new ab(this);
                abVar2.a = (ImageView) inflate2.findViewById(R.id.software_icon);
                abVar2.b = (TextView) inflate2.findViewById(R.id.software_item_name);
                abVar2.c = (TextView) inflate2.findViewById(R.id.TextView_versionName);
                abVar2.d = (TextView) inflate2.findViewById(R.id.update_button);
                inflate2.setTag(abVar2);
            } else {
                inflate2 = view;
                abVar2 = (ab) view.getTag();
            }
            if (this.c == null || this.c.size() == 0 || i3 >= this.c.size()) {
                return inflate2;
            }
            b bVar = (b) this.c.get(i3);
            if (bVar.e != null) {
                abVar2.a.setImageDrawable(bVar.e);
            } else {
                abVar2.a.setImageResource(R.drawable.sw_default_icon);
            }
            abVar2.e = bVar;
            abVar2.b.setText(bVar.d);
            abVar2.c.setText(this.e + bVar.c);
            abVar2.f = "";
            abVar2.d.setText((int) R.string.menu_uninstall);
            abVar2.d.setVisibility(0);
            abVar2.d.setTag(bVar);
            abVar2.d.setOnClickListener(this.n);
            view2 = inflate2;
        } else if (i2 == l) {
            if (view == null || view.getId() != 20) {
                inflate = LayoutInflater.from(this.b).inflate((int) R.layout.local_soft_local_manage_item, (ViewGroup) null);
                inflate.setId(20);
                abVar = new ab(this);
                abVar.a = (ImageView) inflate.findViewById(R.id.software_icon);
                abVar.b = (TextView) inflate.findViewById(R.id.software_item_name);
                abVar.c = (TextView) inflate.findViewById(R.id.TextView_versionName);
                abVar.d = (TextView) inflate.findViewById(R.id.update_button);
                abVar.d.setVisibility(8);
                inflate.setTag(abVar);
            } else {
                inflate = view;
                abVar = (ab) view.getTag();
            }
            abVar.d.setVisibility(8);
            if (this.d == null || this.d.size() == 0 || i3 >= this.d.size()) {
                return inflate;
            }
            b bVar2 = (b) this.d.get(i3);
            abVar.e = bVar2;
            if (bVar2.e != null) {
                abVar.a.setImageDrawable(bVar2.e);
            } else {
                abVar.a.setImageResource(R.drawable.sw_default_icon);
            }
            abVar.b.setText(bVar2.d);
            abVar.c.setText(this.e + bVar2.c);
            view2 = inflate;
        } else {
            view2 = null;
        }
        view2.setOnClickListener(this.o);
        return view2;
    }

    public final int getChildrenCount(int i2) {
        if (i2 == k) {
            if (this.c != null) {
                return this.c.size();
            }
            return 0;
        } else if (i2 != l || this.d == null) {
            return 0;
        } else {
            return this.d.size();
        }
    }

    public final Object getGroup(int i2) {
        return null;
    }

    public final int getGroupCount() {
        boolean z = this.c != null && this.c.size() > 0;
        boolean z2 = this.d != null && this.d.size() > 0;
        if (z && z2) {
            l = 1;
            k = 0;
            return 2;
        } else if (z) {
            k = 0;
            l = -1;
            return 1;
        } else if (z2) {
            k = -1;
            l = 0;
            return 1;
        } else {
            k = -1;
            l = -1;
            return 0;
        }
    }

    public final long getGroupId(int i2) {
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getGroupView(int i2, boolean z, View view, ViewGroup viewGroup) {
        View view2;
        if (view == null) {
            if (this.m == null) {
                this.m = (LayoutInflater) this.b.getSystemService("layout_inflater");
            }
            view2 = this.m.inflate((int) R.layout.local_manager_group, viewGroup, false);
        } else {
            view2 = view;
        }
        TextView textView = (TextView) view2.findViewById(R.id.LocalGroundName);
        if (i2 == k) {
            textView.setText("我的应用");
        } else if (i2 == l) {
            textView.setText("系统应用");
        }
        return view2;
    }

    public final boolean hasStableIds() {
        return true;
    }

    public final boolean isChildSelectable(int i2, int i3) {
        return false;
    }

    public final void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
