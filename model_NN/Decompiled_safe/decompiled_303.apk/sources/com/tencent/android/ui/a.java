package com.tencent.android.ui;

import android.os.Handler;
import android.os.Message;

final class a extends Handler {
    private /* synthetic */ LocalSoftManageActivity a;

    a(LocalSoftManageActivity localSoftManageActivity) {
        this.a = localSoftManageActivity;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        if (this.a.managerListAdapter != null) {
            for (int i = 0; i < this.a.managerListAdapter.getGroupCount(); i++) {
                this.a.manageListView.expandGroup(i);
            }
        }
    }
}
