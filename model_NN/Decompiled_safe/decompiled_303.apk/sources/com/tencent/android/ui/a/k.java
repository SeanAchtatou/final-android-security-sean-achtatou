package com.tencent.android.ui.a;

import java.util.Comparator;

public final class k implements Comparator {
    public final int compare(Object obj, Object obj2) {
        if (!(obj instanceof j) || !(obj2 instanceof j)) {
            return 0;
        }
        return (int) (((j) obj2).e - ((j) obj).e);
    }
}
