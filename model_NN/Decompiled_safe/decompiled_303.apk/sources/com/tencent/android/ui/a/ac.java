package com.tencent.android.ui.a;

import android.content.DialogInterface;
import com.tencent.android.a.b;
import com.tencent.android.ui.LocalSoftManageActivity;

final class ac implements DialogInterface.OnClickListener {
    private /* synthetic */ Object a;
    private /* synthetic */ r b;

    ac(r rVar, Object obj) {
        this.b = rVar;
        this.a = obj;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.ui.a.i.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.android.ui.a.i.a(java.lang.String, com.tencent.android.ui.a.j):void
      com.tencent.android.ui.a.i.a(java.lang.String, boolean):void */
    public final void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        j jVar = (j) this.a;
        b.a(jVar.d);
        this.b.a.a.remove(jVar);
        this.b.a.b.remove(jVar.a.l);
        if (this.b.a.p != null) {
            this.b.a.p.a(jVar.a.l, false);
        }
        this.b.a.o.sendEmptyMessage(LocalSoftManageActivity.MSG_download);
    }
}
