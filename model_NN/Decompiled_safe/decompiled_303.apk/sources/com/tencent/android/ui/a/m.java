package com.tencent.android.ui.a;

import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.widget.Toast;
import com.tencent.android.ui.LocalSoftManageActivity;
import java.util.List;

final class m extends Handler {
    private /* synthetic */ u a;

    m(u uVar) {
        this.a = uVar;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case LocalSoftManageActivity.MSG_downloadFail:
                String str = (String) message.obj;
                int i = message.arg1;
                int i2 = message.arg2;
                j jVar = (j) this.a.c.get(str);
                if (jVar != null) {
                    jVar.g = (long) i;
                    jVar.h = (long) i2;
                }
                u.a(this.a, str, i, i2);
                return;
            case LocalSoftManageActivity.MSG_getListFail:
                a aVar = (a) message.obj;
                u.a(this.a, aVar.a, aVar.b);
                this.a.notifyDataSetChanged();
                return;
            case LocalSoftManageActivity.MSG_settab:
                this.a.d = (List) message.obj;
                this.a.j();
                this.a.notifyDataSetChanged();
                return;
            case LocalSoftManageActivity.MSG_updatelist:
                Toast.makeText(this.a.h, "网络异常", 0).show();
                return;
            case LocalSoftManageActivity.MSG_downloadlist:
                try {
                    if (LocalSoftManageActivity.getNeed()) {
                        this.a.v.c();
                    }
                    LocalSoftManageActivity.reset();
                    return;
                } catch (RemoteException e) {
                    e.printStackTrace();
                    return;
                }
            default:
                return;
        }
    }
}
