package com.tencent.android.ui.a;

import android.os.Message;
import com.tencent.android.ui.LocalSoftManageActivity;
import com.tencent.module.download.b;

final class p extends b {
    private /* synthetic */ u a;

    p(u uVar) {
        this.a = uVar;
    }

    public final void a(String str) {
    }

    public final void a(String str, int i, int i2) {
        this.a.f.sendMessage(Message.obtain(this.a.f, LocalSoftManageActivity.MSG_downloadFail, i, i2, str));
    }

    public final void a(String str, int i, String str2) {
        Message.obtain(this.a.f, (int) LocalSoftManageActivity.MSG_updatelist).sendToTarget();
    }

    public final void a(String str, String str2) {
        a aVar = new a(this.a);
        aVar.a = str;
        aVar.b = str2;
        Message.obtain(this.a.f, LocalSoftManageActivity.MSG_getListFail, 0, 0, aVar).sendToTarget();
    }
}
