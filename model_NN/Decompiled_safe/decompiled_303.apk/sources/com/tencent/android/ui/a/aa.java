package com.tencent.android.ui.a;

import java.util.Comparator;

public final class aa implements Comparator {
    public final int compare(Object obj, Object obj2) {
        if ((obj instanceof j) && (obj2 instanceof j)) {
            j jVar = (j) obj;
            j jVar2 = (j) obj2;
            if (!(jVar.a == null || jVar.a.b == null || jVar2.a == null || jVar2.a.b == null)) {
                String str = jVar.a.b;
                String str2 = jVar2.a.b;
                if (str.length() == str2.length()) {
                    for (int i = 0; i < str.length(); i++) {
                        if (str.charAt(i) != str2.charAt(i)) {
                            return str.charAt(i) - str2.charAt(i);
                        }
                    }
                    return 0;
                }
                int length = str.length() > str2.length() ? str2.length() : str.length();
                for (int i2 = 0; i2 < length; i2++) {
                    if (str.charAt(i2) != str2.charAt(i2)) {
                        return str.charAt(i2) - str2.charAt(i2);
                    }
                }
                return length == str.length() ? -1 : 1;
            }
        }
        return 0;
    }
}
