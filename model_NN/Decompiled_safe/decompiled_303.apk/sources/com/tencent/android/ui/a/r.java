package com.tencent.android.ui.a;

import android.os.Environment;
import android.view.View;
import com.tencent.launcher.base.BaseApp;
import com.tencent.module.setting.e;
import com.tencent.qqlauncher.R;

final class r implements View.OnClickListener {
    final /* synthetic */ u a;

    r(u uVar) {
        this.a = uVar;
    }

    public final void onClick(View view) {
        if (!Environment.getExternalStorageState().equals("mounted")) {
            BaseApp.a((int) R.string.sdcard_remove_install);
            return;
        }
        Object tag = view.getTag();
        if (tag != null && (tag instanceof j)) {
            e eVar = new e(this.a.h);
            eVar.a((int) R.string.local_soft_manager_download_deleteAPK);
            eVar.b(this.a.h.getString(R.string.local_soft_manager_download_delete) + " " + ((j) tag).a.b + " " + this.a.h.getString(R.string.local_soft_manager_download_file));
            eVar.a((int) R.string.confirm, new ac(this, tag));
            eVar.b((int) R.string.cancel, new ad(this));
            eVar.c().show();
        }
    }
}
