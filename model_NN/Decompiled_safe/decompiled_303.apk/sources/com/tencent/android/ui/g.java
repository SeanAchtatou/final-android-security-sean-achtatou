package com.tencent.android.ui;

import android.widget.ExpandableListView;

final class g implements ExpandableListView.OnGroupCollapseListener {
    private /* synthetic */ LocalSoftManageActivity a;

    g(LocalSoftManageActivity localSoftManageActivity) {
        this.a = localSoftManageActivity;
    }

    public final void onGroupCollapse(int i) {
        this.a.downloadListView.expandGroup(i);
    }
}
