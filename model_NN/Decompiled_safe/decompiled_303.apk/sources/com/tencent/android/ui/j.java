package com.tencent.android.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class j extends BroadcastReceiver {
    private /* synthetic */ LocalSoftManageActivity a;

    j(LocalSoftManageActivity localSoftManageActivity) {
        this.a = localSoftManageActivity;
    }

    public final void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        String schemeSpecificPart = intent.getData().getSchemeSpecificPart();
        boolean booleanExtra = intent.getBooleanExtra("android.intent.extra.REPLACING", false);
        if ("android.intent.action.PACKAGE_CHANGED".equals(action)) {
            return;
        }
        if ("android.intent.action.PACKAGE_REMOVED".equals(action)) {
            if (!booleanExtra) {
                this.a.managerListAdapter.b(schemeSpecificPart);
            }
        } else if (!booleanExtra) {
            this.a.managerListAdapter.a(schemeSpecificPart);
            this.a.managerListAdapter.a();
        }
    }
}
