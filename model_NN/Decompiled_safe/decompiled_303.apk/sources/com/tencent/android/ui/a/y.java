package com.tencent.android.ui.a;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import com.tencent.android.b.a.b;
import com.tencent.module.appcenter.g;

final class y implements View.OnClickListener {
    private /* synthetic */ x a;

    y(x xVar) {
        this.a = xVar;
    }

    public final void onClick(View view) {
        Object tag = view.getTag();
        if (tag != null && (tag instanceof b)) {
            b bVar = (b) tag;
            String str = bVar.a != null ? bVar.a : this.a.b.getPackageManager().resolveActivity(x.b(), 0).activityInfo.packageName;
            if (str != null) {
                this.a.b.startActivity(new Intent("android.intent.action.DELETE", Uri.parse("package:" + str)));
                g.a().a(AndroidDLoader.b.b);
            }
        }
    }
}
