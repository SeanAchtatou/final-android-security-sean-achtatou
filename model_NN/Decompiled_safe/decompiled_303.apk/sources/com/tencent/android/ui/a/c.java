package com.tencent.android.ui.a;

import AndroidDLoader.b;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import com.tencent.module.appcenter.g;

final class c implements View.OnClickListener {
    private /* synthetic */ af a;

    c(af afVar) {
        this.a = afVar;
    }

    public final void onClick(View view) {
        Object tag = view.getTag();
        if (tag != null && (tag instanceof ah)) {
            j jVar = (j) ((ah) tag).e;
            Intent intent = new Intent("android.intent.action.VIEW");
            Uri parse = Uri.parse("file://" + jVar.d);
            if (jVar != null) {
                intent.setDataAndType(parse, "application/vnd.android.package-archive");
                this.a.a.startActivity(intent);
                g.a().a(b.a);
            }
        }
    }
}
