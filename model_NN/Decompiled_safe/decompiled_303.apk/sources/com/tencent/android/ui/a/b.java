package com.tencent.android.ui.a;

import android.os.Environment;
import android.view.View;
import android.widget.TextView;
import com.tencent.android.ui.LocalSoftManageActivity;
import com.tencent.launcher.a;
import com.tencent.launcher.base.BaseApp;
import com.tencent.module.appcenter.g;
import com.tencent.qqlauncher.R;

final class b implements View.OnClickListener {
    private /* synthetic */ af a;

    b(af afVar) {
        this.a = afVar;
    }

    public final void onClick(View view) {
        Object tag = view.getTag();
        if (!Environment.getExternalStorageState().equals("mounted")) {
            BaseApp.a((int) R.string.sdcard_remove_can_not_download);
        } else if (tag != null && (tag instanceof ah)) {
            ah ahVar = (ah) tag;
            ((TextView) view).setText((int) R.string.local_soft_loading);
            j jVar = new j();
            if (view instanceof TextView) {
                ((TextView) view).setTextColor(-15773832);
            }
            jVar.a = ((j) ahVar.e).a;
            jVar.f = ((com.tencent.android.b.a.b) this.a.f.get(jVar.a.l)).e != null ? a.b(((com.tencent.android.b.a.b) this.a.f.get(jVar.a.l)).e) : null;
            jVar.d = Environment.getExternalStorageDirectory().getAbsolutePath();
            this.a.g.a(jVar);
            this.a.e.sendEmptyMessage(LocalSoftManageActivity.MSG_update);
            g.a().a(AndroidDLoader.b.c);
        }
    }
}
