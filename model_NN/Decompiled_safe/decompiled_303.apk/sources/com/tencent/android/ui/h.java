package com.tencent.android.ui;

import android.view.View;
import com.tencent.qqlauncher.R;

final class h implements View.OnClickListener {
    private /* synthetic */ LocalSoftManageActivity a;

    h(LocalSoftManageActivity localSoftManageActivity) {
        this.a = localSoftManageActivity;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.RelativeLayout_local_soft /*2131493086*/:
                this.a.setSelectTabIndex(0);
                return;
            case R.id.RelativeLayout_local_soft_update /*2131493089*/:
                this.a.setSelectTabIndex(1);
                return;
            case R.id.RelativeLayout_download_list /*2131493092*/:
                this.a.setSelectTabIndex(2);
                return;
            default:
                return;
        }
    }
}
