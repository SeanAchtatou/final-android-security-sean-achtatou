package com.tencent.android.ui.a;

import android.view.View;
import com.tencent.module.setting.e;
import com.tencent.qqlauncher.R;

final class s implements View.OnClickListener {
    final /* synthetic */ u a;

    s(u uVar) {
        this.a = uVar;
    }

    public final void onClick(View view) {
        Object tag = view.getTag();
        if (tag != null && (tag instanceof j)) {
            e eVar = new e(this.a.h);
            eVar.a((int) R.string.cancel_download);
            eVar.b(this.a.h.getString(R.string.cancel_download_tips) + " " + ((j) tag).a.b + " " + this.a.h.getString(R.string.cancel_download_down));
            eVar.a((int) R.string.confirm, new h(this, tag));
            eVar.b((int) R.string.cancel, new g(this));
            eVar.c().show();
        }
    }
}
