package com.tencent.android.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.widget.Toast;
import com.tencent.android.c.a;
import com.tencent.launcher.base.BaseApp;
import com.tencent.module.appcenter.g;
import com.tencent.module.setting.DesktopSwitchSpecialEffectSettingActivity;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public final class b {
    public static int a = 0;
    public static String b = "";
    public static String c;
    public static a d;
    public static int e = 20;
    private static final String f = Build.MODEL;
    private static String g = null;
    private static String[] h = null;

    public static String a() {
        if (g == null || g.length() <= 0) {
            g = "QQLAUNCHER_GA/110200&na_2/000000&ADR&" + (com.tencent.launcher.base.b.c / 16) + "" + (com.tencent.launcher.base.b.d / 16) + "" + "14" + "&" + f + "&V2";
        }
        return g;
    }

    public static String a(int i) {
        switch (i) {
            case 1:
                return "找不到服务器,请检查网络";
            case 2:
                return "连接服务器超时,请检查网络";
            case 3:
                return "读取网络错误,请检查网络";
            case 4:
                return "协议不支持";
            case 5:
                return "未知网络错误";
            case 6:
            case DesktopSwitchSpecialEffectSettingActivity.nRotationSetting /*7*/:
            case DesktopSwitchSpecialEffectSettingActivity.nCubeSetting /*8*/:
            case 9:
            default:
                return "未知错误";
            case 10:
                return "服务器数据异常";
        }
    }

    public static String a(Context context, String str) {
        if (!"mounted".equals(Environment.getExternalStorageState()) || !Environment.getExternalStorageDirectory().canWrite()) {
            String absolutePath = context.getFilesDir().getAbsolutePath();
            return !absolutePath.endsWith("/") ? absolutePath + "/" : absolutePath;
        }
        File file = new File(Environment.getExternalStorageDirectory().getPath() + str);
        if (!file.exists()) {
            file.mkdirs();
        }
        String absolutePath2 = file.getAbsolutePath();
        return !absolutePath2.endsWith("/") ? absolutePath2 + "/" : absolutePath2;
    }

    public static void a(ArrayList arrayList) {
        if (arrayList != null && arrayList.size() > 0) {
            h = new String[arrayList.size()];
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < arrayList.size()) {
                    h[i2] = (String) arrayList.get(i2);
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public static boolean a(String str) {
        File file = new File(str);
        if (!file.exists()) {
            return false;
        }
        try {
            Runtime.getRuntime().exec("chmod 777 " + file.getAbsolutePath());
            Runtime.getRuntime().exec("chmod 777 " + file.getParent());
            file.delete();
            return true;
        } catch (IOException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public static boolean a(String str, Activity activity) {
        File file = new File(str);
        if (!file.exists()) {
            Toast.makeText(BaseApp.b(), "安装包不存在", 0).show();
            return false;
        }
        g.a().a(AndroidDLoader.b.a);
        try {
            Runtime.getRuntime().exec("chmod 777 " + file.getAbsolutePath());
            Runtime.getRuntime().exec("chmod 777 " + file.getParent());
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        Intent intent = new Intent();
        intent.addFlags(268435456);
        intent.setAction("android.intent.action.VIEW");
        String name = file.getName();
        String lowerCase = name.substring(name.lastIndexOf(".") + 1, name.length()).toLowerCase();
        String str2 = (lowerCase.equals("m4a") || lowerCase.equals("mp3") || lowerCase.equals("mid") || lowerCase.equals("xmf") || lowerCase.equals("ogg") || lowerCase.equals("wav")) ? "audio" : (lowerCase.equals("3gp") || lowerCase.equals("mp4")) ? "video" : (lowerCase.equals("jpg") || lowerCase.equals("gif") || lowerCase.equals("png") || lowerCase.equals("jpeg") || lowerCase.equals("bmp")) ? "image" : lowerCase.equals("apk") ? "application/vnd.android.package-archive" : "*";
        intent.setDataAndType(Uri.fromFile(file), !lowerCase.equals("apk") ? str2 + "/*" : str2);
        activity.startActivityForResult(intent, 10086);
        return true;
    }
}
