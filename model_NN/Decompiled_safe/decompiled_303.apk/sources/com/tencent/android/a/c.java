package com.tencent.android.a;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import java.io.File;
import java.lang.reflect.Method;

public final class c {
    private Class a;
    private Class b;
    private Method c;
    private Method d;
    private Object e;
    private DisplayMetrics f;
    private Resources g;

    public c(Context context) {
        this.a = null;
        this.b = null;
        this.a = Class.forName("android.content.pm.PackageParser");
        this.c = this.a.getMethod("parsePackage", File.class, String.class, DisplayMetrics.class, Integer.TYPE);
        this.d = AssetManager.class.getMethod("addAssetPath", String.class);
        this.f = new DisplayMetrics();
        this.f.setToDefaults();
        this.g = context.getResources();
    }

    public final com.tencent.android.b.a.c a(String str) {
        this.e = this.a.getConstructor(String.class).newInstance(str);
        Object invoke = this.c.invoke(this.e, new File(str), str, this.f, 0);
        if (this.b == null) {
            this.b = invoke.getClass();
        }
        ApplicationInfo applicationInfo = (ApplicationInfo) this.b.getField("applicationInfo").get(invoke);
        AssetManager newInstance = AssetManager.class.newInstance();
        this.d.invoke(newInstance, str);
        Resources resources = new Resources(newInstance, this.g.getDisplayMetrics(), this.g.getConfiguration());
        com.tencent.android.b.a.c cVar = new com.tencent.android.b.a.c();
        cVar.b = (String) this.b.getField("packageName").get(invoke);
        cVar.d = ((Integer) this.b.getField("mVersionCode").get(invoke)).toString();
        cVar.c = (String) this.b.getField("mVersionName").get(invoke);
        CharSequence charSequence = null;
        if (applicationInfo.labelRes != 0) {
            try {
                charSequence = resources.getText(applicationInfo.labelRes);
            } catch (Resources.NotFoundException e2) {
            }
        }
        if (charSequence == null) {
            charSequence = applicationInfo.nonLocalizedLabel != null ? applicationInfo.nonLocalizedLabel : applicationInfo.packageName;
        }
        cVar.a = charSequence.toString();
        if (applicationInfo.icon != 0) {
            cVar.e = resources.getDrawable(applicationInfo.icon);
        }
        return cVar;
    }
}
