package com.tencent.android.a;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class a {
    private static DecimalFormat a = new DecimalFormat("##.00");
    private static final String[] b = {"GB", "MB", "KB", "B"};
    private static final long[] c = {1073741824, 1048576, 1024, 1};
    private static Date d = new Date();
    private static Date e = new Date();
    private static SimpleDateFormat f = new SimpleDateFormat("HH:mm");
    private static SimpleDateFormat g = new SimpleDateFormat("MM-dd HH:mm");
    private static SimpleDateFormat h = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    private a() {
    }

    public static String a(long j) {
        if (j < 1) {
            return "0B";
        }
        for (int i = 0; i < c.length; i++) {
            long j2 = c[i];
            if (j >= j2) {
                return a.format(j2 > 1 ? ((double) j) / ((double) j2) : (double) j) + " " + b[i];
            }
        }
        return null;
    }

    public static String b(long j) {
        Date date = new Date();
        date.setTime(j);
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    public static String c(long j) {
        long currentTimeMillis = System.currentTimeMillis();
        d.setTime(currentTimeMillis);
        e.setTime(j);
        long time = (currentTimeMillis - e.getTime()) / 1000;
        if (time >= 0) {
            if (time < 60) {
                return "刚刚";
            }
            if (time < 1800) {
                return (time / 60) + "分钟前";
            }
            int hours = d.getHours();
            long minutes = (((long) d.getMinutes()) * 60) + (((long) hours) * 3600) + ((long) d.getSeconds());
            if (time < minutes) {
                return "今天" + f.format(e);
            }
            if (time < 86400 + minutes) {
                return "昨天" + f.format(e);
            }
            if (time < minutes + 172800) {
                return "前天" + f.format(e);
            }
            if (d.getYear() == e.getYear()) {
                return g.format(e);
            }
        }
        return h.format(e);
    }
}
