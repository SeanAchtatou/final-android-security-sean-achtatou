package com.tencent.android.net.a;

final class c extends Thread {
    /* access modifiers changed from: private */
    public boolean a = true;
    private boolean b = false;
    private String c = "";
    private /* synthetic */ b d;

    c(b bVar) {
        this.d = bVar;
    }

    public final void run() {
        while (this.a) {
            synchronized (this.d.a) {
                try {
                    this.d.a.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (this.a) {
                e eVar = null;
                while (true) {
                    if (this.d.d.size() > 0) {
                        if (this.a) {
                            synchronized (this.d.b) {
                                if (this.d.d.size() > 0) {
                                    eVar = (e) this.d.d.get(0);
                                    this.d.d.remove(eVar);
                                }
                            }
                            if (eVar != null) {
                                synchronized (this.d.c) {
                                    this.d.e.add(eVar);
                                }
                                this.b = true;
                                this.c = eVar.c;
                                b.a(this.d, eVar);
                                this.b = false;
                                this.c = "";
                                synchronized (this.d.c) {
                                    this.d.e.remove(eVar);
                                }
                            }
                        } else {
                            return;
                        }
                    }
                }
                while (true) {
                }
            }
            return;
        }
    }
}
