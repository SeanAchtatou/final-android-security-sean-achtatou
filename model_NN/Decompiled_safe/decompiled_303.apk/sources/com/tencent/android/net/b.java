package com.tencent.android.net;

import AndroidDLoader.Category;
import AndroidDLoader.ReqAddLabelPack;
import AndroidDLoader.ReqCategoryPack;
import AndroidDLoader.ReqCheckUpdatePack;
import AndroidDLoader.ReqCommentPack;
import AndroidDLoader.ReqEverydayForYouPack;
import AndroidDLoader.ReqGetFlashScreenPack;
import AndroidDLoader.ReqGetLoadingTextPack;
import AndroidDLoader.ReqGetRelatedSoftwaresPack;
import AndroidDLoader.ReqGetScrollablePicAdvPack;
import AndroidDLoader.ReqGetSoftwaresLatestPack;
import AndroidDLoader.ReqGetSoftwaresOnTopPack;
import AndroidDLoader.ReqGetUserCommendsPack;
import AndroidDLoader.ReqIconBytesPack;
import AndroidDLoader.ReqReportStatDataPack;
import AndroidDLoader.ReqRequiredSoftwaresPack;
import AndroidDLoader.ReqSearchByLabelPack;
import AndroidDLoader.ReqSoftDetailPack;
import AndroidDLoader.ReqSoftwarePack;
import AndroidDLoader.ResAddLabelPack;
import AndroidDLoader.ResCategoryPack;
import AndroidDLoader.ResCheckUpdatePack;
import AndroidDLoader.ResEverydayForYouPack;
import AndroidDLoader.ResGetFlashScreenPack;
import AndroidDLoader.ResGetHotSoftwaresPack;
import AndroidDLoader.ResGetSoftwaresLatestPack;
import AndroidDLoader.ResGetSoftwaresOnTopPack;
import AndroidDLoader.ResHandshakePack;
import AndroidDLoader.ResIconBytesPack;
import AndroidDLoader.ResReportStatDataPack;
import AndroidDLoader.ResSearchByLabelPack;
import AndroidDLoader.ResSoftDetailPack;
import AndroidDLoader.ResSoftwareListPack;
import AndroidDLoader.ResSoftwarePack;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import com.qq.jce.wup.UniPacket;
import com.tencent.android.b.a.d;
import com.tencent.module.setting.AboutSettingActivity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class b {
    private static b a = null;
    private static int c = 1;
    private d b = null;

    private b(d dVar) {
        this.b = dVar;
    }

    protected static b a(d dVar) {
        if (a == null) {
            a = new b(dVar);
        }
        return a;
    }

    public static void a() {
        a = null;
    }

    public static byte[] a(int i) {
        UniPacket uniPacket = new UniPacket();
        int i2 = c;
        c = i2 + 1;
        uniPacket.setRequestId(i2);
        uniPacket.setEncodeName("UTF-8");
        uniPacket.setServantName("dloader");
        uniPacket.setFuncName("getSoftwaresLatest");
        ReqGetSoftwaresLatestPack reqGetSoftwaresLatestPack = new ReqGetSoftwaresLatestPack();
        reqGetSoftwaresLatestPack.a(com.tencent.android.a.b.a());
        reqGetSoftwaresLatestPack.a(com.tencent.android.a.b.a);
        reqGetSoftwaresLatestPack.a(0L);
        reqGetSoftwaresLatestPack.b(i);
        reqGetSoftwaresLatestPack.a();
        uniPacket.put("req", reqGetSoftwaresLatestPack);
        return uniPacket.encode();
    }

    public static byte[] a(int i, byte b2) {
        UniPacket uniPacket = new UniPacket();
        int i2 = c;
        c = i2 + 1;
        uniPacket.setRequestId(i2);
        uniPacket.setEncodeName("UTF-8");
        uniPacket.setServantName("dloader");
        uniPacket.setFuncName("getSoftwaresOnTop");
        ReqGetSoftwaresOnTopPack reqGetSoftwaresOnTopPack = new ReqGetSoftwaresOnTopPack();
        reqGetSoftwaresOnTopPack.a(com.tencent.android.a.b.a());
        reqGetSoftwaresOnTopPack.a(com.tencent.android.a.b.a);
        reqGetSoftwaresOnTopPack.a(0L);
        reqGetSoftwaresOnTopPack.b(i);
        reqGetSoftwaresOnTopPack.a();
        reqGetSoftwaresOnTopPack.a(b2);
        uniPacket.put("req", reqGetSoftwaresOnTopPack);
        return uniPacket.encode();
    }

    public static byte[] a(int i, int i2) {
        UniPacket uniPacket = new UniPacket();
        int i3 = c;
        c = i3 + 1;
        uniPacket.setRequestId(i3);
        uniPacket.setEncodeName("UTF-8");
        uniPacket.setServantName("dloader");
        uniPacket.setFuncName("searchByLabel");
        ReqSearchByLabelPack reqSearchByLabelPack = new ReqSearchByLabelPack();
        reqSearchByLabelPack.a(com.tencent.android.a.b.a());
        reqSearchByLabelPack.a(com.tencent.android.a.b.a);
        reqSearchByLabelPack.b(com.tencent.android.a.b.b);
        reqSearchByLabelPack.a(0L);
        reqSearchByLabelPack.b(i);
        reqSearchByLabelPack.a();
        reqSearchByLabelPack.c(i2);
        reqSearchByLabelPack.b();
        uniPacket.put("req", reqSearchByLabelPack);
        return uniPacket.encode();
    }

    protected static byte[] a(int i, int i2, int i3) {
        UniPacket uniPacket = new UniPacket();
        int i4 = c;
        c = i4 + 1;
        uniPacket.setRequestId(i4);
        uniPacket.setEncodeName("UTF-8");
        uniPacket.setServantName("dloader");
        uniPacket.setFuncName("getCommentPageByProduct");
        ReqCommentPack reqCommentPack = new ReqCommentPack();
        reqCommentPack.a(com.tencent.android.a.b.a());
        reqCommentPack.a(com.tencent.android.a.b.a);
        reqCommentPack.b(i);
        reqCommentPack.c(i2);
        reqCommentPack.a();
        reqCommentPack.b();
        reqCommentPack.d(i3);
        uniPacket.put("req", reqCommentPack);
        return uniPacket.encode();
    }

    public static byte[] a(int i, int i2, int i3, int i4) {
        UniPacket uniPacket = new UniPacket();
        int i5 = c;
        c = i5 + 1;
        uniPacket.setRequestId(i5);
        uniPacket.setEncodeName("UTF-8");
        uniPacket.setServantName("dloader");
        uniPacket.setFuncName("getRelatedSoftwares");
        ReqGetRelatedSoftwaresPack reqGetRelatedSoftwaresPack = new ReqGetRelatedSoftwaresPack();
        reqGetRelatedSoftwaresPack.a(com.tencent.android.a.b.a());
        reqGetRelatedSoftwaresPack.a(com.tencent.android.a.b.a);
        reqGetRelatedSoftwaresPack.a(0L);
        reqGetRelatedSoftwaresPack.b(i);
        reqGetRelatedSoftwaresPack.a();
        reqGetRelatedSoftwaresPack.c(i2);
        reqGetRelatedSoftwaresPack.d(i3);
        reqGetRelatedSoftwaresPack.e(i4);
        uniPacket.put("req", reqGetRelatedSoftwaresPack);
        return uniPacket.encode();
    }

    public static byte[] a(String str, int i) {
        UniPacket uniPacket = new UniPacket();
        uniPacket.setEncodeName("UTF-8");
        int i2 = c;
        c = i2 + 1;
        uniPacket.setRequestId(i2);
        uniPacket.setServantName("dloader");
        uniPacket.setFuncName("getSoftPageByRank");
        ReqSoftwarePack reqSoftwarePack = new ReqSoftwarePack();
        reqSoftwarePack.a(com.tencent.android.a.b.a());
        reqSoftwarePack.a(com.tencent.android.a.b.a);
        reqSoftwarePack.b(str);
        reqSoftwarePack.b(com.tencent.android.a.b.e);
        reqSoftwarePack.c(i);
        reqSoftwarePack.a();
        reqSoftwarePack.b();
        uniPacket.put("req", reqSoftwarePack);
        return uniPacket.encode();
    }

    public static byte[] a(String str, int i, int i2) {
        UniPacket uniPacket = new UniPacket();
        int i3 = c;
        c = i3 + 1;
        uniPacket.setRequestId(i3);
        uniPacket.setEncodeName("UTF-8");
        uniPacket.setServantName("dloader");
        uniPacket.setFuncName("addLabel");
        ReqAddLabelPack reqAddLabelPack = new ReqAddLabelPack();
        reqAddLabelPack.a(com.tencent.android.a.b.a());
        reqAddLabelPack.a(com.tencent.android.a.b.a);
        reqAddLabelPack.c(com.tencent.android.a.b.b);
        reqAddLabelPack.a(0L);
        reqAddLabelPack.b(str);
        reqAddLabelPack.b(i);
        reqAddLabelPack.c(i2);
        reqAddLabelPack.a();
        uniPacket.put("req", reqAddLabelPack);
        return uniPacket.encode();
    }

    public static byte[] a(ArrayList arrayList) {
        UniPacket uniPacket = new UniPacket();
        int i = c;
        c = i + 1;
        uniPacket.setRequestId(i);
        uniPacket.setServantName("dloader");
        uniPacket.setFuncName("checkUpdate");
        ReqCheckUpdatePack reqCheckUpdatePack = new ReqCheckUpdatePack();
        reqCheckUpdatePack.a(com.tencent.android.a.b.a());
        reqCheckUpdatePack.a(com.tencent.android.a.b.a);
        reqCheckUpdatePack.a(arrayList);
        uniPacket.put("req", reqCheckUpdatePack);
        return uniPacket.encode();
    }

    public static byte[] a(List list) {
        UniPacket uniPacket = new UniPacket();
        int i = c;
        c = i + 1;
        uniPacket.setRequestId(i);
        uniPacket.setServantName("dloader");
        uniPacket.setFuncName("getIconBytes");
        ReqIconBytesPack reqIconBytesPack = new ReqIconBytesPack();
        reqIconBytesPack.a(com.tencent.android.a.b.a());
        reqIconBytesPack.a(com.tencent.android.a.b.a);
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(new Integer(((Category) it.next()).a()));
        }
        reqIconBytesPack.a();
        reqIconBytesPack.a(arrayList);
        uniPacket.put("req", reqIconBytesPack);
        return uniPacket.encode();
    }

    public static byte[] a(Map map) {
        UniPacket uniPacket = new UniPacket();
        int i = c;
        c = i + 1;
        uniPacket.setRequestId(i);
        uniPacket.setEncodeName("UTF-8");
        uniPacket.setServantName("dloader");
        uniPacket.setFuncName("reportStatData");
        ReqReportStatDataPack reqReportStatDataPack = new ReqReportStatDataPack();
        reqReportStatDataPack.a(com.tencent.android.a.b.a());
        reqReportStatDataPack.a(com.tencent.android.a.b.a);
        reqReportStatDataPack.a(0L);
        reqReportStatDataPack.a(map);
        uniPacket.put("req", reqReportStatDataPack);
        return uniPacket.encode();
    }

    public static byte[] b() {
        UniPacket uniPacket = new UniPacket();
        int i = c;
        c = i + 1;
        uniPacket.setRequestId(i);
        uniPacket.setServantName("dloader");
        uniPacket.setFuncName("getCategorys");
        ReqCategoryPack reqCategoryPack = new ReqCategoryPack();
        reqCategoryPack.a(com.tencent.android.a.b.a());
        reqCategoryPack.a(com.tencent.android.a.b.a);
        HashMap hashMap = new HashMap();
        hashMap.clear();
        if (com.tencent.android.a.b.c != null) {
            hashMap.put((byte) 1, com.tencent.android.a.b.c);
        } else {
            com.tencent.android.a.b.c = "";
            hashMap.put((byte) 1, com.tencent.android.a.b.c);
        }
        reqCategoryPack.a(hashMap);
        reqCategoryPack.a();
        uniPacket.put("req", reqCategoryPack);
        return uniPacket.encode();
    }

    public static byte[] b(int i) {
        UniPacket uniPacket = new UniPacket();
        int i2 = c;
        c = i2 + 1;
        uniPacket.setRequestId(i2);
        uniPacket.setEncodeName("UTF-8");
        uniPacket.setServantName("dloader");
        uniPacket.setFuncName("everydayForYou");
        ReqEverydayForYouPack reqEverydayForYouPack = new ReqEverydayForYouPack();
        reqEverydayForYouPack.a(com.tencent.android.a.b.a());
        reqEverydayForYouPack.a(com.tencent.android.a.b.a);
        reqEverydayForYouPack.a(0L);
        reqEverydayForYouPack.b(i);
        reqEverydayForYouPack.a();
        uniPacket.put("req", reqEverydayForYouPack);
        return uniPacket.encode();
    }

    protected static byte[] b(int i, int i2, int i3) {
        UniPacket uniPacket = new UniPacket();
        int i4 = c;
        c = i4 + 1;
        uniPacket.setRequestId(i4);
        uniPacket.setEncodeName("UTF-8");
        uniPacket.setServantName("dloader");
        uniPacket.setFuncName("getSoftDetail");
        ReqSoftDetailPack reqSoftDetailPack = new ReqSoftDetailPack();
        reqSoftDetailPack.a(com.tencent.android.a.b.a());
        reqSoftDetailPack.a(com.tencent.android.a.b.a);
        reqSoftDetailPack.b(i);
        reqSoftDetailPack.c(i2);
        reqSoftDetailPack.d(i3);
        reqSoftDetailPack.a();
        uniPacket.put("req", reqSoftDetailPack);
        return uniPacket.encode();
    }

    public static byte[] c() {
        UniPacket uniPacket = new UniPacket();
        int i = c;
        c = i + 1;
        uniPacket.setRequestId(i);
        uniPacket.setEncodeName("UTF-8");
        uniPacket.setServantName("dloader");
        uniPacket.setFuncName("getScrollablePicAdv");
        ReqGetScrollablePicAdvPack reqGetScrollablePicAdvPack = new ReqGetScrollablePicAdvPack();
        reqGetScrollablePicAdvPack.a(com.tencent.android.a.b.a());
        reqGetScrollablePicAdvPack.a(com.tencent.android.a.b.a);
        reqGetScrollablePicAdvPack.a(0L);
        uniPacket.put("req", reqGetScrollablePicAdvPack);
        return uniPacket.encode();
    }

    public static byte[] c(int i) {
        UniPacket uniPacket = new UniPacket();
        int i2 = c;
        c = i2 + 1;
        uniPacket.setRequestId(i2);
        uniPacket.setEncodeName("UTF-8");
        uniPacket.setServantName("dloader");
        uniPacket.setFuncName("getRequiredSoftwares");
        ReqRequiredSoftwaresPack reqRequiredSoftwaresPack = new ReqRequiredSoftwaresPack();
        reqRequiredSoftwaresPack.a(com.tencent.android.a.b.a());
        reqRequiredSoftwaresPack.a(com.tencent.android.a.b.a);
        reqRequiredSoftwaresPack.b(i);
        reqRequiredSoftwaresPack.a();
        reqRequiredSoftwaresPack.b();
        uniPacket.put("req", reqRequiredSoftwaresPack);
        return uniPacket.encode();
    }

    public static byte[] d() {
        UniPacket uniPacket = new UniPacket();
        int i = c;
        c = i + 1;
        uniPacket.setRequestId(i);
        uniPacket.setEncodeName("UTF-8");
        uniPacket.setServantName("dloader");
        uniPacket.setFuncName("getFlashScreen");
        ReqGetFlashScreenPack reqGetFlashScreenPack = new ReqGetFlashScreenPack();
        reqGetFlashScreenPack.a(com.tencent.android.a.b.a());
        reqGetFlashScreenPack.a(com.tencent.android.a.b.a);
        uniPacket.put("req", reqGetFlashScreenPack);
        return uniPacket.encode();
    }

    public static byte[] e() {
        UniPacket uniPacket = new UniPacket();
        int i = c;
        c = i + 1;
        uniPacket.setRequestId(i);
        uniPacket.setEncodeName("UTF-8");
        uniPacket.setServantName("dloader");
        uniPacket.setFuncName("getLoadingText");
        ReqGetLoadingTextPack reqGetLoadingTextPack = new ReqGetLoadingTextPack();
        reqGetLoadingTextPack.a(com.tencent.android.a.b.a());
        reqGetLoadingTextPack.a(com.tencent.android.a.b.a);
        uniPacket.put("req", reqGetLoadingTextPack);
        return uniPacket.encode();
    }

    public static byte[] f() {
        UniPacket uniPacket = new UniPacket();
        int i = c;
        c = i + 1;
        uniPacket.setRequestId(i);
        uniPacket.setEncodeName("UTF-8");
        uniPacket.setServantName("dloader");
        uniPacket.setFuncName("getUserCommends");
        ReqGetUserCommendsPack reqGetUserCommendsPack = new ReqGetUserCommendsPack();
        reqGetUserCommendsPack.a(com.tencent.android.a.b.a());
        reqGetUserCommendsPack.a(com.tencent.android.a.b.a);
        reqGetUserCommendsPack.a(0L);
        uniPacket.put("req", reqGetUserCommendsPack);
        return uniPacket.encode();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.net.d.a(android.os.Handler, java.util.List):void
     arg types: [android.os.Handler, java.util.ArrayList]
     candidates:
      com.tencent.android.net.d.a(android.os.Handler, int):void
      com.tencent.android.net.d.a(android.os.Handler, AndroidDLoader.ResCategoryPack):void
      com.tencent.android.net.d.a(android.os.Handler, AndroidDLoader.ResSoftDetailPack):void
      com.tencent.android.net.d.a(android.os.Handler, java.util.ArrayList):void
      com.tencent.android.net.d.a(android.os.Handler, java.util.Map):void
      com.tencent.android.net.d.a(android.os.Handler, java.util.List):void */
    public final void a(int i, byte[] bArr, Handler handler, int i2) {
        if (bArr != null) {
            UniPacket uniPacket = new UniPacket();
            uniPacket.setEncodeName("UTF-8");
            uniPacket.decode(bArr);
            switch (i) {
                case 101:
                    ResHandshakePack resHandshakePack = (ResHandshakePack) uniPacket.get("res");
                    if (resHandshakePack.a() == 0) {
                        com.tencent.android.a.b.a = resHandshakePack.b();
                        d c2 = com.tencent.android.a.b.d.c();
                        c2.b(resHandshakePack.b());
                        if (resHandshakePack.a != null) {
                            this.b.a(handler, resHandshakePack.c(), resHandshakePack.d(), resHandshakePack.e(), resHandshakePack.f());
                        }
                        com.tencent.android.a.b.d.a(c2);
                        return;
                    }
                    this.b.a(handler, resHandshakePack.a());
                    return;
                case 102:
                    ResCategoryPack resCategoryPack = (ResCategoryPack) uniPacket.get("res");
                    if (resCategoryPack.a() == 0) {
                        System.out.println("m : " + resCategoryPack.b().toString());
                        if (resCategoryPack.c != null) {
                            String str = (String) resCategoryPack.a.get((byte) 1);
                            if (str != null && !str.trim().equals("")) {
                                com.tencent.android.a.b.c = str;
                            }
                            this.b.a(handler, (List) resCategoryPack.c);
                            this.b.a(handler, resCategoryPack);
                            return;
                        }
                        return;
                    }
                    this.b.c(handler, resCategoryPack.a());
                    this.b.b(handler, resCategoryPack.a());
                    return;
                case 103:
                    ResSoftwarePack resSoftwarePack = (ResSoftwarePack) uniPacket.get("res");
                    if (resSoftwarePack.a() == 0 && resSoftwarePack.a == 3) {
                        this.b.a(handler, resSoftwarePack.c, resSoftwarePack.d, resSoftwarePack.b, resSoftwarePack.e);
                        return;
                    }
                    return;
                case 104:
                    ResSoftDetailPack resSoftDetailPack = (ResSoftDetailPack) uniPacket.get("res");
                    if (resSoftDetailPack.a() == 0) {
                        this.b.a(handler, resSoftDetailPack);
                        return;
                    }
                    return;
                case 105:
                    uniPacket.get("res");
                    return;
                case 106:
                    uniPacket.get("res");
                    return;
                case 107:
                    ResCheckUpdatePack resCheckUpdatePack = (ResCheckUpdatePack) uniPacket.get("res");
                    if (resCheckUpdatePack.a() == 0) {
                        this.b.a(handler, resCheckUpdatePack.a);
                        return;
                    }
                    return;
                case 108:
                    uniPacket.get("res");
                    return;
                case 109:
                    uniPacket.get("res");
                    return;
                case 110:
                    ResIconBytesPack resIconBytesPack = (ResIconBytesPack) uniPacket.get("res");
                    if (resIconBytesPack.a() == 0) {
                        Map b2 = resIconBytesPack.b();
                        Set<Integer> keySet = b2.keySet();
                        Bitmap[] bitmapArr = new Bitmap[b2.size()];
                        int i3 = 0;
                        for (Integer num : keySet) {
                            byte[] bArr2 = (byte[]) b2.get(num);
                            bitmapArr[i3] = BitmapFactory.decodeByteArray(bArr2, 0, bArr2.length);
                            i3++;
                        }
                        return;
                    }
                    return;
                case 111:
                    uniPacket.get("res");
                    return;
                case 112:
                    uniPacket.get("res");
                    return;
                case 223:
                    ResAddLabelPack resAddLabelPack = (ResAddLabelPack) uniPacket.get("res");
                    if (resAddLabelPack.a() == 0) {
                        this.b.a(handler, resAddLabelPack.b(), resAddLabelPack.c());
                        return;
                    }
                    this.b.j(handler, resAddLabelPack.a());
                    return;
                case 224:
                    ResSearchByLabelPack resSearchByLabelPack = (ResSearchByLabelPack) uniPacket.get("res");
                    if (resSearchByLabelPack.a() == 0) {
                        this.b.a(handler, resSearchByLabelPack.b(), resSearchByLabelPack.c(), resSearchByLabelPack.d());
                        return;
                    }
                    this.b.k(handler, resSearchByLabelPack.a());
                    return;
                case 302:
                    ResGetFlashScreenPack resGetFlashScreenPack = (ResGetFlashScreenPack) uniPacket.get("res");
                    if (resGetFlashScreenPack.a() == 0) {
                        this.b.a(handler, resGetFlashScreenPack.b(), resGetFlashScreenPack.c(), resGetFlashScreenPack.d());
                        return;
                    }
                    this.b.d(handler, resGetFlashScreenPack.a());
                    return;
                case 304:
                    ResGetSoftwaresLatestPack resGetSoftwaresLatestPack = (ResGetSoftwaresLatestPack) uniPacket.get("res");
                    if (resGetSoftwaresLatestPack.a() == 0) {
                        this.b.a(handler, resGetSoftwaresLatestPack.b());
                        return;
                    }
                    this.b.e(handler, resGetSoftwaresLatestPack.a());
                    return;
                case 305:
                    ResGetSoftwaresOnTopPack resGetSoftwaresOnTopPack = (ResGetSoftwaresOnTopPack) uniPacket.get("res");
                    if (resGetSoftwaresOnTopPack.a() == 0) {
                        this.b.b(handler, resGetSoftwaresOnTopPack.b());
                        return;
                    }
                    this.b.f(handler, resGetSoftwaresOnTopPack.a());
                    return;
                case 306:
                    ResGetSoftwaresOnTopPack resGetSoftwaresOnTopPack2 = (ResGetSoftwaresOnTopPack) uniPacket.get("res");
                    if (resGetSoftwaresOnTopPack2.a() == 0) {
                        this.b.c(handler, resGetSoftwaresOnTopPack2.b());
                        return;
                    }
                    this.b.g(handler, resGetSoftwaresOnTopPack2.a());
                    return;
                case 307:
                    ResGetHotSoftwaresPack resGetHotSoftwaresPack = (ResGetHotSoftwaresPack) uniPacket.get("res");
                    if (resGetHotSoftwaresPack.a() != 0) {
                        this.b.f(handler, resGetHotSoftwaresPack.a());
                        return;
                    }
                    return;
                case 308:
                    uniPacket.get("res");
                    return;
                case 309:
                    uniPacket.get("res");
                    return;
                case 501:
                    uniPacket.get("res");
                    return;
                case 502:
                    uniPacket.get("res");
                    return;
                case 600:
                    uniPacket.get("res");
                    return;
                case 700:
                    if (((ResReportStatDataPack) uniPacket.get("res")).a() == 0) {
                        this.b.q(handler, i2);
                        return;
                    } else {
                        this.b.d();
                        return;
                    }
                case AboutSettingActivity.UPDATE_READY /*800*/:
                    ResEverydayForYouPack resEverydayForYouPack = (ResEverydayForYouPack) uniPacket.get("res");
                    if (resEverydayForYouPack.a() == 0) {
                        this.b.a(handler, resEverydayForYouPack.c(), resEverydayForYouPack.b(), resEverydayForYouPack.d());
                        return;
                    }
                    this.b.h(handler, resEverydayForYouPack.a());
                    return;
                case 900:
                    ResSoftwareListPack resSoftwareListPack = (ResSoftwareListPack) uniPacket.get("res");
                    if (resSoftwareListPack.a() == 0) {
                        this.b.d(handler, resSoftwareListPack.a);
                        return;
                    }
                    this.b.i(handler, resSoftwareListPack.a());
                    return;
                case 901:
                    uniPacket.get("res");
                    return;
                default:
                    return;
            }
        }
    }

    public final void a(Handler handler, int i, int i2, String str) {
        this.b.a(handler, i, i2, str);
    }
}
