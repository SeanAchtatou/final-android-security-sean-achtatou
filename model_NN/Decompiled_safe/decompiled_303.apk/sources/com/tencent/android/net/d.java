package com.tencent.android.net;

import AndroidDLoader.ResCategoryPack;
import AndroidDLoader.ResSoftDetailPack;
import android.os.Handler;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface d {
    void a(Handler handler, byte b, String str, String str2, String str3);

    void a(Handler handler, int i);

    void a(Handler handler, int i, int i2);

    void a(Handler handler, int i, int i2, String str);

    void a(Handler handler, ResCategoryPack resCategoryPack);

    void a(Handler handler, ResSoftDetailPack resSoftDetailPack);

    void a(Handler handler, String str, String str2, String str3);

    void a(Handler handler, ArrayList arrayList);

    void a(Handler handler, ArrayList arrayList, int i, int i2);

    void a(Handler handler, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3);

    void a(Handler handler, List list);

    void a(Handler handler, List list, List list2, int i, String str);

    void a(Handler handler, Map map);

    void b(Handler handler, int i);

    void b(Handler handler, ArrayList arrayList);

    void c(Handler handler, int i);

    void c(Handler handler, ArrayList arrayList);

    void d();

    void d(Handler handler, int i);

    void d(Handler handler, ArrayList arrayList);

    void e(Handler handler, int i);

    void f(Handler handler, int i);

    void g(Handler handler, int i);

    void h(Handler handler, int i);

    void i(Handler handler, int i);

    void j(Handler handler, int i);

    void k(Handler handler, int i);

    void q(Handler handler, int i);
}
