package com.tencent.android.net.a;

public final class a {
    private static int a = 0;
    private static a g = null;
    private b b;
    private b c;
    private int d;
    private int e;
    private int f;

    private a() {
        this.b = null;
        this.c = null;
        this.d = 2;
        this.e = 2;
        this.f = 2;
        this.b = new b(this.d, 0);
        this.c = new b(this.e, 1);
        this.b.a();
        this.c.a();
    }

    public static a a() {
        if (g == null) {
            g = new a();
        }
        return g;
    }

    public final int a(int i) {
        int a2 = this.b.a(i);
        return a2 == -1 ? this.c.a(i) : a2;
    }

    public final int a(e eVar) {
        int i = a;
        a = i + 1;
        eVar.a(i);
        if (eVar.b() == 0) {
            this.b.a(eVar);
        } else if (eVar.b() == 1) {
            this.c.a(eVar);
        }
        return eVar.a();
    }

    public final void b() {
        if (this.b != null) {
            this.b.b();
            this.b = null;
        }
        if (this.c != null) {
            this.c.b();
            this.c = null;
        }
        g = null;
    }
}
