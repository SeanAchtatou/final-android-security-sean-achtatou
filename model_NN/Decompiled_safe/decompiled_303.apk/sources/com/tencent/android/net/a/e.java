package com.tencent.android.net.a;

import android.os.Handler;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

public final class e {
    public boolean a = false;
    public boolean b = false;
    public String c = "";
    public Handler d = null;
    private d e = null;
    private int f = 0;
    private int g = 0;
    private int h = 0;
    private boolean i = false;
    private String j = null;
    private int k = 0;
    private boolean l = true;
    private int m = 0;
    private byte[] n = null;
    private HttpPost o = null;
    private HttpGet p = null;
    private Map q = new HashMap();
    private int r = -1;
    private URI s;

    public e(String str, boolean z, int i2, byte[] bArr, d dVar) {
        this.c = str;
        this.l = z;
        this.m = i2;
        this.n = bArr;
        this.e = dVar;
        try {
            this.s = new URI(this.c);
        } catch (URISyntaxException e2) {
            e2.printStackTrace();
        }
    }

    public final int a() {
        return this.f;
    }

    public final void a(int i2) {
        this.f = i2;
    }

    public final int b() {
        return this.g;
    }

    public final void c() {
        this.g = 1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:168:0x03bc, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x03bd, code lost:
        r1 = r3;
     */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x034b  */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x0355  */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x039a  */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x03a4  */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x03bc A[ExcHandler: all (th java.lang.Throwable), Splitter:B:97:0x0259] */
    /* JADX WARNING: Removed duplicated region for block: B:171:0x03c0  */
    /* JADX WARNING: Removed duplicated region for block: B:194:0x0407  */
    /* JADX WARNING: Removed duplicated region for block: B:195:0x0409  */
    /* JADX WARNING: Removed duplicated region for block: B:196:0x040c  */
    /* JADX WARNING: Removed duplicated region for block: B:197:0x040f  */
    /* JADX WARNING: Removed duplicated region for block: B:198:0x0412  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0166  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0170  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x01cb  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x01d5  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0230  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x023a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void d() {
        /*
            r12 = this;
            r10 = 3
            r2 = 1
            r9 = 0
            r8 = 2
            r7 = 0
            boolean r0 = r12.a
            if (r0 == 0) goto L_0x000a
        L_0x0009:
            return
        L_0x000a:
            java.net.URI r0 = r12.s
            if (r0 != 0) goto L_0x0036
            r0 = r9
        L_0x000f:
            if (r0 != 0) goto L_0x041b
            com.tencent.android.net.a.d r0 = r12.e
            int r1 = r12.m
            r2 = 6
            java.lang.String r3 = ""
            r0.a(r1, r2, r3, r12)
            java.lang.String r0 = "HTTP"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "ERROR_URI_format_error:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.net.URI r2 = r12.s
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.v(r0, r1)
            goto L_0x0009
        L_0x0036:
            java.lang.String r1 = r0.getScheme()
            if (r1 == 0) goto L_0x0048
            java.lang.String r0 = r0.getScheme()
            java.lang.String r1 = "http"
            boolean r0 = r0.startsWith(r1)
            if (r0 != 0) goto L_0x004a
        L_0x0048:
            r0 = r9
            goto L_0x000f
        L_0x004a:
            r0 = r2
            goto L_0x000f
        L_0x004c:
            r1 = move-exception
            r2 = r3
            r11 = r0
            r0 = r1
            r1 = r11
        L_0x0051:
            r0.printStackTrace()     // Catch:{ all -> 0x03c8 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x03c8 }
            r3.<init>()     // Catch:{ all -> 0x03c8 }
            java.lang.String r4 = "doGet, ClientProtocolException : "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x03c8 }
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x03c8 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x03c8 }
            r3.toString()     // Catch:{ all -> 0x03c8 }
            com.tencent.android.net.a.d r3 = r12.e     // Catch:{ all -> 0x03c8 }
            if (r3 == 0) goto L_0x007e
            int r3 = r12.h     // Catch:{ all -> 0x03c8 }
            if (r3 < r8) goto L_0x007e
            com.tencent.android.net.a.d r3 = r12.e     // Catch:{ all -> 0x03c8 }
            int r4 = r12.m     // Catch:{ all -> 0x03c8 }
            r5 = 4
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x03c8 }
            r3.a(r4, r5, r0, r12)     // Catch:{ all -> 0x03c8 }
        L_0x007e:
            if (r2 == 0) goto L_0x0415
            org.apache.http.conn.ClientConnectionManager r0 = r2.getConnectionManager()
            r0.shutdown()
            r0 = r7
        L_0x0088:
            if (r1 == 0) goto L_0x0402
            r1 = r0
            r0 = r7
        L_0x008c:
            int r2 = r12.h
            int r2 = r2 + 1
            r12.h = r2
            r2 = r0
            r0 = r1
        L_0x0094:
            int r1 = r12.h
            if (r1 >= r10) goto L_0x0009
            org.apache.http.params.BasicHttpParams r1 = new org.apache.http.params.BasicHttpParams     // Catch:{ ClientProtocolException -> 0x03fb, SocketException -> 0x03f4, SocketTimeoutException -> 0x03ed, IOException -> 0x03e6, DecodeException -> 0x03da, Throwable -> 0x03d0, all -> 0x03cb }
            r1.<init>()     // Catch:{ ClientProtocolException -> 0x03fb, SocketException -> 0x03f4, SocketTimeoutException -> 0x03ed, IOException -> 0x03e6, DecodeException -> 0x03da, Throwable -> 0x03d0, all -> 0x03cb }
            r3 = 30000(0x7530, float:4.2039E-41)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r1, r3)     // Catch:{ ClientProtocolException -> 0x03fb, SocketException -> 0x03f4, SocketTimeoutException -> 0x03ed, IOException -> 0x03e6, DecodeException -> 0x03da, Throwable -> 0x03d0, all -> 0x03cb }
            r3 = 30000(0x7530, float:4.2039E-41)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r1, r3)     // Catch:{ ClientProtocolException -> 0x03fb, SocketException -> 0x03f4, SocketTimeoutException -> 0x03ed, IOException -> 0x03e6, DecodeException -> 0x03da, Throwable -> 0x03d0, all -> 0x03cb }
            r3 = 4096(0x1000, float:5.74E-42)
            org.apache.http.params.HttpConnectionParams.setSocketBufferSize(r1, r3)     // Catch:{ ClientProtocolException -> 0x03fb, SocketException -> 0x03f4, SocketTimeoutException -> 0x03ed, IOException -> 0x03e6, DecodeException -> 0x03da, Throwable -> 0x03d0, all -> 0x03cb }
            r3 = 1
            org.apache.http.client.params.HttpClientParams.setRedirecting(r1, r3)     // Catch:{ ClientProtocolException -> 0x03fb, SocketException -> 0x03f4, SocketTimeoutException -> 0x03ed, IOException -> 0x03e6, DecodeException -> 0x03da, Throwable -> 0x03d0, all -> 0x03cb }
            org.apache.http.impl.client.DefaultHttpClient r3 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ ClientProtocolException -> 0x03fb, SocketException -> 0x03f4, SocketTimeoutException -> 0x03ed, IOException -> 0x03e6, DecodeException -> 0x03da, Throwable -> 0x03d0, all -> 0x03cb }
            r3.<init>(r1)     // Catch:{ ClientProtocolException -> 0x03fb, SocketException -> 0x03f4, SocketTimeoutException -> 0x03ed, IOException -> 0x03e6, DecodeException -> 0x03da, Throwable -> 0x03d0, all -> 0x03cb }
            boolean r1 = r12.i     // Catch:{ ClientProtocolException -> 0x03fb, SocketException -> 0x03f4, SocketTimeoutException -> 0x03ed, IOException -> 0x03e6, DecodeException -> 0x03da, Throwable -> 0x03d0, all -> 0x03cb }
            if (r1 == 0) goto L_0x00cb
            org.apache.http.HttpHost r1 = new org.apache.http.HttpHost     // Catch:{ ClientProtocolException -> 0x03fb, SocketException -> 0x03f4, SocketTimeoutException -> 0x03ed, IOException -> 0x03e6, DecodeException -> 0x03da, Throwable -> 0x03d0, all -> 0x03cb }
            java.lang.String r4 = r12.j     // Catch:{ ClientProtocolException -> 0x03fb, SocketException -> 0x03f4, SocketTimeoutException -> 0x03ed, IOException -> 0x03e6, DecodeException -> 0x03da, Throwable -> 0x03d0, all -> 0x03cb }
            int r5 = r12.k     // Catch:{ ClientProtocolException -> 0x03fb, SocketException -> 0x03f4, SocketTimeoutException -> 0x03ed, IOException -> 0x03e6, DecodeException -> 0x03da, Throwable -> 0x03d0, all -> 0x03cb }
            r1.<init>(r4, r5)     // Catch:{ ClientProtocolException -> 0x03fb, SocketException -> 0x03f4, SocketTimeoutException -> 0x03ed, IOException -> 0x03e6, DecodeException -> 0x03da, Throwable -> 0x03d0, all -> 0x03cb }
            org.apache.http.params.HttpParams r4 = r3.getParams()     // Catch:{ ClientProtocolException -> 0x03fb, SocketException -> 0x03f4, SocketTimeoutException -> 0x03ed, IOException -> 0x03e6, DecodeException -> 0x03da, Throwable -> 0x03d0, all -> 0x03cb }
            java.lang.String r5 = "http.route.default-proxy"
            r4.setParameter(r5, r1)     // Catch:{ ClientProtocolException -> 0x03fb, SocketException -> 0x03f4, SocketTimeoutException -> 0x03ed, IOException -> 0x03e6, DecodeException -> 0x03da, Throwable -> 0x03d0, all -> 0x03cb }
        L_0x00cb:
            boolean r0 = r12.a     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            if (r0 == 0) goto L_0x00dd
            org.apache.http.conn.ClientConnectionManager r0 = r3.getConnectionManager()     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            r0.shutdown()     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            goto L_0x0009
        L_0x00d8:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0051
        L_0x00dd:
            boolean r0 = r12.l     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            if (r0 == 0) goto L_0x0174
            org.apache.http.client.methods.HttpPost r0 = new org.apache.http.client.methods.HttpPost     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            r0.<init>()     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            r12.o = r0     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            org.apache.http.client.methods.HttpPost r0 = r12.o     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            java.net.URI r1 = r12.s     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            r0.setURI(r1)     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            byte[] r0 = r12.n     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            if (r0 == 0) goto L_0x0104
            byte[] r0 = r12.n     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            int r0 = r0.length     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            if (r0 <= 0) goto L_0x0104
            org.apache.http.client.methods.HttpPost r0 = r12.o     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            org.apache.http.entity.ByteArrayEntity r1 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            byte[] r4 = r12.n     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            r1.<init>(r4)     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            r0.setEntity(r1)     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
        L_0x0104:
            java.util.Map r0 = r12.q     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            int r0 = r0.size()     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            if (r0 <= 0) goto L_0x023e
            java.util.Map r0 = r12.q     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            java.util.Set r0 = r0.keySet()     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            java.util.Iterator r4 = r0.iterator()     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
        L_0x0116:
            boolean r0 = r4.hasNext()     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            if (r0 == 0) goto L_0x023e
            java.lang.Object r0 = r4.next()     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            boolean r1 = r12.l     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            if (r1 == 0) goto L_0x01d9
            org.apache.http.client.methods.HttpPost r5 = r12.o     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            java.util.Map r1 = r12.q     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            java.lang.Object r1 = r1.get(r0)     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            r5.addHeader(r0, r1)     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            goto L_0x0116
        L_0x0134:
            r0 = move-exception
            r1 = r2
            r2 = r3
        L_0x0137:
            r0.printStackTrace()     // Catch:{ all -> 0x03c8 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x03c8 }
            r3.<init>()     // Catch:{ all -> 0x03c8 }
            java.lang.String r4 = "doGet, SocketException : "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x03c8 }
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x03c8 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x03c8 }
            r3.toString()     // Catch:{ all -> 0x03c8 }
            com.tencent.android.net.a.d r3 = r12.e     // Catch:{ all -> 0x03c8 }
            if (r3 == 0) goto L_0x0164
            int r3 = r12.h     // Catch:{ all -> 0x03c8 }
            if (r3 < r8) goto L_0x0164
            com.tencent.android.net.a.d r3 = r12.e     // Catch:{ all -> 0x03c8 }
            int r4 = r12.m     // Catch:{ all -> 0x03c8 }
            r5 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x03c8 }
            r3.a(r4, r5, r0, r12)     // Catch:{ all -> 0x03c8 }
        L_0x0164:
            if (r2 == 0) goto L_0x0412
            org.apache.http.conn.ClientConnectionManager r0 = r2.getConnectionManager()
            r0.shutdown()
            r0 = r7
        L_0x016e:
            if (r1 == 0) goto L_0x0402
            r1 = r0
            r0 = r7
            goto L_0x008c
        L_0x0174:
            org.apache.http.client.methods.HttpGet r0 = new org.apache.http.client.methods.HttpGet     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            r0.<init>()     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            r12.p = r0     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            org.apache.http.client.methods.HttpGet r0 = r12.p     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            java.net.URI r1 = r12.s     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            r0.setURI(r1)     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            goto L_0x0104
        L_0x0183:
            r0 = move-exception
            r1 = r2
            r2 = r3
        L_0x0186:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x03c8 }
            r3.<init>()     // Catch:{ all -> 0x03c8 }
            java.lang.String r4 = "doGet, SocketTimeoutException : "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x03c8 }
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x03c8 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x03c8 }
            r3.toString()     // Catch:{ all -> 0x03c8 }
            r0.printStackTrace()     // Catch:{ all -> 0x03c8 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x03c8 }
            r3.<init>()     // Catch:{ all -> 0x03c8 }
            java.lang.String r4 = "doGet, SocketTimeoutException : "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x03c8 }
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x03c8 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x03c8 }
            r3.toString()     // Catch:{ all -> 0x03c8 }
            com.tencent.android.net.a.d r3 = r12.e     // Catch:{ all -> 0x03c8 }
            if (r3 == 0) goto L_0x01c9
            int r3 = r12.h     // Catch:{ all -> 0x03c8 }
            if (r3 < r8) goto L_0x01c9
            com.tencent.android.net.a.d r3 = r12.e     // Catch:{ all -> 0x03c8 }
            int r4 = r12.m     // Catch:{ all -> 0x03c8 }
            r5 = 2
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x03c8 }
            r3.a(r4, r5, r0, r12)     // Catch:{ all -> 0x03c8 }
        L_0x01c9:
            if (r2 == 0) goto L_0x040f
            org.apache.http.conn.ClientConnectionManager r0 = r2.getConnectionManager()
            r0.shutdown()
            r0 = r7
        L_0x01d3:
            if (r1 == 0) goto L_0x0402
            r1 = r0
            r0 = r7
            goto L_0x008c
        L_0x01d9:
            org.apache.http.client.methods.HttpGet r5 = r12.p     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            java.util.Map r1 = r12.q     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            java.lang.Object r1 = r1.get(r0)     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            r5.addHeader(r0, r1)     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            goto L_0x0116
        L_0x01e8:
            r0 = move-exception
            r1 = r2
            r2 = r3
        L_0x01eb:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x03c8 }
            r3.<init>()     // Catch:{ all -> 0x03c8 }
            java.lang.String r4 = "doGet, IOException : "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x03c8 }
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x03c8 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x03c8 }
            r3.toString()     // Catch:{ all -> 0x03c8 }
            r0.printStackTrace()     // Catch:{ all -> 0x03c8 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x03c8 }
            r3.<init>()     // Catch:{ all -> 0x03c8 }
            java.lang.String r4 = "doGet, IOException : "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x03c8 }
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x03c8 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x03c8 }
            r3.toString()     // Catch:{ all -> 0x03c8 }
            com.tencent.android.net.a.d r3 = r12.e     // Catch:{ all -> 0x03c8 }
            if (r3 == 0) goto L_0x022e
            int r3 = r12.h     // Catch:{ all -> 0x03c8 }
            if (r3 < r8) goto L_0x022e
            com.tencent.android.net.a.d r3 = r12.e     // Catch:{ all -> 0x03c8 }
            int r4 = r12.m     // Catch:{ all -> 0x03c8 }
            r5 = 3
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x03c8 }
            r3.a(r4, r5, r0, r12)     // Catch:{ all -> 0x03c8 }
        L_0x022e:
            if (r2 == 0) goto L_0x040c
            org.apache.http.conn.ClientConnectionManager r0 = r2.getConnectionManager()
            r0.shutdown()
            r0 = r7
        L_0x0238:
            if (r1 == 0) goto L_0x0402
            r1 = r0
            r0 = r7
            goto L_0x008c
        L_0x023e:
            java.util.Date r0 = new java.util.Date     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            r0.<init>(r4)     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            r0.toLocaleString()     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            org.apache.http.protocol.BasicHttpContext r0 = new org.apache.http.protocol.BasicHttpContext     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            r0.<init>()     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            boolean r1 = r12.l     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            if (r1 == 0) goto L_0x026d
            org.apache.http.client.methods.HttpPost r1 = r12.o     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            org.apache.http.HttpResponse r0 = r3.execute(r1, r0)     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
        L_0x0259:
            boolean r1 = r12.a     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            if (r1 == 0) goto L_0x0274
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            r1.shutdown()     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            goto L_0x0009
        L_0x0266:
            r1 = move-exception
            r2 = r3
            r11 = r0
            r0 = r1
            r1 = r11
            goto L_0x0137
        L_0x026d:
            org.apache.http.client.methods.HttpGet r1 = r12.p     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            org.apache.http.HttpResponse r0 = r3.execute(r1, r0)     // Catch:{ ClientProtocolException -> 0x00d8, SocketException -> 0x0134, SocketTimeoutException -> 0x0183, IOException -> 0x01e8, DecodeException -> 0x03e1, Throwable -> 0x03d6, all -> 0x03bc }
            goto L_0x0259
        L_0x0274:
            org.apache.http.StatusLine r1 = r0.getStatusLine()     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            int r1 = r1.getStatusCode()     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            java.lang.String r2 = "Content-Type"
            org.apache.http.Header[] r2 = r0.getHeaders(r2)     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            r4 = r9
        L_0x0283:
            int r5 = r2.length     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            if (r4 >= r5) goto L_0x0418
            r5 = r2[r4]     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            java.lang.String r5 = r5.getName()     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            java.lang.String r6 = "Content-Type"
            boolean r5 = r5.equalsIgnoreCase(r6)     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            if (r5 == 0) goto L_0x02b6
            r2 = r2[r4]     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
        L_0x0296:
            if (r2 == 0) goto L_0x02b9
            r2.getValue()     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            java.lang.String r2 = r2.getValue()     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            java.lang.String r4 = "text"
            boolean r2 = r2.startsWith(r4)     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            if (r2 == 0) goto L_0x02c0
            java.io.IOException r1 = new java.io.IOException     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            java.lang.String r2 = "Content-Type Error"
            r1.<init>(r2)     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            throw r1     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
        L_0x02af:
            r1 = move-exception
            r2 = r3
            r11 = r0
            r0 = r1
            r1 = r11
            goto L_0x0186
        L_0x02b6:
            int r4 = r4 + 1
            goto L_0x0283
        L_0x02b9:
            java.lang.String r2 = "HTTP"
            java.lang.String r4 = "no content-type"
            android.util.Log.v(r2, r4)     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
        L_0x02c0:
            java.util.Date r2 = new java.util.Date     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            r2.<init>(r4)     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            r2.toLocaleString()     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 == r2) goto L_0x0359
            r2 = 206(0xce, float:2.89E-43)
            if (r1 == r2) goto L_0x0359
            com.tencent.android.net.a.d r2 = r12.e     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            if (r2 == 0) goto L_0x02e1
            com.tencent.android.net.a.d r2 = r12.e     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            int r4 = r12.m     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            java.lang.String r5 = ""
            r2.a(r4, r1, r5, r12)     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
        L_0x02e1:
            boolean r1 = r12.l     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            if (r1 == 0) goto L_0x02fa
            org.apache.http.client.methods.HttpPost r1 = r12.o     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            r1.abort()     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
        L_0x02ea:
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            r1.shutdown()     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            goto L_0x0009
        L_0x02f3:
            r1 = move-exception
            r2 = r3
            r11 = r0
            r0 = r1
            r1 = r11
            goto L_0x01eb
        L_0x02fa:
            org.apache.http.client.methods.HttpGet r1 = r12.p     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            r1.abort()     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            goto L_0x02ea
        L_0x0300:
            r1 = move-exception
            r2 = r3
            r11 = r0
            r0 = r1
            r1 = r11
        L_0x0305:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x03c8 }
            r3.<init>()     // Catch:{ all -> 0x03c8 }
            java.lang.String r4 = "doGet, DecodeException : "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x03c8 }
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x03c8 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x03c8 }
            r3.toString()     // Catch:{ all -> 0x03c8 }
            r0.printStackTrace()     // Catch:{ all -> 0x03c8 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x03c8 }
            r3.<init>()     // Catch:{ all -> 0x03c8 }
            java.lang.String r4 = "doGet, DecodeException : "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x03c8 }
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x03c8 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x03c8 }
            r3.toString()     // Catch:{ all -> 0x03c8 }
            com.tencent.android.net.a.d r3 = r12.e     // Catch:{ all -> 0x03c8 }
            if (r3 == 0) goto L_0x0349
            int r3 = r12.h     // Catch:{ all -> 0x03c8 }
            if (r3 < r8) goto L_0x0349
            com.tencent.android.net.a.d r3 = r12.e     // Catch:{ all -> 0x03c8 }
            int r4 = r12.m     // Catch:{ all -> 0x03c8 }
            r5 = 10
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x03c8 }
            r3.a(r4, r5, r0, r12)     // Catch:{ all -> 0x03c8 }
        L_0x0349:
            if (r2 == 0) goto L_0x0409
            org.apache.http.conn.ClientConnectionManager r0 = r2.getConnectionManager()
            r0.shutdown()
            r0 = r7
        L_0x0353:
            if (r1 == 0) goto L_0x0402
            r1 = r0
            r0 = r7
            goto L_0x008c
        L_0x0359:
            boolean r1 = r12.a     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            if (r1 == 0) goto L_0x03a8
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            r1.shutdown()     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            goto L_0x0009
        L_0x0366:
            r1 = move-exception
            r2 = r3
            r11 = r0
            r0 = r1
            r1 = r11
        L_0x036b:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x03c8 }
            r3.<init>()     // Catch:{ all -> 0x03c8 }
            java.lang.String r4 = "doGet, Throwable : "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x03c8 }
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x03c8 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x03c8 }
            r3.toString()     // Catch:{ all -> 0x03c8 }
            r0.printStackTrace()     // Catch:{ all -> 0x03c8 }
            com.tencent.android.net.a.d r3 = r12.e     // Catch:{ all -> 0x03c8 }
            if (r3 == 0) goto L_0x0398
            int r3 = r12.h     // Catch:{ all -> 0x03c8 }
            if (r3 < r8) goto L_0x0398
            com.tencent.android.net.a.d r3 = r12.e     // Catch:{ all -> 0x03c8 }
            int r4 = r12.m     // Catch:{ all -> 0x03c8 }
            r5 = 5
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x03c8 }
            r3.a(r4, r5, r0, r12)     // Catch:{ all -> 0x03c8 }
        L_0x0398:
            if (r2 == 0) goto L_0x0407
            org.apache.http.conn.ClientConnectionManager r0 = r2.getConnectionManager()
            r0.shutdown()
            r0 = r7
        L_0x03a2:
            if (r1 == 0) goto L_0x0402
            r1 = r0
            r0 = r7
            goto L_0x008c
        L_0x03a8:
            com.tencent.android.net.a.d r1 = r12.e     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            if (r1 == 0) goto L_0x03b3
            com.tencent.android.net.a.d r1 = r12.e     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            int r2 = r12.m     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            r1.a(r2, r0, r12)     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
        L_0x03b3:
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            r1.shutdown()     // Catch:{ ClientProtocolException -> 0x004c, SocketException -> 0x0266, SocketTimeoutException -> 0x02af, IOException -> 0x02f3, DecodeException -> 0x0300, Throwable -> 0x0366, all -> 0x03bc }
            goto L_0x0009
        L_0x03bc:
            r0 = move-exception
            r1 = r3
        L_0x03be:
            if (r1 == 0) goto L_0x03c7
            org.apache.http.conn.ClientConnectionManager r1 = r1.getConnectionManager()
            r1.shutdown()
        L_0x03c7:
            throw r0
        L_0x03c8:
            r0 = move-exception
            r1 = r2
            goto L_0x03be
        L_0x03cb:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
            goto L_0x03be
        L_0x03d0:
            r1 = move-exception
            r11 = r1
            r1 = r2
            r2 = r0
            r0 = r11
            goto L_0x036b
        L_0x03d6:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x036b
        L_0x03da:
            r1 = move-exception
            r11 = r1
            r1 = r2
            r2 = r0
            r0 = r11
            goto L_0x0305
        L_0x03e1:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0305
        L_0x03e6:
            r1 = move-exception
            r11 = r1
            r1 = r2
            r2 = r0
            r0 = r11
            goto L_0x01eb
        L_0x03ed:
            r1 = move-exception
            r11 = r1
            r1 = r2
            r2 = r0
            r0 = r11
            goto L_0x0186
        L_0x03f4:
            r1 = move-exception
            r11 = r1
            r1 = r2
            r2 = r0
            r0 = r11
            goto L_0x0137
        L_0x03fb:
            r1 = move-exception
            r11 = r1
            r1 = r2
            r2 = r0
            r0 = r11
            goto L_0x0051
        L_0x0402:
            r11 = r1
            r1 = r0
            r0 = r11
            goto L_0x008c
        L_0x0407:
            r0 = r2
            goto L_0x03a2
        L_0x0409:
            r0 = r2
            goto L_0x0353
        L_0x040c:
            r0 = r2
            goto L_0x0238
        L_0x040f:
            r0 = r2
            goto L_0x01d3
        L_0x0412:
            r0 = r2
            goto L_0x016e
        L_0x0415:
            r0 = r2
            goto L_0x0088
        L_0x0418:
            r2 = r7
            goto L_0x0296
        L_0x041b:
            r2 = r7
            r0 = r7
            goto L_0x0094
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.net.a.e.d():void");
    }
}
