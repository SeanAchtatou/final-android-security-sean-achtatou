package com.tencent.android.net;

import android.os.Handler;
import com.tencent.android.net.a.d;
import com.tencent.android.net.a.e;
import com.tencent.module.setting.AboutSettingActivity;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class a implements d {
    private static a a = null;
    private static String d = "http://dl.3g.qq.com/webapp_dloader/AndroidDLoader";
    private b b = null;
    private com.tencent.android.net.a.a c = null;
    private int e = 0;

    private a(d dVar) {
        this.b = b.a(dVar);
        this.c = com.tencent.android.net.a.a.a();
    }

    public static a a(d dVar) {
        if (a == null) {
            a = new a(dVar);
        }
        return a;
    }

    public final int a(Handler handler) {
        e eVar = new e(d + "?aid=getCategorys", true, 102, b.b(), this);
        eVar.d = handler;
        this.c.a(eVar);
        return eVar.a();
    }

    public final int a(Handler handler, int i) {
        e eVar = new e(d + "?aid=getSoftwaresLatest", true, 304, b.a(i), this);
        eVar.d = handler;
        this.c.a(eVar);
        return eVar.a();
    }

    public final int a(Handler handler, int i, int i2) {
        e eVar = new e(d + "?aid=searchByLabel", true, 224, b.a(i, i2), this);
        eVar.d = handler;
        this.c.a(eVar);
        return eVar.a();
    }

    public final int a(Handler handler, int i, int i2, int i3) {
        e eVar = new e(d + "?aid=getCommentPageByProduct", true, 105, b.a(i, i2, i3), this);
        eVar.d = handler;
        this.c.a(eVar);
        return eVar.a();
    }

    public final int a(Handler handler, int i, int i2, int i3, int i4) {
        e eVar = new e(d + "?aid=getRelatedSoftwares", true, 501, b.a(i, i2, i3, i4), this);
        eVar.d = handler;
        this.c.a(eVar);
        return eVar.a();
    }

    public final int a(Handler handler, String str, int i) {
        e eVar = new e(d + "?aid=getSoftPageByRank", true, 103, b.a(str, i), this);
        eVar.d = handler;
        this.c.a(eVar);
        return eVar.a();
    }

    public final int a(Handler handler, String str, int i, int i2) {
        e eVar = new e(d + "?aid=addLabel", true, 223, b.a(str, i, i2), this);
        eVar.d = handler;
        this.c.a(eVar);
        return eVar.a();
    }

    public final int a(Handler handler, ArrayList arrayList) {
        e eVar = new e(d + "?aid=checkUpdate", true, 107, b.a(arrayList), this);
        eVar.d = handler;
        this.c.a(eVar);
        return eVar.a();
    }

    public final int a(Handler handler, List list) {
        e eVar = new e(d + "?aid=getIconBytes", true, 110, b.a(list), this);
        eVar.d = handler;
        this.c.a(eVar);
        return eVar.a();
    }

    public final int a(Map map) {
        e eVar = new e(d + "?aid=reportStatData", true, 700, b.a(map), this);
        eVar.d = null;
        this.c.a(eVar);
        return eVar.a();
    }

    public final void a() {
        if (this.c != null) {
            this.c.b();
            this.c = null;
        }
        if (this.b != null) {
            b.a();
            a = null;
        }
        a = null;
    }

    public final void a(int i, int i2, String str, e eVar) {
        this.b.a(eVar.d, i, i2, str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x002e A[SYNTHETIC, Splitter:B:16:0x002e] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0033 A[Catch:{ IOException -> 0x005d }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(int r9, org.apache.http.HttpResponse r10, com.tencent.android.net.a.e r11) {
        /*
            r8 = this;
            r2 = 0
            org.apache.http.HttpEntity r0 = r10.getEntity()
            java.io.InputStream r0 = r0.getContent()     // Catch:{ Exception -> 0x0075, all -> 0x0067 }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0078, all -> 0x006a }
            r1.<init>()     // Catch:{ Exception -> 0x0078, all -> 0x006a }
        L_0x000e:
            int r2 = r0.read()     // Catch:{ Exception -> 0x0019, all -> 0x0070 }
            r3 = -1
            if (r2 == r3) goto L_0x0037
            r1.write(r2)     // Catch:{ Exception -> 0x0019, all -> 0x0070 }
            goto L_0x000e
        L_0x0019:
            r2 = move-exception
            r7 = r2
            r2 = r0
            r0 = r7
        L_0x001d:
            r0.printStackTrace()     // Catch:{ all -> 0x002b }
            com.tencent.android.net.b r3 = r8.b     // Catch:{ all -> 0x002b }
            android.os.Handler r4 = r11.d     // Catch:{ all -> 0x002b }
            r5 = 0
            java.lang.String r6 = ""
            r3.a(r4, r9, r5, r6)     // Catch:{ all -> 0x002b }
            throw r0     // Catch:{ all -> 0x002b }
        L_0x002b:
            r0 = move-exception
        L_0x002c:
            if (r2 == 0) goto L_0x0031
            r2.close()     // Catch:{ IOException -> 0x005d }
        L_0x0031:
            if (r1 == 0) goto L_0x0036
            r1.close()     // Catch:{ IOException -> 0x005d }
        L_0x0036:
            throw r0
        L_0x0037:
            byte[] r2 = r1.toByteArray()     // Catch:{ Exception -> 0x0019, all -> 0x0070 }
            int r3 = r2.length     // Catch:{ Exception -> 0x0019, all -> 0x0070 }
            if (r3 <= 0) goto L_0x0044
            int r3 = r8.e     // Catch:{ Exception -> 0x0019, all -> 0x0070 }
            int r4 = r2.length     // Catch:{ Exception -> 0x0019, all -> 0x0070 }
            int r3 = r3 + r4
            r8.e = r3     // Catch:{ Exception -> 0x0019, all -> 0x0070 }
        L_0x0044:
            if (r0 == 0) goto L_0x0049
            r0.close()     // Catch:{ IOException -> 0x0058 }
        L_0x0049:
            r1.close()     // Catch:{ IOException -> 0x0058 }
        L_0x004c:
            com.tencent.android.net.b r0 = r8.b     // Catch:{ Exception -> 0x0062 }
            android.os.Handler r1 = r11.d     // Catch:{ Exception -> 0x0062 }
            int r3 = r11.a()     // Catch:{ Exception -> 0x0062 }
            r0.a(r9, r2, r1, r3)     // Catch:{ Exception -> 0x0062 }
        L_0x0057:
            return
        L_0x0058:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x004c
        L_0x005d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0036
        L_0x0062:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0057
        L_0x0067:
            r0 = move-exception
            r1 = r2
            goto L_0x002c
        L_0x006a:
            r1 = move-exception
            r7 = r1
            r1 = r2
            r2 = r0
            r0 = r7
            goto L_0x002c
        L_0x0070:
            r2 = move-exception
            r7 = r2
            r2 = r0
            r0 = r7
            goto L_0x002c
        L_0x0075:
            r0 = move-exception
            r1 = r2
            goto L_0x001d
        L_0x0078:
            r1 = move-exception
            r7 = r1
            r1 = r2
            r2 = r0
            r0 = r7
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.net.a.a(int, org.apache.http.HttpResponse, com.tencent.android.net.a.e):void");
    }

    public final int b(Handler handler) {
        e eVar = new e(d + "?aid=getScrollablePicAdv", true, 301, b.c(), this);
        eVar.d = handler;
        this.c.a(eVar);
        return eVar.a();
    }

    public final int b(Handler handler, int i) {
        e eVar = new e(d + "?aid=getSoftwaresOnTopByScroe", true, 305, b.a(i, (byte) 1), this);
        eVar.d = handler;
        this.c.a(eVar);
        return eVar.a();
    }

    public final int b(Handler handler, int i, int i2, int i3) {
        e eVar = new e(d + "?aid=getSoftDetail", true, 104, b.b(i, i2, i3), this);
        eVar.d = handler;
        this.c.a(eVar);
        return eVar.a();
    }

    public final int c(Handler handler) {
        e eVar = new e(d + "?aid=getFlashScreen", true, 302, b.d(), this);
        eVar.d = handler;
        this.c.a(eVar);
        return eVar.a();
    }

    public final int c(Handler handler, int i) {
        e eVar = new e(d + "?aid=getSoftwaresOnTopByDownloadCount", true, 306, b.a(i, (byte) 2), this);
        eVar.d = handler;
        this.c.a(eVar);
        return eVar.a();
    }

    public final int d(Handler handler) {
        e eVar = new e(d + "?aid=getLoadingText", true, 303, b.e(), this);
        eVar.d = handler;
        this.c.a(eVar);
        return eVar.a();
    }

    public final int d(Handler handler, int i) {
        e eVar = new e(d + "?aid=everydayForYou", true, AboutSettingActivity.UPDATE_READY, b.b(i), this);
        eVar.d = handler;
        this.c.a(eVar);
        return eVar.a();
    }

    public final int e(Handler handler) {
        e eVar = new e(d + "?aid=getUserCommends", true, 308, b.f(), this);
        eVar.d = handler;
        this.c.a(eVar);
        return eVar.a();
    }

    public final int e(Handler handler, int i) {
        e eVar = new e(d + "?aid=getRequiredSoftwares", true, 900, b.c(i), this);
        eVar.d = handler;
        this.c.a(eVar);
        return eVar.a();
    }
}
