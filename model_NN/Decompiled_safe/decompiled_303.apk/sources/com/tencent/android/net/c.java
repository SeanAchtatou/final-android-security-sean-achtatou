package com.tencent.android.net;

import com.tencent.android.net.a.a;
import com.tencent.android.net.a.d;
import com.tencent.android.net.a.e;

public final class c implements d {
    private static c d = null;
    private final String a = "FileHttpEngine";
    private e b = null;
    private a c = null;
    private int e = 0;

    private c(e eVar) {
        this.b = eVar;
        this.c = a.a();
    }

    public static c a(e eVar) {
        if (d == null) {
            d = new c(eVar);
        }
        return d;
    }

    public final int a(String str) {
        e eVar = new e(str, false, 10000, null, this);
        eVar.c();
        this.c.a(eVar);
        return eVar.a();
    }

    public final void a() {
        if (this.c != null) {
            this.c.b();
            this.c = null;
        }
        d = null;
    }

    public final void a(int i, int i2, String str, e eVar) {
        if (i == 10000) {
            this.b.a(eVar.c);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:46:0x0086 A[SYNTHETIC, Splitter:B:46:0x0086] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x008f A[Catch:{ Exception -> 0x0093 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(int r11, org.apache.http.HttpResponse r12, com.tencent.android.net.a.e r13) {
        /*
            r10 = this;
            r9 = 10000(0x2710, float:1.4013E-41)
            r8 = 0
            r7 = 0
            org.apache.http.HttpEntity r0 = r12.getEntity()
            r1 = 51200(0xc800, float:7.1746E-41)
            byte[] r1 = new byte[r1]
            long r2 = r0.getContentLength()
            int r2 = (int) r2
            int r3 = r10.e
            int r3 = r3 + r2
            r10.e = r3
            boolean r3 = r13.a
            if (r3 == 0) goto L_0x001c
        L_0x001b:
            return
        L_0x001c:
            java.io.InputStream r0 = r0.getContent()     // Catch:{ Exception -> 0x00a4, all -> 0x0081 }
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x00a8, all -> 0x0098 }
            r3.<init>()     // Catch:{ Exception -> 0x00a8, all -> 0x0098 }
            r4 = r7
        L_0x0026:
            if (r4 >= r2) goto L_0x0062
            int r5 = r0.read(r1)     // Catch:{ Exception -> 0x0035, all -> 0x009d }
            r6 = -1
            if (r5 == r6) goto L_0x0062
            int r4 = r4 + r5
            r6 = 0
            r3.write(r1, r6, r5)     // Catch:{ Exception -> 0x0035, all -> 0x009d }
            goto L_0x0026
        L_0x0035:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
        L_0x0039:
            r3 = 1
            r0.printStackTrace()     // Catch:{ all -> 0x00a2 }
            if (r11 != r9) goto L_0x0046
            com.tencent.android.net.e r0 = r10.b     // Catch:{ all -> 0x00a2 }
            java.lang.String r4 = r13.c     // Catch:{ all -> 0x00a2 }
            r0.a(r4)     // Catch:{ all -> 0x00a2 }
        L_0x0046:
            if (r2 == 0) goto L_0x004f
            boolean r0 = r13.a     // Catch:{ Exception -> 0x007a }
            if (r0 != 0) goto L_0x004f
            r2.close()     // Catch:{ Exception -> 0x007a }
        L_0x004f:
            if (r1 == 0) goto L_0x0054
            r1.close()     // Catch:{ Exception -> 0x007a }
        L_0x0054:
            r0 = r3
            r1 = r8
        L_0x0056:
            if (r11 != r9) goto L_0x001b
            if (r0 != 0) goto L_0x001b
            com.tencent.android.net.e r0 = r10.b
            java.lang.String r2 = r13.c
            r0.a(r2, r1)
            goto L_0x001b
        L_0x0062:
            byte[] r1 = r3.toByteArray()     // Catch:{ Exception -> 0x0035, all -> 0x009d }
            if (r0 == 0) goto L_0x006f
            boolean r2 = r13.a     // Catch:{ Exception -> 0x0074 }
            if (r2 != 0) goto L_0x006f
            r0.close()     // Catch:{ Exception -> 0x0074 }
        L_0x006f:
            r3.close()     // Catch:{ Exception -> 0x0074 }
            r0 = r7
            goto L_0x0056
        L_0x0074:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r7
            goto L_0x0056
        L_0x007a:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r3
            r1 = r8
            goto L_0x0056
        L_0x0081:
            r0 = move-exception
            r1 = r8
            r2 = r8
        L_0x0084:
            if (r2 == 0) goto L_0x008d
            boolean r3 = r13.a     // Catch:{ Exception -> 0x0093 }
            if (r3 != 0) goto L_0x008d
            r2.close()     // Catch:{ Exception -> 0x0093 }
        L_0x008d:
            if (r1 == 0) goto L_0x0092
            r1.close()     // Catch:{ Exception -> 0x0093 }
        L_0x0092:
            throw r0
        L_0x0093:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0092
        L_0x0098:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r8
            goto L_0x0084
        L_0x009d:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x0084
        L_0x00a2:
            r0 = move-exception
            goto L_0x0084
        L_0x00a4:
            r0 = move-exception
            r1 = r8
            r2 = r8
            goto L_0x0039
        L_0x00a8:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r8
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.net.c.a(int, org.apache.http.HttpResponse, com.tencent.android.net.a.e):void");
    }
}
