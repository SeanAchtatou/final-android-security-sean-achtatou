package com.tencent.android.net.a;

import android.os.PowerManager;
import com.tencent.launcher.base.BaseApp;
import java.util.ArrayList;
import java.util.Vector;

public final class b {
    /* access modifiers changed from: private */
    public Object a = new Object();
    /* access modifiers changed from: private */
    public Object b = new Object();
    /* access modifiers changed from: private */
    public Object c = new Object();
    /* access modifiers changed from: private */
    public Vector d = new Vector();
    /* access modifiers changed from: private */
    public Vector e = new Vector();
    private ArrayList f = null;
    private int g = 0;
    private PowerManager h = null;

    public b(int i, int i2) {
        int i3 = i <= 0 ? 2 : i;
        this.g = i2;
        this.f = new ArrayList();
        for (int i4 = 0; i4 < i3; i4++) {
            this.f.add(new c(this));
        }
    }

    static /* synthetic */ void a(b bVar, e eVar) {
        if (bVar.h == null) {
            bVar.h = (PowerManager) BaseApp.b().getSystemService("power");
        }
        PowerManager.WakeLock newWakeLock = bVar.h.newWakeLock(1, "httpNet");
        newWakeLock.acquire();
        eVar.d();
        newWakeLock.release();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0034, code lost:
        r0 = r7.c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0036, code lost:
        monitor-enter(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003d, code lost:
        if (r7.e.size() <= 0) goto L_0x0063;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003f, code lost:
        r1 = r7.e.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0049, code lost:
        if (r1.hasNext() == false) goto L_0x0063;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x004b, code lost:
        r7 = (com.tencent.android.net.a.e) r1.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0055, code lost:
        if (r8 != r7.a()) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0057, code lost:
        r7.b = false;
        r7.a = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005d, code lost:
        monitor-exit(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        monitor-exit(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        return 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        return -1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int a(int r8) {
        /*
            r7 = this;
            r6 = -1
            r5 = 1
            r4 = 0
            if (r8 >= 0) goto L_0x0007
            r0 = r6
        L_0x0006:
            return r0
        L_0x0007:
            java.lang.Object r1 = r7.b
            monitor-enter(r1)
            java.util.Vector r0 = r7.d     // Catch:{ all -> 0x0060 }
            int r0 = r0.size()     // Catch:{ all -> 0x0060 }
            if (r0 <= 0) goto L_0x0033
            java.util.Vector r0 = r7.d     // Catch:{ all -> 0x0060 }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x0060 }
        L_0x0018:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x0060 }
            if (r0 == 0) goto L_0x0033
            java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x0060 }
            com.tencent.android.net.a.e r0 = (com.tencent.android.net.a.e) r0     // Catch:{ all -> 0x0060 }
            int r3 = r0.a()     // Catch:{ all -> 0x0060 }
            if (r8 != r3) goto L_0x0018
            r2 = 0
            r0.b = r2     // Catch:{ all -> 0x0060 }
            r2 = 1
            r0.a = r2     // Catch:{ all -> 0x0060 }
            monitor-exit(r1)     // Catch:{ all -> 0x0060 }
            r0 = r4
            goto L_0x0006
        L_0x0033:
            monitor-exit(r1)     // Catch:{ all -> 0x0060 }
            java.lang.Object r0 = r7.c
            monitor-enter(r0)
            java.util.Vector r1 = r7.e     // Catch:{ all -> 0x0066 }
            int r1 = r1.size()     // Catch:{ all -> 0x0066 }
            if (r1 <= 0) goto L_0x0063
            java.util.Vector r1 = r7.e     // Catch:{ all -> 0x0066 }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x0066 }
        L_0x0045:
            boolean r2 = r1.hasNext()     // Catch:{ all -> 0x0066 }
            if (r2 == 0) goto L_0x0063
            java.lang.Object r7 = r1.next()     // Catch:{ all -> 0x0066 }
            com.tencent.android.net.a.e r7 = (com.tencent.android.net.a.e) r7     // Catch:{ all -> 0x0066 }
            int r2 = r7.a()     // Catch:{ all -> 0x0066 }
            if (r8 != r2) goto L_0x0045
            r1 = 0
            r7.b = r1     // Catch:{ all -> 0x0066 }
            r1 = 1
            r7.a = r1     // Catch:{ all -> 0x0066 }
            monitor-exit(r0)     // Catch:{ all -> 0x0066 }
            r0 = r5
            goto L_0x0006
        L_0x0060:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0060 }
            throw r0
        L_0x0063:
            monitor-exit(r0)     // Catch:{ all -> 0x0066 }
            r0 = r6
            goto L_0x0006
        L_0x0066:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0066 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.net.a.b.a(int):int");
    }

    public final int a(e eVar) {
        this.d.add(eVar);
        synchronized (this.a) {
            this.a.notifyAll();
        }
        return eVar.a();
    }

    public final void a() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f.size()) {
                ((c) this.f.get(i2)).start();
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public final void b() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f.size()) {
                boolean unused = ((c) this.f.get(i2)).a = false;
                i = i2 + 1;
            } else {
                this.d.clear();
                synchronized (this.a) {
                    this.a.notifyAll();
                }
                return;
            }
        }
    }
}
