package com.tencent.android.c;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public final class b extends SQLiteOpenHelper {
    public b(Context context) {
        super(context, "com.tencent.txappcentenr", (SQLiteDatabase.CursorFactory) null, 110200);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private static void a(SQLiteDatabase sQLiteDatabase) {
        try {
            sQLiteDatabase.setPageSize(8192);
            sQLiteDatabase.execSQL("create table if not exists CATEGORY_ITEM (N_CATEGORY_ID INTEGER,S_CATEGORY_NAME TEXT,S_CATEGORYICON TEXT,N_SOFT_COUNT INTEGER);");
            sQLiteDatabase.execSQL("create table if not exists CATEGORY_TYPE (N_TYPE_ID INTEGER);");
            sQLiteDatabase.execSQL("create table if not exists APP_INFO(N_GUID INTEGER,S_QUA TEXT,S_IMEI,TEXT,S_PUSHVERSION INTEGER,S_VERSION STRING, N_TIME INTEGER);");
            sQLiteDatabase.execSQL("create table if not exists ICON_FILES_SAVED(ICON_ID INTEGER PRIMARY KEY,URL TEXT UNIQUE,PATH TEXT,FREQUENCY INTEGER);");
            sQLiteDatabase.execSQL("create table if not exists DOWNLOAD_FILES(ID INTEGER PRIMARY KEY,URL VARCHAR UNIQUE,PATH VARCHAR,STATE INTEGER,DATETIME DATETIME ,PRODUCTID INTEGER ,SOFTID INTEGER , FILEID INTEGER , PACKAGENAME TEXT,APPNAME TEXT , ICONURL TEXT , DLSIZE INTEGER , TOTALSIZE INTEGER, VERSIONCODE INTEGER, STATPAGENO INTEGER,PAGENOPATH TEXT);");
            sQLiteDatabase.execSQL("create table if not exists SEARCH_HISTORY(SEARCHWORD TEXT PRIMARY KEY,FREQUENCY INTEGER);");
            ContentValues contentValues = new ContentValues();
            contentValues.put("N_GUID", (Integer) 0);
            sQLiteDatabase.insert("APP_INFO", null, contentValues);
        } catch (Exception e) {
            Log.e("DBHelper createTable Error:", e.toString());
        }
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        a(sQLiteDatabase);
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        try {
            sQLiteDatabase.execSQL("drop table if exists CATEGORY_ITEM;");
            sQLiteDatabase.execSQL("drop table if exists CATEGORY_TYPE;");
            sQLiteDatabase.execSQL("drop table if exists APP_INFO;");
            sQLiteDatabase.execSQL("drop table if exists ICON_FILES_SAVED;");
            sQLiteDatabase.execSQL("drop table if exists DOWNLOAD_FILES;");
            sQLiteDatabase.execSQL("drop table if exists SEARCH_HISTORY;");
        } catch (Exception e) {
            Log.e("DBHelper resetDb Error:", e.toString());
        }
        a(sQLiteDatabase);
    }
}
