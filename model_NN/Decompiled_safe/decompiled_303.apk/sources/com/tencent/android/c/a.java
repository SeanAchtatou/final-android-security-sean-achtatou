package com.tencent.android.c;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.tencent.android.b.a.d;
import com.tencent.launcher.base.BaseApp;
import java.util.ArrayList;
import java.util.Iterator;

public final class a {
    private static a a = null;
    private static b b = null;
    private static SQLiteDatabase c = null;

    public static com.tencent.android.b.a.a a(String str) {
        com.tencent.android.b.a.a aVar;
        Cursor rawQuery = e().rawQuery("select * from FLASH_SCREEN_PIC_INFO where url = '" + str + "'", null);
        rawQuery.moveToFirst();
        if (!rawQuery.isAfterLast()) {
            aVar = new com.tencent.android.b.a.a();
            aVar.a = rawQuery.getString(rawQuery.getColumnIndex("url"));
            aVar.b = rawQuery.getBlob(rawQuery.getColumnIndex("data"));
            aVar.c = rawQuery.getLong(rawQuery.getColumnIndex("start_date"));
            aVar.d = rawQuery.getLong(rawQuery.getColumnIndex("end_date"));
        } else {
            aVar = null;
        }
        rawQuery.close();
        return aVar;
    }

    public static a a() {
        if (a == null) {
            a = new a();
        }
        return a;
    }

    public static void a(com.tencent.android.b.a.a aVar) {
        if (aVar != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("url", aVar.a);
            contentValues.put("data", aVar.b);
            contentValues.put("start_date", Long.valueOf(aVar.c));
            contentValues.put("end_date", Long.valueOf(aVar.d));
            try {
                e().delete("FLASH_SCREEN_PIC_INFO", null, null);
                e().insert("FLASH_SCREEN_PIC_INFO", null, contentValues);
            } catch (Exception e) {
                Log.e("setFlashLogo", e.toString());
                e.printStackTrace();
            }
        }
    }

    public static void a(ArrayList arrayList) {
        if (arrayList != null && arrayList.size() > 0) {
            try {
                e().delete("LOADINIG_TEXT", null, null);
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("loading_text", (String) it.next());
                    e().insert("LOADINIG_TEXT", null, contentValues);
                }
            } catch (Exception e) {
                Log.e("saveLoadingText", e.toString());
                e.printStackTrace();
            }
        }
    }

    public static void b() {
        b bVar = new b(BaseApp.b());
        b = bVar;
        c = bVar.getWritableDatabase();
    }

    private static SQLiteDatabase e() {
        if (c == null || b == null) {
            b bVar = new b(BaseApp.b());
            b = bVar;
            c = bVar.getWritableDatabase();
        }
        return c;
    }

    public final synchronized boolean a(d dVar) {
        boolean z;
        ContentValues contentValues = new ContentValues();
        contentValues.put("N_GUID", Integer.valueOf(dVar.e()));
        contentValues.put("S_QUA", dVar.c());
        contentValues.put("S_IMEI", dVar.d());
        contentValues.put("S_PUSHVERSION", Integer.valueOf(dVar.b()));
        contentValues.put("S_VERSION", dVar.a());
        try {
            e().delete("APP_INFO", "1=1", null);
            e().insert("APP_INFO", null, contentValues);
            z = true;
        } catch (Exception e) {
            Log.e("setAppInfo", e.toString());
            e.printStackTrace();
            z = false;
        }
        return z;
    }

    public final synchronized d c() {
        d dVar;
        Cursor rawQuery = e().rawQuery("select * from APP_INFO", null);
        rawQuery.moveToFirst();
        if (!rawQuery.isAfterLast()) {
            dVar = new d();
            dVar.b(rawQuery.getInt(0));
            dVar.b(rawQuery.getString(1));
            dVar.c(rawQuery.getString(2));
            dVar.a(rawQuery.getInt(3));
            dVar.a(rawQuery.getString(4));
        } else {
            dVar = null;
        }
        rawQuery.close();
        return dVar;
    }

    public final synchronized void d() {
        try {
            e().execSQL("Delete from ICON_FILES_SAVED");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }
}
