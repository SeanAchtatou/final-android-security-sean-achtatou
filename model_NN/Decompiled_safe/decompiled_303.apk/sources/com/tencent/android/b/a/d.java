package com.tencent.android.b.a;

public final class d {
    private String a;
    private String b;
    private int c = 0;
    private int d;
    private String e;

    public final String a() {
        return this.e;
    }

    public final void a(int i) {
        this.d = i;
    }

    public final void a(String str) {
        this.e = str;
    }

    public final int b() {
        return this.d;
    }

    public final void b(int i) {
        this.c = i;
    }

    public final void b(String str) {
        this.a = str;
    }

    public final String c() {
        return this.a;
    }

    public final void c(String str) {
        this.b = str;
    }

    public final String d() {
        return this.b;
    }

    public final int e() {
        return this.c;
    }
}
