package com.tencent.android.b.a;

import android.graphics.drawable.Drawable;

public final class c {
    public String a = "";
    public String b = "";
    public String c = "";
    public String d = "";
    public Drawable e;
    public String f;
    public long g;
    public long h = 0;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        c cVar = (c) obj;
        if (this.a == null) {
            if (cVar.a != null) {
                return false;
            }
        } else if (!this.a.equals(cVar.a)) {
            return false;
        }
        if (this.b == null) {
            if (cVar.b != null) {
                return false;
            }
        } else if (!this.b.equals(cVar.b)) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return (((this.a == null ? 0 : this.a.hashCode()) + 31) * 31) + (this.b == null ? 0 : this.b.hashCode());
    }

    public final String toString() {
        return this.a + "\t" + this.g;
    }
}
