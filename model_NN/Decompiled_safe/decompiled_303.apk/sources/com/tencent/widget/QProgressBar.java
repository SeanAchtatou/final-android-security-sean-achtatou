package com.tencent.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ProgressBar;

public class QProgressBar extends ProgressBar {
    private String a;
    private Paint b;
    private Rect c;

    public QProgressBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public QProgressBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.b = new Paint();
        this.c = new Rect();
        this.b.setColor(-1);
        this.b.setTextSize(18.0f);
        this.b.setAntiAlias(true);
    }

    /* access modifiers changed from: protected */
    public synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Rect rect = this.c;
        this.b.getTextBounds(this.a, 0, this.a.length(), rect);
        canvas.drawText(this.a, (float) ((getWidth() - rect.width()) / 2), (float) ((getHeight() / 2) - rect.centerY()), this.b);
    }

    public synchronized void setProgress(int i) {
        this.a = String.format("%d%%", Integer.valueOf((i * 100) / getMax()));
        super.setProgress(i);
    }
}
