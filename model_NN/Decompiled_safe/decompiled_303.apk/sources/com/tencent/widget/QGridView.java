package com.tencent.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.GridView;
import android.widget.Scroller;

public class QGridView extends GridView {
    /* access modifiers changed from: private */
    public boolean a = false;
    private int b = 300;
    private Scroller c;
    private GestureDetector d = new GestureDetector(getContext(), new d(this));

    public QGridView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setCacheColorHint(0);
        setVerticalScrollBarEnabled(false);
        this.c = new Scroller(getContext());
    }

    public QGridView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setCacheColorHint(0);
        setVerticalScrollBarEnabled(false);
        this.c = new Scroller(getContext());
    }

    public void computeScroll() {
        if (this.c.computeScrollOffset()) {
            scrollTo(this.c.getCurrX(), this.c.getCurrY());
        }
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if ((action == 1 || action == 3) && this.a) {
            this.a = false;
            this.c.startScroll(0, getScrollY(), 0, -getScrollY(), this.b);
        }
        this.d.onTouchEvent(motionEvent);
        return super.dispatchTouchEvent(motionEvent);
    }
}
