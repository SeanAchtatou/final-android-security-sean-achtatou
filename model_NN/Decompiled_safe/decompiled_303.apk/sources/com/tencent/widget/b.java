package com.tencent.widget;

final class b implements Runnable {
    private int a;
    private /* synthetic */ WidgetFrame b;

    b(WidgetFrame widgetFrame) {
        this.b = widgetFrame;
    }

    public final void a() {
        this.a = this.b.getWindowAttachCount();
    }

    public final void run() {
        if (this.b.getParent() != null && this.b.hasWindowFocus() && this.a == this.b.getWindowAttachCount() && !this.b.a && this.b.performLongClick()) {
            boolean unused = this.b.a = true;
        }
    }
}
