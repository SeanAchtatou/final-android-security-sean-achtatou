package com.tencent.widget;

import android.view.GestureDetector;
import android.view.MotionEvent;

final class d implements GestureDetector.OnGestureListener {
    private int a;
    private /* synthetic */ QGridView b;

    d(QGridView qGridView) {
        this.b = qGridView;
    }

    public final boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return false;
    }

    public final void onLongPress(MotionEvent motionEvent) {
    }

    public final boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        int firstVisiblePosition = this.b.getFirstVisiblePosition();
        int lastVisiblePosition = this.b.getLastVisiblePosition();
        int count = this.b.getCount();
        if (!this.b.a || firstVisiblePosition == 0 || lastVisiblePosition == count - 1) {
            if (!this.b.a) {
                this.a = (int) motionEvent2.getRawY();
            }
            if (this.b.getChildAt(firstVisiblePosition) != null && (this.b.a || (firstVisiblePosition == 0 && f2 < 0.0f))) {
                this.b.scrollTo(0, this.a - ((int) motionEvent2.getRawY()));
                boolean unused = this.b.a = true;
                return true;
            } else if (this.b.getChildAt(lastVisiblePosition - firstVisiblePosition) == null || (!this.b.a && (lastVisiblePosition != count - 1 || f2 <= 0.0f))) {
                boolean unused2 = this.b.a = false;
                return false;
            } else {
                this.b.scrollTo(0, this.a - ((int) motionEvent2.getRawY()));
                boolean unused3 = this.b.a = true;
                return true;
            }
        } else {
            this.b.scrollTo(0, 0);
            boolean unused4 = this.b.a = false;
            return false;
        }
    }

    public final void onShowPress(MotionEvent motionEvent) {
    }

    public final boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }
}
