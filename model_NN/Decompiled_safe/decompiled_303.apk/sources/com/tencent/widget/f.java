package com.tencent.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.tencent.launcher.base.b;
import com.tencent.qqlauncher.R;

public final class f extends PopupWindow implements KeyEvent.Callback {
    private final Context a;
    private final LayoutInflater b = ((Activity) this.a).getLayoutInflater();
    private final WindowManager c = ((WindowManager) this.a.getSystemService("window"));
    private View d = this.b.inflate((int) R.layout.quickaction, (ViewGroup) null);
    private int e;
    private int f;
    private ImageView g;
    private ImageView h;
    private ViewGroup i;
    private Animation j;
    private View k;
    private Rect l;

    public f(Context context, View view, Rect rect) {
        super(context);
        this.k = view;
        this.l = rect;
        this.a = context;
        super.setContentView(this.d);
        this.e = this.c.getDefaultDisplay().getWidth();
        setWindowLayoutMode(-1, -2);
        this.f = this.a.getResources().getDimensionPixelSize(R.dimen.quickaction_shadow_horiz);
        setWidth(-2);
        setHeight(-2);
        setBackgroundDrawable(new ColorDrawable(0));
        this.g = (ImageView) this.d.findViewById(R.id.arrow_up);
        this.h = (ImageView) this.d.findViewById(R.id.arrow_down);
        this.i = (ViewGroup) this.d.findViewById(R.id.quickaction);
        setFocusable(true);
        setTouchable(true);
        setOutsideTouchable(true);
        this.j = AnimationUtils.loadAnimation(this.a, R.anim.quickaction);
        this.j.setInterpolator(new g(this));
    }

    private void a(int i2, int i3) {
        ImageView imageView = i2 == R.id.arrow_up ? this.g : this.h;
        ImageView imageView2 = i2 == R.id.arrow_up ? this.h : this.g;
        int intrinsicWidth = this.a.getResources().getDrawable(R.drawable.quickaction_arrow_up).getIntrinsicWidth();
        imageView.setVisibility(0);
        ((ViewGroup.MarginLayoutParams) imageView.getLayoutParams()).leftMargin = (i3 - (intrinsicWidth / 2)) - ((int) (6.0f * b.b));
        imageView2.setVisibility(4);
    }

    public final void a() {
        int i2;
        int i3;
        int centerX = this.l.centerX();
        super.showAtLocation(this.k, 0, 0, 0);
        if (isShowing()) {
            getContentView().measure(-1, -2);
            int measuredHeight = getContentView().getMeasuredHeight();
            int i4 = -this.f;
            if (this.l.top > measuredHeight) {
                a(R.id.arrow_down, centerX);
                i2 = (this.l.top - measuredHeight) + ((int) (9.0f * b.b));
                i3 = R.style.QuickActionAboveAnimation;
            } else {
                a(R.id.arrow_up, centerX);
                i2 = this.l.bottom;
                i3 = R.style.QuickActionBelowAnimation;
            }
            setAnimationStyle(i3);
            this.i.startAnimation(this.j);
            update(i4, i2, -1, -1);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final void a(int i2, int i3, View.OnClickListener onClickListener) {
        Drawable drawable = this.a.getResources().getDrawable(i2);
        String obj = this.a.getResources().getText(i3).toString();
        View inflate = this.b.inflate((int) R.layout.quickaction_item, this.i, false);
        TextView textView = (TextView) inflate.findViewById(R.id.quickaction_text);
        textView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, drawable, (Drawable) null, (Drawable) null);
        textView.setText(obj);
        inflate.setOnClickListener(onClickListener);
        this.i.addView(inflate);
    }

    public final boolean onKeyDown(int i2, KeyEvent keyEvent) {
        return false;
    }

    public final boolean onKeyLongPress(int i2, KeyEvent keyEvent) {
        return false;
    }

    public final boolean onKeyMultiple(int i2, int i3, KeyEvent keyEvent) {
        return false;
    }

    public final boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return false;
        }
        dismiss();
        return true;
    }
}
