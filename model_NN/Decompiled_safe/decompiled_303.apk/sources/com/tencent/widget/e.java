package com.tencent.widget;

import android.view.View;
import android.widget.TabHost;

final class e implements TabHost.OnTabChangeListener {
    private /* synthetic */ QTabHost a;

    e(QTabHost qTabHost) {
        this.a = qTabHost;
    }

    public final void onTabChanged(String str) {
        View currentTabView = this.a.getCurrentTabView();
        this.a.a.a((currentTabView.getWidth() / 2) + currentTabView.getLeft());
    }
}
