package com.tencent.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import com.tencent.qqlauncher.R;

public class QTabCursor extends View {
    private int a;
    private Bitmap b;
    private Paint c = new Paint();

    public QTabCursor(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        if (this.c == null) {
            this.c = new Paint();
        }
        this.c.setColor(-3355444);
        this.c.setStrokeWidth(3.0f);
        this.b = BitmapFactory.decodeResource(getResources(), R.drawable.tab_cursor);
    }

    public final void a(int i) {
        this.a = i;
        postInvalidate();
    }

    public void onDraw(Canvas canvas) {
        int height = (getHeight() - 3) / 2;
        Bitmap bitmap = this.b;
        canvas.drawLine(0.0f, (float) height, (float) getWidth(), (float) height, this.c);
        if (this.a <= 0) {
            this.a = getWidth() / 2;
        }
        canvas.drawBitmap(bitmap, (float) (this.a - (bitmap.getWidth() / 2)), 0.0f, (Paint) null);
    }
}
