package com.tencent.widget;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import com.tencent.qqlauncher.R;

public class CircularView extends ImageView {
    private long a;
    private Context b;
    private boolean c;
    private Animation d;
    private Handler e;
    private Runnable f;

    public CircularView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CircularView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.b = context;
        this.e = new Handler();
        this.d = AnimationUtils.loadAnimation(this.b, R.anim.loading_move);
        this.a = 30000;
        this.d.setInterpolator(new LinearInterpolator());
        if (getDrawable() == null) {
            setImageResource(R.drawable.waiting);
        }
    }

    public void setAnimation(Animation animation) {
        Animation animation2;
        if (animation == null) {
            animation2 = this.d;
        } else {
            this.d = animation;
            animation2 = animation;
        }
        super.setAnimation(animation2);
    }

    public void setVisibility(int i) {
        super.setVisibility(i);
        switch (i) {
            case 0:
                setAnimation(this.d);
                startAnimation(this.d);
                this.c = true;
                if (this.f != null) {
                    this.e.removeCallbacks(this.f);
                }
                this.f = new c(this);
                this.e.postDelayed(this.f, this.a);
                return;
            default:
                clearAnimation();
                if (this.f != null) {
                    this.e.removeCallbacks(this.f);
                }
                this.c = false;
                return;
        }
    }
}
