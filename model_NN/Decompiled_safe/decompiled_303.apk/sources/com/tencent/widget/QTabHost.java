package com.tencent.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TabHost;

public class QTabHost extends TabHost {
    /* access modifiers changed from: private */
    public QTabCursor a;

    public QTabHost(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setup() {
        super.setup();
        setOnTabChangedListener(new e(this));
    }
}
