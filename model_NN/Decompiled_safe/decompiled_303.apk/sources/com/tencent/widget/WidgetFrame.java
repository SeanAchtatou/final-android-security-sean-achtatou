package com.tencent.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.widget.LinearLayout;

public class WidgetFrame extends LinearLayout {
    /* access modifiers changed from: private */
    public boolean a = false;
    private b b = null;

    public WidgetFrame(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void cancelLongPress() {
        super.cancelLongPress();
        this.a = false;
        if (this.b != null) {
            removeCallbacks(this.b);
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.a) {
            this.a = false;
            return true;
        }
        switch (motionEvent.getAction()) {
            case 0:
                this.a = false;
                if (this.b == null) {
                    this.b = new b(this);
                }
                this.b.a();
                postDelayed(this.b, (long) ViewConfiguration.getLongPressTimeout());
                break;
            case 1:
            case 3:
                this.a = false;
                if (this.b != null) {
                    removeCallbacks(this.b);
                    break;
                }
                break;
        }
        return false;
    }
}
