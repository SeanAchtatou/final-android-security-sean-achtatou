package com.tencent.widget;

import android.view.animation.Interpolator;

final class g implements Interpolator {
    private /* synthetic */ f a;

    g(f fVar) {
        this.a = fVar;
    }

    public final float getInterpolation(float f) {
        float f2 = (1.55f * f) - 1.1f;
        return 1.2f - (f2 * f2);
    }
}
