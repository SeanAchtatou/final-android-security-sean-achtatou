package com.tencent.qphone.widget.soso;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import com.tencent.qqlauncher.R;

public class SosoWidgetPrefActivity extends PreferenceActivity {
    private final String ACTION_SOSO_UPDATE = "com.tencent.qqlauncher.widget.soso_hot";

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.xml.view_preference_screen);
        addPreferencesFromResource(R.xml.sosowidget_prefs);
        setTitleColor(-1);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        sendBroadcast(new Intent("com.tencent.qqlauncher.widget.soso_hot"));
        super.onStop();
    }
}
