package com.tencent.qphone.widget.soso;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

final class s implements View.OnClickListener {
    private /* synthetic */ SosoSearchMainActivity a;

    s(SosoSearchMainActivity sosoSearchMainActivity) {
        this.a = sosoSearchMainActivity;
    }

    public final void onClick(View view) {
        String str = "";
        try {
            this.a.searchText.setText("");
            str = "";
            this.a.refreshList(str);
        } catch (ActivityNotFoundException e) {
            Intent intent = new Intent();
            try {
                Uri parse = Uri.parse("http://wap.soso.com/s.q?type=sweb&st=input&g_f=5352&g_ut=3&biz=widget&key=" + URLEncoder.encode(str, "UTF-8"));
                intent.setAction("android.intent.action.VIEW");
                intent.setData(parse);
                intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
                intent.setFlags(268435456);
                this.a.startActivity(intent);
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
        }
    }
}
