package com.tencent.qphone.widget.soso;

import android.view.View;
import java.util.List;

final class q implements View.OnClickListener {
    private /* synthetic */ List a;
    private /* synthetic */ SosoSearchMainActivity b;

    q(SosoSearchMainActivity sosoSearchMainActivity, List list) {
        this.b = sosoSearchMainActivity;
        this.a = list;
    }

    public final void onClick(View view) {
        if (this.a.size() != 0) {
            this.b.showDeleteDialog();
        }
    }
}
