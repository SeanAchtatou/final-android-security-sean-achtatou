package com.tencent.qphone.widget.soso;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.tencent.qqlauncher.R;

public class ClearHistoryDialog extends Activity implements View.OnClickListener {
    public static final String FROM_WEB_SEARCH = "com.tencent.qphone.widget.soso.FROM_WEB_SEARCH";

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.delete_confirm /*2131492940*/:
                setResult(-1, null);
                finish();
                return;
            case R.id.delete_cancel /*2131492941*/:
                setResult(0, null);
                finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.delete_dialog);
        if (FROM_WEB_SEARCH.equals(getIntent().getAction())) {
            ((TextView) findViewById(R.id.msgTV)).setText((int) R.string.quit_web_search);
        }
        findViewById(R.id.delete_confirm).setOnClickListener(this);
        findViewById(R.id.delete_cancel).setOnClickListener(this);
    }
}
