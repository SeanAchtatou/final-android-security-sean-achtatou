package com.tencent.qphone.widget.soso;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.TextView;

public class AutoTextItem extends TextView {
    private Paint a;

    public AutoTextItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = null;
        this.a = new Paint(1);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        String obj = super.getText().toString();
        int length = SosoSearchMainActivity.sText != null ? SosoSearchMainActivity.sText.trim().length() : 0;
        if (length == 0) {
            this.a.setTextSize(16.0f);
            this.a.setColor(-16777216);
            canvas.drawText(obj, 0.0f, 15.0f, this.a);
            return;
        }
        try {
            if (length >= obj.length()) {
                length = obj.length();
            }
            this.a.setTextSize(16.0f);
            this.a.setColor(Color.parseColor("#2c82af"));
            canvas.drawText(obj.substring(0, length), 0.0f, 15.0f, this.a);
            this.a.setColor(-16777216);
            Rect rect = new Rect();
            this.a.getTextBounds(obj, 0, length, rect);
            canvas.drawText(obj.substring(length), (float) (rect.width() + 4), 15.0f, this.a);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
