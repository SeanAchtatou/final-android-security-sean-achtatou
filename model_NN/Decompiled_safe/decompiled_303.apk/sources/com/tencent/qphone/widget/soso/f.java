package com.tencent.qphone.widget.soso;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.qphone.widget.soso.a.a;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

final class f extends BaseAdapter {
    final /* synthetic */ SosoSearchMainActivity a;
    private Context b = null;
    private LayoutInflater c;
    private List d;
    private int e = 0;

    public f(SosoSearchMainActivity sosoSearchMainActivity, Context context) {
        this.a = sosoSearchMainActivity;
        this.b = context;
        this.c = (LayoutInflater) context.getSystemService("layout_inflater");
        this.d = new ArrayList();
    }

    public final void a(int i) {
        this.e = i;
        this.d.clear();
        if (i == 0) {
            this.d.addAll((Collection) this.a.searchResult.get(4));
            for (int i2 = 1; i2 < 4; i2++) {
                this.d.addAll((Collection) this.a.searchResult.get(Integer.valueOf(i2)));
            }
            return;
        }
        this.d.addAll((Collection) this.a.searchResult.get(Integer.valueOf(i)));
    }

    public final int getCount() {
        int i = 0;
        if (this.d != null) {
            i = this.d.size();
        }
        return this.e == 0 ? i + 1 : i;
    }

    public final Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        View inflate;
        String str;
        int i2;
        int i3 = this.e == 0 ? i - 1 : i;
        if (this.d == null || i3 < 0) {
            View inflate2 = this.c.inflate((int) R.layout.list_row_1, viewGroup, false);
            o oVar = new o(this.a);
            oVar.a = (TextView) inflate2.findViewById(R.id.title);
            oVar.c = (ImageView) inflate2.findViewById(R.id.icon);
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(this.a.getString(R.string.soso_connect_search) + "\"" + SosoSearchMainActivity.sText + "\"");
            oVar.a.setTextColor(this.a.getResources().getColor(R.color.blue));
            spannableStringBuilder.setSpan(new ForegroundColorSpan(this.a.getResources().getColor(R.color.color_red_text)), 5, SosoSearchMainActivity.sText.length() + 5, 34);
            oVar.a.setText(spannableStringBuilder);
            oVar.c.setImageResource(this.a.RES_ICON[3]);
            inflate2.setOnClickListener(new a(this));
            return inflate2;
        }
        a aVar = (a) this.d.get(i3);
        o oVar2 = new o(this.a);
        if (aVar.b() == null || aVar.b().length() != 0) {
            inflate = this.c.inflate((int) R.layout.list_row, viewGroup, false);
            oVar2.b = (TextView) inflate.findViewById(R.id.desc);
        } else {
            inflate = this.c.inflate((int) R.layout.list_row_1, viewGroup, false);
        }
        oVar2.a = (TextView) inflate.findViewById(R.id.title);
        oVar2.c = (ImageView) inflate.findViewById(R.id.icon);
        String a2 = aVar.a();
        SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(a2);
        int indexOf = a2.toLowerCase().indexOf(SosoSearchMainActivity.sText.toLowerCase());
        if (indexOf >= 0 && SosoSearchMainActivity.sText.trim().length() > 0) {
            spannableStringBuilder2.setSpan(new ForegroundColorSpan(this.a.getResources().getColor(R.color.color_red_text)), indexOf, SosoSearchMainActivity.sText.length() + indexOf, 34);
        }
        oVar2.a.setText(spannableStringBuilder2);
        if (!(aVar.b() == null || aVar.b().length() == 0)) {
            String b2 = aVar.b();
            int indexOf2 = b2.toLowerCase().indexOf(SosoSearchMainActivity.sText.toLowerCase());
            if (indexOf2 > 10) {
                String str2 = b2.length() > (SosoSearchMainActivity.sText.length() + indexOf2) + 10 ? "..." + b2.substring(indexOf2 - 10, indexOf2 + SosoSearchMainActivity.sText.length() + 10) + "..." : "..." + b2.substring(indexOf2 - 10);
                str = str2;
                i2 = str2.toLowerCase().indexOf(SosoSearchMainActivity.sText.toLowerCase());
            } else {
                int i4 = indexOf2;
                str = b2;
                i2 = i4;
            }
            SpannableStringBuilder spannableStringBuilder3 = new SpannableStringBuilder(str);
            if (i2 >= 0 && SosoSearchMainActivity.sText.trim().length() > 0) {
                spannableStringBuilder3.setSpan(new ForegroundColorSpan(this.a.getResources().getColor(R.color.color_red_text)), i2, SosoSearchMainActivity.sText.length() + i2, 34);
            }
            oVar2.b.setText(spannableStringBuilder3);
        }
        if (SosoSearchMainActivity.cur_Tab == 0) {
            oVar2.c.setImageResource(this.a.RES_ICON[aVar.d() - 1]);
        } else {
            oVar2.c.setVisibility(8);
        }
        inflate.setOnClickListener(new b(this, aVar));
        return inflate;
    }
}
