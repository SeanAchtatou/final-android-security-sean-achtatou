package com.tencent.qphone.widget.soso;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.qphone.widget.soso.a.a;
import com.tencent.qqlauncher.R;
import java.util.List;

final class e extends BaseAdapter {
    final /* synthetic */ SosoSearchMainActivity a;
    private LayoutInflater b;
    private List c;

    public e(SosoSearchMainActivity sosoSearchMainActivity, Context context, List list) {
        this.a = sosoSearchMainActivity;
        this.b = LayoutInflater.from(context);
        this.c = list;
    }

    public final int getCount() {
        return this.c.size();
    }

    public final Object getItem(int i) {
        return this.c;
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (i < this.c.size()) {
            View inflate = this.b.inflate((int) R.layout.list_row_1, viewGroup, false);
            o oVar = new o(this.a);
            oVar.a = (TextView) inflate.findViewById(R.id.title);
            oVar.c = (ImageView) inflate.findViewById(R.id.icon);
            a aVar = (a) this.c.get(i);
            oVar.c.setImageResource(this.a.RES_ICON[aVar.d() - 1]);
            String a2 = aVar.a();
            oVar.a.setText(a2);
            inflate.setOnClickListener(new n(this, aVar, a2));
            return inflate;
        }
        this.a.clearLinearLayout.setVisibility(8);
        return this.a.clearLinearLayout;
    }
}
