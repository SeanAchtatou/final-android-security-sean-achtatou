package com.tencent.qphone.widget.soso;

import android.webkit.WebView;
import android.webkit.WebViewClient;

final class j extends WebViewClient {
    private /* synthetic */ SearchResultActivity a;

    j(SearchResultActivity searchResultActivity) {
        this.a = searchResultActivity;
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        this.a.handler.sendEmptyMessage(0);
        webView.loadUrl(str);
        return true;
    }
}
