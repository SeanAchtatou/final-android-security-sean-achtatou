package com.tencent.qphone.widget.soso;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RemoteViews;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

public class SosoWidgetProvider extends AppWidgetProvider {
    private static ComponentName b = new ComponentName("com.tencent.qqlauncher", "com.tencent.qphone.widget.soso.SosoWidgetProvider");
    private final boolean a = false;
    private final String c = "http://wap.soso.com/top10/index.jsp?g_f=2158&g_ut=2&channel=soso_kw_newsword";
    private ArrayList d;

    private void a(Context context, AppWidgetManager appWidgetManager, int[] iArr) {
        for (int updateAppWidget : iArr) {
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.tencent_widget_soso_show);
            a(context, remoteViews);
            Intent intent = new Intent();
            intent.setClass(context, SosoSearchMainActivity.class);
            intent.setFlags(268435456);
            remoteViews.setOnClickPendingIntent(R.id.search_bkg_layout, PendingIntent.getActivity(context, 0, intent, 0));
            Intent intent2 = new Intent();
            intent2.setAction("com.tencent.qqlauncher.widget.soso.SEARCH_START_MOREHOT");
            remoteViews.setOnClickPendingIntent(R.id.more_key, PendingIntent.getBroadcast(context, 0, intent2, 0));
            appWidgetManager.updateAppWidget(updateAppWidget, remoteViews);
        }
    }

    private void a(Context context, RemoteViews remoteViews) {
        if (remoteViews == null) {
            return;
        }
        if (context.getSharedPreferences("com.tencent.qphone.widget.soso_preferences", 1).getBoolean("hot_words", true)) {
            remoteViews.setViewVisibility(R.id.search_hot_text, 0);
            if (this.d != null) {
                for (int i = 0; i < this.d.size(); i++) {
                    Intent intent = new Intent();
                    intent.setAction("com.tencent.qqlauncher.widget.soso.SEARCH_HOT_START_ACTION_" + i);
                    Bundle bundle = new Bundle();
                    bundle.putStringArrayList("USING_HOT_WORD", this.d);
                    intent.putExtras(bundle);
                    if (context != null) {
                        PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, intent, 134217728);
                        if (remoteViews != null) {
                            switch (i) {
                                case 0:
                                    remoteViews.setTextViewText(R.id.t1, (CharSequence) this.d.get(i));
                                    remoteViews.setOnClickPendingIntent(R.id.t1, broadcast);
                                    remoteViews.setViewVisibility(R.id.t1, 0);
                                    remoteViews.setViewVisibility(R.id.divider1, 0);
                                    continue;
                                case 1:
                                    remoteViews.setTextViewText(R.id.t2, (CharSequence) this.d.get(i));
                                    remoteViews.setOnClickPendingIntent(R.id.t2, broadcast);
                                    remoteViews.setViewVisibility(R.id.t2, 0);
                                    remoteViews.setViewVisibility(R.id.divider2, 0);
                                    continue;
                                case 2:
                                    remoteViews.setTextViewText(R.id.t3, (CharSequence) this.d.get(i));
                                    remoteViews.setOnClickPendingIntent(R.id.t3, broadcast);
                                    remoteViews.setViewVisibility(R.id.t3, 0);
                                    remoteViews.setViewVisibility(R.id.divider3, 0);
                                    continue;
                                case 3:
                                    remoteViews.setTextViewText(R.id.t4, (CharSequence) this.d.get(i));
                                    remoteViews.setOnClickPendingIntent(R.id.t4, broadcast);
                                    remoteViews.setViewVisibility(R.id.t4, 0);
                                    remoteViews.setViewVisibility(R.id.divider4, 0);
                                    continue;
                                case 4:
                                    remoteViews.setTextViewText(R.id.t5, (CharSequence) this.d.get(i));
                                    remoteViews.setViewVisibility(R.id.t5, 0);
                                    remoteViews.setOnClickPendingIntent(R.id.t5, broadcast);
                                    continue;
                            }
                        }
                    }
                }
                for (int size = this.d.size(); size < 5; size++) {
                    if (remoteViews != null) {
                        switch (size) {
                            case 0:
                                remoteViews.setTextViewText(R.id.t1, "");
                                remoteViews.setViewVisibility(R.id.t1, 4);
                                remoteViews.setViewVisibility(R.id.divider1, 4);
                                continue;
                            case 1:
                                remoteViews.setTextViewText(R.id.t2, "");
                                remoteViews.setViewVisibility(R.id.t2, 4);
                                remoteViews.setViewVisibility(R.id.divider2, 4);
                                continue;
                            case 2:
                                remoteViews.setTextViewText(R.id.t3, "");
                                remoteViews.setViewVisibility(R.id.t3, 4);
                                remoteViews.setViewVisibility(R.id.divider3, 4);
                                continue;
                            case 3:
                                remoteViews.setTextViewText(R.id.t4, "");
                                remoteViews.setViewVisibility(R.id.t4, 4);
                                remoteViews.setViewVisibility(R.id.divider4, 4);
                                continue;
                            case 4:
                                remoteViews.setTextViewText(R.id.t5, "");
                                remoteViews.setViewVisibility(R.id.t5, 4);
                                continue;
                        }
                    }
                }
                return;
            }
            return;
        }
        remoteViews.setViewVisibility(R.id.search_hot_text, 8);
    }

    public void onDisabled(Context context) {
        context.stopService(new Intent("com.tencent.qqlauncher.widget.soso.SOSO_SERVICE"));
        super.onDisabled(context);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00c4, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00c5, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00c4 A[ExcHandler: UnsupportedEncodingException (r0v24 'e' java.io.UnsupportedEncodingException A[CUSTOM_DECLARE]), Splitter:B:12:0x0072] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r5, android.content.Intent r6) {
        /*
            r4 = this;
            r3 = 268435456(0x10000000, float:2.5243549E-29)
            super.onReceive(r5, r6)
            java.lang.String r0 = r6.getAction()
            java.lang.String r1 = "com.tencent.qqlauncher.widget.soso.GLOBAL_SEARCH"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x001e
            android.content.Intent r0 = new android.content.Intent
            java.lang.Class<com.tencent.qphone.widget.soso.SosoSearchMainActivity> r1 = com.tencent.qphone.widget.soso.SosoSearchMainActivity.class
            r0.<init>(r5, r1)
            r0.setFlags(r3)
            r5.startActivity(r0)
        L_0x001e:
            java.lang.String r0 = r6.getAction()
            java.lang.String r1 = "com.tencent.qqlauncher.widget.soso.SEND_HOT"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0045
            java.lang.String r0 = "HOT_WORD"
            java.util.ArrayList r0 = r6.getStringArrayListExtra(r0)
            r4.d = r0
            android.appwidget.AppWidgetManager r0 = android.appwidget.AppWidgetManager.getInstance(r5)
            android.content.ComponentName r1 = new android.content.ComponentName
            java.lang.Class<com.tencent.qphone.widget.soso.SosoWidgetProvider> r2 = com.tencent.qphone.widget.soso.SosoWidgetProvider.class
            r1.<init>(r5, r2)
            int[] r1 = r0.getAppWidgetIds(r1)
            r4.a(r5, r0, r1)
        L_0x0044:
            return
        L_0x0045:
            java.lang.String r0 = r6.getAction()
            java.lang.String r1 = "com.tencent.qqlauncher.widget.soso_hot"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0064
            android.appwidget.AppWidgetManager r0 = android.appwidget.AppWidgetManager.getInstance(r5)
            android.content.ComponentName r1 = new android.content.ComponentName
            java.lang.Class<com.tencent.qphone.widget.soso.SosoWidgetProvider> r2 = com.tencent.qphone.widget.soso.SosoWidgetProvider.class
            r1.<init>(r5, r2)
            int[] r1 = r0.getAppWidgetIds(r1)
            r4.a(r5, r0, r1)
            goto L_0x0044
        L_0x0064:
            java.lang.String r0 = r6.getAction()
            java.lang.String r1 = "com.tencent.qqlauncher.widget.soso.SEARCH_HOT_START_ACTION"
            boolean r0 = r0.startsWith(r1)
            if (r0 == 0) goto L_0x010c
            java.lang.String r0 = ""
            java.lang.String r1 = r6.getAction()     // Catch:{ UnsupportedEncodingException -> 0x00c4, ActivityNotFoundException -> 0x00ca }
            java.lang.String r2 = r6.getAction()     // Catch:{ UnsupportedEncodingException -> 0x00c4, ActivityNotFoundException -> 0x00ca }
            int r2 = r2.length()     // Catch:{ UnsupportedEncodingException -> 0x00c4, ActivityNotFoundException -> 0x00ca }
            r3 = 1
            int r2 = r2 - r3
            java.lang.String r1 = r1.substring(r2)     // Catch:{ UnsupportedEncodingException -> 0x00c4, ActivityNotFoundException -> 0x00ca }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ UnsupportedEncodingException -> 0x00c4, ActivityNotFoundException -> 0x00ca }
            java.lang.String r2 = "USING_HOT_WORD"
            java.util.ArrayList r2 = r6.getStringArrayListExtra(r2)     // Catch:{ UnsupportedEncodingException -> 0x00c4, ActivityNotFoundException -> 0x00ca }
            java.lang.Object r4 = r2.get(r1)     // Catch:{ UnsupportedEncodingException -> 0x00c4, ActivityNotFoundException -> 0x00ca }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ UnsupportedEncodingException -> 0x00c4, ActivityNotFoundException -> 0x00ca }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x00c4, ActivityNotFoundException -> 0x0155 }
            r0.<init>()     // Catch:{ UnsupportedEncodingException -> 0x00c4, ActivityNotFoundException -> 0x0155 }
            java.lang.String r1 = "http://wap.soso.com/s.q?type=sweb&st=input&g_f=5352&g_ut=3&biz=widget&key="
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ UnsupportedEncodingException -> 0x00c4, ActivityNotFoundException -> 0x0155 }
            java.lang.String r1 = "UTF-8"
            java.lang.String r1 = java.net.URLEncoder.encode(r4, r1)     // Catch:{ UnsupportedEncodingException -> 0x00c4, ActivityNotFoundException -> 0x0155 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ UnsupportedEncodingException -> 0x00c4, ActivityNotFoundException -> 0x0155 }
            java.lang.String r0 = r0.toString()     // Catch:{ UnsupportedEncodingException -> 0x00c4, ActivityNotFoundException -> 0x0155 }
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch:{ UnsupportedEncodingException -> 0x00c4, ActivityNotFoundException -> 0x0155 }
            android.content.Intent r1 = new android.content.Intent     // Catch:{ UnsupportedEncodingException -> 0x00c4, ActivityNotFoundException -> 0x0155 }
            java.lang.Class<com.tencent.qphone.widget.soso.SearchResultActivity> r2 = com.tencent.qphone.widget.soso.SearchResultActivity.class
            r1.<init>(r5, r2)     // Catch:{ UnsupportedEncodingException -> 0x00c4, ActivityNotFoundException -> 0x0155 }
            r1.setData(r0)     // Catch:{ UnsupportedEncodingException -> 0x00c4, ActivityNotFoundException -> 0x0155 }
            r0 = 335544320(0x14000000, float:6.4623485E-27)
            r1.setFlags(r0)     // Catch:{ UnsupportedEncodingException -> 0x00c4, ActivityNotFoundException -> 0x0155 }
            r5.startActivity(r1)     // Catch:{ UnsupportedEncodingException -> 0x00c4, ActivityNotFoundException -> 0x0155 }
            goto L_0x0044
        L_0x00c4:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0044
        L_0x00ca:
            r1 = move-exception
        L_0x00cb:
            android.content.Intent r1 = new android.content.Intent
            r1.<init>()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x0106 }
            r2.<init>()     // Catch:{ UnsupportedEncodingException -> 0x0106 }
            java.lang.String r3 = "http://wap.soso.com/s.q?type=sweb&st=input&g_f=5352&g_ut=3&biz=widget&key="
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x0106 }
            java.lang.String r3 = "UTF-8"
            java.lang.String r0 = java.net.URLEncoder.encode(r0, r3)     // Catch:{ UnsupportedEncodingException -> 0x0106 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x0106 }
            java.lang.String r0 = r0.toString()     // Catch:{ UnsupportedEncodingException -> 0x0106 }
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch:{ UnsupportedEncodingException -> 0x0106 }
            java.lang.String r2 = "android.intent.action.VIEW"
            r1.setAction(r2)     // Catch:{ UnsupportedEncodingException -> 0x0106 }
            r1.setData(r0)     // Catch:{ UnsupportedEncodingException -> 0x0106 }
            java.lang.String r0 = "com.android.browser"
            java.lang.String r2 = "com.android.browser.BrowserActivity"
            r1.setClassName(r0, r2)     // Catch:{ UnsupportedEncodingException -> 0x0106 }
            r0 = 268435456(0x10000000, float:2.5243549E-29)
            r1.setFlags(r0)     // Catch:{ UnsupportedEncodingException -> 0x0106 }
            r5.startActivity(r1)     // Catch:{ UnsupportedEncodingException -> 0x0106 }
            goto L_0x0044
        L_0x0106:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0044
        L_0x010c:
            java.lang.String r0 = r6.getAction()
            java.lang.String r1 = "com.tencent.qqlauncher.widget.soso.SEARCH_START_MOREHOT"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0044
            java.lang.String r0 = "http://wap.soso.com/top10/index.jsp?g_f=2158&g_ut=2&channel=soso_kw_newsword"
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch:{ ActivityNotFoundException -> 0x0132 }
            android.content.Intent r1 = new android.content.Intent     // Catch:{ ActivityNotFoundException -> 0x0132 }
            java.lang.Class<com.tencent.qphone.widget.soso.SearchResultActivity> r2 = com.tencent.qphone.widget.soso.SearchResultActivity.class
            r1.<init>(r5, r2)     // Catch:{ ActivityNotFoundException -> 0x0132 }
            r1.setData(r0)     // Catch:{ ActivityNotFoundException -> 0x0132 }
            r0 = 335544320(0x14000000, float:6.4623485E-27)
            r1.setFlags(r0)     // Catch:{ ActivityNotFoundException -> 0x0132 }
            r5.startActivity(r1)     // Catch:{ ActivityNotFoundException -> 0x0132 }
            goto L_0x0044
        L_0x0132:
            r0 = move-exception
            android.content.Intent r0 = new android.content.Intent
            r0.<init>()
            java.lang.String r1 = "http://wap.soso.com/top10/index.jsp?g_f=2158&g_ut=2&channel=soso_kw_newsword"
            android.net.Uri r1 = android.net.Uri.parse(r1)
            java.lang.String r2 = "android.intent.action.VIEW"
            r0.setAction(r2)
            r0.setData(r1)
            java.lang.String r1 = "com.android.browser"
            java.lang.String r2 = "com.android.browser.BrowserActivity"
            r0.setClassName(r1, r2)
            r0.setFlags(r3)
            r5.startActivity(r0)
            goto L_0x0044
        L_0x0155:
            r0 = move-exception
            r0 = r4
            goto L_0x00cb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qphone.widget.soso.SosoWidgetProvider.onReceive(android.content.Context, android.content.Intent):void");
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] iArr) {
        a(context, appWidgetManager, iArr);
        context.startService(new Intent("com.tencent.qqlauncher.widget.soso.SOSO_SERVICE"));
    }
}
