package com.tencent.qphone.widget.soso;

import android.view.View;
import com.tencent.qphone.widget.soso.a.a;

final class n implements View.OnClickListener {
    private /* synthetic */ a a;
    private /* synthetic */ String b;
    private /* synthetic */ e c;

    n(e eVar, a aVar, String str) {
        this.c = eVar;
        this.a = aVar;
        this.b = str;
    }

    public final void onClick(View view) {
        if (this.a.d() == 1) {
            this.c.a.startBrowserIntent(this.a.a(), this.a.c(), this.a.d(), this.a.b());
            return;
        }
        SosoSearchMainActivity.sText = this.b;
        this.c.a.searchText.setText(SosoSearchMainActivity.sText);
        this.c.a.searchText.setSelection(SosoSearchMainActivity.sText.length());
    }
}
