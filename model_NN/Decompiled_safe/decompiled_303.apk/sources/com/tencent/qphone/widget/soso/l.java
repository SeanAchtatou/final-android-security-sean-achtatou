package com.tencent.qphone.widget.soso;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import com.tencent.qphone.widget.soso.a.a;
import java.util.ArrayList;

public final class l {
    private d a = this.a.a();

    public l(Context context) {
        this.a = new d(context);
    }

    public final ArrayList a(String str) {
        String str2;
        ArrayList arrayList = new ArrayList();
        if (str != null) {
            str2 = "_id like " + DatabaseUtils.sqlEscapeString(str + "%").replace("_", "\\_") + " escape '\\'";
        } else {
            str2 = str;
        }
        Cursor a2 = this.a.a(str2);
        if (a2 != null && a2.getCount() > 0) {
            a2.moveToFirst();
            for (int i = 0; i < a2.getCount(); i++) {
                a aVar = new a();
                aVar.a(a2.getInt(1));
                aVar.a(a2.getString(0));
                aVar.b(a2.getString(2));
                aVar.a(Uri.parse(a2.getString(3)));
                arrayList.add(aVar);
                a2.moveToNext();
            }
        }
        a2.close();
        return arrayList;
    }

    public final void a() {
        this.a.c();
    }

    public final void a(String str, String str2, String str3) {
        this.a.a(str, str2, str3);
    }

    public final void b() {
        this.a.b();
    }
}
