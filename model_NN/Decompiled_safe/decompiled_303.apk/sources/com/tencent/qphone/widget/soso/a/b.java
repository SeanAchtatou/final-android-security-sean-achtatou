package com.tencent.qphone.widget.soso.a;

import android.content.Context;
import java.util.ArrayList;
import java.util.List;

public final class b {
    private static String[] a = {"全部", "联系人", "音乐", "短信"};

    /* JADX WARNING: Removed duplicated region for block: B:22:0x00ba  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.util.List a(android.content.Context r10, java.lang.String r11) {
        /*
            r5 = 1
            r4 = 2
            r8 = 0
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            r0 = 6
            java.lang.String[] r2 = new java.lang.String[r0]
            java.lang.String r0 = "_id"
            r2[r8] = r0
            java.lang.String r0 = "artist"
            r2[r5] = r0
            java.lang.String r0 = "album"
            r2[r4] = r0
            r0 = 3
            java.lang.String r1 = "_display_name"
            r2[r0] = r1
            r0 = 4
            java.lang.String r1 = "_data"
            r2[r0] = r1
            r0 = 5
            java.lang.String r1 = "duration"
            r2[r0] = r1
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "%"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r11)
            java.lang.String r1 = "%"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.String r3 = "_display_name  LIKE ? or artist  LIKE ?"
            java.lang.String[] r4 = new java.lang.String[r4]
            r4[r8] = r0
            r4[r5] = r0
            java.lang.String r5 = "_display_name COLLATE LOCALIZED ASC"
            r7 = 0
            android.content.ContentResolver r0 = r10.getContentResolver()     // Catch:{ Exception -> 0x00ae, all -> 0x00b6 }
            android.net.Uri r1 = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI     // Catch:{ Exception -> 0x00ae, all -> 0x00b6 }
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x00ae, all -> 0x00b6 }
            if (r0 != 0) goto L_0x005d
            if (r0 == 0) goto L_0x005b
            r0.close()
        L_0x005b:
            r0 = r6
        L_0x005c:
            return r0
        L_0x005d:
            int r1 = r0.getCount()     // Catch:{ Exception -> 0x00c3, all -> 0x00be }
            r2 = r8
        L_0x0062:
            if (r2 >= r1) goto L_0x00a7
            r0.moveToPosition(r2)     // Catch:{ Exception -> 0x00c3, all -> 0x00be }
            com.tencent.qphone.widget.soso.a.a r3 = new com.tencent.qphone.widget.soso.a.a     // Catch:{ Exception -> 0x00c3, all -> 0x00be }
            r3.<init>()     // Catch:{ Exception -> 0x00c3, all -> 0x00be }
            r4 = 2
            r3.a(r4)     // Catch:{ Exception -> 0x00c3, all -> 0x00be }
            java.lang.String r4 = "_display_name"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x00c3, all -> 0x00be }
            java.lang.String r4 = r0.getString(r4)     // Catch:{ Exception -> 0x00c3, all -> 0x00be }
            r3.a(r4)     // Catch:{ Exception -> 0x00c3, all -> 0x00be }
            java.lang.String r4 = "artist"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x00c3, all -> 0x00be }
            java.lang.String r4 = r0.getString(r4)     // Catch:{ Exception -> 0x00c3, all -> 0x00be }
            r3.b(r4)     // Catch:{ Exception -> 0x00c3, all -> 0x00be }
            java.lang.String r4 = "_id"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x00c3, all -> 0x00be }
            long r4 = r0.getLong(r4)     // Catch:{ Exception -> 0x00c3, all -> 0x00be }
            android.net.Uri r7 = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI     // Catch:{ Exception -> 0x00c3, all -> 0x00be }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x00c3, all -> 0x00be }
            android.net.Uri r4 = android.net.Uri.withAppendedPath(r7, r4)     // Catch:{ Exception -> 0x00c3, all -> 0x00be }
            r3.a(r4)     // Catch:{ Exception -> 0x00c3, all -> 0x00be }
            r6.add(r3)     // Catch:{ Exception -> 0x00c3, all -> 0x00be }
            int r2 = r2 + 1
            goto L_0x0062
        L_0x00a7:
            if (r0 == 0) goto L_0x00ac
            r0.close()
        L_0x00ac:
            r0 = r6
            goto L_0x005c
        L_0x00ae:
            r0 = move-exception
            r0 = r7
        L_0x00b0:
            if (r0 == 0) goto L_0x00ac
            r0.close()
            goto L_0x00ac
        L_0x00b6:
            r0 = move-exception
            r1 = r7
        L_0x00b8:
            if (r1 == 0) goto L_0x00bd
            r1.close()
        L_0x00bd:
            throw r0
        L_0x00be:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x00b8
        L_0x00c3:
            r1 = move-exception
            goto L_0x00b0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qphone.widget.soso.a.b.a(android.content.Context, java.lang.String):java.util.List");
    }

    public static List a(Context context, String str, int i) {
        ArrayList arrayList = new ArrayList();
        switch (i) {
            case 0:
                arrayList.addAll(a(context, str));
                arrayList.addAll(b(context, str));
                arrayList.addAll(c(context, str));
                return arrayList;
            case 1:
                return c(context, str);
            case 2:
                return a(context, str);
            case 3:
                return b(context, str);
            default:
                return arrayList;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x009e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.util.List b(android.content.Context r11, java.lang.String r12) {
        /*
            r8 = 0
            r7 = 0
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "%"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r12)
            java.lang.String r1 = "%"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.String r3 = "body  LIKE ? or address like ?"
            r1 = 2
            java.lang.String[] r4 = new java.lang.String[r1]
            r4[r8] = r0
            r1 = 1
            r4[r1] = r0
            android.content.ContentResolver r0 = r11.getContentResolver()     // Catch:{ Exception -> 0x0092, all -> 0x009a }
            java.lang.String r1 = "content://sms"
            android.net.Uri r1 = android.net.Uri.parse(r1)     // Catch:{ Exception -> 0x0092, all -> 0x009a }
            r2 = 0
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0092, all -> 0x009a }
            int r1 = r0.getCount()     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            r2 = r8
        L_0x003f:
            if (r2 >= r1) goto L_0x008c
            r0.moveToPosition(r2)     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            com.tencent.qphone.widget.soso.a.a r3 = new com.tencent.qphone.widget.soso.a.a     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            r3.<init>()     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            r4 = 3
            r3.a(r4)     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            java.lang.String r4 = "address"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            java.lang.String r4 = r0.getString(r4)     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            r3.a(r4)     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            java.lang.String r5 = "body"
            int r5 = r0.getColumnIndex(r5)     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            java.lang.String r5 = r0.getString(r5)     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            r3.b(r5)     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            java.lang.String r7 = "thread_id"
            int r7 = r0.getColumnIndex(r7)     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            long r7 = r0.getLong(r7)     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            java.lang.String r9 = "content://mms-sms/conversations"
            android.net.Uri r9 = android.net.Uri.parse(r9)     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            java.lang.String r7 = java.lang.String.valueOf(r7)     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            android.net.Uri r7 = android.net.Uri.withAppendedPath(r9, r7)     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            r3.a(r7)     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            if (r4 == 0) goto L_0x0089
            if (r5 == 0) goto L_0x0089
            r6.add(r3)     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
        L_0x0089:
            int r2 = r2 + 1
            goto L_0x003f
        L_0x008c:
            if (r0 == 0) goto L_0x0091
            r0.close()
        L_0x0091:
            return r6
        L_0x0092:
            r0 = move-exception
            r0 = r7
        L_0x0094:
            if (r0 == 0) goto L_0x0091
            r0.close()
            goto L_0x0091
        L_0x009a:
            r0 = move-exception
            r1 = r7
        L_0x009c:
            if (r1 == 0) goto L_0x00a1
            r1.close()
        L_0x00a1:
            throw r0
        L_0x00a2:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x009c
        L_0x00a7:
            r1 = move-exception
            goto L_0x0094
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qphone.widget.soso.a.b.b(android.content.Context, java.lang.String):java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0096  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.util.List c(android.content.Context r10, java.lang.String r11) {
        /*
            r2 = 1
            r8 = 0
            r7 = 0
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "%"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r11)
            java.lang.String r1 = "%"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.String r3 = "data1  LIKE ? or display_name  LIKE ?"
            r1 = 2
            java.lang.String[] r4 = new java.lang.String[r1]
            r4[r8] = r0
            r4[r2] = r0
            android.content.ContentResolver r0 = r10.getContentResolver()     // Catch:{ Exception -> 0x008a, all -> 0x0092 }
            java.lang.String r1 = "content://com.android.contacts/data/phones"
            android.net.Uri r1 = android.net.Uri.parse(r1)     // Catch:{ Exception -> 0x008a, all -> 0x0092 }
            r2 = 0
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x008a, all -> 0x0092 }
            int r1 = r0.getCount()     // Catch:{ Exception -> 0x009f, all -> 0x009a }
            r2 = r8
        L_0x003f:
            if (r2 >= r1) goto L_0x0084
            r0.moveToPosition(r2)     // Catch:{ Exception -> 0x009f, all -> 0x009a }
            com.tencent.qphone.widget.soso.a.a r3 = new com.tencent.qphone.widget.soso.a.a     // Catch:{ Exception -> 0x009f, all -> 0x009a }
            r3.<init>()     // Catch:{ Exception -> 0x009f, all -> 0x009a }
            r4 = 1
            r3.a(r4)     // Catch:{ Exception -> 0x009f, all -> 0x009a }
            java.lang.String r4 = "display_name"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x009f, all -> 0x009a }
            java.lang.String r4 = r0.getString(r4)     // Catch:{ Exception -> 0x009f, all -> 0x009a }
            r3.a(r4)     // Catch:{ Exception -> 0x009f, all -> 0x009a }
            java.lang.String r4 = "data1"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x009f, all -> 0x009a }
            java.lang.String r4 = r0.getString(r4)     // Catch:{ Exception -> 0x009f, all -> 0x009a }
            r3.b(r4)     // Catch:{ Exception -> 0x009f, all -> 0x009a }
            java.lang.String r4 = "contact_id"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x009f, all -> 0x009a }
            long r4 = r0.getLong(r4)     // Catch:{ Exception -> 0x009f, all -> 0x009a }
            android.net.Uri r7 = android.provider.ContactsContract.Contacts.CONTENT_URI     // Catch:{ Exception -> 0x009f, all -> 0x009a }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x009f, all -> 0x009a }
            android.net.Uri r4 = android.net.Uri.withAppendedPath(r7, r4)     // Catch:{ Exception -> 0x009f, all -> 0x009a }
            r3.a(r4)     // Catch:{ Exception -> 0x009f, all -> 0x009a }
            r6.add(r3)     // Catch:{ Exception -> 0x009f, all -> 0x009a }
            int r2 = r2 + 1
            goto L_0x003f
        L_0x0084:
            if (r0 == 0) goto L_0x0089
            r0.close()
        L_0x0089:
            return r6
        L_0x008a:
            r0 = move-exception
            r0 = r7
        L_0x008c:
            if (r0 == 0) goto L_0x0089
            r0.close()
            goto L_0x0089
        L_0x0092:
            r0 = move-exception
            r1 = r7
        L_0x0094:
            if (r1 == 0) goto L_0x0099
            r1.close()
        L_0x0099:
            throw r0
        L_0x009a:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0094
        L_0x009f:
            r1 = move-exception
            goto L_0x008c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qphone.widget.soso.a.b.c(android.content.Context, java.lang.String):java.util.List");
    }
}
