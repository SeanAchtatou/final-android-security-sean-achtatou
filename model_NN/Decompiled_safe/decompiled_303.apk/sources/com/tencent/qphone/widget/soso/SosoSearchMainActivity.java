package com.tencent.qphone.widget.soso;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.tencent.qphone.widget.soso.a.b;
import com.tencent.qqlauncher.R;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class SosoSearchMainActivity extends Activity {
    /* access modifiers changed from: private */
    public static final int[] BTN_ARRAY = {R.id.browserBtn, R.id.contactBtn, R.id.musicBtn, R.id.smsBtn};
    private static final int MAX_TXT_CNT = 10;
    private static final int REQUEST_CODE = 10001;
    /* access modifiers changed from: private */
    public static String SEARCH_ENGINE = SOSO_WEB_URI_PRE;
    private static final String SOSO_WEB_URI_PRE = "http://wap.soso.com/s.q?type=sweb&st=input&g_f=5352&g_ut=3&biz=widget&key=";
    /* access modifiers changed from: private */
    public static int cur_Tab = 0;
    public static String sText = "";
    /* access modifiers changed from: private */
    public final int[] RES_ICON = {R.drawable.contact, R.drawable.music, R.drawable.sms, R.drawable.browser};
    /* access modifiers changed from: private */
    public ImageButton clearBtn;
    LinearLayout clearLinearLayout;
    ListView listview;
    /* access modifiers changed from: private */
    public BaseAdapter mAdapter;
    private l mDBConnector;
    private View.OnClickListener searchBtnListener = new s(this);
    /* access modifiers changed from: private */
    public Hashtable searchResult = new Hashtable();
    /* access modifiers changed from: private */
    public EditText searchText;
    /* access modifiers changed from: private */
    public RelativeLayout tablayout;

    private void cleanBtnBg() {
        ((Button) findViewById(BTN_ARRAY[cur_Tab])).setBackgroundColor(17170443);
    }

    private void initButton() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < BTN_ARRAY.length) {
                Button button = (Button) findViewById(BTN_ARRAY[i2]);
                if (i2 == 0) {
                    button.setBackgroundResource(R.drawable.tab_selected_left);
                }
                button.setOnClickListener(new r(this, i2, button));
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void refreshList(String str) {
        this.searchResult.clear();
        if (str.equals("") || str.trim().length() == 0) {
            ArrayList a = this.mDBConnector.a(null);
            this.searchResult.put(4, a);
            this.mAdapter = new e(this, this, a);
            ((Button) this.clearLinearLayout.findViewById(R.id.clear_all_btn)).setOnClickListener(new q(this, a));
            if (a.size() == 0) {
                this.clearLinearLayout.setVisibility(8);
            } else {
                this.clearLinearLayout.setVisibility(0);
            }
        } else {
            this.clearLinearLayout.setVisibility(8);
            this.searchResult.put(4, this.mDBConnector.a(str));
            this.searchResult.put(1, b.a(this, str, 1));
            this.searchResult.put(2, b.a(this, str, 2));
            this.searchResult.put(3, b.a(this, str, 3));
            setBtnEnable();
            this.mAdapter = new f(this, this);
            ((f) this.mAdapter).a(cur_Tab);
        }
        this.listview.setAdapter((ListAdapter) this.mAdapter);
        this.mAdapter.notifyDataSetChanged();
    }

    private void setBtnEnable() {
        for (int i = 1; i < BTN_ARRAY.length; i++) {
            Button button = (Button) findViewById(BTN_ARRAY[i]);
            if (((List) this.searchResult.get(Integer.valueOf(i))).size() > 0) {
                button.setTextColor(-16777216);
            } else {
                button.setTextColor((int) R.color.color_disable);
                if (cur_Tab == i) {
                    ((Button) findViewById(BTN_ARRAY[cur_Tab])).setBackgroundColor(17170443);
                    cur_Tab = 0;
                    Button button2 = (Button) findViewById(BTN_ARRAY[0]);
                    if (cur_Tab == 0) {
                        button2.setBackgroundResource(R.drawable.tab_selected_left);
                    } else if (cur_Tab == BTN_ARRAY.length - 1) {
                        button2.setBackgroundResource(R.drawable.tab_selected_right);
                    } else {
                        button2.setBackgroundResource(R.drawable.tab_selected_mid);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void startBrowserIntent(String str) {
        try {
            this.mDBConnector.a(str, "", "");
            Uri parse = Uri.parse(SEARCH_ENGINE + URLEncoder.encode(str, "UTF-8"));
            Intent intent = new Intent(this, SearchResultActivity.class);
            intent.setData(parse);
            startActivityForResult(intent, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void startBrowserIntent(String str, Uri uri, int i, String str2) {
        if (i == 4) {
            try {
                Intent intent = new Intent(this, SearchResultActivity.class);
                intent.setData(uri);
                startActivityForResult(intent, 1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            startActivity(new Intent("android.intent.action.VIEW", uri));
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (REQUEST_CODE == i && -1 == i2) {
            this.mDBConnector.a();
            refreshList("");
            Toast.makeText(this, (int) R.string.deleted_hint, 0).show();
            return;
        }
        switch (i2) {
            case -1:
                finish();
                break;
        }
        super.onActivityResult(i, i2, intent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.main_search);
        this.tablayout = (RelativeLayout) findViewById(R.id.tablayout);
        this.mDBConnector = new l(this);
        ArrayList a = this.mDBConnector.a(null);
        this.searchResult.put(4, a);
        this.mAdapter = new e(this, this, a);
        this.clearLinearLayout = (LinearLayout) findViewById(R.id.clear_bottom_btn);
        ((Button) this.clearLinearLayout.findViewById(R.id.clear_all_btn)).setOnClickListener(new t(this, a));
        if (a.size() == 0) {
            this.clearLinearLayout.setVisibility(8);
        } else {
            this.clearLinearLayout.setVisibility(0);
        }
        this.searchText = (EditText) findViewById(R.id.search_text);
        this.searchText.addTextChangedListener(new c(this));
        this.searchText.setSelection(0);
        this.listview = (ListView) findViewById(R.id.search_list);
        this.listview.setAdapter((ListAdapter) this.mAdapter);
        cur_Tab = 0;
        this.clearBtn = (ImageButton) findViewById(R.id.clear_btn);
        this.clearBtn.setOnClickListener(this.searchBtnListener);
        initButton();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.mDBConnector.b();
        sText = "";
    }

    public boolean onMenuItemSelected(int i, MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.home /*2131493372*/:
                finish();
                break;
            case R.id.exit /*2131493373*/:
                setResult(-1, new Intent());
                finish();
                break;
        }
        return super.onMenuItemSelected(i, menuItem);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.getItem(0).setVisible(false);
        return super.onPrepareOptionsMenu(menu);
    }

    /* access modifiers changed from: protected */
    public void showDeleteDialog() {
        startActivityForResult(new Intent(this, ClearHistoryDialog.class), REQUEST_CODE);
    }
}
