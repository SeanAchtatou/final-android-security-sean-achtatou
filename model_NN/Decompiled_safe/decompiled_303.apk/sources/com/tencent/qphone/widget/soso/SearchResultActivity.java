package com.tencent.qphone.widget.soso;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import com.tencent.qqlauncher.R;

public class SearchResultActivity extends Activity {
    private static final int REQUEST_CODE = 10002;
    private static final int REQUEST_CODE_BACKKEY = 10003;
    Handler handler;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public WebView mWebView;
    ProgressBar pb;

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (REQUEST_CODE == i && -1 == i2) {
            setResult(-1, null);
            finish();
        } else if (REQUEST_CODE_BACKKEY == i && -1 == i2) {
            finish();
        } else {
            super.onActivityResult(i, i2, intent);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        Uri data = getIntent().getData();
        setContentView((int) R.layout.soso_main);
        this.pb = (ProgressBar) findViewById(R.id.loading);
        this.mWebView = (WebView) findViewById(R.id.webview);
        WebSettings settings = this.mWebView.getSettings();
        settings.setSavePassword(false);
        settings.setSaveFormData(false);
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(false);
        this.mWebView.setScrollBarStyle(0);
        this.mWebView.requestFocus();
        this.mWebView.setWebViewClient(new j(this));
        this.mWebView.setWebChromeClient(new h(this));
        this.mWebView.setDownloadListener(new i(this));
        this.mWebView.loadUrl(data.toString());
        this.handler = new g(this);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        return true;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4 && this.mWebView.canGoBack()) {
            this.mWebView.goBack();
            return true;
        } else if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        } else {
            finish();
            return true;
        }
    }

    public boolean onMenuItemSelected(int i, MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.home /*2131493372*/:
                Intent intent = new Intent(this, SosoSearchMainActivity.class);
                intent.setFlags(131072);
                startActivity(intent);
                finish();
                break;
            case R.id.exit /*2131493373*/:
                setResult(-1, null);
                finish();
                break;
        }
        return super.onMenuItemSelected(i, menuItem);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        WebView.enablePlatformNotifications();
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        WebView.disablePlatformNotifications();
        super.onStop();
    }
}
