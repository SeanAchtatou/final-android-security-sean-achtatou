package com.tencent.qphone.widget.soso;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Xml;
import com.tencent.qqlauncher.R;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.StringTokenizer;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class SosoService extends Service {
    private final boolean a = false;
    private final String b = "SosoService";
    private final String c = "http://wap.soso.com/zxcvb/hotwords.jsp?channel=qq_browser";
    private final String d = "SOSO_WIDGET";
    private final String e = "hots";
    private final String f = "isfirst";
    private final String g = "#";
    private final String h = "com.tencent.qqlauncher.widget.soso.SEND_HOT";
    private final int i = 12;
    private final int j = 0;
    private final int k = 1;
    private int l = 0;
    private ArrayList m = new ArrayList();
    private ArrayList n = new ArrayList();
    private ArrayList o = new ArrayList();
    private ArrayList p = new ArrayList();
    private SharedPreferences q;
    private p r;
    /* access modifiers changed from: private */
    public boolean s = true;
    private AlarmManager t;
    /* access modifiers changed from: private */
    public Runnable u = new m(this);

    private static String a(ArrayList arrayList) {
        String str = "";
        if (arrayList.size() <= 1) {
            return arrayList.size() == 1 ? (String) arrayList.get(0) : str;
        }
        for (int i2 = 0; i2 < arrayList.size() - 1; i2++) {
            str = (str + ((String) arrayList.get(i2))) + "#";
        }
        return str + ((String) arrayList.get(arrayList.size() - 1));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
        if (r0 != false) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.ArrayList a(boolean r5) {
        /*
            r4 = this;
            r3 = 1
            if (r5 != 0) goto L_0x0015
            r0 = 0
            java.util.Calendar r1 = java.util.Calendar.getInstance()
            r2 = 10
            int r1 = r1.get(r2)
            int r1 = r1 % 8
            if (r1 != 0) goto L_0x0013
            r0 = r3
        L_0x0013:
            if (r0 == 0) goto L_0x0031
        L_0x0015:
            boolean r0 = r4.a()
            if (r0 == 0) goto L_0x0031
            r4.l = r3
            android.content.SharedPreferences r0 = r4.q
            android.content.SharedPreferences$Editor r0 = r0.edit()
            java.lang.String r1 = "hots"
            java.util.ArrayList r2 = r4.o
            java.lang.String r2 = a(r2)
            r0.putString(r1, r2)
            r0.commit()
        L_0x0031:
            int r0 = r4.l
            if (r0 != r3) goto L_0x0038
            java.util.ArrayList r0 = r4.o
        L_0x0037:
            return r0
        L_0x0038:
            r0 = 0
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qphone.widget.soso.SosoService.a(boolean):java.util.ArrayList");
    }

    static /* synthetic */ void a(SosoService sosoService, boolean z) {
        "sendNewList:" + z;
        ArrayList a2 = sosoService.a(z);
        if (a2 != null) {
            sosoService.p = b(a2);
            Intent intent = new Intent("com.tencent.qqlauncher.widget.soso.SEND_HOT");
            intent.putStringArrayListExtra("HOT_WORD", sosoService.p);
            sosoService.sendBroadcast(intent);
        }
    }

    private boolean a() {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        String str;
        String str2;
        String substring;
        String substring2;
        ArrayList arrayList = new ArrayList();
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
            boolean z6 = (activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null || (!activeNetworkInfo.getExtraInfo().equalsIgnoreCase("cmwap") && !activeNetworkInfo.getExtraInfo().equalsIgnoreCase("uniwap") && !activeNetworkInfo.getExtraInfo().equalsIgnoreCase("3gwap"))) ? false : true;
            if (z6) {
                int length = "http://".length();
                int indexOf = "http://wap.soso.com/zxcvb/hotwords.jsp?channel=qq_browser".indexOf("/", length);
                if (indexOf < 0) {
                    substring = "http://wap.soso.com/zxcvb/hotwords.jsp?channel=qq_browser".substring(length);
                    substring2 = "";
                } else {
                    substring = "http://wap.soso.com/zxcvb/hotwords.jsp?channel=qq_browser".substring(length, indexOf);
                    substring2 = "http://wap.soso.com/zxcvb/hotwords.jsp?channel=qq_browser".substring(indexOf);
                }
                String str3 = "http://10.0.0.172:80" + substring2;
                str2 = substring;
                str = str3;
            } else {
                str = "http://wap.soso.com/zxcvb/hotwords.jsp?channel=qq_browser";
                str2 = null;
            }
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            if (z6) {
                httpURLConnection.setRequestProperty("X-Online-Host", str2);
            }
            httpURLConnection.setConnectTimeout(5000);
            httpURLConnection.connect();
            InputStream inputStream = httpURLConnection.getInputStream();
            XmlPullParser newPullParser = Xml.newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            boolean z7 = false;
            for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
                switch (eventType) {
                    case 2:
                        if (!"word".equals(newPullParser.getName())) {
                            break;
                        } else {
                            z7 = true;
                            break;
                        }
                    case 3:
                        if (!"word".equals(newPullParser.getName())) {
                            break;
                        } else {
                            z7 = false;
                            break;
                        }
                    case 4:
                        if (!z7) {
                            break;
                        } else {
                            String text = newPullParser.getText();
                            if (arrayList.indexOf(text) >= 0) {
                                break;
                            } else {
                                arrayList.add(text);
                                break;
                            }
                        }
                }
            }
            this.o = arrayList;
            try {
                getBaseContext().getSharedPreferences("soso", 0).edit().putLong("lastGetTime", System.currentTimeMillis()).commit();
                return true;
            } catch (MalformedURLException e2) {
                e = e2;
                z5 = true;
                e.printStackTrace();
                return z5;
            } catch (SocketTimeoutException e3) {
                e = e3;
                z4 = true;
                e.printStackTrace();
                return z4;
            } catch (IOException e4) {
                e = e4;
                z3 = true;
                e.printStackTrace();
                return z3;
            } catch (XmlPullParserException e5) {
                e = e5;
                z2 = true;
                e.printStackTrace();
                return z2;
            } catch (Exception e6) {
                e = e6;
                z = true;
                e.printStackTrace();
                return z;
            }
        } catch (MalformedURLException e7) {
            e = e7;
            z5 = false;
            e.printStackTrace();
            return z5;
        } catch (SocketTimeoutException e8) {
            e = e8;
            z4 = false;
            e.printStackTrace();
            return z4;
        } catch (IOException e9) {
            e = e9;
            z3 = false;
            e.printStackTrace();
            return z3;
        } catch (XmlPullParserException e10) {
            e = e10;
            z2 = false;
            e.printStackTrace();
            return z2;
        } catch (Exception e11) {
            e = e11;
            z = false;
            e.printStackTrace();
            return z;
        }
    }

    private static ArrayList b(ArrayList arrayList) {
        int i2;
        int i3;
        ArrayList arrayList2 = new ArrayList();
        int random = (arrayList == null || arrayList.size() == 0) ? 0 : ((int) (Math.random() * 100.0d)) % arrayList.size();
        int i4 = random;
        int i5 = 0;
        while (i4 < arrayList.size()) {
            if (i5 > 12 || ((String) arrayList.get(i4)).length() + i5 > 12) {
                i3 = i5;
            } else {
                arrayList2.add(arrayList.get(i4));
                i3 = ((String) arrayList.get(i4)).length() + i5;
            }
            i4++;
            i5 = i3;
        }
        int i6 = 0;
        while (i6 < random) {
            if (i5 > 12 || ((String) arrayList.get(i6)).length() + i5 > 12) {
                i2 = i5;
            } else {
                arrayList2.add(arrayList.get(i6));
                i2 = ((String) arrayList.get(i6)).length() + i5;
            }
            i6++;
            i5 = i2;
        }
        return arrayList2;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        this.q = getSharedPreferences("SOSO_WIDGET", 0);
        this.m.add(getResources().getString(R.string.default_hot1));
        this.m.add(getResources().getString(R.string.default_hot2));
        this.m.add(getResources().getString(R.string.default_hot3));
        this.r = new p(this);
        registerReceiver(this.r, new IntentFilter("com.tencent.qqlauncher.widget.soso.CHECK_UAPDTE_ACTION"));
    }

    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.r);
    }

    public int onStartCommand(Intent intent, int i2, int i3) {
        String string = this.q.getString("hots", null);
        if (string == null || string.equals("")) {
            string = a(this.m);
            SharedPreferences.Editor edit = this.q.edit();
            edit.putString("hots", string);
            edit.commit();
        }
        ArrayList arrayList = new ArrayList();
        StringTokenizer stringTokenizer = new StringTokenizer(string, "#");
        while (stringTokenizer.hasMoreTokens()) {
            arrayList.add(stringTokenizer.nextToken());
        }
        this.n = arrayList;
        this.p = b(this.n);
        Intent intent2 = new Intent("com.tencent.qqlauncher.widget.soso.SEND_HOT");
        intent2.putStringArrayListExtra("HOT_WORD", this.p);
        sendBroadcast(intent2);
        this.t = (AlarmManager) getSystemService("alarm");
        Intent intent3 = new Intent();
        intent3.setAction("com.tencent.qqlauncher.widget.soso.CHECK_UAPDTE_ACTION");
        PendingIntent broadcast = PendingIntent.getBroadcast(this, 0, intent3, 0);
        this.t.cancel(broadcast);
        this.t.setRepeating(2, SystemClock.elapsedRealtime() + 3600000, 3600000, broadcast);
        new Thread(this.u).start();
        return super.onStartCommand(intent, i2, i3);
    }
}
