package com.tencent.qphone.widget.soso;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public final class d {
    private final Context a;
    private k b = new k(this, this.a);
    private SQLiteDatabase c;

    public d(Context context) {
        this.a = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public final long a(String str, String str2, String str3) {
        this.c = this.b.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("_id", str);
        contentValues.put("_searchType", (Integer) 4);
        contentValues.put("_searchSummary", str2);
        contentValues.put("_searchUrl", str3);
        contentValues.put("_searchTime", Long.valueOf(System.currentTimeMillis()));
        this.c.delete("search_soso", "_id =?", new String[]{str});
        return this.c.insert("search_soso", null, contentValues);
    }

    public final Cursor a(String str) {
        this.c = this.b.getWritableDatabase();
        return this.c.query("search_soso", new String[]{"_id", "_searchType", "_searchSummary", "_searchUrl"}, str, null, null, null, "_searchTime desc", "20");
    }

    public final d a() {
        this.c = this.b.getWritableDatabase();
        return this;
    }

    public final void b() {
        this.b.close();
    }

    public final void c() {
        this.c = this.b.getWritableDatabase();
        this.c.execSQL("delete from search_soso");
    }
}
