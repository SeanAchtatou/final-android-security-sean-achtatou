package com.tencent.qphone.widget.soso.a;

import android.net.Uri;

public final class a {
    private String a = null;
    private String b = null;
    private Uri c = null;
    private int d = -1;

    public final String a() {
        return this.a != null ? this.a : "";
    }

    public final void a(int i) {
        this.d = i;
    }

    public final void a(Uri uri) {
        this.c = uri;
    }

    public final void a(String str) {
        this.a = str;
    }

    public final String b() {
        return this.b != null ? this.b : "";
    }

    public final void b(String str) {
        this.b = str;
    }

    public final Uri c() {
        return this.c;
    }

    public final int d() {
        return this.d;
    }
}
