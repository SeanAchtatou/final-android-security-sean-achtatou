package com.tencent.qphone.widget.soso;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public final class k extends SQLiteOpenHelper {
    private /* synthetic */ d a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(d dVar, Context context) {
        super(context, "searchHistory", (SQLiteDatabase.CursorFactory) null, 3);
        this.a = dVar;
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("create table IF NOT EXISTS search_soso(_id text primary key,_searchType int,_searchSummary text,_searchUrl text,_searchTime long)");
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("create table IF NOT EXISTS search_soso(_id text primary key,_searchType int,_searchSummary text,_searchUrl text,_searchTime long)");
    }
}
