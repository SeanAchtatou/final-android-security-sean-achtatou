package com.tencent.qphone.widget.soso;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class HistoryListView extends ListView {
    private boolean a;

    public HistoryListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public boolean hasFocus() {
        return true;
    }

    public boolean hasWindowFocus() {
        return true;
    }

    public boolean isFocused() {
        return true;
    }

    public boolean isInTouchMode() {
        return this.a || super.isInTouchMode();
    }

    /* access modifiers changed from: protected */
    public int[] onCreateDrawableState(int i) {
        return super.onCreateDrawableState(i);
    }
}
