package com.tencent.qphone.widget.soso;

import android.content.Intent;
import android.net.Uri;
import android.webkit.DownloadListener;

final class i implements DownloadListener {
    private /* synthetic */ SearchResultActivity a;

    i(SearchResultActivity searchResultActivity) {
        this.a = searchResultActivity;
    }

    public final void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        this.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
    }
}
