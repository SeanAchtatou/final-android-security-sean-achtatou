package com.tencent.qphone.widget.soso;

import android.view.View;
import android.widget.Button;
import com.tencent.qqlauncher.R;
import java.util.List;

final class r implements View.OnClickListener {
    private /* synthetic */ int a;
    private /* synthetic */ Button b;
    private /* synthetic */ SosoSearchMainActivity c;

    r(SosoSearchMainActivity sosoSearchMainActivity, int i, Button button) {
        this.c = sosoSearchMainActivity;
        this.a = i;
        this.b = button;
    }

    public final void onClick(View view) {
        if (this.a <= 0 || ((List) this.c.searchResult.get(Integer.valueOf(this.a))).size() != 0) {
            ((Button) this.c.findViewById(SosoSearchMainActivity.BTN_ARRAY[SosoSearchMainActivity.cur_Tab])).setBackgroundColor(17170443);
            int unused = SosoSearchMainActivity.cur_Tab = this.a;
            if (this.a == 0) {
                this.b.setBackgroundResource(R.drawable.tab_selected_left);
            } else if (this.a == SosoSearchMainActivity.BTN_ARRAY.length - 1) {
                this.b.setBackgroundResource(R.drawable.tab_selected_right);
            } else {
                this.b.setBackgroundResource(R.drawable.tab_selected_mid);
            }
            ((f) this.c.mAdapter).a(SosoSearchMainActivity.cur_Tab);
            this.c.mAdapter.notifyDataSetChanged();
        }
    }
}
