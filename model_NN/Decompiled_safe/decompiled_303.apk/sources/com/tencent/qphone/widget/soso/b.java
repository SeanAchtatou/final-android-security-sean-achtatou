package com.tencent.qphone.widget.soso;

import android.net.Uri;
import android.view.View;
import android.widget.Toast;
import com.tencent.qphone.widget.soso.a.a;
import com.tencent.qqlauncher.R;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

final class b implements View.OnClickListener {
    private /* synthetic */ a a;
    private /* synthetic */ f b;

    b(f fVar, a aVar) {
        this.b = fVar;
        this.a = aVar;
    }

    public final void onClick(View view) {
        try {
            if (this.a.d() == 4) {
                SosoSearchMainActivity.sText = this.a.a();
                this.b.a.searchText.setText(SosoSearchMainActivity.sText);
                this.b.a.searchText.setSelection(SosoSearchMainActivity.sText.length());
                Uri uri = null;
                try {
                    uri = Uri.parse(SosoSearchMainActivity.SEARCH_ENGINE + URLEncoder.encode(SosoSearchMainActivity.sText, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                this.a.a(uri);
            }
            this.b.a.startBrowserIntent(this.a.a(), this.a.c(), this.a.d(), this.a.b());
        } catch (Exception e2) {
            Toast.makeText(this.b.a, (int) R.string.result_deleted, 0).show();
        }
    }
}
