package com.tencent.qphone.widget.soso;

import android.text.Editable;
import android.text.TextWatcher;

final class c implements TextWatcher {
    private boolean a;
    private /* synthetic */ SosoSearchMainActivity b;

    /* synthetic */ c(SosoSearchMainActivity sosoSearchMainActivity) {
        this(sosoSearchMainActivity, (byte) 0);
    }

    private c(SosoSearchMainActivity sosoSearchMainActivity, byte b2) {
        this.b = sosoSearchMainActivity;
        this.a = true;
    }

    public final void afterTextChanged(Editable editable) {
        String trim = this.b.searchText.getText().toString().trim();
        SosoSearchMainActivity.sText = trim;
        if (trim.length() == 0) {
            this.a = true;
            this.b.tablayout.setVisibility(8);
            this.b.clearBtn.setVisibility(8);
        } else {
            this.a = false;
            this.b.tablayout.setVisibility(0);
            this.b.clearBtn.setVisibility(0);
        }
        this.b.refreshList(trim);
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
