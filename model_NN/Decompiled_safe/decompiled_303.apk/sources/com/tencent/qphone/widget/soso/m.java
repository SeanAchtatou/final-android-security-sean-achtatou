package com.tencent.qphone.widget.soso;

final class m implements Runnable {
    private /* synthetic */ SosoService a;

    m(SosoService sosoService) {
        this.a = sosoService;
    }

    public final void run() {
        long currentTimeMillis = System.currentTimeMillis() - this.a.getBaseContext().getSharedPreferences("soso", 0).getLong("lastGetTime", 0);
        if (currentTimeMillis <= 0 || currentTimeMillis >= 3600000) {
            SosoService.a(this.a, this.a.s);
        }
    }
}
