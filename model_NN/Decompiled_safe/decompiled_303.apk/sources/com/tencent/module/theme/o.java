package com.tencent.module.theme;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.tencent.qqlauncher.R;

final class o extends BroadcastReceiver {
    final /* synthetic */ ThemeSettingActivity a;

    /* synthetic */ o(ThemeSettingActivity themeSettingActivity) {
        this(themeSettingActivity, (byte) 0);
    }

    private o(ThemeSettingActivity themeSettingActivity, byte b) {
        this.a = themeSettingActivity;
    }

    public final void onReceive(Context context, Intent intent) {
        Object obj;
        String stringExtra = intent.getStringExtra("url");
        if (stringExtra.equals("http://softfile.3g.qq.com:8080/msoft/179/15250/18354/Theme_MacTheme.apk")) {
            obj = "com.tencent.qqlauncher.theme.mac";
        } else if (stringExtra.equals("http://softfile.3g.qq.com:8080/msoft/179/15250/18354/Theme_WaterTheme.apk")) {
            obj = "com.tencent.qqlauncher.theme.water";
        } else if (stringExtra.equals("http://softfile.3g.qq.com:8080/msoft/179/15250/18354/Theme_CuteTheme.apk")) {
            obj = "com.tencent.qqlauncher.theme.cute";
        } else {
            return;
        }
        int size = this.a.themeList.size();
        for (int i = 0; i < size; i++) {
            if (((q) this.a.themeList.get(i)).a.a.equals(obj)) {
                ((q) this.a.themeList.get(i)).d = false;
            }
            this.a.mAdapter.notifyDataSetChanged();
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle((int) R.string.theme_download_complete);
        builder.setMessage((int) R.string.theme_download_complete_msg);
        builder.setPositiveButton((int) R.string.comfirm, new h(this, stringExtra));
        builder.setNegativeButton((int) R.string.cancel, new i(this));
        builder.create().show();
        abortBroadcast();
    }
}
