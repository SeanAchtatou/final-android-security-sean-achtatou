package com.tencent.module.theme;

import android.app.Dialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.tencent.launcher.SearchAppTestActivity;
import com.tencent.qqlauncher.R;

final class s extends Handler {
    private /* synthetic */ ThemePreViewActivity a;

    s(ThemePreViewActivity themePreViewActivity) {
        this.a = themePreViewActivity;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case SearchAppTestActivity.TAB_TYPE_NET:
                Toast.makeText(this.a.mContext, (int) R.string.theme_apply_time_out, 0).show();
                this.a.dialog.dismiss();
                return;
            case 200:
                Dialog unused = this.a.dialog = new Dialog(this.a.mContext, R.style.FullHeightDialog);
                this.a.dialog.setContentView((int) R.layout.theme_pre_applying);
                this.a.dialog.show();
                return;
            case 300:
                Intent intent = new Intent();
                intent.setAction("com.tencent.launcher.applytheme");
                intent.putExtra("packagename", this.a.mThemePackageName);
                this.a.sendOrderedBroadcast(intent, null);
                return;
            default:
                return;
        }
    }
}
