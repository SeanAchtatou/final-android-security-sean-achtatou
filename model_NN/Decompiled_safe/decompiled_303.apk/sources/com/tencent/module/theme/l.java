package com.tencent.module.theme;

import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import com.tencent.launcher.BubbleTextView;
import com.tencent.launcher.DockView;
import com.tencent.launcher.DrawerTextView;
import com.tencent.launcher.FolderIcon;
import com.tencent.launcher.Launcher;
import com.tencent.launcher.am;
import com.tencent.launcher.base.BaseApp;
import com.tencent.launcher.base.b;
import com.tencent.launcher.bq;
import com.tencent.launcher.ff;
import com.tencent.launcher.gb;
import com.tencent.launcher.ha;
import com.tencent.launcher.home.i;
import com.tencent.qqlauncher.R;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public final class l {
    private static l g = new l();
    public WeakReference a;
    public WeakReference b;
    public WeakReference c;
    public WeakReference d;
    public WeakReference e;
    public WeakReference f;
    private final Context h = BaseApp.b();
    private c i;
    private Bitmap[] j;
    private Bitmap k;
    private Bitmap l;
    private Bitmap m;
    private Bitmap n;
    private final PackageManager o = this.h.getPackageManager();
    private e p;

    private l() {
        String b2 = i.a().b("curr_theme", (String) null);
        int b3 = i.a().b("curr_theme_type", -1);
        if (b3 != -1) {
            if (b2 == null || b2.equals("com.tencent.qqlauncher")) {
                BubbleTextView.a(true);
            } else {
                BubbleTextView.a(false);
            }
            this.p = a(b2, b3);
            this.i = r.a(this.p);
            if (this.i != null) {
                i();
            }
        }
    }

    public static Drawable a(e eVar, String str) {
        try {
            return r.a(eVar).a(str);
        } catch (OutOfMemoryError e2) {
            return null;
        }
    }

    public static l a() {
        if (g == null) {
            g = new l();
        }
        return g;
    }

    private void a(am amVar, c cVar) {
        Intent intent;
        ActivityInfo activityInfo;
        if (amVar != null && (intent = amVar.c) != null) {
            String className = intent.getComponent() == null ? null : intent.getComponent().getClassName();
            if (className != null) {
                Drawable a2 = cVar != null ? cVar.a(className.toLowerCase().replace(".", "_")) : null;
                if (a2 != null) {
                    if (amVar.d instanceof gb) {
                        gb gbVar = (gb) amVar.d;
                        gbVar.c(a2);
                        gbVar.a((Bitmap) null);
                        gbVar.b((Bitmap) null);
                        if (amVar.n != -200) {
                            gbVar.a(b.k, b.k);
                            return;
                        } else {
                            gbVar.a(b.m, b.m);
                            return;
                        }
                    }
                } else if (amVar.d instanceof gb) {
                    gb gbVar2 = (gb) amVar.d;
                    if (gbVar2.c()) {
                        try {
                            activityInfo = this.o.getActivityInfo(intent.getComponent(), 0);
                        } catch (PackageManager.NameNotFoundException e2) {
                            e2.printStackTrace();
                            activityInfo = null;
                        }
                        if (activityInfo != null) {
                            try {
                                if (activityInfo.loadIcon(this.o) != null) {
                                    gbVar2.b(activityInfo.loadIcon(this.o));
                                }
                            } catch (OutOfMemoryError e3) {
                            }
                        }
                    }
                }
            }
            if (amVar.d instanceof gb) {
                gb gbVar3 = (gb) amVar.d;
                if (!(this.p == null ? true : this.p.a.equals("com.tencent.qqlauncher"))) {
                    gbVar3.a(a((ha) amVar));
                    gbVar3.a(-1);
                    if (amVar.n != -200) {
                        gbVar3.a(b.k, b.l);
                    } else {
                        gbVar3.a(b.m, b.n);
                    }
                    gbVar3.b(this.k);
                } else if (amVar.n == -100) {
                    gbVar3.a(a((ha) amVar));
                    gbVar3.a(b.i, b.j);
                    gbVar3.a(0);
                    gbVar3.b((Bitmap) null);
                    Rect b2 = gbVar3.b();
                    b2.offsetTo(b2.left, 0);
                } else if (amVar.n == -200) {
                    gbVar3.a(b.j, b.j);
                    gbVar3.a((Bitmap) null);
                    gbVar3.b((Bitmap) null);
                    gbVar3.a(-1);
                } else {
                    gbVar3.a(b.i, b.j);
                    gbVar3.a((Bitmap) null);
                    gbVar3.b((Bitmap) null);
                    gbVar3.a(-1);
                }
            }
        }
    }

    private void a(e eVar) {
        if (eVar == null) {
            i.a().c("curr_theme");
            i.a().c("curr_theme_type");
        } else {
            String str = eVar.a;
            int i2 = eVar.c;
            i.a().a("curr_theme", str);
            i.a().a("curr_theme_type", i2);
        }
        this.p = eVar;
        this.i = r.a(this.p);
        if (this.p.a.equals("com.tencent.qqlauncher")) {
            BubbleTextView.a(true);
        } else {
            BubbleTextView.a(false);
        }
        i();
        c();
        a(this.h);
        Launcher.getLauncher().updateDrawerBackGroundColor(this.p == null ? true : this.p.a.equals("com.tencent.qqlauncher") ? null : this.i.b("drawer_color_bg"));
        Launcher.getLauncher().invalidateForTheme();
    }

    private void i() {
        if (this.p.a.equals("com.tencent.qqlauncher")) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inDither = false;
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            try {
                this.j = new Bitmap[1];
                this.j[0] = BitmapFactory.decodeResource(this.h.getResources(), R.drawable.iconbg, options);
            } catch (OutOfMemoryError e2) {
                this.j = null;
            }
            this.k = null;
            return;
        }
        this.j = this.i.a();
        this.k = this.i.c("theme_icon_shade");
        this.l = this.i.c("folder_bg");
        this.m = this.i.c("folder_shade_close");
        this.n = this.i.c("folder_shade_open");
    }

    public final Bitmap a(ha haVar) {
        return (haVar == null || !(haVar instanceof am)) ? a((String) null) : (!(haVar instanceof am) || ((am) haVar).a == null) ? a((String) null) : a(((am) haVar).a.toString());
    }

    public final Bitmap a(String str) {
        if (this.j == null || this.j.length == 0) {
            return null;
        }
        if (this.j.length == 1) {
            return this.j[0];
        }
        if (str == null) {
            return this.j[new Random().nextInt(this.j.length)];
        }
        return this.j[Math.max(0, str.hashCode() % this.j.length)];
    }

    public final Drawable a(Resources resources) {
        return ((this.p == null ? true : this.p.a.equals("com.tencent.qqlauncher")) || this.l == null) ? resources.getDrawable(R.drawable.folder_bg_default) : new BitmapDrawable(this.l);
    }

    public final Drawable a(am amVar) {
        if (this.p == null || amVar == null) {
            return null;
        }
        if (this.p.a.equals("com.tencent.qqlauncher")) {
            return null;
        }
        Intent intent = amVar.c;
        if (intent == null || intent.getComponent() == null) {
            return null;
        }
        String className = intent.getComponent().getClassName();
        if (className == null) {
            return null;
        }
        return this.i.a(className.toLowerCase().replace(".", "_"));
    }

    public final e a(String str, int i2) {
        if (str.equals("com.tencent.qqlauncher")) {
            e eVar = new e();
            eVar.a = "com.tencent.qqlauncher";
            eVar.b = this.h.getResources().getString(R.string.theme_default);
            return eVar;
        }
        k a2 = r.a(i2);
        if (a2 == null) {
            return null;
        }
        return a2.a(this.h, str);
    }

    public final void a(Context context) {
        c a2 = r.a(this.p);
        if (a2 != null) {
            Button button = (Button) this.a.get();
            if (button != null) {
                Drawable a3 = a2.a("all_app_grid_collapse");
                if (a3 == null) {
                    a3 = context.getResources().getDrawable(R.drawable.all_app_grid_collapse);
                }
                button.setBackgroundDrawable(a3);
            }
            ImageView imageView = (ImageView) this.b.get();
            if (imageView != null) {
                Drawable a4 = a2.a("ic_tray_home_expand");
                if (a4 == null) {
                    a4 = context.getResources().getDrawable(R.drawable.ic_tray_home_expand);
                }
                imageView.setImageDrawable(a4);
            }
            ImageView imageView2 = (ImageView) this.d.get();
            if (imageView2 != null) {
                Drawable a5 = a2.a("ic_launcher_market");
                if (a5 == null) {
                    a5 = context.getResources().getDrawable(R.drawable.ic_launcher_market);
                }
                imageView2.setImageDrawable(a5);
            }
            ImageView imageView3 = (ImageView) this.c.get();
            if (imageView != null) {
                Drawable a6 = a2.a("ic_delete");
                if (a6 == null) {
                    a6 = context.getResources().getDrawable(R.drawable.ic_delete);
                }
                imageView3.setImageDrawable(a6);
            }
            ImageView imageView4 = (ImageView) this.e.get();
            if (imageView != null) {
                Drawable a7 = a2.a("ic_launcher_search");
                if (a7 == null) {
                    a7 = context.getResources().getDrawable(R.drawable.ic_launcher_search);
                }
                imageView4.setImageDrawable(a7);
            }
            View view = (View) this.f.get();
            if (view != null) {
                Drawable a8 = a2.a("dock_background");
                if (a8 == null) {
                    a8 = context.getResources().getDrawable(R.drawable.dock_background);
                }
                view.setBackgroundDrawable(a8);
            }
        }
    }

    public final void a(String str, aj ajVar) {
        e a2 = a(str, 1);
        if (a2 != null) {
            a(a2);
            new j(this, ajVar).start();
        }
    }

    public final Drawable b(Resources resources) {
        return ((this.p == null ? true : this.p.a.equals("com.tencent.qqlauncher")) || this.m == null) ? resources.getDrawable(R.drawable.folder_shade_close_default) : new BitmapDrawable(this.m);
    }

    public final void b() {
        Bitmap bitmap = null;
        try {
            if (!(this.p == null ? true : this.p.a.equals("com.tencent.qqlauncher"))) {
                bitmap = this.i.c("theme_wallpaper");
            } else {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inDither = true;
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                bitmap = BitmapFactory.decodeResource(this.h.getResources(), R.drawable.wallpaper_default, options);
            }
        } catch (OutOfMemoryError e2) {
        }
        if (bitmap != null) {
            try {
                ((WallpaperManager) this.h.getSystemService("wallpaper")).setBitmap(bitmap);
                bitmap.recycle();
            } catch (Exception e3) {
            }
        }
    }

    public final void b(String str) {
        e a2 = a(str, 1);
        if (a2 != null) {
            a(a2);
        }
    }

    public final Drawable c(Resources resources) {
        return ((this.p == null ? true : this.p.a.equals("com.tencent.qqlauncher")) || this.n == null) ? resources.getDrawable(R.drawable.folder_shade_open_default) : new BitmapDrawable(this.n);
    }

    public final void c() {
        ArrayList allFolders;
        ArrayList<am> arrayList;
        ArrayList<am> arrayList2;
        c a2 = r.a(this.p);
        ff model = Launcher.getModel();
        if (model != null) {
            ArrayList h2 = model.h();
            if (h2 != null) {
                Iterator it = h2.iterator();
                while (it.hasNext()) {
                    ha haVar = (ha) it.next();
                    if (haVar instanceof am) {
                        a((am) haVar, a2);
                    } else if ((haVar instanceof bq) && (arrayList2 = ((bq) haVar).b) != null) {
                        for (am amVar : arrayList2) {
                            if (amVar instanceof am) {
                                a(amVar, a2);
                            }
                        }
                    }
                }
            }
            ArrayList i2 = model.i();
            if (i2 != null) {
                Iterator it2 = i2.iterator();
                while (it2.hasNext()) {
                    ha haVar2 = (ha) it2.next();
                    if (haVar2 instanceof am) {
                        a((am) haVar2, a2);
                    } else if ((haVar2 instanceof bq) && (arrayList = ((bq) haVar2).b) != null) {
                        for (am amVar2 : arrayList) {
                            if (amVar2 instanceof am) {
                                a(amVar2, a2);
                            }
                        }
                    }
                }
            }
            Launcher launcher = Launcher.getLauncher();
            if (launcher != null && (allFolders = launcher.getAllFolders()) != null) {
                Iterator it3 = allFolders.iterator();
                while (it3.hasNext()) {
                    View view = (View) it3.next();
                    if (view instanceof FolderIcon) {
                        ((FolderIcon) view).a();
                    } else if (view instanceof DockView) {
                        ((DockView) view).c();
                    } else if (view instanceof DrawerTextView) {
                        ((DrawerTextView) view).f();
                    }
                }
            }
        }
    }

    public final void c(String str) {
        e a2 = a(str, 1);
        if (a2 != null) {
            a(a2);
            new j(this, null).start();
        }
    }

    public final boolean d() {
        if (this.p == null) {
            return true;
        }
        return this.p.a.equals("com.tencent.qqlauncher");
    }

    public final Bitmap e() {
        return this.k;
    }

    public final List f() {
        ArrayList arrayList = new ArrayList();
        e eVar = new e();
        eVar.a = "com.tencent.qqlauncher";
        eVar.b = this.h.getResources().getString(R.string.theme_default);
        arrayList.add(eVar);
        List<e> arrayList2 = new ArrayList<>();
        k a2 = r.a(1);
        if (a2 != null) {
            arrayList2 = a2.a(this.h);
        }
        for (e add : arrayList2) {
            arrayList.add(add);
        }
        return arrayList;
    }

    public final e g() {
        if (this.p == null) {
            e eVar = new e();
            eVar.a = "com.tencent.qqlauncher";
            eVar.b = this.h.getResources().getString(R.string.theme_default);
            this.p = eVar;
        }
        return this.p;
    }

    public final String h() {
        if (this.p == null ? true : this.p.a.equals("com.tencent.qqlauncher")) {
            return null;
        }
        return this.i.b("drawer_color_bg");
    }
}
