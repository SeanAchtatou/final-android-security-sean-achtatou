package com.tencent.module.theme;

public final class r {
    public static c a(e eVar) {
        if (eVar == null) {
            return null;
        }
        try {
            switch (eVar.c) {
                case 1:
                    return new g(eVar);
                case 0:
                    return new d(eVar);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static k a(int i) {
        switch (i) {
            case 0:
                return p.a();
            case 1:
                return n.a();
            case 2:
                return m.a();
            default:
                return null;
        }
    }
}
