package com.tencent.module.theme;

import android.content.DialogInterface;

final class ai implements DialogInterface.OnClickListener {
    private /* synthetic */ e a;
    private /* synthetic */ ThemeSettingActivity b;

    ai(ThemeSettingActivity themeSettingActivity, e eVar) {
        this.b = themeSettingActivity;
        this.a = eVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.b.loadRecomendTheme(this.a.a);
        dialogInterface.dismiss();
    }
}
