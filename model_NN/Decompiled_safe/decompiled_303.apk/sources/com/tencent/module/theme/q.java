package com.tencent.module.theme;

import android.graphics.drawable.Drawable;

public final class q {
    public e a;
    Drawable b;
    boolean c;
    boolean d;
    private /* synthetic */ ThemeSettingActivity e;

    public q(ThemeSettingActivity themeSettingActivity, e eVar, Drawable drawable, boolean z) {
        this.e = themeSettingActivity;
        this.a = eVar;
        if (drawable != null) {
            this.b = drawable;
        }
        this.c = z;
    }
}
