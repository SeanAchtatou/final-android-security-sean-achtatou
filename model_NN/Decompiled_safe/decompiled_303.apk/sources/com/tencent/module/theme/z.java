package com.tencent.module.theme;

import android.app.Dialog;
import android.os.Handler;
import android.os.Message;
import com.tencent.launcher.SearchAppTestActivity;
import com.tencent.qqlauncher.R;

final class z extends Handler {
    private /* synthetic */ ThemeSettingActivity a;

    z(ThemeSettingActivity themeSettingActivity) {
        this.a = themeSettingActivity;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case SearchAppTestActivity.TAB_TYPE_NET:
                Dialog dialog = new Dialog(this.a.mContext, R.style.FullHeightDialog);
                dialog.setContentView((int) R.layout.theme_pre_applying);
                dialog.show();
                return;
            case 200:
                l.a().c("com.tencent.qqlauncher");
                this.a.finish();
                return;
            case 300:
                this.a.mAdapter.notifyDataSetChanged();
                return;
            default:
                return;
        }
    }
}
