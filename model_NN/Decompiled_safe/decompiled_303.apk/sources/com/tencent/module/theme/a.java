package com.tencent.module.theme;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class a extends BroadcastReceiver {
    private /* synthetic */ ThemeSettingActivity a;

    /* synthetic */ a(ThemeSettingActivity themeSettingActivity) {
        this(themeSettingActivity, (byte) 0);
    }

    private a(ThemeSettingActivity themeSettingActivity, byte b) {
        this.a = themeSettingActivity;
    }

    public final void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("android.intent.action.PACKAGE_CHANGED".equals(action) || "android.intent.action.PACKAGE_REMOVED".equals(action) || "android.intent.action.PACKAGE_ADDED".equals(action)) {
            intent.getData().getSchemeSpecificPart();
            boolean booleanExtra = intent.getBooleanExtra("android.intent.extra.REPLACING", false);
            if ("android.intent.action.PACKAGE_CHANGED".equals(action)) {
                return;
            }
            if ("android.intent.action.PACKAGE_REMOVED".equals(action)) {
                if (!booleanExtra) {
                    this.a.updateThemeList();
                }
            } else if (!booleanExtra) {
                this.a.updateThemeList();
            }
        }
    }
}
