package com.tencent.module.theme;

import android.view.View;
import android.widget.AdapterView;

final class w implements AdapterView.OnItemSelectedListener {
    private /* synthetic */ ThemePreViewActivity a;

    w(ThemePreViewActivity themePreViewActivity) {
        this.a = themePreViewActivity;
    }

    public final void onItemSelected(AdapterView adapterView, View view, int i, long j) {
        int childCount = this.a.mNavigation.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            if (i2 == i) {
                this.a.mNavigation.getChildAt(i2).setSelected(true);
            } else {
                this.a.mNavigation.getChildAt(i2).setSelected(false);
            }
        }
    }

    public final void onNothingSelected(AdapterView adapterView) {
    }
}
