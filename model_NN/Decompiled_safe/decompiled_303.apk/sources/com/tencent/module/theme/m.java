package com.tencent.module.theme;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import com.tencent.util.f;
import java.util.ArrayList;
import java.util.List;

public final class m implements k {
    private static m a;

    private m() {
    }

    public static m a() {
        if (a == null) {
            a = new m();
        }
        return a;
    }

    public final e a(Context context, String str) {
        context.getPackageManager();
        try {
            f fVar = new f(context, str);
            e eVar = new e();
            eVar.a = str;
            eVar.b = fVar.a("theme_name");
            eVar.c = 1;
            eVar.e = fVar.a("theme_author");
            eVar.f = fVar.a("theme_description");
            eVar.d = Integer.parseInt(fVar.a("theme_version"));
            return eVar;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public final List a(Context context) {
        Intent intent = new Intent("org.adw.launcher.THEMES");
        intent.addCategory("android.intent.category.DEFAULT");
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
        int size = queryIntentActivities.size();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < size; i++) {
            e a2 = a(context, queryIntentActivities.get(i).activityInfo.packageName);
            if (a2 != null) {
                arrayList.add(a2);
            }
        }
        return arrayList;
    }
}
