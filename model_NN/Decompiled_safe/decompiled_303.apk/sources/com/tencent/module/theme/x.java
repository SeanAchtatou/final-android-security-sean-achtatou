package com.tencent.module.theme;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import com.tencent.qqlauncher.R;

final class x implements DialogInterface.OnClickListener {
    private /* synthetic */ ThemePreViewActivity a;

    x(ThemePreViewActivity themePreViewActivity) {
        this.a = themePreViewActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        try {
            this.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=" + this.a.mContext.getResources().getString(R.string.qqlauncher))));
        } catch (ActivityNotFoundException e) {
        }
        dialogInterface.dismiss();
    }
}
