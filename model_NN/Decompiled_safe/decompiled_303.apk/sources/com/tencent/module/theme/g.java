package com.tencent.module.theme;

import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.tencent.launcher.base.BaseApp;
import com.tencent.util.f;

public final class g implements c {
    private e a;
    private f b;

    public g(e eVar) {
        this.a = eVar;
        try {
            this.b = new f(BaseApp.b(), this.a.a);
        } catch (PackageManager.NameNotFoundException e) {
            throw new MSFThemeExcepiton("theme packName not found.");
        }
    }

    public final Drawable a(String str) {
        try {
            return this.b.b(str);
        } catch (OutOfMemoryError e) {
            return null;
        }
    }

    public final Bitmap[] a() {
        Bitmap[] bitmapArr = new Bitmap[10];
        int i = 0;
        for (int i2 = 0; i2 < 10; i2++) {
            Bitmap c = this.b.c("theme_icon_bg_" + i2);
            if (c != null) {
                bitmapArr[i] = c;
                i++;
            }
        }
        Bitmap[] bitmapArr2 = new Bitmap[i];
        for (int i3 = 0; i3 < i; i3++) {
            Bitmap bitmap = bitmapArr[i3];
            if (bitmap != null) {
                bitmapArr2[i3] = bitmap;
            }
        }
        return bitmapArr2;
    }

    public final String b(String str) {
        return this.b.a(str);
    }

    public final Bitmap c(String str) {
        return this.b.c(str);
    }
}
