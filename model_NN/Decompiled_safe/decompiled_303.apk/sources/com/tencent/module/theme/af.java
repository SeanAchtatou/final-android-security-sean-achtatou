package com.tencent.module.theme;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.tencent.launcher.base.BaseApp;
import com.tencent.qqlauncher.R;

final class af implements AdapterView.OnItemClickListener {
    private /* synthetic */ ThemeSettingActivity a;

    af(ThemeSettingActivity themeSettingActivity) {
        this.a = themeSettingActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        q qVar = (q) this.a.themeList.get(i);
        e eVar = qVar.a;
        String str = eVar.a;
        if (str.equals("com.tencent.qqlauncher")) {
            this.a.applyDefaultTheme();
        } else if (qVar.c) {
            Intent intent = new Intent(this.a.mContext, ThemePreViewActivity.class);
            intent.putExtra("package_name", str);
            intent.putExtra("theme_type", 1);
            try {
                this.a.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
                BaseApp.a(this.a.getResources().getString(R.string.theme_package_error));
            }
        } else if (qVar.d) {
            BaseApp.a((int) R.string.theme_downloading);
        } else {
            this.a.downOrInstallTheme(eVar);
        }
    }
}
