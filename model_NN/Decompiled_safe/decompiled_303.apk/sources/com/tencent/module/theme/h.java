package com.tencent.module.theme;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import com.tencent.module.appcenter.c;
import java.io.File;
import java.io.IOException;

final class h implements DialogInterface.OnClickListener {
    private /* synthetic */ String a;
    private /* synthetic */ o b;

    h(o oVar, String str) {
        this.b = oVar;
        this.a = str;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        File file = new File(c.a + "/" + (ThemeSettingActivity.decodeUrl2FileName(this.a) + ".apk"));
        try {
            Runtime.getRuntime().exec("chmod 777 " + file.getAbsolutePath());
            Runtime.getRuntime().exec("chmod 777 " + file.getParent());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent();
        intent.addFlags(268435456);
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        this.b.a.mContext.startActivity(intent);
        dialogInterface.dismiss();
    }
}
