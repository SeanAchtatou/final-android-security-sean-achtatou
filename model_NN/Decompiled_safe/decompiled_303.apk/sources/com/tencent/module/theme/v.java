package com.tencent.module.theme;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class v extends BroadcastReceiver {
    private /* synthetic */ ThemePreViewActivity a;

    v(ThemePreViewActivity themePreViewActivity) {
        this.a = themePreViewActivity;
    }

    public final void onReceive(Context context, Intent intent) {
        if ("android.intent.action.PACKAGE_REMOVED".equals(intent.getAction()) && intent.getData().getSchemeSpecificPart().equals(this.a.mTheme.a)) {
            this.a.finish();
        }
    }
}
