package com.tencent.module.theme;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import java.io.File;
import java.io.IOException;

final class y implements DialogInterface.OnClickListener {
    private /* synthetic */ File a;
    private /* synthetic */ ThemeSettingActivity b;

    y(ThemeSettingActivity themeSettingActivity, File file) {
        this.b = themeSettingActivity;
        this.a = file;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        try {
            Runtime.getRuntime().exec("chmod 777 " + this.a.getAbsolutePath());
            Runtime.getRuntime().exec("chmod 777 " + this.a.getParent());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent();
        intent.addFlags(268435456);
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(this.a), "application/vnd.android.package-archive");
        this.b.startActivity(intent);
        dialogInterface.dismiss();
    }
}
