package com.tencent.module.theme;

import android.content.DialogInterface;

final class ab implements DialogInterface.OnClickListener {
    private /* synthetic */ ThemeSettingActivity a;

    ab(ThemeSettingActivity themeSettingActivity) {
        this.a = themeSettingActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.mHandler.sendEmptyMessage(100);
        this.a.mHandler.sendEmptyMessageDelayed(200, 300);
        dialogInterface.dismiss();
    }
}
