package com.tencent.module.theme;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import com.tencent.qqlauncher.R;

final class u extends BroadcastReceiver {
    private /* synthetic */ ThemePreViewActivity a;

    u(ThemePreViewActivity themePreViewActivity) {
        this.a = themePreViewActivity;
    }

    public final void onReceive(Context context, Intent intent) {
        if (intent.getBooleanExtra("result", false)) {
            Intent intent2 = new Intent();
            intent2.setClassName("com.tencent.qqlauncher", "com.tencent.launcher.Launcher");
            this.a.mContext.startActivity(intent2);
            this.a.dialog.dismiss();
            this.a.finish();
            return;
        }
        Toast.makeText(this.a.mContext, (int) R.string.apply_theme_failed, 0).show();
    }
}
