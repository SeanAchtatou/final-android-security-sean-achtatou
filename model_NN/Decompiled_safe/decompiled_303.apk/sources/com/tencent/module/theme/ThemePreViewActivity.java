package com.tencent.module.theme;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

public class ThemePreViewActivity extends Activity implements View.OnClickListener {
    private static final int APPLY_THEME = 300;
    private static final int MAX_PREVIEW_COUNT = 5;
    private static final int SHOW_APPLYING_DIALOG = 200;
    public static final String THEME_APPLY_BROADCAST = "theme_apply_broadcast";
    private static final int UPDATE_TIME_OUT = 100;
    /* access modifiers changed from: private */
    public Dialog dialog;
    private b mAdapter;
    private BroadcastReceiver mAppRemoveReceiver = new v(this);
    private TextView mApplyButton;
    /* access modifiers changed from: private */
    public Context mContext;
    private ImageView mDeleteButton;
    Handler mHandler = new s(this);
    /* access modifiers changed from: private */
    public ViewGroup mNavigation;
    private BroadcastReceiver mReceiver = new u(this);
    /* access modifiers changed from: private */
    public e mTheme;
    private Gallery mThemeGallery;
    private String mThemeName;
    /* access modifiers changed from: private */
    public String mThemePackageName;
    private int mThemeType;
    private TextView mTitleTextView;

    private void applyTheme() {
        if (!qqLuancherIsInstalled()) {
            showInstallDialog();
        } else if (!checkVesionCode()) {
            Toast.makeText(this.mContext, (int) R.string.apply_theme_failed, 0).show();
            this.mApplyButton.setText((int) R.string.update_qqlauncher);
        } else {
            this.mHandler.sendEmptyMessage(SHOW_APPLYING_DIALOG);
            this.mHandler.sendEmptyMessageDelayed(APPLY_THEME, 300);
            this.mHandler.sendEmptyMessage(APPLY_THEME);
            this.mHandler.sendEmptyMessageDelayed(100, 30000);
        }
    }

    private boolean checkVesionCode() {
        try {
            PackageInfo packageInfo = this.mContext.getPackageManager().getPackageInfo("com.tencent.qqlauncher", 0);
            if (packageInfo == null) {
                return false;
            }
            int i = packageInfo.versionCode;
            try {
                int themeSupportVersion = getThemeSupportVersion();
                if (themeSupportVersion == -1) {
                    return false;
                }
                return themeSupportVersion <= i;
            } catch (Exception e) {
                return false;
            }
        } catch (PackageManager.NameNotFoundException e2) {
            return false;
        }
    }

    private Drawable getThemeDrawable(String str) {
        l.a();
        return l.a(this.mTheme, str);
    }

    private String getThemeName() {
        return this.mTheme.b;
    }

    private ArrayList getThemePreViewList() {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < 5; i++) {
            Drawable themeDrawable = getThemeDrawable("theme_preview_" + i);
            if (themeDrawable != null) {
                arrayList.add(themeDrawable);
            }
        }
        return arrayList;
    }

    private int getThemeSupportVersion() {
        return this.mTheme.g;
    }

    private boolean qqLuancherIsInstalled() {
        try {
            return this.mContext.getPackageManager().getPackageInfo("com.tencent.qqlauncher", 0) != null;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private void showInstallDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
        builder.setTitle((int) R.string.install_qqlauncher);
        builder.setMessage((int) R.string.qqlauncher_no_install);
        builder.setPositiveButton((int) R.string.ok, new x(this));
        builder.setNegativeButton((int) R.string.cancel, new t(this));
        builder.create().show();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.theme_delete /*2131493332*/:
                startActivity(new Intent("android.intent.action.DELETE", Uri.parse("package:" + this.mThemePackageName)));
                return;
            case R.id.theme_gallery /*2131493333*/:
            default:
                return;
            case R.id.theme_apply /*2131493334*/:
                if (this.mApplyButton.getText().toString().equals(this.mContext.getResources().getString(R.string.update_qqlauncher))) {
                    try {
                        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=" + this.mContext.getResources().getString(R.string.qqlauncher))));
                    } catch (ActivityNotFoundException e) {
                    }
                    this.mApplyButton.setText((int) R.string.theme_apply);
                    return;
                }
                applyTheme();
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.theme_pre_activity);
        Intent intent = getIntent();
        this.mThemePackageName = intent.getStringExtra("package_name");
        if (this.mThemePackageName == null) {
            finish();
            return;
        }
        this.mThemeType = intent.getIntExtra("theme_type", -1);
        if (this.mThemeType == -1) {
            finish();
            return;
        }
        this.mContext = this;
        this.mTheme = l.a().a(this.mThemePackageName, this.mThemeType);
        if (this.mTheme == null) {
            finish();
            return;
        }
        this.mThemeGallery = (Gallery) findViewById(R.id.theme_gallery);
        this.mApplyButton = (TextView) findViewById(R.id.theme_apply);
        this.mDeleteButton = (ImageView) findViewById(R.id.theme_delete);
        this.mTitleTextView = (TextView) findViewById(R.id.theme_title);
        this.mNavigation = (ViewGroup) findViewById(R.id.theme_navigation);
        this.mThemeName = getThemeName();
        this.mTitleTextView.setText(this.mThemeName);
        ArrayList themePreViewList = getThemePreViewList();
        int size = (themePreViewList == null || themePreViewList.isEmpty()) ? 1 : themePreViewList.size();
        for (int i = 0; i < size; i++) {
            this.mNavigation.addView((ImageView) LayoutInflater.from(this.mContext).inflate((int) R.layout.theme_pre_nav_item, (ViewGroup) null));
        }
        this.mNavigation.getChildAt(0).setSelected(true);
        this.mAdapter = new b(this, this.mContext, themePreViewList);
        this.mThemeGallery.setAdapter((SpinnerAdapter) this.mAdapter);
        this.mApplyButton.setOnClickListener(this);
        this.mDeleteButton.setOnClickListener(this);
        this.mThemeGallery.setOnItemSelectedListener(new w(this));
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(THEME_APPLY_BROADCAST);
        registerReceiver(this.mReceiver, intentFilter);
        IntentFilter intentFilter2 = new IntentFilter();
        intentFilter2.addAction("android.intent.action.PACKAGE_REMOVED");
        intentFilter2.addDataScheme("package");
        registerReceiver(this.mAppRemoveReceiver, intentFilter2);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mHandler.removeMessages(100);
        try {
            unregisterReceiver(this.mReceiver);
            unregisterReceiver(this.mAppRemoveReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }
}
