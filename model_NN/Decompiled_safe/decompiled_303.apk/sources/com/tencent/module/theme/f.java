package com.tencent.module.theme;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.qqlauncher.R;
import java.util.List;

final class f extends BaseAdapter {
    List a;
    int b;
    private Context c;
    private int d = -1;
    private /* synthetic */ ThemeSettingActivity e;

    public f(ThemeSettingActivity themeSettingActivity, Context context, List list) {
        this.e = themeSettingActivity;
        this.c = context;
        this.a = list;
    }

    public final int getCount() {
        return this.a.size();
    }

    public final Object getItem(int i) {
        return null;
    }

    public final long getItemId(int i) {
        return 0;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        ViewGroup viewGroup2 = (ViewGroup) (view == null ? LayoutInflater.from(this.c).inflate((int) R.layout.theme_item, (ViewGroup) null) : view);
        TextView textView = (TextView) viewGroup2.findViewById(R.id.theme_name);
        ImageView imageView = (ImageView) viewGroup2.findViewById(R.id.theme_thumb);
        ImageView imageView2 = (ImageView) viewGroup2.findViewById(R.id.theme_using);
        ImageView imageView3 = (ImageView) viewGroup2.findViewById(R.id.theme_download_shade);
        if (i == this.b) {
            imageView2.setVisibility(0);
        } else {
            imageView2.setVisibility(4);
        }
        Drawable drawable = ((q) this.a.get(i)).b;
        if (drawable == null) {
            imageView.setImageResource(R.drawable.theme_thumb_default);
        } else {
            imageView.setImageDrawable(drawable);
        }
        textView.setText(((q) this.a.get(i)).a.b);
        if (((q) this.a.get(i)).c) {
            imageView3.setVisibility(4);
        } else {
            imageView3.setVisibility(0);
            if (((q) this.a.get(i)).d) {
                imageView3.setImageResource(R.drawable.theme_downloading);
                ((AnimationDrawable) imageView3.getDrawable()).start();
            } else {
                imageView3.setImageResource(R.drawable.theme_download_shade);
            }
        }
        return viewGroup2;
    }
}
