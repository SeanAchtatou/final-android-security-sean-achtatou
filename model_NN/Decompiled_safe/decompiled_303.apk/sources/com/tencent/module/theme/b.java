package com.tencent.module.theme;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

final class b extends BaseAdapter {
    private Context a;
    private ArrayList b;
    private /* synthetic */ ThemePreViewActivity c;

    public b(ThemePreViewActivity themePreViewActivity, Context context, ArrayList arrayList) {
        this.c = themePreViewActivity;
        this.a = context;
        this.b = arrayList;
    }

    public final int getCount() {
        if (this.b == null || this.b.isEmpty()) {
            return 1;
        }
        return this.b.size();
    }

    public final Object getItem(int i) {
        return null;
    }

    public final long getItemId(int i) {
        return 0;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        Drawable drawable;
        View inflate = view == null ? LayoutInflater.from(this.a).inflate((int) R.layout.theme_pre_item, (ViewGroup) null) : view;
        ImageView imageView = (ImageView) inflate.findViewById(R.id.theme_preview_item);
        if (this.b.size() <= i || (drawable = (Drawable) this.b.get(i)) == null) {
            imageView.setImageResource(R.drawable.theme_pre_default_img);
        } else {
            imageView.setImageDrawable(drawable);
        }
        return inflate;
    }
}
