package com.tencent.module.theme;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.RemoteException;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.tencent.launcher.base.BaseApp;
import com.tencent.module.appcenter.c;
import com.tencent.module.download.DownloadInfo;
import com.tencent.module.download.DownloadService;
import com.tencent.module.download.b;
import com.tencent.module.download.r;
import com.tencent.module.download.y;
import com.tencent.qqlauncher.R;
import com.tencent.util.f;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ThemeSettingActivity extends Activity {
    private static final int APPLY_DEFAULT_THEME = 200;
    private static final int REFRESH_THEME_LIST = 300;
    private static final int SHOW_LOADING_DIALOG = 100;
    private static final String cutePackageName = "com.tencent.qqlauncher.theme.cute";
    private static final String cuteUrl = "http://softfile.3g.qq.com:8080/msoft/179/15250/18354/Theme_CuteTheme.apk";
    private static final String macPackageName = "com.tencent.qqlauncher.theme.mac";
    private static final String macUrl = "http://softfile.3g.qq.com:8080/msoft/179/15250/18354/Theme_MacTheme.apk";
    private static final String waterPackageName = "com.tencent.qqlauncher.theme.water";
    private static final String waterUrl = "http://softfile.3g.qq.com:8080/msoft/179/15250/18354/Theme_WaterTheme.apk";
    private AlphaAnimation alphaAnim;
    ServiceConnection connection;
    /* access modifiers changed from: private */
    public f mAdapter;
    private b mCallback;
    /* access modifiers changed from: private */
    public Context mContext;
    private BroadcastReceiver mDownReceiver = new o(this);
    /* access modifiers changed from: private */
    public Handler mHandler = new z(this);
    private r mListListener;
    private View mLoadMoreView;
    private BroadcastReceiver mReceiver = new a(this);
    /* access modifiers changed from: private */
    public y mService;
    private GridView mThemeGridView;
    List themeList;

    private boolean checkSDcardAndNetEnable() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isAvailable()) {
            BaseApp.a((int) R.string.theme_network_disable);
            return false;
        } else if (Environment.getExternalStorageState().equals("mounted")) {
            return true;
        } else {
            BaseApp.a((int) R.string.theme_sdcard_disable);
            return false;
        }
    }

    private boolean checkUrlIsLoading(String str, List list) {
        if (str == null || list == null || list.isEmpty()) {
            return false;
        }
        Iterator it = list.iterator();
        while (it.hasNext()) {
            if (((DownloadInfo) it.next()).a.equals(str)) {
                return true;
            }
        }
        return false;
    }

    public static String decodeUrl2FileName(String str) {
        String str2 = "";
        if (str != null) {
            str2 = str.indexOf("?") > 0 ? str.substring(str.lastIndexOf("/") + 1, str.indexOf("?")) : str.substring(str.lastIndexOf("/") + 1);
        }
        int indexOf = str2.indexOf(".apk");
        return indexOf > 0 ? str2.substring(0, indexOf) : str2;
    }

    private void downLoadServiceInit() {
        this.connection = new ae(this);
        this.mCallback = new ad(this);
        bindService(new Intent(this, DownloadService.class), this.connection, 1);
    }

    /* access modifiers changed from: private */
    public void downOrInstallTheme(e eVar) {
        String str = eVar.a;
        File file = new File(c.a + "/" + (decodeUrl2FileName(str.equals(macPackageName) ? macUrl : str.equals(waterPackageName) ? waterUrl : str.equals(cutePackageName) ? cuteUrl : null) + ".apk"));
        boolean z = false;
        if (file.exists()) {
            try {
                if (new com.tencent.android.a.c(this.mContext).a(file.getAbsolutePath()) != null) {
                    z = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!z) {
                try {
                    file.delete();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
        if (z) {
            showInstallDialog(eVar, file);
        } else {
            showDownLoadDialog(eVar);
        }
    }

    private Drawable getDrawabeFromPackage(String str) {
        try {
            if (str.equals("com.tencent.qqlauncher")) {
                return this.mContext.getResources().getDrawable(R.drawable.theme_thumb);
            }
            try {
                return new f(this.mContext, str).b("theme_thumb");
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                return null;
            }
        } catch (OutOfMemoryError e2) {
        }
    }

    private List getThemeList(List list) {
        List<e> f = l.a().f();
        ArrayList arrayList = new ArrayList();
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        for (e eVar : f) {
            if (eVar.a.equals(macPackageName)) {
                z3 = true;
            }
            if (eVar.a.equals(waterPackageName)) {
                z2 = true;
            }
            if (eVar.a.equals(cutePackageName)) {
                z = true;
            }
            arrayList.add(new q(this, eVar, getDrawabeFromPackage(eVar.a), true));
        }
        if (!z3) {
            e eVar2 = new e();
            eVar2.b = this.mContext.getResources().getString(R.string.mac_theme_name);
            eVar2.a = macPackageName;
            q qVar = new q(this, eVar2, this.mContext.getResources().getDrawable(R.drawable.mac_theme_thumb), false);
            new File(c.a + "/" + (decodeUrl2FileName(macUrl) + ".apk")).exists();
            qVar.d = false;
            arrayList.add(qVar);
        }
        if (!z2) {
            e eVar3 = new e();
            eVar3.b = this.mContext.getResources().getString(R.string.water_theme_name);
            eVar3.a = waterPackageName;
            q qVar2 = new q(this, eVar3, this.mContext.getResources().getDrawable(R.drawable.water_theme_thumb), false);
            new File(c.a + "/" + (decodeUrl2FileName(waterUrl) + ".apk")).exists();
            qVar2.d = false;
            arrayList.add(qVar2);
        }
        if (!z) {
            e eVar4 = new e();
            eVar4.b = this.mContext.getResources().getString(R.string.cute_theme_name);
            eVar4.a = cutePackageName;
            q qVar3 = new q(this, eVar4, this.mContext.getResources().getDrawable(R.drawable.cute_theme_thumb), false);
            new File(c.a + "/" + (decodeUrl2FileName(cuteUrl) + ".apk")).exists();
            qVar3.d = false;
            arrayList.add(qVar3);
        }
        return arrayList;
    }

    private void goToAppCenter(String str) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("http://a.app.qq.com/g/s?aid=searchsoft_a&g_f=990233&softname=" + URLEncoder.encode(str, "utf-8")));
            intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void loadMoreTheme() {
        String string = getResources().getString(R.string.theme_search_key);
        try {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=" + string)));
        } catch (ActivityNotFoundException e) {
            goToAppCenter(string);
        }
    }

    /* access modifiers changed from: private */
    public void loadRecomendTheme(String str) {
        if (checkSDcardAndNetEnable()) {
            DownloadInfo downloadInfo = new DownloadInfo();
            downloadInfo.j = "com.tencent.launcher.theme.download.complete";
            if (str.equals(macPackageName)) {
                downloadInfo.d = getResources().getString(R.string.mac_theme_name);
                downloadInfo.a = macUrl;
                downloadInfo.h = 836;
            } else if (str.equals(waterPackageName)) {
                downloadInfo.d = getResources().getString(R.string.water_theme_name);
                downloadInfo.a = waterUrl;
                downloadInfo.h = 676;
            } else if (str.equals(cutePackageName)) {
                downloadInfo.d = getResources().getString(R.string.cute_theme_name);
                downloadInfo.a = cuteUrl;
                downloadInfo.h = 676;
            } else {
                downloadInfo.a = null;
            }
            if (downloadInfo.a != null) {
                try {
                    this.mService.a(downloadInfo, "ThemeSettingActivity", this.mCallback);
                    int size = this.themeList.size();
                    for (int i = 0; i < size; i++) {
                        if (((q) this.themeList.get(i)).a.a.equals(str)) {
                            ((q) this.themeList.get(i)).d = true;
                        }
                    }
                    this.mAdapter.notifyDataSetChanged();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void refreshThemeList(List list) {
        if (list != null && !list.isEmpty()) {
            new ArrayList();
            for (q qVar : this.themeList) {
                if (!qVar.c) {
                    qVar.d = false;
                    String str = qVar.a.a.equals(macPackageName) ? macUrl : qVar.a.a.equals(waterPackageName) ? waterUrl : qVar.a.a.equals(cutePackageName) ? cuteUrl : null;
                    if (str != null) {
                        Iterator it = list.iterator();
                        while (it.hasNext()) {
                            if (str.equals(((DownloadInfo) it.next()).a)) {
                                qVar.d = true;
                            }
                        }
                    }
                }
            }
            this.mHandler.sendEmptyMessage(REFRESH_THEME_LIST);
        }
    }

    private void showInstallDialog(e eVar, File file) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
        builder.setTitle((int) R.string.theme_install_recomend);
        builder.setMessage((int) R.string.theme_install_recomend_local_msg);
        builder.setPositiveButton((int) R.string.comfirm, new y(this, file));
        builder.setNegativeButton((int) R.string.cancel, new ac(this));
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public void updateThemeList() {
        List list = null;
        if (this.mService != null) {
            try {
                list = this.mService.b();
            } catch (Exception e) {
            }
        }
        List themeList2 = getThemeList(list);
        this.themeList = themeList2;
        this.mAdapter.a = themeList2;
        this.mAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public void applyDefaultTheme() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
        builder.setTitle((int) R.string.theme_apply);
        builder.setMessage((int) R.string.theme_apply_default);
        builder.setPositiveButton((int) R.string.comfirm, new ab(this));
        builder.setNegativeButton((int) R.string.cancel, new aa(this));
        builder.create().show();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mContext = this;
        setContentView((int) R.layout.theme_main_activity);
        this.mThemeGridView = (GridView) findViewById(R.id.theme_gridview);
        this.mLoadMoreView = findViewById(R.id.theme_download_more);
        downLoadServiceInit();
        this.themeList = getThemeList(null);
        this.mLoadMoreView.setOnClickListener(new ag(this));
        this.mAdapter = new f(this, this.mContext, this.themeList);
        e g = l.a().g();
        int size = this.themeList.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                break;
            } else if (g.a.equals(((q) this.themeList.get(i)).a.a)) {
                this.mAdapter.b = i;
                break;
            } else {
                i++;
            }
        }
        this.mThemeGridView.setAdapter((ListAdapter) this.mAdapter);
        this.mThemeGridView.setOnItemClickListener(new af(this));
        this.alphaAnim = new AlphaAnimation(0.3f, 0.3f);
        this.alphaAnim.setFillAfter(true);
        IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
        intentFilter.addAction("android.intent.action.PACKAGE_CHANGED");
        intentFilter.addDataScheme("package");
        registerReceiver(this.mReceiver, intentFilter);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        try {
            unregisterReceiver(this.mReceiver);
            unbindService(this.connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        e g = l.a().g();
        int size = this.themeList.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                break;
            } else if (g.a.equals(((q) this.themeList.get(i)).a.a)) {
                this.mAdapter.b = i;
                this.mAdapter.notifyDataSetChanged();
                break;
            } else {
                i++;
            }
        }
        IntentFilter intentFilter = new IntentFilter("com.tencent.launcher.theme.download.complete");
        intentFilter.setPriority(1000);
        registerReceiver(this.mDownReceiver, intentFilter);
        if (this.mService != null) {
            try {
                refreshThemeList(this.mService.b());
            } catch (Exception e) {
            }
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        unregisterReceiver(this.mDownReceiver);
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void showDownLoadDialog(e eVar) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
        builder.setTitle((int) R.string.theme_install_recomend);
        builder.setMessage((int) R.string.theme_install_recomend_msg);
        builder.setPositiveButton((int) R.string.comfirm, new ai(this, eVar));
        builder.setNegativeButton((int) R.string.cancel, new ah(this));
        builder.create().show();
    }
}
