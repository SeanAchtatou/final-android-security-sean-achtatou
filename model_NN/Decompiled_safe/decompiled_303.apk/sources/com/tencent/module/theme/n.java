package com.tencent.module.theme;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import com.tencent.util.f;
import java.util.ArrayList;
import java.util.List;

public final class n implements k {
    private static n a;

    private n() {
    }

    public static n a() {
        if (a == null) {
            a = new n();
        }
        return a;
    }

    public final e a(Context context, String str) {
        try {
            f fVar = new f(context, str);
            e eVar = new e();
            eVar.a = str;
            eVar.b = fVar.a("theme_name");
            eVar.c = 1;
            eVar.e = fVar.a("theme_author");
            eVar.f = fVar.a("theme_description");
            eVar.d = Integer.parseInt(fVar.a("theme_version"));
            eVar.g = Integer.parseInt(fVar.a("theme_support_version"));
            return eVar;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final List a(Context context) {
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(new Intent("com.tencent.msf.THEME"), 0);
        int size = queryIntentActivities.size();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < size; i++) {
            e a2 = a(context, queryIntentActivities.get(i).activityInfo.packageName);
            if (a2 != null) {
                arrayList.add(a2);
            }
        }
        return arrayList;
    }
}
