package com.tencent.module.theme;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import com.tencent.module.download.s;
import com.tencent.module.download.y;
import java.util.List;

final class ae implements ServiceConnection {
    private /* synthetic */ ThemeSettingActivity a;

    ae(ThemeSettingActivity themeSettingActivity) {
        this.a = themeSettingActivity;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        y unused = this.a.mService = s.a(iBinder);
        List list = null;
        try {
            list = this.a.mService.b();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        if (list != null) {
            try {
                this.a.refreshThemeList(list);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        y unused = this.a.mService = null;
    }
}
