package com.tencent.module.qqwidget;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;

final class k implements ServiceConnection {
    private /* synthetic */ QQWidgetFrame a;

    k(QQWidgetFrame qQWidgetFrame) {
        this.a = qQWidgetFrame;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        l gVar;
        QQWidgetFrame qQWidgetFrame = this.a;
        if (iBinder == null) {
            gVar = null;
        } else {
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.tencent.module.qqwidget.WidgetHandler");
            gVar = (queryLocalInterface == null || !(queryLocalInterface instanceof l)) ? new g(iBinder) : (l) queryLocalInterface;
        }
        qQWidgetFrame.a = gVar;
        try {
            this.a.a.a(this.a.b, this.a.c);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        this.a.a = null;
    }
}
