package com.tencent.module.qqwidget;

import android.appwidget.AppWidgetHost;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.XmlResourceParser;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import com.tencent.launcher.BrightnessActivity;
import com.tencent.launcher.base.BaseApp;
import com.tencent.util.a;
import com.tencent.util.f;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class h {
    /* access modifiers changed from: private */
    public static h a;
    /* access modifiers changed from: private */
    public static Thread c;
    private AppWidgetHost b;
    private ArrayList d = new ArrayList();
    private ConcurrentHashMap e = new ConcurrentHashMap();

    private h() {
    }

    public static a a(Context context, String str, int i) {
        try {
            XmlResourceParser c2 = new f(context, str).c(i);
            AttributeSet asAttributeSet = Xml.asAttributeSet(c2);
            a.a(c2, "QQWidget");
            int attributeCount = asAttributeSet.getAttributeCount();
            a aVar = new a();
            for (int i2 = 0; i2 < attributeCount; i2++) {
                String attributeName = c2.getAttributeName(i2);
                String attributeValue = c2.getAttributeValue(i2);
                if ("name".equals(attributeName)) {
                    aVar.b = attributeValue;
                } else if ("icon".equals(attributeName)) {
                    aVar.c = Integer.parseInt(attributeValue.substring(1));
                } else if ("spanX".equals(attributeName)) {
                    aVar.r = Integer.parseInt(attributeValue);
                } else if ("spanY".equals(attributeName)) {
                    aVar.s = Integer.parseInt(attributeValue);
                } else if ("layoutId".equals(attributeName)) {
                    aVar.h = Integer.parseInt(attributeValue.substring(1));
                } else if ("maxInstance".equals(attributeName)) {
                    aVar.g = Integer.parseInt(attributeValue);
                }
            }
            return aVar;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static h a() {
        if (a == null) {
            a = b(BaseApp.b());
        }
        return a;
    }

    private static h b(Context context) {
        synchronized (h.class) {
            h hVar = new h();
            a = hVar;
            hVar.b = new AppWidgetHost(context, 1048576);
            f fVar = new f(context);
            c = fVar;
            fVar.start();
        }
        return a;
    }

    public final a a(int i) {
        if (i >= 0 || i < this.d.size()) {
            return new a((a) this.d.get(i));
        }
        return null;
    }

    public final a a(String str, String str2) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.d.size()) {
                return null;
            }
            a aVar = (a) this.d.get(i2);
            if (aVar.e.equals(str) && aVar.f.equals(str2)) {
                return new a(aVar);
            }
            i = i2 + 1;
        }
    }

    public final synchronized e a(Context context, a aVar) {
        e eVar;
        if (aVar.d == a.a) {
            int allocateAppWidgetId = this.b.allocateAppWidgetId();
            this.b.deleteAppWidgetId(allocateAppWidgetId);
            aVar.d = allocateAppWidgetId;
        }
        if (((e) this.e.get(aVar)) != null) {
            b(aVar);
        }
        eVar = new e(context, aVar);
        this.e.put(aVar, eVar);
        return eVar;
    }

    public final void a(Context context) {
        a a2;
        System.currentTimeMillis();
        this.d.clear();
        List<ResolveInfo> queryIntentServices = context.getPackageManager().queryIntentServices(new Intent("com.tencent.qqwidget.service"), BrightnessActivity.DEFAULT_BACKLIGHT);
        for (int i = 0; i < queryIntentServices.size(); i++) {
            ServiceInfo serviceInfo = queryIntentServices.get(i).serviceInfo;
            if (!serviceInfo.exported) {
                Log.w("QQWidgetManager", serviceInfo.packageName + "|" + serviceInfo.name + ": is not exported.");
            }
            int i2 = serviceInfo.metaData.getInt("tencent.qqwidget.service", 0);
            if (!(i2 == 0 || (a2 = a(context, serviceInfo.packageName, i2)) == null)) {
                a2.e = serviceInfo.packageName;
                a2.f = serviceInfo.name;
                this.d.add(a2);
            }
        }
    }

    public final void a(a aVar) {
        if (this.d != null && aVar != null) {
            this.d.add(aVar);
        }
    }

    public final ArrayList b() {
        if (c != null) {
            try {
                c.join();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return this.d;
    }

    public final synchronized void b(a aVar) {
        e eVar = (e) this.e.get(aVar);
        if (eVar != null) {
            this.e.remove(aVar);
            try {
                eVar.c.invoke(eVar.a, new Object[0]);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return;
    }

    public final synchronized void c() {
        Iterator it = this.e.keySet().iterator();
        for (boolean hasNext = it.hasNext(); hasNext; hasNext = it.hasNext()) {
            e eVar = (e) this.e.get((a) it.next());
            try {
                eVar.b.invoke(eVar.a, new Object[0]);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        this.e.clear();
    }
}
