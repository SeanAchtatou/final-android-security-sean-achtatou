package com.tencent.module.qqwidget;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;

final class n implements c {
    private IBinder a;

    n(IBinder iBinder) {
        this.a = iBinder;
    }

    public final void a(Bundle bundle) {
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.module.qqwidget.WidgetListener");
            if (bundle != null) {
                obtain.writeInt(1);
                bundle.writeToParcel(obtain, 0);
            } else {
                obtain.writeInt(0);
            }
            this.a.transact(1, obtain, null, 1);
        } finally {
            obtain.recycle();
        }
    }

    public final IBinder asBinder() {
        return this.a;
    }
}
