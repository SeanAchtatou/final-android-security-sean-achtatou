package com.tencent.module.qqwidget;

final class d implements Runnable {
    private int a;
    private /* synthetic */ e b;

    d(e eVar) {
        this.b = eVar;
    }

    public final void a() {
        this.a = this.b.getWindowAttachCount();
    }

    public final void run() {
        if (this.b.getParent() != null && this.b.hasWindowFocus() && this.a == this.b.getWindowAttachCount() && !this.b.d && this.b.performLongClick()) {
            this.b.d = true;
        }
    }
}
