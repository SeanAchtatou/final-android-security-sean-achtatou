package com.tencent.module.qqwidget;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.LinearLayout;
import com.tencent.util.f;
import java.lang.reflect.Method;

public final class e extends LinearLayout {
    View a;
    Method b;
    Method c;
    boolean d = false;
    private a e;
    private Method f;
    private d g = null;
    private float h = 0.0f;
    private float i = 0.0f;

    protected e(Context context, a aVar) {
        super(context);
        try {
            setOrientation(1);
            setGravity(17);
            this.a = new f(context, aVar.e).a(aVar.h);
            this.e = aVar;
            Class<?> cls = this.a.getClass();
            this.f = cls.getMethod("startWidgetService", Integer.TYPE, String.class);
            this.b = cls.getMethod("stopWidgetService", new Class[0]);
            this.c = cls.getMethod("deleteWidget", new Class[0]);
            addView(this.a, new LinearLayout.LayoutParams(-1, -1));
        } catch (Exception e2) {
            e2.printStackTrace();
            if (aVar != null) {
                Log.w("QQWidget", "widget fail widgetInfo=" + aVar.toString());
            } else {
                Log.w("QQWidget", "widget fail widgetInfo=null");
            }
            throw new QQWidgetFormatExcepiton();
        }
    }

    public final void a() {
        int i2 = this.e.d;
        String str = this.e.f;
        try {
            this.f.invoke(this.a, Integer.valueOf(i2), str);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void cancelLongPress() {
        super.cancelLongPress();
        this.d = false;
        if (this.g != null) {
            removeCallbacks(this.g);
        }
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.d) {
            this.d = false;
            return true;
        }
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        switch (motionEvent.getAction()) {
            case 0:
                this.d = false;
                if (this.g == null) {
                    this.g = new d(this);
                }
                this.g.a();
                postDelayed(this.g, (long) ViewConfiguration.getLongPressTimeout());
                this.h = y;
                this.i = x;
                break;
            case 1:
            case 3:
                this.d = false;
                if (this.g != null) {
                    removeCallbacks(this.g);
                    break;
                }
                break;
        }
        return false;
    }
}
