package com.tencent.module.qqwidget;

import android.os.Bundle;

final class b extends i {
    private /* synthetic */ QQWidgetService a;

    b(QQWidgetService qQWidgetService) {
        this.a = qQWidgetService;
    }

    public final Bundle a(Bundle bundle) {
        int i = bundle.getInt("__action", -1);
        int i2 = bundle.getInt("__widget_id", -1);
        switch (i) {
            case 1001:
                if (i2 != -1) {
                    this.a.a.remove(Integer.valueOf(i2));
                    break;
                }
                break;
            case 1002:
                if (i2 != -1) {
                    this.a.a.remove(Integer.valueOf(i2));
                    break;
                }
                break;
        }
        return this.a.a();
    }

    public final void a(int i, c cVar) {
        this.a.a.put(Integer.valueOf(i), cVar);
    }
}
