package com.tencent.module.qqwidget;

import android.content.Context;
import android.content.ServiceConnection;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class QQWidgetFrame extends FrameLayout {
    protected l a;
    protected int b = -1;
    c c = new j(this);
    private Context d;
    private ServiceConnection e = new k(this);

    public QQWidgetFrame(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.d = context;
    }

    /* access modifiers changed from: protected */
    public final void onFinishInflate() {
        super.onFinishInflate();
    }
}
