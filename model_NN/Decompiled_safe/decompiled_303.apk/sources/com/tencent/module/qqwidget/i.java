package com.tencent.module.qqwidget;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

public abstract class i extends Binder implements l {
    public i() {
        attachInterface(this, "com.tencent.module.qqwidget.WidgetHandler");
    }

    public IBinder asBinder() {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        c nVar;
        switch (i) {
            case 1:
                parcel.enforceInterface("com.tencent.module.qqwidget.WidgetHandler");
                Bundle a = a(parcel.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(parcel) : null);
                parcel2.writeNoException();
                if (a != null) {
                    parcel2.writeInt(1);
                    a.writeToParcel(parcel2, 1);
                } else {
                    parcel2.writeInt(0);
                }
                return true;
            case 2:
                parcel.enforceInterface("com.tencent.module.qqwidget.WidgetHandler");
                int readInt = parcel.readInt();
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder == null) {
                    nVar = null;
                } else {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.tencent.module.qqwidget.WidgetListener");
                    nVar = (queryLocalInterface == null || !(queryLocalInterface instanceof c)) ? new n(readStrongBinder) : (c) queryLocalInterface;
                }
                a(readInt, nVar);
                parcel2.writeNoException();
                return true;
            case 1598968902:
                parcel2.writeString("com.tencent.module.qqwidget.WidgetHandler");
                return true;
            default:
                return super.onTransact(i, parcel, parcel2, i2);
        }
    }
}
