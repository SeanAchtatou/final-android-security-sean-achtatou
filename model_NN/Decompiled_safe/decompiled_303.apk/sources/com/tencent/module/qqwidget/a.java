package com.tencent.module.qqwidget;

import android.content.ContentValues;
import com.tencent.launcher.bd;

public final class a extends bd {
    public static int a = -1;
    public String b;
    public int c;
    public int d = a;
    public String e;
    public String f;
    public int g = 0;

    public a() {
        this.m = 10000;
    }

    public a(a aVar) {
        this.l = aVar.l;
        this.p = aVar.p;
        this.q = aVar.q;
        this.r = aVar.r;
        this.s = aVar.s;
        this.o = aVar.o;
        this.m = aVar.m;
        this.n = aVar.n;
        this.h = aVar.h;
        this.b = aVar.b;
        this.c = aVar.c;
        this.d = aVar.d;
        this.e = aVar.e;
        this.f = aVar.f;
        this.g = aVar.g;
    }

    public final void a(ContentValues contentValues) {
        super.a(contentValues);
        contentValues.put("title", this.b);
        contentValues.put("packageName", this.e);
        contentValues.put("className", this.f);
        contentValues.put("layoutId", Integer.valueOf(this.h));
        contentValues.put("appWidgetId", Integer.valueOf(this.d));
    }

    public final String toString() {
        return "[name=" + this.b + ",packageName=" + this.e + ",serviceName=" + this.f + "]";
    }
}
