package com.tencent.module.switcher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.provider.Settings;
import com.tencent.qqlauncher.R;

public final class ab extends ac {
    private int c;
    private int d;
    private BroadcastReceiver e = new n(this);

    public ab(Context context) {
        super(context);
        a(context);
    }

    public final void a() {
        Context context = this.a;
        Settings.System.putInt(context.getContentResolver(), "airplane_mode_on", Settings.System.getInt(context.getContentResolver(), "airplane_mode_on", 0) == 1 ? 0 : 1);
        context.sendBroadcast(new Intent("android.intent.action.AIRPLANE_MODE"));
    }

    public final void a(Context context) {
        switch (Settings.System.getInt(context.getContentResolver(), "airplane_mode_on", 0) == 1) {
            case false:
                this.c = R.drawable.ic_appwidget_settings_airplane_off;
                this.d = R.drawable.appwidget_settings_ind_off_c;
                break;
            default:
                this.c = R.drawable.ic_appwidget_settings_airplane_on;
                this.d = R.drawable.appwidget_settings_ind_on_c;
                break;
        }
        Resources resources = context.getResources();
        a(resources.getDrawable(this.c), resources.getDrawable(this.d));
    }

    public final String c() {
        return this.a.getString(R.string.airplane_switcher_toast);
    }

    public final Intent d() {
        Intent intent = new Intent();
        intent.setClassName("com.android.settings", "com.android.settings.WirelessSettings");
        return intent;
    }

    /* access modifiers changed from: protected */
    public final void e() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.AIRPLANE_MODE");
        this.a.registerReceiver(this.e, intentFilter);
    }

    /* access modifiers changed from: protected */
    public final void f() {
        this.a.unregisterReceiver(this.e);
        super.f();
    }
}
