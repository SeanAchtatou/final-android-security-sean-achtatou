package com.tencent.module.switcher;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.util.Log;
import com.tencent.qqlauncher.R;

public final class f extends ac {
    private int c;
    private int d;
    private boolean e = false;
    private Boolean f = null;
    private Boolean g = null;
    private boolean h = false;
    /* access modifiers changed from: private */
    public BluetoothAdapter i;
    private BroadcastReceiver j = new p(this);

    public f(Context context) {
        super(context);
        a(context);
    }

    public final void a() {
        boolean z;
        switch (b()) {
            case 0:
                z = true;
                break;
            case 1:
                z = false;
                break;
            case 5:
                if (this.g != null) {
                    if (this.g.booleanValue()) {
                        z = false;
                        break;
                    } else {
                        z = true;
                        break;
                    }
                }
            case 2:
            case 3:
            case 4:
            default:
                z = false;
                break;
        }
        this.g = Boolean.valueOf(z);
        if (this.e) {
            this.h = true;
            return;
        }
        this.e = true;
        if (this.i == null) {
            this.i = BluetoothAdapter.getDefaultAdapter();
        }
        new q(this, z).execute(new Void[0]);
    }

    public final void a(Context context) {
        switch (b()) {
            case 0:
                this.c = R.drawable.ic_appwidget_settings_bluetooth_off;
                this.d = R.drawable.appwidget_settings_ind_off_c;
                break;
            case 1:
                this.c = R.drawable.ic_appwidget_settings_bluetooth_on;
                this.d = R.drawable.appwidget_settings_ind_on_c;
                break;
            case 2:
            case 3:
            case 4:
            default:
                this.c = R.drawable.ic_appwidget_settings_bluetooth_off;
                this.d = R.drawable.appwidget_settings_ind_off_c;
                break;
            case 5:
                if (!(this.g != null && this.g.booleanValue())) {
                    this.c = R.drawable.ic_appwidget_settings_bluetooth_off;
                    this.d = R.drawable.appwidget_settings_ind_off_c;
                    break;
                } else {
                    this.c = R.drawable.ic_appwidget_settings_bluetooth_on;
                    this.d = R.drawable.appwidget_settings_ind_mid_c;
                    break;
                }
                break;
        }
        Resources resources = context.getResources();
        a(resources.getDrawable(this.c), resources.getDrawable(this.d));
    }

    public final void a(Intent intent) {
        if ("android.bluetooth.adapter.action.STATE_CHANGED".equals(intent.getAction())) {
            int intExtra = intent.getIntExtra("android.bluetooth.adapter.extra.STATE", -1);
            boolean z = this.e;
            switch (intExtra) {
                case 10:
                    this.e = false;
                    this.f = false;
                    break;
                case 11:
                    this.e = true;
                    this.f = false;
                    break;
                case 12:
                    this.e = false;
                    this.f = true;
                    break;
                case 13:
                    this.e = true;
                    this.f = true;
                    break;
            }
            if (z && !this.e && this.h) {
                Log.v("BluetoothSwitcher", "processing deferred state change");
                if (this.f != null && this.g != null && this.g.equals(this.f)) {
                    Log.v("BluetoothSwitcher", "... but intended state matches, so no changes.");
                } else if (this.g != null) {
                    this.e = true;
                    boolean booleanValue = this.g.booleanValue();
                    if (this.i == null) {
                        this.i = BluetoothAdapter.getDefaultAdapter();
                    }
                    new q(this, booleanValue).execute(new Void[0]);
                }
                this.h = false;
            }
        }
    }

    public final int b() {
        if (this.e) {
            return 5;
        }
        if (this.i == null) {
            this.i = BluetoothAdapter.getDefaultAdapter();
            if (this.i == null) {
                return 0;
            }
        }
        switch (this.i.getState()) {
            case 10:
                return 0;
            case 11:
            case 13:
                return 5;
            case 12:
                return 1;
            default:
                return 4;
        }
    }

    public final String c() {
        return this.a.getString(R.string.bluetooth_switcher_toast);
    }

    public final Intent d() {
        Intent intent = new Intent();
        intent.setClassName("com.android.settings", "com.android.settings.WirelessSettings");
        return intent;
    }

    /* access modifiers changed from: protected */
    public final void e() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.bluetooth.adapter.action.STATE_CHANGED");
        this.a.registerReceiver(this.j, intentFilter);
    }

    /* access modifiers changed from: protected */
    public final void f() {
        this.a.unregisterReceiver(this.j);
        super.f();
    }
}
