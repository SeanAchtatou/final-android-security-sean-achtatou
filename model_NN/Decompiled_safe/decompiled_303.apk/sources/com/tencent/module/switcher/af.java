package com.tencent.module.switcher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import com.tencent.qqlauncher.R;

public final class af extends ac {
    public static boolean c;
    private static Uri d = Uri.parse("content://telephony/carriers");
    private PhoneStateListener e = new w(this);
    private BroadcastReceiver f = new z(this);
    /* access modifiers changed from: private */
    public Handler g = new y(this);
    private BroadcastReceiver h = new x(this);

    public af(Context context) {
        super(context);
        b(context);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("gprs_status_update");
        this.a.registerReceiver(this.h, intentFilter);
    }

    public static void a(Context context) {
        c = false;
        Intent intent = new Intent();
        intent.setAction("gprs_status_update");
        context.sendBroadcast(intent);
    }

    /* access modifiers changed from: private */
    public void b(Context context) {
        boolean z = Settings.Secure.getInt(this.a.getContentResolver(), "mobile_data", 1) == 1;
        Resources resources = context.getResources();
        a(z ? resources.getDrawable(R.drawable.ic_appwidget_settings_gprs_on) : resources.getDrawable(R.drawable.ic_appwidget_settings_gprs_off), (Drawable) null);
    }

    public final void a() {
        Intent intent = new Intent();
        intent.setClassName("com.android.phone", "com.android.phone.Settings");
        try {
            this.a.startActivity(intent);
            c = true;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final boolean b() {
        return Settings.Secure.getInt(this.a.getContentResolver(), "mobile_data", 1) == 1;
    }

    public final String c() {
        return this.a.getString(R.string.gprs_switcher_toast);
    }

    public final Intent d() {
        Intent intent = new Intent();
        intent.setClassName("com.android.phone", "com.android.phone.Settings");
        return intent;
    }

    /* access modifiers changed from: protected */
    public final void e() {
        ((TelephonyManager) this.a.getSystemService("phone")).listen(this.e, 64);
    }

    /* access modifiers changed from: protected */
    public final void f() {
        ((TelephonyManager) this.a.getSystemService("phone")).listen(this.e, 0);
        this.a.unregisterReceiver(this.h);
        super.f();
    }
}
