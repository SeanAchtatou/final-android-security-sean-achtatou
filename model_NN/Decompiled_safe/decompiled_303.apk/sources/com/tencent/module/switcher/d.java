package com.tencent.module.switcher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class d extends BroadcastReceiver {
    final /* synthetic */ l a;

    d(l lVar) {
        this.a = lVar;
    }

    public final void onReceive(Context context, Intent intent) {
        if ("android.location.PROVIDERS_CHANGED".equals(intent.getAction())) {
            this.a.b.post(new t(this, context));
        }
    }
}
