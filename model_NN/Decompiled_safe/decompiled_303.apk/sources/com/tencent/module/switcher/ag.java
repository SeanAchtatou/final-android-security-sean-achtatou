package com.tencent.module.switcher;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import com.tencent.launcher.base.b;

final class ag extends AsyncTask {
    private /* synthetic */ Context a;
    private /* synthetic */ boolean b;
    private /* synthetic */ boolean c;
    private /* synthetic */ k d;

    ag(k kVar, Context context, boolean z, boolean z2) {
        this.d = kVar;
        this.a = context;
        this.b = z;
        this.c = z2;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        if (b.a < 5) {
            Intent intent = new Intent();
            intent.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            intent.addCategory("android.intent.category.ALTERNATIVE");
            intent.setData(Uri.parse("custom:2"));
            this.a.sendBroadcast(intent);
            return true;
        } else if (this.b) {
            if (!this.c) {
                ContentResolver.setMasterSyncAutomatically(true);
            }
            return true;
        } else {
            if (this.c) {
                ContentResolver.setMasterSyncAutomatically(false);
            }
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        this.d.a(((Boolean) obj).booleanValue() ? 1 : 0);
        this.d.b(this.d.a);
    }
}
