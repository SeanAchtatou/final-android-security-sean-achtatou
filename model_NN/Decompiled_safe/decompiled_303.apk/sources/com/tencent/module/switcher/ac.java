package com.tencent.module.switcher;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Handler;

public abstract class ac {
    protected final Context a;
    protected Handler b = new Handler();
    private r c;
    private Drawable d;
    private Drawable e;

    public ac(Context context) {
        this.a = context;
    }

    public abstract void a();

    /* access modifiers changed from: protected */
    public final void a(Drawable drawable, Drawable drawable2) {
        this.d = drawable;
        this.e = drawable2;
        if (this.c != null) {
            this.c.b();
        }
    }

    public final void a(r rVar) {
        this.c = rVar;
    }

    public abstract String c();

    public Intent d() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void e() {
    }

    /* access modifiers changed from: protected */
    public void f() {
    }

    public final void g() {
        try {
            f();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final Drawable h() {
        return this.d;
    }
}
