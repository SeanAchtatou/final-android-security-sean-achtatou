package com.tencent.module.switcher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class ah extends BroadcastReceiver {
    final /* synthetic */ k a;

    ah(k kVar) {
        this.a = kVar;
    }

    public final void onReceive(Context context, Intent intent) {
        if ("com.android.sync.SYNC_CONN_STATUS_CHANGED".equals(intent.getAction())) {
            this.a.c(context);
        }
        this.a.b.post(new o(this, context));
    }
}
