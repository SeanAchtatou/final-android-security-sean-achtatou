package com.tencent.module.switcher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class z extends BroadcastReceiver {
    final /* synthetic */ af a;

    z(af afVar) {
        this.a = afVar;
    }

    public final void onReceive(Context context, Intent intent) {
        if ("android.location.PROVIDERS_CHANGED".equals(intent.getAction())) {
            this.a.g.post(new s(this, context));
        }
    }
}
