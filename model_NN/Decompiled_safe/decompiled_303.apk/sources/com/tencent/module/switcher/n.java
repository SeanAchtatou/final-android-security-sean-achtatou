package com.tencent.module.switcher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class n extends BroadcastReceiver {
    final /* synthetic */ ab a;

    n(ab abVar) {
        this.a = abVar;
    }

    public final void onReceive(Context context, Intent intent) {
        if ("android.intent.action.AIRPLANE_MODE".equals(intent.getAction())) {
            this.a.b.post(new m(this, context));
        }
    }
}
