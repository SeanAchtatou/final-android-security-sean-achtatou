package com.tencent.module.switcher;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.provider.Settings;
import com.tencent.qqlauncher.R;

public final class ad extends ac {
    private int c;
    private int d;

    public ad(Context context) {
        super(context);
        b(context);
    }

    public static int a(Context context) {
        boolean z;
        try {
            z = Settings.System.getInt(context.getContentResolver(), "accelerometer_rotation") == 1;
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
            z = false;
        }
        return z ? 1 : 0;
    }

    private void b(Context context) {
        switch (a(context)) {
            case 0:
                this.c = R.drawable.ic_appwidget_settings_gravity_off;
                this.d = R.drawable.appwidget_settings_ind_off_c;
                break;
            case 1:
                this.c = R.drawable.ic_appwidget_settings_gravity_on;
                this.d = R.drawable.appwidget_settings_ind_on_c;
                break;
            default:
                this.c = R.drawable.ic_appwidget_settings_gravity_off;
                this.d = R.drawable.appwidget_settings_ind_off_c;
                break;
        }
        Resources resources = context.getResources();
        a(resources.getDrawable(this.c), resources.getDrawable(this.d));
    }

    public final void a() {
        Context context = this.a;
        switch (a(context)) {
            case 0:
                Settings.System.putInt(context.getContentResolver(), "accelerometer_rotation", 1);
                break;
            case 1:
                Settings.System.putInt(context.getContentResolver(), "accelerometer_rotation", 0);
                break;
        }
        b(context);
    }

    public final String c() {
        return this.a.getString(R.string.grayity_switcher_toast);
    }

    public final Intent d() {
        Intent intent = new Intent();
        intent.setClassName("com.android.settings", "com.android.settings.DisplaySettings");
        return intent;
    }
}
