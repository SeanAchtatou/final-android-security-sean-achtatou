package com.tencent.module.switcher;

import android.content.Context;
import android.view.View;
import com.tencent.launcher.base.BaseApp;
import com.tencent.launcher.base.b;
import com.tencent.launcher.home.i;
import com.tencent.module.setting.DesktopSwitchSpecialEffectSettingActivity;
import com.tencent.qqlauncher.R;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public final class v {
    private static v a = null;
    private static List c = new ArrayList();
    private List b = null;

    private v() {
    }

    public static SwitcherView a(Context context, int i) {
        SwitcherView switcherView = (SwitcherView) View.inflate(context, R.layout.switcher, null);
        ac b2 = b(context, i);
        if (b2 != null) {
            c.add(new WeakReference(b2));
        }
        switcherView.a(b2);
        return switcherView;
    }

    public static v a() {
        if (a == null) {
            a = new v();
        }
        return a;
    }

    public static ac b(Context context, int i) {
        switch (i) {
            case 0:
                return new aa(context);
            case 1:
                return new f(context);
            case 2:
                return new l(context);
            case 3:
                return new k(context);
            case 4:
                return new i(context);
            case 5:
                return new aj(context);
            case 6:
                return new ad(context);
            case DesktopSwitchSpecialEffectSettingActivity.nRotationSetting:
                return new ab(context);
            case DesktopSwitchSpecialEffectSettingActivity.nCubeSetting:
                return new af(context);
            default:
                return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.home.i.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.launcher.home.i.b(java.lang.String, int):int
      com.tencent.launcher.home.i.b(java.lang.String, java.lang.String):java.lang.String
      com.tencent.launcher.home.i.b(java.lang.String, boolean):boolean */
    public final List b() {
        if (this.b == null) {
            Context b2 = BaseApp.b();
            ArrayList arrayList = new ArrayList();
            int i = b.a;
            i a2 = i.a();
            if (a2.b("type_wifi", true)) {
                arrayList.add(a(b2, 0));
            }
            if (a2.b("type_brightness", true)) {
                arrayList.add(a(b2, 4));
            }
            if (a2.b("type_vibrate", true)) {
                arrayList.add(a(b2, 5));
            }
            if (a2.b("type_gps", true)) {
                arrayList.add(a(b2, 2));
            }
            if (a2.b("type_gravity", true)) {
                arrayList.add(a(b2, 6));
            }
            if (a2.b("type_airplane", true)) {
                arrayList.add(a(b2, 7));
            }
            if (i > 4) {
                if (a2.b("type_bluetooth", true)) {
                    arrayList.add(a(b2, 1));
                }
                if (a2.b("type_sync", true)) {
                    arrayList.add(a(b2, 3));
                }
            }
            this.b = arrayList;
        }
        return this.b;
    }

    public final void c() {
        if (this.b != null) {
            for (int i = 0; i < this.b.size(); i++) {
                ac a2 = ((SwitcherView) this.b.get(i)).a();
                if (a2 != null) {
                    a2.g();
                }
            }
            this.b.clear();
        }
        int size = c.size();
        for (int i2 = 0; i2 < size; i2++) {
            ac acVar = (ac) ((WeakReference) c.get(i2)).get();
            if (acVar != null) {
                acVar.g();
            }
        }
        c.clear();
        a = null;
    }
}
