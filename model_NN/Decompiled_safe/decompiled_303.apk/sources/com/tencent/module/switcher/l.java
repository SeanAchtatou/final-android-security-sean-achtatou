package com.tencent.module.switcher;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.Settings;
import com.tencent.launcher.base.BaseApp;
import com.tencent.launcher.base.b;
import com.tencent.qqlauncher.R;

public final class l extends ac {
    private int c;
    private int d;
    private LocationManager e;
    private BroadcastReceiver f = new d(this);

    public l(Context context) {
        super(context);
        this.e = (LocationManager) context.getSystemService("location");
        b(context);
    }

    public final int a(Context context) {
        return b.a >= 8 ? Settings.System.getString(context.getContentResolver(), "location_providers_allowed").contains("gps") : this.e.isProviderEnabled("gps") ? 1 : 0;
    }

    public final void a() {
        if (b.a >= 9) {
            BaseApp.a(this.a.getString(R.string.gps_switcher_toast));
            Intent intent = new Intent();
            intent.setClassName("com.android.settings", "com.android.settings.SecuritySettings");
            try {
                this.a.startActivity(intent);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        Context context = this.a;
        Intent intent2 = new Intent();
        intent2.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
        intent2.addCategory("android.intent.category.ALTERNATIVE");
        intent2.setData(Uri.parse("custom:3"));
        try {
            PendingIntent.getBroadcast(context, 0, intent2, 0).send();
        } catch (PendingIntent.CanceledException e3) {
            e3.printStackTrace();
        }
        this.b.postDelayed(new e(this, context), 200);
    }

    public final void b(Context context) {
        switch (a(context)) {
            case 0:
                this.c = R.drawable.ic_appwidget_settings_gps_off;
                this.d = R.drawable.appwidget_settings_ind_off_c;
                break;
            case 1:
                this.c = R.drawable.ic_appwidget_settings_gps_on;
                this.d = R.drawable.appwidget_settings_ind_on_c;
                break;
            default:
                this.c = R.drawable.ic_appwidget_settings_gps_off;
                this.d = R.drawable.appwidget_settings_ind_off_c;
                break;
        }
        Resources resources = context.getResources();
        a(resources.getDrawable(this.c), resources.getDrawable(this.d));
    }

    public final String c() {
        return this.a.getString(R.string.gps_switcher_toast);
    }

    public final Intent d() {
        Intent intent = new Intent();
        intent.setClassName("com.android.settings", "com.android.settings.SecuritySettings");
        return intent;
    }

    /* access modifiers changed from: protected */
    public final void e() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.location.PROVIDERS_CHANGED");
        this.a.registerReceiver(this.f, intentFilter);
    }

    /* access modifiers changed from: protected */
    public final void f() {
        this.a.unregisterReceiver(this.f);
        super.f();
    }
}
