package com.tencent.module.switcher;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.provider.Settings;
import android.util.Log;
import com.tencent.launcher.BrightnessActivity;
import com.tencent.qqlauncher.R;

public final class i extends ac {
    private int c;
    private int d;
    private BroadcastReceiver e = new h(this);

    public i(Context context) {
        super(context);
        c(context);
    }

    public static int a(Context context) {
        try {
            return Settings.System.getInt(context.getContentResolver(), "screen_brightness");
        } catch (Exception e2) {
            Log.d("BrightnessSwitcher", "getBrightness: " + e2);
            return 30;
        }
    }

    public static boolean b(Context context) {
        try {
            return Settings.System.getInt(context.getContentResolver(), BrightnessActivity.BRIGHT_MODE) == 1;
        } catch (Exception e2) {
            Log.d("BrightnessSwitcher", "getBrightnessMode: " + e2);
            return false;
        }
    }

    public final void a() {
        Context context = this.a;
        try {
            PendingIntent.getActivity(context, 0, new Intent(context, BrightnessActivity.class), 0).send();
        } catch (PendingIntent.CanceledException e2) {
            e2.printStackTrace();
        }
        this.b.postDelayed(new g(this), 200);
    }

    public final String c() {
        return this.a.getString(R.string.brightness_switcher_toast);
    }

    public final void c(Context context) {
        int a = a(context);
        if (b(context)) {
            this.c = R.drawable.ic_appwidget_settings_brightness_auto;
            this.d = R.drawable.appwidget_settings_ind_on_c;
        } else if (30 < a && a <= 128) {
            this.c = R.drawable.ic_appwidget_settings_brightness_mid;
            this.d = R.drawable.appwidget_settings_ind_on_c;
        } else if (a <= 128 || a > 255) {
            this.c = R.drawable.ic_appwidget_settings_brightness_off;
            this.d = R.drawable.appwidget_settings_ind_off_c;
        } else {
            this.c = R.drawable.ic_appwidget_settings_brightness_on;
            this.d = R.drawable.appwidget_settings_ind_on_c;
        }
        Resources resources = context.getResources();
        a(resources.getDrawable(this.c), resources.getDrawable(this.d));
    }

    public final Intent d() {
        Intent intent = new Intent();
        intent.setClassName("com.android.settings", "com.android.settings.DisplaySettings");
        return intent;
    }

    /* access modifiers changed from: protected */
    public final void e() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.tencent.qqlauncher.brightness.STATE_CHANGED");
        this.a.registerReceiver(this.e, intentFilter);
    }

    /* access modifiers changed from: protected */
    public final void f() {
        try {
            this.a.unregisterReceiver(this.e);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        super.f();
    }
}
