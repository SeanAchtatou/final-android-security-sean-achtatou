package com.tencent.module.switcher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class p extends BroadcastReceiver {
    final /* synthetic */ f a;

    p(f fVar) {
        this.a = fVar;
    }

    public final void onReceive(Context context, Intent intent) {
        if ("android.bluetooth.adapter.action.STATE_CHANGED".equals(intent.getAction())) {
            this.a.a(intent);
        }
        this.a.b.post(new c(this, context));
    }
}
