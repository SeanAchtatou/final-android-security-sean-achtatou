package com.tencent.module.switcher;

import android.os.AsyncTask;

final class q extends AsyncTask {
    private /* synthetic */ boolean a;
    private /* synthetic */ f b;

    q(f fVar, boolean z) {
        this.b = fVar;
        this.a = z;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        if (this.a) {
            this.b.i.enable();
            return null;
        }
        this.b.i.disable();
        return null;
    }
}
