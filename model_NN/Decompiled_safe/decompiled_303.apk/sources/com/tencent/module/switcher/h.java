package com.tencent.module.switcher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class h extends BroadcastReceiver {
    final /* synthetic */ i a;

    h(i iVar) {
        this.a = iVar;
    }

    public final void onReceive(Context context, Intent intent) {
        if ("com.tencent.qqlauncher.brightness.STATE_CHANGED".equals(intent.getAction())) {
            this.a.b.post(new ae(this, context));
        }
    }
}
