package com.tencent.module.switcher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.media.AudioManager;
import com.tencent.qqlauncher.R;

public final class aj extends ac {
    private int c;
    private int d;
    private BroadcastReceiver e = new ai(this);

    public aj(Context context) {
        super(context);
        a(context);
    }

    public final void a() {
        Context context = this.a;
        AudioManager audioManager = (AudioManager) context.getSystemService("audio");
        if (audioManager.getRingerMode() == 2) {
            audioManager.setRingerMode(0);
        } else {
            audioManager.setRingerMode(2);
        }
        a(context);
    }

    public final void a(Context context) {
        if (((AudioManager) context.getSystemService("audio")).getRingerMode() == 0) {
            this.c = R.drawable.ic_appwidget_settings_rightone_off_nor;
            this.d = R.drawable.appwidget_settings_ind_on_c;
        } else {
            this.c = R.drawable.ic_appwidget_settings_rightone_on_nor;
            this.d = R.drawable.appwidget_settings_ind_off_c;
        }
        Resources resources = context.getResources();
        a(resources.getDrawable(this.c), resources.getDrawable(this.d));
    }

    public final String c() {
        return this.a.getString(R.string.vibrate_switcher_toast);
    }

    public final Intent d() {
        Intent intent = new Intent();
        intent.setClassName("com.android.settings", "com.android.settings.SoundSettings");
        return intent;
    }

    /* access modifiers changed from: protected */
    public final void e() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.media.RINGER_MODE_CHANGED");
        this.a.registerReceiver(this.e, intentFilter);
    }

    /* access modifiers changed from: protected */
    public final void f() {
        this.a.unregisterReceiver(this.e);
        super.f();
    }
}
