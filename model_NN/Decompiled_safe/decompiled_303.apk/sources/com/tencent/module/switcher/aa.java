package com.tencent.module.switcher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.net.wifi.WifiManager;
import android.util.Log;
import com.tencent.qqlauncher.R;

public final class aa extends ac {
    /* access modifiers changed from: private */
    public final WifiManager c;
    private int d;
    private int e;
    private boolean f = false;
    private Boolean g = null;
    private Boolean h = null;
    private boolean i = false;
    private BroadcastReceiver j = new b(this);

    public aa(Context context) {
        super(context);
        this.c = (WifiManager) context.getSystemService("wifi");
        a(context);
    }

    private void a(boolean z) {
        if (this.c == null) {
            Log.d("WifiSwitcher", "No wifiManager.");
        } else {
            new a(this, z).execute(new Void[0]);
        }
    }

    public final void a() {
        boolean z;
        switch (b()) {
            case 0:
                z = true;
                break;
            case 1:
                z = false;
                break;
            case 5:
                if (this.h != null) {
                    if (this.h.booleanValue()) {
                        z = false;
                        break;
                    } else {
                        z = true;
                        break;
                    }
                }
            case 2:
            case 3:
            case 4:
            default:
                z = false;
                break;
        }
        this.h = Boolean.valueOf(z);
        if (this.f) {
            this.i = true;
            return;
        }
        this.f = true;
        a(z);
    }

    public final void a(Context context) {
        switch (b()) {
            case 0:
                this.d = R.drawable.ic_appwidget_settings_wifi_off;
                this.e = R.drawable.appwidget_settings_ind_off_c;
                break;
            case 1:
                this.d = R.drawable.ic_appwidget_settings_wifi_on;
                this.e = R.drawable.appwidget_settings_ind_on_c;
                break;
            case 2:
            case 3:
            case 4:
            default:
                this.d = R.drawable.ic_appwidget_settings_wifi_off;
                this.e = R.drawable.appwidget_settings_ind_off_c;
                break;
            case 5:
                if (!(this.h != null && this.h.booleanValue())) {
                    this.d = R.drawable.ic_appwidget_settings_wifi_off;
                    this.e = R.drawable.appwidget_settings_ind_off_c;
                    break;
                } else {
                    this.d = R.drawable.ic_appwidget_settings_wifi_on;
                    this.e = R.drawable.appwidget_settings_ind_mid_c;
                    break;
                }
                break;
        }
        Resources resources = context.getResources();
        a(resources.getDrawable(this.d), resources.getDrawable(this.e));
    }

    public final void a(Intent intent) {
        if ("android.net.wifi.WIFI_STATE_CHANGED".equals(intent.getAction())) {
            int intExtra = intent.getIntExtra("wifi_state", -1);
            boolean z = this.f;
            switch (intExtra) {
                case 0:
                    this.f = true;
                    this.g = true;
                    break;
                case 1:
                    this.f = false;
                    this.g = false;
                    break;
                case 2:
                    this.f = true;
                    this.g = false;
                    break;
                case 3:
                    this.f = false;
                    this.g = true;
                    break;
            }
            if (z && !this.f && this.i) {
                Log.v("WifiSwitcher", "processing deferred state change");
                if (this.g != null && this.h != null && this.h.equals(this.g)) {
                    Log.v("WifiSwitcher", "... but intended state matches, so no changes.");
                } else if (this.h != null) {
                    this.f = true;
                    a(this.h.booleanValue());
                }
                this.i = false;
            }
        }
    }

    public final int b() {
        if (this.f) {
            return 5;
        }
        switch (this.c != null ? this.c.getWifiState() : 0) {
            case 0:
            case 2:
                return 5;
            case 1:
                return 0;
            case 3:
                return 1;
            default:
                return 4;
        }
    }

    public final String c() {
        return this.a.getString(R.string.wifi_switcher_toast);
    }

    public final Intent d() {
        Intent intent = new Intent();
        intent.setClassName("com.android.settings", "com.android.settings.WirelessSettings");
        return intent;
    }

    /* access modifiers changed from: protected */
    public final void e() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        this.a.registerReceiver(this.j, intentFilter);
    }

    /* access modifiers changed from: protected */
    public final void f() {
        try {
            this.a.unregisterReceiver(this.j);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        super.f();
    }
}
