package com.tencent.module.switcher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class b extends BroadcastReceiver {
    final /* synthetic */ aa a;

    b(aa aaVar) {
        this.a = aaVar;
    }

    public final void onReceive(Context context, Intent intent) {
        if ("android.net.wifi.WIFI_STATE_CHANGED".equals(intent.getAction())) {
            this.a.a(intent);
        }
        this.a.b.post(new u(this, context));
    }
}
