package com.tencent.module.switcher;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import com.tencent.qqlauncher.R;

public final class k extends ac {
    private int c;
    private int d;
    private Boolean e = null;
    private Boolean f = null;
    private BroadcastReceiver g = new ah(this);

    public k(Context context) {
        super(context);
        b(context);
    }

    public static int a(Context context) {
        return ((ConnectivityManager) context.getSystemService("connectivity")).getBackgroundDataSetting() && ContentResolver.getMasterSyncAutomatically() ? 1 : 0;
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        switch (i) {
            case 0:
                this.e = false;
                return;
            case 1:
                this.e = true;
                return;
            default:
                return;
        }
    }

    public final void a() {
        boolean z = true;
        Context context = this.a;
        switch (((ConnectivityManager) context.getSystemService("connectivity")).getBackgroundDataSetting() && ContentResolver.getMasterSyncAutomatically()) {
            case false:
                break;
            default:
                z = false;
                break;
        }
        context.getSystemService("connectivity");
        ((ConnectivityManager) context.getSystemService("connectivity")).getBackgroundDataSetting();
        new ag(this, context, z, ContentResolver.getMasterSyncAutomatically()).execute(new Void[0]);
    }

    public final void b(Context context) {
        switch (a(context)) {
            case 0:
                this.c = R.drawable.ic_appwidget_settings_sync_off;
                this.d = R.drawable.appwidget_settings_ind_off_c;
                break;
            case 1:
                this.c = R.drawable.ic_appwidget_settings_sync_on;
                this.d = R.drawable.appwidget_settings_ind_on_c;
                break;
        }
        Resources resources = context.getResources();
        a(resources.getDrawable(this.c), resources.getDrawable(this.d));
    }

    public final String c() {
        return this.a.getString(R.string.sync_switcher_toast);
    }

    public final void c(Context context) {
        a(a(context));
    }

    public final Intent d() {
        Intent intent = new Intent();
        intent.setClassName("com.android.providers.subscribedfeeds", "com.android.settings.ManageAccountsSettings");
        return intent;
    }

    /* access modifiers changed from: protected */
    public final void e() {
        new IntentFilter().addAction("com.android.sync.SYNC_CONN_STATUS_CHANGED");
    }

    /* access modifiers changed from: protected */
    public final void f() {
        this.a.unregisterReceiver(this.g);
        super.f();
    }
}
