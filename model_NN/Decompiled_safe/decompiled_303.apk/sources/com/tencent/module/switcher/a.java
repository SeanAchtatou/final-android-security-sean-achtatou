package com.tencent.module.switcher;

import android.os.AsyncTask;

final class a extends AsyncTask {
    private /* synthetic */ boolean a;
    private /* synthetic */ aa b;

    a(aa aaVar, boolean z) {
        this.b = aaVar;
        this.a = z;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        int wifiState = this.b.c.getWifiState();
        if (this.a && (wifiState == 2 || wifiState == 3)) {
            this.b.c.setWifiEnabled(false);
        }
        this.b.c.setWifiEnabled(this.a);
        return null;
    }
}
