package com.tencent.module.switcher;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.tencent.launcher.base.BaseApp;
import com.tencent.qqlauncher.R;

public class SwitcherView extends LinearLayout implements View.OnClickListener, View.OnLongClickListener, r {
    private ImageView a;
    private ac b;
    private Context c;
    private boolean d = false;

    public SwitcherView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = context;
        setOnLongClickListener(this);
    }

    public final ac a() {
        return this.b;
    }

    public final void a(ac acVar) {
        this.b = acVar;
        if (this.b != null) {
            this.b.e();
        }
        this.b.a(this);
        this.a.setImageDrawable(this.b.h());
    }

    public final void b() {
        this.a.setImageDrawable(this.b.h());
    }

    public void onClick(View view) {
        this.b.a();
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.a = (ImageView) findViewById(R.id.icon);
        this.a.setOnClickListener(this);
        this.a.setFocusable(true);
        this.a.setBackgroundResource(R.drawable.switcher_bg_selector);
        this.d = true;
    }

    public boolean onLongClick(View view) {
        if (this.b == null) {
            return false;
        }
        BaseApp.a(this.b.c());
        Intent d2 = this.b.d();
        if (d2 != null) {
            try {
                this.c.startActivity(d2);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return true;
    }
}
