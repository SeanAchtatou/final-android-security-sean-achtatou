package com.tencent.module.download.a;

import android.os.Handler;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

public final class b {
    public boolean a = false;
    public boolean b = false;
    public String c = "";
    private a d = null;
    private int e = 0;
    private int f = 0;
    private int g = 0;
    private boolean h = false;
    private String i = null;
    private int j = 0;
    private boolean k = true;
    private int l = 0;
    private byte[] m = null;
    private Handler n = null;
    private HttpPost o = null;
    private HttpGet p = null;
    private Map q = new HashMap();
    private int r = -1;
    private URI s;

    public b(String str, a aVar) {
        this.c = str;
        this.k = false;
        this.l = 10001;
        this.m = null;
        this.d = aVar;
        try {
            this.s = new URI(this.c);
        } catch (URISyntaxException e2) {
            e2.printStackTrace();
        }
    }

    public final int a() {
        return this.e;
    }

    public final void a(int i2) {
        this.e = i2;
    }

    public final void b() {
        this.f = 2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:161:0x034a, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x034b, code lost:
        r1 = r3;
     */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x02f1  */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x02fb  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x033c  */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x0346  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x034a A[ExcHandler: all (th java.lang.Throwable), Splitter:B:93:0x0209] */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x034e  */
    /* JADX WARNING: Removed duplicated region for block: B:187:0x0395  */
    /* JADX WARNING: Removed duplicated region for block: B:188:0x0397  */
    /* JADX WARNING: Removed duplicated region for block: B:189:0x039a  */
    /* JADX WARNING: Removed duplicated region for block: B:190:0x039d  */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x03a0  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0128  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0132  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0184  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x018e  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x01e0  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x01ea  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c() {
        /*
            r11 = this;
            r2 = 1
            r9 = 0
            r8 = 2
            r7 = 0
            boolean r0 = r11.a
            if (r0 == 0) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            java.net.URI r0 = r11.s
            java.lang.String r1 = r0.getScheme()
            if (r1 == 0) goto L_0x001d
            java.lang.String r0 = r0.getScheme()
            java.lang.String r1 = "http"
            boolean r0 = r0.startsWith(r1)
            if (r0 != 0) goto L_0x00a5
        L_0x001d:
            r0 = r9
        L_0x001e:
            if (r0 == 0) goto L_0x0008
            r2 = r7
            r0 = r7
        L_0x0022:
            int r1 = r11.g
            r3 = 3
            if (r1 >= r3) goto L_0x0008
            org.apache.http.params.BasicHttpParams r1 = new org.apache.http.params.BasicHttpParams     // Catch:{ ClientProtocolException -> 0x0389, SocketException -> 0x0382, SocketTimeoutException -> 0x037b, IOException -> 0x0374, DecodeException -> 0x0368, Throwable -> 0x035e, all -> 0x0359 }
            r1.<init>()     // Catch:{ ClientProtocolException -> 0x0389, SocketException -> 0x0382, SocketTimeoutException -> 0x037b, IOException -> 0x0374, DecodeException -> 0x0368, Throwable -> 0x035e, all -> 0x0359 }
            r3 = 30000(0x7530, float:4.2039E-41)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r1, r3)     // Catch:{ ClientProtocolException -> 0x0389, SocketException -> 0x0382, SocketTimeoutException -> 0x037b, IOException -> 0x0374, DecodeException -> 0x0368, Throwable -> 0x035e, all -> 0x0359 }
            r3 = 30000(0x7530, float:4.2039E-41)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r1, r3)     // Catch:{ ClientProtocolException -> 0x0389, SocketException -> 0x0382, SocketTimeoutException -> 0x037b, IOException -> 0x0374, DecodeException -> 0x0368, Throwable -> 0x035e, all -> 0x0359 }
            r3 = 4096(0x1000, float:5.74E-42)
            org.apache.http.params.HttpConnectionParams.setSocketBufferSize(r1, r3)     // Catch:{ ClientProtocolException -> 0x0389, SocketException -> 0x0382, SocketTimeoutException -> 0x037b, IOException -> 0x0374, DecodeException -> 0x0368, Throwable -> 0x035e, all -> 0x0359 }
            r3 = 1
            org.apache.http.client.params.HttpClientParams.setRedirecting(r1, r3)     // Catch:{ ClientProtocolException -> 0x0389, SocketException -> 0x0382, SocketTimeoutException -> 0x037b, IOException -> 0x0374, DecodeException -> 0x0368, Throwable -> 0x035e, all -> 0x0359 }
            org.apache.http.impl.client.DefaultHttpClient r3 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ ClientProtocolException -> 0x0389, SocketException -> 0x0382, SocketTimeoutException -> 0x037b, IOException -> 0x0374, DecodeException -> 0x0368, Throwable -> 0x035e, all -> 0x0359 }
            r3.<init>(r1)     // Catch:{ ClientProtocolException -> 0x0389, SocketException -> 0x0382, SocketTimeoutException -> 0x037b, IOException -> 0x0374, DecodeException -> 0x0368, Throwable -> 0x035e, all -> 0x0359 }
            boolean r1 = r11.h     // Catch:{ ClientProtocolException -> 0x0389, SocketException -> 0x0382, SocketTimeoutException -> 0x037b, IOException -> 0x0374, DecodeException -> 0x0368, Throwable -> 0x035e, all -> 0x0359 }
            if (r1 == 0) goto L_0x005a
            org.apache.http.HttpHost r1 = new org.apache.http.HttpHost     // Catch:{ ClientProtocolException -> 0x0389, SocketException -> 0x0382, SocketTimeoutException -> 0x037b, IOException -> 0x0374, DecodeException -> 0x0368, Throwable -> 0x035e, all -> 0x0359 }
            java.lang.String r4 = r11.i     // Catch:{ ClientProtocolException -> 0x0389, SocketException -> 0x0382, SocketTimeoutException -> 0x037b, IOException -> 0x0374, DecodeException -> 0x0368, Throwable -> 0x035e, all -> 0x0359 }
            int r5 = r11.j     // Catch:{ ClientProtocolException -> 0x0389, SocketException -> 0x0382, SocketTimeoutException -> 0x037b, IOException -> 0x0374, DecodeException -> 0x0368, Throwable -> 0x035e, all -> 0x0359 }
            r1.<init>(r4, r5)     // Catch:{ ClientProtocolException -> 0x0389, SocketException -> 0x0382, SocketTimeoutException -> 0x037b, IOException -> 0x0374, DecodeException -> 0x0368, Throwable -> 0x035e, all -> 0x0359 }
            org.apache.http.params.HttpParams r4 = r3.getParams()     // Catch:{ ClientProtocolException -> 0x0389, SocketException -> 0x0382, SocketTimeoutException -> 0x037b, IOException -> 0x0374, DecodeException -> 0x0368, Throwable -> 0x035e, all -> 0x0359 }
            java.lang.String r5 = "http.route.default-proxy"
            r4.setParameter(r5, r1)     // Catch:{ ClientProtocolException -> 0x0389, SocketException -> 0x0382, SocketTimeoutException -> 0x037b, IOException -> 0x0374, DecodeException -> 0x0368, Throwable -> 0x035e, all -> 0x0359 }
        L_0x005a:
            boolean r0 = r11.a     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            if (r0 == 0) goto L_0x00a8
            org.apache.http.conn.ClientConnectionManager r0 = r3.getConnectionManager()     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            r0.shutdown()     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            goto L_0x0008
        L_0x0066:
            r0 = move-exception
            r1 = r2
            r2 = r3
        L_0x0069:
            r0.printStackTrace()     // Catch:{ all -> 0x0356 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0356 }
            r3.<init>()     // Catch:{ all -> 0x0356 }
            java.lang.String r4 = "doGet, ClientProtocolException : "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0356 }
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x0356 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0356 }
            r3.toString()     // Catch:{ all -> 0x0356 }
            com.tencent.module.download.a.a r3 = r11.d     // Catch:{ all -> 0x0356 }
            if (r3 == 0) goto L_0x008d
            int r3 = r11.g     // Catch:{ all -> 0x0356 }
            if (r3 < r8) goto L_0x008d
            r0.toString()     // Catch:{ all -> 0x0356 }
        L_0x008d:
            if (r2 == 0) goto L_0x03a3
            org.apache.http.conn.ClientConnectionManager r0 = r2.getConnectionManager()
            r0.shutdown()
            r0 = r7
        L_0x0097:
            if (r1 == 0) goto L_0x0390
            r1 = r0
            r0 = r7
        L_0x009b:
            int r2 = r11.g
            int r2 = r2 + 1
            r11.g = r2
            r2 = r0
            r0 = r1
            goto L_0x0022
        L_0x00a5:
            r0 = r2
            goto L_0x001e
        L_0x00a8:
            boolean r0 = r11.k     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            if (r0 == 0) goto L_0x0136
            org.apache.http.client.methods.HttpPost r0 = new org.apache.http.client.methods.HttpPost     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            r0.<init>()     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            r11.o = r0     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            org.apache.http.client.methods.HttpPost r0 = r11.o     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            java.net.URI r1 = r11.s     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            r0.setURI(r1)     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            byte[] r0 = r11.m     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            if (r0 == 0) goto L_0x00cf
            byte[] r0 = r11.m     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            int r0 = r0.length     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            if (r0 <= 0) goto L_0x00cf
            org.apache.http.client.methods.HttpPost r0 = r11.o     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            org.apache.http.entity.ByteArrayEntity r1 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            byte[] r4 = r11.m     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            r1.<init>(r4)     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            r0.setEntity(r1)     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
        L_0x00cf:
            java.util.Map r0 = r11.q     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            int r0 = r0.size()     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            if (r0 <= 0) goto L_0x01ee
            java.util.Map r0 = r11.q     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            java.util.Set r0 = r0.keySet()     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            java.util.Iterator r4 = r0.iterator()     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
        L_0x00e1:
            boolean r0 = r4.hasNext()     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            if (r0 == 0) goto L_0x01ee
            java.lang.Object r0 = r4.next()     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            boolean r1 = r11.k     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            if (r1 == 0) goto L_0x0192
            org.apache.http.client.methods.HttpPost r5 = r11.o     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            java.util.Map r1 = r11.q     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            java.lang.Object r1 = r1.get(r0)     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            r5.addHeader(r0, r1)     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            goto L_0x00e1
        L_0x00ff:
            r0 = move-exception
            r1 = r2
            r2 = r3
        L_0x0102:
            r0.printStackTrace()     // Catch:{ all -> 0x0356 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0356 }
            r3.<init>()     // Catch:{ all -> 0x0356 }
            java.lang.String r4 = "doGet, SocketException : "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0356 }
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x0356 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0356 }
            r3.toString()     // Catch:{ all -> 0x0356 }
            com.tencent.module.download.a.a r3 = r11.d     // Catch:{ all -> 0x0356 }
            if (r3 == 0) goto L_0x0126
            int r3 = r11.g     // Catch:{ all -> 0x0356 }
            if (r3 < r8) goto L_0x0126
            r0.toString()     // Catch:{ all -> 0x0356 }
        L_0x0126:
            if (r2 == 0) goto L_0x03a0
            org.apache.http.conn.ClientConnectionManager r0 = r2.getConnectionManager()
            r0.shutdown()
            r0 = r7
        L_0x0130:
            if (r1 == 0) goto L_0x0390
            r1 = r0
            r0 = r7
            goto L_0x009b
        L_0x0136:
            org.apache.http.client.methods.HttpGet r0 = new org.apache.http.client.methods.HttpGet     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            r0.<init>()     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            r11.p = r0     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            org.apache.http.client.methods.HttpGet r0 = r11.p     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            java.net.URI r1 = r11.s     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            r0.setURI(r1)     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            goto L_0x00cf
        L_0x0145:
            r0 = move-exception
            r1 = r2
            r2 = r3
        L_0x0148:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0356 }
            r3.<init>()     // Catch:{ all -> 0x0356 }
            java.lang.String r4 = "doGet, SocketTimeoutException : "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0356 }
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x0356 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0356 }
            r3.toString()     // Catch:{ all -> 0x0356 }
            r0.printStackTrace()     // Catch:{ all -> 0x0356 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0356 }
            r3.<init>()     // Catch:{ all -> 0x0356 }
            java.lang.String r4 = "doGet, SocketTimeoutException : "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0356 }
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x0356 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0356 }
            r3.toString()     // Catch:{ all -> 0x0356 }
            com.tencent.module.download.a.a r3 = r11.d     // Catch:{ all -> 0x0356 }
            if (r3 == 0) goto L_0x0182
            int r3 = r11.g     // Catch:{ all -> 0x0356 }
            if (r3 < r8) goto L_0x0182
            r0.toString()     // Catch:{ all -> 0x0356 }
        L_0x0182:
            if (r2 == 0) goto L_0x039d
            org.apache.http.conn.ClientConnectionManager r0 = r2.getConnectionManager()
            r0.shutdown()
            r0 = r7
        L_0x018c:
            if (r1 == 0) goto L_0x0390
            r1 = r0
            r0 = r7
            goto L_0x009b
        L_0x0192:
            org.apache.http.client.methods.HttpGet r5 = r11.p     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            java.util.Map r1 = r11.q     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            java.lang.Object r1 = r1.get(r0)     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            r5.addHeader(r0, r1)     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            goto L_0x00e1
        L_0x01a1:
            r0 = move-exception
            r1 = r2
            r2 = r3
        L_0x01a4:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0356 }
            r3.<init>()     // Catch:{ all -> 0x0356 }
            java.lang.String r4 = "doGet, IOException : "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0356 }
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x0356 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0356 }
            r3.toString()     // Catch:{ all -> 0x0356 }
            r0.printStackTrace()     // Catch:{ all -> 0x0356 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0356 }
            r3.<init>()     // Catch:{ all -> 0x0356 }
            java.lang.String r4 = "doGet, IOException : "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0356 }
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x0356 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0356 }
            r3.toString()     // Catch:{ all -> 0x0356 }
            com.tencent.module.download.a.a r3 = r11.d     // Catch:{ all -> 0x0356 }
            if (r3 == 0) goto L_0x01de
            int r3 = r11.g     // Catch:{ all -> 0x0356 }
            if (r3 < r8) goto L_0x01de
            r0.toString()     // Catch:{ all -> 0x0356 }
        L_0x01de:
            if (r2 == 0) goto L_0x039a
            org.apache.http.conn.ClientConnectionManager r0 = r2.getConnectionManager()
            r0.shutdown()
            r0 = r7
        L_0x01e8:
            if (r1 == 0) goto L_0x0390
            r1 = r0
            r0 = r7
            goto L_0x009b
        L_0x01ee:
            java.util.Date r0 = new java.util.Date     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            r0.<init>(r4)     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            r0.toLocaleString()     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            org.apache.http.protocol.BasicHttpContext r0 = new org.apache.http.protocol.BasicHttpContext     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            r0.<init>()     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            boolean r1 = r11.k     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            if (r1 == 0) goto L_0x021d
            org.apache.http.client.methods.HttpPost r1 = r11.o     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            org.apache.http.HttpResponse r0 = r3.execute(r1, r0)     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
        L_0x0209:
            boolean r1 = r11.a     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            if (r1 == 0) goto L_0x0224
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            r1.shutdown()     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            goto L_0x0008
        L_0x0216:
            r1 = move-exception
            r2 = r3
            r10 = r0
            r0 = r1
            r1 = r10
            goto L_0x0069
        L_0x021d:
            org.apache.http.client.methods.HttpGet r1 = r11.p     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            org.apache.http.HttpResponse r0 = r3.execute(r1, r0)     // Catch:{ ClientProtocolException -> 0x0066, SocketException -> 0x00ff, SocketTimeoutException -> 0x0145, IOException -> 0x01a1, DecodeException -> 0x036f, Throwable -> 0x0364, all -> 0x034a }
            goto L_0x0209
        L_0x0224:
            org.apache.http.StatusLine r1 = r0.getStatusLine()     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            int r1 = r1.getStatusCode()     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            java.lang.String r2 = "Content-Type"
            org.apache.http.Header[] r2 = r0.getHeaders(r2)     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            r4 = r9
        L_0x0233:
            int r5 = r2.length     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            if (r4 >= r5) goto L_0x03a6
            r5 = r2[r4]     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            java.lang.String r5 = r5.getName()     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            java.lang.String r6 = "Content-Type"
            boolean r5 = r5.equalsIgnoreCase(r6)     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            if (r5 == 0) goto L_0x0266
            r2 = r2[r4]     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
        L_0x0246:
            if (r2 == 0) goto L_0x0269
            r2.getValue()     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            java.lang.String r2 = r2.getValue()     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            java.lang.String r4 = "text"
            boolean r2 = r2.startsWith(r4)     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            if (r2 == 0) goto L_0x0269
            java.io.IOException r1 = new java.io.IOException     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            java.lang.String r2 = "Content-Type Error"
            r1.<init>(r2)     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            throw r1     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
        L_0x025f:
            r1 = move-exception
            r2 = r3
            r10 = r0
            r0 = r1
            r1 = r10
            goto L_0x0102
        L_0x0266:
            int r4 = r4 + 1
            goto L_0x0233
        L_0x0269:
            java.util.Date r2 = new java.util.Date     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            r2.<init>(r4)     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            r2.toLocaleString()     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 == r2) goto L_0x02a3
            r2 = 206(0xce, float:2.89E-43)
            if (r1 == r2) goto L_0x02a3
            boolean r1 = r11.k     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            if (r1 == 0) goto L_0x0296
            org.apache.http.client.methods.HttpPost r1 = r11.o     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            r1.abort()     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
        L_0x0286:
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            r1.shutdown()     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            goto L_0x0008
        L_0x028f:
            r1 = move-exception
            r2 = r3
            r10 = r0
            r0 = r1
            r1 = r10
            goto L_0x0148
        L_0x0296:
            org.apache.http.client.methods.HttpGet r1 = r11.p     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            r1.abort()     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            goto L_0x0286
        L_0x029c:
            r1 = move-exception
            r2 = r3
            r10 = r0
            r0 = r1
            r1 = r10
            goto L_0x01a4
        L_0x02a3:
            boolean r1 = r11.a     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            if (r1 == 0) goto L_0x02ff
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            r1.shutdown()     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            goto L_0x0008
        L_0x02b0:
            r1 = move-exception
            r2 = r3
            r10 = r0
            r0 = r1
            r1 = r10
        L_0x02b5:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0356 }
            r3.<init>()     // Catch:{ all -> 0x0356 }
            java.lang.String r4 = "doGet, DecodeException : "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0356 }
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x0356 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0356 }
            r3.toString()     // Catch:{ all -> 0x0356 }
            r0.printStackTrace()     // Catch:{ all -> 0x0356 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0356 }
            r3.<init>()     // Catch:{ all -> 0x0356 }
            java.lang.String r4 = "doGet, DecodeException : "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0356 }
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x0356 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0356 }
            r3.toString()     // Catch:{ all -> 0x0356 }
            com.tencent.module.download.a.a r3 = r11.d     // Catch:{ all -> 0x0356 }
            if (r3 == 0) goto L_0x02ef
            int r3 = r11.g     // Catch:{ all -> 0x0356 }
            if (r3 < r8) goto L_0x02ef
            r0.toString()     // Catch:{ all -> 0x0356 }
        L_0x02ef:
            if (r2 == 0) goto L_0x0397
            org.apache.http.conn.ClientConnectionManager r0 = r2.getConnectionManager()
            r0.shutdown()
            r0 = r7
        L_0x02f9:
            if (r1 == 0) goto L_0x0390
            r1 = r0
            r0 = r7
            goto L_0x009b
        L_0x02ff:
            com.tencent.module.download.a.a r1 = r11.d     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            if (r1 == 0) goto L_0x0308
            com.tencent.module.download.a.a r1 = r11.d     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            r1.a(r0, r11)     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
        L_0x0308:
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            r1.shutdown()     // Catch:{ ClientProtocolException -> 0x0216, SocketException -> 0x025f, SocketTimeoutException -> 0x028f, IOException -> 0x029c, DecodeException -> 0x02b0, Throwable -> 0x0311, all -> 0x034a }
            goto L_0x0008
        L_0x0311:
            r1 = move-exception
            r2 = r3
            r10 = r0
            r0 = r1
            r1 = r10
        L_0x0316:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0356 }
            r3.<init>()     // Catch:{ all -> 0x0356 }
            java.lang.String r4 = "doGet, Throwable : "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0356 }
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x0356 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0356 }
            r3.toString()     // Catch:{ all -> 0x0356 }
            r0.printStackTrace()     // Catch:{ all -> 0x0356 }
            com.tencent.module.download.a.a r3 = r11.d     // Catch:{ all -> 0x0356 }
            if (r3 == 0) goto L_0x033a
            int r3 = r11.g     // Catch:{ all -> 0x0356 }
            if (r3 < r8) goto L_0x033a
            r0.toString()     // Catch:{ all -> 0x0356 }
        L_0x033a:
            if (r2 == 0) goto L_0x0395
            org.apache.http.conn.ClientConnectionManager r0 = r2.getConnectionManager()
            r0.shutdown()
            r0 = r7
        L_0x0344:
            if (r1 == 0) goto L_0x0390
            r1 = r0
            r0 = r7
            goto L_0x009b
        L_0x034a:
            r0 = move-exception
            r1 = r3
        L_0x034c:
            if (r1 == 0) goto L_0x0355
            org.apache.http.conn.ClientConnectionManager r1 = r1.getConnectionManager()
            r1.shutdown()
        L_0x0355:
            throw r0
        L_0x0356:
            r0 = move-exception
            r1 = r2
            goto L_0x034c
        L_0x0359:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x034c
        L_0x035e:
            r1 = move-exception
            r10 = r1
            r1 = r2
            r2 = r0
            r0 = r10
            goto L_0x0316
        L_0x0364:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0316
        L_0x0368:
            r1 = move-exception
            r10 = r1
            r1 = r2
            r2 = r0
            r0 = r10
            goto L_0x02b5
        L_0x036f:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x02b5
        L_0x0374:
            r1 = move-exception
            r10 = r1
            r1 = r2
            r2 = r0
            r0 = r10
            goto L_0x01a4
        L_0x037b:
            r1 = move-exception
            r10 = r1
            r1 = r2
            r2 = r0
            r0 = r10
            goto L_0x0148
        L_0x0382:
            r1 = move-exception
            r10 = r1
            r1 = r2
            r2 = r0
            r0 = r10
            goto L_0x0102
        L_0x0389:
            r1 = move-exception
            r10 = r1
            r1 = r2
            r2 = r0
            r0 = r10
            goto L_0x0069
        L_0x0390:
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x009b
        L_0x0395:
            r0 = r2
            goto L_0x0344
        L_0x0397:
            r0 = r2
            goto L_0x02f9
        L_0x039a:
            r0 = r2
            goto L_0x01e8
        L_0x039d:
            r0 = r2
            goto L_0x018c
        L_0x03a0:
            r0 = r2
            goto L_0x0130
        L_0x03a3:
            r0 = r2
            goto L_0x0097
        L_0x03a6:
            r2 = r7
            goto L_0x0246
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.module.download.a.b.c():void");
    }
}
