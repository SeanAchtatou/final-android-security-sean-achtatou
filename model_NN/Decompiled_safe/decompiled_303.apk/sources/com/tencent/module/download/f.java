package com.tencent.module.download;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import com.tencent.android.ui.LocalSoftManageActivity;
import com.tencent.launcher.base.BaseApp;
import com.tencent.qqlauncher.R;
import java.util.Map;

public final class f {
    private static f a = null;
    private static int b = 30;
    private static int c = 40;
    private static int d = 50;
    private static int e = 60;
    private int f = 0;
    private NotificationManager g = ((NotificationManager) BaseApp.b().getSystemService("notification"));
    private Notification h;
    private RemoteViews i;
    private int j;

    private f() {
    }

    public static f a() {
        if (a == null) {
            a = new f();
        }
        return a;
    }

    public static void b() {
        NotificationManager notificationManager = (NotificationManager) BaseApp.b().getSystemService("notification");
        notificationManager.cancel(e);
        notificationManager.cancelAll();
        a = null;
    }

    public final void a(int i2) {
        PendingIntent pendingIntent;
        if (i2 >= 0) {
            if (i2 == 0) {
                this.g.cancel(d);
            }
            long currentTimeMillis = System.currentTimeMillis();
            Context b2 = BaseApp.b();
            String str = i2 + b2.getString(R.string.status_bar_downloaded_title);
            String string = b2.getString(R.string.status_bar_downloaded_content);
            if (b2.getResources().getConfiguration().locale.getLanguage().toLowerCase().contains("zh")) {
                Intent intent = new Intent(b2, LocalSoftManageActivity.class);
                intent.setFlags(603979776);
                intent.putExtra("android.intent.extra.TITLE", 2);
                pendingIntent = PendingIntent.getActivity(b2, 0, intent, 0);
            } else {
                pendingIntent = null;
            }
            Notification notification = new Notification(R.drawable.status_download, null, currentTimeMillis);
            notification.setLatestEventInfo(b2, str, string, pendingIntent);
            this.g.notify(d, notification);
        }
    }

    public final void a(Map map) {
        if (this.h == null) {
            long currentTimeMillis = System.currentTimeMillis();
            Context b2 = BaseApp.b();
            PendingIntent pendingIntent = null;
            if (b2.getResources().getConfiguration().locale.getLanguage().toLowerCase().contains("zh")) {
                Intent intent = new Intent(b2, LocalSoftManageActivity.class);
                intent.setFlags(603979776);
                intent.putExtra("android.intent.extra.TITLE", 2);
                pendingIntent = PendingIntent.getActivity(b2, 0, intent, 0);
            }
            int i2 = 0;
            String str = null;
            int i3 = 0;
            int i4 = 0;
            for (DownloadInfo downloadInfo : map.values()) {
                if (downloadInfo.e == 0 || downloadInfo.e == 1) {
                    i2 = (int) (((long) i2) + downloadInfo.h);
                    i4 = (int) (((long) i4) + downloadInfo.g);
                    if (str == null) {
                        str = downloadInfo.d;
                    }
                    i3++;
                }
            }
            if (i3 > 0) {
                if (i3 <= 1) {
                    str = String.format("%s正在下载", str);
                } else if (i3 > 1) {
                    str = String.format("%s等%d个应用正在下载", str, Integer.valueOf(i3));
                }
                int i5 = i4 / i2;
                if (i5 < 0) {
                    i5 = 0;
                } else if (i5 > 100) {
                    i5 = 100;
                }
                this.h = new Notification(R.drawable.appcenter_logo, null, currentTimeMillis);
                Notification notification = this.h;
                notification.contentView = new RemoteViews(b2.getPackageName(), (int) R.layout.downloading_notification);
                this.i = notification.contentView;
                notification.contentView.setTextViewText(R.id.text_notification_downloading, str);
                notification.contentView.setProgressBar(R.id.progress_notification_downloading, 100, i5, false);
                notification.contentView.setTextViewText(R.id.progress_text, i5 + "%");
                notification.contentIntent = pendingIntent;
                this.g.notify(e, notification);
                return;
            }
            return;
        }
        Context b3 = BaseApp.b();
        long j2 = 0;
        long j3 = 0;
        String str2 = null;
        int i6 = 0;
        for (DownloadInfo downloadInfo2 : map.values()) {
            if (downloadInfo2.e == 1) {
                j3 += downloadInfo2.h;
                j2 += downloadInfo2.g;
                if (str2 == null) {
                    str2 = downloadInfo2.d;
                }
                i6++;
            }
        }
        if (i6 <= 0) {
            this.g.cancel(e);
            return;
        }
        int i7 = (int) ((j2 * 100) / j3);
        if (i7 < 0) {
            i7 = 0;
        } else if (i7 > 100) {
            i7 = 100;
        }
        if (i7 != this.j) {
            if (i7 == 100) {
                this.j = 0;
            } else {
                this.j = i7;
            }
            if (i6 <= 1) {
                str2 = String.format("%s正在下载", str2);
            } else if (i6 > 1) {
                str2 = String.format("%s等%d个应用正在下载", str2, Integer.valueOf(i6));
            }
            Notification notification2 = this.h;
            notification2.contentView = new RemoteViews(b3.getPackageName(), (int) R.layout.downloading_notification);
            notification2.contentView.setTextViewText(R.id.text_notification_downloading, str2);
            notification2.contentView.setProgressBar(R.id.progress_notification_downloading, 100, i7, false);
            notification2.contentView.setTextViewText(R.id.progress_text, i7 + "%");
            this.g.notify(e, notification2);
        }
    }

    public final void c() {
        this.g.cancel(d);
    }
}
