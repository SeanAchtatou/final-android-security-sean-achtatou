package com.tencent.module.download;

import java.util.List;

final class h extends s {
    private /* synthetic */ DownloadService a;

    h(DownloadService downloadService) {
        this.a = downloadService;
    }

    public final void a() {
        this.a.a.b();
    }

    public final void a(DownloadInfo downloadInfo, String str, i iVar) {
        if (downloadInfo != null && this.a.a.a(downloadInfo, iVar)) {
            String str2 = downloadInfo.a;
            if (str2 != null) {
                this.a.a.a(str2, str, iVar);
                this.a.a.a(downloadInfo);
            } else if (iVar != null) {
                iVar.a(str2, 4, "url错误");
            }
        }
    }

    public final void a(i iVar) {
        this.a.a.a(iVar);
    }

    public final void a(String str) {
        this.a.a.c(str);
    }

    public final void a(String str, i iVar) {
        this.a.a.b(str);
        this.a.a.d(str);
    }

    public final void a(String str, j jVar) {
        this.a.a.a(str, jVar);
    }

    public final void a(String str, String str2) {
        this.a.a.a(str, str2);
    }

    public final void a(String str, String str2, i iVar) {
        this.a.a.a(str, str2, iVar);
    }

    public final List b() {
        return this.a.a.d();
    }

    public final void b(DownloadInfo downloadInfo, String str, i iVar) {
        String str2 = downloadInfo.a;
        if (str2 != null) {
            this.a.a.a(str2, str, iVar);
            this.a.a.a(str2);
        } else if (iVar != null) {
            iVar.a(str2, 4, "url错误");
        }
    }

    public final void c() {
        this.a.a.c();
    }
}
