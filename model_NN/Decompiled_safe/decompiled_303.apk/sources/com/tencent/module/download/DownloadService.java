package com.tencent.module.download;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

public class DownloadService extends Service {
    /* access modifiers changed from: private */
    public a a;
    private BroadcastReceiver b;
    private s c = new h(this);

    public IBinder onBind(Intent intent) {
        return this.c;
    }

    public void onCreate() {
        super.onCreate();
        setForeground(false);
        this.a = a.a(getBaseContext());
        this.b = new g(this);
        registerReceiver(this.b, new IntentFilter("com.tencent.qqlauncher.QUIT"));
    }

    public void onDestroy() {
        if (this.a != null) {
            this.a.a();
            this.a = null;
        }
        unregisterReceiver(this.b);
        super.onDestroy();
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        return 2;
    }
}
