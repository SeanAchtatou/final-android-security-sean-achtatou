package com.tencent.module.download;

import android.os.Parcel;
import android.os.Parcelable;

final class aa implements Parcelable.Creator {
    aa() {
    }

    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new DownloadInfo(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new DownloadInfo[i];
    }
}
