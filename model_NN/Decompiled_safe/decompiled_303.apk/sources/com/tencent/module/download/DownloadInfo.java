package com.tencent.module.download;

import AndroidDLoader.a;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class DownloadInfo implements Parcelable {
    public static final Parcelable.Creator CREATOR = new aa();
    public String a = "";
    public String b = "";
    public String c = "";
    public String d = "";
    public int e = 0;
    public long f = 0;
    public long g = 0;
    public long h = 0;
    public Bitmap i;
    public String j;
    public int k = 0;
    public int l = 0;
    public int m = 0;
    public int n = a.a.a();
    public String o = "";

    public DownloadInfo() {
    }

    public DownloadInfo(Parcel parcel) {
        this.a = parcel.readString();
        this.b = parcel.readString();
        this.c = parcel.readString();
        this.d = parcel.readString();
        this.e = parcel.readInt();
        this.f = parcel.readLong();
        this.h = parcel.readLong();
        this.i = (Bitmap) parcel.readParcelable(null);
        this.j = parcel.readString();
        this.k = parcel.readInt();
        this.l = parcel.readInt();
        this.m = parcel.readInt();
        this.n = parcel.readInt();
        this.o = parcel.readString();
    }

    public final int a() {
        return this.e;
    }

    public final void a(int i2) {
        this.k = i2;
    }

    public final void b(int i2) {
        this.l = i2;
    }

    public final void c(int i2) {
        this.m = i2;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (this.a == null || obj == null || !(obj instanceof DownloadInfo)) {
            return false;
        }
        DownloadInfo downloadInfo = (DownloadInfo) obj;
        if (downloadInfo.a == null) {
            return false;
        }
        if (this.a.equals(downloadInfo.a)) {
            return true;
        }
        return super.equals(obj);
    }

    public int hashCode() {
        return this.a.hashCode() * 31;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeInt(this.e);
        parcel.writeLong(this.f);
        parcel.writeLong(this.h);
        parcel.writeParcelable(this.i, 0);
        parcel.writeString(this.j);
        parcel.writeInt(this.k);
        parcel.writeInt(this.l);
        parcel.writeInt(this.m);
        parcel.writeInt(this.n);
        parcel.writeString(this.o);
    }
}
