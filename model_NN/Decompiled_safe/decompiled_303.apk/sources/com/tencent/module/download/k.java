package com.tencent.module.download;

import android.os.Message;

final class k extends b {
    private /* synthetic */ DownloadActivity a;

    k(DownloadActivity downloadActivity) {
        this.a = downloadActivity;
    }

    public final void a(String str) {
        Message.obtain(this.a.mHandler, 23, 0, 0, str).sendToTarget();
    }

    public final void a(String str, int i, int i2) {
        Message.obtain(this.a.mHandler, 21, i2, i, str).sendToTarget();
    }

    public final void a(String str, int i, String str2) {
        Message.obtain(this.a.mHandler, 20, i, 0, str2).sendToTarget();
    }

    public final void a(String str, String str2) {
        Message.obtain(this.a.mHandler, 22, 0, 0, new String[]{str, str2}).sendToTarget();
    }
}
