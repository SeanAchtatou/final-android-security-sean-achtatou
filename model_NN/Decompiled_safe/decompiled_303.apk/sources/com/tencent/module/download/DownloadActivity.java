package com.tencent.module.download;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.view.View;
import android.widget.TextView;
import com.tencent.qqlauncher.R;

public class DownloadActivity extends Activity implements View.OnClickListener {
    private static final String url1 = "http://softfile.3g.qq.com:8080/msoft/674/14714/17494/27654/GO_Launcher_EX_v2.37_331.apk?g_f=&a=";
    private static final String url2 = "http://softfile.3g.qq.com:8080/msoft/179/2410/18613/QQMobileToken3.1_Android_Build0011.apk?g_f=&a=";
    private static final String url3 = "http://softfile.3g.qq.com:8080/msoft/673/17183/20796/26551/20110708_yuanshirendamaoxian_android_1.0.0.apk?g_f=&a=";
    private b mCallback;
    private b mCallback2;
    private b mCallback3;
    /* access modifiers changed from: private */
    public TextView mDownlaodList;
    /* access modifiers changed from: private */
    public Handler mHandler = new p(this);
    /* access modifiers changed from: private */
    public r mListListener;
    /* access modifiers changed from: private */
    public y mService;
    /* access modifiers changed from: private */
    public TextView mStatus1;
    /* access modifiers changed from: private */
    public TextView mStatus2;
    /* access modifiers changed from: private */
    public TextView mStatus3;

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void onClick(View view) {
        try {
            switch (view.getId()) {
                case R.id.download1 /*2131492945*/:
                    DownloadInfo downloadInfo = new DownloadInfo();
                    downloadInfo.a = url1;
                    this.mService.a(downloadInfo, "DownloadActivity", this.mCallback);
                    return;
                case R.id.cancel1 /*2131492946*/:
                    this.mService.a(url1, this.mCallback);
                    return;
                case R.id.status1 /*2131492947*/:
                case R.id.status2 /*2131492950*/:
                default:
                    return;
                case R.id.download2 /*2131492948*/:
                    DownloadInfo downloadInfo2 = new DownloadInfo();
                    downloadInfo2.a = url2;
                    this.mService.a(downloadInfo2, "DownloadActivity", this.mCallback2);
                    return;
                case R.id.cancel2 /*2131492949*/:
                    this.mService.a(url2, this.mCallback2);
                    return;
                case R.id.download3 /*2131492951*/:
                    DownloadInfo downloadInfo3 = new DownloadInfo();
                    downloadInfo3.a = url3;
                    this.mService.a(downloadInfo3, "DownloadActivity", this.mCallback3);
                    return;
                case R.id.cancel3 /*2131492952*/:
                    this.mService.a(url3, this.mCallback3);
                    return;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        e.printStackTrace();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.download);
        this.mListListener = new o(this);
        bindService(new Intent(this, DownloadService.class), new n(this), 1);
        findViewById(R.id.download1).setOnClickListener(this);
        findViewById(R.id.cancel1).setOnClickListener(this);
        findViewById(R.id.download2).setOnClickListener(this);
        findViewById(R.id.cancel2).setOnClickListener(this);
        findViewById(R.id.download3).setOnClickListener(this);
        findViewById(R.id.cancel3).setOnClickListener(this);
        this.mStatus1 = (TextView) findViewById(R.id.status1);
        this.mStatus2 = (TextView) findViewById(R.id.status2);
        this.mStatus3 = (TextView) findViewById(R.id.status3);
        this.mDownlaodList = (TextView) findViewById(R.id.download_list);
        this.mCallback = new m(this);
        this.mCallback2 = new l(this);
        this.mCallback3 = new k(this);
    }
}
