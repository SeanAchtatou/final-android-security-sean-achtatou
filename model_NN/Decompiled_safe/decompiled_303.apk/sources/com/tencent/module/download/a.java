package com.tencent.module.download;

import android.content.Context;
import android.os.Environment;
import android.os.RemoteException;
import com.tencent.android.a.c;
import com.tencent.launcher.base.BaseApp;
import com.tencent.qqlauncher.R;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

public final class a implements t {
    private static a a;
    private static String e = "";
    private static String r = "http://dl.3g.qq.com/webapp_dloader/AndroidDLoader";
    private static int s = 1;
    private q b;
    private u c;
    private Context d;
    /* access modifiers changed from: private */
    public Vector f = new Vector();
    private Map g = new ConcurrentHashMap();
    private Map h = new ConcurrentHashMap();
    private Map i = new ConcurrentHashMap();
    private Map j = new ConcurrentHashMap();
    private Map k = new ConcurrentHashMap();
    private int l = 0;
    private int m = 0;
    /* access modifiers changed from: private */
    public Object n = new Object();
    /* access modifiers changed from: private */
    public boolean o = true;
    private Thread p;
    private Runnable q = new x(this);

    private a(Context context) {
        String absolutePath;
        this.d = context;
        this.b = q.a();
        this.c = u.a(this);
        Context b2 = BaseApp.b();
        if (!"mounted".equals(Environment.getExternalStorageState()) || !Environment.getExternalStorageDirectory().canWrite()) {
            absolutePath = b2.getFilesDir().getAbsolutePath();
            if (!absolutePath.endsWith("/")) {
                absolutePath = absolutePath + "/";
            }
        } else {
            File file = new File(Environment.getExternalStorageDirectory().getPath() + "/Tencent/QQDownload/Apk/");
            if (!file.exists()) {
                file.mkdirs();
            }
            absolutePath = file.getAbsolutePath();
            if (!absolutePath.endsWith("/")) {
                absolutePath = absolutePath + "/";
            }
        }
        File file2 = new File(absolutePath);
        if (!file2.exists()) {
            file2.mkdirs();
        }
        e = absolutePath;
    }

    public static a a(Context context) {
        if (a == null) {
            a = new a(context);
        }
        return a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00c7 A[SYNTHETIC, Splitter:B:55:0x00c7] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00d4 A[SYNTHETIC, Splitter:B:62:0x00d4] */
    /* JADX WARNING: Removed duplicated region for block: B:93:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void a(com.tencent.module.download.a r10, java.lang.String r11, byte[] r12, boolean r13, boolean r14) {
        /*
            r9 = 4
            r8 = 0
            r5 = 0
            r2 = 1
            if (r11 == 0) goto L_0x000c
            int r0 = r11.length()
            if (r0 != 0) goto L_0x000d
        L_0x000c:
            return
        L_0x000d:
            if (r14 != 0) goto L_0x0014
            if (r12 == 0) goto L_0x000c
            int r0 = r12.length
            if (r0 == 0) goto L_0x000c
        L_0x0014:
            java.util.Map r0 = r10.h
            boolean r0 = r0.containsKey(r11)
            if (r0 != 0) goto L_0x00b1
            java.lang.String r1 = f(r11)
            java.util.Map r0 = r10.h
            r0.put(r11, r1)
            if (r14 != 0) goto L_0x003b
            if (r13 != 0) goto L_0x003b
            java.util.Map r0 = r10.i
            java.lang.Object r0 = r0.get(r11)
            com.tencent.module.download.DownloadInfo r0 = (com.tencent.module.download.DownloadInfo) r0
            if (r0 == 0) goto L_0x003b
            int r3 = r0.e
            if (r3 == r2) goto L_0x003b
            r0.e = r2
            r0.b = r1
        L_0x003b:
            r10.a(r11, r1, r2)
            if (r12 == 0) goto L_0x0054
            int r0 = r12.length
            if (r0 <= 0) goto L_0x0054
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x00c0, all -> 0x00d0 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x00c0, all -> 0x00d0 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00c0, all -> 0x00d0 }
            r4 = 1
            r3.<init>(r0, r4)     // Catch:{ Exception -> 0x00c0, all -> 0x00d0 }
            r3.write(r12)     // Catch:{ Exception -> 0x01de }
            r3.close()     // Catch:{ Exception -> 0x00bb }
        L_0x0054:
            if (r14 == 0) goto L_0x000c
            java.io.File r0 = new java.io.File
            r0.<init>(r1)
            java.lang.String r3 = g(r11)
            java.io.File r4 = new java.io.File
            r4.<init>(r3)
            boolean r6 = r4.exists()
            if (r6 == 0) goto L_0x006d
            r4.delete()
        L_0x006d:
            boolean r6 = r0.renameTo(r4)
            if (r6 != 0) goto L_0x00ec
            r6 = 1000(0x3e8, double:4.94E-321)
            java.lang.Thread.sleep(r6)
            boolean r6 = r4.exists()
            if (r6 == 0) goto L_0x0081
            r4.delete()
        L_0x0081:
            boolean r0 = r0.renameTo(r4)
            if (r0 != 0) goto L_0x0113
            java.util.Map r0 = r10.j
            java.lang.Object r0 = r0.get(r11)
            java.util.Map r0 = (java.util.Map) r0
            if (r0 == 0) goto L_0x000c
            int r2 = r0.size()
            if (r2 <= 0) goto L_0x000c
            java.util.Collection r0 = r0.values()
            java.util.Iterator r2 = r0.iterator()
        L_0x009f:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x00dd
            java.lang.Object r0 = r2.next()
            com.tencent.module.download.i r0 = (com.tencent.module.download.i) r0
            java.lang.String r3 = "重命名失败"
            r0.a(r11, r9, r3)
            goto L_0x009f
        L_0x00b1:
            java.util.Map r0 = r10.h
            java.lang.Object r0 = r0.get(r11)
            java.lang.String r0 = (java.lang.String) r0
            r1 = r0
            goto L_0x003b
        L_0x00bb:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0054
        L_0x00c0:
            r0 = move-exception
            r3 = r5
        L_0x00c2:
            r0.printStackTrace()     // Catch:{ all -> 0x01da }
            if (r3 == 0) goto L_0x0054
            r3.close()     // Catch:{ Exception -> 0x00cb }
            goto L_0x0054
        L_0x00cb:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0054
        L_0x00d0:
            r0 = move-exception
            r1 = r5
        L_0x00d2:
            if (r1 == 0) goto L_0x00d7
            r1.close()     // Catch:{ Exception -> 0x00d8 }
        L_0x00d7:
            throw r0
        L_0x00d8:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00d7
        L_0x00dd:
            r10.a(r11, r1, r9)
            java.util.Map r0 = r10.h
            r0.remove(r11)
            java.util.Map r0 = r10.g
            r0.remove(r11)
            goto L_0x000c
        L_0x00ec:
            com.tencent.android.a.c r0 = new com.tencent.android.a.c     // Catch:{ Exception -> 0x0110 }
            android.content.Context r1 = r10.d     // Catch:{ Exception -> 0x0110 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0110 }
            com.tencent.android.b.a.c r0 = r0.a(r3)     // Catch:{ Exception -> 0x0110 }
            if (r0 != 0) goto L_0x01e1
            r0 = r2
        L_0x00fa:
            if (r0 == 0) goto L_0x0113
            r4.delete()
            java.lang.String r0 = "invalid apk"
            r10.b(r11, r0)
            java.util.Map r0 = r10.h
            r0.remove(r11)
            java.util.Map r0 = r10.g
            r0.remove(r11)
            goto L_0x000c
        L_0x0110:
            r0 = move-exception
            r0 = r2
            goto L_0x00fa
        L_0x0113:
            java.util.Map r0 = r10.h
            r0.remove(r11)
            java.util.Map r0 = r10.g
            r0.remove(r11)
            java.util.Map r0 = r10.i
            java.lang.Object r0 = r0.get(r11)
            com.tencent.module.download.DownloadInfo r0 = (com.tencent.module.download.DownloadInfo) r0
            if (r0 == 0) goto L_0x013e
            java.lang.String r1 = r0.j
            if (r1 == 0) goto L_0x013e
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r4 = r0.j
            r1.<init>(r4)
            java.lang.String r4 = "url"
            r1.putExtra(r4, r11)
            android.content.Context r4 = com.tencent.launcher.base.BaseApp.b()
            r4.sendOrderedBroadcast(r1, r5)
        L_0x013e:
            int r1 = r10.m
            int r1 = r1 + 1
            r10.m = r1
            r1 = 3
            r10.a(r11, r3, r1)
            r10.e()
            r10.c(r11, r3)
            if (r0 == 0) goto L_0x000c
            AndroidDLoader.DownSoft r1 = new AndroidDLoader.DownSoft
            r1.<init>()
            int r3 = r0.k
            r1.a = r3
            int r3 = r0.m
            r1.c = r3
            int r3 = r0.l
            r1.b = r3
            r1.d = r2
            r1.f = r8
            long r3 = r0.h
            int r3 = (int) r3
            r1.e = r3
            int r3 = r0.n
            java.lang.String r0 = r0.o
            com.qq.jce.wup.UniPacket r4 = new com.qq.jce.wup.UniPacket
            r4.<init>()
            int r6 = com.tencent.module.download.a.s
            int r7 = r6 + 1
            com.tencent.module.download.a.s = r7
            r4.setRequestId(r6)
            java.lang.String r6 = "dloader"
            r4.setServantName(r6)
            java.lang.String r6 = "reportSoft"
            r4.setFuncName(r6)
            AndroidDLoader.ReqDownSoftPack r6 = new AndroidDLoader.ReqDownSoftPack
            r6.<init>()
            java.lang.String r7 = com.tencent.android.a.b.a()
            r6.a(r7)
            int r7 = com.tencent.android.a.b.a
            r6.a(r7)
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            r7.add(r1)
            r6.a(r7)
            r6.a()
            r6.b(r0)
            r6.b(r3)
            java.lang.String r0 = "req"
            r4.put(r0, r6)
            byte[] r4 = r4.encode()
            com.tencent.android.net.a.e r0 = new com.tencent.android.net.a.e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = com.tencent.module.download.a.r
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = "?aid=reportSoft"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            r3 = 108(0x6c, float:1.51E-43)
            r0.<init>(r1, r2, r3, r4, r5)
            com.tencent.module.download.w r1 = new com.tencent.module.download.w
            r1.<init>(r10, r0)
            r1.start()
            goto L_0x000c
        L_0x01da:
            r0 = move-exception
            r1 = r3
            goto L_0x00d2
        L_0x01de:
            r0 = move-exception
            goto L_0x00c2
        L_0x01e1:
            r0 = r8
            goto L_0x00fa
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.module.download.a.a(com.tencent.module.download.a, java.lang.String, byte[], boolean, boolean):void");
    }

    private void a(String str, String str2, int i2) {
        DownloadInfo downloadInfo = (DownloadInfo) this.i.get(str);
        if (downloadInfo != null && downloadInfo.e != i2) {
            downloadInfo.e = i2;
            downloadInfo.b = str2;
            if (i2 == 3 && downloadInfo.f == 0) {
                downloadInfo.f = System.currentTimeMillis();
            }
            if (this.d.getResources().getConfiguration().locale.getLanguage().toLowerCase().contains("zh")) {
                f();
            }
        }
    }

    private void a(String str, byte[] bArr, boolean z) {
        if (z || !(bArr == null || bArr.length == 0)) {
            this.f.add(new c(this, str, bArr, z));
            synchronized (this.n) {
                this.n.notify();
            }
        }
    }

    private void c(String str, String str2) {
        BaseApp.a("\"" + ((DownloadInfo) this.i.get(str)).d + "\"下载完毕");
        Map map = (Map) this.j.get(str);
        if (map != null && map.size() > 0) {
            Collection<i> values = map.values();
            this.j.remove(str);
            try {
                for (i a2 : values) {
                    a2.a(str, str2);
                }
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
    }

    private void e() {
        if (this.d.getResources().getConfiguration().locale.getLanguage().toLowerCase().contains("zh")) {
            f.a().a(this.i);
        }
    }

    private static String f(String str) {
        File file = new File(e);
        if (!file.exists()) {
            file.mkdirs();
        }
        String str2 = g(str) + ".tmp";
        try {
            new File(str2).createNewFile();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return str2;
    }

    private void f() {
        if (this.d.getResources().getConfiguration().locale.getLanguage().toLowerCase().contains("zh")) {
            f a2 = f.a();
            if (this.m == 0) {
                a2.c();
            } else {
                a2.a(this.m);
            }
        }
    }

    private static String g(String str) {
        String str2 = "";
        if (str != null) {
            str2 = str.indexOf("?") > 0 ? str.substring(str.lastIndexOf("/") + 1, str.indexOf("?")) : str.substring(str.lastIndexOf("/") + 1);
        }
        int indexOf = str2.indexOf(".apk");
        if (indexOf > 0) {
            str2 = str2.substring(0, indexOf);
        }
        return e + str2 + ".apk";
    }

    private void g() {
        Collection<j> values = this.k.values();
        if (values != null && values.size() > 0) {
            List d2 = d();
            for (j a2 : values) {
                a2.a(d2);
            }
        }
    }

    private boolean h(String str) {
        boolean z;
        if (str == null || str.length() == 0) {
            return false;
        }
        if (this.p == null) {
            this.p = new Thread(this.q);
            this.p.start();
        }
        if (!this.h.containsKey(str)) {
            String g2 = g(str);
            File file = new File(g2);
            if (file.exists()) {
                try {
                    z = new c(this.d).a(g2) != null;
                } catch (Exception e2) {
                    e2.printStackTrace();
                    z = false;
                }
                if (!z) {
                    file.delete();
                    return false;
                }
                c(str, g2);
                return false;
            }
            this.h.put(str, f(str));
        } else if (this.g.containsKey(str)) {
            return false;
        }
        if (!this.g.containsKey(str)) {
            this.g.put(str, Integer.valueOf(this.c.a(str)));
            Map map = (Map) this.j.get(str);
            if (map != null && map.size() > 0) {
                try {
                    for (i a2 : map.values()) {
                        a2.a(str, 1, 0);
                    }
                } catch (RemoteException e3) {
                    e3.printStackTrace();
                }
            }
            g();
        }
        return true;
    }

    public final synchronized int a(DownloadInfo downloadInfo) {
        int i2;
        if (downloadInfo != null) {
            if (this.i.get(downloadInfo.a) == null) {
                this.i.put(downloadInfo.a, downloadInfo);
                downloadInfo.e = 0;
            }
            if (this.d.getResources().getConfiguration().locale.getLanguage().toLowerCase().contains("zh")) {
                f();
                e();
            }
            if (h(downloadInfo.a)) {
                i2 = 0;
            }
        }
        i2 = 3;
        return i2;
    }

    public final void a() {
        a = null;
        this.i.clear();
        f.a();
        f.b();
        this.b.b();
        this.c.a();
        this.j.clear();
        if (this.p != null) {
            this.o = false;
            synchronized (this.n) {
                this.n.notify();
            }
        }
    }

    public final void a(i iVar) {
        for (Map map : this.j.values()) {
            if (map.containsValue(iVar)) {
                for (String str : map.keySet()) {
                    if (((i) map.get(str)).equals(iVar)) {
                        map.remove(str);
                    }
                }
            }
        }
    }

    public final void a(String str, int i2, int i3) {
        DownloadInfo downloadInfo = (DownloadInfo) this.i.get(str);
        if (downloadInfo != null) {
            downloadInfo.g = (long) i3;
            downloadInfo.h = (long) i2;
        }
        e();
        Map map = (Map) this.j.get(str);
        if (map != null && map.size() > 0) {
            try {
                for (i a2 : map.values()) {
                    a2.a(str, i2, i3);
                }
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
    }

    public final void a(String str, j jVar) {
        if (jVar != null) {
            this.k.put(str, jVar);
            jVar.a(d());
        }
    }

    public final void a(String str, String str2) {
        Map map = (Map) this.j.get(str);
        if (map != null) {
            map.remove(str2);
        }
    }

    public final void a(String str, String str2, i iVar) {
        Object obj = (Map) this.j.get(str);
        if (obj == null) {
            obj = new HashMap();
        }
        obj.put(str2, iVar);
        this.j.put(str, obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.module.download.a.a(java.lang.String, byte[], boolean):void
     arg types: [java.lang.String, byte[], int]
     candidates:
      com.tencent.module.download.a.a(java.lang.String, java.lang.String, int):void
      com.tencent.module.download.a.a(java.lang.String, int, int):void
      com.tencent.module.download.a.a(java.lang.String, java.lang.String, com.tencent.module.download.i):void
      com.tencent.module.download.t.a(java.lang.String, int, int):void
      com.tencent.module.download.a.a(java.lang.String, byte[], boolean):void */
    public final void a(String str, byte[] bArr) {
        a(str, bArr, false);
    }

    public final boolean a(DownloadInfo downloadInfo, i iVar) {
        boolean z;
        if (!(Environment.getExternalStorageState().equals("mounted"))) {
            BaseApp.a((int) R.string.sdcard_remove_can_not_download);
            return false;
        }
        String g2 = g(downloadInfo.a);
        File file = new File(g2);
        if (!file.exists()) {
            return true;
        }
        try {
            z = new c(this.d).a(g2) != null;
        } catch (Exception e2) {
            e2.printStackTrace();
            z = false;
        }
        if (!z) {
            file.delete();
            return true;
        }
        BaseApp.a("文件已存在");
        try {
            iVar.a(downloadInfo.a, g2);
        } catch (RemoteException e3) {
            e3.printStackTrace();
        }
        return false;
    }

    public final boolean a(String str) {
        if (str == null || str.length() == 0) {
            return false;
        }
        b(str);
        this.h.remove(str);
        this.i.remove(str);
        return h(str);
    }

    public final void b() {
        for (DownloadInfo downloadInfo : this.i.values()) {
            if (downloadInfo.e == 3) {
                this.i.remove(downloadInfo.a);
            }
        }
    }

    public final synchronized void b(String str) {
        if (str != null) {
            if (str.length() != 0) {
                if (this.g.containsKey(str)) {
                    int intValue = ((Integer) this.g.remove(str)).intValue();
                    if (intValue >= 0) {
                        this.b.a(intValue);
                    }
                    this.i.remove(str);
                    if (this.d.getResources().getConfiguration().locale.getLanguage().toLowerCase().contains("zh")) {
                        f();
                        e();
                    }
                    g();
                }
                if (this.h.containsKey(str)) {
                    File file = new File((String) this.h.remove(str));
                    if (file.exists()) {
                        file.delete();
                    }
                }
            }
        }
    }

    public final void b(String str, String str2) {
        BaseApp.a("下载失败!");
        Map map = (Map) this.j.get(str);
        if (map != null && map.size() > 0) {
            Collection<i> values = map.values();
            this.j.remove(str);
            try {
                for (i a2 : values) {
                    a2.a(str, 0, str2);
                }
                b(str);
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.module.download.a.a(java.lang.String, byte[], boolean):void
     arg types: [java.lang.String, byte[], int]
     candidates:
      com.tencent.module.download.a.a(java.lang.String, java.lang.String, int):void
      com.tencent.module.download.a.a(java.lang.String, int, int):void
      com.tencent.module.download.a.a(java.lang.String, java.lang.String, com.tencent.module.download.i):void
      com.tencent.module.download.t.a(java.lang.String, int, int):void
      com.tencent.module.download.a.a(java.lang.String, byte[], boolean):void */
    public final void b(String str, byte[] bArr) {
        a(str, bArr, true);
    }

    public final void c() {
        this.m = 0;
        f.a().c();
    }

    public final void c(String str) {
        this.k.remove(str);
    }

    public final List d() {
        ArrayList arrayList = new ArrayList();
        for (DownloadInfo downloadInfo : this.i.values()) {
            if (downloadInfo.e == 1 || downloadInfo.e == 0) {
                arrayList.add(downloadInfo);
            }
        }
        return arrayList;
    }

    public final void d(String str) {
        this.j.remove(str);
    }

    public final void e(String str) {
        Map map = (Map) this.j.get(str);
        if (map != null && map.size() > 0) {
            Collection<i> values = map.values();
            this.j.remove(str);
            try {
                b(str);
                for (i a2 : values) {
                    a2.a(str);
                }
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
    }
}
