package com.tencent.module.download;

final class x implements Runnable {
    private /* synthetic */ a a;

    x(a aVar) {
        this.a = aVar;
    }

    public final void run() {
        while (this.a.o) {
            synchronized (this.a.n) {
                try {
                    this.a.n.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            while (this.a.f.size() > 0) {
                try {
                    c cVar = (c) this.a.f.remove(0);
                    if (cVar != null) {
                        a.a(this.a, cVar.a, cVar.b, cVar.c, cVar.d);
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }
}
