package com.tencent.module.download;

import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import com.tencent.launcher.SearchAppTestActivity;
import com.tencent.module.appcenter.SoftWareActivity;
import java.util.List;

final class p extends Handler {
    private /* synthetic */ DownloadActivity a;

    p(DownloadActivity downloadActivity) {
        this.a = downloadActivity;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                this.a.mStatus1.setText("下载异常, errorCode:" + message.arg1 + ",error:" + message.obj);
                return;
            case 1:
                this.a.mStatus1.setText("url:" + message.obj + ",进度:" + ((message.arg1 * 100) / message.arg2) + "%");
                return;
            case 2:
                String[] strArr = (String[]) message.obj;
                this.a.mStatus1.setText("下载完成:" + strArr[0] + "<br/> 保存路径： " + strArr[1]);
                try {
                    this.a.mService.a(strArr[0], "DownloadActivity");
                    return;
                } catch (RemoteException e) {
                    e.printStackTrace();
                    return;
                }
            case 3:
                this.a.mStatus2.setText("下载取消:" + message.obj);
                return;
            case 10:
                this.a.mStatus2.setText("下载异常, errorCode:" + message.arg1 + ",error:" + message.obj);
                return;
            case 11:
                this.a.mStatus2.setText("url:" + message.obj + ",进度:" + ((message.arg1 * 100) / message.arg2) + "%");
                return;
            case 12:
                String[] strArr2 = (String[]) message.obj;
                this.a.mStatus2.setText("下载完成:" + strArr2[0] + "<br/> 保存路径： " + strArr2[1]);
                try {
                    this.a.mService.a(strArr2[0], "DownloadActivity");
                    return;
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                    return;
                }
            case 13:
                this.a.mStatus2.setText("下载取消:" + message.obj);
                return;
            case SoftWareActivity.PAGE_SIZE:
                this.a.mStatus3.setText("下载异常, errorCode:" + message.arg1 + ",error:" + message.obj);
                return;
            case 21:
                this.a.mStatus3.setText("url:" + message.obj + ",进度:" + ((message.arg1 * 100) / message.arg2) + "%");
                return;
            case 22:
                String[] strArr3 = (String[]) message.obj;
                this.a.mStatus3.setText("下载完成:" + strArr3[0] + "<br/> 保存路径： " + strArr3[1]);
                try {
                    this.a.mService.a(strArr3[0], "DownloadActivity");
                    return;
                } catch (RemoteException e3) {
                    e3.printStackTrace();
                    return;
                }
            case 23:
                this.a.mStatus3.setText("下载取消:" + message.obj);
                return;
            case SearchAppTestActivity.TAB_TYPE_NET:
                StringBuffer stringBuffer = new StringBuffer();
                for (DownloadInfo downloadInfo : (List) message.obj) {
                    stringBuffer.append(downloadInfo.a);
                    stringBuffer.append(":");
                    stringBuffer.append(downloadInfo.e);
                    stringBuffer.append("\n");
                }
                this.a.mDownlaodList.setText(stringBuffer.toString());
                return;
            default:
                return;
        }
    }
}
