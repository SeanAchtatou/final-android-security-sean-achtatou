package com.tencent.module.download;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

public abstract class b extends Binder implements i {
    public b() {
        attachInterface(this, "com.tencent.module.download.IDownloadListener");
    }

    public static i a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.tencent.module.download.IDownloadListener");
        return (queryLocalInterface == null || !(queryLocalInterface instanceof i)) ? new e(iBinder) : (i) queryLocalInterface;
    }

    public IBinder asBinder() {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        switch (i) {
            case 1:
                parcel.enforceInterface("com.tencent.module.download.IDownloadListener");
                a(parcel.readString(), parcel.readInt(), parcel.readString());
                return true;
            case 2:
                parcel.enforceInterface("com.tencent.module.download.IDownloadListener");
                a(parcel.readString(), parcel.readInt(), parcel.readInt());
                return true;
            case 3:
                parcel.enforceInterface("com.tencent.module.download.IDownloadListener");
                a(parcel.readString(), parcel.readString());
                return true;
            case 4:
                parcel.enforceInterface("com.tencent.module.download.IDownloadListener");
                a(parcel.readString());
                return true;
            case 1598968902:
                parcel2.writeString("com.tencent.module.download.IDownloadListener");
                return true;
            default:
                return super.onTransact(i, parcel, parcel2, i2);
        }
    }
}
