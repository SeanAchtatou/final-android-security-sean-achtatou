package com.tencent.module.download;

import com.tencent.module.download.a.a;
import com.tencent.module.download.a.b;

public final class u implements a {
    private static u d = null;
    private final String a = "DownloadEngine";
    private t b = null;
    private q c;
    private int e = 0;

    private u(t tVar) {
        this.b = tVar;
        this.c = q.a();
    }

    public static u a(t tVar) {
        if (d == null) {
            d = new u(tVar);
        }
        return d;
    }

    public final int a(String str) {
        b bVar = new b(str, this);
        bVar.b();
        this.c.a(bVar);
        return bVar.a();
    }

    public final void a() {
        if (this.c != null) {
            this.c.b();
            this.c = null;
        }
        d = null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00d3 A[SYNTHETIC, Splitter:B:43:0x00d3] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00dc A[Catch:{ Exception -> 0x00e3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00f0 A[SYNTHETIC, Splitter:B:55:0x00f0] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00f9 A[Catch:{ Exception -> 0x00fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0102  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(org.apache.http.HttpResponse r14, com.tencent.module.download.a.b r15) {
        /*
            r13 = this;
            r11 = 0
            r10 = 0
            org.apache.http.HttpEntity r0 = r14.getEntity()
            r1 = 51200(0xc800, float:7.1746E-41)
            byte[] r1 = new byte[r1]
            long r2 = r0.getContentLength()
            int r2 = (int) r2
            int r3 = r13.e
            int r3 = r3 + r2
            r13.e = r3
            boolean r3 = r15.a
            if (r3 == 0) goto L_0x001a
        L_0x0019:
            return
        L_0x001a:
            java.io.InputStream r0 = r0.getContent()     // Catch:{ Exception -> 0x00bf, all -> 0x00eb }
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x011e, all -> 0x010d }
            r3.<init>()     // Catch:{ Exception -> 0x011e, all -> 0x010d }
            r4 = r10
            r5 = r10
            r6 = r3
            r3 = r10
        L_0x0027:
            if (r5 >= r2) goto L_0x012d
            int r7 = r0.read(r1)     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            r8 = -1
            if (r7 == r8) goto L_0x012d
            int r5 = r5 + r7
            int r3 = r3 + r7
            r8 = 0
            r6.write(r1, r8, r7)     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            com.tencent.module.download.t r7 = r13.b     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            java.lang.String r8 = r15.c     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            r7.a(r8, r2, r5)     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            boolean r7 = r15.a     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            if (r7 == 0) goto L_0x006f
            boolean r1 = r15.b     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            if (r1 == 0) goto L_0x012d
            r6.toByteArray()     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            r1.<init>()     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
        L_0x004d:
            byte[] r2 = r1.toByteArray()     // Catch:{ Exception -> 0x0128, all -> 0x0117 }
            if (r0 == 0) goto L_0x005a
            boolean r3 = r15.a     // Catch:{ Exception -> 0x00b8 }
            if (r3 != 0) goto L_0x005a
            r0.close()     // Catch:{ Exception -> 0x00b8 }
        L_0x005a:
            r1.close()     // Catch:{ Exception -> 0x00b8 }
            r0 = r10
            r1 = r2
        L_0x005f:
            boolean r2 = r15.a
            if (r2 == 0) goto L_0x0102
            boolean r0 = r15.b
            if (r0 != 0) goto L_0x0019
            com.tencent.module.download.t r0 = r13.b
            java.lang.String r1 = r15.c
            r0.e(r1)
            goto L_0x0019
        L_0x006f:
            r7 = 358400(0x57800, float:5.02225E-40)
            if (r3 < r7) goto L_0x0027
            byte[] r3 = r6.toByteArray()     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            java.lang.String r7 = "DownloadEngine"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            r8.<init>()     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            java.lang.String r9 = "savecache receiveLen:"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            java.lang.StringBuilder r8 = r8.append(r5)     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            java.lang.String r9 = " num:"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            int r9 = r4 + 1
            java.lang.StringBuilder r4 = r8.append(r4)     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            java.lang.String r8 = " temp:"
            java.lang.StringBuilder r4 = r4.append(r8)     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            int r8 = r3.length     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            java.lang.StringBuilder r4 = r4.append(r8)     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            android.util.Log.v(r7, r4)     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            com.tencent.module.download.t r4 = r13.b     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            java.lang.String r7 = r15.c     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            r4.a(r7, r3)     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            r3.<init>()     // Catch:{ Exception -> 0x0123, all -> 0x0112 }
            r4 = r9
            r6 = r3
            r3 = r10
            goto L_0x0027
        L_0x00b8:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r10
            r1 = r2
            goto L_0x005f
        L_0x00bf:
            r0 = move-exception
            r1 = r11
            r2 = r11
        L_0x00c2:
            r3 = 1
            r0.printStackTrace()     // Catch:{ all -> 0x011c }
            com.tencent.module.download.t r4 = r13.b     // Catch:{ all -> 0x011c }
            java.lang.String r5 = r15.c     // Catch:{ all -> 0x011c }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x011c }
            r4.b(r5, r0)     // Catch:{ all -> 0x011c }
            if (r2 == 0) goto L_0x00da
            boolean r0 = r15.a     // Catch:{ Exception -> 0x00e3 }
            if (r0 != 0) goto L_0x00da
            r2.close()     // Catch:{ Exception -> 0x00e3 }
        L_0x00da:
            if (r1 == 0) goto L_0x00df
            r1.close()     // Catch:{ Exception -> 0x00e3 }
        L_0x00df:
            r0 = r3
            r1 = r11
            goto L_0x005f
        L_0x00e3:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r3
            r1 = r11
            goto L_0x005f
        L_0x00eb:
            r0 = move-exception
            r1 = r11
            r2 = r11
        L_0x00ee:
            if (r2 == 0) goto L_0x00f7
            boolean r3 = r15.a     // Catch:{ Exception -> 0x00fd }
            if (r3 != 0) goto L_0x00f7
            r2.close()     // Catch:{ Exception -> 0x00fd }
        L_0x00f7:
            if (r1 == 0) goto L_0x00fc
            r1.close()     // Catch:{ Exception -> 0x00fd }
        L_0x00fc:
            throw r0
        L_0x00fd:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00fc
        L_0x0102:
            if (r0 != 0) goto L_0x0019
            com.tencent.module.download.t r0 = r13.b
            java.lang.String r2 = r15.c
            r0.b(r2, r1)
            goto L_0x0019
        L_0x010d:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r11
            goto L_0x00ee
        L_0x0112:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r6
            goto L_0x00ee
        L_0x0117:
            r2 = move-exception
            r12 = r2
            r2 = r0
            r0 = r12
            goto L_0x00ee
        L_0x011c:
            r0 = move-exception
            goto L_0x00ee
        L_0x011e:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r11
            goto L_0x00c2
        L_0x0123:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r6
            goto L_0x00c2
        L_0x0128:
            r2 = move-exception
            r12 = r2
            r2 = r0
            r0 = r12
            goto L_0x00c2
        L_0x012d:
            r1 = r6
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.module.download.u.a(org.apache.http.HttpResponse, com.tencent.module.download.a.b):void");
    }
}
