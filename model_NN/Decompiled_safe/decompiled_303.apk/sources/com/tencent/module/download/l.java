package com.tencent.module.download;

import android.os.Message;

final class l extends b {
    private /* synthetic */ DownloadActivity a;

    l(DownloadActivity downloadActivity) {
        this.a = downloadActivity;
    }

    public final void a(String str) {
        Message.obtain(this.a.mHandler, 13, 0, 0, str).sendToTarget();
    }

    public final void a(String str, int i, int i2) {
        Message.obtain(this.a.mHandler, 11, i2, i, str).sendToTarget();
    }

    public final void a(String str, int i, String str2) {
        Message.obtain(this.a.mHandler, 10, i, 0, str2).sendToTarget();
    }

    public final void a(String str, String str2) {
        Message.obtain(this.a.mHandler, 12, 0, 0, new String[]{str, str2}).sendToTarget();
    }
}
