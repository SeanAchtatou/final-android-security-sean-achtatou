package com.tencent.module.download;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.tencent.module.setting.DesktopSwitchSpecialEffectSettingActivity;
import java.util.List;

public abstract class s extends Binder implements y {
    public s() {
        attachInterface(this, "com.tencent.module.download.IDownloadService");
    }

    public static y a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.tencent.module.download.IDownloadService");
        return (queryLocalInterface == null || !(queryLocalInterface instanceof y)) ? new d(iBinder) : (y) queryLocalInterface;
    }

    public IBinder asBinder() {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        j vVar;
        switch (i) {
            case 1:
                parcel.enforceInterface("com.tencent.module.download.IDownloadService");
                a(parcel.readString(), parcel.readString(), b.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 2:
                parcel.enforceInterface("com.tencent.module.download.IDownloadService");
                a(parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 3:
                parcel.enforceInterface("com.tencent.module.download.IDownloadService");
                a(b.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 4:
                parcel.enforceInterface("com.tencent.module.download.IDownloadService");
                a(parcel.readInt() != 0 ? (DownloadInfo) DownloadInfo.CREATOR.createFromParcel(parcel) : null, parcel.readString(), b.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 5:
                parcel.enforceInterface("com.tencent.module.download.IDownloadService");
                b(parcel.readInt() != 0 ? (DownloadInfo) DownloadInfo.CREATOR.createFromParcel(parcel) : null, parcel.readString(), b.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 6:
                parcel.enforceInterface("com.tencent.module.download.IDownloadService");
                a(parcel.readString(), b.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case DesktopSwitchSpecialEffectSettingActivity.nRotationSetting /*7*/:
                parcel.enforceInterface("com.tencent.module.download.IDownloadService");
                a();
                parcel2.writeNoException();
                return true;
            case DesktopSwitchSpecialEffectSettingActivity.nCubeSetting /*8*/:
                parcel.enforceInterface("com.tencent.module.download.IDownloadService");
                List b = b();
                parcel2.writeNoException();
                parcel2.writeTypedList(b);
                return true;
            case 9:
                parcel.enforceInterface("com.tencent.module.download.IDownloadService");
                String readString = parcel.readString();
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder == null) {
                    vVar = null;
                } else {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.tencent.module.download.IDownloadListListener");
                    vVar = (queryLocalInterface == null || !(queryLocalInterface instanceof j)) ? new v(readStrongBinder) : (j) queryLocalInterface;
                }
                a(readString, vVar);
                parcel2.writeNoException();
                return true;
            case 10:
                parcel.enforceInterface("com.tencent.module.download.IDownloadService");
                a(parcel.readString());
                parcel2.writeNoException();
                return true;
            case 11:
                parcel.enforceInterface("com.tencent.module.download.IDownloadService");
                c();
                parcel2.writeNoException();
                return true;
            case 1598968902:
                parcel2.writeString("com.tencent.module.download.IDownloadService");
                return true;
            default:
                return super.onTransact(i, parcel, parcel2, i2);
        }
    }
}
