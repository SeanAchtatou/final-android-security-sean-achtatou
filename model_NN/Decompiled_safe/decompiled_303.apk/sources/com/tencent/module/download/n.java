package com.tencent.module.download;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;

final class n implements ServiceConnection {
    private /* synthetic */ DownloadActivity a;

    n(DownloadActivity downloadActivity) {
        this.a = downloadActivity;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        y unused = this.a.mService = s.a(iBinder);
        try {
            this.a.mService.a("DownloadActivity", this.a.mListListener);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        y unused = this.a.mService = null;
    }
}
