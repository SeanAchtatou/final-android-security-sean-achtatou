package com.tencent.module.download;

import android.os.IBinder;
import android.os.Parcel;
import java.util.List;

final class v implements j {
    private IBinder a;

    v(IBinder iBinder) {
        this.a = iBinder;
    }

    public final void a(List list) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.module.download.IDownloadListListener");
            obtain.writeTypedList(list);
            this.a.transact(1, obtain, obtain2, 0);
            obtain2.readException();
            obtain2.readTypedList(list, DownloadInfo.CREATOR);
        } finally {
            obtain2.recycle();
            obtain.recycle();
        }
    }

    public final IBinder asBinder() {
        return this.a;
    }
}
