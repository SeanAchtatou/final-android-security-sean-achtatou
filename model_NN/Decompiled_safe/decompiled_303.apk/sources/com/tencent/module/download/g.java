package com.tencent.module.download;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Process;

final class g extends BroadcastReceiver {
    private /* synthetic */ DownloadService a;

    g(DownloadService downloadService) {
        this.a = downloadService;
    }

    public final void onReceive(Context context, Intent intent) {
        if ("com.tencent.qqlauncher.QUIT".equals(intent.getAction())) {
            if (this.a.a != null) {
                this.a.a.a();
                a unused = this.a.a = null;
            }
            this.a.stopSelf();
            Process.killProcess(Process.myPid());
        }
    }
}
