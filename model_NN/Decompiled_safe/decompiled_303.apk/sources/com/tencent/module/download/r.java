package com.tencent.module.download;

import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;
import java.util.ArrayList;

public abstract class r extends Binder implements j {
    public r() {
        attachInterface(this, "com.tencent.module.download.IDownloadListListener");
    }

    public IBinder asBinder() {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        switch (i) {
            case 1:
                parcel.enforceInterface("com.tencent.module.download.IDownloadListListener");
                ArrayList createTypedArrayList = parcel.createTypedArrayList(DownloadInfo.CREATOR);
                a(createTypedArrayList);
                parcel2.writeNoException();
                parcel2.writeTypedList(createTypedArrayList);
                return true;
            case 1598968902:
                parcel2.writeString("com.tencent.module.download.IDownloadListListener");
                return true;
            default:
                return super.onTransact(i, parcel, parcel2, i2);
        }
    }
}
