package com.tencent.module.download;

import android.os.IBinder;
import android.os.Parcel;

final class e implements i {
    private IBinder a;

    e(IBinder iBinder) {
        this.a = iBinder;
    }

    public final void a(String str) {
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.module.download.IDownloadListener");
            obtain.writeString(str);
            this.a.transact(4, obtain, null, 1);
        } finally {
            obtain.recycle();
        }
    }

    public final void a(String str, int i, int i2) {
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.module.download.IDownloadListener");
            obtain.writeString(str);
            obtain.writeInt(i);
            obtain.writeInt(i2);
            this.a.transact(2, obtain, null, 1);
        } finally {
            obtain.recycle();
        }
    }

    public final void a(String str, int i, String str2) {
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.module.download.IDownloadListener");
            obtain.writeString(str);
            obtain.writeInt(i);
            obtain.writeString(str2);
            this.a.transact(1, obtain, null, 1);
        } finally {
            obtain.recycle();
        }
    }

    public final void a(String str, String str2) {
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.module.download.IDownloadListener");
            obtain.writeString(str);
            obtain.writeString(str2);
            this.a.transact(3, obtain, null, 1);
        } finally {
            obtain.recycle();
        }
    }

    public final IBinder asBinder() {
        return this.a;
    }
}
