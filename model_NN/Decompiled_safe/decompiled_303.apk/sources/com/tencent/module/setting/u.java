package com.tencent.module.setting;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import com.tencent.launcher.a;

public final class u {
    CharSequence a;
    Drawable b;
    String c;
    String d;
    Bundle e;

    u(Context context, PackageManager packageManager, ResolveInfo resolveInfo) {
        this.a = resolveInfo.loadLabel(packageManager);
        if (this.a == null && resolveInfo.activityInfo != null) {
            this.a = resolveInfo.activityInfo.name;
        }
        this.b = a.a(resolveInfo.loadIcon(packageManager), context);
        this.c = resolveInfo.activityInfo.applicationInfo.packageName;
        this.d = resolveInfo.activityInfo.name;
    }

    u(Context context, CharSequence charSequence, Drawable drawable) {
        this.a = charSequence;
        this.b = a.a(drawable, context);
    }
}
