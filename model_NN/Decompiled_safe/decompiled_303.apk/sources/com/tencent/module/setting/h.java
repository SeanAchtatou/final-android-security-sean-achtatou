package com.tencent.module.setting;

import android.os.Message;
import android.view.View;

final class h implements View.OnClickListener {
    private /* synthetic */ CustomAlertController a;

    h(CustomAlertController customAlertController) {
        this.a = customAlertController;
    }

    public final void onClick(View view) {
        Message message = null;
        if (view == this.a.m && this.a.o != null) {
            message = Message.obtain(this.a.o);
        } else if (view == this.a.p && this.a.r != null) {
            message = Message.obtain(this.a.r);
        } else if (view == this.a.s && this.a.u != null) {
            message = Message.obtain(this.a.u);
        }
        if (message != null) {
            message.sendToTarget();
        }
        this.a.E.obtainMessage(1, this.a.b).sendToTarget();
    }
}
