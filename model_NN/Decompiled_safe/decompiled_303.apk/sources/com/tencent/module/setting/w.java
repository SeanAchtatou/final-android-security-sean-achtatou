package com.tencent.module.setting;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import com.tencent.qqlauncher.R;

final class w implements AdapterView.OnItemClickListener {
    private /* synthetic */ DesktopSwitchSpecialEffectSettingActivity a;

    w(DesktopSwitchSpecialEffectSettingActivity desktopSwitchSpecialEffectSettingActivity) {
        this.a = desktopSwitchSpecialEffectSettingActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        switch (i) {
            case 0:
                Toast.makeText(this.a.getApplicationContext(), "nClassicSetting " + this.a.getString(R.string.switch_specialeffect_unimplement), 0).show();
                return;
            case 1:
                Toast.makeText(this.a.getApplicationContext(), "nClassicNoBackSetting " + this.a.getString(R.string.switch_specialeffect_unimplement), 0).show();
                return;
            case 2:
                Toast.makeText(this.a.getApplicationContext(), "nFadeSetting " + this.a.getString(R.string.switch_specialeffect_unimplement), 0).show();
                return;
            case 3:
            default:
                return;
            case 4:
                Toast.makeText(this.a.getApplicationContext(), "nTurntableSetting " + this.a.getString(R.string.switch_specialeffect_unimplement), 0).show();
                return;
            case 5:
                Toast.makeText(this.a.getApplicationContext(), "nPageSetting " + this.a.getString(R.string.switch_specialeffect_unimplement), 0).show();
                return;
            case 6:
                Toast.makeText(this.a.getApplicationContext(), "nCascadingSetting " + this.a.getString(R.string.switch_specialeffect_unimplement), 0).show();
                return;
            case DesktopSwitchSpecialEffectSettingActivity.nRotationSetting:
                Toast.makeText(this.a.getApplicationContext(), "nRotationSetting " + this.a.getString(R.string.switch_specialeffect_unimplement), 0).show();
                return;
            case DesktopSwitchSpecialEffectSettingActivity.nCubeSetting:
                Toast.makeText(this.a.getApplicationContext(), "nCubeSetting  " + this.a.getString(R.string.switch_specialeffect_unimplement), 0).show();
                return;
        }
    }
}
