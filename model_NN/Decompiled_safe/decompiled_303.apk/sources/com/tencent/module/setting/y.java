package com.tencent.module.setting;

import android.content.DialogInterface;
import android.content.Intent;
import com.tencent.launcher.Launcher;

final class y implements DialogInterface.OnClickListener {
    private /* synthetic */ SettingActivity a;

    y(SettingActivity settingActivity) {
        this.a = settingActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent();
        intent.setClass(this.a, Launcher.class);
        intent.setFlags(67108864);
        intent.putExtra("flag", 1);
        this.a.startActivity(intent);
    }
}
