package com.tencent.module.setting;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.tencent.qqlauncher.R;
import java.util.List;

public final class ad extends BaseAdapter {
    private final LayoutInflater a;
    private List b;
    private int c = R.layout.pick_item;

    public ad(Context context, List list) {
        this.a = (LayoutInflater) context.getSystemService("layout_inflater");
        this.b = list;
    }

    public final int getCount() {
        return this.b.size();
    }

    public final Object getItem(int i) {
        return this.b.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.a.inflate(this.c, viewGroup, false) : view;
        u uVar = (u) getItem(i);
        TextView textView = (TextView) inflate;
        textView.setText(uVar.a);
        textView.setCompoundDrawablesWithIntrinsicBounds(uVar.b, (Drawable) null, (Drawable) null, (Drawable) null);
        return inflate;
    }
}
