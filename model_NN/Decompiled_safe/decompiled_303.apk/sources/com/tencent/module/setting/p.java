package com.tencent.module.setting;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.tencent.qqlauncher.R;

final class p extends BaseAdapter {
    private Context a;
    private Integer[] b = {Integer.valueOf((int) R.drawable.folder_item_bg), Integer.valueOf((int) R.drawable.folder_item_bg), Integer.valueOf((int) R.drawable.folder_item_bg), Integer.valueOf((int) R.drawable.folder_item_bg), Integer.valueOf((int) R.drawable.folder_item_bg), Integer.valueOf((int) R.drawable.folder_item_bg), Integer.valueOf((int) R.drawable.folder_item_bg), Integer.valueOf((int) R.drawable.folder_item_bg), Integer.valueOf((int) R.drawable.folder_item_bg), Integer.valueOf((int) R.drawable.folder_item_bg), Integer.valueOf((int) R.drawable.folder_item_bg), Integer.valueOf((int) R.drawable.folder_item_bg), Integer.valueOf((int) R.drawable.folder_item_bg), Integer.valueOf((int) R.drawable.folder_item_bg), Integer.valueOf((int) R.drawable.folder_item_bg)};

    public p(Context context) {
        this.a = context;
    }

    public final int getCount() {
        return this.b.length;
    }

    public final Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        ImageView imageView;
        if (view == null) {
            imageView = new ImageView(this.a);
            imageView.setLayoutParams(new AbsListView.LayoutParams(70, 70));
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        } else {
            imageView = (ImageView) view;
        }
        imageView.setImageResource(this.b[i].intValue());
        return imageView;
    }
}
