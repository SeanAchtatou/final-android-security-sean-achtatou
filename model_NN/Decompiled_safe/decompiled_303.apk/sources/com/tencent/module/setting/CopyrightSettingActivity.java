package com.tencent.module.setting;

import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.view.ViewGroup;
import android.widget.TextView;
import com.tencent.qqlauncher.R;

public class CopyrightSettingActivity extends PreferenceActivity {
    public static String toHexColor(int i) {
        return toHexColor(Color.red(i), Color.green(i), Color.blue(i));
    }

    public static String toHexColor(int i, int i2, int i3) {
        StringBuffer stringBuffer = new StringBuffer("#");
        if (i < 0 || i > 255) {
            stringBuffer.append("00");
        } else {
            String hexString = Integer.toHexString(i);
            if (hexString.length() == 1) {
                stringBuffer.append('0');
            }
            stringBuffer.append(hexString);
        }
        if (i2 < 0 || i2 > 255) {
            stringBuffer.append("00");
        } else {
            String hexString2 = Integer.toHexString(i2);
            if (hexString2.length() == 1) {
                stringBuffer.append('0');
            }
            stringBuffer.append(hexString2);
        }
        if (i3 < 0 || i3 > 255) {
            stringBuffer.append("00");
        } else {
            String hexString3 = Integer.toHexString(i3);
            if (hexString3.length() == 1) {
                stringBuffer.append('0');
            }
            stringBuffer.append(hexString3);
        }
        return stringBuffer.toString();
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        boolean z = true;
        if (getWindow().getDecorView().getBackground() instanceof BitmapDrawable) {
            z = false;
        }
        if (!z) {
            ViewGroup viewGroup = (ViewGroup) findViewById(R.id.qqlauncher_content);
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                if (viewGroup.getChildAt(i) instanceof TextView) {
                    ((TextView) viewGroup.getChildAt(i)).setTextColor(Color.parseColor("#000000"));
                }
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        addPreferencesFromResource(R.xml.qqlaunchersetting_copyright);
        setContentView((int) R.layout.setting_copyright);
        TextView textView = (TextView) findViewById(R.id.software_license);
        textView.setText(Html.fromHtml(getString(R.string.setting_copyright_content)));
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        CharSequence text = textView.getText();
        if (text instanceof Spannable) {
            int length = text.length();
            Spannable spannable = (Spannable) textView.getText();
            URLSpan[] uRLSpanArr = (URLSpan[]) spannable.getSpans(0, length, URLSpan.class);
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text);
            spannableStringBuilder.clearSpans();
            for (URLSpan uRLSpan : uRLSpanArr) {
                spannableStringBuilder.setSpan(new ab(this, uRLSpan.getURL()), spannable.getSpanStart(uRLSpan), spannable.getSpanEnd(uRLSpan), 34);
            }
            textView.setText(spannableStringBuilder);
        }
    }
}
