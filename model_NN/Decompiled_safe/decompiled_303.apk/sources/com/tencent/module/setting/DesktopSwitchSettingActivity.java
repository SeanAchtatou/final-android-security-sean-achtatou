package com.tencent.module.setting;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.view.KeyEvent;
import com.tencent.launcher.home.i;
import com.tencent.qqlauncher.R;

public class DesktopSwitchSettingActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    public final i homeConfig = i.a();
    private ListPreference lp;
    private ListPreference mListPreference;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getPreferenceManager().setSharedPreferencesName("home_config");
        addPreferencesFromResource(R.xml.qqlaunchersetting_switchdesktop);
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
        this.lp = (ListPreference) findPreference("setting_switchdesktop_specialeffects");
        this.lp.getEntry();
        this.lp.setSummary(this.lp.getEntry());
        this.mListPreference = (ListPreference) getPreferenceScreen().findPreference("setting_switchdesktop_specialeffects");
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || keyEvent.getRepeatCount() != 0) {
            return false;
        }
        Intent intent = new Intent();
        intent.setClass(this, SettingActivity.class);
        startActivity(intent);
        finish();
        return false;
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        String key = preference.getKey();
        if (key != null && key.equals("setting_switchdesktop_specialeffects")) {
            this.lp.setSummary(this.lp.getEntry());
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mListPreference.setSummary(this.lp.getEntry());
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        if (str.equals("setting_switchdesktop_specialeffects")) {
            this.lp.setSummary(this.lp.getEntry());
        }
    }
}
