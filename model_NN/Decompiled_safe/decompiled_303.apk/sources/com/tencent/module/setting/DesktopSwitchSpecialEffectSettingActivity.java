package com.tencent.module.setting;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DesktopSwitchSpecialEffectSettingActivity extends ListActivity {
    public static final int nCascadingSetting = 6;
    public static final int nClassicNoBackSetting = 1;
    public static final int nClassicSetting = 0;
    public static final int nCubeSetting = 8;
    public static final int nFadeSetting = 2;
    public static final int nPageSetting = 5;
    public static final int nRotationSetting = 7;
    public static final int nTurntableSetting = 4;
    ListView m_ListView;

    private List getData() {
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        hashMap.put("desktopSwitchSpecialEffectInfo", getString(R.string.switch_specialeffect_sutra));
        arrayList.add(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("desktopSwitchSpecialEffectInfo", getString(R.string.switch_specialeffect_sutra_regress));
        arrayList.add(hashMap2);
        HashMap hashMap3 = new HashMap();
        hashMap3.put("desktopSwitchSpecialEffectInfo", getString(R.string.switch_specialeffect_fade));
        arrayList.add(hashMap3);
        HashMap hashMap4 = new HashMap();
        hashMap4.put("desktopSwitchSpecialEffectInfo", getString(R.string.switch_specialeffect_rotate));
        arrayList.add(hashMap4);
        HashMap hashMap5 = new HashMap();
        hashMap5.put("desktopSwitchSpecialEffectInfo", getString(R.string.switch_specialeffect_lapel));
        arrayList.add(hashMap5);
        HashMap hashMap6 = new HashMap();
        hashMap6.put("desktopSwitchSpecialEffectInfo", getString(R.string.switch_specialeffect_cascade));
        arrayList.add(hashMap6);
        HashMap hashMap7 = new HashMap();
        hashMap7.put("desktopSwitchSpecialEffectInfo", getString(R.string.switch_specialeffect_round));
        arrayList.add(hashMap7);
        HashMap hashMap8 = new HashMap();
        hashMap8.put("desktopSwitchSpecialEffectInfo", getString(R.string.switch_specialeffect_cube));
        arrayList.add(hashMap8);
        return arrayList;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.setting_desktopswitch_specialeffect);
        setListAdapter(new SimpleAdapter(this, getData(), R.layout.setting_desktopswitch_specialeffect_item, new String[]{"desktopSwitchSpecialEffectInfo"}, new int[]{R.id.desktopSwitchSpecialEffectInfo}));
        this.m_ListView = getListView();
        this.m_ListView.setOnItemSelectedListener(new v(this));
        this.m_ListView.setOnItemClickListener(new w(this));
    }
}
