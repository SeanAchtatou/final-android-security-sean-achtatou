package com.tencent.module.setting;

import android.view.View;
import android.widget.AdapterView;

final class i implements AdapterView.OnItemClickListener {
    private /* synthetic */ CustomAlertController a;
    private /* synthetic */ a b;

    i(a aVar, CustomAlertController customAlertController) {
        this.b = aVar;
        this.a = customAlertController;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.b.n.onClick(this.a.b, i);
        if (!this.b.q) {
            this.a.b.dismiss();
        }
    }
}
