package com.tencent.module.setting;

import android.app.AlertDialog;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.tencent.qqlauncher.R;

final class al extends Handler {
    final /* synthetic */ AboutSettingActivity a;

    al(AboutSettingActivity aboutSettingActivity) {
        this.a = aboutSettingActivity;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case AboutSettingActivity.UPDATE_READY:
                this.a.m_pDialog.cancel();
                new AlertDialog.Builder(this.a).setTitle((int) R.string.tipupdate_title).setMessage(this.a.getResources().getString(R.string.tipupdate_content_request_download)).setPositiveButton((int) R.string.confirm, new ae(this)).setNegativeButton((int) R.string.cancel, new af(this)).create().show();
                break;
            case AboutSettingActivity.UPDATE_NONEED:
                this.a.m_pDialog.cancel();
                Toast.makeText(this.a.getApplicationContext(), this.a.getString(R.string.update_software_noneed_update), 0).show();
                break;
            case AboutSettingActivity.UPDATE_ERROR:
                this.a.m_pDialog.cancel();
                Toast.makeText(this.a.getApplicationContext(), this.a.getString(R.string.update_software_error), 0).show();
                break;
        }
        super.handleMessage(message);
    }
}
