package com.tencent.module.setting;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import com.tencent.launcher.Launcher;
import com.tencent.qqlauncher.R;

public class TipNotify extends Activity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.tipnotify);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || keyEvent.getRepeatCount() != 0) {
            return false;
        }
        ((NotificationManager) getSystemService("notification")).cancel(0);
        Intent intent = new Intent();
        intent.setClass(this, Launcher.class);
        startActivity(intent);
        finish();
        return true;
    }
}
