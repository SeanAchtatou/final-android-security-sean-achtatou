package com.tencent.module.setting;

import android.app.Activity;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.tencent.qqlauncher.R;

public class TopicsSettingActivity extends Activity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.setting_topic);
        GridView gridView = (GridView) findViewById(R.id.settingTopic_View);
        gridView.setAdapter((ListAdapter) new p(this));
        gridView.setOnItemClickListener(new o(this));
    }
}
