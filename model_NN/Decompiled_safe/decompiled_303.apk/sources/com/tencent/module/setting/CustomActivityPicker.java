package com.tencent.module.setting;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CustomActivityPicker extends CustomAlertActivity implements DialogInterface.OnCancelListener, DialogInterface.OnClickListener {
    private ad mAdapter;
    private Intent mBaseIntent;

    /* access modifiers changed from: protected */
    public Intent getIntentForPosition(int i) {
        u uVar = (u) this.mAdapter.getItem(i);
        Intent intent = new Intent(this.mBaseIntent);
        if (uVar.c == null || uVar.d == null) {
            intent.setAction("android.intent.action.CREATE_SHORTCUT");
            intent.putExtra("android.intent.extra.shortcut.NAME", uVar.a);
        } else {
            intent.setClassName(uVar.c, uVar.d);
            if (uVar.e != null) {
                intent.putExtras(uVar.e);
            }
        }
        return intent;
    }

    /* access modifiers changed from: protected */
    public List getItems() {
        Drawable drawable;
        PackageManager packageManager = getPackageManager();
        ArrayList arrayList = new ArrayList();
        Intent intent = getIntent();
        ArrayList<String> stringArrayListExtra = intent.getStringArrayListExtra("android.intent.extra.shortcut.NAME");
        ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("android.intent.extra.shortcut.ICON_RESOURCE");
        if (stringArrayListExtra != null && parcelableArrayListExtra != null && stringArrayListExtra.size() == parcelableArrayListExtra.size()) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= stringArrayListExtra.size()) {
                    break;
                }
                String str = stringArrayListExtra.get(i2);
                try {
                    Intent.ShortcutIconResource shortcutIconResource = (Intent.ShortcutIconResource) parcelableArrayListExtra.get(i2);
                    Resources resourcesForApplication = packageManager.getResourcesForApplication(shortcutIconResource.packageName);
                    drawable = resourcesForApplication.getDrawable(resourcesForApplication.getIdentifier(shortcutIconResource.resourceName, null, null));
                } catch (PackageManager.NameNotFoundException e) {
                    drawable = null;
                }
                arrayList.add(new u(this, str, drawable));
                i = i2 + 1;
            }
        }
        if (this.mBaseIntent != null) {
            putIntentItems(this.mBaseIntent, arrayList);
        }
        return arrayList;
    }

    public void onCancel(DialogInterface dialogInterface) {
        setResult(0);
        finish();
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        setResult(-1, getIntentForPosition(i));
        finish();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        Parcelable parcelableExtra = intent.getParcelableExtra("android.intent.extra.INTENT");
        if (parcelableExtra instanceof Intent) {
            this.mBaseIntent = (Intent) parcelableExtra;
        } else {
            this.mBaseIntent = new Intent("android.intent.action.MAIN", (Uri) null);
            this.mBaseIntent.addCategory("android.intent.category.DEFAULT");
        }
        a aVar = this.mAlertParams;
        aVar.n = this;
        aVar.j = this;
        if (intent.hasExtra("android.intent.extra.TITLE")) {
            aVar.c = intent.getStringExtra("android.intent.extra.TITLE");
        } else {
            aVar.c = getTitle();
        }
        this.mAdapter = new ad(this, getItems());
        aVar.m = this.mAdapter;
        setupAlert();
    }

    /* access modifiers changed from: protected */
    public void putIntentItems(Intent intent, List list) {
        PackageManager packageManager = getPackageManager();
        List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 0);
        Collections.sort(queryIntentActivities, new ResolveInfo.DisplayNameComparator(packageManager));
        int size = queryIntentActivities.size();
        for (int i = 0; i < size; i++) {
            list.add(new u(this, packageManager, queryIntentActivities.get(i)));
        }
    }

    public void showWallPaper(int i) {
        startActivity(getIntentForPosition(i));
        finish();
    }
}
