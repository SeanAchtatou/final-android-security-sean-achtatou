package com.tencent.module.setting;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LockScreenSettingActivity extends ListActivity {
    public static final int nEnableAudioEffectSetting = 3;
    public static final int nEnableLockScreenSetting = 0;
    public static final int nUnlockAnimationSetting = 2;
    public static final int nUnlockMethodSetting = 1;
    ListView m_ListView;

    private List getData() {
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        hashMap.put("lockScreenInfo", "");
        arrayList.add(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("lockScreenInfo", getString(R.string.lock_screen_open));
        arrayList.add(hashMap2);
        HashMap hashMap3 = new HashMap();
        hashMap3.put("lockScreenInfo", getString(R.string.lock_screen_mode));
        arrayList.add(hashMap3);
        HashMap hashMap4 = new HashMap();
        hashMap4.put("lockScreenInfo", getString(R.string.lock_screen_animation));
        arrayList.add(hashMap4);
        HashMap hashMap5 = new HashMap();
        hashMap5.put("lockScreenInfo", getString(R.string.lock_screen_sound));
        arrayList.add(hashMap5);
        HashMap hashMap6 = new HashMap();
        hashMap6.put("lockScreenInfo", " ");
        arrayList.add(hashMap6);
        return arrayList;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.setting_lockscreen);
        setListAdapter(new SimpleAdapter(this, getData(), R.layout.setting_lockscree_item, new String[]{"lockScreenInfo"}, new int[]{R.id.lockScreenInfo}));
        this.m_ListView = getListView();
        this.m_ListView.setOnItemSelectedListener(new c(this));
        this.m_ListView.setOnItemClickListener(new b(this));
    }
}
