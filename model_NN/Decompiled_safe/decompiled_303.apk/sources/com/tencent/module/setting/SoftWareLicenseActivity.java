package com.tencent.module.setting;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class SoftWareLicenseActivity extends Activity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        WebView webView = new WebView(this);
        webView.loadUrl("file:///android_asset/right.html");
        webView.setScrollBarStyle(0);
        setContentView(webView);
    }
}
