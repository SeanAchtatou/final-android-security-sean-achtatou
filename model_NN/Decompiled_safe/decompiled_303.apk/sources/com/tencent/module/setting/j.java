package com.tencent.module.setting;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.CursorAdapter;
import com.tencent.module.setting.CustomAlertController;
import com.tencent.qqlauncher.R;

final class j extends CursorAdapter {
    private final int a;
    private final int b;
    private /* synthetic */ CustomAlertController.RecycleListView c;
    private /* synthetic */ a d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, boolean):void}
     arg types: [android.content.Context, android.database.Cursor, int]
     candidates:
      ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, int):void}
      ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, boolean):void} */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    j(a aVar, Context context, Cursor cursor, CustomAlertController.RecycleListView recycleListView) {
        super(context, cursor, false);
        this.d = aVar;
        this.c = recycleListView;
        Cursor cursor2 = getCursor();
        this.a = cursor2.getColumnIndexOrThrow(this.d.t);
        this.b = cursor2.getColumnIndexOrThrow(this.d.u);
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        ((CheckedTextView) view.findViewById(R.id.text1)).setText(cursor.getString(this.a));
        this.c.setItemChecked(cursor.getPosition(), cursor.getInt(this.b) == 1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.d.b.inflate((int) R.layout.select_dialog_multichoice, viewGroup, false);
    }
}
