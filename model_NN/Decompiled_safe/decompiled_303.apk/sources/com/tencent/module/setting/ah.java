package com.tencent.module.setting;

import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.Toast;
import com.tencent.qqlauncher.R;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

final class ah extends AsyncTask {
    private final ProgressDialog a;
    private /* synthetic */ SettingActivity b;

    /* synthetic */ ah(SettingActivity settingActivity) {
        this(settingActivity, (byte) 0);
    }

    private ah(SettingActivity settingActivity, byte b2) {
        this.b = settingActivity;
        this.a = new ProgressDialog(this.b);
    }

    private String a() {
        Drawable drawable;
        Bitmap bitmap;
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            return this.b.getResources().getString(R.string.setting_backup_sdcard_unmounted);
        }
        File file = new File(SettingActivity.BACKUP_DIR);
        File file2 = new File(Environment.getDataDirectory() + "/data/" + "com.tencent.qqlauncher" + "/databases/launcher.db");
        File file3 = new File(Environment.getDataDirectory() + "/data/" + "com.tencent.qqlauncher" + "/databases/launcher.db-shm");
        File file4 = new File(Environment.getDataDirectory() + "/data/" + "com.tencent.qqlauncher" + "/databases/launcher.db-wal");
        File file5 = new File(SettingActivity.BACKUP_DIR, "backup.db");
        File file6 = new File(SettingActivity.BACKUP_DIR, "backup.db-shm");
        File file7 = new File(SettingActivity.BACKUP_DIR, "backup.db-wal");
        File file8 = new File(Environment.getDataDirectory() + "/data/" + "com.tencent.qqlauncher" + "/shared_prefs/home_config.xml");
        File file9 = new File(SettingActivity.BACKUP_DIR, "backup.xml");
        File file10 = new File(SettingActivity.BACKUP_DIR, "wallpaper.png");
        try {
            if (!file.isDirectory() && !file.exists()) {
                file.mkdirs();
            }
            if (file5.exists()) {
                file5.delete();
            }
            if (file6.exists()) {
                file6.delete();
            }
            if (file7.exists()) {
                file7.delete();
            }
            if (file9.exists()) {
                file9.delete();
            }
            if (file10.exists()) {
                file10.delete();
            }
            file10.createNewFile();
            WallpaperManager wallpaperManager = (WallpaperManager) this.b.getSystemService("wallpaper");
            if (wallpaperManager.getWallpaperInfo() == null && (drawable = wallpaperManager.getDrawable()) != null && (drawable instanceof BitmapDrawable) && (bitmap = ((BitmapDrawable) drawable).getBitmap()) != null) {
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(file10);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 80, fileOutputStream);
                    fileOutputStream.flush();
                    fileOutputStream.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
            file5.createNewFile();
            SettingActivity.copyFile(file2, file5);
            file9.createNewFile();
            SettingActivity.copyFile(file8, file9);
            if (file3.exists()) {
                file6.createNewFile();
                SettingActivity.copyFile(file3, file6);
            }
            if (file4.exists()) {
                file7.createNewFile();
                SettingActivity.copyFile(file4, file7);
            }
            return this.b.getResources().getString(R.string.setting_backup_success);
        } catch (IOException e3) {
            e3.printStackTrace();
            return this.b.getResources().getString(R.string.setting_backup_error);
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        String str = (String) obj;
        if (this.a.isShowing()) {
            this.a.dismiss();
        }
        if (this.b.getResources().getString(R.string.setting_backup_success).equals(str)) {
            this.b.backPreference.setSummary(this.b.getString(R.string.setting_backup_summary, new Object[]{SettingActivity.dateformat.format(new Date())}));
        }
        Toast.makeText(this.b, str, 0).show();
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        this.a.setMessage(this.b.getResources().getString(R.string.setting_backup_progress_msg));
        this.a.show();
    }
}
