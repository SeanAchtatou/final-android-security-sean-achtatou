package com.tencent.module.setting;

import android.content.DialogInterface;
import android.content.Intent;
import com.tencent.launcher.Launcher;

final class z implements DialogInterface.OnClickListener {
    private /* synthetic */ String a;
    private /* synthetic */ SettingActivity b;

    z(SettingActivity settingActivity, String str) {
        this.b = settingActivity;
        this.a = str;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.b.getPackageManager().clearPackagePreferredActivities(this.a);
        Intent intent = new Intent();
        intent.setClass(this.b, Launcher.class);
        intent.setFlags(67108864);
        intent.putExtra("flag", 1);
        this.b.startActivity(intent);
    }
}
