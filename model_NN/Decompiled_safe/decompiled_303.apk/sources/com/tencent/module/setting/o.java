package com.tencent.module.setting;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import com.tencent.qqlauncher.R;

final class o implements AdapterView.OnItemClickListener {
    private /* synthetic */ TopicsSettingActivity a;

    o(TopicsSettingActivity topicsSettingActivity) {
        this.a = topicsSettingActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Toast.makeText(this.a.getApplicationContext(), this.a.getString(R.string.setting_topics_drawable_selected, new Object[]{Integer.valueOf(i + 1)}), 0).show();
    }
}
