package com.tencent.module.setting;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import com.tencent.launcher.base.a;

public class BasePreferenceActivity extends PreferenceActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        a.a.incrementAndGet();
        super.onCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        a.a.decrementAndGet();
        super.onDestroy();
    }
}
