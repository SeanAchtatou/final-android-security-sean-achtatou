package com.tencent.module.setting;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

final class ae implements DialogInterface.OnClickListener {
    private /* synthetic */ al a;

    ae(al alVar) {
        this.a = alVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(AboutSettingActivity.updateUrl)));
    }
}
