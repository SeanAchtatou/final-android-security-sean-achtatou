package com.tencent.module.setting;

import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;
import com.tencent.launcher.Launcher;
import com.tencent.qqlauncher.R;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

final class f extends AsyncTask {
    private final ProgressDialog a;
    private /* synthetic */ SettingActivity b;

    /* synthetic */ f(SettingActivity settingActivity) {
        this(settingActivity, (byte) 0);
    }

    private f(SettingActivity settingActivity, byte b2) {
        this.b = settingActivity;
        this.a = new ProgressDialog(this.b);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.launcher.home.i.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.launcher.home.i.a(java.lang.String, int):void
      com.tencent.launcher.home.i.a(java.lang.String, long):void
      com.tencent.launcher.home.i.a(java.lang.String, java.lang.String):void
      com.tencent.launcher.home.i.a(java.lang.String, boolean):void */
    private String a() {
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            return this.b.getResources().getString(R.string.setting_backup_sdcard_unmounted);
        }
        File file = new File(SettingActivity.BACKUP_DIR, "backup.db");
        File file2 = new File(SettingActivity.BACKUP_DIR, "backup.xml");
        File file3 = new File(SettingActivity.BACKUP_DIR, "wallpaper.png");
        if (!file.exists() || !file2.exists()) {
            return this.b.getResources().getString(R.string.setting_backup_file_not_found);
        }
        if (!file.canRead() || !file2.canRead()) {
            return this.b.getResources().getString(R.string.setting_backup_file_not_readable);
        }
        File file4 = new File(Environment.getDataDirectory() + "/data/" + "com.tencent.qqlauncher" + "/databases/launcher.db");
        File file5 = new File(Environment.getDataDirectory() + "/data/" + "com.tencent.qqlauncher" + "/shared_prefs/home_config.xml");
        File file6 = new File(Environment.getDataDirectory() + "/data/" + "com.tencent.qqlauncher" + "/databases/launcher.db-shm");
        File file7 = new File(Environment.getDataDirectory() + "/data/" + "com.tencent.qqlauncher" + "/databases/launcher.db-wal");
        File file8 = new File(SettingActivity.BACKUP_DIR, "backup.db-shm");
        File file9 = new File(SettingActivity.BACKUP_DIR, "backup.db-wal");
        if (file4.exists()) {
            file4.delete();
        }
        if (file6.exists()) {
            file6.delete();
        }
        if (file7.exists()) {
            file7.delete();
        }
        if (file5.exists()) {
            file5.delete();
        }
        try {
            file4.createNewFile();
            SettingActivity.copyFile(file, file4);
            file5.createNewFile();
            SettingActivity.copyFile(file2, file5);
            if (file3.exists() && file3.canRead()) {
                try {
                    ((WallpaperManager) this.b.getSystemService("wallpaper")).setBitmap(BitmapFactory.decodeStream(new FileInputStream(file3)));
                } catch (Exception e) {
                    Log.e("", "Failed to set wallpaper: " + e);
                }
            }
            if (file8.exists()) {
                file6.createNewFile();
                SettingActivity.copyFile(file8, file6);
            }
            if (file9.exists()) {
                file7.createNewFile();
                SettingActivity.copyFile(file9, file7);
            }
            this.b.homeConfig.a("setting_config_reset", true);
            Launcher.getLauncher().requestRestartHome();
            return this.b.getResources().getString(R.string.setting_reset_success);
        } catch (IOException e2) {
            return this.b.getResources().getString(R.string.setting_reset_error);
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        String str = (String) obj;
        if (this.a.isShowing()) {
            this.a.dismiss();
        }
        Toast.makeText(this.b, str, 0).show();
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        this.a.setMessage(this.b.getResources().getString(R.string.setting_reset_progress_msg));
        this.a.show();
    }
}
