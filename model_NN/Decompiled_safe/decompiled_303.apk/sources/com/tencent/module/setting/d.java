package com.tencent.module.setting;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.tencent.launcher.gy;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

public class d extends BaseAdapter {
    private Context a;
    private ArrayList b;
    private LayoutInflater c = LayoutInflater.from(this.a);
    private /* synthetic */ a d;

    public d(a aVar, Context context, ArrayList arrayList) {
        this.d = aVar;
        this.a = context;
        this.b = arrayList;
    }

    public int getCount() {
        return this.b.size();
    }

    public Object getItem(int i) {
        return this.b.get(i);
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.c.inflate((int) R.layout.folder_add_select_dialog_multichoice, (ViewGroup) null) : view;
        TextView textView = (TextView) inflate;
        if (i >= this.b.size()) {
            return inflate;
        }
        gy gyVar = (gy) this.b.get(i);
        textView.setText(gyVar.b);
        textView.setCompoundDrawablesWithIntrinsicBounds(gyVar.c, (Drawable) null, (Drawable) null, (Drawable) null);
        return inflate;
    }
}
