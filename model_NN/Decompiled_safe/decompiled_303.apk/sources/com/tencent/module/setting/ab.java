package com.tencent.module.setting;

import android.content.Intent;
import android.text.style.ClickableSpan;
import android.view.View;

final class ab extends ClickableSpan {
    private String a;
    private /* synthetic */ CopyrightSettingActivity b;

    ab(CopyrightSettingActivity copyrightSettingActivity, String str) {
        this.b = copyrightSettingActivity;
        this.a = str;
    }

    public final void onClick(View view) {
        Intent intent = new Intent();
        intent.setClass(this.b, SoftWareLicenseActivity.class);
        this.b.startActivity(intent);
    }
}
