package com.tencent.module.setting;

import com.tencent.launcher.BrightnessActivity;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Random;

final class n {
    private byte[] a;
    private byte[] b;
    private byte[] c;
    private int d;
    private int e;
    private int f;
    private int g;
    private byte[] h;
    private boolean i = true;
    private int j;
    private Random k = new Random();

    n() {
    }

    private static long a(byte[] bArr, int i2) {
        long j2 = 0;
        int i3 = i2 + 4;
        for (int i4 = i2; i4 < i3; i4++) {
            j2 = (j2 << 8) | ((long) (bArr[i4] & 255));
        }
        return (j2 >>> 32) | (4294967295L & j2);
    }

    private void a() {
        this.f = 0;
        while (this.f < 8) {
            if (this.i) {
                byte[] bArr = this.a;
                int i2 = this.f;
                bArr[i2] = (byte) (bArr[i2] ^ this.b[this.f]);
            } else {
                byte[] bArr2 = this.a;
                int i3 = this.f;
                bArr2[i3] = (byte) (bArr2[i3] ^ this.c[this.e + this.f]);
            }
            this.f++;
        }
        System.arraycopy(a(this.a), 0, this.c, this.d, 8);
        this.f = 0;
        while (this.f < 8) {
            byte[] bArr3 = this.c;
            int i4 = this.d + this.f;
            bArr3[i4] = (byte) (bArr3[i4] ^ this.b[this.f]);
            this.f++;
        }
        System.arraycopy(this.a, 0, this.b, 0, 8);
        this.e = this.d;
        this.d += 8;
        this.f = 0;
        this.i = false;
    }

    private byte[] a(byte[] bArr) {
        try {
            long a2 = a(bArr, 0);
            long a3 = a(bArr, 4);
            long a4 = a(this.h, 0);
            long a5 = a(this.h, 4);
            long a6 = a(this.h, 8);
            long a7 = a(this.h, 12);
            long j2 = a2;
            long j3 = a3;
            int i2 = 16;
            long j4 = 0;
            while (true) {
                int i3 = i2 - 1;
                if (i2 > 0) {
                    j4 = (j4 + 2654435769L) & 4294967295L;
                    j2 = (j2 + ((((j3 << 4) + a4) ^ (j3 + j4)) ^ ((j3 >>> 5) + a5))) & 4294967295L;
                    j3 = (j3 + ((((j2 << 4) + a6) ^ (j2 + j4)) ^ ((j2 >>> 5) + a7))) & 4294967295L;
                    i2 = i3;
                } else {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(8);
                    DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
                    dataOutputStream.writeInt((int) j2);
                    dataOutputStream.writeInt((int) j3);
                    dataOutputStream.close();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (IOException e2) {
            return null;
        }
    }

    private boolean b(byte[] bArr, int i2) {
        this.f = 0;
        while (this.f < 8) {
            if (this.j + this.f >= i2) {
                return true;
            }
            byte[] bArr2 = this.b;
            int i3 = this.f;
            bArr2[i3] = (byte) (bArr2[i3] ^ bArr[(this.d + 0) + this.f]);
            this.f++;
        }
        this.b = b(this.b);
        if (this.b == null) {
            return false;
        }
        this.j += 8;
        this.d += 8;
        this.f = 0;
        return true;
    }

    private byte[] b(byte[] bArr) {
        try {
            long a2 = a(bArr, 0);
            long a3 = a(bArr, 4);
            long a4 = a(this.h, 0);
            long a5 = a(this.h, 4);
            long a6 = a(this.h, 8);
            long a7 = a(this.h, 12);
            long j2 = a2;
            long j3 = a3;
            int i2 = 16;
            long j4 = 3816266640L;
            while (true) {
                int i3 = i2 - 1;
                if (i2 > 0) {
                    j3 = (j3 - ((((j2 << 4) + a6) ^ (j2 + j4)) ^ ((j2 >>> 5) + a7))) & 4294967295L;
                    j2 = (j2 - ((((j3 << 4) + a4) ^ (j3 + j4)) ^ ((j3 >>> 5) + a5))) & 4294967295L;
                    j4 = (j4 - 2654435769L) & 4294967295L;
                    i2 = i3;
                } else {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(8);
                    DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
                    dataOutputStream.writeInt((int) j2);
                    dataOutputStream.writeInt((int) j3);
                    dataOutputStream.close();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (IOException e2) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final byte[] a(byte[] bArr, byte[] bArr2) {
        int length = bArr.length;
        this.e = 0;
        this.d = 0;
        this.h = bArr2;
        byte[] bArr3 = new byte[8];
        if (length % 8 != 0 || length < 16) {
            return null;
        }
        this.b = b(bArr);
        this.f = this.b[0] & 7;
        int i2 = (length - this.f) - 10;
        if (i2 < 0) {
            return null;
        }
        for (int i3 = 0; i3 < bArr3.length; i3++) {
            bArr3[i3] = 0;
        }
        this.c = new byte[i2];
        this.e = 0;
        this.d = 8;
        this.j = 8;
        this.f++;
        this.g = 1;
        while (this.g <= 2) {
            if (this.f < 8) {
                this.f++;
                this.g++;
            }
            if (this.f == 8) {
                if (!b(bArr, length)) {
                    return null;
                }
                bArr3 = bArr;
            }
        }
        int i4 = i2;
        byte[] bArr4 = bArr3;
        int i5 = 0;
        while (i4 != 0) {
            if (this.f < 8) {
                this.c[i5] = (byte) (bArr4[(this.e + 0) + this.f] ^ this.b[this.f]);
                i5++;
                i4--;
                this.f++;
            }
            if (this.f == 8) {
                this.e = this.d - 8;
                if (!b(bArr, length)) {
                    return null;
                }
                bArr4 = bArr;
            }
        }
        this.g = 1;
        byte[] bArr5 = bArr4;
        while (this.g < 8) {
            if (this.f < 8) {
                if ((bArr5[(this.e + 0) + this.f] ^ this.b[this.f]) != 0) {
                    return null;
                }
                this.f++;
            }
            if (this.f == 8) {
                this.e = this.d;
                if (!b(bArr, length)) {
                    return null;
                }
                bArr5 = bArr;
            }
            this.g++;
        }
        return this.c;
    }

    /* access modifiers changed from: protected */
    public final byte[] b(byte[] bArr, byte[] bArr2) {
        int length = bArr.length;
        this.a = new byte[8];
        this.b = new byte[8];
        this.f = 1;
        this.g = 0;
        this.e = 0;
        this.d = 0;
        this.h = bArr2;
        this.i = true;
        this.f = (length + 10) % 8;
        if (this.f != 0) {
            this.f = 8 - this.f;
        }
        this.c = new byte[(this.f + length + 10)];
        this.a[0] = (byte) ((this.k.nextInt() & 248) | this.f);
        for (int i2 = 1; i2 <= this.f; i2++) {
            this.a[i2] = (byte) (this.k.nextInt() & BrightnessActivity.MAXIMUM_BACKLIGHT);
        }
        this.f++;
        for (int i3 = 0; i3 < 8; i3++) {
            this.b[i3] = 0;
        }
        this.g = 1;
        while (this.g <= 2) {
            if (this.f < 8) {
                byte[] bArr3 = this.a;
                int i4 = this.f;
                this.f = i4 + 1;
                bArr3[i4] = (byte) (this.k.nextInt() & BrightnessActivity.MAXIMUM_BACKLIGHT);
                this.g++;
            }
            if (this.f == 8) {
                a();
            }
        }
        int i5 = length;
        int i6 = 0;
        while (i5 > 0) {
            if (this.f < 8) {
                byte[] bArr4 = this.a;
                int i7 = this.f;
                this.f = i7 + 1;
                bArr4[i7] = bArr[i6];
                i5--;
                i6++;
            }
            if (this.f == 8) {
                a();
            }
        }
        this.g = 1;
        while (this.g <= 7) {
            if (this.f < 8) {
                byte[] bArr5 = this.a;
                int i8 = this.f;
                this.f = i8 + 1;
                bArr5[i8] = 0;
                this.g++;
            }
            if (this.f == 8) {
                a();
            }
        }
        return this.c;
    }
}
