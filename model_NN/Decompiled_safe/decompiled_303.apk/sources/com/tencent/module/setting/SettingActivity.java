package com.tencent.module.setting;

import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.widget.Toast;
import com.tencent.launcher.Launcher;
import com.tencent.launcher.home.i;
import com.tencent.module.screenlock.LockService;
import com.tencent.qqlauncher.R;
import com.tencent.util.p;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SettingActivity extends BasePreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String BACKUP_DIR = (SD_PATH + "/Tencent/QQLauncher/backup/");
    private static final String BACKUP_WALLPAPER = "wallpaper.png";
    private static final String DB_BACKUP_FILENAME = "backup.db";
    private static final String NAMESPACE = "com.tencent.qqlauncher";
    private static final String PREF_BACKUP_FILENAME = "backup.xml";
    private static final String SD_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
    static final SimpleDateFormat dateformat = new SimpleDateFormat("yyyy/MM/dd HH时 ");
    private static final String feedBackUrl = "http://fwd.3g.qq.com:8080/forward.jsp?bid=480&sid=${sid}";
    /* access modifiers changed from: private */
    public Preference backPreference;
    public final i homeConfig = i.a();
    private ListPreference lp;
    private ListPreference mListPreference;

    public static void copyFile(File file, File file2) {
        FileChannel channel = new FileInputStream(file).getChannel();
        FileChannel channel2 = new FileOutputStream(file2).getChannel();
        try {
            channel.transferTo(0, channel.size(), channel2);
        } finally {
            if (channel != null) {
                channel.close();
            }
            if (channel2 != null) {
                channel2.close();
            }
        }
    }

    private void handleClearDefault() {
        ResolveInfo c = p.c(this);
        if (c == null) {
            Toast.makeText(this, (int) R.string.toast_no_default, 0).show();
            return;
        }
        String packageName = getPackageName();
        if (packageName.equals(c.activityInfo.packageName)) {
            getPackageManager().clearPackagePreferredActivities(packageName);
            Toast.makeText(this, (int) R.string.toast_clear_self, 0).show();
            return;
        }
        startActivity(p.a(c.activityInfo.packageName));
        Toast.makeText(this, (int) R.string.toast_clear_default, 0).show();
    }

    private void setLastBackupTime(Preference preference) {
        File file = new File(BACKUP_DIR, DB_BACKUP_FILENAME);
        if (file.exists()) {
            preference.setSummary(getString(R.string.setting_backup_summary, new Object[]{dateformat.format(new Date(file.lastModified()))}));
        }
    }

    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        getPreferenceManager().setSharedPreferencesName("home_config");
        addPreferencesFromResource(R.xml.qqlaunchersetting);
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
        String b = this.homeConfig.b("setting_normal_functionlist", "");
        if (b == "") {
            str = "0";
        } else if (b.equals("0")) {
            this.homeConfig.a("setting_normal_functionlist", b);
            str = b;
        } else {
            if (b.equals("1")) {
                this.homeConfig.a("setting_normal_functionlist", b);
            }
            str = b;
        }
        this.lp = (ListPreference) findPreference("setting_normal_functionlist");
        if (str.equals("0")) {
            this.lp.setSummary(getResources().getString(R.string.setting_qqlauncher_functionlist_matrixmode));
        } else {
            this.lp.setSummary(getResources().getString(R.string.setting_qqlauncher_functionlist_listmode));
        }
        if (str.equals("0")) {
            this.lp.setValueIndex(0);
        } else {
            this.lp.setValueIndex(1);
        }
        this.mListPreference = (ListPreference) getPreferenceScreen().findPreference("setting_normal_functionlist");
        this.backPreference = findPreference("setting_normal_backup");
        setLastBackupTime(this.backPreference);
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        String key = preference.getKey();
        if (key != null) {
            if (key.equals("setting_normal_screen")) {
                Intent intent = new Intent();
                intent.setClass(this, DesktopSwitchSettingActivity.class);
                startActivity(intent);
                finish();
            } else if (key.equals("setting_normal_backup")) {
                new AlertDialog.Builder(this).setTitle((int) R.string.setting_qqlauncher_backup).setItems((int) R.array.backup_launcher_values, new x(this)).create().show();
            } else if (key.equals("setting_normal_about")) {
                Intent intent2 = new Intent();
                intent2.setClass(this, AboutSettingActivity.class);
                startActivity(intent2);
                finish();
            } else if (key.equals("setting_normal_help")) {
                Intent intent3 = new Intent();
                intent3.setClass(this, HelpSettingActivity.class);
                startActivity(intent3);
                finish();
            } else if (key.equals("setting_normal_exit")) {
                ResolveInfo c = p.c(this);
                String packageName = getPackageName();
                if (c == null || !packageName.equals(c.activityInfo.packageName)) {
                    Intent intent4 = new Intent();
                    intent4.setClass(this, Launcher.class);
                    intent4.setFlags(67108864);
                    intent4.putExtra("flag", 1);
                    startActivity(intent4);
                } else {
                    new AlertDialog.Builder(this).setTitle((int) R.string.dlg_notice).setMessage((int) R.string.notice_clear_default).setPositiveButton((int) R.string.dlg_btn_clear, new z(this, packageName)).setNegativeButton((int) R.string.dlg_btn_noclear, new y(this)).create().show();
                }
            } else if (key.equals("setting_normal_lockdesktop")) {
                if (((CheckBoxPreference) preference).isChecked()) {
                    startService(new Intent(this, LockService.class));
                } else {
                    stopService(new Intent(this, LockService.class));
                    ((KeyguardManager) getSystemService("keyguard")).newKeyguardLock(getPackageName()).reenableKeyguard();
                }
            } else if (key.equals("setting_normal_feedback")) {
                if (!getResources().getConfiguration().locale.getLanguage().toLowerCase().contains("zh")) {
                    Intent intent5 = new Intent("android.intent.action.SEND");
                    intent5.putExtra("android.intent.extra.EMAIL", new String[]{"qqlauncher@tencent.com"});
                    intent5.putExtra("subject", "feedback");
                    intent5.setType("message/rfc882");
                    intent5.addFlags(268435456);
                    startActivity(intent5);
                } else if (p.b((Context) this)) {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse(feedBackUrl)));
                } else {
                    e eVar = new e(this);
                    eVar.a((int) R.string.network_tips_title);
                    eVar.b(getResources().getString(R.string.network_tips_content));
                    eVar.b();
                    eVar.a((int) R.string.confirm, new aa(this));
                    eVar.c().show();
                }
            } else if (key.equals("setting_normal_clear_default")) {
                handleClearDefault();
            } else if (key.equals("import_data")) {
                Launcher.showImportDataDialogForSettingActivity(this);
            }
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.homeConfig.b("setting_normal_functionlist", "");
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        if (str.equals("setting_normal_functionlist")) {
            this.homeConfig.b("setting_normal_functionlist", "");
            this.lp.setSummary(this.lp.getEntry());
        }
    }
}
