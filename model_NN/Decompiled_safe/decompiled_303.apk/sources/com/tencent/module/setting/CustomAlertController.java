package com.tencent.module.setting;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.tencent.a.a;
import com.tencent.qqlauncher.R;

public final class CustomAlertController {
    private View A;
    private boolean B;
    /* access modifiers changed from: private */
    public ListAdapter C;
    /* access modifiers changed from: private */
    public int D = -1;
    /* access modifiers changed from: private */
    public Handler E;
    private View.OnClickListener F = new h(this);
    /* access modifiers changed from: private */
    public final Context a;
    /* access modifiers changed from: private */
    public final DialogInterface b;
    private final Window c;
    private CharSequence d;
    private CharSequence e;
    /* access modifiers changed from: private */
    public ListView f;
    private View g;
    private int h;
    private int i;
    private int j;
    private int k;
    private boolean l = false;
    /* access modifiers changed from: private */
    public Button m;
    private CharSequence n;
    /* access modifiers changed from: private */
    public Message o;
    /* access modifiers changed from: private */
    public Button p;
    private CharSequence q;
    /* access modifiers changed from: private */
    public Message r;
    /* access modifiers changed from: private */
    public Button s;
    private CharSequence t;
    /* access modifiers changed from: private */
    public Message u;
    private ScrollView v;
    private int w = -1;
    private ImageView x;
    private TextView y;
    private TextView z;

    public class RecycleListView extends ListView {
        boolean a = true;

        public RecycleListView(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public RecycleListView(Context context, AttributeSet attributeSet, int i) {
            super(context, attributeSet, i);
        }
    }

    public CustomAlertController(Context context, DialogInterface dialogInterface, Window window) {
        this.a = context;
        this.b = dialogInterface;
        this.c = window;
        this.E = new ag(dialogInterface);
    }

    private void a(Button button) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) button.getLayoutParams();
        layoutParams.gravity = 1;
        layoutParams.weight = 0.5f;
        button.setLayoutParams(layoutParams);
        this.c.findViewById(R.id.leftSpacer).setVisibility(0);
        this.c.findViewById(R.id.rightSpacer).setVisibility(0);
    }

    private static boolean c(View view) {
        if (view.onCheckIsTextEditor()) {
            return true;
        }
        if (!(view instanceof ViewGroup)) {
            return false;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        int childCount = viewGroup.getChildCount();
        while (childCount > 0) {
            childCount--;
            if (c(viewGroup.getChildAt(childCount))) {
                return true;
            }
        }
        return false;
    }

    public final void a() {
        boolean z2;
        boolean z3;
        int i2;
        this.c.requestFeature(1);
        if (this.g == null || !c(this.g)) {
            this.c.setFlags(131072, 131072);
        }
        this.c.setContentView((int) R.layout.custom_alert_dialog);
        LinearLayout linearLayout = (LinearLayout) this.c.findViewById(R.id.contentPanel);
        this.v = (ScrollView) this.c.findViewById(R.id.scrollView);
        this.v.setFocusable(false);
        this.z = (TextView) this.c.findViewById(R.id.message);
        if (this.z != null) {
            if (this.e != null) {
                this.z.setText(this.e);
            } else {
                this.z.setVisibility(8);
                this.v.removeView(this.z);
                if (this.f != null) {
                    linearLayout.removeView(this.c.findViewById(R.id.scrollView));
                    linearLayout.addView(this.f, new LinearLayout.LayoutParams(-1, -1));
                    linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
                } else {
                    linearLayout.setVisibility(8);
                }
            }
        }
        boolean z4 = false;
        this.m = (Button) this.c.findViewById(R.id.button1);
        this.m.setOnClickListener(this.F);
        if (TextUtils.isEmpty(this.n)) {
            this.m.setVisibility(8);
        } else {
            this.m.setText(this.n);
            this.m.setVisibility(0);
            z4 = true;
        }
        this.p = (Button) this.c.findViewById(R.id.button2);
        this.p.setOnClickListener(this.F);
        if (TextUtils.isEmpty(this.q)) {
            this.p.setVisibility(8);
        } else {
            this.p.setText(this.q);
            this.p.setVisibility(0);
            z4 |= true;
        }
        this.s = (Button) this.c.findViewById(R.id.button3);
        this.s.setOnClickListener(this.F);
        if (TextUtils.isEmpty(this.t)) {
            this.s.setVisibility(8);
            z2 = z4;
        } else {
            this.s.setText(this.t);
            this.s.setVisibility(0);
            z2 = z4 | true;
        }
        if (z2) {
            a(this.m);
        } else if (z2) {
            a(this.s);
        } else if (z2) {
            a(this.s);
        }
        boolean z5 = z2;
        LinearLayout linearLayout2 = (LinearLayout) this.c.findViewById(R.id.topPanel);
        TypedArray obtainStyledAttributes = this.a.obtainStyledAttributes(null, a.c, R.attr.customAlertDialogStyle, 0);
        if (this.A != null) {
            linearLayout2.addView(this.A, new LinearLayout.LayoutParams(-1, -2));
            z3 = true;
        } else {
            if (!TextUtils.isEmpty(this.d)) {
                this.y = (TextView) this.c.findViewById(R.id.alertTitle);
                this.y.setText(this.d);
                z3 = true;
            } else {
                z3 = false;
            }
        }
        View findViewById = this.c.findViewById(R.id.buttonPanel);
        if (!z5) {
            findViewById.setVisibility(8);
            this.c.getAttributes().gravity = 81;
            this.c.getAttributes().windowAnimations = R.style.custommenudialog;
        } else {
            this.x = (ImageView) this.c.findViewById(R.id.button_divider);
        }
        FrameLayout frameLayout = null;
        if (this.g != null) {
            frameLayout = (FrameLayout) this.c.findViewById(R.id.customPanel);
            FrameLayout frameLayout2 = (FrameLayout) this.c.findViewById(R.id.custom);
            frameLayout2.addView(this.g, new ViewGroup.LayoutParams(-1, -1));
            if (this.l) {
                frameLayout2.setPadding(this.h, this.i, this.j, this.k);
            }
            if (this.f != null) {
                ((LinearLayout.LayoutParams) frameLayout.getLayoutParams()).weight = 0.0f;
            }
        } else {
            this.c.findViewById(R.id.customPanel).setVisibility(8);
        }
        View[] viewArr = new View[4];
        boolean[] zArr = new boolean[4];
        if (z3) {
            viewArr[0] = linearLayout2;
            zArr[0] = false;
            i2 = 0 + 1;
        } else {
            i2 = 0;
        }
        if (linearLayout.getVisibility() == 8) {
            linearLayout = null;
        }
        viewArr[i2] = linearLayout;
        zArr[i2] = this.f != null;
        int i3 = i2 + 1;
        if (frameLayout != null) {
            viewArr[i3] = frameLayout;
            zArr[i3] = this.B;
            i3++;
        }
        if (z5) {
            viewArr[i3] = findViewById;
            zArr[i3] = true;
        }
        if (!(this.f == null || this.C == null)) {
            this.f.setAdapter(this.C);
            if (this.D >= 0) {
                this.f.setItemChecked(this.D, true);
                this.f.setSelection(this.D);
            }
            if (this.d.toString().equals(this.a.getString(R.string.title_select_wallpaper))) {
                this.f.setOnItemClickListener(new g(this));
            }
        }
        obtainStyledAttributes.recycle();
    }

    public final void a(int i2, CharSequence charSequence, DialogInterface.OnClickListener onClickListener, Message message) {
        Message obtainMessage = onClickListener != null ? this.E.obtainMessage(i2, onClickListener) : message;
        switch (i2) {
            case -3:
                this.t = charSequence;
                this.u = obtainMessage;
                return;
            case -2:
                this.q = charSequence;
                this.r = obtainMessage;
                return;
            case -1:
                this.n = charSequence;
                this.o = obtainMessage;
                return;
            default:
                throw new IllegalArgumentException("Button does not exist");
        }
    }

    public final void a(View view) {
        this.A = view;
    }

    public final void a(View view, int i2, int i3, int i4, int i5) {
        this.g = view;
        this.l = true;
        this.h = i2;
        this.i = i3;
        this.j = i4;
        this.k = i5;
    }

    public final void a(CharSequence charSequence) {
        this.d = charSequence;
        if (this.y != null) {
            this.y.setText(charSequence);
        }
    }

    public final boolean a(KeyEvent keyEvent) {
        return this.v != null && this.v.executeKeyEvent(keyEvent);
    }

    public final void b() {
        if (this.x != null) {
            this.x.setVisibility(0);
        }
    }

    public final void b(View view) {
        this.g = view;
        this.l = false;
    }

    public final void b(CharSequence charSequence) {
        this.e = charSequence;
        if (this.z != null) {
            this.z.setText(charSequence);
        }
    }

    public final boolean b(KeyEvent keyEvent) {
        return this.v != null && this.v.executeKeyEvent(keyEvent);
    }

    public final void c() {
        this.B = true;
    }

    public final ListView d() {
        return this.f;
    }

    public final Button e() {
        if (this.o != null) {
            return this.m;
        }
        return null;
    }
}
