package com.tencent.module.setting;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import com.tencent.module.setting.CustomAlertController;
import com.tencent.qqlauncher.R;

final class m implements AdapterView.OnItemClickListener {
    private /* synthetic */ CustomAlertController.RecycleListView a;
    private /* synthetic */ CustomAlertController b;
    private /* synthetic */ a c;

    m(a aVar, CustomAlertController.RecycleListView recycleListView, CustomAlertController customAlertController) {
        this.c = aVar;
        this.a = recycleListView;
        this.b = customAlertController;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (this.c.o != null) {
            int i2 = 0;
            for (boolean z : this.c.o) {
                if (z) {
                    i2++;
                }
            }
            if (this.c.o[i] || this.c.x == -1 || i2 < this.c.x) {
                this.c.o[i] = this.a.isItemChecked(i);
                this.b.a(this.c.a.getString(R.string.add_application, Integer.valueOf(this.a.isItemChecked(i) ? i2 + 1 : i2 - 1), Integer.valueOf(this.c.x)));
            } else {
                this.a.setItemChecked(i, false);
                Toast.makeText(this.c.a, this.c.a.getString(R.string.custom_dialog_max_item, Integer.valueOf(this.c.x)), 0).show();
                return;
            }
        }
        this.c.s.onClick(this.b.b, i, this.a.isItemChecked(i));
    }
}
