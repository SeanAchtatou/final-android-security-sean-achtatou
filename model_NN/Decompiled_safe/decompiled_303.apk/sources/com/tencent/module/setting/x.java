package com.tencent.module.setting;

import android.app.AlertDialog;
import android.content.DialogInterface;
import com.tencent.qqlauncher.R;

final class x implements DialogInterface.OnClickListener {
    final /* synthetic */ SettingActivity a;

    x(SettingActivity settingActivity) {
        this.a = settingActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case 0:
                AlertDialog create = new AlertDialog.Builder(this.a).create();
                create.setTitle(this.a.getResources().getString(R.string.setting_backup_title));
                create.setMessage(this.a.getResources().getString(R.string.setting_backup_dialog_confirm));
                create.setButton(-1, this.a.getResources().getString(17039370), new t(this));
                create.setButton(-2, this.a.getResources().getString(17039360), new r(this));
                create.show();
                return;
            case 1:
                AlertDialog create2 = new AlertDialog.Builder(this.a).create();
                create2.setTitle(this.a.getResources().getString(R.string.setting_reset_title));
                create2.setMessage(this.a.getResources().getString(R.string.setting_reset_dialog_confirm));
                create2.setButton(-1, this.a.getResources().getString(17039370), new s(this));
                create2.setButton(-2, this.a.getResources().getString(17039360), new q(this));
                create2.show();
                return;
            default:
                return;
        }
    }
}
