package com.tencent.module.setting;

import android.content.Context;
import android.content.DialogInterface;
import android.widget.ListAdapter;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

public final class e {
    private final a a;

    public e(Context context) {
        this.a = new a(context);
    }

    public final e a() {
        this.a.d = this.a.a.getText(R.string.userfolder_close_title);
        return this;
    }

    public final e a(int i) {
        this.a.c = this.a.a.getText(i);
        return this;
    }

    public final e a(int i, DialogInterface.OnClickListener onClickListener) {
        this.a.e = this.a.a.getText(i);
        this.a.f = onClickListener;
        return this;
    }

    public final e a(DialogInterface.OnClickListener onClickListener) {
        this.a.l = this.a.a.getResources().getTextArray(R.array.app_icon_order_values);
        this.a.n = onClickListener;
        this.a.r = -1;
        this.a.q = true;
        return this;
    }

    public final e a(ListAdapter listAdapter, DialogInterface.OnClickListener onClickListener) {
        this.a.m = listAdapter;
        this.a.n = onClickListener;
        return this;
    }

    public final e a(CharSequence charSequence) {
        this.a.c = charSequence;
        return this;
    }

    public final e a(ArrayList arrayList, boolean[] zArr, int i, DialogInterface.OnMultiChoiceClickListener onMultiChoiceClickListener) {
        this.a.w = arrayList;
        this.a.s = onMultiChoiceClickListener;
        this.a.p = true;
        this.a.o = zArr;
        this.a.x = i;
        return this;
    }

    public final e b() {
        this.a.v = true;
        return this;
    }

    public final e b(int i, DialogInterface.OnClickListener onClickListener) {
        this.a.g = this.a.a.getText(i);
        this.a.h = onClickListener;
        return this;
    }

    public final e b(ListAdapter listAdapter, DialogInterface.OnClickListener onClickListener) {
        this.a.m = listAdapter;
        this.a.n = onClickListener;
        this.a.r = -1;
        this.a.q = true;
        return this;
    }

    public final e b(CharSequence charSequence) {
        this.a.d = charSequence;
        return this;
    }

    public final CustomAlertDialog c() {
        CustomAlertDialog customAlertDialog = new CustomAlertDialog(this.a.a, (byte) 0);
        this.a.a(customAlertDialog.a);
        customAlertDialog.setCancelable(this.a.i);
        customAlertDialog.setOnCancelListener(this.a.j);
        if (this.a.k != null) {
            customAlertDialog.setOnKeyListener(this.a.k);
        }
        return customAlertDialog;
    }
}
