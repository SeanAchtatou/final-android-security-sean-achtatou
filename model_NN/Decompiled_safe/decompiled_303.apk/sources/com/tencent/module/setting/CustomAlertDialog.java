package com.tencent.module.setting;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.ListView;
import com.tencent.qqlauncher.R;

public class CustomAlertDialog extends Dialog implements DialogInterface {
    /* access modifiers changed from: private */
    public CustomAlertController a;

    private CustomAlertDialog(Context context) {
        super(context, R.style.customAlertDialog);
        this.a = new CustomAlertController(context, this, getWindow());
    }

    protected CustomAlertDialog(Context context, byte b) {
        this(context);
    }

    public final Button a() {
        return this.a.e();
    }

    public final ListView b() {
        return this.a.d();
    }

    public final void c() {
        this.a.b();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setCanceledOnTouchOutside(true);
        this.a.a();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.a.a(keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.a.b(keyEvent)) {
            return true;
        }
        return super.onKeyUp(i, keyEvent);
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        this.a.a(charSequence);
    }
}
