package com.tencent.module.setting;

import android.os.Message;

final class ai extends Thread {
    private /* synthetic */ AboutSettingActivity a;

    ai(AboutSettingActivity aboutSettingActivity) {
        this.a = aboutSettingActivity;
    }

    public final void run() {
        if (AboutSettingActivity.getUpdateUrl()) {
            Message message = new Message();
            if (AboutSettingActivity.updateType == 0) {
                message.what = AboutSettingActivity.UPDATE_NONEED;
            } else {
                message.what = AboutSettingActivity.UPDATE_READY;
            }
            this.a.messageHandler.sendMessage(message);
            return;
        }
        Message message2 = new Message();
        message2.what = AboutSettingActivity.UPDATE_ERROR;
        this.a.messageHandler.sendMessage(message2);
    }
}
