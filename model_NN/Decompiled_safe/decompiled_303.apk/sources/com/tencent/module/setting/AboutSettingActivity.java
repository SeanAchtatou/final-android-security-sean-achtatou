package com.tencent.module.setting;

import KQQConfig.SDKUpgradeReq;
import KQQConfig.SDKUpgradeRes;
import KQQConfig.UpgradeInfo;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.view.KeyEvent;
import com.qq.jce.wup.UniPacket;
import com.tencent.launcher.home.i;
import com.tencent.qqlauncher.R;
import com.tencent.util.p;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class AboutSettingActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final int AUTOUPDATE = 2;
    public static final int CONFIG_CMD_SDK_CONFIG = 2;
    public static final int CONFIG_CMD_SDK_UPGRADE = 1;
    public static final int CONFIG_CMD_SERVERLIST_UPGRADE = 4;
    private static String HTTP_UPDATE_URL = "http://configqq.3g.qq.com/qqlauncher/upgrade.jsp";
    public static final int MANUALUPDATE = 1;
    private static final String MANUAL_UPDATE_URL = "http://119.147.14.245:14000";
    public static final int NOUPDATE = 0;
    public static final byte[] TEAkey = {-16, 68, 31, 95, -12, 45, -91, -113, -36, -9, -108, -102, -70, 98, -44, 17};
    public static final int UPDATE_ERROR = 802;
    public static final int UPDATE_NONEED = 801;
    public static final int UPDATE_READY = 800;
    public static final int launcherAPPID = 537032319;
    public static int updateType = 0;
    public static String updateUrl = "";
    public final i homeConfig = i.a();
    /* access modifiers changed from: private */
    public ProgressDialog m_pDialog;
    Handler messageHandler = new al(this);

    public static boolean getUpdateUrl() {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(HTTP_UPDATE_URL).openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setInstanceFollowRedirects(true);
            SDKUpgradeReq sDKUpgradeReq = new SDKUpgradeReq();
            sDKUpgradeReq.a = 1;
            sDKUpgradeReq.c = false;
            ArrayList arrayList = new ArrayList();
            arrayList.add("0");
            sDKUpgradeReq.b = arrayList;
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(Integer.valueOf((int) launcherAPPID));
            sDKUpgradeReq.d = arrayList2;
            UniPacket uniPacket = new UniPacket();
            uniPacket.setRequestId(1);
            uniPacket.setServantName("KQQConfig.SDKUpgradeReq");
            uniPacket.setEncodeName("utf-8");
            uniPacket.put("iCmdType", 1);
            uniPacket.setFuncName("SDKUpgradeReq");
            uniPacket.put("req", sDKUpgradeReq);
            ac acVar = new ac();
            byte[] b = acVar.b(uniPacket.encode(), TEAkey);
            DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
            dataOutputStream.write(b);
            dataOutputStream.flush();
            dataOutputStream.close();
            DataInputStream dataInputStream = new DataInputStream(httpURLConnection.getInputStream());
            int available = dataInputStream.available();
            if (available <= 0) {
                dataInputStream.close();
                httpURLConnection.disconnect();
                return false;
            }
            byte[] bArr = new byte[available];
            dataInputStream.read(bArr);
            byte[] a = acVar.a(bArr, TEAkey);
            if (a == null) {
                return false;
            }
            UniPacket uniPacket2 = new UniPacket();
            uniPacket2.setEncodeName("utf-8");
            uniPacket2.setFuncName("SDKUpgradeRes");
            uniPacket2.decode(a);
            SDKUpgradeRes sDKUpgradeRes = (SDKUpgradeRes) uniPacket2.getByClass("res", new SDKUpgradeRes());
            int a2 = ((UpgradeInfo) sDKUpgradeRes.a.get(0)).a();
            updateType = a2;
            if (a2 != 0) {
                updateUrl = ((UpgradeInfo) sDKUpgradeRes.a.get(0)).b();
            }
            dataInputStream.close();
            httpURLConnection.disconnect();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getPreferenceManager().setSharedPreferencesName("home_config");
        addPreferencesFromResource(R.xml.qqlaunchersetting_about);
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || keyEvent.getRepeatCount() != 0) {
            return false;
        }
        Intent intent = new Intent();
        intent.setClass(this, SettingActivity.class);
        startActivity(intent);
        finish();
        return false;
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        String key = preference.getKey();
        if (key != null) {
            if (key.equals("setting_about_updateVersion")) {
                this.m_pDialog = new ProgressDialog(this);
                this.m_pDialog.setProgressStyle(0);
                this.m_pDialog.setTitle(getString(R.string.update_software_title));
                this.m_pDialog.setMessage(getString(R.string.update_software_message));
                this.m_pDialog.setIndeterminate(false);
                this.m_pDialog.setCancelable(true);
                this.m_pDialog.setButton(getString(R.string.update_software_cancel), new ak(this));
                this.m_pDialog.show();
                new ai(this).start();
            } else if (key.equals("setting_about_recommendShare")) {
                Intent intent = new Intent("android.intent.action.SEND");
                intent.putExtra("android.intent.extra.SUBJECT", getString(R.string.about_recommendShare));
                intent.putExtra("android.intent.extra.TEXT", getString(R.string.setting_qqlauncher_share));
                intent.setType("text/plain");
                startActivity(Intent.createChooser(intent, getTitle()));
            } else if (key.equals("setting_about_softwareRemark")) {
                if (p.b((Context) this)) {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse(getString(R.string.about_softwareRemark_url))));
                } else {
                    e eVar = new e(this);
                    eVar.a((int) R.string.network_tips_title);
                    eVar.b(getResources().getString(R.string.network_tips_content));
                    eVar.b();
                    eVar.a((int) R.string.confirm, new aj(this));
                    eVar.c().show();
                }
            } else if (key.equals("setting_about_copyrights")) {
                Intent intent2 = new Intent();
                intent2.setClass(this, CopyrightSettingActivity.class);
                startActivity(intent2);
            }
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
    }
}
