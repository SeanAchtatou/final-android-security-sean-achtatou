package com.tencent.module.setting;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.tencent.module.setting.CustomAlertController;
import java.util.ArrayList;

final class k extends d {
    private /* synthetic */ CustomAlertController.RecycleListView a;
    private /* synthetic */ a b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    k(a aVar, Context context, ArrayList arrayList, CustomAlertController.RecycleListView recycleListView) {
        super(aVar, context, arrayList);
        this.b = aVar;
        this.a = recycleListView;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View view2 = super.getView(i, view, viewGroup);
        if (this.b.o != null && this.b.o[i]) {
            this.a.setItemChecked(i, true);
        }
        return view2;
    }
}
