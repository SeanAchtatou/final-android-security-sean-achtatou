package com.tencent.module.setting;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import com.tencent.module.setting.CustomAlertController;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

public final class a {
    private CharSequence A;
    private DialogInterface.OnClickListener B;
    private View C;
    private int D;
    private int E;
    private int F;
    private int G;
    private boolean H = false;
    private Cursor I;
    private AdapterView.OnItemSelectedListener J;
    private boolean K = true;
    public final Context a;
    public final LayoutInflater b;
    public CharSequence c;
    public CharSequence d;
    public CharSequence e;
    public DialogInterface.OnClickListener f;
    public CharSequence g;
    public DialogInterface.OnClickListener h;
    public boolean i;
    public DialogInterface.OnCancelListener j;
    public DialogInterface.OnKeyListener k;
    public CharSequence[] l;
    public ListAdapter m;
    public DialogInterface.OnClickListener n;
    public boolean[] o;
    public boolean p;
    public boolean q;
    public int r = -1;
    public DialogInterface.OnMultiChoiceClickListener s;
    public String t;
    public String u;
    public boolean v;
    public ArrayList w;
    public int x = -1;
    private int y = -1;
    private View z;

    public a(Context context) {
        this.a = context;
        this.i = true;
        this.b = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    public final void a(CustomAlertController customAlertController) {
        ListAdapter arrayAdapter;
        if (this.z != null) {
            customAlertController.a(this.z);
        } else if (this.c != null) {
            customAlertController.a(this.c);
        }
        if (this.d != null) {
            customAlertController.b(this.d);
        }
        if (this.e != null) {
            customAlertController.a(-1, this.e, this.f, null);
        }
        if (this.g != null) {
            customAlertController.a(-2, this.g, this.h, null);
        }
        if (this.A != null) {
            customAlertController.a(-3, this.A, this.B, null);
        }
        if (this.v) {
            customAlertController.c();
        }
        if (!(this.l == null && this.I == null && this.m == null && this.w == null)) {
            CustomAlertController.RecycleListView recycleListView = (CustomAlertController.RecycleListView) this.b.inflate((int) R.layout.select_dialog, (ViewGroup) null);
            if (this.p) {
                arrayAdapter = this.I == null ? this.w == null ? new l(this, this.a, this.l, recycleListView) : new k(this, this.a, this.w, recycleListView) : new j(this, this.a, this.I, recycleListView);
            } else {
                int i2 = this.q ? R.layout.select_dialog_singlechoice : R.layout.select_dialog_item;
                arrayAdapter = this.I == null ? this.m != null ? this.m : new ArrayAdapter(this.a, i2, (int) R.id.text1, this.l) : new SimpleCursorAdapter(this.a, i2, this.I, new String[]{this.t}, new int[]{R.id.text1});
            }
            ListAdapter unused = customAlertController.C = arrayAdapter;
            int unused2 = customAlertController.D = this.r;
            if (this.n != null) {
                recycleListView.setOnItemClickListener(new i(this, customAlertController));
            } else if (this.s != null) {
                recycleListView.setOnItemClickListener(new m(this, recycleListView, customAlertController));
            }
            if (this.J != null) {
                recycleListView.setOnItemSelectedListener(this.J);
            }
            if (this.q) {
                recycleListView.setChoiceMode(1);
            } else if (this.p) {
                recycleListView.setChoiceMode(2);
            }
            recycleListView.a = this.K;
            ListView unused3 = customAlertController.f = recycleListView;
        }
        if (this.C == null) {
            return;
        }
        if (this.H) {
            customAlertController.a(this.C, this.D, this.E, this.F, this.G);
        } else {
            customAlertController.b(this.C);
        }
    }
}
