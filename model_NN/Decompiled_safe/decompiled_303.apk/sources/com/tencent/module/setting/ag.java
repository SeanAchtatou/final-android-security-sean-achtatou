package com.tencent.module.setting;

import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import java.lang.ref.WeakReference;

final class ag extends Handler {
    private WeakReference a;

    public ag(DialogInterface dialogInterface) {
        this.a = new WeakReference(dialogInterface);
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case -3:
            case -2:
            case -1:
                ((DialogInterface.OnClickListener) message.obj).onClick((DialogInterface) this.a.get(), message.what);
                return;
            case 0:
            default:
                return;
            case 1:
                ((DialogInterface) message.obj).dismiss();
                return;
        }
    }
}
