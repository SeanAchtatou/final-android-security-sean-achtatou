package com.tencent.module.setting;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

final class g implements AdapterView.OnItemClickListener {
    private /* synthetic */ CustomAlertController a;

    g(CustomAlertController customAlertController) {
        this.a = customAlertController;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Object item = this.a.C.getItem(i);
        this.a.C.getClass();
        new Intent().setClass(this.a.a, item.getClass());
        ((CustomActivityPicker) this.a.a).showWallPaper(i);
    }
}
