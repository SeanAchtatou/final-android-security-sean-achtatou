package com.tencent.module.setting;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import com.tencent.qqlauncher.R;

final class b implements AdapterView.OnItemClickListener {
    private /* synthetic */ LockScreenSettingActivity a;

    b(LockScreenSettingActivity lockScreenSettingActivity) {
        this.a = lockScreenSettingActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        switch (i) {
            case 0:
                Toast.makeText(this.a.getApplicationContext(), "nEnableLockScreenSetting" + this.a.getString(R.string.switch_specialeffect_unimplement), 0).show();
                return;
            case 1:
                Toast.makeText(this.a.getApplicationContext(), "nUnlockMethodSetting" + this.a.getString(R.string.switch_specialeffect_unimplement), 0).show();
                return;
            case 2:
                Toast.makeText(this.a.getApplicationContext(), "nUnlockAnimationSetting" + this.a.getString(R.string.switch_specialeffect_unimplement), 0).show();
                return;
            case 3:
                Toast.makeText(this.a.getApplicationContext(), "nEnableAudioEffectSetting" + this.a.getString(R.string.switch_specialeffect_unimplement), 0).show();
                return;
            default:
                return;
        }
    }
}
