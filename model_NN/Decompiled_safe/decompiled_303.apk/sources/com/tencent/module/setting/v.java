package com.tencent.module.setting;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import com.tencent.qqlauncher.R;

final class v implements AdapterView.OnItemSelectedListener {
    private /* synthetic */ DesktopSwitchSpecialEffectSettingActivity a;

    v(DesktopSwitchSpecialEffectSettingActivity desktopSwitchSpecialEffectSettingActivity) {
        this.a = desktopSwitchSpecialEffectSettingActivity;
    }

    public final void onItemSelected(AdapterView adapterView, View view, int i, long j) {
        Toast.makeText(this.a.getApplicationContext(), this.a.getString(R.string.switch_specialeffect_select) + Long.toString(adapterView.getSelectedItemId()), 0).show();
    }

    public final void onNothingSelected(AdapterView adapterView) {
    }
}
