package com.tencent.module.setting;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import com.tencent.launcher.base.a;
import com.tencent.qqlauncher.R;

public abstract class CustomAlertActivity extends Activity implements DialogInterface {
    protected CustomAlertController mAlert;
    protected a mAlertParams;

    private boolean isOutOfBounds(MotionEvent motionEvent) {
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        View findViewById = getWindow().findViewById(R.id.parentPanel);
        findViewById.invalidate();
        return x < 0 || y < 0 || x > findViewById.getWidth() + 0 || y > findViewById.getHeight() + 0;
    }

    public void cancel() {
        finish();
    }

    public void dismiss() {
        if (!isFinishing()) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a.a.incrementAndGet();
        this.mAlert = new CustomAlertController(this, this, getWindow());
        this.mAlertParams = new a(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        a.a.decrementAndGet();
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.mAlert.a(keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.mAlert.b(keyEvent)) {
            return true;
        }
        return super.onKeyUp(i, keyEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() != 0 || !isOutOfBounds(motionEvent)) {
            return false;
        }
        cancel();
        return true;
    }

    /* access modifiers changed from: protected */
    public void setupAlert() {
        this.mAlertParams.a(this.mAlert);
        this.mAlert.a();
    }
}
