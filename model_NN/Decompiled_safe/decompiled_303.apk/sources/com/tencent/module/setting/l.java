package com.tencent.module.setting;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.tencent.module.setting.CustomAlertController;
import com.tencent.qqlauncher.R;

final class l extends ArrayAdapter {
    private /* synthetic */ CustomAlertController.RecycleListView a;
    private /* synthetic */ a b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    l(a aVar, Context context, CharSequence[] charSequenceArr, CustomAlertController.RecycleListView recycleListView) {
        super(context, (int) R.layout.select_dialog_multichoice, (int) R.id.text1, charSequenceArr);
        this.b = aVar;
        this.a = recycleListView;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View view2 = super.getView(i, view, viewGroup);
        if (this.b.o != null && this.b.o[i]) {
            this.a.setItemChecked(i, true);
        }
        return view2;
    }
}
