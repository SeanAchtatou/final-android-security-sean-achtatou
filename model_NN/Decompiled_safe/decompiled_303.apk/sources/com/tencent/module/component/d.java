package com.tencent.module.component;

import android.view.View;
import com.tencent.launcher.Launcher;
import com.tencent.widget.f;

final class d implements View.OnClickListener {
    private /* synthetic */ l a;
    private /* synthetic */ f b;
    private /* synthetic */ TaskActivity c;

    d(TaskActivity taskActivity, l lVar, f fVar) {
        this.c = taskActivity;
        this.a = lVar;
        this.b = fVar;
    }

    public final void onClick(View view) {
        TaskActivity.access$100(this.c, this.a);
        int access$200 = ((int) this.c.getAvailMemory()) / Launcher.APPWIDGET_HOST_ID;
        boolean unused = this.c.bFlag = true;
        this.c.onMemoryUpdate((long) this.c.nTotalMemory, (long) access$200);
        this.b.dismiss();
    }
}
