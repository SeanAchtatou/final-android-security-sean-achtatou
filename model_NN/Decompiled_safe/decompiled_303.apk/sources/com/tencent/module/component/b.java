package com.tencent.module.component;

import android.view.View;
import android.widget.ImageView;
import com.tencent.launcher.base.BaseApp;
import com.tencent.qqlauncher.R;
import com.tencent.widget.f;

final class b implements View.OnClickListener {
    private /* synthetic */ View a;
    private /* synthetic */ ap b;
    private /* synthetic */ f c;
    private /* synthetic */ TaskActivity d;

    b(TaskActivity taskActivity, View view, ap apVar, f fVar) {
        this.d = taskActivity;
        this.a = view;
        this.b = apVar;
        this.c = fVar;
    }

    public final void onClick(View view) {
        ap apVar = this.b;
        this.d.getContentResolver().delete(TaskComponentProvider.a, "packageName=?", new String[]{apVar.b});
        ((ImageView) this.a.findViewById(R.id.task_exclude)).setVisibility(8);
        BaseApp.a(this.d.getString(R.string.task_remove_exclude));
        this.c.dismiss();
    }
}
