package com.tencent.module.component;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

final class ac extends SQLiteOpenHelper {
    public ac(Context context) {
        super(context, "taskcomponent.db", (SQLiteDatabase.CursorFactory) null, 5);
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE taskcomponent (_id INTEGER PRIMARY KEY,packageName TEXT);");
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }
}
