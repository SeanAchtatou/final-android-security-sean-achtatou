package com.tencent.module.component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

final class r implements Runnable {
    private /* synthetic */ Process a;
    private /* synthetic */ HashMap b;
    private /* synthetic */ ag c;

    r(ag agVar, Process process, HashMap hashMap) {
        this.c = agVar;
        this.a = process;
        this.b = hashMap;
    }

    public final void run() {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.a.getInputStream()), 8192);
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    try {
                        bufferedReader.close();
                        return;
                    } catch (IOException e) {
                        return;
                    }
                } else if (!readLine.contains("root")) {
                    String[] split = readLine.split("[\\s]+");
                    if (split.length == 9) {
                        this.b.put(Integer.valueOf((int) ag.a(split[1])), Integer.valueOf((int) ag.a(split[4])));
                    }
                }
            } catch (IOException e2) {
                e2.printStackTrace();
                try {
                    bufferedReader.close();
                    return;
                } catch (IOException e3) {
                    return;
                }
            } catch (Throwable th) {
                try {
                    bufferedReader.close();
                } catch (IOException e4) {
                }
                throw th;
            }
        }
    }
}
