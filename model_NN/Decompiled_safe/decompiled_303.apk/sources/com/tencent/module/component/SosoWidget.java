package com.tencent.module.component;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import com.tencent.qphone.widget.soso.SearchResultActivity;
import com.tencent.qphone.widget.soso.SosoSearchMainActivity;
import com.tencent.qqlauncher.R;
import com.tencent.widget.WidgetFrame;
import java.util.ArrayList;

public class SosoWidget extends WidgetFrame implements View.OnClickListener {
    private static boolean l = true;
    /* access modifiers changed from: private */
    public static ArrayList m;
    private final boolean a = false;
    private final String b = "SosoWidget";
    private final String c = "http://wap.soso.com/top10/index.jsp?g_f=2158&g_ut=2&channel=soso_kw_newsword";
    private Context d;
    private View e;
    private TextView f;
    private TextView g;
    private TextView h;
    private TextView i;
    private TextView j;
    private TextView k;
    private BroadcastReceiver n;

    public SosoWidget(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.d = context;
        l = this.d.getSharedPreferences("com.tencent.qphone.widget.soso_preferences", 1).getBoolean("hot_words", true);
        this.n = new t(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.tencent.qqlauncher.widget.soso.GLOBAL_SEARCH");
        intentFilter.addAction("com.tencent.qqlauncher.widget.soso.SEND_HOT");
        intentFilter.addAction("com.tencent.qqlauncher.widget.soso_hot");
        this.d.registerReceiver(this.n, intentFilter);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x004b, code lost:
        r0 = "";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0045, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0046, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0045 A[ExcHandler: UnsupportedEncodingException (r0v11 'e' java.io.UnsupportedEncodingException A[CUSTOM_DECLARE]), Splitter:B:1:0x0002] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(int r6) {
        /*
            r5 = this;
            java.lang.String r1 = ""
            java.util.ArrayList r0 = com.tencent.module.component.SosoWidget.m     // Catch:{ UnsupportedEncodingException -> 0x0045, ActivityNotFoundException -> 0x004a }
            int r2 = r0.size()     // Catch:{ UnsupportedEncodingException -> 0x0045, ActivityNotFoundException -> 0x004a }
            if (r6 < r2) goto L_0x000b
        L_0x000a:
            return
        L_0x000b:
            java.lang.Object r0 = r0.get(r6)     // Catch:{ UnsupportedEncodingException -> 0x0045, ActivityNotFoundException -> 0x004a }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ UnsupportedEncodingException -> 0x0045, ActivityNotFoundException -> 0x004a }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x0045, ActivityNotFoundException -> 0x008e }
            r1.<init>()     // Catch:{ UnsupportedEncodingException -> 0x0045, ActivityNotFoundException -> 0x008e }
            java.lang.String r2 = "http://wap.soso.com/s.q?type=sweb&st=input&g_f=5352&g_ut=3&biz=widget&key="
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ UnsupportedEncodingException -> 0x0045, ActivityNotFoundException -> 0x008e }
            java.lang.String r2 = "UTF-8"
            java.lang.String r2 = java.net.URLEncoder.encode(r0, r2)     // Catch:{ UnsupportedEncodingException -> 0x0045, ActivityNotFoundException -> 0x008e }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ UnsupportedEncodingException -> 0x0045, ActivityNotFoundException -> 0x008e }
            java.lang.String r1 = r1.toString()     // Catch:{ UnsupportedEncodingException -> 0x0045, ActivityNotFoundException -> 0x008e }
            android.net.Uri r1 = android.net.Uri.parse(r1)     // Catch:{ UnsupportedEncodingException -> 0x0045, ActivityNotFoundException -> 0x008e }
            android.content.Intent r2 = new android.content.Intent     // Catch:{ UnsupportedEncodingException -> 0x0045, ActivityNotFoundException -> 0x008e }
            android.content.Context r3 = r5.d     // Catch:{ UnsupportedEncodingException -> 0x0045, ActivityNotFoundException -> 0x008e }
            java.lang.Class<com.tencent.qphone.widget.soso.SearchResultActivity> r4 = com.tencent.qphone.widget.soso.SearchResultActivity.class
            r2.<init>(r3, r4)     // Catch:{ UnsupportedEncodingException -> 0x0045, ActivityNotFoundException -> 0x008e }
            r2.setData(r1)     // Catch:{ UnsupportedEncodingException -> 0x0045, ActivityNotFoundException -> 0x008e }
            r1 = 335544320(0x14000000, float:6.4623485E-27)
            r2.setFlags(r1)     // Catch:{ UnsupportedEncodingException -> 0x0045, ActivityNotFoundException -> 0x008e }
            android.content.Context r1 = r5.d     // Catch:{ UnsupportedEncodingException -> 0x0045, ActivityNotFoundException -> 0x008e }
            r1.startActivity(r2)     // Catch:{ UnsupportedEncodingException -> 0x0045, ActivityNotFoundException -> 0x008e }
            goto L_0x000a
        L_0x0045:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x000a
        L_0x004a:
            r0 = move-exception
            r0 = r1
        L_0x004c:
            android.content.Intent r1 = new android.content.Intent
            r1.<init>()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x0088 }
            r2.<init>()     // Catch:{ UnsupportedEncodingException -> 0x0088 }
            java.lang.String r3 = "http://wap.soso.com/s.q?type=sweb&st=input&g_f=5352&g_ut=3&biz=widget&key="
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x0088 }
            java.lang.String r3 = "UTF-8"
            java.lang.String r0 = java.net.URLEncoder.encode(r0, r3)     // Catch:{ UnsupportedEncodingException -> 0x0088 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x0088 }
            java.lang.String r0 = r0.toString()     // Catch:{ UnsupportedEncodingException -> 0x0088 }
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch:{ UnsupportedEncodingException -> 0x0088 }
            java.lang.String r2 = "android.intent.action.VIEW"
            r1.setAction(r2)     // Catch:{ UnsupportedEncodingException -> 0x0088 }
            r1.setData(r0)     // Catch:{ UnsupportedEncodingException -> 0x0088 }
            java.lang.String r0 = "com.android.browser"
            java.lang.String r2 = "com.android.browser.BrowserActivity"
            r1.setClassName(r0, r2)     // Catch:{ UnsupportedEncodingException -> 0x0088 }
            r0 = 268435456(0x10000000, float:2.5243549E-29)
            r1.setFlags(r0)     // Catch:{ UnsupportedEncodingException -> 0x0088 }
            android.content.Context r0 = r5.d     // Catch:{ UnsupportedEncodingException -> 0x0088 }
            r0.startActivity(r1)     // Catch:{ UnsupportedEncodingException -> 0x0088 }
            goto L_0x000a
        L_0x0088:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x000a
        L_0x008e:
            r1 = move-exception
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.module.component.SosoWidget.a(int):void");
    }

    static /* synthetic */ void a(SosoWidget sosoWidget) {
        if (l) {
            ArrayList arrayList = m;
            sosoWidget.findViewById(R.id.search_hot_text).setVisibility(0);
            if (arrayList != null && arrayList.size() > 0) {
                sosoWidget.f.setText("");
                sosoWidget.g.setText("");
                sosoWidget.h.setText("");
                sosoWidget.i.setText("");
                sosoWidget.j.setText("");
                int i2 = 0;
                while (i2 < arrayList.size() && i2 < 5) {
                    String str = (String) arrayList.get(i2);
                    switch (i2) {
                        case 0:
                            sosoWidget.f.setText(str);
                            break;
                        case 1:
                            sosoWidget.g.setText(str);
                            break;
                        case 2:
                            sosoWidget.h.setText(str);
                            break;
                        case 3:
                            sosoWidget.i.setText(str);
                            break;
                        case 4:
                            sosoWidget.j.setText(str);
                            break;
                    }
                    i2++;
                }
                return;
            }
            return;
        }
        sosoWidget.findViewById(R.id.search_hot_text).setVisibility(8);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.search_bkg_layout /*2131493103*/:
                Intent intent = new Intent();
                intent.setClass(this.d, SosoSearchMainActivity.class);
                intent.setFlags(268435456);
                this.d.startActivity(intent);
                return;
            case R.id.t1 /*2131493313*/:
                a(0);
                return;
            case R.id.t2 /*2131493315*/:
                a(1);
                return;
            case R.id.t3 /*2131493317*/:
                a(2);
                return;
            case R.id.t4 /*2131493319*/:
                a(3);
                return;
            case R.id.t5 /*2131493321*/:
                a(4);
                return;
            case R.id.more_key /*2131493322*/:
                Uri parse = Uri.parse("http://wap.soso.com/top10/index.jsp?g_f=2158&g_ut=2&channel=soso_kw_newsword");
                try {
                    Intent intent2 = new Intent(this.d, SearchResultActivity.class);
                    intent2.setData(parse);
                    intent2.setFlags(335544320);
                    this.d.startActivity(intent2);
                    return;
                } catch (ActivityNotFoundException e2) {
                    try {
                        Intent intent3 = new Intent();
                        intent3.setAction("android.intent.action.VIEW");
                        intent3.setData(parse);
                        intent3.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
                        intent3.setFlags(268435456);
                        this.d.startActivity(intent3);
                        return;
                    } catch (ActivityNotFoundException e3) {
                        this.d.startActivity(new Intent("android.intent.action.VIEW", parse));
                        return;
                    }
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.e = findViewById(R.id.search_bkg_layout);
        this.f = (TextView) findViewById(R.id.t1);
        this.g = (TextView) findViewById(R.id.t2);
        this.h = (TextView) findViewById(R.id.t3);
        this.i = (TextView) findViewById(R.id.t4);
        this.j = (TextView) findViewById(R.id.t5);
        this.k = (TextView) findViewById(R.id.more_key);
        this.e.setOnClickListener(this);
        this.f.setOnClickListener(this);
        this.g.setOnClickListener(this);
        this.h.setOnClickListener(this);
        this.i.setOnClickListener(this);
        this.j.setOnClickListener(this);
        this.k.setOnClickListener(this);
        if (this.d != null) {
            this.d.startService(new Intent("com.tencent.qqlauncher.widget.soso.SOSO_SERVICE"));
        }
        super.onFinishInflate();
    }
}
