package com.tencent.module.component;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ProgressBar;
import com.tencent.launcher.BrightnessActivity;
import com.tencent.launcher.base.b;
import com.tencent.qqlauncher.R;

public class CustomProgressBar extends ProgressBar {
    private String a;
    private Paint b;
    private int c = 0;
    private int d;
    private int e;
    private boolean f = false;

    public CustomProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public CustomProgressBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        System.out.println("2");
        a();
    }

    private void a() {
        this.b = new Paint();
        if (this.e == 1) {
            this.b.setColor(getResources().getColor(R.color.progress_text_color));
        } else if (this.e == 0) {
            this.b.setColor(getResources().getColor(R.color.progress_text_color));
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.c == 0) {
            this.c = getMax();
        }
        Rect rect = new Rect();
        if (this.e == 1) {
            this.b.getTextBounds(this.a, 0, this.a.length(), rect);
            canvas.drawText(this.a, (float) ((((this.d * getWidth()) / getMax()) - ((int) this.b.measureText(this.a))) - 10), (float) ((getHeight() / 2) - rect.centerY()), this.b);
        } else {
            this.b.getTextBounds(this.a, 0, this.a.length(), rect);
            int indexOf = this.a.indexOf("/");
            String substring = this.a.substring(0, indexOf);
            String substring2 = this.a.substring(indexOf + 1, this.a.length());
            this.b.setAntiAlias(true);
            this.b.setARGB(BrightnessActivity.MAXIMUM_BACKLIGHT, 230, 242, 217);
            this.b.setTextSize(16.0f * b.b);
            int height = ((getHeight() / 2) - rect.centerY()) + 2;
            canvas.drawText(substring, (float) ((((this.d * getWidth()) / getMax()) - ((int) this.b.measureText(substring))) - 10), (float) height, this.b);
            this.b.setAntiAlias(true);
            this.b.setARGB(BrightnessActivity.MAXIMUM_BACKLIGHT, 177, 184, 190);
            this.b.setTextSize(11.0f * b.b);
            int measureText = (int) this.b.measureText(substring2);
            int width = (getWidth() - measureText) - 10;
            if (((((this.c - this.d) * getWidth()) / getMax()) - measureText) - 10 > 0) {
                canvas.drawText(substring2, (float) width, (float) height, this.b);
            } else {
                int width2 = ((this.d * getWidth()) / getMax()) + 5;
                int width3 = ((this.c - this.d) * getWidth()) / getMax();
                String str = substring2;
                for (int i = 0; i < substring2.length(); i++) {
                    str = substring2.substring(i);
                    if (this.b.measureText(str) < ((float) width3)) {
                        break;
                    }
                }
                canvas.drawText(str, (float) width2, (float) height, this.b);
            }
        }
    }

    public synchronized void setProgress(int i) {
        this.d = i;
        this.a = Integer.valueOf(i) + "M/" + Integer.valueOf(getMax()) + "M";
        super.setProgress(i);
    }
}
