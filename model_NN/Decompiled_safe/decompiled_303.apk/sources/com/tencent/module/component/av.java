package com.tencent.module.component;

import android.os.Handler;
import android.os.Message;
import com.tencent.launcher.SearchAppTestActivity;

final class av extends Handler {
    private /* synthetic */ SwitcherActivity a;

    av(SwitcherActivity switcherActivity) {
        this.a = switcherActivity;
    }

    public final void handleMessage(Message message) {
        boolean unused = this.a.inSetBrightness = true;
        switch (message.what) {
            case SearchAppTestActivity.TAB_TYPE_NET:
                this.a.setBrightness(message.arg1);
                break;
        }
        boolean unused2 = this.a.inSetBrightness = false;
    }
}
