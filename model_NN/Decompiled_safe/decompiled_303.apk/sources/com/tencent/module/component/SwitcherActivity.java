package com.tencent.module.component;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.view.View;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.SeekBar;
import android.widget.TextView;
import com.tencent.launcher.base.BaseApp;
import com.tencent.module.switcher.aa;
import com.tencent.module.switcher.ab;
import com.tencent.module.switcher.ac;
import com.tencent.module.switcher.ad;
import com.tencent.module.switcher.af;
import com.tencent.module.switcher.aj;
import com.tencent.module.switcher.f;
import com.tencent.module.switcher.i;
import com.tencent.module.switcher.k;
import com.tencent.module.switcher.l;
import com.tencent.module.switcher.v;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

public class SwitcherActivity extends Activity implements View.OnLongClickListener, SeekBar.OnSeekBarChangeListener {
    private static final int SCREEN_BRIGHTNESS = 100;
    /* access modifiers changed from: private */
    public ab airplaneSwitcher;
    /* access modifiers changed from: private */
    public TextView airplaneTextView;
    /* access modifiers changed from: private */
    public f bluetoothSwitcher;
    /* access modifiers changed from: private */
    public TextView bluetoothTextView;
    /* access modifiers changed from: private */
    public i brightnessSwitcher;
    /* access modifiers changed from: private */
    public TextView brightnessTextView;
    /* access modifiers changed from: private */
    public af gprsSwitcher;
    private TextView gprsTextView;
    /* access modifiers changed from: private */
    public l gpsSwitcher;
    /* access modifiers changed from: private */
    public TextView gpsTextView;
    /* access modifiers changed from: private */
    public ad grayitySwitcher;
    /* access modifiers changed from: private */
    public TextView grayityTextView;
    private ArrayList gridViewItem = new ArrayList();
    private Handler handler = new av(this);
    /* access modifiers changed from: private */
    public boolean inSetBrightness = false;
    /* access modifiers changed from: private */
    public boolean isSetGps = false;
    /* access modifiers changed from: private */
    public Context mContext;
    private GridView mGridView;
    private SeekBar mSeekBar;
    private k syncSwitcher;
    private TextView syncTextView;
    /* access modifiers changed from: private */
    public aj vibrateSwitcher;
    /* access modifiers changed from: private */
    public TextView vibrateTextView;
    /* access modifiers changed from: private */
    public aa wifiSwitcher;
    /* access modifiers changed from: private */
    public TextView wifiTextView;

    private void initSwitchers() {
        this.airplaneTextView = (TextView) findViewById(R.id.sw_item_airplane);
        this.airplaneSwitcher = (ab) v.b(this, 7);
        this.airplaneTextView.setOnLongClickListener(this);
        this.airplaneTextView.setTag(this.airplaneSwitcher);
        setAriplaneIcon(this.airplaneSwitcher, this.airplaneTextView, false);
        this.airplaneTextView.setText(getString(R.string.airplane_switcher_toast));
        this.airplaneTextView.setOnClickListener(new au(this));
        this.vibrateTextView = (TextView) findViewById(R.id.sw_item_vibrate);
        this.vibrateSwitcher = (aj) v.b(this, 5);
        this.vibrateTextView.setOnLongClickListener(this);
        this.vibrateTextView.setTag(this.vibrateSwitcher);
        setVibrateIcon(this.vibrateSwitcher, this.vibrateTextView, false);
        this.vibrateTextView.setText(getString(R.string.vibrate_switcher_toast));
        this.vibrateTextView.setOnClickListener(new ar(this));
        this.grayityTextView = (TextView) findViewById(R.id.sw_item_grayity);
        this.grayitySwitcher = (ad) v.b(this, 6);
        this.grayityTextView.setOnLongClickListener(this);
        this.grayityTextView.setTag(this.grayitySwitcher);
        setGrayityIcon(this.grayitySwitcher, this.grayityTextView, false);
        this.grayityTextView.setText(getString(R.string.grayity_switcher_toast));
        this.grayityTextView.setOnClickListener(new aq(this));
        this.brightnessTextView = (TextView) findViewById(R.id.sw_item_brightness);
        this.brightnessSwitcher = (i) v.b(this, 4);
        this.brightnessTextView.setOnLongClickListener(this);
        this.brightnessTextView.setTag(this.brightnessSwitcher);
        setBrightnessIcon(this.brightnessSwitcher, this.brightnessTextView, false);
        this.brightnessTextView.setText(getString(R.string.brightness_switcher_toast));
        this.brightnessTextView.setOnClickListener(new at(this));
        this.wifiTextView = (TextView) findViewById(R.id.sw_item_wifi);
        this.wifiSwitcher = (aa) v.b(this, 0);
        this.wifiTextView.setOnLongClickListener(this);
        this.wifiTextView.setTag(this.wifiSwitcher);
        setWifiIcon(this.wifiSwitcher, this.wifiTextView, false);
        this.wifiTextView.setText(getString(R.string.wifi_switcher_toast));
        this.wifiTextView.setOnClickListener(new as(this));
        this.bluetoothTextView = (TextView) findViewById(R.id.sw_item_bluetooth);
        this.bluetoothSwitcher = (f) v.b(this, 1);
        this.bluetoothTextView.setOnLongClickListener(this);
        this.bluetoothTextView.setTag(this.bluetoothSwitcher);
        setBluetoothIcon(this.bluetoothSwitcher, this.bluetoothTextView, false);
        this.bluetoothTextView.setText(getString(R.string.bluetooth_switcher_toast));
        this.bluetoothTextView.setOnClickListener(new ao(this));
        this.gpsTextView = (TextView) findViewById(R.id.sw_item_gps);
        this.gpsSwitcher = (l) v.b(this, 2);
        this.gpsTextView.setOnLongClickListener(this);
        this.gpsTextView.setTag(this.gpsSwitcher);
        setGpsIcon(this.gpsSwitcher, this.gpsTextView, false);
        this.gpsTextView.setText(getString(R.string.gps_switcher_toast));
        this.gpsTextView.setOnClickListener(new an(this));
        this.gprsTextView = (TextView) findViewById(R.id.sw_item_gprs);
        this.gprsSwitcher = (af) v.b(this, 8);
        this.gprsTextView.setOnLongClickListener(this);
        this.gprsTextView.setTag(this.gprsSwitcher);
        setGprsIcon(this.gprsSwitcher, this.gprsTextView);
        this.gprsTextView.setText(getString(R.string.gprs_switcher_toast));
        this.gprsTextView.setOnClickListener(new am(this));
    }

    /* access modifiers changed from: private */
    public void setAriplaneIcon(ab abVar, TextView textView, boolean z) {
        Drawable drawable;
        if (Settings.System.getInt(getContentResolver(), "airplane_mode_on", 0) == 1) {
            if (!z) {
                drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_airplane_on);
            }
            drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_airplane_off);
        } else {
            if (z) {
                drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_airplane_on);
            }
            drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_airplane_off);
        }
        textView.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
    }

    /* access modifiers changed from: private */
    public void setBluetoothIcon(f fVar, TextView textView, boolean z) {
        Drawable drawable;
        if (fVar.b() == 1) {
            if (!z) {
                drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_bluetooth_on);
            }
            drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_bluetooth_off);
        } else {
            if (z) {
                drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_bluetooth_on);
            }
            drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_bluetooth_off);
        }
        textView.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
    }

    /* access modifiers changed from: private */
    public void setBrightness(int i) {
        int i2 = 30;
        if (i >= 30) {
            i2 = i;
        }
        Settings.System.putInt(getContentResolver(), "screen_brightness", i2);
        if (this.inSetBrightness) {
            WindowManager.LayoutParams attributes = getWindow().getAttributes();
            if (i2 <= 255) {
                attributes.screenBrightness = (float) (((double) ((float) i2)) / 255.0d);
            }
            getWindow().setAttributes(attributes);
        }
        setBrightnessIcon(this.brightnessSwitcher, this.brightnessTextView, false);
    }

    /* access modifiers changed from: private */
    public void setBrightnessIcon(i iVar, TextView textView, boolean z) {
        Drawable drawable;
        int a = i.a(this);
        if (i.b(this)) {
            if (!z) {
                drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_brightness_auto);
            }
            drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_brightness_off);
        } else if (30 < a && a <= 128) {
            drawable = z ? getResources().getDrawable(R.drawable.ic_appwidget_settings_brightness_on) : getResources().getDrawable(R.drawable.ic_appwidget_settings_brightness_mid);
        } else if (128 >= a || a > 255) {
            if (z) {
                drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_brightness_mid);
            }
            drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_brightness_off);
        } else {
            drawable = z ? getResources().getDrawable(R.drawable.ic_appwidget_settings_brightness_auto) : getResources().getDrawable(R.drawable.ic_appwidget_settings_brightness_on);
        }
        textView.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
    }

    private void setGprsIcon(af afVar, TextView textView) {
        textView.setCompoundDrawablesWithIntrinsicBounds(afVar.b() ? getResources().getDrawable(R.drawable.ic_appwidget_settings_gprs_on) : getResources().getDrawable(R.drawable.ic_appwidget_settings_gprs_off), (Drawable) null, (Drawable) null, (Drawable) null);
    }

    /* access modifiers changed from: private */
    public void setGpsIcon(l lVar, TextView textView, boolean z) {
        Drawable drawable;
        if (lVar.a(this) == 1) {
            if (!z) {
                drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_gps_on);
            }
            drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_gps_off);
        } else {
            if (z) {
                drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_gps_on);
            }
            drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_gps_off);
        }
        textView.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
    }

    /* access modifiers changed from: private */
    public void setGrayityIcon(ad adVar, TextView textView, boolean z) {
        Drawable drawable;
        if (ad.a(this) == 1) {
            if (!z) {
                drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_gravity_on);
            }
            drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_gravity_off);
        } else {
            if (z) {
                drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_gravity_on);
            }
            drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_gravity_off);
        }
        textView.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
    }

    private void setSyncIcon(k kVar, TextView textView, boolean z) {
        Drawable drawable;
        if (k.a(this) == 1) {
            if (!z) {
                drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_sync_on);
            }
            drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_sync_off);
        } else {
            if (z) {
                drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_sync_on);
            }
            drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_sync_off);
        }
        textView.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
    }

    /* access modifiers changed from: private */
    public void setVibrateIcon(aj ajVar, TextView textView, boolean z) {
        Drawable drawable;
        if (((AudioManager) getSystemService("audio")).getRingerMode() == 0) {
            if (!z) {
                drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_rightone_off_nor);
            }
            drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_rightone_on_nor);
        } else {
            if (z) {
                drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_rightone_off_nor);
            }
            drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_rightone_on_nor);
        }
        textView.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
    }

    /* access modifiers changed from: private */
    public void setWifiIcon(aa aaVar, TextView textView, boolean z) {
        Drawable drawable;
        if (aaVar.b() == 1) {
            if (!z) {
                drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_wifi_on);
            }
            drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_wifi_off);
        } else {
            if (z) {
                drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_wifi_on);
            }
            drawable = getResources().getDrawable(R.drawable.ic_appwidget_settings_wifi_off);
        }
        textView.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.switcher_details);
        this.mContext = this;
        initSwitchers();
        this.mSeekBar = (SeekBar) findViewById(R.id.task_seekbar);
        this.mSeekBar.setProgressDrawable(getResources().getDrawable(R.drawable.custom_seekbar_background));
        this.mSeekBar.setProgress(i.a(this));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        int size = this.gridViewItem.size();
        for (int i = 0; i < size; i++) {
            ((ac) this.gridViewItem.get(i)).g();
        }
        super.onDestroy();
    }

    public boolean onLongClick(View view) {
        Object tag = view.getTag();
        if (!(tag instanceof ac)) {
            return true;
        }
        ac acVar = (ac) tag;
        BaseApp.a(acVar.c());
        Intent d = acVar.d();
        if (d == null) {
            return true;
        }
        try {
            startActivity(d);
            return true;
        } catch (Exception e) {
            return true;
        }
    }

    public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        this.inSetBrightness = false;
        this.handler.removeMessages(100);
        Message message = new Message();
        message.what = 100;
        message.arg1 = i;
        this.handler.sendMessage(message);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.isSetGps) {
            this.isSetGps = false;
            setGpsIcon(this.gpsSwitcher, this.gpsTextView, false);
        }
        if (af.c) {
            af.a(this.mContext);
            setGprsIcon(this.gprsSwitcher, this.gprsTextView);
        }
        super.onResume();
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
    }
}
