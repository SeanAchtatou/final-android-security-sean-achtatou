package com.tencent.module.component;

import android.app.Activity;
import android.app.ActivityManager;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.launcher.Launcher;
import com.tencent.launcher.base.c;
import com.tencent.qqlauncher.R;
import com.tencent.widget.f;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class TaskActivity extends Activity implements View.OnClickListener, af {
    /* access modifiers changed from: private */
    public boolean bFlag = false;
    private ImageView dividingLine;
    private AdapterView.OnItemClickListener itemClick = new j(this);
    private Button killButton;
    private aa mTaskAdapter;
    private ArrayList mTaskList = new ArrayList();
    /* access modifiers changed from: private */
    public ag mTaskManager;
    private int nAvailbleMemory = 0;
    /* access modifiers changed from: private */
    public int nTotalMemory = 0;
    private CustomProgressBar progressBar;
    private GridView taskGrid;
    private TextView taskMemoryLeft;
    private TextView taskMemoryRight;

    static /* synthetic */ void access$100(TaskActivity taskActivity, l lVar) {
        taskActivity.mTaskManager.a(lVar);
        taskActivity.mTaskList.remove(lVar);
        taskActivity.mTaskAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public long getAvailMemory() {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ((ActivityManager) getSystemService("activity")).getMemoryInfo(memoryInfo);
        return memoryInfo.availMem;
    }

    private long getTotalMemory() {
        long j = 0;
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("/proc/meminfo"), 8192);
            j = (long) (Integer.valueOf(bufferedReader.readLine().split("\\s+")[1]).intValue() * Launcher.APPWIDGET_HOST_ID);
            bufferedReader.close();
            return j;
        } catch (IOException e) {
            return j;
        }
    }

    private void initUI() {
        this.taskGrid = (GridView) findViewById(R.id.task_grid);
        this.killButton = (Button) findViewById(R.id.task_kill);
        this.dividingLine = (ImageView) findViewById(R.id.dividingline2);
        this.mTaskAdapter = new aa(this, this, this.mTaskList);
        this.mTaskAdapter.setNotifyOnChange(false);
        this.taskGrid.setAdapter((ListAdapter) this.mTaskAdapter);
        this.nTotalMemory = ((int) getTotalMemory()) / 1048576;
        this.nAvailbleMemory = ((int) getAvailMemory()) / 1048576;
        this.progressBar = (CustomProgressBar) findViewById(R.id.task_progressbar);
        this.progressBar.setIndeterminate(false);
        this.progressBar.setVisibility(0);
        this.progressBar.setMax(this.nTotalMemory);
        if (this.nTotalMemory > this.nAvailbleMemory * 5) {
            this.progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.custom_progressbar_high));
        } else {
            this.progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.custom_progressbar_low));
        }
        this.progressBar.setProgress(this.nTotalMemory - this.nAvailbleMemory);
        this.killButton.setOnClickListener(new k(this));
        this.taskGrid.setOnItemClickListener(this.itemClick);
    }

    private void killTask(l lVar) {
        this.mTaskManager.a(lVar);
        this.mTaskList.remove(lVar);
        this.mTaskAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void showTaskActionDialog(l lVar, View view) {
        int[] iArr = new int[2];
        view.getLocationInWindow(iArr);
        f fVar = new f(this, view, new Rect(iArr[0], iArr[1], iArr[0] + view.getWidth(), iArr[1] + view.getHeight()));
        c.a().a("__QuickActionWindow", fVar);
        fVar.a(R.drawable.ic_task_close, R.string.quick_close, new d(this, lVar, fVar));
        ap apVar = new ap();
        apVar.b = lVar.b();
        boolean b = m.b(apVar, this);
        int i = b ? R.string.quick_unlock : R.string.quick_lock;
        if (!b) {
            fVar.a(R.drawable.ic_task_lock, i, new e(this, view, apVar, fVar));
        } else {
            fVar.a(R.drawable.ic_task_unlock, i, new b(this, view, apVar, fVar));
        }
        fVar.a(R.drawable.ic_task_go, R.string.quick_jump, new c(this, lVar, fVar));
        fVar.a(R.drawable.ic_task_detail, R.string.quick_details, new a(this, lVar, fVar));
        fVar.a();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.task_refresh /*2131492982*/:
                this.mTaskManager.d();
                return;
            case R.id.task_title /*2131492983*/:
            default:
                return;
            case R.id.task_kill /*2131492984*/:
                this.mTaskManager.a(true);
                this.progressBar.setProgress(((int) getAvailMemory()) / 1048576);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setProgressBarVisibility(true);
        setContentView((int) R.layout.task_manager);
        initUI();
        this.mTaskManager = ag.a();
        this.mTaskManager.a(this);
        this.mTaskManager.d();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mTaskManager.b(this);
        super.onDestroy();
    }

    public void onMemoryUpdate(long j, long j2) {
        int i = (int) (j2 / 1024);
        if (i != this.nAvailbleMemory) {
            Rect bounds = this.progressBar.getProgressDrawable().getBounds();
            if (this.nTotalMemory > i * 5) {
                this.progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.custom_progressbar_high));
            } else {
                this.progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.custom_progressbar_low));
            }
            this.progressBar.getProgressDrawable().setBounds(bounds);
            this.progressBar.setProgress(this.nTotalMemory - i);
        }
        if (i > this.nAvailbleMemory && this.bFlag) {
            Toast.makeText(this, getString(R.string.release_memory, new Object[]{Integer.valueOf(i - this.nAvailbleMemory)}), 0).show();
        }
        this.nAvailbleMemory = i;
        this.bFlag = false;
    }

    public void onTaskListUpdate(ArrayList arrayList) {
        this.mTaskList.clear();
        this.mTaskList.addAll(arrayList);
        this.mTaskAdapter.notifyDataSetChanged();
    }
}
