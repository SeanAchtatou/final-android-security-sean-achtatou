package com.tencent.module.component;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

final class q extends SQLiteOpenHelper {
    public q(Context context) {
        super(context, "favcomponent.db", (SQLiteDatabase.CursorFactory) null, 5);
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE favcomponent (_id INTEGER PRIMARY KEY,title TEXT,intent TEXT,itemType INTEGER,isShortcut INTEGER,iconType INTEGER,iconPackage TEXT,iconResource TEXT,icon BLOB,uri TEXT,packageName TEXT,className TEXT,layoutId INTEGER);");
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }
}
