package com.tencent.module.component;

import android.view.View;
import android.widget.AdapterView;

final class j implements AdapterView.OnItemClickListener {
    private /* synthetic */ TaskActivity a;

    j(TaskActivity taskActivity) {
        this.a = taskActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        l lVar = (l) adapterView.getItemAtPosition(i);
        if (lVar != null) {
            this.a.showTaskActionDialog(lVar, view);
        }
    }
}
