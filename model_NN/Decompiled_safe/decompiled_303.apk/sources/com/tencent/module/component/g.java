package com.tencent.module.component;

import android.view.View;
import android.widget.AdapterView;

final class g implements AdapterView.OnItemClickListener {
    private /* synthetic */ TaskLayout a;

    g(TaskLayout taskLayout) {
        this.a = taskLayout;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        s sVar = (s) adapterView.getItemAtPosition(i);
        if (sVar != null) {
            this.a.a(sVar, true);
        }
    }
}
