package com.tencent.module.component;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import java.util.List;

final class x extends ArrayAdapter {
    private /* synthetic */ TaskLayout a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public x(TaskLayout taskLayout, Context context, List list) {
        super(context, 0, list);
        this.a = taskLayout;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        ImageView imageView = view == null ? new ImageView(this.a.g) : (ImageView) view;
        imageView.setImageDrawable(((s) getItem(i)).a());
        return imageView;
    }
}
