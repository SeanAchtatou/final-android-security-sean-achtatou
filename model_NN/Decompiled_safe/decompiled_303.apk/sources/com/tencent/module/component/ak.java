package com.tencent.module.component;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import com.tencent.qqlauncher.R;

final class ak extends AlertDialog {
    final /* synthetic */ TaskWidget a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    protected ak(TaskWidget taskWidget, Context context) {
        super(context);
        this.a = taskWidget;
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        View inflate = View.inflate(this.a.c, R.layout.widget_toast, null);
        getWindow().setContentView(inflate);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(-2, -2, 0, -50, 2, 2, -3);
        layoutParams.dimAmount = 0.5f;
        getWindow().setAttributes(layoutParams);
        getWindow().getAttributes().windowAnimations = 16973826;
        inflate.findViewById(R.id.widget_toast_button1).setOnClickListener(new ai(this));
        inflate.findViewById(R.id.widget_toast_button1).requestFocus();
        inflate.findViewById(R.id.widget_toast_button2).setOnClickListener(new aj(this));
        setOnCancelListener(new ah(this));
    }
}
