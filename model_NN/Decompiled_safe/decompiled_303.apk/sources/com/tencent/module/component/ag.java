package com.tencent.module.component;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import com.tencent.launcher.base.BaseApp;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class ag {
    private static ag a;
    /* access modifiers changed from: private */
    public Context b = BaseApp.b();
    private Resources c = this.b.getResources();
    /* access modifiers changed from: private */
    public PackageManager d = this.b.getPackageManager();
    private ActivityManager e = ((ActivityManager) this.b.getSystemService("activity"));
    private ArrayList f = new ArrayList();
    private n g;
    private ad h;

    private ag() {
    }

    public static long a(String str) {
        if (str == null || str.equals("")) {
            return 0;
        }
        try {
            String replaceAll = str.trim().replaceAll(",", "");
            int indexOf = replaceAll.indexOf(".");
            if (indexOf > 0) {
                replaceAll = replaceAll.substring(0, indexOf);
            }
            return Long.parseLong(replaceAll);
        } catch (Exception e2) {
            return 0;
        }
    }

    public static synchronized ag a() {
        ag agVar;
        synchronized (ag.class) {
            if (a == null) {
                a = new ag();
            }
            agVar = a;
        }
        return agVar;
    }

    static /* synthetic */ ArrayList b(ag agVar) {
        HashMap e2 = agVar.e();
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = agVar.e.getRunningAppProcesses();
        ArrayList arrayList = new ArrayList();
        for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
            if (!next.processName.equals("system") && !next.processName.equals("com.android.phone") && !next.processName.equals("com.tencent.qqlauncher")) {
                l lVar = new l(agVar, next, e2);
                if (lVar.a != null) {
                    arrayList.add(lVar);
                }
            }
        }
        return arrayList;
    }

    static /* synthetic */ long c(ag agVar) {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        agVar.e.getMemoryInfo(memoryInfo);
        return memoryInfo.availMem / 1024;
    }

    private HashMap e() {
        HashMap hashMap = new HashMap();
        try {
            Process exec = Runtime.getRuntime().exec("ps");
            Thread thread = new Thread(new r(this, exec, hashMap));
            thread.start();
            exec.waitFor();
            while (thread.isAlive()) {
                Thread.sleep(50);
            }
            return hashMap;
        } catch (IOException e2) {
            System.err.println("RunScript have a IO error :" + e2.getMessage());
            return null;
        } catch (InterruptedException e3) {
            System.err.println("RunScript have a interrupte error:" + e3.getMessage());
            return null;
        } catch (Exception e4) {
            System.err.print("RunScript have a error :" + e4.getMessage());
            return null;
        }
    }

    public final void a(long j, long j2) {
        int size = this.f.size();
        for (int i = 0; i < size; i++) {
            af afVar = (af) this.f.get(i);
            if (afVar != null) {
                afVar.onMemoryUpdate(j, j2);
            }
        }
    }

    public final void a(af afVar) {
        this.f.add(afVar);
    }

    public final void a(l lVar) {
        try {
            ActivityManager.class.getDeclaredMethod("killBackgroundProcesses", String.class).invoke(this.e, lVar.c);
        } catch (Exception e2) {
        }
        this.e.restartPackage(lVar.c);
    }

    public final void a(ArrayList arrayList) {
        int size = this.f.size();
        for (int i = 0; i < size; i++) {
            af afVar = (af) this.f.get(i);
            if (afVar != null) {
                afVar.onTaskListUpdate(arrayList);
            }
        }
    }

    public final void a(boolean z) {
        if (this.h == null || !this.h.a || (z && !this.h.b)) {
            this.h = new ad(this, z);
            this.h.start();
        }
    }

    public final void b() {
        this.f.clear();
    }

    public final void b(af afVar) {
        this.f.remove(afVar);
    }

    public final void c() {
        if (this.g == null || !this.g.a) {
            this.g = new n(this, false);
            this.g.execute(new Integer[0]);
        }
    }

    public final void d() {
        if (this.g == null || !this.g.a || !this.g.b) {
            this.g = new n(this, true);
            this.g.execute(new Integer[0]);
        }
    }
}
