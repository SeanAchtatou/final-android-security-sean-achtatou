package com.tencent.module.component;

import android.app.ActivityManager;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.graphics.drawable.Drawable;
import com.tencent.launcher.a;
import com.tencent.qqlauncher.R;
import java.util.HashMap;

final class s {
    /* access modifiers changed from: private */
    public ApplicationInfo a;
    private int b;
    /* access modifiers changed from: private */
    public String c;
    private /* synthetic */ TaskLayout d;

    public s(TaskLayout taskLayout, ActivityManager.RunningAppProcessInfo runningAppProcessInfo, HashMap hashMap) {
        this.d = taskLayout;
        this.c = runningAppProcessInfo.processName;
        Integer num = (Integer) hashMap.get(new Integer(runningAppProcessInfo.pid));
        if (num != null) {
            this.b = num.intValue();
        } else {
            this.b = 0;
        }
        try {
            this.a = taskLayout.i.getApplicationInfo(this.c, 8192);
        } catch (Exception e) {
        }
    }

    public final Drawable a() {
        return a.a(this.a.loadIcon(this.d.i), this.d.g, (int) this.d.h.getDimension(R.dimen.thumbnail_icon_size));
    }

    public final String b() {
        return this.a.packageName;
    }

    public final CharSequence c() {
        return this.a.loadLabel(this.d.i);
    }

    public final Intent d() {
        PackageInfo packageArchiveInfo = this.d.i.getPackageArchiveInfo(this.c, 8192);
        try {
            Intent launchIntentForPackage = this.d.i.getLaunchIntentForPackage(this.c);
            if (launchIntentForPackage != null) {
                Intent cloneFilter = launchIntentForPackage.cloneFilter();
                cloneFilter.addFlags(4194304);
                return cloneFilter;
            } else if (packageArchiveInfo == null) {
                return null;
            } else {
                if (packageArchiveInfo.activities.length == 1) {
                    Intent intent = new Intent("android.intent.action.MAIN");
                    intent.addFlags(4194304);
                    intent.setClassName(packageArchiveInfo.packageName, packageArchiveInfo.activities[0].name);
                    return intent;
                }
                Intent a2 = ab.a(packageArchiveInfo.packageName, this.d.i);
                if (a2 == null) {
                    return null;
                }
                a2.addFlags(4194304);
                return a2;
            }
        } catch (Exception e) {
            return null;
        }
    }
}
