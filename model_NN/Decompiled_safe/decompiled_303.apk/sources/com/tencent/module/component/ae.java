package com.tencent.module.component;

import android.os.AsyncTask;
import com.tencent.qqlauncher.R;

final class ae extends AsyncTask {
    private /* synthetic */ TaskLayout a;

    /* synthetic */ ae(TaskLayout taskLayout) {
        this(taskLayout, (byte) 0);
    }

    private ae(TaskLayout taskLayout, byte b) {
        this.a = taskLayout;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        String unused = this.a.k = this.a.g.getString(R.string.task_title) + "(" + ((int) (TaskLayout.c(this.a) / 1024)) + "M/" + ((int) (al.a() / 1024)) + "M)";
        publishProgress(1);
        TaskLayout.d(this.a);
        publishProgress(2);
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onProgressUpdate(Object[] objArr) {
        Integer[] numArr = (Integer[]) objArr;
        if (numArr.length == 1) {
            switch (numArr[0].intValue()) {
                case 1:
                    this.a.e.setText(this.a.k);
                    break;
                case 2:
                    this.a.d.notifyDataSetChanged();
                    break;
            }
            super.onProgressUpdate(numArr);
        }
    }
}
