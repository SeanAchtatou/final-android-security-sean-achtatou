package com.tencent.module.component;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.tencent.qphone.widget.soso.SosoSearchMainActivity;
import java.util.ArrayList;

final class t extends BroadcastReceiver {
    private /* synthetic */ SosoWidget a;

    /* synthetic */ t(SosoWidget sosoWidget) {
        this(sosoWidget, (byte) 0);
    }

    private t(SosoWidget sosoWidget, byte b) {
        this.a = sosoWidget;
    }

    public final void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("com.tencent.qqlauncher.widget.soso.GLOBAL_SEARCH")) {
            Intent intent2 = new Intent(context, SosoSearchMainActivity.class);
            intent2.setFlags(268435456);
            context.startActivity(intent2);
        }
        if (intent.getAction().equals("com.tencent.qqlauncher.widget.soso.SEND_HOT")) {
            ArrayList unused = SosoWidget.m = intent.getStringArrayListExtra("HOT_WORD");
            SosoWidget.a(this.a);
        } else if (intent.getAction().equals("com.tencent.qqlauncher.widget.soso_hot")) {
            SosoWidget.a(this.a);
        }
    }
}
