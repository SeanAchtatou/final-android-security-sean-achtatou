package com.tencent.module.component;

import android.view.View;
import android.widget.ImageView;
import com.tencent.launcher.base.BaseApp;
import com.tencent.qqlauncher.R;
import com.tencent.widget.f;

final class e implements View.OnClickListener {
    private /* synthetic */ View a;
    private /* synthetic */ ap b;
    private /* synthetic */ f c;
    private /* synthetic */ TaskActivity d;

    e(TaskActivity taskActivity, View view, ap apVar, f fVar) {
        this.d = taskActivity;
        this.a = view;
        this.b = apVar;
        this.c = fVar;
    }

    public final void onClick(View view) {
        m.a(this.b, this.d);
        ((ImageView) this.a.findViewById(R.id.task_exclude)).setVisibility(0);
        BaseApp.a(this.d.getString(R.string.task_add_exclude));
        this.c.dismiss();
    }
}
