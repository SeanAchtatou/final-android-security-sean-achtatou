package com.tencent.module.component;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.tencent.qqlauncher.R;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class TaskLayout extends LinearLayout implements View.OnClickListener {
    private GridView a;
    private ImageButton b;
    private ImageButton c;
    /* access modifiers changed from: private */
    public x d;
    /* access modifiers changed from: private */
    public TextView e;
    private ArrayList f;
    /* access modifiers changed from: private */
    public Context g;
    /* access modifiers changed from: private */
    public Resources h;
    /* access modifiers changed from: private */
    public PackageManager i;
    private ActivityManager j;
    /* access modifiers changed from: private */
    public String k;
    private ae l;
    private AdapterView.OnItemLongClickListener m = new f(this);
    private AdapterView.OnItemClickListener n = new g(this);

    public TaskLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.g = context;
        this.h = context.getResources();
        this.i = context.getPackageManager();
        this.j = (ActivityManager) this.g.getSystemService("activity");
        this.f = new ArrayList();
        this.d = new x(this, context, this.f);
        this.k = this.g.getString(R.string.task_title);
    }

    public static long a(String str) {
        if (str == null || str.equals("")) {
            return 0;
        }
        try {
            String replaceAll = str.trim().replaceAll(",", "");
            int indexOf = replaceAll.indexOf(".");
            if (indexOf > 0) {
                replaceAll = replaceAll.substring(0, indexOf);
            }
            return Long.parseLong(replaceAll);
        } catch (Exception e2) {
            return 0;
        }
    }

    private HashMap a() {
        HashMap hashMap = new HashMap();
        try {
            Process exec = Runtime.getRuntime().exec("ps");
            Thread thread = new Thread(new i(this, exec, hashMap));
            thread.start();
            exec.waitFor();
            while (thread.isAlive()) {
                Thread.sleep(50);
            }
            return hashMap;
        } catch (IOException e2) {
            System.err.println("RunScript have a IO error :" + e2.getMessage());
            return null;
        } catch (InterruptedException e3) {
            System.err.println("RunScript have a interrupte error:" + e3.getMessage());
            return null;
        } catch (Exception e4) {
            System.err.print("RunScript have a error :" + e4.getMessage());
            return null;
        }
    }

    static /* synthetic */ void a(TaskLayout taskLayout, s sVar) {
        String[] stringArray = taskLayout.h.getStringArray(R.array.menu_task_operation);
        ap apVar = new ap();
        apVar.b = sVar.b();
        if (m.b(apVar, taskLayout.g)) {
            stringArray[3] = taskLayout.h.getString(R.string.task_unexclude);
        } else {
            stringArray[3] = taskLayout.h.getString(R.string.task_exclude);
        }
        new AlertDialog.Builder(taskLayout.g).setTitle(sVar.c()).setItems(stringArray, new h(taskLayout, sVar)).create().show();
    }

    /* access modifiers changed from: private */
    public void a(s sVar, boolean z) {
        try {
            ActivityManager.class.getDeclaredMethod("killBackgroundProcesses", String.class).invoke(this.j, sVar.c);
        } catch (Exception e2) {
        }
        this.j.restartPackage(sVar.c);
        if (z) {
            this.f.remove(sVar);
            this.d.notifyDataSetChanged();
        }
    }

    static /* synthetic */ long c(TaskLayout taskLayout) {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        taskLayout.j.getMemoryInfo(memoryInfo);
        return memoryInfo.availMem / 1024;
    }

    static /* synthetic */ void d(TaskLayout taskLayout) {
        HashMap a2 = taskLayout.a();
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = taskLayout.j.getRunningAppProcesses();
        taskLayout.f.clear();
        for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
            if (!next.processName.equals("system") && !next.processName.equals("com.android.phone")) {
                s sVar = new s(taskLayout, next, a2);
                if (sVar.a != null) {
                    taskLayout.f.add(sVar);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.module.component.TaskLayout.a(com.tencent.module.component.s, boolean):void
     arg types: [com.tencent.module.component.s, int]
     candidates:
      com.tencent.module.component.TaskLayout.a(com.tencent.module.component.TaskLayout, java.lang.String):java.lang.String
      com.tencent.module.component.TaskLayout.a(com.tencent.module.component.TaskLayout, com.tencent.module.component.s):void
      com.tencent.module.component.TaskLayout.a(com.tencent.module.component.s, boolean):void */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.task_refresh /*2131492982*/:
                if (this.l != null && this.l.getStatus() == AsyncTask.Status.FINISHED) {
                    new ae(this).execute(new Integer[0]);
                    return;
                }
                return;
            case R.id.task_title /*2131492983*/:
            default:
                return;
            case R.id.task_kill /*2131492984*/:
                String packageName = this.g.getPackageName();
                Iterator it = this.f.iterator();
                while (it.hasNext()) {
                    s sVar = (s) it.next();
                    if (!sVar.b().equals(packageName)) {
                        ap apVar = new ap();
                        apVar.b = sVar.b();
                        if (!m.b(apVar, this.g)) {
                            a(sVar, false);
                        }
                    }
                }
                new ae(this).execute(new Integer[0]);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.a = (GridView) findViewById(R.id.task_grid);
        this.b = (ImageButton) findViewById(R.id.task_refresh);
        this.c = (ImageButton) findViewById(R.id.task_kill);
        this.e = (TextView) findViewById(R.id.task_title);
        this.e.setText(this.k);
        this.a.setAdapter((ListAdapter) this.d);
        this.b.setOnClickListener(this);
        this.c.setOnClickListener(this);
        this.a.setOnItemLongClickListener(this.m);
        this.a.setOnItemClickListener(this.n);
        this.l = new ae(this);
        this.l.execute(new Integer[0]);
        super.onFinishInflate();
    }
}
