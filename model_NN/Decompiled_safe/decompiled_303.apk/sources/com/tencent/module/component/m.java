package com.tencent.module.component;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public final class m {
    public static void a(ap apVar, Context context) {
        ContentValues contentValues = new ContentValues();
        ContentResolver contentResolver = context.getContentResolver();
        contentValues.put("packageName", apVar.b);
        Uri insert = contentResolver.insert(TaskComponentProvider.b, contentValues);
        if (insert != null) {
            apVar.a = (long) Integer.parseInt(insert.getPathSegments().get(1));
        }
    }

    public static boolean b(ap apVar, Context context) {
        Cursor query = context.getContentResolver().query(TaskComponentProvider.a, new String[]{"packageName"}, "packageName=?", new String[]{apVar.b}, null);
        try {
            return query.moveToFirst();
        } finally {
            query.close();
        }
    }
}
