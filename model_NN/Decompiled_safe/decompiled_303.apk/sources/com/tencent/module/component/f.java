package com.tencent.module.component;

import android.view.View;
import android.widget.AdapterView;

final class f implements AdapterView.OnItemLongClickListener {
    private /* synthetic */ TaskLayout a;

    f(TaskLayout taskLayout) {
        this.a = taskLayout;
    }

    public final boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
        s sVar = (s) adapterView.getItemAtPosition(i);
        if (sVar == null) {
            return false;
        }
        TaskLayout.a(this.a, sVar);
        return false;
    }
}
