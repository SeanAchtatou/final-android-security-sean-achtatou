package com.tencent.module.component;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.qqlauncher.R;
import java.util.List;

final class aa extends ArrayAdapter {
    private final LayoutInflater a;
    private /* synthetic */ TaskActivity b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public aa(TaskActivity taskActivity, Context context, List list) {
        super(context, 0, list);
        this.b = taskActivity;
        this.a = LayoutInflater.from(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.a.inflate((int) R.layout.task_gridview_item, viewGroup, false) : view;
        TextView textView = (TextView) inflate.findViewById(R.id.app_name_textview);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.task_exclude);
        l lVar = (l) getItem(i);
        textView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, lVar.a(), (Drawable) null, (Drawable) null);
        textView.setText(lVar.c());
        ap apVar = new ap();
        apVar.b = lVar.b();
        imageView.setVisibility(m.b(apVar, this.b) ? 0 : 8);
        return inflate;
    }
}
