package com.tencent.module.component;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class FavComponentProvider extends ContentProvider {
    private static Uri a = Uri.parse("content://com.tencent.FavComponent/favcomponent?notify=true");
    private static Uri b = Uri.parse("content://com.tencent.FavComponent/favcomponent?notify=false");
    private SQLiteOpenHelper c;

    /* JADX INFO: finally extract failed */
    public int bulkInsert(Uri uri, ContentValues[] contentValuesArr) {
        o oVar = new o(uri);
        SQLiteDatabase writableDatabase = this.c.getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            for (ContentValues insert : contentValuesArr) {
                if (writableDatabase.insert(oVar.a, null, insert) < 0) {
                    writableDatabase.endTransaction();
                    return 0;
                }
            }
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            return contentValuesArr.length;
        } catch (Throwable th) {
            writableDatabase.endTransaction();
            throw th;
        }
    }

    public int delete(Uri uri, String str, String[] strArr) {
        o oVar = new o(uri, str, strArr);
        return this.c.getWritableDatabase().delete(oVar.a, oVar.b, oVar.c);
    }

    public String getType(Uri uri) {
        return null;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        long insert = this.c.getWritableDatabase().insert(new o(uri).a, null, contentValues);
        if (insert <= 0) {
            return null;
        }
        return ContentUris.withAppendedId(uri, insert);
    }

    public boolean onCreate() {
        this.c = new q(getContext());
        return true;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        o oVar = new o(uri, str, strArr2);
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setTables(oVar.a);
        Cursor query = sQLiteQueryBuilder.query(this.c.getWritableDatabase(), strArr, oVar.b, oVar.c, null, null, str2);
        query.setNotificationUri(getContext().getContentResolver(), uri);
        return query;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        o oVar = new o(uri, str, strArr);
        return this.c.getWritableDatabase().update(oVar.a, contentValues, oVar.b, oVar.c);
    }
}
