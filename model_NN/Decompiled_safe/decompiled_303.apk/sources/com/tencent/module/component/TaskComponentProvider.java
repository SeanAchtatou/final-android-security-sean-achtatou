package com.tencent.module.component;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class TaskComponentProvider extends ContentProvider {
    public static final Uri a = Uri.parse("content://com.tencent.qqlauncher.TaskComponent/taskcomponent?notify=true");
    public static final Uri b = Uri.parse("content://com.tencent.qqlauncher.TaskComponent/taskcomponent?notify=false");
    private SQLiteOpenHelper c;

    /* JADX INFO: finally extract failed */
    public int bulkInsert(Uri uri, ContentValues[] contentValuesArr) {
        z zVar = new z(uri);
        SQLiteDatabase writableDatabase = this.c.getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            for (ContentValues insert : contentValuesArr) {
                if (writableDatabase.insert(zVar.a, null, insert) < 0) {
                    writableDatabase.endTransaction();
                    return 0;
                }
            }
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            return contentValuesArr.length;
        } catch (Throwable th) {
            writableDatabase.endTransaction();
            throw th;
        }
    }

    public int delete(Uri uri, String str, String[] strArr) {
        z zVar = new z(uri, str, strArr);
        return this.c.getWritableDatabase().delete(zVar.a, zVar.b, zVar.c);
    }

    public String getType(Uri uri) {
        return null;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        long insert = this.c.getWritableDatabase().insert(new z(uri).a, null, contentValues);
        if (insert <= 0) {
            return null;
        }
        return ContentUris.withAppendedId(uri, insert);
    }

    public boolean onCreate() {
        this.c = new ac(getContext());
        return true;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        z zVar = new z(uri, str, strArr2);
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setTables(zVar.a);
        Cursor query = sQLiteQueryBuilder.query(this.c.getWritableDatabase(), strArr, zVar.b, zVar.c, null, null, str2);
        query.setNotificationUri(getContext().getContentResolver(), uri);
        return query;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        z zVar = new z(uri, str, strArr);
        return this.c.getWritableDatabase().update(zVar.a, contentValues, zVar.b, zVar.c);
    }
}
