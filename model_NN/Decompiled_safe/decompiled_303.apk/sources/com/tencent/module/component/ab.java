package com.tencent.module.component;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import java.util.List;

public final class ab {
    private static List a = null;

    public static Intent a(String str, PackageManager packageManager) {
        for (ResolveInfo resolveInfo : a(packageManager)) {
            if (str.equals(resolveInfo.activityInfo.packageName)) {
                Intent intent = new Intent("android.intent.action.MAIN");
                intent.addCategory("android.intent.category.LAUNCHER");
                intent.setClassName(str, resolveInfo.activityInfo.name);
                return intent;
            }
        }
        return null;
    }

    private static synchronized List a(PackageManager packageManager) {
        List list;
        synchronized (ab.class) {
            if (a == null) {
                Intent intent = new Intent("android.intent.action.MAIN");
                intent.addCategory("android.intent.category.LAUNCHER");
                a = packageManager.queryIntentActivities(intent, 0);
            }
            list = a;
        }
        return list;
    }
}
