package com.tencent.module.component;

import android.view.View;

final class an implements View.OnClickListener {
    private /* synthetic */ SwitcherActivity a;

    an(SwitcherActivity switcherActivity) {
        this.a = switcherActivity;
    }

    public final void onClick(View view) {
        boolean unused = this.a.isSetGps = true;
        this.a.setGpsIcon(this.a.gpsSwitcher, this.a.gpsTextView, true);
        this.a.gpsSwitcher.a();
    }
}
