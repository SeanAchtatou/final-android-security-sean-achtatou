package com.tencent.module.component;

import android.view.View;
import com.tencent.util.p;
import com.tencent.widget.f;

final class a implements View.OnClickListener {
    private /* synthetic */ l a;
    private /* synthetic */ f b;
    private /* synthetic */ TaskActivity c;

    a(TaskActivity taskActivity, l lVar, f fVar) {
        this.c = taskActivity;
        this.a = lVar;
        this.b = fVar;
    }

    public final void onClick(View view) {
        this.c.startActivity(p.a(this.a.b()));
        this.b.dismiss();
    }
}
