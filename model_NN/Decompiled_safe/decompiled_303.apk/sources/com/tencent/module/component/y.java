package com.tencent.module.component;

import android.content.Intent;
import android.view.View;
import com.tencent.qqlauncher.R;

final class y implements View.OnClickListener {
    private /* synthetic */ SwitcherWidget a;

    y(SwitcherWidget switcherWidget) {
        this.a = switcherWidget;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_more /*2131493363*/:
                Intent intent = new Intent(this.a.b, SwitcherActivity.class);
                intent.addFlags(8388608);
                this.a.b.startActivity(intent);
                return;
            default:
                return;
        }
    }
}
