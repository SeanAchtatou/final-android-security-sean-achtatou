package com.tencent.module.component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class al {
    public static long a() {
        long j = 0;
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec("cat /proc/meminfo").getInputStream()), 8192);
            while (true) {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine != null) {
                        if (readLine.contains("MemTotal")) {
                            String[] split = readLine.split(" ");
                            j = Long.parseLong(split[split.length - 2]);
                            break;
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    try {
                        bufferedReader.close();
                        return 0;
                    } catch (IOException e2) {
                        return 0;
                    }
                } catch (Throwable th) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e3) {
                    }
                    throw th;
                }
            }
            try {
                bufferedReader.close();
                return j;
            } catch (IOException e4) {
                return j;
            }
        } catch (Exception e5) {
            e5.printStackTrace();
            return 0;
        }
    }
}
