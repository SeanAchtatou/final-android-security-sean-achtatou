package com.tencent.module.component;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.launcher.Launcher;
import com.tencent.qqlauncher.R;
import com.tencent.widget.WidgetFrame;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class TaskWidget extends WidgetFrame implements View.OnClickListener, View.OnTouchListener, af {
    private ImageView a;
    private ImageView b;
    /* access modifiers changed from: private */
    public Context c;
    /* access modifiers changed from: private */
    public ag d;
    private View e;
    private TextView f;
    private TextView g;
    private View h;
    private String i = "";
    private int j = 0;
    private int k = 0;
    /* access modifiers changed from: private */
    public boolean l = false;

    public TaskWidget(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = context;
        this.d = ag.a();
    }

    private static long a() {
        long j2 = 0;
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("/proc/meminfo"), 8192);
            j2 = (long) (Integer.valueOf(bufferedReader.readLine().split("\\s+")[1]).intValue() * Launcher.APPWIDGET_HOST_ID);
            bufferedReader.close();
            return j2;
        } catch (IOException e2) {
            return j2;
        }
    }

    private void a(int i2, int i3) {
        if (i2 > i3 * 5) {
            this.f.setBackgroundResource(R.drawable.taskwidget_process_hight);
            this.f.setShadowLayer(1.0f, 1.0f, 1.0f, Color.parseColor("#b26b24"));
        } else {
            this.f.setBackgroundResource(R.drawable.taskwidget_process_low);
            this.f.setShadowLayer(1.0f, 1.0f, 1.0f, Color.parseColor("#5f9c2a"));
        }
        int width = (this.e.getWidth() - this.e.getPaddingLeft()) - this.e.getPaddingRight();
        if (width <= 0) {
            width = (((int) getResources().getDimension(R.dimen.taskwidget_processbar_width)) - ((int) getResources().getDimension(R.dimen.taskwidget_processbar_paddingLeft))) - ((int) getResources().getDimension(R.dimen.taskwidget_processbar_paddingRight));
        }
        int i4 = ((i2 - i3) * width) / i2;
        if (i4 > width - this.g.getWidth()) {
            this.g.setVisibility(4);
        } else {
            this.g.setVisibility(0);
        }
        this.f.getLayoutParams().width = i4;
        this.f.setText((i2 - i3) + "M ");
        this.g.setText(i2 + "M");
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.task_refresh /*2131492982*/:
                this.d.c();
                return;
            case R.id.task_kill /*2131492984*/:
                new ak(this, this.c).show();
                return;
            case R.id.taskwidget_progressbar /*2131493365*/:
                this.c.startActivity(new Intent(this.c, TaskActivity.class));
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.a = (ImageView) findViewById(R.id.task_refresh);
        this.b = (ImageView) findViewById(R.id.task_kill);
        this.k = ((int) a()) / 1048576;
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ((ActivityManager) this.c.getSystemService("activity")).getMemoryInfo(memoryInfo);
        this.j = ((int) memoryInfo.availMem) / 1048576;
        ((Activity) this.c).setProgressBarVisibility(true);
        this.e = findViewById(R.id.taskwidget_progressbar);
        this.f = (TextView) findViewById(R.id.taskwidget_used_memory);
        this.g = (TextView) findViewById(R.id.taskwidget_total_memory);
        this.h = findViewById(R.id.taskwidget_progressbar);
        this.e.setOnClickListener(this);
        this.e.setFocusable(true);
        this.e.setOnTouchListener(this);
        a(this.k, this.j);
        this.a.setOnClickListener(this);
        this.a.setFocusable(true);
        this.b.setOnClickListener(this);
        this.b.setFocusable(true);
        this.d.a(this);
        this.d.c();
        super.onFinishInflate();
    }

    public void onMemoryUpdate(long j2, long j3) {
        if (this.j == 0 && this.k == 0) {
            this.j = (int) (j3 / 1024);
            this.k = (int) (j2 / 1024);
        }
        int i2 = (int) (j3 / 1024);
        this.i = this.c.getString(R.string.task_title) + "(" + (this.k - i2) + "M/" + ((int) (j2 / 1024)) + "M)";
        if (i2 != this.j) {
            a(this.k, i2);
        }
        if (i2 > this.j && this.l) {
            Toast.makeText(this.c, this.c.getString(R.string.release_memory, Integer.valueOf(i2 - this.j)), 0).show();
        }
        this.j = i2;
        this.l = false;
    }

    public void onTaskListUpdate(ArrayList arrayList) {
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.f.setPressed(true);
                this.g.setPressed(true);
                this.h.setPressed(true);
                this.f.setShadowLayer(0.0f, 0.0f, 0.0f, Color.parseColor("#00000000"));
                break;
            case 1:
                this.f.setPressed(false);
                this.g.setPressed(false);
                this.h.setPressed(false);
                this.f.setShadowLayer(1.0f, 1.0f, -1.0f, Color.parseColor("#5f9c2a"));
                this.c.startActivity(new Intent(this.c, TaskActivity.class));
                break;
            case 3:
                this.f.setPressed(false);
                this.g.setPressed(false);
                this.h.setPressed(false);
                this.f.setShadowLayer(1.0f, 1.0f, -1.0f, Color.parseColor("#5f9c2a"));
                break;
        }
        return false;
    }
}
