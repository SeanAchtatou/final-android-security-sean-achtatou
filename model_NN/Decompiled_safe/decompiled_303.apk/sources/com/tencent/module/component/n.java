package com.tencent.module.component;

import android.os.AsyncTask;
import java.util.ArrayList;

final class n extends AsyncTask {
    boolean a = true;
    boolean b = false;
    private long c;
    private long d;
    private ArrayList e;
    private /* synthetic */ ag f;

    n(ag agVar, boolean z) {
        this.f = agVar;
        this.b = z;
    }

    private Integer a() {
        try {
            this.c = al.a();
            this.d = ag.c(this.f);
            publishProgress(1);
            if (this.b) {
                this.e = ag.b(this.f);
                publishProgress(2);
            }
            return null;
        } catch (Exception e2) {
            return null;
        } finally {
            this.a = false;
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onProgressUpdate(Object[] objArr) {
        Integer[] numArr = (Integer[]) objArr;
        if (numArr.length == 1) {
            switch (numArr[0].intValue()) {
                case 1:
                    this.f.a(this.c, this.d);
                    break;
                case 2:
                    this.f.a(this.e);
                    break;
            }
            super.onProgressUpdate(numArr);
        }
    }
}
