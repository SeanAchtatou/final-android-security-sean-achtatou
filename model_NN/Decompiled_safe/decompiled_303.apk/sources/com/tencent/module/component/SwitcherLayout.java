package com.tencent.module.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.tencent.module.switcher.SwitcherView;
import com.tencent.module.switcher.v;
import com.tencent.qqlauncher.R;

public class SwitcherLayout extends LinearLayout {
    private LinearLayout.LayoutParams a = new LinearLayout.LayoutParams(-2, -1);
    private Context b;

    public SwitcherLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = context;
        this.a.weight = 1.0f;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.row1);
        SwitcherView a2 = v.a(this.b, 0);
        LinearLayout linearLayout2 = (LinearLayout) inflate(this.b, R.layout.switcher_frame_item, null);
        linearLayout2.addView(a2);
        linearLayout.addView(linearLayout2, this.a);
        SwitcherView a3 = v.a(this.b, 4);
        LinearLayout linearLayout3 = (LinearLayout) inflate(this.b, R.layout.switcher_frame_item, null);
        linearLayout3.addView(a3);
        linearLayout.addView(linearLayout3, this.a);
        SwitcherView a4 = v.a(this.b, 5);
        LinearLayout linearLayout4 = (LinearLayout) inflate(this.b, R.layout.switcher_frame_item, null);
        linearLayout4.addView(a4);
        linearLayout.addView(linearLayout4, this.a);
        SwitcherView a5 = v.a(this.b, 3);
        LinearLayout linearLayout5 = (LinearLayout) inflate(this.b, R.layout.switcher_frame_item, null);
        linearLayout5.addView(a5);
        linearLayout.addView(linearLayout5, this.a);
        LinearLayout linearLayout6 = (LinearLayout) findViewById(R.id.row2);
        SwitcherView a6 = v.a(this.b, 1);
        LinearLayout linearLayout7 = (LinearLayout) inflate(this.b, R.layout.switcher_frame_item, null);
        linearLayout7.addView(a6);
        linearLayout6.addView(linearLayout7, this.a);
        SwitcherView a7 = v.a(this.b, 7);
        LinearLayout linearLayout8 = (LinearLayout) inflate(this.b, R.layout.switcher_frame_item, null);
        linearLayout8.addView(a7);
        linearLayout6.addView(linearLayout8, this.a);
        SwitcherView a8 = v.a(this.b, 6);
        LinearLayout linearLayout9 = (LinearLayout) inflate(this.b, R.layout.switcher_frame_item, null);
        linearLayout9.addView(a8);
        linearLayout6.addView(linearLayout9, this.a);
        SwitcherView a9 = v.a(this.b, 2);
        LinearLayout linearLayout10 = (LinearLayout) inflate(this.b, R.layout.switcher_frame_item, null);
        linearLayout10.addView(a9);
        linearLayout6.addView(linearLayout10, this.a);
        super.onFinishInflate();
    }
}
