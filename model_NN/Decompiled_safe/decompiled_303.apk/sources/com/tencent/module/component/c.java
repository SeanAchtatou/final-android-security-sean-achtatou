package com.tencent.module.component;

import android.content.Intent;
import android.view.View;
import com.tencent.launcher.base.BaseApp;
import com.tencent.qqlauncher.R;
import com.tencent.widget.f;

final class c implements View.OnClickListener {
    private /* synthetic */ l a;
    private /* synthetic */ f b;
    private /* synthetic */ TaskActivity c;

    c(TaskActivity taskActivity, l lVar, f fVar) {
        this.c = taskActivity;
        this.a = lVar;
        this.b = fVar;
    }

    public final void onClick(View view) {
        Intent d = this.a.d();
        if (d != null) {
            try {
                this.c.startActivity(d);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            BaseApp.a(this.c.getString(R.string.task_no_activity));
        }
        this.b.dismiss();
    }
}
