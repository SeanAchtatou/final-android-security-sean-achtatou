package com.tencent.module.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.tencent.module.switcher.SwitcherView;
import com.tencent.module.switcher.v;
import com.tencent.qqlauncher.R;
import com.tencent.widget.WidgetFrame;
import java.util.ArrayList;

public class SwitcherWidget extends WidgetFrame {
    private LinearLayout.LayoutParams a = new LinearLayout.LayoutParams(-2, -1);
    /* access modifiers changed from: private */
    public Context b;
    private View.OnClickListener c = new y(this);

    public SwitcherWidget(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = context;
        this.a.weight = 1.0f;
    }

    private View a(int i) {
        SwitcherView a2 = v.a(this.b, i);
        a2.setOnLongClickListener(null);
        LinearLayout linearLayout = (LinearLayout) inflate(this.b, R.layout.switcher_frame_item, null);
        linearLayout.addView(a2, this.a);
        return linearLayout;
    }

    public void addFocusables(ArrayList arrayList, int i) {
        super.addFocusables(arrayList, i);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.row1);
        linearLayout.addView(a(0), this.a);
        linearLayout.addView(a(4), this.a);
        linearLayout.addView(a(5), this.a);
        linearLayout.addView(a(8), this.a);
        findViewById(R.id.btn_more).setOnClickListener(this.c);
        findViewById(R.id.btn_more).setFocusable(true);
        super.onFinishInflate();
    }
}
