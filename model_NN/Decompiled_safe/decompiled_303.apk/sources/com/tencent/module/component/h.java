package com.tencent.module.component;

import android.content.DialogInterface;
import android.content.Intent;
import com.tencent.launcher.base.BaseApp;
import com.tencent.qqlauncher.R;
import com.tencent.util.p;

final class h implements DialogInterface.OnClickListener {
    private /* synthetic */ s a;
    private /* synthetic */ TaskLayout b;

    h(TaskLayout taskLayout, s sVar) {
        this.b = taskLayout;
        this.a = sVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case 0:
                this.b.a(this.a, true);
                return;
            case 1:
                Intent d = this.a.d();
                if (d != null) {
                    try {
                        this.b.g.startActivity(d);
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                } else {
                    BaseApp.a(this.b.h.getString(R.string.task_no_activity));
                    return;
                }
            case 2:
                this.b.g.startActivity(p.a(this.a.b()));
                return;
            case 3:
                ap apVar = new ap();
                apVar.b = this.a.b();
                if (m.b(apVar, this.b.g)) {
                    this.b.g.getContentResolver().delete(TaskComponentProvider.a, "packageName=?", new String[]{apVar.b});
                    BaseApp.a(this.b.h.getString(R.string.task_remove_exclude));
                    return;
                }
                m.a(apVar, this.b.g);
                BaseApp.a(this.b.h.getString(R.string.task_add_exclude));
                return;
            default:
                return;
        }
    }
}
