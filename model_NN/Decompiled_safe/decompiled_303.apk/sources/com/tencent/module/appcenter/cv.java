package com.tencent.module.appcenter;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

final class cv extends Handler {
    private /* synthetic */ SoftWareActivity a;

    cv(SoftWareActivity softWareActivity) {
        this.a = softWareActivity;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 2:
            case 4:
            default:
                return;
            case 3:
                Bitmap bitmap = (Bitmap) message.obj;
                if (this.a.picURL.size() == 1) {
                    this.a.infoSnapshot1.setImageBitmap(bitmap);
                    this.a.infoSnapshot2.setVisibility(8);
                    return;
                } else if (this.a.picURL.size() <= 1) {
                    return;
                } else {
                    if (message.arg1 == 0) {
                        this.a.infoSnapshot1.setImageBitmap(bitmap);
                        return;
                    } else if (message.arg1 == 1) {
                        this.a.infoSnapshot2.setImageBitmap(bitmap);
                        return;
                    } else {
                        return;
                    }
                }
            case 5:
                this.a.markListMoreItem.b();
                Toast.makeText(this.a, "no comments", 1).show();
                return;
            case 6:
                this.a.softwareIcon.setImageBitmap((Bitmap) message.obj);
                return;
        }
    }
}
