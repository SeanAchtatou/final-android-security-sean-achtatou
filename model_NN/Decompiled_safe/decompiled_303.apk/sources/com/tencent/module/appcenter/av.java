package com.tencent.module.appcenter;

import AndroidDLoader.Comment;
import android.view.View;
import android.widget.RatingBar;
import com.tencent.qqlauncher.R;

final class av implements View.OnClickListener {
    private /* synthetic */ SoftWareActivity a;

    av(SoftWareActivity softWareActivity) {
        this.a = softWareActivity;
    }

    public final void onClick(View view) {
        d.a();
        this.a.getString(R.string.default_user);
        d.a();
        String obj = this.a.commentContent.getText().toString();
        String str = obj.length() == 0 ? " " : obj;
        float rating = ((RatingBar) this.a.findViewById(R.id.edit_ratingBar)).getRating();
        Comment unused = this.a.userComment = new Comment();
        this.a.userComment.b = "";
        this.a.userComment.d = (byte) Math.round(rating);
        this.a.userComment.a = str;
        this.a.setWaitScreen((View) null);
    }
}
