package com.tencent.module.appcenter;

import android.content.SharedPreferences;

final class df {
    /* access modifiers changed from: private */
    public boolean a;
    /* access modifiers changed from: private */
    public boolean b;
    /* access modifiers changed from: private */
    public boolean c;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public int g;
    /* access modifiers changed from: private */
    public int h;
    private SharedPreferences.OnSharedPreferenceChangeListener i;
    private /* synthetic */ bf j;

    /* synthetic */ df(bf bfVar) {
        this(bfVar, (byte) 0);
    }

    private df(bf bfVar, byte b2) {
        this.j = bfVar;
        this.a = false;
        this.b = false;
        this.c = false;
        this.d = false;
        this.e = false;
        this.f = 2;
        this.g = -1;
        this.h = this.f;
        this.i = new dj(this);
    }

    public final void a() {
        SharedPreferences sharedPreferences = this.j.d.getSharedPreferences("com.tencent.android.qqdownload.settings", 0);
        this.a = sharedPreferences.getBoolean("StartDownloadAuto_preference", false);
        this.b = sharedPreferences.getBoolean("DeleteDownloadAuto_preference", false);
        this.c = sharedPreferences.getBoolean("AllowNonWifi_preference", true);
        this.d = sharedPreferences.getBoolean("LoadAppPic_preference", true);
        this.e = sharedPreferences.getBoolean("UpdateWarn_preference", true);
        try {
            this.f = Integer.parseInt(sharedPreferences.getString("max_download_thread_preference", "2"));
        } catch (Exception e2) {
            e2.printStackTrace();
            this.f = 2;
        }
        this.h = this.f;
        try {
            this.g = Integer.parseInt(sharedPreferences.getString("max_download_size_preference", "-1"));
        } catch (Exception e3) {
            e3.printStackTrace();
            this.g = -1;
        }
    }
}
