package com.tencent.module.appcenter;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Scroller;

public class WorkSpaceView extends FrameLayout {
    public boolean a;
    public boolean b;
    private int c;
    private int d;
    private int e;
    private float f;
    private float g;
    private Scroller h;
    private int i;
    private VelocityTracker j;
    private ac k;
    private boolean l;
    private boolean m;

    public WorkSpaceView(Context context) {
        this(context, null);
    }

    public WorkSpaceView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public WorkSpaceView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.d = 0;
        this.e = -1;
        this.i = 0;
        this.a = false;
        this.m = false;
        this.b = false;
        this.h = new Scroller(getContext());
        this.c = this.d;
    }

    private void a(int i2) {
        if (this.h.isFinished()) {
            int max = Math.max(0, Math.min(i2, getChildCount() - 1));
            boolean z = max != this.c;
            this.e = max;
            View focusedChild = getFocusedChild();
            if (focusedChild != null && z && focusedChild == getChildAt(this.c)) {
                focusedChild.clearFocus();
            }
            int width = (getWidth() * max) - getScrollX();
            this.h.startScroll(getScrollX(), 0, width, 0, Math.abs(width) * 2);
            if (this.k != null) {
                this.k.a(max);
            }
            invalidate();
        }
    }

    public final void a() {
        int width = getWidth();
        a((getScrollX() + (width / 2)) / width);
    }

    public final void a(ac acVar) {
        this.k = acVar;
    }

    public void computeScroll() {
        if (this.h.computeScrollOffset()) {
            scrollTo(this.h.getCurrX(), this.h.getCurrY());
            postInvalidate();
        } else if (this.e != -1) {
            this.c = Math.max(0, Math.min(this.e, getChildCount() - 1));
            this.e = -1;
        }
    }

    public boolean dispatchUnhandledMove(View view, int i2) {
        if (i2 == 17) {
            if (this.c > 0) {
                a(this.c - 1);
                return true;
            }
        } else if (i2 == 66 && this.c < getChildCount() - 1) {
            a(this.c + 1);
            return true;
        }
        return super.dispatchUnhandledMove(view, i2);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        switch (action) {
            case 0:
                this.f = x;
                this.g = y;
                this.l = true;
                this.i = this.h.isFinished() ? 0 : 1;
                break;
            case 1:
                this.i = 0;
                break;
            case 2:
                int abs = (int) Math.abs(x - this.f);
                int abs2 = (int) Math.abs(y - this.g);
                if (abs >= 15 || abs2 >= 15) {
                    if (abs <= abs2) {
                        this.i = 0;
                        break;
                    } else {
                        this.i = 1;
                        break;
                    }
                }
        }
        return this.i != 0;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        int i6 = 0;
        for (int i7 = 0; i7 < childCount; i7++) {
            View childAt = getChildAt(i7);
            if (childAt.getVisibility() != 8) {
                int measuredWidth = getMeasuredWidth();
                childAt.layout(i6, 0, i6 + measuredWidth, getMeasuredHeight());
                i6 += measuredWidth;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int childCount = getChildCount();
        for (int i4 = 0; i4 < childCount; i4++) {
            getChildAt(i4).measure(i2, i3);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int right;
        if (getChildCount() == 0) {
            return false;
        }
        if (this.j == null) {
            this.j = VelocityTracker.obtain();
        }
        this.j.addMovement(motionEvent);
        int action = motionEvent.getAction();
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        switch (action) {
            case 0:
                this.a = false;
                if (!this.h.isFinished()) {
                    this.h.abortAnimation();
                }
                this.f = x;
                this.m = false;
                this.b = false;
                this.i = 1;
                break;
            case 1:
                if (this.i == 1) {
                    VelocityTracker velocityTracker = this.j;
                    velocityTracker.computeCurrentVelocity(1000);
                    int xVelocity = (int) velocityTracker.getXVelocity();
                    if (xVelocity > 200 && this.c > 0) {
                        a(this.c - 1);
                    } else if (xVelocity >= -200 || this.c >= getChildCount() - 1) {
                        int width = getWidth();
                        a((getScrollX() + (width / 2)) / width);
                    } else {
                        a(this.c + 1);
                    }
                    if (this.j != null) {
                        this.j.recycle();
                        this.j = null;
                    }
                }
                this.a = false;
                break;
            case 2:
                if (this.i == 1) {
                    int i2 = (int) (this.f - x);
                    if (((int) Math.abs(y - this.g)) > 30 && !this.m) {
                        this.b = true;
                        this.a = false;
                        int width2 = getWidth();
                        a((getScrollX() + (width2 / 2)) / width2);
                        break;
                    } else {
                        if (Math.abs(i2) > 10 && !this.b) {
                            this.m = true;
                        }
                        this.f = x;
                        int scrollX = getScrollX();
                        if (i2 < 0) {
                            if (scrollX > 0) {
                                scrollBy(Math.max(-scrollX, i2), 0);
                            }
                        } else if (i2 > 0 && (right = (getChildAt(getChildCount() - 1).getRight() - scrollX) - getWidth()) > 0) {
                            scrollBy(Math.min(right, i2), 0);
                        }
                        this.a = true;
                        break;
                    }
                }
                break;
            case 3:
                this.i = 0;
                break;
        }
        return true;
    }

    public void requestChildFocus(View view, View view2) {
        super.requestChildFocus(view, view2);
        Rect rect = new Rect();
        view2.getDrawingRect(rect);
        view2.requestRectangleOnScreen(rect);
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z) {
        int indexOfChild = indexOfChild(view);
        if (indexOfChild == this.c && this.h.isFinished()) {
            return false;
        }
        a(indexOfChild);
        return true;
    }
}
