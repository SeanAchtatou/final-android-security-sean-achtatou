package com.tencent.module.appcenter;

import android.os.Message;
import com.tencent.module.download.b;

final class bc extends b {
    private /* synthetic */ SoftWareActivity a;

    bc(SoftWareActivity softWareActivity) {
        this.a = softWareActivity;
    }

    public final void a(String str) {
        Message.obtain(this.a.mHandler, 3, 0, 0, str).sendToTarget();
    }

    public final void a(String str, int i, int i2) {
        Message.obtain(this.a.mHandler, 1, i2, i, str).sendToTarget();
    }

    public final void a(String str, int i, String str2) {
        Message.obtain(this.a.mHandler, 0, i, 0, str2).sendToTarget();
    }

    public final void a(String str, String str2) {
        Message.obtain(this.a.mHandler, 2, 0, 0, new String[]{str, str2}).sendToTarget();
    }
}
