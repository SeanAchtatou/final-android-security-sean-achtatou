package com.tencent.module.appcenter;

import AndroidDLoader.Software;
import AndroidDLoader.a;
import android.app.Activity;
import android.view.View;

final class y implements View.OnClickListener {
    private /* synthetic */ bh a;

    y(bh bhVar) {
        this.a = bhVar;
    }

    public final void onClick(View view) {
        Object tag = view.getTag();
        if (tag != null && (tag instanceof Software)) {
            Software software = (Software) tag;
            SoftWareActivity.openSoftwareActivity((Activity) this.a.b, software.a, software.d, software.i, a.b);
            if (this.a.d == null || this.a.d.get(Integer.valueOf(software.a)) == null) {
                g.a().b(a.b);
            } else {
                g.a().b(a.i);
            }
        }
    }
}
