package com.tencent.module.appcenter;

import AndroidDLoader.Software;
import AndroidDLoader.a;
import android.view.View;

final class de implements View.OnClickListener {
    private /* synthetic */ dm a;

    de(dm dmVar) {
        this.a = dmVar;
    }

    public final void onClick(View view) {
        Object tag = view.getTag();
        if (tag != null && (tag instanceof Software)) {
            Software software = (Software) tag;
            SoftWareActivity.openSoftwareActivity(this.a.b, software.a, software.d, software.i, a.f);
        }
    }
}
