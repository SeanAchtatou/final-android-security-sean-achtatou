package com.tencent.module.appcenter;

final class s extends Thread {
    private boolean a = true;
    private /* synthetic */ ab b;

    s(ab abVar) {
        this.b = abVar;
    }

    public final void a() {
        this.a = false;
        synchronized (this.b.f) {
            this.b.f.notify();
        }
    }

    public final void run() {
        String str;
        byte[] bArr;
        while (this.a) {
            synchronized (this.b.f) {
                try {
                    this.b.f.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            while (true) {
                if (this.b.l.size() > 0) {
                    synchronized (this.b.l) {
                        str = (String) this.b.l.get(0);
                        bArr = (byte[]) this.b.m.get(str);
                        this.b.l.remove(0);
                        this.b.m.remove(str);
                    }
                    if (bArr != null && bArr.length > 0) {
                        ab.a(this.b, str, bArr);
                    }
                }
            }
            while (true) {
            }
        }
    }
}
