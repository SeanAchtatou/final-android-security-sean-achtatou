package com.tencent.module.appcenter;

import android.view.View;
import java.util.Iterator;

final class b implements View.OnClickListener {
    private /* synthetic */ TabFrame a;

    b(TabFrame tabFrame) {
        this.a = tabFrame;
    }

    public final void onClick(View view) {
        if (view != null) {
            Iterator it = this.a.e.iterator();
            while (it.hasNext()) {
                ch chVar = (ch) it.next();
                if (chVar.a() == view) {
                    this.a.b(chVar);
                }
            }
        }
    }
}
