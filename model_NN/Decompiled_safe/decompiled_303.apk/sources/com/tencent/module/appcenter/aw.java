package com.tencent.module.appcenter;

import android.os.Handler;
import android.widget.AbsListView;

final class aw implements be {
    private /* synthetic */ SoftWareActivity a;

    aw(SoftWareActivity softWareActivity) {
        this.a = softWareActivity;
    }

    public final void a(AbsListView absListView, int i) {
        if (absListView == this.a.relativeList) {
            SoftWareActivity softWareActivity = this.a;
            d a2 = d.a();
            Handler access$800 = this.a.handler;
            d.a();
            int unused = softWareActivity.requestId = a2.a(access$800, i, this.a.software.a, this.a.software.d, this.a.software.i);
        }
    }

    public final void b(AbsListView absListView, int i) {
        if (absListView == this.a.relativeList && this.a.swRelativeAdapter != null) {
            this.a.swRelativeAdapter.a = i == 0;
            if (this.a.swRelativeAdapter.a) {
                this.a.swRelativeAdapter.notifyDataSetChanged();
            }
        }
    }
}
