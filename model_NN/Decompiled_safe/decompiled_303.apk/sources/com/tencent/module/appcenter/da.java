package com.tencent.module.appcenter;

import AndroidDLoader.Comment;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;
import com.tencent.qqlauncher.R;
import java.util.List;

public final class da extends ArrayAdapter {
    private LayoutInflater a;
    private Context b;
    private int c = R.layout.software_mark_list_item;

    public da(Context context, List list) {
        super(context, (int) R.layout.software_mark_list_item, list);
        this.b = context;
        this.a = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    public final Context getContext() {
        return this.b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.a.inflate(this.c, viewGroup, false) : view;
        if (i % 2 != 0) {
            inflate.setBackgroundColor(this.b.getResources().getColor(R.color.background_for_all));
        } else {
            inflate.setBackgroundColor(-1181704);
        }
        Object item = getItem(i);
        if (item instanceof Comment) {
            ((RatingBar) inflate.findViewById(R.id.RatingBar01)).setRating((float) ((Comment) item).d);
            ((TextView) inflate.findViewById(R.id.TextQQNickName)).setText(((Comment) item).b);
            ((TextView) inflate.findViewById(R.id.TextCommentTime)).setText(((Comment) item).c);
            ((TextView) inflate.findViewById(R.id.TextCommentInfo)).setText(((Comment) item).a);
        }
        return inflate;
    }
}
