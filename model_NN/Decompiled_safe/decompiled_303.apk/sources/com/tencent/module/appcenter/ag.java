package com.tencent.module.appcenter;

final class ag {
    private final int a;
    private final int b;
    private int c;
    /* access modifiers changed from: private */
    public int d;
    private /* synthetic */ AutoBreakLineLayout e;

    public ag(AutoBreakLineLayout autoBreakLineLayout, int i, int i2) {
        this.e = autoBreakLineLayout;
        this.a = i;
        this.b = i2;
    }

    public final int a() {
        return this.d;
    }

    public final void a(int i, int i2) {
        this.c = this.c == 0 ? i : this.c + this.e.a + i;
        this.d = Math.max(this.d, i2);
    }

    public final boolean a(int i) {
        if (this.b != 0) {
            if ((this.c == 0 ? i : this.c + this.e.a + i) > this.a) {
                return true;
            }
        }
        return false;
    }

    public final int b() {
        return this.c;
    }
}
