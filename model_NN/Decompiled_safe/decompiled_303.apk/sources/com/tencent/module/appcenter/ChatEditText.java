package com.tencent.module.appcenter;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.EditText;

public class ChatEditText extends EditText {
    private String a = "Liu Ming ChatEditText";
    private boolean b = false;
    private Context c;

    public ChatEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = context;
    }

    public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
        return super.onCreateInputConnection(editorInfo);
    }

    public void onEditorAction(int i) {
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        return super.onKeyUp(i, keyEvent);
    }

    public boolean onPrivateIMECommand(String str, Bundle bundle) {
        return false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return false;
    }
}
