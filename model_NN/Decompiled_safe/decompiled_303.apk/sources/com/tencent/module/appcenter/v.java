package com.tencent.module.appcenter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

final class v extends Thread {
    private boolean a = true;
    private /* synthetic */ ab b;

    v(ab abVar) {
        this.b = abVar;
    }

    public final void a() {
        this.a = false;
        synchronized (this.b.e) {
            this.b.e.notify();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.module.appcenter.ab.a(com.tencent.module.appcenter.ab, boolean):java.lang.String
     arg types: [com.tencent.module.appcenter.ab, int]
     candidates:
      com.tencent.module.appcenter.ab.a(com.tencent.module.appcenter.ab, java.lang.String):void
      com.tencent.module.appcenter.ab.a(java.lang.String, android.graphics.Bitmap):void
      com.tencent.module.appcenter.ab.a(java.lang.String, java.lang.String):void
      com.tencent.module.appcenter.ab.a(java.lang.String, byte[]):void
      com.tencent.module.appcenter.ab.a(com.tencent.module.appcenter.ab, boolean):java.lang.String */
    public final void run() {
        String str;
        String str2;
        Bitmap bitmap;
        while (this.a) {
            synchronized (this.b.e) {
                try {
                    this.b.e.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            while (true) {
                if (this.b.j.size() > 0) {
                    synchronized (this.b.j) {
                        str = (String) this.b.j.get(0);
                        str2 = (String) this.b.k.get(str);
                        this.b.j.remove(str);
                        this.b.k.remove(str);
                    }
                    if (str2 != null) {
                        try {
                            if (str2.length() > 0) {
                                Bitmap bitmap2 = null;
                                try {
                                    bitmap2 = BitmapFactory.decodeFile(ab.a(this.b, false) + str2);
                                } catch (Exception e2) {
                                }
                                if (bitmap2 != null) {
                                    this.b.g.a(str, bitmap2);
                                    this.b.a(str, bitmap2);
                                } else {
                                    try {
                                        bitmap = BitmapFactory.decodeFile(ab.a(this.b, true) + str2);
                                    } catch (Exception e3) {
                                        bitmap = bitmap2;
                                    }
                                    if (bitmap != null) {
                                        this.b.g.a(str, bitmap);
                                        this.b.a(str, bitmap);
                                    } else {
                                        ab.a(this.b, str);
                                    }
                                }
                            }
                        } catch (OutOfMemoryError e4) {
                            e4.printStackTrace();
                        }
                    }
                }
            }
            while (true) {
            }
        }
    }
}
