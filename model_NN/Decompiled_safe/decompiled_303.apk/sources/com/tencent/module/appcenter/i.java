package com.tencent.module.appcenter;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.tencent.qqlauncher.R;

final class i extends Handler {
    private /* synthetic */ ShowSnapshotActivity a;

    i(ShowSnapshotActivity showSnapshotActivity) {
        this.a = showSnapshotActivity;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        if (message.what != 0) {
            this.a.processBar.clearAnimation();
            Toast.makeText(this.a, (int) R.string.get_snapshot_error, 0).show();
            return;
        }
        Object[] objArr = (Object[]) message.obj;
        Bitmap bitmap = (Bitmap) objArr[1];
        if (((String) objArr[0]).equalsIgnoreCase(this.a.picurl)) {
            this.a.imageView.setImageBitmap(bitmap);
            this.a.processBar.setVisibility(8);
            this.a.dismissProgressDialog();
        }
    }
}
