package com.tencent.module.appcenter;

import android.view.animation.Animation;
import android.widget.RelativeLayout;
import android.widget.TextView;

final class aq implements Animation.AnimationListener {
    private /* synthetic */ TextView a;
    private /* synthetic */ int b;
    private /* synthetic */ AppCenterActivity c;

    aq(AppCenterActivity appCenterActivity, TextView textView, int i) {
        this.c = appCenterActivity;
        this.a = textView;
        this.b = i;
    }

    public final void onAnimationEnd(Animation animation) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.leftMargin = this.a.getLeft() + this.c.hotWordX[this.b];
        layoutParams.topMargin = this.c.hotWordY[this.b] + this.a.getTop();
        layoutParams.addRule(10);
        layoutParams.addRule(9);
        this.a.setLayoutParams(layoutParams);
        this.a.clearAnimation();
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
