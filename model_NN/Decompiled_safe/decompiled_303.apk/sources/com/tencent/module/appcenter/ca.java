package com.tencent.module.appcenter;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;
import com.tencent.android.a.b;
import com.tencent.android.b.a.a;
import com.tencent.launcher.base.BaseApp;
import com.tencent.util.p;
import java.util.ArrayList;

final class ca extends Handler {
    private /* synthetic */ AppCenterActivity a;

    ca(AppCenterActivity appCenterActivity) {
        this.a = appCenterActivity;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case 900:
                Toast.makeText(BaseApp.b(), b.a(message.arg1), 0).show();
                return;
            case 1010:
            default:
                return;
            case 1401:
                Log.v("AppCenterActivity", "MSG_getStablePicAdv");
                this.a.stableAdCtrl.a((ArrayList) message.obj);
                return;
            case 1402:
                Log.v("AppCenterActivity", "MSG_getScrollablePicAdv");
                this.a.scrollAdCtrl.a((ArrayList) message.obj);
                return;
            case 1403:
                Log.v("AppCenterActivity", "MSG_getFlashScreen");
                String[] strArr = (String[]) message.obj;
                String str = strArr[0];
                String str2 = strArr[1];
                String str3 = strArr[2];
                Log.v("AppCenterActivity", "============== url:" + str + " startDate:" + str2 + " endDate:" + str3);
                if (str != null && str.length() != 0 && str2 != null && str2.length() != 0 && str3 != null && str3.length() != 0) {
                    a aVar = new a();
                    aVar.a = str;
                    aVar.c = p.b(str2);
                    aVar.d = p.b(str3);
                    com.tencent.android.c.a.a();
                    com.tencent.android.c.a.a(aVar);
                    d.a().a(str, this.a.iconHandler);
                    return;
                }
                return;
            case 1404:
                Log.v("AppCenterActivity", "MSG_getLoadingText");
                ArrayList arrayList = (ArrayList) message.obj;
                b.a(arrayList);
                com.tencent.android.c.a.a();
                com.tencent.android.c.a.a(arrayList);
                return;
        }
    }
}
