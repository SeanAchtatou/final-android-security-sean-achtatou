package com.tencent.module.appcenter;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;
import java.util.List;

public class AlertDialogAdapter extends Dialog {
    public int a = -1;
    public int b = -1;
    public int c = -1;
    public int d;
    private Context e = null;
    private View f = null;
    private Button g;
    private Button h;
    private int i = 0;
    private int j = 0;
    private List k = new ArrayList();
    private String l = "";
    private final int m = -2;
    private final int n = -1;
    private int o = -1;
    private int p = -1;

    public AlertDialogAdapter(Context context) {
        super(context, R.style.dialog);
        this.e = context;
    }

    public final void a(int i2) {
        this.o = i2;
    }

    public final void a(View.OnClickListener onClickListener, View.OnClickListener onClickListener2) {
        View inflate = ((LayoutInflater) this.e.getSystemService("layout_inflater")).inflate((int) R.layout.double_alert, (ViewGroup) findViewById(R.id.LinearLayout01));
        this.f = inflate;
        setContentView(this.f);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.dialog_icon);
        if (this.p > 0) {
            try {
                imageView.setImageResource(this.p);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        TextView textView = (TextView) inflate.findViewById(R.id.alertTitleTextView1);
        if (this.o != -1) {
            textView.setVisibility(8);
            ViewStub viewStub = (ViewStub) inflate.findViewById(R.id.viewstub);
            viewStub.setLayoutResource(this.o);
            viewStub.setInflatedId(-1);
            viewStub.setVisibility(0);
        } else {
            if (this.a > 0) {
                textView.setText(this.e.getResources().getString(this.a));
            }
            if (this.l != null && this.l.length() > 0) {
                textView.setText(this.l);
            }
        }
        ((TextView) inflate.findViewById(R.id.alertTitleTextView)).setText(this.e.getResources().getString(this.d));
        this.g = (Button) inflate.findViewById(R.id.AlertOKSubmit);
        if (this.b > 0) {
            try {
                this.g.setText(this.b);
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        this.g.setOnClickListener(onClickListener);
        this.h = (Button) inflate.findViewById(R.id.AlertCancelSubmit);
        if (this.c > 0) {
            try {
                this.h.setText(this.c);
            } catch (Exception e4) {
                e4.printStackTrace();
            }
        }
        this.h.setOnClickListener(onClickListener2);
    }
}
