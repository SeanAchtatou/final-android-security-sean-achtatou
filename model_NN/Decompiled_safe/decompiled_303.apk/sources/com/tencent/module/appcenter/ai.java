package com.tencent.module.appcenter;

import AndroidDLoader.ScrollablePicAdv;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;

final class ai extends Handler {
    private /* synthetic */ a a;

    ai(a aVar) {
        this.a = aVar;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        Object[] objArr = (Object[]) message.obj;
        String str = (String) objArr[0];
        Bitmap bitmap = (Bitmap) objArr[1];
        for (int i = 0; i < this.a.b.size(); i++) {
            if (((ScrollablePicAdv) this.a.b.get(i)).a.equals(str)) {
                ((ImageView) this.a.e.get(i)).setImageBitmap(bitmap);
                return;
            }
        }
    }
}
