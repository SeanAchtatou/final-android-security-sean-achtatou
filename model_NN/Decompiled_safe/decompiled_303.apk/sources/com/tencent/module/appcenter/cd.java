package com.tencent.module.appcenter;

import android.view.View;
import com.tencent.qqlauncher.R;

final class cd implements View.OnClickListener {
    private /* synthetic */ AppCenterActivity a;

    cd(AppCenterActivity appCenterActivity) {
        this.a = appCenterActivity;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.recommend_layout /*2131492989*/:
                this.a.setSelectMainTabIndex(0);
                return;
            case R.id.lastest_layout /*2131492993*/:
                this.a.setSelectMainTabIndex(1);
                return;
            case R.id.top_layout /*2131492997*/:
                this.a.setSelectMainTabIndex(2);
                return;
            default:
                return;
        }
    }
}
