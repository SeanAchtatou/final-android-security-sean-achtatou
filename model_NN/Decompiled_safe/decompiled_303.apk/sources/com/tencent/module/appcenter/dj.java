package com.tencent.module.appcenter;

import android.content.SharedPreferences;

final class dj implements SharedPreferences.OnSharedPreferenceChangeListener {
    private /* synthetic */ df a;

    dj(df dfVar) {
        this.a = dfVar;
    }

    public final void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        boolean unused = this.a.a = sharedPreferences.getBoolean("StartDownloadAuto_preference", false);
        boolean unused2 = this.a.b = sharedPreferences.getBoolean("DeleteDownloadAuto_preference", false);
        boolean unused3 = this.a.c = sharedPreferences.getBoolean("AllowNonWifi_preference", true);
        boolean unused4 = this.a.d = sharedPreferences.getBoolean("LoadAppPic_preference", true);
        boolean unused5 = this.a.e = sharedPreferences.getBoolean("UpdateWarn_preference", true);
        try {
            int unused6 = this.a.f = Integer.parseInt(sharedPreferences.getString("max_download_thread_preference", "2"));
        } catch (Exception e) {
            int unused7 = this.a.f = 2;
        }
        if (this.a.h != this.a.f) {
            d.a();
            int unused8 = this.a.h = this.a.f;
        }
        try {
            int unused9 = this.a.g = Integer.parseInt(sharedPreferences.getString("max_download_size_preference", "-1"));
        } catch (Exception e2) {
            int unused10 = this.a.g = -1;
        }
    }
}
