package com.tencent.module.appcenter;

import AndroidDLoader.ResCategoryPack;
import AndroidDLoader.ResSoftDetailPack;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.tencent.android.net.a;
import com.tencent.android.net.c;
import com.tencent.android.net.e;
import com.tencent.android.ui.a.ae;
import com.tencent.launcher.base.BaseApp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public final class d implements com.tencent.android.net.d, e, c {
    private static d b;
    private a c;
    private com.tencent.android.net.a.a d;
    private ab e;
    private c f;
    private g g;
    private ae h;

    private d() {
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.h = null;
        this.c = a.a(this);
        this.d = com.tencent.android.net.a.a.a();
        this.e = ab.a(BaseApp.b());
        this.f = c.a(this);
        this.g = g.a();
        this.g.b();
    }

    public static d a() {
        if (b == null) {
            b = new d();
        }
        return b;
    }

    public final int a(int i) {
        return this.d.a(i);
    }

    public final int a(Handler handler) {
        return this.c.a(handler, (ArrayList) null);
    }

    public final int a(Handler handler, int i, int i2, int i3) {
        return this.c.a(handler, i, i2, i3);
    }

    public final int a(Handler handler, int i, int i2, int i3, int i4) {
        return this.c.a(handler, i, i2, i3, i4);
    }

    public final int a(Handler handler, String str, int i) {
        return this.c.a(handler, str, i);
    }

    public final int a(Handler handler, String str, int i, int i2) {
        return this.c.a(handler, str, i, i2);
    }

    public final int a(Map map) {
        return this.c.a(map);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.module.appcenter.ab.a(java.lang.String, android.os.Handler, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, android.os.Handler, int]
     candidates:
      com.tencent.module.appcenter.ab.a(com.tencent.module.appcenter.ab, java.lang.String, android.graphics.Bitmap):void
      com.tencent.module.appcenter.ab.a(com.tencent.module.appcenter.ab, java.lang.String, byte[]):void
      com.tencent.module.appcenter.ab.a(java.lang.String, android.os.Handler, boolean):android.graphics.Bitmap */
    public final Bitmap a(String str, Handler handler) {
        return this.e.a(str, handler, true);
    }

    public final Bitmap a(String str, Handler handler, boolean z) {
        return this.e.a(str, handler, z);
    }

    public final void a(Handler handler, byte b2, String str, String str2, String str3) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 1000;
            obtain.arg1 = b2;
            obtain.obj = new String[]{str, str2, str3};
            handler.sendMessage(obtain);
        }
    }

    public final void a(Handler handler, int i) {
        Log.v("HTTP", "Error: onReceiveHandshakeError" + i);
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 3104;
            obtain.arg1 = i;
            handler.sendMessage(obtain);
        }
    }

    public final void a(Handler handler, int i, int i2) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 1309;
            obtain.arg1 = i;
            obtain.arg2 = i2;
            handler.sendMessage(obtain);
        }
    }

    public final void a(Handler handler, int i, int i2, String str) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 900;
            obtain.arg1 = i2;
            obtain.arg2 = i;
            obtain.obj = str;
            handler.sendMessage(obtain);
            this.h = null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.net.a.a(android.os.Handler, java.util.List):int
     arg types: [android.os.Handler, java.util.ArrayList]
     candidates:
      com.tencent.android.net.a.a(android.os.Handler, int):int
      com.tencent.android.net.a.a(android.os.Handler, java.util.ArrayList):int
      com.tencent.android.net.a.a(android.os.Handler, java.util.List):int */
    public final void a(Handler handler, ResCategoryPack resCategoryPack) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 1004;
            obtain.obj = resCategoryPack;
            handler.sendMessage(obtain);
        }
        this.c.a(handler, (List) resCategoryPack.b);
    }

    public final void a(Handler handler, ResSoftDetailPack resSoftDetailPack) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 1006;
            obtain.obj = resSoftDetailPack;
            handler.sendMessage(obtain);
        }
    }

    public final void a(Handler handler, String str, String str2, String str3) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 1403;
            obtain.obj = new String[]{str, str2, str3};
            handler.sendMessage(obtain);
        }
    }

    public final void a(Handler handler, ArrayList arrayList) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 1405;
            obtain.obj = arrayList;
            handler.sendMessage(obtain);
        }
    }

    public final void a(Handler handler, ArrayList arrayList, int i, int i2) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 1310;
            obtain.arg1 = i;
            obtain.arg2 = i2;
            obtain.obj = arrayList;
            handler.sendMessage(obtain);
        }
    }

    public final void a(Handler handler, ArrayList arrayList, ae aeVar) {
        this.h = aeVar;
        this.c.a(handler, arrayList);
    }

    public final void a(Handler handler, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 2100;
            obtain.obj = new Object[]{arrayList, arrayList2, arrayList3};
            handler.sendMessage(obtain);
        }
    }

    public final void a(Handler handler, List list) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 1003;
            obtain.obj = list;
            handler.sendMessage(obtain);
        }
    }

    public final void a(Handler handler, List list, List list2, int i, String str) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 1005;
            Object[] objArr = new Object[4];
            for (int i2 = 0; i2 < objArr.length; i2++) {
                objArr[i2] = new Object();
            }
            objArr[0] = list;
            objArr[1] = list2;
            objArr[2] = new Integer(i);
            objArr[3] = str;
            obtain.obj = objArr;
            handler.sendMessage(obtain);
        }
    }

    public final void a(Handler handler, Map map) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 1010;
            obtain.obj = null;
            handler.sendMessage(obtain);
        }
        if (this.h != null) {
            this.h.a(map);
            this.h = null;
        }
    }

    public final void a(String str) {
        this.e.a(str);
    }

    public final synchronized void a(String str, byte[] bArr) {
        this.e.a(str, bArr);
    }

    public final int b(Handler handler) {
        return this.c.a(handler);
    }

    public final int b(Handler handler, int i, int i2) {
        return this.c.a(handler, i, i2);
    }

    public final int b(Handler handler, int i, int i2, int i3) {
        return this.c.b(handler, i, i2, i3);
    }

    public final int b(String str) {
        return this.f.a(str);
    }

    public final void b() {
        this.c.a();
        this.d.b();
        this.e.a();
        this.f.a();
        this.g.c();
        b = null;
        this.h = null;
    }

    public final void b(Handler handler, int i) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 2200;
            obtain.arg1 = i;
            handler.sendMessage(obtain);
        }
        Log.v("HTTP", "Error: onReceiveCategorisError" + i);
    }

    public final void b(Handler handler, ArrayList arrayList) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 1406;
            obtain.obj = arrayList;
            handler.sendMessage(obtain);
        }
    }

    public final int c(Handler handler) {
        return this.c.c(handler);
    }

    public final void c() {
        this.g.e();
    }

    public final void c(Handler handler, int i) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 2200;
            obtain.arg1 = i;
            handler.sendMessage(obtain);
        }
        Log.v("HTTP", "Error: onReceiveHotWordsError" + i);
    }

    public final void c(Handler handler, ArrayList arrayList) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 1407;
            obtain.obj = arrayList;
            handler.sendMessage(obtain);
        }
    }

    public final int d(Handler handler) {
        return this.c.d(handler);
    }

    public final void d() {
        this.g.g();
    }

    public final void d(Handler handler, int i) {
        Log.v("HTTP", "Error: onReceiveGetFlashScreenError" + i);
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 3101;
            obtain.arg1 = i;
            handler.sendMessage(obtain);
        }
    }

    public final void d(Handler handler, ArrayList arrayList) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 2300;
            obtain.obj = arrayList;
            handler.sendMessage(obtain);
        }
    }

    public final int e(Handler handler) {
        return this.c.e(handler);
    }

    public final void e(Handler handler, int i) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 2200;
            obtain.arg1 = i;
            handler.sendMessage(obtain);
        }
    }

    public final int f(Handler handler) {
        return this.c.b(handler);
    }

    public final void f(Handler handler, int i) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 2200;
            obtain.arg1 = i;
            handler.sendMessage(obtain);
        }
    }

    /* access modifiers changed from: protected */
    public final void finalize() {
        super.finalize();
    }

    public final void g(Handler handler, int i) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 2200;
            obtain.arg1 = i;
            handler.sendMessage(obtain);
        }
    }

    public final void h(Handler handler, int i) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 2200;
            obtain.arg1 = i;
            handler.sendMessage(obtain);
        }
    }

    public final void i(Handler handler, int i) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 2200;
            obtain.arg1 = i;
            handler.sendMessage(obtain);
        }
    }

    public final void j(Handler handler, int i) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 1312;
            obtain.arg1 = i;
            handler.sendMessage(obtain);
        }
        Log.v("HTTP", "Error: onReceiveAddLabelError" + i);
    }

    public final void k(Handler handler, int i) {
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 2200;
            obtain.arg1 = i;
            handler.sendMessage(obtain);
        }
        Log.v("HTTP", "Error: onReceiveSearchByLabelError" + i);
    }

    public final int l(Handler handler, int i) {
        return this.c.b(handler, i);
    }

    public final int m(Handler handler, int i) {
        return this.c.d(handler, i);
    }

    public final int n(Handler handler, int i) {
        return this.c.e(handler, i);
    }

    public final int o(Handler handler, int i) {
        return this.c.a(handler, i);
    }

    public final int p(Handler handler, int i) {
        return this.c.c(handler, i);
    }

    public final void q(Handler handler, int i) {
        this.g.f();
        SharedPreferences sharedPreferences = BaseApp.b().getSharedPreferences("reportStats", 0);
        sharedPreferences.edit().putString("stats_date", new SimpleDateFormat("yyyy-MM-dd").format(new Date())).commit();
        if (handler != null) {
            Message obtain = Message.obtain();
            obtain.what = 2000;
            obtain.arg1 = i;
            handler.sendMessage(obtain);
        }
    }
}
