package com.tencent.module.appcenter;

import android.view.View;
import android.widget.Toast;
import com.tencent.qqlauncher.R;

final class cs implements View.OnClickListener {
    private /* synthetic */ SoftWareActivity a;

    cs(SoftWareActivity softWareActivity) {
        this.a = softWareActivity;
    }

    public final void onClick(View view) {
        String unused = this.a.addLabelName = this.a.infoEditTags.getText().toString();
        if (this.a.addLabelName.length() > 5) {
            Toast.makeText(this.a, (int) R.string.max_length_five, 1).show();
        } else if (this.a.addLabelName.trim().length() == 0) {
            Toast.makeText(this.a, (int) R.string.input_should_not_empty, 1).show();
        } else {
            int unused2 = this.a.requestId = d.a().a(this.a.handler, this.a.addLabelName, this.a.mProductid, this.a.mSoftid);
            this.a.setWaitScreen((View) null);
        }
    }
}
