package com.tencent.module.appcenter;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class QQAppMarketService extends Service {
    private final IBinder a = new bu(this);
    private d b;
    private bf c;

    public IBinder onBind(Intent intent) {
        Log.v("Service", "Service onBind");
        return this.a;
    }

    public void onCreate() {
        Log.v("Service", "Service onCreate0");
        super.onCreate();
        Log.v("Service", "Service onCreate1");
        this.c = new bf(this);
        Log.v("Service", "Service onCreate2");
        this.b = d.a();
        Log.v("Service", "Service onCreate3");
    }

    public void onDestroy() {
        this.b.b();
        this.c.a();
        Log.v("Service", "Service onDestroy");
        super.onDestroy();
        new aa(this).start();
    }

    public void onStart(Intent intent, int i) {
        Log.v("Service", "Service onStart");
        super.onStart(intent, i);
    }
}
