package com.tencent.module.appcenter;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class SearchLayout extends TextView {
    public SearchLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }
}
