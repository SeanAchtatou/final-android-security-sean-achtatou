package com.tencent.module.appcenter;

import AndroidDLoader.Software;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import com.tencent.qqlauncher.R;
import com.tencent.util.p;
import java.util.ArrayList;

public final class dm extends BaseAdapter {
    public boolean a = true;
    /* access modifiers changed from: private */
    public TActivity b = null;
    private ArrayList c = null;
    private ImageView d = null;
    private TextView e = null;
    private TextView f = null;
    private TextView g = null;
    private TextView h = null;
    private RatingBar i = null;
    private Handler j = new dd(this);
    private View.OnClickListener k = new de(this);
    private /* synthetic */ SearchTagActivity l;

    public dm(SearchTagActivity searchTagActivity, TActivity tActivity) {
        this.l = searchTagActivity;
        this.b = tActivity;
    }

    public final void a(ArrayList arrayList) {
        if (this.c == null) {
            this.c = new ArrayList();
        }
        if (arrayList != null) {
            this.c.addAll(arrayList);
        }
    }

    public final int getCount() {
        if (this.c == null) {
            return 0;
        }
        return this.c.size();
    }

    public final Object getItem(int i2) {
        if (this.c == null) {
            return null;
        }
        return this.c.get(i2);
    }

    public final long getItemId(int i2) {
        return (long) i2;
    }

    public final View getView(int i2, View view, ViewGroup viewGroup) {
        View inflate = view == null ? LayoutInflater.from(this.b).inflate((int) R.layout.general_software_list_item, (ViewGroup) null) : view;
        if (this.c != null && i2 < this.c.size()) {
            Software software = (Software) this.c.get(i2);
            inflate.setTag(software);
            inflate.setOnClickListener(this.k);
            this.d = (ImageView) inflate.findViewById(R.id.software_icon);
            this.e = (TextView) inflate.findViewById(R.id.software_item_name);
            this.e.setSelected(true);
            this.g = (TextView) inflate.findViewById(R.id.share_way);
            this.h = (TextView) inflate.findViewById(R.id.software_fees);
            this.i = (RatingBar) inflate.findViewById(R.id.RatingBar01);
            Bitmap a2 = d.a().a(software.c, this.j, this.a);
            if (a2 != null) {
                this.d.setImageBitmap(a2);
            } else {
                this.d.setImageResource(R.drawable.sw_default_icon);
            }
            this.e.setText(software.b);
            this.g.setText(p.a(software.k()));
            this.h.setText(software.g);
            this.i.setRating((float) (software.m / 2));
        }
        return inflate;
    }
}
