package com.tencent.module.appcenter;

import android.os.Handler;
import android.os.Message;
import java.util.List;

final class ao extends Handler {
    private /* synthetic */ AppCenterActivity a;

    ao(AppCenterActivity appCenterActivity) {
        this.a = appCenterActivity;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case 900:
                this.a.dismissProgressDialog();
                return;
            case 1003:
                this.a.onReceiveHotWords((List) message.obj);
                break;
            case 1005:
                Object[] objArr = (Object[]) message.obj;
                List list = (List) objArr[0];
                int unused = this.a.totalCount = ((Integer) objArr[2]).intValue();
                if (list.size() == 0) {
                    this.a.mListView.removeFooterView(this.a.mFootLoadingView);
                    boolean unused2 = this.a.bAllHttpMsgReceived = true;
                }
                for (int i = 0; i < this.a.totalCount; i++) {
                    this.a.mNetAppList.add(list.get(i));
                }
                this.a.mNetAdapter.notifyDataSetChanged();
                AppCenterActivity.access$1512(this.a, list.size());
                this.a.mSearchCountView.setText(this.a.nAppCount + "条关于" + this.a.getTextFilter() + "的搜索结果");
                break;
            case 2200:
                break;
            default:
                return;
        }
        this.a.dismissProgressDialog();
    }
}
