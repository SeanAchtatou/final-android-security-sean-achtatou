package com.tencent.module.appcenter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.tencent.qqlauncher.R;
import com.tencent.util.p;

public abstract class TActivity extends Activity {
    public static final String EXIT_ACTION = "com.tencent.android.qqdownloader.EXIT_ACTION";
    private static final String IS_PROGRESS_DIALOG_SHOWING = "TACTIVITY_IS_PROGRESS_DIALOG_SHOWING";
    private static final String IS_TIMEOUT_DIALOG_SHOWING = "TACTIVITY_IS_TIMEOUT_DIALOG_SHOWING";
    private static final int MSG_RETRY = 1;
    private static final int MSG_TIMEOUT = 0;
    private static final String TAG = "TActivity";
    protected static boolean lastConfigurationChanged = false;
    public final int MAX_RETRY_TIMES = (this.WAIT_TIME_IN_MS.length - 1);
    public final int[] WAIT_TIME_IN_MS = {30000, 30000, 30000};
    protected bf dataManager;
    /* access modifiers changed from: private */
    public AlertDialogAdapter mAlertDialog = null;
    private ServiceConnection mConnection = new r(this);
    View mContentView;
    protected d mCtrl;
    private BroadcastReceiver mExitAppReceiver = new q(this);
    private boolean mIsBound;
    private ProgressDialog mProgressbarDialog = null;
    private int mRetryTimes = 0;
    private int processbarStringId = -1;
    DialogInterface.OnKeyListener progressDialogKeyListenner = new m(this);
    Handler toasthandler;

    static /* synthetic */ void access$000(TActivity tActivity) {
        if (!tActivity.onRetry()) {
            tActivity.setWaitScreen(tActivity.mContentView);
        }
    }

    private void handleRetry() {
        if (!onRetry()) {
            setWaitScreen(this.mContentView);
        }
    }

    public void dismissProgressDialog() {
        if (this.mProgressbarDialog != null && this.mProgressbarDialog.isShowing()) {
            try {
                this.mProgressbarDialog.dismiss();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        Log.v(TAG, "end timeout-----------------dismissProgressDialog");
    }

    /* access modifiers changed from: package-private */
    public void doBindService() {
    }

    /* access modifiers changed from: package-private */
    public void doUnbindService() {
        if (this.mIsBound) {
            this.mIsBound = false;
        }
    }

    public int getCurrentRetryWaitTimeInMilSeconds() {
        int i;
        int[] iArr = this.WAIT_TIME_IN_MS;
        if (this.mRetryTimes < this.MAX_RETRY_TIMES) {
            i = this.mRetryTimes;
            this.mRetryTimes = i + 1;
        } else {
            i = this.mRetryTimes;
        }
        return iArr[i];
    }

    public bf getDataManager() {
        return this.dataManager;
    }

    public d getMainontroller() {
        return this.mCtrl;
    }

    /* access modifiers changed from: protected */
    public synchronized DialogInterface.OnKeyListener getProgressDialogKeyListenner() {
        return this.progressDialogKeyListenner;
    }

    public void onBindSuccess() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().requestFeature(1);
        doBindService();
        registerReceiver(this.mExitAppReceiver, new IntentFilter(EXIT_ACTION));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        doUnbindService();
        unregisterReceiver(this.mExitAppReceiver);
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        Log.v(TAG, "end timeout-----------------onPause");
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public boolean onProgressDialogBack() {
        return true;
    }

    public void onReceivedBlankDataFromServer(int i) {
        int i2 = i == -1 ? R.string.Error_Server_ReturnNull : i;
        if (this.toasthandler == null) {
            this.toasthandler = new p(this, i2);
        }
        this.toasthandler.removeMessages(0);
        this.toasthandler.sendEmptyMessageDelayed(0, 500);
        setNormalContentView();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        lastConfigurationChanged = false;
        this.mRetryTimes = 0;
    }

    public abstract boolean onRetry();

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean(IS_TIMEOUT_DIALOG_SHOWING, false);
        if (this.mProgressbarDialog != null) {
            bundle.putBoolean(IS_PROGRESS_DIALOG_SHOWING, this.mProgressbarDialog.isShowing());
        } else {
            bundle.putBoolean(IS_PROGRESS_DIALOG_SHOWING, false);
        }
        super.onSaveInstanceState(bundle);
    }

    public abstract boolean onTimeout();

    public void openinstallApk(String str) {
    }

    public void setNormalContentView() {
        setNormalContentView(this.mContentView);
    }

    public void setNormalContentView(View view) {
        this.mRetryTimes = 0;
        dismissProgressDialog();
        if (view != null) {
            setContentView(view);
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void setProgressDialogKeyListenner(DialogInterface.OnKeyListener onKeyListener) {
        this.progressDialogKeyListenner = onKeyListener;
    }

    public void setWaitScreen(int i) {
        this.processbarStringId = i;
        setWaitScreen((View) null);
    }

    public void setWaitScreen(View view) {
        if ((this.mProgressbarDialog == null || !this.mProgressbarDialog.isShowing()) && !lastConfigurationChanged) {
            showProgressDialog();
        }
    }

    /* access modifiers changed from: package-private */
    public void showProgressDialog() {
        this.mProgressbarDialog = new ProgressDialog(this);
        this.mProgressbarDialog.setOnKeyListener(getProgressDialogKeyListenner());
        this.mProgressbarDialog.show();
        this.mProgressbarDialog.setContentView((int) R.layout.progress_dialog);
        TextView textView = (TextView) this.mProgressbarDialog.findViewById(R.id.ProgressBarTextView);
        if (this.processbarStringId != -1) {
            textView.setText(this.processbarStringId);
            this.processbarStringId = -1;
            return;
        }
        String a = p.a();
        if (a != null && a.length() > 0) {
            textView.setText(a);
        }
    }

    /* access modifiers changed from: package-private */
    public void showTimeoutDialog() {
        this.mAlertDialog = new AlertDialogAdapter(this);
        this.mAlertDialog.d = R.string.msg_timeout;
        this.mAlertDialog.a = R.string.msg_timeout_content;
        AlertDialogAdapter alertDialogAdapter = this.mAlertDialog;
        alertDialogAdapter.b = R.string.msg_timeout_retry;
        alertDialogAdapter.c = R.string.msg_timeout_cancel;
        this.mAlertDialog.a(new o(this), new n(this));
        this.mAlertDialog.show();
    }
}
