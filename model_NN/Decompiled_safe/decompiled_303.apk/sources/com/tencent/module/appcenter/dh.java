package com.tencent.module.appcenter;

import android.view.View;
import android.widget.EditText;
import com.tencent.qqlauncher.R;

final class dh implements View.OnClickListener {
    private /* synthetic */ AlertDialogAdapter a;
    private /* synthetic */ SoftWareActivity b;

    dh(SoftWareActivity softWareActivity, AlertDialogAdapter alertDialogAdapter) {
        this.b = softWareActivity;
        this.a = alertDialogAdapter;
    }

    public final void onClick(View view) {
        String obj = ((EditText) this.a.findViewById(R.id.add_label)).getText().toString();
        if (obj != null && obj.length() > 0) {
            this.a.dismiss();
            String unused = this.b.addLabelName = obj;
            d.a();
            this.b.setWaitScreen((View) null);
        }
    }
}
