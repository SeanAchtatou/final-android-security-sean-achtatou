package com.tencent.module.appcenter;

import AndroidDLoader.Software;
import AndroidDLoader.a;
import android.app.Activity;
import android.view.View;

final class bj implements View.OnClickListener {
    private /* synthetic */ bg a;

    bj(bg bgVar) {
        this.a = bgVar;
    }

    public final void onClick(View view) {
        Object tag = view.getTag();
        if (tag != null && (tag instanceof Software)) {
            a aVar = a.a;
            if (this.a.j == 0) {
                aVar = a.d;
            } else if (this.a.j == 1) {
                aVar = a.e;
            }
            Software software = (Software) tag;
            SoftWareActivity.openSoftwareActivity((Activity) this.a.b, software.a, software.d, software.i, aVar);
            g.a().b(aVar);
        }
    }
}
