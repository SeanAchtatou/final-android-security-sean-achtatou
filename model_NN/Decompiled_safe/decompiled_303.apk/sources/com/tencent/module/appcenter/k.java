package com.tencent.module.appcenter;

import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.tencent.android.a.b;
import com.tencent.launcher.base.BaseApp;
import java.util.ArrayList;

final class k extends Handler {
    private /* synthetic */ SearchTagActivity a;

    k(SearchTagActivity searchTagActivity) {
        this.a = searchTagActivity;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 900:
            case 2200:
                this.a.dismissProgressDialog();
                Toast.makeText(BaseApp.b(), b.a(message.arg1), 0).show();
                if (this.a.listMoreItem != null) {
                    this.a.listMoreItem.a();
                    return;
                }
                return;
            case 1310:
                this.a.dismissProgressDialog();
                this.a.onReceiveSearchResults((ArrayList) message.obj);
                return;
            default:
                this.a.dismissProgressDialog();
                Toast.makeText(BaseApp.b(), b.a(message.arg1), 0).show();
                if (this.a.listMoreItem != null) {
                    this.a.listMoreItem.a();
                    return;
                }
                return;
        }
    }
}
