package com.tencent.module.appcenter;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.tencent.qqlauncher.R;

public class ControlledScrollView extends LinearLayout implements ac {
    private int a = R.drawable.round;
    private int b = this.a;
    private WorkSpaceView c;
    private Context d;
    private LinearLayout e;
    private boolean f = true;

    public ControlledScrollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setOrientation(1);
        this.d = context;
        this.c = new WorkSpaceView(this.d);
        this.c.a(this);
        this.c.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.e = new LinearLayout(this.d);
        this.e.setOrientation(0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 81;
        this.e.setPadding(0, 11, 0, 11);
        this.e.setLayoutParams(layoutParams);
        super.addView(this.c);
        super.addView(this.e);
    }

    public final void a(int i) {
    }

    public final void a(View view) {
        this.c.addView(view);
    }

    public void requestLayout() {
        super.requestLayout();
    }
}
