package com.tencent.module.appcenter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import com.tencent.android.a.b;
import com.tencent.android.c.a;
import java.io.File;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

public final class ab {
    private static ab a = null;
    private static String h = "/Tencent/QQDownload/Icon/";
    private static String i = "/Tencent/QQDownload/IconCache/";
    private Context b = null;
    private Map c = new ConcurrentHashMap();
    private Vector d = new Vector();
    /* access modifiers changed from: private */
    public Object e = new Object();
    /* access modifiers changed from: private */
    public Object f = new Object();
    /* access modifiers changed from: private */
    public l g;
    /* access modifiers changed from: private */
    public Vector j = new Vector();
    /* access modifiers changed from: private */
    public Map k = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    public Vector l = new Vector();
    /* access modifiers changed from: private */
    public Map m = new ConcurrentHashMap();
    private v n = null;
    private s o = null;
    private int p = 1000;

    private ab(Context context) {
        this.b = context;
        this.g = new l(this);
        this.n = new v(this);
        this.n.start();
        this.o = new s(this);
        this.o.start();
        new e(this).start();
    }

    protected static ab a(Context context) {
        if (a == null) {
            a = new ab(context);
        }
        return a;
    }

    static /* synthetic */ String a(ab abVar, boolean z) {
        return z ? b.a(abVar.b, i) : b.a(abVar.b, h);
    }

    static /* synthetic */ void a(ab abVar, String str) {
        if (!abVar.d.contains(str)) {
            abVar.d.add(str);
            d.a().b(str);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x007b A[SYNTHETIC, Splitter:B:24:0x007b] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0088 A[SYNTHETIC, Splitter:B:31:0x0088] */
    /* JADX WARNING: Removed duplicated region for block: B:40:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void a(com.tencent.module.appcenter.ab r5, java.lang.String r6, byte[] r7) {
        /*
            java.lang.String r0 = b(r6)
            int r1 = r7.length
            r2 = 16384(0x4000, float:2.2959E-41)
            if (r1 >= r2) goto L_0x0052
            android.content.Context r1 = r5.b
            java.lang.String r2 = com.tencent.module.appcenter.ab.h
            java.lang.String r1 = com.tencent.android.a.b.a(r1, r2)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0025:
            r2 = 0
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x0074, all -> 0x0084 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x0074, all -> 0x0084 }
            boolean r0 = r3.exists()     // Catch:{ Exception -> 0x0074, all -> 0x0084 }
            if (r0 != 0) goto L_0x0034
            r3.mkdirs()     // Catch:{ Exception -> 0x0074, all -> 0x0084 }
        L_0x0034:
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0074, all -> 0x0084 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0074, all -> 0x0084 }
            boolean r1 = r0.exists()     // Catch:{ Exception -> 0x0074, all -> 0x0084 }
            if (r1 == 0) goto L_0x0042
            r0.delete()     // Catch:{ Exception -> 0x0074, all -> 0x0084 }
        L_0x0042:
            r0.createNewFile()     // Catch:{ Exception -> 0x0074, all -> 0x0084 }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0074, all -> 0x0084 }
            r3 = 1
            r1.<init>(r0, r3)     // Catch:{ Exception -> 0x0074, all -> 0x0084 }
            r1.write(r7)     // Catch:{ Exception -> 0x0093 }
            r1.close()     // Catch:{ IOException -> 0x006f }
        L_0x0051:
            return
        L_0x0052:
            android.content.Context r1 = r5.b
            java.lang.String r2 = com.tencent.module.appcenter.ab.i
            java.lang.String r1 = com.tencent.android.a.b.a(r1, r2)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0025
        L_0x006f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0051
        L_0x0074:
            r0 = move-exception
            r1 = r2
        L_0x0076:
            r0.printStackTrace()     // Catch:{ all -> 0x0091 }
            if (r1 == 0) goto L_0x0051
            r1.close()     // Catch:{ IOException -> 0x007f }
            goto L_0x0051
        L_0x007f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0051
        L_0x0084:
            r0 = move-exception
            r1 = r2
        L_0x0086:
            if (r1 == 0) goto L_0x008b
            r1.close()     // Catch:{ IOException -> 0x008c }
        L_0x008b:
            throw r0
        L_0x008c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x008b
        L_0x0091:
            r0 = move-exception
            goto L_0x0086
        L_0x0093:
            r0 = move-exception
            goto L_0x0076
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.module.appcenter.ab.a(com.tencent.module.appcenter.ab, java.lang.String, byte[]):void");
    }

    /* access modifiers changed from: private */
    public void a(String str, Bitmap bitmap) {
        if (bitmap != null) {
            Handler handler = (Handler) this.c.remove(str);
            if (handler != null) {
                Message obtain = Message.obtain();
                obtain.what = 0;
                obtain.obj = new Object[]{str, bitmap};
                handler.sendMessage(obtain);
                return;
            }
            return;
        }
        System.out.println("----------get none icon!");
        this.c.remove(str);
    }

    static /* synthetic */ void a(String str, String str2) {
        File file = new File(str);
        File file2 = new File(str2);
        if (file.exists() || file2.exists()) {
            File[] listFiles = file.listFiles();
            File[] listFiles2 = file2.listFiles();
            if ((listFiles != null && listFiles.length > 4000) || (listFiles2 != null && listFiles2.length > 500)) {
                a(listFiles);
                a(listFiles2);
                a.a().d();
            }
        }
    }

    private static void a(File[] fileArr) {
        if (fileArr != null) {
            for (File delete : fileArr) {
                delete.delete();
            }
        }
    }

    private static String b(String str) {
        Uri parse = Uri.parse(str);
        String str2 = parse.getPath() + "_" + parse.getQuery();
        if (str2 == null) {
            return str2;
        }
        return str2.replace("/", "_").replace("\\", "_").replace("*", "_").replace("?", "_").replace("=", "_").replace(".", "_") + ".qqmm";
    }

    private synchronized void b(String str, String str2) {
        boolean z = false;
        synchronized (this.j) {
            if (!this.j.contains(str)) {
                this.j.add(str);
                this.k.put(str, str2);
                z = true;
            }
        }
        if (z) {
            synchronized (this.e) {
                this.e.notify();
            }
        }
    }

    public final synchronized Bitmap a(String str, Handler handler, boolean z) {
        Bitmap bitmap;
        System.currentTimeMillis();
        if (str != null) {
            if (str.length() < 5) {
                bitmap = null;
            } else {
                boolean z2 = false;
                try {
                    Bitmap a2 = this.g.a(str);
                    if (a2 != null) {
                        bitmap = a2;
                    } else if (!z) {
                        bitmap = null;
                    } else if (!this.j.contains(str)) {
                        this.c.put(str, handler);
                        String b2 = b(str);
                        if (b2 == null || b2.length() <= 0) {
                            z2 = true;
                        } else {
                            b(str, b2);
                        }
                        System.currentTimeMillis();
                        if (!z2) {
                            bitmap = null;
                        } else if (!this.d.contains(str)) {
                            this.d.add(str);
                            d.a().b(str);
                        }
                    } else {
                        bitmap = null;
                    }
                } catch (Exception e2) {
                }
            }
        }
        System.currentTimeMillis();
        bitmap = null;
        return bitmap;
    }

    public final void a() {
        if (this.n != null) {
            this.n.a();
            this.n = null;
        }
        if (this.o != null) {
            this.o.a();
            this.o = null;
        }
        a = null;
    }

    public final void a(String str) {
        if (this.d.contains(str)) {
            this.d.remove(str);
        }
    }

    public final void a(String str, byte[] bArr) {
        if (this.d.contains(str)) {
            this.d.remove(str);
        }
        if (!this.g.b(str)) {
            try {
                Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
                this.g.a(str, decodeByteArray);
                synchronized (this.l) {
                    if (!this.l.contains(str)) {
                        this.l.add(str);
                        this.m.put(str, bArr);
                    }
                }
                synchronized (this.f) {
                    this.f.notify();
                }
                a(str, decodeByteArray);
            } catch (OutOfMemoryError e2) {
                e2.printStackTrace();
            }
        }
    }
}
