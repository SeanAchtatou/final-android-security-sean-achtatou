package com.tencent.module.appcenter;

import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

final class p extends Handler {
    private /* synthetic */ int a;
    private /* synthetic */ TActivity b;

    p(TActivity tActivity, int i) {
        this.b = tActivity;
        this.a = i;
    }

    public final void handleMessage(Message message) {
        Toast makeText = Toast.makeText(this.b, this.a, 1);
        makeText.setGravity(17, 0, 0);
        makeText.show();
    }
}
