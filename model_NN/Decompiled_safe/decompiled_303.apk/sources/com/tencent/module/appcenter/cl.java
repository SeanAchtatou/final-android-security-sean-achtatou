package com.tencent.module.appcenter;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.RemoteException;
import android.view.View;
import android.widget.Toast;
import com.tencent.android.a.b;
import com.tencent.android.ui.LocalSoftManageActivity;
import com.tencent.launcher.am;
import com.tencent.launcher.base.BaseApp;
import com.tencent.module.download.DownloadInfo;
import com.tencent.module.theme.ThemeSettingActivity;
import com.tencent.qqlauncher.R;
import java.io.File;

final class cl implements View.OnClickListener {
    private /* synthetic */ SoftWareActivity a;

    cl(SoftWareActivity softWareActivity) {
        this.a = softWareActivity;
    }

    public final void onClick(View view) {
        switch (this.a.downloadStatus) {
            case 0:
                if (this.a.bStartSoftware) {
                    this.a.startActivity(((am) this.a.mAppList.get(this.a.nStartSoftwareIndex)).c);
                    return;
                }
                try {
                    DownloadInfo downloadInfo = new DownloadInfo();
                    downloadInfo.a = this.a.software.i();
                    downloadInfo.h = (long) this.a.software.h();
                    downloadInfo.c = this.a.software.j();
                    downloadInfo.d = this.a.software.d();
                    downloadInfo.a(this.a.software.a);
                    downloadInfo.b(this.a.software.d);
                    downloadInfo.c(this.a.software.i);
                    downloadInfo.n = this.a.stat.a();
                    downloadInfo.o = String.valueOf(this.a.stat.a());
                    try {
                        downloadInfo.i = ((BitmapDrawable) this.a.softwareIcon.getDrawable()).getBitmap();
                    } catch (Exception e) {
                        downloadInfo.i = null;
                    }
                    if (this.a.mService != null) {
                        this.a.mService.a(downloadInfo, "SoftwareActivity", this.a.mCallback);
                        return;
                    } else {
                        BaseApp.a("服务已断开");
                        return;
                    }
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                    return;
                }
            case 1:
                Intent intent = new Intent();
                intent.putExtra("android.intent.extra.TITLE", 2);
                intent.setClass(this.a, LocalSoftManageActivity.class);
                this.a.startActivity(intent);
                return;
            case 2:
                String str = c.a + "/" + (ThemeSettingActivity.decodeUrl2FileName(this.a.software.i()) + ".apk");
                if (!new File(str).exists()) {
                    Toast.makeText(BaseApp.b(), "安装包不存在", 0).show();
                    if (!this.a.bDownloadButtonStatusIsUpdate) {
                        this.a.downloadAction.setText("下载");
                        return;
                    }
                    this.a.downloadAction.setTextColor(-1);
                    this.a.downloadAction.setBackgroundDrawable(this.a.getResources().getDrawable(R.drawable.software_button_highlight));
                    this.a.downloadAction.setText("更新");
                    return;
                }
                b.a(str, this.a);
                return;
            default:
                return;
        }
    }
}
