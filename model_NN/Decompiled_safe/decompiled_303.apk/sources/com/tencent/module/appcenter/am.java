package com.tencent.module.appcenter;

import AndroidDLoader.Software;
import AndroidDLoader.a;
import android.view.View;
import android.widget.AdapterView;

final class am implements AdapterView.OnItemClickListener {
    private /* synthetic */ AppCenterActivity a;

    am(AppCenterActivity appCenterActivity) {
        this.a = appCenterActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Software software = (Software) adapterView.getAdapter().getItem(i);
        if (software != null) {
            SoftWareActivity.openSoftwareActivity(this.a, software.a(), software.c(), software.g(), a.f);
        }
    }
}
