package com.tencent.module.appcenter;

import AndroidDLoader.Software;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import com.tencent.launcher.base.BaseApp;
import com.tencent.qqlauncher.R;
import com.tencent.util.p;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public final class bh extends BaseAdapter {
    public boolean a = true;
    /* access modifiers changed from: private */
    public Context b = null;
    private ArrayList c = null;
    /* access modifiers changed from: private */
    public Map d = null;
    private ImageView e = null;
    private TextView f = null;
    private TextView g = null;
    private TextView h = null;
    private TextView i = null;
    private RatingBar j = null;
    private Handler k = new x(this);
    private View.OnClickListener l = new y(this);

    public bh(Context context) {
        this.b = context;
    }

    public final void a(ArrayList arrayList) {
        if (this.c == null) {
            this.c = new ArrayList();
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            Software software = (Software) it.next();
            if (!BaseApp.a.equals(software.l)) {
                this.c.add(software);
            }
        }
    }

    public final void b(ArrayList arrayList) {
        if (this.d == null) {
            this.d = new HashMap();
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            Software software = (Software) it.next();
            this.d.put(Integer.valueOf(software.a), software);
        }
    }

    public final int getCount() {
        if (this.c == null) {
            return 0;
        }
        return this.c.size();
    }

    public final Object getItem(int i2) {
        if (this.c == null) {
            return null;
        }
        return this.c.get(i2);
    }

    public final long getItemId(int i2) {
        return (long) i2;
    }

    public final View getView(int i2, View view, ViewGroup viewGroup) {
        View inflate = view == null ? LayoutInflater.from(this.b).inflate((int) R.layout.everyday_for_you_list_item, (ViewGroup) null) : view;
        if (this.c != null && i2 < this.c.size()) {
            Software software = (Software) this.c.get(i2);
            inflate.setTag(software);
            inflate.setOnClickListener(this.l);
            this.e = (ImageView) inflate.findViewById(R.id.software_icon);
            this.f = (TextView) inflate.findViewById(R.id.software_item_name);
            this.f.setSelected(true);
            this.h = (TextView) inflate.findViewById(R.id.share_way);
            this.i = (TextView) inflate.findViewById(R.id.software_fees);
            this.j = (RatingBar) inflate.findViewById(R.id.RatingBar01);
            Bitmap a2 = d.a().a(software.c, this.k, this.a);
            if (a2 != null) {
                this.e.setImageBitmap(a2);
            } else {
                this.e.setImageResource(R.drawable.sw_default_icon);
            }
            ImageView imageView = (ImageView) inflate.findViewById(R.id.first_rel);
            imageView.setVisibility(4);
            if (this.d != null && this.d.size() > 0 && this.d.containsKey(Integer.valueOf(software.a))) {
                imageView.setVisibility(0);
            }
            this.f.setText(software.b);
            this.h.setText(p.a(software.n));
            d.a();
            this.i.setText(software.g);
            this.j.setRating((float) (software.m / 2));
        }
        return inflate;
    }
}
