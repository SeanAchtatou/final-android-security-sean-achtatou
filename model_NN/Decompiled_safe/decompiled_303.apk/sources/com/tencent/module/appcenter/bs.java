package com.tencent.module.appcenter;

import android.view.View;
import android.widget.TextView;
import com.tencent.qqlauncher.R;

public final class bs {
    private View a;
    private View b;
    private f c;
    private int d = -1;

    public final bs a(int i) {
        this.d = i;
        return this;
    }

    public final bs a(View view) {
        this.a = view;
        return this;
    }

    public final bs a(f fVar) {
        this.c = fVar;
        return this;
    }

    public final ch a() {
        ch chVar = new ch();
        View unused = chVar.b = this.a;
        View unused2 = chVar.c = this.b;
        f unused3 = chVar.d = this.c;
        if (this.d != -1) {
            TextView textView = (TextView) this.a.findViewById(R.id.appcenter_tab_text);
            textView.setText(this.d);
            textView.setTextSize((float) ((int) textView.getContext().getResources().getDimension(R.dimen.appcenter_tab_title_nor)));
        }
        return chVar;
    }

    public final bs b(View view) {
        this.b = view;
        return this;
    }
}
