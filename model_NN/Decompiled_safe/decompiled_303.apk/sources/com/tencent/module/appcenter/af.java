package com.tencent.module.appcenter;

import AndroidDLoader.StatData;
import AndroidDLoader.StatList;
import AndroidDLoader.a;
import android.content.SharedPreferences;
import android.util.Log;
import com.tencent.launcher.base.BaseApp;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

final class af implements ah {
    private Map a = new ConcurrentHashMap();
    private Map b = new ConcurrentHashMap();
    private /* synthetic */ g c;

    af(g gVar) {
        this.c = gVar;
    }

    public final byte a() {
        return 3;
    }

    public final void a(a aVar) {
        byte a2 = (byte) aVar.a();
        if (this.a.containsKey(Byte.valueOf(a2))) {
            this.a.put(Byte.valueOf(a2), Integer.valueOf(((Integer) this.a.get(Byte.valueOf(a2))).intValue() + 1));
        } else {
            this.a.put(Byte.valueOf(a2), 1);
        }
        String str = "addColumnCount key:" + aVar.toString() + " conut:" + this.a.get(Byte.valueOf(a2));
        if (str == null) {
            str = "............";
        }
        Log.v("StatManager", str);
    }

    public final void b() {
        SharedPreferences sharedPreferences = BaseApp.b().getSharedPreferences("columnStat", 0);
        for (byte b2 = 0; b2 < 35; b2 = (byte) (b2 + 1)) {
            a a2 = a.a(b2);
            if (a2 != null) {
                int i = sharedPreferences.getInt(a2.toString(), -1);
                if (i > 0) {
                    this.b.put(Byte.valueOf(b2), Integer.valueOf(i));
                }
                String str = "loadColumnStatCount :" + a2.toString() + " count:" + i;
                if (str == null) {
                    str = "............";
                }
                Log.v("StatManager", str);
            }
        }
    }

    public final void c() {
        SharedPreferences.Editor edit = BaseApp.b().getSharedPreferences("columnStat", 0).edit();
        for (Byte b2 : this.a.keySet()) {
            edit.putInt(a.a(b2.byteValue()).toString(), ((Integer) this.a.get(b2)).intValue());
        }
        edit.commit();
    }

    public final void d() {
        for (Byte b2 : this.b.keySet()) {
            int intValue = ((Integer) this.b.get(b2)).intValue();
            int i = 0;
            if (this.a.containsKey(b2)) {
                i = ((Integer) this.a.get(b2)).intValue();
            }
            this.a.put(b2, Integer.valueOf(i + intValue));
        }
    }

    public final void e() {
        SharedPreferences.Editor edit = BaseApp.b().getSharedPreferences("columnStat", 0).edit();
        for (Byte byteValue : this.b.keySet()) {
            edit.putInt(a.a(byteValue.byteValue()).toString(), 0);
        }
        edit.commit();
        this.b.clear();
    }

    public final StatList f() {
        if (this.b.size() <= 0) {
            return null;
        }
        StatList statList = new StatList();
        statList.a = new ArrayList();
        for (Byte b2 : this.b.keySet()) {
            int intValue = ((Integer) this.b.get(b2)).intValue();
            StatData statData = new StatData();
            statData.c = 3;
            statData.d = intValue;
            statData.b = b2.byteValue();
            statData.a = b2.byteValue();
            statList.a.add(statData);
        }
        return statList;
    }
}
