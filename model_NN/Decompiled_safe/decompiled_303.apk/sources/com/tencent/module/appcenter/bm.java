package com.tencent.module.appcenter;

import AndroidDLoader.Software;
import AndroidDLoader.a;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import com.tencent.launcher.base.BaseApp;
import com.tencent.qqlauncher.R;
import com.tencent.util.p;
import java.util.ArrayList;
import java.util.Iterator;

public final class bm extends BaseAdapter {
    public boolean a = true;
    /* access modifiers changed from: private */
    public TActivity b = null;
    /* access modifiers changed from: private */
    public a c = null;
    private ArrayList d = null;
    private ImageView e = null;
    private TextView f = null;
    private TextView g = null;
    private TextView h = null;
    private TextView i = null;
    private RatingBar j = null;
    private Handler k = new az(this);
    private View.OnClickListener l = new bb(this);

    public bm(TActivity tActivity, a aVar) {
        this.b = tActivity;
        this.c = aVar;
    }

    public final ArrayList a() {
        return this.d;
    }

    public final void a(ArrayList arrayList) {
        if (this.d == null) {
            this.d = new ArrayList();
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            Software software = (Software) it.next();
            if (!BaseApp.a.equals(software.l)) {
                this.d.add(software);
            }
        }
    }

    public final int getCount() {
        if (this.d == null) {
            return 0;
        }
        return this.d.size();
    }

    public final Object getItem(int i2) {
        if (this.d == null) {
            return null;
        }
        return this.d.get(i2);
    }

    public final long getItemId(int i2) {
        return (long) i2;
    }

    public final View getView(int i2, View view, ViewGroup viewGroup) {
        View inflate = view == null ? LayoutInflater.from(this.b).inflate((int) R.layout.general_software_list_item, (ViewGroup) null) : view;
        if (this.d != null && i2 < this.d.size()) {
            Software software = (Software) this.d.get(i2);
            inflate.setTag(software);
            inflate.setOnClickListener(this.l);
            this.e = (ImageView) inflate.findViewById(R.id.software_icon);
            this.f = (TextView) inflate.findViewById(R.id.software_item_name);
            this.f.setSelected(true);
            this.h = (TextView) inflate.findViewById(R.id.share_way);
            this.i = (TextView) inflate.findViewById(R.id.software_fees);
            this.j = (RatingBar) inflate.findViewById(R.id.RatingBar01);
            Bitmap a2 = d.a().a(software.c, this.k, this.a);
            if (a2 != null) {
                this.e.setImageBitmap(a2);
            } else {
                this.e.setImageResource(R.drawable.sw_default_icon);
            }
            this.f.setText(software.b);
            this.h.setText(p.a(software.n));
            d.a();
            this.i.setText(software.g);
            this.j.setRating((float) (software.m / 2));
        }
        return inflate;
    }
}
