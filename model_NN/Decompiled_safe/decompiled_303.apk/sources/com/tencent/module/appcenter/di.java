package com.tencent.module.appcenter;

import android.widget.CompoundButton;

final class di implements CompoundButton.OnCheckedChangeListener {
    private /* synthetic */ SoftWareActivity a;

    di(SoftWareActivity softWareActivity) {
        this.a = softWareActivity;
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (compoundButton == this.a.viewCannotDownload) {
            this.a.EnableOtherCheckbox(!z);
            if (z) {
                this.a.viewCannotInstall.setEnabled(false);
            } else {
                this.a.viewCannotInstall.setEnabled(true);
            }
        } else if (compoundButton == this.a.viewCannotInstall) {
            this.a.EnableOtherCheckbox(!z);
            if (z) {
                this.a.viewCannotDownload.setEnabled(false);
            } else {
                this.a.viewCannotDownload.setEnabled(true);
            }
        }
    }
}
