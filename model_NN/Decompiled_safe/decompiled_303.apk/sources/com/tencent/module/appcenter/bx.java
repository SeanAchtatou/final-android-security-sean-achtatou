package com.tencent.module.appcenter;

import android.widget.ExpandableListView;

final class bx implements ExpandableListView.OnGroupCollapseListener {
    private /* synthetic */ AppCenterActivity a;

    bx(AppCenterActivity appCenterActivity) {
        this.a = appCenterActivity;
    }

    public final void onGroupCollapse(int i) {
        this.a.recommendRequireSoftware.expandGroup(i);
    }
}
