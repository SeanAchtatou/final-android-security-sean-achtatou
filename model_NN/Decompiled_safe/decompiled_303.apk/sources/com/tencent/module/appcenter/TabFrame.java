package com.tencent.module.appcenter;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.launcher.base.b;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;
import java.util.Iterator;

public class TabFrame extends LinearLayout {
    private ViewGroup a;
    private ViewGroup b;
    private int c = -1;
    private Context d;
    /* access modifiers changed from: private */
    public ArrayList e = new ArrayList();

    public TabFrame(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.d = context;
    }

    public final void a() {
        ArrayList arrayList = this.e;
        if (arrayList != null && arrayList.size() > 0) {
            b bVar = new b(this);
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                ch chVar = (ch) it.next();
                View a2 = chVar.a();
                a2.setOnClickListener(bVar);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -1);
                layoutParams.weight = 1.0f;
                layoutParams.leftMargin = (int) (b.b * 4.0f);
                layoutParams.rightMargin = (int) (b.b * 4.0f);
                this.a.addView(a2, layoutParams);
                if (((TextView) a2.findViewById(R.id.appcenter_tab_text)).getText().toString().equals(this.d.getString(R.string.recommend_top_list))) {
                    this.b.addView(chVar.c());
                }
            }
        }
    }

    public final void a(int i) {
        b((ch) this.e.get(i));
    }

    public final void a(ch chVar) {
        this.e.add(chVar);
    }

    public final void b(ch chVar) {
        if (this.c == -1 || this.e.get(this.c) != chVar) {
            Iterator it = this.e.iterator();
            while (it.hasNext()) {
                ((ch) it.next()).a(false);
            }
            chVar.a(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.a = (ViewGroup) findViewById(R.id.tabHeader);
        this.b = (ViewGroup) findViewById(R.id.tabContent);
    }
}
