package com.tencent.module.appcenter;

import android.view.View;
import com.tencent.qqlauncher.R;

final class bv implements View.OnClickListener {
    private /* synthetic */ dn a;

    bv(dn dnVar) {
        this.a = dnVar;
    }

    public final void onClick(View view) {
        this.a.a = 1;
        this.a.d.setVisibility(0);
        this.a.c.setVisibility(0);
        this.a.e.setVisibility(8);
        this.a.a();
        d a2 = d.a();
        switch (this.a.h) {
            case R.id.ListView_latest_list /*2131493001*/:
                a2.o(this.a.f, this.a.b);
                return;
            case R.id.ListView_ontop_by_donwload_count /*2131493023*/:
                a2.p(this.a.f, this.a.b);
                return;
            case R.id.ListView_ontop_by_score /*2131493024*/:
                a2.l(this.a.f, this.a.b);
                return;
            case R.id.listview_recommend_daily_pick /*2131493134*/:
                a2.m(this.a.f, this.a.b);
                return;
            case R.id.listview_recommend_require_software /*2131493136*/:
                a2.n(this.a.f, this.a.b);
                return;
            default:
                return;
        }
    }
}
