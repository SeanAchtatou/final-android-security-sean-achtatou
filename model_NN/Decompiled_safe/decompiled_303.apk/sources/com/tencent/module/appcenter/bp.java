package com.tencent.module.appcenter;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;
import com.tencent.android.a.b;
import com.tencent.launcher.base.BaseApp;
import com.tencent.module.setting.AboutSettingActivity;
import java.util.ArrayList;

final class bp extends Handler {
    private /* synthetic */ dn a;

    bp(dn dnVar) {
        this.a = dnVar;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case 900:
                int i = message.arg1;
                int i2 = message.arg2;
                dn.d(this.a);
                switch (i2) {
                    case 304:
                    case 305:
                    case 306:
                    case 307:
                    case 308:
                    case 309:
                    case AboutSettingActivity.UPDATE_READY /*800*/:
                        return;
                    default:
                        Toast.makeText(BaseApp.b(), b.a(i), 0).show();
                        return;
                }
            case 1405:
                Log.v("AppCenterActivity", "MSG_getSoftwaresLatest");
                ArrayList arrayList = (ArrayList) message.obj;
                this.a.j.removeMessages(0);
                if (arrayList == null || arrayList.size() == 0) {
                    this.a.b();
                    return;
                } else {
                    dn.c(this.a);
                    return;
                }
            case 1406:
                Log.v("AppCenterActivity", "MSG_getSoftwaresOnTopByScore");
                ArrayList arrayList2 = (ArrayList) message.obj;
                this.a.j.removeMessages(0);
                if (arrayList2 == null || arrayList2.size() == 0) {
                    this.a.b();
                    return;
                } else {
                    dn.c(this.a);
                    return;
                }
            case 1407:
                Log.v("AppCenterActivity", "MSG_getSoftwaresOnTopByDownloadCount");
                ArrayList arrayList3 = (ArrayList) message.obj;
                this.a.j.removeMessages(0);
                if (arrayList3 == null || arrayList3.size() == 0) {
                    this.a.b();
                    return;
                }
                dn.c(this.a);
                this.a.g.onTopDownloadListAdapter.a(arrayList3);
                this.a.g.onTopDownloadListAdapter.notifyDataSetChanged();
                return;
            case 1408:
                Log.v("AppCenterActivity", "MSG_getHotSoftwares");
                return;
            case 1409:
                Log.v("AppCenterActivity", "MSG_getUserCommends");
                this.a.b();
                this.a.g.recommendDailyPickAdapter.notifyDataSetChanged();
                return;
            case 2100:
                Log.v("AppCenterActivity", "MSG_everydayForYou");
                Object[] objArr = (Object[]) message.obj;
                ArrayList arrayList4 = (ArrayList) objArr[0];
                ArrayList arrayList5 = (ArrayList) objArr[1];
                ArrayList arrayList6 = (ArrayList) objArr[2];
                this.a.j.removeMessages(0);
                if (arrayList4 == null || arrayList4.size() == 0) {
                    this.a.b();
                    return;
                }
                if (this.a.b == 1) {
                    if (arrayList5 == null || arrayList5.size() == 0) {
                        int unused = this.a.g.requestId = d.a().f(this.a.g.netHandler);
                    } else if (this.a.g.scrollAdCtrl != null) {
                        this.a.g.scrollAdCtrl.a(arrayList5);
                    }
                    if (arrayList6 != null || arrayList6.size() > 0) {
                        this.a.g.recommendDailyPickAdapter.b(arrayList6);
                        this.a.g.recommendDailyPickAdapter.a(arrayList6);
                    }
                }
                dn.c(this.a);
                this.a.g.recommendDailyPickAdapter.a(arrayList4);
                this.a.g.recommendDailyPickAdapter.notifyDataSetChanged();
                return;
            case 2200:
                int i3 = message.arg1;
                dn.d(this.a);
                Toast.makeText(BaseApp.b(), b.a(i3), 0).show();
                return;
            case 2300:
                Log.v("AppCenterActivity", "MSG_guessIt");
                ArrayList arrayList7 = (ArrayList) message.obj;
                if (arrayList7 == null || arrayList7.size() == 0) {
                    this.a.b();
                } else {
                    dn.c(this.a);
                }
                this.a.g.recommendRequireSoftwareAdapter.a(arrayList7);
                this.a.g.recommendRequireSoftwareAdapter.notifyDataSetChanged();
                for (int i4 = 0; i4 < this.a.g.recommendRequireSoftwareAdapter.getGroupCount(); i4++) {
                    this.a.g.recommendRequireSoftware.expandGroup(i4);
                }
                return;
            default:
                return;
        }
    }
}
