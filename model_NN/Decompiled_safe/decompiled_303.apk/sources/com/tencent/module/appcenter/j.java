package com.tencent.module.appcenter;

import android.os.Handler;
import android.widget.AbsListView;

final class j implements be {
    private /* synthetic */ SearchTagActivity a;

    j(SearchTagActivity searchTagActivity) {
        this.a = searchTagActivity;
    }

    public final void a(AbsListView absListView, int i) {
        SearchTagActivity searchTagActivity = this.a;
        d access$200 = this.a.ctrl;
        Handler access$100 = this.a.handler;
        d unused = this.a.ctrl;
        int unused2 = searchTagActivity.requestId = access$200.b(access$100, this.a.labelId, i);
    }

    public final void b(AbsListView absListView, int i) {
        if (this.a.listAdapter != null) {
            this.a.listAdapter.a = i == 0;
            if (this.a.listAdapter.a) {
                this.a.listAdapter.notifyDataSetChanged();
            }
        }
    }
}
