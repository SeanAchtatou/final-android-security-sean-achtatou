package com.tencent.module.appcenter;

import android.content.Context;
import com.tencent.android.c.a;

public final class bf {
    private static bf c;
    private a a;
    private df b;
    /* access modifiers changed from: private */
    public Context d;
    private int e = 0;
    private int f = 20;
    private int g = 80;
    private int h = 15000;
    private int i = 1000;
    private String j = "";

    bf(QQAppMarketService qQAppMarketService) {
        c = this;
        this.d = qQAppMarketService;
        this.b = new df(this);
        this.b.a();
        this.a = a.a();
        a.b();
        this.j = qQAppMarketService.getSharedPreferences("data", 0).getString("sid", "00");
    }

    public final void a() {
        this.a = null;
        this.b = null;
        c = null;
    }
}
