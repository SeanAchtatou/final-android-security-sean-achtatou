package com.tencent.module.appcenter;

import android.content.DialogInterface;
import android.view.KeyEvent;

final class m implements DialogInterface.OnKeyListener {
    private /* synthetic */ TActivity a;

    m(TActivity tActivity) {
        this.a = tActivity;
    }

    public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i != 4) {
            return true;
        }
        if (this.a.onProgressDialogBack()) {
            this.a.setNormalContentView();
            return true;
        }
        this.a.setNormalContentView();
        return false;
    }
}
