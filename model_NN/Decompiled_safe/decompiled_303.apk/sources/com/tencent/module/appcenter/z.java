package com.tencent.module.appcenter;

import AndroidDLoader.Software;
import AndroidDLoader.SoftwareGroup;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import com.tencent.launcher.base.BaseApp;
import com.tencent.qqlauncher.R;
import com.tencent.util.p;
import java.util.ArrayList;

public final class z extends BaseExpandableListAdapter {
    public boolean a = true;
    /* access modifiers changed from: private */
    public Context b = null;
    private ArrayList c = null;
    private ImageView d = null;
    private TextView e = null;
    private TextView f = null;
    private TextView g = null;
    private TextView h = null;
    private RatingBar i = null;
    private Handler j = new t(this);
    private View.OnClickListener k = new u(this);
    private LayoutInflater l;

    public z(Context context) {
        this.b = context;
    }

    public final void a(ArrayList arrayList) {
        if (this.c == null) {
            this.c = new ArrayList();
        }
        if (arrayList != null) {
            int size = arrayList.size() - 1;
            loop0:
            while (true) {
                if (size < 0) {
                    break;
                }
                SoftwareGroup softwareGroup = (SoftwareGroup) arrayList.get(size);
                for (int size2 = softwareGroup.b.size() - 1; size2 >= 0; size2--) {
                    if (BaseApp.a.equals(((Software) softwareGroup.b.get(size2)).l)) {
                        softwareGroup.b.remove(size2);
                        break loop0;
                    }
                }
                size--;
            }
            this.c.addAll(arrayList);
        }
    }

    public final Object getChild(int i2, int i3) {
        SoftwareGroup softwareGroup = (SoftwareGroup) getGroup(i2);
        if (softwareGroup == null || softwareGroup.b == null || i3 >= softwareGroup.b.size()) {
            return null;
        }
        return softwareGroup.b.get(i3);
    }

    public final long getChildId(int i2, int i3) {
        Software software = (Software) getChild(i2, i3);
        if (software != null) {
            return (long) software.d;
        }
        return 0;
    }

    public final View getChildView(int i2, int i3, boolean z, View view, ViewGroup viewGroup) {
        View inflate = view == null ? LayoutInflater.from(this.b).inflate((int) R.layout.general_software_list_item, (ViewGroup) null) : view;
        if (this.c != null) {
            Software software = (Software) getChild(i2, i3);
            if (software == null) {
                return inflate;
            }
            inflate.setTag(software);
            inflate.setOnClickListener(this.k);
            this.d = (ImageView) inflate.findViewById(R.id.software_icon);
            this.e = (TextView) inflate.findViewById(R.id.software_item_name);
            this.e.setSelected(true);
            this.g = (TextView) inflate.findViewById(R.id.share_way);
            this.h = (TextView) inflate.findViewById(R.id.software_fees);
            this.i = (RatingBar) inflate.findViewById(R.id.RatingBar01);
            Bitmap a2 = d.a().a(software.c, this.j, this.a);
            if (a2 != null) {
                this.d.setImageBitmap(a2);
            } else {
                this.d.setImageResource(R.drawable.sw_default_icon);
            }
            this.e.setText(software.b);
            this.g.setText(p.a(software.n));
            d.a();
            this.h.setText(software.g);
            this.i.setRating((float) (software.m / 2));
        }
        return inflate;
    }

    public final int getChildrenCount(int i2) {
        SoftwareGroup softwareGroup = (SoftwareGroup) getGroup(i2);
        if (softwareGroup == null || softwareGroup.b == null) {
            return 0;
        }
        return softwareGroup.b.size();
    }

    public final Object getGroup(int i2) {
        if (this.c != null) {
            return this.c.get(i2);
        }
        return null;
    }

    public final int getGroupCount() {
        if (this.c != null) {
            return this.c.size();
        }
        return 0;
    }

    public final long getGroupId(int i2) {
        if (this.c != null) {
            return (long) i2;
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getGroupView(int i2, boolean z, View view, ViewGroup viewGroup) {
        View view2;
        if (view == null) {
            if (this.l == null) {
                this.l = (LayoutInflater) this.b.getSystemService("layout_inflater");
            }
            view2 = this.l.inflate((int) R.layout.local_manager_group, viewGroup, false);
        } else {
            view2 = view;
        }
        TextView textView = (TextView) view2.findViewById(R.id.LocalGroundName);
        SoftwareGroup softwareGroup = (SoftwareGroup) getGroup(i2);
        if (softwareGroup != null) {
            textView.setText(softwareGroup.a);
        }
        return view2;
    }

    public final boolean hasStableIds() {
        return false;
    }

    public final boolean isChildSelectable(int i2, int i3) {
        return false;
    }
}
