package com.tencent.module.appcenter;

import android.view.View;
import android.widget.TextView;
import com.tencent.qqlauncher.R;

public final class ch {
    private boolean a;
    /* access modifiers changed from: private */
    public View b;
    /* access modifiers changed from: private */
    public View c;
    /* access modifiers changed from: private */
    public f d;

    public final View a() {
        return this.b;
    }

    public final void a(int i) {
        ((TextView) this.b.findViewById(R.id.appcenter_tab_text)).setTextColor(i);
    }

    public final void a(boolean z) {
        if (this.a != z) {
            if (z) {
                if (this.d != null) {
                    this.d.a(this);
                }
            } else if (this.d != null) {
                this.d.b(this);
            }
            this.a = z;
        }
    }

    public final TextView b() {
        return (TextView) this.b.findViewById(R.id.appcenter_tab_text);
    }

    public final void b(int i) {
        ((TextView) this.b.findViewById(R.id.appcenter_tab_text)).setTextSize((float) i);
    }

    public final View c() {
        return this.c;
    }
}
