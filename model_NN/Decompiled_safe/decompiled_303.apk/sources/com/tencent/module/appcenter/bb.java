package com.tencent.module.appcenter;

import AndroidDLoader.Software;
import android.view.View;

final class bb implements View.OnClickListener {
    private /* synthetic */ bm a;

    bb(bm bmVar) {
        this.a = bmVar;
    }

    public final void onClick(View view) {
        Object tag = view.getTag();
        if (tag != null && (tag instanceof Software)) {
            Software software = (Software) tag;
            SoftWareActivity.openSoftwareActivity(this.a.b, software.a, software.d, software.i, this.a.c);
        }
    }
}
