package com.tencent.module.appcenter;

import AndroidDLoader.StablePicAdv;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import java.util.ArrayList;

public final class w {
    private View a;
    private ImageView b;
    private ImageView c;
    private ImageView d;
    private ImageView e;
    private ImageView f;
    private ImageView g;
    private ImageView h;
    private ImageView i;
    private ArrayList j;
    private Handler k;
    private LinearLayout l;
    private LinearLayout m;

    public final void a(Configuration configuration) {
        if (configuration.orientation == 2) {
            this.l.setVisibility(8);
            this.m.setVisibility(0);
            this.a.requestLayout();
            return;
        }
        this.m.setVisibility(8);
        this.l.setVisibility(0);
        this.a.requestLayout();
    }

    public final void a(ArrayList arrayList) {
        this.j = arrayList;
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < arrayList.size()) {
                StablePicAdv stablePicAdv = (StablePicAdv) arrayList.get(i3);
                switch (i3) {
                    case 0:
                        Bitmap a2 = d.a().a(stablePicAdv.a, this.k);
                        if (a2 != null) {
                            this.b.setImageBitmap(a2);
                            this.f.setImageBitmap(a2);
                        }
                        this.b.setTag(stablePicAdv);
                        this.f.setTag(stablePicAdv);
                        break;
                    case 1:
                        Bitmap a3 = d.a().a(stablePicAdv.a, this.k);
                        if (a3 != null) {
                            this.c.setImageBitmap(a3);
                            this.g.setImageBitmap(a3);
                        }
                        this.c.setTag(stablePicAdv);
                        this.g.setTag(stablePicAdv);
                        break;
                    case 2:
                        Bitmap a4 = d.a().a(stablePicAdv.a, this.k);
                        if (a4 != null) {
                            this.d.setImageBitmap(a4);
                            this.h.setImageBitmap(a4);
                        }
                        this.d.setTag(stablePicAdv);
                        this.h.setTag(stablePicAdv);
                        break;
                    case 3:
                        Bitmap a5 = d.a().a(stablePicAdv.a, this.k);
                        if (a5 != null) {
                            this.e.setImageBitmap(a5);
                            this.i.setImageBitmap(a5);
                        }
                        this.e.setTag(stablePicAdv);
                        this.i.setTag(stablePicAdv);
                        break;
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }
}
