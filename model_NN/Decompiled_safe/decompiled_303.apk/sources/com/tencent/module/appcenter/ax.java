package com.tencent.module.appcenter;

import android.widget.RatingBar;
import com.tencent.module.setting.DesktopSwitchSpecialEffectSettingActivity;
import com.tencent.qqlauncher.R;

final class ax implements RatingBar.OnRatingBarChangeListener {
    private /* synthetic */ SoftWareActivity a;

    ax(SoftWareActivity softWareActivity) {
        this.a = softWareActivity;
    }

    public final void onRatingChanged(RatingBar ratingBar, float f, boolean z) {
        if (((double) Math.abs(f)) < 0.01d) {
            this.a.markBt.setEnabled(false);
            this.a.markBt.setTextColor(-7960954);
            this.a.markStartText.setText((int) R.string.click_star_to_mark);
            return;
        }
        this.a.markBt.setEnabled(true);
        this.a.markBt.setTextColor(-14869219);
        switch (Math.round(2.0f * f)) {
            case 1:
            case 2:
                this.a.markStartText.setText((int) R.string.very_bad);
                return;
            case 3:
            case 4:
                this.a.markStartText.setText((int) R.string.not_too_bad);
                return;
            case 5:
            case 6:
                this.a.markStartText.setText((int) R.string.just_soso);
                return;
            case DesktopSwitchSpecialEffectSettingActivity.nRotationSetting /*7*/:
            case DesktopSwitchSpecialEffectSettingActivity.nCubeSetting /*8*/:
                this.a.markStartText.setText((int) R.string.good);
                return;
            case 9:
            case 10:
                this.a.markStartText.setText((int) R.string.excellent);
                return;
            default:
                return;
        }
    }
}
