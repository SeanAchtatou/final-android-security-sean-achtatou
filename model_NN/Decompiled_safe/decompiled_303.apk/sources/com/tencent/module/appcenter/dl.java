package com.tencent.module.appcenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import java.lang.ref.WeakReference;

final class dl extends BroadcastReceiver {
    private WeakReference a;

    public dl(AppCenterActivity appCenterActivity) {
        this.a = new WeakReference(appCenterActivity);
    }

    public final void onReceive(Context context, Intent intent) {
        if ("com.tencent.qqlauncher.QUIT".equals(intent.getAction()) && this.a.get() != null) {
            ((AppCenterActivity) this.a.get()).finish();
        }
    }
}
