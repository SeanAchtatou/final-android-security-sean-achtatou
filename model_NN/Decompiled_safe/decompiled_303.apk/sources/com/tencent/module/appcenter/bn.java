package com.tencent.module.appcenter;

import android.util.Log;
import android.widget.AbsListView;
import com.tencent.qqlauncher.R;

final class bn implements AbsListView.OnScrollListener {
    private /* synthetic */ dn a;

    bn(dn dnVar) {
        this.a = dnVar;
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (i == i3 - i2 && this.a.a == 0) {
            this.a.a = 1;
            this.a.a();
            d a2 = d.a();
            switch (absListView.getId()) {
                case R.id.ListView_latest_list /*2131493001*/:
                    a2.o(this.a.f, this.a.b);
                    return;
                case R.id.ListView_ontop_by_donwload_count /*2131493023*/:
                    a2.p(this.a.f, this.a.b);
                    Log.v("AppCenterActivity", "OnTop download");
                    return;
                case R.id.ListView_ontop_by_score /*2131493024*/:
                    a2.l(this.a.f, this.a.b);
                    Log.v("AppCenterActivity", "OnTop score");
                    return;
                case R.id.listview_recommend_daily_pick /*2131493134*/:
                    a2.m(this.a.f, this.a.b);
                    return;
                case R.id.listview_recommend_player_recommend /*2131493135*/:
                    a2.e(this.a.f);
                    return;
                case R.id.listview_recommend_require_software /*2131493136*/:
                    a2.n(this.a.f, this.a.b);
                    return;
                default:
                    return;
            }
        }
    }

    public final void onScrollStateChanged(AbsListView absListView, int i) {
        boolean z = false;
        switch (absListView.getId()) {
            case R.id.ListView_latest_list /*2131493001*/:
            case R.id.ListView_ontop_by_score /*2131493024*/:
            default:
                return;
            case R.id.ListView_ontop_by_donwload_count /*2131493023*/:
                bg access$2900 = this.a.g.onTopDownloadListAdapter;
                if (i == 0) {
                    z = true;
                }
                access$2900.a = z;
                if (this.a.g.onTopDownloadListAdapter.a) {
                    this.a.g.onTopDownloadListAdapter.notifyDataSetChanged();
                    return;
                }
                return;
            case R.id.listview_recommend_daily_pick /*2131493134*/:
                bh access$3200 = this.a.g.recommendDailyPickAdapter;
                if (i == 0) {
                    z = true;
                }
                access$3200.a = z;
                if (this.a.g.recommendDailyPickAdapter.a) {
                    this.a.g.recommendDailyPickAdapter.notifyDataSetChanged();
                    return;
                }
                return;
            case R.id.listview_recommend_require_software /*2131493136*/:
                z access$3300 = this.a.g.recommendRequireSoftwareAdapter;
                if (i == 0) {
                    z = true;
                }
                access$3300.a = z;
                if (this.a.g.recommendRequireSoftwareAdapter.a) {
                    this.a.g.recommendRequireSoftwareAdapter.notifyDataSetChanged();
                    return;
                }
                return;
        }
    }
}
