package com.tencent.module.appcenter;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.tencent.android.c.a;
import java.io.ByteArrayOutputStream;

final class as extends Handler {
    private /* synthetic */ AppCenterActivity a;

    as(AppCenterActivity appCenterActivity) {
        this.a = appCenterActivity;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        Object[] objArr = (Object[]) message.obj;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ((Bitmap) objArr[1]).compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        a.a();
        com.tencent.android.b.a.a a2 = a.a((String) objArr[0]);
        if (a2 != null) {
            a2.b = byteArray;
            a.a();
            a.a(a2);
            Log.v("AppCenterActivity", "=======getFlashData:" + byteArray.length);
        }
    }
}
