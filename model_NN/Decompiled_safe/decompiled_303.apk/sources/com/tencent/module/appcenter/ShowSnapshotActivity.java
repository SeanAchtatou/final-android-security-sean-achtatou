package com.tencent.module.appcenter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.tencent.qqlauncher.R;
import com.tencent.util.p;

public class ShowSnapshotActivity extends TActivity {
    private Handler iconHandler = new i(this);
    /* access modifiers changed from: private */
    public ImageView imageView;
    /* access modifiers changed from: private */
    public String picurl;
    /* access modifiers changed from: private */
    public View processBar;

    public void getSoftSnapshot(String str) {
        Bitmap a = d.a().a(str, this.iconHandler);
        if (a != null) {
            this.imageView.setImageBitmap(a);
            this.processBar.setVisibility(8);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().setFlags(4, 4);
        setContentView((int) R.layout.show_app_snapshot);
        int a = p.a((Activity) this);
        int b = p.b((Activity) this);
        this.imageView = (ImageView) findViewById(R.id.app_snapshot);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.imageView.getLayoutParams();
        layoutParams.width = a - 30;
        layoutParams.height = b - 60;
        this.imageView.setLayoutParams(layoutParams);
        this.picurl = getIntent().getStringExtra("picUrl");
        this.processBar = findViewById(R.id.processbar);
        getSoftSnapshot(this.picurl);
        this.imageView.setOnClickListener(new h(this));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public boolean onRetry() {
        return false;
    }

    public boolean onTimeout() {
        return false;
    }
}
