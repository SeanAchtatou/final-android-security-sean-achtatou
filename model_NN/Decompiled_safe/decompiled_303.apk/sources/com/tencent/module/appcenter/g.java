package com.tencent.module.appcenter;

import AndroidDLoader.StatList;
import AndroidDLoader.a;
import AndroidDLoader.b;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;

public final class g {
    private static g a = null;
    private af b = new af(this);
    private bl c = new bl(this);
    private ad d = new ad(this);
    private ah[] e = {this.b, this.c, this.d};
    private a f = null;
    private ArrayList g = new ArrayList();

    private g() {
    }

    public static g a() {
        if (a == null) {
            a = new g();
        }
        return a;
    }

    private synchronized void h() {
        for (ah d2 : this.e) {
            d2.d();
        }
    }

    public final synchronized void a(a aVar) {
        this.d.a(aVar);
    }

    public final synchronized void a(b bVar) {
        this.c.a(bVar);
    }

    public final void b() {
        long currentTimeMillis = System.currentTimeMillis();
        for (ah b2 : this.e) {
            b2.b();
        }
        String str = "load stat data:" + (System.currentTimeMillis() - currentTimeMillis);
        if (str == null) {
            str = "............";
        }
        Log.v("StatManager", str);
    }

    public final synchronized void b(a aVar) {
        this.b.a(aVar);
    }

    public final void c() {
        h();
        for (ah c2 : this.e) {
            c2.c();
        }
    }

    public final synchronized void d() {
    }

    public final void e() {
        d a2 = d.a();
        HashMap hashMap = new HashMap();
        for (int i = 0; i < this.e.length; i++) {
            StatList f2 = this.e[i].f();
            if (f2 != null) {
                hashMap.put(Byte.valueOf(this.e[i].a()), f2);
            }
        }
        if (hashMap.size() > 0) {
            a2.a(hashMap);
        }
        Log.v("StatManager", "sendUpLoadStatData");
    }

    public final void f() {
        for (ah e2 : this.e) {
            e2.e();
        }
        Log.v("StatManager", "upload success");
    }

    public final void g() {
        h();
        for (ah e2 : this.e) {
            e2.e();
        }
        Log.v("StatManager", "upload fail");
    }
}
