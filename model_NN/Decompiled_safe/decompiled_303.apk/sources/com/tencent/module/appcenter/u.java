package com.tencent.module.appcenter;

import AndroidDLoader.Software;
import AndroidDLoader.a;
import android.app.Activity;
import android.view.View;

final class u implements View.OnClickListener {
    private /* synthetic */ z a;

    u(z zVar) {
        this.a = zVar;
    }

    public final void onClick(View view) {
        Object tag = view.getTag();
        if (tag != null && (tag instanceof Software)) {
            Software software = (Software) tag;
            SoftWareActivity.openSoftwareActivity((Activity) this.a.b, software.a, software.d, software.i, a.c);
            g.a().b(a.h);
        }
    }
}
