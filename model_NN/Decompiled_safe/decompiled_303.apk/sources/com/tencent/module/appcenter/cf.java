package com.tencent.module.appcenter;

import android.widget.AbsListView;

final class cf implements AbsListView.OnScrollListener {
    private /* synthetic */ ae a;

    cf(ae aeVar) {
        this.a = aeVar;
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if ((i == i3 - i2 || (i3 == 1 && i2 == 0)) && this.a.a == 0) {
            this.a.a = 1;
            this.a.h.a(absListView, this.a.b);
        }
    }

    public final void onScrollStateChanged(AbsListView absListView, int i) {
        this.a.h.b(absListView, i);
    }
}
