package com.tencent.module.appcenter;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class RequiredAppsLayout extends TextView {
    public RequiredAppsLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }
}
