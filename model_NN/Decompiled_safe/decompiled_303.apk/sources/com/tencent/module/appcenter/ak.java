package com.tencent.module.appcenter;

import android.content.Context;
import android.os.Process;
import java.text.SimpleDateFormat;
import java.util.Date;

final class ak extends Thread {
    private /* synthetic */ AppCenterActivity a;

    ak(AppCenterActivity appCenterActivity) {
        this.a = appCenterActivity;
    }

    public final void run() {
        Process.setThreadPriority(10);
        Context baseContext = this.a.getBaseContext();
        if (baseContext != null) {
            if (!new SimpleDateFormat("yyyy-MM-dd").format(new Date()).equals(baseContext.getSharedPreferences("reportStats", 0).getString("stats_date", ""))) {
                d.a().c();
            }
        }
    }
}
