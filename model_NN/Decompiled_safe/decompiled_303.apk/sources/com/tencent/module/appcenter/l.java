package com.tencent.module.appcenter;

import android.graphics.Bitmap;
import java.util.Map;
import java.util.WeakHashMap;

final class l {
    private Map a = new WeakHashMap();
    private Map b = new WeakHashMap();
    private /* synthetic */ ab c;

    public l(ab abVar) {
        this.c = abVar;
    }

    public final Bitmap a(String str) {
        if (str == null && str.length() == 0) {
            return null;
        }
        Bitmap bitmap = (Bitmap) this.a.get(str);
        return bitmap == null ? (Bitmap) this.b.get(str) : bitmap;
    }

    public final void a(String str, Bitmap bitmap) {
        if (bitmap != null) {
            if (bitmap.getHeight() <= 64 || bitmap.getWidth() <= 64) {
                if (this.a.size() > 32) {
                    this.a.clear();
                }
                this.a.put(str, bitmap);
                return;
            }
            if (this.b.size() > 8) {
                this.b.clear();
            }
            this.b.put(str, bitmap);
        }
    }

    public final boolean b(String str) {
        if (this.a.containsKey(str)) {
            return true;
        }
        return this.b.containsKey(str);
    }
}
