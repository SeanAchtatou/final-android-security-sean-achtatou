package com.tencent.module.appcenter;

import android.content.Intent;
import android.view.View;
import com.tencent.launcher.SearchAppTestActivity;
import com.tencent.launcher.base.b;
import com.tencent.qqlauncher.R;

final class an implements f {
    private /* synthetic */ AppCenterActivity a;

    an(AppCenterActivity appCenterActivity) {
        this.a = appCenterActivity;
    }

    public final void a(ch chVar) {
        View a2 = chVar.a();
        chVar.a(this.a.getResources().getColor(R.color.ac_text_tab_select));
        chVar.b((int) (this.a.getResources().getDimension(R.dimen.home_tab_text_size) / b.b));
        a2.findViewById(R.id.appcenter_tab_title_indicater).setVisibility(0);
        int unused = this.a.nNowTabIndex = this.a.getTabIndex(chVar);
        if (this.a.nNowTabIndex == 4) {
            Intent intent = new Intent();
            intent.setClass(this.a, SearchAppTestActivity.class);
            this.a.startActivity(intent);
            return;
        }
        chVar.c().setVisibility(0);
        if (this.a.nLastTabIndex == 4 || this.a.nLastTabIndex == 0) {
            this.a.CreateOther();
            switch (this.a.nNowTabIndex) {
                case 1:
                    this.a.setSelectMainTabIndex(0);
                    this.a.setSelectedRecommendTab(0);
                    return;
                case 2:
                    this.a.setSelectMainTabIndex(0);
                    this.a.setSelectedRecommendTab(2);
                    return;
                case 3:
                    this.a.setSelectMainTabIndex(2);
                    this.a.setSelectedOnTopTab(1);
                    return;
                default:
                    return;
            }
        } else {
            switch (this.a.nNowTabIndex) {
                case 1:
                    this.a.setSelectMainTabIndex(0);
                    this.a.setSelectedRecommendTab(0);
                    return;
                case 2:
                    this.a.setSelectMainTabIndex(0);
                    this.a.setSelectedRecommendTab(2);
                    return;
                case 3:
                    this.a.setSelectMainTabIndex(2);
                    this.a.setSelectedOnTopTab(1);
                    return;
                default:
                    return;
            }
        }
    }

    public final void b(ch chVar) {
        View a2 = chVar.a();
        chVar.a(this.a.getResources().getColor(R.color.ac_text_tab_nor));
        chVar.b((int) (this.a.getResources().getDimension(R.dimen.home_tab_nor_size) / b.b));
        a2.findViewById(R.id.appcenter_tab_title_indicater).setVisibility(4);
        int unused = this.a.nLastTabIndex = this.a.getTabIndex(chVar);
        if (chVar.c() != null) {
            chVar.c().setVisibility(4);
        }
    }
}
