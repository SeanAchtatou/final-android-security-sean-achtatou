package com.tencent.module.appcenter;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ListView;

public class ScollLockedListView extends ListView {
    private WorkSpaceView a;

    public ScollLockedListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ScollLockedListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public final void a(WorkSpaceView workSpaceView) {
        this.a = workSpaceView;
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (this.a == null || !this.a.a || this.a.b) {
            return super.dispatchTouchEvent(motionEvent);
        }
        this.a.dispatchTouchEvent(motionEvent);
        return true;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return super.onInterceptTouchEvent(motionEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.a.a();
        return super.onTouchEvent(motionEvent);
    }
}
