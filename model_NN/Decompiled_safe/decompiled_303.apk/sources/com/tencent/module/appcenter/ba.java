package com.tencent.module.appcenter;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;

final class ba extends Handler {
    private /* synthetic */ SoftWareActivity a;

    ba(SoftWareActivity softWareActivity) {
        this.a = softWareActivity;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        Object[] objArr = (Object[]) message.obj;
        String str = (String) objArr[0];
        Bitmap bitmap = (Bitmap) objArr[1];
        if (str.equalsIgnoreCase(this.a.software.b())) {
            Message message2 = new Message();
            message2.what = 6;
            message2.obj = bitmap;
            this.a.async_handler.sendMessage(message2);
            return;
        }
        for (int i = 0; i < this.a.picURL.size(); i++) {
            if (str.equalsIgnoreCase((String) this.a.picURL.get(i))) {
                Message message3 = new Message();
                message3.what = 3;
                message3.obj = bitmap;
                message3.arg1 = i;
                this.a.async_handler.sendMessage(message3);
                return;
            }
        }
    }
}
