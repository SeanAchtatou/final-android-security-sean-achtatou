package com.tencent.module.appcenter;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.tencent.module.download.s;
import com.tencent.module.download.y;

final class at implements ServiceConnection {
    private /* synthetic */ SoftWareActivity a;

    at(SoftWareActivity softWareActivity) {
        this.a = softWareActivity;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        y unused = this.a.mService = s.a(iBinder);
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        y unused = this.a.mService = null;
    }
}
