package com.tencent.module.appcenter;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;
import com.tencent.qqlauncher.R;

public final class dn {
    public int a = 0;
    public int b = 1;
    protected View c = null;
    protected TextView d = null;
    protected TextView e = null;
    protected Handler f = new bp(this);
    final /* synthetic */ AppCenterActivity g;
    /* access modifiers changed from: private */
    public int h = 0;
    private ListView i = null;
    private Handler j = new bq(this);
    private Handler k = new bo(this);
    private AbsListView.OnScrollListener l = new bn(this);
    /* access modifiers changed from: private */
    public View.OnClickListener m = new bv(this);

    public dn(AppCenterActivity appCenterActivity, int i2, ListView listView, View view) {
        this.g = appCenterActivity;
        this.h = i2;
        this.c = view;
        this.d = (TextView) this.c.findViewById(R.id.TextView01);
        this.e = (TextView) this.c.findViewById(R.id.WaitingBtn);
        this.d.setVisibility(0);
        this.c.setVisibility(0);
        this.e.setVisibility(8);
        this.i = listView;
        this.i.addFooterView(this.c);
        this.i.setOnScrollListener(this.l);
    }

    static /* synthetic */ void c(dn dnVar) {
        dnVar.a = 0;
        dnVar.b++;
        dnVar.d.setVisibility(0);
        dnVar.c.setVisibility(0);
        dnVar.e.setVisibility(8);
    }

    static /* synthetic */ void d(dn dnVar) {
        dnVar.j.removeMessages(0);
        Message obtain = Message.obtain();
        obtain.what = 0;
        dnVar.j.sendMessage(obtain);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        Message obtain = Message.obtain();
        obtain.what = 0;
        this.j.sendMessageDelayed(obtain, 30000);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.a = 3;
        this.i.removeFooterView(this.c);
    }

    public final void c() {
        this.c.setVisibility(0);
    }

    public final void d() {
        this.c.setVisibility(8);
    }
}
