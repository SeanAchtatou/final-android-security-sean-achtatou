package com.tencent.module.appcenter;

import android.content.Intent;
import android.view.View;

final class cm implements View.OnClickListener {
    private /* synthetic */ SoftWareActivity a;

    cm(SoftWareActivity softWareActivity) {
        this.a = softWareActivity;
    }

    public final void onClick(View view) {
        d.a();
        Integer num = (Integer) view.getTag();
        Intent intent = new Intent(this.a, SearchTagActivity.class);
        intent.putExtra("labelName", (String) this.a.labelMap.get(num));
        intent.putExtra("labelId", num.intValue());
        this.a.startActivity(intent);
    }
}
