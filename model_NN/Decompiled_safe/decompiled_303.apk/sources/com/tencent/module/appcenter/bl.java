package com.tencent.module.appcenter;

import AndroidDLoader.StatData;
import AndroidDLoader.StatList;
import AndroidDLoader.b;
import android.content.SharedPreferences;
import android.util.Log;
import com.tencent.launcher.base.BaseApp;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

final class bl implements ah {
    private Map a = new ConcurrentHashMap();
    private Map b = new ConcurrentHashMap();
    private /* synthetic */ g c;

    bl(g gVar) {
        this.c = gVar;
    }

    public final byte a() {
        return 2;
    }

    public final void a(b bVar) {
        byte a2 = (byte) bVar.a();
        if (this.a.containsKey(Byte.valueOf(a2))) {
            this.a.put(Byte.valueOf(a2), Integer.valueOf(((Integer) this.a.get(Byte.valueOf(a2))).intValue() + 1));
        } else {
            this.a.put(Byte.valueOf(a2), 1);
        }
        String str = "addLocalApkCount key:" + bVar.toString() + " conut:" + this.a.get(Byte.valueOf(a2));
        if (str == null) {
            str = "............";
        }
        Log.v("StatManager", str);
    }

    public final void b() {
        int i;
        SharedPreferences sharedPreferences = BaseApp.b().getSharedPreferences("apkStat", 0);
        for (byte b2 = 0; b2 < 6; b2 = (byte) (b2 + 1)) {
            b a2 = b.a(b2);
            if (a2 != null && (i = sharedPreferences.getInt(a2.toString(), -1)) > 0) {
                this.b.put(Byte.valueOf(b2), Integer.valueOf(i));
            }
        }
    }

    public final void c() {
        SharedPreferences.Editor edit = BaseApp.b().getSharedPreferences("apkStat", 0).edit();
        for (Byte b2 : this.a.keySet()) {
            edit.putInt(b.a(b2.byteValue()).toString(), ((Integer) this.a.get(b2)).intValue());
        }
        edit.commit();
    }

    public final void d() {
        for (Byte b2 : this.b.keySet()) {
            int intValue = ((Integer) this.b.get(b2)).intValue();
            int i = 0;
            if (this.a.containsKey(b2)) {
                i = ((Integer) this.a.get(b2)).intValue();
            }
            this.a.put(b2, Integer.valueOf(i + intValue));
        }
    }

    public final void e() {
        SharedPreferences.Editor edit = BaseApp.b().getSharedPreferences("apkStat", 0).edit();
        for (Byte byteValue : this.b.keySet()) {
            edit.putInt(b.a(byteValue.byteValue()).toString(), 0);
        }
        edit.commit();
        this.b.clear();
    }

    public final StatList f() {
        if (this.b.size() <= 0) {
            return null;
        }
        StatList statList = new StatList();
        statList.a = new ArrayList();
        for (Byte b2 : this.b.keySet()) {
            int intValue = ((Integer) this.b.get(b2)).intValue();
            StatData statData = new StatData();
            statData.c = 2;
            statData.d = intValue;
            statData.b = b2.byteValue();
            statList.a.add(statData);
        }
        return statList;
    }
}
