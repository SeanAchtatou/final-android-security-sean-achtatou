package com.tencent.module.appcenter;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.tencent.a.a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class AutoBreakLineLayout extends ViewGroup {
    /* access modifiers changed from: private */
    public final int a;
    private final int b;
    private List c = Collections.emptyList();

    public AutoBreakLineLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.a);
        this.a = obtainStyledAttributes.getDimensionPixelSize(0, 5);
        this.b = obtainStyledAttributes.getDimensionPixelSize(1, 5);
        obtainStyledAttributes.recycle();
    }

    private static int a(int i, int i2, int i3) {
        if (i == -1) {
            return View.MeasureSpec.makeMeasureSpec(i2, 1073741824);
        }
        if (i != -2) {
            return View.MeasureSpec.makeMeasureSpec(i, 1073741824);
        }
        return View.MeasureSpec.makeMeasureSpec(i2, i3 == 0 ? 0 : Integer.MIN_VALUE);
    }

    private List a() {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < getChildCount(); i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() != 8) {
                arrayList.add(childAt);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int measuredWidth = getMeasuredWidth() - getPaddingRight();
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        Iterator it = this.c.iterator();
        int i5 = paddingLeft;
        ag agVar = (ag) it.next();
        for (View view : a()) {
            int measuredWidth2 = view.getMeasuredWidth();
            int measuredHeight = view.getMeasuredHeight();
            if (i5 + measuredWidth2 > measuredWidth) {
                i5 = getPaddingLeft();
                paddingTop += agVar.d + this.b;
                if (it.hasNext()) {
                    agVar = (ag) it.next();
                }
            }
            view.layout(i5, paddingTop, i5 + measuredWidth2, measuredHeight + paddingTop);
            i5 += measuredWidth2 + this.a;
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i) - (getPaddingLeft() + getPaddingRight());
        int size2 = View.MeasureSpec.getSize(i2) - (getPaddingTop() + getPaddingBottom());
        ArrayList arrayList = new ArrayList();
        ag agVar = new ag(this, size, mode);
        arrayList.add(agVar);
        ag agVar2 = agVar;
        for (View view : a()) {
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            view.measure(a(layoutParams.width, size, mode), a(layoutParams.height, size2, mode2));
            int measuredWidth = view.getMeasuredWidth();
            int measuredHeight = view.getMeasuredHeight();
            if (agVar2.a(measuredWidth)) {
                agVar2 = new ag(this, size, mode);
                arrayList.add(agVar2);
            }
            agVar2.a(measuredWidth, measuredHeight);
        }
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i3 < arrayList.size()) {
            ag agVar3 = (ag) arrayList.get(i3);
            i4 += agVar3.a();
            if (i3 < arrayList.size() - 1) {
                i4 += this.b;
            }
            i3++;
            i5 = Math.max(i5, agVar3.b());
        }
        setMeasuredDimension(mode == 1073741824 ? View.MeasureSpec.getSize(i) : getPaddingLeft() + getPaddingRight() + i5, mode2 == 1073741824 ? View.MeasureSpec.getSize(i2) : getPaddingTop() + getPaddingBottom() + i4);
        this.c = Collections.unmodifiableList(arrayList);
    }
}
