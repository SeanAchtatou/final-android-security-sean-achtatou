package com.tencent.module.appcenter;

import AndroidDLoader.a;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;

public class SearchTagActivity extends TActivity {
    /* access modifiers changed from: private */
    public d ctrl;
    /* access modifiers changed from: private */
    public Handler handler = new k(this);
    /* access modifiers changed from: private */
    public int labelId;
    private String labelName;
    /* access modifiers changed from: private */
    public dm listAdapter;
    /* access modifiers changed from: private */
    public ae listMoreItem;
    private be moreDataListener = new j(this);
    /* access modifiers changed from: private */
    public int requestId;
    private ArrayList resultList;
    private ListView searchTagList;

    /* access modifiers changed from: private */
    public void onReceiveSearchResults(ArrayList arrayList) {
        if (this.listAdapter == null && (arrayList == null || arrayList.size() == 0)) {
            Toast.makeText(this, "暂无结果", 1).show();
        }
        if (arrayList == null || arrayList.size() == 0) {
            this.listMoreItem.b();
        } else {
            this.listMoreItem.c();
            this.resultList.addAll(arrayList);
        }
        this.listAdapter.a(arrayList);
        this.listAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.search_tag_list);
        this.searchTagList = (ListView) findViewById(R.id.listview_search_tag);
        Intent intent = getIntent();
        this.labelId = intent.getIntExtra("labelId", -1);
        this.labelName = intent.getStringExtra("labelName");
        ((TextView) findViewById(R.id.title_text)).setText(this.labelName);
        this.ctrl = d.a();
        this.listMoreItem = new ae(this.searchTagList, getLayoutInflater().inflate((int) R.layout.list_waiting, (ViewGroup) null), this.moreDataListener);
        this.resultList = new ArrayList();
        if (this.listAdapter == null) {
            this.listAdapter = new dm(this, this);
            this.searchTagList.setAdapter((ListAdapter) this.listAdapter);
        }
        g.a().a(a.f);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        d.a().b();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public boolean onProgressDialogBack() {
        this.ctrl.a(this.requestId);
        return super.onProgressDialogBack();
    }

    public boolean onRetry() {
        return false;
    }

    public boolean onTimeout() {
        return false;
    }
}
