package com.tencent.module.appcenter;

import android.view.View;
import com.tencent.qqlauncher.R;

final class cp implements View.OnClickListener {
    private /* synthetic */ SoftWareActivity a;

    cp(SoftWareActivity softWareActivity) {
        this.a = softWareActivity;
    }

    public final void onClick(View view) {
        int length = this.a.textSDescription.length();
        if (this.a.readingFull) {
            this.a.infoContent.setText(length > 100 ? this.a.textSDescription.substring(0, 100) + "..." : this.a.textSDescription);
            this.a.readFullTxt.setText((int) R.string.read_full);
        } else {
            this.a.readFullTxt.setText((int) R.string.read_five);
            this.a.infoContent.setText(this.a.textSDescription);
        }
        if (this.a.readingFull) {
            this.a.readFullImg.startAnimation(this.a.animation_down);
            boolean unused = this.a.TOOL_IS_UP = false;
        } else {
            this.a.readFullImg.startAnimation(this.a.animation_up);
            boolean unused2 = this.a.TOOL_IS_UP = true;
        }
        this.a.readingFull = !this.a.readingFull;
    }
}
