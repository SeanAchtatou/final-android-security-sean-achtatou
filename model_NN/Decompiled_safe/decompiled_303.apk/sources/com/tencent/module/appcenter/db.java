package com.tencent.module.appcenter;

import AndroidDLoader.ResSoftDetailPack;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;
import com.tencent.android.a.b;
import com.tencent.launcher.base.BaseApp;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;
import java.util.List;

final class db extends Handler {
    private /* synthetic */ SoftWareActivity a;

    db(SoftWareActivity softWareActivity) {
        this.a = softWareActivity;
    }

    public final void handleMessage(Message message) {
        String str = "on net:" + message.what;
        if (str == null) {
            str = "............";
        }
        Log.v("SoftwareActivity", str);
        switch (message.what) {
            case 900:
                this.a.dismissProgressDialog();
                Toast.makeText(BaseApp.b(), b.a(message.arg1), 1).show();
                return;
            case 1001:
                this.a.dismissProgressDialog();
                this.a.onReceiveAddComment(message.arg1);
                return;
            case 1002:
                this.a.onReceiveComment((List) message.obj);
                return;
            case 1006:
                this.a.dismissProgressDialog();
                this.a.onReceiveSoftDetail((ResSoftDetailPack) message.obj);
                return;
            case 1309:
                if (message.arg2 == this.a.software.a) {
                    this.a.dismissProgressDialog();
                    Toast.makeText(BaseApp.b(), (int) R.string.add_tag_success, 1).show();
                    this.a.infoEditTags.setText("");
                    SoftWareActivity.userTagged.put(this.a.software.l, "tagged");
                    this.a.canWriteTag();
                    return;
                }
                return;
            case 1310:
                return;
            case 1312:
                this.a.dismissProgressDialog();
                Toast.makeText(BaseApp.b(), this.a.getString(R.string.add_tag_failed) + "ID=" + message.arg1, 1).show();
                return;
            case 1700:
                this.a.onReceiveRelativeList((ArrayList) message.obj);
                return;
            case 1701:
                Toast.makeText(BaseApp.b(), "获取相关软件失败", 1).show();
                return;
            case 1801:
                Toast.makeText(this.a, (int) R.string.report_success, 1).show();
                return;
            case 1802:
                Toast.makeText(this.a, (int) R.string.report_failed, 1).show();
                return;
            case 2200:
                this.a.dismissProgressDialog();
                Toast.makeText(this.a, "服务器错误, ID=" + message.arg1, 1).show();
                return;
            case 3000:
            case 3001:
                if (this.a.software != null) {
                    this.a.canWriteTag();
                    this.a.updateDownloadStatus();
                    this.a.bindMarkLayout();
                    return;
                }
                return;
            default:
                this.a.dismissProgressDialog();
                Toast.makeText(this.a, "服务器错误, ID=" + message.arg1, 1).show();
                return;
        }
    }
}
