package com.tencent.module.appcenter;

import AndroidDLoader.Comment;
import AndroidDLoader.ResSoftDetailPack;
import AndroidDLoader.Software;
import AndroidDLoader.a;
import android.app.Activity;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import com.tencent.launcher.Launcher;
import com.tencent.launcher.am;
import com.tencent.launcher.bl;
import com.tencent.launcher.ff;
import com.tencent.launcher.ha;
import com.tencent.module.download.DownloadInfo;
import com.tencent.module.download.DownloadService;
import com.tencent.module.download.b;
import com.tencent.module.download.y;
import com.tencent.module.theme.ThemeSettingActivity;
import com.tencent.qqlauncher.R;
import com.tencent.util.p;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class SoftWareActivity extends TActivity {
    private static final byte File_Certified_FAIL = -1;
    private static final byte File_Certified_OK = 1;
    private static final byte File_Certified_UNTESTED = 0;
    protected static final int MAX_LENGTH = 100;
    public static final int MSG_DOWNLOAD_SOFTGIC = 3;
    public static final int MSG_DOWNLOAD_SOFTICON = 6;
    public static final int MSG_SET_ADAPTER = 4;
    public static final int MSG_SET_NO_COMMENTS = 5;
    public static final int MSG_UPDATE_PROGRESS = 2;
    public static final String ORIGINE_PIC_EXT_NAME = "480.png";
    public static final int PAGE_SIZE = 20;
    public static final int SOFTWARE_DOWNLOADING = 1;
    public static final int SOFTWARE_DOWNLOAD_DELETE = 3;
    public static final int SOFTWARE_DOWNLOAD_OVER = 2;
    public static final int SOFTWARE_DOWNLOAD_READY = 0;
    public static final int TAB_INTRODUCE = 0;
    public static final int TAB_MARK = 1;
    public static final int TAB_RELATIVE = 2;
    protected static final String TAG = "SoftwareActivity";
    public static Drawable snapShotCache;
    /* access modifiers changed from: private */
    public static HashMap userTagged;
    private final int DOWNLOADING = 1;
    private final int DOWNLOAD_CANCEL = 4;
    private final int DOWNLOAD_ERROR = 3;
    private final int DOWNLOAD_OVER = 2;
    private final int NO_DOWNLOAD = 0;
    /* access modifiers changed from: private */
    public boolean TOOL_IS_UP = true;
    private View.OnClickListener addLabelBtnLisnener = new cs(this);
    /* access modifiers changed from: private */
    public String addLabelName;
    RotateAnimation animation_down = null;
    RotateAnimation animation_up = null;
    /* access modifiers changed from: private */
    public Handler async_handler = new cv(this);
    /* access modifiers changed from: private */
    public boolean bDownloadButtonStatusIsUpdate = false;
    private byte bHasComment;
    private boolean bIsSafe = false;
    /* access modifiers changed from: private */
    public boolean bStartSoftware = false;
    private View.OnClickListener badReportBtnLisnener = new cw(this);
    private View buttonLayout;
    private ImageView cancelButton;
    private View.OnClickListener cancelClickListener = new ck(this);
    private int col;
    /* access modifiers changed from: private */
    public EditText commentContent;
    private List commentsData;
    private int currentPage = 1;
    Button downloadAction;
    Button downloadButton;
    View.OnClickListener downloadButtonListener = new cl(this);
    private ServiceConnection downloadConnection;
    /* access modifiers changed from: private */
    public int downloadStatus = 0;
    TextView downloadingText;
    private View.OnClickListener favouriteBtnLisnener = new cu(this);
    /* access modifiers changed from: private */
    public Handler handler = new db(this);
    private Handler iconHandler = new ba(this);
    TextView infoContent;
    View infoDevLayout;
    TextView infoDeveloper;
    TextView infoDownloadNumber;
    EditText infoEditTags;
    TextView infoFileSize;
    TextView infoPublishDate;
    View infoQQSafeTop;
    ImageView infoSnapshot1;
    ImageView infoSnapshot2;
    private AutoBreakLineLayout infoTagPop;
    private RelativeLayout infoTags;
    Button infobtBadReport;
    Button infobtFav;
    Button infobtShare;
    Button infobtTags;
    private int initTab;
    private byte isCertified = 0;
    /* access modifiers changed from: private */
    public Map labelMap;
    private LayoutInflater layoutInflater;
    /* access modifiers changed from: private */
    public ArrayList mAppList = new ArrayList();
    /* access modifiers changed from: private */
    public b mCallback;
    private int mFileid;
    /* access modifiers changed from: private */
    public Handler mHandler = new ay(this);
    private int mPreActivity = 0;
    /* access modifiers changed from: private */
    public int mProductid;
    /* access modifiers changed from: private */
    public y mService;
    /* access modifiers changed from: private */
    public int mSoftid;
    /* access modifiers changed from: private */
    public Button markBt;
    private View.OnClickListener markBtSubmit = new av(this);
    private View markEditLayout;
    /* access modifiers changed from: private */
    public ListView markList;
    private da markListAdapter;
    /* access modifiers changed from: private */
    public ae markListMoreItem;
    /* access modifiers changed from: private */
    public TextView markStartText;
    private be moreMarkDataListener = new au(this);
    private be moreRelativeDataListener = new aw(this);
    private int nCommentCount;
    int nCurrentVerison;
    int nDownloadedVersion;
    int nLatestVersion;
    private int nSoftWareDownloadStatus;
    /* access modifiers changed from: private */
    public int nStartSoftwareIndex = -1;
    private TextView noRelative;
    private TextView nomark;
    private View.OnClickListener onDownloadButtonClickListener = new dc(this);
    private ArrayList picOriURL;
    /* access modifiers changed from: private */
    public ArrayList picURL;
    private ResSoftDetailPack pkgSoftDetail;
    private View.OnClickListener popTextClick = new cn(this);
    private ProgressBar processbar;
    private View processbarLayout;
    private RatingBar ratingBarEdit;
    RatingBar ratingBarSmall;
    private View.OnClickListener readFullClickListener = new cp(this);
    /* access modifiers changed from: private */
    public ImageView readFullImg;
    /* access modifiers changed from: private */
    public TextView readFullTxt;
    private View readerFull;
    protected boolean readingFull;
    /* access modifiers changed from: private */
    public ListView relativeList;
    private ae relativeListMoreItem;
    private CompoundButton.OnCheckedChangeListener reportCheckListener = new di(this);
    private ArrayList reports;
    /* access modifiers changed from: private */
    public int requestId;
    private int row;
    private ff sModel;
    private ScrollView scroll;
    private View.OnClickListener shareBtnLisnener = new cx(this);
    private View.OnClickListener snapOnClick = new cr(this);
    private View.OnClickListener snapShotClick = new co(this);
    /* access modifiers changed from: private */
    public Software software;
    ImageView softwareIcon;
    View softwareLayout;
    TextView softwareMark;
    TextView softwareName;
    TextView softwareTitle;
    private ImageView startButton;
    private View.OnClickListener startClickListener = new cj(this);
    /* access modifiers changed from: private */
    public a stat = null;
    /* access modifiers changed from: private */
    public bm swRelativeAdapter;
    private ImageView tab1Bg;
    private ImageView tab2Bg;
    private ImageView tab3Bg;
    private ViewFlipper tabContent;
    private View tabIntroduce;
    private View tabMark;
    private View tabRelative;
    private ImageView[] tabsBg;
    private TextView[] tabsText;
    private int[] tagBackground = {R.color.tag_color_1, R.color.tag_color_2, R.color.tag_color_3, R.color.tag_color_4, R.color.tag_color_5};
    View.OnClickListener tagClickListener = new cm(this);
    private View tagLayout;
    private View tagPopContainer;
    private PopupWindow tagPopWindow;
    private int[] tagSize = {14, 15, 16, 17, 18, 19, 20};
    private TextView textIntroduce;
    private TextView textMark;
    private TextView textRelative;
    /* access modifiers changed from: private */
    public String textSDescription;
    /* access modifiers changed from: private */
    public Comment userComment;
    /* access modifiers changed from: private */
    public CheckBox viewCannotDownload;
    /* access modifiers changed from: private */
    public CheckBox viewCannotInstall;
    private CheckBox viewReportCharge;
    private CheckBox viewReportInfoError;
    private CheckBox viewReportTooOld;
    private CheckBox viewReportVirsu;
    private TextView waitingText;

    private void BindDataToUI() {
        this.softwareLayout.setVisibility(0);
        this.softwareName.setText(this.software.d());
        this.ratingBarSmall.setRating(((float) this.software.m) / 2.0f);
        this.softwareMark.setText(this.nCommentCount + getString(R.string.ManypeoCom));
        this.infoPublishDate.setText(this.software.f());
        this.infoFileSize.setText(p.a((long) (this.software.h() * Launcher.APPWIDGET_HOST_ID)));
        this.infoDownloadNumber.setText(p.a(this.software.k()));
        String l = this.software.l();
        if (l == null || l.length() == 0) {
            this.infoDevLayout.setVisibility(8);
        } else {
            this.infoDeveloper.setText(l);
        }
        if (this.bIsSafe) {
            this.infoQQSafeTop.setVisibility(0);
        } else {
            this.infoQQSafeTop.setVisibility(8);
        }
        this.softwareTitle.setText(this.software.d());
        if (this.textSDescription.length() <= 100) {
            this.infoContent.setText(this.textSDescription);
            this.readerFull.setVisibility(8);
            return;
        }
        this.infoContent.setText(this.textSDescription.substring(0, 100) + "...");
        this.readerFull.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void EnableOtherCheckbox(boolean z) {
        this.viewReportCharge.setEnabled(z);
        this.viewReportVirsu.setEnabled(z);
        this.viewReportInfoError.setEnabled(z);
        this.viewReportTooOld.setEnabled(z);
    }

    private void JudgeDownloadButtonStatus() {
        PackageInfo packageInfo;
        int i = 0;
        if (!judgeIfFileExist()) {
            if (this.nSoftWareDownloadStatus == 0) {
                while (true) {
                    if (i >= this.mAppList.size()) {
                        break;
                    } else if (((am) this.mAppList.get(i)).a.equals(this.software.b)) {
                        try {
                            packageInfo = getPackageManager().getPackageInfo(this.software.j(), 0);
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                            packageInfo = null;
                        }
                        if (packageInfo != null) {
                            int intValue = Integer.valueOf(packageInfo.versionCode).intValue();
                            int intValue2 = Integer.valueOf(this.software.e()).intValue();
                            if (intValue2 > intValue) {
                                this.downloadAction.setTextColor(-1);
                                this.downloadAction.setBackgroundDrawable(getResources().getDrawable(R.drawable.software_button_highlight));
                                this.downloadAction.setText("更新");
                                this.bDownloadButtonStatusIsUpdate = true;
                            } else if (intValue2 == intValue) {
                                this.downloadAction.setText("启动");
                                this.bStartSoftware = true;
                                this.nStartSoftwareIndex = i;
                            }
                        }
                    } else {
                        i++;
                    }
                }
                if (!this.bDownloadButtonStatusIsUpdate && !this.bStartSoftware) {
                    this.downloadAction.setText("下载");
                }
            } else if (this.nSoftWareDownloadStatus == 1) {
                this.downloadAction.setText("下载中");
            } else if (this.nSoftWareDownloadStatus == 2) {
                this.downloadAction.setText("安装");
            }
        }
    }

    /* access modifiers changed from: private */
    public void bindMarkLayout() {
        String str = "hascommented onbindMarkLayout" + ((int) this.bHasComment);
        if (str == null) {
            str = "............";
        }
        Log.v("Test", str);
        if (this.bHasComment == 0 && isSoftwareInstalled()) {
            enableMarkLayout(true);
            this.commentContent.setHint((int) R.string.pls_input_mark);
            this.commentContent.setText("");
        } else if (this.bHasComment == 1 && isSoftwareInstalled()) {
            enableMarkLayout(false);
            this.commentContent.setHint((int) R.string.has_remarket);
            this.commentContent.setText("");
        } else if (!isSoftwareInstalled()) {
            enableMarkLayout(false);
            this.commentContent.setHint((int) R.string.not_installed);
            this.commentContent.setText("");
        }
    }

    private void bindSnapshot(ControlledScrollView controlledScrollView, Bitmap[] bitmapArr) {
        int length = bitmapArr.length;
        for (int i = 0; i < length; i++) {
            ImageView imageView = new ImageView(this);
            imageView.setBackgroundResource(R.drawable.app_single_snapshot_bg);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setPadding(0, 0, 0, 0);
            imageView.setLayoutParams(new FrameLayout.LayoutParams(p.a((Activity) this) - 15, 200));
            controlledScrollView.a(imageView);
        }
    }

    private void bindTagLayout() {
        if (isSoftwareInstalled()) {
            this.infobtTags.setEnabled(true);
            this.infoEditTags.setEnabled(true);
            this.infoEditTags.setHint((int) R.string.pls_input_tag);
        } else if (!isSoftwareInstalled()) {
            this.infobtTags.setEnabled(false);
            this.infoEditTags.setEnabled(false);
            this.infoEditTags.setHint((int) R.string.not_installed_tag);
        }
    }

    private void bindTags() {
        ArrayList arrayList = new ArrayList();
        for (Integer num : this.labelMap.keySet()) {
            TextView textView = new TextView(this);
            textView.setText((String) this.labelMap.get(num));
            textView.setTag(num);
            textView.setOnClickListener(this.tagClickListener);
            arrayList.add(textView);
            if (arrayList.size() >= 10) {
                break;
            }
        }
        genPositionForTag(arrayList);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < arrayList.size()) {
                this.infoTags.addView((View) arrayList.get(i2));
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private void buildTagPopWindow() {
        for (String text : this.labelMap.values()) {
            TextView textView = new TextView(this);
            textView.setText(text);
            textView.setOnClickListener(this.popTextClick);
            this.infoTagPop.addView(textView);
        }
    }

    private int calcDownloadingProcess(DownloadInfo downloadInfo) {
        return 0;
    }

    /* access modifiers changed from: private */
    public void canWriteTag() {
        if (!isSoftwareInstalled()) {
            this.infoEditTags.setHint((int) R.string.not_installed_for_tag);
            this.infoEditTags.setEnabled(false);
            this.infobtTags.setEnabled(false);
            this.infobtTags.setTextColor(-7960954);
            this.infoTags.setEnabled(false);
            this.infoEditTags.setFocusable(false);
        } else if (userTagged.containsKey(this.software.l)) {
            this.infoEditTags.setHint((int) R.string.has_tagged);
            this.infoEditTags.setEnabled(false);
            this.infobtTags.setEnabled(false);
            this.infobtTags.setTextColor(-7960954);
            this.infoTags.setEnabled(false);
            this.infoEditTags.setFocusable(false);
        } else {
            this.infoEditTags.setFocusable(false);
            this.infoEditTags.setHint((int) R.string.pls_input_tag);
            this.infobtTags.setEnabled(true);
            this.infoEditTags.setEnabled(true);
            this.infobtTags.setTextColor(-14869219);
            this.infoTags.setEnabled(true);
            this.infoEditTags.setFocusable(true);
            this.infoEditTags.clearFocus();
            this.softwareLayout.requestFocus();
        }
    }

    private void createAddLabelDialog() {
        AlertDialogAdapter alertDialogAdapter = new AlertDialogAdapter(this);
        alertDialogAdapter.d = R.string.add_lable;
        alertDialogAdapter.a(R.layout.add_label);
        alertDialogAdapter.b = R.string.str_ok;
        alertDialogAdapter.c = R.string.str_cancel;
        alertDialogAdapter.a(new dh(this, alertDialogAdapter), new dg(this, alertDialogAdapter));
        alertDialogAdapter.show();
    }

    /* access modifiers changed from: private */
    public void createBadReportDialog() {
        this.reports = new ArrayList();
        ArrayList arrayList = new ArrayList();
        arrayList.add(getString(R.string.report_cannot_download));
        arrayList.add(getString(R.string.report_cannot_install));
        arrayList.add(getString(R.string.report_charge));
        arrayList.add(getString(R.string.report_virsu));
        arrayList.add(getString(R.string.report_info_error));
        arrayList.add(getString(R.string.report_too_old));
        AlertDialogAdapter alertDialogAdapter = new AlertDialogAdapter(this);
        alertDialogAdapter.d = R.string.bad_report_bt;
        alertDialogAdapter.a(R.layout.report_bad_item);
        alertDialogAdapter.b = R.string.str_ok;
        alertDialogAdapter.c = R.string.str_cancel;
        alertDialogAdapter.a(new ct(this), new dk(this, alertDialogAdapter));
        this.viewCannotDownload = (CheckBox) alertDialogAdapter.findViewById(R.id.checkbox_item_1);
        this.viewCannotDownload.setText(getString(R.string.report_cannot_download));
        this.viewCannotDownload.setOnCheckedChangeListener(this.reportCheckListener);
        this.reports.add(this.viewCannotDownload);
        this.viewCannotInstall = (CheckBox) alertDialogAdapter.findViewById(R.id.checkbox_item_2);
        this.viewCannotInstall.setText(getString(R.string.report_cannot_install));
        this.viewCannotInstall.setOnCheckedChangeListener(this.reportCheckListener);
        this.reports.add(this.viewCannotInstall);
        this.viewReportCharge = (CheckBox) alertDialogAdapter.findViewById(R.id.checkbox_item_3);
        this.viewReportCharge.setText(getString(R.string.report_charge));
        this.reports.add(this.viewReportCharge);
        this.viewReportVirsu = (CheckBox) alertDialogAdapter.findViewById(R.id.checkbox_item_4);
        this.viewReportVirsu.setText(getString(R.string.report_virsu));
        this.reports.add(this.viewReportVirsu);
        this.viewReportInfoError = (CheckBox) alertDialogAdapter.findViewById(R.id.checkbox_item_5);
        this.viewReportInfoError.setText(getString(R.string.report_info_error));
        this.reports.add(this.viewReportInfoError);
        this.viewReportTooOld = (CheckBox) alertDialogAdapter.findViewById(R.id.checkbox_item_6);
        this.viewReportTooOld.setText(getString(R.string.report_too_old));
        this.reports.add(this.viewReportTooOld);
        alertDialogAdapter.show();
    }

    private void createShowImageDialog(Drawable drawable) {
        View inflate = this.layoutInflater.inflate((int) R.layout.show_app_snapshot, (ViewGroup) null);
        PopupWindow popupWindow = new PopupWindow(this);
        popupWindow.setWidth(-1);
        popupWindow.setHeight(-1);
        popupWindow.setFocusable(true);
        popupWindow.setTouchable(false);
        popupWindow.setContentView(inflate);
        popupWindow.showAtLocation(this.softwareLayout, 17, 0, 0);
    }

    private void enableMarkLayout(boolean z) {
        this.commentContent.setEnabled(z);
        this.ratingBarEdit.setEnabled(z);
        if (!z) {
            this.markBt.setEnabled(false);
            this.markBt.setTextColor(-7960954);
        }
    }

    private void getAllApp() {
        this.sModel = Launcher.getModel();
        bl e = this.sModel.e();
        if (e == null) {
            finish();
            return;
        }
        int count = e.getCount();
        this.mAppList.clear();
        for (int i = 0; i < count; i++) {
            ha haVar = (ha) e.getItem(i);
            if (haVar instanceof am) {
                this.mAppList.add((am) haVar);
            }
        }
    }

    private String getOriSnapshotUrl(String str) {
        try {
            return str.substring(0, str.length() - 7) + ORIGINE_PIC_EXT_NAME;
        } catch (Exception e) {
            return str;
        }
    }

    private void initHeader() {
        this.softwareIcon = (ImageView) findViewById(R.id.software_icon);
        this.softwareName = (TextView) findViewById(R.id.software_name);
        this.ratingBarSmall = (RatingBar) findViewById(R.id.RatingBar01);
        this.softwareMark = (TextView) findViewById(R.id.software_mark);
        this.downloadAction = (Button) findViewById(R.id.downloadAction);
        this.downloadAction.setOnClickListener(this.downloadButtonListener);
        this.downloadButton = (Button) findViewById(R.id.update_button);
        this.downloadingText = (TextView) findViewById(R.id.downloading_text);
        this.softwareTitle = (TextView) findViewById(R.id.title_text);
        this.downloadButton.setOnClickListener(this.onDownloadButtonClickListener);
        this.buttonLayout = findViewById(R.id.button_layout);
        this.processbarLayout = findViewById(R.id.process_layout);
        this.processbar = (ProgressBar) findViewById(R.id.processbar);
        this.startButton = (ImageView) findViewById(R.id.start_button);
        this.cancelButton = (ImageView) findViewById(R.id.cancel_button);
        this.waitingText = (TextView) findViewById(R.id.waiting_text);
        this.startButton.setOnClickListener(this.startClickListener);
        this.cancelButton.setOnClickListener(this.cancelClickListener);
    }

    private void initIntroduce() {
        this.infoPublishDate = (TextView) findViewById(R.id.sw_publishtime);
        this.infoFileSize = (TextView) findViewById(R.id.sw_filesize);
        this.infoDownloadNumber = (TextView) findViewById(R.id.sw_download_times);
        this.infoDeveloper = (TextView) findViewById(R.id.sw_developer);
        this.infoQQSafeTop = findViewById(R.id.safety_certification);
        this.infoContent = (TextView) findViewById(R.id.sw_content);
        this.infoSnapshot1 = (ImageView) findViewById(R.id.snapshot_1);
        this.infoSnapshot1.setOnClickListener(this.snapShotClick);
        this.infoSnapshot2 = (ImageView) findViewById(R.id.snapshot_2);
        this.infoSnapshot2.setOnClickListener(this.snapShotClick);
        this.infobtFav = (Button) findViewById(R.id.bt_favourite);
        this.infobtShare = (Button) findViewById(R.id.bt_share);
        this.infobtBadReport = (Button) findViewById(R.id.bt_bad_report);
        this.infoTags = (RelativeLayout) findViewById(R.id.sw_tags);
        this.infoEditTags = (EditText) findViewById(R.id.lable_edit);
        this.infobtTags = (Button) findViewById(R.id.bt_sw_tags);
        this.infoDevLayout = findViewById(R.id.linear_dev);
        this.infobtFav.setOnClickListener(this.favouriteBtnLisnener);
        this.infobtShare.setOnClickListener(this.shareBtnLisnener);
        this.infobtBadReport.setOnClickListener(this.badReportBtnLisnener);
        this.infobtTags.setOnClickListener(this.addLabelBtnLisnener);
        this.tagPopContainer = this.layoutInflater.inflate((int) R.layout.tag_popup_window, (ViewGroup) null);
        this.infoTagPop = (AutoBreakLineLayout) this.tagPopContainer.findViewById(R.id.tag_popup);
        this.tagLayout = findViewById(R.id.tag_layout);
        this.tagPopWindow = new PopupWindow(this);
        this.tagPopWindow.setWidth(-1);
        this.tagPopWindow.setHeight(-2);
        this.tagPopWindow.setFocusable(false);
        this.tagPopWindow.setTouchable(true);
        this.tagPopWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.tag_popup_bg));
        this.tagPopWindow.setContentView(this.tagPopContainer);
        this.readerFull = findViewById(R.id.read_full_bt);
        this.readerFull.setOnClickListener(this.readFullClickListener);
        this.readFullTxt = (TextView) findViewById(R.id.read_full_txt);
        this.readFullImg = (ImageView) findViewById(R.id.read_full_img);
        this.animation_up = new RotateAnimation(0.0f, 180.0f, 1, 0.5f, 1, 0.5f);
        this.animation_down = new RotateAnimation(-180.0f, 0.0f, 1, 0.5f, 1, 0.5f);
        this.animation_up.setFillAfter(true);
        this.animation_down.setFillAfter(true);
        this.animation_down.setDuration(500);
        this.animation_up.setDuration(500);
        this.scroll = (ScrollView) findViewById(R.id.scroll_view);
        this.infoEditTags.setOnClickListener(new cq(this));
        Bitmap[] bitmapArr = {null, null, null};
    }

    private void initMarkList() {
        this.markList = (ListView) findViewById(R.id.sw_remark_list);
        View inflate = this.layoutInflater.inflate((int) R.layout.sw_detail_remark_list_header, (ViewGroup) null);
        this.markList.addHeaderView(inflate);
        this.nomark = (TextView) findViewById(R.id.no_marks);
        this.markBt = (Button) inflate.findViewById(R.id.sw_mark_submit);
        this.markBt.setOnClickListener(this.markBtSubmit);
        this.markBt.setEnabled(false);
        this.markBt.setTextColor(-7960954);
        this.markListMoreItem = new ae(this.markList, this.layoutInflater.inflate((int) R.layout.list_waiting, (ViewGroup) null), this.moreMarkDataListener);
        this.markEditLayout = inflate.findViewById(R.id.remark_layout);
        this.ratingBarEdit = (RatingBar) inflate.findViewById(R.id.edit_ratingBar);
        this.commentContent = (EditText) inflate.findViewById(R.id.sw_mark_edit);
        this.markStartText = (TextView) inflate.findViewById(R.id.mark_star_text);
        this.ratingBarEdit.setOnRatingBarChangeListener(new ax(this));
        if (this.commentsData == null) {
            this.commentsData = new ArrayList();
        }
        this.markListAdapter = new da(this, this.commentsData);
        this.markList.setAdapter((ListAdapter) this.markListAdapter);
    }

    private void initRelativeList() {
        this.relativeList = (ListView) findViewById(R.id.sw_relative_list);
        this.noRelative = (TextView) findViewById(R.id.no_relative);
        this.relativeListMoreItem = new ae(this.relativeList, this.layoutInflater.inflate((int) R.layout.list_waiting, (ViewGroup) null), this.moreRelativeDataListener);
        this.swRelativeAdapter = new bm(this, this.stat);
        this.relativeList.setAdapter((ListAdapter) this.swRelativeAdapter);
    }

    private void initTabControl() {
        this.tab1Bg = (ImageView) findViewById(R.id.imageview_tab1);
        this.tab2Bg = (ImageView) findViewById(R.id.imageview_tab2);
        this.tab3Bg = (ImageView) findViewById(R.id.imageview_tab3);
        this.tabsBg = new ImageView[]{this.tab1Bg, this.tab2Bg, this.tab3Bg};
        this.textIntroduce = (TextView) findViewById(R.id.text_introduce);
        this.textMark = (TextView) findViewById(R.id.text_mark);
        this.textRelative = (TextView) findViewById(R.id.text_relative);
        this.tabsText = new TextView[]{this.textIntroduce, this.textMark, this.textRelative};
        this.tabIntroduce = findViewById(R.id.tab_introduce);
        this.tabIntroduce.setOnClickListener(new ci(this));
        this.tabMark = findViewById(R.id.tab_mark);
        this.tabMark.setOnClickListener(new cz(this));
        this.tabRelative = findViewById(R.id.tab_relative);
        this.tabRelative.setOnClickListener(new cy(this));
        this.tabContent = (ViewFlipper) findViewById(R.id.content_viewflipper);
    }

    private void initTextViewTag(TextView textView, int i, int i2, int i3, int i4) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.leftMargin = i;
        layoutParams.topMargin = i2;
        layoutParams.addRule(9);
        layoutParams.addRule(10);
        textView.setPadding(0, 0, 0, 0);
        textView.setGravity(19);
        textView.setTextColor(i3);
        textView.setSingleLine();
        textView.setTextSize(1, (float) i4);
        textView.setLayoutParams(layoutParams);
    }

    private void initUI() {
        initHeader();
        initIntroduce();
    }

    private ArrayList initWeight(int i) {
        ArrayList arrayList = new ArrayList();
        if (this.row == 1) {
            arrayList.add(0);
            arrayList.add(2);
            arrayList.add(1);
        }
        if (this.row == 2) {
            arrayList.add(5);
            arrayList.add(0);
            arrayList.add(2);
            arrayList.add(1);
            arrayList.add(4);
            arrayList.add(6);
        } else if (this.row == 3) {
            arrayList.add(0);
            arrayList.add(6);
            arrayList.add(9);
            arrayList.add(10);
            arrayList.add(8);
            arrayList.add(1);
            arrayList.add(2);
            arrayList.add(4);
            arrayList.add(5);
        } else {
            arrayList.add(0);
            arrayList.add(6);
            arrayList.add(12);
            arrayList.add(9);
            arrayList.add(14);
            arrayList.add(4);
            arrayList.add(1);
            arrayList.add(2);
            arrayList.add(5);
            arrayList.add(8);
            arrayList.add(10);
            arrayList.add(13);
        }
        return arrayList;
    }

    private boolean isInUpdateList() {
        return false;
    }

    private boolean isSoftwareInstalled() {
        if (this.software != null) {
            getAllApp();
            for (int i = 0; i < this.mAppList.size(); i++) {
                if (((am) this.mAppList.get(i)).c.getComponent().getPackageName().equals(this.software.l)) {
                    this.nStartSoftwareIndex = i;
                    return true;
                }
            }
        }
        return false;
    }

    private boolean judgeIfFileExist() {
        String str = c.a + "/" + (ThemeSettingActivity.decodeUrl2FileName(this.software.i()) + ".apk");
        if (!new File(str).exists()) {
            return false;
        }
        updateStatus(str);
        return true;
    }

    /* access modifiers changed from: private */
    public void onReceiveSoftDetail(ResSoftDetailPack resSoftDetailPack) {
        this.pkgSoftDetail = resSoftDetailPack;
        d.a();
        this.software = resSoftDetailPack.f();
        this.textSDescription = resSoftDetailPack.c();
        this.nCommentCount = resSoftDetailPack.d();
        this.picURL = resSoftDetailPack.e();
        this.labelMap = resSoftDetailPack.g();
        this.isCertified = resSoftDetailPack.h();
        this.bHasComment = resSoftDetailPack.b();
        String str = "hascommented onreciver" + ((int) this.bHasComment);
        if (str == null) {
            str = "............";
        }
        Log.v("Test", str);
        this.picOriURL = resSoftDetailPack.i();
        this.bIsSafe = resSoftDetailPack.h() == 1;
        if (this.isCertified == 1) {
            this.infoQQSafeTop.setVisibility(0);
        } else {
            this.infoQQSafeTop.setVisibility(8);
        }
        if (this.picURL.size() > 0) {
            getSoftSnapshot();
        }
        if (this.software.l == null || this.software.l.equalsIgnoreCase("")) {
            onReceivedBlankDataFromServer(R.string.Error_Server_ReturnPackageNameNull);
        }
        updateDownloadStatus();
        JudgeDownloadButtonStatus();
        BindDataToUI();
        bindTags();
        buildTagPopWindow();
        getSoftIcon();
        canWriteTag();
    }

    public static void openSoftwareActivity(Activity activity, int i, int i2, int i3, a aVar) {
        Intent intent = new Intent();
        intent.putExtra("Software_Open_Product_ID", i);
        intent.putExtra("Software_Open_Soft_ID", i2);
        intent.putExtra("Software_Open_File_ID", i3);
        intent.putExtra("stat", aVar.a());
        intent.setClass(activity, SoftWareActivity.class);
        activity.startActivity(intent);
    }

    public static void openSoftwareActivityWithStatus(Activity activity, int i, int i2, int i3, a aVar, int i4) {
        Intent intent = new Intent();
        intent.putExtra("Software_Open_Product_ID", i);
        intent.putExtra("Software_Open_Soft_ID", i2);
        intent.putExtra("Software_Open_File_ID", i3);
        intent.putExtra("stat", aVar.a());
        intent.putExtra("SoftWareDownloadStatus", i4);
        intent.setClass(activity, SoftWareActivity.class);
        activity.startActivity(intent);
    }

    private void setStatus(DownloadInfo downloadInfo) {
        Log.v(TAG, "setStatus");
        if (downloadInfo != null && this.software != null && downloadInfo.a.equalsIgnoreCase(this.software.k)) {
            switch (downloadInfo.a()) {
                case 0:
                    this.buttonLayout.setVisibility(8);
                    this.downloadingText.setVisibility(8);
                    this.downloadingText.setVisibility(8);
                    this.processbar.setProgress(calcDownloadingProcess(downloadInfo));
                    this.processbarLayout.setVisibility(0);
                    this.cancelButton.setVisibility(0);
                    this.startButton.setBackgroundResource(R.drawable.sw_waiting);
                    this.startButton.setEnabled(false);
                    this.startButton.setVisibility(0);
                    this.waitingText.setVisibility(0);
                    return;
                case 1:
                    this.processbar.setProgress(calcDownloadingProcess(downloadInfo));
                    this.cancelButton.setVisibility(8);
                    this.waitingText.setVisibility(8);
                    this.buttonLayout.setVisibility(8);
                    this.processbarLayout.setVisibility(0);
                    this.startButton.setBackgroundResource(R.drawable.sw_pause);
                    this.startButton.setEnabled(true);
                    this.downloadingText.setVisibility(0);
                    return;
                case 2:
                default:
                    this.processbarLayout.setVisibility(8);
                    this.waitingText.setVisibility(8);
                    this.buttonLayout.setVisibility(0);
                    this.downloadButton.setText((int) R.string.free_download);
                    this.downloadAction.setText((int) R.string.free_download);
                    return;
                case 3:
                    this.processbarLayout.setVisibility(8);
                    this.waitingText.setVisibility(8);
                    this.buttonLayout.setVisibility(0);
                    this.downloadButton.setText((int) R.string.menu_installing);
                    return;
                case 4:
                    Toast.makeText(this, "由于网络原因下载失败,请重试", 1).show();
                    this.processbarLayout.setVisibility(8);
                    this.waitingText.setVisibility(8);
                    this.downloadButton.setText(getString(R.string.continue_downloading));
                    this.buttonLayout.setVisibility(0);
                    return;
                case 5:
                    this.processbarLayout.setVisibility(8);
                    this.waitingText.setVisibility(8);
                    this.buttonLayout.setVisibility(0);
                    this.downloadButton.setText((int) R.string.menu_open);
                    this.downloadAction.setText((int) R.string.menu_open);
                    return;
                case 6:
                    this.buttonLayout.setVisibility(8);
                    this.downloadingText.setVisibility(8);
                    this.downloadingText.setVisibility(8);
                    this.processbar.setProgress(calcDownloadingProcess(downloadInfo));
                    this.processbarLayout.setVisibility(0);
                    this.cancelButton.setVisibility(0);
                    this.startButton.setBackgroundResource(R.drawable.sw_waiting);
                    this.startButton.setEnabled(false);
                    this.startButton.setVisibility(0);
                    this.waitingText.setVisibility(0);
                    return;
            }
        }
    }

    private void setTabColor(int i) {
        for (int length = this.tabsBg.length - 1; length >= 0; length--) {
            if (i == length) {
                this.tabsBg[length].setVisibility(0);
                this.tabsText[length].setTextColor(getResources().getColor(R.color.text_tab));
            } else {
                this.tabsBg[length].setVisibility(4);
                this.tabsText[length].setTextColor(getResources().getColor(17170443));
            }
        }
    }

    /* access modifiers changed from: private */
    public void updateDownloadStatus() {
        d.a();
        d.a();
        if (isInUpdateList()) {
            this.downloadButton.setText((int) R.string.isUpdate);
        } else if (isSoftwareInstalled()) {
            this.downloadButton.setText((int) R.string.menu_open);
        } else {
            this.downloadButton.setText((int) R.string.free_download);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0033  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void updateStatus(java.lang.String r9) {
        /*
            r8 = this;
            r6 = 0
            r5 = 2
            r4 = 1
            java.lang.String r0 = android.os.Environment.getExternalStorageState()
            java.lang.String r1 = "mounted"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0016
            r0 = 2131296507(0x7f0900fb, float:1.8210933E38)
            com.tencent.launcher.base.BaseApp.a(r0)
        L_0x0015:
            return
        L_0x0016:
            android.content.pm.PackageManager r0 = r8.getPackageManager()     // Catch:{ NameNotFoundException -> 0x003d }
            r1 = 1
            android.content.pm.PackageInfo r0 = r0.getPackageArchiveInfo(r9, r1)     // Catch:{ NameNotFoundException -> 0x003d }
            android.content.pm.PackageManager r1 = r8.getPackageManager()     // Catch:{ NameNotFoundException -> 0x00c7 }
            AndroidDLoader.Software r2 = r8.software     // Catch:{ NameNotFoundException -> 0x00c7 }
            java.lang.String r2 = r2.j()     // Catch:{ NameNotFoundException -> 0x00c7 }
            r3 = 0
            android.content.pm.PackageInfo r1 = r1.getPackageInfo(r2, r3)     // Catch:{ NameNotFoundException -> 0x00c7 }
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x0031:
            if (r0 != 0) goto L_0x0042
            android.widget.Button r0 = r8.downloadAction
            java.lang.String r1 = "安装"
            r0.setText(r1)
            r8.downloadStatus = r5
            goto L_0x0015
        L_0x003d:
            r0 = move-exception
            r0 = r6
        L_0x003f:
            r1 = r0
            r0 = r6
            goto L_0x0031
        L_0x0042:
            if (r1 == 0) goto L_0x0015
            if (r0 == 0) goto L_0x0015
            int r1 = r1.versionCode
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            int r1 = r1.intValue()
            r8.nDownloadedVersion = r1
            AndroidDLoader.Software r1 = r8.software
            java.lang.String r1 = r1.e()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            int r1 = r1.intValue()
            r8.nLatestVersion = r1
            int r0 = r0.versionCode
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            int r0 = r0.intValue()
            r8.nCurrentVerison = r0
            int r0 = r8.nLatestVersion
            int r1 = r8.nDownloadedVersion
            if (r0 != r1) goto L_0x0094
            int r0 = r8.nDownloadedVersion
            int r1 = r8.nCurrentVerison
            if (r0 == r1) goto L_0x00bc
            int r0 = r8.nDownloadedVersion
            int r1 = r8.nCurrentVerison
            if (r0 <= r1) goto L_0x008a
            android.widget.Button r0 = r8.downloadAction
            java.lang.String r1 = "安装"
            r0.setText(r1)
            r8.downloadStatus = r5
            goto L_0x0015
        L_0x008a:
            android.widget.Button r0 = r8.downloadAction
            java.lang.String r1 = "启动"
            r0.setText(r1)
            r8.bStartSoftware = r4
            goto L_0x0015
        L_0x0094:
            int r0 = r8.nLatestVersion
            int r1 = r8.nDownloadedVersion
            if (r0 <= r1) goto L_0x00bc
            int r0 = r8.nLatestVersion
            int r1 = r8.nCurrentVerison
            if (r0 == r1) goto L_0x00bc
            int r0 = r8.nLatestVersion
            int r1 = r8.nCurrentVerison
            if (r0 <= r1) goto L_0x00b1
            android.widget.Button r0 = r8.downloadAction
            java.lang.String r1 = "更新"
            r0.setText(r1)
            r8.bDownloadButtonStatusIsUpdate = r4
            goto L_0x0015
        L_0x00b1:
            android.widget.Button r0 = r8.downloadAction
            java.lang.String r1 = "启动"
            r0.setText(r1)
            r8.bStartSoftware = r4
            goto L_0x0015
        L_0x00bc:
            android.widget.Button r0 = r8.downloadAction
            java.lang.String r1 = "启动"
            r0.setText(r1)
            r8.bStartSoftware = r4
            goto L_0x0015
        L_0x00c7:
            r1 = move-exception
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.module.appcenter.SoftWareActivity.updateStatus(java.lang.String):void");
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
    }

    public void genPositionForTag(ArrayList arrayList) {
        int i;
        int a = p.a((Activity) this);
        this.infoTags.getLayoutParams();
        this.col = 4;
        this.row = 4;
        if (arrayList.size() <= 1) {
            this.row = 1;
        }
        if (arrayList.size() < 4) {
            i = 50;
            this.row = 2;
        } else if (arrayList.size() < 7) {
            i = 100;
            this.row = 3;
        } else {
            i = 150;
            this.row = 4;
        }
        int i2 = a / this.col;
        int i3 = i / this.row;
        Rect[] rectArr = new Rect[(this.col * this.row)];
        ArrayList initWeight = initWeight(rectArr.length);
        int i4 = 0;
        int i5 = 0;
        while (i5 < this.row) {
            int i6 = i4;
            int i7 = 0;
            while (i7 < this.col) {
                rectArr[i6] = new Rect(i2 * i7, i3 * i5, (i7 + 1) * i2, (i5 + 1) * i3);
                i7++;
                i6++;
            }
            i5++;
            i4 = i6;
        }
        int size = arrayList.size();
        int i8 = 0;
        ArrayList arrayList2 = initWeight;
        while (i8 < size) {
            Random random = new Random(System.currentTimeMillis());
            int nextInt = random.nextInt(arrayList2.size());
            Rect rect = rectArr[((Integer) arrayList2.get(nextInt)).intValue()];
            int nextInt2 = rect.right > a / 2 ? random.nextInt(i2) : random.nextInt(i2 / 2);
            int nextInt3 = random.nextInt(i3 / 2);
            int i9 = nextInt2 + rect.left;
            if (i9 < 5) {
                i9 += 5;
            }
            initTextViewTag((TextView) arrayList.get(i8), i9, rect.top + nextInt3 + 40, getResources().getColor(this.tagBackground[i8 % this.tagBackground.length]), this.tagSize[random.nextInt(this.tagSize.length)]);
            arrayList2.remove(nextInt);
            i8++;
            arrayList2 = arrayList2.size() == 0 ? initWeight(rectArr.length) : arrayList2;
        }
    }

    public void getSoftIcon() {
        Bitmap a = d.a().a(this.software.b(), this.iconHandler);
        if (a != null) {
            this.softwareIcon.setImageBitmap(a);
        } else {
            this.softwareIcon.setImageResource(R.drawable.sw_default_icon);
        }
    }

    public void getSoftSnapshot() {
        if (this.picOriURL == null) {
            this.picOriURL = this.picURL;
        }
        this.infoSnapshot1.setTag(this.picOriURL.get(0));
        if (this.picURL.size() == 1) {
            this.infoSnapshot2.setVisibility(8);
        } else {
            this.infoSnapshot2.setTag(this.picOriURL.get(1));
        }
        for (int i = 0; i < this.picURL.size(); i++) {
            Bitmap a = d.a().a((String) this.picURL.get(i), this.iconHandler);
            if (a != null) {
                if (i == 0) {
                    this.infoSnapshot1.setImageBitmap(a);
                } else if (i == 1) {
                    this.infoSnapshot2.setImageBitmap(a);
                }
            }
        }
    }

    public void onAdapterDataChange() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.layoutInflater = (LayoutInflater) getSystemService("layout_inflater");
        setContentView((int) R.layout.software_header);
        initTabControl();
        if (userTagged == null) {
            userTagged = new HashMap();
        }
        new Bundle();
        Bundle extras = bundle == null ? getIntent().getExtras() : bundle;
        this.mProductid = extras.getInt("Software_Open_Product_ID");
        this.mSoftid = extras.getInt("Software_Open_Soft_ID");
        this.mFileid = extras.getInt("Software_Open_File_ID");
        this.initTab = extras.getInt("Tab");
        this.nSoftWareDownloadStatus = extras.getInt("SoftWareDownloadStatus");
        this.mPreActivity = extras.getInt("stat");
        this.stat = a.a(this.mPreActivity);
        this.softwareLayout = findViewById(R.id.software_layout);
        this.downloadConnection = new at(this);
        bindService(new Intent(this, DownloadService.class), this.downloadConnection, 1);
        this.mCallback = new bc(this);
        initUI();
        this.softwareLayout.setVisibility(4);
        this.requestId = d.a().b(this.handler, this.mProductid, this.mSoftid, this.mFileid);
        setWaitScreen((View) null);
        getAllApp();
        g.a().a(a.g);
        this.handler.postDelayed(new bd(this), 1000);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        snapShotCache = null;
        d.a();
        d.a().b();
        unbindService(this.downloadConnection);
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onDownloadStateChange(DownloadInfo downloadInfo) {
        setStatus(downloadInfo);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public boolean onProgressDialogBack() {
        d.a().a(this.requestId);
        return false;
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(String str, int i, int i2) {
        if (this.software != null && this.software.k.equalsIgnoreCase(str)) {
            try {
                int i3 = (int) ((((long) i2) * 100) / ((long) i));
                this.downloadingText.setText(i3 + "%");
                this.downloadingText.setVisibility(0);
                this.buttonLayout.setVisibility(8);
                this.processbar.setProgress(i3);
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onReceiveAddComment(int i) {
        String string = getResources().getString(R.string.format_error);
        String string2 = getResources().getString(R.string.server_error);
        String string3 = getResources().getString(R.string.similar_content);
        switch (i) {
            case 0:
                onAdapterDataChange();
                Toast makeText = Toast.makeText(this, (int) R.string.app_edit_success_title, 1);
                makeText.setGravity(17, 0, 0);
                makeText.show();
                this.bHasComment = 1;
                this.pkgSoftDetail.a(this.bHasComment);
                bindMarkLayout();
                this.commentsData.add(0, this.userComment);
                this.markListAdapter.notifyDataSetChanged();
                String str = "hascommented onAdapterDataChange" + ((int) this.bHasComment);
                if (str == null) {
                    str = "............";
                }
                Log.v("Test", str);
                return;
            case 1:
                Toast makeText2 = Toast.makeText(this, string, 1);
                makeText2.setGravity(17, 0, 0);
                makeText2.show();
                return;
            case 2:
                Toast makeText3 = Toast.makeText(this, string2, 1);
                makeText3.setGravity(17, 0, 0);
                makeText3.show();
                return;
            case 3:
                onAdapterDataChange();
                Toast makeText4 = Toast.makeText(this, (int) R.string.app_edited_title, 1);
                makeText4.setGravity(17, 0, 0);
                makeText4.show();
                return;
            case 4:
                Toast makeText5 = Toast.makeText(this, string3, 1);
                makeText5.setGravity(17, 0, 0);
                makeText5.show();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onReceiveComment(List list) {
        if (this.commentsData == null) {
            this.commentsData = new ArrayList();
        }
        int size = list.size();
        if (this.markListAdapter == null) {
            this.markListAdapter = new da(this, this.commentsData);
            this.markList.setAdapter((ListAdapter) this.markListAdapter);
        }
        if (list == null || list.size() == 0) {
            this.markListMoreItem.b();
            if (this.commentsData.size() == 0) {
                this.nomark.setVisibility(0);
            }
        } else {
            this.markListMoreItem.c();
        }
        for (int i = 0; i < size; i++) {
            String str = ((Comment) list.get(i)).c;
            ((Comment) list.get(i)).c = str.substring(0, str.indexOf(" "));
            ((Comment) list.get(i)).b = ((Comment) list.get(i)).b.trim();
            if (((Comment) list.get(i)).b.equalsIgnoreCase("")) {
                ((Comment) list.get(i)).b = "游客";
            }
            this.commentsData.add(list.get(i));
        }
        this.markListAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public void onReceiveRelativeList(ArrayList arrayList) {
        if (arrayList == null || arrayList.size() == 0) {
            this.relativeListMoreItem.b();
            if (this.swRelativeAdapter.a() == null || this.swRelativeAdapter.a().size() == 0) {
                this.noRelative.setVisibility(0);
            }
        } else {
            this.relativeListMoreItem.c();
        }
        this.swRelativeAdapter.a(arrayList);
        this.swRelativeAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        boolean isSoftwareInstalled = isSoftwareInstalled();
        if (this.downloadAction.getText().equals("安装")) {
            if (isSoftwareInstalled) {
                this.downloadAction.setText("启动");
                this.bStartSoftware = true;
                this.downloadStatus = 0;
            } else {
                this.downloadAction.setText("安装");
            }
        }
        if (isSoftwareInstalled) {
            canWriteTag();
        }
    }

    public boolean onRetry() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putInt("Software_Open_Product_ID", this.mProductid);
        bundle.putInt("Software_Open_Soft_ID", this.mSoftid);
        bundle.putInt("Software_Open_File_ID", this.mFileid);
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    public boolean onTimeout() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void refreshLabelButton() {
    }

    public void selectTabIndex(int i) {
        setTabColor(i);
        this.tabContent.setDisplayedChild(i);
    }
}
