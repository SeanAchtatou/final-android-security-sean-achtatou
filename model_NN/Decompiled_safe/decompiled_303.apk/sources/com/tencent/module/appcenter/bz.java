package com.tencent.module.appcenter;

import android.view.View;
import com.tencent.qqlauncher.R;

final class bz implements View.OnClickListener {
    private /* synthetic */ AppCenterActivity a;

    bz(AppCenterActivity appCenterActivity) {
        this.a = appCenterActivity;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.tab_daily_pick /*2131493002*/:
                this.a.setSelectedRecommendTab(0);
                return;
            case R.id.tab_player_recommend /*2131493005*/:
                this.a.setSelectedRecommendTab(1);
                return;
            case R.id.tab_guest_you_like /*2131493008*/:
                this.a.setSelectedRecommendTab(2);
                return;
            default:
                return;
        }
    }
}
