package com.tencent.module.appcenter;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

final class r implements ServiceConnection {
    private /* synthetic */ TActivity a;

    r(TActivity tActivity) {
        this.a = tActivity;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        this.a.mCtrl = null;
        this.a.dataManager = null;
        String str = "onServiceDisconnected" + this.a;
        if (str == null) {
            str = "............";
        }
        Log.v("Service", str);
    }
}
