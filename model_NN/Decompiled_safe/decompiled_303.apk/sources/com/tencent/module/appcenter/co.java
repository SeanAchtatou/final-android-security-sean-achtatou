package com.tencent.module.appcenter;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;

final class co implements View.OnClickListener {
    private /* synthetic */ SoftWareActivity a;

    co(SoftWareActivity softWareActivity) {
        this.a = softWareActivity;
    }

    public final void onClick(View view) {
        ImageView imageView = (ImageView) view;
        SoftWareActivity.snapShotCache = imageView.getDrawable();
        Intent intent = new Intent(this.a, ShowSnapshotActivity.class);
        intent.putExtra("picUrl", (String) imageView.getTag());
        this.a.startActivity(intent);
    }
}
