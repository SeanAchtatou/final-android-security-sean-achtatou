package com.tencent.module.appcenter;

import android.view.View;

final class cq implements View.OnClickListener {
    private /* synthetic */ SoftWareActivity a;

    cq(SoftWareActivity softWareActivity) {
        this.a = softWareActivity;
    }

    public final void onClick(View view) {
        if (this.a.infoEditTags.isEnabled()) {
            this.a.infoEditTags.setFocusable(true);
            this.a.infoEditTags.setFocusableInTouchMode(true);
            this.a.infoEditTags.requestFocus();
        }
    }
}
