package com.tencent.module.appcenter;

import AndroidDLoader.ScrollablePicAdv;
import AndroidDLoader.a;
import android.view.View;

final class aj implements View.OnClickListener {
    private /* synthetic */ a a;

    aj(a aVar) {
        this.a = aVar;
    }

    public final void onClick(View view) {
        int intValue = ((Integer) view.getTag()).intValue();
        if (intValue >= 0 && intValue < this.a.b.size()) {
            ScrollablePicAdv scrollablePicAdv = (ScrollablePicAdv) this.a.b.get(intValue);
            SoftWareActivity.openSoftwareActivity(this.a.a, scrollablePicAdv.b, scrollablePicAdv.c, scrollablePicAdv.d, a.b);
            g.a().d();
        }
    }
}
