package com.tencent.module.appcenter;

import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import com.tencent.launcher.Launcher;

final class ay extends Handler {
    private /* synthetic */ SoftWareActivity a;

    ay(SoftWareActivity softWareActivity) {
        this.a = softWareActivity;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                this.a.downloadAction.setText("下载异常");
                int unused = this.a.downloadStatus = 3;
                return;
            case 1:
                if (this.a.software.h() != 0) {
                    this.a.downloadAction.setText(((message.arg1 * 100) / (this.a.software.h() * Launcher.APPWIDGET_HOST_ID)) + "%");
                } else {
                    this.a.downloadAction.setText("下载中");
                }
                int unused2 = this.a.downloadStatus = 1;
                return;
            case 2:
                String[] strArr = (String[]) message.obj;
                this.a.downloadAction.setText("安装");
                int unused3 = this.a.downloadStatus = 2;
                try {
                    this.a.mService.a(strArr[0], "SoftwareActivity");
                    return;
                } catch (RemoteException e) {
                    e.printStackTrace();
                    return;
                }
            case 3:
                this.a.downloadAction.setText("下载");
                int unused4 = this.a.downloadStatus = 0;
                return;
            default:
                return;
        }
    }
}
