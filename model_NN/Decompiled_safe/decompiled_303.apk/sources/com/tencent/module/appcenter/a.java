package com.tencent.module.appcenter;

import AndroidDLoader.ScrollablePicAdv;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.qqlauncher.R;
import java.util.ArrayList;
import java.util.Iterator;

public final class a implements ac {
    final /* synthetic */ AppCenterActivity a;
    /* access modifiers changed from: private */
    public ArrayList b = new ArrayList();
    private WorkSpaceView c = null;
    private ArrayList d = new ArrayList();
    /* access modifiers changed from: private */
    public ArrayList e = new ArrayList();
    private LinearLayout f = null;
    private ArrayList g = new ArrayList();
    private int h = -1;
    private View.OnClickListener i = new aj(this);
    private Handler j = new ai(this);

    public a(AppCenterActivity appCenterActivity, WorkSpaceView workSpaceView, LinearLayout linearLayout) {
        this.a = appCenterActivity;
        this.c = workSpaceView;
        this.f = linearLayout;
        this.f.setVisibility(8);
        workSpaceView.a(this);
    }

    public final void a(int i2) {
        if (this.g.size() != 0 && i2 >= 0 && i2 <= this.g.size() && this.h != i2) {
            Iterator it = this.g.iterator();
            while (it.hasNext()) {
                ((ImageView) it.next()).setImageResource(R.drawable.scroll_ad_dot_white);
            }
            ((ImageView) this.g.get(i2)).setImageResource(R.drawable.scroll_ad_dot_black);
            this.h = i2;
        }
    }

    public final void a(ArrayList arrayList) {
        long currentTimeMillis = System.currentTimeMillis();
        if (arrayList != null && arrayList.size() > 0) {
            this.b = arrayList;
            this.h = -1;
            this.c.removeAllViews();
            this.f.removeAllViews();
            this.d.clear();
            this.g.clear();
            this.e.clear();
            int size = this.b.size();
            for (int i2 = 0; i2 < size; i2++) {
                ScrollablePicAdv scrollablePicAdv = (ScrollablePicAdv) this.b.get(i2);
                View inflate = LayoutInflater.from(this.a.getApplicationContext()).inflate((int) R.layout.scroll_ad_item, (ViewGroup) null);
                ImageView imageView = (ImageView) inflate.findViewById(R.id.scroll_ad_icon_bg);
                imageView.setImageBitmap(d.a().a(scrollablePicAdv.a, this.j));
                this.e.add(imageView);
                ((TextView) inflate.findViewById(R.id.scroll_ad_title)).setText(scrollablePicAdv.e);
                TextView textView = (TextView) inflate.findViewById(R.id.scroll_ad_desc);
                textView.setBackgroundDrawable(this.a.getResources().getDrawable(R.drawable.scroll_ad_desc_background));
                textView.getBackground().setAlpha(100);
                textView.setTextColor(-1);
                textView.setText(scrollablePicAdv.f);
                inflate.setTag(Integer.valueOf(i2));
                inflate.setOnClickListener(this.i);
                this.c.addView(inflate);
                this.d.add(inflate);
                ImageView imageView2 = new ImageView(this.a.getApplicationContext());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                layoutParams.leftMargin = 5;
                layoutParams.rightMargin = 5;
                layoutParams.topMargin = 2;
                layoutParams.bottomMargin = 2;
                imageView2.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView2.setLayoutParams(layoutParams);
                if (i2 == 0) {
                    imageView2.setImageResource(R.drawable.scroll_ad_dot_black);
                } else {
                    imageView2.setImageResource(R.drawable.scroll_ad_dot_white);
                }
                this.f.addView(imageView2);
                this.g.add(imageView2);
            }
            if (this.b.size() < 2) {
                this.f.setVisibility(8);
            } else {
                this.f.setVisibility(0);
            }
        }
        Log.v("AppCenterActivity", "setScrollAdData Time----------------" + (System.currentTimeMillis() - currentTimeMillis));
        this.c.requestLayout();
    }
}
