package com.tencent.module.appcenter;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.tencent.qqlauncher.R;

public final class ae {
    public int a = 0;
    public int b = 1;
    protected ListView c = null;
    protected View d = null;
    protected TextView e = null;
    protected ProgressBar f = null;
    protected TextView g = null;
    /* access modifiers changed from: private */
    public be h;
    private Handler i = new cg(this);
    private AbsListView.OnScrollListener j = new cf(this);
    /* access modifiers changed from: private */
    public View.OnClickListener k = new ce(this);

    public ae(ListView listView, View view, be beVar) {
        this.h = beVar;
        this.d = view;
        this.c = listView;
        this.c.addFooterView(this.d);
        this.c.setOnScrollListener(this.j);
        this.d = view;
        this.e = (TextView) this.d.findViewById(R.id.TextView01);
        this.f = (ProgressBar) this.d.findViewById(R.id.ProgressBar01);
        this.g = (TextView) this.d.findViewById(R.id.WaitingBtn);
        this.e.setVisibility(0);
        this.f.setVisibility(0);
        this.g.setVisibility(8);
    }

    public final void a() {
        this.i.removeMessages(0);
        Message obtain = Message.obtain();
        obtain.what = 0;
        this.i.sendMessage(obtain);
    }

    public final void b() {
        this.a = 3;
        if (this.c.getAdapter() != null) {
            this.c.removeFooterView(this.d);
        }
    }

    public final void c() {
        this.a = 0;
        this.b++;
        this.e.setVisibility(0);
        this.f.setVisibility(0);
        this.g.setVisibility(8);
    }
}
