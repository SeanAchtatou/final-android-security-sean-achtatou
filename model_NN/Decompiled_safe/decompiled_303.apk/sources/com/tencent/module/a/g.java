package com.tencent.module.a;

public final class g extends l {
    private int a;
    private int b;

    public g() {
    }

    public g(int i, int i2) {
        super(i, i2);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.graphics.Matrix a(int r11, com.tencent.module.a.a r12) {
        /*
            r10 = this;
            r9 = 0
            r8 = 1065353216(0x3f800000, float:1.0)
            r7 = 1
            android.graphics.Matrix r0 = r10.f
            int r1 = r10.g
            int r2 = r11 * r1
            int r3 = r12.getChildCount()
            int r4 = r10.e
            if (r4 <= r2) goto L_0x0046
            int r4 = r2 + r1
            r10.a = r4
            int r4 = r10.e
            int r4 = r1 - r4
            int r2 = r2 + r4
            float r2 = (float) r2
            float r4 = (float) r1
            float r2 = r2 / r4
        L_0x001e:
            int r4 = r10.a
            boolean r5 = r12.a()
            if (r5 == 0) goto L_0x007b
            int r5 = r3 - r7
            if (r11 < r5) goto L_0x0051
            int r5 = r10.e
            if (r5 >= 0) goto L_0x0051
            int r2 = r10.e
            int r2 = -r2
            float r2 = (float) r2
            float r4 = (float) r1
            float r2 = r2 / r4
            int r1 = r1 * r3
            r3 = r2
            r2 = r1
            r1 = r9
        L_0x0038:
            int r4 = (r3 > r8 ? 1 : (r3 == r8 ? 0 : -1))
            if (r4 > 0) goto L_0x0042
            r4 = -1082130432(0xffffffffbf800000, float:-1.0)
            int r4 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r4 >= 0) goto L_0x0067
        L_0x0042:
            r0.reset()
        L_0x0045:
            return r0
        L_0x0046:
            r10.a = r2
            int r4 = r10.e
            int r4 = r4 + r1
            int r2 = r4 - r2
            float r2 = (float) r2
            float r4 = (float) r1
            float r2 = r2 / r4
            goto L_0x001e
        L_0x0051:
            if (r11 > 0) goto L_0x007b
            int r5 = r10.e
            int r6 = r3 - r7
            int r6 = r6 * r1
            if (r5 <= r6) goto L_0x007b
            int r2 = r10.e
            int r4 = r3 - r7
            int r4 = r4 * r1
            int r2 = r2 - r4
            float r2 = (float) r2
            float r4 = (float) r1
            float r2 = r2 / r4
            int r1 = r1 * r3
            r3 = r2
            r2 = r9
            goto L_0x0038
        L_0x0067:
            int r2 = -r2
            float r2 = (float) r2
            int r4 = r10.b
            int r4 = -r4
            float r4 = (float) r4
            r0.setTranslate(r2, r4)
            r0.postScale(r3, r8)
            float r1 = (float) r1
            int r2 = r10.b
            float r2 = (float) r2
            r0.postTranslate(r1, r2)
            goto L_0x0045
        L_0x007b:
            r1 = r4
            r3 = r2
            r2 = r4
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.module.a.g.a(int, com.tencent.module.a.a):android.graphics.Matrix");
    }
}
