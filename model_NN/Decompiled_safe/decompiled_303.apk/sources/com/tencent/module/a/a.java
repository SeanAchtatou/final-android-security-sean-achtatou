package com.tencent.module.a;

import android.graphics.Canvas;
import android.view.View;

public interface a {
    boolean a();

    boolean drawChild(Canvas canvas, View view, long j);

    View getChildAt(int i);

    int getChildCount();
}
