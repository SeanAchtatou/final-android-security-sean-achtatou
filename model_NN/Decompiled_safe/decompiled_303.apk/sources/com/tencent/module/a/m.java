package com.tencent.module.a;

import android.graphics.Camera;

public final class m extends n {
    private Camera a;
    private float b;
    private int c;

    public m() {
        this(0, 0);
    }

    public m(int i, int i2) {
        super(i, i2);
        this.b = 120.0f / ((float) i);
        this.a = new Camera();
        this.c = i2 / 2;
    }

    public final void a(int i) {
        super.a(i);
        this.b = 120.0f / ((float) i);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:? A[ADDED_TO_REGION, ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0032  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.graphics.Canvas r10, int r11, android.view.View r12, long r13) {
        /*
            r9 = this;
            r8 = 1
            int r0 = r9.g
            android.graphics.Camera r1 = r9.a
            android.graphics.Matrix r2 = r9.f
            int r3 = r11 * r0
            com.tencent.module.a.a r7 = com.tencent.module.a.e.a()
            int r4 = r7.getChildCount()
            int r5 = r9.e
            int r3 = r5 - r3
            float r3 = (float) r3
            float r6 = r9.b
            float r3 = r3 * r6
            int r3 = (int) r3
            boolean r6 = r7.a()
            if (r6 == 0) goto L_0x0095
            int r6 = r4 - r8
            if (r11 < r6) goto L_0x0037
            if (r5 >= 0) goto L_0x0037
            int r3 = r0 + r5
            float r3 = (float) r3
            float r4 = r9.b
            float r3 = r3 * r4
            int r3 = (int) r3
            r6 = r3
        L_0x002e:
            r3 = 120(0x78, float:1.68E-43)
            if (r6 > r3) goto L_0x0036
            r3 = -120(0xffffffffffffff88, float:NaN)
            if (r6 >= r3) goto L_0x0049
        L_0x0036:
            return
        L_0x0037:
            if (r11 > 0) goto L_0x0095
            int r6 = r4 - r8
            int r6 = r6 * r0
            if (r5 <= r6) goto L_0x0095
            int r3 = r4 * r0
            int r3 = r5 - r3
            float r3 = (float) r3
            float r4 = r9.b
            float r3 = r3 * r4
            int r3 = (int) r3
            r6 = r3
            goto L_0x002e
        L_0x0049:
            r1.save()
            float r3 = (float) r6
            r1.rotateY(r3)
            r1.getMatrix(r2)
            r1.restore()
            r1 = 0
            int r3 = r9.c
            int r3 = -r3
            float r3 = (float) r3
            r2.preTranslate(r1, r3)
            float r1 = (float) r5
            int r3 = r9.c
            float r3 = (float) r3
            r2.postTranslate(r1, r3)
            r10.save()
            r10.concat(r2)
            int r1 = r12.getLeft()
            float r1 = (float) r1
            int r2 = r12.getTop()
            float r2 = (float) r2
            int r3 = r12.getRight()
            float r3 = (float) r3
            int r4 = r12.getBottom()
            float r4 = (float) r4
            int r5 = r6 * 255
            int r0 = r5 / r0
            int r5 = r0 + 255
            r6 = 20
            r0 = r10
            r0.saveLayerAlpha(r1, r2, r3, r4, r5, r6)
            r7.drawChild(r10, r12, r13)
            r10.restore()
            r10.restore()
            goto L_0x0036
        L_0x0095:
            r6 = r3
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.module.a.m.a(android.graphics.Canvas, int, android.view.View, long):void");
    }

    public final void b(int i) {
        super.b(i);
        this.c = i / 2;
    }
}
