package com.tencent.module.a;

import android.graphics.Canvas;
import android.view.View;

public class n extends l {
    public n() {
    }

    public n(int i, int i2) {
        super(i, i2);
    }

    public void a(Canvas canvas, int i, View view, long j) {
    }

    public final void a(Canvas canvas, long j) {
        int i = this.g;
        a a = e.a();
        int childCount = a.getChildCount();
        int i2 = this.e;
        int i3 = i2 > 0 ? i2 / i : childCount - 1;
        int i4 = i2 <= 0 ? 0 : i2 >= i * (childCount - 1) ? 0 : i3 + 1;
        canvas.save();
        a(canvas, i3, a.getChildAt(i3), j);
        canvas.restore();
        canvas.save();
        a(canvas, i4, a.getChildAt(i4), j);
        canvas.restore();
    }
}
