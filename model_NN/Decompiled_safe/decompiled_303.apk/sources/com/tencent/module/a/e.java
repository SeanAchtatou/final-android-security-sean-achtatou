package com.tencent.module.a;

import com.tencent.module.setting.DesktopSwitchSpecialEffectSettingActivity;

public final class e {
    private static e a;
    private static a b;
    private static final int[] f = {10};
    private static final int[] g = {3, 5, 7};
    private int c;
    private int d;
    private int e = 0;

    private e(a aVar) {
        b = aVar;
    }

    public static a a() {
        return b;
    }

    public static e a(a aVar) {
        if (a == null) {
            a = new e(aVar);
        }
        return a;
    }

    public static void b(a aVar) {
        b = aVar;
    }

    public final h a(int i) {
        this.e = i;
        switch (i) {
            case 2:
                return new f(this.c, this.d);
            case 3:
                return new j(this.c, this.d);
            case 4:
                return new d(this.c, this.d);
            case 5:
                return new c(this.c, this.d);
            case 6:
                return new g(this.c, this.d);
            case DesktopSwitchSpecialEffectSettingActivity.nRotationSetting /*7*/:
                return new i(this.c, this.d);
            case DesktopSwitchSpecialEffectSettingActivity.nCubeSetting /*8*/:
                return new m(this.c, this.d);
            case 9:
                return new b(this.c, this.d);
            default:
                return new k(this.c, this.d);
        }
    }

    public final void a(int i, int i2) {
        this.c = i;
        this.d = i2;
    }

    public final boolean b() {
        int i = this.e;
        int[] iArr = g;
        for (int i2 : iArr) {
            if (i2 == i) {
                return true;
            }
        }
        return false;
    }

    public final boolean c() {
        int i = this.e;
        int[] iArr = f;
        for (int i2 : iArr) {
            if (i2 == i) {
                return false;
            }
        }
        return true;
    }
}
