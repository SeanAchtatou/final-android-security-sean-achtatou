package com.tencent.module.a;

import android.graphics.Camera;
import android.graphics.Matrix;

public final class c extends b {
    public c() {
    }

    public c(int i, int i2) {
        super(i, i2);
        this.d = 180.0f / ((float) i);
    }

    /* access modifiers changed from: protected */
    public final Matrix a(int i, a aVar) {
        int i2 = this.g;
        Camera camera = this.a;
        Matrix matrix = this.f;
        int i3 = i * i2;
        int i4 = (int) (((float) (i3 - this.e)) * this.d);
        this.b = i3 + (i2 / 2);
        if (i4 > 90 || i4 < -90) {
            matrix.setScale(0.0f, 0.0f);
            return matrix;
        }
        camera.save();
        camera.rotateY((float) i4);
        camera.getMatrix(matrix);
        camera.restore();
        matrix.preTranslate((float) (-this.b), (float) (-this.c));
        matrix.postTranslate((float) ((i2 / 2) + this.e), (float) this.c);
        return matrix;
    }

    public final void a(int i) {
        super.a(i);
        this.d = 180.0f / ((float) i);
    }
}
