package com.tencent.module.a;

import android.graphics.Canvas;

public final class k extends l {
    public k() {
    }

    public k(int i, int i2) {
        super(i, i2);
    }

    public final void a(Canvas canvas, long j) {
        int i = this.g;
        a a = e.a();
        int childCount = a.getChildCount();
        int i2 = this.e > 0 ? this.e / i : childCount - 1;
        int i3 = this.e <= 0 ? 0 : (this.e >= i * (childCount - 1) || i2 >= childCount - 1) ? 0 : i2 + 1;
        canvas.save();
        a.drawChild(canvas, a.getChildAt(i2), j);
        canvas.restore();
        canvas.save();
        a.drawChild(canvas, a.getChildAt(i3), j);
        canvas.restore();
    }
}
