package com.tencent.module.a;

public final class i extends b {
    public i() {
    }

    public i(int i, int i2) {
        super(i, i2);
        this.d = 90.0f / ((float) i);
        this.c = (i2 * 4) / 3;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x003f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.graphics.Matrix a(int r14, com.tencent.module.a.a r15) {
        /*
            r13 = this;
            r11 = 90
            r10 = 1
            int r0 = r13.g
            android.graphics.Camera r1 = r13.a
            android.graphics.Matrix r2 = r13.f
            int r3 = r14 * r0
            int r4 = r15.getChildCount()
            int r5 = r13.e
            float r6 = r13.d
            int r7 = r5 - r3
            float r7 = (float) r7
            float r7 = r7 * r6
            int r7 = (int) r7
            int r8 = r0 / 2
            int r3 = r3 + r8
            r13.b = r3
            int r3 = r13.b
            int r8 = r0 / 2
            int r8 = r8 + r5
            boolean r9 = r15.a()
            if (r9 == 0) goto L_0x007f
            int r9 = r4 - r10
            if (r14 < r9) goto L_0x0048
            if (r5 >= 0) goto L_0x0048
            float r3 = (float) r5
            float r3 = r3 * r6
            int r3 = (int) r3
            int r3 = r3 + 90
            int r4 = r4 * r0
            int r0 = r0 / 2
            int r0 = r4 - r0
            r4 = 0
            r12 = r3
            r3 = r4
            r4 = r0
            r0 = r12
        L_0x003d:
            if (r0 > r11) goto L_0x0043
            r5 = -90
            if (r0 >= r5) goto L_0x0060
        L_0x0043:
            r2.reset()
            r0 = r2
        L_0x0047:
            return r0
        L_0x0048:
            if (r14 > 0) goto L_0x007f
            int r9 = r4 - r10
            int r9 = r9 * r0
            if (r5 <= r9) goto L_0x007f
            int r3 = r4 - r10
            int r3 = r3 * r0
            int r3 = r5 - r3
            float r3 = (float) r3
            float r3 = r3 * r6
            int r3 = (int) r3
            int r3 = r3 - r11
            int r5 = r0 / 2
            int r0 = r0 * r4
            r4 = r5
            r12 = r0
            r0 = r3
            r3 = r12
            goto L_0x003d
        L_0x0060:
            r1.save()
            float r0 = (float) r0
            r1.rotateZ(r0)
            r1.getMatrix(r2)
            r1.restore()
            int r0 = -r4
            float r0 = (float) r0
            int r1 = r13.c
            int r1 = -r1
            float r1 = (float) r1
            r2.preTranslate(r0, r1)
            float r0 = (float) r3
            int r1 = r13.c
            float r1 = (float) r1
            r2.postTranslate(r0, r1)
            r0 = r2
            goto L_0x0047
        L_0x007f:
            r0 = r7
            r4 = r3
            r3 = r8
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.module.a.i.a(int, com.tencent.module.a.a):android.graphics.Matrix");
    }

    public final void a(int i) {
        super.a(i);
        this.d = 90.0f / ((float) i);
    }

    public final void b(int i) {
        super.b(i);
        this.c = (i * 4) / 3;
    }
}
