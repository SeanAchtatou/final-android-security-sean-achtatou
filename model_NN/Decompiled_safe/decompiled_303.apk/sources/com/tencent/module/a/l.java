package com.tencent.module.a;

import android.graphics.Canvas;
import android.graphics.DrawFilter;
import android.graphics.Matrix;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Rect;

public class l implements h {
    private static final DrawFilter h = new PaintFlagsDrawFilter(0, 3);
    private int a;
    private int b;
    private Rect c = new Rect();
    private Rect d = new Rect();
    protected int e;
    protected Matrix f = new Matrix();
    protected int g;

    public l() {
    }

    public l(int i, int i2) {
        this.g = i;
        this.b = i2;
    }

    /* access modifiers changed from: protected */
    public Matrix a(int i, a aVar) {
        this.f.reset();
        return this.f;
    }

    public void a(int i) {
        this.g = i;
    }

    public final void a(int i, int i2) {
        this.e = i;
        this.a = i2;
    }

    public void a(Canvas canvas, long j) {
        int i = this.g;
        a a2 = e.a();
        int childCount = a2.getChildCount();
        int i2 = this.e > 0 ? this.e / i : childCount - 1;
        int i3 = this.e <= 0 ? 0 : this.e >= i * (childCount - 1) ? 0 : i2 + 1;
        canvas.setDrawFilter(h);
        canvas.save();
        canvas.concat(a(i2, a2));
        a2.drawChild(canvas, a2.getChildAt(i2), j);
        canvas.restore();
        canvas.save();
        canvas.concat(a(i3, a2));
        a2.drawChild(canvas, a2.getChildAt(i3), j);
        canvas.restore();
    }

    public void b(int i) {
        this.b = i;
    }

    public final void b(Canvas canvas, long j) {
        a(canvas, j);
    }
}
