package com.tencent.module.a;

public final class f extends n {
    public f() {
        this(0, 0);
    }

    public f(int i, int i2) {
        super(i, i2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0071 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0079 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.graphics.Canvas r11, int r12, android.view.View r13, long r14) {
        /*
            r10 = this;
            r9 = 1
            int r0 = r10.g
            android.graphics.Matrix r1 = r10.f
            int r2 = r12 * r0
            com.tencent.module.a.a r7 = com.tencent.module.a.e.a()
            int r3 = r7.getChildCount()
            r4 = 100
            int r5 = r10.e
            int r6 = r2 - r5
            float r2 = (float) r2
            r8 = 0
            r1.setTranslate(r2, r8)
            boolean r1 = r7.a()
            if (r1 == 0) goto L_0x0089
            int r1 = r3 - r9
            if (r12 < r1) goto L_0x0063
            if (r5 >= 0) goto L_0x0063
            int r1 = -r0
            int r1 = r1 - r5
        L_0x0028:
            int r2 = r0 / 2
            int r0 = r0 / 5
            int r3 = -r2
            if (r1 <= r3) goto L_0x006e
            int r3 = -r0
            if (r1 > r3) goto L_0x006e
            int r1 = r1 + r2
            int r1 = r1 * 155
            int r0 = r2 - r0
            int r0 = r1 / r0
            int r0 = r0 + 100
            r5 = r0
        L_0x003c:
            r11.save()
            int r0 = r13.getLeft()
            float r1 = (float) r0
            int r0 = r13.getTop()
            float r2 = (float) r0
            int r0 = r13.getRight()
            float r3 = (float) r0
            int r0 = r13.getBottom()
            float r4 = (float) r0
            r6 = 20
            r0 = r11
            r0.saveLayerAlpha(r1, r2, r3, r4, r5, r6)
            r7.drawChild(r11, r13, r14)
            r11.restore()
            r11.restore()
            return
        L_0x0063:
            if (r12 > 0) goto L_0x0089
            int r1 = r3 - r9
            int r1 = r1 * r0
            if (r5 <= r1) goto L_0x0089
            int r1 = r3 * r0
            int r1 = r1 - r5
            goto L_0x0028
        L_0x006e:
            int r3 = -r0
            if (r1 <= r3) goto L_0x0077
            if (r1 > r0) goto L_0x0077
            r0 = 255(0xff, float:3.57E-43)
            r5 = r0
            goto L_0x003c
        L_0x0077:
            if (r1 <= r0) goto L_0x0087
            if (r1 >= r2) goto L_0x0087
            int r1 = r1 - r2
            int r1 = -r1
            int r1 = r1 * 155
            int r0 = r2 - r0
            int r0 = r1 / r0
            int r0 = r0 + 100
            r5 = r0
            goto L_0x003c
        L_0x0087:
            r5 = r4
            goto L_0x003c
        L_0x0089:
            r1 = r6
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.module.a.f.a(android.graphics.Canvas, int, android.view.View, long):void");
    }
}
