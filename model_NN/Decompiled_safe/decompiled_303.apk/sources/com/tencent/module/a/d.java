package com.tencent.module.a;

import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.view.View;
import com.tencent.launcher.BrightnessActivity;

public final class d extends l {
    private Camera a;
    private int b;
    private Matrix c;

    public d() {
        this(0, 0);
    }

    public d(int i, int i2) {
        super(i, i2);
        this.c = new Matrix();
        this.a = new Camera();
        this.b = i2 / 2;
    }

    /* access modifiers changed from: protected */
    public final Matrix a(int i, a aVar) {
        Matrix matrix = this.c;
        int childCount = aVar.getChildCount();
        int i2 = this.g;
        matrix.reset();
        if (aVar.a() && aVar.a() && ((i < childCount - 1 || this.e >= 0) && i <= 0 && this.e > (childCount - 1) * i2)) {
            matrix.postTranslate((float) (childCount * i2), 0.0f);
        }
        return matrix;
    }

    public final void a(int i) {
        super.a(i);
    }

    public final void a(Canvas canvas, long j) {
        int i = this.g;
        a a2 = e.a();
        int i2 = this.e;
        int childCount = a2.getChildCount();
        int i3 = i2 > 0 ? i2 / i : childCount - 1;
        int i4 = i2 <= 0 ? 0 : i2 >= i * (childCount - 1) ? 0 : i3 + 1;
        canvas.save();
        View childAt = a2.getChildAt(i3);
        int i5 = this.g;
        Camera camera = this.a;
        Matrix matrix = this.f;
        int i6 = i3 * i5;
        a a3 = e.a();
        int childCount2 = a3.getChildCount();
        int i7 = this.e;
        int i8 = i5 / 2;
        int i9 = i7 + i8;
        int i10 = (!a3.a() || i3 < childCount2 - 1 || i7 >= 0) ? i6 - i7 : (-i5) - i7;
        int i11 = this.b;
        camera.save();
        camera.translate(0.0f, 0.0f, (float) (-i10));
        camera.getMatrix(matrix);
        camera.restore();
        matrix.preTranslate((float) (-i8), (float) (-i11));
        matrix.postTranslate((float) i9, (float) i11);
        canvas.save();
        matrix.preTranslate((float) (-i6), 0.0f);
        canvas.concat(matrix);
        canvas.saveLayerAlpha((float) childAt.getLeft(), (float) childAt.getTop(), (float) childAt.getRight(), (float) childAt.getBottom(), ((i10 * BrightnessActivity.MAXIMUM_BACKLIGHT) / i5) + BrightnessActivity.MAXIMUM_BACKLIGHT, 20);
        a3.drawChild(canvas, childAt, j);
        canvas.restore();
        canvas.restore();
        canvas.restore();
        canvas.save();
        canvas.concat(a(i4, a2));
        a2.drawChild(canvas, a2.getChildAt(i4), j);
        canvas.restore();
    }

    public final void b(int i) {
        super.b(i);
        this.b = i / 2;
    }
}
