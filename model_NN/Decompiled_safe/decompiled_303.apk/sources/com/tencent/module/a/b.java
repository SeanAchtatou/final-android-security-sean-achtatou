package com.tencent.module.a;

import android.graphics.Camera;

public class b extends l {
    protected Camera a;
    protected int b;
    protected int c;
    protected float d;

    public b() {
        this(0, 0);
    }

    public b(int i, int i2) {
        super(i, i2);
        this.c = i2 / 2;
        this.d = 90.0f / ((float) i);
        this.a = new Camera();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0039  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Matrix a(int r14, com.tencent.module.a.a r15) {
        /*
            r13 = this;
            r12 = 0
            r11 = 1
            r10 = 1119092736(0x42b40000, float:90.0)
            int r0 = r13.g
            android.graphics.Camera r1 = r13.a
            android.graphics.Matrix r2 = r13.f
            int r3 = r14 * r0
            int r4 = r15.getChildCount()
            int r5 = r13.e
            float r6 = r13.d
            int r7 = r3 - r5
            float r7 = (float) r7
            float r7 = r7 * r6
            if (r5 <= r3) goto L_0x0044
            int r8 = r3 + r0
            r13.b = r8
        L_0x001e:
            int r8 = r13.b
            float r8 = (float) r8
            boolean r9 = r15.a()
            if (r9 == 0) goto L_0x0078
            int r9 = r4 - r11
            if (r14 < r9) goto L_0x0047
            if (r5 >= 0) goto L_0x0047
            int r4 = -r5
            float r4 = (float) r4
            float r4 = r4 * r6
            float r4 = r4 - r10
            int r0 = r0 + r3
            float r0 = (float) r0
            r3 = r0
            r0 = r12
        L_0x0035:
            int r5 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r5 > 0) goto L_0x003f
            r5 = -1028390912(0xffffffffc2b40000, float:-90.0)
            int r5 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r5 >= 0) goto L_0x005c
        L_0x003f:
            r2.reset()
            r0 = r2
        L_0x0043:
            return r0
        L_0x0044:
            r13.b = r3
            goto L_0x001e
        L_0x0047:
            if (r14 > 0) goto L_0x0078
            int r3 = r4 - r11
            int r3 = r3 * r0
            if (r5 <= r3) goto L_0x0078
            int r3 = r4 - r11
            int r3 = r3 * r0
            int r3 = r5 - r3
            float r3 = (float) r3
            float r3 = r3 * r6
            float r3 = r10 - r3
            int r0 = r0 * r4
            float r0 = (float) r0
            r4 = r3
            r3 = r12
            goto L_0x0035
        L_0x005c:
            r1.save()
            r1.rotateY(r4)
            r1.getMatrix(r2)
            r1.restore()
            float r1 = -r3
            int r3 = r13.c
            int r3 = -r3
            float r3 = (float) r3
            r2.preTranslate(r1, r3)
            int r1 = r13.c
            float r1 = (float) r1
            r2.postTranslate(r0, r1)
            r0 = r2
            goto L_0x0043
        L_0x0078:
            r0 = r8
            r3 = r8
            r4 = r7
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.module.a.b.a(int, com.tencent.module.a.a):android.graphics.Matrix");
    }

    public void a(int i) {
        super.a(i);
        this.d = 90.0f / ((float) i);
    }

    public void b(int i) {
        super.b(i);
        this.c = i / 2;
    }
}
