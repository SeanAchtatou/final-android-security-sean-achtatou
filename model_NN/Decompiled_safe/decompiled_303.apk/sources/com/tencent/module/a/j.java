package com.tencent.module.a;

import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.view.View;
import com.tencent.launcher.BrightnessActivity;

public final class j extends n {
    private Camera a;
    private float b;
    private int c;

    public j() {
        this(0, 0);
    }

    public j(int i, int i2) {
        super(i, i2);
        this.b = 90.0f / ((float) i);
        this.a = new Camera();
        this.c = i2 / 2;
    }

    public final void a(int i) {
        super.a(i);
        this.b = 90.0f / ((float) i);
    }

    public final void a(Canvas canvas, int i, View view, long j) {
        int i2;
        int i3 = this.g;
        Camera camera = this.a;
        Matrix matrix = this.f;
        a a2 = e.a();
        int childCount = a2.getChildCount();
        int i4 = (int) (((float) (this.e - (i * i3))) * this.b);
        int i5 = i3 / 2;
        int i6 = this.e + i5;
        if (a2.a()) {
            if (i >= childCount - 1 && this.e < 0) {
                i2 = (int) (((float) (this.e + i3)) * this.b);
                camera.save();
                camera.rotateY((float) i2);
                camera.getMatrix(matrix);
                camera.restore();
                matrix.preTranslate((float) (-i5), (float) (-this.c));
                matrix.postTranslate((float) i6, (float) this.c);
                canvas.save();
                canvas.concat(matrix);
                int i7 = ((i2 * BrightnessActivity.MAXIMUM_BACKLIGHT) / i3) + BrightnessActivity.MAXIMUM_BACKLIGHT;
                canvas.saveLayerAlpha((float) view.getLeft(), (float) view.getTop(), (float) view.getRight(), (float) view.getBottom(), i7, 20);
                a2.drawChild(canvas, view, j);
                canvas.restore();
                canvas.restore();
            } else if (i <= 0 && this.e > (childCount - 1) * i3) {
                i2 = (int) (((float) (this.e - (childCount * i3))) * this.b);
                camera.save();
                camera.rotateY((float) i2);
                camera.getMatrix(matrix);
                camera.restore();
                matrix.preTranslate((float) (-i5), (float) (-this.c));
                matrix.postTranslate((float) i6, (float) this.c);
                canvas.save();
                canvas.concat(matrix);
                int i72 = ((i2 * BrightnessActivity.MAXIMUM_BACKLIGHT) / i3) + BrightnessActivity.MAXIMUM_BACKLIGHT;
                canvas.saveLayerAlpha((float) view.getLeft(), (float) view.getTop(), (float) view.getRight(), (float) view.getBottom(), i72, 20);
                a2.drawChild(canvas, view, j);
                canvas.restore();
                canvas.restore();
            }
        }
        i2 = i4;
        camera.save();
        camera.rotateY((float) i2);
        camera.getMatrix(matrix);
        camera.restore();
        matrix.preTranslate((float) (-i5), (float) (-this.c));
        matrix.postTranslate((float) i6, (float) this.c);
        canvas.save();
        canvas.concat(matrix);
        int i722 = ((i2 * BrightnessActivity.MAXIMUM_BACKLIGHT) / i3) + BrightnessActivity.MAXIMUM_BACKLIGHT;
        canvas.saveLayerAlpha((float) view.getLeft(), (float) view.getTop(), (float) view.getRight(), (float) view.getBottom(), i722, 20);
        a2.drawChild(canvas, view, j);
        canvas.restore();
        canvas.restore();
    }

    public final void b(int i) {
        this.c = i / 2;
    }
}
