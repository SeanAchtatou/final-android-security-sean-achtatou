package com.tencent.module.screenlock;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class a extends BroadcastReceiver {
    private /* synthetic */ LockService a;

    a(LockService lockService) {
        this.a = lockService;
    }

    public final void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.SCREEN_OFF") && this.a.d.getCallState() == 0) {
            this.a.c.reenableKeyguard();
            this.a.c.disableKeyguard();
            this.a.b.post(new c(this));
        }
    }
}
