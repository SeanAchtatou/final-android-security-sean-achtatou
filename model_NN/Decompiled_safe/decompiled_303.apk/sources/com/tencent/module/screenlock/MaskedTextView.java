package com.tencent.module.screenlock;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;
import com.tencent.launcher.BrightnessActivity;

public class MaskedTextView extends View {
    private Paint a = new Paint();
    private Shader b;
    private float c = 50.0f;
    private long d = System.currentTimeMillis();
    private boolean e = false;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
     arg types: [int, int, int, int, int[], float[], android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void} */
    public MaskedTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a.setColor(-7829368);
        this.a.setAntiAlias(true);
        this.a.setTextSize(26.0f);
        this.b = new LinearGradient(0.0f, 0.0f, 200.0f, 0.0f, new int[]{Color.argb((int) BrightnessActivity.MAXIMUM_BACKLIGHT, 120, 120, 120), Color.argb((int) BrightnessActivity.MAXIMUM_BACKLIGHT, 120, 120, 120), Color.argb((int) BrightnessActivity.MAXIMUM_BACKLIGHT, (int) BrightnessActivity.MAXIMUM_BACKLIGHT, (int) BrightnessActivity.MAXIMUM_BACKLIGHT, (int) BrightnessActivity.MAXIMUM_BACKLIGHT)}, new float[]{0.0f, 0.7f, 1.0f}, Shader.TileMode.MIRROR);
        this.a.setShader(this.b);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        long currentTimeMillis = System.currentTimeMillis();
        this.c = (((float) (currentTimeMillis - this.d)) / 4.5f) + this.c;
        Matrix matrix = new Matrix();
        if (this.e) {
            matrix.setTranslate(this.c, 0.0f);
            invalidate();
        } else {
            matrix.setTranslate(0.0f, 0.0f);
        }
        this.b.setLocalMatrix(matrix);
        canvas.drawText("slide to unlock", 0.0f, 25.0f, this.a);
        this.d = currentTimeMillis;
    }
}
