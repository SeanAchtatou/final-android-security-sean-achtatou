package com.tencent.module.screenlock;

import android.app.KeyguardManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.Log;

public class LockService extends Service {
    private Context a;
    /* access modifiers changed from: private */
    public Handler b = new Handler();
    /* access modifiers changed from: private */
    public KeyguardManager.KeyguardLock c;
    /* access modifiers changed from: private */
    public TelephonyManager d;
    private BroadcastReceiver e = new a(this);
    private BroadcastReceiver f = new b(this);

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        Log.e("", "---------------------hello world");
        startForeground(0, null);
        this.a = getBaseContext();
        this.c = ((KeyguardManager) getSystemService("keyguard")).newKeyguardLock(getPackageName());
        this.d = (TelephonyManager) getSystemService("phone");
        registerReceiver(this.e, new IntentFilter("android.intent.action.SCREEN_OFF"));
        registerReceiver(this.f, new IntentFilter("com.tencent.qqlauncher.lock_action"));
        super.onCreate();
    }

    public void onDestroy() {
        Log.e("", "---------------------bye world");
        this.c.reenableKeyguard();
        unregisterReceiver(this.e);
        unregisterReceiver(this.f);
        super.onDestroy();
    }
}
