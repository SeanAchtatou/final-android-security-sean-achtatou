package com.tencent.module.screenlock;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

public class SlidableButton extends Button {
    private boolean a = false;
    private boolean b = false;
    private View c;

    public SlidableButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() != 0) {
            return false;
        }
        this.c.setVisibility(4);
        this.a = true;
        return false;
    }
}
