package com.qq.taf.jce.dynamic;

import com.qq.taf.jce.JceDecodeException;
import com.qq.taf.jce.JceInputStream;
import com.tencent.module.setting.DesktopSwitchSpecialEffectSettingActivity;
import java.io.UnsupportedEncodingException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public final class DynamicInputStream {
    private ByteBuffer bs;
    private String sServerEncoding = "GBK";

    public DynamicInputStream(ByteBuffer bs2) {
        this.bs = bs2;
    }

    public DynamicInputStream(byte[] bs2) {
        this.bs = ByteBuffer.wrap(bs2);
    }

    public int setServerEncoding(String se) {
        this.sServerEncoding = se;
        return 0;
    }

    /* Debug info: failed to restart local var, previous not found, register: 15 */
    public JceField read() {
        try {
            JceInputStream.HeadData hd = new JceInputStream.HeadData();
            JceInputStream.readHead(hd, this.bs);
            switch (hd.type) {
                case 0:
                    return JceField.create(this.bs.get(), hd.tag);
                case 1:
                    return JceField.create(this.bs.getShort(), hd.tag);
                case 2:
                    return JceField.create(this.bs.getInt(), hd.tag);
                case 3:
                    return JceField.create(this.bs.getLong(), hd.tag);
                case 4:
                    return JceField.create(this.bs.getFloat(), hd.tag);
                case 5:
                    return JceField.create(this.bs.getDouble(), hd.tag);
                case 6:
                    int len = this.bs.get();
                    if (len < 0) {
                        len += 256;
                    }
                    return readString(hd, len);
                case DesktopSwitchSpecialEffectSettingActivity.nRotationSetting /*7*/:
                    return readString(hd, this.bs.getInt());
                case DesktopSwitchSpecialEffectSettingActivity.nCubeSetting /*8*/:
                    int len2 = ((NumberField) read()).intValue();
                    JceField[] keys = new JceField[len2];
                    JceField[] values = new JceField[len2];
                    for (int i = 0; i < len2; i++) {
                        keys[i] = read();
                        values[i] = read();
                    }
                    return JceField.createMap(keys, values, hd.tag);
                case 9:
                    int len3 = ((NumberField) read()).intValue();
                    JceField[] fs = new JceField[len3];
                    for (int i2 = 0; i2 < len3; i2++) {
                        fs[i2] = read();
                    }
                    return JceField.createList(fs, hd.tag);
                case 10:
                    List<JceField> ls = new ArrayList<>();
                    while (true) {
                        JceField f = read();
                        if (f == null) {
                            return JceField.createStruct((JceField[]) ls.toArray(new JceField[0]), hd.tag);
                        }
                        ls.add(f);
                    }
                case 11:
                    return null;
                case 12:
                    return JceField.createZero(hd.tag);
                case 13:
                    int tag = hd.tag;
                    JceInputStream.readHead(hd, this.bs);
                    if (hd.type != 0) {
                        throw new JceDecodeException("type mismatch, simple_list only support byte, tag: " + tag + ", type: " + ((int) hd.type));
                    }
                    byte[] data = new byte[((NumberField) read()).intValue()];
                    this.bs.get(data);
                    return JceField.create(data, tag);
                default:
                    return null;
            }
        } catch (BufferUnderflowException e) {
            BufferUnderflowException bufferUnderflowException = e;
            return null;
        }
    }

    private JceField readString(JceInputStream.HeadData hd, int len) {
        String s;
        byte[] ss = new byte[len];
        this.bs.get(ss);
        try {
            s = new String(ss, this.sServerEncoding);
        } catch (UnsupportedEncodingException e) {
            s = new String(ss);
        }
        return JceField.create(s, hd.tag);
    }
}
