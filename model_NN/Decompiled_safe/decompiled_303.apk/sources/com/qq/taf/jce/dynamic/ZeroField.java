package com.qq.taf.jce.dynamic;

public final class ZeroField extends NumberField {
    ZeroField(int tag) {
        super(tag);
    }

    public Number getNumber() {
        return 0;
    }

    public byte byteValue() {
        return 0;
    }

    public double doubleValue() {
        return 0.0d;
    }

    public float floatValue() {
        return 0.0f;
    }

    public int intValue() {
        return 0;
    }

    public long longValue() {
        return 0;
    }

    public short shortValue() {
        return 0;
    }
}
