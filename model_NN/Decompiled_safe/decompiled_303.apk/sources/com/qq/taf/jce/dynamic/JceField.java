package com.qq.taf.jce.dynamic;

import com.qq.taf.jce.JceDecodeException;

public class JceField {
    private static ZeroField[] zs = new ZeroField[256];
    private int tag;

    JceField(int tag2) {
        this.tag = tag2;
    }

    public int getTag() {
        return this.tag;
    }

    public static JceField create(byte data, int tag2) {
        return new ByteField(data, tag2);
    }

    public static JceField create(short data, int tag2) {
        return new ShortField(data, tag2);
    }

    public static JceField create(int data, int tag2) {
        return new IntField(data, tag2);
    }

    public static JceField create(long data, int tag2) {
        return new LongField(data, tag2);
    }

    public static JceField create(float data, int tag2) {
        return new FloatField(data, tag2);
    }

    public static JceField create(double data, int tag2) {
        return new DoubleField(data, tag2);
    }

    public static JceField create(String data, int tag2) {
        return new StringField(data, tag2);
    }

    public static JceField create(byte[] data, int tag2) {
        return new ByteArrayField(data, tag2);
    }

    public static JceField createList(JceField[] data, int tag2) {
        return new ListField(data, tag2);
    }

    public static JceField createMap(JceField[] keys, JceField[] values, int tag2) {
        return new MapField(keys, values, tag2);
    }

    public static JceField createStruct(JceField[] data, int tag2) {
        return new StructField(data, tag2);
    }

    static {
        for (int i = 0; i < zs.length; i++) {
            zs[i] = new ZeroField(i);
        }
    }

    public static JceField createZero(int tag2) {
        if (tag2 >= 0 && tag2 < 255) {
            return zs[tag2];
        }
        throw new JceDecodeException("invalid tag: " + tag2);
    }
}
