package com.qq.taf.jce.dynamic;

public class StringField extends JceField {
    private String data;

    StringField(String data2, int tag) {
        super(tag);
        this.data = data2;
    }

    public String get() {
        return this.data;
    }

    public void set(String s) {
        this.data = s;
    }
}
