package mobi.intuitit.android.widget;

import android.database.ContentObserver;
import android.os.Handler;
import android.util.Log;

public final class e extends ContentObserver {
    private b a;

    public e(Handler handler, b bVar) {
        super(handler);
        this.a = bVar;
    }

    public final void onChange(boolean z) {
        if (this.a != null) {
            Log.d("WidgetContentObserver", "onChange");
            this.a.a();
            return;
        }
        Log.d("WidgetContentObserver", "onChange -> no listerner");
    }
}
