package mobi.intuitit.android.widget;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;

final class i extends AsyncQueryHandler {
    private /* synthetic */ d a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i(d dVar, ContentResolver contentResolver) {
        super(contentResolver);
        this.a = dVar;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0042 A[Catch:{ Exception -> 0x0066 }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a8 A[Catch:{ Exception -> 0x0066 }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0056 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onQueryComplete(int r7, java.lang.Object r8, android.database.Cursor r9) {
        /*
            r6 = this;
            super.onQueryComplete(r7, r8, r9)
            java.lang.String r0 = "LAUNCHER"
            java.lang.String r1 = "API v1 QUERY COMPLETE"
            android.util.Log.d(r0, r1)
            mobi.intuitit.android.widget.d r0 = r6.a
            java.util.ArrayList r0 = r0.d
            r0.clear()
            mobi.intuitit.android.widget.d r0 = r6.a
            mobi.intuitit.android.widget.m[] r0 = r0.c
            int r0 = r0.length
        L_0x0016:
            if (r9 == 0) goto L_0x00bc
            boolean r1 = r9.moveToNext()
            if (r1 == 0) goto L_0x00bc
            mobi.intuitit.android.widget.x r1 = new mobi.intuitit.android.widget.x
            mobi.intuitit.android.widget.d r2 = r6.a
            r1.<init>(r2, r0)
            r2 = 1
            int r2 = r0 - r2
        L_0x0028:
            if (r2 < 0) goto L_0x00b3
            mobi.intuitit.android.widget.v r3 = new mobi.intuitit.android.widget.v     // Catch:{ Exception -> 0x0066 }
            mobi.intuitit.android.widget.d r4 = r6.a     // Catch:{ Exception -> 0x0066 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0066 }
            mobi.intuitit.android.widget.d r4 = r6.a     // Catch:{ Exception -> 0x0066 }
            mobi.intuitit.android.widget.m[] r4 = r4.c     // Catch:{ Exception -> 0x0066 }
            r4 = r4[r2]     // Catch:{ Exception -> 0x0066 }
            int r5 = r4.a     // Catch:{ Exception -> 0x0066 }
            switch(r5) {
                case 100: goto L_0x005d;
                case 101: goto L_0x0078;
                case 102: goto L_0x008a;
                case 103: goto L_0x0081;
                case 104: goto L_0x006b;
                default: goto L_0x003c;
            }     // Catch:{ Exception -> 0x0066 }
        L_0x003c:
            mobi.intuitit.android.widget.d r5 = r6.a     // Catch:{ Exception -> 0x0066 }
            boolean r5 = r5.e     // Catch:{ Exception -> 0x0066 }
            if (r5 == 0) goto L_0x00a2
            boolean r4 = r4.e     // Catch:{ Exception -> 0x0066 }
            if (r4 == 0) goto L_0x00a2
            mobi.intuitit.android.widget.d r4 = r6.a     // Catch:{ Exception -> 0x0066 }
            int r4 = r4.f     // Catch:{ Exception -> 0x0066 }
            if (r4 < 0) goto L_0x0097
            mobi.intuitit.android.widget.d r4 = r6.a     // Catch:{ Exception -> 0x0066 }
            int r4 = r4.f     // Catch:{ Exception -> 0x0066 }
            java.lang.String r4 = r9.getString(r4)     // Catch:{ Exception -> 0x0066 }
            r3.b = r4     // Catch:{ Exception -> 0x0066 }
        L_0x0056:
            mobi.intuitit.android.widget.v[] r4 = r1.a     // Catch:{ Exception -> 0x0066 }
            r4[r2] = r3     // Catch:{ Exception -> 0x0066 }
            int r2 = r2 + -1
            goto L_0x0028
        L_0x005d:
            int r5 = r4.d     // Catch:{ Exception -> 0x0066 }
            java.lang.String r5 = r9.getString(r5)     // Catch:{ Exception -> 0x0066 }
            r3.a = r5     // Catch:{ Exception -> 0x0066 }
            goto L_0x003c
        L_0x0066:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0016
        L_0x006b:
            int r5 = r4.d     // Catch:{ Exception -> 0x0066 }
            java.lang.String r5 = r9.getString(r5)     // Catch:{ Exception -> 0x0066 }
            android.text.Spanned r5 = android.text.Html.fromHtml(r5)     // Catch:{ Exception -> 0x0066 }
            r3.a = r5     // Catch:{ Exception -> 0x0066 }
            goto L_0x003c
        L_0x0078:
            int r5 = r4.d     // Catch:{ Exception -> 0x0066 }
            byte[] r5 = r9.getBlob(r5)     // Catch:{ Exception -> 0x0066 }
            r3.a = r5     // Catch:{ Exception -> 0x0066 }
            goto L_0x003c
        L_0x0081:
            int r5 = r4.d     // Catch:{ Exception -> 0x0066 }
            java.lang.String r5 = r9.getString(r5)     // Catch:{ Exception -> 0x0066 }
            r3.a = r5     // Catch:{ Exception -> 0x0066 }
            goto L_0x003c
        L_0x008a:
            int r5 = r4.d     // Catch:{ Exception -> 0x0066 }
            int r5 = r9.getInt(r5)     // Catch:{ Exception -> 0x0066 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x0066 }
            r3.a = r5     // Catch:{ Exception -> 0x0066 }
            goto L_0x003c
        L_0x0097:
            int r4 = r9.getPosition()     // Catch:{ Exception -> 0x0066 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ Exception -> 0x0066 }
            r3.b = r4     // Catch:{ Exception -> 0x0066 }
            goto L_0x0056
        L_0x00a2:
            mobi.intuitit.android.widget.d r4 = r6.a     // Catch:{ Exception -> 0x0066 }
            int r4 = r4.f     // Catch:{ Exception -> 0x0066 }
            if (r4 < 0) goto L_0x0056
            mobi.intuitit.android.widget.d r4 = r6.a     // Catch:{ Exception -> 0x0066 }
            int r4 = r4.f     // Catch:{ Exception -> 0x0066 }
            java.lang.String r4 = r9.getString(r4)     // Catch:{ Exception -> 0x0066 }
            r3.b = r4     // Catch:{ Exception -> 0x0066 }
            goto L_0x0056
        L_0x00b3:
            mobi.intuitit.android.widget.d r2 = r6.a     // Catch:{ Exception -> 0x0066 }
            java.util.ArrayList r2 = r2.d     // Catch:{ Exception -> 0x0066 }
            r2.add(r1)     // Catch:{ Exception -> 0x0066 }
            goto L_0x0016
        L_0x00bc:
            if (r9 == 0) goto L_0x00c1
            r9.close()
        L_0x00c1:
            java.lang.System.gc()
            mobi.intuitit.android.widget.d r0 = r6.a
            r0.notifyDataSetInvalidated()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: mobi.intuitit.android.widget.i.onQueryComplete(int, java.lang.Object, android.database.Cursor):void");
    }
}
