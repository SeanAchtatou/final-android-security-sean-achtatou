package mobi.intuitit.android.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public final class g {
    private static g a;
    private final HashMap b = new HashMap();
    private final HashMap c = new HashMap();
    private final HashMap d = new HashMap();
    private final HashMap e = new HashMap();

    public static g a() {
        if (a == null) {
            a = new g();
        }
        return a;
    }

    public final Drawable a(Context context, int i, int i2) {
        SoftReference softReference;
        Drawable drawable = (!this.d.containsKey(Integer.valueOf(i2)) || this.d.get(Integer.valueOf(i2)) == null || (softReference = (SoftReference) this.d.get(Integer.valueOf(i2))) == null) ? null : (Drawable) softReference.get();
        if (drawable != null) {
            return drawable;
        }
        InputStream openRawResource = context.getResources().openRawResource(i2);
        try {
            Drawable createFromStream = Drawable.createFromStream(openRawResource, context.getResources().getResourceName(i2));
            try {
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            this.d.put(Integer.valueOf(i2), new SoftReference(createFromStream));
            ArrayList arrayList = (ArrayList) this.e.get(Integer.valueOf(i));
            if (arrayList == null) {
                arrayList = new ArrayList();
            }
            arrayList.add(Integer.valueOf(i2));
            this.e.put(Integer.valueOf(i), arrayList);
            return createFromStream;
        } finally {
            try {
                openRawResource.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        }
    }

    public final Drawable a(Context context, int i, String str) {
        Drawable drawable;
        SoftReference softReference;
        Drawable drawable2 = (!this.b.containsKey(str) || this.b.get(str) == null || (softReference = (SoftReference) this.b.get(str)) == null) ? null : (Drawable) softReference.get();
        if (drawable2 != null) {
            return drawable2;
        }
        Uri parse = Uri.parse(str);
        String scheme = parse.getScheme();
        if ("android.resource".equals(scheme)) {
            Log.w("ImageView", "Unable to open content: " + parse);
            drawable = drawable2;
        } else if ("content".equals(scheme) || "file".equals(scheme)) {
            try {
                drawable = Drawable.createFromStream(context.getContentResolver().openInputStream(parse), null);
            } catch (Exception e2) {
                Log.w("ImageView", "Unable to open content: " + parse, e2);
                drawable = drawable2;
            }
        } else {
            drawable = Drawable.createFromPath(parse.toString());
        }
        this.b.put(str, new SoftReference(drawable));
        ArrayList arrayList = (ArrayList) this.c.get(Integer.valueOf(i));
        if (arrayList == null) {
            arrayList = new ArrayList();
        }
        arrayList.add(str);
        this.c.put(Integer.valueOf(i), arrayList);
        return drawable;
    }

    public final String a(int i) {
        Drawable drawable;
        Drawable drawable2;
        ArrayList arrayList = (ArrayList) this.c.get(Integer.valueOf(i));
        if (arrayList != null) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                String str = (String) it.next();
                if (this.b.containsKey(str) && this.b.get(str) != null) {
                    SoftReference softReference = (SoftReference) this.b.get(str);
                    if (!(softReference == null || (drawable2 = (Drawable) softReference.get()) == null)) {
                        drawable2.setCallback(null);
                    }
                    this.b.remove(str);
                    Log.d("ListViewImageManager", "image URI removed from cache : " + str);
                }
            }
        }
        this.c.remove(Integer.valueOf(i));
        ArrayList arrayList2 = (ArrayList) this.e.get(Integer.valueOf(i));
        if (arrayList2 != null) {
            Iterator it2 = arrayList2.iterator();
            while (it2.hasNext()) {
                Integer num = (Integer) it2.next();
                if (this.d.containsKey(num) && this.d.get(num) != null) {
                    SoftReference softReference2 = (SoftReference) this.d.get(num);
                    if (!(softReference2 == null || (drawable = (Drawable) softReference2.get()) == null)) {
                        drawable.setCallback(null);
                    }
                    this.d.remove(num);
                    Log.d("ListViewImageManager", "image ID removed from cache : " + num);
                }
            }
        }
        this.e.remove(Integer.valueOf(i));
        return null;
    }
}
