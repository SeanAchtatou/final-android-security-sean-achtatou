package mobi.intuitit.android.widget;

import android.os.Parcel;
import android.os.Parcelable;

final class c implements Parcelable.Creator {
    c() {
    }

    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new BoundRemoteViews(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new BoundRemoteViews[i];
    }
}
