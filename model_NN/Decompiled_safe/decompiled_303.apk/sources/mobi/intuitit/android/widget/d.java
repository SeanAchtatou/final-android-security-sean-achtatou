package mobi.intuitit.android.widget;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.launcher.SearchAppTestActivity;
import java.util.ArrayList;

public final class d extends w {
    private static g n = g.a();
    final int a;
    final int b;
    m[] c;
    public ArrayList d = new ArrayList();
    public final boolean e;
    final int f;
    ComponentName g;
    private LayoutInflater h;
    private int i;
    private boolean j = true;
    private final ContentResolver k;
    private final Intent l;
    private i m;
    private Handler o = new Handler();
    private Runnable p = new f(this);

    public d(Context context, Intent intent, ComponentName componentName, int i2, int i3) {
        this.a = i2;
        this.b = i3;
        this.k = context.getContentResolver();
        this.l = intent;
        this.g = componentName;
        this.h = LayoutInflater.from(context);
        this.j = intent.getBooleanExtra("mobi.intuitit.android.hpp.EXTRA_DATA_PROVIDER_ALLOW_REQUERY", false);
        this.i = intent.getIntExtra("mobi.intuitit.android.hpp.EXTRA_ITEM_LAYOUT_ID", -1);
        if (this.i <= 0) {
            throw new IllegalArgumentException("The passed layout id is illegal");
        }
        this.e = intent.getBooleanExtra("mobi.intuitit.android.hpp.EXTRA_ITEM_CHILDREN_CLICKABLE", false);
        this.f = intent.getIntExtra("mobi.intuitit.android.hpp.EXTRA_ITEM_ACTION_VIEW_URI_INDEX", -1);
        a(intent);
        this.m = new i(this, this.k);
        this.o.post(this.p);
    }

    private void a(Intent intent) {
        int[] intArrayExtra = intent.getIntArrayExtra("mobi.intuitit.android.hpp.EXTRA_VIEW_TYPES");
        int[] intArrayExtra2 = intent.getIntArrayExtra("mobi.intuitit.android.hpp.EXTRA_VIEW_IDS");
        int[] intArrayExtra3 = intent.getIntArrayExtra("mobi.intuitit.android.hpp.EXTRA_CURSOR_INDICES");
        int[] intArrayExtra4 = intent.getIntArrayExtra("mobi.intuitit.android.hpp.EXTRA_DEFAULT_RESOURCES");
        boolean[] booleanArrayExtra = intent.getBooleanArrayExtra("mobi.intuitit.android.hpp.EXTRA_VIEW_CLICKABLE");
        if (intArrayExtra == null || intArrayExtra2 == null || intArrayExtra3 == null) {
            throw new IllegalArgumentException("A mapping component is missing");
        } else if (intArrayExtra.length == intArrayExtra2.length && intArrayExtra.length == intArrayExtra3.length) {
            int length = intArrayExtra.length;
            this.c = new m[length];
            for (int i2 = length - 1; i2 >= 0; i2--) {
                this.c[i2] = new m(this, intArrayExtra[i2], intArrayExtra2[i2], intArrayExtra3[i2]);
            }
            if (booleanArrayExtra != null && booleanArrayExtra.length == length) {
                for (int i3 = length - 1; i3 >= 0; i3--) {
                    this.c[i3].e = booleanArrayExtra[i3];
                }
            }
            if (intArrayExtra4 != null && intArrayExtra4.length == length) {
                for (int i4 = length - 1; i4 >= 0; i4--) {
                    this.c[i4].c = intArrayExtra4[i4];
                }
            }
        } else {
            throw new IllegalArgumentException("Mapping inconsistent");
        }
    }

    static /* synthetic */ void a(d dVar) {
        if (dVar.c != null) {
            Log.d("LAUNCHER", "API v1 START QUERY");
            dVar.m.startQuery(1, "cookie", Uri.parse(dVar.l.getStringExtra("mobi.intuitit.android.hpp.EXTRA_DATA_URI")), dVar.l.getStringArrayExtra("mobi.intuitit.android.hpp.EXTRA_PROJECTION"), dVar.l.getStringExtra("mobi.intuitit.android.hpp.EXTRA_SELECTION"), dVar.l.getStringArrayExtra("mobi.intuitit.android.hpp.EXTRA_SELECTION_ARGUMENTS"), dVar.l.getStringExtra("mobi.intuitit.android.hpp.EXTRA_SORT_ORDER"));
        }
    }

    public final void a() {
        Log.d("LauncherPP_WLA", "notifyToRegenerate widgetId = " + this.a);
        this.o.post(this.p);
    }

    public final void b() {
        n.a(this.a);
        this.d.clear();
    }

    public final int getCount() {
        return this.d.size();
    }

    public final Object getItem(int i2) {
        return this.d.get(i2);
    }

    public final long getItemId(int i2) {
        return (long) i2;
    }

    public final int getItemViewType(int i2) {
        return 0;
    }

    public final View getView(int i2, View view, ViewGroup viewGroup) {
        p pVar;
        View view2;
        View view3;
        if (view == null) {
            View inflate = this.h.inflate(this.i, (ViewGroup) null);
            pVar = new p(this.c.length);
            inflate.setTag(pVar);
            view2 = inflate;
        } else {
            pVar = (p) view.getTag();
            view2 = view;
        }
        if (i2 < getCount()) {
            Context context = view2.getContext();
            if (this.c != null) {
                int length = this.c.length - 1;
                while (length >= 0) {
                    try {
                        m mVar = this.c[length];
                        if (pVar.a[length] != null) {
                            view3 = pVar.a[length];
                        } else {
                            View findViewById = view2.findViewById(mVar.b);
                            pVar.a[length] = findViewById;
                            view3 = findViewById;
                        }
                        v vVar = ((x) this.d.get(i2)).a[length];
                        switch (mVar.a) {
                            case SearchAppTestActivity.TAB_TYPE_NET:
                                if (view3 instanceof TextView) {
                                    if (vVar.a == null) {
                                        ((TextView) view3).setText(mVar.c);
                                        break;
                                    } else {
                                        ((TextView) view3).setText((String) vVar.a);
                                        break;
                                    }
                                }
                                break;
                            case 101:
                                if (view3 instanceof ImageView) {
                                    ImageView imageView = (ImageView) view3;
                                    if (vVar.a == null) {
                                        if (mVar.c <= 0) {
                                            imageView.setImageDrawable(null);
                                            break;
                                        } else {
                                            imageView.setImageResource(mVar.c);
                                            break;
                                        }
                                    } else {
                                        byte[] bArr = (byte[]) vVar.a;
                                        imageView.setImageBitmap(BitmapFactory.decodeByteArray(bArr, 0, bArr.length));
                                        break;
                                    }
                                }
                                break;
                            case 102:
                                if (view3 instanceof ImageView) {
                                    ImageView imageView2 = (ImageView) view3;
                                    if (((Integer) vVar.a).intValue() <= 0) {
                                        if (mVar.c <= 0) {
                                            imageView2.setImageDrawable(null);
                                            break;
                                        } else {
                                            imageView2.setImageDrawable(n.a(context, this.a, mVar.c));
                                            break;
                                        }
                                    } else {
                                        imageView2.setImageDrawable(n.a(context, this.a, ((Integer) vVar.a).intValue()));
                                        break;
                                    }
                                }
                                break;
                            case 103:
                                if (view3 instanceof ImageView) {
                                    ImageView imageView3 = (ImageView) view3;
                                    if (vVar.a != null && !vVar.a.equals("")) {
                                        imageView3.setImageDrawable(n.a(context, this.a, (String) vVar.a));
                                        break;
                                    } else {
                                        imageView3.setImageDrawable(null);
                                        break;
                                    }
                                }
                                break;
                            case 104:
                                if (view3 instanceof TextView) {
                                    if (vVar.a == null) {
                                        ((TextView) view3).setText(mVar.c);
                                        break;
                                    } else {
                                        ((TextView) view3).setText((Spanned) vVar.a);
                                        break;
                                    }
                                }
                                break;
                        }
                        pVar.b = null;
                        if (this.e && mVar.e) {
                            view3.setTag(vVar.b);
                            view3.setOnClickListener(new l(this));
                        } else if (this.f >= 0) {
                            pVar.b = vVar.b;
                        }
                        length--;
                    } catch (OutOfMemoryError e2) {
                        Log.d("LauncherPP_WLA", "****** freeMemory = " + (Runtime.getRuntime().freeMemory() / 1000) + " Kb");
                        System.gc();
                        e2.printStackTrace();
                    } catch (Exception e3) {
                        Log.d("LauncherPP_WLA", "****** freeMemory = " + (Runtime.getRuntime().freeMemory() / 1000) + " Kb");
                        e3.printStackTrace();
                    }
                }
                if (Runtime.getRuntime().freeMemory() < 500000) {
                    Log.d("LauncherPP_WLA", "force gargabe collecting below 500kb");
                    System.gc();
                }
            }
        }
        return view2;
    }

    public final int getViewTypeCount() {
        return 1;
    }
}
