package mobi.intuitit.android.widget;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;

public final class u extends w {
    /* access modifiers changed from: private */
    public BoundRemoteViews a = null;
    /* access modifiers changed from: private */
    public final Context b;
    /* access modifiers changed from: private */
    public Intent c;
    /* access modifiers changed from: private */
    public final h d;
    private ComponentName e;
    private Handler f = new Handler();
    private Runnable g = new a(this);

    public u(Context context, Intent intent, ComponentName componentName) {
        this.b = context;
        this.e = componentName;
        this.c = intent;
        this.a = (BoundRemoteViews) intent.getParcelableExtra("mobi.intuitit.android.hpp.EXTRA_ITEM_LAYOUT_REMOTEVIEWS");
        this.d = new h(this, this.b.getContentResolver());
        this.f.post(this.g);
    }

    public final synchronized void a() {
        this.f.post(this.g);
    }

    public final void a(Intent intent) {
        if (intent.hasExtra("mobi.intuitit.android.hpp.EXTRA_ITEM_LAYOUT_REMOTEVIEWS")) {
            if (this.a != null) {
                this.a.a();
            }
            this.c = intent;
            this.a = (BoundRemoteViews) intent.getParcelableExtra("mobi.intuitit.android.hpp.EXTRA_ITEM_LAYOUT_REMOTEVIEWS");
            this.f.post(this.g);
        }
    }

    public final void b() {
        this.a.a();
    }

    public final int getCount() {
        return this.a.b();
    }

    public final Object getItem(int i) {
        this.a.a(i);
        return this.a;
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        this.a.a(i);
        if (view == null) {
            return this.a.a(this.b);
        }
        this.a.a(view);
        return view;
    }
}
