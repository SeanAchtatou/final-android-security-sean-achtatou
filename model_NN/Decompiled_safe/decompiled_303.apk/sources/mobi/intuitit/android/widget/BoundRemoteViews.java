package mobi.intuitit.android.widget;

import android.app.PendingIntent;
import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import com.tencent.module.setting.DesktopSwitchSpecialEffectSettingActivity;
import mobi.intuitit.android.widget.SimpleRemoteViews;

public class BoundRemoteViews extends SimpleRemoteViews {
    public static final Parcelable.Creator CREATOR = new c();
    /* access modifiers changed from: private */
    public s b;
    /* access modifiers changed from: private */
    public int c;

    public class BindingAction extends SimpleRemoteViews.ReflectionAction {
        private int b;
        private int c;

        public BindingAction(Parcel parcel) {
            super(parcel);
        }

        /* access modifiers changed from: protected */
        public final int a() {
            return 99;
        }

        public final Object a(Context context) {
            try {
                switch (this.a) {
                    case 9:
                    case 10:
                        return context.getString(this.c);
                    case 11:
                    default:
                        return null;
                    case 12:
                        return BitmapFactory.decodeResource(context.getResources(), this.c);
                }
            } catch (Exception e) {
                return null;
            }
        }

        public final Object a(Cursor cursor) {
            try {
                switch (this.a) {
                    case 2:
                        return Byte.valueOf((byte) cursor.getInt(this.b));
                    case 3:
                        return Short.valueOf((short) cursor.getInt(this.b));
                    case 4:
                        return Integer.valueOf(cursor.getInt(this.b));
                    case 5:
                        return Long.valueOf(cursor.getLong(this.b));
                    case 6:
                        return Float.valueOf(cursor.getFloat(this.b));
                    case DesktopSwitchSpecialEffectSettingActivity.nRotationSetting:
                        return Double.valueOf(cursor.getDouble(this.b));
                    case DesktopSwitchSpecialEffectSettingActivity.nCubeSetting:
                        return Character.valueOf(cursor.getString(this.b).charAt(0));
                    case 9:
                    case 10:
                        return cursor.getString(this.b);
                    case 11:
                        return Uri.parse(cursor.getString(this.b));
                    case 12:
                        byte[] blob = cursor.getBlob(this.b);
                        return BitmapFactory.decodeByteArray(blob, 0, blob.length);
                    default:
                        return null;
                }
            } catch (Exception e) {
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public final void a(Parcel parcel) {
            this.b = parcel.readInt();
            this.c = parcel.readInt();
        }

        /* access modifiers changed from: protected */
        public final void a(Parcel parcel, int i) {
            parcel.writeInt(this.b);
            parcel.writeInt(this.c);
        }

        public final void a(View view) {
            super.a(view);
        }

        /* access modifiers changed from: protected */
        public final Object b() {
            return BoundRemoteViews.this.b.a(BoundRemoteViews.this.c, this);
        }
    }

    public class SetBoundOnClickIntent extends SimpleRemoteViews.Action {
        /* access modifiers changed from: private */
        public final String b;
        private final int c;
        private final int d;
        /* access modifiers changed from: private */
        public final PendingIntent e;

        public SetBoundOnClickIntent(Parcel parcel) {
            this.d = parcel.readInt();
            this.b = parcel.readString();
            this.c = parcel.readInt();
            this.e = (PendingIntent) PendingIntent.CREATOR.createFromParcel(parcel);
        }

        public final String a(Cursor cursor) {
            return cursor.getString(this.c);
        }

        public final void a(View view) {
            View findViewById = view.findViewById(this.d);
            if (findViewById != null && this.e != null) {
                findViewById.setOnClickListener(new y(this, BoundRemoteViews.this.c));
            }
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(100);
            parcel.writeInt(this.d);
            parcel.writeString(this.b);
            parcel.writeInt(this.c);
            this.e.writeToParcel(parcel, 0);
        }
    }

    public BoundRemoteViews(Parcel parcel) {
        super(parcel);
    }

    /* access modifiers changed from: protected */
    public final SimpleRemoteViews.Action a(int i, Parcel parcel) {
        return i == 99 ? new BindingAction(parcel) : i == 100 ? new SetBoundOnClickIntent(parcel) : super.a(i, parcel);
    }

    public final void a() {
        if (this.b != null) {
            this.b.a();
        }
        this.b = null;
    }

    public final void a(int i) {
        this.c = i;
    }

    public final void a(Cursor cursor, Context context) {
        this.b = new s(this, cursor, context);
    }

    public final void a(View view) {
        try {
            if (this.a != null) {
                int size = this.a.size();
                for (int i = 0; i < size; i++) {
                    SimpleRemoteViews.Action action = (SimpleRemoteViews.Action) this.a.get(i);
                    if ((action instanceof BindingAction) || (action instanceof SetBoundOnClickIntent)) {
                        action.a(view);
                    }
                }
            }
        } catch (OutOfMemoryError e) {
            System.gc();
        }
    }

    public final int b() {
        if (this.b != null) {
            return this.b.a.size();
        }
        return 0;
    }
}
