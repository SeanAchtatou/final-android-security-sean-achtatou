package mobi.intuitit.android.widget;

import android.appwidget.AppWidgetHostView;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

final class q extends BroadcastReceiver implements AbsListView.OnScrollListener {
    final /* synthetic */ WidgetSpace a;

    q(WidgetSpace widgetSpace) {
        this.a = widgetSpace;
    }

    private synchronized String a(Context context, Intent intent) {
        String message;
        try {
            String stringExtra = intent.getStringExtra("mobi.intuitit.android.hpp.EXTRA_DATA_URI");
            k kVar = (k) WidgetSpace.c.get(stringExtra);
            if (kVar != null) {
                kVar.a = null;
                context.getContentResolver().unregisterContentObserver(kVar.c);
                kVar.d = null;
                kVar.c = null;
                if (kVar.e != null) {
                    kVar.e.b();
                }
                WidgetSpace.c.remove(stringExtra);
            }
            message = null;
        } catch (Exception e) {
            e.printStackTrace();
            message = e.getMessage();
        }
        return message;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v2, resolved type: android.widget.ListView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v41, resolved type: mobi.intuitit.android.widget.d} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v42, resolved type: mobi.intuitit.android.widget.u} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v50, resolved type: mobi.intuitit.android.widget.u} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v35, resolved type: mobi.intuitit.android.widget.u} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized java.lang.String a(android.content.Context r18, android.content.Intent r19, android.appwidget.AppWidgetHostView r20) {
        /*
            r17 = this;
            monitor-enter(r17)
            java.lang.String r5 = "mobi.intuitit.android.hpp.EXTRA_VIEW_ID"
            r6 = -1
            r0 = r19
            r1 = r5
            r2 = r6
            int r10 = r0.getIntExtra(r1, r2)     // Catch:{ all -> 0x01f9 }
            if (r10 > 0) goto L_0x0012
            java.lang.String r5 = "Dummy view id needed."
        L_0x0010:
            monitor-exit(r17)
            return r5
        L_0x0012:
            android.appwidget.AppWidgetProviderInfo r5 = r20.getAppWidgetInfo()     // Catch:{ all -> 0x01f9 }
            android.content.ComponentName r8 = r5.provider     // Catch:{ all -> 0x01f9 }
            int r9 = r20.getAppWidgetId()     // Catch:{ all -> 0x01f9 }
            r0 = r17
            mobi.intuitit.android.widget.WidgetSpace r0 = r0.a     // Catch:{ Exception -> 0x01d3 }
            r5 = r0
            android.content.Context r5 = r5.getContext()     // Catch:{ Exception -> 0x01d3 }
            android.appwidget.AppWidgetProviderInfo r6 = r20.getAppWidgetInfo()     // Catch:{ Exception -> 0x01d3 }
            android.content.ComponentName r6 = r6.provider     // Catch:{ Exception -> 0x01d3 }
            java.lang.String r6 = r6.getPackageName()     // Catch:{ Exception -> 0x01d3 }
            r7 = 2
            android.content.Context r6 = r5.createPackageContext(r6, r7)     // Catch:{ Exception -> 0x01d3 }
            r0 = r20
            r1 = r10
            android.view.View r5 = r0.findViewById(r1)     // Catch:{ Exception -> 0x01d3 }
            if (r5 != 0) goto L_0x0040
            java.lang.String r5 = "Dummy view needed."
            goto L_0x0010
        L_0x0040:
            boolean r7 = r5 instanceof android.widget.AbsListView     // Catch:{ Exception -> 0x01d3 }
            if (r7 == 0) goto L_0x0139
            r0 = r5
            android.widget.AbsListView r0 = (android.widget.AbsListView) r0     // Catch:{ Exception -> 0x01d3 }
            r20 = r0
            r11 = r20
        L_0x004b:
            java.lang.String r5 = "mobi.intuitit.android.hpp.EXTRA_DATA_URI"
            r0 = r19
            r1 = r5
            java.lang.String r12 = r0.getStringExtra(r1)     // Catch:{ Exception -> 0x01d3 }
            java.util.HashMap r5 = mobi.intuitit.android.widget.WidgetSpace.c     // Catch:{ Exception -> 0x01d3 }
            java.lang.Object r20 = r5.get(r12)     // Catch:{ Exception -> 0x01d3 }
            mobi.intuitit.android.widget.k r20 = (mobi.intuitit.android.widget.k) r20     // Catch:{ Exception -> 0x01d3 }
            if (r20 != 0) goto L_0x01c6
            r5 = 1
            r13 = r5
        L_0x0060:
            if (r13 == 0) goto L_0x01da
            mobi.intuitit.android.widget.k r14 = new mobi.intuitit.android.widget.k     // Catch:{ Exception -> 0x01d3 }
            r0 = r17
            mobi.intuitit.android.widget.WidgetSpace r0 = r0.a     // Catch:{ Exception -> 0x01d3 }
            r5 = r0
            r14.<init>(r5)     // Catch:{ Exception -> 0x01d3 }
            java.lang.String r5 = "mobi.intuitit.android.hpp.EXTRA_ITEM_LAYOUT_REMOTEVIEWS"
            r0 = r19
            r1 = r5
            boolean r5 = r0.hasExtra(r1)     // Catch:{ Exception -> 0x01d3 }
            if (r5 == 0) goto L_0x01ca
            mobi.intuitit.android.widget.u r5 = new mobi.intuitit.android.widget.u     // Catch:{ Exception -> 0x01d3 }
            r0 = r5
            r1 = r6
            r2 = r19
            r3 = r8
            r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x01d3 }
        L_0x0081:
            mobi.intuitit.android.widget.z r6 = new mobi.intuitit.android.widget.z     // Catch:{ Exception -> 0x01d3 }
            r0 = r6
            r1 = r17
            r2 = r5
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x01d3 }
            mobi.intuitit.android.widget.e r7 = new mobi.intuitit.android.widget.e     // Catch:{ Exception -> 0x01d3 }
            android.os.Handler r15 = r14.d     // Catch:{ Exception -> 0x01d3 }
            r7.<init>(r15, r6)     // Catch:{ Exception -> 0x01d3 }
            r14.c = r7     // Catch:{ Exception -> 0x01d3 }
            java.lang.String r6 = "mobi.intuitit.android.hpp.EXTRA_DATA_URI"
            r0 = r19
            r1 = r6
            java.lang.String r6 = r0.getStringExtra(r1)     // Catch:{ Exception -> 0x01d3 }
            android.net.Uri r6 = android.net.Uri.parse(r6)     // Catch:{ Exception -> 0x01d3 }
            android.content.ContentResolver r7 = r18.getContentResolver()     // Catch:{ Exception -> 0x01d3 }
            r15 = 1
            r0 = r14
            android.database.ContentObserver r0 = r0.c     // Catch:{ Exception -> 0x01d3 }
            r16 = r0
            r0 = r7
            r1 = r6
            r2 = r15
            r3 = r16
            r0.registerContentObserver(r1, r2, r3)     // Catch:{ Exception -> 0x01d3 }
            r14.e = r5     // Catch:{ Exception -> 0x01d3 }
            java.lang.String r5 = "WidgetSpace"
            java.lang.String r6 = "makeScrollable : recreate listview adapter"
            android.util.Log.d(r5, r6)     // Catch:{ Exception -> 0x01d3 }
            r5 = r14
        L_0x00bc:
            mobi.intuitit.android.widget.w r6 = r5.e     // Catch:{ Exception -> 0x01d3 }
            r11.setAdapter(r6)     // Catch:{ Exception -> 0x01d3 }
            mobi.intuitit.android.widget.w r6 = r5.e     // Catch:{ Exception -> 0x01d3 }
            boolean r6 = r6 instanceof mobi.intuitit.android.widget.d     // Catch:{ Exception -> 0x01d3 }
            if (r6 == 0) goto L_0x00e3
            r0 = r5
            mobi.intuitit.android.widget.w r0 = r0.e     // Catch:{ Exception -> 0x01d3 }
            r18 = r0
            mobi.intuitit.android.widget.d r18 = (mobi.intuitit.android.widget.d) r18     // Catch:{ Exception -> 0x01d3 }
            r0 = r18
            boolean r0 = r0.e     // Catch:{ Exception -> 0x01d3 }
            r6 = r0
            if (r6 != 0) goto L_0x00e3
            mobi.intuitit.android.widget.t r6 = new mobi.intuitit.android.widget.t     // Catch:{ Exception -> 0x01d3 }
            r0 = r6
            r1 = r17
            r2 = r8
            r3 = r9
            r4 = r10
            r0.<init>(r1, r2, r3, r4)     // Catch:{ Exception -> 0x01d3 }
            r11.setOnItemClickListener(r6)     // Catch:{ Exception -> 0x01d3 }
        L_0x00e3:
            r6 = 0
            r11.setFocusableInTouchMode(r6)     // Catch:{ Exception -> 0x01d3 }
            r0 = r11
            r1 = r17
            r0.setOnScrollListener(r1)     // Catch:{ Exception -> 0x01d3 }
            r5.b = r9     // Catch:{ Exception -> 0x01d3 }
            r5.a = r11     // Catch:{ Exception -> 0x01d3 }
            r5.f = r12     // Catch:{ Exception -> 0x01d3 }
            java.util.HashMap r6 = mobi.intuitit.android.widget.WidgetSpace.c     // Catch:{ Exception -> 0x01d3 }
            r6.put(r12, r5)     // Catch:{ Exception -> 0x01d3 }
            java.lang.String r6 = "mobi.intuitit.android.hpp.EXTRA_LISTVIEW_POSITION"
            r7 = -1
            r0 = r19
            r1 = r6
            r2 = r7
            int r6 = r0.getIntExtra(r1, r2)     // Catch:{ Exception -> 0x01d3 }
            if (r6 < 0) goto L_0x0108
            r11.setSelection(r6)     // Catch:{ Exception -> 0x01d3 }
        L_0x0108:
            if (r13 != 0) goto L_0x0115
            mobi.intuitit.android.widget.w r6 = r5.e     // Catch:{ Exception -> 0x01d3 }
            boolean r6 = r6 instanceof mobi.intuitit.android.widget.d     // Catch:{ Exception -> 0x01d3 }
            if (r6 == 0) goto L_0x0115
            mobi.intuitit.android.widget.w r5 = r5.e     // Catch:{ Exception -> 0x01d3 }
            r5.a()     // Catch:{ Exception -> 0x01d3 }
        L_0x0115:
            java.lang.System.gc()     // Catch:{ Exception -> 0x01d3 }
            java.lang.String r5 = "WidgetSpace"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01d3 }
            r6.<init>()     // Catch:{ Exception -> 0x01d3 }
            java.lang.String r7 = "AFTER ADDING, Our Scrollable widgets array contains:"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x01d3 }
            java.util.HashMap r7 = mobi.intuitit.android.widget.WidgetSpace.c     // Catch:{ Exception -> 0x01d3 }
            int r7 = r7.size()     // Catch:{ Exception -> 0x01d3 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x01d3 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x01d3 }
            android.util.Log.d(r5, r6)     // Catch:{ Exception -> 0x01d3 }
            r5 = 0
            goto L_0x0010
        L_0x0139:
            java.lang.String r5 = "mobi.intuitit.android.hpp.EXTRA_LISTVIEW_REMOTEVIEWS"
            r0 = r19
            r1 = r5
            boolean r5 = r0.hasExtra(r1)     // Catch:{ Exception -> 0x01d3 }
            if (r5 == 0) goto L_0x0170
            java.lang.String r5 = "mobi.intuitit.android.hpp.EXTRA_LISTVIEW_REMOTEVIEWS"
            r0 = r19
            r1 = r5
            android.os.Parcelable r5 = r0.getParcelableExtra(r1)     // Catch:{ Exception -> 0x01d3 }
            mobi.intuitit.android.widget.SimpleRemoteViews r5 = (mobi.intuitit.android.widget.SimpleRemoteViews) r5     // Catch:{ Exception -> 0x01d3 }
            android.view.View r5 = r5.a(r6)     // Catch:{ Exception -> 0x01d3 }
            boolean r7 = r5 instanceof android.widget.AbsListView     // Catch:{ Exception -> 0x01d3 }
            if (r7 == 0) goto L_0x0169
            android.widget.AbsListView r5 = (android.widget.AbsListView) r5     // Catch:{ Exception -> 0x01d3 }
            r0 = r17
            r1 = r20
            r2 = r10
            r3 = r5
            boolean r7 = r0.a(r1, r2, r3)     // Catch:{ Exception -> 0x01d3 }
            if (r7 != 0) goto L_0x016d
            java.lang.String r5 = "Cannot replace the dummy with the list view inflated from the passed RemoteViews."
            goto L_0x0010
        L_0x0169:
            java.lang.String r5 = "could not create AbsListView from the passed RemoteViews"
            goto L_0x0010
        L_0x016d:
            r11 = r5
            goto L_0x004b
        L_0x0170:
            java.lang.String r5 = "mobi.intuitit.android.hpp.EXTRA_LISTVIEW_LAYOUT_ID"
            r7 = -1
            r0 = r19
            r1 = r5
            r2 = r7
            int r5 = r0.getIntExtra(r1, r2)     // Catch:{ Exception -> 0x01d3 }
            if (r5 > 0) goto L_0x01a3
            android.widget.ListView r5 = new android.widget.ListView     // Catch:{ Exception -> 0x01d3 }
            r0 = r17
            mobi.intuitit.android.widget.WidgetSpace r0 = r0.a     // Catch:{ Exception -> 0x01d3 }
            r7 = r0
            android.content.Context r7 = r7.getContext()     // Catch:{ Exception -> 0x01d3 }
            r5.<init>(r7)     // Catch:{ Exception -> 0x01d3 }
            r7 = 0
            r5.setCacheColorHint(r7)     // Catch:{ Exception -> 0x01d3 }
            r0 = r17
            r1 = r20
            r2 = r10
            r3 = r5
            boolean r7 = r0.a(r1, r2, r3)     // Catch:{ Exception -> 0x01d3 }
            if (r7 == 0) goto L_0x01a1
        L_0x019b:
            if (r5 != 0) goto L_0x01ff
            java.lang.String r5 = "Cannot create the default list view."
            goto L_0x0010
        L_0x01a1:
            r5 = 0
            goto L_0x019b
        L_0x01a3:
            android.view.LayoutInflater r7 = android.view.LayoutInflater.from(r6)     // Catch:{ Exception -> 0x01d3 }
            r11 = 0
            android.view.View r5 = r7.inflate(r5, r11)     // Catch:{ Exception -> 0x01d3 }
            boolean r7 = r5 instanceof android.widget.AbsListView     // Catch:{ Exception -> 0x01d3 }
            if (r7 == 0) goto L_0x01c2
            android.widget.AbsListView r5 = (android.widget.AbsListView) r5     // Catch:{ Exception -> 0x01d3 }
            r0 = r17
            r1 = r20
            r2 = r10
            r3 = r5
            boolean r7 = r0.a(r1, r2, r3)     // Catch:{ Exception -> 0x01d3 }
            if (r7 != 0) goto L_0x01fc
            java.lang.String r5 = "Cannot replace the dummy with the list view inflated from the passed layout resource id."
            goto L_0x0010
        L_0x01c2:
            java.lang.String r5 = "Cannot inflate a list view from the passed layout resource id."
            goto L_0x0010
        L_0x01c6:
            r5 = 0
            r13 = r5
            goto L_0x0060
        L_0x01ca:
            mobi.intuitit.android.widget.d r5 = new mobi.intuitit.android.widget.d     // Catch:{ Exception -> 0x01d3 }
            r7 = r19
            r5.<init>(r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x01d3 }
            goto L_0x0081
        L_0x01d3:
            r5 = move-exception
            java.lang.String r5 = r5.getMessage()     // Catch:{ all -> 0x01f9 }
            goto L_0x0010
        L_0x01da:
            java.lang.String r5 = "WidgetSpace"
            java.lang.String r6 = "makeScrollable : restore listview adapter"
            android.util.Log.d(r5, r6)     // Catch:{ Exception -> 0x01d3 }
            r0 = r20
            mobi.intuitit.android.widget.w r0 = r0.e     // Catch:{ Exception -> 0x01d3 }
            r5 = r0
            boolean r5 = r5 instanceof mobi.intuitit.android.widget.u     // Catch:{ Exception -> 0x01d3 }
            if (r5 == 0) goto L_0x01f5
            r0 = r20
            mobi.intuitit.android.widget.w r0 = r0.e     // Catch:{ Exception -> 0x01d3 }
            r18 = r0
            mobi.intuitit.android.widget.u r18 = (mobi.intuitit.android.widget.u) r18     // Catch:{ Exception -> 0x01d3 }
            r18.a(r19)     // Catch:{ Exception -> 0x01d3 }
        L_0x01f5:
            r5 = r20
            goto L_0x00bc
        L_0x01f9:
            r5 = move-exception
            monitor-exit(r17)
            throw r5
        L_0x01fc:
            r11 = r5
            goto L_0x004b
        L_0x01ff:
            r11 = r5
            goto L_0x004b
        */
        throw new UnsupportedOperationException("Method not decompiled: mobi.intuitit.android.widget.q.a(android.content.Context, android.content.Intent, android.appwidget.AppWidgetHostView):java.lang.String");
    }

    private static String a(Intent intent) {
        try {
            String stringExtra = intent.getStringExtra("mobi.intuitit.android.hpp.EXTRA_DATA_URI");
            int intExtra = intent.getIntExtra("mobi.intuitit.android.hpp.EXTRA_LISTVIEW_POSITION", 0);
            k kVar = (k) WidgetSpace.c.get(stringExtra);
            if (kVar != null) {
                kVar.a.setSelection(intExtra);
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    private boolean a(ViewGroup viewGroup, int i, View view) {
        int childCount = viewGroup.getChildCount() - 1;
        boolean z = false;
        while (childCount >= 0) {
            View childAt = viewGroup.getChildAt(childCount);
            if (childAt.getId() == i) {
                viewGroup.removeView(childAt);
                view.setId(i);
                viewGroup.addView(view, childCount, childAt.getLayoutParams());
                return true;
            }
            childCount--;
            z = childAt instanceof ViewGroup ? a((ViewGroup) childAt, i, view) | z : z;
        }
        return z;
    }

    public final void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.i("WidgetSpace - onReceive: ", "" + intent);
        int intExtra = intent.getIntExtra("appWidgetId", -1);
        if (intExtra < 0) {
            intExtra = intent.getIntExtra("mobi.intuitit.android.hpp.EXTRA_APPWIDGET_ID", -1);
        }
        if (intExtra < 0) {
            Log.e("WidgetSpace", "Scroll Provider cannot get a legal widget id");
            return;
        }
        AppWidgetHostView h = this.a.h(intExtra);
        if (h == null) {
            this.a.getContext().sendBroadcast(intent.setAction("mobi.intuitit.android.hpp.ERROR_SCROLL_CURSOR").putExtra("mobi.intuitit.android.hpp.EXTRA_ERROR_MESSAGE", "Cannot find app widget with id: " + intExtra));
            return;
        }
        ComponentName componentName = h.getAppWidgetInfo().provider;
        String a2 = TextUtils.equals(action, "mobi.intuitit.android.hpp.ACTION_SCROLL_WIDGET_START") ? a(context, intent, h) : TextUtils.equals(action, "mobi.intuitit.android.hpp.ACTION_SCROLL_WIDGET_SELECT_ITEM") ? a(intent) : TextUtils.equals(action, "mobi.intuitit.android.hpp.ACTION_SCROLL_WIDGET_CLOSE") ? a(context, intent) : TextUtils.equals(action, "mobi.intuitit.android.hpp.ACTION_SCROLL_WIDGET_CLEAR_IMAGE_CACHE") ? g.a().a(intExtra) : "unknow action";
        if (a2 == null) {
            intent.setComponent(componentName);
            this.a.getContext().sendBroadcast(intent.setAction("mobi.intuitit.android.hpp.ACTION_FINISH"));
            return;
        }
        intent.setComponent(componentName);
        this.a.getContext().sendBroadcast(intent.setAction("mobi.intuitit.android.hpp.ERROR_SCROLL_CURSOR").putExtra("mobi.intuitit.android.hpp.EXTRA_ERROR_MESSAGE", a2));
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
    }

    public final void onScrollStateChanged(AbsListView absListView, int i) {
        this.a.a = i == 0;
    }
}
