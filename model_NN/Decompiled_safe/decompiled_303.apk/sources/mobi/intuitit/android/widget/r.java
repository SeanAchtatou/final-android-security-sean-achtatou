package mobi.intuitit.android.widget;

import android.appwidget.AppWidgetHostView;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.widget.ImageView;
import mobi.intuitit.android.widget.WidgetSpace;

final class r extends BroadcastReceiver {
    final /* synthetic */ WidgetSpace a;

    r(WidgetSpace widgetSpace) {
        this.a = widgetSpace;
    }

    private void a(AppWidgetHostView appWidgetHostView, int i, Intent intent, boolean z) {
        if (appWidgetHostView == null) {
            throw new WidgetSpace.FrameAnimationException("Cannot find queried widget " + intent.getIntExtra("appWidgetId", -1) + " in the current screen.");
        }
        try {
            AnimationDrawable animationDrawable = (AnimationDrawable) ((ImageView) appWidgetHostView.findViewById(i)).getDrawable();
            if (animationDrawable != null) {
                if (z) {
                    animationDrawable.start();
                    this.a.getContext().sendBroadcast(intent.setComponent(appWidgetHostView.getAppWidgetInfo().provider).setAction("mobi.intuitit.android.hpp.NOTIFICATION_FRAME_ANIMATION_STARTED"));
                    return;
                }
                animationDrawable.stop();
                this.a.getContext().sendBroadcast(intent.setComponent(appWidgetHostView.getAppWidgetInfo().provider).setAction("mobi.intuitit.android.hpp.NOTIFICATION_FRAME_ANIMATION_STOPPED"));
            }
        } catch (Exception e) {
            throw new WidgetSpace.FrameAnimationException("Fail to start frame animation on queried ImageView: " + i);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0056, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0090, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0093, code lost:
        r12.setComponent(r1.getAppWidgetInfo().provider);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00ed, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00ee, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x015e, code lost:
        throw new mobi.intuitit.android.widget.WidgetSpace.TweenAnimationException("Cannot load resources");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x015f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0160, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x017d, code lost:
        throw new mobi.intuitit.android.widget.WidgetSpace.TweenAnimationException("Cannot start animation: " + r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0056 A[ExcHandler: FrameAnimationException (e mobi.intuitit.android.widget.WidgetSpace$FrameAnimationException), Splitter:B:9:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0090 A[ExcHandler: TweenAnimationException (e mobi.intuitit.android.widget.WidgetSpace$TweenAnimationException), Splitter:B:9:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ed A[ExcHandler: Exception (r0v3 'e' java.lang.Exception A[CUSTOM_DECLARE]), Splitter:B:6:0x0038] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onReceive(android.content.Context r11, android.content.Intent r12) {
        /*
            r10 = this;
            r9 = 10
            r8 = -1
            java.lang.String r0 = r12.getAction()
            java.lang.String r1 = "AnimationProvider"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r12)
            java.lang.String r2 = r2.toString()
            android.util.Log.i(r1, r2)
            java.lang.String r1 = "appWidgetId"
            int r1 = r12.getIntExtra(r1, r8)
            if (r1 >= 0) goto L_0x002d
            java.lang.String r1 = "mobi.intuitit.android.hpp.EXTRA_APPWIDGET_ID"
            int r1 = r12.getIntExtra(r1, r8)
        L_0x002d:
            if (r1 >= 0) goto L_0x0037
            java.lang.String r0 = "WidgetSpace"
            java.lang.String r1 = "Scroll Provider cannot get a legal widget id"
            android.util.Log.e(r0, r1)
        L_0x0036:
            return
        L_0x0037:
            r2 = 0
            mobi.intuitit.android.widget.WidgetSpace r3 = r10.a     // Catch:{ FrameAnimationException -> 0x0182, TweenAnimationException -> 0x017e, Exception -> 0x00ed }
            mobi.intuitit.android.widget.WidgetSpace r4 = r10.a     // Catch:{ FrameAnimationException -> 0x0182, TweenAnimationException -> 0x017e, Exception -> 0x00ed }
            int r4 = r4.b     // Catch:{ FrameAnimationException -> 0x0182, TweenAnimationException -> 0x017e, Exception -> 0x00ed }
            android.appwidget.AppWidgetHostView r1 = r3.a(r4, r1)     // Catch:{ FrameAnimationException -> 0x0182, TweenAnimationException -> 0x017e, Exception -> 0x00ed }
            java.lang.String r2 = "mobi.intuitit.android.hpp.ACTION_START_FRAME_ANIMATION"
            boolean r2 = r2.equals(r0)     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            if (r2 == 0) goto L_0x007c
            java.lang.String r0 = "mobi.intuitit.android.hpp.EXTRA_IMAGEVIEW_ID"
            r2 = 0
            int r0 = r12.getIntExtra(r0, r2)     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            r2 = 1
            r10.a(r1, r0, r12, r2)     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            goto L_0x0036
        L_0x0056:
            r0 = move-exception
        L_0x0057:
            if (r1 == 0) goto L_0x0062
            android.appwidget.AppWidgetProviderInfo r1 = r1.getAppWidgetInfo()
            android.content.ComponentName r1 = r1.provider
            r12.setComponent(r1)
        L_0x0062:
            mobi.intuitit.android.widget.WidgetSpace r1 = r10.a
            android.content.Context r1 = r1.getContext()
            java.lang.String r2 = r0.a
            android.content.Intent r2 = r12.setAction(r2)
            java.lang.String r3 = "mobi.intuitit.android.hpp.EXTRA_ERROR_MESSAGE"
            java.lang.String r0 = r0.getMessage()
            android.content.Intent r0 = r2.putExtra(r3, r0)
            r1.sendBroadcast(r0)
            goto L_0x0036
        L_0x007c:
            java.lang.String r2 = "mobi.intuitit.android.hpp.ACTION_STOP_FRAME_ANIMATION"
            boolean r2 = r2.equals(r0)     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            if (r2 == 0) goto L_0x00b6
            java.lang.String r0 = "mobi.intuitit.android.hpp.EXTRA_IMAGEVIEW_ID"
            r2 = 0
            int r0 = r12.getIntExtra(r0, r2)     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            r2 = 0
            r10.a(r1, r0, r12, r2)     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            goto L_0x0036
        L_0x0090:
            r0 = move-exception
        L_0x0091:
            if (r1 == 0) goto L_0x009c
            android.appwidget.AppWidgetProviderInfo r1 = r1.getAppWidgetInfo()
            android.content.ComponentName r1 = r1.provider
            r12.setComponent(r1)
        L_0x009c:
            mobi.intuitit.android.widget.WidgetSpace r1 = r10.a
            android.content.Context r1 = r1.getContext()
            java.lang.String r2 = r0.a
            android.content.Intent r2 = r12.setAction(r2)
            java.lang.String r3 = "mobi.intuitit.android.hpp.EXTRA_ERROR_MESSAGE"
            java.lang.String r0 = r0.getMessage()
            android.content.Intent r0 = r2.putExtra(r3, r0)
            r1.sendBroadcast(r0)
            goto L_0x0036
        L_0x00b6:
            java.lang.String r2 = "mobi.intuitit.android.hpp.ACTION_START_TWEEN_ANIMATION"
            boolean r0 = r2.equals(r0)     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            if (r0 == 0) goto L_0x0036
            java.lang.String r0 = "mobi.intuitit.android.hpp.EXTRA_VIEW_ID"
            r2 = 0
            int r0 = r12.getIntExtra(r0, r2)     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            if (r1 != 0) goto L_0x00f3
            java.lang.NullPointerException r0 = new java.lang.NullPointerException     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            r2.<init>()     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            java.lang.String r3 = "Cannot find queried widget "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            java.lang.String r3 = "appWidgetId"
            r4 = -1
            int r3 = r12.getIntExtra(r3, r4)     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            java.lang.String r3 = " in the current screen."
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            java.lang.String r2 = r2.toString()     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            r0.<init>(r2)     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            throw r0     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
        L_0x00ed:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0036
        L_0x00f3:
            java.lang.String r2 = "mobi.intuitit.android.hpp.EXTRA_ANIMATION_ID"
            r3 = -1
            int r2 = r12.getIntExtra(r2, r3)     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            mobi.intuitit.android.widget.WidgetSpace r3 = r10.a     // Catch:{ NameNotFoundException -> 0x0154, Exception -> 0x015f, FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090 }
            android.content.Context r3 = r3.getContext()     // Catch:{ NameNotFoundException -> 0x0154, Exception -> 0x015f, FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090 }
            android.appwidget.AppWidgetProviderInfo r4 = r1.getAppWidgetInfo()     // Catch:{ NameNotFoundException -> 0x0154, Exception -> 0x015f, FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090 }
            android.content.ComponentName r4 = r4.provider     // Catch:{ NameNotFoundException -> 0x0154, Exception -> 0x015f, FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090 }
            java.lang.String r4 = r4.getPackageName()     // Catch:{ NameNotFoundException -> 0x0154, Exception -> 0x015f, FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090 }
            r5 = 2
            android.content.Context r3 = r3.createPackageContext(r4, r5)     // Catch:{ NameNotFoundException -> 0x0154, Exception -> 0x015f, FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090 }
            android.view.animation.Animation r3 = android.view.animation.AnimationUtils.loadAnimation(r3, r2)     // Catch:{ NameNotFoundException -> 0x0154, Exception -> 0x015f, FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090 }
            mobi.intuitit.android.widget.n r4 = new mobi.intuitit.android.widget.n     // Catch:{ NameNotFoundException -> 0x0154, Exception -> 0x015f, FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090 }
            android.appwidget.AppWidgetProviderInfo r5 = r1.getAppWidgetInfo()     // Catch:{ NameNotFoundException -> 0x0154, Exception -> 0x015f, FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090 }
            android.content.ComponentName r5 = r5.provider     // Catch:{ NameNotFoundException -> 0x0154, Exception -> 0x015f, FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090 }
            android.content.Intent r5 = r12.setComponent(r5)     // Catch:{ NameNotFoundException -> 0x0154, Exception -> 0x015f, FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090 }
            r4.<init>(r10, r5)     // Catch:{ NameNotFoundException -> 0x0154, Exception -> 0x015f, FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090 }
            r3.setAnimationListener(r4)     // Catch:{ NameNotFoundException -> 0x0154, Exception -> 0x015f, FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090 }
            java.lang.String r4 = "mobi.intuitit.android.hpp.EXTRA_ANIMATION_STARTTIME"
            r5 = -1
            long r4 = r12.getLongExtra(r4, r5)     // Catch:{ NameNotFoundException -> 0x0154, Exception -> 0x015f, FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090 }
            r6 = 0
            int r6 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r6 <= 0) goto L_0x0136
            r3.setStartTime(r4)     // Catch:{ NameNotFoundException -> 0x0154, Exception -> 0x015f, FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090 }
        L_0x0136:
            int r4 = r3.getRepeatCount()     // Catch:{ NameNotFoundException -> 0x0154, Exception -> 0x015f, FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090 }
            if (r4 != r8) goto L_0x0140
            r4 = 0
            r3.setRepeatCount(r4)     // Catch:{ NameNotFoundException -> 0x0154, Exception -> 0x015f, FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090 }
        L_0x0140:
            int r4 = r3.getRepeatCount()     // Catch:{ NameNotFoundException -> 0x0154, Exception -> 0x015f, FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090 }
            if (r4 <= r9) goto L_0x014b
            r4 = 10
            r3.setRepeatCount(r4)     // Catch:{ NameNotFoundException -> 0x0154, Exception -> 0x015f, FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090 }
        L_0x014b:
            android.view.View r0 = r1.findViewById(r0)     // Catch:{ NameNotFoundException -> 0x0154, Exception -> 0x015f, FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090 }
            r0.startAnimation(r3)     // Catch:{ NameNotFoundException -> 0x0154, Exception -> 0x015f, FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090 }
            goto L_0x0036
        L_0x0154:
            r0 = move-exception
            mobi.intuitit.android.widget.WidgetSpace$TweenAnimationException r0 = new mobi.intuitit.android.widget.WidgetSpace$TweenAnimationException     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            mobi.intuitit.android.widget.WidgetSpace r2 = r10.a     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            java.lang.String r3 = "Cannot load resources"
            r0.<init>(r3)     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            throw r0     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
        L_0x015f:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            mobi.intuitit.android.widget.WidgetSpace$TweenAnimationException r0 = new mobi.intuitit.android.widget.WidgetSpace$TweenAnimationException     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            mobi.intuitit.android.widget.WidgetSpace r3 = r10.a     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            r4.<init>()     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            java.lang.String r5 = "Cannot start animation: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            java.lang.String r2 = r2.toString()     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            r0.<init>(r2)     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
            throw r0     // Catch:{ FrameAnimationException -> 0x0056, TweenAnimationException -> 0x0090, Exception -> 0x00ed }
        L_0x017e:
            r0 = move-exception
            r1 = r2
            goto L_0x0091
        L_0x0182:
            r0 = move-exception
            r1 = r2
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: mobi.intuitit.android.widget.r.onReceive(android.content.Context, android.content.Intent):void");
    }
}
