package mobi.intuitit.android.widget;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.database.Cursor;
import android.util.Log;

final class h extends AsyncQueryHandler {
    private /* synthetic */ u a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h(u uVar, ContentResolver contentResolver) {
        super(contentResolver);
        this.a = uVar;
    }

    /* access modifiers changed from: protected */
    public final void onQueryComplete(int i, Object obj, Cursor cursor) {
        super.onQueryComplete(i, obj, cursor);
        Log.d("LAUNCHER", "API v2 QUERY COMPLETE");
        this.a.a.a(cursor, this.a.b);
        cursor.close();
        this.a.notifyDataSetInvalidated();
    }
}
