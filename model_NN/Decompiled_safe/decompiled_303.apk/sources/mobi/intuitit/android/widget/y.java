package mobi.intuitit.android.widget;

import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Rect;
import android.util.Log;
import android.view.View;
import mobi.intuitit.android.widget.BoundRemoteViews;

final class y implements View.OnClickListener {
    private final int a;
    private /* synthetic */ BoundRemoteViews.SetBoundOnClickIntent b;

    public y(BoundRemoteViews.SetBoundOnClickIntent setBoundOnClickIntent, int i) {
        this.b = setBoundOnClickIntent;
        this.a = i;
    }

    public final void onClick(View view) {
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        Rect rect = new Rect();
        rect.left = iArr[0];
        rect.top = iArr[1];
        rect.right = rect.left + view.getWidth();
        rect.bottom = rect.top + view.getHeight();
        Intent intent = new Intent();
        intent.setSourceBounds(rect);
        intent.putExtra(this.b.b, (String) BoundRemoteViews.this.b.a(this.a, this.b));
        try {
            this.b.e.send(view.getContext(), 0, intent, null, null);
        } catch (PendingIntent.CanceledException e) {
            Log.e("SetOnClickPendingIntent", "Cannot send pending intent: ", e);
        }
    }
}
