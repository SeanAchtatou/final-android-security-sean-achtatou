package mobi.intuitit.android.widget;

import android.os.Parcel;
import android.os.Parcelable;

final class j implements Parcelable.Creator {
    j() {
    }

    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new SimpleRemoteViews(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new SimpleRemoteViews[i];
    }
}
