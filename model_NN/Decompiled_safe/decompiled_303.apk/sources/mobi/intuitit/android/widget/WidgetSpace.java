package mobi.intuitit.android.widget;

import android.appwidget.AppWidgetHostView;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import java.util.HashMap;

public abstract class WidgetSpace extends ViewGroup {
    static HashMap c = new HashMap();
    protected boolean a;
    protected int b;
    private BroadcastReceiver d = new r(this);
    private q e = new q(this);

    class AnimationException extends Exception {
        public String a;

        AnimationException(String str, String str2) {
            super(str2);
            this.a = str;
        }
    }

    class FrameAnimationException extends AnimationException {
        public FrameAnimationException(String str) {
            super("mobi.intuitit.android.hpp.ERROR_FRAME_ANIMATION", str);
        }
    }

    class TweenAnimationException extends AnimationException {
        public TweenAnimationException(String str) {
            super("mobi.intuitit.android.hpp.ERROR_TWEEN_ANIMATION", str);
        }
    }

    public WidgetSpace(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public WidgetSpace(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public final void D() {
        Context context = getContext();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("mobi.intuitit.android.hpp.ACTION_START_FRAME_ANIMATION");
        intentFilter.addAction("mobi.intuitit.android.hpp.ACTION_STOP_FRAME_ANIMATION");
        intentFilter.addAction("mobi.intuitit.android.hpp.ACTION_START_TWEEN_ANIMATION");
        context.registerReceiver(this.d, intentFilter);
        IntentFilter intentFilter2 = new IntentFilter();
        intentFilter2.addAction("mobi.intuitit.android.hpp.ACTION_SCROLL_WIDGET_START");
        intentFilter2.addAction("mobi.intuitit.android.hpp.ACTION_SCROLL_WIDGET_CLOSE");
        intentFilter2.addAction("mobi.intuitit.android.hpp.ACTION_SCROLL_WIDGET_CLEAR_IMAGE_CACHE");
        intentFilter2.addAction("mobi.intuitit.android.hpp.ACTION_SCROLL_WIDGET_SELECT_ITEM");
        context.registerReceiver(this.e, intentFilter2);
    }

    public final void E() {
        Context context = getContext();
        try {
            context.unregisterReceiver(this.d);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        try {
            context.unregisterReceiver(this.e);
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    /* access modifiers changed from: package-private */
    public final AppWidgetHostView a(int i, int i2) {
        if (i2 < 0) {
            return null;
        }
        View childAt = getChildAt(i);
        if (childAt == null) {
            return null;
        }
        if (childAt instanceof AppWidgetHostView) {
            AppWidgetHostView appWidgetHostView = (AppWidgetHostView) childAt;
            if (appWidgetHostView.getAppWidgetId() == i2) {
                return appWidgetHostView;
            }
        } else if (childAt instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) getChildAt(i);
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt2 = viewGroup.getChildAt(childCount);
                if ((childAt2 instanceof AppWidgetHostView) && ((AppWidgetHostView) childAt2).getAppWidgetId() == i2) {
                    return (AppWidgetHostView) childAt2;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final AppWidgetHostView h(int i) {
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            AppWidgetHostView a2 = a(childCount, i);
            if (a2 != null) {
                return a2;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
    }
}
