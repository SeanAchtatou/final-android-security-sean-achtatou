package mobi.intuitit.android.widget;

import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.tencent.module.setting.DesktopSwitchSpecialEffectSettingActivity;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class SimpleRemoteViews implements Parcelable {
    public static final Parcelable.Creator CREATOR = new j();
    protected ArrayList a;
    private int b;

    public abstract class Action implements Parcelable {
        protected Action() {
        }

        public abstract void a(View view);

        public int describeContents() {
            return 0;
        }
    }

    public class ActionException extends RuntimeException {
        public ActionException(Exception exc) {
            super(exc);
        }

        public ActionException(String str) {
            super(str);
        }
    }

    public class ReflectionAction extends Action {
        int a;
        private int b;
        private String c;
        private Object d;

        ReflectionAction(Parcel parcel) {
            this.b = parcel.readInt();
            this.c = parcel.readString();
            this.a = parcel.readInt();
            a(parcel);
        }

        private Class c() {
            switch (this.a) {
                case 1:
                    return Boolean.TYPE;
                case 2:
                    return Byte.TYPE;
                case 3:
                    return Short.TYPE;
                case 4:
                    return Integer.TYPE;
                case 5:
                    return Long.TYPE;
                case 6:
                    return Float.TYPE;
                case DesktopSwitchSpecialEffectSettingActivity.nRotationSetting:
                    return Double.TYPE;
                case DesktopSwitchSpecialEffectSettingActivity.nCubeSetting:
                    return Character.TYPE;
                case 9:
                    return String.class;
                case 10:
                    return CharSequence.class;
                case 11:
                    return Uri.class;
                case 12:
                    return Bitmap.class;
                case 13:
                    return Bundle.class;
                default:
                    return null;
            }
        }

        /* access modifiers changed from: protected */
        public int a() {
            return 2;
        }

        /* access modifiers changed from: protected */
        public void a(Parcel parcel) {
            switch (this.a) {
                case 1:
                    this.d = Boolean.valueOf(parcel.readInt() != 0);
                    return;
                case 2:
                    this.d = Byte.valueOf(parcel.readByte());
                    return;
                case 3:
                    this.d = Short.valueOf((short) parcel.readInt());
                    return;
                case 4:
                    this.d = Integer.valueOf(parcel.readInt());
                    return;
                case 5:
                    this.d = Long.valueOf(parcel.readLong());
                    return;
                case 6:
                    this.d = Float.valueOf(parcel.readFloat());
                    return;
                case DesktopSwitchSpecialEffectSettingActivity.nRotationSetting:
                    this.d = Double.valueOf(parcel.readDouble());
                    return;
                case DesktopSwitchSpecialEffectSettingActivity.nCubeSetting:
                    this.d = Character.valueOf((char) parcel.readInt());
                    return;
                case 9:
                    this.d = parcel.readString();
                    return;
                case 10:
                    this.d = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
                    return;
                case 11:
                    this.d = Uri.CREATOR.createFromParcel(parcel);
                    return;
                case 12:
                    this.d = Bitmap.CREATOR.createFromParcel(parcel);
                    return;
                case 13:
                    this.d = parcel.readBundle();
                    return;
                default:
                    return;
            }
        }

        /* access modifiers changed from: protected */
        public void a(Parcel parcel, int i) {
            switch (this.a) {
                case 1:
                    parcel.writeInt(((Boolean) this.d).booleanValue() ? 1 : 0);
                    return;
                case 2:
                    parcel.writeByte(((Byte) this.d).byteValue());
                    return;
                case 3:
                    parcel.writeInt(((Short) this.d).shortValue());
                    return;
                case 4:
                    parcel.writeInt(((Integer) this.d).intValue());
                    return;
                case 5:
                    parcel.writeLong(((Long) this.d).longValue());
                    return;
                case 6:
                    parcel.writeFloat(((Float) this.d).floatValue());
                    return;
                case DesktopSwitchSpecialEffectSettingActivity.nRotationSetting:
                    parcel.writeDouble(((Double) this.d).doubleValue());
                    return;
                case DesktopSwitchSpecialEffectSettingActivity.nCubeSetting:
                    parcel.writeInt(((Character) this.d).charValue());
                    return;
                case 9:
                    parcel.writeString((String) this.d);
                    return;
                case 10:
                    TextUtils.writeToParcel((CharSequence) this.d, parcel, i);
                    return;
                case 11:
                    ((Uri) this.d).writeToParcel(parcel, i);
                    return;
                case 12:
                    ((Bitmap) this.d).writeToParcel(parcel, i);
                    return;
                case 13:
                    parcel.writeBundle((Bundle) this.d);
                    return;
                default:
                    return;
            }
        }

        public void a(View view) {
            View findViewById = view.findViewById(this.b);
            if (findViewById == null) {
                throw new ActionException("can't find view: 0x" + Integer.toHexString(this.b));
            }
            Class c2 = c();
            if (c2 == null) {
                throw new ActionException("bad type: " + this.a);
            }
            Class<?> cls = findViewById.getClass();
            try {
                Method method = cls.getMethod(this.c, c());
                try {
                    view.getContext();
                    method.invoke(findViewById, b());
                } catch (Exception e2) {
                    throw new ActionException(e2);
                }
            } catch (NoSuchMethodException e3) {
                throw new ActionException("view: " + cls.getName() + " doesn't have method: " + this.c + "(" + c2.getName() + ")");
            }
        }

        /* access modifiers changed from: protected */
        public Object b() {
            return this.d;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(a());
            parcel.writeInt(this.b);
            parcel.writeString(this.c);
            parcel.writeInt(this.a);
            a(parcel, i);
        }
    }

    class SetDrawableParameters extends Action {
        private int a;
        private boolean b;
        private int c;
        private int d;
        private PorterDuff.Mode e;
        private int f;

        public SetDrawableParameters(Parcel parcel) {
            this.a = parcel.readInt();
            this.b = parcel.readInt() != 0;
            this.c = parcel.readInt();
            this.d = parcel.readInt();
            if (parcel.readInt() != 0) {
                this.e = PorterDuff.Mode.valueOf(parcel.readString());
            } else {
                this.e = null;
            }
            this.f = parcel.readInt();
        }

        public final void a(View view) {
            View findViewById = view.findViewById(this.a);
            if (findViewById != null) {
                Drawable background = this.b ? findViewById.getBackground() : findViewById instanceof ImageView ? ((ImageView) findViewById).getDrawable() : null;
                if (background != null) {
                    if (this.c != -1) {
                        background.setAlpha(this.c);
                    }
                    if (!(this.d == -1 || this.e == null)) {
                        background.setColorFilter(this.d, this.e);
                    }
                    if (this.f != -1) {
                        background.setLevel(this.f);
                    }
                }
            }
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(3);
            parcel.writeInt(this.a);
            parcel.writeInt(this.b ? 1 : 0);
            parcel.writeInt(this.c);
            parcel.writeInt(this.d);
            if (this.e != null) {
                parcel.writeInt(1);
                parcel.writeString(this.e.toString());
            } else {
                parcel.writeInt(0);
            }
            parcel.writeInt(this.f);
        }
    }

    class SetLayoutSize extends Action {
        private int a;
        private int b;
        private int c;

        public SetLayoutSize(Parcel parcel) {
            this.b = parcel.readInt();
            this.a = parcel.readInt();
            this.c = parcel.readInt();
        }

        public final void a(View view) {
            View findViewById = view.findViewById(this.b);
            if (findViewById != null) {
                ViewGroup.LayoutParams layoutParams = findViewById.getLayoutParams();
                if (this.a == 1) {
                    layoutParams.width = this.c;
                } else {
                    layoutParams.height = this.c;
                }
                findViewById.setLayoutParams(layoutParams);
            }
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(5);
            parcel.writeInt(this.b);
            parcel.writeInt(this.a);
            parcel.writeInt(this.c);
        }
    }

    public class SetOnClickPendingIntent extends Action {
        PendingIntent a;
        private int b;

        public SetOnClickPendingIntent(Parcel parcel) {
            this.b = parcel.readInt();
            this.a = PendingIntent.readPendingIntentOrNullFromParcel(parcel);
        }

        public final void a(View view) {
            View findViewById = view.findViewById(this.b);
            if (findViewById != null && this.a != null) {
                findViewById.setOnClickListener(new o(this));
            }
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(1);
            parcel.writeInt(this.b);
            this.a.writeToParcel(parcel, 0);
        }
    }

    public SimpleRemoteViews(Parcel parcel) {
        this.b = parcel.readInt();
        int readInt = parcel.readInt();
        if (readInt > 0) {
            this.a = new ArrayList(readInt);
            int i = 0;
            while (i < readInt) {
                int readInt2 = parcel.readInt();
                Action a2 = a(readInt2, parcel);
                if (a2 != null) {
                    this.a.add(a2);
                    i++;
                } else {
                    throw new ActionException("Tag " + readInt2 + " not found");
                }
            }
        }
    }

    public final View a(Context context) {
        View inflate = LayoutInflater.from(context).inflate(this.b, (ViewGroup) null);
        try {
            if (this.a != null) {
                int size = this.a.size();
                for (int i = 0; i < size; i++) {
                    ((Action) this.a.get(i)).a(inflate);
                }
            }
        } catch (OutOfMemoryError e) {
            System.gc();
        }
        return inflate;
    }

    /* access modifiers changed from: protected */
    public Action a(int i, Parcel parcel) {
        switch (i) {
            case 1:
                return new SetOnClickPendingIntent(parcel);
            case 2:
                return new ReflectionAction(parcel);
            case 3:
                return new SetDrawableParameters(parcel);
            case 4:
            default:
                return null;
            case 5:
                return new SetLayoutSize(parcel);
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.b);
        int size = this.a != null ? this.a.size() : 0;
        parcel.writeInt(size);
        for (int i2 = 0; i2 < size; i2++) {
            ((Action) this.a.get(i2)).writeToParcel(parcel, 0);
        }
    }
}
