package mobi.intuitit.android.widget;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import mobi.intuitit.android.widget.BoundRemoteViews;
import mobi.intuitit.android.widget.SimpleRemoteViews;

final class s {
    final ArrayList a;
    private HashMap b;
    private /* synthetic */ BoundRemoteViews c;

    public s(BoundRemoteViews boundRemoteViews, Cursor cursor, Context context) {
        this.c = boundRemoteViews;
        this.a = new ArrayList(cursor != null ? cursor.getCount() : 0);
        this.b = new HashMap();
        ArrayList arrayList = boundRemoteViews.a;
        for (int i = 0; i < arrayList.size(); i++) {
            SimpleRemoteViews.Action action = (SimpleRemoteViews.Action) arrayList.get(i);
            if (action instanceof BoundRemoteViews.BindingAction) {
                this.b.put(action, ((BoundRemoteViews.BindingAction) action).a(context));
            } else if (action instanceof BoundRemoteViews.SetBoundOnClickIntent) {
                this.b.put(action, null);
            }
        }
        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                HashMap hashMap = new HashMap();
                for (int i2 = 0; i2 < arrayList.size(); i2++) {
                    SimpleRemoteViews.Action action2 = (SimpleRemoteViews.Action) arrayList.get(i2);
                    if (action2 instanceof BoundRemoteViews.BindingAction) {
                        hashMap.put(action2, ((BoundRemoteViews.BindingAction) action2).a(cursor));
                    } else if (action2 instanceof BoundRemoteViews.SetBoundOnClickIntent) {
                        hashMap.put(action2, ((BoundRemoteViews.SetBoundOnClickIntent) action2).a(cursor));
                    }
                }
                this.a.add(hashMap);
                cursor.moveToNext();
            }
        }
    }

    private static void a(HashMap hashMap) {
        for (SimpleRemoteViews.Action action : hashMap.keySet()) {
            Object obj = hashMap.get(action);
            if (obj instanceof Bitmap) {
                ((Bitmap) obj).recycle();
            }
        }
        hashMap.clear();
    }

    public final Object a(int i, SimpleRemoteViews.Action action) {
        HashMap hashMap = (HashMap) this.a.get(i);
        Object obj = hashMap.containsKey(action) ? hashMap.get(action) : null;
        return obj == null ? this.b.get(action) : obj;
    }

    public final void a() {
        Iterator it = this.a.iterator();
        while (it.hasNext()) {
            a((HashMap) it.next());
        }
        this.a.clear();
        a(this.b);
    }
}
