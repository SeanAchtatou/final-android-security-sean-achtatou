package mobi.intuitit.android.widget;

import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.view.View;
import android.widget.AdapterView;

final class t implements AdapterView.OnItemClickListener {
    private ComponentName a;
    private int b;
    private int c;
    private /* synthetic */ q d;

    t(q qVar, ComponentName componentName, int i, int i2) {
        this.d = qVar;
        this.a = componentName;
        this.b = i;
        this.c = i2;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        try {
            Object obj = ((p) view.getTag()).b;
            if (obj == null || !(obj instanceof String)) {
                Intent intent = new Intent("mobi.intuitit.android.hpp.ACTION_ITEM_CLICK");
                intent.setComponent(this.a);
                intent.putExtra("appWidgetId", this.b).putExtra("mobi.intuitit.android.hpp.EXTRA_APPWIDGET_ID", this.b);
                intent.putExtra("mobi.intuitit.android.hpp.EXTRA_LISTVIEW_ID", this.c);
                intent.putExtra("mobi.intuitit.android.hpp.EXTRA_ITEM_POS", i);
                Rect rect = new Rect();
                int[] iArr = new int[2];
                view.getLocationOnScreen(iArr);
                rect.left = iArr[0];
                rect.top = iArr[1];
                rect.right = rect.left + view.getWidth();
                rect.bottom = rect.top + view.getHeight();
                intent.putExtra("mobi.intuitit.android.hpp.EXTRA_ITEM_SOURCE_BOUNDS", rect);
                this.d.a.getContext().sendBroadcast(intent);
                return;
            }
            this.d.a.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse((String) obj)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
