package mobi.intuitit.android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;

public abstract class WidgetCellLayout extends ViewGroup {
    public WidgetCellLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public WidgetCellLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
    }
}
