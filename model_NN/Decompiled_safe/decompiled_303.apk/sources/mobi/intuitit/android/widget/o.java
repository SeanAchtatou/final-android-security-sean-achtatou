package mobi.intuitit.android.widget;

import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Rect;
import android.util.Log;
import android.view.View;
import mobi.intuitit.android.widget.SimpleRemoteViews;

final class o implements View.OnClickListener {
    private /* synthetic */ SimpleRemoteViews.SetOnClickPendingIntent a;

    o(SimpleRemoteViews.SetOnClickPendingIntent setOnClickPendingIntent) {
        this.a = setOnClickPendingIntent;
    }

    public final void onClick(View view) {
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        Rect rect = new Rect();
        rect.left = iArr[0];
        rect.top = iArr[1];
        rect.right = rect.left + view.getWidth();
        rect.bottom = rect.top + view.getHeight();
        Intent intent = new Intent();
        intent.setSourceBounds(rect);
        try {
            this.a.a.send(view.getContext(), 0, intent, null, null);
        } catch (PendingIntent.CanceledException e) {
            Log.e("SetOnClickPendingIntent", "Cannot send pending intent: ", e);
        }
    }
}
