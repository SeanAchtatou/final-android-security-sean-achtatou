package mobi.intuitit.android.widget;

import android.content.Intent;
import android.view.animation.Animation;

final class n implements Animation.AnimationListener {
    private Intent a;
    private /* synthetic */ r b;

    n(r rVar, Intent intent) {
        this.b = rVar;
        this.a = intent;
    }

    public final void onAnimationEnd(Animation animation) {
        this.b.a.getContext().sendBroadcast(this.a.setAction("mobi.intuitit.android.hpp.NOTIFICATION_TWEEN_ANIMATION_ENDED"));
        this.a = null;
        animation.setAnimationListener(null);
    }

    public final void onAnimationRepeat(Animation animation) {
        this.b.a.getContext().sendBroadcast(this.a.setAction("mobi.intuitit.android.hpp.NOTIFICATION_TWEEN_ANIMATION_REPEATED"));
    }

    public final void onAnimationStart(Animation animation) {
        this.b.a.getContext().sendBroadcast(this.a.setAction("mobi.intuitit.android.hpp.NOTIFICATION_TWEEN_ANIMATION_STARTED"));
    }
}
