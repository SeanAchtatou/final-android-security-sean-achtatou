package mobi.intuitit.android.widget;

import android.content.Intent;
import android.graphics.Rect;
import android.view.View;

final class l implements View.OnClickListener {
    private /* synthetic */ d a;

    l(d dVar) {
        this.a = dVar;
    }

    public final void onClick(View view) {
        try {
            Intent intent = new Intent("mobi.intuitit.android.hpp.ACTION_VIEW_CLICK");
            intent.setComponent(this.a.g);
            intent.putExtra("appWidgetId", this.a.a).putExtra("mobi.intuitit.android.hpp.EXTRA_APPWIDGET_ID", this.a.a);
            intent.putExtra("mobi.intuitit.android.hpp.EXTRA_VIEW_ID", view.getId());
            intent.putExtra("mobi.intuitit.android.hpp.EXTRA_LISTVIEW_ID", this.a.b);
            intent.putExtra("mobi.intuitit.android.hpp.EXTRA_ITEM_POS", (String) view.getTag());
            Rect rect = new Rect();
            int[] iArr = new int[2];
            view.getLocationOnScreen(iArr);
            rect.left = iArr[0];
            rect.top = iArr[1];
            rect.right = rect.left + view.getWidth();
            rect.bottom = rect.top + view.getHeight();
            intent.putExtra("mobi.intuitit.android.hpp.EXTRA_ITEM_SOURCE_BOUNDS", rect);
            view.getContext().sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
