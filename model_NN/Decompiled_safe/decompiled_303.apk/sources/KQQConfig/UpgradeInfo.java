package KQQConfig;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;

public final class UpgradeInfo extends JceStruct {
    private static /* synthetic */ boolean h = (!UpgradeInfo.class.desiredAssertionStatus());
    private int a = 0;
    private byte b = 0;
    private int c = 0;
    private int d = 0;
    private String e = "";
    private String f = "";
    private String g = "";

    public final int a() {
        return this.c;
    }

    public final String b() {
        return this.g;
    }

    public final Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e2) {
            if (h) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public final void display(StringBuilder sb, int i) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i);
        jceDisplayer.display(this.a, "iAppid");
        jceDisplayer.display(this.b, "bAppType");
        jceDisplayer.display(this.c, "iUpgradeType");
        jceDisplayer.display(this.d, "iUpgradeSdkId");
        jceDisplayer.display(this.e, "strTitle");
        jceDisplayer.display(this.f, "strUpgradeDesc");
        jceDisplayer.display(this.g, "strUrl");
    }

    public final boolean equals(Object obj) {
        UpgradeInfo upgradeInfo = (UpgradeInfo) obj;
        return JceUtil.equals(this.a, upgradeInfo.a) && JceUtil.equals(this.b, upgradeInfo.b) && JceUtil.equals(this.c, upgradeInfo.c) && JceUtil.equals(this.d, upgradeInfo.d) && JceUtil.equals(this.e, upgradeInfo.e) && JceUtil.equals(this.f, upgradeInfo.f) && JceUtil.equals(this.g, upgradeInfo.g);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte */
    public final void readFrom(JceInputStream jceInputStream) {
        this.a = jceInputStream.read(this.a, 1, true);
        this.b = jceInputStream.read(this.b, 2, true);
        this.c = jceInputStream.read(this.c, 3, true);
        this.d = jceInputStream.read(this.d, 4, true);
        this.e = jceInputStream.readString(5, false);
        this.f = jceInputStream.readString(6, false);
        this.g = jceInputStream.readString(7, false);
    }

    public final void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.a, 1);
        jceOutputStream.write(this.b, 2);
        jceOutputStream.write(this.c, 3);
        jceOutputStream.write(this.d, 4);
        if (this.e != null) {
            jceOutputStream.write(this.e, 5);
        }
        if (this.f != null) {
            jceOutputStream.write(this.f, 6);
        }
        if (this.g != null) {
            jceOutputStream.write(this.g, 7);
        }
    }
}
