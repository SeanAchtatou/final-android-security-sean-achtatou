package AndroidDLoader;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;

public final class Comment extends JceStruct {
    private static /* synthetic */ boolean l = (!Comment.class.desiredAssertionStatus());
    public String a = "";
    public String b = "";
    public String c = "";
    public byte d = 0;
    private int e = 0;
    private String f = "";
    private int g = 0;
    private String h = "";
    private byte i = 0;
    private int j = 0;
    private int k = 0;

    public final Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e2) {
            if (l) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public final void display(StringBuilder sb, int i2) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i2);
        jceDisplayer.display(this.e, "nCommentId");
        jceDisplayer.display(this.f, "sTitle");
        jceDisplayer.display(this.a, "sContent");
        jceDisplayer.display(this.b, "sNickname");
        jceDisplayer.display(this.g, "nUserid");
        jceDisplayer.display(this.h, "sCookieId");
        jceDisplayer.display(this.c, "sCreateTime");
        jceDisplayer.display(this.i, "nLevel");
        jceDisplayer.display(this.d, "nScore");
        jceDisplayer.display(this.j, "nSoftId");
        jceDisplayer.display(this.k, "nProductId");
    }

    public final boolean equals(Object obj) {
        Comment comment = (Comment) obj;
        return JceUtil.equals(this.e, comment.e) && JceUtil.equals(this.f, comment.f) && JceUtil.equals(this.a, comment.a) && JceUtil.equals(this.b, comment.b) && JceUtil.equals(this.g, comment.g) && JceUtil.equals(this.h, comment.h) && JceUtil.equals(this.c, comment.c) && JceUtil.equals(this.i, comment.i) && JceUtil.equals(this.d, comment.d) && JceUtil.equals(this.j, comment.j) && JceUtil.equals(this.k, comment.k);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte */
    public final void readFrom(JceInputStream jceInputStream) {
        this.e = jceInputStream.read(this.e, 0, true);
        this.f = jceInputStream.readString(1, true);
        this.a = jceInputStream.readString(2, true);
        this.b = jceInputStream.readString(3, true);
        this.g = jceInputStream.read(this.g, 4, true);
        this.h = jceInputStream.readString(5, true);
        this.c = jceInputStream.readString(6, true);
        this.i = jceInputStream.read(this.i, 7, true);
        this.d = jceInputStream.read(this.d, 8, true);
        this.j = jceInputStream.read(this.j, 9, true);
        this.k = jceInputStream.read(this.k, 10, true);
    }

    public final void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.e, 0);
        jceOutputStream.write(this.f, 1);
        jceOutputStream.write(this.a, 2);
        jceOutputStream.write(this.b, 3);
        jceOutputStream.write(this.g, 4);
        jceOutputStream.write(this.h, 5);
        jceOutputStream.write(this.c, 6);
        jceOutputStream.write(this.i, 7);
        jceOutputStream.write(this.d, 8);
        jceOutputStream.write(this.j, 9);
        jceOutputStream.write(this.k, 10);
    }
}
