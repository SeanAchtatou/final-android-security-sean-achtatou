package AndroidDLoader;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;

public final class ScrollablePicAdv extends JceStruct {
    private static /* synthetic */ boolean i = (!ScrollablePicAdv.class.desiredAssertionStatus());
    public String a = "";
    public int b = 0;
    public int c = 0;
    public int d = 0;
    public String e = "";
    public String f = "";
    private int g = 0;
    private String h = "";

    public final Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e2) {
            if (i) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public final void display(StringBuilder sb, int i2) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i2);
        jceDisplayer.display(this.g, "advId");
        jceDisplayer.display(this.a, "picUrl");
        jceDisplayer.display(this.h, "picAlt");
        jceDisplayer.display(this.b, "productid");
        jceDisplayer.display(this.c, "softwareid");
        jceDisplayer.display(this.d, "fileid");
        jceDisplayer.display(this.e, "appName");
        jceDisplayer.display(this.f, "appDesc");
    }

    public final boolean equals(Object obj) {
        ScrollablePicAdv scrollablePicAdv = (ScrollablePicAdv) obj;
        return JceUtil.equals(this.g, scrollablePicAdv.g) && JceUtil.equals(this.a, scrollablePicAdv.a) && JceUtil.equals(this.h, scrollablePicAdv.h) && JceUtil.equals(this.b, scrollablePicAdv.b) && JceUtil.equals(this.c, scrollablePicAdv.c) && JceUtil.equals(this.d, scrollablePicAdv.d) && JceUtil.equals(this.e, scrollablePicAdv.e) && JceUtil.equals(this.f, scrollablePicAdv.f);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    public final void readFrom(JceInputStream jceInputStream) {
        this.g = jceInputStream.read(this.g, 0, true);
        this.a = jceInputStream.readString(1, true);
        this.h = jceInputStream.readString(2, true);
        this.b = jceInputStream.read(this.b, 3, true);
        this.c = jceInputStream.read(this.c, 4, true);
        this.d = jceInputStream.read(this.d, 5, true);
        this.e = jceInputStream.readString(6, true);
        this.f = jceInputStream.readString(7, true);
    }

    public final void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.g, 0);
        jceOutputStream.write(this.a, 1);
        jceOutputStream.write(this.h, 2);
        jceOutputStream.write(this.b, 3);
        jceOutputStream.write(this.c, 4);
        jceOutputStream.write(this.d, 5);
        jceOutputStream.write(this.e, 6);
        jceOutputStream.write(this.f, 7);
    }
}
