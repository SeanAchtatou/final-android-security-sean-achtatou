package AndroidDLoader;

public final class a {
    private static a A = new a(21, 21, "Content_Profile_HomePage");
    private static a B = new a(22, 22, "Content_Category_List");
    private static a C = new a(24, 24, "Guest_Profile_Shares");
    private static a D = new a(25, 25, "Guest_Profile_Fav");
    private static a E = new a(26, 26, "AppList_Related_softwares");
    private static a F = new a(27, 27, "Content_Comment_score");
    private static a G = new a(28, 28, "master_Fav_software_list");
    private static a H = new a(29, 29, "Gutest_Fav_software_list");
    private static a I = new a(30, 30, "Content_gutest_Profile_HomePage");
    private static a J = new a(31, 31, "AppList_GameCategory_DownloadCountTopN");
    private static a K = new a(32, 32, "AppList_GameCategory_CommendsSoftwares");
    private static a L = new a(33, 33, "AppList_GameCategory_LatestSoftwares");
    private static /* synthetic */ boolean M = (!a.class.desiredAssertionStatus());
    public static final a a = new a(0, 0, "UNKNOWN");
    public static final a b = new a(1, 1, "AppList_HomePage_HandpickedInEveryday");
    public static final a c = new a(5, 5, "AppList_HomePage_GuessYourLove");
    public static final a d = new a(7, 7, "AppList_HomePage_ScoreTopN");
    public static final a e = new a(8, 8, "AppList_HomePage_DownloadCountTopN");
    public static final a f = new a(18, 18, "AppList_Search_ResultList");
    public static final a g = new a(20, 20, "Content_Software_Details");
    public static final a h = new a(23, 23, "AppList_Required_softwares");
    public static final a i = new a(34, 34, "First_Release");
    private static a[] j = new a[35];
    private static a m = new a(2, 2, "AppList_HomePage_UserCommends");
    private static a n = new a(3, 3, "AppList_HomePage_UserCommendsFav");
    private static a o = new a(4, 4, "AppList_HomePage_UserCommendsShare");
    private static a p = new a(6, 6, "AppList_HomePage_LatestSoftware");
    private static a q = new a(9, 9, "AppList_Category_CommendsSoftwares");
    private static a r = new a(10, 10, "AppList_Category_LatestSoftwares");
    private static a s = new a(11, 11, "AppList_Category_HotSoftwares");
    private static a t = new a(12, 12, "AppList_Topic_TopicDetail");
    private static a u = new a(13, 13, "AppList_Manager_Application");
    private static a v = new a(14, 14, "AppList_Manager_DownloadList");
    private static a w = new a(15, 15, "AppList_Profile_UpdatableApp");
    private static a x = new a(16, 16, "AppList_Profile_Shares");
    private static a y = new a(17, 17, "AppList_Profile_Fav");
    private static a z = new a(19, 19, "Content_Topic_TopicList");
    private int k;
    private String l = new String();

    private a(int i2, int i3, String str) {
        this.l = str;
        this.k = i3;
        j[i2] = this;
    }

    public static a a(int i2) {
        for (int i3 = 0; i3 < j.length; i3++) {
            if (j[i3].k == i2) {
                return j[i3];
            }
        }
        if (M) {
            return null;
        }
        throw new AssertionError();
    }

    public final int a() {
        return this.k;
    }

    public final String toString() {
        return this.l;
    }
}
