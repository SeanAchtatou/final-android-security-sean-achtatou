package AndroidDLoader;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;

public final class DownSoft extends JceStruct {
    private static /* synthetic */ boolean k = (!DownSoft.class.desiredAssertionStatus());
    public int a = 0;
    public int b = 0;
    public int c = 0;
    public byte d = 0;
    public int e = 0;
    public byte f = 0;
    private int g = 0;
    private int h = 0;
    private int i = 0;
    private int j = 0;

    public final Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e2) {
            if (k) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public final void display(StringBuilder sb, int i2) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i2);
        jceDisplayer.display(this.a, "nProductId");
        jceDisplayer.display(this.b, "nSoftId");
        jceDisplayer.display(this.c, "nFileId");
        jceDisplayer.display(this.d, "nSuccess");
        jceDisplayer.display(this.e, "nDownSize");
        jceDisplayer.display(this.g, "nUsedTime");
        jceDisplayer.display(this.h, "nMaxSpeed");
        jceDisplayer.display(this.i, "nAvgSpeed");
        jceDisplayer.display(this.j, "nRetryTimes");
        jceDisplayer.display(this.f, "nDownType");
    }

    public final boolean equals(Object obj) {
        DownSoft downSoft = (DownSoft) obj;
        return JceUtil.equals(this.a, downSoft.a) && JceUtil.equals(this.b, downSoft.b) && JceUtil.equals(this.c, downSoft.c) && JceUtil.equals(this.d, downSoft.d) && JceUtil.equals(this.e, downSoft.e) && JceUtil.equals(this.g, downSoft.g) && JceUtil.equals(this.h, downSoft.h) && JceUtil.equals(this.i, downSoft.i) && JceUtil.equals(this.j, downSoft.j) && JceUtil.equals(this.f, downSoft.f);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte */
    public final void readFrom(JceInputStream jceInputStream) {
        this.a = jceInputStream.read(this.a, 0, true);
        this.b = jceInputStream.read(this.b, 1, true);
        this.c = jceInputStream.read(this.c, 2, true);
        this.d = jceInputStream.read(this.d, 3, true);
        this.e = jceInputStream.read(this.e, 4, true);
        this.g = jceInputStream.read(this.g, 5, true);
        this.h = jceInputStream.read(this.h, 6, true);
        this.i = jceInputStream.read(this.i, 7, true);
        this.j = jceInputStream.read(this.j, 8, true);
        this.f = jceInputStream.read(this.f, 9, true);
    }

    public final void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.a, 0);
        jceOutputStream.write(this.b, 1);
        jceOutputStream.write(this.c, 2);
        jceOutputStream.write(this.d, 3);
        jceOutputStream.write(this.e, 4);
        jceOutputStream.write(this.g, 5);
        jceOutputStream.write(this.h, 6);
        jceOutputStream.write(this.i, 7);
        jceOutputStream.write(this.j, 8);
        jceOutputStream.write(this.f, 9);
    }
}
