package AndroidDLoader;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public final class ResSoftwarePack extends JceStruct {
    private static ArrayList k;
    private static Map l;
    private static Map m;
    private static ArrayList n;
    private static ArrayList o;
    private static /* synthetic */ boolean p = (!ResSoftwarePack.class.desiredAssertionStatus());
    public byte a = 0;
    public int b = 0;
    public ArrayList c = null;
    public ArrayList d = null;
    public String e = "";
    private byte f = 0;
    private int g = 0;
    private Map h = null;
    private Map i = null;
    private ArrayList j = null;

    public final byte a() {
        return this.f;
    }

    public final Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e2) {
            if (p) {
                return null;
            }
            throw new AssertionError();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.display(java.util.Collection, java.lang.String):com.qq.taf.jce.JceDisplayer
     arg types: [java.util.ArrayList, java.lang.String]
     candidates:
      com.qq.taf.jce.JceDisplayer.display(byte, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(com.qq.taf.jce.JceStruct, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.String, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Map, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(boolean, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(byte[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Collection, java.lang.String):com.qq.taf.jce.JceDisplayer */
    public final void display(StringBuilder sb, int i2) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i2);
        jceDisplayer.display(this.f, "nCode");
        jceDisplayer.display(this.g, "nCategoryId");
        jceDisplayer.display(this.a, "nRankType");
        jceDisplayer.display(this.b, "nCount");
        jceDisplayer.display((Collection) this.c, "listSoftItems");
        jceDisplayer.display(this.h, "mapPushUpdate");
        jceDisplayer.display(this.i, "mapNewPushVersion");
        jceDisplayer.display((Collection) this.j, "listAdIconItems");
        jceDisplayer.display((Collection) this.d, "listKeywordItems");
        jceDisplayer.display(this.e, "keyword");
    }

    public final boolean equals(Object obj) {
        ResSoftwarePack resSoftwarePack = (ResSoftwarePack) obj;
        return JceUtil.equals(this.f, resSoftwarePack.f) && JceUtil.equals(this.g, resSoftwarePack.g) && JceUtil.equals(this.a, resSoftwarePack.a) && JceUtil.equals(this.b, resSoftwarePack.b) && JceUtil.equals(this.c, resSoftwarePack.c) && JceUtil.equals(this.h, resSoftwarePack.h) && JceUtil.equals(this.i, resSoftwarePack.i) && JceUtil.equals(this.j, resSoftwarePack.j) && JceUtil.equals(this.d, resSoftwarePack.d) && JceUtil.equals(this.e, resSoftwarePack.e);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.ArrayList, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object */
    public final void readFrom(JceInputStream jceInputStream) {
        this.f = jceInputStream.read(this.f, 0, true);
        this.g = jceInputStream.read(this.g, 1, true);
        this.a = jceInputStream.read(this.a, 2, true);
        this.b = jceInputStream.read(this.b, 3, false);
        if (k == null) {
            k = new ArrayList();
            k.add(new Software());
        }
        this.c = (ArrayList) jceInputStream.read((Object) k, 4, false);
        if (l == null) {
            l = new HashMap();
            l.put((byte) 0, (byte) 0);
        }
        this.h = (Map) jceInputStream.read((Object) l, 5, false);
        if (m == null) {
            m = new HashMap();
            m.put((byte) 0, "");
        }
        this.i = (Map) jceInputStream.read((Object) m, 6, false);
        if (n == null) {
            n = new ArrayList();
            n.add(new AdIcon());
        }
        this.j = (ArrayList) jceInputStream.read((Object) n, 7, false);
        if (o == null) {
            o = new ArrayList();
            o.add("");
        }
        this.d = (ArrayList) jceInputStream.read((Object) o, 8, false);
        this.e = jceInputStream.readString(9, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
     arg types: [java.util.ArrayList, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void */
    public final void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.f, 0);
        jceOutputStream.write(this.g, 1);
        jceOutputStream.write(this.a, 2);
        jceOutputStream.write(this.b, 3);
        if (this.c != null) {
            jceOutputStream.write((Collection) this.c, 4);
        }
        if (this.h != null) {
            jceOutputStream.write(this.h, 5);
        }
        if (this.i != null) {
            jceOutputStream.write(this.i, 6);
        }
        if (this.j != null) {
            jceOutputStream.write((Collection) this.j, 7);
        }
        if (this.d != null) {
            jceOutputStream.write((Collection) this.d, 8);
        }
        if (this.e != null) {
            jceOutputStream.write(this.e, 9);
        }
    }
}
