package AndroidDLoader;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;

public final class Software extends JceStruct {
    private static /* synthetic */ boolean t = (!Software.class.desiredAssertionStatus());
    public int a = 0;
    public String b = "";
    public String c = "";
    public int d = 0;
    public String e = "";
    public String f = "";
    public String g = "";
    public String h = "";
    public int i = 0;
    public int j = 0;
    public String k = "";
    public String l = "";
    public byte m = 0;
    public int n = 0;
    private int o = 0;
    private String p = "";
    private String q = "";
    private String r = "";
    private String s = "";

    public final int a() {
        return this.a;
    }

    public final String b() {
        return this.c;
    }

    public final int c() {
        return this.d;
    }

    public final Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e2) {
            if (t) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public final String d() {
        return this.e;
    }

    public final void display(StringBuilder sb, int i2) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i2);
        jceDisplayer.display(this.a, "nProductId");
        jceDisplayer.display(this.b, "sProductName");
        jceDisplayer.display(this.c, "sLogoUrl");
        jceDisplayer.display(this.d, "nSoftId");
        jceDisplayer.display(this.e, "sSoftName");
        jceDisplayer.display(this.f, "sSoftVersion");
        jceDisplayer.display(this.g, "sFee");
        jceDisplayer.display(this.h, "sPublishTime");
        jceDisplayer.display(this.i, "nFileId");
        jceDisplayer.display(this.j, "nFileSize");
        jceDisplayer.display(this.k, "sFileurl");
        jceDisplayer.display(this.l, "sFileuid");
        jceDisplayer.display(this.m, "nScore");
        jceDisplayer.display(this.o, "nCommentCount");
        jceDisplayer.display(this.n, "nDownloadCount");
        jceDisplayer.display(this.p, "sFunction");
        jceDisplayer.display(this.q, "sPrefix");
        jceDisplayer.display(this.r, "sFeedesc");
        jceDisplayer.display(this.s, "cpname");
    }

    public final String e() {
        return this.f;
    }

    public final boolean equals(Object obj) {
        Software software = (Software) obj;
        return JceUtil.equals(this.a, software.a) && JceUtil.equals(this.b, software.b) && JceUtil.equals(this.c, software.c) && JceUtil.equals(this.d, software.d) && JceUtil.equals(this.e, software.e) && JceUtil.equals(this.f, software.f) && JceUtil.equals(this.g, software.g) && JceUtil.equals(this.h, software.h) && JceUtil.equals(this.i, software.i) && JceUtil.equals(this.j, software.j) && JceUtil.equals(this.k, software.k) && JceUtil.equals(this.l, software.l) && JceUtil.equals(this.m, software.m) && JceUtil.equals(this.o, software.o) && JceUtil.equals(this.n, software.n) && JceUtil.equals(this.p, software.p) && JceUtil.equals(this.q, software.q) && JceUtil.equals(this.r, software.r) && JceUtil.equals(this.s, software.s);
    }

    public final String f() {
        return this.h;
    }

    public final int g() {
        return this.i;
    }

    public final int h() {
        return this.j;
    }

    public final String i() {
        return this.k;
    }

    public final String j() {
        return this.l;
    }

    public final int k() {
        return this.n;
    }

    public final String l() {
        return this.s;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte */
    public final void readFrom(JceInputStream jceInputStream) {
        this.a = jceInputStream.read(this.a, 0, true);
        this.b = jceInputStream.readString(1, true);
        this.c = jceInputStream.readString(2, true);
        this.d = jceInputStream.read(this.d, 3, true);
        this.e = jceInputStream.readString(4, true);
        this.f = jceInputStream.readString(5, true);
        this.g = jceInputStream.readString(6, true);
        this.h = jceInputStream.readString(7, true);
        this.i = jceInputStream.read(this.i, 8, true);
        this.j = jceInputStream.read(this.j, 9, true);
        this.k = jceInputStream.readString(10, true);
        this.l = jceInputStream.readString(11, true);
        this.m = jceInputStream.read(this.m, 12, true);
        this.o = jceInputStream.read(this.o, 13, true);
        this.n = jceInputStream.read(this.n, 14, true);
        this.p = jceInputStream.readString(15, true);
        this.q = jceInputStream.readString(16, true);
        this.r = jceInputStream.readString(17, true);
        this.s = jceInputStream.readString(18, false);
    }

    public final void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.a, 0);
        jceOutputStream.write(this.b, 1);
        jceOutputStream.write(this.c, 2);
        jceOutputStream.write(this.d, 3);
        jceOutputStream.write(this.e, 4);
        jceOutputStream.write(this.f, 5);
        jceOutputStream.write(this.g, 6);
        jceOutputStream.write(this.h, 7);
        jceOutputStream.write(this.i, 8);
        jceOutputStream.write(this.j, 9);
        jceOutputStream.write(this.k, 10);
        jceOutputStream.write(this.l, 11);
        jceOutputStream.write(this.m, 12);
        jceOutputStream.write(this.o, 13);
        jceOutputStream.write(this.n, 14);
        jceOutputStream.write(this.p, 15);
        jceOutputStream.write(this.q, 16);
        jceOutputStream.write(this.r, 17);
        if (this.s != null) {
            jceOutputStream.write(this.s, 18);
        }
    }
}
