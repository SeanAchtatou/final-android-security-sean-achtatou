package AndroidDLoader;

public final class b {
    public static final b a = new b(0, 0, "Install");
    public static final b b = new b(3, 3, "Uninstall");
    public static final b c = new b(5, 5, "Update");
    private static b[] d = new b[6];
    private static b g = new b(1, 1, "Remind_Update");
    private static b h = new b(2, 2, "Unmatch");
    private static b i = new b(4, 4, "Boot");
    private static /* synthetic */ boolean j = (!b.class.desiredAssertionStatus());
    private int e;
    private String f = new String();

    private b(int i2, int i3, String str) {
        this.f = str;
        this.e = i3;
        d[i2] = this;
    }

    public static b a(int i2) {
        for (int i3 = 0; i3 < d.length; i3++) {
            if (d[i3].e == i2) {
                return d[i3];
            }
        }
        if (j) {
            return null;
        }
        throw new AssertionError();
    }

    public final int a() {
        return this.e;
    }

    public final String toString() {
        return this.f;
    }
}
