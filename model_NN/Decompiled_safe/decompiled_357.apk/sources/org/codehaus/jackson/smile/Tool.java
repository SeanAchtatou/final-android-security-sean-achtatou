package org.codehaus.jackson.smile;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.smile.SmileGenerator;
import org.codehaus.jackson.smile.SmileParser;

public class Tool {
    public static final String SUFFIX = ".lzf";
    public final JsonFactory jsonFactory = new JsonFactory();
    public final SmileFactory smileFactory = new SmileFactory();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.smile.SmileFactory.configure(org.codehaus.jackson.smile.SmileGenerator$Feature, boolean):org.codehaus.jackson.smile.SmileFactory
     arg types: [org.codehaus.jackson.smile.SmileGenerator$Feature, int]
     candidates:
      org.codehaus.jackson.smile.SmileFactory.configure(org.codehaus.jackson.smile.SmileParser$Feature, boolean):org.codehaus.jackson.smile.SmileFactory
      org.codehaus.jackson.JsonFactory.configure(org.codehaus.jackson.JsonGenerator$Feature, boolean):org.codehaus.jackson.JsonFactory
      org.codehaus.jackson.JsonFactory.configure(org.codehaus.jackson.JsonParser$Feature, boolean):org.codehaus.jackson.JsonFactory
      org.codehaus.jackson.smile.SmileFactory.configure(org.codehaus.jackson.smile.SmileGenerator$Feature, boolean):org.codehaus.jackson.smile.SmileFactory */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.smile.SmileFactory.configure(org.codehaus.jackson.smile.SmileParser$Feature, boolean):org.codehaus.jackson.smile.SmileFactory
     arg types: [org.codehaus.jackson.smile.SmileParser$Feature, int]
     candidates:
      org.codehaus.jackson.smile.SmileFactory.configure(org.codehaus.jackson.smile.SmileGenerator$Feature, boolean):org.codehaus.jackson.smile.SmileFactory
      org.codehaus.jackson.JsonFactory.configure(org.codehaus.jackson.JsonGenerator$Feature, boolean):org.codehaus.jackson.JsonFactory
      org.codehaus.jackson.JsonFactory.configure(org.codehaus.jackson.JsonParser$Feature, boolean):org.codehaus.jackson.JsonFactory
      org.codehaus.jackson.smile.SmileFactory.configure(org.codehaus.jackson.smile.SmileParser$Feature, boolean):org.codehaus.jackson.smile.SmileFactory */
    public Tool() {
        this.smileFactory.configure(SmileGenerator.Feature.CHECK_SHARED_NAMES, true);
        this.smileFactory.configure(SmileGenerator.Feature.CHECK_SHARED_STRING_VALUES, true);
        this.smileFactory.configure(SmileGenerator.Feature.ENCODE_BINARY_AS_7BIT, true);
        this.smileFactory.configure(SmileGenerator.Feature.WRITE_HEADER, true);
        this.smileFactory.configure(SmileGenerator.Feature.WRITE_END_MARKER, false);
        this.smileFactory.configure(SmileParser.Feature.REQUIRE_HEADER, false);
    }

    private void process(String[] args) throws IOException {
        String oper = null;
        String filename = null;
        if (args.length == 2) {
            oper = args[0];
            filename = args[1];
        } else if (args.length == 1) {
            oper = args[0];
        } else {
            showUsage();
        }
        if ("-e".equals(oper)) {
            encode(inputStream(filename));
        } else if ("-d".equals(oper)) {
            decode(inputStream(filename));
        } else if ("-v".equals(oper)) {
            verify(inputStream(filename), inputStream(filename));
        } else {
            showUsage();
        }
    }

    private InputStream inputStream(String filename) throws IOException {
        if (filename == null) {
            return System.in;
        }
        File src2 = new File(filename);
        if (!src2.exists()) {
            System.err.println("File '" + filename + "' does not exist.");
            System.exit(1);
        }
        return new FileInputStream(src2);
    }

    private void decode(InputStream in) throws IOException {
        JsonParser jp = this.smileFactory.createJsonParser(in);
        JsonGenerator jg = this.jsonFactory.createJsonGenerator(System.out, JsonEncoding.UTF8);
        while (true) {
            if (jp.nextToken() == null && jp.nextToken() == null) {
                jp.close();
                jg.close();
                return;
            }
            jg.copyCurrentEvent(jp);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.smile.SmileFactory.createJsonGenerator(java.io.OutputStream, org.codehaus.jackson.JsonEncoding):org.codehaus.jackson.smile.SmileGenerator
     arg types: [java.io.PrintStream, org.codehaus.jackson.JsonEncoding]
     candidates:
      org.codehaus.jackson.smile.SmileFactory.createJsonGenerator(java.io.OutputStream, org.codehaus.jackson.JsonEncoding):org.codehaus.jackson.JsonGenerator
      org.codehaus.jackson.JsonFactory.createJsonGenerator(java.io.File, org.codehaus.jackson.JsonEncoding):org.codehaus.jackson.JsonGenerator
      org.codehaus.jackson.JsonFactory.createJsonGenerator(java.io.OutputStream, org.codehaus.jackson.JsonEncoding):org.codehaus.jackson.JsonGenerator
      org.codehaus.jackson.smile.SmileFactory.createJsonGenerator(java.io.OutputStream, org.codehaus.jackson.JsonEncoding):org.codehaus.jackson.smile.SmileGenerator */
    private void encode(InputStream in) throws IOException {
        JsonParser jp = this.jsonFactory.createJsonParser(in);
        JsonGenerator jg = this.smileFactory.createJsonGenerator((OutputStream) System.out, JsonEncoding.UTF8);
        while (jp.nextToken() != null) {
            jg.copyCurrentEvent(jp);
        }
        jp.close();
        jg.close();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.smile.SmileFactory.createJsonGenerator(java.io.OutputStream, org.codehaus.jackson.JsonEncoding):org.codehaus.jackson.smile.SmileGenerator
     arg types: [java.io.ByteArrayOutputStream, org.codehaus.jackson.JsonEncoding]
     candidates:
      org.codehaus.jackson.smile.SmileFactory.createJsonGenerator(java.io.OutputStream, org.codehaus.jackson.JsonEncoding):org.codehaus.jackson.JsonGenerator
      org.codehaus.jackson.JsonFactory.createJsonGenerator(java.io.File, org.codehaus.jackson.JsonEncoding):org.codehaus.jackson.JsonGenerator
      org.codehaus.jackson.JsonFactory.createJsonGenerator(java.io.OutputStream, org.codehaus.jackson.JsonEncoding):org.codehaus.jackson.JsonGenerator
      org.codehaus.jackson.smile.SmileFactory.createJsonGenerator(java.io.OutputStream, org.codehaus.jackson.JsonEncoding):org.codehaus.jackson.smile.SmileGenerator */
    private void verify(InputStream in, InputStream in2) throws IOException {
        String text1;
        String text2;
        JsonParser jp = this.jsonFactory.createJsonParser(in);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream(4000);
        JsonGenerator jg = this.smileFactory.createJsonGenerator((OutputStream) bytes, JsonEncoding.UTF8);
        while (jp.nextToken() != null) {
            jg.copyCurrentEvent(jp);
        }
        jp.close();
        jg.close();
        JsonParser jp2 = this.jsonFactory.createJsonParser(in2);
        byte[] smile = bytes.toByteArray();
        JsonParser jp22 = this.smileFactory.createJsonParser(smile);
        int count = 0;
        do {
            JsonToken t = jp2.nextToken();
            if (t != null) {
                JsonToken t2 = jp22.nextToken();
                count++;
                if (t != t2) {
                    throw new IOException("Input and encoded differ, token #" + count + "; expected " + t + ", got " + t2);
                }
                text1 = jp2.getText();
                text2 = jp22.getText();
            } else {
                System.out.println("OK: verified " + count + " tokens (from " + smile.length + " bytes of Smile encoded data), input and encoded contents are identical");
                return;
            }
        } while (text1.equals(text2));
        throw new IOException("Input and encoded differ, token #" + count + "; expected text '" + text1 + "', got '" + text2 + "'");
    }

    /* access modifiers changed from: protected */
    public void showUsage() {
        System.err.println("Usage: java " + getClass().getName() + " -e/-d [file]");
        System.err.println(" (if no file given, reads from stdin -- always writes to stdout)");
        System.err.println(" -d: decode Smile encoded input as JSON");
        System.err.println(" -e: encode JSON (text) input as Smile");
        System.err.println(" -v: encode JSON (text) input as Smile; read back, verify, do not write out");
        System.exit(1);
    }

    public static void main(String[] args) throws IOException {
        new Tool().process(args);
    }
}
