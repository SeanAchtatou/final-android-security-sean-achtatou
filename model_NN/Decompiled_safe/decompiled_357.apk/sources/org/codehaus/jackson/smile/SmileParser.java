package org.codehaus.jackson.smile;

import com.google.common.base.Ascii;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import org.codehaus.jackson.Base64Variant;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.ObjectCodec;
import org.codehaus.jackson.impl.StreamBasedParserBase;
import org.codehaus.jackson.io.IOContext;
import org.codehaus.jackson.org.objectweb.asm.Opcodes;
import org.codehaus.jackson.sym.BytesToNameCanonicalizer;
import org.codehaus.jackson.sym.Name;

public class SmileParser extends StreamBasedParserBase {
    private static final int[] NO_INTS = new int[0];
    private static final String[] NO_STRINGS = new String[0];
    protected static final ThreadLocal<SoftReference<SmileBufferRecycler<String>>> _smileRecyclerRef = new ThreadLocal<>();
    protected boolean _got32BitFloat;
    protected boolean _mayContainRawBinary;
    protected ObjectCodec _objectCodec;
    protected int _quad1;
    protected int _quad2;
    protected int[] _quadBuffer = NO_INTS;
    protected int _seenNameCount = 0;
    protected String[] _seenNames = NO_STRINGS;
    protected int _seenStringValueCount = -1;
    protected String[] _seenStringValues = null;
    protected final SmileBufferRecycler<String> _smileBufferRecycler;
    protected final BytesToNameCanonicalizer _symbols;
    protected boolean _tokenIncomplete = false;
    protected int _typeByte;

    public enum Feature {
        REQUIRE_HEADER(true);
        
        final boolean _defaultState;
        final int _mask = (1 << ordinal());

        public static int collectDefaults() {
            int flags = 0;
            for (Feature f : values()) {
                if (f.enabledByDefault()) {
                    flags |= f.getMask();
                }
            }
            return flags;
        }

        private Feature(boolean defaultState) {
            this._defaultState = defaultState;
        }

        public boolean enabledByDefault() {
            return this._defaultState;
        }

        public int getMask() {
            return this._mask;
        }
    }

    public SmileParser(IOContext ctxt, int parserFeatures, int smileFeatures, ObjectCodec codec, BytesToNameCanonicalizer sym, InputStream in, byte[] inputBuffer, int start, int end, boolean bufferRecyclable) {
        super(ctxt, parserFeatures, in, inputBuffer, start, end, bufferRecyclable);
        this._objectCodec = codec;
        this._symbols = sym;
        this._tokenInputRow = -1;
        this._tokenInputCol = -1;
        this._smileBufferRecycler = _smileBufferRecycler();
    }

    public ObjectCodec getCodec() {
        return this._objectCodec;
    }

    public void setCodec(ObjectCodec c) {
        this._objectCodec = c;
    }

    /* access modifiers changed from: protected */
    public boolean handleSignature(boolean consumeFirstByte, boolean throwException) throws IOException, JsonParseException {
        boolean z;
        if (consumeFirstByte) {
            this._inputPtr++;
        }
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        if (this._inputBuffer[this._inputPtr] != 41) {
            if (throwException) {
                _reportError("Malformed content: signature not valid, starts with 0x3a but followed by 0x" + Integer.toHexString(this._inputBuffer[this._inputPtr]) + ", not 0x29");
            }
            return false;
        }
        int i = this._inputPtr + 1;
        this._inputPtr = i;
        if (i >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        if (this._inputBuffer[this._inputPtr] != 10) {
            if (throwException) {
                _reportError("Malformed content: signature not valid, starts with 0x3a, 0x29, but followed by 0x" + Integer.toHexString(this._inputBuffer[this._inputPtr]) + ", not 0xA");
            }
            return false;
        }
        int i2 = this._inputPtr + 1;
        this._inputPtr = i2;
        if (i2 >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i3 = this._inputPtr;
        this._inputPtr = i3 + 1;
        byte b = bArr[i3];
        int versionBits = (b >> 4) & 15;
        if (versionBits != 0) {
            _reportError("Header version number bits (0x" + Integer.toHexString(versionBits) + ") indicate unrecognized version; only 0x0 handled by parser");
        }
        if ((b & 1) == 0) {
            this._seenNames = null;
            this._seenNameCount = -1;
        }
        if ((b & 2) != 0) {
            this._seenStringValues = NO_STRINGS;
            this._seenStringValueCount = 0;
        }
        if ((b & 4) != 0) {
            z = true;
        } else {
            z = false;
        }
        this._mayContainRawBinary = z;
        return true;
    }

    protected static final SmileBufferRecycler<String> _smileBufferRecycler() {
        SoftReference<SmileBufferRecycler<String>> ref = _smileRecyclerRef.get();
        SmileBufferRecycler<String> br = ref == null ? null : ref.get();
        if (br != null) {
            return br;
        }
        SmileBufferRecycler<String> br2 = new SmileBufferRecycler<>();
        _smileRecyclerRef.set(new SoftReference(br2));
        return br2;
    }

    /* access modifiers changed from: protected */
    public void _finishString() throws IOException, JsonParseException {
        _throwInternal();
    }

    public void close() throws IOException {
        super.close();
        this._symbols.release();
    }

    /* access modifiers changed from: protected */
    public void _releaseBuffers() throws IOException {
        super._releaseBuffers();
        String[] nameBuf = this._seenNames;
        if (nameBuf != null && nameBuf.length > 0) {
            this._seenNames = null;
            Arrays.fill(nameBuf, 0, this._seenNameCount, (Object) null);
            this._smileBufferRecycler.releaseSeenNamesBuffer(nameBuf);
        }
        String[] valueBuf = this._seenStringValues;
        if (valueBuf != null && valueBuf.length > 0) {
            this._seenStringValues = null;
            Arrays.fill(valueBuf, 0, this._seenStringValueCount, (Object) null);
            this._smileBufferRecycler.releaseSeenStringValuesBuffer(valueBuf);
        }
    }

    public boolean mayContainRawBinary() {
        return this._mayContainRawBinary;
    }

    public JsonToken nextToken() throws IOException, JsonParseException {
        boolean z;
        if (this._tokenIncomplete) {
            _skipIncomplete();
        }
        this._tokenInputTotal = (this._currInputProcessed + ((long) this._inputPtr)) - 1;
        this._binaryValue = null;
        if (this._parsingContext.inObject() && this._currToken != JsonToken.FIELD_NAME) {
            JsonToken _handleFieldName = _handleFieldName();
            this._currToken = _handleFieldName;
            return _handleFieldName;
        } else if (this._inputPtr < this._inputEnd || loadMore()) {
            byte[] bArr = this._inputBuffer;
            int i = this._inputPtr;
            this._inputPtr = i + 1;
            byte b = bArr[i];
            this._typeByte = b;
            switch ((b >> 5) & 7) {
                case 0:
                    if (b == 0) {
                        _reportError("Invalid token byte 0x00");
                    }
                    return _handleSharedString(b - 1);
                case 1:
                    int typeBits = b & 31;
                    if (typeBits < 4) {
                        switch (typeBits) {
                            case 0:
                                this._textBuffer.resetWithEmpty();
                                JsonToken jsonToken = JsonToken.VALUE_STRING;
                                this._currToken = jsonToken;
                                return jsonToken;
                            case 1:
                                JsonToken jsonToken2 = JsonToken.VALUE_NULL;
                                this._currToken = jsonToken2;
                                return jsonToken2;
                            case 2:
                                JsonToken jsonToken3 = JsonToken.VALUE_FALSE;
                                this._currToken = jsonToken3;
                                return jsonToken3;
                            default:
                                JsonToken jsonToken4 = JsonToken.VALUE_TRUE;
                                this._currToken = jsonToken4;
                                return jsonToken4;
                        }
                    } else if (typeBits < 8) {
                        if ((typeBits & 3) <= 2) {
                            this._tokenIncomplete = true;
                            this._numTypesValid = 0;
                            JsonToken jsonToken5 = JsonToken.VALUE_NUMBER_INT;
                            this._currToken = jsonToken5;
                            return jsonToken5;
                        }
                    } else if (typeBits < 12) {
                        int subtype = typeBits & 3;
                        if (subtype <= 2) {
                            this._tokenIncomplete = true;
                            this._numTypesValid = 0;
                            if (subtype == 0) {
                                z = true;
                            } else {
                                z = false;
                            }
                            this._got32BitFloat = z;
                            JsonToken jsonToken6 = JsonToken.VALUE_NUMBER_FLOAT;
                            this._currToken = jsonToken6;
                            return jsonToken6;
                        }
                    } else if (typeBits != 26 || !handleSignature(false, false)) {
                        _reportError("Unrecognized token byte 0x3A (malformed segment header?");
                        break;
                    } else if (this._currToken == null) {
                        return nextToken();
                    } else {
                        this._currToken = null;
                        return null;
                    }
                    break;
                case 2:
                case 3:
                case 4:
                case 5:
                    this._currToken = JsonToken.VALUE_STRING;
                    if (this._seenStringValueCount >= 0) {
                        _addSeenStringValue();
                    } else {
                        this._tokenIncomplete = true;
                    }
                    return this._currToken;
                case 6:
                    this._numberInt = SmileUtil.zigzagDecode((int) (b & Ascii.US));
                    this._numTypesValid = 1;
                    JsonToken jsonToken7 = JsonToken.VALUE_NUMBER_INT;
                    this._currToken = jsonToken7;
                    return jsonToken7;
                case 7:
                    switch (b & Ascii.US) {
                        case 0:
                        case 4:
                            this._tokenIncomplete = true;
                            JsonToken jsonToken8 = JsonToken.VALUE_STRING;
                            this._currToken = jsonToken8;
                            return jsonToken8;
                        case 8:
                            this._tokenIncomplete = true;
                            JsonToken jsonToken9 = JsonToken.VALUE_EMBEDDED_OBJECT;
                            this._currToken = jsonToken9;
                            return jsonToken9;
                        case Opcodes.FCONST_1:
                        case Opcodes.FCONST_2:
                        case Opcodes.DCONST_0:
                        case Opcodes.DCONST_1:
                            if (this._inputPtr >= this._inputEnd) {
                                loadMoreGuaranteed();
                            }
                            byte[] bArr2 = this._inputBuffer;
                            int i2 = this._inputPtr;
                            this._inputPtr = i2 + 1;
                            return _handleSharedString(((b & 3) << 8) + (bArr2[i2] & SmileConstants.BYTE_MARKER_END_OF_CONTENT));
                        case Opcodes.DLOAD:
                            this._parsingContext = this._parsingContext.createChildArrayContext(this._tokenInputRow, this._tokenInputCol);
                            JsonToken jsonToken10 = JsonToken.START_ARRAY;
                            this._currToken = jsonToken10;
                            return jsonToken10;
                        case Opcodes.ALOAD:
                            if (!this._parsingContext.inArray()) {
                                _reportMismatchedEndMarker(93, '}');
                            }
                            this._parsingContext = this._parsingContext.getParent();
                            JsonToken jsonToken11 = JsonToken.END_ARRAY;
                            this._currToken = jsonToken11;
                            return jsonToken11;
                        case 26:
                            this._parsingContext = this._parsingContext.createChildObjectContext(this._tokenInputRow, this._tokenInputCol);
                            JsonToken jsonToken12 = JsonToken.START_OBJECT;
                            this._currToken = jsonToken12;
                            return jsonToken12;
                        case 27:
                            _reportError("Invalid type marker byte 0xFB in value mode (would be END_OBJECT in key mode)");
                            this._tokenIncomplete = true;
                            JsonToken jsonToken13 = JsonToken.VALUE_EMBEDDED_OBJECT;
                            this._currToken = jsonToken13;
                            return jsonToken13;
                        case 29:
                            this._tokenIncomplete = true;
                            JsonToken jsonToken132 = JsonToken.VALUE_EMBEDDED_OBJECT;
                            this._currToken = jsonToken132;
                            return jsonToken132;
                        case 31:
                            this._currToken = null;
                            return null;
                    }
            }
            _reportError("Invalid type marker byte 0x" + Integer.toHexString(b & SmileConstants.BYTE_MARKER_END_OF_CONTENT) + " for expected value token");
            return null;
        } else {
            _handleEOF();
            close();
            this._currToken = null;
            return null;
        }
    }

    private final JsonToken _handleSharedString(int index) throws IOException, JsonParseException {
        if (index >= this._seenStringValueCount) {
            _reportInvalidSharedStringValue(index);
        }
        this._textBuffer.resetWithString(this._seenStringValues[index]);
        JsonToken jsonToken = JsonToken.VALUE_STRING;
        this._currToken = jsonToken;
        return jsonToken;
    }

    private final void _addSeenStringValue() throws IOException, JsonParseException {
        _finishToken();
        if (this._seenStringValueCount < this._seenStringValues.length) {
            String[] strArr = this._seenStringValues;
            int i = this._seenStringValueCount;
            this._seenStringValueCount = i + 1;
            strArr[i] = this._textBuffer.contentsAsString();
            return;
        }
        _expandSeenStringValues();
    }

    private final void _expandSeenStringValues() {
        int newSize;
        String[] newShared;
        String[] oldShared = this._seenStringValues;
        int len = oldShared.length;
        if (len == 0) {
            newShared = (String[]) this._smileBufferRecycler.allocSeenStringValuesBuffer();
            if (newShared == null) {
                newShared = new String[64];
            }
        } else if (len == 1024) {
            newShared = oldShared;
            this._seenStringValueCount = 0;
        } else {
            if (len == 64) {
                newSize = 256;
            } else {
                newSize = 1024;
            }
            newShared = new String[newSize];
            System.arraycopy(oldShared, 0, newShared, 0, oldShared.length);
        }
        this._seenStringValues = newShared;
        String[] strArr = this._seenStringValues;
        int i = this._seenStringValueCount;
        this._seenStringValueCount = i + 1;
        strArr[i] = this._textBuffer.contentsAsString();
    }

    public String getCurrentName() throws IOException, JsonParseException {
        return this._parsingContext.getCurrentName();
    }

    public JsonParser.NumberType getNumberType() throws IOException, JsonParseException {
        if (this._got32BitFloat) {
            return JsonParser.NumberType.FLOAT;
        }
        return super.getNumberType();
    }

    public String getText() throws IOException, JsonParseException {
        if (this._tokenIncomplete) {
            this._tokenIncomplete = false;
            int tb = this._typeByte;
            int type = (tb >> 5) & 7;
            if (type == 2 || type == 3) {
                _decodeShortAsciiValue((tb & 63) + 1);
                return this._textBuffer.contentsAsString();
            } else if (type == 4 || type == 5) {
                _decodeShortUnicodeValue((tb & 63) + 2);
                return this._textBuffer.contentsAsString();
            } else {
                _finishToken();
            }
        }
        if (this._currToken == JsonToken.VALUE_STRING) {
            return this._textBuffer.contentsAsString();
        }
        JsonToken t = this._currToken;
        if (t == null) {
            return null;
        }
        if (t == JsonToken.FIELD_NAME) {
            return this._parsingContext.getCurrentName();
        }
        if (t.isNumeric()) {
            return getNumberValue().toString();
        }
        return this._currToken.asString();
    }

    public char[] getTextCharacters() throws IOException, JsonParseException {
        if (this._currToken == null) {
            return null;
        }
        if (this._tokenIncomplete) {
            _finishToken();
        }
        switch (this._currToken) {
            case VALUE_STRING:
                return this._textBuffer.getTextBuffer();
            case FIELD_NAME:
                if (!this._nameCopied) {
                    String name = this._parsingContext.getCurrentName();
                    int nameLen = name.length();
                    if (this._nameCopyBuffer == null) {
                        this._nameCopyBuffer = this._ioContext.allocNameCopyBuffer(nameLen);
                    } else if (this._nameCopyBuffer.length < nameLen) {
                        this._nameCopyBuffer = new char[nameLen];
                    }
                    name.getChars(0, nameLen, this._nameCopyBuffer, 0);
                    this._nameCopied = true;
                }
                return this._nameCopyBuffer;
            case VALUE_NUMBER_INT:
            case VALUE_NUMBER_FLOAT:
                return getNumberValue().toString().toCharArray();
            default:
                return this._currToken.asCharArray();
        }
    }

    public int getTextLength() throws IOException, JsonParseException {
        if (this._currToken == null) {
            return 0;
        }
        if (this._tokenIncomplete) {
            _finishToken();
        }
        switch (this._currToken) {
            case VALUE_STRING:
                return this._textBuffer.size();
            case FIELD_NAME:
                return this._parsingContext.getCurrentName().length();
            case VALUE_NUMBER_INT:
            case VALUE_NUMBER_FLOAT:
                return getNumberValue().toString().length();
            default:
                return this._currToken.asCharArray().length;
        }
    }

    public int getTextOffset() throws IOException, JsonParseException {
        return 0;
    }

    public byte[] getBinaryValue(Base64Variant b64variant) throws IOException, JsonParseException {
        if (this._tokenIncomplete) {
            _finishToken();
        }
        if (this._currToken != JsonToken.VALUE_EMBEDDED_OBJECT) {
            _reportError("Current token (" + this._currToken + ") not VALUE_EMBEDDED_OBJECT, can not access as binary");
        }
        return this._binaryValue;
    }

    /* access modifiers changed from: protected */
    public byte[] _decodeBase64(Base64Variant b64variant) throws IOException, JsonParseException {
        _throwInternal();
        return null;
    }

    /* access modifiers changed from: protected */
    public final JsonToken _handleFieldName() throws IOException, JsonParseException {
        String name;
        String name2;
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        byte b = bArr[i];
        this._typeByte = b;
        switch ((b >> 6) & 3) {
            case 0:
                switch (b) {
                    case 32:
                        this._parsingContext.setCurrentName("");
                        return JsonToken.FIELD_NAME;
                    case 48:
                    case 49:
                    case 50:
                    case 51:
                        if (this._inputPtr >= this._inputEnd) {
                            loadMoreGuaranteed();
                        }
                        byte[] bArr2 = this._inputBuffer;
                        int i2 = this._inputPtr;
                        this._inputPtr = i2 + 1;
                        int index = ((b & 3) << 8) + (bArr2[i2] & SmileConstants.BYTE_MARKER_END_OF_CONTENT);
                        if (index >= this._seenNameCount) {
                            _reportInvalidSharedName(index);
                        }
                        this._parsingContext.setCurrentName(this._seenNames[index]);
                        return JsonToken.FIELD_NAME;
                    case Opcodes.CALOAD:
                        _throwInternal();
                        return JsonToken.FIELD_NAME;
                }
            case 1:
                int index2 = b & 63;
                if (index2 >= this._seenNameCount) {
                    _reportInvalidSharedName(index2);
                }
                this._parsingContext.setCurrentName(this._seenNames[index2]);
                return JsonToken.FIELD_NAME;
            case 2:
                int len = (b & 63) + 1;
                Name n = _findDecodedFromSymbols(len);
                if (n != null) {
                    name2 = n.getName();
                    this._inputPtr += len;
                } else {
                    name2 = _addDecodedToSymbols(len, _decodeShortAsciiName(len));
                }
                if (this._seenNames != null) {
                    if (this._seenNameCount >= this._seenNames.length) {
                        this._seenNames = _expandSeenNames(this._seenNames);
                    }
                    String[] strArr = this._seenNames;
                    int i3 = this._seenNameCount;
                    this._seenNameCount = i3 + 1;
                    strArr[i3] = name2;
                }
                this._parsingContext.setCurrentName(name2);
                return JsonToken.FIELD_NAME;
            case 3:
                int len2 = b & 63;
                if (len2 <= 55) {
                    int len3 = len2 + 2;
                    Name n2 = _findDecodedFromSymbols(len3);
                    if (n2 != null) {
                        name = n2.getName();
                        this._inputPtr += len3;
                    } else {
                        name = _addDecodedToSymbols(len3, _decodeShortUnicodeName(len3));
                    }
                    if (this._seenNames != null) {
                        if (this._seenNameCount >= this._seenNames.length) {
                            this._seenNames = _expandSeenNames(this._seenNames);
                        }
                        String[] strArr2 = this._seenNames;
                        int i4 = this._seenNameCount;
                        this._seenNameCount = i4 + 1;
                        strArr2[i4] = name;
                    }
                    this._parsingContext.setCurrentName(name);
                    return JsonToken.FIELD_NAME;
                } else if (len2 == 59) {
                    if (!this._parsingContext.inObject()) {
                        _reportMismatchedEndMarker(Opcodes.LUSHR, ']');
                    }
                    this._parsingContext = this._parsingContext.getParent();
                    return JsonToken.END_OBJECT;
                }
                break;
        }
        _reportError("Invalid type marker byte 0x" + Integer.toHexString(b) + " for expected field name (or END_OBJECT marker)");
        return null;
    }

    private final String[] _expandSeenNames(String[] oldShared) {
        int newSize;
        int len = oldShared.length;
        if (len == 0) {
            String[] newShared = (String[]) this._smileBufferRecycler.allocSeenNamesBuffer();
            if (newShared == null) {
                return new String[64];
            }
            return newShared;
        } else if (len == 1024) {
            String[] newShared2 = oldShared;
            this._seenNameCount = 0;
            return newShared2;
        } else {
            if (len == 64) {
                newSize = 256;
            } else {
                newSize = 1024;
            }
            String[] newShared3 = new String[newSize];
            System.arraycopy(oldShared, 0, newShared3, 0, oldShared.length);
            return newShared3;
        }
    }

    private final String _addDecodedToSymbols(int len, String name) {
        if (len < 5) {
            return this._symbols.addName(name, this._quad1, 0).getName();
        }
        if (len < 9) {
            return this._symbols.addName(name, this._quad1, this._quad2).getName();
        }
        return this._symbols.addName(name, this._quadBuffer, (len + 3) >> 2).getName();
    }

    private final String _decodeShortAsciiName(int len) throws IOException, JsonParseException {
        int inPtr;
        char[] outBuf = this._textBuffer.emptyAndGetCurrentSegment();
        byte[] inBuf = this._inputBuffer;
        int inPtr2 = this._inputPtr;
        int inEnd = (inPtr2 + len) - 3;
        int inPtr3 = inPtr2;
        int outPtr = 0;
        while (inPtr3 < inEnd) {
            int outPtr2 = outPtr + 1;
            int inPtr4 = inPtr3 + 1;
            outBuf[outPtr] = (char) inBuf[inPtr3];
            int outPtr3 = outPtr2 + 1;
            int inPtr5 = inPtr4 + 1;
            outBuf[outPtr2] = (char) inBuf[inPtr4];
            int outPtr4 = outPtr3 + 1;
            int inPtr6 = inPtr5 + 1;
            outBuf[outPtr3] = (char) inBuf[inPtr5];
            outPtr = outPtr4 + 1;
            inPtr3 = inPtr6 + 1;
            outBuf[outPtr4] = (char) inBuf[inPtr6];
        }
        int left = len & 3;
        if (left > 0) {
            int outPtr5 = outPtr + 1;
            inPtr = inPtr3 + 1;
            outBuf[outPtr] = (char) inBuf[inPtr3];
            if (left > 1) {
                outPtr = outPtr5 + 1;
                inPtr3 = inPtr + 1;
                outBuf[outPtr5] = (char) inBuf[inPtr];
                if (left > 2) {
                    int i = outPtr + 1;
                    inPtr = inPtr3 + 1;
                    outBuf[outPtr] = (char) inBuf[inPtr3];
                }
            }
            this._inputPtr = inPtr;
            this._textBuffer.setCurrentLength(len);
            return this._textBuffer.contentsAsString();
        }
        inPtr = inPtr3;
        this._inputPtr = inPtr;
        this._textBuffer.setCurrentLength(len);
        return this._textBuffer.contentsAsString();
    }

    private final String _decodeShortUnicodeName(int len) throws IOException, JsonParseException {
        int outPtr;
        char[] outBuf = this._textBuffer.emptyAndGetCurrentSegment();
        int inPtr = this._inputPtr;
        this._inputPtr += len;
        int[] codes = SmileConstants.sUtf8UnitLengths;
        byte[] inBuf = this._inputBuffer;
        int end = inPtr + len;
        int inPtr2 = inPtr;
        int outPtr2 = 0;
        while (inPtr2 < end) {
            int inPtr3 = inPtr2 + 1;
            int i = inBuf[inPtr2] & 255;
            int code = codes[i];
            if (code != 0) {
                switch (code) {
                    case 1:
                        i = ((i & 31) << 6) | (inBuf[inPtr3] & 63);
                        inPtr3++;
                        outPtr = outPtr2;
                        continue;
                        outPtr2 = outPtr + 1;
                        outBuf[outPtr] = (char) i;
                        inPtr2 = inPtr3;
                    case 2:
                        int inPtr4 = inPtr3 + 1;
                        inPtr3 = inPtr4 + 1;
                        i = ((i & 15) << 12) | ((inBuf[inPtr3] & 63) << 6) | (inBuf[inPtr4] & 63);
                        outPtr = outPtr2;
                        continue;
                        outPtr2 = outPtr + 1;
                        outBuf[outPtr] = (char) i;
                        inPtr2 = inPtr3;
                    case 3:
                        int inPtr5 = inPtr3 + 1;
                        int inPtr6 = inPtr5 + 1;
                        int i2 = (((((i & 7) << 18) | ((inBuf[inPtr3] & 63) << Ascii.FF)) | ((inBuf[inPtr5] & 63) << 6)) | (inBuf[inPtr6] & 63)) - 65536;
                        outPtr = outPtr2 + 1;
                        outBuf[outPtr2] = (char) (55296 | (i2 >> 10));
                        i = 56320 | (i2 & 1023);
                        inPtr3 = inPtr6 + 1;
                        continue;
                        outPtr2 = outPtr + 1;
                        outBuf[outPtr] = (char) i;
                        inPtr2 = inPtr3;
                    default:
                        _reportError("Invalid byte " + Integer.toHexString(i) + " in short Unicode text block");
                        break;
                }
            }
            outPtr = outPtr2;
            outPtr2 = outPtr + 1;
            outBuf[outPtr] = (char) i;
            inPtr2 = inPtr3;
        }
        this._textBuffer.setCurrentLength(outPtr2);
        return this._textBuffer.contentsAsString();
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private final Name _findDecodedFromSymbols(int len) throws IOException, JsonParseException {
        int q2;
        if (this._inputEnd - this._inputPtr < len) {
            _loadToHaveAtLeast(len);
        }
        if (len < 5) {
            int inPtr = this._inputPtr;
            byte[] inBuf = this._inputBuffer;
            byte b = inBuf[inPtr];
            int len2 = len - 1;
            int q = b;
            if (len2 > 0) {
                int inPtr2 = inPtr + 1;
                q = (b << 8) + inBuf[inPtr2];
                int len3 = len2 - 1;
                if (len3 > 0) {
                    int inPtr3 = inPtr2 + 1;
                    q = (q << 8) + inBuf[inPtr3];
                    if (len3 - 1 > 0) {
                        q = (q << 8) + inBuf[inPtr3 + 1];
                    }
                }
            }
            this._quad1 = q;
            return this._symbols.findName(q);
        } else if (len >= 9) {
            return _findDecodedLong(len);
        } else {
            int inPtr4 = this._inputPtr;
            byte[] inBuf2 = this._inputBuffer;
            int inPtr5 = inPtr4 + 1;
            int inPtr6 = inPtr5 + 1;
            int inPtr7 = inPtr6 + 1;
            int inPtr8 = inPtr7 + 1;
            int q1 = (((((inBuf2[inPtr4] << 8) + inBuf2[inPtr5]) << 8) + inBuf2[inPtr6]) << 8) + inBuf2[inPtr7];
            int inPtr9 = inPtr8 + 1;
            byte b2 = inBuf2[inPtr8];
            int len4 = len - 5;
            int q22 = b2;
            if (len4 > 0) {
                int inPtr10 = inPtr9 + 1;
                q2 = (b2 << 8) + inBuf2[inPtr9];
                int len5 = len4 - 1;
                if (len5 >= 0) {
                    inPtr9 = inPtr10 + 1;
                    q22 = (q2 << 8) + inBuf2[inPtr10];
                    if (len5 - 1 >= 0) {
                        int i = inPtr9 + 1;
                        q2 = (q22 << 8) + inBuf2[inPtr9];
                    }
                }
                this._quad1 = q1;
                this._quad2 = q2;
                return this._symbols.findName(q1, q2);
            }
            q2 = q22;
            this._quad1 = q1;
            this._quad2 = q2;
            return this._symbols.findName(q1, q2);
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private final Name _findDecodedLong(int len) throws IOException, JsonParseException {
        int offset;
        int offset2;
        int q;
        int bufLen = (len + 3) >> 2;
        if (bufLen > this._quadBuffer.length) {
            this._quadBuffer = _growArrayTo(this._quadBuffer, bufLen);
        }
        int offset3 = 0;
        int inPtr = this._inputPtr;
        byte[] inBuf = this._inputBuffer;
        while (true) {
            int inPtr2 = inPtr + 1;
            int inPtr3 = inPtr2 + 1;
            int inPtr4 = inPtr3 + 1;
            inPtr = inPtr4 + 1;
            offset = offset3 + 1;
            this._quadBuffer[offset3] = (((((inBuf[inPtr] << 8) | inBuf[inPtr2]) << 8) | inBuf[inPtr3]) << 8) | inBuf[inPtr4];
            len -= 4;
            if (len <= 3) {
                break;
            }
            offset3 = offset;
        }
        if (len > 0) {
            int inPtr5 = inPtr + 1;
            byte b = inBuf[inPtr];
            int len2 = len - 1;
            if (len2 >= 0) {
                int inPtr6 = inPtr5 + 1;
                q = (b << 8) + inBuf[inPtr5];
                if (len2 - 1 >= 0) {
                    int inPtr7 = inPtr6 + 1;
                    q = (q << 8) + inBuf[inPtr6];
                }
            } else {
                q = b;
            }
            offset2 = offset + 1;
            this._quadBuffer[offset] = q;
        } else {
            offset2 = offset;
        }
        return this._symbols.findName(this._quadBuffer, offset2);
    }

    private static int[] _growArrayTo(int[] arr, int minSize) {
        int[] newArray = new int[(minSize + 4)];
        if (arr != null) {
            System.arraycopy(arr, 0, newArray, 0, arr.length);
        }
        return newArray;
    }

    /* access modifiers changed from: protected */
    public void _parseNumericValue(int expType) throws IOException, JsonParseException {
        if (this._tokenIncomplete) {
            int tb = this._typeByte;
            if (((tb >> 5) & 7) != 1) {
                _reportError("Current token (" + this._currToken + ") not numeric, can not use numeric value accessors");
            }
            this._tokenIncomplete = false;
            _finishNumberToken(tb);
        }
    }

    /* access modifiers changed from: protected */
    public void _finishToken() throws IOException, JsonParseException {
        this._tokenIncomplete = false;
        int tb = this._typeByte;
        int type = (tb >> 5) & 7;
        if (type == 1) {
            _finishNumberToken(tb);
        } else if (type <= 3) {
            _decodeShortAsciiValue((tb & 63) + 1);
        } else if (type <= 5) {
            _decodeShortUnicodeValue((tb & 63) + 2);
        } else {
            if (type == 7) {
                switch ((tb & 31) >> 2) {
                    case 0:
                        _decodeLongAscii();
                        return;
                    case 1:
                        _decodeLongUnicode();
                        return;
                    case 2:
                        this._binaryValue = _read7BitBinaryWithLength();
                        return;
                    case 7:
                        _finishRawBinary();
                        return;
                }
            }
            _throwInternal();
        }
    }

    /* access modifiers changed from: protected */
    public final void _finishNumberToken(int tb) throws IOException, JsonParseException {
        int tb2 = tb & 31;
        int type = tb2 >> 2;
        if (type == 1) {
            int subtype = tb2 & 3;
            if (subtype == 0) {
                _finishInt();
            } else if (subtype == 1) {
                _finishLong();
            } else if (subtype == 2) {
                _finishBigInteger();
            } else {
                _throwInternal();
            }
        } else {
            if (type == 2) {
                switch (tb2 & 3) {
                    case 0:
                        _finishFloat();
                        return;
                    case 1:
                        _finishDouble();
                        return;
                    case 2:
                        _finishBigDecimal();
                        return;
                }
            }
            _throwInternal();
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private final void _finishInt() throws IOException, JsonParseException {
        int value;
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        byte b = bArr[i];
        if (b < 0) {
            value = b & 63;
        } else {
            if (this._inputPtr >= this._inputEnd) {
                loadMoreGuaranteed();
            }
            byte[] bArr2 = this._inputBuffer;
            int i2 = this._inputPtr;
            this._inputPtr = i2 + 1;
            byte b2 = bArr2[i2];
            int value2 = b;
            if (b2 >= 0) {
                value2 = (b << 7) + b2;
                if (this._inputPtr >= this._inputEnd) {
                    loadMoreGuaranteed();
                }
                byte[] bArr3 = this._inputBuffer;
                int i3 = this._inputPtr;
                this._inputPtr = i3 + 1;
                b2 = bArr3[i3];
                if (b2 >= 0) {
                    value2 = (value2 << 7) + b2;
                    if (this._inputPtr >= this._inputEnd) {
                        loadMoreGuaranteed();
                    }
                    byte[] bArr4 = this._inputBuffer;
                    int i4 = this._inputPtr;
                    this._inputPtr = i4 + 1;
                    b2 = bArr4[i4];
                    if (b2 >= 0) {
                        value2 = (value2 << 7) + b2;
                        if (this._inputPtr >= this._inputEnd) {
                            loadMoreGuaranteed();
                        }
                        byte[] bArr5 = this._inputBuffer;
                        int i5 = this._inputPtr;
                        this._inputPtr = i5 + 1;
                        b2 = bArr5[i5];
                        if (b2 >= 0) {
                            _reportError("Corrupt input; 32-bit VInt extends beyond 5 data bytes");
                        }
                    }
                }
            }
            value = (value2 << 6) + (b2 & 63);
        }
        this._numberInt = SmileUtil.zigzagDecode(value);
        this._numTypesValid = 1;
    }

    private final void _finishLong() throws IOException, JsonParseException {
        long l = (long) _fourBytesToInt();
        while (true) {
            if (this._inputPtr >= this._inputEnd) {
                loadMoreGuaranteed();
            }
            byte[] bArr = this._inputBuffer;
            int i = this._inputPtr;
            this._inputPtr = i + 1;
            byte b = bArr[i];
            if (b < 0) {
                this._numberLong = SmileUtil.zigzagDecode((l << 6) + ((long) (b & 63)));
                this._numTypesValid = 2;
                return;
            }
            l = (l << 7) + ((long) b);
        }
    }

    private final void _finishBigInteger() throws IOException, JsonParseException {
        this._numberBigInt = new BigInteger(_read7BitBinaryWithLength());
        this._numTypesValid = 4;
    }

    private final void _finishFloat() throws IOException, JsonParseException {
        int i = _fourBytesToInt();
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        this._numberDouble = (double) Float.intBitsToFloat((i << 7) + bArr[i2]);
        this._numTypesValid = 8;
    }

    private final void _finishDouble() throws IOException, JsonParseException {
        long value = (((long) _fourBytesToInt()) << 28) + ((long) _fourBytesToInt());
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        long value2 = (value << 7) + ((long) bArr[i]);
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr2 = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        this._numberDouble = Double.longBitsToDouble((value2 << 7) + ((long) bArr2[i2]));
        this._numTypesValid = 8;
    }

    private final int _fourBytesToInt() throws IOException, JsonParseException {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        byte b = bArr[i];
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr2 = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        int i3 = (b << 7) + bArr2[i2];
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr3 = this._inputBuffer;
        int i4 = this._inputPtr;
        this._inputPtr = i4 + 1;
        int i5 = (i3 << 7) + bArr3[i4];
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr4 = this._inputBuffer;
        int i6 = this._inputPtr;
        this._inputPtr = i6 + 1;
        return (i5 << 7) + bArr4[i6];
    }

    private final void _finishBigDecimal() throws IOException, JsonParseException {
        this._numberBigDecimal = new BigDecimal(new BigInteger(_read7BitBinaryWithLength()), SmileUtil.zigzagDecode(_readUnsignedVInt()));
        this._numTypesValid = 16;
    }

    private final int _readUnsignedVInt() throws IOException, JsonParseException {
        int value = 0;
        while (true) {
            if (this._inputPtr >= this._inputEnd) {
                loadMoreGuaranteed();
            }
            byte[] bArr = this._inputBuffer;
            int i = this._inputPtr;
            this._inputPtr = i + 1;
            byte b = bArr[i];
            if (b < 0) {
                return (value << 6) + (b & 63);
            }
            value = (value << 7) + b;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private final byte[] _read7BitBinaryWithLength() throws IOException, JsonParseException {
        int byteLen = _readUnsignedVInt();
        byte[] result = new byte[byteLen];
        int lastOkPtr = byteLen - 7;
        int ptr = 0;
        while (ptr <= lastOkPtr) {
            if (this._inputEnd - this._inputPtr < 8) {
                _loadToHaveAtLeast(8);
            }
            byte[] bArr = this._inputBuffer;
            int i = this._inputPtr;
            this._inputPtr = i + 1;
            int i2 = bArr[i] << Ascii.EM;
            byte[] bArr2 = this._inputBuffer;
            int i3 = this._inputPtr;
            this._inputPtr = i3 + 1;
            int i4 = i2 + (bArr2[i3] << Ascii.DC2);
            byte[] bArr3 = this._inputBuffer;
            int i5 = this._inputPtr;
            this._inputPtr = i5 + 1;
            int i6 = i4 + (bArr3[i5] << Ascii.VT);
            byte[] bArr4 = this._inputBuffer;
            int i7 = this._inputPtr;
            this._inputPtr = i7 + 1;
            int i1 = i6 + (bArr4[i7] << 4);
            byte[] bArr5 = this._inputBuffer;
            int i8 = this._inputPtr;
            this._inputPtr = i8 + 1;
            byte b = bArr5[i8];
            int i12 = i1 + (b >> 3);
            int i9 = (b & 7) << Ascii.NAK;
            byte[] bArr6 = this._inputBuffer;
            int i10 = this._inputPtr;
            this._inputPtr = i10 + 1;
            int i11 = i9 + (bArr6[i10] << Ascii.SO);
            byte[] bArr7 = this._inputBuffer;
            int i13 = this._inputPtr;
            this._inputPtr = i13 + 1;
            int i14 = i11 + (bArr7[i13] << 7);
            byte[] bArr8 = this._inputBuffer;
            int i15 = this._inputPtr;
            this._inputPtr = i15 + 1;
            int i22 = i14 + bArr8[i15];
            int ptr2 = ptr + 1;
            result[ptr] = (byte) (i12 >> 24);
            int ptr3 = ptr2 + 1;
            result[ptr2] = (byte) (i12 >> 16);
            int ptr4 = ptr3 + 1;
            result[ptr3] = (byte) (i12 >> 8);
            int ptr5 = ptr4 + 1;
            result[ptr4] = (byte) i12;
            int ptr6 = ptr5 + 1;
            result[ptr5] = (byte) (i22 >> 16);
            int ptr7 = ptr6 + 1;
            result[ptr6] = (byte) (i22 >> 8);
            result[ptr7] = (byte) i22;
            ptr = ptr7 + 1;
        }
        int toDecode = result.length - ptr;
        if (toDecode > 0) {
            if (this._inputEnd - this._inputPtr < toDecode + 1) {
                _loadToHaveAtLeast(toDecode + 1);
            }
            byte[] bArr9 = this._inputBuffer;
            int i16 = this._inputPtr;
            this._inputPtr = i16 + 1;
            int i17 = 1;
            int value = bArr9[i16];
            while (i17 < toDecode) {
                byte[] bArr10 = this._inputBuffer;
                int i18 = this._inputPtr;
                this._inputPtr = i18 + 1;
                value = (value << 7) + bArr10[i18];
                result[ptr] = (byte) (value >> (7 - i17));
                i17++;
                ptr++;
            }
            byte[] bArr11 = this._inputBuffer;
            int i19 = this._inputPtr;
            this._inputPtr = i19 + 1;
            result[ptr] = (byte) (bArr11[i19] + (value << toDecode));
        }
        return result;
    }

    /* access modifiers changed from: protected */
    public final void _decodeShortAsciiValue(int len) throws IOException, JsonParseException {
        if (this._inputEnd - this._inputPtr < len) {
            _loadToHaveAtLeast(len);
        }
        char[] outBuf = this._textBuffer.emptyAndGetCurrentSegment();
        byte[] inBuf = this._inputBuffer;
        int inPtr = this._inputPtr;
        int end = inPtr + len;
        int outPtr = 0;
        while (inPtr < end) {
            outBuf[outPtr] = (char) inBuf[inPtr];
            inPtr++;
            outPtr++;
        }
        this._inputPtr = inPtr;
        this._textBuffer.setCurrentLength(len);
    }

    /* access modifiers changed from: protected */
    public final void _decodeShortUnicodeValue(int len) throws IOException, JsonParseException {
        int outPtr;
        if (this._inputEnd - this._inputPtr < len) {
            _loadToHaveAtLeast(len);
        }
        char[] outBuf = this._textBuffer.emptyAndGetCurrentSegment();
        int inPtr = this._inputPtr;
        this._inputPtr += len;
        int[] codes = SmileConstants.sUtf8UnitLengths;
        byte[] inputBuf = this._inputBuffer;
        int end = inPtr + len;
        int inPtr2 = inPtr;
        int outPtr2 = 0;
        while (inPtr2 < end) {
            int inPtr3 = inPtr2 + 1;
            int i = inputBuf[inPtr2] & 255;
            int code = codes[i];
            if (code != 0) {
                switch (code) {
                    case 1:
                        i = ((i & 31) << 6) | (inputBuf[inPtr3] & 63);
                        inPtr3++;
                        outPtr = outPtr2;
                        continue;
                        outPtr2 = outPtr + 1;
                        outBuf[outPtr] = (char) i;
                        inPtr2 = inPtr3;
                    case 2:
                        int inPtr4 = inPtr3 + 1;
                        inPtr3 = inPtr4 + 1;
                        i = ((i & 15) << 12) | ((inputBuf[inPtr3] & 63) << 6) | (inputBuf[inPtr4] & 63);
                        outPtr = outPtr2;
                        continue;
                        outPtr2 = outPtr + 1;
                        outBuf[outPtr] = (char) i;
                        inPtr2 = inPtr3;
                    case 3:
                        int inPtr5 = inPtr3 + 1;
                        int inPtr6 = inPtr5 + 1;
                        int i2 = (((((i & 7) << 18) | ((inputBuf[inPtr3] & 63) << Ascii.FF)) | ((inputBuf[inPtr5] & 63) << 6)) | (inputBuf[inPtr6] & 63)) - 65536;
                        outPtr = outPtr2 + 1;
                        outBuf[outPtr2] = (char) (55296 | (i2 >> 10));
                        i = 56320 | (i2 & 1023);
                        inPtr3 = inPtr6 + 1;
                        continue;
                        outPtr2 = outPtr + 1;
                        outBuf[outPtr] = (char) i;
                        inPtr2 = inPtr3;
                    default:
                        _reportError("Invalid byte " + Integer.toHexString(i) + " in short Unicode text block");
                        break;
                }
            }
            outPtr = outPtr2;
            outPtr2 = outPtr + 1;
            outBuf[outPtr] = (char) i;
            inPtr2 = inPtr3;
        }
        this._textBuffer.setCurrentLength(outPtr2);
    }

    private final void _decodeLongAscii() throws IOException, JsonParseException {
        int inPtr;
        int outPtr;
        int outPtr2 = 0;
        char[] outBuf = this._textBuffer.emptyAndGetCurrentSegment();
        while (true) {
            if (this._inputPtr >= this._inputEnd) {
                loadMoreGuaranteed();
            }
            int inPtr2 = this._inputPtr;
            int left = this._inputEnd - inPtr2;
            if (outPtr2 >= outBuf.length) {
                outBuf = this._textBuffer.finishCurrentSegment();
                outPtr2 = 0;
            }
            int left2 = Math.min(left, outBuf.length - outPtr2);
            while (true) {
                inPtr = inPtr2 + 1;
                byte b = this._inputBuffer[inPtr2];
                if (b == -4) {
                    this._inputPtr = inPtr;
                    this._textBuffer.setCurrentLength(outPtr2);
                    return;
                }
                outPtr = outPtr2 + 1;
                outBuf[outPtr2] = (char) b;
                left2--;
                if (left2 <= 0) {
                    break;
                }
                inPtr2 = inPtr;
                outPtr2 = outPtr;
            }
            this._inputPtr = inPtr;
            outPtr2 = outPtr;
        }
    }

    private final void _decodeLongUnicode() throws IOException, JsonParseException {
        int ptr;
        int outPtr;
        int outPtr2;
        int outPtr3 = 0;
        char[] outBuf = this._textBuffer.emptyAndGetCurrentSegment();
        int[] codes = SmileConstants.sUtf8UnitLengths;
        byte[] inputBuffer = this._inputBuffer;
        while (true) {
            int ptr2 = this._inputPtr;
            if (ptr2 >= this._inputEnd) {
                loadMoreGuaranteed();
                ptr2 = this._inputPtr;
            }
            if (outPtr3 >= outBuf.length) {
                outBuf = this._textBuffer.finishCurrentSegment();
                outPtr3 = 0;
            }
            int max = this._inputEnd;
            int max2 = ptr2 + (outBuf.length - outPtr3);
            if (max2 < max) {
                max = max2;
                ptr = ptr2;
                outPtr = outPtr3;
            } else {
                ptr = ptr2;
                outPtr = outPtr3;
            }
            while (true) {
                if (ptr < max) {
                    int ptr3 = ptr + 1;
                    int c = inputBuffer[ptr] & SmileConstants.BYTE_MARKER_END_OF_CONTENT;
                    if (codes[c] != 0) {
                        this._inputPtr = ptr3;
                        if (c == 252) {
                            this._textBuffer.setCurrentLength(outPtr);
                            return;
                        }
                        switch (codes[c]) {
                            case 1:
                                c = _decodeUtf8_2(c);
                                outPtr2 = outPtr;
                                break;
                            case 2:
                                if (this._inputEnd - this._inputPtr < 2) {
                                    c = _decodeUtf8_3(c);
                                    outPtr2 = outPtr;
                                    break;
                                } else {
                                    c = _decodeUtf8_3fast(c);
                                    outPtr2 = outPtr;
                                    break;
                                }
                            case 3:
                            default:
                                _reportInvalidChar(c);
                                outPtr2 = outPtr;
                                break;
                            case 4:
                                int c2 = _decodeUtf8_4(c);
                                outPtr2 = outPtr + 1;
                                outBuf[outPtr] = (char) (55296 | (c2 >> 10));
                                if (outPtr2 >= outBuf.length) {
                                    outBuf = this._textBuffer.finishCurrentSegment();
                                    outPtr2 = 0;
                                }
                                c = 56320 | (c2 & 1023);
                                break;
                        }
                        if (outPtr2 >= outBuf.length) {
                            outBuf = this._textBuffer.finishCurrentSegment();
                            outPtr2 = 0;
                        }
                        outBuf[outPtr2] = (char) c;
                        outPtr3 = outPtr2 + 1;
                    } else {
                        outBuf[outPtr] = (char) c;
                        ptr = ptr3;
                        outPtr++;
                    }
                } else {
                    this._inputPtr = ptr;
                    outPtr3 = outPtr;
                }
            }
        }
    }

    private final void _finishRawBinary() throws IOException, JsonParseException {
        int byteLen = _readUnsignedVInt();
        this._binaryValue = new byte[byteLen];
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        int ptr = 0;
        while (true) {
            int toAdd = Math.min(byteLen, this._inputEnd - this._inputPtr);
            System.arraycopy(this._inputBuffer, this._inputPtr, this._binaryValue, ptr, toAdd);
            this._inputPtr += toAdd;
            ptr += toAdd;
            byteLen -= toAdd;
            if (byteLen > 0) {
                loadMoreGuaranteed();
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void _skipIncomplete() throws java.io.IOException, org.codehaus.jackson.JsonParseException {
        /*
            r5 = this;
            r3 = 0
            r5._tokenIncomplete = r3
            int r2 = r5._typeByte
            int r3 = r2 >> 5
            r3 = r3 & 7
            switch(r3) {
                case 1: goto L_0x0010;
                case 2: goto L_0x0056;
                case 3: goto L_0x0056;
                case 4: goto L_0x005e;
                case 5: goto L_0x005e;
                case 6: goto L_0x000c;
                case 7: goto L_0x0066;
                default: goto L_0x000c;
            }
        L_0x000c:
            r5._throwInternal()
        L_0x000f:
            return
        L_0x0010:
            r2 = r2 & 31
            int r3 = r2 >> 2
            switch(r3) {
                case 1: goto L_0x0018;
                case 2: goto L_0x003e;
                default: goto L_0x0017;
            }
        L_0x0017:
            goto L_0x000c
        L_0x0018:
            r3 = r2 & 3
            switch(r3) {
                case 0: goto L_0x001e;
                case 1: goto L_0x0031;
                case 2: goto L_0x003a;
                default: goto L_0x001d;
            }
        L_0x001d:
            goto L_0x000c
        L_0x001e:
            int r1 = r5._inputEnd
            byte[] r0 = r5._inputBuffer
        L_0x0022:
            int r3 = r5._inputPtr
            if (r3 >= r1) goto L_0x0036
            int r3 = r5._inputPtr
            int r4 = r3 + 1
            r5._inputPtr = r4
            byte r3 = r0[r3]
            if (r3 >= 0) goto L_0x0022
            goto L_0x000f
        L_0x0031:
            r3 = 4
            r5._skipBytes(r3)
            goto L_0x001e
        L_0x0036:
            r5.loadMoreGuaranteed()
            goto L_0x001e
        L_0x003a:
            r5._skip7BitBinary()
            goto L_0x000f
        L_0x003e:
            r3 = r2 & 3
            switch(r3) {
                case 0: goto L_0x0044;
                case 1: goto L_0x0049;
                case 2: goto L_0x004f;
                default: goto L_0x0043;
            }
        L_0x0043:
            goto L_0x000c
        L_0x0044:
            r3 = 5
            r5._skipBytes(r3)
            goto L_0x000f
        L_0x0049:
            r3 = 10
            r5._skipBytes(r3)
            goto L_0x000f
        L_0x004f:
            r5._readUnsignedVInt()
            r5._skip7BitBinary()
            goto L_0x000f
        L_0x0056:
            r3 = r2 & 63
            int r3 = r3 + 1
            r5._skipBytes(r3)
            goto L_0x000f
        L_0x005e:
            r3 = r2 & 63
            int r3 = r3 + 2
            r5._skipBytes(r3)
            goto L_0x000f
        L_0x0066:
            r2 = r2 & 31
            int r3 = r2 >> 2
            switch(r3) {
                case 0: goto L_0x006e;
                case 1: goto L_0x006e;
                case 2: goto L_0x0086;
                case 3: goto L_0x006d;
                case 4: goto L_0x006d;
                case 5: goto L_0x006d;
                case 6: goto L_0x006d;
                case 7: goto L_0x008a;
                default: goto L_0x006d;
            }
        L_0x006d:
            goto L_0x000c
        L_0x006e:
            int r1 = r5._inputEnd
            byte[] r0 = r5._inputBuffer
        L_0x0072:
            int r3 = r5._inputPtr
            if (r3 >= r1) goto L_0x0082
            int r3 = r5._inputPtr
            int r4 = r3 + 1
            r5._inputPtr = r4
            byte r3 = r0[r3]
            r4 = -4
            if (r3 != r4) goto L_0x0072
            goto L_0x000f
        L_0x0082:
            r5.loadMoreGuaranteed()
            goto L_0x006e
        L_0x0086:
            r5._skip7BitBinary()
            goto L_0x000f
        L_0x008a:
            int r3 = r5._readUnsignedVInt()
            r5._skipBytes(r3)
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.smile.SmileParser._skipIncomplete():void");
    }

    /* access modifiers changed from: protected */
    public void _skipBytes(int len) throws IOException, JsonParseException {
        while (true) {
            int toAdd = Math.min(len, this._inputEnd - this._inputPtr);
            this._inputPtr += toAdd;
            len -= toAdd;
            if (len > 0) {
                loadMoreGuaranteed();
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void _skip7BitBinary() throws IOException, JsonParseException {
        int origBytes = _readUnsignedVInt();
        int chunks = origBytes / 7;
        int encBytes = chunks * 8;
        int origBytes2 = origBytes - (chunks * 7);
        if (origBytes2 > 0) {
            encBytes += origBytes2 + 1;
        }
        _skipBytes(encBytes);
    }

    private final int _decodeUtf8_2(int c) throws IOException, JsonParseException {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        byte b = bArr[i];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & SmileConstants.BYTE_MARKER_END_OF_CONTENT, this._inputPtr);
        }
        return ((c & 31) << 6) | (b & 63);
    }

    private final int _decodeUtf8_3(int c1) throws IOException, JsonParseException {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        int c12 = c1 & 15;
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        byte b = bArr[i];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & SmileConstants.BYTE_MARKER_END_OF_CONTENT, this._inputPtr);
        }
        int c = (c12 << 6) | (b & 63);
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr2 = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        byte b2 = bArr2[i2];
        if ((b2 & 192) != 128) {
            _reportInvalidOther(b2 & SmileConstants.BYTE_MARKER_END_OF_CONTENT, this._inputPtr);
        }
        return (c << 6) | (b2 & 63);
    }

    private final int _decodeUtf8_3fast(int c1) throws IOException, JsonParseException {
        int c12 = c1 & 15;
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        byte b = bArr[i];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & SmileConstants.BYTE_MARKER_END_OF_CONTENT, this._inputPtr);
        }
        int c = (c12 << 6) | (b & 63);
        byte[] bArr2 = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        byte b2 = bArr2[i2];
        if ((b2 & 192) != 128) {
            _reportInvalidOther(b2 & SmileConstants.BYTE_MARKER_END_OF_CONTENT, this._inputPtr);
        }
        return (c << 6) | (b2 & 63);
    }

    private final int _decodeUtf8_4(int c) throws IOException, JsonParseException {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        byte b = bArr[i];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & SmileConstants.BYTE_MARKER_END_OF_CONTENT, this._inputPtr);
        }
        int c2 = ((c & 7) << 6) | (b & 63);
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr2 = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        byte b2 = bArr2[i2];
        if ((b2 & 192) != 128) {
            _reportInvalidOther(b2 & SmileConstants.BYTE_MARKER_END_OF_CONTENT, this._inputPtr);
        }
        int c3 = (c2 << 6) | (b2 & 63);
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr3 = this._inputBuffer;
        int i3 = this._inputPtr;
        this._inputPtr = i3 + 1;
        byte b3 = bArr3[i3];
        if ((b3 & 192) != 128) {
            _reportInvalidOther(b3 & SmileConstants.BYTE_MARKER_END_OF_CONTENT, this._inputPtr);
        }
        return ((c3 << 6) | (b3 & 63)) - 65536;
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidSharedName(int index) throws IOException {
        if (this._seenNames == null) {
            _reportError("Encountered shared name reference, even though document header explicitly declared no shared name references are included");
        }
        _reportError("Invalid shared name reference " + index + "; only got " + this._seenNameCount + " names in buffer (invalid content)");
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidSharedStringValue(int index) throws IOException {
        if (this._seenStringValues == null) {
            _reportError("Encountered shared text value reference, even though document header did not declared shared text value references may be included");
        }
        _reportError("Invalid shared text value reference " + index + "; only got " + this._seenStringValueCount + " names in buffer (invalid content)");
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidChar(int c) throws JsonParseException {
        if (c < 32) {
            _throwInvalidSpace(c);
        }
        _reportInvalidInitial(c);
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidInitial(int mask) throws JsonParseException {
        _reportError("Invalid UTF-8 start byte 0x" + Integer.toHexString(mask));
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidOther(int mask) throws JsonParseException {
        _reportError("Invalid UTF-8 middle byte 0x" + Integer.toHexString(mask));
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidOther(int mask, int ptr) throws JsonParseException {
        this._inputPtr = ptr;
        _reportInvalidOther(mask);
    }
}
