package org.codehaus.jackson.util;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import org.codehaus.jackson.Base64Variant;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonLocation;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonStreamContext;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.ObjectCodec;
import org.codehaus.jackson.SerializableString;
import org.codehaus.jackson.impl.JsonParserMinimalBase;
import org.codehaus.jackson.impl.JsonReadContext;
import org.codehaus.jackson.impl.JsonWriteContext;
import org.codehaus.jackson.io.SerializedString;
import org.codehaus.jackson.org.objectweb.asm.Opcodes;

public class TokenBuffer extends JsonGenerator {
    protected static final int DEFAULT_PARSER_FEATURES = JsonParser.Feature.collectDefaults();
    protected int _appendOffset;
    protected boolean _closed;
    protected Segment _first;
    protected int _generatorFeatures = DEFAULT_PARSER_FEATURES;
    protected Segment _last;
    protected ObjectCodec _objectCodec;
    protected JsonWriteContext _writeContext = JsonWriteContext.createRootContext();

    public TokenBuffer(ObjectCodec codec) {
        this._objectCodec = codec;
        Segment segment = new Segment();
        this._last = segment;
        this._first = segment;
        this._appendOffset = 0;
    }

    public JsonParser asParser() {
        return asParser(this._objectCodec);
    }

    public JsonParser asParser(ObjectCodec codec) {
        return new Parser(this._first, codec);
    }

    public JsonParser asParser(JsonParser src2) {
        Parser p = new Parser(this._first, src2.getCodec());
        p.setLocation(src2.getTokenLocation());
        return p;
    }

    /* JADX INFO: Multiple debug info for r0v0 java.lang.Object: [D('n' java.lang.Number), D('n' java.lang.Object)] */
    public void serialize(JsonGenerator jgen) throws IOException, JsonGenerationException {
        Segment segment = this._first;
        int ptr = -1;
        while (true) {
            ptr++;
            if (ptr >= 16) {
                ptr = 0;
                segment = segment.next();
                if (segment == null) {
                    return;
                }
            }
            JsonToken t = segment.type(ptr);
            if (t != null) {
                switch (AnonymousClass1.$SwitchMap$org$codehaus$jackson$JsonToken[t.ordinal()]) {
                    case 1:
                        jgen.writeStartObject();
                        break;
                    case 2:
                        jgen.writeEndObject();
                        break;
                    case 3:
                        jgen.writeStartArray();
                        break;
                    case 4:
                        jgen.writeEndArray();
                        break;
                    case 5:
                        Object ob = segment.get(ptr);
                        if (!(ob instanceof SerializableString)) {
                            jgen.writeFieldName((String) ob);
                            break;
                        } else {
                            jgen.writeFieldName((SerializableString) ob);
                            break;
                        }
                    case 6:
                        Object ob2 = segment.get(ptr);
                        if (!(ob2 instanceof SerializableString)) {
                            jgen.writeString((String) ob2);
                            break;
                        } else {
                            jgen.writeString((SerializableString) ob2);
                            break;
                        }
                    case 7:
                        Number n = (Number) segment.get(ptr);
                        if (!(n instanceof BigInteger)) {
                            if (!(n instanceof Long)) {
                                jgen.writeNumber(n.intValue());
                                break;
                            } else {
                                jgen.writeNumber(n.longValue());
                                break;
                            }
                        } else {
                            jgen.writeNumber((BigInteger) n);
                            break;
                        }
                    case 8:
                        Object n2 = segment.get(ptr);
                        if (n2 instanceof BigDecimal) {
                            jgen.writeNumber((BigDecimal) n2);
                            break;
                        } else if (n2 instanceof Float) {
                            jgen.writeNumber(((Float) n2).floatValue());
                            break;
                        } else if (n2 instanceof Double) {
                            jgen.writeNumber(((Double) n2).doubleValue());
                            break;
                        } else if (n2 == null) {
                            jgen.writeNull();
                            break;
                        } else if (n2 instanceof String) {
                            jgen.writeNumber((String) n2);
                            break;
                        } else {
                            throw new JsonGenerationException("Unrecognized value type for VALUE_NUMBER_FLOAT: " + n2.getClass().getName() + ", can not serialize");
                        }
                    case 9:
                        jgen.writeBoolean(true);
                        break;
                    case 10:
                        jgen.writeBoolean(false);
                        break;
                    case 11:
                        jgen.writeNull();
                        break;
                    case Opcodes.FCONST_1:
                        jgen.writeObject(segment.get(ptr));
                        break;
                    default:
                        throw new RuntimeException("Internal error: should never end up through this code path");
                }
            } else {
                return;
            }
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[TokenBuffer: ");
        JsonParser jp = asParser();
        int count = 0;
        while (true) {
            try {
                JsonToken t = jp.nextToken();
                if (t == null) {
                    break;
                }
                if (count < 100) {
                    if (count > 0) {
                        sb.append(", ");
                    }
                    sb.append(t.toString());
                }
                count++;
            } catch (IOException ioe) {
                throw new IllegalStateException(ioe);
            }
        }
        if (count >= 100) {
            sb.append(" ... (truncated ").append(count - 100).append(" entries)");
        }
        sb.append(']');
        return sb.toString();
    }

    public JsonGenerator enable(JsonGenerator.Feature f) {
        this._generatorFeatures |= f.getMask();
        return this;
    }

    public JsonGenerator disable(JsonGenerator.Feature f) {
        this._generatorFeatures &= f.getMask() ^ -1;
        return this;
    }

    public boolean isEnabled(JsonGenerator.Feature f) {
        return (this._generatorFeatures & f.getMask()) != 0;
    }

    public JsonGenerator useDefaultPrettyPrinter() {
        return this;
    }

    public JsonGenerator setCodec(ObjectCodec oc) {
        this._objectCodec = oc;
        return this;
    }

    public ObjectCodec getCodec() {
        return this._objectCodec;
    }

    public final JsonWriteContext getOutputContext() {
        return this._writeContext;
    }

    public void flush() throws IOException {
    }

    public void close() throws IOException {
        this._closed = true;
    }

    public boolean isClosed() {
        return this._closed;
    }

    public final void writeStartArray() throws IOException, JsonGenerationException {
        _append(JsonToken.START_ARRAY);
        this._writeContext = this._writeContext.createChildArrayContext();
    }

    public final void writeEndArray() throws IOException, JsonGenerationException {
        _append(JsonToken.END_ARRAY);
        JsonWriteContext c = this._writeContext.getParent();
        if (c != null) {
            this._writeContext = c;
        }
    }

    public final void writeStartObject() throws IOException, JsonGenerationException {
        _append(JsonToken.START_OBJECT);
        this._writeContext = this._writeContext.createChildObjectContext();
    }

    public final void writeEndObject() throws IOException, JsonGenerationException {
        _append(JsonToken.END_OBJECT);
        JsonWriteContext c = this._writeContext.getParent();
        if (c != null) {
            this._writeContext = c;
        }
    }

    public final void writeFieldName(String name) throws IOException, JsonGenerationException {
        _append(JsonToken.FIELD_NAME, name);
        this._writeContext.writeFieldName(name);
    }

    public void writeFieldName(SerializableString name) throws IOException, JsonGenerationException {
        _append(JsonToken.FIELD_NAME, name);
        this._writeContext.writeFieldName(name.getValue());
    }

    public void writeFieldName(SerializedString name) throws IOException, JsonGenerationException {
        _append(JsonToken.FIELD_NAME, name);
        this._writeContext.writeFieldName(name.getValue());
    }

    public void writeString(String text) throws IOException, JsonGenerationException {
        if (text == null) {
            writeNull();
        } else {
            _append(JsonToken.VALUE_STRING, text);
        }
    }

    public void writeString(char[] text, int offset, int len) throws IOException, JsonGenerationException {
        writeString(new String(text, offset, len));
    }

    public void writeString(SerializableString text) throws IOException, JsonGenerationException {
        if (text == null) {
            writeNull();
        } else {
            _append(JsonToken.VALUE_STRING, text);
        }
    }

    public void writeRawUTF8String(byte[] text, int offset, int length) throws IOException, JsonGenerationException {
        _reportUnsupportedOperation();
    }

    public void writeUTF8String(byte[] text, int offset, int length) throws IOException, JsonGenerationException {
        _reportUnsupportedOperation();
    }

    public void writeRaw(String text) throws IOException, JsonGenerationException {
        _reportUnsupportedOperation();
    }

    public void writeRaw(String text, int offset, int len) throws IOException, JsonGenerationException {
        _reportUnsupportedOperation();
    }

    public void writeRaw(char[] text, int offset, int len) throws IOException, JsonGenerationException {
        _reportUnsupportedOperation();
    }

    public void writeRaw(char c) throws IOException, JsonGenerationException {
        _reportUnsupportedOperation();
    }

    public void writeRawValue(String text) throws IOException, JsonGenerationException {
        _reportUnsupportedOperation();
    }

    public void writeRawValue(String text, int offset, int len) throws IOException, JsonGenerationException {
        _reportUnsupportedOperation();
    }

    public void writeRawValue(char[] text, int offset, int len) throws IOException, JsonGenerationException {
        _reportUnsupportedOperation();
    }

    public void writeNumber(int i) throws IOException, JsonGenerationException {
        _append(JsonToken.VALUE_NUMBER_INT, Integer.valueOf(i));
    }

    public void writeNumber(long l) throws IOException, JsonGenerationException {
        _append(JsonToken.VALUE_NUMBER_INT, Long.valueOf(l));
    }

    public void writeNumber(double d) throws IOException, JsonGenerationException {
        _append(JsonToken.VALUE_NUMBER_FLOAT, Double.valueOf(d));
    }

    public void writeNumber(float f) throws IOException, JsonGenerationException {
        _append(JsonToken.VALUE_NUMBER_FLOAT, Float.valueOf(f));
    }

    public void writeNumber(BigDecimal dec) throws IOException, JsonGenerationException {
        if (dec == null) {
            writeNull();
        } else {
            _append(JsonToken.VALUE_NUMBER_FLOAT, dec);
        }
    }

    public void writeNumber(BigInteger v) throws IOException, JsonGenerationException {
        if (v == null) {
            writeNull();
        } else {
            _append(JsonToken.VALUE_NUMBER_INT, v);
        }
    }

    public void writeNumber(String encodedValue) throws IOException, JsonGenerationException {
        _append(JsonToken.VALUE_NUMBER_FLOAT, encodedValue);
    }

    public void writeBoolean(boolean state) throws IOException, JsonGenerationException {
        _append(state ? JsonToken.VALUE_TRUE : JsonToken.VALUE_FALSE);
    }

    public void writeNull() throws IOException, JsonGenerationException {
        _append(JsonToken.VALUE_NULL);
    }

    public void writeObject(Object value) throws IOException, JsonProcessingException {
        _append(JsonToken.VALUE_EMBEDDED_OBJECT, value);
    }

    public void writeTree(JsonNode rootNode) throws IOException, JsonProcessingException {
        _append(JsonToken.VALUE_EMBEDDED_OBJECT, rootNode);
    }

    public void writeBinary(Base64Variant b64variant, byte[] data, int offset, int len) throws IOException, JsonGenerationException {
        byte[] copy = new byte[len];
        System.arraycopy(data, offset, copy, 0, len);
        writeObject(copy);
    }

    public void copyCurrentEvent(JsonParser jp) throws IOException, JsonProcessingException {
        switch (AnonymousClass1.$SwitchMap$org$codehaus$jackson$JsonToken[jp.getCurrentToken().ordinal()]) {
            case 1:
                writeStartObject();
                return;
            case 2:
                writeEndObject();
                return;
            case 3:
                writeStartArray();
                return;
            case 4:
                writeEndArray();
                return;
            case 5:
                writeFieldName(jp.getCurrentName());
                return;
            case 6:
                if (jp.hasTextCharacters()) {
                    writeString(jp.getTextCharacters(), jp.getTextOffset(), jp.getTextLength());
                    return;
                } else {
                    writeString(jp.getText());
                    return;
                }
            case 7:
                switch (jp.getNumberType()) {
                    case INT:
                        writeNumber(jp.getIntValue());
                        return;
                    case BIG_INTEGER:
                        writeNumber(jp.getBigIntegerValue());
                        return;
                    default:
                        writeNumber(jp.getLongValue());
                        return;
                }
            case 8:
                switch (jp.getNumberType()) {
                    case BIG_DECIMAL:
                        writeNumber(jp.getDecimalValue());
                        return;
                    case FLOAT:
                        writeNumber(jp.getFloatValue());
                        return;
                    default:
                        writeNumber(jp.getDoubleValue());
                        return;
                }
            case 9:
                writeBoolean(true);
                return;
            case 10:
                writeBoolean(false);
                return;
            case 11:
                writeNull();
                return;
            case Opcodes.FCONST_1:
                writeObject(jp.getEmbeddedObject());
                return;
            default:
                throw new RuntimeException("Internal error: should never end up through this code path");
        }
    }

    public void copyCurrentStructure(JsonParser jp) throws IOException, JsonProcessingException {
        JsonToken t = jp.getCurrentToken();
        if (t == JsonToken.FIELD_NAME) {
            writeFieldName(jp.getCurrentName());
            t = jp.nextToken();
        }
        switch (t) {
            case START_OBJECT:
                writeStartObject();
                while (jp.nextToken() != JsonToken.END_OBJECT) {
                    copyCurrentStructure(jp);
                }
                writeEndObject();
                return;
            case END_OBJECT:
            default:
                copyCurrentEvent(jp);
                return;
            case START_ARRAY:
                writeStartArray();
                while (jp.nextToken() != JsonToken.END_ARRAY) {
                    copyCurrentStructure(jp);
                }
                writeEndArray();
                return;
        }
    }

    /* access modifiers changed from: protected */
    public final void _append(JsonToken type) {
        Segment next = this._last.append(this._appendOffset, type);
        if (next == null) {
            this._appendOffset++;
            return;
        }
        this._last = next;
        this._appendOffset = 1;
    }

    /* access modifiers changed from: protected */
    public final void _append(JsonToken type, Object value) {
        Segment next = this._last.append(this._appendOffset, type, value);
        if (next == null) {
            this._appendOffset++;
            return;
        }
        this._last = next;
        this._appendOffset = 1;
    }

    /* access modifiers changed from: protected */
    public void _reportUnsupportedOperation() {
        throw new UnsupportedOperationException("Called operation not supported for TokenBuffer");
    }

    protected static final class Parser extends JsonParserMinimalBase {
        protected transient ByteArrayBuilder _byteBuilder;
        protected boolean _closed;
        protected ObjectCodec _codec;
        protected JsonLocation _location = null;
        protected JsonReadContext _parsingContext;
        protected Segment _segment;
        protected int _segmentPtr;

        public Parser(Segment firstSeg, ObjectCodec codec) {
            super(0);
            this._segment = firstSeg;
            this._segmentPtr = -1;
            this._codec = codec;
            this._parsingContext = JsonReadContext.createRootContext(-1, -1);
        }

        public void setLocation(JsonLocation l) {
            this._location = l;
        }

        public ObjectCodec getCodec() {
            return this._codec;
        }

        public void setCodec(ObjectCodec c) {
            this._codec = c;
        }

        public JsonToken peekNextToken() throws IOException, JsonParseException {
            if (this._closed) {
                return null;
            }
            Segment seg = this._segment;
            int ptr = this._segmentPtr + 1;
            if (ptr >= 16) {
                ptr = 0;
                seg = seg == null ? null : seg.next();
            }
            if (seg == null) {
                return null;
            }
            return seg.type(ptr);
        }

        public void close() throws IOException {
            if (!this._closed) {
                this._closed = true;
            }
        }

        public JsonToken nextToken() throws IOException, JsonParseException {
            if (this._closed || this._segment == null) {
                return null;
            }
            int i = this._segmentPtr + 1;
            this._segmentPtr = i;
            if (i >= 16) {
                this._segmentPtr = 0;
                this._segment = this._segment.next();
                if (this._segment == null) {
                    return null;
                }
            }
            this._currToken = this._segment.type(this._segmentPtr);
            if (this._currToken == JsonToken.FIELD_NAME) {
                Object ob = _currentObject();
                this._parsingContext.setCurrentName(ob instanceof String ? (String) ob : ob.toString());
            } else if (this._currToken == JsonToken.START_OBJECT) {
                this._parsingContext = this._parsingContext.createChildObjectContext(-1, -1);
            } else if (this._currToken == JsonToken.START_ARRAY) {
                this._parsingContext = this._parsingContext.createChildArrayContext(-1, -1);
            } else if (this._currToken == JsonToken.END_OBJECT || this._currToken == JsonToken.END_ARRAY) {
                this._parsingContext = this._parsingContext.getParent();
                if (this._parsingContext == null) {
                    this._parsingContext = JsonReadContext.createRootContext(-1, -1);
                }
            }
            return this._currToken;
        }

        public boolean isClosed() {
            return this._closed;
        }

        public JsonStreamContext getParsingContext() {
            return this._parsingContext;
        }

        public JsonLocation getTokenLocation() {
            return getCurrentLocation();
        }

        public JsonLocation getCurrentLocation() {
            return this._location == null ? JsonLocation.NA : this._location;
        }

        public String getCurrentName() {
            return this._parsingContext.getCurrentName();
        }

        public String getText() {
            if (this._currToken == JsonToken.VALUE_STRING || this._currToken == JsonToken.FIELD_NAME) {
                Object ob = _currentObject();
                if (ob instanceof String) {
                    return (String) ob;
                }
                if (ob == null) {
                    return null;
                }
                return ob.toString();
            } else if (this._currToken == null) {
                return null;
            } else {
                switch (this._currToken) {
                    case VALUE_NUMBER_INT:
                    case VALUE_NUMBER_FLOAT:
                        Object ob2 = _currentObject();
                        if (ob2 == null) {
                            return null;
                        }
                        return ob2.toString();
                    default:
                        return this._currToken.asString();
                }
            }
        }

        public char[] getTextCharacters() {
            String str = getText();
            if (str == null) {
                return null;
            }
            return str.toCharArray();
        }

        public int getTextLength() {
            String str = getText();
            if (str == null) {
                return 0;
            }
            return str.length();
        }

        public int getTextOffset() {
            return 0;
        }

        public boolean hasTextCharacters() {
            return false;
        }

        public BigInteger getBigIntegerValue() throws IOException, JsonParseException {
            Number n = getNumberValue();
            if (n instanceof BigInteger) {
                return (BigInteger) n;
            }
            switch (getNumberType()) {
                case BIG_DECIMAL:
                    return ((BigDecimal) n).toBigInteger();
                default:
                    return BigInteger.valueOf(n.longValue());
            }
        }

        public BigDecimal getDecimalValue() throws IOException, JsonParseException {
            Number n = getNumberValue();
            if (n instanceof BigDecimal) {
                return (BigDecimal) n;
            }
            switch (getNumberType()) {
                case INT:
                case LONG:
                    return BigDecimal.valueOf(n.longValue());
                case BIG_INTEGER:
                    return new BigDecimal((BigInteger) n);
                case BIG_DECIMAL:
                case FLOAT:
                default:
                    return BigDecimal.valueOf(n.doubleValue());
            }
        }

        public double getDoubleValue() throws IOException, JsonParseException {
            return getNumberValue().doubleValue();
        }

        public float getFloatValue() throws IOException, JsonParseException {
            return getNumberValue().floatValue();
        }

        /* Debug info: failed to restart local var, previous not found, register: 2 */
        public int getIntValue() throws IOException, JsonParseException {
            if (this._currToken == JsonToken.VALUE_NUMBER_INT) {
                return ((Number) _currentObject()).intValue();
            }
            return getNumberValue().intValue();
        }

        public long getLongValue() throws IOException, JsonParseException {
            return getNumberValue().longValue();
        }

        public JsonParser.NumberType getNumberType() throws IOException, JsonParseException {
            Number n = getNumberValue();
            if (n instanceof Integer) {
                return JsonParser.NumberType.INT;
            }
            if (n instanceof Long) {
                return JsonParser.NumberType.LONG;
            }
            if (n instanceof Double) {
                return JsonParser.NumberType.DOUBLE;
            }
            if (n instanceof BigDecimal) {
                return JsonParser.NumberType.BIG_DECIMAL;
            }
            if (n instanceof Float) {
                return JsonParser.NumberType.FLOAT;
            }
            if (n instanceof BigInteger) {
                return JsonParser.NumberType.BIG_INTEGER;
            }
            return null;
        }

        public final Number getNumberValue() throws IOException, JsonParseException {
            _checkIsNumber();
            return (Number) _currentObject();
        }

        public Object getEmbeddedObject() {
            if (this._currToken == JsonToken.VALUE_EMBEDDED_OBJECT) {
                return _currentObject();
            }
            return null;
        }

        public byte[] getBinaryValue(Base64Variant b64variant) throws IOException, JsonParseException {
            if (this._currToken == JsonToken.VALUE_EMBEDDED_OBJECT) {
                Object ob = _currentObject();
                if (ob instanceof byte[]) {
                    return (byte[]) ob;
                }
            }
            if (this._currToken != JsonToken.VALUE_STRING) {
                throw _constructError("Current token (" + this._currToken + ") not VALUE_STRING (or VALUE_EMBEDDED_OBJECT with byte[]), can not access as binary");
            }
            String str = getText();
            if (str == null) {
                return null;
            }
            ByteArrayBuilder builder = this._byteBuilder;
            if (builder == null) {
                builder = new ByteArrayBuilder(100);
                this._byteBuilder = builder;
            }
            _decodeBase64(str, builder, b64variant);
            return builder.toByteArray();
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0022, code lost:
            r2 = r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0023, code lost:
            if (r5 < r3) goto L_0x0028;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0025, code lost:
            _reportBase64EOF();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0028, code lost:
            r4 = r5 + 1;
            r1 = r12.charAt(r5);
            r0 = r14.decodeBase64Char(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0032, code lost:
            if (r0 >= 0) goto L_0x0038;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0034, code lost:
            _reportInvalidBase64(r14, r1, 1, null);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0038, code lost:
            r2 = (r2 << 6) | r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x003c, code lost:
            if (r4 < r3) goto L_0x0041;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x003e, code lost:
            _reportBase64EOF();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0041, code lost:
            r5 = r4 + 1;
            r1 = r12.charAt(r4);
            r0 = r14.decodeBase64Char(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x004b, code lost:
            if (r0 >= 0) goto L_0x008b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x004d, code lost:
            if (r0 == -2) goto L_0x0053;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x004f, code lost:
            _reportInvalidBase64(r14, r1, 2, null);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0053, code lost:
            if (r5 < r3) goto L_0x0058;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0055, code lost:
            _reportBase64EOF();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0058, code lost:
            r4 = r5 + 1;
            r1 = r12.charAt(r5);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0062, code lost:
            if (r14.usesPaddingChar(r1) != false) goto L_0x0084;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x0064, code lost:
            _reportInvalidBase64(r14, r1, 3, "expected padding character '" + r14.getPaddingChar() + "'");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0084, code lost:
            r13.append(r2 >> 4);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x008b, code lost:
            r2 = (r2 << 6) | r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x008f, code lost:
            if (r5 < r3) goto L_0x0094;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0091, code lost:
            _reportBase64EOF();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x0094, code lost:
            r4 = r5 + 1;
            r1 = r12.charAt(r5);
            r0 = r14.decodeBase64Char(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x009e, code lost:
            if (r0 >= 0) goto L_0x00ac;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x00a0, code lost:
            if (r0 == -2) goto L_0x00a5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x00a2, code lost:
            _reportInvalidBase64(r14, r1, 3, null);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x00a5, code lost:
            r13.appendTwoBytes(r2 >> 2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x00ac, code lost:
            r13.appendThreeBytes((r2 << 6) | r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:7:0x0018, code lost:
            r0 = r14.decodeBase64Char(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x001c, code lost:
            if (r0 >= 0) goto L_0x0022;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x001e, code lost:
            _reportInvalidBase64(r14, r1, 0, null);
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void _decodeBase64(java.lang.String r12, org.codehaus.jackson.util.ByteArrayBuilder r13, org.codehaus.jackson.Base64Variant r14) throws java.io.IOException, org.codehaus.jackson.JsonParseException {
            /*
                r11 = this;
                r10 = 3
                r9 = -2
                r8 = 0
                r4 = 0
                int r3 = r12.length()
            L_0x0008:
                if (r4 >= r3) goto L_0x0013
            L_0x000a:
                int r5 = r4 + 1
                char r1 = r12.charAt(r4)
                if (r5 < r3) goto L_0x0014
                r4 = r5
            L_0x0013:
                return
            L_0x0014:
                r6 = 32
                if (r1 <= r6) goto L_0x00b5
                int r0 = r14.decodeBase64Char(r1)
                if (r0 >= 0) goto L_0x0022
                r6 = 0
                r11._reportInvalidBase64(r14, r1, r6, r8)
            L_0x0022:
                r2 = r0
                if (r5 < r3) goto L_0x0028
                r11._reportBase64EOF()
            L_0x0028:
                int r4 = r5 + 1
                char r1 = r12.charAt(r5)
                int r0 = r14.decodeBase64Char(r1)
                if (r0 >= 0) goto L_0x0038
                r6 = 1
                r11._reportInvalidBase64(r14, r1, r6, r8)
            L_0x0038:
                int r6 = r2 << 6
                r2 = r6 | r0
                if (r4 < r3) goto L_0x0041
                r11._reportBase64EOF()
            L_0x0041:
                int r5 = r4 + 1
                char r1 = r12.charAt(r4)
                int r0 = r14.decodeBase64Char(r1)
                if (r0 >= 0) goto L_0x008b
                if (r0 == r9) goto L_0x0053
                r6 = 2
                r11._reportInvalidBase64(r14, r1, r6, r8)
            L_0x0053:
                if (r5 < r3) goto L_0x0058
                r11._reportBase64EOF()
            L_0x0058:
                int r4 = r5 + 1
                char r1 = r12.charAt(r5)
                boolean r6 = r14.usesPaddingChar(r1)
                if (r6 != 0) goto L_0x0084
                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                r6.<init>()
                java.lang.String r7 = "expected padding character '"
                java.lang.StringBuilder r6 = r6.append(r7)
                char r7 = r14.getPaddingChar()
                java.lang.StringBuilder r6 = r6.append(r7)
                java.lang.String r7 = "'"
                java.lang.StringBuilder r6 = r6.append(r7)
                java.lang.String r6 = r6.toString()
                r11._reportInvalidBase64(r14, r1, r10, r6)
            L_0x0084:
                int r2 = r2 >> 4
                r13.append(r2)
                goto L_0x0008
            L_0x008b:
                int r6 = r2 << 6
                r2 = r6 | r0
                if (r5 < r3) goto L_0x0094
                r11._reportBase64EOF()
            L_0x0094:
                int r4 = r5 + 1
                char r1 = r12.charAt(r5)
                int r0 = r14.decodeBase64Char(r1)
                if (r0 >= 0) goto L_0x00ac
                if (r0 == r9) goto L_0x00a5
                r11._reportInvalidBase64(r14, r1, r10, r8)
            L_0x00a5:
                int r2 = r2 >> 2
                r13.appendTwoBytes(r2)
                goto L_0x0008
            L_0x00ac:
                int r6 = r2 << 6
                r2 = r6 | r0
                r13.appendThreeBytes(r2)
                goto L_0x0008
            L_0x00b5:
                r4 = r5
                goto L_0x000a
            */
            throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.util.TokenBuffer.Parser._decodeBase64(java.lang.String, org.codehaus.jackson.util.ByteArrayBuilder, org.codehaus.jackson.Base64Variant):void");
        }

        /* access modifiers changed from: protected */
        public final Object _currentObject() {
            return this._segment.get(this._segmentPtr);
        }

        /* access modifiers changed from: protected */
        public final void _checkIsNumber() throws JsonParseException {
            if (this._currToken == null || !this._currToken.isNumeric()) {
                throw _constructError("Current token (" + this._currToken + ") not numeric, can not use numeric value accessors");
            }
        }

        /* access modifiers changed from: protected */
        public void _reportInvalidBase64(Base64Variant b64variant, char ch, int bindex, String msg) throws JsonParseException {
            String base;
            if (ch <= ' ') {
                base = "Illegal white space character (code 0x" + Integer.toHexString(ch) + ") as character #" + (bindex + 1) + " of 4-char base64 unit: can only used between units";
            } else if (b64variant.usesPaddingChar(ch)) {
                base = "Unexpected padding character ('" + b64variant.getPaddingChar() + "') as character #" + (bindex + 1) + " of 4-char base64 unit: padding only legal as 3rd or 4th character";
            } else if (!Character.isDefined(ch) || Character.isISOControl(ch)) {
                base = "Illegal character (code 0x" + Integer.toHexString(ch) + ") in base64 content";
            } else {
                base = "Illegal character '" + ch + "' (code 0x" + Integer.toHexString(ch) + ") in base64 content";
            }
            if (msg != null) {
                base = base + ": " + msg;
            }
            throw _constructError(base);
        }

        /* access modifiers changed from: protected */
        public void _reportBase64EOF() throws JsonParseException {
            throw _constructError("Unexpected end-of-String in base64 content");
        }

        /* access modifiers changed from: protected */
        public void _handleEOF() throws JsonParseException {
            _throwInternal();
        }
    }

    protected static final class Segment {
        public static final int TOKENS_PER_SEGMENT = 16;
        private static final JsonToken[] TOKEN_TYPES_BY_INDEX = new JsonToken[16];
        protected Segment _next;
        protected long _tokenTypes;
        protected final Object[] _tokens = new Object[16];

        static {
            JsonToken[] t = JsonToken.values();
            System.arraycopy(t, 1, TOKEN_TYPES_BY_INDEX, 1, Math.min(15, t.length - 1));
        }

        public JsonToken type(int index) {
            long l = this._tokenTypes;
            if (index > 0) {
                l >>= index << 2;
            }
            return TOKEN_TYPES_BY_INDEX[((int) l) & 15];
        }

        public Object get(int index) {
            return this._tokens[index];
        }

        public Segment next() {
            return this._next;
        }

        public Segment append(int index, JsonToken tokenType) {
            if (index < 16) {
                set(index, tokenType);
                return null;
            }
            this._next = new Segment();
            this._next.set(0, tokenType);
            return this._next;
        }

        public Segment append(int index, JsonToken tokenType, Object value) {
            if (index < 16) {
                set(index, tokenType, value);
                return null;
            }
            this._next = new Segment();
            this._next.set(0, tokenType, value);
            return this._next;
        }

        public void set(int index, JsonToken tokenType) {
            long typeCode = (long) tokenType.ordinal();
            if (index > 0) {
                typeCode <<= index << 2;
            }
            this._tokenTypes |= typeCode;
        }

        public void set(int index, JsonToken tokenType, Object value) {
            this._tokens[index] = value;
            long typeCode = (long) tokenType.ordinal();
            if (index > 0) {
                typeCode <<= index << 2;
            }
            this._tokenTypes |= typeCode;
        }
    }
}
