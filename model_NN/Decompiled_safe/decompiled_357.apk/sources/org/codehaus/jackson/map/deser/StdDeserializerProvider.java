package org.codehaus.jackson.map.deser;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.BeanProperty;
import org.codehaus.jackson.map.ContextualDeserializer;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.DeserializerFactory;
import org.codehaus.jackson.map.DeserializerProvider;
import org.codehaus.jackson.map.Deserializers;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.KeyDeserializer;
import org.codehaus.jackson.map.ResolvableDeserializer;
import org.codehaus.jackson.map.TypeDeserializer;
import org.codehaus.jackson.map.introspect.AnnotatedClass;
import org.codehaus.jackson.map.type.ArrayType;
import org.codehaus.jackson.map.type.CollectionType;
import org.codehaus.jackson.map.type.MapType;
import org.codehaus.jackson.map.util.ClassUtil;
import org.codehaus.jackson.type.JavaType;

public class StdDeserializerProvider extends DeserializerProvider {
    static final HashMap<JavaType, KeyDeserializer> _keyDeserializers = StdKeyDeserializers.constructAll();
    protected final ConcurrentHashMap<JavaType, JsonDeserializer<Object>> _cachedDeserializers;
    protected DeserializerFactory _factory;
    protected final HashMap<JavaType, JsonDeserializer<Object>> _incompleteDeserializers;

    public StdDeserializerProvider() {
        this(BeanDeserializerFactory.instance);
    }

    public StdDeserializerProvider(DeserializerFactory f) {
        this._cachedDeserializers = new ConcurrentHashMap<>(64, 0.75f, 2);
        this._incompleteDeserializers = new HashMap<>(8);
        this._factory = f;
    }

    public DeserializerProvider withAdditionalDeserializers(Deserializers d) {
        this._factory = this._factory.withAdditionalDeserializers(d);
        return this;
    }

    public DeserializerProvider withDeserializerModifier(BeanDeserializerModifier modifier) {
        this._factory = this._factory.withDeserializerModifier(modifier);
        return this;
    }

    public JsonDeserializer<Object> findValueDeserializer(DeserializationConfig config, JavaType propertyType, BeanProperty property) throws JsonMappingException {
        JsonDeserializer<Object> deser = _findCachedDeserializer(propertyType);
        if (deser != null) {
            if (deser instanceof ContextualDeserializer) {
                deser = ((ContextualDeserializer) deser).createContextual(config, property);
            }
            return deser;
        }
        JsonDeserializer<Object> deser2 = _createAndCacheValueDeserializer(config, propertyType, property);
        if (deser2 == null) {
            deser2 = _handleUnknownValueDeserializer(propertyType);
        }
        if (deser2 instanceof ContextualDeserializer) {
            deser2 = ((ContextualDeserializer) deser2).createContextual(config, property);
        }
        return deser2;
    }

    public JsonDeserializer<Object> findTypedValueDeserializer(DeserializationConfig config, JavaType type, BeanProperty property) throws JsonMappingException {
        JsonDeserializer<Object> deser = findValueDeserializer(config, type, property);
        TypeDeserializer typeDeser = this._factory.findTypeDeserializer(config, type, property);
        if (typeDeser != null) {
            return new WrappedDeserializer(typeDeser, deser);
        }
        return deser;
    }

    public KeyDeserializer findKeyDeserializer(DeserializationConfig config, JavaType type, BeanProperty property) throws JsonMappingException {
        Class<?> raw = type.getRawClass();
        if (raw == String.class || raw == Object.class) {
            return null;
        }
        KeyDeserializer kdes = _keyDeserializers.get(type);
        if (kdes != null) {
            return kdes;
        }
        if (type.isEnumType()) {
            return StdKeyDeserializers.constructEnumKeyDeserializer(config, type);
        }
        KeyDeserializer kdes2 = StdKeyDeserializers.findStringBasedKeyDeserializer(config, type);
        if (kdes2 != null) {
            return kdes2;
        }
        return _handleUnknownKeyDeserializer(type);
    }

    public boolean hasValueDeserializerFor(DeserializationConfig config, JavaType type) {
        JsonDeserializer<Object> deser = _findCachedDeserializer(type);
        if (deser == null) {
            try {
                deser = _createAndCacheValueDeserializer(config, type, null);
            } catch (Exception e) {
                return false;
            }
        }
        if (deser != null) {
            return true;
        }
        return false;
    }

    public int cachedDeserializersCount() {
        return this._cachedDeserializers.size();
    }

    public void flushCachedDeserializers() {
        this._cachedDeserializers.clear();
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<Object> _findCachedDeserializer(JavaType type) {
        return this._cachedDeserializers.get(type);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        return r3;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.codehaus.jackson.map.JsonDeserializer<java.lang.Object> _createAndCacheValueDeserializer(org.codehaus.jackson.map.DeserializationConfig r6, org.codehaus.jackson.type.JavaType r7, org.codehaus.jackson.map.BeanProperty r8) throws org.codehaus.jackson.map.JsonMappingException {
        /*
            r5 = this;
            java.util.HashMap<org.codehaus.jackson.type.JavaType, org.codehaus.jackson.map.JsonDeserializer<java.lang.Object>> r2 = r5._incompleteDeserializers
            monitor-enter(r2)
            org.codehaus.jackson.map.JsonDeserializer r1 = r5._findCachedDeserializer(r7)     // Catch:{ all -> 0x0048 }
            if (r1 == 0) goto L_0x000c
            monitor-exit(r2)     // Catch:{ all -> 0x0048 }
            r2 = r1
        L_0x000b:
            return r2
        L_0x000c:
            java.util.HashMap<org.codehaus.jackson.type.JavaType, org.codehaus.jackson.map.JsonDeserializer<java.lang.Object>> r3 = r5._incompleteDeserializers     // Catch:{ all -> 0x0048 }
            int r0 = r3.size()     // Catch:{ all -> 0x0048 }
            if (r0 <= 0) goto L_0x0021
            java.util.HashMap<org.codehaus.jackson.type.JavaType, org.codehaus.jackson.map.JsonDeserializer<java.lang.Object>> r3 = r5._incompleteDeserializers     // Catch:{ all -> 0x0048 }
            java.lang.Object r1 = r3.get(r7)     // Catch:{ all -> 0x0048 }
            org.codehaus.jackson.map.JsonDeserializer r1 = (org.codehaus.jackson.map.JsonDeserializer) r1     // Catch:{ all -> 0x0048 }
            if (r1 == 0) goto L_0x0021
            monitor-exit(r2)     // Catch:{ all -> 0x0048 }
            r2 = r1
            goto L_0x000b
        L_0x0021:
            org.codehaus.jackson.map.JsonDeserializer r3 = r5._createAndCache2(r6, r7, r8)     // Catch:{ all -> 0x0037 }
            if (r0 != 0) goto L_0x0034
            java.util.HashMap<org.codehaus.jackson.type.JavaType, org.codehaus.jackson.map.JsonDeserializer<java.lang.Object>> r4 = r5._incompleteDeserializers     // Catch:{ all -> 0x0048 }
            int r4 = r4.size()     // Catch:{ all -> 0x0048 }
            if (r4 <= 0) goto L_0x0034
            java.util.HashMap<org.codehaus.jackson.type.JavaType, org.codehaus.jackson.map.JsonDeserializer<java.lang.Object>> r4 = r5._incompleteDeserializers     // Catch:{ all -> 0x0048 }
            r4.clear()     // Catch:{ all -> 0x0048 }
        L_0x0034:
            monitor-exit(r2)     // Catch:{ all -> 0x0048 }
            r2 = r3
            goto L_0x000b
        L_0x0037:
            r3 = move-exception
            if (r0 != 0) goto L_0x0047
            java.util.HashMap<org.codehaus.jackson.type.JavaType, org.codehaus.jackson.map.JsonDeserializer<java.lang.Object>> r4 = r5._incompleteDeserializers     // Catch:{ all -> 0x0048 }
            int r4 = r4.size()     // Catch:{ all -> 0x0048 }
            if (r4 <= 0) goto L_0x0047
            java.util.HashMap<org.codehaus.jackson.type.JavaType, org.codehaus.jackson.map.JsonDeserializer<java.lang.Object>> r4 = r5._incompleteDeserializers     // Catch:{ all -> 0x0048 }
            r4.clear()     // Catch:{ all -> 0x0048 }
        L_0x0047:
            throw r3     // Catch:{ all -> 0x0048 }
        L_0x0048:
            r3 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0048 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.map.deser.StdDeserializerProvider._createAndCacheValueDeserializer(org.codehaus.jackson.map.DeserializationConfig, org.codehaus.jackson.type.JavaType, org.codehaus.jackson.map.BeanProperty):org.codehaus.jackson.map.JsonDeserializer");
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<Object> _createAndCache2(DeserializationConfig config, JavaType type, BeanProperty property) throws JsonMappingException {
        try {
            JsonDeserializer<Object> deser = _createDeserializer(config, type, property);
            if (deser == null) {
                return null;
            }
            boolean isResolvable = deser instanceof ResolvableDeserializer;
            boolean addToCache = deser.getClass() == BeanDeserializer.class;
            if (!addToCache) {
                AnnotationIntrospector aintr = config.getAnnotationIntrospector();
                Boolean cacheAnn = aintr.findCachability(AnnotatedClass.construct(deser.getClass(), aintr, null));
                if (cacheAnn != null) {
                    addToCache = cacheAnn.booleanValue();
                }
            }
            if (isResolvable) {
                this._incompleteDeserializers.put(type, deser);
                _resolveDeserializer(config, (ResolvableDeserializer) deser);
                this._incompleteDeserializers.remove(type);
            }
            if (addToCache) {
                this._cachedDeserializers.put(type, deser);
            }
            return deser;
        } catch (IllegalArgumentException iae) {
            throw new JsonMappingException(iae.getMessage(), null, iae);
        }
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<Object> _createDeserializer(DeserializationConfig config, JavaType type, BeanProperty property) throws JsonMappingException {
        if (type.isEnumType()) {
            return this._factory.createEnumDeserializer(config, this, type, property);
        }
        if (type.isContainerType()) {
            if (type instanceof ArrayType) {
                return this._factory.createArrayDeserializer(config, this, (ArrayType) type, property);
            }
            if (type instanceof MapType) {
                return this._factory.createMapDeserializer(config, this, (MapType) type, property);
            }
            if (type instanceof CollectionType) {
                return this._factory.createCollectionDeserializer(config, this, (CollectionType) type, property);
            }
        }
        if (JsonNode.class.isAssignableFrom(type.getRawClass())) {
            return this._factory.createTreeDeserializer(config, this, type, property);
        }
        return this._factory.createBeanDeserializer(config, this, type, property);
    }

    /* access modifiers changed from: protected */
    public void _resolveDeserializer(DeserializationConfig config, ResolvableDeserializer ser) throws JsonMappingException {
        ser.resolve(config, this);
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<Object> _handleUnknownValueDeserializer(JavaType type) throws JsonMappingException {
        if (!ClassUtil.isConcrete(type.getRawClass())) {
            throw new JsonMappingException("Can not find a Value deserializer for abstract type " + type);
        }
        throw new JsonMappingException("Can not find a Value deserializer for type " + type);
    }

    /* access modifiers changed from: protected */
    public KeyDeserializer _handleUnknownKeyDeserializer(JavaType type) throws JsonMappingException {
        throw new JsonMappingException("Can not find a (Map) Key deserializer for type " + type);
    }

    protected static final class WrappedDeserializer extends JsonDeserializer<Object> {
        final JsonDeserializer<Object> _deserializer;
        final TypeDeserializer _typeDeserializer;

        public WrappedDeserializer(TypeDeserializer typeDeser, JsonDeserializer<Object> deser) {
            this._typeDeserializer = typeDeser;
            this._deserializer = deser;
        }

        public Object deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            return this._deserializer.deserializeWithType(jp, ctxt, this._typeDeserializer);
        }

        public Object deserializeWithType(JsonParser jp, DeserializationContext ctxt, TypeDeserializer typeDeserializer) throws IOException, JsonProcessingException {
            throw new IllegalStateException("Type-wrapped deserializer's deserializeWithType should never get called");
        }
    }
}
