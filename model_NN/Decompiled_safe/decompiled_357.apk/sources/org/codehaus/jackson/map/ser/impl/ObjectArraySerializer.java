package org.codehaus.jackson.map.ser.impl;

import java.lang.reflect.Type;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.BeanProperty;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.ResolvableSerializer;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.TypeSerializer;
import org.codehaus.jackson.map.annotate.JacksonStdImpl;
import org.codehaus.jackson.map.ser.ArraySerializers;
import org.codehaus.jackson.map.ser.ContainerSerializerBase;
import org.codehaus.jackson.map.ser.impl.PropertySerializerMap;
import org.codehaus.jackson.map.type.ArrayType;
import org.codehaus.jackson.map.type.TypeFactory;
import org.codehaus.jackson.node.ObjectNode;
import org.codehaus.jackson.schema.JsonSchema;
import org.codehaus.jackson.schema.SchemaAware;
import org.codehaus.jackson.type.JavaType;

@JacksonStdImpl
public class ObjectArraySerializer extends ArraySerializers.AsArraySerializer<Object[]> implements ResolvableSerializer {
    protected PropertySerializerMap _dynamicSerializers = PropertySerializerMap.emptyMap();
    protected JsonSerializer<Object> _elementSerializer;
    protected final JavaType _elementType;
    protected final boolean _staticTyping;

    public ObjectArraySerializer(JavaType elemType, boolean staticTyping, TypeSerializer vts, BeanProperty property) {
        super(Object[].class, vts, property);
        this._elementType = elemType;
        this._staticTyping = staticTyping;
    }

    public ContainerSerializerBase<?> _withValueTypeSerializer(TypeSerializer vts) {
        return new ObjectArraySerializer(this._elementType, this._staticTyping, vts, this._property);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0046, code lost:
        r9 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0048, code lost:
        throw r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x005c, code lost:
        r8 = r8.getCause();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0067, code lost:
        throw ((java.lang.Error) r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x006c, code lost:
        throw org.codehaus.jackson.map.JsonMappingException.wrapWithPath(r8, r9, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x006d, code lost:
        r9 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x006e, code lost:
        r1 = r9;
        r9 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0071, code lost:
        r9 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0072, code lost:
        r1 = r9;
        r9 = r2;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0046 A[ExcHandler: IOException (r9v6 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:9:0x0018] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0068  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void serializeContents(java.lang.Object[] r12, org.codehaus.jackson.JsonGenerator r13, org.codehaus.jackson.map.SerializerProvider r14) throws java.io.IOException, org.codehaus.jackson.JsonGenerationException {
        /*
            r11 = this;
            int r5 = r12.length
            if (r5 != 0) goto L_0x0004
        L_0x0003:
            return
        L_0x0004:
            org.codehaus.jackson.map.JsonSerializer<java.lang.Object> r9 = r11._elementSerializer
            if (r9 == 0) goto L_0x000e
            org.codehaus.jackson.map.JsonSerializer<java.lang.Object> r9 = r11._elementSerializer
            r11.serializeContentsUsing(r12, r13, r14, r9)
            goto L_0x0003
        L_0x000e:
            org.codehaus.jackson.map.TypeSerializer r9 = r11._valueTypeSerializer
            if (r9 == 0) goto L_0x0016
            r11.serializeTypedContents(r12, r13, r14)
            goto L_0x0003
        L_0x0016:
            r3 = 0
            r2 = 0
            org.codehaus.jackson.map.ser.impl.PropertySerializerMap r7 = r11._dynamicSerializers     // Catch:{ IOException -> 0x0046, Exception -> 0x004e }
        L_0x001a:
            if (r3 >= r5) goto L_0x0003
            r2 = r12[r3]     // Catch:{ IOException -> 0x0046, Exception -> 0x0071 }
            if (r2 != 0) goto L_0x0026
            r14.defaultSerializeNull(r13)     // Catch:{ IOException -> 0x0046, Exception -> 0x006d }
        L_0x0023:
            int r3 = r3 + 1
            goto L_0x001a
        L_0x0026:
            java.lang.Class r0 = r2.getClass()     // Catch:{ IOException -> 0x0046, Exception -> 0x006d }
            org.codehaus.jackson.map.JsonSerializer r6 = r7.serializerFor(r0)     // Catch:{ IOException -> 0x0046, Exception -> 0x006d }
            if (r6 != 0) goto L_0x0042
            org.codehaus.jackson.type.JavaType r9 = r11._elementType     // Catch:{ IOException -> 0x0046, Exception -> 0x006d }
            boolean r9 = r9.hasGenericTypes()     // Catch:{ IOException -> 0x0046, Exception -> 0x006d }
            if (r9 == 0) goto L_0x0049
            org.codehaus.jackson.type.JavaType r9 = r11._elementType     // Catch:{ IOException -> 0x0046, Exception -> 0x006d }
            org.codehaus.jackson.type.JavaType r9 = r9.forcedNarrowBy(r0)     // Catch:{ IOException -> 0x0046, Exception -> 0x006d }
            org.codehaus.jackson.map.JsonSerializer r6 = r11._findAndAddDynamic(r7, r9, r14)     // Catch:{ IOException -> 0x0046, Exception -> 0x006d }
        L_0x0042:
            r6.serialize(r2, r13, r14)     // Catch:{ IOException -> 0x0046, Exception -> 0x006d }
            goto L_0x0023
        L_0x0046:
            r9 = move-exception
            r4 = r9
            throw r4
        L_0x0049:
            org.codehaus.jackson.map.JsonSerializer r6 = r11._findAndAddDynamic(r7, r0, r14)     // Catch:{ IOException -> 0x0046, Exception -> 0x006d }
            goto L_0x0042
        L_0x004e:
            r9 = move-exception
            r1 = r9
            r9 = r2
        L_0x0051:
            r8 = r1
        L_0x0052:
            boolean r10 = r8 instanceof java.lang.reflect.InvocationTargetException
            if (r10 == 0) goto L_0x0061
            java.lang.Throwable r10 = r8.getCause()
            if (r10 == 0) goto L_0x0061
            java.lang.Throwable r8 = r8.getCause()
            goto L_0x0052
        L_0x0061:
            boolean r10 = r8 instanceof java.lang.Error
            if (r10 == 0) goto L_0x0068
            java.lang.Error r8 = (java.lang.Error) r8
            throw r8
        L_0x0068:
            org.codehaus.jackson.map.JsonMappingException r9 = org.codehaus.jackson.map.JsonMappingException.wrapWithPath(r8, r9, r3)
            throw r9
        L_0x006d:
            r9 = move-exception
            r1 = r9
            r9 = r2
            goto L_0x0051
        L_0x0071:
            r9 = move-exception
            r1 = r9
            r9 = r2
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.map.ser.impl.ObjectArraySerializer.serializeContents(java.lang.Object[], org.codehaus.jackson.JsonGenerator, org.codehaus.jackson.map.SerializerProvider):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0017, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0019, code lost:
        throw r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x003e, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x003f, code lost:
        r0 = r7;
        r7 = r1;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0017 A[ExcHandler: IOException (r7v4 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:2:0x0007] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0038  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void serializeContentsUsing(java.lang.Object[] r10, org.codehaus.jackson.JsonGenerator r11, org.codehaus.jackson.map.SerializerProvider r12, org.codehaus.jackson.map.JsonSerializer<java.lang.Object> r13) throws java.io.IOException, org.codehaus.jackson.JsonGenerationException {
        /*
            r9 = this;
            int r4 = r10.length
            org.codehaus.jackson.map.TypeSerializer r6 = r9._valueTypeSerializer
            r2 = 0
            r1 = 0
        L_0x0005:
            if (r2 >= r4) goto L_0x003d
            r1 = r10[r2]     // Catch:{ IOException -> 0x0017, Exception -> 0x003e }
            if (r1 != 0) goto L_0x0011
            r12.defaultSerializeNull(r11)     // Catch:{ IOException -> 0x0017, Exception -> 0x001e }
        L_0x000e:
            int r2 = r2 + 1
            goto L_0x0005
        L_0x0011:
            if (r6 != 0) goto L_0x001a
            r13.serialize(r1, r11, r12)     // Catch:{ IOException -> 0x0017, Exception -> 0x001e }
            goto L_0x000e
        L_0x0017:
            r7 = move-exception
            r3 = r7
            throw r3
        L_0x001a:
            r13.serializeWithType(r1, r11, r12, r6)     // Catch:{ IOException -> 0x0017, Exception -> 0x001e }
            goto L_0x000e
        L_0x001e:
            r7 = move-exception
            r0 = r7
            r7 = r1
        L_0x0021:
            r5 = r0
        L_0x0022:
            boolean r8 = r5 instanceof java.lang.reflect.InvocationTargetException
            if (r8 == 0) goto L_0x0031
            java.lang.Throwable r8 = r5.getCause()
            if (r8 == 0) goto L_0x0031
            java.lang.Throwable r5 = r5.getCause()
            goto L_0x0022
        L_0x0031:
            boolean r8 = r5 instanceof java.lang.Error
            if (r8 == 0) goto L_0x0038
            java.lang.Error r5 = (java.lang.Error) r5
            throw r5
        L_0x0038:
            org.codehaus.jackson.map.JsonMappingException r7 = org.codehaus.jackson.map.JsonMappingException.wrapWithPath(r5, r7, r2)
            throw r7
        L_0x003d:
            return
        L_0x003e:
            r7 = move-exception
            r0 = r7
            r7 = r1
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.map.ser.impl.ObjectArraySerializer.serializeContentsUsing(java.lang.Object[], org.codehaus.jackson.JsonGenerator, org.codehaus.jackson.map.SerializerProvider, org.codehaus.jackson.map.JsonSerializer):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0025, code lost:
        r10 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0027, code lost:
        throw r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0028, code lost:
        r10 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0029, code lost:
        r1 = r10;
        r10 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0036, code lost:
        r8 = r8.getCause();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0041, code lost:
        throw ((java.lang.Error) r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0046, code lost:
        throw org.codehaus.jackson.map.JsonMappingException.wrapWithPath(r8, r10, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x004c, code lost:
        r10 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x004d, code lost:
        r1 = r10;
        r10 = r2;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0025 A[ExcHandler: IOException (r10v4 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:1:0x0005] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0042  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void serializeTypedContents(java.lang.Object[] r13, org.codehaus.jackson.JsonGenerator r14, org.codehaus.jackson.map.SerializerProvider r15) throws java.io.IOException, org.codehaus.jackson.JsonGenerationException {
        /*
            r12 = this;
            int r5 = r13.length
            org.codehaus.jackson.map.TypeSerializer r9 = r12._valueTypeSerializer
            r3 = 0
            r2 = 0
            org.codehaus.jackson.map.ser.impl.PropertySerializerMap r7 = r12._dynamicSerializers     // Catch:{ IOException -> 0x0025, Exception -> 0x0028 }
        L_0x0007:
            if (r3 >= r5) goto L_0x0047
            r2 = r13[r3]     // Catch:{ IOException -> 0x0025, Exception -> 0x004c }
            if (r2 != 0) goto L_0x0013
            r15.defaultSerializeNull(r14)     // Catch:{ IOException -> 0x0025, Exception -> 0x0048 }
        L_0x0010:
            int r3 = r3 + 1
            goto L_0x0007
        L_0x0013:
            java.lang.Class r0 = r2.getClass()     // Catch:{ IOException -> 0x0025, Exception -> 0x0048 }
            org.codehaus.jackson.map.JsonSerializer r6 = r7.serializerFor(r0)     // Catch:{ IOException -> 0x0025, Exception -> 0x0048 }
            if (r6 != 0) goto L_0x0021
            org.codehaus.jackson.map.JsonSerializer r6 = r12._findAndAddDynamic(r7, r0, r15)     // Catch:{ IOException -> 0x0025, Exception -> 0x0048 }
        L_0x0021:
            r6.serializeWithType(r2, r14, r15, r9)     // Catch:{ IOException -> 0x0025, Exception -> 0x0048 }
            goto L_0x0010
        L_0x0025:
            r10 = move-exception
            r4 = r10
            throw r4
        L_0x0028:
            r10 = move-exception
            r1 = r10
            r10 = r2
        L_0x002b:
            r8 = r1
        L_0x002c:
            boolean r11 = r8 instanceof java.lang.reflect.InvocationTargetException
            if (r11 == 0) goto L_0x003b
            java.lang.Throwable r11 = r8.getCause()
            if (r11 == 0) goto L_0x003b
            java.lang.Throwable r8 = r8.getCause()
            goto L_0x002c
        L_0x003b:
            boolean r11 = r8 instanceof java.lang.Error
            if (r11 == 0) goto L_0x0042
            java.lang.Error r8 = (java.lang.Error) r8
            throw r8
        L_0x0042:
            org.codehaus.jackson.map.JsonMappingException r10 = org.codehaus.jackson.map.JsonMappingException.wrapWithPath(r8, r10, r3)
            throw r10
        L_0x0047:
            return
        L_0x0048:
            r10 = move-exception
            r1 = r10
            r10 = r2
            goto L_0x002b
        L_0x004c:
            r10 = move-exception
            r1 = r10
            r10 = r2
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.map.ser.impl.ObjectArraySerializer.serializeTypedContents(java.lang.Object[], org.codehaus.jackson.JsonGenerator, org.codehaus.jackson.map.SerializerProvider):void");
    }

    public JsonNode getSchema(SerializerProvider provider, Type typeHint) throws JsonMappingException {
        ObjectNode o = createSchemaNode("array", true);
        if (typeHint != null) {
            JavaType javaType = TypeFactory.type(typeHint);
            if (javaType.isArrayType()) {
                Class<?> componentType = ((ArrayType) javaType).getContentType().getRawClass();
                if (componentType == Object.class) {
                    o.put("items", JsonSchema.getDefaultSchemaNode());
                } else {
                    JsonSerializer<Object> ser = provider.findValueSerializer(componentType, this._property);
                    o.put("items", ser instanceof SchemaAware ? ((SchemaAware) ser).getSchema(provider, null) : JsonSchema.getDefaultSchemaNode());
                }
            }
        }
        return o;
    }

    public void resolve(SerializerProvider provider) throws JsonMappingException {
        if (this._staticTyping) {
            this._elementSerializer = provider.findValueSerializer(this._elementType, this._property);
        }
    }

    /* access modifiers changed from: protected */
    public final JsonSerializer<Object> _findAndAddDynamic(PropertySerializerMap map, Class<?> type, SerializerProvider provider) throws JsonMappingException {
        PropertySerializerMap.SerializerAndMapResult result = map.findAndAddSerializer(type, provider, this._property);
        if (map != result.map) {
            this._dynamicSerializers = result.map;
        }
        return result.serializer;
    }

    /* access modifiers changed from: protected */
    public final JsonSerializer<Object> _findAndAddDynamic(PropertySerializerMap map, JavaType type, SerializerProvider provider) throws JsonMappingException {
        PropertySerializerMap.SerializerAndMapResult result = map.findAndAddSerializer(type, provider, this._property);
        if (map != result.map) {
            this._dynamicSerializers = result.map;
        }
        return result.serializer;
    }
}
