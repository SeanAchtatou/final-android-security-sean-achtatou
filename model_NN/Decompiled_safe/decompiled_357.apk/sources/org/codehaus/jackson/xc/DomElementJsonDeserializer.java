package org.codehaus.jackson.xc;

import java.io.IOException;
import java.util.Iterator;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.deser.StdDeserializer;
import org.codehaus.jackson.node.ArrayNode;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class DomElementJsonDeserializer extends StdDeserializer<Element> {
    private final DocumentBuilder builder;

    public DomElementJsonDeserializer() {
        super(Element.class);
        try {
            DocumentBuilderFactory bf = DocumentBuilderFactory.newInstance();
            bf.setNamespaceAware(true);
            this.builder = bf.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new RuntimeException();
        }
    }

    public DomElementJsonDeserializer(DocumentBuilder builder2) {
        super(Element.class);
        this.builder = builder2;
    }

    public Element deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        return fromNode(this.builder.newDocument(), jp.readValueAsTree());
    }

    /* access modifiers changed from: protected */
    public Element fromNode(Document document, JsonNode jsonNode) throws IOException {
        String ns;
        String name;
        String name2;
        String value;
        String ns2;
        String name3;
        String value2;
        if (jsonNode.get("namespace") != null) {
            ns = jsonNode.get("namespace").getValueAsText();
        } else {
            ns = null;
        }
        if (jsonNode.get("name") != null) {
            name = jsonNode.get("name").getValueAsText();
        } else {
            name = null;
        }
        if (name == null) {
            throw new JsonMappingException("No name for DOM element was provided in the JSON object.");
        }
        Element element = document.createElementNS(ns, name);
        JsonNode attributesNode = jsonNode.get("attributes");
        if (attributesNode != null && (attributesNode instanceof ArrayNode)) {
            Iterator<JsonNode> atts = attributesNode.getElements();
            while (atts.hasNext()) {
                JsonNode node = atts.next();
                if (node.get("namespace") != null) {
                    ns2 = node.get("namespace").getValueAsText();
                } else {
                    ns2 = null;
                }
                if (node.get("name") != null) {
                    name3 = node.get("name").getValueAsText();
                } else {
                    name3 = null;
                }
                if (node.get("$") != null) {
                    value2 = node.get("$").getValueAsText();
                } else {
                    value2 = null;
                }
                if (name3 != null) {
                    element.setAttributeNS(ns2, name3, value2);
                }
            }
        }
        JsonNode childsNode = jsonNode.get("children");
        if (childsNode != null && (childsNode instanceof ArrayNode)) {
            Iterator<JsonNode> els = childsNode.getElements();
            while (els.hasNext()) {
                JsonNode node2 = els.next();
                if (node2.get("name") != null) {
                    name2 = node2.get("name").getValueAsText();
                } else {
                    name2 = null;
                }
                if (node2.get("$") != null) {
                    value = node2.get("$").getValueAsText();
                } else {
                    value = null;
                }
                if (value != null) {
                    element.appendChild(document.createTextNode(value));
                } else if (name2 != null) {
                    element.appendChild(fromNode(document, node2));
                }
            }
        }
        return element;
    }
}
