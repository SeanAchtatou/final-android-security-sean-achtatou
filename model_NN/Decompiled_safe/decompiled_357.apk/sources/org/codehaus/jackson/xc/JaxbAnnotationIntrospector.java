package org.codehaus.jackson.xc;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.ref.SoftReference;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessOrder;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorOrder;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
import org.codehaus.jackson.Version;
import org.codehaus.jackson.Versioned;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.BeanProperty;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.KeyDeserializer;
import org.codehaus.jackson.map.annotate.JsonCachable;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.introspect.Annotated;
import org.codehaus.jackson.map.introspect.AnnotatedClass;
import org.codehaus.jackson.map.introspect.AnnotatedConstructor;
import org.codehaus.jackson.map.introspect.AnnotatedField;
import org.codehaus.jackson.map.introspect.AnnotatedMember;
import org.codehaus.jackson.map.introspect.AnnotatedMethod;
import org.codehaus.jackson.map.introspect.AnnotatedParameter;
import org.codehaus.jackson.map.jsontype.NamedType;
import org.codehaus.jackson.map.jsontype.TypeResolverBuilder;
import org.codehaus.jackson.map.jsontype.impl.StdTypeResolverBuilder;
import org.codehaus.jackson.map.util.ClassUtil;
import org.codehaus.jackson.type.JavaType;
import org.codehaus.jackson.util.VersionUtil;

public class JaxbAnnotationIntrospector extends AnnotationIntrospector implements Versioned {
    protected static final String MARKER_FOR_DEFAULT = "##default";
    private static final ThreadLocal<SoftReference<PropertyDescriptors>> _propertyDescriptors = new ThreadLocal<>();
    protected final JsonDeserializer<?> _dataHandlerDeserializer;
    protected final JsonSerializer<?> _dataHandlerSerializer;
    protected final String _jaxbPackageName = XmlElement.class.getPackage().getName();

    /* JADX WARN: Type inference failed for: r3v6, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r3v9, types: [java.lang.Object] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public JaxbAnnotationIntrospector() {
        /*
            r4 = this;
            r4.<init>()
            java.lang.Class<javax.xml.bind.annotation.XmlElement> r3 = javax.xml.bind.annotation.XmlElement.class
            java.lang.Package r3 = r3.getPackage()
            java.lang.String r3 = r3.getName()
            r4._jaxbPackageName = r3
            r2 = 0
            r1 = 0
            java.lang.String r3 = "org.codehaus.jackson.xc.DataHandlerJsonSerializer"
            java.lang.Class r3 = java.lang.Class.forName(r3)     // Catch:{ Throwable -> 0x0032 }
            java.lang.Object r3 = r3.newInstance()     // Catch:{ Throwable -> 0x0032 }
            r0 = r3
            org.codehaus.jackson.map.JsonSerializer r0 = (org.codehaus.jackson.map.JsonSerializer) r0     // Catch:{ Throwable -> 0x0032 }
            r2 = r0
            java.lang.String r3 = "org.codehaus.jackson.xc.DataHandlerJsonDeserializer"
            java.lang.Class r3 = java.lang.Class.forName(r3)     // Catch:{ Throwable -> 0x0032 }
            java.lang.Object r3 = r3.newInstance()     // Catch:{ Throwable -> 0x0032 }
            r0 = r3
            org.codehaus.jackson.map.JsonDeserializer r0 = (org.codehaus.jackson.map.JsonDeserializer) r0     // Catch:{ Throwable -> 0x0032 }
            r1 = r0
        L_0x002d:
            r4._dataHandlerSerializer = r2
            r4._dataHandlerDeserializer = r1
            return
        L_0x0032:
            r3 = move-exception
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.xc.JaxbAnnotationIntrospector.<init>():void");
    }

    public Version version() {
        return VersionUtil.versionFor(getClass());
    }

    public boolean isHandled(Annotation ann) {
        Class<?> cls = ann.annotationType();
        Package pkg = cls.getPackage();
        if ((pkg != null ? pkg.getName() : cls.getName()).startsWith(this._jaxbPackageName)) {
            return true;
        }
        if (cls == JsonCachable.class) {
            return true;
        }
        return false;
    }

    public Boolean findCachability(AnnotatedClass ac) {
        JsonCachable ann = (JsonCachable) ac.getAnnotation(JsonCachable.class);
        if (ann != null) {
            return ann.value() ? Boolean.TRUE : Boolean.FALSE;
        }
        return null;
    }

    public String findRootName(AnnotatedClass ac) {
        XmlRootElement elem = findRootElementAnnotation(ac);
        if (elem == null) {
            return null;
        }
        String name = elem.name();
        if (MARKER_FOR_DEFAULT.equals(name)) {
            return "";
        }
        return name;
    }

    public String[] findPropertiesToIgnore(AnnotatedClass ac) {
        return null;
    }

    public Boolean findIgnoreUnknownProperties(AnnotatedClass ac) {
        return null;
    }

    public Boolean isIgnorableType(AnnotatedClass ac) {
        return null;
    }

    /* JADX WARN: Type inference failed for: r1v3, types: [org.codehaus.jackson.map.introspect.VisibilityChecker] */
    /* JADX WARN: Type inference failed for: r1v8, types: [org.codehaus.jackson.map.introspect.VisibilityChecker] */
    /* JADX WARN: Type inference failed for: r1v13, types: [org.codehaus.jackson.map.introspect.VisibilityChecker] */
    /* JADX WARN: Type inference failed for: r1v18, types: [org.codehaus.jackson.map.introspect.VisibilityChecker] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public org.codehaus.jackson.map.introspect.VisibilityChecker<?> findAutoDetectVisibility(org.codehaus.jackson.map.introspect.AnnotatedClass r4, org.codehaus.jackson.map.introspect.VisibilityChecker<?> r5) {
        /*
            r3 = this;
            javax.xml.bind.annotation.XmlAccessType r0 = r3.findAccessType(r4)
            if (r0 != 0) goto L_0x0008
            r1 = r5
        L_0x0007:
            return r1
        L_0x0008:
            int[] r1 = org.codehaus.jackson.xc.JaxbAnnotationIntrospector.AnonymousClass1.$SwitchMap$javax$xml$bind$annotation$XmlAccessType
            int r2 = r0.ordinal()
            r1 = r1[r2]
            switch(r1) {
                case 1: goto L_0x0015;
                case 2: goto L_0x002e;
                case 3: goto L_0x0047;
                case 4: goto L_0x0060;
                default: goto L_0x0013;
            }
        L_0x0013:
            r1 = r5
            goto L_0x0007
        L_0x0015:
            org.codehaus.jackson.annotate.JsonAutoDetect$Visibility r1 = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.ANY
            org.codehaus.jackson.map.introspect.VisibilityChecker r1 = r5.withFieldVisibility(r1)
            org.codehaus.jackson.annotate.JsonAutoDetect$Visibility r2 = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.NONE
            org.codehaus.jackson.map.introspect.VisibilityChecker r1 = r1.withSetterVisibility(r2)
            org.codehaus.jackson.annotate.JsonAutoDetect$Visibility r2 = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.NONE
            org.codehaus.jackson.map.introspect.VisibilityChecker r1 = r1.withGetterVisibility(r2)
            org.codehaus.jackson.annotate.JsonAutoDetect$Visibility r2 = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.NONE
            org.codehaus.jackson.map.introspect.VisibilityChecker r1 = r1.withIsGetterVisibility(r2)
            goto L_0x0007
        L_0x002e:
            org.codehaus.jackson.annotate.JsonAutoDetect$Visibility r1 = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.NONE
            org.codehaus.jackson.map.introspect.VisibilityChecker r1 = r5.withFieldVisibility(r1)
            org.codehaus.jackson.annotate.JsonAutoDetect$Visibility r2 = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.NONE
            org.codehaus.jackson.map.introspect.VisibilityChecker r1 = r1.withSetterVisibility(r2)
            org.codehaus.jackson.annotate.JsonAutoDetect$Visibility r2 = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.NONE
            org.codehaus.jackson.map.introspect.VisibilityChecker r1 = r1.withGetterVisibility(r2)
            org.codehaus.jackson.annotate.JsonAutoDetect$Visibility r2 = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.NONE
            org.codehaus.jackson.map.introspect.VisibilityChecker r1 = r1.withIsGetterVisibility(r2)
            goto L_0x0007
        L_0x0047:
            org.codehaus.jackson.annotate.JsonAutoDetect$Visibility r1 = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.NONE
            org.codehaus.jackson.map.introspect.VisibilityChecker r1 = r5.withFieldVisibility(r1)
            org.codehaus.jackson.annotate.JsonAutoDetect$Visibility r2 = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.PUBLIC_ONLY
            org.codehaus.jackson.map.introspect.VisibilityChecker r1 = r1.withSetterVisibility(r2)
            org.codehaus.jackson.annotate.JsonAutoDetect$Visibility r2 = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.PUBLIC_ONLY
            org.codehaus.jackson.map.introspect.VisibilityChecker r1 = r1.withGetterVisibility(r2)
            org.codehaus.jackson.annotate.JsonAutoDetect$Visibility r2 = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.PUBLIC_ONLY
            org.codehaus.jackson.map.introspect.VisibilityChecker r1 = r1.withIsGetterVisibility(r2)
            goto L_0x0007
        L_0x0060:
            org.codehaus.jackson.annotate.JsonAutoDetect$Visibility r1 = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.PUBLIC_ONLY
            org.codehaus.jackson.map.introspect.VisibilityChecker r1 = r5.withFieldVisibility(r1)
            org.codehaus.jackson.annotate.JsonAutoDetect$Visibility r2 = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.PUBLIC_ONLY
            org.codehaus.jackson.map.introspect.VisibilityChecker r1 = r1.withSetterVisibility(r2)
            org.codehaus.jackson.annotate.JsonAutoDetect$Visibility r2 = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.PUBLIC_ONLY
            org.codehaus.jackson.map.introspect.VisibilityChecker r1 = r1.withGetterVisibility(r2)
            org.codehaus.jackson.annotate.JsonAutoDetect$Visibility r2 = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.PUBLIC_ONLY
            org.codehaus.jackson.map.introspect.VisibilityChecker r1 = r1.withIsGetterVisibility(r2)
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.xc.JaxbAnnotationIntrospector.findAutoDetectVisibility(org.codehaus.jackson.map.introspect.AnnotatedClass, org.codehaus.jackson.map.introspect.VisibilityChecker):org.codehaus.jackson.map.introspect.VisibilityChecker");
    }

    /* renamed from: org.codehaus.jackson.xc.JaxbAnnotationIntrospector$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$javax$xml$bind$annotation$XmlAccessType = new int[XmlAccessType.values().length];

        static {
            try {
                $SwitchMap$javax$xml$bind$annotation$XmlAccessType[XmlAccessType.FIELD.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$javax$xml$bind$annotation$XmlAccessType[XmlAccessType.NONE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$javax$xml$bind$annotation$XmlAccessType[XmlAccessType.PROPERTY.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$javax$xml$bind$annotation$XmlAccessType[XmlAccessType.PUBLIC_MEMBER.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean isPropertiesAccessible(Annotated ac) {
        XmlAccessType accessType = findAccessType(ac);
        if (accessType == null) {
            accessType = XmlAccessType.PUBLIC_MEMBER;
        }
        return accessType == XmlAccessType.PUBLIC_MEMBER || accessType == XmlAccessType.PROPERTY;
    }

    /* access modifiers changed from: protected */
    public XmlAccessType findAccessType(Annotated ac) {
        XmlAccessorType at = findAnnotation(XmlAccessorType.class, ac, true, true, true);
        if (at == null) {
            return null;
        }
        return at.value();
    }

    public TypeResolverBuilder<?> findTypeResolver(AnnotatedClass ac, JavaType baseType) {
        return null;
    }

    public TypeResolverBuilder<?> findPropertyTypeResolver(AnnotatedMember am, JavaType baseType) {
        if (baseType.isContainerType()) {
            return null;
        }
        return _typeResolverFromXmlElements(am);
    }

    public TypeResolverBuilder<?> findPropertyContentTypeResolver(AnnotatedMember am, JavaType containerType) {
        if (containerType.isContainerType()) {
            return _typeResolverFromXmlElements(am);
        }
        throw new IllegalArgumentException("Must call method with a container type (got " + containerType + ")");
    }

    /* access modifiers changed from: protected */
    public TypeResolverBuilder<?> _typeResolverFromXmlElements(AnnotatedMember am) {
        XmlElements elems = findAnnotation(XmlElements.class, am, false, false, false);
        XmlElementRefs elemRefs = findAnnotation(XmlElementRefs.class, am, false, false, false);
        if (elems == null && elemRefs == null) {
            return null;
        }
        return new StdTypeResolverBuilder().init(JsonTypeInfo.Id.NAME, null).inclusion(JsonTypeInfo.As.WRAPPER_OBJECT);
    }

    public List<NamedType> findSubtypes(Annotated a) {
        XmlRootElement rootElement;
        XmlElements elems = findAnnotation(XmlElements.class, a, false, false, false);
        if (elems != null) {
            ArrayList<NamedType> result = new ArrayList<>();
            for (XmlElement elem : elems.value()) {
                String name = elem.name();
                if (MARKER_FOR_DEFAULT.equals(name)) {
                    name = null;
                }
                result.add(new NamedType(elem.type(), name));
            }
            return result;
        }
        XmlElementRefs elemRefs = findAnnotation(XmlElementRefs.class, a, false, false, false);
        if (elemRefs == null) {
            return null;
        }
        ArrayList<NamedType> result2 = new ArrayList<>();
        for (XmlElementRef elemRef : elemRefs.value()) {
            Class<?> refType = elemRef.type();
            if (!JAXBElement.class.isAssignableFrom(refType)) {
                String name2 = elemRef.name();
                if ((name2 == null || MARKER_FOR_DEFAULT.equals(name2)) && (rootElement = refType.getAnnotation(XmlRootElement.class)) != null) {
                    name2 = rootElement.name();
                }
                if (name2 == null || MARKER_FOR_DEFAULT.equals(name2)) {
                    name2 = Introspector.decapitalize(refType.getSimpleName());
                }
                result2.add(new NamedType(refType, name2));
            }
        }
        return result2;
    }

    public String findTypeName(AnnotatedClass ac) {
        XmlType type = findAnnotation(XmlType.class, ac, false, false, false);
        if (type != null) {
            String name = type.name();
            if (!MARKER_FOR_DEFAULT.equals(name)) {
                return name;
            }
        }
        return null;
    }

    public boolean isIgnorableMethod(AnnotatedMethod m) {
        return m.getAnnotation(XmlTransient.class) != null;
    }

    public boolean isIgnorableConstructor(AnnotatedConstructor c) {
        return false;
    }

    public boolean isIgnorableField(AnnotatedField f) {
        return f.getAnnotation(XmlTransient.class) != null;
    }

    public JsonSerializer<?> findSerializer(Annotated am, BeanProperty property) {
        XmlAdapter<Object, Object> adapter = findAdapter(am, true);
        if (adapter != null) {
            return new XmlAdapterJsonSerializer(adapter, property);
        }
        Class<?> type = am.getRawType();
        if (type == null || this._dataHandlerSerializer == null || !isDataHandler(type)) {
            return null;
        }
        return this._dataHandlerSerializer;
    }

    private boolean isDataHandler(Class<?> type) {
        return type != null && !Object.class.equals(type) && ("javax.activation.DataHandler".equals(type.getName()) || isDataHandler(type.getSuperclass()));
    }

    public Class<?> findSerializationType(Annotated a) {
        XmlElement annotation = findAnnotation(XmlElement.class, a, false, false, false);
        if (annotation == null || annotation.type() == XmlElement.DEFAULT.class) {
            return null;
        }
        if (isIndexedType(a.getRawType())) {
            return null;
        }
        return annotation.type();
    }

    public JsonSerialize.Inclusion findSerializationInclusion(Annotated a, JsonSerialize.Inclusion defValue) {
        XmlElementWrapper w = a.getAnnotation(XmlElementWrapper.class);
        if (w != null) {
            return w.nillable() ? JsonSerialize.Inclusion.ALWAYS : JsonSerialize.Inclusion.NON_NULL;
        }
        XmlElement e = a.getAnnotation(XmlElement.class);
        if (e != null) {
            return e.nillable() ? JsonSerialize.Inclusion.ALWAYS : JsonSerialize.Inclusion.NON_NULL;
        }
        return defValue;
    }

    public JsonSerialize.Typing findSerializationTyping(Annotated a) {
        return null;
    }

    public Class<?>[] findSerializationViews(Annotated a) {
        return null;
    }

    public String[] findSerializationPropertyOrder(AnnotatedClass ac) {
        String[] order;
        XmlType type = findAnnotation(XmlType.class, ac, true, true, true);
        if (type == null || (order = type.propOrder()) == null || order.length == 0) {
            return null;
        }
        PropertyDescriptors props = getDescriptors(ac.getRawType());
        int len = order.length;
        for (int i = 0; i < len; i++) {
            String propName = order[i];
            if (props.findByPropertyName(propName) == null && propName.length() != 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("get");
                sb.append(Character.toUpperCase(propName.charAt(0)));
                if (propName.length() > 1) {
                    sb.append(propName.substring(1));
                }
                PropertyDescriptor desc = props.findByMethodName(sb.toString());
                if (desc != null) {
                    order[i] = desc.getName();
                }
            }
        }
        return order;
    }

    public Boolean findSerializationSortAlphabetically(AnnotatedClass ac) {
        XmlAccessorOrder order = findAnnotation(XmlAccessorOrder.class, ac, true, true, true);
        if (order == null) {
            return null;
        }
        return Boolean.valueOf(order.value() == XmlAccessOrder.ALPHABETICAL);
    }

    public String findGettablePropertyName(AnnotatedMethod am) {
        PropertyDescriptor desc = findPropertyDescriptor(am);
        if (desc != null) {
            return findJaxbSpecifiedPropertyName(desc);
        }
        return null;
    }

    public boolean hasAsValueAnnotation(AnnotatedMethod am) {
        return false;
    }

    public String findEnumValue(Enum<?> e) {
        Class<?> enumClass = e.getDeclaringClass();
        String enumValue = e.name();
        try {
            XmlEnumValue xmlEnumValue = enumClass.getDeclaredField(enumValue).getAnnotation(XmlEnumValue.class);
            return xmlEnumValue != null ? xmlEnumValue.value() : enumValue;
        } catch (NoSuchFieldException e2) {
            throw new IllegalStateException("Could not locate Enum entry '" + enumValue + "' (Enum class " + enumClass.getName() + ")", e2);
        }
    }

    public String findSerializablePropertyName(AnnotatedField af) {
        if (isInvisible(af)) {
            return null;
        }
        Field field = af.getAnnotated();
        String name = findJaxbPropertyName(field, field.getType(), "");
        return name == null ? field.getName() : name;
    }

    public JsonDeserializer<?> findDeserializer(Annotated am, BeanProperty property) {
        XmlAdapter<Object, Object> adapter = findAdapter(am, false);
        if (adapter != null) {
            return new XmlAdapterJsonDeserializer(adapter, property);
        }
        Class<?> type = am.getRawType();
        if (type == null || this._dataHandlerDeserializer == null || !isDataHandler(type)) {
            return null;
        }
        return this._dataHandlerDeserializer;
    }

    public Class<KeyDeserializer> findKeyDeserializer(Annotated am) {
        return null;
    }

    public Class<JsonDeserializer<?>> findContentDeserializer(Annotated am) {
        return null;
    }

    public Class<?> findDeserializationType(Annotated a, JavaType baseType, String propName) {
        if (!baseType.isContainerType()) {
            return _doFindDeserializationType(a, baseType, propName);
        }
        return null;
    }

    public Class<?> findDeserializationKeyType(Annotated am, JavaType baseKeyType, String propName) {
        return null;
    }

    public Class<?> findDeserializationContentType(Annotated a, JavaType baseContentType, String propName) {
        return _doFindDeserializationType(a, baseContentType, propName);
    }

    /* access modifiers changed from: protected */
    public Class<?> _doFindDeserializationType(Annotated a, JavaType baseType, String propName) {
        XmlElement annotation;
        Class<?> type;
        if (a.hasAnnotation(XmlJavaTypeAdapter.class)) {
            return null;
        }
        XmlElement annotation2 = findAnnotation(XmlElement.class, a, false, false, false);
        if (annotation2 != null && (type = annotation2.type()) != XmlElement.DEFAULT.class) {
            return type;
        }
        if (!(a instanceof AnnotatedMethod) || propName == null || (annotation = findFieldAnnotation(XmlElement.class, ((AnnotatedMethod) a).getDeclaringClass(), propName)) == null || annotation.type() == XmlElement.DEFAULT.class) {
            return null;
        }
        return annotation.type();
    }

    public String findSettablePropertyName(AnnotatedMethod am) {
        PropertyDescriptor desc = findPropertyDescriptor(am);
        if (desc != null) {
            return findJaxbSpecifiedPropertyName(desc);
        }
        return null;
    }

    public boolean hasAnySetterAnnotation(AnnotatedMethod am) {
        return false;
    }

    public boolean hasCreatorAnnotation(Annotated am) {
        return false;
    }

    public String findDeserializablePropertyName(AnnotatedField af) {
        if (isInvisible(af)) {
            return null;
        }
        Field field = af.getAnnotated();
        String name = findJaxbPropertyName(field, field.getType(), "");
        return name == null ? field.getName() : name;
    }

    public String findPropertyNameForParam(AnnotatedParameter param) {
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean isInvisible(AnnotatedField f) {
        boolean invisible = true;
        for (Annotation annotation : f.getAnnotated().getDeclaredAnnotations()) {
            if (isHandled(annotation)) {
                invisible = false;
            }
        }
        if (!invisible) {
            return invisible;
        }
        XmlAccessType accessType = XmlAccessType.PUBLIC_MEMBER;
        XmlAccessorType at = findAnnotation(XmlAccessorType.class, f, true, true, true);
        if (at != null) {
            accessType = at.value();
        }
        return accessType != XmlAccessType.FIELD && (accessType != XmlAccessType.PUBLIC_MEMBER || !Modifier.isPublic(f.getAnnotated().getModifiers()));
    }

    /* access modifiers changed from: protected */
    public <A extends Annotation> A findAnnotation(Class<A> annotationClass, Annotated annotated, boolean includePackage, boolean includeClass, boolean includeSuperclasses) {
        Class memberClass;
        A annotation;
        PropertyDescriptor pd;
        A annotation2;
        if ((annotated instanceof AnnotatedMethod) && (pd = findPropertyDescriptor((AnnotatedMethod) annotated)) != null && (annotation2 = new AnnotatedProperty(pd, null).getAnnotation(annotationClass)) != null) {
            return annotation2;
        }
        AnnotatedElement annType = annotated.getAnnotated();
        if (annotated instanceof AnnotatedParameter) {
            AnnotatedParameter param = (AnnotatedParameter) annotated;
            A annotation3 = param.getAnnotation(annotationClass);
            if (annotation3 != null) {
                return annotation3;
            }
            memberClass = param.getMember().getDeclaringClass();
        } else {
            A annotation4 = annType.getAnnotation(annotationClass);
            if (annotation4 != null) {
                return annotation4;
            }
            if (annType instanceof Member) {
                memberClass = ((Member) annType).getDeclaringClass();
                if (includeClass && (annotation = memberClass.getAnnotation(annotationClass)) != null) {
                    return annotation;
                }
            } else if (annType instanceof Class) {
                memberClass = (Class) annType;
            } else {
                throw new IllegalStateException("Unsupported annotated member: " + annotated.getClass().getName());
            }
        }
        if (memberClass != null) {
            if (includeSuperclasses) {
                Class superclass = memberClass.getSuperclass();
                while (superclass != null && superclass != Object.class) {
                    A annotation5 = superclass.getAnnotation(annotationClass);
                    if (annotation5 != null) {
                        return annotation5;
                    }
                    superclass = superclass.getSuperclass();
                }
            }
            if (includePackage) {
                return memberClass.getPackage().getAnnotation(annotationClass);
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public <A extends Annotation> A findFieldAnnotation(Class<A> annotationType, Class<?> cls, String fieldName) {
        do {
            for (Field f : cls.getDeclaredFields()) {
                if (fieldName.equals(f.getName())) {
                    return f.getAnnotation(annotationType);
                }
            }
            if (cls.isInterface() || cls == Object.class) {
                return null;
            }
            cls = cls.getSuperclass();
        } while (cls != null);
        return null;
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    /* access modifiers changed from: protected */
    public PropertyDescriptors getDescriptors(Class<?> forClass) {
        SoftReference<PropertyDescriptors> ref = _propertyDescriptors.get();
        PropertyDescriptors descriptors = ref == null ? null : (PropertyDescriptors) ref.get();
        if (descriptors == null || descriptors.getBeanClass() != forClass) {
            try {
                descriptors = PropertyDescriptors.find(forClass);
                _propertyDescriptors.set(new SoftReference(descriptors));
            } catch (IntrospectionException e) {
                throw new IllegalArgumentException("Problem introspecting bean properties: " + e.getMessage(), e);
            }
        }
        return descriptors;
    }

    /* access modifiers changed from: protected */
    public PropertyDescriptor findPropertyDescriptor(AnnotatedMethod m) {
        return getDescriptors(m.getDeclaringClass()).findByMethodName(m.getName());
    }

    /* access modifiers changed from: protected */
    public String findJaxbSpecifiedPropertyName(PropertyDescriptor prop) {
        return findJaxbPropertyName(new AnnotatedProperty(prop, null), prop.getPropertyType(), prop.getName());
    }

    protected static String findJaxbPropertyName(AnnotatedElement ae, Class<?> aeType, String defaultName) {
        XmlRootElement rootElement;
        XmlElementWrapper elementWrapper = ae.getAnnotation(XmlElementWrapper.class);
        if (elementWrapper != null) {
            String name = elementWrapper.name();
            return !MARKER_FOR_DEFAULT.equals(name) ? name : defaultName;
        }
        XmlAttribute attribute = ae.getAnnotation(XmlAttribute.class);
        if (attribute != null) {
            String name2 = attribute.name();
            if (!MARKER_FOR_DEFAULT.equals(name2)) {
                return name2;
            }
            return defaultName;
        }
        XmlElement element = ae.getAnnotation(XmlElement.class);
        if (element != null) {
            String name3 = element.name();
            if (!MARKER_FOR_DEFAULT.equals(name3)) {
                return name3;
            }
            return defaultName;
        }
        XmlElementRef elementRef = ae.getAnnotation(XmlElementRef.class);
        if (elementRef != null) {
            String name4 = elementRef.name();
            if (!MARKER_FOR_DEFAULT.equals(name4)) {
                return name4;
            }
            if (!(aeType == null || (rootElement = aeType.getAnnotation(XmlRootElement.class)) == null)) {
                String name5 = rootElement.name();
                if (!MARKER_FOR_DEFAULT.equals(name5)) {
                    return name5;
                }
                return Introspector.decapitalize(aeType.getSimpleName());
            }
        }
        if (ae.getAnnotation(XmlValue.class) != null) {
            return "value";
        }
        return null;
    }

    private XmlRootElement findRootElementAnnotation(AnnotatedClass ac) {
        return findAnnotation(XmlRootElement.class, ac, true, false, true);
    }

    /* access modifiers changed from: protected */
    public XmlAdapter<Object, Object> findAdapter(Annotated am, boolean forSerialization) {
        XmlAdapter<Object, Object> adapter;
        Class<?> potentialAdaptee;
        XmlJavaTypeAdapter adapterInfo;
        XmlAdapter<Object, Object> adapter2;
        if (am instanceof AnnotatedClass) {
            return findAdapterForClass((AnnotatedClass) am, forSerialization);
        }
        Class<?> memberType = am.getRawType();
        if (memberType == Void.TYPE && (am instanceof AnnotatedMethod)) {
            memberType = ((AnnotatedMethod) am).getParameterClass(0);
        }
        Member member = (Member) am.getAnnotated();
        if (member != null && (potentialAdaptee = member.getDeclaringClass()) != null && (adapterInfo = potentialAdaptee.getAnnotation(XmlJavaTypeAdapter.class)) != null && (adapter2 = checkAdapter(adapterInfo, memberType)) != null) {
            return adapter2;
        }
        XmlJavaTypeAdapter adapterInfo2 = findAnnotation(XmlJavaTypeAdapter.class, am, true, false, false);
        if (adapterInfo2 != null && (adapter = checkAdapter(adapterInfo2, memberType)) != null) {
            return adapter;
        }
        XmlJavaTypeAdapters adapters = findAnnotation(XmlJavaTypeAdapters.class, am, true, false, false);
        if (adapters != null) {
            for (XmlJavaTypeAdapter info : adapters.value()) {
                XmlAdapter<Object, Object> adapter3 = checkAdapter(info, memberType);
                if (adapter3 != null) {
                    return adapter3;
                }
            }
        }
        return null;
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    private final XmlAdapter<Object, Object> checkAdapter(XmlJavaTypeAdapter adapterInfo, Class<?> typeNeeded) {
        Class<?> adaptedType = adapterInfo.type();
        if (adaptedType == XmlJavaTypeAdapter.DEFAULT.class || adaptedType.isAssignableFrom(typeNeeded)) {
            return (XmlAdapter) ClassUtil.createInstance(adapterInfo.value(), false);
        }
        return null;
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    /* access modifiers changed from: protected */
    public XmlAdapter<Object, Object> findAdapterForClass(AnnotatedClass ac, boolean forSerialization) {
        XmlJavaTypeAdapter adapterInfo = ac.getAnnotated().getAnnotation(XmlJavaTypeAdapter.class);
        if (adapterInfo != null) {
            return (XmlAdapter) ClassUtil.createInstance(adapterInfo.value(), false);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean isIndexedType(Class<?> raw) {
        return raw.isArray() || Collection.class.isAssignableFrom(raw) || Map.class.isAssignableFrom(raw);
    }

    private static class AnnotatedProperty implements AnnotatedElement {
        private final PropertyDescriptor pd;

        /* synthetic */ AnnotatedProperty(PropertyDescriptor x0, AnonymousClass1 x1) {
            this(x0);
        }

        private AnnotatedProperty(PropertyDescriptor pd2) {
            this.pd = pd2;
        }

        public boolean isAnnotationPresent(Class<? extends Annotation> annotationClass) {
            Method m = this.pd.getReadMethod();
            if (m != null && m.isAnnotationPresent(annotationClass)) {
                return true;
            }
            Method m2 = this.pd.getWriteMethod();
            if (m2 == null || !m2.isAnnotationPresent(annotationClass)) {
                return false;
            }
            return true;
        }

        public <T extends Annotation> T getAnnotation(Class<T> annotationClass) {
            T ann;
            Method m = this.pd.getReadMethod();
            if (m != null && (ann = m.getAnnotation(annotationClass)) != null) {
                return ann;
            }
            Method m2 = this.pd.getWriteMethod();
            if (m2 != null) {
                return m2.getAnnotation(annotationClass);
            }
            return null;
        }

        public Annotation[] getAnnotations() {
            throw new UnsupportedOperationException();
        }

        public Annotation[] getDeclaredAnnotations() {
            throw new UnsupportedOperationException();
        }
    }

    protected static final class PropertyDescriptors {
        private Map<String, PropertyDescriptor> _byMethodName;
        private Map<String, PropertyDescriptor> _byPropertyName;
        private final Class<?> _forClass;
        private final List<PropertyDescriptor> _properties;

        public PropertyDescriptors(Class<?> forClass, List<PropertyDescriptor> properties) {
            this._forClass = forClass;
            this._properties = properties;
        }

        public Class<?> getBeanClass() {
            return this._forClass;
        }

        public PropertyDescriptor findByPropertyName(String name) {
            if (this._byPropertyName == null) {
                this._byPropertyName = new HashMap(this._properties.size());
                for (PropertyDescriptor desc : this._properties) {
                    this._byPropertyName.put(desc.getName(), desc);
                }
            }
            return this._byPropertyName.get(name);
        }

        public PropertyDescriptor findByMethodName(String name) {
            if (this._byMethodName == null) {
                this._byMethodName = new HashMap(this._properties.size());
                for (PropertyDescriptor desc : this._properties) {
                    Method getter = desc.getReadMethod();
                    if (getter != null) {
                        this._byMethodName.put(getter.getName(), desc);
                    }
                    Method setter = desc.getWriteMethod();
                    if (setter != null) {
                        this._byMethodName.put(setter.getName(), desc);
                    }
                }
            }
            return this._byMethodName.get(name);
        }

        public static PropertyDescriptors find(Class<?> forClass) throws IntrospectionException {
            List<PropertyDescriptor> descriptors;
            String name;
            BeanInfo beanInfo = Introspector.getBeanInfo(forClass);
            if (beanInfo.getPropertyDescriptors().length == 0) {
                descriptors = Collections.emptyList();
            } else {
                descriptors = new ArrayList<>();
                Map<String, PropertyDescriptor> partials = null;
                for (PropertyDescriptor pd : beanInfo.getPropertyDescriptors()) {
                    Method read = pd.getReadMethod();
                    String readName = read == null ? null : JaxbAnnotationIntrospector.findJaxbPropertyName(read, pd.getPropertyType(), null);
                    Method write = pd.getWriteMethod();
                    String writeName = write == null ? null : JaxbAnnotationIntrospector.findJaxbPropertyName(write, pd.getPropertyType(), null);
                    if (write == null) {
                        if (readName == null) {
                            readName = pd.getName();
                        }
                        partials = _processReadMethod(partials, read, readName, descriptors);
                    } else if (read == null) {
                        if (writeName == null) {
                            writeName = pd.getName();
                        }
                        partials = _processWriteMethod(partials, write, writeName, descriptors);
                    } else if (readName == null || writeName == null || readName.equals(writeName)) {
                        if (readName != null) {
                            name = readName;
                        } else if (writeName != null) {
                            name = writeName;
                        } else {
                            name = pd.getName();
                        }
                        descriptors.add(new PropertyDescriptor(name, read, write));
                    } else {
                        partials = _processWriteMethod(_processReadMethod(partials, read, readName, descriptors), write, writeName, descriptors);
                    }
                }
            }
            return new PropertyDescriptors(forClass, descriptors);
        }

        private static Map<String, PropertyDescriptor> _processReadMethod(Map<String, PropertyDescriptor> partials, Method method, String propertyName, List<PropertyDescriptor> pds) throws IntrospectionException {
            if (partials == null) {
                partials = new HashMap<>();
            } else {
                PropertyDescriptor pd = partials.get(propertyName);
                if (pd != null) {
                    pd.setReadMethod(method);
                    if (pd.getWriteMethod() != null) {
                        pds.add(pd);
                        partials.remove(propertyName);
                        return partials;
                    }
                }
            }
            partials.put(propertyName, new PropertyDescriptor(propertyName, method, (Method) null));
            return partials;
        }

        private static Map<String, PropertyDescriptor> _processWriteMethod(Map<String, PropertyDescriptor> partials, Method method, String propertyName, List<PropertyDescriptor> pds) throws IntrospectionException {
            if (partials == null) {
                partials = new HashMap<>();
            } else {
                PropertyDescriptor pd = partials.get(propertyName);
                if (pd != null) {
                    pd.setWriteMethod(method);
                    if (pd.getReadMethod() != null) {
                        pds.add(pd);
                        partials.remove(propertyName);
                        return partials;
                    }
                }
            }
            partials.put(propertyName, new PropertyDescriptor(propertyName, (Method) null, method));
            return partials;
        }
    }
}
