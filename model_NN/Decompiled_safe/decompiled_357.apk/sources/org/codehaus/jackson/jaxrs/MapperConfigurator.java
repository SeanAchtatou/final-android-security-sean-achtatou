package org.codehaus.jackson.jaxrs;

import java.util.ArrayList;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.introspect.JacksonAnnotationIntrospector;
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector;

public class MapperConfigurator {
    protected Annotations[] _defaultAnnotationsToUse;
    protected ObjectMapper _defaultMapper;
    protected Class<? extends AnnotationIntrospector> _jaxbIntrospectorClass;
    protected ObjectMapper _mapper;

    public MapperConfigurator(ObjectMapper mapper, Annotations[] defAnnotations) {
        this._mapper = mapper;
        this._defaultAnnotationsToUse = defAnnotations;
    }

    public synchronized ObjectMapper getConfiguredMapper() {
        return this._mapper;
    }

    public synchronized ObjectMapper getDefaultMapper() {
        if (this._defaultMapper == null) {
            this._defaultMapper = new ObjectMapper();
            _setAnnotations(this._defaultMapper, this._defaultAnnotationsToUse);
        }
        return this._defaultMapper;
    }

    public synchronized void setMapper(ObjectMapper m) {
        this._mapper = m;
    }

    public synchronized void setAnnotationsToUse(Annotations[] annotationsToUse) {
        _setAnnotations(mapper(), annotationsToUse);
    }

    public synchronized void configure(DeserializationConfig.Feature f, boolean state) {
        mapper().configure(f, state);
    }

    public synchronized void configure(SerializationConfig.Feature f, boolean state) {
        mapper().configure(f, state);
    }

    public synchronized void configure(JsonParser.Feature f, boolean state) {
        mapper().configure(f, state);
    }

    public synchronized void configure(JsonGenerator.Feature f, boolean state) {
        mapper().configure(f, state);
    }

    /* access modifiers changed from: protected */
    public ObjectMapper mapper() {
        if (this._mapper == null) {
            this._mapper = new ObjectMapper();
            _setAnnotations(this._mapper, this._defaultAnnotationsToUse);
        }
        return this._mapper;
    }

    /* access modifiers changed from: protected */
    public void _setAnnotations(ObjectMapper mapper, Annotations[] annotationsToUse) {
        AnnotationIntrospector intr;
        if (annotationsToUse == null || annotationsToUse.length == 0) {
            intr = AnnotationIntrospector.nopInstance();
        } else {
            intr = _resolveIntrospectors(annotationsToUse);
        }
        mapper.getDeserializationConfig().setAnnotationIntrospector(intr);
        mapper.getSerializationConfig().setAnnotationIntrospector(intr);
    }

    /* Debug info: failed to restart local var, previous not found, register: 10 */
    /* access modifiers changed from: protected */
    public AnnotationIntrospector _resolveIntrospectors(Annotations[] annotationsToUse) {
        ArrayList<AnnotationIntrospector> intr = new ArrayList<>();
        for (Annotations a : annotationsToUse) {
            if (a != null) {
                intr.add(_resolveIntrospector(a));
            }
        }
        if (intr.size() == 0) {
            return AnnotationIntrospector.nopInstance();
        }
        AnnotationIntrospector curr = (AnnotationIntrospector) intr.get(0);
        int len = intr.size();
        for (int i = 1; i < len; i++) {
            curr = AnnotationIntrospector.pair(curr, (AnnotationIntrospector) intr.get(i));
        }
        return curr;
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    /* access modifiers changed from: protected */
    public AnnotationIntrospector _resolveIntrospector(Annotations ann) {
        switch (ann) {
            case JACKSON:
                return new JacksonAnnotationIntrospector();
            case JAXB:
                try {
                    if (this._jaxbIntrospectorClass == null) {
                        this._jaxbIntrospectorClass = JaxbAnnotationIntrospector.class;
                    }
                    return (AnnotationIntrospector) this._jaxbIntrospectorClass.newInstance();
                } catch (Exception e) {
                    Exception e2 = e;
                    throw new IllegalStateException("Failed to instantiate JaxbAnnotationIntrospector: " + e2.getMessage(), e2);
                }
            default:
                throw new IllegalStateException();
        }
    }
}
