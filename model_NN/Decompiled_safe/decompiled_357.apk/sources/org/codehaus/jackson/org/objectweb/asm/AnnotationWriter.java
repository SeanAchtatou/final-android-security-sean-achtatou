package org.codehaus.jackson.org.objectweb.asm;

final class AnnotationWriter implements AnnotationVisitor {
    private final ClassWriter a;
    private int b;
    private final boolean c;
    private final ByteVector d;
    private final ByteVector e;
    private final int f;
    AnnotationWriter g;
    AnnotationWriter h;

    AnnotationWriter(ClassWriter classWriter, boolean z, ByteVector byteVector, ByteVector byteVector2, int i) {
        this.a = classWriter;
        this.c = z;
        this.d = byteVector;
        this.e = byteVector2;
        this.f = i;
    }

    static void a(AnnotationWriter[] annotationWriterArr, int i, ByteVector byteVector) {
        int length = ((annotationWriterArr.length - i) * 2) + 1;
        for (int i2 = i; i2 < annotationWriterArr.length; i2++) {
            length += annotationWriterArr[i2] == null ? 0 : annotationWriterArr[i2].a();
        }
        byteVector.putInt(length).putByte(annotationWriterArr.length - i);
        for (int i3 = i; i3 < annotationWriterArr.length; i3++) {
            AnnotationWriter annotationWriter = null;
            AnnotationWriter annotationWriter2 = annotationWriterArr[i3];
            int i4 = 0;
            while (annotationWriter2 != null) {
                i4++;
                annotationWriter2.visitEnd();
                annotationWriter2.h = annotationWriter;
                AnnotationWriter annotationWriter3 = annotationWriter2;
                annotationWriter2 = annotationWriter2.g;
                annotationWriter = annotationWriter3;
            }
            byteVector.putShort(i4);
            for (AnnotationWriter annotationWriter4 = annotationWriter; annotationWriter4 != null; annotationWriter4 = annotationWriter4.h) {
                byteVector.putByteArray(annotationWriter4.d.a, 0, annotationWriter4.d.b);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int a() {
        int i = 0;
        for (AnnotationWriter annotationWriter = this; annotationWriter != null; annotationWriter = annotationWriter.g) {
            i += annotationWriter.d.b;
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    public void a(ByteVector byteVector) {
        int i = 2;
        int i2 = 0;
        AnnotationWriter annotationWriter = null;
        AnnotationWriter annotationWriter2 = this;
        while (annotationWriter2 != null) {
            i2++;
            i += annotationWriter2.d.b;
            annotationWriter2.visitEnd();
            annotationWriter2.h = annotationWriter;
            AnnotationWriter annotationWriter3 = annotationWriter2;
            annotationWriter2 = annotationWriter2.g;
            annotationWriter = annotationWriter3;
        }
        byteVector.putInt(i);
        byteVector.putShort(i2);
        while (annotationWriter != null) {
            byteVector.putByteArray(annotationWriter.d.a, 0, annotationWriter.d.b);
            annotationWriter = annotationWriter.h;
        }
    }

    public void visit(String str, Object obj) {
        this.b++;
        if (this.c) {
            this.d.putShort(this.a.newUTF8(str));
        }
        if (obj instanceof String) {
            this.d.b(Opcodes.DREM, this.a.newUTF8((String) obj));
        } else if (obj instanceof Byte) {
            this.d.b(66, this.a.a((int) ((Byte) obj).byteValue()).a);
        } else if (obj instanceof Boolean) {
            this.d.b(90, this.a.a(((Boolean) obj).booleanValue() ? 1 : 0).a);
        } else if (obj instanceof Character) {
            this.d.b(67, this.a.a((int) ((Character) obj).charValue()).a);
        } else if (obj instanceof Short) {
            this.d.b(83, this.a.a((int) ((Short) obj).shortValue()).a);
        } else if (obj instanceof Type) {
            this.d.b(99, this.a.newUTF8(((Type) obj).getDescriptor()));
        } else if (obj instanceof byte[]) {
            byte[] bArr = (byte[]) obj;
            this.d.b(91, bArr.length);
            for (byte b2 : bArr) {
                this.d.b(66, this.a.a((int) b2).a);
            }
        } else if (obj instanceof boolean[]) {
            boolean[] zArr = (boolean[]) obj;
            this.d.b(91, zArr.length);
            for (int i = 0; i < zArr.length; i++) {
                this.d.b(90, this.a.a(zArr[i] ? 1 : 0).a);
            }
        } else if (obj instanceof short[]) {
            short[] sArr = (short[]) obj;
            this.d.b(91, sArr.length);
            for (short s : sArr) {
                this.d.b(83, this.a.a((int) s).a);
            }
        } else if (obj instanceof char[]) {
            char[] cArr = (char[]) obj;
            this.d.b(91, cArr.length);
            for (char c2 : cArr) {
                this.d.b(67, this.a.a((int) c2).a);
            }
        } else if (obj instanceof int[]) {
            int[] iArr = (int[]) obj;
            this.d.b(91, iArr.length);
            for (int a2 : iArr) {
                this.d.b(73, this.a.a(a2).a);
            }
        } else if (obj instanceof long[]) {
            long[] jArr = (long[]) obj;
            this.d.b(91, jArr.length);
            for (long a3 : jArr) {
                this.d.b(74, this.a.a(a3).a);
            }
        } else if (obj instanceof float[]) {
            float[] fArr = (float[]) obj;
            this.d.b(91, fArr.length);
            for (float a4 : fArr) {
                this.d.b(70, this.a.a(a4).a);
            }
        } else if (obj instanceof double[]) {
            double[] dArr = (double[]) obj;
            this.d.b(91, dArr.length);
            for (double a5 : dArr) {
                this.d.b(68, this.a.a(a5).a);
            }
        } else {
            Item a6 = this.a.a(obj);
            this.d.b(".s.IFJDCS".charAt(a6.b), a6.a);
        }
    }

    public AnnotationVisitor visitAnnotation(String str, String str2) {
        this.b++;
        if (this.c) {
            this.d.putShort(this.a.newUTF8(str));
        }
        this.d.b(64, this.a.newUTF8(str2)).putShort(0);
        return new AnnotationWriter(this.a, true, this.d, this.d, this.d.b - 2);
    }

    public AnnotationVisitor visitArray(String str) {
        this.b++;
        if (this.c) {
            this.d.putShort(this.a.newUTF8(str));
        }
        this.d.b(91, 0);
        return new AnnotationWriter(this.a, false, this.d, this.d, this.d.b - 2);
    }

    public void visitEnd() {
        if (this.e != null) {
            byte[] bArr = this.e.a;
            bArr[this.f] = (byte) (this.b >>> 8);
            bArr[this.f + 1] = (byte) this.b;
        }
    }

    public void visitEnum(String str, String str2, String str3) {
        this.b++;
        if (this.c) {
            this.d.putShort(this.a.newUTF8(str));
        }
        this.d.b(Opcodes.LSUB, this.a.newUTF8(str2)).putShort(this.a.newUTF8(str3));
    }
}
