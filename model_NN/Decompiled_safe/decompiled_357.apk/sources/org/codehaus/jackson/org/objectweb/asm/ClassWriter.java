package org.codehaus.jackson.org.objectweb.asm;

public class ClassWriter implements ClassVisitor {
    public static final int COMPUTE_FRAMES = 2;
    public static final int COMPUTE_MAXS = 1;
    static final byte[] a;
    MethodWriter A;
    MethodWriter B;
    private short D;
    Item[] E;
    String F;
    private final boolean G;
    private final boolean H;
    boolean I;
    ClassReader J;
    int b;
    int c;
    final ByteVector d;
    Item[] e;
    int f;
    final Item g;
    final Item h;
    final Item i;
    private int j;
    private int k;
    private int l;
    private int m;
    private int n;
    private int[] o;
    private int p;
    private ByteVector q;
    private int r;
    private int s;
    private AnnotationWriter t;
    private AnnotationWriter u;
    private Attribute v;
    private int w;
    private ByteVector x;
    FieldWriter y;
    FieldWriter z;

    static {
        byte[] bArr = new byte[220];
        for (int i2 = 0; i2 < bArr.length; i2++) {
            bArr[i2] = (byte) ("AAAAAAAAAAAAAAAABCKLLDDDDDEEEEEEEEEEEEEEEEEEEEAAAAAAAADDDDDEEEEEEEEEEEEEEEEEEEEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAAAAAAAAAAAAAAAAAAAIIIIIIIIIIIIIIIIDNOAAAAAAGGGGGGGHHFBFAAFFAAQPIIJJIIIIIIIIIIIIIIIIII".charAt(i2) - 'A');
        }
        a = bArr;
    }

    public ClassWriter(int i2) {
        this.c = 1;
        this.d = new ByteVector();
        this.e = new Item[Opcodes.ACC_NATIVE];
        this.f = (int) (0.75d * ((double) this.e.length));
        this.g = new Item();
        this.h = new Item();
        this.i = new Item();
        this.H = (i2 & 1) != 0;
        this.G = (i2 & 2) != 0;
    }

    public ClassWriter(ClassReader classReader, int i2) {
        this(i2);
        classReader.a(this);
        this.J = classReader;
    }

    private Item a(Item item) {
        Item item2 = this.e[item.j % this.e.length];
        while (item2 != null && (item2.b != item.b || !item.a(item2))) {
            item2 = item2.k;
        }
        return item2;
    }

    private void a(int i2, int i3, int i4) {
        this.d.b(i2, i3).putShort(i4);
    }

    private Item b(String str) {
        this.h.a(8, str, null, null);
        Item a2 = a(this.h);
        if (a2 != null) {
            return a2;
        }
        this.d.b(8, newUTF8(str));
        int i2 = this.c;
        this.c = i2 + 1;
        Item item = new Item(i2, this.h);
        b(item);
        return item;
    }

    private void b(Item item) {
        if (this.c > this.f) {
            int length = this.e.length;
            int i2 = (length * 2) + 1;
            Item[] itemArr = new Item[i2];
            for (int i3 = length - 1; i3 >= 0; i3--) {
                Item item2 = this.e[i3];
                while (item2 != null) {
                    int length2 = item2.j % itemArr.length;
                    Item item3 = item2.k;
                    item2.k = itemArr[length2];
                    itemArr[length2] = item2;
                    item2 = item3;
                }
            }
            this.e = itemArr;
            this.f = (int) (((double) i2) * 0.75d);
        }
        int length3 = item.j % this.e.length;
        item.k = this.e[length3];
        this.e[length3] = item;
    }

    private Item c(Item item) {
        this.D = (short) (this.D + 1);
        Item item2 = new Item(this.D, this.g);
        b(item2);
        if (this.E == null) {
            this.E = new Item[16];
        }
        if (this.D == this.E.length) {
            Item[] itemArr = new Item[(this.E.length * 2)];
            System.arraycopy(this.E, 0, itemArr, 0, this.E.length);
            this.E = itemArr;
        }
        this.E[this.D] = item2;
        return item2;
    }

    /* access modifiers changed from: package-private */
    public int a(int i2, int i3) {
        this.h.b = 15;
        this.h.d = ((long) i2) | (((long) i3) << 32);
        this.h.j = Integer.MAX_VALUE & (i2 + 15 + i3);
        Item a2 = a(this.h);
        if (a2 == null) {
            String str = this.E[i2].g;
            String str2 = this.E[i3].g;
            this.h.c = c(getCommonSuperClass(str, str2));
            a2 = new Item(0, this.h);
            b(a2);
        }
        return a2.c;
    }

    /* access modifiers changed from: package-private */
    public int a(String str, int i2) {
        this.g.b = 14;
        this.g.c = i2;
        this.g.g = str;
        this.g.j = Integer.MAX_VALUE & (str.hashCode() + 14 + i2);
        Item a2 = a(this.g);
        if (a2 == null) {
            a2 = c(this.g);
        }
        return a2.a;
    }

    /* access modifiers changed from: package-private */
    public Item a(double d2) {
        this.g.a(d2);
        Item a2 = a(this.g);
        if (a2 != null) {
            return a2;
        }
        this.d.putByte(6).putLong(this.g.d);
        Item item = new Item(this.c, this.g);
        b(item);
        this.c += 2;
        return item;
    }

    /* access modifiers changed from: package-private */
    public Item a(float f2) {
        this.g.a(f2);
        Item a2 = a(this.g);
        if (a2 != null) {
            return a2;
        }
        this.d.putByte(4).putInt(this.g.c);
        int i2 = this.c;
        this.c = i2 + 1;
        Item item = new Item(i2, this.g);
        b(item);
        return item;
    }

    /* access modifiers changed from: package-private */
    public Item a(int i2) {
        this.g.a(i2);
        Item a2 = a(this.g);
        if (a2 != null) {
            return a2;
        }
        this.d.putByte(3).putInt(i2);
        int i3 = this.c;
        this.c = i3 + 1;
        Item item = new Item(i3, this.g);
        b(item);
        return item;
    }

    /* access modifiers changed from: package-private */
    public Item a(long j2) {
        this.g.a(j2);
        Item a2 = a(this.g);
        if (a2 != null) {
            return a2;
        }
        this.d.putByte(5).putLong(j2);
        Item item = new Item(this.c, this.g);
        b(item);
        this.c += 2;
        return item;
    }

    /* access modifiers changed from: package-private */
    public Item a(Object obj) {
        if (obj instanceof Integer) {
            return a(((Integer) obj).intValue());
        }
        if (obj instanceof Byte) {
            return a(((Byte) obj).intValue());
        }
        if (obj instanceof Character) {
            return a((int) ((Character) obj).charValue());
        }
        if (obj instanceof Short) {
            return a(((Short) obj).intValue());
        }
        if (obj instanceof Boolean) {
            return a(((Boolean) obj).booleanValue() ? 1 : 0);
        } else if (obj instanceof Float) {
            return a(((Float) obj).floatValue());
        } else {
            if (obj instanceof Long) {
                return a(((Long) obj).longValue());
            }
            if (obj instanceof Double) {
                return a(((Double) obj).doubleValue());
            }
            if (obj instanceof String) {
                return b((String) obj);
            }
            if (obj instanceof Type) {
                Type type = (Type) obj;
                return a(type.getSort() == 10 ? type.getInternalName() : type.getDescriptor());
            }
            throw new IllegalArgumentException(new StringBuffer().append("value ").append(obj).toString());
        }
    }

    /* access modifiers changed from: package-private */
    public Item a(String str) {
        this.h.a(7, str, null, null);
        Item a2 = a(this.h);
        if (a2 != null) {
            return a2;
        }
        this.d.b(7, newUTF8(str));
        int i2 = this.c;
        this.c = i2 + 1;
        Item item = new Item(i2, this.h);
        b(item);
        return item;
    }

    /* access modifiers changed from: package-private */
    public Item a(String str, String str2) {
        this.h.a(12, str, str2, null);
        Item a2 = a(this.h);
        if (a2 != null) {
            return a2;
        }
        a(12, newUTF8(str), newUTF8(str2));
        int i2 = this.c;
        this.c = i2 + 1;
        Item item = new Item(i2, this.h);
        b(item);
        return item;
    }

    /* access modifiers changed from: package-private */
    public Item a(String str, String str2, String str3) {
        this.i.a(9, str, str2, str3);
        Item a2 = a(this.i);
        if (a2 != null) {
            return a2;
        }
        a(9, newClass(str), newNameType(str2, str3));
        int i2 = this.c;
        this.c = i2 + 1;
        Item item = new Item(i2, this.i);
        b(item);
        return item;
    }

    /* access modifiers changed from: package-private */
    public Item a(String str, String str2, String str3, boolean z2) {
        int i2 = z2 ? 11 : 10;
        this.i.a(i2, str, str2, str3);
        Item a2 = a(this.i);
        if (a2 != null) {
            return a2;
        }
        a(i2, newClass(str), newNameType(str2, str3));
        int i3 = this.c;
        this.c = i3 + 1;
        Item item = new Item(i3, this.i);
        b(item);
        return item;
    }

    /* access modifiers changed from: package-private */
    public int c(String str) {
        this.g.a(13, str, null, null);
        Item a2 = a(this.g);
        if (a2 == null) {
            a2 = c(this.g);
        }
        return a2.a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* access modifiers changed from: protected */
    public String getCommonSuperClass(String str, String str2) {
        try {
            Class<?> cls = Class.forName(str.replace('/', '.'));
            Class<?> cls2 = Class.forName(str2.replace('/', '.'));
            if (cls.isAssignableFrom(cls2)) {
                return str;
            }
            if (cls2.isAssignableFrom(cls)) {
                return str2;
            }
            if (cls.isInterface() || cls2.isInterface()) {
                return "java/lang/Object";
            }
            do {
                cls = cls.getSuperclass();
            } while (!cls.isAssignableFrom(cls2));
            return cls.getName().replace('.', '/');
        } catch (Exception e2) {
            throw new RuntimeException(e2.toString());
        }
    }

    public int newClass(String str) {
        return a(str).a;
    }

    public int newConst(Object obj) {
        return a(obj).a;
    }

    public int newField(String str, String str2, String str3) {
        return a(str, str2, str3).a;
    }

    public int newMethod(String str, String str2, String str3, boolean z2) {
        return a(str, str2, str3, z2).a;
    }

    public int newNameType(String str, String str2) {
        return a(str, str2).a;
    }

    public int newUTF8(String str) {
        this.g.a(1, str, null, null);
        Item a2 = a(this.g);
        if (a2 == null) {
            this.d.putByte(1).putUTF8(str);
            int i2 = this.c;
            this.c = i2 + 1;
            a2 = new Item(i2, this.g);
            b(a2);
        }
        return a2.a;
    }

    public byte[] toByteArray() {
        int i2;
        int i3;
        int i4 = 0;
        FieldWriter fieldWriter = this.y;
        int i5 = (this.n * 2) + 24;
        FieldWriter fieldWriter2 = fieldWriter;
        while (fieldWriter2 != null) {
            i5 += fieldWriter2.a();
            fieldWriter2 = fieldWriter2.a;
            i4++;
        }
        MethodWriter methodWriter = this.A;
        int i6 = 0;
        while (methodWriter != null) {
            i5 += methodWriter.a();
            methodWriter = methodWriter.a;
            i6++;
        }
        if (this.l != 0) {
            i2 = 0 + 1;
            i5 += 8;
            newUTF8("Signature");
        } else {
            i2 = 0;
        }
        if (this.p != 0) {
            i2++;
            i5 += 8;
            newUTF8("SourceFile");
        }
        if (this.q != null) {
            i2++;
            i5 += this.q.b + 4;
            newUTF8("SourceDebugExtension");
        }
        if (this.r != 0) {
            i2++;
            i5 += 10;
            newUTF8("EnclosingMethod");
        }
        if ((this.j & Opcodes.ACC_DEPRECATED) != 0) {
            i2++;
            i5 += 6;
            newUTF8("Deprecated");
        }
        if ((this.j & 4096) != 0 && ((this.b & 65535) < 49 || (this.j & 262144) != 0)) {
            i2++;
            i5 += 6;
            newUTF8("Synthetic");
        }
        if (this.x != null) {
            i2++;
            i5 += this.x.b + 8;
            newUTF8("InnerClasses");
        }
        if (this.t != null) {
            i2++;
            i5 += this.t.a() + 8;
            newUTF8("RuntimeVisibleAnnotations");
        }
        if (this.u != null) {
            i2++;
            i5 += this.u.a() + 8;
            newUTF8("RuntimeInvisibleAnnotations");
        }
        int i7 = i5;
        if (this.v != null) {
            int a2 = i2 + this.v.a();
            i3 = this.v.a(this, null, 0, -1, -1) + i7;
            i2 = a2;
        } else {
            i3 = i7;
        }
        ByteVector byteVector = new ByteVector(i3 + this.d.b);
        byteVector.putInt(-889275714).putInt(this.b);
        byteVector.putShort(this.c).putByteArray(this.d.a, 0, this.d.b);
        byteVector.putShort(((393216 | ((this.j & 262144) / 64)) ^ -1) & this.j).putShort(this.k).putShort(this.m);
        byteVector.putShort(this.n);
        for (int i8 = 0; i8 < this.n; i8++) {
            byteVector.putShort(this.o[i8]);
        }
        byteVector.putShort(i4);
        for (FieldWriter fieldWriter3 = this.y; fieldWriter3 != null; fieldWriter3 = fieldWriter3.a) {
            fieldWriter3.a(byteVector);
        }
        byteVector.putShort(i6);
        for (MethodWriter methodWriter2 = this.A; methodWriter2 != null; methodWriter2 = methodWriter2.a) {
            methodWriter2.a(byteVector);
        }
        byteVector.putShort(i2);
        if (this.l != 0) {
            byteVector.putShort(newUTF8("Signature")).putInt(2).putShort(this.l);
        }
        if (this.p != 0) {
            byteVector.putShort(newUTF8("SourceFile")).putInt(2).putShort(this.p);
        }
        if (this.q != null) {
            int i9 = this.q.b - 2;
            byteVector.putShort(newUTF8("SourceDebugExtension")).putInt(i9);
            byteVector.putByteArray(this.q.a, 2, i9);
        }
        if (this.r != 0) {
            byteVector.putShort(newUTF8("EnclosingMethod")).putInt(4);
            byteVector.putShort(this.r).putShort(this.s);
        }
        if ((this.j & Opcodes.ACC_DEPRECATED) != 0) {
            byteVector.putShort(newUTF8("Deprecated")).putInt(0);
        }
        if ((this.j & 4096) != 0 && ((this.b & 65535) < 49 || (this.j & 262144) != 0)) {
            byteVector.putShort(newUTF8("Synthetic")).putInt(0);
        }
        if (this.x != null) {
            byteVector.putShort(newUTF8("InnerClasses"));
            byteVector.putInt(this.x.b + 2).putShort(this.w);
            byteVector.putByteArray(this.x.a, 0, this.x.b);
        }
        if (this.t != null) {
            byteVector.putShort(newUTF8("RuntimeVisibleAnnotations"));
            this.t.a(byteVector);
        }
        if (this.u != null) {
            byteVector.putShort(newUTF8("RuntimeInvisibleAnnotations"));
            this.u.a(byteVector);
        }
        if (this.v != null) {
            this.v.a(this, null, 0, -1, -1, byteVector);
        }
        if (!this.I) {
            return byteVector.a;
        }
        ClassWriter classWriter = new ClassWriter(2);
        new ClassReader(byteVector.a).accept(classWriter, 4);
        return classWriter.toByteArray();
    }

    public void visit(int i2, int i3, String str, String str2, String str3, String[] strArr) {
        this.b = i2;
        this.j = i3;
        this.k = newClass(str);
        this.F = str;
        if (str2 != null) {
            this.l = newUTF8(str2);
        }
        this.m = str3 == null ? 0 : newClass(str3);
        if (strArr != null && strArr.length > 0) {
            this.n = strArr.length;
            this.o = new int[this.n];
            for (int i4 = 0; i4 < this.n; i4++) {
                this.o[i4] = newClass(strArr[i4]);
            }
        }
    }

    public AnnotationVisitor visitAnnotation(String str, boolean z2) {
        ByteVector byteVector = new ByteVector();
        byteVector.putShort(newUTF8(str)).putShort(0);
        AnnotationWriter annotationWriter = new AnnotationWriter(this, true, byteVector, byteVector, 2);
        if (z2) {
            annotationWriter.g = this.t;
            this.t = annotationWriter;
        } else {
            annotationWriter.g = this.u;
            this.u = annotationWriter;
        }
        return annotationWriter;
    }

    public void visitAttribute(Attribute attribute) {
        attribute.a = this.v;
        this.v = attribute;
    }

    public void visitEnd() {
    }

    public FieldVisitor visitField(int i2, String str, String str2, String str3, Object obj) {
        return new FieldWriter(this, i2, str, str2, str3, obj);
    }

    public void visitInnerClass(String str, String str2, String str3, int i2) {
        if (this.x == null) {
            this.x = new ByteVector();
        }
        this.w++;
        this.x.putShort(str == null ? 0 : newClass(str));
        this.x.putShort(str2 == null ? 0 : newClass(str2));
        this.x.putShort(str3 == null ? 0 : newUTF8(str3));
        this.x.putShort(i2);
    }

    public MethodVisitor visitMethod(int i2, String str, String str2, String str3, String[] strArr) {
        return new MethodWriter(this, i2, str, str2, str3, strArr, this.H, this.G);
    }

    public void visitOuterClass(String str, String str2, String str3) {
        this.r = newClass(str);
        if (str2 != null && str3 != null) {
            this.s = newNameType(str2, str3);
        }
    }

    public void visitSource(String str, String str2) {
        if (str != null) {
            this.p = newUTF8(str);
        }
        if (str2 != null) {
            this.q = new ByteVector().putUTF8(str2);
        }
    }
}
