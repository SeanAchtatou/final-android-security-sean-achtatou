package org.codehaus.jackson.org.objectweb.asm;

import org.codehaus.jackson.smile.SmileConstants;

final class Frame {
    static final int[] a;
    Label b;
    int[] c;
    int[] d;
    private int[] e;
    private int[] f;
    private int g;
    private int h;
    private int[] i;

    static {
        int[] iArr = new int[202];
        for (int i2 = 0; i2 < iArr.length; i2++) {
            iArr[i2] = "EFFFFFFFFGGFFFGGFFFEEFGFGFEEEEEEEEEEEEEEEEEEEEDEDEDDDDDCDCDEEEEEEEEEEEEEEEEEEEEBABABBBBDCFFFGGGEDCDCDCDCDCDCDCDCDCDCEEEEDDDDDDDCDCDCEFEFDDEEFFDEDEEEBDDBBDDDDDDCCCCCCCCEFEDDDCDCDEEEEEEEEEEFEEEEEEDDEEDDEE".charAt(i2) - 'E';
        }
        a = iArr;
    }

    Frame() {
    }

    private int a() {
        if (this.g > 0) {
            int[] iArr = this.f;
            int i2 = this.g - 1;
            this.g = i2;
            return iArr[i2];
        }
        Label label = this.b;
        int i3 = label.f - 1;
        label.f = i3;
        return 50331648 | (-i3);
    }

    private int a(int i2) {
        if (this.e == null || i2 >= this.e.length) {
            return 33554432 | i2;
        }
        int i3 = this.e[i2];
        if (i3 != 0) {
            return i3;
        }
        int i4 = 33554432 | i2;
        this.e[i2] = i4;
        return i4;
    }

    private int a(ClassWriter classWriter, int i2) {
        int c2;
        if (i2 == 16777222) {
            c2 = classWriter.c(classWriter.F) | 24117248;
        } else if ((-1048576 & i2) != 25165824) {
            return i2;
        } else {
            c2 = classWriter.c(classWriter.E[1048575 & i2].g) | 24117248;
        }
        for (int i3 = 0; i3 < this.h; i3++) {
            int i4 = this.i[i3];
            int i5 = -268435456 & i4;
            int i6 = 251658240 & i4;
            if (i6 == 33554432) {
                i4 = this.c[i4 & 8388607] + i5;
            } else if (i6 == 50331648) {
                i4 = this.d[this.d.length - (i4 & 8388607)] + i5;
            }
            if (i2 == i4) {
                return c2;
            }
        }
        return i2;
    }

    private void a(int i2, int i3) {
        if (this.e == null) {
            this.e = new int[10];
        }
        int length = this.e.length;
        if (i2 >= length) {
            int[] iArr = new int[Math.max(i2 + 1, length * 2)];
            System.arraycopy(this.e, 0, iArr, 0, length);
            this.e = iArr;
        }
        this.e[i2] = i3;
    }

    private void a(String str) {
        char charAt = str.charAt(0);
        if (charAt == '(') {
            c((Type.getArgumentsAndReturnSizes(str) >> 2) - 1);
        } else if (charAt == 'J' || charAt == 'D') {
            c(2);
        } else {
            c(1);
        }
    }

    private void a(ClassWriter classWriter, String str) {
        int b2 = b(classWriter, str);
        if (b2 != 0) {
            b(b2);
            if (b2 == 16777220 || b2 == 16777219) {
                b(16777216);
            }
        }
    }

    private static boolean a(ClassWriter classWriter, int i2, int[] iArr, int i3) {
        int i4;
        int i5 = iArr[i3];
        if (i5 == i2) {
            return false;
        }
        if ((268435455 & i2) != 16777221) {
            i4 = i2;
        } else if (i5 == 16777221) {
            return false;
        } else {
            i4 = 16777221;
        }
        if (i5 == 0) {
            iArr[i3] = i4;
            return true;
        }
        if ((i5 & 267386880) == 24117248 || (i5 & -268435456) != 0) {
            if (i4 == 16777221) {
                return false;
            }
            i4 = (-1048576 & i4) == (-1048576 & i5) ? (i5 & 267386880) == 24117248 ? classWriter.a(i4 & 1048575, 1048575 & i5) | (i4 & -268435456) | 24117248 : classWriter.c("java/lang/Object") | 24117248 : ((i4 & 267386880) == 24117248 || (i4 & -268435456) != 0) ? classWriter.c("java/lang/Object") | 24117248 : 16777216;
        } else if (i5 != 16777221) {
            i4 = 16777216;
        } else if ((i4 & 267386880) != 24117248 && (i4 & -268435456) == 0) {
            i4 = 16777216;
        }
        if (i5 == i4) {
            return false;
        }
        iArr[i3] = i4;
        return true;
    }

    private static int b(ClassWriter classWriter, String str) {
        int i2;
        int indexOf = str.charAt(0) == '(' ? str.indexOf(41) + 1 : 0;
        switch (str.charAt(indexOf)) {
            case 'B':
            case 'C':
            case 'I':
            case Opcodes.AASTORE /*83*/:
            case Opcodes.DUP_X1 /*90*/:
                return 16777217;
            case 'D':
                return 16777219;
            case 'F':
                return 16777218;
            case 'J':
                return 16777220;
            case 'L':
                return classWriter.c(str.substring(indexOf + 1, str.length() - 1)) | 24117248;
            case Opcodes.SASTORE /*86*/:
                return 0;
            default:
                int i3 = indexOf + 1;
                while (str.charAt(i3) == '[') {
                    i3++;
                }
                switch (str.charAt(i3)) {
                    case 'B':
                        i2 = 16777226;
                        break;
                    case 'C':
                        i2 = 16777227;
                        break;
                    case 'D':
                        i2 = 16777219;
                        break;
                    case 'F':
                        i2 = 16777218;
                        break;
                    case 'I':
                        i2 = 16777217;
                        break;
                    case 'J':
                        i2 = 16777220;
                        break;
                    case Opcodes.AASTORE /*83*/:
                        i2 = 16777228;
                        break;
                    case Opcodes.DUP_X1 /*90*/:
                        i2 = 16777225;
                        break;
                    default:
                        i2 = classWriter.c(str.substring(i3 + 1, str.length() - 1)) | 24117248;
                        break;
                }
                return ((i3 - indexOf) << 28) | i2;
        }
    }

    private void b(int i2) {
        if (this.f == null) {
            this.f = new int[10];
        }
        int length = this.f.length;
        if (this.g >= length) {
            int[] iArr = new int[Math.max(this.g + 1, length * 2)];
            System.arraycopy(this.f, 0, iArr, 0, length);
            this.f = iArr;
        }
        int[] iArr2 = this.f;
        int i3 = this.g;
        this.g = i3 + 1;
        iArr2[i3] = i2;
        int i4 = this.b.f + this.g;
        if (i4 > this.b.g) {
            this.b.g = i4;
        }
    }

    private void c(int i2) {
        if (this.g >= i2) {
            this.g -= i2;
            return;
        }
        this.b.f -= i2 - this.g;
        this.g = 0;
    }

    private void d(int i2) {
        if (this.i == null) {
            this.i = new int[2];
        }
        int length = this.i.length;
        if (this.h >= length) {
            int[] iArr = new int[Math.max(this.h + 1, length * 2)];
            System.arraycopy(this.i, 0, iArr, 0, length);
            this.i = iArr;
        }
        int[] iArr2 = this.i;
        int i3 = this.h;
        this.h = i3 + 1;
        iArr2[i3] = i2;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, ClassWriter classWriter, Item item) {
        switch (i2) {
            case 0:
            case Opcodes.INEG /*116*/:
            case Opcodes.LNEG /*117*/:
            case Opcodes.FNEG /*118*/:
            case Opcodes.DNEG /*119*/:
            case Opcodes.I2B /*145*/:
            case Opcodes.I2C /*146*/:
            case Opcodes.I2S /*147*/:
            case Opcodes.GOTO /*167*/:
            case Opcodes.RETURN /*177*/:
                return;
            case 1:
                b(16777221);
                return;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 16:
            case Opcodes.SIPUSH /*17*/:
            case Opcodes.ILOAD /*21*/:
                b(16777217);
                return;
            case 9:
            case 10:
            case Opcodes.LLOAD /*22*/:
                b(16777220);
                b(16777216);
                return;
            case 11:
            case Opcodes.FCONST_1 /*12*/:
            case Opcodes.FCONST_2 /*13*/:
            case Opcodes.FLOAD /*23*/:
                b(16777218);
                return;
            case Opcodes.DCONST_0 /*14*/:
            case Opcodes.DCONST_1 /*15*/:
            case Opcodes.DLOAD /*24*/:
                b(16777219);
                b(16777216);
                return;
            case Opcodes.LDC /*18*/:
                switch (item.b) {
                    case 3:
                        b(16777217);
                        return;
                    case 4:
                        b(16777218);
                        return;
                    case 5:
                        b(16777220);
                        b(16777216);
                        return;
                    case 6:
                        b(16777219);
                        b(16777216);
                        return;
                    case 7:
                        b(24117248 | classWriter.c("java/lang/Class"));
                        return;
                    default:
                        b(24117248 | classWriter.c("java/lang/String"));
                        return;
                }
            case 19:
            case 20:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case SmileConstants.TOKEN_MISC_INTEGER /*36*/:
            case 37:
            case 38:
            case 39:
            case SmileConstants.TOKEN_MISC_FP /*40*/:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case SmileConstants.MAX_SHARED_STRING_LENGTH_BYTES /*65*/:
            case 66:
            case 67:
            case 68:
            case 69:
            case 70:
            case 71:
            case 72:
            case 73:
            case 74:
            case 75:
            case 76:
            case 77:
            case 78:
            case SmileConstants.MIN_BUFFER_FOR_POSSIBLE_SHORT_STRING /*196*/:
            case Opcodes.MULTIANEWARRAY /*197*/:
            default:
                c(i3);
                a(classWriter, item.g);
                return;
            case Opcodes.ALOAD /*25*/:
                b(a(i3));
                return;
            case 46:
            case 51:
            case Opcodes.CALOAD /*52*/:
            case Opcodes.SALOAD /*53*/:
                c(2);
                b(16777217);
                return;
            case 47:
            case Opcodes.D2L /*143*/:
                c(2);
                b(16777220);
                b(16777216);
                return;
            case 48:
                c(2);
                b(16777218);
                return;
            case 49:
            case Opcodes.L2D /*138*/:
                c(2);
                b(16777219);
                b(16777216);
                return;
            case 50:
                c(1);
                b(a() - 268435456);
                return;
            case Opcodes.ISTORE /*54*/:
            case 56:
            case Opcodes.ASTORE /*58*/:
                a(i3, a());
                if (i3 > 0) {
                    int a2 = a(i3 - 1);
                    if (a2 == 16777220 || a2 == 16777219) {
                        a(i3 - 1, 16777216);
                        return;
                    } else if ((251658240 & a2) != 16777216) {
                        a(i3 - 1, a2 | 8388608);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case Opcodes.LSTORE /*55*/:
            case Opcodes.DSTORE /*57*/:
                c(1);
                a(i3, a());
                a(i3 + 1, 16777216);
                if (i3 > 0) {
                    int a3 = a(i3 - 1);
                    if (a3 == 16777220 || a3 == 16777219) {
                        a(i3 - 1, 16777216);
                        return;
                    } else if ((251658240 & a3) != 16777216) {
                        a(i3 - 1, a3 | 8388608);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case Opcodes.IASTORE /*79*/:
            case Opcodes.FASTORE /*81*/:
            case Opcodes.AASTORE /*83*/:
            case Opcodes.BASTORE /*84*/:
            case Opcodes.CASTORE /*85*/:
            case Opcodes.SASTORE /*86*/:
                c(3);
                return;
            case Opcodes.LASTORE /*80*/:
            case Opcodes.DASTORE /*82*/:
                c(4);
                return;
            case Opcodes.POP /*87*/:
            case Opcodes.IFEQ /*153*/:
            case Opcodes.IFNE /*154*/:
            case Opcodes.IFLT /*155*/:
            case Opcodes.IFGE /*156*/:
            case Opcodes.IFGT /*157*/:
            case Opcodes.IFLE /*158*/:
            case Opcodes.TABLESWITCH /*170*/:
            case Opcodes.LOOKUPSWITCH /*171*/:
            case Opcodes.IRETURN /*172*/:
            case Opcodes.FRETURN /*174*/:
            case Opcodes.ARETURN /*176*/:
            case Opcodes.ATHROW /*191*/:
            case Opcodes.MONITORENTER /*194*/:
            case Opcodes.MONITOREXIT /*195*/:
            case Opcodes.IFNULL /*198*/:
            case Opcodes.IFNONNULL /*199*/:
                c(1);
                return;
            case Opcodes.POP2 /*88*/:
            case Opcodes.IF_ICMPEQ /*159*/:
            case 160:
            case Opcodes.IF_ICMPLT /*161*/:
            case Opcodes.IF_ICMPGE /*162*/:
            case Opcodes.IF_ICMPGT /*163*/:
            case Opcodes.IF_ICMPLE /*164*/:
            case Opcodes.IF_ACMPEQ /*165*/:
            case Opcodes.IF_ACMPNE /*166*/:
            case Opcodes.LRETURN /*173*/:
            case Opcodes.DRETURN /*175*/:
                c(2);
                return;
            case Opcodes.DUP /*89*/:
                int a4 = a();
                b(a4);
                b(a4);
                return;
            case Opcodes.DUP_X1 /*90*/:
                int a5 = a();
                int a6 = a();
                b(a5);
                b(a6);
                b(a5);
                return;
            case Opcodes.DUP_X2 /*91*/:
                int a7 = a();
                int a8 = a();
                int a9 = a();
                b(a7);
                b(a9);
                b(a8);
                b(a7);
                return;
            case Opcodes.DUP2 /*92*/:
                int a10 = a();
                int a11 = a();
                b(a11);
                b(a10);
                b(a11);
                b(a10);
                return;
            case Opcodes.DUP2_X1 /*93*/:
                int a12 = a();
                int a13 = a();
                int a14 = a();
                b(a13);
                b(a12);
                b(a14);
                b(a13);
                b(a12);
                return;
            case Opcodes.DUP2_X2 /*94*/:
                int a15 = a();
                int a16 = a();
                int a17 = a();
                int a18 = a();
                b(a16);
                b(a15);
                b(a18);
                b(a17);
                b(a16);
                b(a15);
                return;
            case Opcodes.SWAP /*95*/:
                int a19 = a();
                int a20 = a();
                b(a19);
                b(a20);
                return;
            case 96:
            case 100:
            case Opcodes.IMUL /*104*/:
            case Opcodes.IDIV /*108*/:
            case Opcodes.IREM /*112*/:
            case Opcodes.ISHL /*120*/:
            case Opcodes.ISHR /*122*/:
            case Opcodes.IUSHR /*124*/:
            case Opcodes.IAND /*126*/:
            case 128:
            case Opcodes.IXOR /*130*/:
            case Opcodes.L2I /*136*/:
            case Opcodes.D2I /*142*/:
            case Opcodes.FCMPL /*149*/:
            case Opcodes.FCMPG /*150*/:
                c(2);
                b(16777217);
                return;
            case Opcodes.LADD /*97*/:
            case Opcodes.LSUB /*101*/:
            case Opcodes.LMUL /*105*/:
            case Opcodes.LDIV /*109*/:
            case Opcodes.LREM /*113*/:
            case Opcodes.LAND /*127*/:
            case Opcodes.LOR /*129*/:
            case Opcodes.LXOR /*131*/:
                c(4);
                b(16777220);
                b(16777216);
                return;
            case Opcodes.FADD /*98*/:
            case Opcodes.FSUB /*102*/:
            case Opcodes.FMUL /*106*/:
            case Opcodes.FDIV /*110*/:
            case Opcodes.FREM /*114*/:
            case Opcodes.L2F /*137*/:
            case Opcodes.D2F /*144*/:
                c(2);
                b(16777218);
                return;
            case Opcodes.DADD /*99*/:
            case Opcodes.DSUB /*103*/:
            case Opcodes.DMUL /*107*/:
            case Opcodes.DDIV /*111*/:
            case Opcodes.DREM /*115*/:
                c(4);
                b(16777219);
                b(16777216);
                return;
            case Opcodes.LSHL /*121*/:
            case Opcodes.LSHR /*123*/:
            case Opcodes.LUSHR /*125*/:
                c(3);
                b(16777220);
                b(16777216);
                return;
            case Opcodes.IINC /*132*/:
                a(i3, 16777217);
                return;
            case Opcodes.I2L /*133*/:
            case Opcodes.F2L /*140*/:
                c(1);
                b(16777220);
                b(16777216);
                return;
            case Opcodes.I2F /*134*/:
                c(1);
                b(16777218);
                return;
            case Opcodes.I2D /*135*/:
            case Opcodes.F2D /*141*/:
                c(1);
                b(16777219);
                b(16777216);
                return;
            case Opcodes.F2I /*139*/:
            case Opcodes.ARRAYLENGTH /*190*/:
            case Opcodes.INSTANCEOF /*193*/:
                c(1);
                b(16777217);
                return;
            case Opcodes.LCMP /*148*/:
            case Opcodes.DCMPL /*151*/:
            case Opcodes.DCMPG /*152*/:
                c(4);
                b(16777217);
                return;
            case Opcodes.JSR /*168*/:
            case Opcodes.RET /*169*/:
                throw new RuntimeException("JSR/RET are not supported with computeFrames option");
            case Opcodes.GETSTATIC /*178*/:
                a(classWriter, item.i);
                return;
            case Opcodes.PUTSTATIC /*179*/:
                a(item.i);
                return;
            case Opcodes.GETFIELD /*180*/:
                c(1);
                a(classWriter, item.i);
                return;
            case Opcodes.PUTFIELD /*181*/:
                a(item.i);
                a();
                return;
            case Opcodes.INVOKEVIRTUAL /*182*/:
            case Opcodes.INVOKESPECIAL /*183*/:
            case Opcodes.INVOKESTATIC /*184*/:
            case Opcodes.INVOKEINTERFACE /*185*/:
                a(item.i);
                if (i2 != 184) {
                    int a21 = a();
                    if (i2 == 183 && item.h.charAt(0) == '<') {
                        d(a21);
                    }
                }
                a(classWriter, item.i);
                return;
            case Opcodes.INVOKEDYNAMIC /*186*/:
                a(item.h);
                a(classWriter, item.h);
                return;
            case Opcodes.NEW /*187*/:
                b(25165824 | classWriter.a(item.g, i3));
                return;
            case Opcodes.NEWARRAY /*188*/:
                a();
                switch (i3) {
                    case 4:
                        b(285212681);
                        return;
                    case 5:
                        b(285212683);
                        return;
                    case 6:
                        b(285212674);
                        return;
                    case 7:
                        b(285212675);
                        return;
                    case 8:
                        b(285212682);
                        return;
                    case 9:
                        b(285212684);
                        return;
                    case 10:
                        b(285212673);
                        return;
                    default:
                        b(285212676);
                        return;
                }
            case Opcodes.ANEWARRAY /*189*/:
                String str = item.g;
                a();
                if (str.charAt(0) == '[') {
                    a(classWriter, new StringBuffer().append('[').append(str).toString());
                    return;
                } else {
                    b(classWriter.c(str) | 292552704);
                    return;
                }
            case 192:
                String str2 = item.g;
                a();
                if (str2.charAt(0) == '[') {
                    a(classWriter, str2);
                    return;
                } else {
                    b(classWriter.c(str2) | 24117248);
                    return;
                }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ClassWriter classWriter, int i2, Type[] typeArr, int i3) {
        int i4;
        this.c = new int[i3];
        this.d = new int[0];
        if ((i2 & 8) != 0) {
            i4 = 0;
        } else if ((262144 & i2) == 0) {
            this.c[0] = 24117248 | classWriter.c(classWriter.F);
            i4 = 0 + 1;
        } else {
            this.c[0] = 16777222;
            i4 = 0 + 1;
        }
        int i5 = i4;
        for (Type descriptor : typeArr) {
            int b2 = b(classWriter, descriptor.getDescriptor());
            int i6 = i5 + 1;
            this.c[i5] = b2;
            if (b2 == 16777220 || b2 == 16777219) {
                this.c[i6] = 16777216;
                i5 = i6 + 1;
            } else {
                i5 = i6;
            }
        }
        for (int i7 = i5; i7 < i3; i7++) {
            this.c[i7] = 16777216;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(ClassWriter classWriter, Frame frame, int i2) {
        boolean z;
        boolean z2;
        int i3;
        boolean z3 = false;
        int length = this.c.length;
        int length2 = this.d.length;
        if (frame.c == null) {
            frame.c = new int[length];
            z3 = true;
        }
        boolean z4 = z3;
        for (int i4 = 0; i4 < length; i4++) {
            if (this.e == null || i4 >= this.e.length) {
                i3 = this.c[i4];
            } else {
                i3 = this.e[i4];
                if (i3 == 0) {
                    i3 = this.c[i4];
                } else {
                    int i5 = -268435456 & i3;
                    int i6 = 251658240 & i3;
                    if (i6 != 16777216) {
                        int i7 = i6 == 33554432 ? i5 + this.c[8388607 & i3] : i5 + this.d[length2 - (8388607 & i3)];
                        i3 = ((i3 & 8388608) == 0 || !(i7 == 16777220 || i7 == 16777219)) ? i7 : 16777216;
                    }
                }
            }
            if (this.i != null) {
                i3 = a(classWriter, i3);
            }
            z4 |= a(classWriter, i3, frame.c, i4);
        }
        if (i2 > 0) {
            boolean z5 = z4;
            for (int i8 = 0; i8 < length; i8++) {
                z5 |= a(classWriter, this.c[i8], frame.c, i8);
            }
            if (frame.d == null) {
                frame.d = new int[1];
                z2 = true;
            } else {
                z2 = z5;
            }
            return z2 | a(classWriter, i2, frame.d, 0);
        }
        int length3 = this.d.length + this.b.f;
        if (frame.d == null) {
            frame.d = new int[(this.g + length3)];
            z = true;
        } else {
            z = z4;
        }
        boolean z6 = z;
        for (int i9 = 0; i9 < length3; i9++) {
            int i10 = this.d[i9];
            if (this.i != null) {
                i10 = a(classWriter, i10);
            }
            z6 |= a(classWriter, i10, frame.d, i9);
        }
        for (int i11 = 0; i11 < this.g; i11++) {
            int i12 = this.f[i11];
            int i13 = -268435456 & i12;
            int i14 = 251658240 & i12;
            if (i14 != 16777216) {
                int i15 = i14 == 33554432 ? i13 + this.c[8388607 & i12] : i13 + this.d[length2 - (8388607 & i12)];
                i12 = ((i12 & 8388608) == 0 || !(i15 == 16777220 || i15 == 16777219)) ? i15 : 16777216;
            }
            if (this.i != null) {
                i12 = a(classWriter, i12);
            }
            z6 |= a(classWriter, i12, frame.d, length3 + i11);
        }
        return z6;
    }
}
