package org.codehaus.jackson.org.objectweb.asm.signature;

import org.codehaus.jackson.org.objectweb.asm.Opcodes;

public class SignatureReader {
    private final String a;

    public SignatureReader(String str) {
        this.a = str;
    }

    private static int a(String str, int i, SignatureVisitor signatureVisitor) {
        int i2 = i + 1;
        char charAt = str.charAt(i);
        switch (charAt) {
            case 'B':
            case 'C':
            case 'D':
            case 'F':
            case 'I':
            case 'J':
            case Opcodes.AASTORE:
            case Opcodes.SASTORE:
            case Opcodes.DUP_X1:
                signatureVisitor.visitBaseType(charAt);
                return i2;
            case 'E':
            case 'G':
            case 'H':
            case 'K':
            case 'L':
            case 'M':
            case 'N':
            case Opcodes.IASTORE:
            case Opcodes.LASTORE:
            case Opcodes.FASTORE:
            case Opcodes.DASTORE:
            case Opcodes.CASTORE:
            case Opcodes.POP:
            case Opcodes.POP2:
            case Opcodes.DUP:
            default:
                boolean z = false;
                int i3 = i2;
                int i4 = i2;
                boolean z2 = false;
                while (true) {
                    int i5 = i4 + 1;
                    char charAt2 = str.charAt(i4);
                    switch (charAt2) {
                        case '.':
                        case ';':
                            if (!z) {
                                String substring = str.substring(i3, i5 - 1);
                                if (z2) {
                                    signatureVisitor.visitInnerClassType(substring);
                                } else {
                                    signatureVisitor.visitClassType(substring);
                                }
                            }
                            if (charAt2 != ';') {
                                z2 = true;
                                z = false;
                                i3 = i5;
                                i4 = i5;
                                break;
                            } else {
                                signatureVisitor.visitEnd();
                                return i5;
                            }
                        case '<':
                            String substring2 = str.substring(i3, i5 - 1);
                            if (z2) {
                                signatureVisitor.visitInnerClassType(substring2);
                            } else {
                                signatureVisitor.visitClassType(substring2);
                            }
                            int i6 = i5;
                            while (true) {
                                char charAt3 = str.charAt(i6);
                                switch (charAt3) {
                                    case '*':
                                        i6++;
                                        signatureVisitor.visitTypeArgument();
                                    case '+':
                                    case '-':
                                        i6 = a(str, i6 + 1, signatureVisitor.visitTypeArgument(charAt3));
                                    case '>':
                                        i4 = i6;
                                        z = true;
                                        break;
                                    default:
                                        i6 = a(str, i6, signatureVisitor.visitTypeArgument(SignatureVisitor.INSTANCEOF));
                                }
                            }
                        default:
                            i4 = i5;
                            break;
                    }
                }
            case Opcodes.BASTORE:
                int indexOf = str.indexOf(59, i2);
                signatureVisitor.visitTypeVariable(str.substring(i2, indexOf));
                return indexOf + 1;
            case Opcodes.DUP_X2:
                return a(str, i2, signatureVisitor.visitArrayType());
        }
    }

    public void accept(SignatureVisitor signatureVisitor) {
        int i;
        int i2;
        char charAt;
        String str = this.a;
        int length = str.length();
        if (str.charAt(0) == '<') {
            int i3 = 2;
            while (true) {
                int indexOf = str.indexOf(58, i3);
                signatureVisitor.visitFormalTypeParameter(str.substring(i3 - 1, indexOf));
                int i4 = indexOf + 1;
                char charAt2 = str.charAt(i4);
                if (charAt2 == 'L' || charAt2 == '[' || charAt2 == 'T') {
                    i4 = a(str, i4, signatureVisitor.visitClassBound());
                }
                while (true) {
                    i2 = i4 + 1;
                    charAt = str.charAt(i4);
                    if (charAt != ':') {
                        break;
                    }
                    i4 = a(str, i2, signatureVisitor.visitInterfaceBound());
                }
                if (charAt == '>') {
                    break;
                }
                i3 = i2;
            }
            i = i2;
        } else {
            i = 0;
        }
        if (str.charAt(i) == '(') {
            int i5 = i + 1;
            while (str.charAt(i5) != ')') {
                i5 = a(str, i5, signatureVisitor.visitParameterType());
            }
            int a2 = a(str, i5 + 1, signatureVisitor.visitReturnType());
            while (a2 < length) {
                a2 = a(str, a2 + 1, signatureVisitor.visitExceptionType());
            }
            return;
        }
        int a3 = a(str, i, signatureVisitor.visitSuperclass());
        while (a3 < length) {
            a3 = a(str, a3, signatureVisitor.visitInterface());
        }
    }

    public void acceptType(SignatureVisitor signatureVisitor) {
        a(this.a, 0, signatureVisitor);
    }
}
