package org.codehaus.jackson.org.objectweb.asm;

import com.google.common.base.Ascii;
import org.codehaus.jackson.Base64Variant;
import org.codehaus.jackson.smile.SmileConstants;

class MethodWriter implements MethodVisitor {
    private int A;
    private Handler B;
    private Handler C;
    private int D;
    private ByteVector E;
    private int F;
    private ByteVector G;
    private int H;
    private ByteVector I;
    private Attribute J;
    private boolean K;
    private int L;
    private final int M;
    private Label N;
    private Label O;
    private Label P;
    private int Q;
    private int R;
    private int S;
    MethodWriter a;
    final ClassWriter b;
    private int c;
    private final int d;
    private final int e;
    private final String f;
    String g;
    int h;
    int i;
    int j;
    int[] k;
    private ByteVector l;
    private AnnotationWriter m;
    private AnnotationWriter n;
    private AnnotationWriter[] o;
    private AnnotationWriter[] p;
    private Attribute q;
    private ByteVector r = new ByteVector();
    private int s;
    private int t;
    private int u;
    private ByteVector v;
    private int w;
    private int[] x;
    private int y;
    private int[] z;

    MethodWriter(ClassWriter classWriter, int i2, String str, String str2, String str3, String[] strArr, boolean z2, boolean z3) {
        if (classWriter.A == null) {
            classWriter.A = this;
        } else {
            classWriter.B.a = this;
        }
        classWriter.B = this;
        this.b = classWriter;
        this.c = i2;
        this.d = classWriter.newUTF8(str);
        this.e = classWriter.newUTF8(str2);
        this.f = str2;
        this.g = str3;
        if (strArr != null && strArr.length > 0) {
            this.j = strArr.length;
            this.k = new int[this.j];
            for (int i3 = 0; i3 < this.j; i3++) {
                this.k[i3] = classWriter.newClass(strArr[i3]);
            }
        }
        this.M = z3 ? 0 : z2 ? 1 : 2;
        if (z2 || z3) {
            if (z3 && "<init>".equals(str)) {
                this.c |= 262144;
            }
            int argumentsAndReturnSizes = Type.getArgumentsAndReturnSizes(this.f) >> 2;
            this.t = (i2 & 8) != 0 ? argumentsAndReturnSizes - 1 : argumentsAndReturnSizes;
            this.N = new Label();
            this.N.a |= 8;
            visitLabel(this.N);
        }
    }

    static int a(byte[] bArr, int i2) {
        return ((bArr[i2] & SmileConstants.BYTE_MARKER_END_OF_CONTENT) << Ascii.CAN) | ((bArr[i2 + 1] & SmileConstants.BYTE_MARKER_END_OF_CONTENT) << Ascii.DLE) | ((bArr[i2 + 2] & SmileConstants.BYTE_MARKER_END_OF_CONTENT) << 8) | (bArr[i2 + 3] & SmileConstants.BYTE_MARKER_END_OF_CONTENT);
    }

    static int a(int[] iArr, int[] iArr2, int i2, int i3) {
        int i4 = i3 - i2;
        for (int i5 = 0; i5 < iArr.length; i5++) {
            if (i2 < iArr[i5] && iArr[i5] <= i3) {
                i4 += iArr2[i5];
            } else if (i3 < iArr[i5] && iArr[i5] <= i2) {
                i4 -= iArr2[i5];
            }
        }
        return i4;
    }

    private void a(int i2, int i3) {
        for (int i4 = i2; i4 < i3; i4++) {
            int i5 = this.z[i4];
            int i6 = -268435456 & i5;
            if (i6 == 0) {
                int i7 = i5 & 1048575;
                switch (i5 & 267386880) {
                    case 24117248:
                        this.v.putByte(7).putShort(this.b.newClass(this.b.E[i7].g));
                        continue;
                    case 25165824:
                        this.v.putByte(8).putShort(this.b.E[i7].c);
                        continue;
                    default:
                        this.v.putByte(i7);
                        continue;
                }
            } else {
                StringBuffer stringBuffer = new StringBuffer();
                int i8 = i6 >> 28;
                while (true) {
                    int i9 = i8 - 1;
                    if (i8 > 0) {
                        stringBuffer.append('[');
                        i8 = i9;
                    } else {
                        if ((i5 & 267386880) != 24117248) {
                            switch (i5 & 15) {
                                case 1:
                                    stringBuffer.append('I');
                                    break;
                                case 2:
                                    stringBuffer.append('F');
                                    break;
                                case 3:
                                    stringBuffer.append('D');
                                    break;
                                case 4:
                                case 5:
                                case 6:
                                case 7:
                                case 8:
                                default:
                                    stringBuffer.append('J');
                                    break;
                                case 9:
                                    stringBuffer.append('Z');
                                    break;
                                case 10:
                                    stringBuffer.append('B');
                                    break;
                                case 11:
                                    stringBuffer.append('C');
                                    break;
                                case Opcodes.FCONST_1:
                                    stringBuffer.append('S');
                                    break;
                            }
                        } else {
                            stringBuffer.append('L');
                            stringBuffer.append(this.b.E[i5 & 1048575].g);
                            stringBuffer.append(';');
                        }
                        this.v.putByte(7).putShort(this.b.newClass(stringBuffer.toString()));
                    }
                }
            }
        }
    }

    private void a(int i2, int i3, int i4) {
        int i5 = i3 + 3 + i4;
        if (this.z == null || this.z.length < i5) {
            this.z = new int[i5];
        }
        this.z[0] = i2;
        this.z[1] = i3;
        this.z[2] = i4;
        this.y = 3;
    }

    private void a(int i2, Label label) {
        Edge edge = new Edge();
        edge.a = i2;
        edge.b = label;
        edge.c = this.P.j;
        this.P.j = edge;
    }

    private void a(Object obj) {
        if (obj instanceof String) {
            this.v.putByte(7).putShort(this.b.newClass((String) obj));
        } else if (obj instanceof Integer) {
            this.v.putByte(((Integer) obj).intValue());
        } else {
            this.v.putByte(8).putShort(((Label) obj).c);
        }
    }

    private void a(Label label, Label[] labelArr) {
        if (this.P != null) {
            if (this.M == 0) {
                this.P.h.a((int) Opcodes.LOOKUPSWITCH, 0, (ClassWriter) null, (Item) null);
                a(0, label);
                label.a().a |= 16;
                for (int i2 = 0; i2 < labelArr.length; i2++) {
                    a(0, labelArr[i2]);
                    labelArr[i2].a().a |= 16;
                }
            } else {
                this.Q--;
                a(this.Q, label);
                for (Label a2 : labelArr) {
                    a(this.Q, a2);
                }
            }
            e();
        }
    }

    static void a(byte[] bArr, int i2, int i3) {
        bArr[i2] = (byte) (i3 >>> 8);
        bArr[i2 + 1] = (byte) i3;
    }

    static void a(int[] iArr, int[] iArr2, Label label) {
        if ((label.a & 4) == 0) {
            label.c = a(iArr, iArr2, 0, label.c);
            label.a |= 4;
        }
    }

    static short b(byte[] bArr, int i2) {
        return (short) (((bArr[i2] & SmileConstants.BYTE_MARKER_END_OF_CONTENT) << 8) | (bArr[i2 + 1] & SmileConstants.BYTE_MARKER_END_OF_CONTENT));
    }

    private void b() {
        if (this.x != null) {
            if (this.v == null) {
                this.v = new ByteVector();
            }
            c();
            this.u++;
        }
        this.x = this.z;
        this.z = null;
    }

    private void b(Frame frame) {
        int[] iArr = frame.c;
        int[] iArr2 = frame.d;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (i2 < iArr.length) {
            int i5 = iArr[i2];
            if (i5 == 16777216) {
                i4++;
            } else {
                i3 += i4 + 1;
                i4 = 0;
            }
            if (i5 == 16777220 || i5 == 16777219) {
                i2++;
            }
            i2++;
        }
        int i6 = 0;
        int i7 = 0;
        while (i6 < iArr2.length) {
            int i8 = iArr2[i6];
            i7++;
            if (i8 == 16777220 || i8 == 16777219) {
                i6++;
            }
            i6++;
        }
        a(frame.b.c, i3, i7);
        int i9 = 0;
        while (i3 > 0) {
            int i10 = iArr[i9];
            int[] iArr3 = this.z;
            int i11 = this.y;
            this.y = i11 + 1;
            iArr3[i11] = i10;
            if (i10 == 16777220 || i10 == 16777219) {
                i9++;
            }
            i9++;
            i3--;
        }
        int i12 = 0;
        while (i12 < iArr2.length) {
            int i13 = iArr2[i12];
            int[] iArr4 = this.z;
            int i14 = this.y;
            this.y = i14 + 1;
            iArr4[i14] = i13;
            if (i13 == 16777220 || i13 == 16777219) {
                i12++;
            }
            i12++;
        }
        b();
    }

    static int c(byte[] bArr, int i2) {
        return ((bArr[i2] & SmileConstants.BYTE_MARKER_END_OF_CONTENT) << 8) | (bArr[i2 + 1] & SmileConstants.BYTE_MARKER_END_OF_CONTENT);
    }

    private void c() {
        char c2;
        int i2;
        int i3;
        char c3;
        int i4 = 0;
        int i5 = this.z[1];
        int i6 = this.z[2];
        if ((this.b.b & 65535) < 50) {
            this.v.putShort(this.z[0]).putShort(i5);
            a(3, i5 + 3);
            this.v.putShort(i6);
            a(i5 + 3, i5 + 3 + i6);
            return;
        }
        int i7 = this.x[1];
        int i8 = this.u == 0 ? this.z[0] : (this.z[0] - this.x[0]) - 1;
        if (i6 == 0) {
            int i9 = i5 - i7;
            switch (i9) {
                case -3:
                case Base64Variant.BASE64_VALUE_PADDING:
                case -1:
                    c3 = 248;
                    i2 = i5;
                    break;
                case 0:
                    i2 = i7;
                    c3 = i8 < 64 ? (char) 0 : 251;
                    break;
                case 1:
                case 2:
                case 3:
                    i2 = i7;
                    c3 = 252;
                    break;
                default:
                    i2 = i7;
                    c3 = 255;
                    break;
            }
            int i10 = i9;
            c2 = c3;
            i3 = i10;
        } else if (i5 == i7 && i6 == 1) {
            c2 = i8 < 63 ? '@' : 247;
            i2 = i7;
            i3 = 0;
        } else {
            c2 = 255;
            i2 = i7;
            i3 = 0;
        }
        if (c2 != 255) {
            int i11 = 3;
            while (true) {
                if (i4 < i2) {
                    if (this.z[i11] != this.x[i11]) {
                        c2 = 255;
                    } else {
                        i11++;
                        i4++;
                    }
                }
            }
        }
        switch (c2) {
            case 0:
                this.v.putByte(i8);
                return;
            case '@':
                this.v.putByte(i8 + 64);
                a(i5 + 3, i5 + 4);
                return;
            case 247:
                this.v.putByte(247).putShort(i8);
                a(i5 + 3, i5 + 4);
                return;
            case 248:
                this.v.putByte(i3 + 251).putShort(i8);
                return;
            case 251:
                this.v.putByte(251).putShort(i8);
                return;
            case SmileConstants.INT_MARKER_END_OF_STRING:
                this.v.putByte(i3 + 251).putShort(i8);
                a(i2 + 3, i5 + 3);
                return;
            default:
                this.v.putByte(255).putShort(i8).putShort(i5);
                a(3, i5 + 3);
                this.v.putShort(i6);
                a(i5 + 3, i5 + 3 + i6);
                return;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r2v28 */
    /* JADX WARN: Type inference failed for: r2v31, types: [int] */
    /* JADX WARN: Type inference failed for: r2v32, types: [int] */
    /* JADX WARN: Type inference failed for: r2v33, types: [int] */
    /* JADX WARN: Type inference failed for: r2v34, types: [int] */
    /* JADX WARN: Type inference failed for: r2v35, types: [int] */
    /* JADX WARN: Type inference failed for: r2v36, types: [int] */
    /* JADX WARN: Type inference failed for: r6v43, types: [int] */
    /* JADX WARN: Type inference failed for: r6v46, types: [int] */
    /* JADX WARN: Type inference failed for: r2v37, types: [int] */
    /* JADX WARN: Type inference failed for: r2v38, types: [int] */
    /* JADX WARN: Type inference failed for: r6v91, types: [int] */
    /* JADX WARN: Type inference failed for: r6v94, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void d() {
        /*
            r13 = this;
            org.codehaus.jackson.org.objectweb.asm.ByteVector r0 = r13.r
            byte[] r0 = r0.a
            r1 = 0
            int[] r1 = new int[r1]
            r2 = 0
            int[] r2 = new int[r2]
            org.codehaus.jackson.org.objectweb.asm.ByteVector r3 = r13.r
            int r3 = r3.b
            boolean[] r3 = new boolean[r3]
            r4 = 3
            r12 = r4
            r4 = r1
            r1 = r12
        L_0x0014:
            r5 = 3
            if (r1 != r5) goto L_0x0018
            r1 = 2
        L_0x0018:
            r5 = 0
            r12 = r5
            r5 = r4
            r4 = r2
            r2 = r1
            r1 = r12
        L_0x001e:
            int r6 = r0.length
            if (r1 >= r6) goto L_0x0132
            byte r6 = r0[r1]
            r6 = r6 & 255(0xff, float:3.57E-43)
            r7 = 0
            byte[] r8 = org.codehaus.jackson.org.objectweb.asm.ClassWriter.a
            byte r8 = r8[r6]
            switch(r8) {
                case 0: goto L_0x0059;
                case 1: goto L_0x0120;
                case 2: goto L_0x0126;
                case 3: goto L_0x0120;
                case 4: goto L_0x0059;
                case 5: goto L_0x0126;
                case 6: goto L_0x0126;
                case 7: goto L_0x012c;
                case 8: goto L_0x005e;
                case 9: goto L_0x00a6;
                case 10: goto L_0x0120;
                case 11: goto L_0x0126;
                case 12: goto L_0x0126;
                case 13: goto L_0x00ab;
                case 14: goto L_0x00df;
                case 15: goto L_0x002d;
                case 16: goto L_0x010a;
                default: goto L_0x002d;
            }
        L_0x002d:
            int r1 = r1 + 4
            r6 = r1
            r1 = r7
        L_0x0031:
            if (r1 == 0) goto L_0x03a1
            int r7 = r5.length
            int r7 = r7 + 1
            int[] r7 = new int[r7]
            int r8 = r4.length
            int r8 = r8 + 1
            int[] r8 = new int[r8]
            r9 = 0
            r10 = 0
            int r11 = r5.length
            java.lang.System.arraycopy(r5, r9, r7, r10, r11)
            r9 = 0
            r10 = 0
            int r11 = r4.length
            java.lang.System.arraycopy(r4, r9, r8, r10, r11)
            int r5 = r5.length
            r7[r5] = r6
            int r4 = r4.length
            r8[r4] = r1
            if (r1 <= 0) goto L_0x039c
            r1 = 3
            r2 = r8
            r4 = r7
        L_0x0054:
            r5 = r4
            r4 = r2
            r2 = r1
            r1 = r6
            goto L_0x001e
        L_0x0059:
            int r1 = r1 + 1
            r6 = r1
            r1 = r7
            goto L_0x0031
        L_0x005e:
            r8 = 201(0xc9, float:2.82E-43)
            if (r6 <= r8) goto L_0x0099
            r8 = 218(0xda, float:3.05E-43)
            if (r6 >= r8) goto L_0x0095
            r8 = 49
            int r6 = r6 - r8
        L_0x0069:
            int r8 = r1 + 1
            int r8 = c(r0, r8)
            int r8 = r8 + r1
            r12 = r8
            r8 = r6
            r6 = r12
        L_0x0073:
            int r6 = a(r5, r4, r1, r6)
            r9 = -32768(0xffffffffffff8000, float:NaN)
            if (r6 < r9) goto L_0x007f
            r9 = 32767(0x7fff, float:4.5916E-41)
            if (r6 <= r9) goto L_0x03ac
        L_0x007f:
            boolean r6 = r3[r1]
            if (r6 != 0) goto L_0x03ac
            r6 = 167(0xa7, float:2.34E-43)
            if (r8 == r6) goto L_0x008b
            r6 = 168(0xa8, float:2.35E-43)
            if (r8 != r6) goto L_0x00a4
        L_0x008b:
            r6 = 2
        L_0x008c:
            r7 = 1
            r3[r1] = r7
        L_0x008f:
            int r1 = r1 + 3
            r12 = r6
            r6 = r1
            r1 = r12
            goto L_0x0031
        L_0x0095:
            r8 = 20
            int r6 = r6 - r8
            goto L_0x0069
        L_0x0099:
            int r8 = r1 + 1
            short r8 = b(r0, r8)
            int r8 = r8 + r1
            r12 = r8
            r8 = r6
            r6 = r12
            goto L_0x0073
        L_0x00a4:
            r6 = 5
            goto L_0x008c
        L_0x00a6:
            int r1 = r1 + 5
            r6 = r1
            r1 = r7
            goto L_0x0031
        L_0x00ab:
            r6 = 1
            if (r2 != r6) goto L_0x00d5
            r6 = 0
            int r6 = a(r5, r4, r6, r1)
            r6 = r6 & 3
            int r6 = -r6
        L_0x00b6:
            int r7 = r1 + 4
            r1 = r1 & 3
            int r1 = r7 - r1
            int r7 = r1 + 8
            int r7 = a(r0, r7)
            int r8 = r1 + 4
            int r8 = a(r0, r8)
            int r7 = r7 - r8
            int r7 = r7 + 1
            int r7 = r7 * 4
            int r7 = r7 + 12
            int r1 = r1 + r7
            r12 = r6
            r6 = r1
            r1 = r12
            goto L_0x0031
        L_0x00d5:
            boolean r6 = r3[r1]
            if (r6 != 0) goto L_0x03a9
            r6 = r1 & 3
            r7 = 1
            r3[r1] = r7
            goto L_0x00b6
        L_0x00df:
            r6 = 1
            if (r2 != r6) goto L_0x0100
            r6 = 0
            int r6 = a(r5, r4, r6, r1)
            r6 = r6 & 3
            int r6 = -r6
        L_0x00ea:
            int r7 = r1 + 4
            r1 = r1 & 3
            int r1 = r7 - r1
            int r7 = r1 + 4
            int r7 = a(r0, r7)
            int r7 = r7 * 8
            int r7 = r7 + 8
            int r1 = r1 + r7
            r12 = r6
            r6 = r1
            r1 = r12
            goto L_0x0031
        L_0x0100:
            boolean r6 = r3[r1]
            if (r6 != 0) goto L_0x03a6
            r6 = r1 & 3
            r7 = 1
            r3[r1] = r7
            goto L_0x00ea
        L_0x010a:
            int r6 = r1 + 1
            byte r6 = r0[r6]
            r6 = r6 & 255(0xff, float:3.57E-43)
            r8 = 132(0x84, float:1.85E-43)
            if (r6 != r8) goto L_0x011a
            int r1 = r1 + 6
            r6 = r1
            r1 = r7
            goto L_0x0031
        L_0x011a:
            int r1 = r1 + 4
            r6 = r1
            r1 = r7
            goto L_0x0031
        L_0x0120:
            int r1 = r1 + 2
            r6 = r1
            r1 = r7
            goto L_0x0031
        L_0x0126:
            int r1 = r1 + 3
            r6 = r1
            r1 = r7
            goto L_0x0031
        L_0x012c:
            int r1 = r1 + 5
            r6 = r1
            r1 = r7
            goto L_0x0031
        L_0x0132:
            r1 = 3
            if (r2 >= r1) goto L_0x0399
            int r1 = r2 + -1
        L_0x0137:
            if (r1 != 0) goto L_0x0395
            org.codehaus.jackson.org.objectweb.asm.ByteVector r1 = new org.codehaus.jackson.org.objectweb.asm.ByteVector
            org.codehaus.jackson.org.objectweb.asm.ByteVector r2 = r13.r
            int r2 = r2.b
            r1.<init>(r2)
            r2 = 0
        L_0x0143:
            org.codehaus.jackson.org.objectweb.asm.ByteVector r6 = r13.r
            int r6 = r6.b
            if (r2 >= r6) goto L_0x02b6
            byte r6 = r0[r2]
            r6 = r6 & 255(0xff, float:3.57E-43)
            byte[] r7 = org.codehaus.jackson.org.objectweb.asm.ClassWriter.a
            byte r7 = r7[r6]
            switch(r7) {
                case 0: goto L_0x015b;
                case 1: goto L_0x029e;
                case 2: goto L_0x02a6;
                case 3: goto L_0x029e;
                case 4: goto L_0x015b;
                case 5: goto L_0x02a6;
                case 6: goto L_0x02a6;
                case 7: goto L_0x02ae;
                case 8: goto L_0x0161;
                case 9: goto L_0x01ca;
                case 10: goto L_0x029e;
                case 11: goto L_0x02a6;
                case 12: goto L_0x02a6;
                case 13: goto L_0x01df;
                case 14: goto L_0x0237;
                case 15: goto L_0x0154;
                case 16: goto L_0x0284;
                default: goto L_0x0154;
            }
        L_0x0154:
            r6 = 4
            r1.putByteArray(r0, r2, r6)
            int r2 = r2 + 4
            goto L_0x0143
        L_0x015b:
            r1.putByte(r6)
            int r2 = r2 + 1
            goto L_0x0143
        L_0x0161:
            r7 = 201(0xc9, float:2.82E-43)
            if (r6 <= r7) goto L_0x0191
            r7 = 218(0xda, float:3.05E-43)
            if (r6 >= r7) goto L_0x018d
            r7 = 49
            int r6 = r6 - r7
        L_0x016c:
            int r7 = r2 + 1
            int r7 = c(r0, r7)
            int r7 = r7 + r2
            r12 = r7
            r7 = r6
            r6 = r12
        L_0x0176:
            int r6 = a(r5, r4, r2, r6)
            boolean r8 = r3[r2]
            if (r8 == 0) goto L_0x01c3
            r8 = 167(0xa7, float:2.34E-43)
            if (r7 != r8) goto L_0x019c
            r7 = 200(0xc8, float:2.8E-43)
            r1.putByte(r7)
        L_0x0187:
            r1.putInt(r6)
        L_0x018a:
            int r2 = r2 + 3
            goto L_0x0143
        L_0x018d:
            r7 = 20
            int r6 = r6 - r7
            goto L_0x016c
        L_0x0191:
            int r7 = r2 + 1
            short r7 = b(r0, r7)
            int r7 = r7 + r2
            r12 = r7
            r7 = r6
            r6 = r12
            goto L_0x0176
        L_0x019c:
            r8 = 168(0xa8, float:2.35E-43)
            if (r7 != r8) goto L_0x01a6
            r7 = 201(0xc9, float:2.82E-43)
            r1.putByte(r7)
            goto L_0x0187
        L_0x01a6:
            r8 = 166(0xa6, float:2.33E-43)
            if (r7 > r8) goto L_0x01c0
            int r7 = r7 + 1
            r7 = r7 ^ 1
            r8 = 1
            int r7 = r7 - r8
        L_0x01b0:
            r1.putByte(r7)
            r7 = 8
            r1.putShort(r7)
            r7 = 200(0xc8, float:2.8E-43)
            r1.putByte(r7)
            int r6 = r6 + -3
            goto L_0x0187
        L_0x01c0:
            r7 = r7 ^ 1
            goto L_0x01b0
        L_0x01c3:
            r1.putByte(r7)
            r1.putShort(r6)
            goto L_0x018a
        L_0x01ca:
            int r7 = r2 + 1
            int r7 = a(r0, r7)
            int r7 = r7 + r2
            int r7 = a(r5, r4, r2, r7)
            r1.putByte(r6)
            r1.putInt(r7)
            int r2 = r2 + 5
            goto L_0x0143
        L_0x01df:
            int r6 = r2 + 4
            r7 = r2 & 3
            int r6 = r6 - r7
            r7 = 170(0xaa, float:2.38E-43)
            r1.putByte(r7)
            r7 = 0
            r8 = 0
            r9 = 4
            int r10 = r1.b
            int r10 = r10 % 4
            int r9 = r9 - r10
            int r9 = r9 % 4
            r1.putByteArray(r7, r8, r9)
            int r7 = a(r0, r6)
            int r7 = r7 + r2
            int r6 = r6 + 4
            int r7 = a(r5, r4, r2, r7)
            r1.putInt(r7)
            int r7 = a(r0, r6)
            int r6 = r6 + 4
            r1.putInt(r7)
            int r8 = a(r0, r6)
            int r7 = r8 - r7
            int r7 = r7 + 1
            int r6 = r6 + 4
            r8 = 4
            int r8 = r6 - r8
            int r8 = a(r0, r8)
            r1.putInt(r8)
            r12 = r7
            r7 = r6
            r6 = r12
        L_0x0224:
            if (r6 <= 0) goto L_0x0392
            int r8 = a(r0, r7)
            int r8 = r8 + r2
            int r7 = r7 + 4
            int r8 = a(r5, r4, r2, r8)
            r1.putInt(r8)
            int r6 = r6 + -1
            goto L_0x0224
        L_0x0237:
            int r6 = r2 + 4
            r7 = r2 & 3
            int r6 = r6 - r7
            r7 = 171(0xab, float:2.4E-43)
            r1.putByte(r7)
            r7 = 0
            r8 = 0
            r9 = 4
            int r10 = r1.b
            int r10 = r10 % 4
            int r9 = r9 - r10
            int r9 = r9 % 4
            r1.putByteArray(r7, r8, r9)
            int r7 = a(r0, r6)
            int r7 = r7 + r2
            int r6 = r6 + 4
            int r7 = a(r5, r4, r2, r7)
            r1.putInt(r7)
            int r7 = a(r0, r6)
            int r6 = r6 + 4
            r1.putInt(r7)
            r12 = r7
            r7 = r6
            r6 = r12
        L_0x0268:
            if (r6 <= 0) goto L_0x0392
            int r8 = a(r0, r7)
            r1.putInt(r8)
            int r7 = r7 + 4
            int r8 = a(r0, r7)
            int r8 = r8 + r2
            int r7 = r7 + 4
            int r8 = a(r5, r4, r2, r8)
            r1.putInt(r8)
            int r6 = r6 + -1
            goto L_0x0268
        L_0x0284:
            int r6 = r2 + 1
            byte r6 = r0[r6]
            r6 = r6 & 255(0xff, float:3.57E-43)
            r7 = 132(0x84, float:1.85E-43)
            if (r6 != r7) goto L_0x0296
            r6 = 6
            r1.putByteArray(r0, r2, r6)
            int r2 = r2 + 6
            goto L_0x0143
        L_0x0296:
            r6 = 4
            r1.putByteArray(r0, r2, r6)
            int r2 = r2 + 4
            goto L_0x0143
        L_0x029e:
            r6 = 2
            r1.putByteArray(r0, r2, r6)
            int r2 = r2 + 2
            goto L_0x0143
        L_0x02a6:
            r6 = 3
            r1.putByteArray(r0, r2, r6)
            int r2 = r2 + 3
            goto L_0x0143
        L_0x02ae:
            r6 = 5
            r1.putByteArray(r0, r2, r6)
            int r2 = r2 + 5
            goto L_0x0143
        L_0x02b6:
            int r0 = r13.u
            if (r0 <= 0) goto L_0x0309
            int r0 = r13.M
            if (r0 != 0) goto L_0x0304
            r0 = 0
            r13.u = r0
            r0 = 0
            r13.v = r0
            r0 = 0
            r13.x = r0
            r0 = 0
            r13.z = r0
            org.codehaus.jackson.org.objectweb.asm.Frame r0 = new org.codehaus.jackson.org.objectweb.asm.Frame
            r0.<init>()
            org.codehaus.jackson.org.objectweb.asm.Label r2 = r13.N
            r0.b = r2
            java.lang.String r2 = r13.f
            org.codehaus.jackson.org.objectweb.asm.Type[] r2 = org.codehaus.jackson.org.objectweb.asm.Type.getArgumentTypes(r2)
            org.codehaus.jackson.org.objectweb.asm.ClassWriter r6 = r13.b
            int r7 = r13.c
            int r8 = r13.t
            r0.a(r6, r7, r2, r8)
            r13.b(r0)
            org.codehaus.jackson.org.objectweb.asm.Label r0 = r13.N
        L_0x02e7:
            if (r0 == 0) goto L_0x0309
            int r2 = r0.c
            r6 = 3
            int r2 = r2 - r6
            int r6 = r0.a
            r6 = r6 & 32
            if (r6 != 0) goto L_0x02f9
            if (r2 < 0) goto L_0x0301
            boolean r2 = r3[r2]
            if (r2 == 0) goto L_0x0301
        L_0x02f9:
            a(r5, r4, r0)
            org.codehaus.jackson.org.objectweb.asm.Frame r2 = r0.h
            r13.b(r2)
        L_0x0301:
            org.codehaus.jackson.org.objectweb.asm.Label r0 = r0.i
            goto L_0x02e7
        L_0x0304:
            org.codehaus.jackson.org.objectweb.asm.ClassWriter r0 = r13.b
            r2 = 1
            r0.I = r2
        L_0x0309:
            org.codehaus.jackson.org.objectweb.asm.Handler r0 = r13.B
        L_0x030b:
            if (r0 == 0) goto L_0x031f
            org.codehaus.jackson.org.objectweb.asm.Label r2 = r0.a
            a(r5, r4, r2)
            org.codehaus.jackson.org.objectweb.asm.Label r2 = r0.b
            a(r5, r4, r2)
            org.codehaus.jackson.org.objectweb.asm.Label r2 = r0.c
            a(r5, r4, r2)
            org.codehaus.jackson.org.objectweb.asm.Handler r0 = r0.f
            goto L_0x030b
        L_0x031f:
            r0 = 0
        L_0x0320:
            r2 = 2
            if (r0 >= r2) goto L_0x0357
            if (r0 != 0) goto L_0x0351
            org.codehaus.jackson.org.objectweb.asm.ByteVector r2 = r13.E
        L_0x0327:
            if (r2 == 0) goto L_0x0354
            byte[] r3 = r2.a
            r6 = 0
        L_0x032c:
            int r7 = r2.b
            if (r6 >= r7) goto L_0x0354
            int r7 = c(r3, r6)
            r8 = 0
            int r8 = a(r5, r4, r8, r7)
            a(r3, r6, r8)
            int r9 = r6 + 2
            int r9 = c(r3, r9)
            int r7 = r7 + r9
            r9 = 0
            int r7 = a(r5, r4, r9, r7)
            int r7 = r7 - r8
            int r8 = r6 + 2
            a(r3, r8, r7)
            int r6 = r6 + 10
            goto L_0x032c
        L_0x0351:
            org.codehaus.jackson.org.objectweb.asm.ByteVector r2 = r13.G
            goto L_0x0327
        L_0x0354:
            int r0 = r0 + 1
            goto L_0x0320
        L_0x0357:
            org.codehaus.jackson.org.objectweb.asm.ByteVector r0 = r13.I
            if (r0 == 0) goto L_0x0375
            org.codehaus.jackson.org.objectweb.asm.ByteVector r0 = r13.I
            byte[] r0 = r0.a
            r2 = 0
        L_0x0360:
            org.codehaus.jackson.org.objectweb.asm.ByteVector r3 = r13.I
            int r3 = r3.b
            if (r2 >= r3) goto L_0x0375
            r3 = 0
            int r6 = c(r0, r2)
            int r3 = a(r5, r4, r3, r6)
            a(r0, r2, r3)
            int r2 = r2 + 4
            goto L_0x0360
        L_0x0375:
            org.codehaus.jackson.org.objectweb.asm.Attribute r0 = r13.J
        L_0x0377:
            if (r0 == 0) goto L_0x038f
            org.codehaus.jackson.org.objectweb.asm.Label[] r2 = r0.getLabels()
            if (r2 == 0) goto L_0x038c
            int r3 = r2.length
            r6 = 1
            int r3 = r3 - r6
        L_0x0382:
            if (r3 < 0) goto L_0x038c
            r6 = r2[r3]
            a(r5, r4, r6)
            int r3 = r3 + -1
            goto L_0x0382
        L_0x038c:
            org.codehaus.jackson.org.objectweb.asm.Attribute r0 = r0.a
            goto L_0x0377
        L_0x038f:
            r13.r = r1
            return
        L_0x0392:
            r2 = r7
            goto L_0x0143
        L_0x0395:
            r2 = r4
            r4 = r5
            goto L_0x0014
        L_0x0399:
            r1 = r2
            goto L_0x0137
        L_0x039c:
            r1 = r2
            r4 = r7
            r2 = r8
            goto L_0x0054
        L_0x03a1:
            r1 = r2
            r2 = r4
            r4 = r5
            goto L_0x0054
        L_0x03a6:
            r6 = r7
            goto L_0x00ea
        L_0x03a9:
            r6 = r7
            goto L_0x00b6
        L_0x03ac:
            r6 = r7
            goto L_0x008f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.org.objectweb.asm.MethodWriter.d():void");
    }

    private void e() {
        if (this.M == 0) {
            Label label = new Label();
            label.h = new Frame();
            label.h.b = label;
            label.a(this, this.r.b, this.r.a);
            this.O.i = label;
            this.O = label;
        } else {
            this.P.g = this.R;
        }
        this.P = null;
    }

    /* access modifiers changed from: package-private */
    public final int a() {
        int i2;
        int i3;
        if (this.h != 0) {
            return this.i + 6;
        }
        if (this.K) {
            d();
        }
        int i4 = 8;
        if (this.r.b > 0) {
            this.b.newUTF8("Code");
            int i5 = 8 + this.r.b + 18 + (this.A * 8);
            if (this.E != null) {
                this.b.newUTF8("LocalVariableTable");
                i5 += this.E.b + 8;
            }
            if (this.G != null) {
                this.b.newUTF8("LocalVariableTypeTable");
                i5 += this.G.b + 8;
            }
            if (this.I != null) {
                this.b.newUTF8("LineNumberTable");
                i5 += this.I.b + 8;
            }
            if (this.v != null) {
                this.b.newUTF8((this.b.b & 65535) >= 50 ? "StackMapTable" : "StackMap");
                i3 = i5 + this.v.b + 8;
            } else {
                i3 = i5;
            }
            i4 = this.J != null ? this.J.a(this.b, this.r.a, this.r.b, this.s, this.t) + i3 : i3;
        }
        if (this.j > 0) {
            this.b.newUTF8("Exceptions");
            i4 += (this.j * 2) + 8;
        }
        if ((this.c & 4096) != 0 && ((this.b.b & 65535) < 49 || (this.c & 262144) != 0)) {
            this.b.newUTF8("Synthetic");
            i4 += 6;
        }
        if ((this.c & Opcodes.ACC_DEPRECATED) != 0) {
            this.b.newUTF8("Deprecated");
            i4 += 6;
        }
        if (this.g != null) {
            this.b.newUTF8("Signature");
            this.b.newUTF8(this.g);
            i4 += 8;
        }
        if (this.l != null) {
            this.b.newUTF8("AnnotationDefault");
            i4 += this.l.b + 6;
        }
        if (this.m != null) {
            this.b.newUTF8("RuntimeVisibleAnnotations");
            i4 += this.m.a() + 8;
        }
        if (this.n != null) {
            this.b.newUTF8("RuntimeInvisibleAnnotations");
            i4 += this.n.a() + 8;
        }
        if (this.o != null) {
            this.b.newUTF8("RuntimeVisibleParameterAnnotations");
            int length = i4 + ((this.o.length - this.S) * 2) + 7;
            for (int length2 = this.o.length - 1; length2 >= this.S; length2--) {
                length += this.o[length2] == null ? 0 : this.o[length2].a();
            }
            i4 = length;
        }
        if (this.p != null) {
            this.b.newUTF8("RuntimeInvisibleParameterAnnotations");
            int length3 = i4 + ((this.p.length - this.S) * 2) + 7;
            for (int length4 = this.p.length - 1; length4 >= this.S; length4--) {
                length3 += this.p[length4] == null ? 0 : this.p[length4].a();
            }
            i2 = length3;
        } else {
            i2 = i4;
        }
        return this.q != null ? this.q.a(this.b, null, 0, -1, -1) + i2 : i2;
    }

    /* access modifiers changed from: package-private */
    public final void a(ByteVector byteVector) {
        byteVector.putShort(((393216 | ((this.c & 262144) / 64)) ^ -1) & this.c).putShort(this.d).putShort(this.e);
        if (this.h != 0) {
            byteVector.putByteArray(this.b.J.b, this.h, this.i);
            return;
        }
        int i2 = this.r.b > 0 ? 0 + 1 : 0;
        if (this.j > 0) {
            i2++;
        }
        if ((this.c & 4096) != 0 && ((this.b.b & 65535) < 49 || (this.c & 262144) != 0)) {
            i2++;
        }
        if ((this.c & Opcodes.ACC_DEPRECATED) != 0) {
            i2++;
        }
        if (this.g != null) {
            i2++;
        }
        if (this.l != null) {
            i2++;
        }
        if (this.m != null) {
            i2++;
        }
        if (this.n != null) {
            i2++;
        }
        if (this.o != null) {
            i2++;
        }
        if (this.p != null) {
            i2++;
        }
        if (this.q != null) {
            i2 += this.q.a();
        }
        byteVector.putShort(i2);
        if (this.r.b > 0) {
            int i3 = this.r.b + 12 + (this.A * 8);
            if (this.E != null) {
                i3 += this.E.b + 8;
            }
            if (this.G != null) {
                i3 += this.G.b + 8;
            }
            if (this.I != null) {
                i3 += this.I.b + 8;
            }
            int i4 = this.v != null ? i3 + this.v.b + 8 : i3;
            byteVector.putShort(this.b.newUTF8("Code")).putInt(this.J != null ? this.J.a(this.b, this.r.a, this.r.b, this.s, this.t) + i4 : i4);
            byteVector.putShort(this.s).putShort(this.t);
            byteVector.putInt(this.r.b).putByteArray(this.r.a, 0, this.r.b);
            byteVector.putShort(this.A);
            if (this.A > 0) {
                for (Handler handler = this.B; handler != null; handler = handler.f) {
                    byteVector.putShort(handler.a.c).putShort(handler.b.c).putShort(handler.c.c).putShort(handler.e);
                }
            }
            int i5 = this.E != null ? 0 + 1 : 0;
            if (this.G != null) {
                i5++;
            }
            if (this.I != null) {
                i5++;
            }
            if (this.v != null) {
                i5++;
            }
            if (this.J != null) {
                i5 += this.J.a();
            }
            byteVector.putShort(i5);
            if (this.E != null) {
                byteVector.putShort(this.b.newUTF8("LocalVariableTable"));
                byteVector.putInt(this.E.b + 2).putShort(this.D);
                byteVector.putByteArray(this.E.a, 0, this.E.b);
            }
            if (this.G != null) {
                byteVector.putShort(this.b.newUTF8("LocalVariableTypeTable"));
                byteVector.putInt(this.G.b + 2).putShort(this.F);
                byteVector.putByteArray(this.G.a, 0, this.G.b);
            }
            if (this.I != null) {
                byteVector.putShort(this.b.newUTF8("LineNumberTable"));
                byteVector.putInt(this.I.b + 2).putShort(this.H);
                byteVector.putByteArray(this.I.a, 0, this.I.b);
            }
            if (this.v != null) {
                byteVector.putShort(this.b.newUTF8((this.b.b & 65535) >= 50 ? "StackMapTable" : "StackMap"));
                byteVector.putInt(this.v.b + 2).putShort(this.u);
                byteVector.putByteArray(this.v.a, 0, this.v.b);
            }
            if (this.J != null) {
                this.J.a(this.b, this.r.a, this.r.b, this.t, this.s, byteVector);
            }
        }
        if (this.j > 0) {
            byteVector.putShort(this.b.newUTF8("Exceptions")).putInt((this.j * 2) + 2);
            byteVector.putShort(this.j);
            for (int i6 = 0; i6 < this.j; i6++) {
                byteVector.putShort(this.k[i6]);
            }
        }
        if ((this.c & 4096) != 0 && ((this.b.b & 65535) < 49 || (this.c & 262144) != 0)) {
            byteVector.putShort(this.b.newUTF8("Synthetic")).putInt(0);
        }
        if ((this.c & Opcodes.ACC_DEPRECATED) != 0) {
            byteVector.putShort(this.b.newUTF8("Deprecated")).putInt(0);
        }
        if (this.g != null) {
            byteVector.putShort(this.b.newUTF8("Signature")).putInt(2).putShort(this.b.newUTF8(this.g));
        }
        if (this.l != null) {
            byteVector.putShort(this.b.newUTF8("AnnotationDefault"));
            byteVector.putInt(this.l.b);
            byteVector.putByteArray(this.l.a, 0, this.l.b);
        }
        if (this.m != null) {
            byteVector.putShort(this.b.newUTF8("RuntimeVisibleAnnotations"));
            this.m.a(byteVector);
        }
        if (this.n != null) {
            byteVector.putShort(this.b.newUTF8("RuntimeInvisibleAnnotations"));
            this.n.a(byteVector);
        }
        if (this.o != null) {
            byteVector.putShort(this.b.newUTF8("RuntimeVisibleParameterAnnotations"));
            AnnotationWriter.a(this.o, this.S, byteVector);
        }
        if (this.p != null) {
            byteVector.putShort(this.b.newUTF8("RuntimeInvisibleParameterAnnotations"));
            AnnotationWriter.a(this.p, this.S, byteVector);
        }
        if (this.q != null) {
            this.q.a(this.b, null, 0, -1, -1, byteVector);
        }
    }

    public AnnotationVisitor visitAnnotation(String str, boolean z2) {
        ByteVector byteVector = new ByteVector();
        byteVector.putShort(this.b.newUTF8(str)).putShort(0);
        AnnotationWriter annotationWriter = new AnnotationWriter(this.b, true, byteVector, byteVector, 2);
        if (z2) {
            annotationWriter.g = this.m;
            this.m = annotationWriter;
        } else {
            annotationWriter.g = this.n;
            this.n = annotationWriter;
        }
        return annotationWriter;
    }

    public AnnotationVisitor visitAnnotationDefault() {
        this.l = new ByteVector();
        return new AnnotationWriter(this.b, false, this.l, null, 0);
    }

    public void visitAttribute(Attribute attribute) {
        if (attribute.isCodeAttribute()) {
            attribute.a = this.J;
            this.J = attribute;
            return;
        }
        attribute.a = this.q;
        this.q = attribute;
    }

    public void visitCode() {
    }

    public void visitEnd() {
    }

    public void visitFieldInsn(int i2, String str, String str2, String str3) {
        int i3;
        Item a2 = this.b.a(str, str2, str3);
        if (this.P != null) {
            if (this.M == 0) {
                this.P.h.a(i2, 0, this.b, a2);
            } else {
                char charAt = str3.charAt(0);
                switch (i2) {
                    case Opcodes.GETSTATIC:
                        i3 = ((charAt == 'D' || charAt == 'J') ? 2 : 1) + this.Q;
                        break;
                    case Opcodes.PUTSTATIC:
                        i3 = ((charAt == 'D' || charAt == 'J') ? -2 : -1) + this.Q;
                        break;
                    case Opcodes.GETFIELD:
                        i3 = ((charAt == 'D' || charAt == 'J') ? 1 : 0) + this.Q;
                        break;
                    default:
                        i3 = ((charAt == 'D' || charAt == 'J') ? -3 : -2) + this.Q;
                        break;
                }
                if (i3 > this.R) {
                    this.R = i3;
                }
                this.Q = i3;
            }
        }
        this.r.b(i2, a2.a);
    }

    public void visitFrame(int i2, int i3, Object[] objArr, int i4, Object[] objArr2) {
        int i5;
        if (this.M != 0) {
            if (i2 == -1) {
                a(this.r.b, i3, i4);
                for (int i6 = 0; i6 < i3; i6++) {
                    if (objArr[i6] instanceof String) {
                        int[] iArr = this.z;
                        int i7 = this.y;
                        this.y = i7 + 1;
                        iArr[i7] = this.b.c((String) objArr[i6]) | 24117248;
                    } else if (objArr[i6] instanceof Integer) {
                        int[] iArr2 = this.z;
                        int i8 = this.y;
                        this.y = i8 + 1;
                        iArr2[i8] = ((Integer) objArr[i6]).intValue();
                    } else {
                        int[] iArr3 = this.z;
                        int i9 = this.y;
                        this.y = i9 + 1;
                        iArr3[i9] = this.b.a("", ((Label) objArr[i6]).c) | 25165824;
                    }
                }
                for (int i10 = 0; i10 < i4; i10++) {
                    if (objArr2[i10] instanceof String) {
                        int[] iArr4 = this.z;
                        int i11 = this.y;
                        this.y = i11 + 1;
                        iArr4[i11] = this.b.c((String) objArr2[i10]) | 24117248;
                    } else if (objArr2[i10] instanceof Integer) {
                        int[] iArr5 = this.z;
                        int i12 = this.y;
                        this.y = i12 + 1;
                        iArr5[i12] = ((Integer) objArr2[i10]).intValue();
                    } else {
                        int[] iArr6 = this.z;
                        int i13 = this.y;
                        this.y = i13 + 1;
                        iArr6[i13] = this.b.a("", ((Label) objArr2[i10]).c) | 25165824;
                    }
                }
                b();
                return;
            }
            if (this.v == null) {
                this.v = new ByteVector();
                i5 = this.r.b;
            } else {
                i5 = (this.r.b - this.w) - 1;
                if (i5 < 0) {
                    if (i2 != 3) {
                        throw new IllegalStateException();
                    }
                    return;
                }
            }
            switch (i2) {
                case 0:
                    this.v.putByte(255).putShort(i5).putShort(i3);
                    for (int i14 = 0; i14 < i3; i14++) {
                        a(objArr[i14]);
                    }
                    this.v.putShort(i4);
                    for (int i15 = 0; i15 < i4; i15++) {
                        a(objArr2[i15]);
                    }
                    break;
                case 1:
                    this.v.putByte(i3 + 251).putShort(i5);
                    for (int i16 = 0; i16 < i3; i16++) {
                        a(objArr[i16]);
                    }
                    break;
                case 2:
                    this.v.putByte(251 - i3).putShort(i5);
                    break;
                case 3:
                    if (i5 >= 64) {
                        this.v.putByte(251).putShort(i5);
                        break;
                    } else {
                        this.v.putByte(i5);
                        break;
                    }
                case 4:
                    if (i5 < 64) {
                        this.v.putByte(i5 + 64);
                    } else {
                        this.v.putByte(247).putShort(i5);
                    }
                    a(objArr2[0]);
                    break;
            }
            this.w = this.r.b;
            this.u++;
        }
    }

    public void visitIincInsn(int i2, int i3) {
        int i4;
        if (this.P != null && this.M == 0) {
            this.P.h.a((int) Opcodes.IINC, i2, (ClassWriter) null, (Item) null);
        }
        if (this.M != 2 && (i4 = i2 + 1) > this.t) {
            this.t = i4;
        }
        if (i2 > 255 || i3 > 127 || i3 < -128) {
            this.r.putByte(SmileConstants.MIN_BUFFER_FOR_POSSIBLE_SHORT_STRING).b(Opcodes.IINC, i2).putShort(i3);
        } else {
            this.r.putByte(Opcodes.IINC).a(i2, i3);
        }
    }

    public void visitInsn(int i2) {
        this.r.putByte(i2);
        if (this.P != null) {
            if (this.M == 0) {
                this.P.h.a(i2, 0, (ClassWriter) null, (Item) null);
            } else {
                int i3 = this.Q + Frame.a[i2];
                if (i3 > this.R) {
                    this.R = i3;
                }
                this.Q = i3;
            }
            if ((i2 >= 172 && i2 <= 177) || i2 == 191) {
                e();
            }
        }
    }

    public void visitIntInsn(int i2, int i3) {
        if (this.P != null) {
            if (this.M == 0) {
                this.P.h.a(i2, i3, (ClassWriter) null, (Item) null);
            } else if (i2 != 188) {
                int i4 = this.Q + 1;
                if (i4 > this.R) {
                    this.R = i4;
                }
                this.Q = i4;
            }
        }
        if (i2 == 17) {
            this.r.b(i2, i3);
        } else {
            this.r.a(i2, i3);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:38:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0030  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void visitJumpInsn(int r8, org.codehaus.jackson.org.objectweb.asm.Label r9) {
        /*
            r7 = this;
            r6 = 168(0xa8, float:2.35E-43)
            r2 = 0
            r5 = 167(0xa7, float:2.34E-43)
            r4 = 0
            r3 = 1
            org.codehaus.jackson.org.objectweb.asm.Label r0 = r7.P
            if (r0 == 0) goto L_0x0094
            int r0 = r7.M
            if (r0 != 0) goto L_0x005d
            org.codehaus.jackson.org.objectweb.asm.Label r0 = r7.P
            org.codehaus.jackson.org.objectweb.asm.Frame r0 = r0.h
            r0.a(r8, r4, r2, r2)
            org.codehaus.jackson.org.objectweb.asm.Label r0 = r9.a()
            int r1 = r0.a
            r1 = r1 | 16
            r0.a = r1
            r7.a(r4, r9)
            if (r8 == r5) goto L_0x0094
            org.codehaus.jackson.org.objectweb.asm.Label r0 = new org.codehaus.jackson.org.objectweb.asm.Label
            r0.<init>()
        L_0x002a:
            int r1 = r9.a
            r1 = r1 & 2
            if (r1 == 0) goto L_0x00c8
            int r1 = r9.c
            org.codehaus.jackson.org.objectweb.asm.ByteVector r2 = r7.r
            int r2 = r2.b
            int r1 = r1 - r2
            r2 = -32768(0xffffffffffff8000, float:NaN)
            if (r1 >= r2) goto L_0x00c8
            if (r8 != r5) goto L_0x0096
            org.codehaus.jackson.org.objectweb.asm.ByteVector r1 = r7.r
            r2 = 200(0xc8, float:2.8E-43)
            r1.putByte(r2)
        L_0x0044:
            org.codehaus.jackson.org.objectweb.asm.ByteVector r1 = r7.r
            org.codehaus.jackson.org.objectweb.asm.ByteVector r2 = r7.r
            int r2 = r2.b
            int r2 = r2 - r3
            r9.a(r7, r1, r2, r3)
        L_0x004e:
            org.codehaus.jackson.org.objectweb.asm.Label r1 = r7.P
            if (r1 == 0) goto L_0x005c
            if (r0 == 0) goto L_0x0057
            r7.visitLabel(r0)
        L_0x0057:
            if (r8 != r5) goto L_0x005c
            r7.e()
        L_0x005c:
            return
        L_0x005d:
            if (r8 != r6) goto L_0x0086
            int r0 = r9.a
            r0 = r0 & 512(0x200, float:7.175E-43)
            if (r0 != 0) goto L_0x0071
            int r0 = r9.a
            r0 = r0 | 512(0x200, float:7.175E-43)
            r9.a = r0
            int r0 = r7.L
            int r0 = r0 + 1
            r7.L = r0
        L_0x0071:
            org.codehaus.jackson.org.objectweb.asm.Label r0 = r7.P
            int r1 = r0.a
            r1 = r1 | 128(0x80, float:1.794E-43)
            r0.a = r1
            int r0 = r7.Q
            int r0 = r0 + 1
            r7.a(r0, r9)
            org.codehaus.jackson.org.objectweb.asm.Label r0 = new org.codehaus.jackson.org.objectweb.asm.Label
            r0.<init>()
            goto L_0x002a
        L_0x0086:
            int r0 = r7.Q
            int[] r1 = org.codehaus.jackson.org.objectweb.asm.Frame.a
            r1 = r1[r8]
            int r0 = r0 + r1
            r7.Q = r0
            int r0 = r7.Q
            r7.a(r0, r9)
        L_0x0094:
            r0 = r2
            goto L_0x002a
        L_0x0096:
            if (r8 != r6) goto L_0x00a0
            org.codehaus.jackson.org.objectweb.asm.ByteVector r1 = r7.r
            r2 = 201(0xc9, float:2.82E-43)
            r1.putByte(r2)
            goto L_0x0044
        L_0x00a0:
            if (r0 == 0) goto L_0x00a8
            int r1 = r0.a
            r1 = r1 | 16
            r0.a = r1
        L_0x00a8:
            org.codehaus.jackson.org.objectweb.asm.ByteVector r1 = r7.r
            r2 = 166(0xa6, float:2.33E-43)
            if (r8 > r2) goto L_0x00c5
            int r2 = r8 + 1
            r2 = r2 ^ 1
            int r2 = r2 - r3
        L_0x00b3:
            r1.putByte(r2)
            org.codehaus.jackson.org.objectweb.asm.ByteVector r1 = r7.r
            r2 = 8
            r1.putShort(r2)
            org.codehaus.jackson.org.objectweb.asm.ByteVector r1 = r7.r
            r2 = 200(0xc8, float:2.8E-43)
            r1.putByte(r2)
            goto L_0x0044
        L_0x00c5:
            r2 = r8 ^ 1
            goto L_0x00b3
        L_0x00c8:
            org.codehaus.jackson.org.objectweb.asm.ByteVector r1 = r7.r
            r1.putByte(r8)
            org.codehaus.jackson.org.objectweb.asm.ByteVector r1 = r7.r
            org.codehaus.jackson.org.objectweb.asm.ByteVector r2 = r7.r
            int r2 = r2.b
            int r2 = r2 - r3
            r9.a(r7, r1, r2, r4)
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.org.objectweb.asm.MethodWriter.visitJumpInsn(int, org.codehaus.jackson.org.objectweb.asm.Label):void");
    }

    public void visitLabel(Label label) {
        this.K |= label.a(this, this.r.b, this.r.a);
        if ((label.a & 1) == 0) {
            if (this.M == 0) {
                if (this.P != null) {
                    if (label.c == this.P.c) {
                        this.P.a |= label.a & 16;
                        label.h = this.P.h;
                        return;
                    }
                    a(0, label);
                }
                this.P = label;
                if (label.h == null) {
                    label.h = new Frame();
                    label.h.b = label;
                }
                if (this.O != null) {
                    if (label.c == this.O.c) {
                        this.O.a |= label.a & 16;
                        label.h = this.O.h;
                        this.P = this.O;
                        return;
                    }
                    this.O.i = label;
                }
                this.O = label;
            } else if (this.M == 1) {
                if (this.P != null) {
                    this.P.g = this.R;
                    a(this.Q, label);
                }
                this.P = label;
                this.Q = 0;
                this.R = 0;
                if (this.O != null) {
                    this.O.i = label;
                }
                this.O = label;
            }
        }
    }

    public void visitLdcInsn(Object obj) {
        Item a2 = this.b.a(obj);
        if (this.P != null) {
            if (this.M == 0) {
                this.P.h.a(18, 0, this.b, a2);
            } else {
                int i2 = (a2.b == 5 || a2.b == 6) ? this.Q + 2 : this.Q + 1;
                if (i2 > this.R) {
                    this.R = i2;
                }
                this.Q = i2;
            }
        }
        int i3 = a2.a;
        if (a2.b == 5 || a2.b == 6) {
            this.r.b(20, i3);
        } else if (i3 >= 256) {
            this.r.b(19, i3);
        } else {
            this.r.a(18, i3);
        }
    }

    public void visitLineNumber(int i2, Label label) {
        if (this.I == null) {
            this.I = new ByteVector();
        }
        this.H++;
        this.I.putShort(label.c);
        this.I.putShort(i2);
    }

    public void visitLocalVariable(String str, String str2, String str3, Label label, Label label2, int i2) {
        if (str3 != null) {
            if (this.G == null) {
                this.G = new ByteVector();
            }
            this.F++;
            this.G.putShort(label.c).putShort(label2.c - label.c).putShort(this.b.newUTF8(str)).putShort(this.b.newUTF8(str3)).putShort(i2);
        }
        if (this.E == null) {
            this.E = new ByteVector();
        }
        this.D++;
        this.E.putShort(label.c).putShort(label2.c - label.c).putShort(this.b.newUTF8(str)).putShort(this.b.newUTF8(str2)).putShort(i2);
        if (this.M != 2) {
            char charAt = str2.charAt(0);
            int i3 = ((charAt == 'J' || charAt == 'D') ? 2 : 1) + i2;
            if (i3 > this.t) {
                this.t = i3;
            }
        }
    }

    public void visitLookupSwitchInsn(Label label, int[] iArr, Label[] labelArr) {
        int i2 = this.r.b;
        this.r.putByte(Opcodes.LOOKUPSWITCH);
        this.r.putByteArray(null, 0, (4 - (this.r.b % 4)) % 4);
        label.a(this, this.r, i2, true);
        this.r.putInt(labelArr.length);
        for (int i3 = 0; i3 < labelArr.length; i3++) {
            this.r.putInt(iArr[i3]);
            labelArr[i3].a(this, this.r, i2, true);
        }
        a(label, labelArr);
    }

    /*  JADX ERROR: JadxOverflowException in pass: LoopRegionVisitor
        jadx.core.utils.exceptions.JadxOverflowException: LoopRegionVisitor.assignOnlyInLoop endless recursion
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    public void visitMaxs(int r14, int r15) {
        /*
            r13 = this;
            r12 = 2147483647(0x7fffffff, float:NaN)
            r11 = 24117248(0x1700000, float:4.4081038E-38)
            r10 = 0
            r9 = 1
            r8 = 0
            int r0 = r13.M
            if (r0 != 0) goto L_0x0109
            org.codehaus.jackson.org.objectweb.asm.Handler r0 = r13.B
        L_0x000e:
            if (r0 == 0) goto L_0x004f
            org.codehaus.jackson.org.objectweb.asm.Label r1 = r0.a
            org.codehaus.jackson.org.objectweb.asm.Label r1 = r1.a()
            org.codehaus.jackson.org.objectweb.asm.Label r2 = r0.c
            org.codehaus.jackson.org.objectweb.asm.Label r2 = r2.a()
            org.codehaus.jackson.org.objectweb.asm.Label r3 = r0.b
            org.codehaus.jackson.org.objectweb.asm.Label r3 = r3.a()
            java.lang.String r4 = r0.d
            if (r4 != 0) goto L_0x0049
            java.lang.String r4 = "java/lang/Throwable"
        L_0x0028:
            org.codehaus.jackson.org.objectweb.asm.ClassWriter r5 = r13.b
            int r4 = r5.c(r4)
            r4 = r4 | r11
            int r5 = r2.a
            r5 = r5 | 16
            r2.a = r5
        L_0x0035:
            if (r1 == r3) goto L_0x004c
            org.codehaus.jackson.org.objectweb.asm.Edge r5 = new org.codehaus.jackson.org.objectweb.asm.Edge
            r5.<init>()
            r5.a = r4
            r5.b = r2
            org.codehaus.jackson.org.objectweb.asm.Edge r6 = r1.j
            r5.c = r6
            r1.j = r5
            org.codehaus.jackson.org.objectweb.asm.Label r1 = r1.i
            goto L_0x0035
        L_0x0049:
            java.lang.String r4 = r0.d
            goto L_0x0028
        L_0x004c:
            org.codehaus.jackson.org.objectweb.asm.Handler r0 = r0.f
            goto L_0x000e
        L_0x004f:
            org.codehaus.jackson.org.objectweb.asm.Label r0 = r13.N
            org.codehaus.jackson.org.objectweb.asm.Frame r0 = r0.h
            java.lang.String r1 = r13.f
            org.codehaus.jackson.org.objectweb.asm.Type[] r1 = org.codehaus.jackson.org.objectweb.asm.Type.getArgumentTypes(r1)
            org.codehaus.jackson.org.objectweb.asm.ClassWriter r2 = r13.b
            int r3 = r13.c
            int r4 = r13.t
            r0.a(r2, r3, r1, r4)
            r13.b(r0)
            org.codehaus.jackson.org.objectweb.asm.Label r0 = r13.N
            r1 = r8
        L_0x0068:
            if (r0 == 0) goto L_0x00ad
            org.codehaus.jackson.org.objectweb.asm.Label r2 = r0.k
            r0.k = r10
            org.codehaus.jackson.org.objectweb.asm.Frame r3 = r0.h
            int r4 = r0.a
            r4 = r4 & 16
            if (r4 == 0) goto L_0x007c
            int r4 = r0.a
            r4 = r4 | 32
            r0.a = r4
        L_0x007c:
            int r4 = r0.a
            r4 = r4 | 64
            r0.a = r4
            int[] r4 = r3.d
            int r4 = r4.length
            int r5 = r0.g
            int r4 = r4 + r5
            if (r4 <= r1) goto L_0x008b
            r1 = r4
        L_0x008b:
            org.codehaus.jackson.org.objectweb.asm.Edge r0 = r0.j
        L_0x008d:
            if (r0 == 0) goto L_0x00ab
            org.codehaus.jackson.org.objectweb.asm.Label r4 = r0.b
            org.codehaus.jackson.org.objectweb.asm.Label r4 = r4.a()
            org.codehaus.jackson.org.objectweb.asm.ClassWriter r5 = r13.b
            org.codehaus.jackson.org.objectweb.asm.Frame r6 = r4.h
            int r7 = r0.a
            boolean r5 = r3.a(r5, r6, r7)
            if (r5 == 0) goto L_0x00a8
            org.codehaus.jackson.org.objectweb.asm.Label r5 = r4.k
            if (r5 != 0) goto L_0x00a8
            r4.k = r2
            r2 = r4
        L_0x00a8:
            org.codehaus.jackson.org.objectweb.asm.Edge r0 = r0.c
            goto L_0x008d
        L_0x00ab:
            r0 = r2
            goto L_0x0068
        L_0x00ad:
            org.codehaus.jackson.org.objectweb.asm.Label r0 = r13.N
        L_0x00af:
            if (r0 == 0) goto L_0x0106
            org.codehaus.jackson.org.objectweb.asm.Frame r2 = r0.h
            int r3 = r0.a
            r3 = r3 & 32
            if (r3 == 0) goto L_0x00bc
            r13.b(r2)
        L_0x00bc:
            int r2 = r0.a
            r2 = r2 & 64
            if (r2 != 0) goto L_0x0103
            org.codehaus.jackson.org.objectweb.asm.Label r2 = r0.i
            int r3 = r0.c
            if (r2 != 0) goto L_0x00df
            org.codehaus.jackson.org.objectweb.asm.ByteVector r2 = r13.r
            int r2 = r2.b
        L_0x00cc:
            int r2 = r2 - r9
            if (r2 < r3) goto L_0x0103
            int r1 = java.lang.Math.max(r1, r9)
            r4 = r3
        L_0x00d4:
            if (r4 >= r2) goto L_0x00e2
            org.codehaus.jackson.org.objectweb.asm.ByteVector r5 = r13.r
            byte[] r5 = r5.a
            r5[r4] = r8
            int r4 = r4 + 1
            goto L_0x00d4
        L_0x00df:
            int r2 = r2.c
            goto L_0x00cc
        L_0x00e2:
            org.codehaus.jackson.org.objectweb.asm.ByteVector r4 = r13.r
            byte[] r4 = r4.a
            r5 = -65
            r4[r2] = r5
            r13.a(r3, r8, r9)
            int[] r2 = r13.z
            int r3 = r13.y
            int r4 = r3 + 1
            r13.y = r4
            org.codehaus.jackson.org.objectweb.asm.ClassWriter r4 = r13.b
            java.lang.String r5 = "java/lang/Throwable"
            int r4 = r4.c(r5)
            r4 = r4 | r11
            r2[r3] = r4
            r13.b()
        L_0x0103:
            org.codehaus.jackson.org.objectweb.asm.Label r0 = r0.i
            goto L_0x00af
        L_0x0106:
            r13.s = r1
        L_0x0108:
            return
        L_0x0109:
            int r0 = r13.M
            if (r0 != r9) goto L_0x01e5
            org.codehaus.jackson.org.objectweb.asm.Handler r0 = r13.B
        L_0x010f:
            if (r0 == 0) goto L_0x0143
            org.codehaus.jackson.org.objectweb.asm.Label r1 = r0.a
            org.codehaus.jackson.org.objectweb.asm.Label r2 = r0.c
            org.codehaus.jackson.org.objectweb.asm.Label r3 = r0.b
        L_0x0117:
            if (r1 == r3) goto L_0x0140
            org.codehaus.jackson.org.objectweb.asm.Edge r4 = new org.codehaus.jackson.org.objectweb.asm.Edge
            r4.<init>()
            r4.a = r12
            r4.b = r2
            int r5 = r1.a
            r5 = r5 & 128(0x80, float:1.794E-43)
            if (r5 != 0) goto L_0x0131
            org.codehaus.jackson.org.objectweb.asm.Edge r5 = r1.j
            r4.c = r5
            r1.j = r4
        L_0x012e:
            org.codehaus.jackson.org.objectweb.asm.Label r1 = r1.i
            goto L_0x0117
        L_0x0131:
            org.codehaus.jackson.org.objectweb.asm.Edge r5 = r1.j
            org.codehaus.jackson.org.objectweb.asm.Edge r5 = r5.c
            org.codehaus.jackson.org.objectweb.asm.Edge r5 = r5.c
            r4.c = r5
            org.codehaus.jackson.org.objectweb.asm.Edge r5 = r1.j
            org.codehaus.jackson.org.objectweb.asm.Edge r5 = r5.c
            r5.c = r4
            goto L_0x012e
        L_0x0140:
            org.codehaus.jackson.org.objectweb.asm.Handler r0 = r0.f
            goto L_0x010f
        L_0x0143:
            int r0 = r13.L
            if (r0 <= 0) goto L_0x01a5
            org.codehaus.jackson.org.objectweb.asm.Label r0 = r13.N
            r1 = 1
            int r3 = r13.L
            r0.b(r10, r1, r3)
            org.codehaus.jackson.org.objectweb.asm.Label r0 = r13.N
            r1 = r8
        L_0x0153:
            if (r0 == 0) goto L_0x017e
            int r2 = r0.a
            r2 = r2 & 128(0x80, float:1.794E-43)
            if (r2 == 0) goto L_0x017b
            org.codehaus.jackson.org.objectweb.asm.Edge r2 = r0.j
            org.codehaus.jackson.org.objectweb.asm.Edge r2 = r2.c
            org.codehaus.jackson.org.objectweb.asm.Label r2 = r2.b
            int r3 = r2.a
            r3 = r3 & 1024(0x400, float:1.435E-42)
            if (r3 != 0) goto L_0x017b
            int r1 = r1 + 1
            long r3 = (long) r1
            r5 = 32
            long r3 = r3 / r5
            r5 = 32
            long r3 = r3 << r5
            r5 = 1
            int r7 = r1 % 32
            long r5 = r5 << r7
            long r3 = r3 | r5
            int r5 = r13.L
            r2.b(r10, r3, r5)
        L_0x017b:
            org.codehaus.jackson.org.objectweb.asm.Label r0 = r0.i
            goto L_0x0153
        L_0x017e:
            org.codehaus.jackson.org.objectweb.asm.Label r0 = r13.N
        L_0x0180:
            if (r0 == 0) goto L_0x01a5
            int r1 = r0.a
            r1 = r1 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L_0x01a2
            org.codehaus.jackson.org.objectweb.asm.Label r1 = r13.N
        L_0x018a:
            if (r1 == 0) goto L_0x0195
            int r2 = r1.a
            r2 = r2 & -2049(0xfffffffffffff7ff, float:NaN)
            r1.a = r2
            org.codehaus.jackson.org.objectweb.asm.Label r1 = r1.i
            goto L_0x018a
        L_0x0195:
            org.codehaus.jackson.org.objectweb.asm.Edge r1 = r0.j
            org.codehaus.jackson.org.objectweb.asm.Edge r1 = r1.c
            org.codehaus.jackson.org.objectweb.asm.Label r1 = r1.b
            r2 = 0
            int r4 = r13.L
            r1.b(r0, r2, r4)
        L_0x01a2:
            org.codehaus.jackson.org.objectweb.asm.Label r0 = r0.i
            goto L_0x0180
        L_0x01a5:
            org.codehaus.jackson.org.objectweb.asm.Label r0 = r13.N
            r1 = r8
        L_0x01a8:
            if (r0 == 0) goto L_0x01e1
            org.codehaus.jackson.org.objectweb.asm.Label r2 = r0.k
            int r3 = r0.f
            int r4 = r0.g
            int r4 = r4 + r3
            if (r4 <= r1) goto L_0x01b4
            r1 = r4
        L_0x01b4:
            org.codehaus.jackson.org.objectweb.asm.Edge r4 = r0.j
            int r0 = r0.a
            r0 = r0 & 128(0x80, float:1.794E-43)
            if (r0 == 0) goto L_0x01eb
            org.codehaus.jackson.org.objectweb.asm.Edge r0 = r4.c
        L_0x01be:
            if (r0 == 0) goto L_0x01df
            org.codehaus.jackson.org.objectweb.asm.Label r4 = r0.b
            int r5 = r4.a
            r5 = r5 & 8
            if (r5 != 0) goto L_0x01d8
            int r5 = r0.a
            if (r5 != r12) goto L_0x01db
            r5 = r9
        L_0x01cd:
            r4.f = r5
            int r5 = r4.a
            r5 = r5 | 8
            r4.a = r5
            r4.k = r2
            r2 = r4
        L_0x01d8:
            org.codehaus.jackson.org.objectweb.asm.Edge r0 = r0.c
            goto L_0x01be
        L_0x01db:
            int r5 = r0.a
            int r5 = r5 + r3
            goto L_0x01cd
        L_0x01df:
            r0 = r2
            goto L_0x01a8
        L_0x01e1:
            r13.s = r1
            goto L_0x0108
        L_0x01e5:
            r13.s = r14
            r13.t = r15
            goto L_0x0108
        L_0x01eb:
            r0 = r4
            goto L_0x01be
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.org.objectweb.asm.MethodWriter.visitMaxs(int, int):void");
    }

    public void visitMethodInsn(int i2, String str, String str2, String str3) {
        int i3;
        boolean z2 = i2 == 185;
        Item a2 = i2 == 186 ? this.b.a(str2, str3) : this.b.a(str, str2, str3, z2);
        int i4 = a2.c;
        if (this.P != null) {
            if (this.M == 0) {
                this.P.h.a(i2, 0, this.b, a2);
            } else {
                if (i4 == 0) {
                    i4 = Type.getArgumentsAndReturnSizes(str3);
                    a2.c = i4;
                }
                int i5 = (i2 == 184 || i2 == 186) ? (this.Q - (i4 >> 2)) + (i4 & 3) + 1 : (this.Q - (i4 >> 2)) + (i4 & 3);
                if (i5 > this.R) {
                    this.R = i5;
                }
                this.Q = i5;
            }
        }
        if (z2) {
            if (i4 == 0) {
                i3 = Type.getArgumentsAndReturnSizes(str3);
                a2.c = i3;
            } else {
                i3 = i4;
            }
            this.r.b(Opcodes.INVOKEINTERFACE, a2.a).a(i3 >> 2, 0);
            return;
        }
        this.r.b(i2, a2.a);
        if (i2 == 186) {
            this.r.putShort(0);
        }
    }

    public void visitMultiANewArrayInsn(String str, int i2) {
        Item a2 = this.b.a(str);
        if (this.P != null) {
            if (this.M == 0) {
                this.P.h.a((int) Opcodes.MULTIANEWARRAY, i2, this.b, a2);
            } else {
                this.Q += 1 - i2;
            }
        }
        this.r.b(Opcodes.MULTIANEWARRAY, a2.a).putByte(i2);
    }

    public AnnotationVisitor visitParameterAnnotation(int i2, String str, boolean z2) {
        ByteVector byteVector = new ByteVector();
        if ("Ljava/lang/Synthetic;".equals(str)) {
            this.S = Math.max(this.S, i2 + 1);
            return new AnnotationWriter(this.b, false, byteVector, null, 0);
        }
        byteVector.putShort(this.b.newUTF8(str)).putShort(0);
        AnnotationWriter annotationWriter = new AnnotationWriter(this.b, true, byteVector, byteVector, 2);
        if (z2) {
            if (this.o == null) {
                this.o = new AnnotationWriter[Type.getArgumentTypes(this.f).length];
            }
            annotationWriter.g = this.o[i2];
            this.o[i2] = annotationWriter;
            return annotationWriter;
        }
        if (this.p == null) {
            this.p = new AnnotationWriter[Type.getArgumentTypes(this.f).length];
        }
        annotationWriter.g = this.p[i2];
        this.p[i2] = annotationWriter;
        return annotationWriter;
    }

    public void visitTableSwitchInsn(int i2, int i3, Label label, Label[] labelArr) {
        int i4 = this.r.b;
        this.r.putByte(Opcodes.TABLESWITCH);
        this.r.putByteArray(null, 0, (4 - (this.r.b % 4)) % 4);
        label.a(this, this.r, i4, true);
        this.r.putInt(i2).putInt(i3);
        for (Label a2 : labelArr) {
            a2.a(this, this.r, i4, true);
        }
        a(label, labelArr);
    }

    public void visitTryCatchBlock(Label label, Label label2, Label label3, String str) {
        this.A++;
        Handler handler = new Handler();
        handler.a = label;
        handler.b = label2;
        handler.c = label3;
        handler.d = str;
        handler.e = str != null ? this.b.newClass(str) : 0;
        if (this.C == null) {
            this.B = handler;
        } else {
            this.C.f = handler;
        }
        this.C = handler;
    }

    public void visitTypeInsn(int i2, String str) {
        Item a2 = this.b.a(str);
        if (this.P != null) {
            if (this.M == 0) {
                this.P.h.a(i2, this.r.b, this.b, a2);
            } else if (i2 == 187) {
                int i3 = this.Q + 1;
                if (i3 > this.R) {
                    this.R = i3;
                }
                this.Q = i3;
            }
        }
        this.r.b(i2, a2.a);
    }

    public void visitVarInsn(int i2, int i3) {
        if (this.P != null) {
            if (this.M == 0) {
                this.P.h.a(i2, i3, (ClassWriter) null, (Item) null);
            } else if (i2 == 169) {
                this.P.a |= Opcodes.ACC_NATIVE;
                this.P.f = this.Q;
                e();
            } else {
                int i4 = this.Q + Frame.a[i2];
                if (i4 > this.R) {
                    this.R = i4;
                }
                this.Q = i4;
            }
        }
        if (this.M != 2) {
            int i5 = (i2 == 22 || i2 == 24 || i2 == 55 || i2 == 57) ? i3 + 2 : i3 + 1;
            if (i5 > this.t) {
                this.t = i5;
            }
        }
        if (i3 < 4 && i2 != 169) {
            this.r.putByte(i2 < 54 ? ((i2 - 21) << 2) + 26 + i3 : ((i2 - 54) << 2) + 59 + i3);
        } else if (i3 >= 256) {
            this.r.putByte(SmileConstants.MIN_BUFFER_FOR_POSSIBLE_SHORT_STRING).b(i2, i3);
        } else {
            this.r.a(i2, i3);
        }
        if (i2 >= 54 && this.M == 0 && this.A > 0) {
            visitLabel(new Label());
        }
    }
}
