package org.codehaus.jackson.org.objectweb.asm;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class Type {
    public static final int ARRAY = 9;
    public static final int BOOLEAN = 1;
    public static final Type BOOLEAN_TYPE = new Type(1, null, 1509950721, 1);
    public static final int BYTE = 3;
    public static final Type BYTE_TYPE = new Type(3, null, 1107297537, 1);
    public static final int CHAR = 2;
    public static final Type CHAR_TYPE = new Type(2, null, 1124075009, 1);
    public static final int DOUBLE = 8;
    public static final Type DOUBLE_TYPE = new Type(8, null, 1141048066, 1);
    public static final int FLOAT = 6;
    public static final Type FLOAT_TYPE = new Type(6, null, 1174536705, 1);
    public static final int INT = 5;
    public static final Type INT_TYPE = new Type(5, null, 1224736769, 1);
    public static final int LONG = 7;
    public static final Type LONG_TYPE = new Type(7, null, 1241579778, 1);
    public static final int OBJECT = 10;
    public static final int SHORT = 4;
    public static final Type SHORT_TYPE = new Type(4, null, 1392510721, 1);
    public static final int VOID = 0;
    public static final Type VOID_TYPE = new Type(0, null, 1443168256, 1);
    private final int a;
    private final char[] b;
    private final int c;
    private final int d;

    private Type(int i, char[] cArr, int i2, int i3) {
        this.a = i;
        this.b = cArr;
        this.c = i2;
        this.d = i3;
    }

    private static Type a(char[] cArr, int i) {
        switch (cArr[i]) {
            case 'B':
                return BYTE_TYPE;
            case 'C':
                return CHAR_TYPE;
            case 'D':
                return DOUBLE_TYPE;
            case 'F':
                return FLOAT_TYPE;
            case 'I':
                return INT_TYPE;
            case 'J':
                return LONG_TYPE;
            case Opcodes.AASTORE:
                return SHORT_TYPE;
            case Opcodes.SASTORE:
                return VOID_TYPE;
            case Opcodes.DUP_X1:
                return BOOLEAN_TYPE;
            case Opcodes.DUP_X2:
                int i2 = 1;
                while (cArr[i + i2] == '[') {
                    i2++;
                }
                if (cArr[i + i2] == 'L') {
                    while (true) {
                        i2++;
                        if (cArr[i + i2] != ';') {
                        }
                    }
                }
                return new Type(9, cArr, i, i2 + 1);
            default:
                int i3 = 1;
                while (cArr[i + i3] != ';') {
                    i3++;
                }
                return new Type(10, cArr, i + 1, i3 - 1);
        }
    }

    private void a(StringBuffer stringBuffer) {
        if (this.b == null) {
            stringBuffer.append((char) ((this.c & -16777216) >>> 24));
        } else if (this.a == 9) {
            stringBuffer.append(this.b, this.c, this.d);
        } else {
            stringBuffer.append('L');
            stringBuffer.append(this.b, this.c, this.d);
            stringBuffer.append(';');
        }
    }

    private static void a(StringBuffer stringBuffer, Class cls) {
        Class cls2 = cls;
        while (!cls2.isPrimitive()) {
            if (cls2.isArray()) {
                stringBuffer.append('[');
                cls2 = cls2.getComponentType();
            } else {
                stringBuffer.append('L');
                String name = cls2.getName();
                int length = name.length();
                for (int i = 0; i < length; i++) {
                    char charAt = name.charAt(i);
                    if (charAt == '.') {
                        charAt = '/';
                    }
                    stringBuffer.append(charAt);
                }
                stringBuffer.append(';');
                return;
            }
        }
        stringBuffer.append(cls2 == Integer.TYPE ? 'I' : cls2 == Void.TYPE ? 'V' : cls2 == Boolean.TYPE ? 'Z' : cls2 == Byte.TYPE ? 'B' : cls2 == Character.TYPE ? 'C' : cls2 == Short.TYPE ? 'S' : cls2 == Double.TYPE ? 'D' : cls2 == Float.TYPE ? 'F' : 'J');
    }

    public static Type[] getArgumentTypes(String str) {
        char[] charArray = str.toCharArray();
        int i = 0;
        int i2 = 1;
        while (true) {
            int i3 = i2 + 1;
            char c2 = charArray[i2];
            if (c2 == ')') {
                break;
            } else if (c2 == 'L') {
                while (true) {
                    int i4 = i3;
                    i3 = i4 + 1;
                    if (charArray[i4] == ';') {
                        break;
                    }
                }
                i++;
                i2 = i3;
            } else if (c2 != '[') {
                i++;
                i2 = i3;
            } else {
                i2 = i3;
            }
        }
        Type[] typeArr = new Type[i];
        int i5 = 0;
        int i6 = 1;
        while (charArray[i6] != ')') {
            typeArr[i5] = a(charArray, i6);
            i6 += typeArr[i5].d + (typeArr[i5].a == 10 ? 2 : 0);
            i5++;
        }
        return typeArr;
    }

    public static Type[] getArgumentTypes(Method method) {
        Class<?>[] parameterTypes = method.getParameterTypes();
        Type[] typeArr = new Type[parameterTypes.length];
        for (int length = parameterTypes.length - 1; length >= 0; length--) {
            typeArr[length] = getType(parameterTypes[length]);
        }
        return typeArr;
    }

    public static int getArgumentsAndReturnSizes(String str) {
        int i;
        char charAt;
        int i2 = 1;
        int i3 = 1;
        while (true) {
            i = i2 + 1;
            char charAt2 = str.charAt(i2);
            if (charAt2 == ')') {
                break;
            } else if (charAt2 == 'L') {
                while (true) {
                    int i4 = i;
                    i = i4 + 1;
                    if (str.charAt(i4) == ';') {
                        break;
                    }
                }
                i3++;
                i2 = i;
            } else if (charAt2 == '[') {
                i2 = i;
                while (true) {
                    charAt = str.charAt(i2);
                    if (charAt != '[') {
                        break;
                    }
                    i2++;
                }
                if (charAt == 'D' || charAt == 'J') {
                    i3--;
                }
            } else if (charAt2 == 'D' || charAt2 == 'J') {
                i3 += 2;
                i2 = i;
            } else {
                i3++;
                i2 = i;
            }
        }
        char charAt3 = str.charAt(i);
        return (charAt3 == 'V' ? 0 : (charAt3 == 'D' || charAt3 == 'J') ? 2 : 1) | (i3 << 2);
    }

    public static String getConstructorDescriptor(Constructor constructor) {
        Class<?>[] parameterTypes = constructor.getParameterTypes();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append('(');
        for (Class<?> a2 : parameterTypes) {
            a(stringBuffer, a2);
        }
        return stringBuffer.append(")V").toString();
    }

    public static String getDescriptor(Class cls) {
        StringBuffer stringBuffer = new StringBuffer();
        a(stringBuffer, cls);
        return stringBuffer.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static String getInternalName(Class cls) {
        return cls.getName().replace('.', '/');
    }

    public static String getMethodDescriptor(Method method) {
        Class<?>[] parameterTypes = method.getParameterTypes();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append('(');
        for (Class<?> a2 : parameterTypes) {
            a(stringBuffer, a2);
        }
        stringBuffer.append(')');
        a(stringBuffer, method.getReturnType());
        return stringBuffer.toString();
    }

    public static String getMethodDescriptor(Type type, Type[] typeArr) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append('(');
        for (Type a2 : typeArr) {
            a2.a(stringBuffer);
        }
        stringBuffer.append(')');
        type.a(stringBuffer);
        return stringBuffer.toString();
    }

    public static Type getObjectType(String str) {
        char[] charArray = str.toCharArray();
        return new Type(charArray[0] == '[' ? 9 : 10, charArray, 0, charArray.length);
    }

    public static Type getReturnType(String str) {
        return a(str.toCharArray(), str.indexOf(41) + 1);
    }

    public static Type getReturnType(Method method) {
        return getType(method.getReturnType());
    }

    public static Type getType(Class cls) {
        return cls.isPrimitive() ? cls == Integer.TYPE ? INT_TYPE : cls == Void.TYPE ? VOID_TYPE : cls == Boolean.TYPE ? BOOLEAN_TYPE : cls == Byte.TYPE ? BYTE_TYPE : cls == Character.TYPE ? CHAR_TYPE : cls == Short.TYPE ? SHORT_TYPE : cls == Double.TYPE ? DOUBLE_TYPE : cls == Float.TYPE ? FLOAT_TYPE : LONG_TYPE : getType(getDescriptor(cls));
    }

    public static Type getType(String str) {
        return a(str.toCharArray(), 0);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Type)) {
            return false;
        }
        Type type = (Type) obj;
        if (this.a != type.a) {
            return false;
        }
        if (this.a == 10 || this.a == 9) {
            if (this.d != type.d) {
                return false;
            }
            int i = this.c;
            int i2 = type.c;
            int i3 = this.d + i;
            int i4 = i2;
            int i5 = i;
            int i6 = i4;
            while (i5 < i3) {
                if (this.b[i5] != type.b[i6]) {
                    return false;
                }
                i5++;
                i6++;
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public String getClassName() {
        switch (this.a) {
            case 0:
                return "void";
            case 1:
                return "boolean";
            case 2:
                return "char";
            case 3:
                return "byte";
            case 4:
                return "short";
            case 5:
                return "int";
            case 6:
                return "float";
            case 7:
                return "long";
            case 8:
                return "double";
            case 9:
                StringBuffer stringBuffer = new StringBuffer(getElementType().getClassName());
                for (int dimensions = getDimensions(); dimensions > 0; dimensions--) {
                    stringBuffer.append("[]");
                }
                return stringBuffer.toString();
            default:
                return new String(this.b, this.c, this.d).replace('/', '.');
        }
    }

    public String getDescriptor() {
        StringBuffer stringBuffer = new StringBuffer();
        a(stringBuffer);
        return stringBuffer.toString();
    }

    public int getDimensions() {
        int i = 1;
        while (this.b[this.c + i] == '[') {
            i++;
        }
        return i;
    }

    public Type getElementType() {
        return a(this.b, this.c + getDimensions());
    }

    public String getInternalName() {
        return new String(this.b, this.c, this.d);
    }

    public int getOpcode(int i) {
        if (i == 46 || i == 79) {
            return (this.b == null ? (this.c & 65280) >> 8 : 4) + i;
        }
        return (this.b == null ? (this.c & 16711680) >> 16 : 4) + i;
    }

    public int getSize() {
        if (this.b == null) {
            return this.c & 255;
        }
        return 1;
    }

    public int getSort() {
        return this.a;
    }

    public int hashCode() {
        int i = this.a * 13;
        if (this.a != 10 && this.a != 9) {
            return i;
        }
        int i2 = this.c;
        int i3 = this.d + i2;
        int i4 = i2;
        int i5 = i;
        for (int i6 = i4; i6 < i3; i6++) {
            i5 = (i5 + this.b[i6]) * 17;
        }
        return i5;
    }

    public String toString() {
        return getDescriptor();
    }
}
