package twitter4j.api;

import twitter4j.PagableResponseList;
import twitter4j.TwitterException;
import twitter4j.User;
import twitter4j.UserList;

public interface ListSubscribersMethods {
    User checkUserListSubscription(String str, int i, long j) throws TwitterException;

    PagableResponseList<User> getUserListSubscribers(String str, int i, long j) throws TwitterException;

    UserList subscribeUserList(String str, int i) throws TwitterException;

    UserList unsubscribeUserList(String str, int i) throws TwitterException;
}
