package twitter4j.api;

import twitter4j.TwitterException;

public interface LegalResources {
    String getPrivacyPolicy() throws TwitterException;

    String getTermsOfService() throws TwitterException;
}
