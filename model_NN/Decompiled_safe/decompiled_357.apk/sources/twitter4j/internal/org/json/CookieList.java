package twitter4j.internal.org.json;

import java.util.Iterator;
import org.codehaus.jackson.org.objectweb.asm.signature.SignatureVisitor;

public class CookieList {
    public static JSONObject toJSONObject(String string) throws JSONException {
        JSONObject jo = new JSONObject();
        JSONTokener x = new JSONTokener(string);
        while (x.more()) {
            String name = Cookie.unescape(x.nextTo((char) SignatureVisitor.INSTANCEOF));
            x.next((char) SignatureVisitor.INSTANCEOF);
            jo.put(name, Cookie.unescape(x.nextTo(';')));
            x.next();
        }
        return jo;
    }

    public static String toString(JSONObject jo) throws JSONException {
        boolean b = false;
        Iterator keys = jo.keys();
        StringBuffer sb = new StringBuffer();
        while (keys.hasNext()) {
            String string = keys.next().toString();
            if (!jo.isNull(string)) {
                if (b) {
                    sb.append(';');
                }
                sb.append(Cookie.escape(string));
                sb.append("=");
                sb.append(Cookie.escape(jo.getString(string)));
                b = true;
            }
        }
        return sb.toString();
    }
}
