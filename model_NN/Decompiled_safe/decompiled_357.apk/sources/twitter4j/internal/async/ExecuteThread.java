package twitter4j.internal.async;

import twitter4j.internal.logging.Logger;

/* compiled from: DispatcherImpl */
class ExecuteThread extends Thread {
    static Class class$twitter4j$internal$async$ExecuteThread;
    private static Logger logger;
    private boolean alive = true;
    DispatcherImpl q;

    static {
        Class cls;
        if (class$twitter4j$internal$async$ExecuteThread == null) {
            cls = class$("twitter4j.internal.async.ExecuteThread");
            class$twitter4j$internal$async$ExecuteThread = cls;
        } else {
            cls = class$twitter4j$internal$async$ExecuteThread;
        }
        logger = Logger.getLogger(cls);
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError().initCause(x1);
        }
    }

    ExecuteThread(String name, DispatcherImpl q2, int index) {
        super(new StringBuffer().append(name).append("[").append(index).append("]").toString());
        this.q = q2;
    }

    public void shutdown() {
        this.alive = false;
    }

    public void run() {
        while (this.alive) {
            Runnable task = this.q.poll();
            if (task != null) {
                try {
                    task.run();
                } catch (Exception ex) {
                    logger.error("Got an exception while running a taks:", ex);
                }
            }
        }
    }
}
