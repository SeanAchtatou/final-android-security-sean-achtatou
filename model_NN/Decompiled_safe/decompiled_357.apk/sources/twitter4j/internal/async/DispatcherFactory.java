package twitter4j.internal.async;

import java.lang.reflect.InvocationTargetException;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationContext;

public final class DispatcherFactory {
    static Class class$twitter4j$conf$Configuration;
    private Configuration conf;
    private String dispatcherImpl;

    public DispatcherFactory(Configuration conf2) {
        this.dispatcherImpl = conf2.getDispatcherImpl();
        this.conf = conf2;
    }

    public DispatcherFactory() {
        this(ConfigurationContext.getInstance());
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public Dispatcher getInstance() {
        Class cls;
        try {
            Class<?> cls2 = Class.forName(this.dispatcherImpl);
            Class[] clsArr = new Class[1];
            if (class$twitter4j$conf$Configuration == null) {
                cls = class$("twitter4j.conf.Configuration");
                class$twitter4j$conf$Configuration = cls;
            } else {
                cls = class$twitter4j$conf$Configuration;
            }
            clsArr[0] = cls;
            return (Dispatcher) cls2.getConstructor(clsArr).newInstance(this.conf);
        } catch (InstantiationException e) {
            throw new AssertionError(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (ClassNotFoundException e3) {
            throw new AssertionError(e3);
        } catch (ClassCastException e4) {
            throw new AssertionError(e4);
        } catch (NoSuchMethodException e5) {
            throw new AssertionError(e5);
        } catch (InvocationTargetException e6) {
            throw new AssertionError(e6);
        }
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError().initCause(x1);
        }
    }
}
