package twitter4j;

import java.io.Serializable;

public interface AccountSettings extends TwitterResponse, Serializable {
    String getSleepEndTime();

    String getSleepStartTime();

    Location[] getTrendLocations();

    boolean isGeoEnabled();

    boolean isSleepTimeEnabled();
}
