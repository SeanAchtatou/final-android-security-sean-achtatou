package twitter4j;

import twitter4j.internal.org.json.JSONArray;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.internal.util.ParseUtil;

class UserMentionEntityJSONImpl implements UserMentionEntity {
    private static final long serialVersionUID = 6580431141350059702L;
    private int end = -1;
    private long id;
    private String name;
    private String screenName;
    private int start = -1;

    UserMentionEntityJSONImpl(JSONObject json) throws TwitterException {
        init(json);
    }

    private void init(JSONObject json) throws TwitterException {
        try {
            JSONArray indicesArray = json.getJSONArray("indices");
            this.start = indicesArray.getInt(0);
            this.end = indicesArray.getInt(1);
            if (!json.isNull("name")) {
                this.name = json.getString("name");
            }
            if (!json.isNull("screen_name")) {
                this.screenName = json.getString("screen_name");
            }
            this.id = ParseUtil.getLong("id", json);
        } catch (JSONException e) {
            throw new TwitterException(e);
        }
    }

    public String getName() {
        return this.name;
    }

    public String getScreenName() {
        return this.screenName;
    }

    public long getId() {
        return this.id;
    }

    public int getStart() {
        return this.start;
    }

    public int getEnd() {
        return this.end;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserMentionEntityJSONImpl that = (UserMentionEntityJSONImpl) o;
        if (this.end != that.end) {
            return false;
        }
        if (this.id != that.id) {
            return false;
        }
        if (this.start != that.start) {
            return false;
        }
        if (this.name == null ? that.name != null : !this.name.equals(that.name)) {
            return false;
        }
        return this.screenName == null ? that.screenName == null : this.screenName.equals(that.screenName);
    }

    public int hashCode() {
        int i;
        int i2;
        int i3 = ((this.start * 31) + this.end) * 31;
        if (this.name != null) {
            i = this.name.hashCode();
        } else {
            i = 0;
        }
        int i4 = (i3 + i) * 31;
        if (this.screenName != null) {
            i2 = this.screenName.hashCode();
        } else {
            i2 = 0;
        }
        return ((i4 + i2) * 31) + ((int) (this.id ^ (this.id >>> 32)));
    }

    public String toString() {
        return new StringBuffer().append("UserMentionEntityJSONImpl{start=").append(this.start).append(", end=").append(this.end).append(", name='").append(this.name).append('\'').append(", screenName='").append(this.screenName).append('\'').append(", id=").append(this.id).append('}').toString();
    }
}
