package twitter4j;

import java.io.Serializable;
import twitter4j.internal.org.json.JSONArray;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.internal.util.T4JInternalStringUtil;

public class GeoLocation implements Serializable {
    private static final long serialVersionUID = -4847567157651889935L;
    protected double latitude;
    protected double longitude;

    public GeoLocation(double latitude2, double longitude2) {
        this.latitude = latitude2;
        this.longitude = longitude2;
    }

    static GeoLocation getInstance(JSONObject json) throws TwitterException {
        try {
            if (json.isNull("geo")) {
                return null;
            }
            String coordinates = json.getJSONObject("geo").getString("coordinates");
            String[] point = T4JInternalStringUtil.split(coordinates.substring(1, coordinates.length() - 1), ",");
            return new GeoLocation(Double.parseDouble(point[0]), Double.parseDouble(point[1]));
        } catch (JSONException e) {
            throw new TwitterException(e);
        }
    }

    static GeoLocation[][] coordinatesAsGeoLocationArray(JSONArray coordinates) throws TwitterException {
        try {
            GeoLocation[][] boundingBox = new GeoLocation[coordinates.length()][];
            for (int i = 0; i < coordinates.length(); i++) {
                JSONArray array = coordinates.getJSONArray(i);
                boundingBox[i] = new GeoLocation[array.length()];
                for (int j = 0; j < array.length(); j++) {
                    JSONArray coordinate = array.getJSONArray(j);
                    boundingBox[i][j] = new GeoLocation(coordinate.getDouble(1), coordinate.getDouble(0));
                }
            }
            return boundingBox;
        } catch (JSONException e) {
            throw new TwitterException(e);
        }
    }

    public double getLatitude() {
        return this.latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GeoLocation)) {
            return false;
        }
        GeoLocation that = (GeoLocation) o;
        if (Double.compare(that.getLatitude(), this.latitude) != 0) {
            return false;
        }
        return Double.compare(that.getLongitude(), this.longitude) == 0;
    }

    public int hashCode() {
        long temp;
        long temp2;
        if (this.latitude != 0.0d) {
            temp = Double.doubleToLongBits(this.latitude);
        } else {
            temp = 0;
        }
        int result = (int) ((temp >>> 32) ^ temp);
        if (this.longitude != 0.0d) {
            temp2 = Double.doubleToLongBits(this.longitude);
        } else {
            temp2 = 0;
        }
        return (result * 31) + ((int) ((temp2 >>> 32) ^ temp2));
    }

    public String toString() {
        return new StringBuffer().append("GeoLocation{latitude=").append(this.latitude).append(", longitude=").append(this.longitude).append('}').toString();
    }
}
