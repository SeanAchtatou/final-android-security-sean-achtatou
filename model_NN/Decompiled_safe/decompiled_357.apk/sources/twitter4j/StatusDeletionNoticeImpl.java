package twitter4j;

import java.io.Serializable;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.internal.util.ParseUtil;

class StatusDeletionNoticeImpl implements StatusDeletionNotice, Serializable {
    private static final long serialVersionUID = 1723338404242596062L;
    private long statusId;
    private long userId;

    public int compareTo(Object x0) {
        return compareTo((StatusDeletionNotice) x0);
    }

    StatusDeletionNoticeImpl(JSONObject status) {
        this.statusId = ParseUtil.getLong("id", status);
        this.userId = ParseUtil.getLong("user_id", status);
    }

    public long getStatusId() {
        return this.statusId;
    }

    public long getUserId() {
        return this.userId;
    }

    public int compareTo(StatusDeletionNotice that) {
        long delta = this.statusId - that.getStatusId();
        if (delta < -2147483648L) {
            return Integer.MIN_VALUE;
        }
        if (delta > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        return (int) delta;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StatusDeletionNoticeImpl that = (StatusDeletionNoticeImpl) o;
        if (this.statusId != that.statusId) {
            return false;
        }
        return this.userId == that.userId;
    }

    public int hashCode() {
        return (((int) (this.statusId ^ (this.statusId >>> 32))) * 31) + ((int) (this.userId ^ (this.userId >>> 32)));
    }

    public String toString() {
        return new StringBuffer().append("StatusDeletionNoticeImpl{statusId=").append(this.statusId).append(", userId=").append(this.userId).append('}').toString();
    }
}
