package twitter4j.json;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import twitter4j.AccountTotals;
import twitter4j.Category;
import twitter4j.DirectMessage;
import twitter4j.IDs;
import twitter4j.Location;
import twitter4j.Place;
import twitter4j.RateLimitStatus;
import twitter4j.RelatedResults;
import twitter4j.Relationship;
import twitter4j.SavedSearch;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.Trend;
import twitter4j.Trends;
import twitter4j.Tweet;
import twitter4j.TwitterException;
import twitter4j.User;
import twitter4j.UserList;
import twitter4j.internal.org.json.JSONArray;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;

public final class DataObjectFactory {
    private static final Constructor<IDs> IDsConstructor;
    private static final Constructor<AccountTotals> accountTotalsConstructor;
    private static final Constructor<Category> categoryConstructor;
    static Class class$java$lang$String;
    static Class class$twitter4j$internal$org$json$JSONArray;
    static Class class$twitter4j$internal$org$json$JSONObject;
    private static final Constructor<DirectMessage> directMessageConstructor;
    private static final Constructor<Location> locationConstructor;
    private static final Constructor<Place> placeConstructor;
    private static final Constructor<RateLimitStatus> rateLimitStatusConstructor;
    private static final ThreadLocal<Map> rawJsonMap = new ThreadLocal<Map>() {
        /* access modifiers changed from: protected */
        public Object initialValue() {
            return m1initialValue();
        }

        /* access modifiers changed from: protected */
        /* renamed from: initialValue  reason: collision with other method in class */
        public Map m1initialValue() {
            return new HashMap();
        }
    };
    private static final Constructor<RelatedResults> relatedResultsConstructor;
    private static final Constructor<Relationship> relationshipConstructor;
    private static final Constructor<SavedSearch> savedSearchConstructor;
    private static final Constructor<Status> statusConstructor;
    private static final Constructor<StatusDeletionNotice> statusDeletionNoticeConstructor;
    private static final Constructor<Trend> trendConstructor;
    private static final Constructor<Trends> trendsConstructor;
    private static final Constructor<Tweet> tweetConstructor;
    private static final Constructor<User> userConstructor;
    private static final Constructor<UserList> userListConstructor;

    private DataObjectFactory() {
        throw new AssertionError("not intended to be instantiated.");
    }

    static {
        Class cls;
        Class cls2;
        Class cls3;
        Class cls4;
        Class cls5;
        Class cls6;
        Class cls7;
        Class cls8;
        Class cls9;
        Class cls10;
        Class cls11;
        Class cls12;
        Class cls13;
        Class cls14;
        Class cls15;
        Class cls16;
        Class cls17;
        try {
            Class<?> cls18 = Class.forName("twitter4j.StatusJSONImpl");
            Class[] clsArr = new Class[1];
            if (class$twitter4j$internal$org$json$JSONObject == null) {
                cls = class$("twitter4j.internal.org.json.JSONObject");
                class$twitter4j$internal$org$json$JSONObject = cls;
            } else {
                cls = class$twitter4j$internal$org$json$JSONObject;
            }
            clsArr[0] = cls;
            statusConstructor = cls18.getDeclaredConstructor(clsArr);
            statusConstructor.setAccessible(true);
            Class<?> cls19 = Class.forName("twitter4j.UserJSONImpl");
            Class[] clsArr2 = new Class[1];
            if (class$twitter4j$internal$org$json$JSONObject == null) {
                cls2 = class$("twitter4j.internal.org.json.JSONObject");
                class$twitter4j$internal$org$json$JSONObject = cls2;
            } else {
                cls2 = class$twitter4j$internal$org$json$JSONObject;
            }
            clsArr2[0] = cls2;
            userConstructor = cls19.getDeclaredConstructor(clsArr2);
            userConstructor.setAccessible(true);
            Class<?> cls20 = Class.forName("twitter4j.TweetJSONImpl");
            Class[] clsArr3 = new Class[1];
            if (class$twitter4j$internal$org$json$JSONObject == null) {
                cls3 = class$("twitter4j.internal.org.json.JSONObject");
                class$twitter4j$internal$org$json$JSONObject = cls3;
            } else {
                cls3 = class$twitter4j$internal$org$json$JSONObject;
            }
            clsArr3[0] = cls3;
            tweetConstructor = cls20.getDeclaredConstructor(clsArr3);
            tweetConstructor.setAccessible(true);
            Class<?> cls21 = Class.forName("twitter4j.RelationshipJSONImpl");
            Class[] clsArr4 = new Class[1];
            if (class$twitter4j$internal$org$json$JSONObject == null) {
                cls4 = class$("twitter4j.internal.org.json.JSONObject");
                class$twitter4j$internal$org$json$JSONObject = cls4;
            } else {
                cls4 = class$twitter4j$internal$org$json$JSONObject;
            }
            clsArr4[0] = cls4;
            relationshipConstructor = cls21.getDeclaredConstructor(clsArr4);
            relationshipConstructor.setAccessible(true);
            Class<?> cls22 = Class.forName("twitter4j.PlaceJSONImpl");
            Class[] clsArr5 = new Class[1];
            if (class$twitter4j$internal$org$json$JSONObject == null) {
                cls5 = class$("twitter4j.internal.org.json.JSONObject");
                class$twitter4j$internal$org$json$JSONObject = cls5;
            } else {
                cls5 = class$twitter4j$internal$org$json$JSONObject;
            }
            clsArr5[0] = cls5;
            placeConstructor = cls22.getDeclaredConstructor(clsArr5);
            placeConstructor.setAccessible(true);
            Class<?> cls23 = Class.forName("twitter4j.SavedSearchJSONImpl");
            Class[] clsArr6 = new Class[1];
            if (class$twitter4j$internal$org$json$JSONObject == null) {
                cls6 = class$("twitter4j.internal.org.json.JSONObject");
                class$twitter4j$internal$org$json$JSONObject = cls6;
            } else {
                cls6 = class$twitter4j$internal$org$json$JSONObject;
            }
            clsArr6[0] = cls6;
            savedSearchConstructor = cls23.getDeclaredConstructor(clsArr6);
            savedSearchConstructor.setAccessible(true);
            Class<?> cls24 = Class.forName("twitter4j.TrendJSONImpl");
            Class[] clsArr7 = new Class[1];
            if (class$twitter4j$internal$org$json$JSONObject == null) {
                cls7 = class$("twitter4j.internal.org.json.JSONObject");
                class$twitter4j$internal$org$json$JSONObject = cls7;
            } else {
                cls7 = class$twitter4j$internal$org$json$JSONObject;
            }
            clsArr7[0] = cls7;
            trendConstructor = cls24.getDeclaredConstructor(clsArr7);
            trendConstructor.setAccessible(true);
            Class<?> cls25 = Class.forName("twitter4j.TrendsJSONImpl");
            Class[] clsArr8 = new Class[1];
            if (class$java$lang$String == null) {
                cls8 = class$("java.lang.String");
                class$java$lang$String = cls8;
            } else {
                cls8 = class$java$lang$String;
            }
            clsArr8[0] = cls8;
            trendsConstructor = cls25.getDeclaredConstructor(clsArr8);
            trendsConstructor.setAccessible(true);
            Class<?> cls26 = Class.forName("twitter4j.IDsJSONImpl");
            Class[] clsArr9 = new Class[1];
            if (class$java$lang$String == null) {
                cls9 = class$("java.lang.String");
                class$java$lang$String = cls9;
            } else {
                cls9 = class$java$lang$String;
            }
            clsArr9[0] = cls9;
            IDsConstructor = cls26.getDeclaredConstructor(clsArr9);
            IDsConstructor.setAccessible(true);
            Class<?> cls27 = Class.forName("twitter4j.RateLimitStatusJSONImpl");
            Class[] clsArr10 = new Class[1];
            if (class$twitter4j$internal$org$json$JSONObject == null) {
                cls10 = class$("twitter4j.internal.org.json.JSONObject");
                class$twitter4j$internal$org$json$JSONObject = cls10;
            } else {
                cls10 = class$twitter4j$internal$org$json$JSONObject;
            }
            clsArr10[0] = cls10;
            rateLimitStatusConstructor = cls27.getDeclaredConstructor(clsArr10);
            rateLimitStatusConstructor.setAccessible(true);
            Class<?> cls28 = Class.forName("twitter4j.CategoryJSONImpl");
            Class[] clsArr11 = new Class[1];
            if (class$twitter4j$internal$org$json$JSONObject == null) {
                cls11 = class$("twitter4j.internal.org.json.JSONObject");
                class$twitter4j$internal$org$json$JSONObject = cls11;
            } else {
                cls11 = class$twitter4j$internal$org$json$JSONObject;
            }
            clsArr11[0] = cls11;
            categoryConstructor = cls28.getDeclaredConstructor(clsArr11);
            categoryConstructor.setAccessible(true);
            Class<?> cls29 = Class.forName("twitter4j.DirectMessageJSONImpl");
            Class[] clsArr12 = new Class[1];
            if (class$twitter4j$internal$org$json$JSONObject == null) {
                cls12 = class$("twitter4j.internal.org.json.JSONObject");
                class$twitter4j$internal$org$json$JSONObject = cls12;
            } else {
                cls12 = class$twitter4j$internal$org$json$JSONObject;
            }
            clsArr12[0] = cls12;
            directMessageConstructor = cls29.getDeclaredConstructor(clsArr12);
            directMessageConstructor.setAccessible(true);
            Class<?> cls30 = Class.forName("twitter4j.LocationJSONImpl");
            Class[] clsArr13 = new Class[1];
            if (class$twitter4j$internal$org$json$JSONObject == null) {
                cls13 = class$("twitter4j.internal.org.json.JSONObject");
                class$twitter4j$internal$org$json$JSONObject = cls13;
            } else {
                cls13 = class$twitter4j$internal$org$json$JSONObject;
            }
            clsArr13[0] = cls13;
            locationConstructor = cls30.getDeclaredConstructor(clsArr13);
            locationConstructor.setAccessible(true);
            Class<?> cls31 = Class.forName("twitter4j.UserListJSONImpl");
            Class[] clsArr14 = new Class[1];
            if (class$twitter4j$internal$org$json$JSONObject == null) {
                cls14 = class$("twitter4j.internal.org.json.JSONObject");
                class$twitter4j$internal$org$json$JSONObject = cls14;
            } else {
                cls14 = class$twitter4j$internal$org$json$JSONObject;
            }
            clsArr14[0] = cls14;
            userListConstructor = cls31.getDeclaredConstructor(clsArr14);
            userListConstructor.setAccessible(true);
            Class<?> cls32 = Class.forName("twitter4j.RelatedResultsJSONImpl");
            Class[] clsArr15 = new Class[1];
            if (class$twitter4j$internal$org$json$JSONArray == null) {
                cls15 = class$("twitter4j.internal.org.json.JSONArray");
                class$twitter4j$internal$org$json$JSONArray = cls15;
            } else {
                cls15 = class$twitter4j$internal$org$json$JSONArray;
            }
            clsArr15[0] = cls15;
            relatedResultsConstructor = cls32.getDeclaredConstructor(clsArr15);
            relatedResultsConstructor.setAccessible(true);
            Class<?> cls33 = Class.forName("twitter4j.StatusDeletionNoticeImpl");
            Class[] clsArr16 = new Class[1];
            if (class$twitter4j$internal$org$json$JSONObject == null) {
                cls16 = class$("twitter4j.internal.org.json.JSONObject");
                class$twitter4j$internal$org$json$JSONObject = cls16;
            } else {
                cls16 = class$twitter4j$internal$org$json$JSONObject;
            }
            clsArr16[0] = cls16;
            statusDeletionNoticeConstructor = cls33.getDeclaredConstructor(clsArr16);
            statusDeletionNoticeConstructor.setAccessible(true);
            Class<?> cls34 = Class.forName("twitter4j.AccountTotalsJSONImpl");
            Class[] clsArr17 = new Class[1];
            if (class$twitter4j$internal$org$json$JSONObject == null) {
                cls17 = class$("twitter4j.internal.org.json.JSONObject");
                class$twitter4j$internal$org$json$JSONObject = cls17;
            } else {
                cls17 = class$twitter4j$internal$org$json$JSONObject;
            }
            clsArr17[0] = cls17;
            accountTotalsConstructor = cls34.getDeclaredConstructor(clsArr17);
            accountTotalsConstructor.setAccessible(true);
        } catch (NoSuchMethodException e) {
            throw new ExceptionInInitializerError(e);
        } catch (ClassNotFoundException e2) {
            throw new ExceptionInInitializerError(e2);
        }
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError().initCause(x1);
        }
    }

    public static String getRawJSON(Object obj) {
        Object json = rawJsonMap.get().get(obj);
        if (json instanceof String) {
            return (String) json;
        }
        if (json != null) {
            return json.toString();
        }
        return null;
    }

    public static Status createStatus(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return statusConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static User createUser(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return userConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static AccountTotals createAccountTotals(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return accountTotalsConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static Tweet createTweet(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return tweetConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static Relationship createRelationship(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return relationshipConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static Place createPlace(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return placeConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static SavedSearch createSavedSearch(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return savedSearchConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static Trend createTrend(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return trendConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static Trends createTrends(String rawJSON) throws TwitterException {
        try {
            return trendsConstructor.newInstance(rawJSON);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new TwitterException(e2);
        } catch (InvocationTargetException e3) {
            throw new AssertionError(e3);
        }
    }

    public static IDs createIDs(String rawJSON) throws TwitterException {
        try {
            return IDsConstructor.newInstance(rawJSON);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        }
    }

    public static RateLimitStatus createRateLimitStatus(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return rateLimitStatusConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static Category createCategory(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return categoryConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static DirectMessage createDirectMessage(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return directMessageConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static Location createLocation(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return locationConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static UserList createUserList(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return userListConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static RelatedResults createRelatedResults(String rawJSON) throws TwitterException {
        try {
            JSONArray json = new JSONArray(rawJSON);
            return relatedResultsConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static Object createObject(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            JSONObjectType jsonObjectType = JSONObjectType.determine(json);
            if (JSONObjectType.SENDER == jsonObjectType) {
                return registerJSONObject(directMessageConstructor.newInstance(json.getJSONObject("direct_message")), json);
            } else if (JSONObjectType.STATUS == jsonObjectType) {
                return registerJSONObject(statusConstructor.newInstance(json), json);
            } else if (JSONObjectType.DIRECT_MESSAGE == jsonObjectType) {
                return registerJSONObject(directMessageConstructor.newInstance(json.getJSONObject("direct_message")), json);
            } else if (JSONObjectType.DELETE == jsonObjectType) {
                return registerJSONObject(statusDeletionNoticeConstructor.newInstance(json.getJSONObject("delete").getJSONObject("status")), json);
            } else if (JSONObjectType.LIMIT == jsonObjectType) {
                return json;
            } else {
                if (JSONObjectType.SCRUB_GEO == jsonObjectType) {
                    return json;
                }
                return json;
            }
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    static void clearThreadLocalMap() {
        rawJsonMap.get().clear();
    }

    static <T> T registerJSONObject(T key, Object json) {
        rawJsonMap.get().put(key, json);
        return key;
    }
}
