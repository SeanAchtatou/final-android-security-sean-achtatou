package twitter4j;

import java.lang.management.ManagementFactory;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import twitter4j.conf.ConfigurationContext;
import twitter4j.internal.logging.Logger;
import twitter4j.management.APIStatistics;
import twitter4j.management.APIStatisticsMBean;
import twitter4j.management.APIStatisticsOpenMBean;

public class TwitterAPIMonitor {
    private static final TwitterAPIMonitor SINGLETON = new TwitterAPIMonitor();
    static Class class$twitter4j$TwitterAPIMonitor;
    private static final Logger logger;
    private static final Pattern pattern = Pattern.compile("https?:\\/\\/[^\\/]+\\/([a-zA-Z_\\.]*).*");
    private final APIStatistics STATISTICS = new APIStatistics(100);

    static {
        Class cls;
        if (class$twitter4j$TwitterAPIMonitor == null) {
            cls = class$("twitter4j.TwitterAPIMonitor");
            class$twitter4j$TwitterAPIMonitor = cls;
        } else {
            cls = class$twitter4j$TwitterAPIMonitor;
        }
        logger = Logger.getLogger(cls);
        boolean isJDK14orEarlier = false;
        try {
            String versionStr = System.getProperty("java.specification.version");
            if (versionStr != null) {
                isJDK14orEarlier = 1.5d > Double.parseDouble(versionStr);
            }
            if (ConfigurationContext.getInstance().isDalvik()) {
                System.setProperty("http.keepAlive", "false");
            }
        } catch (SecurityException e) {
            isJDK14orEarlier = true;
        }
        try {
            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
            APIStatistics statsMBean = new APIStatistics(100);
            if (isJDK14orEarlier) {
                mbs.registerMBean(statsMBean, new ObjectName("twitter4j.mbean:type=APIStatistics"));
            } else {
                mbs.registerMBean(new APIStatisticsOpenMBean(statsMBean), new ObjectName("twitter4j.mbean:type=APIStatisticsOpenMBean"));
            }
        } catch (InstanceAlreadyExistsException e2) {
            InstanceAlreadyExistsException e3 = e2;
            e3.printStackTrace();
            logger.error(e3.getMessage());
        } catch (MBeanRegistrationException e4) {
            MBeanRegistrationException e5 = e4;
            e5.printStackTrace();
            logger.error(e5.getMessage());
        } catch (NotCompliantMBeanException e6) {
            NotCompliantMBeanException e7 = e6;
            e7.printStackTrace();
            logger.error(e7.getMessage());
        } catch (MalformedObjectNameException e8) {
            MalformedObjectNameException e9 = e8;
            e9.printStackTrace();
            logger.error(e9.getMessage());
        }
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError().initCause(x1);
        }
    }

    private TwitterAPIMonitor() {
    }

    public static TwitterAPIMonitor getInstance() {
        return SINGLETON;
    }

    public APIStatisticsMBean getStatistics() {
        return this.STATISTICS;
    }

    /* access modifiers changed from: package-private */
    public void methodCalled(String twitterUrl, long elapsedTime, boolean success) {
        Matcher matcher = pattern.matcher(twitterUrl);
        if (matcher.matches() && matcher.groupCount() > 0) {
            this.STATISTICS.methodCalled(matcher.group(), elapsedTime, success);
        }
    }
}
