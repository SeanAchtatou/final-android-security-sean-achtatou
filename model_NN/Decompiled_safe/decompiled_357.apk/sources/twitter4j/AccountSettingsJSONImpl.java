package twitter4j;

import java.io.Serializable;
import twitter4j.conf.Configuration;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.json.DataObjectFactoryUtil;
import twitter4j.internal.org.json.JSONArray;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.internal.util.ParseUtil;

class AccountSettingsJSONImpl extends TwitterResponseImpl implements AccountSettings, Serializable {
    private static final long serialVersionUID = 7983363611306383416L;
    private final boolean GEO_ENABLED;
    private final String SLEEP_END_TIME;
    private final String SLEEP_START_TIME;
    private final boolean SLEEP_TIME_ENABLED;
    private final Location[] TREND_LOCATION;

    private AccountSettingsJSONImpl(HttpResponse res, JSONObject json) throws TwitterException {
        super(res);
        try {
            JSONObject sleepTime = json.getJSONObject("sleep_time");
            this.SLEEP_TIME_ENABLED = ParseUtil.getBoolean("enabled", sleepTime);
            this.SLEEP_START_TIME = sleepTime.getString("start_time");
            this.SLEEP_END_TIME = sleepTime.getString("end_time");
            if (json.isNull("trend_location")) {
                this.TREND_LOCATION = new Location[0];
            } else {
                JSONArray locations = json.getJSONArray("trend_location");
                this.TREND_LOCATION = new Location[locations.length()];
                for (int i = 0; i < locations.length(); i++) {
                    this.TREND_LOCATION[i] = new LocationJSONImpl(locations.getJSONObject(i));
                }
            }
            this.GEO_ENABLED = ParseUtil.getBoolean("geo_enabled", json);
        } catch (JSONException e) {
            throw new TwitterException(e);
        }
    }

    AccountSettingsJSONImpl(HttpResponse res, Configuration conf) throws TwitterException {
        this(res, res.asJSONObject());
        if (conf.isJSONStoreEnabled()) {
            DataObjectFactoryUtil.clearThreadLocalMap();
            DataObjectFactoryUtil.registerJSONObject(this, res.asJSONObject());
        }
    }

    AccountSettingsJSONImpl(JSONObject json) throws TwitterException {
        this((HttpResponse) null, json);
    }

    public boolean isSleepTimeEnabled() {
        return this.SLEEP_TIME_ENABLED;
    }

    public String getSleepStartTime() {
        return this.SLEEP_START_TIME;
    }

    public String getSleepEndTime() {
        return this.SLEEP_END_TIME;
    }

    public Location[] getTrendLocations() {
        return this.TREND_LOCATION;
    }

    public boolean isGeoEnabled() {
        return this.GEO_ENABLED;
    }
}
