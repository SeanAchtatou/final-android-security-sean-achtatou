package twitter4j.auth;

import java.io.Serializable;
import javax.crypto.spec.SecretKeySpec;
import org.codehaus.jackson.org.objectweb.asm.signature.SignatureVisitor;
import twitter4j.TwitterException;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.util.T4JInternalStringUtil;

abstract class OAuthToken implements Serializable {
    private static final long serialVersionUID = 3891133932519746686L;
    String[] responseStr;
    private transient SecretKeySpec secretKeySpec;
    private String token;
    private String tokenSecret;

    public OAuthToken(String token2, String tokenSecret2) {
        this.responseStr = null;
        this.token = token2;
        this.tokenSecret = tokenSecret2;
    }

    OAuthToken(HttpResponse response) throws TwitterException {
        this(response.asString());
    }

    OAuthToken(String string) {
        this.responseStr = null;
        this.responseStr = T4JInternalStringUtil.split(string, "&");
        this.tokenSecret = getParameter("oauth_token_secret");
        this.token = getParameter("oauth_token");
    }

    public String getToken() {
        return this.token;
    }

    public String getTokenSecret() {
        return this.tokenSecret;
    }

    /* access modifiers changed from: package-private */
    public void setSecretKeySpec(SecretKeySpec secretKeySpec2) {
        this.secretKeySpec = secretKeySpec2;
    }

    /* access modifiers changed from: package-private */
    public SecretKeySpec getSecretKeySpec() {
        return this.secretKeySpec;
    }

    public String getParameter(String parameter) {
        for (String str : this.responseStr) {
            if (str.startsWith(new StringBuffer().append(parameter).append((char) SignatureVisitor.INSTANCEOF).toString())) {
                return T4JInternalStringUtil.split(str, "=")[1].trim();
            }
        }
        return null;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OAuthToken)) {
            return false;
        }
        OAuthToken that = (OAuthToken) o;
        if (!this.token.equals(that.token)) {
            return false;
        }
        return this.tokenSecret.equals(that.tokenSecret);
    }

    public int hashCode() {
        return (this.token.hashCode() * 31) + this.tokenSecret.hashCode();
    }

    public String toString() {
        return new StringBuffer().append("OAuthToken{token='").append(this.token).append('\'').append(", tokenSecret='").append(this.tokenSecret).append('\'').append(", secretKeySpec=").append(this.secretKeySpec).append('}').toString();
    }
}
