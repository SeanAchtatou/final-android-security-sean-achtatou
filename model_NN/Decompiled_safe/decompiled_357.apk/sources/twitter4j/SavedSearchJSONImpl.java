package twitter4j;

import java.util.Date;
import twitter4j.conf.Configuration;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.json.DataObjectFactoryUtil;
import twitter4j.internal.org.json.JSONArray;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.internal.util.ParseUtil;

final class SavedSearchJSONImpl extends TwitterResponseImpl implements SavedSearch {
    private static final long serialVersionUID = 3083819860391598212L;
    private Date createdAt;
    private int id;
    private String name;
    private int position;
    private String query;

    public int compareTo(Object x0) {
        return compareTo((SavedSearch) x0);
    }

    SavedSearchJSONImpl(HttpResponse res, Configuration conf) throws TwitterException {
        super(res);
        if (conf.isJSONStoreEnabled()) {
            DataObjectFactoryUtil.clearThreadLocalMap();
        }
        JSONObject json = res.asJSONObject();
        init(json);
        if (conf.isJSONStoreEnabled()) {
            DataObjectFactoryUtil.registerJSONObject(this, json);
        }
    }

    SavedSearchJSONImpl(JSONObject savedSearch) throws TwitterException {
        init(savedSearch);
    }

    static ResponseList<SavedSearch> createSavedSearchList(HttpResponse res, Configuration conf) throws TwitterException {
        if (conf.isJSONStoreEnabled()) {
            DataObjectFactoryUtil.clearThreadLocalMap();
        }
        JSONArray json = res.asJSONArray();
        try {
            ResponseList<SavedSearch> savedSearches = new ResponseListImpl<>(json.length(), res);
            for (int i = 0; i < json.length(); i++) {
                JSONObject savedSearchesJSON = json.getJSONObject(i);
                SavedSearch savedSearch = new SavedSearchJSONImpl(savedSearchesJSON);
                savedSearches.add(savedSearch);
                if (conf.isJSONStoreEnabled()) {
                    DataObjectFactoryUtil.registerJSONObject(savedSearch, savedSearchesJSON);
                }
            }
            if (conf.isJSONStoreEnabled()) {
                DataObjectFactoryUtil.registerJSONObject(savedSearches, json);
            }
            return savedSearches;
        } catch (JSONException e) {
            JSONException jsone = e;
            throw new TwitterException(new StringBuffer().append(jsone.getMessage()).append(":").append(res.asString()).toString(), jsone);
        }
    }

    private void init(JSONObject savedSearch) throws TwitterException {
        this.createdAt = ParseUtil.getDate("created_at", savedSearch, "EEE MMM dd HH:mm:ss z yyyy");
        this.query = ParseUtil.getUnescapedString("query", savedSearch);
        this.position = ParseUtil.getInt("position", savedSearch);
        this.name = ParseUtil.getUnescapedString("name", savedSearch);
        this.id = ParseUtil.getInt("id", savedSearch);
    }

    public int compareTo(SavedSearch that) {
        return this.id - that.getId();
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public String getQuery() {
        return this.query;
    }

    public int getPosition() {
        return this.position;
    }

    public String getName() {
        return this.name;
    }

    public int getId() {
        return this.id;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SavedSearch)) {
            return false;
        }
        return this.id == ((SavedSearch) o).getId();
    }

    public int hashCode() {
        return (((((((this.createdAt.hashCode() * 31) + this.query.hashCode()) * 31) + this.position) * 31) + this.name.hashCode()) * 31) + this.id;
    }

    public String toString() {
        return new StringBuffer().append("SavedSearchJSONImpl{createdAt=").append(this.createdAt).append(", query='").append(this.query).append('\'').append(", position=").append(this.position).append(", name='").append(this.name).append('\'').append(", id=").append(this.id).append('}').toString();
    }
}
