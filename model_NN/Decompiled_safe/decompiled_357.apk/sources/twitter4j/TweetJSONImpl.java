package twitter4j;

import java.io.Serializable;
import java.util.Date;
import twitter4j.conf.Configuration;
import twitter4j.internal.json.DataObjectFactoryUtil;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.internal.util.ParseUtil;

final class TweetJSONImpl implements Tweet, Serializable {
    private static final long serialVersionUID = 3019285230338056113L;
    private Annotations annotations;
    private Date createdAt;
    private String fromUser;
    private long fromUserId;
    private GeoLocation geoLocation;
    private long id;
    private String isoLanguageCode;
    private String location;
    private Place place;
    private String profileImageUrl;
    private String source;
    private String text;
    private String toUser;
    private long toUserId;

    public int compareTo(Object x0) {
        return compareTo((Tweet) x0);
    }

    TweetJSONImpl(JSONObject tweet) throws TwitterException {
        this.toUserId = -1;
        this.toUser = null;
        this.isoLanguageCode = null;
        this.geoLocation = null;
        this.annotations = null;
        this.text = ParseUtil.getUnescapedString("text", tweet);
        this.toUserId = ParseUtil.getLong("to_user_id", tweet);
        this.toUser = ParseUtil.getRawString("to_user", tweet);
        this.fromUser = ParseUtil.getRawString("from_user", tweet);
        this.id = ParseUtil.getLong("id", tweet);
        this.fromUserId = ParseUtil.getLong("from_user_id", tweet);
        this.isoLanguageCode = ParseUtil.getRawString("iso_language_code", tweet);
        this.source = ParseUtil.getUnescapedString("source", tweet);
        this.profileImageUrl = ParseUtil.getUnescapedString("profile_image_url", tweet);
        this.createdAt = ParseUtil.getDate("created_at", tweet, "EEE, dd MMM yyyy HH:mm:ss z");
        this.location = ParseUtil.getRawString("location", tweet);
        this.geoLocation = GeoLocation.getInstance(tweet);
        if (!tweet.isNull("annotations")) {
            try {
                this.annotations = new Annotations(tweet.getJSONArray("annotations"));
            } catch (JSONException e) {
            }
        }
        if (!tweet.isNull("place")) {
            try {
                this.place = new PlaceJSONImpl(tweet.getJSONObject("place"));
            } catch (JSONException e2) {
                throw new TwitterException(e2);
            }
        } else {
            this.place = null;
        }
    }

    TweetJSONImpl(JSONObject tweet, Configuration conf) throws TwitterException {
        this(tweet);
        if (conf.isJSONStoreEnabled()) {
            DataObjectFactoryUtil.registerJSONObject(this, tweet);
        }
    }

    public int compareTo(Tweet that) {
        long delta = this.id - that.getId();
        if (delta < -2147483648L) {
            return Integer.MIN_VALUE;
        }
        if (delta > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        return (int) delta;
    }

    public String getText() {
        return this.text;
    }

    public long getToUserId() {
        return this.toUserId;
    }

    public String getToUser() {
        return this.toUser;
    }

    public String getFromUser() {
        return this.fromUser;
    }

    public long getId() {
        return this.id;
    }

    public long getFromUserId() {
        return this.fromUserId;
    }

    public String getIsoLanguageCode() {
        return this.isoLanguageCode;
    }

    public String getSource() {
        return this.source;
    }

    public String getProfileImageUrl() {
        return this.profileImageUrl;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public GeoLocation getGeoLocation() {
        return this.geoLocation;
    }

    public String getLocation() {
        return this.location;
    }

    public Place getPlace() {
        return this.place;
    }

    public Annotations getAnnotations() {
        return this.annotations;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Tweet)) {
            return false;
        }
        return this.id == ((Tweet) o).getId();
    }

    public int hashCode() {
        int result;
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        if (this.text != null) {
            result = this.text.hashCode();
        } else {
            result = 0;
        }
        int i11 = ((result * 31) + ((int) (this.toUserId ^ (this.toUserId >>> 32)))) * 31;
        if (this.toUser != null) {
            i = this.toUser.hashCode();
        } else {
            i = 0;
        }
        int i12 = (i11 + i) * 31;
        if (this.fromUser != null) {
            i2 = this.fromUser.hashCode();
        } else {
            i2 = 0;
        }
        int i13 = (((((i12 + i2) * 31) + ((int) (this.id ^ (this.id >>> 32)))) * 31) + ((int) (this.fromUserId ^ (this.fromUserId >>> 32)))) * 31;
        if (this.isoLanguageCode != null) {
            i3 = this.isoLanguageCode.hashCode();
        } else {
            i3 = 0;
        }
        int i14 = (i13 + i3) * 31;
        if (this.source != null) {
            i4 = this.source.hashCode();
        } else {
            i4 = 0;
        }
        int i15 = (i14 + i4) * 31;
        if (this.profileImageUrl != null) {
            i5 = this.profileImageUrl.hashCode();
        } else {
            i5 = 0;
        }
        int i16 = (i15 + i5) * 31;
        if (this.createdAt != null) {
            i6 = this.createdAt.hashCode();
        } else {
            i6 = 0;
        }
        int i17 = (i16 + i6) * 31;
        if (this.location != null) {
            i7 = this.location.hashCode();
        } else {
            i7 = 0;
        }
        int i18 = (i17 + i7) * 31;
        if (this.place != null) {
            i8 = this.place.hashCode();
        } else {
            i8 = 0;
        }
        int i19 = (i18 + i8) * 31;
        if (this.geoLocation != null) {
            i9 = this.geoLocation.hashCode();
        } else {
            i9 = 0;
        }
        int i20 = (i19 + i9) * 31;
        if (this.annotations != null) {
            i10 = this.annotations.hashCode();
        } else {
            i10 = 0;
        }
        return i20 + i10;
    }

    public String toString() {
        return new StringBuffer().append("TweetJSONImpl{text='").append(this.text).append('\'').append(", toUserId=").append(this.toUserId).append(", toUser='").append(this.toUser).append('\'').append(", fromUser='").append(this.fromUser).append('\'').append(", id=").append(this.id).append(", fromUserId=").append(this.fromUserId).append(", isoLanguageCode='").append(this.isoLanguageCode).append('\'').append(", source='").append(this.source).append('\'').append(", profileImageUrl='").append(this.profileImageUrl).append('\'').append(", createdAt=").append(this.createdAt).append(", location='").append(this.location).append('\'').append(", place=").append(this.place).append(", geoLocation=").append(this.geoLocation).append(", annotations=").append(this.annotations).append('}').toString();
    }
}
