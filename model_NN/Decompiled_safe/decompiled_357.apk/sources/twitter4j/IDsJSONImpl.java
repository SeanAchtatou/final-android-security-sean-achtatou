package twitter4j;

import java.util.Arrays;
import twitter4j.conf.Configuration;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.json.DataObjectFactoryUtil;

final class IDsJSONImpl extends TwitterResponseImpl implements IDs {
    private static final long serialVersionUID = -6585026560164704953L;
    private long[] ids;
    private long nextCursor = -1;
    private long previousCursor = -1;

    IDsJSONImpl(HttpResponse res, Configuration conf) throws TwitterException {
        super(res);
        String json = res.asString();
        init(json);
        System.out.println(conf.isJSONStoreEnabled());
        if (conf.isJSONStoreEnabled()) {
            DataObjectFactoryUtil.clearThreadLocalMap();
            DataObjectFactoryUtil.registerJSONObject(this, json);
        }
    }

    IDsJSONImpl(String json) throws TwitterException {
        init(json);
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void init(java.lang.String r9) throws twitter4j.TwitterException {
        /*
            r8 = this;
            java.lang.String r5 = "{"
            boolean r5 = r9.startsWith(r5)     // Catch:{ JSONException -> 0x004c }
            if (r5 == 0) goto L_0x0065
            twitter4j.internal.org.json.JSONObject r2 = new twitter4j.internal.org.json.JSONObject     // Catch:{ JSONException -> 0x004c }
            r2.<init>(r9)     // Catch:{ JSONException -> 0x004c }
            java.lang.String r5 = "ids"
            twitter4j.internal.org.json.JSONArray r1 = r2.getJSONArray(r5)     // Catch:{ JSONException -> 0x004c }
            int r5 = r1.length()     // Catch:{ JSONException -> 0x004c }
            long[] r5 = new long[r5]     // Catch:{ JSONException -> 0x004c }
            r8.ids = r5     // Catch:{ JSONException -> 0x004c }
            r0 = 0
        L_0x001c:
            int r5 = r1.length()     // Catch:{ JSONException -> 0x004c }
            if (r0 >= r5) goto L_0x0054
            long[] r5 = r8.ids     // Catch:{ NumberFormatException -> 0x0031 }
            java.lang.String r6 = r1.getString(r0)     // Catch:{ NumberFormatException -> 0x0031 }
            long r6 = java.lang.Long.parseLong(r6)     // Catch:{ NumberFormatException -> 0x0031 }
            r5[r0] = r6     // Catch:{ NumberFormatException -> 0x0031 }
            int r0 = r0 + 1
            goto L_0x001c
        L_0x0031:
            r5 = move-exception
            r4 = r5
            twitter4j.TwitterException r5 = new twitter4j.TwitterException     // Catch:{ JSONException -> 0x004c }
            java.lang.StringBuffer r6 = new java.lang.StringBuffer     // Catch:{ JSONException -> 0x004c }
            r6.<init>()     // Catch:{ JSONException -> 0x004c }
            java.lang.String r7 = "Twitter API returned malformed response: "
            java.lang.StringBuffer r6 = r6.append(r7)     // Catch:{ JSONException -> 0x004c }
            java.lang.StringBuffer r6 = r6.append(r2)     // Catch:{ JSONException -> 0x004c }
            java.lang.String r6 = r6.toString()     // Catch:{ JSONException -> 0x004c }
            r5.<init>(r6, r4)     // Catch:{ JSONException -> 0x004c }
            throw r5     // Catch:{ JSONException -> 0x004c }
        L_0x004c:
            r5 = move-exception
            r3 = r5
            twitter4j.TwitterException r5 = new twitter4j.TwitterException
            r5.<init>(r3)
            throw r5
        L_0x0054:
            java.lang.String r5 = "previous_cursor"
            long r5 = twitter4j.internal.util.ParseUtil.getLong(r5, r2)     // Catch:{ JSONException -> 0x004c }
            r8.previousCursor = r5     // Catch:{ JSONException -> 0x004c }
            java.lang.String r5 = "next_cursor"
            long r5 = twitter4j.internal.util.ParseUtil.getLong(r5, r2)     // Catch:{ JSONException -> 0x004c }
            r8.nextCursor = r5     // Catch:{ JSONException -> 0x004c }
        L_0x0064:
            return
        L_0x0065:
            twitter4j.internal.org.json.JSONArray r1 = new twitter4j.internal.org.json.JSONArray     // Catch:{ JSONException -> 0x004c }
            r1.<init>(r9)     // Catch:{ JSONException -> 0x004c }
            int r5 = r1.length()     // Catch:{ JSONException -> 0x004c }
            long[] r5 = new long[r5]     // Catch:{ JSONException -> 0x004c }
            r8.ids = r5     // Catch:{ JSONException -> 0x004c }
            r0 = 0
        L_0x0073:
            int r5 = r1.length()     // Catch:{ JSONException -> 0x004c }
            if (r0 >= r5) goto L_0x0064
            long[] r5 = r8.ids     // Catch:{ NumberFormatException -> 0x0088 }
            java.lang.String r6 = r1.getString(r0)     // Catch:{ NumberFormatException -> 0x0088 }
            long r6 = java.lang.Long.parseLong(r6)     // Catch:{ NumberFormatException -> 0x0088 }
            r5[r0] = r6     // Catch:{ NumberFormatException -> 0x0088 }
            int r0 = r0 + 1
            goto L_0x0073
        L_0x0088:
            r5 = move-exception
            r4 = r5
            twitter4j.TwitterException r5 = new twitter4j.TwitterException     // Catch:{ JSONException -> 0x004c }
            java.lang.StringBuffer r6 = new java.lang.StringBuffer     // Catch:{ JSONException -> 0x004c }
            r6.<init>()     // Catch:{ JSONException -> 0x004c }
            java.lang.String r7 = "Twitter API returned malformed response: "
            java.lang.StringBuffer r6 = r6.append(r7)     // Catch:{ JSONException -> 0x004c }
            java.lang.StringBuffer r6 = r6.append(r1)     // Catch:{ JSONException -> 0x004c }
            java.lang.String r6 = r6.toString()     // Catch:{ JSONException -> 0x004c }
            r5.<init>(r6, r4)     // Catch:{ JSONException -> 0x004c }
            throw r5     // Catch:{ JSONException -> 0x004c }
        */
        throw new UnsupportedOperationException("Method not decompiled: twitter4j.IDsJSONImpl.init(java.lang.String):void");
    }

    public long[] getIDs() {
        return this.ids;
    }

    public boolean hasPrevious() {
        return 0 != this.previousCursor;
    }

    public long getPreviousCursor() {
        return this.previousCursor;
    }

    public boolean hasNext() {
        return 0 != this.nextCursor;
    }

    public long getNextCursor() {
        return this.nextCursor;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof IDs)) {
            return false;
        }
        return Arrays.equals(this.ids, ((IDs) o).getIDs());
    }

    public int hashCode() {
        if (this.ids != null) {
            return Arrays.hashCode(this.ids);
        }
        return 0;
    }

    public String toString() {
        return new StringBuffer().append("IDsJSONImpl{ids=").append(Arrays.toString(this.ids)).append(", previousCursor=").append(this.previousCursor).append(", nextCursor=").append(this.nextCursor).append('}').toString();
    }
}
