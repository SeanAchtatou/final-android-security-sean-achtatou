package com.roobit.restkit;

import org.json.JSONException;
import org.json.JSONObject;

public class Result {
    private Exception exception;
    private String response = "";
    private JSONObject responseJson;
    private int statusCode = 0;

    public int getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(int statusCode2) {
        this.statusCode = statusCode2;
    }

    public String getResponse() {
        return this.response;
    }

    public void setResponse(String response2) {
        this.response = response2;
    }

    public Exception getException() {
        return this.exception;
    }

    public void setException(Exception e) {
        this.exception = e;
    }

    public String debugString() {
        StringBuilder sb = new StringBuilder("Result - ");
        sb.append("Status code: ").append(String.valueOf(this.statusCode)).append("\n");
        if (this.response.length() > 0) {
            sb.append("Response: ").append(this.response).append("\n");
        }
        if (this.exception != null) {
            sb.append("Exception: ").append(this.exception.toString()).append(", ").append(this.exception.getMessage());
        }
        return sb.toString();
    }

    public boolean isSuccess() {
        return getStatusCode() == 200 && !getResponse().contains("error_code");
    }

    public int getErrorCode() {
        try {
            return getResponseAsJson().getInt("error_code");
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public String getErrorMessage() {
        try {
            return getResponseAsJson().getJSONObject("error").getString("message");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private JSONObject getResponseAsJson() throws JSONException {
        if (this.responseJson == null) {
            this.responseJson = new JSONObject(getResponse());
        }
        return this.responseJson;
    }
}
