package com.roobit.restkit;

import android.net.Uri;
import android.util.Log;
import com.google.common.collect.Iterators;
import com.roobit.restkit.RestClientRequestTask;
import java.util.Iterator;
import java.util.Properties;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;

public class RestClient implements RestClientRequestTask.RestClientRequestDelegate {
    private static ObjectMapper objectMapper = null;
    private String baseUrl;
    private RestClientDelegate delegate;
    private Properties headers;
    private Operation operation;
    private Properties params;
    private String path;
    private Properties queryParameters;
    private String queryParametersString;
    private String resource;

    enum Operation {
        GET,
        POST,
        PUT,
        DELETE,
        PATCH
    }

    public interface RestClientDelegate {
        void failedWithError(RestClient restClient, int i);

        void success(RestClient restClient, String str);
    }

    public static RestClient getClientWithBaseUrl(String baseUrl2) {
        return new RestClient(baseUrl2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.ObjectMapper.configure(org.codehaus.jackson.map.DeserializationConfig$Feature, boolean):org.codehaus.jackson.map.ObjectMapper
     arg types: [org.codehaus.jackson.map.DeserializationConfig$Feature, int]
     candidates:
      org.codehaus.jackson.map.ObjectMapper.configure(org.codehaus.jackson.JsonGenerator$Feature, boolean):org.codehaus.jackson.map.ObjectMapper
      org.codehaus.jackson.map.ObjectMapper.configure(org.codehaus.jackson.JsonParser$Feature, boolean):org.codehaus.jackson.map.ObjectMapper
      org.codehaus.jackson.map.ObjectMapper.configure(org.codehaus.jackson.map.SerializationConfig$Feature, boolean):org.codehaus.jackson.map.ObjectMapper
      org.codehaus.jackson.map.ObjectMapper.configure(org.codehaus.jackson.map.DeserializationConfig$Feature, boolean):org.codehaus.jackson.map.ObjectMapper */
    public static ObjectMapper getObjectMapper() {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        }
        return objectMapper;
    }

    protected RestClient(String baseUrl2) {
        this.baseUrl = baseUrl2;
    }

    public RestClient post() {
        this.operation = Operation.POST;
        setQueryParameters(null);
        setHeaders(null);
        return this;
    }

    public RestClient patch() {
        this.operation = Operation.PATCH;
        setQueryParameters(null);
        setHeaders(null);
        return this;
    }

    public RestClient post(String queryParameterString) {
        return post().setQueryParameterString(queryParameterString);
    }

    public RestClient post(Properties params2) {
        return post().setParameters(params2);
    }

    public RestClient postHeaders(Properties headers2) {
        return post().setHeaders(headers2);
    }

    public RestClient postQuery(Properties queryParams) {
        return post().setQueryParameters(queryParams);
    }

    public RestClient postQuery(Properties queryParams, Properties headers2) {
        return post().setQueryParameters(queryParams).setHeaders(headers2);
    }

    private RestClient setHeaders(Properties headers2) {
        this.headers = headers2;
        return this;
    }

    public RestClient patch(Properties queryParams) {
        return patch().setQueryParameters(queryParams);
    }

    public RestClient patch(Properties queryParams, Properties headers2) {
        return patch().setQueryParameters(queryParams).setHeaders(headers2);
    }

    public RestClient setParameters(Properties params2) {
        this.params = params2;
        return this;
    }

    public RestClient setDelegate(RestClientDelegate delegate2) {
        this.delegate = delegate2;
        return this;
    }

    public void execute() {
        new RestClientRequestTask(this).execute(this.operation, buildUri(), this.params, this.headers);
    }

    public Result synchronousExecute() {
        return RestClientRequest.synchronousExecute(this.operation, buildUri(), this.params, this.headers);
    }

    public String getUrl() {
        String url = buildUri().toString();
        Log.d("RestClient", "Url: " + url);
        return url;
    }

    private Uri buildUri() {
        Uri.Builder builder = Uri.parse(buildUrlPath()).buildUpon().appendEncodedPath(getResource());
        if (getQueryParameterString() != null) {
            builder.query(getQueryParameterString());
        }
        Iterator<?> iter = Iterators.forEnumeration(getQueryParameters().propertyNames());
        while (iter.hasNext()) {
            String key = (String) iter.next();
            builder.appendQueryParameter(key, getQueryParameter(key));
        }
        return builder.build();
    }

    private String buildUrlPath() {
        StringBuilder sb = new StringBuilder(getBaseUrl());
        if (this.path != null) {
            sb.append(this.path);
        }
        return sb.toString();
    }

    public String getBaseUrl() {
        return this.baseUrl;
    }

    public RestClient get() {
        this.operation = Operation.GET;
        setQueryParameters(null);
        setHeaders(null);
        return this;
    }

    public RestClient get(String queryParametersString2) {
        return get().setQueryParameterString(queryParametersString2);
    }

    public RestClient get(Properties headers2) {
        return get().setHeaders(headers2);
    }

    public RestClient get(Properties queryParams, Properties headers2) {
        return get().setQueryParameters(queryParams).setHeaders(headers2);
    }

    public void requestStarted() {
    }

    public void requestCancelled() {
    }

    public void requestFinished(Result result) {
        Log.d("RestClient", "Request finished with status code " + String.valueOf(result.getStatusCode()));
        if (result.getStatusCode() == 200) {
            this.delegate.success(this, result.getResponse());
        } else {
            this.delegate.failedWithError(this, result.getStatusCode());
        }
    }

    public String getResource() {
        return this.resource;
    }

    public RestClient setResource(String resource2) {
        this.resource = resource2;
        return this;
    }

    private RestClient setQueryParameterString(String queryParametersString2) {
        this.queryParametersString = queryParametersString2;
        return this;
    }

    private String getQueryParameterString() {
        return this.queryParametersString;
    }

    public RestClient setQueryParameters(Properties queryParameters2) {
        this.queryParameters = queryParameters2;
        return this;
    }

    public Properties getQueryParameters() {
        if (this.queryParameters == null) {
            this.queryParameters = new Properties();
        }
        return this.queryParameters;
    }

    private String getQueryParameter(String key) {
        return getQueryParameters().getProperty(key);
    }
}
