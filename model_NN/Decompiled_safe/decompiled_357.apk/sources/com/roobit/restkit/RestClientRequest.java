package com.roobit.restkit;

import android.net.Uri;
import android.util.Log;
import com.roobit.restkit.RestClient;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.AbstractHttpMessage;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public class RestClientRequest {
    private static final int CONN_TIMEOUT_MS = 30000;
    private static final String TAG = "RestClientRequest";
    private static DefaultHttpClient httpClient;

    public static Result synchronousExecute(RestClient.Operation op, Uri uri, Properties params) {
        Log.d(TAG, "Executing " + op.toString() + " to " + uri.toString() + " without headers");
        return synchronousExecute(op, uri, params, null);
    }

    public static Result synchronousExecute(RestClient.Operation op, Uri uri, Properties props, Properties headers) {
        HttpEntity entity;
        InputStream is;
        Log.d(TAG, "Executing " + op.toString() + " to " + uri.toString());
        DefaultHttpClient client = getHttpClient();
        HttpRequestBase request = createHttpRequest(op, uri, props, headers);
        Result result = new Result();
        try {
            HttpResponse response = client.execute(request);
            int statusCode = response.getStatusLine().getStatusCode();
            result.setStatusCode(statusCode);
            if (statusCode >= 300) {
                throw new HttpResponseException(statusCode, response.getStatusLine().toString());
            }
            entity = response.getEntity();
            if (entity != null) {
                is = null;
                is = entity.getContent();
                result.setResponse(convertStreamToString(is));
                Log.d(TAG, "Consuming entity");
                entity.consumeContent();
                if (is != null) {
                    is.close();
                }
            }
            Log.d(TAG, "result: " + result.debugString());
            return result;
        } catch (HttpResponseException e) {
            HttpResponseException e2 = e;
            e2.printStackTrace();
            result.setStatusCode(e2.getStatusCode());
            result.setResponse(e2.getMessage());
        } catch (SocketTimeoutException e3) {
            result.setException(e3);
        } catch (Exception e4) {
            Exception e5 = e4;
            e5.printStackTrace();
            result.setException(e5);
        } catch (Throwable th) {
            Log.d(TAG, "Consuming entity");
            entity.consumeContent();
            if (is != null) {
                is.close();
            }
            throw th;
        }
    }

    private static String convertStreamToString(InputStream is) throws IOException {
        int read;
        char[] buffer = new char[65536];
        StringBuilder sb = new StringBuilder();
        Reader in = new InputStreamReader(is, "UTF-8");
        do {
            read = in.read(buffer, 0, buffer.length);
            if (read > 0) {
                sb.append(buffer, 0, read);
                continue;
            }
        } while (read >= 0);
        return sb.toString();
    }

    private static DefaultHttpClient getHttpClient() {
        HttpParams params = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(params, CONN_TIMEOUT_MS);
        HttpConnectionParams.setSoTimeout(params, CONN_TIMEOUT_MS);
        httpClient = new DefaultHttpClient(params);
        httpClient.getParams().setParameter("http.protocol.cookie-policy", "compatibility");
        return httpClient;
    }

    private static HttpRequestBase createHttpRequest(RestClient.Operation op, Uri uri, Properties props, Properties headers) {
        switch (op) {
            case POST:
                return createPost(uri, props, headers);
            case GET:
                return createGet(uri, headers);
            case PATCH:
                return createPatch(uri, props, headers);
            default:
                return null;
        }
    }

    private static HttpPost createPatch(Uri uri, Properties props, Properties headers) {
        HttpPost post = createPost(uri, props, headers);
        post.setHeader("X-HTTP-Method-Override", "PATCH");
        return post;
    }

    private static HttpPost createPost(Uri uri, Properties props, Properties headers) {
        HttpPost post = new HttpPost(uri.toString());
        post.addHeader("Accept", "application/json");
        post.addHeader("Content-type", "application/json; charset=utf-8");
        addHeadersToRequest(post, headers);
        addPropertiesToPost(post, props);
        return post;
    }

    private static HttpGet createGet(Uri uri, Properties headers) {
        HttpGet get = new HttpGet(uri.toString());
        addHeadersToRequest(get, headers);
        return get;
    }

    private static void addHeadersToRequest(AbstractHttpMessage request, Properties headers) {
        if (headers != null && !headers.isEmpty()) {
            Log.v(TAG, "Adding http headers: " + headers.toString());
            for (String name : headers.keySet()) {
                request.addHeader(name, headers.getProperty(name));
            }
        }
    }

    private static void addPropertiesToPost(HttpEntityEnclosingRequestBase request, Properties props) {
        if (props != null && !props.isEmpty()) {
            Log.v(TAG, "Posting properties: " + props.toString());
            UrlEncodedFormEntity entity = null;
            List<NameValuePair> nvp = new ArrayList<>();
            for (String key : props.keySet()) {
                nvp.add(new BasicNameValuePair(key, props.getProperty(key)));
            }
            try {
                entity = new UrlEncodedFormEntity(nvp, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                Log.d(TAG, "Failed to add properties to http post: " + e);
            }
            request.setEntity(entity);
        }
    }
}
