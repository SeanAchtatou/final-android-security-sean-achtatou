package com.roobit.restkit;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import com.roobit.restkit.RestClient;
import java.util.Properties;

public class RestClientRequestTask extends AsyncTask<Object, Void, Result> {
    private RestClientRequestDelegate delegate;

    public interface RestClientRequestDelegate {
        void requestCancelled();

        void requestFinished(Result result);

        void requestStarted();
    }

    public RestClientRequestTask(RestClientRequestDelegate delegate2) {
        this.delegate = delegate2;
    }

    /* access modifiers changed from: protected */
    public Result doInBackground(Object... args) {
        return RestClientRequest.synchronousExecute((RestClient.Operation) args[0], (Uri) args[1], (Properties) args[2]);
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        Log.d("RestClientRequest", "Cancelling request");
        try {
            this.delegate.requestCancelled();
        } catch (NullPointerException e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Result result) {
        Log.d("RestClientRequest", "Finishing request");
        try {
            this.delegate.requestFinished(result);
        } catch (NullPointerException e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        Log.d("RestClientRequest", "Starting request");
        try {
            this.delegate.requestStarted();
        } catch (NullPointerException e) {
        }
    }
}
