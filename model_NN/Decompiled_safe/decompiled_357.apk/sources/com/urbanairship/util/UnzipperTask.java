package com.urbanairship.util;

import android.os.AsyncTask;
import com.urbanairship.Logger;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class UnzipperTask extends AsyncTask<File, Integer, Exception> {
    private Delegate delegate;

    public static abstract class Delegate {
        public abstract void onFail(Exception exc);

        public abstract void onProgressUpdate(int i);

        public abstract void onSuccess();
    }

    /* access modifiers changed from: protected */
    public Exception doInBackground(File... fileArr) {
        if (fileArr.length != 2) {
            throw new IllegalArgumentException();
        }
        File file = fileArr[0];
        File file2 = fileArr[1];
        try {
            ZipFile zipFile = new ZipFile(file.getCanonicalPath());
            ArrayList list = Collections.list(zipFile.entries());
            Logger.verbose("Zip file: " + zipFile.getName() + " contains " + list.size() + " files");
            Iterator it = list.iterator();
            while (it.hasNext()) {
                ZipEntry zipEntry = (ZipEntry) it.next();
                if (zipEntry.getSize() != 0) {
                    String name = zipEntry.getName();
                    Logger.info("Unzipping file entry: " + name);
                    File canonicalFile = new File(file2, name).getCanonicalFile();
                    Logger.debug("Creating file " + canonicalFile);
                    canonicalFile.getParentFile().mkdirs();
                    canonicalFile.createNewFile();
                    InputStream inputStream = zipFile.getInputStream(zipEntry);
                    FileOutputStream fileOutputStream = new FileOutputStream(canonicalFile);
                    byte[] bArr = new byte[4096];
                    while (true) {
                        int read = inputStream.read(bArr);
                        if (read <= 0) {
                            break;
                        }
                        fileOutputStream.write(bArr, 0, read);
                    }
                    fileOutputStream.flush();
                    inputStream.close();
                    fileOutputStream.close();
                }
            }
            file.delete();
            return null;
        } catch (Exception e) {
            file.delete();
            return e;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Exception exc) {
        if (exc != null) {
            this.delegate.onFail(exc);
        } else {
            this.delegate.onSuccess();
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Integer... numArr) {
        this.delegate.onProgressUpdate(numArr[0].intValue());
    }

    public void setDelegate(Delegate delegate2) {
        this.delegate = delegate2;
    }
}
