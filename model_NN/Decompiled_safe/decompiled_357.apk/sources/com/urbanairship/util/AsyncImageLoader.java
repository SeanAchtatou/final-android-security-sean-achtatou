package com.urbanairship.util;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class AsyncImageLoader {
    private static Map<String, Drawable> drawableMap = new HashMap();
    /* access modifiers changed from: private */
    public Delegate delegate;

    public static abstract class Delegate {
        public abstract void imageLoaded(String str, Drawable drawable);
    }

    public AsyncImageLoader(String str, Delegate delegate2) {
        this.delegate = delegate2;
        fetchDrawableOnThread(str);
    }

    public static boolean contains(String str) {
        boolean containsKey;
        synchronized (AsyncImageLoader.class) {
            containsKey = drawableMap.containsKey(str);
        }
        return containsKey;
    }

    private InputStream fetch(String str) throws MalformedURLException, IOException, IllegalStateException {
        return new DefaultHttpClient().execute(new HttpGet(str)).getEntity().getContent();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.drawable.Drawable fetchDrawable(java.lang.String r5) {
        /*
            r4 = this;
            r3 = 0
            java.lang.Class<com.urbanairship.util.AsyncImageLoader> r0 = com.urbanairship.util.AsyncImageLoader.class
            monitor-enter(r0)
            java.util.Map<java.lang.String, android.graphics.drawable.Drawable> r1 = com.urbanairship.util.AsyncImageLoader.drawableMap     // Catch:{ all -> 0x0037 }
            boolean r1 = r1.containsKey(r5)     // Catch:{ all -> 0x0037 }
            if (r1 == 0) goto L_0x0017
            java.util.Map<java.lang.String, android.graphics.drawable.Drawable> r1 = com.urbanairship.util.AsyncImageLoader.drawableMap     // Catch:{ all -> 0x0037 }
            java.lang.Object r4 = r1.get(r5)     // Catch:{ all -> 0x0037 }
            android.graphics.drawable.Drawable r4 = (android.graphics.drawable.Drawable) r4     // Catch:{ all -> 0x0037 }
            monitor-exit(r0)     // Catch:{ all -> 0x0037 }
            r0 = r4
        L_0x0016:
            return r0
        L_0x0017:
            monitor-exit(r0)     // Catch:{ all -> 0x0037 }
            java.io.InputStream r0 = r4.fetch(r5)     // Catch:{ MalformedURLException -> 0x002f, IOException -> 0x003a, IllegalStateException -> 0x0042 }
            java.lang.String r1 = "Async Image"
            android.graphics.drawable.Drawable r0 = android.graphics.drawable.Drawable.createFromStream(r0, r1)     // Catch:{ MalformedURLException -> 0x002f, IOException -> 0x003a, IllegalStateException -> 0x0042 }
            java.lang.Class<com.urbanairship.util.AsyncImageLoader> r1 = com.urbanairship.util.AsyncImageLoader.class
            monitor-enter(r1)     // Catch:{ MalformedURLException -> 0x002f, IOException -> 0x003a, IllegalStateException -> 0x0042 }
            java.util.Map<java.lang.String, android.graphics.drawable.Drawable> r2 = com.urbanairship.util.AsyncImageLoader.drawableMap     // Catch:{ all -> 0x002c }
            r2.put(r5, r0)     // Catch:{ all -> 0x002c }
            monitor-exit(r1)     // Catch:{ all -> 0x002c }
            goto L_0x0016
        L_0x002c:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x002c }
            throw r0     // Catch:{ MalformedURLException -> 0x002f, IOException -> 0x003a, IllegalStateException -> 0x0042 }
        L_0x002f:
            r0 = move-exception
            java.lang.String r1 = "fetchDrawable failed"
            com.urbanairship.Logger.error(r1, r0)
            r0 = r3
            goto L_0x0016
        L_0x0037:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0037 }
            throw r1
        L_0x003a:
            r0 = move-exception
            java.lang.String r1 = "fetchDrawable failed"
            com.urbanairship.Logger.error(r1, r0)
            r0 = r3
            goto L_0x0016
        L_0x0042:
            r0 = move-exception
            java.lang.String r1 = "fetchDrawable failed"
            com.urbanairship.Logger.error(r1, r0)
            r0 = r3
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.urbanairship.util.AsyncImageLoader.fetchDrawable(java.lang.String):android.graphics.drawable.Drawable");
    }

    private void fetchDrawableOnThread(final String str) {
        final AnonymousClass1 r0 = new Handler() {
            public void handleMessage(Message message) {
                if (((Drawable) message.obj) != null) {
                    AsyncImageLoader.this.delegate.imageLoaded(str, (Drawable) message.obj);
                }
            }
        };
        new Thread() {
            public void run() {
                r0.sendMessage(r0.obtainMessage(1, AsyncImageLoader.this.fetchDrawable(str)));
            }
        }.start();
    }

    public static Drawable get(String str) {
        Drawable drawable;
        synchronized (AsyncImageLoader.class) {
            drawable = drawableMap.get(str);
        }
        return drawable;
    }
}
