package com.urbanairship;

import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.urbanairship.analytics.Analytics;
import com.urbanairship.iap.IAPManager;
import com.urbanairship.push.PushManager;

public class UAirship {
    private static final UAirship sharedAirship = new UAirship();
    private static final String version = "1.0.0";
    private AirshipConfigOptions airshipConfigOptions;
    private Analytics analytics;
    private Context applicationContext;
    private boolean flying = false;

    private UAirship() {
    }

    public static int getAppIcon() {
        return getAppInfo().icon;
    }

    public static ApplicationInfo getAppInfo() {
        try {
            return getPackageManager().getApplicationInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            Logger.info("NameNotFound for: " + getPackageName() + ". Disabling.");
            return null;
        }
    }

    public static String getAppName() {
        if (getAppInfo() != null) {
            return getPackageManager().getApplicationLabel(getAppInfo()).toString();
        }
        return null;
    }

    public static PackageInfo getPackageInfo() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            Logger.info("NameNotFound for: " + getPackageName() + ". Disabling.");
            return null;
        }
    }

    public static PackageManager getPackageManager() {
        return sharedAirship.applicationContext.getPackageManager();
    }

    public static String getPackageName() {
        return sharedAirship.applicationContext.getPackageName();
    }

    public static String getVersion() {
        return version;
    }

    public static void land() {
        sharedAirship.flying = false;
        if (sharedAirship.airshipConfigOptions.iapEnabled) {
            IAPManager.tearDown();
        }
    }

    public static UAirship shared() {
        return sharedAirship;
    }

    public static void takeOff(Application application) {
        takeOff(application, null);
    }

    public static void takeOff(Application application, AirshipConfigOptions airshipConfigOptions2) {
        if (application == null) {
            throw new IllegalArgumentException("Application argument must not be null");
        } else if (sharedAirship.flying) {
            Logger.error("You can only call UAirship.takeOff once.");
        } else {
            UAirship uAirship = sharedAirship;
            Context applicationContext2 = application.getApplicationContext();
            uAirship.applicationContext = applicationContext2;
            AirshipConfigOptions loadDefaultOptions = airshipConfigOptions2 == null ? AirshipConfigOptions.loadDefaultOptions(applicationContext2) : airshipConfigOptions2;
            sharedAirship.airshipConfigOptions = loadDefaultOptions;
            if (!loadDefaultOptions.inProduction) {
                Logger.logLevel = 3;
            } else {
                Logger.logLevel = 6;
            }
            Logger.TAG = getAppName() + " - UALib";
            Logger.debug("Airship Take Off! Lib Version: 1.0.0 / App key = " + loadDefaultOptions.getAppKey() + " / secret = " + loadDefaultOptions.getAppSecret());
            Logger.debug("In Production? " + loadDefaultOptions.inProduction);
            if (!loadDefaultOptions.isValid()) {
                throw new RuntimeException("Application configuration is invalid.");
            }
            InternalOptions.loadDefaultOptions(applicationContext2);
            sharedAirship.flying = true;
            if (loadDefaultOptions.pushServiceEnabled) {
                Logger.debug("Initializing Push.");
                PushManager.init();
            }
            if (loadDefaultOptions.iapEnabled) {
                Logger.debug("Initializing IAP.");
                IAPManager.init();
            }
            Logger.debug("Initializing Analytics.");
            sharedAirship.analytics = new Analytics();
        }
    }

    public AirshipConfigOptions getAirshipConfigOptions() {
        return this.airshipConfigOptions;
    }

    public Analytics getAnalytics() {
        return this.analytics;
    }

    public Context getApplicationContext() {
        return this.applicationContext;
    }

    public boolean isFlying() {
        return this.flying;
    }
}
