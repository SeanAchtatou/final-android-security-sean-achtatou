package com.urbanairship.analytics;

import com.urbanairship.Logger;
import com.urbanairship.analytics.EventDataManager;
import com.urbanairship.push.PushManager;
import org.json.JSONException;
import org.json.JSONObject;

public class PushServiceStartedEvent extends Event {
    static final String TYPE = "push_service_started";

    /* access modifiers changed from: package-private */
    public JSONObject getData() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(EventDataManager.Events.COLUMN_NAME_SESSION_ID, getEnvironment().getSessionId());
            jSONObject.put("apid", getEnvironment().getApid());
            jSONObject.put("transport", getEnvironment().getPushTransport());
            jSONObject.put("push_enabled", PushManager.shared().getPreferences().isPushEnabled());
        } catch (JSONException e) {
            Logger.error("Error constructing JSON data for " + getType());
        }
        return jSONObject;
    }

    /* access modifiers changed from: package-private */
    public String getType() {
        return TYPE;
    }
}
