package com.urbanairship.analytics;

import android.app.ListActivity;
import com.urbanairship.UAirship;

public class InstrumentedListActivity extends ListActivity {
    public void onStart() {
        super.onStart();
        UAirship.shared().getAnalytics().activityStarted(this);
    }

    public void onStop() {
        super.onStop();
        UAirship.shared().getAnalytics().activityStopped(this);
    }
}
