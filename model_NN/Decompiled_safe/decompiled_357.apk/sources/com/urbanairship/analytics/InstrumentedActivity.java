package com.urbanairship.analytics;

import android.app.Activity;
import com.urbanairship.UAirship;

public class InstrumentedActivity extends Activity {
    public void onStart() {
        super.onStart();
        UAirship.shared().getAnalytics().activityStarted(this);
    }

    public void onStop() {
        super.onStop();
        UAirship.shared().getAnalytics().activityStopped(this);
    }
}
