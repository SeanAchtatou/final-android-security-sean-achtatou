package com.urbanairship.analytics;

import com.urbanairship.Logger;
import com.urbanairship.analytics.Event;
import com.urbanairship.analytics.EventDataManager;
import org.json.JSONException;
import org.json.JSONObject;

class AppBackgroundEvent extends Event {
    static final String TYPE = "app_background";

    AppBackgroundEvent() {
    }

    /* access modifiers changed from: package-private */
    public JSONObject getData() {
        JSONObject jSONObject = new JSONObject();
        Event.Environment environment = getEnvironment();
        try {
            jSONObject.put(EventDataManager.Events.COLUMN_NAME_SESSION_ID, getEnvironment().getSessionId());
            jSONObject.put("connection_type", environment.getConnectionType());
            String connectionSubType = environment.getConnectionSubType();
            if (connectionSubType.length() > 0) {
                jSONObject.put("connection_subtype", connectionSubType);
            }
            jSONObject.put(EventDataManager.Events.COLUMN_NAME_SESSION_ID, environment.getSessionId());
            jSONObject.put("push_id", environment.getPushId());
        } catch (JSONException e) {
            Logger.error("Error constructing JSON data for " + getType());
        }
        return jSONObject;
    }

    /* access modifiers changed from: package-private */
    public String getType() {
        return TYPE;
    }
}
