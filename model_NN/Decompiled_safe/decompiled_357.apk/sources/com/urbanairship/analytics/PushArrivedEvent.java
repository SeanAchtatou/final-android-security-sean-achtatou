package com.urbanairship.analytics;

import com.urbanairship.Logger;
import com.urbanairship.analytics.EventDataManager;
import org.json.JSONException;
import org.json.JSONObject;

public class PushArrivedEvent extends Event {
    static final String TYPE = "push_arrived";
    private String pushId;

    public PushArrivedEvent(String str) {
        this.pushId = str;
    }

    /* access modifiers changed from: package-private */
    public JSONObject getData() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(EventDataManager.Events.COLUMN_NAME_SESSION_ID, getEnvironment().getSessionId());
            jSONObject.put("push_id", this.pushId);
        } catch (JSONException e) {
            Logger.error("Error constructing JSON data for " + getType());
        }
        return jSONObject;
    }

    /* access modifiers changed from: package-private */
    public String getType() {
        return TYPE;
    }
}
