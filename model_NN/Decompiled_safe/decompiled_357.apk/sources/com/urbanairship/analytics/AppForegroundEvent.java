package com.urbanairship.analytics;

import com.urbanairship.Logger;
import com.urbanairship.analytics.Event;
import com.urbanairship.analytics.EventDataManager;
import java.util.Collection;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class AppForegroundEvent extends Event {
    static final String TYPE = "app_foreground";

    AppForegroundEvent() {
    }

    /* access modifiers changed from: package-private */
    public JSONObject getData() {
        JSONObject jSONObject = new JSONObject();
        Event.Environment environment = getEnvironment();
        try {
            jSONObject.put("connection_type", environment.getConnectionType());
            String connectionSubType = environment.getConnectionSubType();
            if (connectionSubType.length() > 0) {
                jSONObject.put("connection_subtype", connectionSubType);
            }
            jSONObject.put("carrier", environment.getCarrier());
            jSONObject.put("time_zone", environment.getTimezone());
            jSONObject.put("daylight_savings", environment.isDaylightSavingsTime());
            jSONObject.put("notification_types", new JSONArray((Collection) environment.getNotificationTypes()));
            jSONObject.put("os_version", environment.getOsVersion());
            jSONObject.put("lib_version", environment.getLibVersion());
            jSONObject.put("package_version", environment.getPackageVersion());
            jSONObject.put(EventDataManager.Events.COLUMN_NAME_SESSION_ID, environment.getSessionId());
            jSONObject.put("push_id", environment.getPushId());
        } catch (JSONException e) {
            Logger.error("Error constructing JSON data for " + getType());
        }
        return jSONObject;
    }

    /* access modifiers changed from: package-private */
    public String getType() {
        return TYPE;
    }
}
