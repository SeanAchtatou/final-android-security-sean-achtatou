package com.urbanairship.analytics;

import android.os.Build;
import com.urbanairship.AirshipConfigOptions;
import com.urbanairship.Logger;
import com.urbanairship.Preferences;
import com.urbanairship.UAirship;
import com.urbanairship.push.PushManager;
import com.urbanairship.restclient.Request;
import com.urbanairship.restclient.Response;
import java.util.Collections;
import java.util.Map;
import org.apache.http.Header;

class EventUploadManager implements Runnable {
    public static final int DEFAULT_BATCH_SIZE = 102400;
    public static final String LAST_SEND_KEY = "LAST_SEND";
    public static final int MAX_BATCH_SIZE = 512000;
    public static final String MAX_BATCH_SIZE_KEY = "MAX_BATCH_SIZE";
    public static final int MAX_TOTAL_DB_SIZE = 5120000;
    public static final String MAX_TOTAL_DB_SIZE_KEY = "MAX_TOTAL_DB_SIZE";
    public static final String MAX_WAIT_KEY = "MAX_WAIT";
    public static final int MAX_WAIT_MS = 604800000;
    public static final String MIN_BATCH_INTERVAL_KEY = "MIN_BATCH_INTERVAL";
    public static final int MIN_BATCH_INTERVAL_MS = 60000;
    private int batchSize;
    private Thread currentThread;
    private EventDataManager dataManager;
    private long lastSendTime;
    private int maxBatchSize;
    private int maxTotalDBSize;
    private int maxWait;
    private int minBatchInterval;
    private Preferences prefs = new Preferences("com.urbanairship.analytics");
    private volatile boolean running;
    private boolean useGzip;

    public EventUploadManager(EventDataManager eventDataManager) {
        loadPrefs();
        this.batchSize = DEFAULT_BATCH_SIZE;
        this.dataManager = eventDataManager;
        this.useGzip = true;
    }

    private void loadPrefs() {
        this.maxTotalDBSize = this.prefs.getInt(MAX_TOTAL_DB_SIZE_KEY, MAX_TOTAL_DB_SIZE);
        this.maxBatchSize = this.prefs.getInt(MAX_BATCH_SIZE_KEY, MAX_BATCH_SIZE);
        this.maxWait = this.prefs.getInt(MAX_WAIT_KEY, MAX_WAIT_MS);
        this.minBatchInterval = this.prefs.getInt(MIN_BATCH_INTERVAL_KEY, MIN_BATCH_INTERVAL_MS);
        this.lastSendTime = this.prefs.getLong(LAST_SEND_KEY, System.currentTimeMillis());
    }

    private void saveHeaders(Response response) {
        Header firstHeader = response.getFirstHeader("X-UA-Max-Total");
        Header firstHeader2 = response.getFirstHeader("X-UA-Max-Batch");
        Header firstHeader3 = response.getFirstHeader("X-UA-Max-Wait");
        Header firstHeader4 = response.getFirstHeader("X-UA-Min-Batch-Interval");
        if (firstHeader != null) {
            int min = Math.min(Integer.parseInt(firstHeader.getValue()) * 1024, (int) MAX_TOTAL_DB_SIZE);
            if (min <= 0) {
                min = 5120000;
            }
            this.maxTotalDBSize = min;
        }
        if (firstHeader2 != null) {
            int min2 = Math.min(Integer.parseInt(firstHeader2.getValue()) * 1024, (int) MAX_BATCH_SIZE);
            if (min2 <= 0) {
                min2 = 512000;
            }
            this.maxBatchSize = min2;
        }
        if (firstHeader3 != null) {
            int min3 = Math.min(Integer.parseInt(firstHeader3.getValue()), (int) MAX_WAIT_MS);
            if (min3 <= 0) {
                min3 = 604800000;
            }
            this.maxWait = min3;
        }
        if (firstHeader4 != null) {
            int max = Math.max(Integer.parseInt(firstHeader4.getValue()), (int) MIN_BATCH_INTERVAL_MS);
            if (max <= 0) {
                max = 60000;
            }
            this.minBatchInterval = max;
        }
        savePrefs();
    }

    private void savePrefs() {
        this.prefs.putInt(MAX_TOTAL_DB_SIZE_KEY, this.maxTotalDBSize);
        this.prefs.putInt(MAX_BATCH_SIZE_KEY, this.maxBatchSize);
        this.prefs.putInt(MAX_WAIT_KEY, this.maxWait);
        this.prefs.putInt(MIN_BATCH_INTERVAL_KEY, this.minBatchInterval);
        this.prefs.putLong(LAST_SEND_KEY, this.lastSendTime);
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x00f3  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x012b  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x015d  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0163  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean send(java.util.Collection<java.lang.String> r9) {
        /*
            r8 = this;
            r7 = 0
            if (r9 != 0) goto L_0x000a
            java.lang.String r0 = "Send failed. No events."
            com.urbanairship.Logger.error(r0)
            r0 = r7
        L_0x0009:
            return r0
        L_0x000a:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Sending "
            java.lang.StringBuilder r0 = r0.append(r1)
            int r1 = r9.size()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = " events."
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.urbanairship.Logger.info(r0)
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream
            r1.<init>()
            java.lang.String r0 = "["
            byte[] r0 = r0.getBytes()     // Catch:{ IOException -> 0x005f }
            r1.write(r0)     // Catch:{ IOException -> 0x005f }
            java.util.Iterator r2 = r9.iterator()     // Catch:{ IOException -> 0x005f }
        L_0x003c:
            boolean r0 = r2.hasNext()     // Catch:{ IOException -> 0x005f }
            if (r0 == 0) goto L_0x0067
            java.lang.Object r0 = r2.next()     // Catch:{ IOException -> 0x005f }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IOException -> 0x005f }
            byte[] r0 = r0.getBytes()     // Catch:{ IOException -> 0x005f }
            r1.write(r0)     // Catch:{ IOException -> 0x005f }
            boolean r0 = r2.hasNext()     // Catch:{ IOException -> 0x005f }
            if (r0 == 0) goto L_0x003c
            java.lang.String r0 = ","
            byte[] r0 = r0.getBytes()     // Catch:{ IOException -> 0x005f }
            r1.write(r0)     // Catch:{ IOException -> 0x005f }
            goto L_0x003c
        L_0x005f:
            r0 = move-exception
            java.lang.String r0 = "Unable to create raw JSON payload."
            com.urbanairship.Logger.error(r0)
            r0 = r7
            goto L_0x0009
        L_0x0067:
            java.lang.String r0 = "]"
            byte[] r0 = r0.getBytes()     // Catch:{ IOException -> 0x005f }
            r1.write(r0)     // Catch:{ IOException -> 0x005f }
            byte[] r0 = r1.toByteArray()
            com.urbanairship.restclient.Request r1 = new com.urbanairship.restclient.Request
            java.lang.String r2 = "POST"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            com.urbanairship.UAirship r4 = com.urbanairship.UAirship.shared()
            com.urbanairship.AirshipConfigOptions r4 = r4.getAirshipConfigOptions()
            java.lang.String r4 = r4.analyticsServer
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = "warp9/"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.<init>(r2, r3)
            r2 = 0
            boolean r3 = r8.useGzip
            if (r3 == 0) goto L_0x00f1
            int r3 = r0.length     // Catch:{ IOException -> 0x0153 }
            int r3 = r3 / 4
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0153 }
            r4.<init>(r3)     // Catch:{ IOException -> 0x0153 }
            java.util.zip.GZIPOutputStream r5 = new java.util.zip.GZIPOutputStream     // Catch:{ IOException -> 0x0153 }
            r5.<init>(r4)     // Catch:{ IOException -> 0x0153 }
            r5.write(r0)     // Catch:{ IOException -> 0x0153 }
            r5.close()     // Catch:{ IOException -> 0x0153 }
            byte[] r4 = r4.toByteArray()     // Catch:{ IOException -> 0x0153 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0153 }
            r5.<init>()     // Catch:{ IOException -> 0x0153 }
            java.lang.String r6 = "GZIP'd: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0153 }
            int r6 = r0.length     // Catch:{ IOException -> 0x0153 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0153 }
            java.lang.String r6 = " into "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0153 }
            int r6 = r4.length     // Catch:{ IOException -> 0x0153 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0153 }
            java.lang.String r6 = " (expected "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0153 }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ IOException -> 0x0153 }
            java.lang.String r5 = ")"
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ IOException -> 0x0153 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x0153 }
            com.urbanairship.Logger.verbose(r3)     // Catch:{ IOException -> 0x0153 }
            org.apache.http.entity.ByteArrayEntity r3 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ IOException -> 0x0153 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x0153 }
            java.lang.String r2 = "gzip"
            r3.setContentEncoding(r2)     // Catch:{ IOException -> 0x0160 }
            r2 = r3
        L_0x00f1:
            if (r2 != 0) goto L_0x0163
            org.apache.http.entity.ByteArrayEntity r2 = new org.apache.http.entity.ByteArrayEntity
            r2.<init>(r0)
            r1.setEntity(r2)
            r0 = r2
        L_0x00fc:
            java.lang.String r2 = "application/json"
            r0.setContentType(r2)
            r1.setEntity(r0)
            r8.setHeaders(r1)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "Sending Analytics to: "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.net.URI r2 = r1.getURI()
            java.lang.String r2 = r2.toASCIIString()
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.urbanairship.Logger.debug(r0)
            com.urbanairship.restclient.Response r0 = r1.execute()
            if (r0 == 0) goto L_0x015d
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Warp 9 response: "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r0.status()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.urbanairship.Logger.debug(r1)
            r8.saveHeaders(r0)
            int r0 = r0.status()
            r1 = 200(0xc8, float:2.8E-43)
            if (r0 != r1) goto L_0x015a
            r0 = 1
            goto L_0x0009
        L_0x0153:
            r3 = move-exception
        L_0x0154:
            java.lang.String r3 = "GZIP of analytics payload failed."
            com.urbanairship.Logger.error(r3)
            goto L_0x00f1
        L_0x015a:
            r0 = r7
            goto L_0x0009
        L_0x015d:
            r0 = r7
            goto L_0x0009
        L_0x0160:
            r2 = move-exception
            r2 = r3
            goto L_0x0154
        L_0x0163:
            r0 = r2
            goto L_0x00fc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.urbanairship.analytics.EventUploadManager.send(java.util.Collection):boolean");
    }

    private void setHeaders(Request request) {
        request.setHeader("X-UA-Device-Family", "android");
        request.setHeader("X-UA-Sent-At", Double.toString(((double) System.currentTimeMillis()) / 1000.0d));
        request.setHeader("X-UA-Package-Name", UAirship.getPackageName());
        request.setHeader("X-UA-Package-Version", UAirship.getPackageInfo().versionName);
        String hashedDeviceId = Analytics.getHashedDeviceId();
        if (hashedDeviceId.length() > 0) {
            request.setHeader("X-UA-Device-ID", hashedDeviceId);
        }
        AirshipConfigOptions airshipConfigOptions = UAirship.shared().getAirshipConfigOptions();
        request.setHeader("X-UA-App-Key", airshipConfigOptions.getAppKey());
        request.setHeader("X-UA-In-Production", Boolean.toString(airshipConfigOptions.inProduction));
        request.setHeader("X-UA-Device-Model", Build.MODEL);
        request.setHeader("X-UA-OS-Version", Build.VERSION.RELEASE);
        String pushId = PushManager.shared().getPreferences().getPushId();
        if (pushId != null && pushId.length() > 0) {
            request.setHeader("X-UA-Apid", pushId);
        }
    }

    public int getMaxTotalDBSize() {
        return this.maxTotalDBSize;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public void run() {
        this.running = true;
        while (this.running) {
            long max = Math.max(0L, (this.lastSendTime + ((long) this.minBatchInterval)) - System.currentTimeMillis());
            if (max > 0) {
                try {
                    Thread.sleep(max);
                } catch (InterruptedException e) {
                    Logger.info("InterruptedException in EventUploadManager. Bailing!");
                    this.running = false;
                    return;
                }
            }
            if (this.running) {
                this.lastSendTime = System.currentTimeMillis();
                this.prefs.putLong(LAST_SEND_KEY, this.lastSendTime);
                int eventCount = this.dataManager.getEventCount();
                if (eventCount <= 0) {
                    this.running = false;
                    Logger.info("Exiting analytics upload thread.");
                    return;
                }
                Map<Long, String> oldestEvents = this.dataManager.getOldestEvents(this.batchSize / (this.dataManager.getDatabaseSize() / eventCount));
                if (send(oldestEvents.values())) {
                    this.dataManager.deleteEventsOlderThan(((Long) Collections.max(oldestEvents.keySet())).longValue());
                }
            } else {
                return;
            }
        }
    }

    public void startUploadingIfNecessary() {
        if (!this.running) {
            Logger.verbose("EventUploadManager - starting upload thread");
            this.currentThread = new Thread(this);
            this.currentThread.start();
        }
    }

    public void stopUpload() {
        this.running = false;
    }
}
