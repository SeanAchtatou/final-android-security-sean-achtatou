package com.urbanairship.analytics;

import android.app.Activity;
import com.urbanairship.Logger;
import com.urbanairship.analytics.Event;
import com.urbanairship.analytics.EventDataManager;
import org.json.JSONException;
import org.json.JSONObject;

class ActivityStoppedEvent extends Event {
    static final String TYPE = "activity_stopped";
    private String className;

    ActivityStoppedEvent(Activity activity) {
        this.className = activity.getComponentName().getClassName();
    }

    /* access modifiers changed from: package-private */
    public JSONObject getData() {
        JSONObject jSONObject = new JSONObject();
        Event.Environment environment = getEnvironment();
        try {
            jSONObject.put("class_name", this.className);
            jSONObject.put(EventDataManager.Events.COLUMN_NAME_SESSION_ID, environment.getSessionId());
        } catch (JSONException e) {
            Logger.error("Error constructing JSON data for " + getType());
        }
        return jSONObject;
    }

    /* access modifiers changed from: package-private */
    public String getType() {
        return TYPE;
    }
}
