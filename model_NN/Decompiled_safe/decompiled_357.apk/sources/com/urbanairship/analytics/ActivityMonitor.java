package com.urbanairship.analytics;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import java.util.HashSet;

class ActivityMonitor {
    private static final int BACKGROUND_DELAY_MS = 2000;
    /* access modifiers changed from: private */
    public HashSet<Integer> activities = new HashSet<>();
    /* access modifiers changed from: private */
    public Delegate delegate;

    public static abstract class Delegate {
        public abstract void onBackground();

        public abstract void onForeground();
    }

    public ActivityMonitor(Delegate delegate2) {
        this.delegate = delegate2;
    }

    public void addActivity(Activity activity) {
        if (this.activities.isEmpty()) {
            this.delegate.onForeground();
        }
        this.activities.add(Integer.valueOf(activity.hashCode()));
    }

    public void removeActivity(final Activity activity) {
        final AnonymousClass1 r0 = new Handler() {
            public void handleMessage(Message message) {
                ActivityMonitor.this.activities.remove(Integer.valueOf(activity.hashCode()));
                if (ActivityMonitor.this.activities.isEmpty()) {
                    ActivityMonitor.this.delegate.onBackground();
                }
            }
        };
        new Thread() {
            public void run() {
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                }
                r0.sendMessage(r0.obtainMessage());
            }
        }.start();
    }
}
