package com.urbanairship.analytics;

import android.app.Activity;
import android.content.Intent;
import android.provider.Settings;
import com.urbanairship.CoreReceiver;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.analytics.ActivityMonitor;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class Analytics {
    public static final String ACTION_ANALYTICS_START = "com.urbanairship.analytics.START";
    private ActivityMonitor activityMonitor = new ActivityMonitor(new ActivityMonitor.Delegate() {
        public void onBackground() {
            boolean unused = Analytics.this.fromBackground = true;
            Analytics.this.addEvent(new AppBackgroundEvent());
            String unused2 = Analytics.this.conversionPushId = null;
        }

        public void onForeground() {
            if (Analytics.this.fromBackground) {
                Session unused = Analytics.this.session = new Session();
                boolean unused2 = Analytics.this.fromBackground = false;
            }
            Analytics.this.addEvent(new AppForegroundEvent());
        }
    });
    /* access modifiers changed from: private */
    public String conversionPushId;
    private EventDataManager dataManager = new EventDataManager();
    /* access modifiers changed from: private */
    public boolean fromBackground = false;
    private final String server = UAirship.shared().getAirshipConfigOptions().analyticsServer;
    /* access modifiers changed from: private */
    public Session session = new Session();
    private EventUploadManager uploadManager = new EventUploadManager(this.dataManager);

    class Session {
        private String id = UUID.randomUUID().toString();

        public Session() {
            Logger.verbose("New session: " + this.id);
        }

        public String getId() {
            return this.id;
        }

        public void log() {
            Logger.verbose("Analytics Session - id: " + this.id);
        }
    }

    static String getHashedDeviceId() {
        String string = Settings.Secure.getString(UAirship.shared().getApplicationContext().getContentResolver(), "android_id");
        if (string == null) {
            return "unavailable";
        }
        byte[] bytes = string.getBytes();
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.update(bytes, 0, bytes.length);
            byte[] digest = instance.digest();
            StringBuilder sb = new StringBuilder();
            int length = digest.length;
            for (int i = 0; i < length; i++) {
                sb.append(String.format("%02x", Byte.valueOf(digest[i])));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            Logger.error("Unable to hash the device ID: SHA1 digester not present");
            return "unavailable";
        }
    }

    public void activityStarted(Activity activity) {
        this.activityMonitor.addActivity(activity);
        addEvent(new ActivityStartedEvent(activity));
    }

    public void activityStopped(Activity activity) {
        addEvent(new ActivityStoppedEvent(activity));
        this.activityMonitor.removeActivity(activity);
    }

    public void addEvent(Event event) {
        String str = UAirship.shared().getAirshipConfigOptions().analyticsServer;
        if (str != null && str.length() != 0 && UAirship.shared().getAirshipConfigOptions().analyticsEnabled) {
            event.log();
            int databaseSize = this.dataManager.getDatabaseSize();
            if (databaseSize > this.uploadManager.getMaxTotalDBSize()) {
                Logger.info("DB size exceeded. Deleting non-critical events.");
                if ("activity_started".equals(event.getType()) || "activity_stopped".equals(event.getType())) {
                    Logger.info("Database full. Not logging activity start/stop events");
                    return;
                }
                Logger.info("Deleting activity start/stop events.");
                this.dataManager.deleteEventType("activity_started");
                this.dataManager.deleteEventType("activity_stopped");
                databaseSize = this.dataManager.getDatabaseSize();
            }
            if (databaseSize > this.uploadManager.getMaxTotalDBSize()) {
                Logger.info("Deleting oldest session.");
                String oldestSessionId = this.dataManager.getOldestSessionId();
                if (oldestSessionId != null && oldestSessionId.length() > 0) {
                    this.dataManager.deleteSession(oldestSessionId);
                }
            }
            this.dataManager.insertEvent(event);
            Intent intent = new Intent(ACTION_ANALYTICS_START);
            intent.setClass(UAirship.shared().getApplicationContext(), CoreReceiver.class);
            UAirship.shared().getApplicationContext().sendBroadcast(intent);
        }
    }

    public String getConversionPushId() {
        return this.conversionPushId;
    }

    /* access modifiers changed from: package-private */
    public String getServer() {
        return this.server;
    }

    /* access modifiers changed from: package-private */
    public Session getSession() {
        return this.session;
    }

    public void setConversionPushId(String str) {
        this.conversionPushId = str;
    }

    public void startUploadingIfNecessary() {
        this.uploadManager.startUploadingIfNecessary();
    }
}
