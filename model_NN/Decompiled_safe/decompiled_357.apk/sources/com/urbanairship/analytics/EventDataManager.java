package com.urbanairship.analytics;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

class EventDataManager {
    private static final String DATABASE_NAME = "ua_analytics.db";
    private static final int DATABASE_VERSION = 1;
    private SQLiteDatabase db;

    static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, EventDataManager.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 1);
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS events (_id INTEGER PRIMARY KEY AUTOINCREMENT,type TEXT,event_id TEXT,time INTEGER,data TEXT,session_id TEXT,event_size INTEGER);");
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            Logger.info("Upgrading anlytics database from version " + i + " to " + i2 + ", which will destroy all old data");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS events");
            onCreate(sQLiteDatabase);
        }
    }

    public static final class Events implements BaseColumns {
        public static final String COLUMN_NAME_DATA = "data";
        public static final String COLUMN_NAME_EVENT_ID = "event_id";
        public static final String COLUMN_NAME_EVENT_SIZE = "event_size";
        public static final String COLUMN_NAME_SESSION_ID = "session_id";
        public static final String COLUMN_NAME_TIME = "time";
        public static final String COLUMN_NAME_TYPE = "type";
        public static final String DEFAULT_SORT_ORDER = "_id ASC";
        public static final String TABLE_NAME = "events";

        private Events() {
        }
    }

    public EventDataManager() {
        try {
            this.db = new DatabaseHelper(UAirship.shared().getApplicationContext()).getWritableDatabase();
            if (this.db == null) {
                Logger.error("Unable to create or open the analytics database.");
            }
        } catch (SQLiteException e) {
            Logger.error("Unable to open Analytics Event DB");
        }
    }

    public void close() {
        if (this.db != null && this.db.isOpen()) {
            this.db.close();
        }
    }

    public void deleteEvent(String str) {
        if (this.db == null) {
            Logger.error("Unable to delete event. Database not open.");
            return;
        }
        this.db.delete(Events.TABLE_NAME, "event_id = ?", new String[]{str});
    }

    public void deleteEventType(String str) {
        if (this.db == null) {
            Logger.error("Unable to delete events. Database not open.");
            return;
        }
        Logger.info("Deleted " + this.db.delete(Events.TABLE_NAME, "type = ?", new String[]{str}) + " rows with event type = " + str);
    }

    public void deleteEvents(Set<String> set) {
        if (this.db == null) {
            Logger.error("Unable to delete events. Database not open.");
            return;
        }
        this.db.beginTransaction();
        for (String str : set) {
            this.db.delete(Events.TABLE_NAME, "event_id = ?", new String[]{str});
        }
        this.db.endTransaction();
    }

    public void deleteEventsOlderThan(long j) {
        Logger.info("Deleting old events");
        if (this.db == null) {
            Logger.error("Unable to delete events. Database not open.");
            return;
        }
        this.db.delete(Events.TABLE_NAME, "_id <= ?", new String[]{Long.toString(j)});
    }

    public void deleteSession(String str) {
        if (this.db == null) {
            Logger.error("Unable to delete session. Database not open.");
            return;
        }
        Logger.info("Deleted " + this.db.delete(Events.TABLE_NAME, "session_id = ?", new String[]{str}) + " rows with session id " + str);
    }

    public SQLiteDatabase getDatabase() {
        return this.db;
    }

    public int getDatabaseSize() {
        if (this.db == null) {
            Logger.error("Unable to get DB size. Database not open.");
            return -1;
        }
        Cursor query = this.db.query(Events.TABLE_NAME, new String[]{"SUM(event_size) as _size"}, null, null, null, null, null);
        Integer valueOf = query.moveToFirst() ? Integer.valueOf(query.getInt(0)) : null;
        query.close();
        if (valueOf != null) {
            return valueOf.intValue();
        }
        return -1;
    }

    public int getEventCount() {
        if (this.db == null) {
            Logger.error("Unable to get event count. Database not open.");
            return -1;
        }
        Cursor query = this.db.query(Events.TABLE_NAME, new String[]{"COUNT(*) as _cnt"}, null, null, null, null, null);
        Integer valueOf = query.moveToFirst() ? Integer.valueOf(query.getInt(0)) : null;
        query.close();
        if (valueOf != null) {
            return valueOf.intValue();
        }
        return -1;
    }

    public Map<String, String> getEvents(int i) {
        if (this.db == null) {
            Logger.error("Unable to get events. Database not open.");
            return null;
        }
        Cursor query = this.db.query(Events.TABLE_NAME, new String[]{Events.COLUMN_NAME_EVENT_ID, Events.COLUMN_NAME_DATA}, null, null, null, null, Events.DEFAULT_SORT_ORDER, "0, " + i);
        HashMap hashMap = new HashMap(i);
        query.moveToFirst();
        while (!query.isAfterLast()) {
            hashMap.put(query.getString(0), query.getString(1));
            query.moveToNext();
        }
        query.close();
        return hashMap;
    }

    public Map<Long, String> getOldestEvents(int i) {
        if (this.db == null) {
            Logger.error("Unable to get events. Database not open.");
            return null;
        }
        Cursor query = this.db.query(Events.TABLE_NAME, new String[]{"_id", Events.COLUMN_NAME_DATA}, null, null, null, null, Events.DEFAULT_SORT_ORDER, "0, " + i);
        HashMap hashMap = new HashMap(i);
        query.moveToFirst();
        while (!query.isAfterLast()) {
            hashMap.put(Long.valueOf(query.getLong(0)), query.getString(1));
            query.moveToNext();
        }
        query.close();
        return hashMap;
    }

    public String getOldestSessionId() {
        if (this.db == null) {
            Logger.error("Unable to get session ID. Database not open.");
            return null;
        }
        Cursor query = this.db.query(Events.TABLE_NAME, new String[]{Events.COLUMN_NAME_SESSION_ID}, null, null, null, null, Events.DEFAULT_SORT_ORDER, "0, 1");
        String string = query.moveToFirst() ? query.getString(0) : null;
        query.close();
        return string;
    }

    public int insertEvent(Event event) {
        if (this.db == null) {
            Logger.error("Unable to insert event. Database not open.");
            return -1;
        }
        ContentValues contentValues = new ContentValues();
        int length = event.jsonRepresentation().toString().length();
        contentValues.put(Events.COLUMN_NAME_TYPE, event.getType());
        contentValues.put(Events.COLUMN_NAME_EVENT_ID, event.getEventId());
        contentValues.put(Events.COLUMN_NAME_DATA, event.jsonRepresentation().toString());
        contentValues.put(Events.COLUMN_NAME_TIME, event.getTime());
        contentValues.put(Events.COLUMN_NAME_SESSION_ID, event.getSessionId());
        contentValues.put(Events.COLUMN_NAME_EVENT_SIZE, Integer.valueOf(length));
        if (this.db.insert(Events.TABLE_NAME, null, contentValues) >= 0) {
            return length;
        }
        Logger.error("Error inserting event into Analytics DB.");
        return -1;
    }
}
