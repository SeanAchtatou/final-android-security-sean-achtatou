package com.urbanairship.restclient;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import com.urbanairship.Logger;
import com.urbanairship.restclient.ssl.PermissiveSSLSocketFactory;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.codehaus.jackson.org.objectweb.asm.Opcodes;

public class Request extends HttpEntityEnclosingRequestBase {
    private static final int BUFFER_SIZE = 8192;
    public static boolean verifySSLHostnames = true;
    File destination;
    DefaultHttpClient httpClient;
    String method;

    private class BackgroundRequest extends AsyncTask<Request, Integer, Response> {
        private AsyncHandler handler;

        public BackgroundRequest(AsyncHandler asyncHandler) {
            this.handler = asyncHandler;
        }

        /* access modifiers changed from: protected */
        public Response doInBackground(Request... requestArr) {
            int i = 0;
            if (requestArr.length > 1) {
                throw new RuntimeException("Background Request only handles executing one Request at a time ");
            } else if (0 >= requestArr.length) {
                return null;
            } else {
                try {
                    Response execute = requestArr[0].execute();
                    if (Request.this.destination == null || execute == null) {
                        return execute;
                    }
                    Request.this.destination.getParentFile().mkdirs();
                    long length = execute.length();
                    InputStream rawBody = execute.rawBody();
                    FileOutputStream fileOutputStream = new FileOutputStream(Request.this.destination);
                    byte[] bArr = new byte[8192];
                    while (true) {
                        int read = rawBody.read(bArr);
                        if (read != -1) {
                            i += read;
                            fileOutputStream.write(bArr, 0, read);
                            publishProgress(Integer.valueOf((int) ((((float) i) / ((float) length)) * 100.0f)));
                        } else {
                            fileOutputStream.flush();
                            rawBody.close();
                            fileOutputStream.close();
                            return execute;
                        }
                    }
                } catch (Exception e) {
                    return null;
                }
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Response response) {
            if (response != null) {
                this.handler.onComplete(response);
            } else {
                this.handler.onError(new Exception("Error when executing request."));
            }
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Integer... numArr) {
            this.handler.onProgress(numArr[0].intValue());
        }
    }

    public Request(String str, String str2) {
        this.method = str;
        setURI(URI.create(str2));
        this.params = new BasicHttpParams();
        if (verifySSLHostnames) {
            this.httpClient = new DefaultHttpClient(this.params);
        } else {
            Logger.error("Verify SSL Cert: false");
            SchemeRegistry schemeRegistry = new SchemeRegistry();
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            schemeRegistry.register(new Scheme("https", new PermissiveSSLSocketFactory(), 443));
            this.httpClient = new DefaultHttpClient(new SingleClientConnManager(this.params, schemeRegistry), this.params);
        }
        HttpConnectionParams.setSocketBufferSize(this.params, Opcodes.ACC_ENUM);
        setTimeout(30000);
        Logger.verbose("Set Timeout: " + HttpConnectionParams.getConnectionTimeout(this.httpClient.getParams()));
        Logger.verbose("Set Socket Buffer Size: " + HttpConnectionParams.getSocketBufferSize(this.httpClient.getParams()));
    }

    public Response execute() {
        try {
            return new Response(this.httpClient.execute(this));
        } catch (IOException e) {
            Logger.error("Error when executing request: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public void executeAsync(final AsyncHandler asyncHandler) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                new BackgroundRequest(asyncHandler).execute(this);
            }
        });
    }

    public String getMethod() {
        return this.method;
    }

    public void setAuth(String str, String str2) {
        UsernamePasswordCredentials usernamePasswordCredentials = new UsernamePasswordCredentials(str, str2);
        BasicCredentialsProvider basicCredentialsProvider = new BasicCredentialsProvider();
        basicCredentialsProvider.setCredentials(AuthScope.ANY, usernamePasswordCredentials);
        this.httpClient.setCredentialsProvider(basicCredentialsProvider);
    }

    public void setDestination(File file) {
        this.destination = file;
    }

    public void setSocketBufferSize(int i) {
        HttpConnectionParams.setSocketBufferSize(this.params, i);
    }

    public void setTimeout(int i) {
        HttpConnectionParams.setConnectionTimeout(this.params, i);
    }
}
