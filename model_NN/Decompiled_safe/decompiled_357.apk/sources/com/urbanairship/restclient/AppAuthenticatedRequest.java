package com.urbanairship.restclient;

import com.urbanairship.AirshipConfigOptions;
import com.urbanairship.UAirship;

public class AppAuthenticatedRequest extends Request {
    public AppAuthenticatedRequest(String str, String str2) {
        super(str, str2);
        AirshipConfigOptions airshipConfigOptions = UAirship.shared().getAirshipConfigOptions();
        String appKey = airshipConfigOptions.getAppKey();
        String appSecret = airshipConfigOptions.getAppSecret();
        if (!(appKey == null || appSecret == null)) {
            setAuth(appKey, appSecret);
        }
        setTimeout(EventUploadManager.MIN_BATCH_INTERVAL_MS);
    }
}
