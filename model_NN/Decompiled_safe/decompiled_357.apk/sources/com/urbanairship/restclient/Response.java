package com.urbanairship.restclient;

import com.urbanairship.Logger;
import java.io.IOException;
import java.io.InputStream;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

public class Response {
    HttpResponse resp;

    public Response(HttpResponse httpResponse) {
        this.resp = httpResponse;
    }

    public String body() {
        if (this.resp.getEntity() == null) {
            return "";
        }
        try {
            return EntityUtils.toString(this.resp.getEntity());
        } catch (Exception e) {
            Logger.error("Error fetching http entity");
            return "";
        }
    }

    public String contentType() {
        Header firstHeader = this.resp.getFirstHeader("Content-Type");
        if (firstHeader != null) {
            return firstHeader.getValue();
        }
        return null;
    }

    public Header getFirstHeader(String str) {
        return this.resp.getFirstHeader(str);
    }

    public long length() {
        return this.resp.getEntity().getContentLength();
    }

    public InputStream rawBody() throws IllegalStateException, IOException {
        return this.resp.getEntity().getContent();
    }

    public String reason() {
        return this.resp.getStatusLine().getReasonPhrase();
    }

    public int status() {
        return this.resp.getStatusLine().getStatusCode();
    }
}
