package com.urbanairship.restclient;

import java.io.IOException;
import java.io.InputStream;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public class PostInputStream extends HttpPost {
    protected DefaultHttpClient httpclient = new DefaultHttpClient();

    public PostInputStream(String str, InputStream inputStream, Long l) {
        super(str);
        setEntity(new InputStreamEntity(inputStream, l.longValue()));
    }

    public Response execute() throws IOException {
        return new Response(this.httpclient.execute(this));
    }
}
