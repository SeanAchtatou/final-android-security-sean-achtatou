package com.urbanairship.iap;

import com.urbanairship.Logger;
import java.io.File;
import java.text.DecimalFormat;
import java.util.Observable;
import java.util.Observer;
import org.json.JSONObject;

public class Product implements Comparable<Product> {
    private String description;
    private File downloadPath;
    private String downloadURLString;
    private double fileSize;
    private String iconURLString;
    String identifier;
    private boolean isFree;
    private ProductObservable notifier;
    private String previewURLString;
    String price;
    private int revision;
    private Status status;
    String title;

    private class ProductObservable extends Observable {
        private ProductObservable() {
        }

        public void notifyObservers(Object obj) {
            setChanged();
            super.notifyObservers(obj);
        }
    }

    public enum Status {
        UNPURCHASED,
        PURCHASED,
        WAITING,
        DOWNLOADING,
        UPDATE,
        INSTALLED
    }

    Product() {
        setStatus(Status.PURCHASED);
    }

    Product(JSONObject jSONObject) {
        this.identifier = jSONObject.optString("product_id");
        this.title = jSONObject.optString("name");
        this.description = jSONObject.optString("description");
        this.price = jSONObject.optString("price", "");
        this.isFree = jSONObject.optBoolean("free");
        if (this.isFree) {
            this.price = "FREE";
        }
        this.revision = jSONObject.optInt("current_revision", 1);
        this.fileSize = jSONObject.optDouble("file_size", 0.0d);
        this.previewURLString = jSONObject.optString("preview_url");
        this.iconURLString = jSONObject.optString("icon_url");
        this.downloadURLString = jSONObject.optString("download_url");
        if (Receipt.contains(this.identifier)) {
            Logger.info("found purchase receipt for " + this.identifier);
            Receipt fetch = Receipt.fetch(this.identifier);
            if (fetch.getProductRevision().intValue() < this.revision) {
                Logger.info("setting status to UPDATE for " + this.identifier);
                this.status = Status.UPDATE;
            } else if (IAPManager.shared().getDownloadManager().hasPendingProduct(this.identifier)) {
                Logger.info(this.identifier + " is pending");
                Product product = IAPManager.shared().getInventory().getProduct(this.identifier);
                if (product != null) {
                    this.status = product.status;
                    Logger.info(this.identifier + " has existing status, copying");
                } else {
                    Logger.info("setting status to PURCHASED for " + this.identifier);
                    this.status = Status.PURCHASED;
                }
            } else {
                this.status = Status.INSTALLED;
                Logger.info("setting status to INSTALLED for " + this.identifier);
            }
            String downloadPathString = fetch.getDownloadPathString();
            if (downloadPathString != null) {
                this.downloadPath = new File(downloadPathString);
                Logger.info("Download path for " + this.title + ": " + this.downloadPath);
            } else {
                Logger.info("No download path found for " + this.title);
            }
        } else {
            this.status = Status.UNPURCHASED;
            Logger.info("no receipt found for " + this.identifier + ", setting status to UNPURCHASED");
        }
        this.notifier = new ProductObservable();
    }

    public void addObserver(Observer observer) {
        this.notifier.addObserver(observer);
    }

    public int compareTo(Product product) {
        return getTitle().compareTo(product.getTitle());
    }

    public void deleteObserver(Observer observer) {
        this.notifier.deleteObserver(observer);
    }

    public String getDescription() {
        return this.description;
    }

    public File getDownloadPath() {
        return this.downloadPath;
    }

    public String getDownloadURLString() {
        return this.downloadURLString;
    }

    public double getFileSize() {
        return this.fileSize;
    }

    public String getHumanReadableFileSize() {
        String str;
        double d;
        double fileSize2 = getFileSize();
        double d2 = fileSize2 / 1000.0d;
        double d3 = fileSize2 / 1000000.0d;
        if (fileSize2 < 1000.0d) {
            d = fileSize2;
            str = "Bytes";
        } else if (d2 < 1.0d || d2 >= 1000.0d) {
            str = "MB";
            d = d3;
        } else {
            str = "KB";
            d = d2;
        }
        return new DecimalFormat("#0.0").format(d) + str;
    }

    public String getIconURLString() {
        return this.iconURLString;
    }

    public String getIdentifier() {
        return this.identifier;
    }

    public String getPreviewURLString() {
        return this.previewURLString;
    }

    public String getPrice() {
        return this.price;
    }

    public int getRevision() {
        return this.revision;
    }

    public Status getStatus() {
        return this.status;
    }

    public String getTitle() {
        return this.title;
    }

    public boolean isFree() {
        return this.isFree;
    }

    /* access modifiers changed from: package-private */
    public void setDownloadPath(File file) {
        this.downloadPath = file;
    }

    /* access modifiers changed from: package-private */
    public void setStatus(Status status2) {
        this.status = status2;
        this.notifier.notifyObservers(this);
    }
}
