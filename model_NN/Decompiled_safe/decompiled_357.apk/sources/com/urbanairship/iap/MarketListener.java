package com.urbanairship.iap;

import android.os.Handler;
import com.google.iap.AbstractMarketListener;
import com.google.iap.BillingService;
import com.google.iap.Consts;
import com.urbanairship.Logger;
import com.urbanairship.iap.Product;

public class MarketListener extends AbstractMarketListener {
    public MarketListener(Handler handler) {
        super(handler);
    }

    public void onBillingSupported(boolean z) {
        IAPManager.onBillingSupported(z);
    }

    public void onPurchaseStateChange(Consts.PurchaseState purchaseState, String str, int i, long j, String str2) {
    }

    public void onPurchaseStateChange(Consts.PurchaseState purchaseState, String str, String str2, long j, String str3, String str4) {
        Logger.info("purchase state changed for " + str + ": " + purchaseState);
        if (purchaseState == Consts.PurchaseState.PURCHASED) {
            Product product = IAPManager.shared().getInventory().getProduct(str);
            Logger.info("storing purchase receipt for " + str);
            new Receipt(Integer.valueOf(product.getRevision()), str, str2, Long.valueOf(j), str3, str4).serialize();
            Logger.info("starting download for " + str);
            IAPManager.shared().getDownloadManager().downloadIfValid(product);
        }
    }

    public void onRequestPurchaseResponse(BillingService.RequestPurchase requestPurchase, Consts.ResponseCode responseCode) {
        if (responseCode != Consts.ResponseCode.RESULT_OK) {
            IAPManager.shared().getInventory().getProduct(requestPurchase.mProductId).setStatus(Product.Status.UNPURCHASED);
        }
    }

    public void onRestoreTransactionsResponse(BillingService.RestoreTransactions restoreTransactions, Consts.ResponseCode responseCode) {
        if (responseCode == Consts.ResponseCode.RESULT_OK) {
            Logger.info("OK response from onRestoreTransactionsRequest, calling firstRun()");
            IAPManager.firstRun();
        }
    }
}
