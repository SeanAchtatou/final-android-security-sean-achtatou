package com.urbanairship.iap;

import com.urbanairship.Logger;
import com.urbanairship.util.Toaster;

public class DefaultIAPEventListener extends IAPEventListener {
    public void billingSupported(boolean z) {
        if (!z) {
            Toaster.longerToast("Billing is not supported on this version of Android Market");
        }
    }

    public void downloadFailed(Product product) {
        Toaster.simpleToast(String.format("Download of %s failed", product.getTitle()));
    }

    public void downloadProgress(Product product, int i) {
        Logger.verbose("Download progress for " + product.getTitle() + ": " + i + "%");
    }

    public void downloadStarted(Product product, int i) {
        if (i == 1) {
            Toaster.simpleToast(String.format("Downloading %s...", product.getTitle()));
        }
    }

    public void downloadSuccessful(Product product) {
        Toaster.simpleToast(product.getTitle() + " was sucessfully installed");
    }

    public void marketUnavailable(Product product) {
        Toaster.longerToast("Error connecting to billing service, please try again later");
    }

    public void restoreStarted() {
        Toaster.simpleToast("Checking for existing purchases");
    }
}
