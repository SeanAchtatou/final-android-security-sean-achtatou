package com.urbanairship.iap;

import android.content.SharedPreferences;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.analytics.EventDataManager;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

class Receipt {
    private static final String RECEIPT_STORE = "com.urbanairship.iap.receipts";
    private String downloadPathString;
    private String orderId;
    private String productId;
    private Integer productRevision;
    private Long purchaseTime;
    private String signature;
    private String signedData;

    Receipt(Integer num, String str) {
        this(num, str, null, null, null, null, null);
    }

    Receipt(Integer num, String str, String str2, Long l, String str3, String str4) {
        this(num, str, str2, l, str3, str4, null);
    }

    Receipt(Integer num, String str, String str2, Long l, String str3, String str4, String str5) {
        this.productId = str;
        this.productRevision = num;
        this.orderId = str2;
        this.purchaseTime = l;
        this.signedData = str3;
        this.signature = str4;
        this.downloadPathString = str5;
    }

    Receipt(String str) {
        try {
            JSONObject jSONObject = (JSONObject) new JSONTokener(str).nextValue();
            this.productId = jSONObject.getString("productId");
            this.productRevision = Integer.valueOf(jSONObject.getInt("productRevision"));
            this.orderId = jSONObject.optString("orderId", null);
            this.purchaseTime = Long.valueOf(jSONObject.optLong("purchaseTime"));
            this.signedData = jSONObject.optString(EventDataManager.Events.COLUMN_NAME_DATA, null);
            this.signature = jSONObject.optString("signature", null);
            this.downloadPathString = jSONObject.optString("downloadPathString", null);
        } catch (JSONException e) {
            Logger.error("Error parsing receipt");
        }
    }

    static boolean contains(String str) {
        return getReceiptStore().contains(str);
    }

    static Receipt fetch(String str) {
        String str2;
        try {
            str2 = getReceiptStore().getString(str, null);
        } catch (ClassCastException e) {
            Logger.error("receipt not found for " + str);
            str2 = null;
        }
        if (str2 != null) {
            return new Receipt(str2);
        }
        return null;
    }

    static SharedPreferences getReceiptStore() {
        return UAirship.shared().getApplicationContext().getSharedPreferences(RECEIPT_STORE, 0);
    }

    static boolean hasReceipts() {
        return !getReceiptStore().getAll().isEmpty();
    }

    /* access modifiers changed from: package-private */
    public String getDownloadPathString() {
        return this.downloadPathString;
    }

    /* access modifiers changed from: package-private */
    public String getOrderId() {
        return this.orderId;
    }

    /* access modifiers changed from: package-private */
    public String getProductId() {
        return this.productId;
    }

    /* access modifiers changed from: package-private */
    public Integer getProductRevision() {
        return this.productRevision;
    }

    /* access modifiers changed from: package-private */
    public Long getPurchaseTime() {
        return this.purchaseTime;
    }

    /* access modifiers changed from: package-private */
    public String getSignature() {
        return this.signature;
    }

    /* access modifiers changed from: package-private */
    public String getSignedData() {
        return this.signedData;
    }

    /* access modifiers changed from: package-private */
    public boolean serialize() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("productRevision", this.productRevision);
            jSONObject.put("productId", this.productId);
            if (this.orderId != null) {
                jSONObject.put("orderId", this.orderId);
            }
            if (this.purchaseTime != null) {
                jSONObject.put("purchaseTime", this.purchaseTime);
            }
            if (this.signedData != null) {
                jSONObject.put(EventDataManager.Events.COLUMN_NAME_DATA, this.signedData);
            }
            if (this.signature != null) {
                jSONObject.put("signature", this.signature);
            }
            if (this.downloadPathString != null) {
                jSONObject.put("downloadPathString", this.downloadPathString);
            }
            try {
                SharedPreferences.Editor edit = getReceiptStore().edit();
                edit.putString(this.productId, jSONObject.toString());
                edit.commit();
                return true;
            } catch (Exception e) {
                Logger.error("error writing receipt");
                return false;
            }
        } catch (JSONException e2) {
            Logger.error("error constructing JSON object out of receipt data");
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void setDownloadPathString(String str) {
        this.downloadPathString = str;
    }

    /* access modifiers changed from: package-private */
    public void setProductRevision(Integer num) {
        this.productRevision = num;
    }
}
