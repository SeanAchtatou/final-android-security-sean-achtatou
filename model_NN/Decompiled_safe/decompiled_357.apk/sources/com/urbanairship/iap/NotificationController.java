package com.urbanairship.iap;

import android.app.NotificationManager;
import com.urbanairship.UAirship;
import com.urbanairship.iap.PurchaseNotificationInfo;
import java.util.HashMap;

class NotificationController {
    private HashMap<String, PurchaseNotificationInfo> notifications = new HashMap<>();

    NotificationController() {
    }

    /* access modifiers changed from: package-private */
    public boolean contains(String str) {
        return this.notifications.containsKey(str);
    }

    /* access modifiers changed from: package-private */
    public PurchaseNotificationInfo get(String str) {
        return this.notifications.get(str);
    }

    /* access modifiers changed from: package-private */
    public void registerPurchaseNotification(String str, String str2, PurchaseNotificationInfo.NotificationType notificationType) {
        this.notifications.put(str, new PurchaseNotificationInfo(notificationType, str2, str));
    }

    /* access modifiers changed from: package-private */
    public void removePurchaseNotification(String str) {
        PurchaseNotificationInfo purchaseNotificationInfo = this.notifications.get(str);
        purchaseNotificationInfo.setFlags(16);
        sendNotification(purchaseNotificationInfo);
        this.notifications.remove(str);
    }

    /* access modifiers changed from: package-private */
    public void sendNotification(PurchaseNotificationInfo purchaseNotificationInfo) {
        ((NotificationManager) UAirship.shared().getApplicationContext().getSystemService("notification")).notify(purchaseNotificationInfo.getNotificationId(), IAPManager.shared().getNotificationBuilder().buildNotification(purchaseNotificationInfo));
    }
}
