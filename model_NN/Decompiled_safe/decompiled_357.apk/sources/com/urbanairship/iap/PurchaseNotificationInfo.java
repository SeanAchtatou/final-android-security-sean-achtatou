package com.urbanairship.iap;

import com.urbanairship.util.NotificationIDGenerator;

public class PurchaseNotificationInfo {
    private int flags;
    private int notificationId;
    private String productId;
    private String productName;
    private int progress = 0;
    private long timestamp;
    private NotificationType type;

    public enum NotificationType {
        DOWNLOAD_FAILED,
        DECOMPRESS_FAILED,
        DOWNLOADING,
        DECOMPRESSING,
        INSTALL_SUCCESSFUL,
        VERIFYING_RECEIPT
    }

    public PurchaseNotificationInfo(NotificationType notificationType, String str, String str2) {
        this.type = notificationType;
        this.productName = str;
        this.productId = str2;
        this.notificationId = NotificationIDGenerator.nextID();
        this.flags = 10;
        this.timestamp = System.currentTimeMillis();
    }

    public int getFlags() {
        return this.flags;
    }

    public int getNotificationId() {
        return this.notificationId;
    }

    public NotificationType getNotificationType() {
        return this.type;
    }

    public String getProductId() {
        return this.productId;
    }

    public String getProductName() {
        return this.productName;
    }

    public int getProgress() {
        return this.progress;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public void setFlags(int i) {
        this.flags = i;
    }

    public void setNotificationId(int i) {
        this.notificationId = i;
    }

    public void setNotificationType(NotificationType notificationType) {
        this.type = notificationType;
    }

    public void setProductId(String str) {
        this.productId = str;
    }

    public void setProductName(String str) {
        this.productName = str;
    }

    public void setProgress(int i) {
        this.progress = i;
    }

    public void setTimestamp(long j) {
        this.timestamp = j;
    }
}
