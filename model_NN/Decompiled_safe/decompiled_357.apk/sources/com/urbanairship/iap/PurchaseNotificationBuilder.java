package com.urbanairship.iap;

import android.app.Notification;

public interface PurchaseNotificationBuilder {
    Notification buildNotification(PurchaseNotificationInfo purchaseNotificationInfo);
}
