package com.urbanairship.iap;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Handler;
import com.google.iap.BillingService;
import com.google.iap.ResponseHandler;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;

public final class IAPManager {
    private static final String IAP_FIRST_RUN_PREFERENCES = "com.urbanairship.iap.first_run";
    private static final String INITIALIZED_KEY = "initialized";
    private static final IAPManager instance = new IAPManager();
    private int appIcon;
    private String appName = null;
    private BillingService billingService;
    private DownloadManager downloadManager;
    private String downloadPath;
    private IAPEventListener eventListener;
    private Inventory inventory;
    private boolean isBillingSupported = false;
    private MarketListener marketListener;
    private PurchaseNotificationBuilder notificationBuilder;
    private String tempPath;

    private IAPManager() {
    }

    static void firstRun() {
        try {
            SharedPreferences.Editor edit = UAirship.shared().getApplicationContext().getSharedPreferences(IAP_FIRST_RUN_PREFERENCES, 0).edit();
            edit.putBoolean(INITIALIZED_KEY, true);
            edit.commit();
        } catch (Exception e) {
            Logger.error("Error writing to shared preferences");
        }
    }

    public static void init() {
        instance.inventory = new Inventory();
        instance.downloadManager = new DownloadManager();
        instance.notificationBuilder = new BasicPurchaseNotificationBuilder();
        instance.billingService = new BillingService();
        instance.billingService.setContext(UAirship.shared().getApplicationContext());
        instance.appName = UAirship.getAppName();
        instance.appIcon = UAirship.getAppIcon();
        instance.billingService.checkBillingSupported();
        instance.eventListener = new DefaultIAPEventListener();
        if (instance.marketListener != null) {
            ResponseHandler.unregister(instance.marketListener);
        }
        instance.marketListener = new MarketListener(new Handler());
        ResponseHandler.register(instance.marketListener);
    }

    public static boolean isBillingSupported() {
        return instance.isBillingSupported;
    }

    static boolean isFirstRun() {
        return !UAirship.shared().getApplicationContext().getSharedPreferences(IAP_FIRST_RUN_PREFERENCES, 0).getBoolean(INITIALIZED_KEY, false);
    }

    static void onBillingSupported(boolean z) {
        instance.isBillingSupported = z;
        if (instance.eventListener != null) {
            instance.eventListener.billingSupported(z);
        }
    }

    public static IAPManager shared() {
        return instance;
    }

    public static void tearDown() {
        instance.billingService.unbind();
        ResponseHandler.unregister(instance.marketListener);
    }

    public int getAppIcon() {
        return this.appIcon;
    }

    public String getAppName() {
        return this.appName;
    }

    /* access modifiers changed from: package-private */
    public BillingService getBillingService() {
        return this.billingService;
    }

    /* access modifiers changed from: package-private */
    public DownloadManager getDownloadManager() {
        return this.downloadManager;
    }

    public String getDownloadPath() {
        return this.downloadPath != null ? this.downloadPath : "iap/downloads/" + UAirship.getAppName() + "/";
    }

    public IAPEventListener getEventListener() {
        return this.eventListener;
    }

    public Inventory getInventory() {
        return this.inventory;
    }

    public PurchaseNotificationBuilder getNotificationBuilder() {
        return this.notificationBuilder;
    }

    public String getTempPath() {
        return this.tempPath != null ? this.tempPath : "iap/temp/" + UAirship.getAppName() + "/";
    }

    /* access modifiers changed from: package-private */
    public boolean payForProduct(Activity activity, Product product) {
        if (!this.isBillingSupported) {
            Logger.error("Billing is not supported on this version of Android Market");
            return false;
        } else if (this.billingService.requestPurchase(activity, product.getIdentifier())) {
            return true;
        } else {
            if (this.eventListener != null) {
                this.eventListener.marketUnavailable(product);
            }
            return false;
        }
    }

    public void setDownloadPath(String str) {
        this.downloadPath = str;
    }

    public void setEventListener(IAPEventListener iAPEventListener) {
        this.eventListener = iAPEventListener;
    }

    public void setNotificationBuilder(PurchaseNotificationBuilder purchaseNotificationBuilder) {
        this.notificationBuilder = purchaseNotificationBuilder;
    }

    public void setTempPath(String str) {
        this.tempPath = str;
    }
}
