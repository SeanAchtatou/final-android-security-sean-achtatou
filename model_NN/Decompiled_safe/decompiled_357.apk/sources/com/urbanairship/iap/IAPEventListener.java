package com.urbanairship.iap;

public abstract class IAPEventListener {
    public abstract void billingSupported(boolean z);

    public abstract void downloadFailed(Product product);

    public abstract void downloadProgress(Product product, int i);

    public abstract void downloadStarted(Product product, int i);

    public abstract void downloadSuccessful(Product product);

    public abstract void marketUnavailable(Product product);

    public abstract void restoreStarted();
}
