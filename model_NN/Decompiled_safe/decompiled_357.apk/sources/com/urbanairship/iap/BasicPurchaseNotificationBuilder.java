package com.urbanairship.iap;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.urbanairship.UAirship;

public class BasicPurchaseNotificationBuilder implements PurchaseNotificationBuilder {
    Context context = UAirship.shared().getApplicationContext();
    int iconDrawable = UAirship.getAppInfo().icon;

    public Notification buildNotification(PurchaseNotificationInfo purchaseNotificationInfo) {
        PendingIntent activity = PendingIntent.getActivity(this.context, 0, new Intent(), 0);
        Notification notification = new Notification(this.iconDrawable, purchaseNotificationInfo.getProductName(), purchaseNotificationInfo.getTimestamp());
        notification.flags = purchaseNotificationInfo.getFlags();
        notification.setLatestEventInfo(this.context, purchaseNotificationInfo.getProductName(), getNotificationMessage(purchaseNotificationInfo), activity);
        return notification;
    }

    /* access modifiers changed from: protected */
    public String getNotificationMessage(PurchaseNotificationInfo purchaseNotificationInfo) {
        switch (purchaseNotificationInfo.getNotificationType()) {
            case DOWNLOADING:
                return String.format("Downloading %s...", purchaseNotificationInfo.getProductName());
            case DECOMPRESSING:
                return String.format("Decompressing %s...", purchaseNotificationInfo.getProductName());
            case DECOMPRESS_FAILED:
                return String.format("Extraction of %s failed", purchaseNotificationInfo.getProductName());
            case DOWNLOAD_FAILED:
                return String.format("Download of %s failed", purchaseNotificationInfo.getProductName());
            case INSTALL_SUCCESSFUL:
                return purchaseNotificationInfo.getProductName() + " was sucessfully installed";
            case VERIFYING_RECEIPT:
                return "Verifying " + purchaseNotificationInfo.getProductName();
            default:
                return "";
        }
    }
}
