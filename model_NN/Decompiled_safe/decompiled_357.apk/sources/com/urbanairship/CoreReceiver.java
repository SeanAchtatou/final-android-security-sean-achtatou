package com.urbanairship;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.urbanairship.analytics.Analytics;
import com.urbanairship.push.PushManager;

public class CoreReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals("android.intent.action.ACTION_SHUTDOWN")) {
            PushManager.stopService();
        } else if (action.equals("android.intent.action.BOOT_COMPLETED")) {
            PushManager.startService();
        } else if (Analytics.ACTION_ANALYTICS_START.equals(action)) {
            UAirship.shared().getAnalytics().startUploadingIfNecessary();
        } else if (action.startsWith(PushManager.ACTION_NOTIFICATION_OPENED_PROXY)) {
            Logger.debug("Received push conversion: " + intent.getStringExtra(PushManager.EXTRA_PUSH_ID));
            UAirship.shared().getAnalytics().setConversionPushId(intent.getStringExtra(PushManager.EXTRA_PUSH_ID));
            Intent intent2 = new Intent(PushManager.ACTION_NOTIFICATION_OPENED);
            intent2.setClass(UAirship.shared().getApplicationContext(), PushManager.shared().getIntentReceiver());
            intent2.putExtras(intent.getExtras());
            UAirship.shared().getApplicationContext().sendBroadcast(intent2);
        }
    }
}
