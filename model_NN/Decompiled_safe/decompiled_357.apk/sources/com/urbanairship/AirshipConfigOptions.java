package com.urbanairship;

import android.content.Context;

public class AirshipConfigOptions extends Options {
    public boolean analyticsEnabled = true;
    public String analyticsServer = "https://combine.urbanairship.com/";
    public String c2dmSender;
    public String developmentAppKey;
    public String developmentAppSecret;
    public String hostURL = "https://go.urbanairship.com/";
    public boolean iapEnabled = true;
    public boolean inProduction = false;
    public String productionAppKey;
    public String productionAppSecret;
    public boolean pushServiceEnabled = true;
    public String transport;

    public enum TransportType {
        HELIUM,
        C2DM,
        HYBRID
    }

    public static AirshipConfigOptions loadDefaultOptions(Context context) {
        AirshipConfigOptions airshipConfigOptions = new AirshipConfigOptions();
        airshipConfigOptions.loadFromProperties(context);
        return airshipConfigOptions;
    }

    public String getAppKey() {
        return this.inProduction ? this.productionAppKey : this.developmentAppKey;
    }

    public String getAppSecret() {
        return this.inProduction ? this.productionAppSecret : this.developmentAppSecret;
    }

    public String getDefaultPropertiesFilename() {
        return "airshipconfig.properties";
    }

    public TransportType getTransport() {
        return TransportType.C2DM.toString().equalsIgnoreCase(this.transport) ? TransportType.C2DM : TransportType.HELIUM.toString().equalsIgnoreCase(this.transport) ? TransportType.HELIUM : TransportType.HYBRID.toString().equalsIgnoreCase(this.transport) ? TransportType.HYBRID : (this.c2dmSender == null || this.c2dmSender.length() <= 0) ? TransportType.HELIUM : TransportType.C2DM;
    }

    public boolean isValid() {
        return getAppKey() != null && getAppKey().length() > 0 && getAppKey().indexOf(32) < 0;
    }
}
