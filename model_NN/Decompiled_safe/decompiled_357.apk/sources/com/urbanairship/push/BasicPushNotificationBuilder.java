package com.urbanairship.push;

import android.app.Notification;
import com.urbanairship.UAirship;
import com.urbanairship.util.NotificationIDGenerator;
import java.util.Map;

public class BasicPushNotificationBuilder implements PushNotificationBuilder {
    public String appName = UAirship.getAppName();
    public int constantNotificationId = -1;
    public int iconDrawableId = UAirship.getAppInfo().icon;

    public Notification buildNotification(String str, Map<String, String> map) {
        Notification notification = new Notification(this.iconDrawableId, str, System.currentTimeMillis());
        notification.flags = 16;
        notification.setLatestEventInfo(UAirship.shared().getApplicationContext(), this.appName, str, null);
        notification.defaults = 0;
        PushPreferences preferences = PushManager.shared().getPreferences();
        if (!preferences.beQuiet()) {
            if (preferences.isVibrateEnabled()) {
                notification.defaults |= 2;
            }
            if (preferences.isSoundEnabled()) {
                notification.defaults |= 1;
            }
        }
        return notification;
    }

    public int getNextId(String str, Map<String, String> map) {
        return this.constantNotificationId > 0 ? this.constantNotificationId : NotificationIDGenerator.nextID();
    }
}
