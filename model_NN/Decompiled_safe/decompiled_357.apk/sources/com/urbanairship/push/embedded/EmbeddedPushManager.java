package com.urbanairship.push.embedded;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.push.PushManager;
import com.urbanairship.push.PushPreferences;
import com.urbanairship.push.PushService;
import com.urbanairship.push.embedded.Config;
import com.urbanairship.push.proto.Messages;
import java.util.HashMap;

public class EmbeddedPushManager {
    private static BroadcastReceiver connectivityChanged = null;
    public static final EmbeddedPushManager instance = new EmbeddedPushManager();
    static final String version = "3.0.0";
    private HeliumConnection connection = null;
    private String ipAddress = null;

    private EmbeddedPushManager() {
    }

    private void cancelReconnect() {
    }

    public static void deliverPush(Messages.PushNotification pushNotification) {
        Logger.debug("Received Helium Push.");
        String message = pushNotification.getMessage();
        String payload = pushNotification.getPayload();
        HashMap hashMap = new HashMap();
        if (pushNotification.getMapCount() > 0) {
            for (Messages.KeyValue next : pushNotification.getMapList()) {
                hashMap.put(next.getKey(), next.getValue());
            }
        } else if (payload != null && payload.length() > 0) {
            hashMap.put(PushManager.EXTRA_STRING_EXTRA, payload);
        }
        PushManager.deliverPush(message, pushNotification.getMessageId(), hashMap);
    }

    public static void init(Context context, String str) {
        Logger.verbose("Embedded Push Initializing...");
        Intent intent = new Intent(context, PushService.class);
        intent.setAction(PushService.ACTION_HEARTBEAT);
        ((AlarmManager) context.getSystemService("alarm")).setInexactRepeating(1, System.currentTimeMillis() + 900000, 900000, PendingIntent.getService(context, 0, intent, 0));
        if (shared().isInHoldingPattern()) {
            Logger.debug("In holding pattern. Will retry after " + PushManager.shared().getPreferences().getRetryAfter());
            PushManager.stopService();
            return;
        }
        if (connectivityChanged == null) {
            connectivityChanged = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    String str = "Connectivity changed: connected=" + Network.isConnected();
                    if (Network.isConnected()) {
                        str = str + ", network type=" + Network.typeName();
                    }
                    Logger.info(str);
                    if (Network.isConnected()) {
                        EmbeddedPushManager.shared().reconnectIfNecessary();
                    }
                }
            };
            context.registerReceiver(connectivityChanged, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
        Logger.verbose("Embedded Push initialization complete.");
    }

    private boolean isReconnectNecessary() {
        String activeIPAddress = Network.getActiveIPAddress();
        Logger.verbose("Current IP: " + activeIPAddress + ". Previous IP: " + this.ipAddress);
        if (this.ipAddress != null || activeIPAddress == null) {
            return (this.ipAddress == null || activeIPAddress == null || this.ipAddress.equals(activeIPAddress)) ? false : true;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void reconnectIfNecessary() {
        Logger.verbose("IP Changed: " + isReconnectNecessary());
        if (!isInHoldingPattern()) {
            Logger.debug("Starting new connection to Helium");
            if (this.connection != null) {
                this.connection.abort();
            }
            this.connection = new HeliumConnection(this);
            this.connection.start();
        }
    }

    protected static void sendRegistrationResponse() {
        Logger.debug("sending valid: " + UAirship.getPackageName() + ", " + PushManager.shared().getPreferences().isPushEnabled());
        PushManager.shared().heliumRegistrationResponseReceived();
    }

    public static EmbeddedPushManager shared() {
        return instance;
    }

    public boolean isInHoldingPattern() {
        PushPreferences preferences = PushManager.shared().getPreferences();
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        long retryAfter = preferences.getRetryAfter();
        if (retryAfter - currentTimeMillis > Config.BoxOffice.MAX_HOLDING_PATTERN) {
            retryAfter = 0;
            preferences.setRetryAfter(0);
        }
        return retryAfter > currentTimeMillis;
    }

    public void resetConnectionIfNecessary() {
        if (this.connection != null) {
            this.connection.resetConnectionIfNecessary();
        }
    }

    public boolean setHoldingPattern(long j) {
        long j2;
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        if (currentTimeMillis >= j) {
            Logger.debug("BoxOffice retry_after response is in the past. Ignoring.");
            return false;
        }
        if (j - currentTimeMillis > Config.BoxOffice.MAX_HOLDING_PATTERN) {
            Logger.debug("BoxOffice retry_after response of " + j + " exceeds our maximum retry delay. Setting to max delay.");
            j2 = currentTimeMillis + Config.BoxOffice.MAX_HOLDING_PATTERN;
        } else {
            j2 = j;
        }
        Logger.debug("Received BoxOffice response to reconnect after: " + j2 + ". Currently: " + currentTimeMillis + ". Shutting down" + "for " + (j2 - currentTimeMillis) + " seconds.");
        return PushManager.shared().getPreferences().setRetryAfter(j2);
    }

    public synchronized void setIPAddress(String str) {
        this.ipAddress = str;
    }

    public void teardown() {
        Logger.verbose("Embedded Push teardown!");
        Context applicationContext = UAirship.shared().getApplicationContext();
        if (connectivityChanged != null) {
            applicationContext.unregisterReceiver(connectivityChanged);
            connectivityChanged = null;
        }
        cancelReconnect();
        if (this.connection != null) {
            this.connection.abort();
            this.connection = null;
        }
    }
}
