package com.urbanairship.push.embedded;

import android.os.Build;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.push.PushManager;
import com.urbanairship.push.PushPreferences;
import com.urbanairship.push.embedded.Crypto;
import com.urbanairship.push.proto.Messages;
import com.urbanairship.push.proto.Rpc;
import com.urbanairship.util.Base64;
import com.urbanairship.util.Base64DecoderException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public class HeliumClient {
    private BoxOfficeClient boxOfficeClient;
    private CodedInputStream in;
    public long lastKeepAlive = System.currentTimeMillis();
    private OutputStream out;
    private PushPreferences prefs = PushManager.shared().getPreferences();
    private HashMap<String, String> pushIDs;

    public class HeliumException extends Exception {
        private static final long serialVersionUID = 1;

        public HeliumException(String str) {
            super(str);
        }
    }

    public HeliumClient(Socket socket, BoxOfficeClient boxOfficeClient2) throws IOException {
        this.boxOfficeClient = boxOfficeClient2;
        this.in = CodedInputStream.newInstance(socket.getInputStream());
        this.out = socket.getOutputStream();
        this.pushIDs = new HashMap<>();
    }

    private void handleFailure(Rpc.Request request) {
        Logger.error("Got a bad request!");
    }

    private boolean messageAlreadySent(String str, String str2) {
        return this.pushIDs.containsKey(str) && this.pushIDs.get(str).equals(str2);
    }

    public static ArrayList<Messages.Relier> protoReliers(String str, String str2) {
        ArrayList<Messages.Relier> arrayList = new ArrayList<>();
        arrayList.add(Messages.Relier.newBuilder().setPackage(str).setAppKey(str2).build());
        return arrayList;
    }

    private void recordMessage(String str, String str2) {
        this.pushIDs.put(str, str2);
    }

    private void sendRequest(Messages.AirshipMethod airshipMethod, ByteString byteString) {
        Rpc.Request build = Rpc.Request.newBuilder().setBody(byteString).setMethodId(airshipMethod).build();
        try {
            short serializedSize = (short) build.getSerializedSize();
            Logger.verbose("Size: " + ((int) serializedSize));
            this.out.write(ByteBuffer.allocate(2).putShort(serializedSize).array());
            build.writeTo(this.out);
        } catch (Exception e) {
            Logger.error(e);
        }
    }

    public long getLastKeepAlive() {
        return this.lastKeepAlive;
    }

    public void handleNotification(Messages.PushNotification pushNotification) {
        String packageName = pushNotification.getPackageName();
        String messageId = pushNotification.getMessageId();
        Logger.verbose("Got app id: " + packageName);
        if (messageAlreadySent(packageName, messageId)) {
            Logger.info("Message " + messageId + " already sent. Discarding.");
            return;
        }
        Logger.info("Message " + messageId + " received.");
        recordMessage(packageName, messageId);
        if (!this.prefs.isPushEnabled()) {
            Logger.warn(String.format("Got push notification, but Push is disabled", new Object[0]));
        } else {
            EmbeddedPushManager.deliverPush(pushNotification);
        }
    }

    public void handleRegistration(Messages.RegistrationResponse registrationResponse) {
        String str;
        String str2;
        Logger.debug("Registration response received!");
        List<Messages.Relier> validReliersList = registrationResponse.getValidReliersList();
        List<Messages.Relier> invalidReliersList = registrationResponse.getInvalidReliersList();
        String str3 = "";
        Iterator<Messages.Relier> it = validReliersList.iterator();
        while (true) {
            str = str3;
            if (!it.hasNext()) {
                break;
            }
            str3 = str + it.next().getPackage() + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR;
        }
        String str4 = "";
        Iterator<Messages.Relier> it2 = invalidReliersList.iterator();
        while (true) {
            str2 = str4;
            if (!it2.hasNext()) {
                break;
            }
            str4 = str2 + it2.next().getPackage() + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR;
        }
        Logger.verbose("Valid: " + str);
        Logger.verbose("Invalid: " + str2);
        if (validReliersList.size() > 0) {
            EmbeddedPushManager.sendRegistrationResponse();
        } else if (invalidReliersList.size() > 0) {
            EmbeddedPushManager.sendRegistrationResponse();
            throw new RuntimeException("Package name on server does not match the application package name.");
        }
        this.boxOfficeClient.resetFailureCount(this.boxOfficeClient.getCurrentServerAddress());
    }

    public void readResponse() throws IOException, HeliumException, Crypto.NullUUID, Base64DecoderException {
        short s = ByteBuffer.wrap(this.in.readRawBytes(2)).getShort();
        if (s == 0) {
            Logger.verbose("keepalive read");
            this.lastKeepAlive = System.currentTimeMillis();
        } else if (BoxOfficeClient.currentKey == null) {
            throw new HeliumException("key unknown!");
        } else {
            Rpc.Request parseFrom = Rpc.Request.parseFrom(new Crypto(BoxOfficeClient.currentKey).decrypt(Base64.decode(this.in.readRawBytes(s))));
            Logger.verbose("response read");
            if (parseFrom.getMethodId() == Messages.AirshipMethod.REGISTER) {
                handleRegistration(Messages.RegistrationResponse.parseFrom(parseFrom.getBody()));
            } else if (parseFrom.getMethodId() == Messages.AirshipMethod.PUSH_NOTIFICATION) {
                handleNotification(Messages.PushNotification.parseFrom(parseFrom.getBody()));
            } else {
                handleFailure(parseFrom);
            }
        }
    }

    public void register() throws Crypto.NullUUID {
        sendRequest(Messages.AirshipMethod.REGISTER, Messages.RegistrationEnvelope.newBuilder().setApid(this.prefs.getPushId()).setRegistration(ByteString.copyFrom(Base64.encodeBytesToBytes(new Crypto(BoxOfficeClient.currentKey).encrypt(Messages.Register.newBuilder().setApid(this.prefs.getPushId()).setOs(Messages.OS.ANDROID).setOsVersion(Build.VERSION.RELEASE).setUaVersion("3.0.0").setSecret(this.prefs.getPushSecret()).addAllReliers(protoReliers(UAirship.getPackageName(), UAirship.shared().getAirshipConfigOptions().getAppKey())).build().toByteArray())))).build().toByteString());
    }
}
