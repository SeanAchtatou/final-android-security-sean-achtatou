package com.urbanairship.push.embedded;

public class Config {

    public static class BoxOffice {
        public static final long MAX_HOLDING_PATTERN = 604800;
        public static String url = "https://boxoffice.urbanairship.com";
    }

    public static class Helium {
        public static int initialRetryInterval = 10000;
        public static long maxRetryInterval = 300000;
        public static long max_keepalive_interval = 300;
        public static int readSleep = 100;
    }
}
