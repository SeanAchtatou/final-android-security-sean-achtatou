package com.urbanairship.push.embedded;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.provider.Settings;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.push.PushManager;
import com.urbanairship.push.PushPreferences;
import com.urbanairship.push.embedded.Config;
import com.urbanairship.restclient.Request;
import com.urbanairship.restclient.Response;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.UUID;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import twitter4j.internal.http.HttpResponseCode;

public class BoxOfficeClient {
    private static final long CACHE_TTL = 86400000;
    protected static UUID currentKey = null;
    private static String deviceId;
    private String currentServer = null;
    private final DecimalFormat ms = new DecimalFormat("0.000");
    private PushPreferences prefs = PushManager.shared().getPreferences();
    private final LinkedList<HeliumServer> servers = new LinkedList<>();

    public static class BoxOfficeException extends Exception {
        private static final long serialVersionUID = 1;

        public BoxOfficeException(String str) {
            super(str);
        }
    }

    protected class HeliumServer {
        /* access modifiers changed from: private */
        public String address;
        private long discoveredAt = System.currentTimeMillis();
        /* access modifiers changed from: private */
        public int failureCount = 0;

        public HeliumServer(String str) {
            this.address = str;
        }

        static /* synthetic */ int access$108(HeliumServer heliumServer) {
            int i = heliumServer.failureCount;
            heliumServer.failureCount = i + 1;
            return i;
        }

        public boolean isValid() {
            long currentTimeMillis = System.currentTimeMillis() - this.discoveredAt;
            if (currentTimeMillis < 0) {
                return false;
            }
            return currentTimeMillis < BoxOfficeClient.CACHE_TTL && this.failureCount < 3;
        }
    }

    public BoxOfficeClient() {
        deviceId = getHashedDeviceId(UAirship.shared().getApplicationContext());
    }

    private void doLookup() throws BoxOfficeException {
        BasicNameValuePair basicNameValuePair = new BasicNameValuePair("apid", this.prefs.getPushId());
        BasicNameValuePair basicNameValuePair2 = new BasicNameValuePair("secret", this.prefs.getPushSecret());
        ArrayList arrayList = new ArrayList();
        arrayList.add(basicNameValuePair);
        arrayList.add(basicNameValuePair2);
        Request request = new Request("POST", Config.BoxOffice.url + "/lookup");
        try {
            request.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
            setRequestHeaders(request);
            PackageInfo packageInfo = UAirship.getPackageInfo();
            String str = packageInfo != null ? packageInfo.versionName : "Unavailable";
            Logger.debug("Set version header :" + str);
            request.setHeader("X-UA-Package-Version", str);
            request.setHeader("User-Agent", String.format("Embedded Push/%s Android/%s [%s]", "3.0.0", Build.VERSION.RELEASE, UAirship.getPackageName()));
            try {
                Response execute = request.execute();
                if (execute == null) {
                    Logger.error("Error posting to /lookup");
                    throw new BoxOfficeException("Error posting to /lookup");
                }
                String contentType = execute.contentType();
                String trim = validateResponse(execute).body().trim();
                if (contentType == null || !contentType.equals("application/json")) {
                    throw new BoxOfficeException("Received invalid BoxOffice response; content type is not application/json");
                }
                processJSONLookup(trim);
            } catch (Exception e) {
                Logger.error("Error posting to /lookup");
                throw new BoxOfficeException("Error posting to /lookup");
            }
        } catch (UnsupportedEncodingException e2) {
            throw new BoxOfficeException("Failed to post to /furstrun; UTF-8 unsupported!");
        }
    }

    private String generateApid() {
        String pushId = this.prefs.getPushId();
        if (pushId != null) {
            return pushId;
        }
        String uuid = UUID.randomUUID().toString();
        Logger.debug("Generating APID: " + uuid);
        this.prefs.setPushId(uuid);
        return uuid;
    }

    private static String getHashedDeviceId(Context context) {
        String string;
        if (context == null || (string = Settings.Secure.getString(context.getContentResolver(), "android_id")) == null) {
            return "";
        }
        byte[] bytes = string.getBytes();
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.update(bytes, 0, bytes.length);
            byte[] digest = instance.digest();
            StringBuilder sb = new StringBuilder();
            int length = digest.length;
            for (int i = 0; i < length; i++) {
                sb.append(String.format("%02x", Byte.valueOf(digest[i])));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            Logger.error("Unable to hash the device ID: SHA1 digester not present");
            return "";
        }
    }

    private void setRequestHeaders(Request request) {
        request.setHeader("X-UA-Device-Family", "Android");
        request.setHeader("X-UA-Device-Model", Build.MODEL);
        request.setHeader("X-UA-OS-Version", Build.VERSION.RELEASE);
        request.setHeader("X-UA-Lib-Version", "3.0.0");
        request.setHeader("X-UA-Package-Name", UAirship.getPackageName());
        request.setHeader("X-UA-Sent-At", this.ms.format(((double) System.currentTimeMillis()) / 1000.0d));
        if (UAirship.shared().getAirshipConfigOptions().analyticsEnabled) {
            request.setHeader("X-UA-Device-ID", deviceId);
        }
        request.setHeader("Authorization", "Basic cU9ES09HVEFTMGVqa1pvTUZRTkt3ZzptMVF4bmFBcFN0cU1BekN5Y2ppQkJn");
    }

    public String firstRun() throws BoxOfficeException {
        String generateApid = generateApid();
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("package", UAirship.getPackageName()));
        arrayList.add(new BasicNameValuePair("apid", generateApid));
        try {
            Request request = new Request("POST", Config.BoxOffice.url + "/firstrun");
            request.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
            setRequestHeaders(request);
            String trim = validateResponse(request.execute()).body().trim();
            setSecret(trim);
            return trim;
        } catch (UnsupportedEncodingException e) {
            throw new BoxOfficeException("Failed to post to /firstrun; UTF-8 unsupported!");
        } catch (Exception e2) {
            Logger.error("Error posting to /firstrun");
            throw new BoxOfficeException("Failed to post to /furstrun");
        }
    }

    public String getCurrentServerAddress() {
        return this.currentServer;
    }

    public void incrementFailureCount() {
        incrementFailureCount(this.currentServer);
    }

    public void incrementFailureCount(String str) {
        synchronized (this.servers) {
            Iterator<HeliumServer> it = this.servers.iterator();
            while (it.hasNext()) {
                HeliumServer next = it.next();
                if (next.address.equals(str)) {
                    HeliumServer.access$108(next);
                    Logger.info("Setting failure count for " + str + " to " + next.failureCount);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003e, code lost:
        if (com.urbanairship.push.embedded.BoxOfficeClient.HeliumServer.access$000(r0).split(":").length == 2) goto L_0x0055;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0051, code lost:
        throw new com.urbanairship.push.embedded.BoxOfficeClient.BoxOfficeException(java.lang.String.format("Got invalid server: '%s'", r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0055, code lost:
        r1 = r5.servers;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0057, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005c, code lost:
        if (r0.isValid() == false) goto L_0x0063;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005e, code lost:
        r5.servers.add(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0063, code lost:
        r5.currentServer = com.urbanairship.push.embedded.BoxOfficeClient.HeliumServer.access$000(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0069, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        return com.urbanairship.push.embedded.BoxOfficeClient.HeliumServer.access$000(r0);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String lookup() throws com.urbanairship.push.embedded.BoxOfficeClient.BoxOfficeException {
        /*
            r5 = this;
            com.urbanairship.push.PushPreferences r0 = r5.prefs
            java.lang.String r0 = r0.getPushSecret()
            if (r0 != 0) goto L_0x000b
            r5.firstRun()
        L_0x000b:
            java.util.LinkedList<com.urbanairship.push.embedded.BoxOfficeClient$HeliumServer> r1 = r5.servers
            monitor-enter(r1)
            java.util.LinkedList<com.urbanairship.push.embedded.BoxOfficeClient$HeliumServer> r0 = r5.servers     // Catch:{ all -> 0x0052 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0052 }
            if (r0 == 0) goto L_0x0019
            r5.doLookup()     // Catch:{ all -> 0x0052 }
        L_0x0019:
            java.util.LinkedList<com.urbanairship.push.embedded.BoxOfficeClient$HeliumServer> r0 = r5.servers     // Catch:{ all -> 0x0052 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0052 }
            if (r0 == 0) goto L_0x0029
            java.lang.String r0 = "No Helium servers returned from BoxOffice lookup."
            com.urbanairship.Logger.info(r0)     // Catch:{ all -> 0x0052 }
            r0 = 0
            monitor-exit(r1)     // Catch:{ all -> 0x0052 }
        L_0x0028:
            return r0
        L_0x0029:
            java.util.LinkedList<com.urbanairship.push.embedded.BoxOfficeClient$HeliumServer> r0 = r5.servers     // Catch:{ all -> 0x0052 }
            java.lang.Object r0 = r0.remove()     // Catch:{ all -> 0x0052 }
            com.urbanairship.push.embedded.BoxOfficeClient$HeliumServer r0 = (com.urbanairship.push.embedded.BoxOfficeClient.HeliumServer) r0     // Catch:{ all -> 0x0052 }
            monitor-exit(r1)     // Catch:{ all -> 0x0052 }
            java.lang.String r1 = r0.address
            java.lang.String r2 = ":"
            java.lang.String[] r1 = r1.split(r2)
            int r1 = r1.length
            r2 = 2
            if (r1 == r2) goto L_0x0055
            com.urbanairship.push.embedded.BoxOfficeClient$BoxOfficeException r1 = new com.urbanairship.push.embedded.BoxOfficeClient$BoxOfficeException
            java.lang.String r2 = "Got invalid server: '%s'"
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r4 = 0
            r3[r4] = r0
            java.lang.String r0 = java.lang.String.format(r2, r3)
            r1.<init>(r0)
            throw r1
        L_0x0052:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0052 }
            throw r0
        L_0x0055:
            java.util.LinkedList<com.urbanairship.push.embedded.BoxOfficeClient$HeliumServer> r1 = r5.servers
            monitor-enter(r1)
            boolean r2 = r0.isValid()     // Catch:{ all -> 0x006f }
            if (r2 == 0) goto L_0x0063
            java.util.LinkedList<com.urbanairship.push.embedded.BoxOfficeClient$HeliumServer> r2 = r5.servers     // Catch:{ all -> 0x006f }
            r2.add(r0)     // Catch:{ all -> 0x006f }
        L_0x0063:
            java.lang.String r2 = r0.address     // Catch:{ all -> 0x006f }
            r5.currentServer = r2     // Catch:{ all -> 0x006f }
            monitor-exit(r1)     // Catch:{ all -> 0x006f }
            java.lang.String r0 = r0.address
            goto L_0x0028
        L_0x006f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x006f }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.urbanairship.push.embedded.BoxOfficeClient.lookup():java.lang.String");
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void processJSONLookup(java.lang.String r11) throws com.urbanairship.push.embedded.BoxOfficeClient.BoxOfficeException {
        /*
            r10 = this;
            r9 = 1
            r8 = 0
            java.lang.String r0 = "Received lookup response from BoxOffice."
            com.urbanairship.Logger.debug(r0)
            org.json.JSONTokener r0 = new org.json.JSONTokener     // Catch:{ JSONException -> 0x0070, NullPointerException -> 0x0084 }
            r0.<init>(r11)     // Catch:{ JSONException -> 0x0070, NullPointerException -> 0x0084 }
            java.lang.Object r0 = r0.nextValue()     // Catch:{ JSONException -> 0x0070, NullPointerException -> 0x0084 }
            org.json.JSONObject r0 = (org.json.JSONObject) r0     // Catch:{ JSONException -> 0x0070, NullPointerException -> 0x0084 }
            java.lang.String r1 = "retry_after"
            boolean r1 = r0.has(r1)     // Catch:{ JSONException -> 0x0070, NullPointerException -> 0x0084 }
            if (r1 == 0) goto L_0x002b
            com.urbanairship.push.embedded.EmbeddedPushManager r1 = com.urbanairship.push.embedded.EmbeddedPushManager.shared()     // Catch:{ JSONException -> 0x0070, NullPointerException -> 0x0084 }
            java.lang.String r2 = "retry_after"
            long r2 = r0.getLong(r2)     // Catch:{ JSONException -> 0x0070, NullPointerException -> 0x0084 }
            r1.setHoldingPattern(r2)     // Catch:{ JSONException -> 0x0070, NullPointerException -> 0x0084 }
            com.urbanairship.push.PushManager.stopService()     // Catch:{ JSONException -> 0x0070, NullPointerException -> 0x0084 }
        L_0x002a:
            return
        L_0x002b:
            java.lang.String r1 = "tut"
            java.lang.String r1 = r0.getString(r1)     // Catch:{ JSONException -> 0x0070, NullPointerException -> 0x0084 }
            java.util.UUID r1 = java.util.UUID.fromString(r1)     // Catch:{ JSONException -> 0x0070, NullPointerException -> 0x0084 }
            com.urbanairship.push.embedded.BoxOfficeClient.currentKey = r1     // Catch:{ JSONException -> 0x0070, NullPointerException -> 0x0084 }
            java.util.LinkedList<com.urbanairship.push.embedded.BoxOfficeClient$HeliumServer> r1 = r10.servers     // Catch:{ JSONException -> 0x0070, NullPointerException -> 0x0084 }
            monitor-enter(r1)     // Catch:{ JSONException -> 0x0070, NullPointerException -> 0x0084 }
            java.util.LinkedList<com.urbanairship.push.embedded.BoxOfficeClient$HeliumServer> r2 = r10.servers     // Catch:{ all -> 0x0081 }
            r2.clear()     // Catch:{ all -> 0x0081 }
            java.lang.String r2 = "servers"
            org.json.JSONArray r2 = r0.getJSONArray(r2)     // Catch:{ all -> 0x0081 }
            int r3 = r2.length()     // Catch:{ all -> 0x0081 }
            r4 = r8
        L_0x004a:
            if (r4 >= r3) goto L_0x005d
            java.util.LinkedList<com.urbanairship.push.embedded.BoxOfficeClient$HeliumServer> r5 = r10.servers     // Catch:{ all -> 0x0081 }
            com.urbanairship.push.embedded.BoxOfficeClient$HeliumServer r6 = new com.urbanairship.push.embedded.BoxOfficeClient$HeliumServer     // Catch:{ all -> 0x0081 }
            java.lang.String r7 = r2.getString(r4)     // Catch:{ all -> 0x0081 }
            r6.<init>(r7)     // Catch:{ all -> 0x0081 }
            r5.add(r6)     // Catch:{ all -> 0x0081 }
            int r4 = r4 + 1
            goto L_0x004a
        L_0x005d:
            monitor-exit(r1)     // Catch:{ all -> 0x0081 }
            java.lang.String r1 = "max_keepalive_interval"
            boolean r1 = r0.has(r1)     // Catch:{ JSONException -> 0x0070, NullPointerException -> 0x0084 }
            if (r1 == 0) goto L_0x002a
            java.lang.String r1 = "max_keepalive_interval"
            int r0 = r0.getInt(r1)     // Catch:{ JSONException -> 0x0070, NullPointerException -> 0x0084 }
            long r0 = (long) r0     // Catch:{ JSONException -> 0x0070, NullPointerException -> 0x0084 }
            com.urbanairship.push.embedded.Config.Helium.max_keepalive_interval = r0     // Catch:{ JSONException -> 0x0070, NullPointerException -> 0x0084 }
            goto L_0x002a
        L_0x0070:
            r0 = move-exception
            com.urbanairship.push.embedded.BoxOfficeClient$BoxOfficeException r0 = new com.urbanairship.push.embedded.BoxOfficeClient$BoxOfficeException
            java.lang.String r1 = "Unparseable JSON: '%s'"
            java.lang.Object[] r2 = new java.lang.Object[r9]
            r2[r8] = r11
            java.lang.String r1 = java.lang.String.format(r1, r2)
            r0.<init>(r1)
            throw r0
        L_0x0081:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0081 }
            throw r0     // Catch:{ JSONException -> 0x0070, NullPointerException -> 0x0084 }
        L_0x0084:
            r0 = move-exception
            com.urbanairship.push.embedded.BoxOfficeClient$BoxOfficeException r0 = new com.urbanairship.push.embedded.BoxOfficeClient$BoxOfficeException
            java.lang.String r1 = "Invalid Response: '%s'"
            java.lang.Object[] r2 = new java.lang.Object[r9]
            r2[r8] = r11
            java.lang.String r1 = java.lang.String.format(r1, r2)
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.urbanairship.push.embedded.BoxOfficeClient.processJSONLookup(java.lang.String):void");
    }

    /* access modifiers changed from: protected */
    public void resetFailureCount(String str) {
        synchronized (this.servers) {
            Iterator<HeliumServer> it = this.servers.iterator();
            while (it.hasNext()) {
                HeliumServer next = it.next();
                if (next.address.equals(str)) {
                    Logger.verbose("Resetting failure count for " + str + " to 0.");
                    int unused = next.failureCount = 0;
                }
            }
        }
    }

    public void setSecret(String str) {
        this.prefs.setPushSecret(str);
    }

    public Response validateResponse(Response response) throws BoxOfficeException {
        switch (response.status()) {
            case HttpResponseCode.OK /*200*/:
                break;
            case HttpResponseCode.NOT_FOUND /*404*/:
                throw new BoxOfficeException("404 - Not Found");
            case HttpResponseCode.INTERNAL_SERVER_ERROR /*500*/:
                throw new BoxOfficeException("Internal Server Error");
            default:
                throw new BoxOfficeException("Bad BoxOffice Response: " + response.status());
        }
        return response;
    }
}
