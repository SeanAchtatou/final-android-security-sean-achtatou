package com.urbanairship.push.embedded;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;

public class Network {
    private static ConnectivityManager connMan() {
        return (ConnectivityManager) UAirship.shared().getApplicationContext().getSystemService("connectivity");
    }

    /* JADX WARNING: Removed duplicated region for block: B:7:0x000c A[Catch:{ SocketException -> 0x004f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getActiveIPAddress() {
        /*
            r0 = 0
            java.util.Enumeration r1 = java.net.NetworkInterface.getNetworkInterfaces()     // Catch:{ SocketException -> 0x0048 }
            r2 = r0
        L_0x0006:
            boolean r0 = r1.hasMoreElements()     // Catch:{ SocketException -> 0x004f }
            if (r0 == 0) goto L_0x0030
            java.lang.Object r0 = r1.nextElement()     // Catch:{ SocketException -> 0x004f }
            java.net.NetworkInterface r0 = (java.net.NetworkInterface) r0     // Catch:{ SocketException -> 0x004f }
            java.util.Enumeration r3 = r0.getInetAddresses()     // Catch:{ SocketException -> 0x004f }
        L_0x0016:
            boolean r0 = r3.hasMoreElements()     // Catch:{ SocketException -> 0x004f }
            if (r0 == 0) goto L_0x0006
            java.lang.Object r0 = r3.nextElement()     // Catch:{ SocketException -> 0x004f }
            java.net.InetAddress r0 = (java.net.InetAddress) r0     // Catch:{ SocketException -> 0x004f }
            boolean r4 = r0.isLoopbackAddress()     // Catch:{ SocketException -> 0x004f }
            if (r4 != 0) goto L_0x0052
            if (r2 != 0) goto L_0x0052
            java.lang.String r0 = r0.getHostAddress()     // Catch:{ SocketException -> 0x004f }
        L_0x002e:
            r2 = r0
            goto L_0x0016
        L_0x0030:
            r0 = r2
        L_0x0031:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Detected active IP address as: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r0)
            java.lang.String r1 = r1.toString()
            com.urbanairship.Logger.verbose(r1)
            return r0
        L_0x0048:
            r1 = move-exception
        L_0x0049:
            java.lang.String r1 = "Error fetching IP address information"
            com.urbanairship.Logger.error(r1)
            goto L_0x0031
        L_0x004f:
            r0 = move-exception
            r0 = r2
            goto L_0x0049
        L_0x0052:
            r0 = r2
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.urbanairship.push.embedded.Network.getActiveIPAddress():java.lang.String");
    }

    private static NetworkInfo info() {
        ConnectivityManager connMan = connMan();
        if (connMan != null) {
            return connMan.getActiveNetworkInfo();
        }
        Logger.error("Error fetching network info.");
        return null;
    }

    public static boolean isConnected() {
        NetworkInfo info = info();
        if (info == null) {
            return false;
        }
        return info.isConnected();
    }

    public static String typeName() {
        NetworkInfo info = info();
        return info == null ? "none" : info.getTypeName();
    }
}
