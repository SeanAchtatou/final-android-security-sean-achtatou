package com.urbanairship.push.embedded;

import com.google.protobuf.InvalidProtocolBufferException;
import com.urbanairship.Logger;
import com.urbanairship.push.embedded.BoxOfficeClient;
import com.urbanairship.push.embedded.Config;
import com.urbanairship.push.embedded.HeliumClient;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public class HeliumConnection extends Thread {
    public static boolean connected = false;
    private BoxOfficeClient boxOffice;
    private HeliumClient helium;
    private long lastRetryInterval = ((long) Config.Helium.initialRetryInterval);
    private EmbeddedPushManager manager;
    private volatile boolean running = false;
    private Socket socket;

    public HeliumConnection(EmbeddedPushManager embeddedPushManager) {
        this.manager = embeddedPushManager;
        this.boxOffice = new BoxOfficeClient();
        setName("HeliumConnectionThread");
    }

    private void close(Socket socket2) {
        connected = false;
        if (socket2 != null) {
            try {
                if (socket2.isConnected() && !socket2.isClosed()) {
                    socket2.close();
                }
            } catch (IOException e) {
                Logger.warn("Error closing socket.");
            }
        }
    }

    private boolean sleepForRetryInterval(long j) {
        long j2 = this.lastRetryInterval;
        long min = System.currentTimeMillis() - j < j2 ? Math.min(j2 * ((long) 4), Config.Helium.maxRetryInterval) : (long) Config.Helium.initialRetryInterval;
        Logger.debug("Rescheduling connection in " + min + "ms.");
        this.lastRetryInterval = min;
        try {
            Thread.sleep(min);
            return true;
        } catch (InterruptedException e) {
            return false;
        }
    }

    public void abort() {
        Logger.debug("Connection aborting.");
        this.running = false;
        Logger.debug("Closing socket.");
        if (this.socket != null) {
            close(this.socket);
        }
        Logger.debug("Service stopped, socket closed successfully.");
    }

    public void resetConnectionIfNecessary() {
        if (this.helium != null && System.currentTimeMillis() - this.helium.getLastKeepAlive() > Config.Helium.max_keepalive_interval * 1000) {
            close(this.socket);
        }
    }

    public void run() {
        Logger.verbose("HeliumConnection - run");
        this.running = true;
        while (this.running) {
            String typeName = Network.typeName();
            this.manager.setIPAddress(Network.getActiveIPAddress());
            long currentTimeMillis = System.currentTimeMillis();
            try {
                String lookup = this.boxOffice.lookup();
                if (lookup == null) {
                    throw new BoxOfficeClient.BoxOfficeException("No Helium servers available for connection.");
                }
                String[] split = lookup.split(":");
                String str = split[0];
                Integer num = new Integer(split[1]);
                if (!this.running) {
                    Logger.debug("Connection sequence aborted. Ending prior to opening Helium connection.");
                    return;
                }
                Logger.debug("Connecting to " + str + ":" + num);
                try {
                    this.socket = new Socket();
                    this.socket.connect(new InetSocketAddress(str, num.intValue()), ((int) Config.Helium.max_keepalive_interval) * 1000);
                    Logger.info("Connection established to " + this.socket.getInetAddress() + ":" + num + " on network type " + typeName);
                    connected = true;
                    this.helium = new HeliumClient(this.socket, this.boxOffice);
                    this.helium.register();
                    while (this.running) {
                        this.helium.readResponse();
                        Thread.sleep((long) Config.Helium.readSleep);
                    }
                    if (!this.running) {
                        Logger.debug("Connection aborted, shutting down. Network type=" + typeName);
                    } else {
                        close(this.socket);
                        this.boxOffice.incrementFailureCount();
                        if (!Network.isConnected() || this.manager.isInHoldingPattern()) {
                            this.running = false;
                        } else if (!sleepForRetryInterval(currentTimeMillis)) {
                            this.running = false;
                            return;
                        }
                    }
                } catch (InterruptedException e) {
                    Logger.debug("Connection thread interrupted.");
                    this.running = false;
                    if (!this.running) {
                        Logger.debug("Connection aborted, shutting down. Network type=" + typeName);
                        return;
                    }
                    close(this.socket);
                    this.boxOffice.incrementFailureCount();
                    if (!Network.isConnected() || this.manager.isInHoldingPattern()) {
                        this.running = false;
                        return;
                    } else if (!sleepForRetryInterval(currentTimeMillis)) {
                        this.running = false;
                        return;
                    }
                } catch (InvalidProtocolBufferException e2) {
                    Logger.debug("Invalid protobuf (expected, likely due to interruption).");
                    if (!this.running) {
                        Logger.debug("Connection aborted, shutting down. Network type=" + typeName);
                    } else {
                        close(this.socket);
                        this.boxOffice.incrementFailureCount();
                        if (!Network.isConnected() || this.manager.isInHoldingPattern()) {
                            this.running = false;
                        } else if (!sleepForRetryInterval(currentTimeMillis)) {
                            this.running = false;
                            return;
                        }
                    }
                } catch (InterruptedIOException e3) {
                    Logger.debug("Socket timed out.");
                    if (!this.running) {
                        Logger.debug("Connection aborted, shutting down. Network type=" + typeName);
                    } else {
                        close(this.socket);
                        this.boxOffice.incrementFailureCount();
                        if (!Network.isConnected() || this.manager.isInHoldingPattern()) {
                            this.running = false;
                        } else if (!sleepForRetryInterval(currentTimeMillis)) {
                            this.running = false;
                            return;
                        }
                    }
                } catch (IOException e4) {
                    Logger.debug("IOException (Expected following restart or connectivity change).");
                    if (!this.running) {
                        Logger.debug("Connection aborted, shutting down. Network type=" + typeName);
                    } else {
                        close(this.socket);
                        this.boxOffice.incrementFailureCount();
                        if (!Network.isConnected() || this.manager.isInHoldingPattern()) {
                            this.running = false;
                        } else if (!sleepForRetryInterval(currentTimeMillis)) {
                            this.running = false;
                            return;
                        }
                    }
                } catch (HeliumClient.HeliumException e5) {
                    Logger.debug("Helium exception - secret not set.");
                    if (!this.running) {
                        Logger.debug("Connection aborted, shutting down. Network type=" + typeName);
                    } else {
                        close(this.socket);
                        this.boxOffice.incrementFailureCount();
                        if (!Network.isConnected() || this.manager.isInHoldingPattern()) {
                            this.running = false;
                        } else if (!sleepForRetryInterval(currentTimeMillis)) {
                            this.running = false;
                            return;
                        }
                    }
                } catch (Exception e6) {
                    Logger.debug("Exception in Helium connection. Network type=" + typeName + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + e6.getMessage());
                    if (!this.running) {
                        Logger.debug("Connection aborted, shutting down. Network type=" + typeName);
                    } else {
                        close(this.socket);
                        this.boxOffice.incrementFailureCount();
                        if (!Network.isConnected() || this.manager.isInHoldingPattern()) {
                            this.running = false;
                        } else if (!sleepForRetryInterval(currentTimeMillis)) {
                            this.running = false;
                            return;
                        }
                    }
                } catch (Throwable th) {
                    if (!this.running) {
                        Logger.debug("Connection aborted, shutting down. Network type=" + typeName);
                    } else {
                        close(this.socket);
                        this.boxOffice.incrementFailureCount();
                        if (!Network.isConnected() || this.manager.isInHoldingPattern()) {
                            this.running = false;
                        } else if (!sleepForRetryInterval(currentTimeMillis)) {
                            this.running = false;
                            return;
                        }
                    }
                    throw th;
                }
            } catch (BoxOfficeClient.BoxOfficeException e7) {
                if (!sleepForRetryInterval(currentTimeMillis)) {
                    this.running = false;
                    return;
                }
            }
        }
    }
}
