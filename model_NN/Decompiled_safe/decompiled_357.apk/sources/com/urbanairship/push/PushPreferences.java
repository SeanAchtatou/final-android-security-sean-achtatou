package com.urbanairship.push;

import com.urbanairship.Preferences;
import com.urbanairship.UAirship;
import com.urbanairship.analytics.PushPreferencesChangedEvent;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;

public class PushPreferences extends Preferences {
    private static final String ALIAS_KEY = "com.urbanairship.push.ALIAS";
    private static final String APID_KEY = "com.urbanairship.push.APID";
    private static final String APID_UPDATE_NEEDED_KEY = "com.urbanairship.push.APID_UPDATE_NEEDED";
    private static final String BOX_OFFICE_SECRET_KEY = "com.urbanairship.push.BOX_OFFICE_SECRET";
    private static final String C2DM_ID_KEY = "com.urbanairship.push.C2DM_KEY";
    private static final String PUSH_ENABLED_KEY = "com.urbanairship.push.PUSH_ENABLED";
    private static final String RETRY_AFTER_KEY = "com.urbanairship.push.RETRY_AFTER";
    private static final String SOUND_ENABLED_KEY = "com.urbanairship.push.SOUND_ENABLED";
    private static final String TAGS_KEY = "com.urbanairship.push.TAGS";
    private static final String VIBRATE_ENABLED_KEY = "com.urbanairship.push.VIBRATE_ENABLED";

    private static final class QuietTime {
        public static final String ENABLED = "com.urbanairship.push.QuietTime.ENABLED";
        public static final String END_HOUR_KEY = "com.urbanairship.push.QuietTime.END_HOUR";
        public static final String END_MIN_KEY = "com.urbanairship.push.QuietTime.END_MINUTE";
        public static final int NOT_SET_VAL = -1;
        public static final String START_HOUR_KEY = "com.urbanairship.push.QuietTime.START_HOUR";
        public static final String START_MIN_KEY = "com.urbanairship.push.QuietTime.START_MINUTE";

        private QuietTime() {
        }
    }

    public PushPreferences() {
        super("com.urbanairship.push");
    }

    private void sendPrefsChangedEvent() {
        UAirship.shared().getAnalytics().addEvent(new PushPreferencesChangedEvent());
    }

    public boolean beQuiet() {
        if (!isQuietTimeEnabled()) {
            return false;
        }
        Date date = new Date();
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        int i = getInt(QuietTime.START_HOUR_KEY, -1);
        int i2 = getInt(QuietTime.START_MIN_KEY, -1);
        int i3 = getInt(QuietTime.END_HOUR_KEY, -1);
        int i4 = getInt(QuietTime.END_MIN_KEY, -1);
        if (-1 == i || -1 == i2 || -1 == i3 || -1 == i4) {
            return false;
        }
        Calendar instance2 = Calendar.getInstance();
        instance2.setTime(date);
        instance2.set(11, i);
        instance2.set(12, i2);
        Calendar instance3 = Calendar.getInstance();
        instance3.setTime(date);
        instance3.set(11, i3);
        instance3.set(12, i4);
        if (instance2.after(instance) && instance3.before(instance2)) {
            instance2.add(6, -1);
        }
        if (instance3.before(instance2)) {
            instance3.add(6, 1);
        }
        return instance.after(instance2) && instance.before(instance3);
    }

    public String getAlias() {
        return getString(ALIAS_KEY, null);
    }

    public String getC2DMId() {
        return getString(C2DM_ID_KEY, null);
    }

    public String getPushId() {
        return getString("com.urbanairship.push.APID", null);
    }

    public String getPushSecret() {
        return getString(BOX_OFFICE_SECRET_KEY, null);
    }

    public Date[] getQuietTimeInterval() {
        int i = getInt(QuietTime.START_HOUR_KEY, -1);
        int i2 = getInt(QuietTime.START_MIN_KEY, -1);
        int i3 = getInt(QuietTime.END_HOUR_KEY, -1);
        int i4 = getInt(QuietTime.END_MIN_KEY, -1);
        if (i == -1 || i2 == -1 || i3 == -1 || i4 == -1) {
            return null;
        }
        Calendar instance = Calendar.getInstance();
        instance.setTime(new Date());
        instance.set(11, i);
        instance.set(12, i2);
        Date time = instance.getTime();
        Calendar instance2 = Calendar.getInstance();
        instance2.setTime(new Date());
        instance2.set(11, i3);
        instance2.set(12, i4);
        if (i3 < i) {
            instance2.add(5, 1);
        }
        return new Date[]{time, instance2.getTime()};
    }

    public long getRetryAfter() {
        return getLong(RETRY_AFTER_KEY, 0);
    }

    public Set<String> getTags() {
        HashSet hashSet = new HashSet();
        String string = getString(TAGS_KEY, null);
        if (string != null) {
            try {
                JSONArray jSONArray = new JSONArray(string);
                int length = jSONArray.length();
                for (int i = 0; i < length; i++) {
                    hashSet.add(jSONArray.getString(i));
                }
            } catch (JSONException e) {
            }
        }
        return hashSet;
    }

    /* access modifiers changed from: package-private */
    public boolean isApidUpdateNeeded() {
        return getBoolean(APID_UPDATE_NEEDED_KEY, true);
    }

    public boolean isPushEnabled() {
        return getBoolean(PUSH_ENABLED_KEY, false);
    }

    public boolean isQuietTimeEnabled() {
        return getBoolean(QuietTime.ENABLED, false);
    }

    public boolean isSoundEnabled() {
        return getBoolean(SOUND_ENABLED_KEY, true);
    }

    public boolean isVibrateEnabled() {
        return getBoolean(VIBRATE_ENABLED_KEY, true);
    }

    public boolean setAlias(String str) {
        return putString(ALIAS_KEY, str);
    }

    /* access modifiers changed from: package-private */
    public boolean setApidUpdateNeeded(boolean z) {
        return putBoolean(APID_UPDATE_NEEDED_KEY, z);
    }

    public boolean setC2DMId(String str) {
        return putString(C2DM_ID_KEY, str);
    }

    /* access modifiers changed from: package-private */
    public boolean setPushEnabled(boolean z) {
        if (isPushEnabled() == z) {
            return true;
        }
        sendPrefsChangedEvent();
        return putBoolean(PUSH_ENABLED_KEY, z);
    }

    public boolean setPushId(String str) {
        return putString("com.urbanairship.push.APID", str);
    }

    public boolean setPushSecret(String str) {
        return putString(BOX_OFFICE_SECRET_KEY, str);
    }

    public boolean setQuietTimeEnabled(boolean z) {
        return putBoolean(QuietTime.ENABLED, z);
    }

    public boolean setQuietTimeInterval(Date date, Date date2) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        int i = instance.get(11);
        int i2 = instance.get(12);
        Calendar instance2 = Calendar.getInstance();
        instance2.setTime(date2);
        int i3 = instance2.get(11);
        int i4 = instance2.get(12);
        sendPrefsChangedEvent();
        return putInt(QuietTime.START_HOUR_KEY, i) && putInt(QuietTime.START_MIN_KEY, i2) && putInt(QuietTime.END_HOUR_KEY, i3) && putInt(QuietTime.END_MIN_KEY, i4);
    }

    public boolean setRetryAfter(long j) {
        return putLong(RETRY_AFTER_KEY, j);
    }

    public boolean setSoundEnabled(boolean z) {
        if (isSoundEnabled() == z) {
            return true;
        }
        sendPrefsChangedEvent();
        return putBoolean(SOUND_ENABLED_KEY, z);
    }

    public boolean setTags(Set<String> set) {
        if (set == null) {
            return false;
        }
        return putString(TAGS_KEY, new JSONArray((Collection) set).toString());
    }

    public boolean setVibrateEnabled(boolean z) {
        if (isVibrateEnabled() == z) {
            return true;
        }
        sendPrefsChangedEvent();
        return putBoolean(VIBRATE_ENABLED_KEY, z);
    }
}
