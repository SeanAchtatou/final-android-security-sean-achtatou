package com.urbanairship.push.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.urbanairship.push.proto.Messages;
import java.io.IOException;
import java.io.InputStream;
import org.codehaus.jackson.org.objectweb.asm.Opcodes;

public final class Rpc {

    public static final class Request extends GeneratedMessageLite {
        public static final int BODY_FIELD_NUMBER = 2;
        public static final int METHOD_ID_FIELD_NUMBER = 1;
        public static final int REQUEST_ID_FIELD_NUMBER = 3;
        private static final Request defaultInstance = new Request(true);
        /* access modifiers changed from: private */
        public ByteString body_;
        /* access modifiers changed from: private */
        public boolean hasBody;
        /* access modifiers changed from: private */
        public boolean hasMethodId;
        /* access modifiers changed from: private */
        public boolean hasRequestId;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public Messages.AirshipMethod methodId_;
        /* access modifiers changed from: private */
        public String requestId_;

        public static final class Builder extends GeneratedMessageLite.Builder<Request, Builder> {
            private Request result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public Request buildParsed() throws InvalidProtocolBufferException {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException(this.result).asInvalidProtocolBufferException();
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new Request();
                return builder;
            }

            public Request build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException(this.result);
            }

            public Request buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                Request request = this.result;
                this.result = null;
                return request;
            }

            public Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new Request();
                return this;
            }

            public Builder clearBody() {
                boolean unused = this.result.hasBody = false;
                ByteString unused2 = this.result.body_ = Request.getDefaultInstance().getBody();
                return this;
            }

            public Builder clearMethodId() {
                boolean unused = this.result.hasMethodId = false;
                Messages.AirshipMethod unused2 = this.result.methodId_ = Messages.AirshipMethod.REGISTER;
                return this;
            }

            public Builder clearRequestId() {
                boolean unused = this.result.hasRequestId = false;
                String unused2 = this.result.requestId_ = Request.getDefaultInstance().getRequestId();
                return this;
            }

            public Builder clone() {
                return create().mergeFrom(this.result);
            }

            public ByteString getBody() {
                return this.result.getBody();
            }

            public Request getDefaultInstanceForType() {
                return Request.getDefaultInstance();
            }

            public Messages.AirshipMethod getMethodId() {
                return this.result.getMethodId();
            }

            public String getRequestId() {
                return this.result.getRequestId();
            }

            public boolean hasBody() {
                return this.result.hasBody();
            }

            public boolean hasMethodId() {
                return this.result.hasMethodId();
            }

            public boolean hasRequestId() {
                return this.result.hasRequestId();
            }

            /* access modifiers changed from: protected */
            public Request internalGetResult() {
                return this.result;
            }

            public boolean isInitialized() {
                return this.result.isInitialized();
            }

            public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                while (true) {
                    int readTag = codedInputStream.readTag();
                    switch (readTag) {
                        case 0:
                            return this;
                        case 8:
                            Messages.AirshipMethod valueOf = Messages.AirshipMethod.valueOf(codedInputStream.readEnum());
                            if (valueOf == null) {
                                break;
                            } else {
                                setMethodId(valueOf);
                                break;
                            }
                        case Opcodes.LDC /*18*/:
                            setBody(codedInputStream.readBytes());
                            break;
                        case 26:
                            setRequestId(codedInputStream.readString());
                            break;
                        default:
                            if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                                break;
                            } else {
                                return this;
                            }
                    }
                }
            }

            public Builder mergeFrom(Request request) {
                if (request == Request.getDefaultInstance()) {
                    return this;
                }
                if (request.hasMethodId()) {
                    setMethodId(request.getMethodId());
                }
                if (request.hasBody()) {
                    setBody(request.getBody());
                }
                if (request.hasRequestId()) {
                    setRequestId(request.getRequestId());
                }
                return this;
            }

            public Builder setBody(ByteString byteString) {
                if (byteString == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasBody = true;
                ByteString unused2 = this.result.body_ = byteString;
                return this;
            }

            public Builder setMethodId(Messages.AirshipMethod airshipMethod) {
                if (airshipMethod == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasMethodId = true;
                Messages.AirshipMethod unused2 = this.result.methodId_ = airshipMethod;
                return this;
            }

            public Builder setRequestId(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasRequestId = true;
                String unused2 = this.result.requestId_ = str;
                return this;
            }
        }

        static {
            Rpc.internalForceInit();
            defaultInstance.initFields();
        }

        private Request() {
            this.body_ = ByteString.EMPTY;
            this.requestId_ = "";
            this.memoizedSerializedSize = -1;
            initFields();
        }

        private Request(boolean z) {
            this.body_ = ByteString.EMPTY;
            this.requestId_ = "";
            this.memoizedSerializedSize = -1;
        }

        public static Request getDefaultInstance() {
            return defaultInstance;
        }

        private void initFields() {
            this.methodId_ = Messages.AirshipMethod.REGISTER;
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public static Builder newBuilder(Request request) {
            return newBuilder().mergeFrom(request);
        }

        public static Request parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static Request parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, extensionRegistryLite)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static Request parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(byteString)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Rpc.Request.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Rpc.Request.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Rpc.Request.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Rpc$Request$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static Request parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(byteString, extensionRegistryLite)).buildParsed();
        }

        public static Request parseFrom(CodedInputStream codedInputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(codedInputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.urbanairship.push.proto.Rpc.Request.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Rpc$Request$Builder
         arg types: [com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Rpc.Request.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Rpc.Request.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Rpc.Request.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Rpc$Request$Builder */
        public static Request parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return newBuilder().mergeFrom(codedInputStream, extensionRegistryLite).buildParsed();
        }

        public static Request parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Rpc.Request.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Rpc.Request.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Rpc.Request.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Rpc$Request$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static Request parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, extensionRegistryLite)).buildParsed();
        }

        public static Request parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [byte[], com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Rpc.Request.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Rpc.Request.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Rpc.Request.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Rpc$Request$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static Request parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(bArr, extensionRegistryLite)).buildParsed();
        }

        public ByteString getBody() {
            return this.body_;
        }

        public Request getDefaultInstanceForType() {
            return defaultInstance;
        }

        public Messages.AirshipMethod getMethodId() {
            return this.methodId_;
        }

        public String getRequestId() {
            return this.requestId_;
        }

        public int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i == -1) {
                i = 0;
                if (hasMethodId()) {
                    i = 0 + CodedOutputStream.computeEnumSize(1, getMethodId().getNumber());
                }
                if (hasBody()) {
                    i += CodedOutputStream.computeBytesSize(2, getBody());
                }
                if (hasRequestId()) {
                    i += CodedOutputStream.computeStringSize(3, getRequestId());
                }
                this.memoizedSerializedSize = i;
            }
            return i;
        }

        public boolean hasBody() {
            return this.hasBody;
        }

        public boolean hasMethodId() {
            return this.hasMethodId;
        }

        public boolean hasRequestId() {
            return this.hasRequestId;
        }

        public final boolean isInitialized() {
            if (!this.hasMethodId) {
                return false;
            }
            return this.hasBody;
        }

        public Builder newBuilderForType() {
            return newBuilder();
        }

        public Builder toBuilder() {
            return newBuilder(this);
        }

        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            getSerializedSize();
            if (hasMethodId()) {
                codedOutputStream.writeEnum(1, getMethodId().getNumber());
            }
            if (hasBody()) {
                codedOutputStream.writeBytes(2, getBody());
            }
            if (hasRequestId()) {
                codedOutputStream.writeString(3, getRequestId());
            }
        }
    }

    public static final class Response extends GeneratedMessageLite {
        public static final int BODY_FIELD_NUMBER = 2;
        public static final int REQUEST_ID_FIELD_NUMBER = 3;
        public static final int STATUS_FIELD_NUMBER = 1;
        private static final Response defaultInstance = new Response(true);
        /* access modifiers changed from: private */
        public ByteString body_;
        /* access modifiers changed from: private */
        public boolean hasBody;
        /* access modifiers changed from: private */
        public boolean hasRequestId;
        /* access modifiers changed from: private */
        public boolean hasStatus;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public String requestId_;
        /* access modifiers changed from: private */
        public Status status_;

        public static final class Builder extends GeneratedMessageLite.Builder<Response, Builder> {
            private Response result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public Response buildParsed() throws InvalidProtocolBufferException {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException(this.result).asInvalidProtocolBufferException();
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new Response();
                return builder;
            }

            public Response build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException(this.result);
            }

            public Response buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                Response response = this.result;
                this.result = null;
                return response;
            }

            public Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new Response();
                return this;
            }

            public Builder clearBody() {
                boolean unused = this.result.hasBody = false;
                ByteString unused2 = this.result.body_ = Response.getDefaultInstance().getBody();
                return this;
            }

            public Builder clearRequestId() {
                boolean unused = this.result.hasRequestId = false;
                String unused2 = this.result.requestId_ = Response.getDefaultInstance().getRequestId();
                return this;
            }

            public Builder clearStatus() {
                boolean unused = this.result.hasStatus = false;
                Status unused2 = this.result.status_ = Status.OK;
                return this;
            }

            public Builder clone() {
                return create().mergeFrom(this.result);
            }

            public ByteString getBody() {
                return this.result.getBody();
            }

            public Response getDefaultInstanceForType() {
                return Response.getDefaultInstance();
            }

            public String getRequestId() {
                return this.result.getRequestId();
            }

            public Status getStatus() {
                return this.result.getStatus();
            }

            public boolean hasBody() {
                return this.result.hasBody();
            }

            public boolean hasRequestId() {
                return this.result.hasRequestId();
            }

            public boolean hasStatus() {
                return this.result.hasStatus();
            }

            /* access modifiers changed from: protected */
            public Response internalGetResult() {
                return this.result;
            }

            public boolean isInitialized() {
                return this.result.isInitialized();
            }

            public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                while (true) {
                    int readTag = codedInputStream.readTag();
                    switch (readTag) {
                        case 0:
                            return this;
                        case 8:
                            Status valueOf = Status.valueOf(codedInputStream.readEnum());
                            if (valueOf == null) {
                                break;
                            } else {
                                setStatus(valueOf);
                                break;
                            }
                        case Opcodes.LDC /*18*/:
                            setBody(codedInputStream.readBytes());
                            break;
                        case 26:
                            setRequestId(codedInputStream.readString());
                            break;
                        default:
                            if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                                break;
                            } else {
                                return this;
                            }
                    }
                }
            }

            public Builder mergeFrom(Response response) {
                if (response == Response.getDefaultInstance()) {
                    return this;
                }
                if (response.hasStatus()) {
                    setStatus(response.getStatus());
                }
                if (response.hasBody()) {
                    setBody(response.getBody());
                }
                if (response.hasRequestId()) {
                    setRequestId(response.getRequestId());
                }
                return this;
            }

            public Builder setBody(ByteString byteString) {
                if (byteString == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasBody = true;
                ByteString unused2 = this.result.body_ = byteString;
                return this;
            }

            public Builder setRequestId(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasRequestId = true;
                String unused2 = this.result.requestId_ = str;
                return this;
            }

            public Builder setStatus(Status status) {
                if (status == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasStatus = true;
                Status unused2 = this.result.status_ = status;
                return this;
            }
        }

        static {
            Rpc.internalForceInit();
            defaultInstance.initFields();
        }

        private Response() {
            this.body_ = ByteString.EMPTY;
            this.requestId_ = "";
            this.memoizedSerializedSize = -1;
            initFields();
        }

        private Response(boolean z) {
            this.body_ = ByteString.EMPTY;
            this.requestId_ = "";
            this.memoizedSerializedSize = -1;
        }

        public static Response getDefaultInstance() {
            return defaultInstance;
        }

        private void initFields() {
            this.status_ = Status.OK;
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public static Builder newBuilder(Response response) {
            return newBuilder().mergeFrom(response);
        }

        public static Response parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static Response parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, extensionRegistryLite)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static Response parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(byteString)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Rpc.Response.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Rpc.Response.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Rpc.Response.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Rpc$Response$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static Response parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(byteString, extensionRegistryLite)).buildParsed();
        }

        public static Response parseFrom(CodedInputStream codedInputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(codedInputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.urbanairship.push.proto.Rpc.Response.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Rpc$Response$Builder
         arg types: [com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Rpc.Response.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Rpc.Response.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Rpc.Response.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Rpc$Response$Builder */
        public static Response parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return newBuilder().mergeFrom(codedInputStream, extensionRegistryLite).buildParsed();
        }

        public static Response parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Rpc.Response.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Rpc.Response.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Rpc.Response.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Rpc$Response$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static Response parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, extensionRegistryLite)).buildParsed();
        }

        public static Response parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [byte[], com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Rpc.Response.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Rpc.Response.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Rpc.Response.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Rpc$Response$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static Response parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(bArr, extensionRegistryLite)).buildParsed();
        }

        public ByteString getBody() {
            return this.body_;
        }

        public Response getDefaultInstanceForType() {
            return defaultInstance;
        }

        public String getRequestId() {
            return this.requestId_;
        }

        public int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i == -1) {
                i = 0;
                if (hasStatus()) {
                    i = 0 + CodedOutputStream.computeEnumSize(1, getStatus().getNumber());
                }
                if (hasBody()) {
                    i += CodedOutputStream.computeBytesSize(2, getBody());
                }
                if (hasRequestId()) {
                    i += CodedOutputStream.computeStringSize(3, getRequestId());
                }
                this.memoizedSerializedSize = i;
            }
            return i;
        }

        public Status getStatus() {
            return this.status_;
        }

        public boolean hasBody() {
            return this.hasBody;
        }

        public boolean hasRequestId() {
            return this.hasRequestId;
        }

        public boolean hasStatus() {
            return this.hasStatus;
        }

        public final boolean isInitialized() {
            return this.hasStatus;
        }

        public Builder newBuilderForType() {
            return newBuilder();
        }

        public Builder toBuilder() {
            return newBuilder(this);
        }

        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            getSerializedSize();
            if (hasStatus()) {
                codedOutputStream.writeEnum(1, getStatus().getNumber());
            }
            if (hasBody()) {
                codedOutputStream.writeBytes(2, getBody());
            }
            if (hasRequestId()) {
                codedOutputStream.writeString(3, getRequestId());
            }
        }
    }

    public enum Status implements Internal.EnumLite {
        OK(0, 0),
        BAD_REQUEST(1, 1),
        METHOD_NOT_FOUND(2, 2),
        RPC_ERROR(3, 3),
        TIMEOUT(4, 4);
        
        private static Internal.EnumLiteMap<Status> internalValueMap = new Internal.EnumLiteMap<Status>() {
            public Status findValueByNumber(int i) {
                return Status.valueOf(i);
            }
        };
        private final int index;
        private final int value;

        private Status(int i, int i2) {
            this.index = i;
            this.value = i2;
        }

        public static Internal.EnumLiteMap<Status> internalGetValueMap() {
            return internalValueMap;
        }

        public static Status valueOf(int i) {
            switch (i) {
                case 0:
                    return OK;
                case 1:
                    return BAD_REQUEST;
                case 2:
                    return METHOD_NOT_FOUND;
                case 3:
                    return RPC_ERROR;
                case 4:
                    return TIMEOUT;
                default:
                    return null;
            }
        }

        public final int getNumber() {
            return this.value;
        }
    }

    private Rpc() {
    }

    public static void internalForceInit() {
    }

    public static void registerAllExtensions(ExtensionRegistryLite extensionRegistryLite) {
    }
}
