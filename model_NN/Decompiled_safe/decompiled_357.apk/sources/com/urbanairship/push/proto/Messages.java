package com.urbanairship.push.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.codehaus.jackson.org.objectweb.asm.Opcodes;

public final class Messages {

    public enum AirshipMethod implements Internal.EnumLite {
        REGISTER(0, 0),
        DEVICE_CONNECT(1, 1),
        PUSH_NOTIFICATION(2, 2),
        ECHO(3, 3),
        HELLO(4, 4);
        
        private static Internal.EnumLiteMap<AirshipMethod> internalValueMap = new Internal.EnumLiteMap<AirshipMethod>() {
            public AirshipMethod findValueByNumber(int i) {
                return AirshipMethod.valueOf(i);
            }
        };
        private final int index;
        private final int value;

        private AirshipMethod(int i, int i2) {
            this.index = i;
            this.value = i2;
        }

        public static Internal.EnumLiteMap<AirshipMethod> internalGetValueMap() {
            return internalValueMap;
        }

        public static AirshipMethod valueOf(int i) {
            switch (i) {
                case 0:
                    return REGISTER;
                case 1:
                    return DEVICE_CONNECT;
                case 2:
                    return PUSH_NOTIFICATION;
                case 3:
                    return ECHO;
                case 4:
                    return HELLO;
                default:
                    return null;
            }
        }

        public final int getNumber() {
            return this.value;
        }
    }

    public static final class DeviceConnect extends GeneratedMessageLite {
        public static final int DEVICE_ID_FIELD_NUMBER = 1;
        private static final DeviceConnect defaultInstance = new DeviceConnect(true);
        /* access modifiers changed from: private */
        public String deviceId_;
        /* access modifiers changed from: private */
        public boolean hasDeviceId;
        private int memoizedSerializedSize;

        public static final class Builder extends GeneratedMessageLite.Builder<DeviceConnect, Builder> {
            private DeviceConnect result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public DeviceConnect buildParsed() throws InvalidProtocolBufferException {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException(this.result).asInvalidProtocolBufferException();
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new DeviceConnect();
                return builder;
            }

            public DeviceConnect build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException(this.result);
            }

            public DeviceConnect buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                DeviceConnect deviceConnect = this.result;
                this.result = null;
                return deviceConnect;
            }

            public Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new DeviceConnect();
                return this;
            }

            public Builder clearDeviceId() {
                boolean unused = this.result.hasDeviceId = false;
                String unused2 = this.result.deviceId_ = DeviceConnect.getDefaultInstance().getDeviceId();
                return this;
            }

            public Builder clone() {
                return create().mergeFrom(this.result);
            }

            public DeviceConnect getDefaultInstanceForType() {
                return DeviceConnect.getDefaultInstance();
            }

            public String getDeviceId() {
                return this.result.getDeviceId();
            }

            public boolean hasDeviceId() {
                return this.result.hasDeviceId();
            }

            /* access modifiers changed from: protected */
            public DeviceConnect internalGetResult() {
                return this.result;
            }

            public boolean isInitialized() {
                return this.result.isInitialized();
            }

            public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                while (true) {
                    int readTag = codedInputStream.readTag();
                    switch (readTag) {
                        case 0:
                            return this;
                        case 10:
                            setDeviceId(codedInputStream.readString());
                            break;
                        default:
                            if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                                break;
                            } else {
                                return this;
                            }
                    }
                }
            }

            public Builder mergeFrom(DeviceConnect deviceConnect) {
                if (deviceConnect == DeviceConnect.getDefaultInstance()) {
                    return this;
                }
                if (deviceConnect.hasDeviceId()) {
                    setDeviceId(deviceConnect.getDeviceId());
                }
                return this;
            }

            public Builder setDeviceId(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasDeviceId = true;
                String unused2 = this.result.deviceId_ = str;
                return this;
            }
        }

        static {
            Messages.internalForceInit();
            defaultInstance.initFields();
        }

        private DeviceConnect() {
            this.deviceId_ = "";
            this.memoizedSerializedSize = -1;
            initFields();
        }

        private DeviceConnect(boolean z) {
            this.deviceId_ = "";
            this.memoizedSerializedSize = -1;
        }

        public static DeviceConnect getDefaultInstance() {
            return defaultInstance;
        }

        private void initFields() {
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public static Builder newBuilder(DeviceConnect deviceConnect) {
            return newBuilder().mergeFrom(deviceConnect);
        }

        public static DeviceConnect parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static DeviceConnect parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, extensionRegistryLite)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static DeviceConnect parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(byteString)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.DeviceConnect.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.DeviceConnect.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.DeviceConnect.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$DeviceConnect$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static DeviceConnect parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(byteString, extensionRegistryLite)).buildParsed();
        }

        public static DeviceConnect parseFrom(CodedInputStream codedInputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(codedInputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.urbanairship.push.proto.Messages.DeviceConnect.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$DeviceConnect$Builder
         arg types: [com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.DeviceConnect.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.DeviceConnect.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.DeviceConnect.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$DeviceConnect$Builder */
        public static DeviceConnect parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return newBuilder().mergeFrom(codedInputStream, extensionRegistryLite).buildParsed();
        }

        public static DeviceConnect parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.DeviceConnect.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.DeviceConnect.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.DeviceConnect.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$DeviceConnect$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static DeviceConnect parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, extensionRegistryLite)).buildParsed();
        }

        public static DeviceConnect parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [byte[], com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.DeviceConnect.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.DeviceConnect.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.DeviceConnect.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$DeviceConnect$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static DeviceConnect parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(bArr, extensionRegistryLite)).buildParsed();
        }

        public DeviceConnect getDefaultInstanceForType() {
            return defaultInstance;
        }

        public String getDeviceId() {
            return this.deviceId_;
        }

        public int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i == -1) {
                i = 0;
                if (hasDeviceId()) {
                    i = 0 + CodedOutputStream.computeStringSize(1, getDeviceId());
                }
                this.memoizedSerializedSize = i;
            }
            return i;
        }

        public boolean hasDeviceId() {
            return this.hasDeviceId;
        }

        public final boolean isInitialized() {
            return this.hasDeviceId;
        }

        public Builder newBuilderForType() {
            return newBuilder();
        }

        public Builder toBuilder() {
            return newBuilder(this);
        }

        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            getSerializedSize();
            if (hasDeviceId()) {
                codedOutputStream.writeString(1, getDeviceId());
            }
        }
    }

    public static final class Hello extends GeneratedMessageLite {
        public static final int MESSAGE_FIELD_NUMBER = 1;
        private static final Hello defaultInstance = new Hello(true);
        /* access modifiers changed from: private */
        public boolean hasMessage;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public String message_;

        public static final class Builder extends GeneratedMessageLite.Builder<Hello, Builder> {
            private Hello result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public Hello buildParsed() throws InvalidProtocolBufferException {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException(this.result).asInvalidProtocolBufferException();
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new Hello();
                return builder;
            }

            public Hello build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException(this.result);
            }

            public Hello buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                Hello hello = this.result;
                this.result = null;
                return hello;
            }

            public Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new Hello();
                return this;
            }

            public Builder clearMessage() {
                boolean unused = this.result.hasMessage = false;
                String unused2 = this.result.message_ = Hello.getDefaultInstance().getMessage();
                return this;
            }

            public Builder clone() {
                return create().mergeFrom(this.result);
            }

            public Hello getDefaultInstanceForType() {
                return Hello.getDefaultInstance();
            }

            public String getMessage() {
                return this.result.getMessage();
            }

            public boolean hasMessage() {
                return this.result.hasMessage();
            }

            /* access modifiers changed from: protected */
            public Hello internalGetResult() {
                return this.result;
            }

            public boolean isInitialized() {
                return this.result.isInitialized();
            }

            public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                while (true) {
                    int readTag = codedInputStream.readTag();
                    switch (readTag) {
                        case 0:
                            return this;
                        case 10:
                            setMessage(codedInputStream.readString());
                            break;
                        default:
                            if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                                break;
                            } else {
                                return this;
                            }
                    }
                }
            }

            public Builder mergeFrom(Hello hello) {
                if (hello == Hello.getDefaultInstance()) {
                    return this;
                }
                if (hello.hasMessage()) {
                    setMessage(hello.getMessage());
                }
                return this;
            }

            public Builder setMessage(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasMessage = true;
                String unused2 = this.result.message_ = str;
                return this;
            }
        }

        static {
            Messages.internalForceInit();
            defaultInstance.initFields();
        }

        private Hello() {
            this.message_ = "";
            this.memoizedSerializedSize = -1;
            initFields();
        }

        private Hello(boolean z) {
            this.message_ = "";
            this.memoizedSerializedSize = -1;
        }

        public static Hello getDefaultInstance() {
            return defaultInstance;
        }

        private void initFields() {
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public static Builder newBuilder(Hello hello) {
            return newBuilder().mergeFrom(hello);
        }

        public static Hello parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static Hello parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, extensionRegistryLite)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static Hello parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(byteString)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.Hello.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.Hello.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.Hello.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$Hello$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static Hello parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(byteString, extensionRegistryLite)).buildParsed();
        }

        public static Hello parseFrom(CodedInputStream codedInputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(codedInputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.urbanairship.push.proto.Messages.Hello.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$Hello$Builder
         arg types: [com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.Hello.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.Hello.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.Hello.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$Hello$Builder */
        public static Hello parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return newBuilder().mergeFrom(codedInputStream, extensionRegistryLite).buildParsed();
        }

        public static Hello parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.Hello.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.Hello.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.Hello.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$Hello$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static Hello parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, extensionRegistryLite)).buildParsed();
        }

        public static Hello parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [byte[], com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.Hello.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.Hello.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.Hello.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$Hello$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static Hello parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(bArr, extensionRegistryLite)).buildParsed();
        }

        public Hello getDefaultInstanceForType() {
            return defaultInstance;
        }

        public String getMessage() {
            return this.message_;
        }

        public int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i == -1) {
                i = 0;
                if (hasMessage()) {
                    i = 0 + CodedOutputStream.computeStringSize(1, getMessage());
                }
                this.memoizedSerializedSize = i;
            }
            return i;
        }

        public boolean hasMessage() {
            return this.hasMessage;
        }

        public final boolean isInitialized() {
            return this.hasMessage;
        }

        public Builder newBuilderForType() {
            return newBuilder();
        }

        public Builder toBuilder() {
            return newBuilder(this);
        }

        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            getSerializedSize();
            if (hasMessage()) {
                codedOutputStream.writeString(1, getMessage());
            }
        }
    }

    public static final class KeyValue extends GeneratedMessageLite {
        public static final int KEY_FIELD_NUMBER = 1;
        public static final int VALUE_FIELD_NUMBER = 2;
        private static final KeyValue defaultInstance = new KeyValue(true);
        /* access modifiers changed from: private */
        public boolean hasKey;
        /* access modifiers changed from: private */
        public boolean hasValue;
        /* access modifiers changed from: private */
        public String key_;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public String value_;

        public static final class Builder extends GeneratedMessageLite.Builder<KeyValue, Builder> {
            private KeyValue result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public KeyValue buildParsed() throws InvalidProtocolBufferException {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException(this.result).asInvalidProtocolBufferException();
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new KeyValue();
                return builder;
            }

            public KeyValue build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException(this.result);
            }

            public KeyValue buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                KeyValue keyValue = this.result;
                this.result = null;
                return keyValue;
            }

            public Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new KeyValue();
                return this;
            }

            public Builder clearKey() {
                boolean unused = this.result.hasKey = false;
                String unused2 = this.result.key_ = KeyValue.getDefaultInstance().getKey();
                return this;
            }

            public Builder clearValue() {
                boolean unused = this.result.hasValue = false;
                String unused2 = this.result.value_ = KeyValue.getDefaultInstance().getValue();
                return this;
            }

            public Builder clone() {
                return create().mergeFrom(this.result);
            }

            public KeyValue getDefaultInstanceForType() {
                return KeyValue.getDefaultInstance();
            }

            public String getKey() {
                return this.result.getKey();
            }

            public String getValue() {
                return this.result.getValue();
            }

            public boolean hasKey() {
                return this.result.hasKey();
            }

            public boolean hasValue() {
                return this.result.hasValue();
            }

            /* access modifiers changed from: protected */
            public KeyValue internalGetResult() {
                return this.result;
            }

            public boolean isInitialized() {
                return this.result.isInitialized();
            }

            public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                while (true) {
                    int readTag = codedInputStream.readTag();
                    switch (readTag) {
                        case 0:
                            return this;
                        case 10:
                            setKey(codedInputStream.readString());
                            break;
                        case Opcodes.LDC /*18*/:
                            setValue(codedInputStream.readString());
                            break;
                        default:
                            if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                                break;
                            } else {
                                return this;
                            }
                    }
                }
            }

            public Builder mergeFrom(KeyValue keyValue) {
                if (keyValue == KeyValue.getDefaultInstance()) {
                    return this;
                }
                if (keyValue.hasKey()) {
                    setKey(keyValue.getKey());
                }
                if (keyValue.hasValue()) {
                    setValue(keyValue.getValue());
                }
                return this;
            }

            public Builder setKey(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasKey = true;
                String unused2 = this.result.key_ = str;
                return this;
            }

            public Builder setValue(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasValue = true;
                String unused2 = this.result.value_ = str;
                return this;
            }
        }

        static {
            Messages.internalForceInit();
            defaultInstance.initFields();
        }

        private KeyValue() {
            this.key_ = "";
            this.value_ = "";
            this.memoizedSerializedSize = -1;
            initFields();
        }

        private KeyValue(boolean z) {
            this.key_ = "";
            this.value_ = "";
            this.memoizedSerializedSize = -1;
        }

        public static KeyValue getDefaultInstance() {
            return defaultInstance;
        }

        private void initFields() {
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public static Builder newBuilder(KeyValue keyValue) {
            return newBuilder().mergeFrom(keyValue);
        }

        public static KeyValue parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static KeyValue parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, extensionRegistryLite)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static KeyValue parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(byteString)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.KeyValue.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.KeyValue.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.KeyValue.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$KeyValue$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static KeyValue parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(byteString, extensionRegistryLite)).buildParsed();
        }

        public static KeyValue parseFrom(CodedInputStream codedInputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(codedInputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.urbanairship.push.proto.Messages.KeyValue.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$KeyValue$Builder
         arg types: [com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.KeyValue.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.KeyValue.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.KeyValue.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$KeyValue$Builder */
        public static KeyValue parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return newBuilder().mergeFrom(codedInputStream, extensionRegistryLite).buildParsed();
        }

        public static KeyValue parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.KeyValue.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.KeyValue.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.KeyValue.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$KeyValue$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static KeyValue parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, extensionRegistryLite)).buildParsed();
        }

        public static KeyValue parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [byte[], com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.KeyValue.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.KeyValue.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.KeyValue.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$KeyValue$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static KeyValue parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(bArr, extensionRegistryLite)).buildParsed();
        }

        public KeyValue getDefaultInstanceForType() {
            return defaultInstance;
        }

        public String getKey() {
            return this.key_;
        }

        public int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i == -1) {
                i = 0;
                if (hasKey()) {
                    i = 0 + CodedOutputStream.computeStringSize(1, getKey());
                }
                if (hasValue()) {
                    i += CodedOutputStream.computeStringSize(2, getValue());
                }
                this.memoizedSerializedSize = i;
            }
            return i;
        }

        public String getValue() {
            return this.value_;
        }

        public boolean hasKey() {
            return this.hasKey;
        }

        public boolean hasValue() {
            return this.hasValue;
        }

        public final boolean isInitialized() {
            if (!this.hasKey) {
                return false;
            }
            return this.hasValue;
        }

        public Builder newBuilderForType() {
            return newBuilder();
        }

        public Builder toBuilder() {
            return newBuilder(this);
        }

        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            getSerializedSize();
            if (hasKey()) {
                codedOutputStream.writeString(1, getKey());
            }
            if (hasValue()) {
                codedOutputStream.writeString(2, getValue());
            }
        }
    }

    public enum OS implements Internal.EnumLite {
        ANDROID(0, 0);
        
        private static Internal.EnumLiteMap<OS> internalValueMap = new Internal.EnumLiteMap<OS>() {
            public OS findValueByNumber(int i) {
                return OS.valueOf(i);
            }
        };
        private final int index;
        private final int value;

        private OS(int i, int i2) {
            this.index = i;
            this.value = i2;
        }

        public static Internal.EnumLiteMap<OS> internalGetValueMap() {
            return internalValueMap;
        }

        public static OS valueOf(int i) {
            switch (i) {
                case 0:
                    return ANDROID;
                default:
                    return null;
            }
        }

        public final int getNumber() {
            return this.value;
        }
    }

    public static final class PushNotification extends GeneratedMessageLite {
        public static final int MAP_FIELD_NUMBER = 5;
        public static final int MESSAGE_FIELD_NUMBER = 2;
        public static final int MESSAGE_ID_FIELD_NUMBER = 1;
        public static final int PACKAGE_NAME_FIELD_NUMBER = 3;
        public static final int PAYLOAD_FIELD_NUMBER = 4;
        private static final PushNotification defaultInstance = new PushNotification(true);
        /* access modifiers changed from: private */
        public boolean hasMessage;
        /* access modifiers changed from: private */
        public boolean hasMessageId;
        /* access modifiers changed from: private */
        public boolean hasPackageName;
        /* access modifiers changed from: private */
        public boolean hasPayload;
        /* access modifiers changed from: private */
        public List<KeyValue> map_;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public String messageId_;
        /* access modifiers changed from: private */
        public String message_;
        /* access modifiers changed from: private */
        public String packageName_;
        /* access modifiers changed from: private */
        public String payload_;

        public static final class Builder extends GeneratedMessageLite.Builder<PushNotification, Builder> {
            private PushNotification result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public PushNotification buildParsed() throws InvalidProtocolBufferException {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException(this.result).asInvalidProtocolBufferException();
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new PushNotification();
                return builder;
            }

            public Builder addAllMap(Iterable<? extends KeyValue> iterable) {
                if (this.result.map_.isEmpty()) {
                    List unused = this.result.map_ = new ArrayList();
                }
                GeneratedMessageLite.Builder.addAll(iterable, this.result.map_);
                return this;
            }

            public Builder addMap(KeyValue.Builder builder) {
                if (this.result.map_.isEmpty()) {
                    List unused = this.result.map_ = new ArrayList();
                }
                this.result.map_.add(builder.build());
                return this;
            }

            public Builder addMap(KeyValue keyValue) {
                if (keyValue == null) {
                    throw new NullPointerException();
                }
                if (this.result.map_.isEmpty()) {
                    List unused = this.result.map_ = new ArrayList();
                }
                this.result.map_.add(keyValue);
                return this;
            }

            public PushNotification build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException(this.result);
            }

            public PushNotification buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.map_ != Collections.EMPTY_LIST) {
                    List unused = this.result.map_ = Collections.unmodifiableList(this.result.map_);
                }
                PushNotification pushNotification = this.result;
                this.result = null;
                return pushNotification;
            }

            public Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new PushNotification();
                return this;
            }

            public Builder clearMap() {
                List unused = this.result.map_ = Collections.emptyList();
                return this;
            }

            public Builder clearMessage() {
                boolean unused = this.result.hasMessage = false;
                String unused2 = this.result.message_ = PushNotification.getDefaultInstance().getMessage();
                return this;
            }

            public Builder clearMessageId() {
                boolean unused = this.result.hasMessageId = false;
                String unused2 = this.result.messageId_ = PushNotification.getDefaultInstance().getMessageId();
                return this;
            }

            public Builder clearPackageName() {
                boolean unused = this.result.hasPackageName = false;
                String unused2 = this.result.packageName_ = PushNotification.getDefaultInstance().getPackageName();
                return this;
            }

            public Builder clearPayload() {
                boolean unused = this.result.hasPayload = false;
                String unused2 = this.result.payload_ = PushNotification.getDefaultInstance().getPayload();
                return this;
            }

            public Builder clone() {
                return create().mergeFrom(this.result);
            }

            public PushNotification getDefaultInstanceForType() {
                return PushNotification.getDefaultInstance();
            }

            public KeyValue getMap(int i) {
                return this.result.getMap(i);
            }

            public int getMapCount() {
                return this.result.getMapCount();
            }

            public List<KeyValue> getMapList() {
                return Collections.unmodifiableList(this.result.map_);
            }

            public String getMessage() {
                return this.result.getMessage();
            }

            public String getMessageId() {
                return this.result.getMessageId();
            }

            public String getPackageName() {
                return this.result.getPackageName();
            }

            public String getPayload() {
                return this.result.getPayload();
            }

            public boolean hasMessage() {
                return this.result.hasMessage();
            }

            public boolean hasMessageId() {
                return this.result.hasMessageId();
            }

            public boolean hasPackageName() {
                return this.result.hasPackageName();
            }

            public boolean hasPayload() {
                return this.result.hasPayload();
            }

            /* access modifiers changed from: protected */
            public PushNotification internalGetResult() {
                return this.result;
            }

            public boolean isInitialized() {
                return this.result.isInitialized();
            }

            public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                while (true) {
                    int readTag = codedInputStream.readTag();
                    switch (readTag) {
                        case 0:
                            return this;
                        case 10:
                            setMessageId(codedInputStream.readString());
                            break;
                        case Opcodes.LDC /*18*/:
                            setMessage(codedInputStream.readString());
                            break;
                        case 26:
                            setPackageName(codedInputStream.readString());
                            break;
                        case 34:
                            setPayload(codedInputStream.readString());
                            break;
                        case 42:
                            KeyValue.Builder newBuilder = KeyValue.newBuilder();
                            codedInputStream.readMessage(newBuilder, extensionRegistryLite);
                            addMap(newBuilder.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                                break;
                            } else {
                                return this;
                            }
                    }
                }
            }

            public Builder mergeFrom(PushNotification pushNotification) {
                if (pushNotification == PushNotification.getDefaultInstance()) {
                    return this;
                }
                if (pushNotification.hasMessageId()) {
                    setMessageId(pushNotification.getMessageId());
                }
                if (pushNotification.hasMessage()) {
                    setMessage(pushNotification.getMessage());
                }
                if (pushNotification.hasPackageName()) {
                    setPackageName(pushNotification.getPackageName());
                }
                if (pushNotification.hasPayload()) {
                    setPayload(pushNotification.getPayload());
                }
                if (!pushNotification.map_.isEmpty()) {
                    if (this.result.map_.isEmpty()) {
                        List unused = this.result.map_ = new ArrayList();
                    }
                    this.result.map_.addAll(pushNotification.map_);
                }
                return this;
            }

            public Builder setMap(int i, KeyValue.Builder builder) {
                this.result.map_.set(i, builder.build());
                return this;
            }

            public Builder setMap(int i, KeyValue keyValue) {
                if (keyValue == null) {
                    throw new NullPointerException();
                }
                this.result.map_.set(i, keyValue);
                return this;
            }

            public Builder setMessage(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasMessage = true;
                String unused2 = this.result.message_ = str;
                return this;
            }

            public Builder setMessageId(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasMessageId = true;
                String unused2 = this.result.messageId_ = str;
                return this;
            }

            public Builder setPackageName(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasPackageName = true;
                String unused2 = this.result.packageName_ = str;
                return this;
            }

            public Builder setPayload(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasPayload = true;
                String unused2 = this.result.payload_ = str;
                return this;
            }
        }

        static {
            Messages.internalForceInit();
            defaultInstance.initFields();
        }

        private PushNotification() {
            this.messageId_ = "";
            this.message_ = "";
            this.packageName_ = "";
            this.payload_ = "";
            this.map_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
            initFields();
        }

        private PushNotification(boolean z) {
            this.messageId_ = "";
            this.message_ = "";
            this.packageName_ = "";
            this.payload_ = "";
            this.map_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        public static PushNotification getDefaultInstance() {
            return defaultInstance;
        }

        private void initFields() {
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public static Builder newBuilder(PushNotification pushNotification) {
            return newBuilder().mergeFrom(pushNotification);
        }

        public static PushNotification parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static PushNotification parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, extensionRegistryLite)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static PushNotification parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(byteString)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.PushNotification.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.PushNotification.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.PushNotification.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$PushNotification$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static PushNotification parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(byteString, extensionRegistryLite)).buildParsed();
        }

        public static PushNotification parseFrom(CodedInputStream codedInputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(codedInputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.urbanairship.push.proto.Messages.PushNotification.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$PushNotification$Builder
         arg types: [com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.PushNotification.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.PushNotification.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.PushNotification.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$PushNotification$Builder */
        public static PushNotification parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return newBuilder().mergeFrom(codedInputStream, extensionRegistryLite).buildParsed();
        }

        public static PushNotification parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.PushNotification.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.PushNotification.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.PushNotification.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$PushNotification$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static PushNotification parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, extensionRegistryLite)).buildParsed();
        }

        public static PushNotification parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [byte[], com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.PushNotification.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.PushNotification.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.PushNotification.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$PushNotification$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static PushNotification parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(bArr, extensionRegistryLite)).buildParsed();
        }

        public PushNotification getDefaultInstanceForType() {
            return defaultInstance;
        }

        public KeyValue getMap(int i) {
            return this.map_.get(i);
        }

        public int getMapCount() {
            return this.map_.size();
        }

        public List<KeyValue> getMapList() {
            return this.map_;
        }

        public String getMessage() {
            return this.message_;
        }

        public String getMessageId() {
            return this.messageId_;
        }

        public String getPackageName() {
            return this.packageName_;
        }

        public String getPayload() {
            return this.payload_;
        }

        public int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasMessageId()) {
                i2 = 0 + CodedOutputStream.computeStringSize(1, getMessageId());
            }
            if (hasMessage()) {
                i2 += CodedOutputStream.computeStringSize(2, getMessage());
            }
            if (hasPackageName()) {
                i2 += CodedOutputStream.computeStringSize(3, getPackageName());
            }
            if (hasPayload()) {
                i2 += CodedOutputStream.computeStringSize(4, getPayload());
            }
            Iterator<KeyValue> it = getMapList().iterator();
            while (true) {
                int i3 = i2;
                if (it.hasNext()) {
                    i2 = CodedOutputStream.computeMessageSize(5, it.next()) + i3;
                } else {
                    this.memoizedSerializedSize = i3;
                    return i3;
                }
            }
        }

        public boolean hasMessage() {
            return this.hasMessage;
        }

        public boolean hasMessageId() {
            return this.hasMessageId;
        }

        public boolean hasPackageName() {
            return this.hasPackageName;
        }

        public boolean hasPayload() {
            return this.hasPayload;
        }

        public final boolean isInitialized() {
            if (!this.hasMessageId) {
                return false;
            }
            if (!this.hasMessage) {
                return false;
            }
            if (!this.hasPackageName) {
                return false;
            }
            for (KeyValue isInitialized : getMapList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            return true;
        }

        public Builder newBuilderForType() {
            return newBuilder();
        }

        public Builder toBuilder() {
            return newBuilder(this);
        }

        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            getSerializedSize();
            if (hasMessageId()) {
                codedOutputStream.writeString(1, getMessageId());
            }
            if (hasMessage()) {
                codedOutputStream.writeString(2, getMessage());
            }
            if (hasPackageName()) {
                codedOutputStream.writeString(3, getPackageName());
            }
            if (hasPayload()) {
                codedOutputStream.writeString(4, getPayload());
            }
            for (KeyValue writeMessage : getMapList()) {
                codedOutputStream.writeMessage(5, writeMessage);
            }
        }
    }

    public static final class Register extends GeneratedMessageLite {
        public static final int APID_FIELD_NUMBER = 1;
        public static final int OS_FIELD_NUMBER = 2;
        public static final int OS_VERSION_FIELD_NUMBER = 3;
        public static final int RELIERS_FIELD_NUMBER = 6;
        public static final int SECRET_FIELD_NUMBER = 5;
        public static final int UA_VERSION_FIELD_NUMBER = 4;
        private static final Register defaultInstance = new Register(true);
        /* access modifiers changed from: private */
        public String apid_;
        /* access modifiers changed from: private */
        public boolean hasApid;
        /* access modifiers changed from: private */
        public boolean hasOs;
        /* access modifiers changed from: private */
        public boolean hasOsVersion;
        /* access modifiers changed from: private */
        public boolean hasSecret;
        /* access modifiers changed from: private */
        public boolean hasUaVersion;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public String osVersion_;
        /* access modifiers changed from: private */
        public OS os_;
        /* access modifiers changed from: private */
        public List<Relier> reliers_;
        /* access modifiers changed from: private */
        public String secret_;
        /* access modifiers changed from: private */
        public String uaVersion_;

        public static final class Builder extends GeneratedMessageLite.Builder<Register, Builder> {
            private Register result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public Register buildParsed() throws InvalidProtocolBufferException {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException(this.result).asInvalidProtocolBufferException();
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new Register();
                return builder;
            }

            public Builder addAllReliers(Iterable<? extends Relier> iterable) {
                if (this.result.reliers_.isEmpty()) {
                    List unused = this.result.reliers_ = new ArrayList();
                }
                GeneratedMessageLite.Builder.addAll(iterable, this.result.reliers_);
                return this;
            }

            public Builder addReliers(Relier.Builder builder) {
                if (this.result.reliers_.isEmpty()) {
                    List unused = this.result.reliers_ = new ArrayList();
                }
                this.result.reliers_.add(builder.build());
                return this;
            }

            public Builder addReliers(Relier relier) {
                if (relier == null) {
                    throw new NullPointerException();
                }
                if (this.result.reliers_.isEmpty()) {
                    List unused = this.result.reliers_ = new ArrayList();
                }
                this.result.reliers_.add(relier);
                return this;
            }

            public Register build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException(this.result);
            }

            public Register buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.reliers_ != Collections.EMPTY_LIST) {
                    List unused = this.result.reliers_ = Collections.unmodifiableList(this.result.reliers_);
                }
                Register register = this.result;
                this.result = null;
                return register;
            }

            public Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new Register();
                return this;
            }

            public Builder clearApid() {
                boolean unused = this.result.hasApid = false;
                String unused2 = this.result.apid_ = Register.getDefaultInstance().getApid();
                return this;
            }

            public Builder clearOs() {
                boolean unused = this.result.hasOs = false;
                OS unused2 = this.result.os_ = OS.ANDROID;
                return this;
            }

            public Builder clearOsVersion() {
                boolean unused = this.result.hasOsVersion = false;
                String unused2 = this.result.osVersion_ = Register.getDefaultInstance().getOsVersion();
                return this;
            }

            public Builder clearReliers() {
                List unused = this.result.reliers_ = Collections.emptyList();
                return this;
            }

            public Builder clearSecret() {
                boolean unused = this.result.hasSecret = false;
                String unused2 = this.result.secret_ = Register.getDefaultInstance().getSecret();
                return this;
            }

            public Builder clearUaVersion() {
                boolean unused = this.result.hasUaVersion = false;
                String unused2 = this.result.uaVersion_ = Register.getDefaultInstance().getUaVersion();
                return this;
            }

            public Builder clone() {
                return create().mergeFrom(this.result);
            }

            public String getApid() {
                return this.result.getApid();
            }

            public Register getDefaultInstanceForType() {
                return Register.getDefaultInstance();
            }

            public OS getOs() {
                return this.result.getOs();
            }

            public String getOsVersion() {
                return this.result.getOsVersion();
            }

            public Relier getReliers(int i) {
                return this.result.getReliers(i);
            }

            public int getReliersCount() {
                return this.result.getReliersCount();
            }

            public List<Relier> getReliersList() {
                return Collections.unmodifiableList(this.result.reliers_);
            }

            public String getSecret() {
                return this.result.getSecret();
            }

            public String getUaVersion() {
                return this.result.getUaVersion();
            }

            public boolean hasApid() {
                return this.result.hasApid();
            }

            public boolean hasOs() {
                return this.result.hasOs();
            }

            public boolean hasOsVersion() {
                return this.result.hasOsVersion();
            }

            public boolean hasSecret() {
                return this.result.hasSecret();
            }

            public boolean hasUaVersion() {
                return this.result.hasUaVersion();
            }

            /* access modifiers changed from: protected */
            public Register internalGetResult() {
                return this.result;
            }

            public boolean isInitialized() {
                return this.result.isInitialized();
            }

            public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                while (true) {
                    int readTag = codedInputStream.readTag();
                    switch (readTag) {
                        case 0:
                            return this;
                        case 10:
                            setApid(codedInputStream.readString());
                            break;
                        case 16:
                            OS valueOf = OS.valueOf(codedInputStream.readEnum());
                            if (valueOf == null) {
                                break;
                            } else {
                                setOs(valueOf);
                                break;
                            }
                        case 26:
                            setOsVersion(codedInputStream.readString());
                            break;
                        case 34:
                            setUaVersion(codedInputStream.readString());
                            break;
                        case 42:
                            setSecret(codedInputStream.readString());
                            break;
                        case 50:
                            Relier.Builder newBuilder = Relier.newBuilder();
                            codedInputStream.readMessage(newBuilder, extensionRegistryLite);
                            addReliers(newBuilder.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                                break;
                            } else {
                                return this;
                            }
                    }
                }
            }

            public Builder mergeFrom(Register register) {
                if (register == Register.getDefaultInstance()) {
                    return this;
                }
                if (register.hasApid()) {
                    setApid(register.getApid());
                }
                if (register.hasOs()) {
                    setOs(register.getOs());
                }
                if (register.hasOsVersion()) {
                    setOsVersion(register.getOsVersion());
                }
                if (register.hasUaVersion()) {
                    setUaVersion(register.getUaVersion());
                }
                if (register.hasSecret()) {
                    setSecret(register.getSecret());
                }
                if (!register.reliers_.isEmpty()) {
                    if (this.result.reliers_.isEmpty()) {
                        List unused = this.result.reliers_ = new ArrayList();
                    }
                    this.result.reliers_.addAll(register.reliers_);
                }
                return this;
            }

            public Builder setApid(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasApid = true;
                String unused2 = this.result.apid_ = str;
                return this;
            }

            public Builder setOs(OS os) {
                if (os == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasOs = true;
                OS unused2 = this.result.os_ = os;
                return this;
            }

            public Builder setOsVersion(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasOsVersion = true;
                String unused2 = this.result.osVersion_ = str;
                return this;
            }

            public Builder setReliers(int i, Relier.Builder builder) {
                this.result.reliers_.set(i, builder.build());
                return this;
            }

            public Builder setReliers(int i, Relier relier) {
                if (relier == null) {
                    throw new NullPointerException();
                }
                this.result.reliers_.set(i, relier);
                return this;
            }

            public Builder setSecret(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasSecret = true;
                String unused2 = this.result.secret_ = str;
                return this;
            }

            public Builder setUaVersion(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasUaVersion = true;
                String unused2 = this.result.uaVersion_ = str;
                return this;
            }
        }

        static {
            Messages.internalForceInit();
            defaultInstance.initFields();
        }

        private Register() {
            this.apid_ = "";
            this.osVersion_ = "";
            this.uaVersion_ = "";
            this.secret_ = "";
            this.reliers_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
            initFields();
        }

        private Register(boolean z) {
            this.apid_ = "";
            this.osVersion_ = "";
            this.uaVersion_ = "";
            this.secret_ = "";
            this.reliers_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        public static Register getDefaultInstance() {
            return defaultInstance;
        }

        private void initFields() {
            this.os_ = OS.ANDROID;
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public static Builder newBuilder(Register register) {
            return newBuilder().mergeFrom(register);
        }

        public static Register parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static Register parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, extensionRegistryLite)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static Register parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(byteString)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.Register.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.Register.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.Register.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$Register$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static Register parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(byteString, extensionRegistryLite)).buildParsed();
        }

        public static Register parseFrom(CodedInputStream codedInputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(codedInputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.urbanairship.push.proto.Messages.Register.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$Register$Builder
         arg types: [com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.Register.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.Register.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.Register.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$Register$Builder */
        public static Register parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return newBuilder().mergeFrom(codedInputStream, extensionRegistryLite).buildParsed();
        }

        public static Register parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.Register.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.Register.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.Register.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$Register$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static Register parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, extensionRegistryLite)).buildParsed();
        }

        public static Register parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [byte[], com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.Register.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.Register.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.Register.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$Register$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static Register parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(bArr, extensionRegistryLite)).buildParsed();
        }

        public String getApid() {
            return this.apid_;
        }

        public Register getDefaultInstanceForType() {
            return defaultInstance;
        }

        public OS getOs() {
            return this.os_;
        }

        public String getOsVersion() {
            return this.osVersion_;
        }

        public Relier getReliers(int i) {
            return this.reliers_.get(i);
        }

        public int getReliersCount() {
            return this.reliers_.size();
        }

        public List<Relier> getReliersList() {
            return this.reliers_;
        }

        public String getSecret() {
            return this.secret_;
        }

        public int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasApid()) {
                i2 = 0 + CodedOutputStream.computeStringSize(1, getApid());
            }
            if (hasOs()) {
                i2 += CodedOutputStream.computeEnumSize(2, getOs().getNumber());
            }
            if (hasOsVersion()) {
                i2 += CodedOutputStream.computeStringSize(3, getOsVersion());
            }
            if (hasUaVersion()) {
                i2 += CodedOutputStream.computeStringSize(4, getUaVersion());
            }
            if (hasSecret()) {
                i2 += CodedOutputStream.computeStringSize(5, getSecret());
            }
            Iterator<Relier> it = getReliersList().iterator();
            while (true) {
                int i3 = i2;
                if (it.hasNext()) {
                    i2 = CodedOutputStream.computeMessageSize(6, it.next()) + i3;
                } else {
                    this.memoizedSerializedSize = i3;
                    return i3;
                }
            }
        }

        public String getUaVersion() {
            return this.uaVersion_;
        }

        public boolean hasApid() {
            return this.hasApid;
        }

        public boolean hasOs() {
            return this.hasOs;
        }

        public boolean hasOsVersion() {
            return this.hasOsVersion;
        }

        public boolean hasSecret() {
            return this.hasSecret;
        }

        public boolean hasUaVersion() {
            return this.hasUaVersion;
        }

        public final boolean isInitialized() {
            if (!this.hasApid) {
                return false;
            }
            if (!this.hasOs) {
                return false;
            }
            if (!this.hasOsVersion) {
                return false;
            }
            if (!this.hasUaVersion) {
                return false;
            }
            if (!this.hasSecret) {
                return false;
            }
            for (Relier isInitialized : getReliersList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            return true;
        }

        public Builder newBuilderForType() {
            return newBuilder();
        }

        public Builder toBuilder() {
            return newBuilder(this);
        }

        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            getSerializedSize();
            if (hasApid()) {
                codedOutputStream.writeString(1, getApid());
            }
            if (hasOs()) {
                codedOutputStream.writeEnum(2, getOs().getNumber());
            }
            if (hasOsVersion()) {
                codedOutputStream.writeString(3, getOsVersion());
            }
            if (hasUaVersion()) {
                codedOutputStream.writeString(4, getUaVersion());
            }
            if (hasSecret()) {
                codedOutputStream.writeString(5, getSecret());
            }
            for (Relier writeMessage : getReliersList()) {
                codedOutputStream.writeMessage(6, writeMessage);
            }
        }
    }

    public static final class RegistrationEnvelope extends GeneratedMessageLite {
        public static final int APID_FIELD_NUMBER = 1;
        public static final int REGISTRATION_FIELD_NUMBER = 2;
        private static final RegistrationEnvelope defaultInstance = new RegistrationEnvelope(true);
        /* access modifiers changed from: private */
        public String apid_;
        /* access modifiers changed from: private */
        public boolean hasApid;
        /* access modifiers changed from: private */
        public boolean hasRegistration;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public ByteString registration_;

        public static final class Builder extends GeneratedMessageLite.Builder<RegistrationEnvelope, Builder> {
            private RegistrationEnvelope result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public RegistrationEnvelope buildParsed() throws InvalidProtocolBufferException {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException(this.result).asInvalidProtocolBufferException();
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new RegistrationEnvelope();
                return builder;
            }

            public RegistrationEnvelope build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException(this.result);
            }

            public RegistrationEnvelope buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                RegistrationEnvelope registrationEnvelope = this.result;
                this.result = null;
                return registrationEnvelope;
            }

            public Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new RegistrationEnvelope();
                return this;
            }

            public Builder clearApid() {
                boolean unused = this.result.hasApid = false;
                String unused2 = this.result.apid_ = RegistrationEnvelope.getDefaultInstance().getApid();
                return this;
            }

            public Builder clearRegistration() {
                boolean unused = this.result.hasRegistration = false;
                ByteString unused2 = this.result.registration_ = RegistrationEnvelope.getDefaultInstance().getRegistration();
                return this;
            }

            public Builder clone() {
                return create().mergeFrom(this.result);
            }

            public String getApid() {
                return this.result.getApid();
            }

            public RegistrationEnvelope getDefaultInstanceForType() {
                return RegistrationEnvelope.getDefaultInstance();
            }

            public ByteString getRegistration() {
                return this.result.getRegistration();
            }

            public boolean hasApid() {
                return this.result.hasApid();
            }

            public boolean hasRegistration() {
                return this.result.hasRegistration();
            }

            /* access modifiers changed from: protected */
            public RegistrationEnvelope internalGetResult() {
                return this.result;
            }

            public boolean isInitialized() {
                return this.result.isInitialized();
            }

            public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                while (true) {
                    int readTag = codedInputStream.readTag();
                    switch (readTag) {
                        case 0:
                            return this;
                        case 10:
                            setApid(codedInputStream.readString());
                            break;
                        case Opcodes.LDC /*18*/:
                            setRegistration(codedInputStream.readBytes());
                            break;
                        default:
                            if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                                break;
                            } else {
                                return this;
                            }
                    }
                }
            }

            public Builder mergeFrom(RegistrationEnvelope registrationEnvelope) {
                if (registrationEnvelope == RegistrationEnvelope.getDefaultInstance()) {
                    return this;
                }
                if (registrationEnvelope.hasApid()) {
                    setApid(registrationEnvelope.getApid());
                }
                if (registrationEnvelope.hasRegistration()) {
                    setRegistration(registrationEnvelope.getRegistration());
                }
                return this;
            }

            public Builder setApid(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasApid = true;
                String unused2 = this.result.apid_ = str;
                return this;
            }

            public Builder setRegistration(ByteString byteString) {
                if (byteString == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasRegistration = true;
                ByteString unused2 = this.result.registration_ = byteString;
                return this;
            }
        }

        static {
            Messages.internalForceInit();
            defaultInstance.initFields();
        }

        private RegistrationEnvelope() {
            this.apid_ = "";
            this.registration_ = ByteString.EMPTY;
            this.memoizedSerializedSize = -1;
            initFields();
        }

        private RegistrationEnvelope(boolean z) {
            this.apid_ = "";
            this.registration_ = ByteString.EMPTY;
            this.memoizedSerializedSize = -1;
        }

        public static RegistrationEnvelope getDefaultInstance() {
            return defaultInstance;
        }

        private void initFields() {
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public static Builder newBuilder(RegistrationEnvelope registrationEnvelope) {
            return newBuilder().mergeFrom(registrationEnvelope);
        }

        public static RegistrationEnvelope parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static RegistrationEnvelope parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, extensionRegistryLite)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static RegistrationEnvelope parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(byteString)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.RegistrationEnvelope.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.RegistrationEnvelope.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.RegistrationEnvelope.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$RegistrationEnvelope$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static RegistrationEnvelope parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(byteString, extensionRegistryLite)).buildParsed();
        }

        public static RegistrationEnvelope parseFrom(CodedInputStream codedInputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(codedInputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.urbanairship.push.proto.Messages.RegistrationEnvelope.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$RegistrationEnvelope$Builder
         arg types: [com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.RegistrationEnvelope.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.RegistrationEnvelope.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.RegistrationEnvelope.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$RegistrationEnvelope$Builder */
        public static RegistrationEnvelope parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return newBuilder().mergeFrom(codedInputStream, extensionRegistryLite).buildParsed();
        }

        public static RegistrationEnvelope parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.RegistrationEnvelope.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.RegistrationEnvelope.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.RegistrationEnvelope.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$RegistrationEnvelope$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static RegistrationEnvelope parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, extensionRegistryLite)).buildParsed();
        }

        public static RegistrationEnvelope parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [byte[], com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.RegistrationEnvelope.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.RegistrationEnvelope.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.RegistrationEnvelope.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$RegistrationEnvelope$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static RegistrationEnvelope parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(bArr, extensionRegistryLite)).buildParsed();
        }

        public String getApid() {
            return this.apid_;
        }

        public RegistrationEnvelope getDefaultInstanceForType() {
            return defaultInstance;
        }

        public ByteString getRegistration() {
            return this.registration_;
        }

        public int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i == -1) {
                i = 0;
                if (hasApid()) {
                    i = 0 + CodedOutputStream.computeStringSize(1, getApid());
                }
                if (hasRegistration()) {
                    i += CodedOutputStream.computeBytesSize(2, getRegistration());
                }
                this.memoizedSerializedSize = i;
            }
            return i;
        }

        public boolean hasApid() {
            return this.hasApid;
        }

        public boolean hasRegistration() {
            return this.hasRegistration;
        }

        public final boolean isInitialized() {
            if (!this.hasApid) {
                return false;
            }
            return this.hasRegistration;
        }

        public Builder newBuilderForType() {
            return newBuilder();
        }

        public Builder toBuilder() {
            return newBuilder(this);
        }

        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            getSerializedSize();
            if (hasApid()) {
                codedOutputStream.writeString(1, getApid());
            }
            if (hasRegistration()) {
                codedOutputStream.writeBytes(2, getRegistration());
            }
        }
    }

    public static final class RegistrationResponse extends GeneratedMessageLite {
        public static final int INVALID_RELIERS_FIELD_NUMBER = 2;
        public static final int VALID_RELIERS_FIELD_NUMBER = 1;
        private static final RegistrationResponse defaultInstance = new RegistrationResponse(true);
        /* access modifiers changed from: private */
        public List<Relier> invalidReliers_;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public List<Relier> validReliers_;

        public static final class Builder extends GeneratedMessageLite.Builder<RegistrationResponse, Builder> {
            private RegistrationResponse result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public RegistrationResponse buildParsed() throws InvalidProtocolBufferException {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException(this.result).asInvalidProtocolBufferException();
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new RegistrationResponse();
                return builder;
            }

            public Builder addAllInvalidReliers(Iterable<? extends Relier> iterable) {
                if (this.result.invalidReliers_.isEmpty()) {
                    List unused = this.result.invalidReliers_ = new ArrayList();
                }
                GeneratedMessageLite.Builder.addAll(iterable, this.result.invalidReliers_);
                return this;
            }

            public Builder addAllValidReliers(Iterable<? extends Relier> iterable) {
                if (this.result.validReliers_.isEmpty()) {
                    List unused = this.result.validReliers_ = new ArrayList();
                }
                GeneratedMessageLite.Builder.addAll(iterable, this.result.validReliers_);
                return this;
            }

            public Builder addInvalidReliers(Relier.Builder builder) {
                if (this.result.invalidReliers_.isEmpty()) {
                    List unused = this.result.invalidReliers_ = new ArrayList();
                }
                this.result.invalidReliers_.add(builder.build());
                return this;
            }

            public Builder addInvalidReliers(Relier relier) {
                if (relier == null) {
                    throw new NullPointerException();
                }
                if (this.result.invalidReliers_.isEmpty()) {
                    List unused = this.result.invalidReliers_ = new ArrayList();
                }
                this.result.invalidReliers_.add(relier);
                return this;
            }

            public Builder addValidReliers(Relier.Builder builder) {
                if (this.result.validReliers_.isEmpty()) {
                    List unused = this.result.validReliers_ = new ArrayList();
                }
                this.result.validReliers_.add(builder.build());
                return this;
            }

            public Builder addValidReliers(Relier relier) {
                if (relier == null) {
                    throw new NullPointerException();
                }
                if (this.result.validReliers_.isEmpty()) {
                    List unused = this.result.validReliers_ = new ArrayList();
                }
                this.result.validReliers_.add(relier);
                return this;
            }

            public RegistrationResponse build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException(this.result);
            }

            public RegistrationResponse buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.validReliers_ != Collections.EMPTY_LIST) {
                    List unused = this.result.validReliers_ = Collections.unmodifiableList(this.result.validReliers_);
                }
                if (this.result.invalidReliers_ != Collections.EMPTY_LIST) {
                    List unused2 = this.result.invalidReliers_ = Collections.unmodifiableList(this.result.invalidReliers_);
                }
                RegistrationResponse registrationResponse = this.result;
                this.result = null;
                return registrationResponse;
            }

            public Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new RegistrationResponse();
                return this;
            }

            public Builder clearInvalidReliers() {
                List unused = this.result.invalidReliers_ = Collections.emptyList();
                return this;
            }

            public Builder clearValidReliers() {
                List unused = this.result.validReliers_ = Collections.emptyList();
                return this;
            }

            public Builder clone() {
                return create().mergeFrom(this.result);
            }

            public RegistrationResponse getDefaultInstanceForType() {
                return RegistrationResponse.getDefaultInstance();
            }

            public Relier getInvalidReliers(int i) {
                return this.result.getInvalidReliers(i);
            }

            public int getInvalidReliersCount() {
                return this.result.getInvalidReliersCount();
            }

            public List<Relier> getInvalidReliersList() {
                return Collections.unmodifiableList(this.result.invalidReliers_);
            }

            public Relier getValidReliers(int i) {
                return this.result.getValidReliers(i);
            }

            public int getValidReliersCount() {
                return this.result.getValidReliersCount();
            }

            public List<Relier> getValidReliersList() {
                return Collections.unmodifiableList(this.result.validReliers_);
            }

            /* access modifiers changed from: protected */
            public RegistrationResponse internalGetResult() {
                return this.result;
            }

            public boolean isInitialized() {
                return this.result.isInitialized();
            }

            public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                while (true) {
                    int readTag = codedInputStream.readTag();
                    switch (readTag) {
                        case 0:
                            return this;
                        case 10:
                            Relier.Builder newBuilder = Relier.newBuilder();
                            codedInputStream.readMessage(newBuilder, extensionRegistryLite);
                            addValidReliers(newBuilder.buildPartial());
                            break;
                        case Opcodes.LDC /*18*/:
                            Relier.Builder newBuilder2 = Relier.newBuilder();
                            codedInputStream.readMessage(newBuilder2, extensionRegistryLite);
                            addInvalidReliers(newBuilder2.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                                break;
                            } else {
                                return this;
                            }
                    }
                }
            }

            public Builder mergeFrom(RegistrationResponse registrationResponse) {
                if (registrationResponse == RegistrationResponse.getDefaultInstance()) {
                    return this;
                }
                if (!registrationResponse.validReliers_.isEmpty()) {
                    if (this.result.validReliers_.isEmpty()) {
                        List unused = this.result.validReliers_ = new ArrayList();
                    }
                    this.result.validReliers_.addAll(registrationResponse.validReliers_);
                }
                if (!registrationResponse.invalidReliers_.isEmpty()) {
                    if (this.result.invalidReliers_.isEmpty()) {
                        List unused2 = this.result.invalidReliers_ = new ArrayList();
                    }
                    this.result.invalidReliers_.addAll(registrationResponse.invalidReliers_);
                }
                return this;
            }

            public Builder setInvalidReliers(int i, Relier.Builder builder) {
                this.result.invalidReliers_.set(i, builder.build());
                return this;
            }

            public Builder setInvalidReliers(int i, Relier relier) {
                if (relier == null) {
                    throw new NullPointerException();
                }
                this.result.invalidReliers_.set(i, relier);
                return this;
            }

            public Builder setValidReliers(int i, Relier.Builder builder) {
                this.result.validReliers_.set(i, builder.build());
                return this;
            }

            public Builder setValidReliers(int i, Relier relier) {
                if (relier == null) {
                    throw new NullPointerException();
                }
                this.result.validReliers_.set(i, relier);
                return this;
            }
        }

        static {
            Messages.internalForceInit();
            defaultInstance.initFields();
        }

        private RegistrationResponse() {
            this.validReliers_ = Collections.emptyList();
            this.invalidReliers_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
            initFields();
        }

        private RegistrationResponse(boolean z) {
            this.validReliers_ = Collections.emptyList();
            this.invalidReliers_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        public static RegistrationResponse getDefaultInstance() {
            return defaultInstance;
        }

        private void initFields() {
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public static Builder newBuilder(RegistrationResponse registrationResponse) {
            return newBuilder().mergeFrom(registrationResponse);
        }

        public static RegistrationResponse parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static RegistrationResponse parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, extensionRegistryLite)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static RegistrationResponse parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(byteString)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.RegistrationResponse.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.RegistrationResponse.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.RegistrationResponse.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$RegistrationResponse$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static RegistrationResponse parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(byteString, extensionRegistryLite)).buildParsed();
        }

        public static RegistrationResponse parseFrom(CodedInputStream codedInputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(codedInputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.urbanairship.push.proto.Messages.RegistrationResponse.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$RegistrationResponse$Builder
         arg types: [com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.RegistrationResponse.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.RegistrationResponse.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.RegistrationResponse.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$RegistrationResponse$Builder */
        public static RegistrationResponse parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return newBuilder().mergeFrom(codedInputStream, extensionRegistryLite).buildParsed();
        }

        public static RegistrationResponse parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.RegistrationResponse.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.RegistrationResponse.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.RegistrationResponse.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$RegistrationResponse$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static RegistrationResponse parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, extensionRegistryLite)).buildParsed();
        }

        public static RegistrationResponse parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [byte[], com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.RegistrationResponse.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.RegistrationResponse.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.RegistrationResponse.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$RegistrationResponse$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static RegistrationResponse parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(bArr, extensionRegistryLite)).buildParsed();
        }

        public RegistrationResponse getDefaultInstanceForType() {
            return defaultInstance;
        }

        public Relier getInvalidReliers(int i) {
            return this.invalidReliers_.get(i);
        }

        public int getInvalidReliersCount() {
            return this.invalidReliers_.size();
        }

        public List<Relier> getInvalidReliersList() {
            return this.invalidReliers_;
        }

        public int getSerializedSize() {
            int i;
            int i2 = this.memoizedSerializedSize;
            if (i2 != -1) {
                return i2;
            }
            int i3 = 0;
            Iterator<Relier> it = getValidReliersList().iterator();
            while (true) {
                i = i3;
                if (!it.hasNext()) {
                    break;
                }
                i3 = CodedOutputStream.computeMessageSize(1, it.next()) + i;
            }
            for (Relier computeMessageSize : getInvalidReliersList()) {
                i = CodedOutputStream.computeMessageSize(2, computeMessageSize) + i;
            }
            this.memoizedSerializedSize = i;
            return i;
        }

        public Relier getValidReliers(int i) {
            return this.validReliers_.get(i);
        }

        public int getValidReliersCount() {
            return this.validReliers_.size();
        }

        public List<Relier> getValidReliersList() {
            return this.validReliers_;
        }

        public final boolean isInitialized() {
            for (Relier isInitialized : getValidReliersList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            for (Relier isInitialized2 : getInvalidReliersList()) {
                if (!isInitialized2.isInitialized()) {
                    return false;
                }
            }
            return true;
        }

        public Builder newBuilderForType() {
            return newBuilder();
        }

        public Builder toBuilder() {
            return newBuilder(this);
        }

        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            getSerializedSize();
            for (Relier writeMessage : getValidReliersList()) {
                codedOutputStream.writeMessage(1, writeMessage);
            }
            for (Relier writeMessage2 : getInvalidReliersList()) {
                codedOutputStream.writeMessage(2, writeMessage2);
            }
        }
    }

    public static final class Relier extends GeneratedMessageLite {
        public static final int APP_KEY_FIELD_NUMBER = 2;
        public static final int PACKAGE_FIELD_NUMBER = 1;
        private static final Relier defaultInstance = new Relier(true);
        /* access modifiers changed from: private */
        public String appKey_;
        /* access modifiers changed from: private */
        public boolean hasAppKey;
        /* access modifiers changed from: private */
        public boolean hasPackage;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public String package_;

        public static final class Builder extends GeneratedMessageLite.Builder<Relier, Builder> {
            private Relier result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public Relier buildParsed() throws InvalidProtocolBufferException {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException(this.result).asInvalidProtocolBufferException();
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new Relier();
                return builder;
            }

            public Relier build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException(this.result);
            }

            public Relier buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                Relier relier = this.result;
                this.result = null;
                return relier;
            }

            public Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new Relier();
                return this;
            }

            public Builder clearAppKey() {
                boolean unused = this.result.hasAppKey = false;
                String unused2 = this.result.appKey_ = Relier.getDefaultInstance().getAppKey();
                return this;
            }

            public Builder clearPackage() {
                boolean unused = this.result.hasPackage = false;
                String unused2 = this.result.package_ = Relier.getDefaultInstance().getPackage();
                return this;
            }

            public Builder clone() {
                return create().mergeFrom(this.result);
            }

            public String getAppKey() {
                return this.result.getAppKey();
            }

            public Relier getDefaultInstanceForType() {
                return Relier.getDefaultInstance();
            }

            public String getPackage() {
                return this.result.getPackage();
            }

            public boolean hasAppKey() {
                return this.result.hasAppKey();
            }

            public boolean hasPackage() {
                return this.result.hasPackage();
            }

            /* access modifiers changed from: protected */
            public Relier internalGetResult() {
                return this.result;
            }

            public boolean isInitialized() {
                return this.result.isInitialized();
            }

            public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                while (true) {
                    int readTag = codedInputStream.readTag();
                    switch (readTag) {
                        case 0:
                            return this;
                        case 10:
                            setPackage(codedInputStream.readString());
                            break;
                        case Opcodes.LDC /*18*/:
                            setAppKey(codedInputStream.readString());
                            break;
                        default:
                            if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                                break;
                            } else {
                                return this;
                            }
                    }
                }
            }

            public Builder mergeFrom(Relier relier) {
                if (relier == Relier.getDefaultInstance()) {
                    return this;
                }
                if (relier.hasPackage()) {
                    setPackage(relier.getPackage());
                }
                if (relier.hasAppKey()) {
                    setAppKey(relier.getAppKey());
                }
                return this;
            }

            public Builder setAppKey(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasAppKey = true;
                String unused2 = this.result.appKey_ = str;
                return this;
            }

            public Builder setPackage(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasPackage = true;
                String unused2 = this.result.package_ = str;
                return this;
            }
        }

        static {
            Messages.internalForceInit();
            defaultInstance.initFields();
        }

        private Relier() {
            this.package_ = "";
            this.appKey_ = "";
            this.memoizedSerializedSize = -1;
            initFields();
        }

        private Relier(boolean z) {
            this.package_ = "";
            this.appKey_ = "";
            this.memoizedSerializedSize = -1;
        }

        public static Relier getDefaultInstance() {
            return defaultInstance;
        }

        private void initFields() {
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public static Builder newBuilder(Relier relier) {
            return newBuilder().mergeFrom(relier);
        }

        public static Relier parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static Relier parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, extensionRegistryLite)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static Relier parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(byteString)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.Relier.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.Relier.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.Relier.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$Relier$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static Relier parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(byteString, extensionRegistryLite)).buildParsed();
        }

        public static Relier parseFrom(CodedInputStream codedInputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(codedInputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.urbanairship.push.proto.Messages.Relier.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$Relier$Builder
         arg types: [com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.Relier.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.Relier.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.Relier.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$Relier$Builder */
        public static Relier parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return newBuilder().mergeFrom(codedInputStream, extensionRegistryLite).buildParsed();
        }

        public static Relier parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.Relier.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.Relier.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.Relier.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$Relier$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static Relier parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, extensionRegistryLite)).buildParsed();
        }

        public static Relier parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType
         arg types: [byte[], com.google.protobuf.ExtensionRegistryLite]
         candidates:
          com.urbanairship.push.proto.Messages.Relier.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.AbstractMessageLite$Builder
          com.urbanairship.push.proto.Messages.Relier.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.urbanairship.push.proto.Messages.Relier.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.urbanairship.push.proto.Messages$Relier$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):BuilderType
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.ByteString, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(com.google.protobuf.CodedInputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(java.io.InputStream, com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.MessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):com.google.protobuf.MessageLite$Builder
          com.google.protobuf.AbstractMessageLite.Builder.mergeFrom(byte[], com.google.protobuf.ExtensionRegistryLite):BuilderType */
        public static Relier parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return ((Builder) newBuilder().mergeFrom(bArr, extensionRegistryLite)).buildParsed();
        }

        public String getAppKey() {
            return this.appKey_;
        }

        public Relier getDefaultInstanceForType() {
            return defaultInstance;
        }

        public String getPackage() {
            return this.package_;
        }

        public int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i == -1) {
                i = 0;
                if (hasPackage()) {
                    i = 0 + CodedOutputStream.computeStringSize(1, getPackage());
                }
                if (hasAppKey()) {
                    i += CodedOutputStream.computeStringSize(2, getAppKey());
                }
                this.memoizedSerializedSize = i;
            }
            return i;
        }

        public boolean hasAppKey() {
            return this.hasAppKey;
        }

        public boolean hasPackage() {
            return this.hasPackage;
        }

        public final boolean isInitialized() {
            if (!this.hasPackage) {
                return false;
            }
            return this.hasAppKey;
        }

        public Builder newBuilderForType() {
            return newBuilder();
        }

        public Builder toBuilder() {
            return newBuilder(this);
        }

        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            getSerializedSize();
            if (hasPackage()) {
                codedOutputStream.writeString(1, getPackage());
            }
            if (hasAppKey()) {
                codedOutputStream.writeString(2, getAppKey());
            }
        }
    }

    private Messages() {
    }

    public static void internalForceInit() {
    }

    public static void registerAllExtensions(ExtensionRegistryLite extensionRegistryLite) {
    }
}
