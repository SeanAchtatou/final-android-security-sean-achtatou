package com.urbanairship.push;

import android.app.Notification;
import java.util.Map;

public interface PushNotificationBuilder {
    Notification buildNotification(String str, Map<String, String> map);

    int getNextId(String str, Map<String, String> map);
}
