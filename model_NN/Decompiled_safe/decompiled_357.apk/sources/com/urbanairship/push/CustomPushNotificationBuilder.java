package com.urbanairship.push;

import android.app.Notification;
import android.net.Uri;
import android.widget.RemoteViews;
import com.urbanairship.UAirship;
import com.urbanairship.util.NotificationIDGenerator;
import java.util.Map;

public class CustomPushNotificationBuilder implements PushNotificationBuilder {
    public int constantNotificationId = -1;
    public int layout;
    public int layoutIconDrawableId = UAirship.getAppInfo().icon;
    public int layoutIconId;
    public int layoutMessageId;
    public int layoutSubjectId;
    public Uri soundUri;
    public int statusBarIconDrawableId = UAirship.getAppInfo().icon;

    public Notification buildNotification(String str, Map<String, String> map) {
        Notification notification = new Notification(this.statusBarIconDrawableId, str, System.currentTimeMillis());
        notification.flags = 16;
        notification.defaults = 0;
        RemoteViews remoteViews = new RemoteViews(UAirship.shared().getApplicationContext().getPackageName(), this.layout);
        remoteViews.setTextViewText(this.layoutSubjectId, UAirship.getAppName());
        remoteViews.setTextViewText(this.layoutMessageId, str);
        remoteViews.setImageViewResource(this.layoutIconId, this.layoutIconDrawableId);
        notification.contentView = remoteViews;
        PushPreferences preferences = PushManager.shared().getPreferences();
        if (!preferences.beQuiet()) {
            if (preferences.isVibrateEnabled()) {
                notification.defaults |= 2;
            }
            if (preferences.isSoundEnabled()) {
                if (this.soundUri != null) {
                    notification.sound = this.soundUri;
                } else {
                    notification.defaults |= 1;
                }
            }
        }
        return notification;
    }

    public int getNextId(String str, Map<String, String> map) {
        return this.constantNotificationId > 0 ? this.constantNotificationId : NotificationIDGenerator.nextID();
    }
}
