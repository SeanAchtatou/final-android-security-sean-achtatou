package com.urbanairship.push.c2dm;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.push.PushManager;
import com.urbanairship.push.PushPreferences;
import com.urbanairship.push.embedded.BoxOfficeClient;
import com.urbanairship.restclient.AsyncHandler;
import com.urbanairship.restclient.Request;
import com.urbanairship.restclient.Response;
import java.io.UnsupportedEncodingException;
import java.util.UUID;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class C2DMPushManager {
    private static long backoffTime = 10000;
    public static final C2DMPushManager instance = new C2DMPushManager();

    private C2DMPushManager() {
    }

    public static void init() {
        Logger.info("Initializing C2DM Push ...");
        if (Build.VERSION.SDK_INT < 8) {
            Logger.debug("C2DM not supported in API level " + Build.VERSION.SDK_INT);
            PushManager.shared().c2dmRegistrationFailed("PHONE_REGISTRATION_ERROR");
            return;
        }
        PushPreferences preferences = PushManager.shared().getPreferences();
        if (preferences.getC2DMId() == null) {
            register();
            return;
        }
        Logger.info("Using C2DM ID: " + preferences.getC2DMId());
        PushManager.shared().updateApidIfNecessary();
    }

    public static void register() {
        register(0);
    }

    public static void register(final long j) {
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(j);
                } catch (InterruptedException e) {
                    Logger.error(e);
                }
                Context applicationContext = UAirship.shared().getApplicationContext();
                String str = UAirship.shared().getAirshipConfigOptions().c2dmSender;
                if (str == null) {
                    Logger.error("The C2DM sender email is not set. Unable to register.");
                    return;
                }
                String pushSecret = PushManager.shared().getPreferences().getPushSecret();
                if (pushSecret == null) {
                    try {
                        pushSecret = new BoxOfficeClient().firstRun();
                    } catch (BoxOfficeClient.BoxOfficeException e2) {
                        Logger.error("BoxOffice failure.");
                    }
                }
                if (pushSecret == null) {
                    Logger.error("BoxOffice firstRun failed. Unable to continue C2DM registration.");
                    return;
                }
                Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
                intent.putExtra("app", PendingIntent.getBroadcast(applicationContext, 0, new Intent(), 0));
                intent.putExtra("sender", str);
                applicationContext.startService(intent);
                Logger.info("Sent C2DM registration, sender: " + str);
            }
        }.start();
    }

    public static void retryRegistration() {
        register(backoffTime);
        backoffTime *= 2;
    }

    public static void sendTestPush(String str, String str2) {
        Logger.info("Attempting to send a test C2DM push");
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        JSONObject jSONObject3 = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(PushManager.shared().getPreferences().getPushId());
        final String uuid = UUID.randomUUID().toString();
        try {
            jSONObject2.put("alert", str2);
            jSONObject3.put("id", uuid);
            jSONObject2.put("extra", jSONObject3);
            jSONObject.put("apids", jSONArray);
            jSONObject.put("android", jSONObject2);
            String jSONObject4 = jSONObject.toString(0);
            Request request = new Request("POST", UAirship.shared().getAirshipConfigOptions().hostURL + "api/push/");
            request.setAuth(UAirship.shared().getAirshipConfigOptions().developmentAppKey, str);
            request.addHeader("Content-Type", "application/json");
            try {
                request.setEntity(new StringEntity(jSONObject4));
                request.executeAsync(new AsyncHandler() {
                    public void onComplete(Response response) {
                        if (response.status() == 200) {
                            Logger.info("C2DM Push" + uuid + " sent");
                        } else {
                            Logger.error("C2DM Push Failed!  Status: " + response.status());
                        }
                    }

                    public void onError(Exception exc) {
                        Logger.error("Couldn't create C2DM Push!");
                    }
                });
            } catch (UnsupportedEncodingException e) {
                Logger.error(e);
            }
        } catch (JSONException e2) {
            Logger.error("Couldn't serialize JSON for C2DM push!", e2);
        }
    }

    public static C2DMPushManager shared() {
        return instance;
    }

    public static void unregister() {
        Context applicationContext = UAirship.shared().getApplicationContext();
        Intent intent = new Intent("com.google.android.c2dm.intent.UNREGISTER");
        intent.putExtra("app", PendingIntent.getBroadcast(applicationContext, 0, new Intent(), 0));
        UAirship.shared().getApplicationContext().startService(intent);
    }
}
