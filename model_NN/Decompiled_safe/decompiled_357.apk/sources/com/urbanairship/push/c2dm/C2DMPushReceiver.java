package com.urbanairship.push.c2dm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.urbanairship.AirshipConfigOptions;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.push.PushManager;
import java.util.HashMap;

public class C2DMPushReceiver extends BroadcastReceiver {
    private void handleRegistrationIntent(Context context, Intent intent) {
        if (UAirship.shared().getAirshipConfigOptions().getTransport() != AirshipConfigOptions.TransportType.HELIUM) {
            String stringExtra = intent.getStringExtra("registration_id");
            String stringExtra2 = intent.getStringExtra("error");
            String stringExtra3 = intent.getStringExtra("unregistered");
            if (stringExtra2 != null) {
                Logger.error("Got error:" + stringExtra2);
                PushManager.shared().c2dmRegistrationFailed(stringExtra2);
            } else if (stringExtra3 != null) {
                Logger.info("Unregistered from C2DM: " + stringExtra3);
            } else if (stringExtra != null) {
                Logger.info("Got C2DM registration id:" + stringExtra);
                PushManager.shared().c2dmRegistrationResponseReceived(stringExtra);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void handleMessage(Context context, Intent intent) {
        HashMap hashMap = new HashMap();
        for (String next : intent.getExtras().keySet()) {
            hashMap.put(next, intent.getStringExtra(next));
        }
        String str = (String) hashMap.remove(PushManager.EXTRA_PUSH_ID);
        Logger.info("Got C2DM push: " + str);
        PushManager.deliverPush((String) hashMap.remove(PushManager.EXTRA_ALERT), str, hashMap);
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals("com.google.android.c2dm.intent.REGISTRATION")) {
            handleRegistrationIntent(context, intent);
        } else if (action.equals("com.google.android.c2dm.intent.RECEIVE")) {
            handleMessage(context, intent);
        } else {
            Logger.warn("Received unknown action: " + action);
        }
    }
}
