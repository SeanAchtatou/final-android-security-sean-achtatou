package com.urbanairship.push;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.urbanairship.AirshipConfigOptions;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.analytics.PushServiceStartedEvent;
import com.urbanairship.push.c2dm.C2DMPushManager;
import com.urbanairship.push.embedded.EmbeddedPushManager;

public class PushService extends Service {
    public static final String ACTION_HEARTBEAT = "com.urbanairship.push.HEARTBEAT";
    public static final String ACTION_START = "com.urbanairship.push.START";
    public static final String ACTION_STOP = "com.urbanairship.push.STOP";
    private PushPreferences prefs = PushManager.shared().getPreferences();
    public boolean started = false;

    private synchronized void setupService() {
        if (!this.started) {
            if (this.prefs.getPushId() != null) {
                UAirship.shared().getAnalytics().addEvent(new PushServiceStartedEvent());
            }
            if (!this.prefs.isPushEnabled()) {
                Logger.verbose("Push is disabled.  Not starting Push Service.");
            } else {
                this.started = true;
                Context applicationContext = UAirship.shared().getApplicationContext();
                AirshipConfigOptions airshipConfigOptions = UAirship.shared().getAirshipConfigOptions();
                String appKey = airshipConfigOptions.getAppKey();
                PushManager.shared().getPreferences().setApidUpdateNeeded(true);
                AirshipConfigOptions.TransportType transport = airshipConfigOptions.getTransport();
                if (transport == AirshipConfigOptions.TransportType.HELIUM) {
                    Logger.debug("Starting Helium");
                    if (this.prefs.getC2DMId() != null) {
                        PushManager.shared().setC2DMId(null);
                        C2DMPushManager.unregister();
                    }
                    EmbeddedPushManager.init(applicationContext, appKey);
                } else if (transport == AirshipConfigOptions.TransportType.C2DM) {
                    Logger.debug("Starting C2DM");
                    C2DMPushManager.init();
                } else {
                    Logger.debug("Starting C2DM");
                    C2DMPushManager.init();
                }
            }
        }
    }

    private void teardownService() {
        AirshipConfigOptions.TransportType transport = UAirship.shared().getAirshipConfigOptions().getTransport();
        if (transport == AirshipConfigOptions.TransportType.HELIUM || transport == AirshipConfigOptions.TransportType.HYBRID) {
            EmbeddedPushManager.shared().teardown();
        }
        this.started = false;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onDestroy() {
        Logger.verbose("Push Service destroyed");
        teardownService();
    }

    public void onStart(Intent intent, int i) {
        Logger.debug("Service started with intent=" + intent);
        super.onStart(intent, i);
        if (intent == null || intent.getAction() == null) {
            Logger.warn("Attempted to start service with null intent or action.");
            return;
        }
        String action = intent.getAction();
        if (action.equals(ACTION_STOP)) {
            if (this.started) {
                stopSelf();
            }
        } else if (action.equals(ACTION_START)) {
            setupService();
        } else if (action.equals(ACTION_HEARTBEAT)) {
            Logger.debug("** Heartbeat - PushService started");
            if (this.started) {
                EmbeddedPushManager.shared().resetConnectionIfNecessary();
            } else {
                setupService();
            }
        } else {
            Logger.warn("Unknown action: " + intent.getAction());
        }
    }
}
