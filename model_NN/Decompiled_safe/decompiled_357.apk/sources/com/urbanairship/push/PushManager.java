package com.urbanairship.push;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.urbanairship.AirshipConfigOptions;
import com.urbanairship.CoreReceiver;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.analytics.PushArrivedEvent;
import com.urbanairship.push.c2dm.C2DMPushManager;
import com.urbanairship.push.embedded.EmbeddedPushManager;
import com.urbanairship.restclient.AppAuthenticatedRequest;
import com.urbanairship.restclient.AsyncHandler;
import com.urbanairship.restclient.Response;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PushManager {
    public static final String ACTION_NOTIFICATION_OPENED = "com.urbanairship.push.NOTIFICATION_OPENED";
    public static final String ACTION_NOTIFICATION_OPENED_PROXY = "com.urbanairship.push.NOTIFICATION_OPENED_PROXY";
    public static final String ACTION_PUSH_RECEIVED = "com.urbanairship.push.PUSH_RECEIVED";
    public static final String ACTION_REGISTRATION_FINISHED = "com.urbanairship.push.REGISTRATION_FINISHED";
    private static final String ACTION_UPDATE_APID = "com.urbanairship.push.UPDATE_APID";
    public static final String EXTRA_ALERT = "com.urbanairship.push.ALERT";
    public static final String EXTRA_APID = "com.urbanairship.push.APID";
    public static final String EXTRA_C2DM_REGISTRATION_ID = "com.urbanairship.push.C2DM_REGISTRATION_ID";
    public static final String EXTRA_NOTIFICATION_ID = "com.urbanairship.push.NOTIFICATION_ID";
    private static final String EXTRA_PING = "com.urbanairship.push.PING";
    public static final String EXTRA_PUSH_ID = "com.urbanairship.push.PUSH_ID";
    public static final String EXTRA_REGISTRATION_ERROR = "com.urbanairship.push.REGISTRATION_ERROR";
    public static final String EXTRA_REGISTRATION_VALID = "com.urbanairship.push.REGISTRATION_VALID";
    public static final String EXTRA_STRING_EXTRA = "com.urbanairship.push.STRING_EXTRA";
    private static final PushManager instance = new PushManager();
    private Class<? extends BroadcastReceiver> intentReceiver;
    private PushNotificationBuilder notificationBuilder;
    /* access modifiers changed from: private */
    public PushPreferences preferences = new PushPreferences();
    private UpdateApidReceiver updateAPIDReceiver = new UpdateApidReceiver();

    private class UpdateApidReceiver extends BroadcastReceiver {
        private UpdateApidReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            PushManager.this.updateApid();
        }
    }

    private PushManager() {
        UAirship.shared().getApplicationContext().registerReceiver(this.updateAPIDReceiver, new IntentFilter(ACTION_UPDATE_APID));
        this.notificationBuilder = new BasicPushNotificationBuilder();
    }

    public static void deliverPush(String str, String str2, Map<String, String> map) {
        Notification buildNotification;
        String uuid = str2 == null ? UUID.randomUUID().toString() : str2;
        UAirship.shared().getAnalytics().addEvent(new PushArrivedEvent(uuid));
        if (map.containsKey(EXTRA_PING)) {
            Logger.verbose("Received UA Ping.");
            return;
        }
        Context applicationContext = UAirship.shared().getApplicationContext();
        Intent intent = new Intent(ACTION_PUSH_RECEIVED);
        PushNotificationBuilder notificationBuilder2 = shared().getNotificationBuilder();
        if (!(notificationBuilder2 == null || (buildNotification = notificationBuilder2.buildNotification(str, map)) == null)) {
            int nextId = notificationBuilder2.getNextId(str, map);
            intent.putExtra(EXTRA_NOTIFICATION_ID, nextId);
            if (buildNotification.contentIntent == null) {
                Intent intent2 = new Intent("com.urbanairship.push.NOTIFICATION_OPENED_PROXY." + UUID.randomUUID().toString());
                intent2.setClass(UAirship.shared().getApplicationContext(), CoreReceiver.class);
                for (String next : map.keySet()) {
                    intent2.putExtra(next, map.get(next));
                }
                intent2.putExtra(EXTRA_ALERT, str);
                intent2.putExtra(EXTRA_PUSH_ID, uuid);
                buildNotification.contentIntent = PendingIntent.getBroadcast(UAirship.shared().getApplicationContext(), 0, intent2, 0);
            }
            ((NotificationManager) applicationContext.getSystemService("notification")).notify(nextId, buildNotification);
        }
        Class<?> intentReceiver2 = shared().getIntentReceiver();
        if (intentReceiver2 != null) {
            intent.setClass(UAirship.shared().getApplicationContext(), intentReceiver2);
            for (String next2 : map.keySet()) {
                intent.putExtra(next2, map.get(next2));
            }
            intent.putExtra(EXTRA_ALERT, str);
            intent.putExtra(EXTRA_PUSH_ID, uuid);
            applicationContext.sendBroadcast(intent);
        }
    }

    public static void disablePush() {
        if (instance.preferences.isPushEnabled()) {
            instance.preferences.setPushEnabled(false);
            if (instance.preferences.getC2DMId() != null) {
                instance.setC2DMId(null);
                C2DMPushManager.unregister();
            }
            stopService();
        }
    }

    public static void enablePush() {
        if (!instance.preferences.isPushEnabled()) {
            instance.preferences.setPushEnabled(true);
            startService();
        }
    }

    public static void init() {
        Logger.verbose("PushManager init");
        startService();
    }

    /* access modifiers changed from: private */
    public void scheduleAPIDUpdate() {
        Intent intent = new Intent();
        intent.setClass(UAirship.shared().getApplicationContext(), PushManager.class);
        intent.setAction(ACTION_UPDATE_APID);
        PendingIntent service = PendingIntent.getService(UAirship.shared().getApplicationContext(), 0, intent, 0);
        Logger.info("Scheduling APID update in 10 minutes");
        ((AlarmManager) UAirship.shared().getApplicationContext().getSystemService("alarm")).set(1, System.currentTimeMillis() + 600000, service);
    }

    public static PushManager shared() {
        return instance;
    }

    public static void startService() {
        Logger.verbose("PushManager startService");
        Context applicationContext = UAirship.shared().getApplicationContext();
        Intent intent = new Intent(applicationContext, PushService.class);
        intent.setAction(PushService.ACTION_START);
        applicationContext.startService(intent);
    }

    public static void stopService() {
        Logger.verbose("PushManager stopService");
        Context applicationContext = UAirship.shared().getApplicationContext();
        Intent intent = new Intent(applicationContext, PushService.class);
        intent.setAction(PushService.ACTION_STOP);
        applicationContext.startService(intent);
    }

    /* access modifiers changed from: private */
    public void updateApid() {
        this.preferences.setApidUpdateNeeded(true);
        String pushId = getPreferences().getPushId();
        Logger.debug("Updating APID: " + pushId);
        if (pushId == null || pushId.length() == 0) {
            Logger.error("No APID. Cannot update.");
            return;
        }
        String str = UAirship.shared().getAirshipConfigOptions().hostURL + "api/apids/" + pushId;
        String c2DMId = getC2DMId();
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("alias", getAlias());
            jSONObject.put("tags", new JSONArray((Collection) getTags()));
            jSONObject.put("c2dm_registration_id", c2DMId);
            Logger.verbose("URL: " + str);
            AppAuthenticatedRequest appAuthenticatedRequest = new AppAuthenticatedRequest("PUT", str);
            try {
                StringEntity stringEntity = new StringEntity(jSONObject.toString());
                stringEntity.setContentType("application/json");
                appAuthenticatedRequest.setEntity(stringEntity);
                Logger.verbose("Body: " + jSONObject.toString());
            } catch (UnsupportedEncodingException e) {
                Logger.error("Error setting registrationRequest entity.");
            }
            appAuthenticatedRequest.executeAsync(new AsyncHandler() {
                public void onComplete(Response response) {
                    String body = response.body();
                    Logger.info("Registration status code: " + response.status());
                    Logger.verbose("Registration result " + body);
                    if (response.status() == 200) {
                        Logger.info("Registration request succeeded.");
                        PushManager.this.preferences.setApidUpdateNeeded(false);
                        return;
                    }
                    Logger.info("Registration request response status: " + response.status());
                }

                public void onError(Exception exc) {
                    Logger.error("Error registering APID");
                    PushManager.this.scheduleAPIDUpdate();
                }
            });
        } catch (JSONException e2) {
            Logger.error("Error creating JSON Registration body.");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void c2dmRegistrationFailed(String str) {
        boolean z;
        Logger.error("C2DM Failure: " + str);
        if ("SERVICE_NOT_AVAILABLE".equals(str)) {
            C2DMPushManager.retryRegistration();
            z = false;
        } else if ("ACCOUNT_MISSING".equals(str)) {
            z = true;
        } else if ("AUTHENTICATION_FAILED".equals(str)) {
            z = true;
        } else if ("TOO_MANY_REGISTRATIONS".equals(str)) {
            z = true;
        } else if ("INVALID_SENDER".equals(str)) {
            Logger.error("Your C2DM sender ID is invalid. Please check your AirshipConfig.");
            z = true;
        } else {
            z = "PHONE_REGISTRATION_ERROR".equals(str);
        }
        if (z) {
            setC2DMId(null);
            if (UAirship.shared().getAirshipConfigOptions().getTransport() == AirshipConfigOptions.TransportType.HYBRID) {
                EmbeddedPushManager.init(UAirship.shared().getApplicationContext(), UAirship.shared().getAirshipConfigOptions().getAppKey());
            }
        }
        Context applicationContext = UAirship.shared().getApplicationContext();
        Class<?> intentReceiver2 = shared().getIntentReceiver();
        if (intentReceiver2 != null) {
            Intent intent = new Intent(ACTION_REGISTRATION_FINISHED);
            intent.setClass(UAirship.shared().getApplicationContext(), intentReceiver2);
            intent.putExtra(EXTRA_APID, this.preferences.getPushId());
            intent.putExtra(EXTRA_REGISTRATION_VALID, false);
            intent.putExtra(EXTRA_REGISTRATION_ERROR, str);
            applicationContext.sendBroadcast(intent);
        }
    }

    public void c2dmRegistrationResponseReceived(String str) {
        setC2DMId(str);
        Context applicationContext = UAirship.shared().getApplicationContext();
        Class<?> intentReceiver2 = shared().getIntentReceiver();
        if (intentReceiver2 != null) {
            Intent intent = new Intent(ACTION_REGISTRATION_FINISHED);
            intent.setClass(UAirship.shared().getApplicationContext(), intentReceiver2);
            intent.putExtra(EXTRA_APID, this.preferences.getPushId());
            intent.putExtra(EXTRA_REGISTRATION_VALID, this.preferences.isPushEnabled());
            intent.putExtra(EXTRA_C2DM_REGISTRATION_ID, str);
            applicationContext.sendBroadcast(intent);
        }
    }

    public String getAlias() {
        return getPreferences().getAlias();
    }

    public String getC2DMId() {
        return getPreferences().getC2DMId();
    }

    public Class<?> getIntentReceiver() {
        return this.intentReceiver;
    }

    public PushNotificationBuilder getNotificationBuilder() {
        return this.notificationBuilder;
    }

    public PushPreferences getPreferences() {
        return this.preferences;
    }

    public Set<String> getTags() {
        return getPreferences().getTags();
    }

    public void heliumRegistrationResponseReceived() {
        if (this.preferences.isApidUpdateNeeded()) {
            updateApid();
        }
        Context applicationContext = UAirship.shared().getApplicationContext();
        Class<?> intentReceiver2 = shared().getIntentReceiver();
        if (intentReceiver2 != null) {
            Intent intent = new Intent(ACTION_REGISTRATION_FINISHED);
            intent.setClass(UAirship.shared().getApplicationContext(), intentReceiver2);
            intent.putExtra(EXTRA_APID, this.preferences.getPushId());
            intent.putExtra(EXTRA_REGISTRATION_VALID, this.preferences.isPushEnabled());
            applicationContext.sendBroadcast(intent);
        }
    }

    public void setAlias(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Alias must be non-null.");
        }
        PushPreferences preferences2 = getPreferences();
        if (!str.equals(preferences2.getAlias())) {
            preferences2.setAlias(str);
            updateApid();
        }
    }

    public void setAliasAndTags(String str, Set<String> set) {
        boolean z;
        if (str == null) {
            throw new IllegalArgumentException("Alias must be non-null.");
        } else if (set == null) {
            throw new IllegalArgumentException("Tags must be non-null.");
        } else {
            PushPreferences preferences2 = getPreferences();
            boolean z2 = false;
            if (!str.equals(preferences2.getAlias())) {
                preferences2.setAlias(str);
                z2 = true;
            }
            if (!set.equals(preferences2.getTags())) {
                preferences2.setTags(set);
                z = true;
            } else {
                z = z2;
            }
            if (z) {
                updateApid();
            }
        }
    }

    public void setC2DMId(String str) {
        PushPreferences preferences2 = getPreferences();
        String c2DMId = preferences2.getC2DMId();
        if (c2DMId != null) {
            if (c2DMId.equals(str)) {
                return;
            }
        } else if (str == null) {
            return;
        }
        preferences2.setC2DMId(str);
        updateApid();
    }

    public void setIntentReceiver(Class<? extends BroadcastReceiver> cls) {
        this.intentReceiver = cls;
    }

    public void setNotificationBuilder(PushNotificationBuilder pushNotificationBuilder) {
        this.notificationBuilder = pushNotificationBuilder;
    }

    public void setTags(Set<String> set) {
        if (set == null) {
            throw new IllegalArgumentException("Tags must be non-null.");
        }
        PushPreferences preferences2 = getPreferences();
        if (!set.equals(preferences2.getTags())) {
            preferences2.setTags(set);
            updateApid();
        }
    }

    public void updateApidIfNecessary() {
        if (this.preferences.isApidUpdateNeeded()) {
            updateApid();
        }
    }
}
