package com.google.common.net;

import com.google.common.annotations.Beta;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import com.google.common.io.ByteStreams;
import com.google.common.primitives.Ints;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import javax.annotation.Nullable;
import org.codehaus.jackson.smile.SmileConstants;

@Beta
public final class InetAddresses {
    /* access modifiers changed from: private */
    public static final Inet4Address ANY4 = ((Inet4Address) forString("0.0.0.0"));
    private static final int IPV4_PART_COUNT = 4;
    private static final int IPV6_PART_COUNT = 8;
    private static final Inet4Address LOOPBACK4 = ((Inet4Address) forString("127.0.0.1"));

    private InetAddresses() {
    }

    private static Inet4Address getInet4Address(byte[] bytes) {
        boolean z;
        if (bytes.length == 4) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkArgument(z, "Byte array has invalid length for an IPv4 address: %s != 4.", Integer.valueOf(bytes.length));
        try {
            InetAddress ipv4 = InetAddress.getByAddress(bytes);
            if (ipv4 instanceof Inet4Address) {
                return (Inet4Address) ipv4;
            }
            throw new UnknownHostException(String.format("'%s' is not an IPv4 address.", ipv4.getHostAddress()));
        } catch (UnknownHostException e) {
            throw new IllegalArgumentException(String.format("Host address '%s' is not a valid IPv4 address.", Arrays.toString(bytes)), e);
        }
    }

    public static InetAddress forString(String ipString) {
        byte[] addr = textToNumericFormatV4(ipString);
        if (addr == null) {
            addr = textToNumericFormatV6(ipString);
        }
        if (addr == null) {
            throw new IllegalArgumentException(String.format("'%s' is not an IP string literal.", ipString));
        }
        try {
            return InetAddress.getByAddress(addr);
        } catch (UnknownHostException e) {
            throw new IllegalArgumentException(String.format("'%s' is extremely broken.", ipString), e);
        }
    }

    public static boolean isInetAddress(String ipString) {
        try {
            forString(ipString);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    private static byte[] textToNumericFormatV4(String ipString) {
        if (ipString.contains(":")) {
            return null;
        }
        String[] address = ipString.split("\\.");
        if (address.length != 4) {
            return null;
        }
        byte[] bytes = new byte[4];
        int i = 0;
        while (i < bytes.length) {
            try {
                int piece = Integer.parseInt(address[i]);
                if (piece < 0 || piece > 255) {
                    return null;
                }
                if (address[i].startsWith("0") && address[i].length() != 1) {
                    return null;
                }
                bytes[i] = (byte) piece;
                i++;
            } catch (NumberFormatException e) {
                return null;
            }
        }
        return bytes;
    }

    /* JADX INFO: Multiple debug info for r7v2 java.lang.String[]: [D('ipString' java.lang.String), D('addressHalves' java.lang.String[])] */
    /* JADX INFO: Multiple debug info for r0v16 int: [D('partsLo' int), D('totalParts' int)] */
    /* JADX INFO: Multiple debug info for r2v2 java.lang.String[]: [D('partsLo' int), D('parts' java.lang.String[])] */
    /* JADX INFO: Multiple debug info for r0v34 int: [D('bytesIndex' int), D('i' int)] */
    /* JADX INFO: Multiple debug info for r0v39 int: [D('partsHi' int), D('i' int)] */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00c4, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] textToNumericFormatV6(java.lang.String r7) {
        /*
            java.lang.String r0 = ":"
            boolean r0 = r7.contains(r0)
            if (r0 != 0) goto L_0x000a
            r7 = 0
        L_0x0009:
            return r7
        L_0x000a:
            java.lang.String r0 = ":::"
            boolean r0 = r7.contains(r0)
            if (r0 == 0) goto L_0x0014
            r7 = 0
            goto L_0x0009
        L_0x0014:
            java.lang.String r0 = "."
            boolean r0 = r7.contains(r0)
            if (r0 == 0) goto L_0x0024
            java.lang.String r7 = convertDottedQuadToHex(r7)
            if (r7 != 0) goto L_0x0024
            r7 = 0
            goto L_0x0009
        L_0x0024:
            r0 = 16
            java.nio.ByteBuffer r5 = java.nio.ByteBuffer.allocate(r0)
            r0 = 0
            r2 = 0
            java.lang.String r0 = "::"
            r1 = 2
            java.lang.String[] r7 = r7.split(r0, r1)
            r0 = 0
            r0 = r7[r0]
            java.lang.String r1 = ""
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x00a0
            r0 = 0
            r0 = r7[r0]
            java.lang.String r1 = ":"
            r3 = 8
            java.lang.String[] r1 = r0.split(r1, r3)
            r0 = 0
        L_0x004a:
            int r3 = r1.length     // Catch:{ NumberFormatException -> 0x009c }
            if (r0 >= r3) goto L_0x006a
            r3 = r1[r0]     // Catch:{ NumberFormatException -> 0x009c }
            java.lang.String r4 = ""
            boolean r3 = r3.equals(r4)     // Catch:{ NumberFormatException -> 0x009c }
            if (r3 == 0) goto L_0x0059
            r7 = 0
            goto L_0x0009
        L_0x0059:
            r3 = r1[r0]     // Catch:{ NumberFormatException -> 0x009c }
            r4 = 16
            int r3 = java.lang.Integer.parseInt(r3, r4)     // Catch:{ NumberFormatException -> 0x009c }
            int r4 = r0 * 2
            short r3 = (short) r3     // Catch:{ NumberFormatException -> 0x009c }
            r5.putShort(r4, r3)     // Catch:{ NumberFormatException -> 0x009c }
            int r0 = r0 + 1
            goto L_0x004a
        L_0x006a:
            int r0 = r1.length     // Catch:{ NumberFormatException -> 0x009c }
            r3 = r0
        L_0x006c:
            int r0 = r7.length
            r1 = 1
            if (r0 <= r1) goto L_0x00da
            r0 = 1
            r0 = r7[r0]
            java.lang.String r1 = ""
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x00c7
            r0 = 1
            r0 = r7[r0]
            java.lang.String r1 = ":"
            r2 = 8
            java.lang.String[] r2 = r0.split(r1, r2)
            r0 = 0
            r1 = r0
        L_0x0088:
            int r0 = r2.length     // Catch:{ NumberFormatException -> 0x00c3 }
            if (r1 >= r0) goto L_0x00ba
            int r0 = r2.length     // Catch:{ NumberFormatException -> 0x00c3 }
            int r0 = r0 - r1
            r4 = 1
            int r0 = r0 - r4
            r4 = r2[r0]     // Catch:{ NumberFormatException -> 0x00c3 }
            java.lang.String r6 = ""
            boolean r4 = r4.equals(r6)     // Catch:{ NumberFormatException -> 0x00c3 }
            if (r4 == 0) goto L_0x00a3
            r7 = 0
            goto L_0x0009
        L_0x009c:
            r7 = move-exception
            r7 = 0
            goto L_0x0009
        L_0x00a0:
            r0 = 1
            r3 = r0
            goto L_0x006c
        L_0x00a3:
            r0 = r2[r0]     // Catch:{ NumberFormatException -> 0x00c3 }
            r4 = 16
            int r4 = java.lang.Integer.parseInt(r0, r4)     // Catch:{ NumberFormatException -> 0x00c3 }
            r0 = 8
            int r0 = r0 - r1
            r6 = 1
            int r0 = r0 - r6
            int r0 = r0 * 2
            short r4 = (short) r4     // Catch:{ NumberFormatException -> 0x00c3 }
            r5.putShort(r0, r4)     // Catch:{ NumberFormatException -> 0x00c3 }
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0088
        L_0x00ba:
            int r0 = r2.length     // Catch:{ NumberFormatException -> 0x00c3 }
        L_0x00bb:
            int r0 = r0 + r3
            r1 = 8
            if (r0 <= r1) goto L_0x00c9
            r7 = 0
            goto L_0x0009
        L_0x00c3:
            r7 = move-exception
            r7 = 0
            goto L_0x0009
        L_0x00c7:
            r0 = 1
            goto L_0x00bb
        L_0x00c9:
            int r7 = r7.length
            r1 = 1
            if (r7 != r1) goto L_0x00d4
            r7 = 8
            if (r0 == r7) goto L_0x00d4
            r7 = 0
            goto L_0x0009
        L_0x00d4:
            byte[] r7 = r5.array()
            goto L_0x0009
        L_0x00da:
            r0 = r2
            goto L_0x00bb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.net.InetAddresses.textToNumericFormatV6(java.lang.String):byte[]");
    }

    private static String convertDottedQuadToHex(String ipString) {
        int lastColon = ipString.lastIndexOf(58);
        String initialPart = ipString.substring(0, lastColon + 1);
        byte[] quad = textToNumericFormatV4(ipString.substring(lastColon + 1));
        if (quad == null) {
            return null;
        }
        String penultimate = Integer.toHexString(((quad[0] & SmileConstants.BYTE_MARKER_END_OF_CONTENT) << 8) | (quad[1] & SmileConstants.BYTE_MARKER_END_OF_CONTENT));
        return initialPart + penultimate + ":" + Integer.toHexString(((quad[2] & SmileConstants.BYTE_MARKER_END_OF_CONTENT) << 8) | (quad[3] & SmileConstants.BYTE_MARKER_END_OF_CONTENT));
    }

    public static String toUriString(InetAddress ip) {
        if (ip instanceof Inet6Address) {
            return "[" + ip.getHostAddress() + "]";
        }
        return ip.getHostAddress();
    }

    public static InetAddress forUriString(String hostAddr) {
        Preconditions.checkNotNull(hostAddr);
        Preconditions.checkArgument(hostAddr.length() > 0, "host string is empty");
        try {
            InetAddress retval = forString(hostAddr);
            if (retval instanceof Inet4Address) {
                return retval;
            }
        } catch (IllegalArgumentException e) {
        }
        if (!hostAddr.startsWith("[") || !hostAddr.endsWith("]")) {
            throw new IllegalArgumentException("Not a valid address: \"" + hostAddr + '\"');
        }
        InetAddress retval2 = forString(hostAddr.substring(1, hostAddr.length() - 1));
        if (retval2 instanceof Inet6Address) {
            return retval2;
        }
        throw new IllegalArgumentException("Not a valid address: \"" + hostAddr + '\"');
    }

    public static boolean isUriInetAddress(String ipString) {
        try {
            forUriString(ipString);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    public static boolean isCompatIPv4Address(Inet6Address ip) {
        if (!ip.isIPv4CompatibleAddress()) {
            return false;
        }
        byte[] bytes = ip.getAddress();
        if (bytes[12] == 0 && bytes[13] == 0 && bytes[14] == 0 && (bytes[15] == 0 || bytes[15] == 1)) {
            return false;
        }
        return true;
    }

    public static Inet4Address getCompatIPv4Address(Inet6Address ip) {
        Preconditions.checkArgument(isCompatIPv4Address(ip), "Address '%s' is not IPv4-compatible.", ip.getHostAddress());
        return getInet4Address(copyOfRange(ip.getAddress(), 12, 16));
    }

    public static boolean is6to4Address(Inet6Address ip) {
        byte[] bytes = ip.getAddress();
        return bytes[0] == 32 && bytes[1] == 2;
    }

    public static Inet4Address get6to4IPv4Address(Inet6Address ip) {
        Preconditions.checkArgument(is6to4Address(ip), "Address '%s' is not a 6to4 address.", ip.getHostAddress());
        return getInet4Address(copyOfRange(ip.getAddress(), 2, 6));
    }

    @Beta
    public static final class TeredoInfo {
        private final Inet4Address client;
        private final int flags;
        private final int port;
        private final Inet4Address server;

        public TeredoInfo(@Nullable Inet4Address server2, @Nullable Inet4Address client2, int port2, int flags2) {
            boolean z;
            Preconditions.checkArgument(port2 >= 0 && port2 <= 65535, "port '%d' is out of range (0 <= port <= 0xffff)", Integer.valueOf(port2));
            if (flags2 < 0 || flags2 > 65535) {
                z = false;
            } else {
                z = true;
            }
            Preconditions.checkArgument(z, "flags '%d' is out of range (0 <= flags <= 0xffff)", Integer.valueOf(flags2));
            if (server2 != null) {
                this.server = server2;
            } else {
                this.server = InetAddresses.ANY4;
            }
            if (client2 != null) {
                this.client = client2;
            } else {
                this.client = InetAddresses.ANY4;
            }
            this.port = port2;
            this.flags = flags2;
        }

        public Inet4Address getServer() {
            return this.server;
        }

        public Inet4Address getClient() {
            return this.client;
        }

        public int getPort() {
            return this.port;
        }

        public int getFlags() {
            return this.flags;
        }
    }

    public static boolean isTeredoAddress(Inet6Address ip) {
        byte[] bytes = ip.getAddress();
        return bytes[0] == 32 && bytes[1] == 1 && bytes[2] == 0 && bytes[3] == 0;
    }

    public static TeredoInfo getTeredoInfo(Inet6Address ip) {
        Preconditions.checkArgument(isTeredoAddress(ip), "Address '%s' is not a Teredo address.", ip.getHostAddress());
        byte[] bytes = ip.getAddress();
        Inet4Address server = getInet4Address(copyOfRange(bytes, 4, 8));
        int flags = ByteStreams.newDataInput(bytes, 8).readShort() & 65535;
        int port = (ByteStreams.newDataInput(bytes, 10).readShort() ^ -1) & 65535;
        byte[] clientBytes = copyOfRange(bytes, 12, 16);
        for (int i = 0; i < clientBytes.length; i++) {
            clientBytes[i] = (byte) (clientBytes[i] ^ -1);
        }
        return new TeredoInfo(server, getInet4Address(clientBytes), port, flags);
    }

    public static boolean isIsatapAddress(Inet6Address ip) {
        if (isTeredoAddress(ip)) {
            return false;
        }
        byte[] bytes = ip.getAddress();
        if ((bytes[8] | 3) != 3) {
            return false;
        }
        return bytes[9] == 0 && bytes[10] == 94 && bytes[11] == -2;
    }

    public static Inet4Address getIsatapIPv4Address(Inet6Address ip) {
        Preconditions.checkArgument(isIsatapAddress(ip), "Address '%s' is not an ISATAP address.", ip.getHostAddress());
        return getInet4Address(copyOfRange(ip.getAddress(), 12, 16));
    }

    public static boolean hasEmbeddedIPv4ClientAddress(Inet6Address ip) {
        return isCompatIPv4Address(ip) || is6to4Address(ip) || isTeredoAddress(ip);
    }

    public static Inet4Address getEmbeddedIPv4ClientAddress(Inet6Address ip) {
        if (isCompatIPv4Address(ip)) {
            return getCompatIPv4Address(ip);
        }
        if (is6to4Address(ip)) {
            return get6to4IPv4Address(ip);
        }
        if (isTeredoAddress(ip)) {
            return getTeredoInfo(ip).getClient();
        }
        throw new IllegalArgumentException(String.format("'%s' has no embedded IPv4 address.", ip.getHostAddress()));
    }

    public static Inet4Address getCoercedIPv4Address(InetAddress ip) {
        long addressAsLong;
        if (ip instanceof Inet4Address) {
            return (Inet4Address) ip;
        }
        byte[] bytes = ip.getAddress();
        boolean leadingBytesOfZero = true;
        int i = 0;
        while (true) {
            if (i >= 15) {
                break;
            } else if (bytes[i] != 0) {
                leadingBytesOfZero = false;
                break;
            } else {
                i++;
            }
        }
        if (leadingBytesOfZero && bytes[15] == 1) {
            return LOOPBACK4;
        }
        if (leadingBytesOfZero && bytes[15] == 0) {
            return ANY4;
        }
        Inet6Address ip6 = (Inet6Address) ip;
        if (hasEmbeddedIPv4ClientAddress(ip6)) {
            addressAsLong = (long) getEmbeddedIPv4ClientAddress(ip6).hashCode();
        } else {
            addressAsLong = ByteBuffer.wrap(ip6.getAddress(), 0, 8).getLong();
        }
        int coercedHash = hash64To32(addressAsLong) | -536870912;
        if (coercedHash == -1) {
            coercedHash = -2;
        }
        return getInet4Address(Ints.toByteArray(coercedHash));
    }

    @VisibleForTesting
    static int hash64To32(long key) {
        long key2 = (-1 ^ key) + (key << 18);
        long key3 = (key2 ^ (key2 >>> 31)) * 21;
        long key4 = key3 ^ (key3 >>> 11);
        long key5 = key4 + (key4 << 6);
        return (int) (key5 ^ (key5 >>> 22));
    }

    public static int coerceToInteger(InetAddress ip) {
        return ByteStreams.newDataInput(getCoercedIPv4Address(ip).getAddress()).readInt();
    }

    public static Inet4Address fromInteger(int address) {
        return getInet4Address(Ints.toByteArray(address));
    }

    public static InetAddress fromLittleEndianByteArray(byte[] addr) throws UnknownHostException {
        byte[] reversed = new byte[addr.length];
        for (int i = 0; i < addr.length; i++) {
            reversed[i] = addr[(addr.length - i) - 1];
        }
        return InetAddress.getByAddress(reversed);
    }

    private static byte[] copyOfRange(byte[] original, int from, int to) {
        Preconditions.checkNotNull(original);
        byte[] result = new byte[(to - from)];
        System.arraycopy(original, from, result, 0, Math.min(to, original.length) - from);
        return result;
    }
}
