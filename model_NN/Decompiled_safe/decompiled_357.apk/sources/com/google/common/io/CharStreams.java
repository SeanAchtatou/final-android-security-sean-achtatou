package com.google.common.io;

import com.google.common.annotations.Beta;
import com.google.common.base.Preconditions;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Beta
public final class CharStreams {
    private static final int BUF_SIZE = 2048;

    private CharStreams() {
    }

    public static InputSupplier<StringReader> newReaderSupplier(final String value) {
        Preconditions.checkNotNull(value);
        return new InputSupplier<StringReader>() {
            public StringReader getInput() {
                return new StringReader(value);
            }
        };
    }

    public static InputSupplier<InputStreamReader> newReaderSupplier(final InputSupplier<? extends InputStream> in, final Charset charset) {
        Preconditions.checkNotNull(in);
        Preconditions.checkNotNull(charset);
        return new InputSupplier<InputStreamReader>() {
            public InputStreamReader getInput() throws IOException {
                return new InputStreamReader((InputStream) in.getInput(), charset);
            }
        };
    }

    public static OutputSupplier<OutputStreamWriter> newWriterSupplier(final OutputSupplier<? extends OutputStream> out, final Charset charset) {
        Preconditions.checkNotNull(out);
        Preconditions.checkNotNull(charset);
        return new OutputSupplier<OutputStreamWriter>() {
            public OutputStreamWriter getOutput() throws IOException {
                return new OutputStreamWriter((OutputStream) out.getOutput(), charset);
            }
        };
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: W
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static <W extends java.lang.Appendable & java.io.Closeable> void write(java.lang.CharSequence r3, com.google.common.io.OutputSupplier<W> r4) throws java.io.IOException {
        /*
            com.google.common.base.Preconditions.checkNotNull(r3)
            r1 = 1
            java.lang.Object r0 = r4.getOutput()
            java.lang.Appendable r0 = (java.lang.Appendable) r0
            r0.append(r3)     // Catch:{ all -> 0x0014 }
            r1 = 0
            java.io.Closeable r0 = (java.io.Closeable) r0
            com.google.common.io.Closeables.close(r0, r1)
            return
        L_0x0014:
            r2 = move-exception
            java.io.Closeable r0 = (java.io.Closeable) r0
            com.google.common.io.Closeables.close(r0, r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.io.CharStreams.write(java.lang.CharSequence, com.google.common.io.OutputSupplier):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.io.CharStreams.copy(java.lang.Readable, java.lang.Appendable):long
     arg types: [R, W]
     candidates:
      com.google.common.io.CharStreams.copy(com.google.common.io.InputSupplier, com.google.common.io.OutputSupplier):long
      com.google.common.io.CharStreams.copy(com.google.common.io.InputSupplier, java.lang.Appendable):long
      com.google.common.io.CharStreams.copy(java.lang.Readable, java.lang.Appendable):long */
    public static <R extends Readable & Closeable, W extends Appendable & Closeable> long copy(InputSupplier<R> from, OutputSupplier<W> to) throws IOException {
        boolean threw = true;
        W out = (Readable) from.getInput();
        try {
            out = (Appendable) to.getOutput();
            threw = false;
            return copy((Readable) out, (Appendable) out);
        } catch (Throwable th) {
            throw th;
        } finally {
            Closeables.close((Closeable) out, threw);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.io.CharStreams.copy(java.lang.Readable, java.lang.Appendable):long
     arg types: [R, java.lang.Appendable]
     candidates:
      com.google.common.io.CharStreams.copy(com.google.common.io.InputSupplier, com.google.common.io.OutputSupplier):long
      com.google.common.io.CharStreams.copy(com.google.common.io.InputSupplier, java.lang.Appendable):long
      com.google.common.io.CharStreams.copy(java.lang.Readable, java.lang.Appendable):long */
    public static <R extends Readable & Closeable> long copy(InputSupplier<R> from, Appendable to) throws IOException {
        boolean threw = true;
        R in = (Readable) from.getInput();
        try {
            long count = copy((Readable) in, to);
            threw = false;
            return count;
        } finally {
            Closeables.close((Closeable) in, threw);
        }
    }

    public static long copy(Readable from, Appendable to) throws IOException {
        CharBuffer buf = CharBuffer.allocate(2048);
        long total = 0;
        while (true) {
            int r = from.read(buf);
            if (r == -1) {
                return total;
            }
            buf.flip();
            to.append(buf, 0, r);
            total += (long) r;
        }
    }

    public static String toString(Readable r) throws IOException {
        return toStringBuilder(r).toString();
    }

    public static <R extends Readable & Closeable> String toString(InputSupplier<R> supplier) throws IOException {
        return toStringBuilder(supplier).toString();
    }

    private static StringBuilder toStringBuilder(Readable r) throws IOException {
        StringBuilder sb = new StringBuilder();
        copy(r, sb);
        return sb;
    }

    private static <R extends Readable & Closeable> StringBuilder toStringBuilder(InputSupplier<R> supplier) throws IOException {
        boolean threw = true;
        R r = (Readable) supplier.getInput();
        try {
            StringBuilder result = toStringBuilder((Readable) r);
            threw = false;
            return result;
        } finally {
            Closeables.close((Closeable) r, threw);
        }
    }

    public static <R extends Readable & Closeable> String readFirstLine(InputSupplier<R> supplier) throws IOException {
        boolean threw = true;
        R r = (Readable) supplier.getInput();
        try {
            String line = new LineReader(r).readLine();
            threw = false;
            return line;
        } finally {
            Closeables.close((Closeable) r, threw);
        }
    }

    public static <R extends Readable & Closeable> List<String> readLines(InputSupplier<R> supplier) throws IOException {
        boolean threw = true;
        R r = (Readable) supplier.getInput();
        try {
            List<String> result = readLines((Readable) r);
            threw = false;
            return result;
        } finally {
            Closeables.close((Closeable) r, threw);
        }
    }

    public static List<String> readLines(Readable r) throws IOException {
        List<String> result = new ArrayList<>();
        LineReader lineReader = new LineReader(r);
        while (true) {
            String line = lineReader.readLine();
            if (line == null) {
                return result;
            }
            result.add(line);
        }
    }

    /* JADX INFO: finally extract failed */
    public static <R extends Readable & Closeable, T> T readLines(InputSupplier<R> supplier, LineProcessor<T> callback) throws IOException {
        String line;
        R r = (Readable) supplier.getInput();
        try {
            LineReader lineReader = new LineReader(r);
            do {
                line = lineReader.readLine();
                if (line == null) {
                    break;
                }
            } while (callback.processLine(line));
            Closeables.close((Closeable) r, false);
            return callback.getResult();
        } catch (Throwable th) {
            Closeables.close((Closeable) r, true);
            throw th;
        }
    }

    public static InputSupplier<Reader> join(final Iterable<? extends InputSupplier<? extends Reader>> suppliers) {
        return new InputSupplier<Reader>() {
            public Reader getInput() throws IOException {
                return new MultiReader(suppliers.iterator());
            }
        };
    }

    public static InputSupplier<Reader> join(InputSupplier<? extends Reader>... suppliers) {
        return join(Arrays.asList(suppliers));
    }

    public static void skipFully(Reader reader, long n) throws IOException {
        while (n > 0) {
            long amt = reader.skip(n);
            if (amt != 0) {
                n -= amt;
            } else if (reader.read() == -1) {
                throw new EOFException();
            } else {
                n--;
            }
        }
    }

    public static Writer asWriter(Appendable target) {
        if (target instanceof Writer) {
            return (Writer) target;
        }
        return new AppendableWriter(target);
    }
}
