package com.google.common.collect;

import com.google.common.annotations.Beta;
import javax.annotation.Nullable;

@Beta
public interface MapEvictionListener<K, V> {
    void onEviction(@Nullable K k, @Nullable V v);
}
