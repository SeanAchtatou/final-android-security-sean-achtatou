package com.google.common.collect;

import com.google.common.annotations.GwtCompatible;
import com.google.common.annotations.GwtIncompatible;
import java.lang.reflect.Array;
import java.util.logging.Logger;

@GwtCompatible(emulated = true)
class Platform {
    private static final Logger logger = Logger.getLogger(Platform.class.getCanonicalName());

    static <T> T[] clone(T[] array) {
        return (Object[]) array.clone();
    }

    static void unsafeArrayCopy(Object[] src2, int srcPos, Object[] dest, int destPos, int length) {
        System.arraycopy(src2, srcPos, dest, destPos, length);
    }

    @GwtIncompatible("Array.newInstance(Class, int)")
    static <T> T[] newArray(Class cls, int length) {
        return (Object[]) Array.newInstance(cls, length);
    }

    static <T> T[] newArray(Object[] objArr, int length) {
        return (Object[]) ((Object[]) Array.newInstance(objArr.getClass().getComponentType(), length));
    }

    static MapMaker tryWeakKeys(MapMaker mapMaker) {
        return mapMaker.weakKeys();
    }

    private Platform() {
    }
}
