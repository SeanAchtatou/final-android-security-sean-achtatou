package com.google.common.collect;

import com.google.common.annotations.GwtCompatible;
import java.util.Map;
import javax.annotation.Nullable;
import org.codehaus.jackson.org.objectweb.asm.signature.SignatureVisitor;

@GwtCompatible(emulated = true, serializable = true)
final class SingletonImmutableMap<K, V> extends ImmutableMap<K, V> {
    private transient Map.Entry<K, V> entry;
    private transient ImmutableSet<Map.Entry<K, V>> entrySet;
    private transient ImmutableSet<K> keySet;
    final transient K singleKey;
    final transient V singleValue;
    private transient ImmutableCollection<V> values;

    SingletonImmutableMap(K singleKey2, V singleValue2) {
        this.singleKey = singleKey2;
        this.singleValue = singleValue2;
    }

    SingletonImmutableMap(Map.Entry<K, V> entry2) {
        this.entry = entry2;
        this.singleKey = entry2.getKey();
        this.singleValue = entry2.getValue();
    }

    private Map.Entry<K, V> entry() {
        Map.Entry<K, V> e = this.entry;
        if (e != null) {
            return e;
        }
        Map.Entry<K, V> immutableEntry = Maps.immutableEntry(this.singleKey, this.singleValue);
        this.entry = immutableEntry;
        return immutableEntry;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public V get(java.lang.Object r2) {
        /*
            r1 = this;
            K r0 = r1.singleKey
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x000b
            V r0 = r1.singleValue
        L_0x000a:
            return r0
        L_0x000b:
            r0 = 0
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.SingletonImmutableMap.get(java.lang.Object):java.lang.Object");
    }

    public int size() {
        return 1;
    }

    public boolean isEmpty() {
        return false;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean containsKey(java.lang.Object r2) {
        /*
            r1 = this;
            K r0 = r1.singleKey
            boolean r0 = r0.equals(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.SingletonImmutableMap.containsKey(java.lang.Object):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean containsValue(java.lang.Object r2) {
        /*
            r1 = this;
            V r0 = r1.singleValue
            boolean r0 = r0.equals(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.SingletonImmutableMap.containsValue(java.lang.Object):boolean");
    }

    /* access modifiers changed from: package-private */
    public boolean isPartialView() {
        return false;
    }

    public ImmutableSet<Map.Entry<K, V>> entrySet() {
        ImmutableSet<Map.Entry<K, V>> es = this.entrySet;
        if (es != null) {
            return es;
        }
        ImmutableSet<Map.Entry<K, V>> of = ImmutableSet.of(entry());
        this.entrySet = of;
        return of;
    }

    public ImmutableSet<K> keySet() {
        ImmutableSet<K> ks = this.keySet;
        if (ks != null) {
            return ks;
        }
        ImmutableSet<K> of = ImmutableSet.of(this.singleKey);
        this.keySet = of;
        return of;
    }

    public ImmutableCollection<V> values() {
        ImmutableCollection<V> v = this.values;
        if (v != null) {
            return v;
        }
        Values values2 = new Values(this.singleValue);
        this.values = values2;
        return values2;
    }

    private static class Values<V> extends ImmutableCollection<V> {
        final V singleValue;

        Values(V singleValue2) {
            this.singleValue = singleValue2;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public boolean contains(java.lang.Object r2) {
            /*
                r1 = this;
                V r0 = r1.singleValue
                boolean r0 = r0.equals(r2)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.SingletonImmutableMap.Values.contains(java.lang.Object):boolean");
        }

        public boolean isEmpty() {
            return false;
        }

        public int size() {
            return 1;
        }

        public UnmodifiableIterator<V> iterator() {
            return Iterators.singletonIterator(this.singleValue);
        }

        /* access modifiers changed from: package-private */
        public boolean isPartialView() {
            return true;
        }
    }

    public boolean equals(@Nullable Object object) {
        if (object == this) {
            return true;
        }
        if (!(object instanceof Map)) {
            return false;
        }
        Map map = (Map) object;
        if (map.size() != 1) {
            return false;
        }
        Map.Entry<?, ?> entry2 = (Map.Entry) map.entrySet().iterator().next();
        return this.singleKey.equals(entry2.getKey()) && this.singleValue.equals(entry2.getValue());
    }

    public int hashCode() {
        return this.singleKey.hashCode() ^ this.singleValue.hashCode();
    }

    public String toString() {
        return '{' + this.singleKey.toString() + ((char) SignatureVisitor.INSTANCEOF) + this.singleValue.toString() + '}';
    }
}
