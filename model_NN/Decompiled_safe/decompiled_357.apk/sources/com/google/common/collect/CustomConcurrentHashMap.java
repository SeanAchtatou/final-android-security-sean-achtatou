package com.google.common.collect;

import com.google.common.base.Equivalence;
import com.google.common.base.Equivalences;
import com.google.common.base.FinalizableReferenceQueue;
import com.google.common.base.FinalizableSoftReference;
import com.google.common.base.FinalizableWeakReference;
import com.google.common.base.Preconditions;
import com.google.common.primitives.Ints;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.AbstractQueue;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.locks.ReentrantLock;
import javax.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;

class CustomConcurrentHashMap<K, V> extends AbstractMap<K, V> implements ConcurrentMap<K, V>, Serializable {
    static final int MAXIMUM_CAPACITY = 1073741824;
    static final int MAX_SEGMENTS = 65536;
    static final int RECENCY_THRESHOLD = 64;
    static final int RETRIES_BEFORE_LOCK = 2;
    static final ValueReference<Object, Object> UNSET = new ValueReference<Object, Object>() {
        public Object get() {
            return null;
        }

        public ValueReference<Object, Object> copyFor(ReferenceEntry<Object, Object> referenceEntry) {
            throw new AssertionError();
        }

        public Object waitForValue() {
            throw new AssertionError();
        }

        public void clear() {
        }
    };
    static final Queue<Object> discardingQueue = new AbstractQueue<Object>() {
        public boolean offer(Object o) {
            return true;
        }

        public Object peek() {
            return null;
        }

        public Object poll() {
            return null;
        }

        public int size() {
            return 0;
        }

        public Iterator<Object> iterator() {
            return Iterators.emptyIterator();
        }
    };
    private static final long serialVersionUID = 4;
    final int concurrencyLevel;
    final transient EntryFactory entryFactory;
    Set<Map.Entry<K, V>> entrySet;
    final MapEvictionListener<? super K, ? super V> evictionListener;
    final long expireAfterAccessNanos;
    final long expireAfterWriteNanos;
    final Equivalence<Object> keyEquivalence;
    Set<K> keySet;
    final Strength keyStrength;
    final int maximumSize;
    final Queue<ReferenceEntry<K, V>> pendingEvictionNotifications;
    final transient int segmentMask;
    final transient int segmentShift;
    final transient CustomConcurrentHashMap<K, V>.Segment[] segments;
    final Equivalence<Object> valueEquivalence;
    final Strength valueStrength;
    Collection<V> values;

    interface Evictable {
        Evictable getNextEvictable();

        Evictable getPreviousEvictable();

        void setNextEvictable(Evictable evictable);

        void setPreviousEvictable(Evictable evictable);
    }

    interface Expirable {
        long getExpirationTime();

        Expirable getNextExpirable();

        Expirable getPreviousExpirable();

        void setExpirationTime(long j);

        void setNextExpirable(Expirable expirable);

        void setPreviousExpirable(Expirable expirable);
    }

    interface ReferenceEntry<K, V> {
        int getHash();

        K getKey();

        ReferenceEntry<K, V> getNext();

        ValueReference<K, V> getValueReference();

        void setValueReference(ValueReference<K, V> valueReference);

        void valueReclaimed();
    }

    enum Strength {
        STRONG {
            /* access modifiers changed from: package-private */
            public <K, V> ValueReference<K, V> referenceValue(ReferenceEntry<K, V> referenceEntry, V value) {
                return new StrongValueReference(value);
            }

            /* access modifiers changed from: package-private */
            public Equivalence<Object> defaultEquivalence() {
                return Equivalences.equals();
            }
        },
        SOFT {
            /* access modifiers changed from: package-private */
            public <K, V> ValueReference<K, V> referenceValue(ReferenceEntry<K, V> entry, V value) {
                return new SoftValueReference(value, entry);
            }

            /* access modifiers changed from: package-private */
            public Equivalence<Object> defaultEquivalence() {
                return Equivalences.identity();
            }
        },
        WEAK {
            /* access modifiers changed from: package-private */
            public <K, V> ValueReference<K, V> referenceValue(ReferenceEntry<K, V> entry, V value) {
                return new WeakValueReference(value, entry);
            }

            /* access modifiers changed from: package-private */
            public Equivalence<Object> defaultEquivalence() {
                return Equivalences.identity();
            }
        };

        /* access modifiers changed from: package-private */
        public abstract Equivalence<Object> defaultEquivalence();

        /* access modifiers changed from: package-private */
        public abstract <K, V> ValueReference<K, V> referenceValue(ReferenceEntry<K, V> referenceEntry, V v);
    }

    interface ValueReference<K, V> {
        void clear();

        ValueReference<K, V> copyFor(ReferenceEntry<K, V> referenceEntry);

        V get();

        V waitForValue() throws InterruptedException;
    }

    CustomConcurrentHashMap(MapMaker builder) {
        this.keyStrength = builder.getKeyStrength();
        this.valueStrength = builder.getValueStrength();
        this.keyEquivalence = builder.getKeyEquivalence();
        this.valueEquivalence = builder.getValueEquivalence();
        this.expireAfterAccessNanos = builder.getExpireAfterAccessNanos();
        this.expireAfterWriteNanos = builder.getExpireAfterWriteNanos();
        this.maximumSize = builder.maximumSize;
        boolean evictsBySize = evictsBySize();
        this.entryFactory = EntryFactory.getFactory(this.keyStrength, expires(), evictsBySize);
        MapEvictionListener<? super K, ? super V> evictionListener2 = builder.evictionListener;
        if (evictionListener2 == null || evictionListener2.equals(NullListener.INSTANCE)) {
            this.pendingEvictionNotifications = discardingQueue;
            this.evictionListener = NullListener.INSTANCE;
        } else {
            this.pendingEvictionNotifications = new ConcurrentLinkedQueue();
            this.evictionListener = evictionListener2;
        }
        this.concurrencyLevel = filterConcurrencyLevel(builder.getConcurrencyLevel());
        int initialCapacity = builder.getInitialCapacity();
        initialCapacity = initialCapacity > MAXIMUM_CAPACITY ? MAXIMUM_CAPACITY : initialCapacity;
        int segmentShift2 = 0;
        int segmentCount = 1;
        while (segmentCount < this.concurrencyLevel && (!evictsBySize || segmentCount * 2 <= this.maximumSize)) {
            segmentShift2++;
            segmentCount <<= 1;
        }
        this.segmentShift = 32 - segmentShift2;
        this.segmentMask = segmentCount - 1;
        this.segments = newSegmentArray(segmentCount);
        int segmentCapacity = initialCapacity / segmentCount;
        int segmentSize = 1;
        while (segmentSize < (segmentCapacity * segmentCount < initialCapacity ? segmentCapacity + 1 : segmentCapacity)) {
            segmentSize <<= 1;
        }
        if (evictsBySize) {
            int maximumSegmentSize = (this.maximumSize / segmentCount) + 1;
            int remainder = this.maximumSize % segmentCount;
            for (int i = 0; i < this.segments.length; i++) {
                if (i == remainder) {
                    maximumSegmentSize--;
                }
                this.segments[i] = createSegment(segmentSize, maximumSegmentSize);
            }
            return;
        }
        for (int i2 = 0; i2 < this.segments.length; i2++) {
            this.segments[i2] = createSegment(segmentSize, -1);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean evictsBySize() {
        return this.maximumSize != -1;
    }

    /* access modifiers changed from: package-private */
    public boolean expires() {
        return expiresAfterWrite() || expiresAfterAccess();
    }

    /* access modifiers changed from: package-private */
    public boolean expiresAfterWrite() {
        return this.expireAfterWriteNanos > 0;
    }

    /* access modifiers changed from: package-private */
    public boolean expiresAfterAccess() {
        return this.expireAfterAccessNanos > 0;
    }

    static int filterConcurrencyLevel(int concurrenyLevel) {
        return Math.min(concurrenyLevel, (int) MAX_SEGMENTS);
    }

    enum EntryFactory {
        STRONG {
            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> newEntry(CustomConcurrentHashMap<K, V> map, K key, int hash, @Nullable ReferenceEntry<K, V> next) {
                return new StrongEntry(map, key, hash, next);
            }
        },
        STRONG_EXPIRABLE {
            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> newEntry(CustomConcurrentHashMap<K, V> map, K key, int hash, @Nullable ReferenceEntry<K, V> next) {
                return new StrongExpirableEntry(map, key, hash, next);
            }

            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> copyEntry(CustomConcurrentHashMap<K, V> map, ReferenceEntry<K, V> original, ReferenceEntry<K, V> newNext) {
                ReferenceEntry<K, V> newEntry = super.copyEntry(map, original, newNext);
                copyExpirableEntry(original, newEntry);
                return newEntry;
            }
        },
        STRONG_EVICTABLE {
            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> newEntry(CustomConcurrentHashMap<K, V> map, K key, int hash, @Nullable ReferenceEntry<K, V> next) {
                return new StrongEvictableEntry(map, key, hash, next);
            }

            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> copyEntry(CustomConcurrentHashMap<K, V> map, ReferenceEntry<K, V> original, ReferenceEntry<K, V> newNext) {
                ReferenceEntry<K, V> newEntry = super.copyEntry(map, original, newNext);
                copyEvictableEntry(original, newEntry);
                return newEntry;
            }
        },
        STRONG_EXPIRABLE_EVICTABLE {
            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> newEntry(CustomConcurrentHashMap<K, V> map, K key, int hash, @Nullable ReferenceEntry<K, V> next) {
                return new StrongExpirableEvictableEntry(map, key, hash, next);
            }

            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> copyEntry(CustomConcurrentHashMap<K, V> map, ReferenceEntry<K, V> original, ReferenceEntry<K, V> newNext) {
                ReferenceEntry<K, V> newEntry = super.copyEntry(map, original, newNext);
                copyExpirableEntry(original, newEntry);
                copyEvictableEntry(original, newEntry);
                return newEntry;
            }
        },
        SOFT {
            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> newEntry(CustomConcurrentHashMap<K, V> map, K key, int hash, @Nullable ReferenceEntry<K, V> next) {
                return new SoftEntry(map, key, hash, next);
            }
        },
        SOFT_EXPIRABLE {
            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> newEntry(CustomConcurrentHashMap<K, V> map, K key, int hash, @Nullable ReferenceEntry<K, V> next) {
                return new SoftExpirableEntry(map, key, hash, next);
            }

            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> copyEntry(CustomConcurrentHashMap<K, V> map, ReferenceEntry<K, V> original, ReferenceEntry<K, V> newNext) {
                ReferenceEntry<K, V> newEntry = super.copyEntry(map, original, newNext);
                copyExpirableEntry(original, newEntry);
                return newEntry;
            }
        },
        SOFT_EVICTABLE {
            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> newEntry(CustomConcurrentHashMap<K, V> map, K key, int hash, @Nullable ReferenceEntry<K, V> next) {
                return new SoftEvictableEntry(map, key, hash, next);
            }

            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> copyEntry(CustomConcurrentHashMap<K, V> map, ReferenceEntry<K, V> original, ReferenceEntry<K, V> newNext) {
                ReferenceEntry<K, V> newEntry = super.copyEntry(map, original, newNext);
                copyEvictableEntry(original, newEntry);
                return newEntry;
            }
        },
        SOFT_EXPIRABLE_EVICTABLE {
            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> newEntry(CustomConcurrentHashMap<K, V> map, K key, int hash, @Nullable ReferenceEntry<K, V> next) {
                return new SoftExpirableEvictableEntry(map, key, hash, next);
            }

            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> copyEntry(CustomConcurrentHashMap<K, V> map, ReferenceEntry<K, V> original, ReferenceEntry<K, V> newNext) {
                ReferenceEntry<K, V> newEntry = super.copyEntry(map, original, newNext);
                copyExpirableEntry(original, newEntry);
                copyEvictableEntry(original, newEntry);
                return newEntry;
            }
        },
        WEAK {
            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> newEntry(CustomConcurrentHashMap<K, V> map, K key, int hash, @Nullable ReferenceEntry<K, V> next) {
                return new WeakEntry(map, key, hash, next);
            }
        },
        WEAK_EXPIRABLE {
            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> newEntry(CustomConcurrentHashMap<K, V> map, K key, int hash, @Nullable ReferenceEntry<K, V> next) {
                return new WeakExpirableEntry(map, key, hash, next);
            }

            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> copyEntry(CustomConcurrentHashMap<K, V> map, ReferenceEntry<K, V> original, ReferenceEntry<K, V> newNext) {
                ReferenceEntry<K, V> newEntry = super.copyEntry(map, original, newNext);
                copyExpirableEntry(original, newEntry);
                return newEntry;
            }
        },
        WEAK_EVICTABLE {
            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> newEntry(CustomConcurrentHashMap<K, V> map, K key, int hash, @Nullable ReferenceEntry<K, V> next) {
                return new WeakEvictableEntry(map, key, hash, next);
            }

            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> copyEntry(CustomConcurrentHashMap<K, V> map, ReferenceEntry<K, V> original, ReferenceEntry<K, V> newNext) {
                ReferenceEntry<K, V> newEntry = super.copyEntry(map, original, newNext);
                copyEvictableEntry(original, newEntry);
                return newEntry;
            }
        },
        WEAK_EXPIRABLE_EVICTABLE {
            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> newEntry(CustomConcurrentHashMap<K, V> map, K key, int hash, @Nullable ReferenceEntry<K, V> next) {
                return new WeakExpirableEvictableEntry(map, key, hash, next);
            }

            /* access modifiers changed from: package-private */
            public <K, V> ReferenceEntry<K, V> copyEntry(CustomConcurrentHashMap<K, V> map, ReferenceEntry<K, V> original, ReferenceEntry<K, V> newNext) {
                ReferenceEntry<K, V> newEntry = super.copyEntry(map, original, newNext);
                copyExpirableEntry(original, newEntry);
                copyEvictableEntry(original, newEntry);
                return newEntry;
            }
        };
        
        static final int EVICTABLE_MASK = 2;
        static final int EXPIRABLE_MASK = 1;
        static final EntryFactory[][] factories = {new EntryFactory[]{STRONG, STRONG_EXPIRABLE, STRONG_EVICTABLE, STRONG_EXPIRABLE_EVICTABLE}, new EntryFactory[]{SOFT, SOFT_EXPIRABLE, SOFT_EVICTABLE, SOFT_EXPIRABLE_EVICTABLE}, new EntryFactory[]{WEAK, WEAK_EXPIRABLE, WEAK_EVICTABLE, WEAK_EXPIRABLE_EVICTABLE}};

        /* access modifiers changed from: package-private */
        public abstract <K, V> ReferenceEntry<K, V> newEntry(CustomConcurrentHashMap<K, V> customConcurrentHashMap, K k, int i, @Nullable ReferenceEntry<K, V> referenceEntry);

        static EntryFactory getFactory(Strength keyStrength, boolean expireAfterWrite, boolean evictsBySize) {
            int i;
            int i2 = 0;
            if (expireAfterWrite) {
                i = 1;
            } else {
                i = 0;
            }
            if (evictsBySize) {
                i2 = 2;
            }
            return factories[keyStrength.ordinal()][i | i2];
        }

        /* access modifiers changed from: package-private */
        @GuardedBy("Segment.this")
        public <K, V> ReferenceEntry<K, V> copyEntry(CustomConcurrentHashMap<K, V> map, ReferenceEntry<K, V> original, ReferenceEntry<K, V> newNext) {
            return newEntry(map, original.getKey(), original.getHash(), newNext);
        }

        /* access modifiers changed from: package-private */
        @GuardedBy("Segment.this")
        public <K, V> void copyExpirableEntry(ReferenceEntry<K, V> original, ReferenceEntry<K, V> newEntry) {
            Expirable originalExpirable = (Expirable) original;
            Expirable newExpirable = (Expirable) newEntry;
            newExpirable.setExpirationTime(originalExpirable.getExpirationTime());
            CustomConcurrentHashMap.connectExpirables(originalExpirable.getPreviousExpirable(), newExpirable);
            CustomConcurrentHashMap.connectExpirables(newExpirable, originalExpirable.getNextExpirable());
            CustomConcurrentHashMap.nullifyExpirable(originalExpirable);
        }

        /* access modifiers changed from: package-private */
        @GuardedBy("Segment.this")
        public <K, V> void copyEvictableEntry(ReferenceEntry<K, V> original, ReferenceEntry<K, V> newEntry) {
            Evictable originalEvictable = (Evictable) original;
            Evictable newEvictable = (Evictable) newEntry;
            CustomConcurrentHashMap.connectEvictables(originalEvictable.getPreviousEvictable(), newEvictable);
            CustomConcurrentHashMap.connectEvictables(newEvictable, originalEvictable.getNextEvictable());
            CustomConcurrentHashMap.nullifyEvictable(originalEvictable);
        }
    }

    /* access modifiers changed from: private */
    public static <K, V> ValueReference<K, V> unset() {
        return UNSET;
    }

    private static class QueueHolder {
        static final FinalizableReferenceQueue queue = new FinalizableReferenceQueue();

        private QueueHolder() {
        }
    }

    private enum NullExpirable implements Expirable {
        INSTANCE;

        public long getExpirationTime() {
            return 0;
        }

        public void setExpirationTime(long time) {
        }

        public Expirable getNextExpirable() {
            return this;
        }

        public void setNextExpirable(Expirable next) {
        }

        public Expirable getPreviousExpirable() {
            return this;
        }

        public void setPreviousExpirable(Expirable previous) {
        }
    }

    enum NullEvictable implements Evictable {
        INSTANCE;

        public Evictable getNextEvictable() {
            return this;
        }

        public void setNextEvictable(Evictable next) {
        }

        public Evictable getPreviousEvictable() {
            return this;
        }

        public void setPreviousEvictable(Evictable previous) {
        }
    }

    enum NullListener implements MapEvictionListener {
        INSTANCE;

        public void onEviction(Object key, Object value) {
        }
    }

    private static class StrongEntry<K, V> implements ReferenceEntry<K, V> {
        final int hash;
        final K key;
        final CustomConcurrentHashMap<K, V> map;
        final ReferenceEntry<K, V> next;
        volatile ValueReference<K, V> valueReference = CustomConcurrentHashMap.unset();

        StrongEntry(CustomConcurrentHashMap<K, V> map2, K key2, int hash2, @Nullable ReferenceEntry<K, V> next2) {
            this.map = map2;
            this.key = key2;
            this.hash = hash2;
            this.next = next2;
        }

        public K getKey() {
            return this.key;
        }

        public ValueReference<K, V> getValueReference() {
            return this.valueReference;
        }

        public void setValueReference(ValueReference<K, V> valueReference2) {
            if (this.valueReference != null) {
                this.valueReference.clear();
            }
            this.valueReference = valueReference2;
        }

        public void valueReclaimed() {
            this.map.reclaimValue(this);
        }

        public int getHash() {
            return this.hash;
        }

        public ReferenceEntry<K, V> getNext() {
            return this.next;
        }
    }

    private static class StrongExpirableEntry<K, V> extends StrongEntry<K, V> implements Expirable {
        @GuardedBy("Segment.this")
        Expirable nextExpirable = NullExpirable.INSTANCE;
        @GuardedBy("Segment.this")
        Expirable previousExpirable = NullExpirable.INSTANCE;
        volatile long time = Long.MAX_VALUE;

        StrongExpirableEntry(CustomConcurrentHashMap<K, V> map, K key, int hash, @Nullable ReferenceEntry<K, V> next) {
            super(map, key, hash, next);
        }

        public long getExpirationTime() {
            return this.time;
        }

        public void setExpirationTime(long time2) {
            this.time = time2;
        }

        public Expirable getNextExpirable() {
            return this.nextExpirable;
        }

        public void setNextExpirable(Expirable next) {
            this.nextExpirable = next;
        }

        public Expirable getPreviousExpirable() {
            return this.previousExpirable;
        }

        public void setPreviousExpirable(Expirable previous) {
            this.previousExpirable = previous;
        }
    }

    private static class StrongEvictableEntry<K, V> extends StrongEntry<K, V> implements Evictable {
        @GuardedBy("Segment.this")
        Evictable nextEvictable = NullEvictable.INSTANCE;
        @GuardedBy("Segment.this")
        Evictable previousEvictable = NullEvictable.INSTANCE;

        StrongEvictableEntry(CustomConcurrentHashMap<K, V> map, K key, int hash, @Nullable ReferenceEntry<K, V> next) {
            super(map, key, hash, next);
        }

        public Evictable getNextEvictable() {
            return this.nextEvictable;
        }

        public void setNextEvictable(Evictable next) {
            this.nextEvictable = next;
        }

        public Evictable getPreviousEvictable() {
            return this.previousEvictable;
        }

        public void setPreviousEvictable(Evictable previous) {
            this.previousEvictable = previous;
        }
    }

    private static class StrongExpirableEvictableEntry<K, V> extends StrongEntry<K, V> implements Expirable, Evictable {
        @GuardedBy("Segment.this")
        Evictable nextEvictable = NullEvictable.INSTANCE;
        @GuardedBy("Segment.this")
        Expirable nextExpirable = NullExpirable.INSTANCE;
        @GuardedBy("Segment.this")
        Evictable previousEvictable = NullEvictable.INSTANCE;
        @GuardedBy("Segment.this")
        Expirable previousExpirable = NullExpirable.INSTANCE;
        volatile long time = Long.MAX_VALUE;

        StrongExpirableEvictableEntry(CustomConcurrentHashMap<K, V> map, K key, int hash, @Nullable ReferenceEntry<K, V> next) {
            super(map, key, hash, next);
        }

        public long getExpirationTime() {
            return this.time;
        }

        public void setExpirationTime(long time2) {
            this.time = time2;
        }

        public Expirable getNextExpirable() {
            return this.nextExpirable;
        }

        public void setNextExpirable(Expirable next) {
            this.nextExpirable = next;
        }

        public Expirable getPreviousExpirable() {
            return this.previousExpirable;
        }

        public void setPreviousExpirable(Expirable previous) {
            this.previousExpirable = previous;
        }

        public Evictable getNextEvictable() {
            return this.nextEvictable;
        }

        public void setNextEvictable(Evictable next) {
            this.nextEvictable = next;
        }

        public Evictable getPreviousEvictable() {
            return this.previousEvictable;
        }

        public void setPreviousEvictable(Evictable previous) {
            this.previousEvictable = previous;
        }
    }

    private static class SoftEntry<K, V> extends FinalizableSoftReference<K> implements ReferenceEntry<K, V> {
        final int hash;
        final CustomConcurrentHashMap<K, V> map;
        final ReferenceEntry<K, V> next;
        volatile ValueReference<K, V> valueReference = CustomConcurrentHashMap.unset();

        SoftEntry(CustomConcurrentHashMap<K, V> map2, K key, int hash2, @Nullable ReferenceEntry<K, V> next2) {
            super(key, QueueHolder.queue);
            this.map = map2;
            this.hash = hash2;
            this.next = next2;
        }

        public K getKey() {
            return get();
        }

        public void finalizeReferent() {
            if (this.map.removeEntry(this)) {
                this.map.pendingEvictionNotifications.offer(this);
            }
        }

        public ValueReference<K, V> getValueReference() {
            return this.valueReference;
        }

        public void setValueReference(ValueReference<K, V> valueReference2) {
            if (this.valueReference != null) {
                this.valueReference.clear();
            }
            this.valueReference = valueReference2;
        }

        public void valueReclaimed() {
            this.map.reclaimValue(this);
        }

        public int getHash() {
            return this.hash;
        }

        public ReferenceEntry<K, V> getNext() {
            return this.next;
        }
    }

    private static class SoftExpirableEntry<K, V> extends SoftEntry<K, V> implements Expirable {
        @GuardedBy("Segment.this")
        Expirable nextExpirable = NullExpirable.INSTANCE;
        @GuardedBy("Segment.this")
        Expirable previousExpirable = NullExpirable.INSTANCE;
        volatile long time = Long.MAX_VALUE;

        SoftExpirableEntry(CustomConcurrentHashMap<K, V> map, K key, int hash, @Nullable ReferenceEntry<K, V> next) {
            super(map, key, hash, next);
        }

        public long getExpirationTime() {
            return this.time;
        }

        public void setExpirationTime(long time2) {
            this.time = time2;
        }

        public Expirable getNextExpirable() {
            return this.nextExpirable;
        }

        public void setNextExpirable(Expirable next) {
            this.nextExpirable = next;
        }

        public Expirable getPreviousExpirable() {
            return this.previousExpirable;
        }

        public void setPreviousExpirable(Expirable previous) {
            this.previousExpirable = previous;
        }
    }

    private static class SoftEvictableEntry<K, V> extends SoftEntry<K, V> implements Evictable {
        @GuardedBy("Segment.this")
        Evictable nextEvictable = NullEvictable.INSTANCE;
        @GuardedBy("Segment.this")
        Evictable previousEvictable = NullEvictable.INSTANCE;

        SoftEvictableEntry(CustomConcurrentHashMap<K, V> map, K key, int hash, @Nullable ReferenceEntry<K, V> next) {
            super(map, key, hash, next);
        }

        public Evictable getNextEvictable() {
            return this.nextEvictable;
        }

        public void setNextEvictable(Evictable next) {
            this.nextEvictable = next;
        }

        public Evictable getPreviousEvictable() {
            return this.previousEvictable;
        }

        public void setPreviousEvictable(Evictable previous) {
            this.previousEvictable = previous;
        }
    }

    private static class SoftExpirableEvictableEntry<K, V> extends SoftEntry<K, V> implements Expirable, Evictable {
        @GuardedBy("Segment.this")
        Evictable nextEvictable = NullEvictable.INSTANCE;
        @GuardedBy("Segment.this")
        Expirable nextExpirable = NullExpirable.INSTANCE;
        @GuardedBy("Segment.this")
        Evictable previousEvictable = NullEvictable.INSTANCE;
        @GuardedBy("Segment.this")
        Expirable previousExpirable = NullExpirable.INSTANCE;
        volatile long time = Long.MAX_VALUE;

        SoftExpirableEvictableEntry(CustomConcurrentHashMap<K, V> map, K key, int hash, @Nullable ReferenceEntry<K, V> next) {
            super(map, key, hash, next);
        }

        public long getExpirationTime() {
            return this.time;
        }

        public void setExpirationTime(long time2) {
            this.time = time2;
        }

        public Expirable getNextExpirable() {
            return this.nextExpirable;
        }

        public void setNextExpirable(Expirable next) {
            this.nextExpirable = next;
        }

        public Expirable getPreviousExpirable() {
            return this.previousExpirable;
        }

        public void setPreviousExpirable(Expirable previous) {
            this.previousExpirable = previous;
        }

        public Evictable getNextEvictable() {
            return this.nextEvictable;
        }

        public void setNextEvictable(Evictable next) {
            this.nextEvictable = next;
        }

        public Evictable getPreviousEvictable() {
            return this.previousEvictable;
        }

        public void setPreviousEvictable(Evictable previous) {
            this.previousEvictable = previous;
        }
    }

    private static class WeakEntry<K, V> extends FinalizableWeakReference<K> implements ReferenceEntry<K, V> {
        final int hash;
        final CustomConcurrentHashMap<K, V> map;
        final ReferenceEntry<K, V> next;
        volatile ValueReference<K, V> valueReference = CustomConcurrentHashMap.unset();

        WeakEntry(CustomConcurrentHashMap<K, V> map2, K key, int hash2, @Nullable ReferenceEntry<K, V> next2) {
            super(key, QueueHolder.queue);
            this.map = map2;
            this.hash = hash2;
            this.next = next2;
        }

        public K getKey() {
            return get();
        }

        public void finalizeReferent() {
            if (this.map.removeEntry(this)) {
                this.map.pendingEvictionNotifications.offer(this);
            }
        }

        public ValueReference<K, V> getValueReference() {
            return this.valueReference;
        }

        public void setValueReference(ValueReference<K, V> valueReference2) {
            if (this.valueReference != null) {
                this.valueReference.clear();
            }
            this.valueReference = valueReference2;
        }

        public void valueReclaimed() {
            this.map.reclaimValue(this);
        }

        public int getHash() {
            return this.hash;
        }

        public ReferenceEntry<K, V> getNext() {
            return this.next;
        }
    }

    private static class WeakExpirableEntry<K, V> extends WeakEntry<K, V> implements Expirable {
        @GuardedBy("Segment.this")
        Expirable nextExpirable = NullExpirable.INSTANCE;
        @GuardedBy("Segment.this")
        Expirable previousExpirable = NullExpirable.INSTANCE;
        volatile long time = Long.MAX_VALUE;

        WeakExpirableEntry(CustomConcurrentHashMap<K, V> map, K key, int hash, @Nullable ReferenceEntry<K, V> next) {
            super(map, key, hash, next);
        }

        public long getExpirationTime() {
            return this.time;
        }

        public void setExpirationTime(long time2) {
            this.time = time2;
        }

        public Expirable getNextExpirable() {
            return this.nextExpirable;
        }

        public void setNextExpirable(Expirable next) {
            this.nextExpirable = next;
        }

        public Expirable getPreviousExpirable() {
            return this.previousExpirable;
        }

        public void setPreviousExpirable(Expirable previous) {
            this.previousExpirable = previous;
        }
    }

    private static class WeakEvictableEntry<K, V> extends WeakEntry<K, V> implements Evictable {
        @GuardedBy("Segment.this")
        Evictable nextEvictable = NullEvictable.INSTANCE;
        @GuardedBy("Segment.this")
        Evictable previousEvictable = NullEvictable.INSTANCE;

        WeakEvictableEntry(CustomConcurrentHashMap<K, V> map, K key, int hash, @Nullable ReferenceEntry<K, V> next) {
            super(map, key, hash, next);
        }

        public Evictable getNextEvictable() {
            return this.nextEvictable;
        }

        public void setNextEvictable(Evictable next) {
            this.nextEvictable = next;
        }

        public Evictable getPreviousEvictable() {
            return this.previousEvictable;
        }

        public void setPreviousEvictable(Evictable previous) {
            this.previousEvictable = previous;
        }
    }

    private static class WeakExpirableEvictableEntry<K, V> extends WeakEntry<K, V> implements Expirable, Evictable {
        @GuardedBy("Segment.this")
        Evictable nextEvictable = NullEvictable.INSTANCE;
        @GuardedBy("Segment.this")
        Expirable nextExpirable = NullExpirable.INSTANCE;
        @GuardedBy("Segment.this")
        Evictable previousEvictable = NullEvictable.INSTANCE;
        @GuardedBy("Segment.this")
        Expirable previousExpirable = NullExpirable.INSTANCE;
        volatile long time = Long.MAX_VALUE;

        WeakExpirableEvictableEntry(CustomConcurrentHashMap<K, V> map, K key, int hash, @Nullable ReferenceEntry<K, V> next) {
            super(map, key, hash, next);
        }

        public long getExpirationTime() {
            return this.time;
        }

        public void setExpirationTime(long time2) {
            this.time = time2;
        }

        public Expirable getNextExpirable() {
            return this.nextExpirable;
        }

        public void setNextExpirable(Expirable next) {
            this.nextExpirable = next;
        }

        public Expirable getPreviousExpirable() {
            return this.previousExpirable;
        }

        public void setPreviousExpirable(Expirable previous) {
            this.previousExpirable = previous;
        }

        public Evictable getNextEvictable() {
            return this.nextEvictable;
        }

        public void setNextEvictable(Evictable next) {
            this.nextEvictable = next;
        }

        public Evictable getPreviousEvictable() {
            return this.previousEvictable;
        }

        public void setPreviousEvictable(Evictable previous) {
            this.previousEvictable = previous;
        }
    }

    private static class WeakValueReference<K, V> extends FinalizableWeakReference<V> implements ValueReference<K, V> {
        final ReferenceEntry<K, V> entry;

        WeakValueReference(V referent, ReferenceEntry<K, V> entry2) {
            super(referent, QueueHolder.queue);
            this.entry = entry2;
        }

        public void finalizeReferent() {
            this.entry.valueReclaimed();
        }

        public ValueReference<K, V> copyFor(ReferenceEntry<K, V> entry2) {
            return new WeakValueReference(get(), entry2);
        }

        public V waitForValue() {
            return get();
        }
    }

    private static class SoftValueReference<K, V> extends FinalizableSoftReference<V> implements ValueReference<K, V> {
        final ReferenceEntry<K, V> entry;

        SoftValueReference(V referent, ReferenceEntry<K, V> entry2) {
            super(referent, QueueHolder.queue);
            this.entry = entry2;
        }

        public void finalizeReferent() {
            this.entry.valueReclaimed();
        }

        public ValueReference<K, V> copyFor(ReferenceEntry<K, V> entry2) {
            return new SoftValueReference(get(), entry2);
        }

        public V waitForValue() {
            return get();
        }
    }

    private static class StrongValueReference<K, V> implements ValueReference<K, V> {
        final V referent;

        StrongValueReference(V referent2) {
            this.referent = referent2;
        }

        public V get() {
            return this.referent;
        }

        public ValueReference<K, V> copyFor(ReferenceEntry<K, V> referenceEntry) {
            return this;
        }

        public V waitForValue() {
            return get();
        }

        public void clear() {
        }
    }

    private static int rehash(int h) {
        int h2 = h + ((h << 15) ^ -12931);
        int h3 = h2 ^ (h2 >>> 10);
        int h4 = h3 + (h3 << 3);
        int h5 = h4 ^ (h4 >>> 6);
        int h6 = h5 + (h5 << 2) + (h5 << 14);
        return (h6 >>> 16) ^ h6;
    }

    /* access modifiers changed from: package-private */
    public void setValueReference(ReferenceEntry<K, V> entry, ValueReference<K, V> valueReference) {
        entry.setValueReference(valueReference);
    }

    /* access modifiers changed from: package-private */
    @GuardedBy("Segment.this")
    public ReferenceEntry<K, V> copyEntry(ReferenceEntry<K, V> original, ReferenceEntry<K, V> newNext) {
        ValueReference<K, V> valueReference = original.getValueReference();
        ReferenceEntry<K, V> newEntry = this.entryFactory.copyEntry(this, original, newNext);
        newEntry.setValueReference(valueReference.copyFor(newEntry));
        return newEntry;
    }

    /* access modifiers changed from: package-private */
    public int hash(Object key) {
        return rehash(this.keyEquivalence.hash(Preconditions.checkNotNull(key)));
    }

    /* access modifiers changed from: package-private */
    public void reclaimValue(ReferenceEntry<K, V> entry) {
        int hash = entry.getHash();
        if (segmentFor(hash).reclaimValue(entry, hash)) {
            this.pendingEvictionNotifications.offer(entry);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean removeEntry(ReferenceEntry<K, V> entry) {
        int hash = entry.getHash();
        return segmentFor(hash).removeEntry(entry, hash);
    }

    @GuardedBy("Segment.this")
    static void connectExpirables(Expirable previous, Expirable next) {
        previous.setNextExpirable(next);
        next.setPreviousExpirable(previous);
    }

    @GuardedBy("Segment.this")
    static void nullifyExpirable(Expirable nulled) {
        nulled.setNextExpirable(NullExpirable.INSTANCE);
        nulled.setPreviousExpirable(NullExpirable.INSTANCE);
    }

    /* access modifiers changed from: package-private */
    public boolean isExpired(ReferenceEntry<K, V> entry) {
        return isExpired((Expirable) entry, System.nanoTime());
    }

    /* access modifiers changed from: package-private */
    public boolean isExpired(Expirable expirable, long now) {
        return now - expirable.getExpirationTime() > 0;
    }

    /* access modifiers changed from: package-private */
    public V getUnexpiredValue(ReferenceEntry<K, V> e) {
        V value = e.getValueReference().get();
        if (!expires() || !isExpired(e)) {
            return value;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void processPendingNotifications() {
        while (true) {
            ReferenceEntry<K, V> entry = this.pendingEvictionNotifications.poll();
            if (entry != null) {
                this.evictionListener.onEviction(entry.getKey(), entry.getValueReference().get());
            } else {
                return;
            }
        }
    }

    @GuardedBy("Segment.this")
    static void connectEvictables(Evictable previous, Evictable next) {
        previous.setNextEvictable(next);
        next.setPreviousEvictable(previous);
    }

    @GuardedBy("Segment.this")
    static void nullifyEvictable(Evictable nulled) {
        nulled.setNextEvictable(NullEvictable.INSTANCE);
        nulled.setPreviousEvictable(NullEvictable.INSTANCE);
    }

    /* access modifiers changed from: package-private */
    public final CustomConcurrentHashMap<K, V>.Segment[] newSegmentArray(int ssize) {
        return (Segment[]) Array.newInstance(Segment.class, ssize);
    }

    /* access modifiers changed from: package-private */
    public CustomConcurrentHashMap<K, V>.Segment segmentFor(int hash) {
        return this.segments[(hash >>> this.segmentShift) & this.segmentMask];
    }

    /* access modifiers changed from: package-private */
    public CustomConcurrentHashMap<K, V>.Segment createSegment(int initialCapacity, int maxSegmentSize) {
        return new Segment(initialCapacity, maxSegmentSize);
    }

    class Segment extends ReentrantLock {
        volatile int count;
        @GuardedBy("Segment.this")
        final Evictable evictionHead = new Evictable() {
            @GuardedBy("Segment.this")
            Evictable nextEvictable = this;
            @GuardedBy("Segment.this")
            Evictable previousEvictable = this;

            public Evictable getNextEvictable() {
                return this.nextEvictable;
            }

            public void setNextEvictable(Evictable next) {
                this.nextEvictable = next;
            }

            public Evictable getPreviousEvictable() {
                return this.previousEvictable;
            }

            public void setPreviousEvictable(Evictable previous) {
                this.previousEvictable = previous;
            }
        };
        final Expirable expirationHead = new Expirable() {
            @GuardedBy("Segment.this")
            Expirable nextExpirable = this;
            @GuardedBy("Segment.this")
            Expirable previousExpirable = this;

            public long getExpirationTime() {
                return Long.MAX_VALUE;
            }

            public void setExpirationTime(long time) {
            }

            public Expirable getNextExpirable() {
                return this.nextExpirable;
            }

            public void setNextExpirable(Expirable next) {
                this.nextExpirable = next;
            }

            public Expirable getPreviousExpirable() {
                return this.previousExpirable;
            }

            public void setPreviousExpirable(Expirable previous) {
                this.previousExpirable = previous;
            }
        };
        final int maxSegmentSize;
        int modCount;
        final Queue<ReferenceEntry<K, V>> recencyQueue;
        final AtomicInteger recencyQueueLength;
        volatile AtomicReferenceArray<ReferenceEntry<K, V>> table;
        int threshold;

        Segment(int initialCapacity, int maxSegmentSize2) {
            initTable(newEntryArray(initialCapacity));
            this.maxSegmentSize = maxSegmentSize2;
            if (CustomConcurrentHashMap.this.evictsBySize() || CustomConcurrentHashMap.this.expires()) {
                this.recencyQueue = new ConcurrentLinkedQueue();
                this.recencyQueueLength = new AtomicInteger();
                return;
            }
            this.recencyQueue = null;
            this.recencyQueueLength = null;
        }

        /* access modifiers changed from: package-private */
        public AtomicReferenceArray<ReferenceEntry<K, V>> newEntryArray(int size) {
            return new AtomicReferenceArray<>(size);
        }

        /* access modifiers changed from: package-private */
        public void initTable(AtomicReferenceArray<ReferenceEntry<K, V>> newTable) {
            this.threshold = (newTable.length() * 3) / 4;
            if (this.threshold == this.maxSegmentSize) {
                this.threshold++;
            }
            this.table = newTable;
        }

        /* access modifiers changed from: package-private */
        @GuardedBy("Segment.this")
        public void setValue(ReferenceEntry<K, V> entry, V value) {
            if (CustomConcurrentHashMap.this.evictsBySize() || CustomConcurrentHashMap.this.expires()) {
                Preconditions.checkState(isLocked());
                recordWrite(entry);
            }
            CustomConcurrentHashMap.this.setValueReference(entry, CustomConcurrentHashMap.this.valueStrength.referenceValue(entry, value));
        }

        /* access modifiers changed from: package-private */
        public void recordRead(ReferenceEntry<K, V> entry) {
            if (CustomConcurrentHashMap.this.expiresAfterAccess()) {
                recordExpirationTime((Expirable) entry, CustomConcurrentHashMap.this.expireAfterAccessNanos);
            }
            this.recencyQueue.add(entry);
            if (this.recencyQueueLength.incrementAndGet() > 64 && tryLock()) {
                try {
                    drainRecencyQueue();
                } finally {
                    unlock();
                }
            }
        }

        /* access modifiers changed from: package-private */
        @GuardedBy("Segment.this")
        public void recordWrite(ReferenceEntry<K, V> entry) {
            Preconditions.checkState(isLocked());
            drainRecencyQueue();
            if (entry instanceof Evictable) {
                addEvictable((Evictable) entry);
            }
            if (entry instanceof Expirable) {
                Expirable expirable = (Expirable) entry;
                recordExpirationTime(expirable, CustomConcurrentHashMap.this.expiresAfterAccess() ? CustomConcurrentHashMap.this.expireAfterAccessNanos : CustomConcurrentHashMap.this.expireAfterWriteNanos);
                addExpirable(expirable);
            }
        }

        /* access modifiers changed from: package-private */
        @GuardedBy("Segment.this")
        public void drainRecencyQueue() {
            Preconditions.checkState(isLocked());
            int drained = 0;
            while (true) {
                ReferenceEntry<K, V> entry = this.recencyQueue.poll();
                if (entry != null) {
                    if (entry instanceof Evictable) {
                        Evictable evictable = (Evictable) entry;
                        if (inEvictionList(evictable)) {
                            addEvictable(evictable);
                        }
                    }
                    if (entry instanceof Expirable) {
                        Expirable expirable = (Expirable) entry;
                        if (inExpirationList(expirable)) {
                            addExpirable(expirable);
                        }
                    }
                    drained++;
                } else {
                    this.recencyQueueLength.addAndGet(-drained);
                    return;
                }
            }
        }

        /* access modifiers changed from: package-private */
        @GuardedBy("Segment.this")
        public void addExpirable(Expirable expirable) {
            Preconditions.checkState(isLocked());
            CustomConcurrentHashMap.connectExpirables(expirable.getPreviousExpirable(), expirable.getNextExpirable());
            CustomConcurrentHashMap.connectExpirables(this.expirationHead.getPreviousExpirable(), expirable);
            CustomConcurrentHashMap.connectExpirables(expirable, this.expirationHead);
        }

        /* access modifiers changed from: package-private */
        public void recordExpirationTime(Expirable expirable, long expirationNanos) {
            expirable.setExpirationTime(System.nanoTime() + expirationNanos);
        }

        /* access modifiers changed from: package-private */
        @GuardedBy("Segment.this")
        public void removeExpirable(Expirable expirable) {
            Preconditions.checkState(isLocked());
            CustomConcurrentHashMap.connectExpirables(expirable.getPreviousExpirable(), expirable.getNextExpirable());
            CustomConcurrentHashMap.nullifyExpirable(expirable);
        }

        /* access modifiers changed from: package-private */
        @GuardedBy("Segment.this")
        public boolean inExpirationList(Expirable expirable) {
            return expirable.getNextExpirable() != NullExpirable.INSTANCE;
        }

        /* access modifiers changed from: package-private */
        @GuardedBy("Segment.this")
        public void expireEntries() {
            Preconditions.checkState(isLocked());
            drainRecencyQueue();
            Expirable expirable = this.expirationHead.getNextExpirable();
            if (expirable != this.expirationHead) {
                long now = System.nanoTime();
                while (expirable != this.expirationHead && CustomConcurrentHashMap.this.isExpired(expirable, now)) {
                    ReferenceEntry referenceEntry = (ReferenceEntry) expirable;
                    if (removeEntry(referenceEntry, referenceEntry.getHash())) {
                        CustomConcurrentHashMap.this.pendingEvictionNotifications.offer(referenceEntry);
                    }
                    removeExpirable(expirable);
                    expirable = this.expirationHead.getNextExpirable();
                }
            }
        }

        /* access modifiers changed from: package-private */
        @GuardedBy("Segment.this")
        public void clearExpirationQueue() {
            Preconditions.checkState(isLocked());
            Expirable expirable = this.expirationHead.getNextExpirable();
            while (expirable != this.expirationHead) {
                Expirable next = expirable.getNextExpirable();
                CustomConcurrentHashMap.nullifyExpirable(expirable);
                expirable = next;
            }
            this.expirationHead.setNextExpirable(this.expirationHead);
            this.expirationHead.setPreviousExpirable(this.expirationHead);
        }

        /* access modifiers changed from: package-private */
        @GuardedBy("Segment.this")
        public void evictEntry() {
            Preconditions.checkState(isLocked());
            drainRecencyQueue();
            Evictable evictable = this.evictionHead.getNextEvictable();
            Preconditions.checkState(evictable != this.evictionHead);
            ReferenceEntry referenceEntry = (ReferenceEntry) evictable;
            if (removeEntry(referenceEntry, referenceEntry.getHash())) {
                CustomConcurrentHashMap.this.pendingEvictionNotifications.offer(referenceEntry);
                return;
            }
            throw new AssertionError();
        }

        /* access modifiers changed from: package-private */
        @GuardedBy("Segment.this")
        public void addEvictable(Evictable evictable) {
            Preconditions.checkState(isLocked());
            if (evictable.getNextEvictable() != this.evictionHead) {
                CustomConcurrentHashMap.connectEvictables(evictable.getPreviousEvictable(), evictable.getNextEvictable());
                CustomConcurrentHashMap.connectEvictables(this.evictionHead.getPreviousEvictable(), evictable);
                CustomConcurrentHashMap.connectEvictables(evictable, this.evictionHead);
            }
        }

        /* access modifiers changed from: package-private */
        @GuardedBy("Segment.this")
        public void removeEvictable(Evictable evictable) {
            Preconditions.checkState(isLocked());
            CustomConcurrentHashMap.connectEvictables(evictable.getPreviousEvictable(), evictable.getNextEvictable());
            CustomConcurrentHashMap.nullifyEvictable(evictable);
        }

        /* access modifiers changed from: package-private */
        @GuardedBy("Segment.this")
        public boolean inEvictionList(Evictable evictable) {
            return evictable.getNextEvictable() != NullEvictable.INSTANCE;
        }

        /* access modifiers changed from: package-private */
        @GuardedBy("Segment.this")
        public void clearEvictionQueue() {
            Preconditions.checkState(isLocked());
            Evictable evictable = this.evictionHead.getNextEvictable();
            while (evictable != this.evictionHead) {
                Evictable next = evictable.getNextEvictable();
                CustomConcurrentHashMap.nullifyEvictable(evictable);
                evictable = next;
            }
            this.evictionHead.setNextEvictable(this.evictionHead);
            this.evictionHead.setPreviousEvictable(this.evictionHead);
        }

        /* access modifiers changed from: package-private */
        public ReferenceEntry<K, V> getFirst(int hash) {
            AtomicReferenceArray<ReferenceEntry<K, V>> table2 = this.table;
            return table2.get((table2.length() - 1) & hash);
        }

        public ReferenceEntry<K, V> getEntry(Object key, int hash) {
            K entryKey;
            if (this.count != 0) {
                for (ReferenceEntry<K, V> e = getFirst(hash); e != null; e = e.getNext()) {
                    if (e.getHash() == hash && (entryKey = e.getKey()) != null && CustomConcurrentHashMap.this.keyEquivalence.equivalent(key, entryKey) && (!CustomConcurrentHashMap.this.expires() || !CustomConcurrentHashMap.this.isExpired(e))) {
                        if (CustomConcurrentHashMap.this.evictsBySize() || CustomConcurrentHashMap.this.expiresAfterAccess()) {
                            recordRead(e);
                        }
                        return e;
                    }
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        public V get(Object key, int hash) {
            ReferenceEntry<K, V> entry = getEntry(key, hash);
            if (entry == null) {
                return null;
            }
            return entry.getValueReference().get();
        }

        /* access modifiers changed from: package-private */
        public boolean containsKey(Object key, int hash) {
            K entryKey;
            if (this.count != 0) {
                ReferenceEntry<K, V> e = getFirst(hash);
                while (e != null) {
                    if (e.getHash() != hash || (entryKey = e.getKey()) == null || !CustomConcurrentHashMap.this.keyEquivalence.equivalent(key, entryKey)) {
                        e = e.getNext();
                    } else if (CustomConcurrentHashMap.this.getUnexpiredValue(e) != null) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        public boolean containsValue(Object value) {
            if (this.count != 0) {
                AtomicReferenceArray<ReferenceEntry<K, V>> table2 = this.table;
                int length = table2.length();
                for (int i = 0; i < length; i++) {
                    for (ReferenceEntry<K, V> e = table2.get(i); e != null; e = e.getNext()) {
                        V entryValue = CustomConcurrentHashMap.this.getUnexpiredValue(e);
                        if (entryValue != null && CustomConcurrentHashMap.this.valueEquivalence.equivalent(value, entryValue)) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        public boolean replace(K key, int hash, V oldValue, V newValue) {
            Preconditions.checkNotNull(oldValue);
            Preconditions.checkNotNull(newValue);
            lock();
            try {
                if (CustomConcurrentHashMap.this.expires()) {
                    expireEntries();
                }
                for (ReferenceEntry<K, V> e = getFirst(hash); e != null; e = e.getNext()) {
                    K entryKey = e.getKey();
                    if (e.getHash() == hash && entryKey != null && CustomConcurrentHashMap.this.keyEquivalence.equivalent(key, entryKey)) {
                        V entryValue = e.getValueReference().get();
                        if (entryValue == null) {
                            unlock();
                            CustomConcurrentHashMap.this.processPendingNotifications();
                            return false;
                        } else if (CustomConcurrentHashMap.this.valueEquivalence.equivalent(oldValue, entryValue)) {
                            setValue(e, newValue);
                            return true;
                        } else if (CustomConcurrentHashMap.this.evictsBySize() || CustomConcurrentHashMap.this.expires()) {
                            recordWrite(e);
                        }
                    }
                }
                unlock();
                CustomConcurrentHashMap.this.processPendingNotifications();
                return false;
            } finally {
                unlock();
                CustomConcurrentHashMap.this.processPendingNotifications();
            }
        }

        /* JADX INFO: finally extract failed */
        /* access modifiers changed from: package-private */
        public V replace(K key, int hash, V newValue) {
            Preconditions.checkNotNull(newValue);
            lock();
            try {
                if (CustomConcurrentHashMap.this.expires()) {
                    expireEntries();
                }
                ReferenceEntry<K, V> e = getFirst(hash);
                while (e != null) {
                    K entryKey = e.getKey();
                    if (e.getHash() != hash || entryKey == null || !CustomConcurrentHashMap.this.keyEquivalence.equivalent(key, entryKey)) {
                        e = e.getNext();
                    } else {
                        V entryValue = e.getValueReference().get();
                        if (entryValue == null) {
                            unlock();
                            CustomConcurrentHashMap.this.processPendingNotifications();
                            return null;
                        }
                        setValue(e, newValue);
                        unlock();
                        CustomConcurrentHashMap.this.processPendingNotifications();
                        return entryValue;
                    }
                }
                unlock();
                CustomConcurrentHashMap.this.processPendingNotifications();
                return null;
            } catch (Throwable th) {
                unlock();
                CustomConcurrentHashMap.this.processPendingNotifications();
                throw th;
            }
        }

        /* JADX INFO: finally extract failed */
        /* access modifiers changed from: package-private */
        public V put(K key, int hash, V value, boolean onlyIfAbsent) {
            Preconditions.checkNotNull(value);
            lock();
            try {
                if (CustomConcurrentHashMap.this.expires()) {
                    expireEntries();
                }
                int newCount = this.count + 1;
                if (newCount > this.threshold) {
                    expand();
                }
                AtomicReferenceArray<ReferenceEntry<K, V>> table2 = this.table;
                int index = hash & (table2.length() - 1);
                ReferenceEntry<K, V> first = table2.get(index);
                ReferenceEntry<K, V> e = first;
                while (e != null) {
                    K entryKey = e.getKey();
                    if (e.getHash() != hash || entryKey == null || !CustomConcurrentHashMap.this.keyEquivalence.equivalent(key, entryKey)) {
                        e = e.getNext();
                    } else {
                        V entryValue = e.getValueReference().get();
                        boolean absent = entryValue == null;
                        if (!onlyIfAbsent || absent) {
                            if (absent) {
                                CustomConcurrentHashMap.this.pendingEvictionNotifications.offer(CustomConcurrentHashMap.this.entryFactory.newEntry(CustomConcurrentHashMap.this, key, hash, null));
                            }
                            setValue(e, value);
                            unlock();
                            CustomConcurrentHashMap.this.processPendingNotifications();
                            return entryValue;
                        }
                        if (CustomConcurrentHashMap.this.evictsBySize() || CustomConcurrentHashMap.this.expires()) {
                            recordWrite(e);
                        }
                        unlock();
                        CustomConcurrentHashMap.this.processPendingNotifications();
                        return entryValue;
                    }
                }
                if (CustomConcurrentHashMap.this.evictsBySize() && newCount > this.maxSegmentSize) {
                    evictEntry();
                    newCount = this.count + 1;
                    first = table2.get(index);
                }
                this.modCount = this.modCount + 1;
                ReferenceEntry<K, V> newEntry = CustomConcurrentHashMap.this.entryFactory.newEntry(CustomConcurrentHashMap.this, key, hash, first);
                setValue(newEntry, value);
                table2.set(index, newEntry);
                this.count = newCount;
                unlock();
                CustomConcurrentHashMap.this.processPendingNotifications();
                return null;
            } catch (Throwable th) {
                unlock();
                CustomConcurrentHashMap.this.processPendingNotifications();
                throw th;
            }
        }

        /* access modifiers changed from: package-private */
        @GuardedBy("Segment.this")
        public void expand() {
            Preconditions.checkState(isLocked());
            AtomicReferenceArray<ReferenceEntry<K, V>> oldTable = this.table;
            int oldCapacity = oldTable.length();
            if (oldCapacity < CustomConcurrentHashMap.MAXIMUM_CAPACITY) {
                AtomicReferenceArray<ReferenceEntry<K, V>> newTable = newEntryArray(oldCapacity << 1);
                this.threshold = (newTable.length() * 3) / 4;
                int newMask = newTable.length() - 1;
                for (int oldIndex = 0; oldIndex < oldCapacity; oldIndex++) {
                    ReferenceEntry<K, V> head = oldTable.get(oldIndex);
                    if (head != null) {
                        ReferenceEntry<K, V> next = head.getNext();
                        int headIndex = head.getHash() & newMask;
                        if (next == null) {
                            newTable.set(headIndex, head);
                        } else {
                            ReferenceEntry<K, V> tail = head;
                            int tailIndex = headIndex;
                            for (ReferenceEntry<K, V> e = next; e != null; e = e.getNext()) {
                                int newIndex = e.getHash() & newMask;
                                if (newIndex != tailIndex) {
                                    tailIndex = newIndex;
                                    tail = e;
                                }
                            }
                            newTable.set(tailIndex, tail);
                            for (ReferenceEntry<K, V> e2 = head; e2 != tail; e2 = e2.getNext()) {
                                if (e2.getKey() != null) {
                                    int newIndex2 = e2.getHash() & newMask;
                                    newTable.set(newIndex2, CustomConcurrentHashMap.this.copyEntry(e2, newTable.get(newIndex2)));
                                }
                            }
                        }
                    }
                }
                this.table = newTable;
            }
        }

        /* access modifiers changed from: package-private */
        public V remove(Object key, int hash, boolean expire) {
            lock();
            if (expire) {
                try {
                    expireEntries();
                } catch (Throwable th) {
                    unlock();
                    CustomConcurrentHashMap.this.processPendingNotifications();
                    throw th;
                }
            }
            int newCount = this.count - 1;
            AtomicReferenceArray<ReferenceEntry<K, V>> table2 = this.table;
            int index = hash & (table2.length() - 1);
            ReferenceEntry<K, V> first = table2.get(index);
            ReferenceEntry<K, V> e = first;
            while (e != null) {
                K entryKey = e.getKey();
                if (e.getHash() != hash || entryKey == null || !CustomConcurrentHashMap.this.keyEquivalence.equivalent(key, entryKey)) {
                    e = e.getNext();
                } else {
                    V entryValue = e.getValueReference().get();
                    this.modCount++;
                    table2.set(index, removeFromTable(first, e));
                    this.count = newCount;
                    unlock();
                    CustomConcurrentHashMap.this.processPendingNotifications();
                    return entryValue;
                }
            }
            unlock();
            CustomConcurrentHashMap.this.processPendingNotifications();
            return null;
        }

        /* JADX INFO: finally extract failed */
        /* access modifiers changed from: package-private */
        public boolean remove(Object key, int hash, Object value) {
            Preconditions.checkNotNull(value);
            lock();
            try {
                if (CustomConcurrentHashMap.this.expires()) {
                    expireEntries();
                }
                int newCount = this.count - 1;
                AtomicReferenceArray<ReferenceEntry<K, V>> table2 = this.table;
                int index = hash & (table2.length() - 1);
                ReferenceEntry<K, V> first = table2.get(index);
                ReferenceEntry<K, V> e = first;
                while (e != null) {
                    K entryKey = e.getKey();
                    if (e.getHash() != hash || entryKey == null || !CustomConcurrentHashMap.this.keyEquivalence.equivalent(key, entryKey)) {
                        e = e.getNext();
                    } else {
                        V entryValue = e.getValueReference().get();
                        if (value == entryValue || !(value == null || entryValue == null || !CustomConcurrentHashMap.this.valueEquivalence.equivalent(value, entryValue))) {
                            this.modCount++;
                            table2.set(index, removeFromTable(first, e));
                            this.count = newCount;
                            unlock();
                            CustomConcurrentHashMap.this.processPendingNotifications();
                            return true;
                        }
                        unlock();
                        CustomConcurrentHashMap.this.processPendingNotifications();
                        return false;
                    }
                }
                unlock();
                CustomConcurrentHashMap.this.processPendingNotifications();
                return false;
            } catch (Throwable th) {
                unlock();
                CustomConcurrentHashMap.this.processPendingNotifications();
                throw th;
            }
        }

        /* JADX INFO: finally extract failed */
        /* access modifiers changed from: package-private */
        public boolean reclaimValue(ReferenceEntry<K, V> entry, int hash) {
            lock();
            try {
                int newCount = this.count - 1;
                AtomicReferenceArray<ReferenceEntry<K, V>> table2 = this.table;
                int index = hash & (table2.length() - 1);
                ReferenceEntry<K, V> first = table2.get(index);
                ReferenceEntry<K, V> e = first;
                while (e != null) {
                    if (e != entry) {
                        e = e.getNext();
                    } else if (e.getValueReference().get() == null) {
                        this.modCount++;
                        table2.set(index, removeFromTable(first, e));
                        this.count = newCount;
                        unlock();
                        return true;
                    } else {
                        unlock();
                        return false;
                    }
                }
                unlock();
                return false;
            } catch (Throwable th) {
                unlock();
                throw th;
            }
        }

        /* JADX INFO: finally extract failed */
        /* access modifiers changed from: package-private */
        public boolean removeEntry(ReferenceEntry<K, V> entry, int hash) {
            lock();
            try {
                int newCount = this.count - 1;
                AtomicReferenceArray<ReferenceEntry<K, V>> table2 = this.table;
                int index = hash & (table2.length() - 1);
                ReferenceEntry<K, V> first = table2.get(index);
                for (ReferenceEntry<K, V> e = first; e != null; e = e.getNext()) {
                    if (e == entry) {
                        this.modCount++;
                        table2.set(index, removeFromTable(first, e));
                        this.count = newCount;
                        unlock();
                        return true;
                    }
                }
                unlock();
                return false;
            } catch (Throwable th) {
                unlock();
                throw th;
            }
        }

        @GuardedBy("Segment.this")
        private ReferenceEntry<K, V> removeFromTable(ReferenceEntry<K, V> first, ReferenceEntry<K, V> removed) {
            Preconditions.checkState(isLocked());
            if (CustomConcurrentHashMap.this.expires()) {
                removeExpirable((Expirable) removed);
            }
            if (CustomConcurrentHashMap.this.evictsBySize()) {
                removeEvictable((Evictable) removed);
            }
            ReferenceEntry<K, V> newFirst = removed.getNext();
            for (ReferenceEntry<K, V> p = first; p != removed; p = p.getNext()) {
                if (p.getKey() != null) {
                    newFirst = CustomConcurrentHashMap.this.copyEntry(p, newFirst);
                }
            }
            return newFirst;
        }

        /* access modifiers changed from: package-private */
        public void clear() {
            if (this.count != 0) {
                lock();
                try {
                    AtomicReferenceArray<ReferenceEntry<K, V>> table2 = this.table;
                    for (int i = 0; i < table2.length(); i++) {
                        table2.set(i, null);
                    }
                    clearExpirationQueue();
                    clearEvictionQueue();
                    this.modCount++;
                    this.count = 0;
                } finally {
                    unlock();
                }
            }
        }
    }

    public boolean isEmpty() {
        CustomConcurrentHashMap<K, V>.Segment[] segments2 = this.segments;
        int[] mc = new int[segments2.length];
        int mcsum = 0;
        for (int i = 0; i < segments2.length; i++) {
            if (segments2[i].count != 0) {
                return false;
            }
            int i2 = segments2[i].modCount;
            mc[i] = i2;
            mcsum += i2;
        }
        if (mcsum != 0) {
            for (int i3 = 0; i3 < segments2.length; i3++) {
                if (segments2[i3].count != 0 || mc[i3] != segments2[i3].modCount) {
                    return false;
                }
            }
        }
        return true;
    }

    public int size() {
        CustomConcurrentHashMap<K, V>.Segment[] segments2 = this.segments;
        long sum = 0;
        long check = 0;
        int[] mc = new int[segments2.length];
        for (int k = 0; k < 2; k++) {
            check = 0;
            sum = 0;
            int mcsum = 0;
            for (int i = 0; i < segments2.length; i++) {
                sum += (long) segments2[i].count;
                int i2 = segments2[i].modCount;
                mc[i] = i2;
                mcsum += i2;
            }
            if (mcsum != 0) {
                int i3 = 0;
                while (true) {
                    if (i3 >= segments2.length) {
                        break;
                    }
                    check += (long) segments2[i3].count;
                    if (mc[i3] != segments2[i3].modCount) {
                        check = -1;
                        break;
                    }
                    i3++;
                }
            }
            if (check == sum) {
                break;
            }
        }
        if (check != sum) {
            long sum2 = 0;
            for (CustomConcurrentHashMap<K, V>.Segment segment : segments2) {
                segment.lock();
            }
            for (CustomConcurrentHashMap<K, V>.Segment segment2 : segments2) {
                sum2 = sum + ((long) segment2.count);
            }
            for (CustomConcurrentHashMap<K, V>.Segment segment3 : segments2) {
                segment3.unlock();
            }
        }
        return Ints.saturatedCast(sum);
    }

    public V get(Object key) {
        int hash = hash(key);
        return segmentFor(hash).get(key, hash);
    }

    public boolean containsKey(Object key) {
        int hash = hash(key);
        return segmentFor(hash).containsKey(key, hash);
    }

    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    public boolean containsValue(java.lang.Object r14) {
        /*
            r13 = this;
            java.lang.String r11 = "value"
            com.google.common.base.Preconditions.checkNotNull(r14, r11)
            com.google.common.collect.CustomConcurrentHashMap<K, V>$Segment[] r10 = r13.segments
            int r11 = r10.length
            int[] r7 = new int[r11]
            r5 = 0
        L_0x000b:
            r11 = 2
            if (r5 >= r11) goto L_0x0049
            r8 = 0
            r3 = 0
        L_0x0010:
            int r11 = r10.length
            if (r3 >= r11) goto L_0x002b
            r11 = r10[r3]
            int r1 = r11.count
            r11 = r10[r3]
            int r11 = r11.modCount
            r7[r3] = r11
            int r8 = r8 + r11
            r11 = r10[r3]
            boolean r11 = r11.containsValue(r14)
            if (r11 == 0) goto L_0x0028
            r11 = 1
        L_0x0027:
            return r11
        L_0x0028:
            int r3 = r3 + 1
            goto L_0x0010
        L_0x002b:
            r2 = 1
            if (r8 == 0) goto L_0x003f
            r3 = 0
        L_0x002f:
            int r11 = r10.length
            if (r3 >= r11) goto L_0x003f
            r11 = r10[r3]
            int r1 = r11.count
            r11 = r7[r3]
            r12 = r10[r3]
            int r12 = r12.modCount
            if (r11 == r12) goto L_0x0043
            r2 = 0
        L_0x003f:
            if (r2 == 0) goto L_0x0046
            r11 = 0
            goto L_0x0027
        L_0x0043:
            int r3 = r3 + 1
            goto L_0x002f
        L_0x0046:
            int r5 = r5 + 1
            goto L_0x000b
        L_0x0049:
            r0 = r10
            int r6 = r0.length
            r4 = 0
        L_0x004c:
            if (r4 >= r6) goto L_0x0056
            r9 = r0[r4]
            r9.lock()
            int r4 = r4 + 1
            goto L_0x004c
        L_0x0056:
            r0 = r10
            int r6 = r0.length     // Catch:{ all -> 0x0081 }
            r4 = 0
        L_0x0059:
            if (r4 >= r6) goto L_0x0074
            r9 = r0[r4]     // Catch:{ all -> 0x0081 }
            boolean r11 = r9.containsValue(r14)     // Catch:{ all -> 0x0081 }
            if (r11 == 0) goto L_0x0071
            r11 = 1
            r0 = r10
            int r6 = r0.length
            r4 = 0
        L_0x0067:
            if (r4 >= r6) goto L_0x0027
            r9 = r0[r4]
            r9.unlock()
            int r4 = r4 + 1
            goto L_0x0067
        L_0x0071:
            int r4 = r4 + 1
            goto L_0x0059
        L_0x0074:
            r0 = r10
            int r6 = r0.length
            r4 = 0
        L_0x0077:
            if (r4 >= r6) goto L_0x0090
            r9 = r0[r4]
            r9.unlock()
            int r4 = r4 + 1
            goto L_0x0077
        L_0x0081:
            r11 = move-exception
            r0 = r10
            int r6 = r0.length
            r4 = 0
        L_0x0085:
            if (r4 >= r6) goto L_0x008f
            r9 = r0[r4]
            r9.unlock()
            int r4 = r4 + 1
            goto L_0x0085
        L_0x008f:
            throw r11
        L_0x0090:
            r11 = 0
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.CustomConcurrentHashMap.containsValue(java.lang.Object):boolean");
    }

    public V put(K key, V value) {
        int hash = hash(key);
        return segmentFor(hash).put(key, hash, value, false);
    }

    public V putIfAbsent(K key, V value) {
        int hash = hash(key);
        return segmentFor(hash).put(key, hash, value, true);
    }

    public void putAll(Map<? extends K, ? extends V> m) {
        for (Map.Entry<? extends K, ? extends V> e : m.entrySet()) {
            put(e.getKey(), e.getValue());
        }
    }

    public V remove(Object key) {
        int hash = hash(key);
        return segmentFor(hash).remove(key, hash, expires());
    }

    public boolean remove(Object key, Object value) {
        int hash = hash(key);
        return segmentFor(hash).remove(key, hash, value);
    }

    public boolean replace(K key, V oldValue, V newValue) {
        int hash = hash(key);
        return segmentFor(hash).replace(key, hash, oldValue, newValue);
    }

    public V replace(K key, V value) {
        int hash = hash(key);
        return segmentFor(hash).replace(key, hash, value);
    }

    public void clear() {
        for (CustomConcurrentHashMap<K, V>.Segment segment : this.segments) {
            segment.clear();
        }
    }

    public Set<K> keySet() {
        Set<K> ks = this.keySet;
        if (ks != null) {
            return ks;
        }
        KeySet keySet2 = new KeySet();
        this.keySet = keySet2;
        return keySet2;
    }

    public Collection<V> values() {
        Collection<V> vs = this.values;
        if (vs != null) {
            return vs;
        }
        Values values2 = new Values();
        this.values = values2;
        return values2;
    }

    public Set<Map.Entry<K, V>> entrySet() {
        Set<Map.Entry<K, V>> es = this.entrySet;
        if (es != null) {
            return es;
        }
        EntrySet entrySet2 = new EntrySet();
        this.entrySet = entrySet2;
        return entrySet2;
    }

    abstract class HashIterator {
        AtomicReferenceArray<ReferenceEntry<K, V>> currentTable;
        CustomConcurrentHashMap<K, V>.WriteThroughEntry lastReturned;
        ReferenceEntry<K, V> nextEntry;
        CustomConcurrentHashMap<K, V>.WriteThroughEntry nextExternal;
        int nextSegmentIndex;
        int nextTableIndex = -1;

        HashIterator() {
            this.nextSegmentIndex = CustomConcurrentHashMap.this.segments.length - 1;
            advance();
        }

        /* access modifiers changed from: package-private */
        public final void advance() {
            this.nextExternal = null;
            if (!nextInChain() && !nextInTable()) {
                while (this.nextSegmentIndex >= 0) {
                    CustomConcurrentHashMap<K, V>.Segment[] segmentArr = CustomConcurrentHashMap.this.segments;
                    int i = this.nextSegmentIndex;
                    this.nextSegmentIndex = i - 1;
                    CustomConcurrentHashMap<K, V>.Segment seg = segmentArr[i];
                    if (seg.count != 0) {
                        this.currentTable = seg.table;
                        this.nextTableIndex = this.currentTable.length() - 1;
                        if (nextInTable()) {
                            return;
                        }
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public boolean nextInChain() {
            if (this.nextEntry != null) {
                this.nextEntry = this.nextEntry.getNext();
                while (this.nextEntry != null) {
                    if (advanceTo(this.nextEntry)) {
                        return true;
                    }
                    this.nextEntry = this.nextEntry.getNext();
                }
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        public boolean nextInTable() {
            while (this.nextTableIndex >= 0) {
                AtomicReferenceArray<ReferenceEntry<K, V>> atomicReferenceArray = this.currentTable;
                int i = this.nextTableIndex;
                this.nextTableIndex = i - 1;
                ReferenceEntry<K, V> referenceEntry = atomicReferenceArray.get(i);
                this.nextEntry = referenceEntry;
                if (referenceEntry != null && (advanceTo(this.nextEntry) || nextInChain())) {
                    return true;
                }
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        public boolean advanceTo(ReferenceEntry<K, V> entry) {
            K key = entry.getKey();
            V value = CustomConcurrentHashMap.this.getUnexpiredValue(entry);
            if (key == null || value == null) {
                return false;
            }
            this.nextExternal = new WriteThroughEntry(key, value);
            return true;
        }

        public boolean hasNext() {
            return this.nextExternal != null;
        }

        /* access modifiers changed from: package-private */
        public CustomConcurrentHashMap<K, V>.WriteThroughEntry nextEntry() {
            if (this.nextExternal == null) {
                throw new NoSuchElementException();
            }
            this.lastReturned = this.nextExternal;
            advance();
            return this.lastReturned;
        }

        public void remove() {
            Preconditions.checkState(this.lastReturned != null);
            CustomConcurrentHashMap.this.remove(this.lastReturned.getKey());
            this.lastReturned = null;
        }
    }

    final class KeyIterator extends CustomConcurrentHashMap<K, V>.HashIterator implements Iterator<K> {
        KeyIterator() {
            super();
        }

        public K next() {
            return nextEntry().getKey();
        }
    }

    final class ValueIterator extends CustomConcurrentHashMap<K, V>.HashIterator implements Iterator<V> {
        ValueIterator() {
            super();
        }

        public V next() {
            return nextEntry().getValue();
        }
    }

    final class WriteThroughEntry extends AbstractMapEntry<K, V> {
        final K key;
        V value;

        WriteThroughEntry(K key2, V value2) {
            this.key = key2;
            this.value = value2;
        }

        public K getKey() {
            return this.key;
        }

        public V getValue() {
            return this.value;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public boolean equals(@javax.annotation.Nullable java.lang.Object r6) {
            /*
                r5 = this;
                r4 = 0
                boolean r2 = r6 instanceof java.util.Map.Entry
                if (r2 == 0) goto L_0x0025
                r0 = r6
                java.util.Map$Entry r0 = (java.util.Map.Entry) r0
                r1 = r0
                K r2 = r5.key
                java.lang.Object r3 = r1.getKey()
                boolean r2 = r2.equals(r3)
                if (r2 == 0) goto L_0x0023
                V r2 = r5.value
                java.lang.Object r3 = r1.getValue()
                boolean r2 = r2.equals(r3)
                if (r2 == 0) goto L_0x0023
                r2 = 1
            L_0x0022:
                return r2
            L_0x0023:
                r2 = r4
                goto L_0x0022
            L_0x0025:
                r2 = r4
                goto L_0x0022
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.CustomConcurrentHashMap.WriteThroughEntry.equals(java.lang.Object):boolean");
        }

        public int hashCode() {
            return this.key.hashCode() ^ this.value.hashCode();
        }

        public V setValue(V newValue) {
            V oldValue = CustomConcurrentHashMap.this.put(this.key, newValue);
            this.value = newValue;
            return oldValue;
        }
    }

    final class EntryIterator extends CustomConcurrentHashMap<K, V>.HashIterator implements Iterator<Map.Entry<K, V>> {
        EntryIterator() {
            super();
        }

        public Map.Entry<K, V> next() {
            return nextEntry();
        }
    }

    final class KeySet extends AbstractSet<K> {
        KeySet() {
        }

        public Iterator<K> iterator() {
            return new KeyIterator();
        }

        public int size() {
            return CustomConcurrentHashMap.this.size();
        }

        public boolean isEmpty() {
            return CustomConcurrentHashMap.this.isEmpty();
        }

        public boolean contains(Object o) {
            return CustomConcurrentHashMap.this.containsKey(o);
        }

        public boolean remove(Object o) {
            return CustomConcurrentHashMap.this.remove(o) != null;
        }

        public void clear() {
            CustomConcurrentHashMap.this.clear();
        }
    }

    final class Values extends AbstractCollection<V> {
        Values() {
        }

        public Iterator<V> iterator() {
            return new ValueIterator();
        }

        public int size() {
            return CustomConcurrentHashMap.this.size();
        }

        public boolean isEmpty() {
            return CustomConcurrentHashMap.this.isEmpty();
        }

        public boolean contains(Object o) {
            return CustomConcurrentHashMap.this.containsValue(o);
        }

        public void clear() {
            CustomConcurrentHashMap.this.clear();
        }
    }

    final class EntrySet extends AbstractSet<Map.Entry<K, V>> {
        EntrySet() {
        }

        public Iterator<Map.Entry<K, V>> iterator() {
            return new EntryIterator();
        }

        public boolean contains(Object o) {
            if (!(o instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) o;
            Object key = entry.getKey();
            if (key == null) {
                return false;
            }
            V v = CustomConcurrentHashMap.this.get(key);
            return v != null && CustomConcurrentHashMap.this.valueEquivalence.equivalent(entry.getValue(), v);
        }

        public boolean remove(Object o) {
            if (!(o instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) o;
            Object key = entry.getKey();
            return key != null && CustomConcurrentHashMap.this.remove(key, entry.getValue());
        }

        public int size() {
            return CustomConcurrentHashMap.this.size();
        }

        public boolean isEmpty() {
            return CustomConcurrentHashMap.this.isEmpty();
        }

        public void clear() {
            CustomConcurrentHashMap.this.clear();
        }
    }

    /* access modifiers changed from: package-private */
    public Object writeReplace() {
        return new SerializationProxy(this.keyStrength, this.valueStrength, this.keyEquivalence, this.valueEquivalence, this.expireAfterWriteNanos, this.expireAfterAccessNanos, this.maximumSize, this.concurrencyLevel, this.evictionListener, this);
    }

    static abstract class AbstractSerializationProxy<K, V> extends ForwardingConcurrentMap<K, V> implements Serializable {
        private static final long serialVersionUID = 2;
        final int concurrencyLevel;
        transient ConcurrentMap<K, V> delegate;
        final MapEvictionListener<? super K, ? super V> evictionListener;
        final long expireAfterAccessNanos;
        final long expireAfterWriteNanos;
        final Equivalence<Object> keyEquivalence;
        final Strength keyStrength;
        final int maximumSize;
        final Equivalence<Object> valueEquivalence;
        final Strength valueStrength;

        AbstractSerializationProxy(Strength keyStrength2, Strength valueStrength2, Equivalence<Object> keyEquivalence2, Equivalence<Object> valueEquivalence2, long expireAfterWriteNanos2, long expireAfterAccessNanos2, int maximumSize2, int concurrencyLevel2, MapEvictionListener<? super K, ? super V> evictionListener2, ConcurrentMap<K, V> delegate2) {
            this.keyStrength = keyStrength2;
            this.valueStrength = valueStrength2;
            this.keyEquivalence = keyEquivalence2;
            this.valueEquivalence = valueEquivalence2;
            this.expireAfterWriteNanos = expireAfterWriteNanos2;
            this.expireAfterAccessNanos = expireAfterAccessNanos2;
            this.maximumSize = maximumSize2;
            this.concurrencyLevel = concurrencyLevel2;
            this.evictionListener = evictionListener2;
            this.delegate = delegate2;
        }

        /* access modifiers changed from: protected */
        public ConcurrentMap<K, V> delegate() {
            return this.delegate;
        }

        /* access modifiers changed from: package-private */
        public void writeMapTo(ObjectOutputStream out) throws IOException {
            out.writeInt(this.delegate.size());
            for (Map.Entry<K, V> entry : this.delegate.entrySet()) {
                out.writeObject(entry.getKey());
                out.writeObject(entry.getValue());
            }
            out.writeObject(null);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.common.collect.MapMaker.expireAfterWrite(long, java.util.concurrent.TimeUnit):com.google.common.collect.MapMaker
         arg types: [long, java.util.concurrent.TimeUnit]
         candidates:
          com.google.common.collect.MapMaker.expireAfterWrite(long, java.util.concurrent.TimeUnit):com.google.common.collect.GenericMapMaker
          com.google.common.collect.GenericMapMaker.expireAfterWrite(long, java.util.concurrent.TimeUnit):com.google.common.collect.GenericMapMaker<K0, V0>
          com.google.common.collect.MapMaker.expireAfterWrite(long, java.util.concurrent.TimeUnit):com.google.common.collect.MapMaker */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.common.collect.MapMaker.expireAfterAccess(long, java.util.concurrent.TimeUnit):com.google.common.collect.MapMaker
         arg types: [long, java.util.concurrent.TimeUnit]
         candidates:
          com.google.common.collect.MapMaker.expireAfterAccess(long, java.util.concurrent.TimeUnit):com.google.common.collect.GenericMapMaker
          com.google.common.collect.GenericMapMaker.expireAfterAccess(long, java.util.concurrent.TimeUnit):com.google.common.collect.GenericMapMaker<K0, V0>
          com.google.common.collect.MapMaker.expireAfterAccess(long, java.util.concurrent.TimeUnit):com.google.common.collect.MapMaker */
        /* access modifiers changed from: package-private */
        public MapMaker readMapMaker(ObjectInputStream in) throws IOException {
            MapMaker mapMaker = new MapMaker().initialCapacity(in.readInt()).setKeyStrength(this.keyStrength).setValueStrength(this.valueStrength).privateKeyEquivalence(this.keyEquivalence).privateValueEquivalence(this.valueEquivalence).concurrencyLevel(this.concurrencyLevel);
            mapMaker.evictionListener(this.evictionListener);
            if (this.expireAfterWriteNanos > 0) {
                mapMaker.expireAfterWrite(this.expireAfterWriteNanos, TimeUnit.NANOSECONDS);
            }
            if (this.expireAfterAccessNanos > 0) {
                mapMaker.expireAfterAccess(this.expireAfterAccessNanos, TimeUnit.NANOSECONDS);
            }
            if (this.maximumSize != -1) {
                mapMaker.maximumSize(this.maximumSize);
            }
            return mapMaker;
        }

        /* access modifiers changed from: package-private */
        public void readEntries(ObjectInputStream in) throws IOException, ClassNotFoundException {
            while (true) {
                K key = in.readObject();
                if (key != null) {
                    this.delegate.put(key, in.readObject());
                } else {
                    return;
                }
            }
        }
    }

    private static class SerializationProxy<K, V> extends AbstractSerializationProxy<K, V> {
        private static final long serialVersionUID = 2;

        SerializationProxy(Strength keyStrength, Strength valueStrength, Equivalence<Object> keyEquivalence, Equivalence<Object> valueEquivalence, long expireAfterWriteNanos, long expireAfterAccessNanos, int maximumSize, int concurrencyLevel, MapEvictionListener<? super K, ? super V> evictionListener, ConcurrentMap<K, V> delegate) {
            super(keyStrength, valueStrength, keyEquivalence, valueEquivalence, expireAfterWriteNanos, expireAfterAccessNanos, maximumSize, concurrencyLevel, evictionListener, delegate);
        }

        private void writeObject(ObjectOutputStream out) throws IOException {
            out.defaultWriteObject();
            writeMapTo(out);
        }

        private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
            in.defaultReadObject();
            this.delegate = readMapMaker(in).makeMap();
            readEntries(in);
        }

        private Object readResolve() {
            return this.delegate;
        }
    }
}
