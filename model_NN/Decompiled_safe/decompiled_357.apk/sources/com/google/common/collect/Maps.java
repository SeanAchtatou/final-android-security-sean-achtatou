package com.google.common.collect;

import com.google.common.annotations.Beta;
import com.google.common.annotations.GwtCompatible;
import com.google.common.annotations.GwtIncompatible;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.MapDifference;
import com.google.common.primitives.Ints;
import java.io.Serializable;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentMap;
import javax.annotation.Nullable;

@GwtCompatible(emulated = true)
public final class Maps {
    static final Joiner.MapJoiner STANDARD_JOINER = Collections2.STANDARD_JOINER.withKeyValueSeparator("=");

    @Beta
    public interface EntryTransformer<K, V1, V2> {
        V2 transformEntry(@Nullable K k, @Nullable V1 v1);
    }

    private Maps() {
    }

    public static <K, V> HashMap<K, V> newHashMap() {
        return new HashMap<>();
    }

    public static <K, V> HashMap<K, V> newHashMapWithExpectedSize(int expectedSize) {
        return new HashMap<>(capacity(expectedSize));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    static int capacity(int expectedSize) {
        Preconditions.checkArgument(expectedSize >= 0);
        return Ints.saturatedCast(Math.max(((long) expectedSize) * 2, 16L));
    }

    public static <K, V> HashMap<K, V> newHashMap(Map<? extends K, ? extends V> map) {
        return new HashMap<>(map);
    }

    public static <K, V> LinkedHashMap<K, V> newLinkedHashMap() {
        return new LinkedHashMap<>();
    }

    public static <K, V> LinkedHashMap<K, V> newLinkedHashMap(Map<? extends K, ? extends V> map) {
        return new LinkedHashMap<>(map);
    }

    public static <K, V> ConcurrentMap<K, V> newConcurrentMap() {
        return new MapMaker().makeMap();
    }

    public static <K extends Comparable, V> TreeMap<K, V> newTreeMap() {
        return new TreeMap<>();
    }

    public static <K, V> TreeMap<K, V> newTreeMap(SortedMap<K, ? extends V> map) {
        return new TreeMap<>((SortedMap) map);
    }

    public static <C, K extends C, V> TreeMap<K, V> newTreeMap(@Nullable Comparator<C> comparator) {
        return new TreeMap<>(comparator);
    }

    public static <K extends Enum<K>, V> EnumMap<K, V> newEnumMap(Class<K> type) {
        return new EnumMap<>((Class) Preconditions.checkNotNull(type));
    }

    public static <K extends Enum<K>, V> EnumMap<K, V> newEnumMap(Map<K, ? extends V> map) {
        return new EnumMap<>(map);
    }

    public static <K, V> IdentityHashMap<K, V> newIdentityHashMap() {
        return new IdentityHashMap<>();
    }

    public static <K, V> BiMap<K, V> synchronizedBiMap(BiMap<K, V> bimap) {
        return Synchronized.biMap(bimap, null);
    }

    public static <K, V> MapDifference<K, V> difference(Map<? extends K, ? extends V> left, Map<? extends K, ? extends V> right) {
        Map<K, V> onlyOnLeft = newHashMap();
        Map<K, V> onlyOnRight = new HashMap<>(right);
        Map<K, V> onBoth = newHashMap();
        Map<K, MapDifference.ValueDifference<V>> differences = newHashMap();
        boolean eq = true;
        for (Map.Entry<? extends K, ? extends V> entry : left.entrySet()) {
            K leftKey = entry.getKey();
            V leftValue = entry.getValue();
            if (right.containsKey(leftKey)) {
                V rightValue = onlyOnRight.remove(leftKey);
                if (Objects.equal(leftValue, rightValue)) {
                    onBoth.put(leftKey, leftValue);
                } else {
                    eq = false;
                    differences.put(leftKey, new ValueDifferenceImpl(leftValue, rightValue));
                }
            } else {
                eq = false;
                onlyOnLeft.put(leftKey, leftValue);
            }
        }
        return mapDifference(eq && onlyOnRight.isEmpty(), onlyOnLeft, onlyOnRight, onBoth, differences);
    }

    private static <K, V> MapDifference<K, V> mapDifference(boolean areEqual, Map<K, V> onlyOnLeft, Map<K, V> onlyOnRight, Map<K, V> onBoth, Map<K, MapDifference.ValueDifference<V>> differences) {
        return new MapDifferenceImpl(areEqual, Collections.unmodifiableMap(onlyOnLeft), Collections.unmodifiableMap(onlyOnRight), Collections.unmodifiableMap(onBoth), Collections.unmodifiableMap(differences));
    }

    static class MapDifferenceImpl<K, V> implements MapDifference<K, V> {
        final boolean areEqual;
        final Map<K, MapDifference.ValueDifference<V>> differences;
        final Map<K, V> onBoth;
        final Map<K, V> onlyOnLeft;
        final Map<K, V> onlyOnRight;

        MapDifferenceImpl(boolean areEqual2, Map<K, V> onlyOnLeft2, Map<K, V> onlyOnRight2, Map<K, V> onBoth2, Map<K, MapDifference.ValueDifference<V>> differences2) {
            this.areEqual = areEqual2;
            this.onlyOnLeft = onlyOnLeft2;
            this.onlyOnRight = onlyOnRight2;
            this.onBoth = onBoth2;
            this.differences = differences2;
        }

        public boolean areEqual() {
            return this.areEqual;
        }

        public Map<K, V> entriesOnlyOnLeft() {
            return this.onlyOnLeft;
        }

        public Map<K, V> entriesOnlyOnRight() {
            return this.onlyOnRight;
        }

        public Map<K, V> entriesInCommon() {
            return this.onBoth;
        }

        public Map<K, MapDifference.ValueDifference<V>> entriesDiffering() {
            return this.differences;
        }

        public boolean equals(Object object) {
            if (object == this) {
                return true;
            }
            if (!(object instanceof MapDifference)) {
                return false;
            }
            MapDifference mapDifference = (MapDifference) object;
            return entriesOnlyOnLeft().equals(mapDifference.entriesOnlyOnLeft()) && entriesOnlyOnRight().equals(mapDifference.entriesOnlyOnRight()) && entriesInCommon().equals(mapDifference.entriesInCommon()) && entriesDiffering().equals(mapDifference.entriesDiffering());
        }

        public int hashCode() {
            return Objects.hashCode(entriesOnlyOnLeft(), entriesOnlyOnRight(), entriesInCommon(), entriesDiffering());
        }

        public String toString() {
            if (this.areEqual) {
                return "equal";
            }
            StringBuilder result = new StringBuilder("not equal");
            if (!this.onlyOnLeft.isEmpty()) {
                result.append(": only on left=").append(this.onlyOnLeft);
            }
            if (!this.onlyOnRight.isEmpty()) {
                result.append(": only on right=").append(this.onlyOnRight);
            }
            if (!this.differences.isEmpty()) {
                result.append(": value differences=").append(this.differences);
            }
            return result.toString();
        }
    }

    static class ValueDifferenceImpl<V> implements MapDifference.ValueDifference<V> {
        private final V left;
        private final V right;

        ValueDifferenceImpl(@Nullable V left2, @Nullable V right2) {
            this.left = left2;
            this.right = right2;
        }

        public V leftValue() {
            return this.left;
        }

        public V rightValue() {
            return this.right;
        }

        public boolean equals(@Nullable Object object) {
            if (!(object instanceof MapDifference.ValueDifference)) {
                return false;
            }
            MapDifference.ValueDifference valueDifference = (MapDifference.ValueDifference) object;
            if (!Objects.equal(this.left, valueDifference.leftValue()) || !Objects.equal(this.right, valueDifference.rightValue())) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return Objects.hashCode(this.left, this.right);
        }

        public String toString() {
            return "(" + ((Object) this.left) + ", " + ((Object) this.right) + ")";
        }
    }

    public static <K, V> ImmutableMap<K, V> uniqueIndex(Iterable<V> values, Function<? super V, K> keyFunction) {
        Preconditions.checkNotNull(keyFunction);
        ImmutableMap.Builder<K, V> builder = ImmutableMap.builder();
        for (V value : values) {
            builder.put(keyFunction.apply(value), value);
        }
        return builder.build();
    }

    @GwtIncompatible("java.util.Properties")
    public static ImmutableMap<String, String> fromProperties(Properties properties) {
        ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
        Enumeration<?> e = properties.propertyNames();
        while (e.hasMoreElements()) {
            String key = (String) e.nextElement();
            builder.put(key, properties.getProperty(key));
        }
        return builder.build();
    }

    @GwtCompatible(serializable = true)
    public static <K, V> Map.Entry<K, V> immutableEntry(@Nullable K key, @Nullable V value) {
        return new ImmutableEntry(key, value);
    }

    static <K, V> Set<Map.Entry<K, V>> unmodifiableEntrySet(Set<Map.Entry<K, V>> entrySet) {
        return new UnmodifiableEntrySet(Collections.unmodifiableSet(entrySet));
    }

    static <K, V> Map.Entry<K, V> unmodifiableEntry(final Map.Entry<K, V> entry) {
        Preconditions.checkNotNull(entry);
        return new AbstractMapEntry<K, V>() {
            public K getKey() {
                return entry.getKey();
            }

            public V getValue() {
                return entry.getValue();
            }
        };
    }

    static class UnmodifiableEntries<K, V> extends ForwardingCollection<Map.Entry<K, V>> {
        private final Collection<Map.Entry<K, V>> entries;

        public /* bridge */ /* synthetic */ boolean add(Object x0) {
            return add((Map.Entry) ((Map.Entry) x0));
        }

        UnmodifiableEntries(Collection<Map.Entry<K, V>> entries2) {
            this.entries = entries2;
        }

        /* access modifiers changed from: protected */
        public Collection<Map.Entry<K, V>> delegate() {
            return this.entries;
        }

        public Iterator<Map.Entry<K, V>> iterator() {
            final Iterator<Map.Entry<K, V>> delegate = super.iterator();
            return new ForwardingIterator<Map.Entry<K, V>>() {
                public Map.Entry<K, V> next() {
                    return Maps.unmodifiableEntry((Map.Entry) super.next());
                }

                public void remove() {
                    throw new UnsupportedOperationException();
                }

                /* access modifiers changed from: protected */
                public Iterator<Map.Entry<K, V>> delegate() {
                    return delegate;
                }
            };
        }

        public boolean add(Map.Entry<K, V> entry) {
            throw new UnsupportedOperationException();
        }

        public boolean addAll(Collection<? extends Map.Entry<K, V>> collection) {
            throw new UnsupportedOperationException();
        }

        public void clear() {
            throw new UnsupportedOperationException();
        }

        public boolean remove(Object object) {
            throw new UnsupportedOperationException();
        }

        public boolean removeAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        public boolean retainAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        public Object[] toArray() {
            return standardToArray();
        }

        public <T> T[] toArray(T[] array) {
            return standardToArray(array);
        }
    }

    static class UnmodifiableEntrySet<K, V> extends UnmodifiableEntries<K, V> implements Set<Map.Entry<K, V>> {
        UnmodifiableEntrySet(Set<Map.Entry<K, V>> entries) {
            super(entries);
        }

        public boolean equals(@Nullable Object object) {
            return Sets.equalsImpl(this, object);
        }

        public int hashCode() {
            return Sets.hashCodeImpl(this);
        }
    }

    public static <K, V> BiMap<K, V> unmodifiableBiMap(BiMap<? extends K, ? extends V> bimap) {
        return new UnmodifiableBiMap(bimap, null);
    }

    private static class UnmodifiableBiMap<K, V> extends ForwardingMap<K, V> implements BiMap<K, V>, Serializable {
        private static final long serialVersionUID = 0;
        final BiMap<? extends K, ? extends V> delegate;
        transient BiMap<V, K> inverse;
        final Map<K, V> unmodifiableMap;
        transient Set<V> values;

        UnmodifiableBiMap(BiMap<? extends K, ? extends V> delegate2, @Nullable BiMap<V, K> inverse2) {
            this.unmodifiableMap = Collections.unmodifiableMap(delegate2);
            this.delegate = delegate2;
            this.inverse = inverse2;
        }

        /* access modifiers changed from: protected */
        public Map<K, V> delegate() {
            return this.unmodifiableMap;
        }

        public V forcePut(K k, V v) {
            throw new UnsupportedOperationException();
        }

        public BiMap<V, K> inverse() {
            BiMap<V, K> result = this.inverse;
            if (result != null) {
                return result;
            }
            UnmodifiableBiMap unmodifiableBiMap = new UnmodifiableBiMap(this.delegate.inverse(), this);
            this.inverse = unmodifiableBiMap;
            return unmodifiableBiMap;
        }

        public Set<V> values() {
            Set<V> result = this.values;
            if (result != null) {
                return result;
            }
            Set<V> unmodifiableSet = Collections.unmodifiableSet(this.delegate.values());
            this.values = unmodifiableSet;
            return unmodifiableSet;
        }
    }

    public static <K, V1, V2> Map<K, V2> transformValues(Map<K, V1> fromMap, final Function<? super V1, V2> function) {
        Preconditions.checkNotNull(function);
        return transformEntries(fromMap, new EntryTransformer<K, V1, V2>() {
            public V2 transformEntry(K k, V1 value) {
                return function.apply(value);
            }
        });
    }

    @Beta
    public static <K, V1, V2> Map<K, V2> transformEntries(Map<K, V1> fromMap, EntryTransformer<? super K, ? super V1, V2> transformer) {
        return new TransformedEntriesMap(fromMap, transformer);
    }

    static class TransformedEntriesMap<K, V1, V2> extends AbstractMap<K, V2> {
        TransformedEntriesMap<K, V1, V2>.EntrySet entrySet;
        final Map<K, V1> fromMap;
        final EntryTransformer<? super K, ? super V1, V2> transformer;

        TransformedEntriesMap(Map<K, V1> fromMap2, EntryTransformer<? super K, ? super V1, V2> transformer2) {
            this.fromMap = (Map) Preconditions.checkNotNull(fromMap2);
            this.transformer = (EntryTransformer) Preconditions.checkNotNull(transformer2);
        }

        public int size() {
            return this.fromMap.size();
        }

        public boolean containsKey(Object key) {
            return this.fromMap.containsKey(key);
        }

        public V2 get(Object key) {
            V1 value = this.fromMap.get(key);
            if (value != null || this.fromMap.containsKey(key)) {
                return this.transformer.transformEntry(key, value);
            }
            return null;
        }

        public V2 remove(Object key) {
            if (this.fromMap.containsKey(key)) {
                return this.transformer.transformEntry(key, this.fromMap.remove(key));
            }
            return null;
        }

        public void clear() {
            this.fromMap.clear();
        }

        public Set<Map.Entry<K, V2>> entrySet() {
            TransformedEntriesMap<K, V1, V2>.EntrySet result = this.entrySet;
            if (result != null) {
                return result;
            }
            TransformedEntriesMap<K, V1, V2>.EntrySet result2 = new EntrySet();
            this.entrySet = result2;
            return result2;
        }

        class EntrySet extends AbstractSet<Map.Entry<K, V2>> {
            EntrySet() {
            }

            public int size() {
                return TransformedEntriesMap.this.size();
            }

            public Iterator<Map.Entry<K, V2>> iterator() {
                final Iterator<Map.Entry<K, V1>> mapIterator = TransformedEntriesMap.this.fromMap.entrySet().iterator();
                return new Iterator<Map.Entry<K, V2>>() {
                    public boolean hasNext() {
                        return mapIterator.hasNext();
                    }

                    public Map.Entry<K, V2> next() {
                        final Map.Entry<K, V1> entry = (Map.Entry) mapIterator.next();
                        return new AbstractMapEntry<K, V2>() {
                            public K getKey() {
                                return entry.getKey();
                            }

                            public V2 getValue() {
                                return TransformedEntriesMap.this.transformer.transformEntry(entry.getKey(), entry.getValue());
                            }
                        };
                    }

                    public void remove() {
                        mapIterator.remove();
                    }
                };
            }

            public void clear() {
                TransformedEntriesMap.this.fromMap.clear();
            }

            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V2
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            public boolean contains(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 0
                    boolean r5 = r8 instanceof java.util.Map.Entry
                    if (r5 != 0) goto L_0x0007
                    r5 = r6
                L_0x0006:
                    return r5
                L_0x0007:
                    r0 = r8
                    java.util.Map$Entry r0 = (java.util.Map.Entry) r0
                    r1 = r0
                    java.lang.Object r2 = r1.getKey()
                    java.lang.Object r3 = r1.getValue()
                    com.google.common.collect.Maps$TransformedEntriesMap r5 = com.google.common.collect.Maps.TransformedEntriesMap.this
                    java.lang.Object r4 = r5.get(r2)
                    if (r4 == 0) goto L_0x0020
                    boolean r5 = r4.equals(r3)
                    goto L_0x0006
                L_0x0020:
                    if (r3 != 0) goto L_0x002c
                    com.google.common.collect.Maps$TransformedEntriesMap r5 = com.google.common.collect.Maps.TransformedEntriesMap.this
                    boolean r5 = r5.containsKey(r2)
                    if (r5 == 0) goto L_0x002c
                    r5 = 1
                    goto L_0x0006
                L_0x002c:
                    r5 = r6
                    goto L_0x0006
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.Maps.TransformedEntriesMap.EntrySet.contains(java.lang.Object):boolean");
            }

            public boolean remove(Object o) {
                if (!contains(o)) {
                    return false;
                }
                TransformedEntriesMap.this.fromMap.remove(((Map.Entry) o).getKey());
                return true;
            }
        }
    }

    public static <K, V> Map<K, V> filterKeys(Map<K, V> unfiltered, final Predicate<? super K> keyPredicate) {
        Preconditions.checkNotNull(keyPredicate);
        Predicate<Map.Entry<K, V>> entryPredicate = new Predicate<Map.Entry<K, V>>() {
            public /* bridge */ /* synthetic */ boolean apply(Object x0) {
                return apply((Map.Entry) ((Map.Entry) x0));
            }

            public boolean apply(Map.Entry<K, V> input) {
                return keyPredicate.apply(input.getKey());
            }
        };
        return unfiltered instanceof AbstractFilteredMap ? filterFiltered((AbstractFilteredMap) unfiltered, entryPredicate) : new FilteredKeyMap((Map) Preconditions.checkNotNull(unfiltered), keyPredicate, entryPredicate);
    }

    public static <K, V> Map<K, V> filterValues(Map<K, V> unfiltered, final Predicate<? super V> valuePredicate) {
        Preconditions.checkNotNull(valuePredicate);
        return filterEntries(unfiltered, new Predicate<Map.Entry<K, V>>() {
            public /* bridge */ /* synthetic */ boolean apply(Object x0) {
                return apply((Map.Entry) ((Map.Entry) x0));
            }

            public boolean apply(Map.Entry<K, V> input) {
                return valuePredicate.apply(input.getValue());
            }
        });
    }

    public static <K, V> Map<K, V> filterEntries(Map<K, V> unfiltered, Predicate<? super Map.Entry<K, V>> entryPredicate) {
        Preconditions.checkNotNull(entryPredicate);
        return unfiltered instanceof AbstractFilteredMap ? filterFiltered((AbstractFilteredMap) unfiltered, entryPredicate) : new FilteredEntryMap((Map) Preconditions.checkNotNull(unfiltered), entryPredicate);
    }

    private static <K, V> Map<K, V> filterFiltered(AbstractFilteredMap<K, V> map, Predicate<? super Map.Entry<K, V>> entryPredicate) {
        return new FilteredEntryMap(map.unfiltered, Predicates.and(map.predicate, entryPredicate));
    }

    private static abstract class AbstractFilteredMap<K, V> extends AbstractMap<K, V> {
        final Predicate<? super Map.Entry<K, V>> predicate;
        final Map<K, V> unfiltered;
        Collection<V> values;

        AbstractFilteredMap(Map<K, V> unfiltered2, Predicate<? super Map.Entry<K, V>> predicate2) {
            this.unfiltered = unfiltered2;
            this.predicate = predicate2;
        }

        /* access modifiers changed from: package-private */
        public boolean apply(Object key, V value) {
            return this.predicate.apply(Maps.immutableEntry(key, value));
        }

        public V put(K key, V value) {
            Preconditions.checkArgument(apply(key, value));
            return this.unfiltered.put(key, value);
        }

        public void putAll(Map<? extends K, ? extends V> map) {
            for (Map.Entry<? extends K, ? extends V> entry : map.entrySet()) {
                Preconditions.checkArgument(apply(entry.getKey(), entry.getValue()));
            }
            this.unfiltered.putAll(map);
        }

        public boolean containsKey(Object key) {
            return this.unfiltered.containsKey(key) && apply(key, this.unfiltered.get(key));
        }

        public V get(Object key) {
            V value = this.unfiltered.get(key);
            if (value == null || !apply(key, value)) {
                return null;
            }
            return value;
        }

        public boolean isEmpty() {
            return entrySet().isEmpty();
        }

        public V remove(Object key) {
            if (containsKey(key)) {
                return this.unfiltered.remove(key);
            }
            return null;
        }

        public Collection<V> values() {
            Collection<V> result = this.values;
            if (result != null) {
                return result;
            }
            Values values2 = new Values();
            this.values = values2;
            return values2;
        }

        class Values extends AbstractCollection<V> {
            Values() {
            }

            public Iterator<V> iterator() {
                final Iterator<Map.Entry<K, V>> entryIterator = AbstractFilteredMap.this.entrySet().iterator();
                return new UnmodifiableIterator<V>() {
                    public boolean hasNext() {
                        return entryIterator.hasNext();
                    }

                    public V next() {
                        return ((Map.Entry) entryIterator.next()).getValue();
                    }
                };
            }

            public int size() {
                return AbstractFilteredMap.this.entrySet().size();
            }

            public void clear() {
                AbstractFilteredMap.this.entrySet().clear();
            }

            public boolean isEmpty() {
                return AbstractFilteredMap.this.entrySet().isEmpty();
            }

            public boolean remove(Object o) {
                Iterator<Map.Entry<K, V>> iterator = AbstractFilteredMap.this.unfiltered.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry<K, V> entry = iterator.next();
                    if (Objects.equal(o, entry.getValue()) && AbstractFilteredMap.this.predicate.apply(entry)) {
                        iterator.remove();
                        return true;
                    }
                }
                return false;
            }

            public boolean removeAll(Collection<?> collection) {
                Preconditions.checkNotNull(collection);
                boolean changed = false;
                Iterator<Map.Entry<K, V>> iterator = AbstractFilteredMap.this.unfiltered.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry<K, V> entry = iterator.next();
                    if (collection.contains(entry.getValue()) && AbstractFilteredMap.this.predicate.apply(entry)) {
                        iterator.remove();
                        changed = true;
                    }
                }
                return changed;
            }

            public boolean retainAll(Collection<?> collection) {
                Preconditions.checkNotNull(collection);
                boolean changed = false;
                Iterator<Map.Entry<K, V>> iterator = AbstractFilteredMap.this.unfiltered.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry<K, V> entry = iterator.next();
                    if (!collection.contains(entry.getValue()) && AbstractFilteredMap.this.predicate.apply(entry)) {
                        iterator.remove();
                        changed = true;
                    }
                }
                return changed;
            }

            public Object[] toArray() {
                return Lists.newArrayList(iterator()).toArray();
            }

            public <T> T[] toArray(T[] array) {
                return Lists.newArrayList(iterator()).toArray(array);
            }
        }
    }

    private static class FilteredKeyMap<K, V> extends AbstractFilteredMap<K, V> {
        Set<Map.Entry<K, V>> entrySet;
        Predicate<? super K> keyPredicate;
        Set<K> keySet;

        FilteredKeyMap(Map<K, V> unfiltered, Predicate<? super K> keyPredicate2, Predicate<Map.Entry<K, V>> entryPredicate) {
            super(unfiltered, entryPredicate);
            this.keyPredicate = keyPredicate2;
        }

        public Set<Map.Entry<K, V>> entrySet() {
            Set<Map.Entry<K, V>> result = this.entrySet;
            if (result != null) {
                return result;
            }
            Set<Map.Entry<K, V>> filter = Sets.filter(this.unfiltered.entrySet(), this.predicate);
            this.entrySet = filter;
            return filter;
        }

        public Set<K> keySet() {
            Set<K> result = this.keySet;
            if (result != null) {
                return result;
            }
            Set<K> filter = Sets.filter(this.unfiltered.keySet(), this.keyPredicate);
            this.keySet = filter;
            return filter;
        }

        public boolean containsKey(Object key) {
            return this.unfiltered.containsKey(key) && this.keyPredicate.apply(key);
        }
    }

    static class FilteredEntryMap<K, V> extends AbstractFilteredMap<K, V> {
        Set<Map.Entry<K, V>> entrySet;
        final Set<Map.Entry<K, V>> filteredEntrySet;
        Set<K> keySet;

        FilteredEntryMap(Map<K, V> unfiltered, Predicate<? super Map.Entry<K, V>> entryPredicate) {
            super(unfiltered, entryPredicate);
            this.filteredEntrySet = Sets.filter(unfiltered.entrySet(), this.predicate);
        }

        public Set<Map.Entry<K, V>> entrySet() {
            Set<Map.Entry<K, V>> result = this.entrySet;
            if (result != null) {
                return result;
            }
            EntrySet entrySet2 = new EntrySet();
            this.entrySet = entrySet2;
            return entrySet2;
        }

        private class EntrySet extends ForwardingSet<Map.Entry<K, V>> {
            private EntrySet() {
            }

            /* access modifiers changed from: protected */
            public Set<Map.Entry<K, V>> delegate() {
                return FilteredEntryMap.this.filteredEntrySet;
            }

            public Iterator<Map.Entry<K, V>> iterator() {
                final Iterator<Map.Entry<K, V>> iterator = FilteredEntryMap.this.filteredEntrySet.iterator();
                return new UnmodifiableIterator<Map.Entry<K, V>>() {
                    public boolean hasNext() {
                        return iterator.hasNext();
                    }

                    public Map.Entry<K, V> next() {
                        final Map.Entry<K, V> entry = (Map.Entry) iterator.next();
                        return new ForwardingMapEntry<K, V>() {
                            /* access modifiers changed from: protected */
                            public Map.Entry<K, V> delegate() {
                                return entry;
                            }

                            public V setValue(V value) {
                                Preconditions.checkArgument(FilteredEntryMap.this.apply(entry.getKey(), value));
                                return super.setValue(value);
                            }
                        };
                    }
                };
            }
        }

        public Set<K> keySet() {
            Set<K> result = this.keySet;
            if (result != null) {
                return result;
            }
            KeySet keySet2 = new KeySet();
            this.keySet = keySet2;
            return keySet2;
        }

        private class KeySet extends AbstractSet<K> {
            private KeySet() {
            }

            public Iterator<K> iterator() {
                final Iterator<Map.Entry<K, V>> iterator = FilteredEntryMap.this.filteredEntrySet.iterator();
                return new UnmodifiableIterator<K>() {
                    public boolean hasNext() {
                        return iterator.hasNext();
                    }

                    public K next() {
                        return ((Map.Entry) iterator.next()).getKey();
                    }
                };
            }

            public int size() {
                return FilteredEntryMap.this.filteredEntrySet.size();
            }

            public void clear() {
                FilteredEntryMap.this.filteredEntrySet.clear();
            }

            public boolean contains(Object o) {
                return FilteredEntryMap.this.containsKey(o);
            }

            public boolean remove(Object o) {
                if (!FilteredEntryMap.this.containsKey(o)) {
                    return false;
                }
                FilteredEntryMap.this.unfiltered.remove(o);
                return true;
            }

            public boolean removeAll(Collection<?> collection) {
                Preconditions.checkNotNull(collection);
                boolean changed = false;
                for (Object obj : collection) {
                    changed |= remove(obj);
                }
                return changed;
            }

            public boolean retainAll(Collection<?> collection) {
                Preconditions.checkNotNull(collection);
                boolean changed = false;
                Iterator<Map.Entry<K, V>> iterator = FilteredEntryMap.this.unfiltered.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry<K, V> entry = iterator.next();
                    if (!collection.contains(entry.getKey()) && FilteredEntryMap.this.predicate.apply(entry)) {
                        iterator.remove();
                        changed = true;
                    }
                }
                return changed;
            }

            public Object[] toArray() {
                return Lists.newArrayList(iterator()).toArray();
            }

            public <T> T[] toArray(T[] array) {
                return Lists.newArrayList(iterator()).toArray(array);
            }
        }
    }

    @GwtCompatible
    static abstract class ImprovedAbstractMap<K, V> extends AbstractMap<K, V> {
        private Set<Map.Entry<K, V>> entrySet;
        private Set<K> keySet;
        private Collection<V> values;

        /* access modifiers changed from: protected */
        public abstract Set<Map.Entry<K, V>> createEntrySet();

        ImprovedAbstractMap() {
        }

        public Set<Map.Entry<K, V>> entrySet() {
            Set<Map.Entry<K, V>> result = this.entrySet;
            if (result != null) {
                return result;
            }
            Set<Map.Entry<K, V>> result2 = createEntrySet();
            this.entrySet = result2;
            return result2;
        }

        public Set<K> keySet() {
            Set<K> result = this.keySet;
            if (result != null) {
                return result;
            }
            final Set<K> delegate = super.keySet();
            Set<K> result2 = new ForwardingSet<K>() {
                /* access modifiers changed from: protected */
                public Set<K> delegate() {
                    return delegate;
                }

                public boolean isEmpty() {
                    return ImprovedAbstractMap.this.isEmpty();
                }

                public boolean remove(Object object) {
                    if (!contains(object)) {
                        return false;
                    }
                    ImprovedAbstractMap.this.remove(object);
                    return true;
                }
            };
            this.keySet = result2;
            return result2;
        }

        public Collection<V> values() {
            Collection<V> result = this.values;
            if (result != null) {
                return result;
            }
            final Collection<V> delegate = super.values();
            Collection<V> result2 = new ForwardingCollection<V>() {
                /* access modifiers changed from: protected */
                public Collection<V> delegate() {
                    return delegate;
                }

                public boolean isEmpty() {
                    return ImprovedAbstractMap.this.isEmpty();
                }
            };
            this.values = result2;
            return result2;
        }

        public boolean isEmpty() {
            return entrySet().isEmpty();
        }
    }

    static <V> V safeGet(Map<?, V> map, Object key) {
        try {
            return map.get(key);
        } catch (ClassCastException e) {
            return null;
        }
    }

    static boolean safeContainsKey(Map<?, ?> map, Object key) {
        try {
            return map.containsKey(key);
        } catch (ClassCastException e) {
            return false;
        }
    }

    static <K, V> Set<Map.Entry<K, V>> entrySetImpl(Map<K, V> map, Supplier<Iterator<Map.Entry<K, V>>> entryIteratorSupplier) {
        return new EntrySetImpl(map, entryIteratorSupplier);
    }

    private static class EntrySetImpl<K, V> extends AbstractSet<Map.Entry<K, V>> {
        private final Supplier<Iterator<Map.Entry<K, V>>> entryIteratorSupplier;
        private final Map<K, V> map;

        /* JADX WARN: Type inference failed for: r3v0, types: [com.google.common.base.Supplier<java.util.Iterator<java.util.Map$Entry<K, V>>>, java.lang.Object] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        EntrySetImpl(java.util.Map<K, V> r2, com.google.common.base.Supplier<java.util.Iterator<java.util.Map.Entry<K, V>>> r3) {
            /*
                r1 = this;
                r1.<init>()
                java.lang.Object r0 = com.google.common.base.Preconditions.checkNotNull(r2)
                java.util.Map r0 = (java.util.Map) r0
                r1.map = r0
                java.lang.Object r0 = com.google.common.base.Preconditions.checkNotNull(r3)
                com.google.common.base.Supplier r0 = (com.google.common.base.Supplier) r0
                r1.entryIteratorSupplier = r0
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.Maps.EntrySetImpl.<init>(java.util.Map, com.google.common.base.Supplier):void");
        }

        public Iterator<Map.Entry<K, V>> iterator() {
            return this.entryIteratorSupplier.get();
        }

        public int size() {
            return this.map.size();
        }

        public void clear() {
            this.map.clear();
        }

        public boolean contains(Object o) {
            if (o instanceof Map.Entry) {
                Map.Entry entry = (Map.Entry) o;
                if (this.map.containsKey(entry.getKey())) {
                    return Objects.equal(this.map.get(entry.getKey()), entry.getValue());
                }
            }
            return false;
        }

        public boolean isEmpty() {
            return this.map.isEmpty();
        }

        public boolean remove(Object o) {
            if (!contains(o)) {
                return false;
            }
            this.map.remove(((Map.Entry) o).getKey());
            return true;
        }

        public int hashCode() {
            return this.map.hashCode();
        }
    }

    static <K, V> boolean containsEntryImpl(Collection<Map.Entry<K, V>> c, Object o) {
        if (!(o instanceof Map.Entry)) {
            return false;
        }
        return c.contains(unmodifiableEntry((Map.Entry) o));
    }

    static <K, V> boolean removeEntryImpl(Collection<Map.Entry<K, V>> c, Object o) {
        if (!(o instanceof Map.Entry)) {
            return false;
        }
        return c.remove(unmodifiableEntry((Map.Entry) o));
    }

    static boolean equalsImpl(Map<?, ?> map, Object object) {
        if (map == object) {
            return true;
        }
        if (object instanceof Map) {
            return map.entrySet().equals(((Map) object).entrySet());
        }
        return false;
    }

    static int hashCodeImpl(Map<?, ?> map) {
        return Sets.hashCodeImpl(map.entrySet());
    }

    static String toStringImpl(Map<?, ?> map) {
        StringBuilder sb = Collections2.newStringBuilderForCollection(map.size()).append('{');
        STANDARD_JOINER.appendTo(sb, map);
        return sb.append('}').toString();
    }

    static <K, V> void putAllImpl(Map<K, V> self, Map<? extends K, ? extends V> map) {
        for (Map.Entry<? extends K, ? extends V> entry : map.entrySet()) {
            self.put(entry.getKey(), entry.getValue());
        }
    }

    static <K, V> Set<K> keySetImpl(Map<K, V> map) {
        return new AbstractMapWrapper(map).keySet();
    }

    static boolean containsKeyImpl(Map<?, ?> map, @Nullable Object key) {
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            if (Objects.equal(entry.getKey(), key)) {
                return true;
            }
        }
        return false;
    }

    static <K, V> Collection<V> valuesImpl(Map<K, V> map) {
        return new AbstractMapWrapper(map).values();
    }

    static boolean containsValueImpl(Map<?, ?> map, @Nullable Object value) {
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            if (Objects.equal(entry.getValue(), value)) {
                return true;
            }
        }
        return false;
    }

    private static final class AbstractMapWrapper<K, V> extends ImprovedAbstractMap<K, V> {
        private final Map<K, V> map;

        AbstractMapWrapper(Map<K, V> map2) {
            this.map = (Map) Preconditions.checkNotNull(map2);
        }

        public void clear() {
            this.map.clear();
        }

        public boolean containsKey(Object key) {
            return this.map.containsKey(key);
        }

        public V remove(Object key) {
            return this.map.remove(key);
        }

        public boolean containsValue(Object value) {
            return this.map.containsValue(value);
        }

        /* access modifiers changed from: protected */
        public Set<Map.Entry<K, V>> createEntrySet() {
            return this.map.entrySet();
        }

        public boolean isEmpty() {
            return this.map.isEmpty();
        }

        public int size() {
            return this.map.size();
        }
    }
}
