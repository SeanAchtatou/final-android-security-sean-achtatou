package com.google.common.collect;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableAsList;
import javax.annotation.Nullable;

final class ImmutableSortedAsList<E> extends ImmutableList<E> {
    private final transient ImmutableList<E> backingList;
    private final transient ImmutableSortedSet<E> backingSet;

    ImmutableSortedAsList(ImmutableSortedSet<E> backingSet2, ImmutableList<E> backingList2) {
        this.backingSet = backingSet2;
        this.backingList = backingList2;
    }

    public boolean contains(Object target) {
        return this.backingSet.indexOf(target) >= 0;
    }

    public int indexOf(Object target) {
        return this.backingSet.indexOf(target);
    }

    public int lastIndexOf(Object target) {
        return this.backingSet.indexOf(target);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList<E>
     arg types: [int, int]
     candidates:
      com.google.common.collect.ImmutableList.subList(int, int):java.util.List
      ClspMth{java.util.List.subList(int, int):java.util.List<E>}
      com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList<E> */
    public ImmutableList<E> subList(int fromIndex, int toIndex) {
        Preconditions.checkPositionIndexes(fromIndex, toIndex, size());
        return fromIndex == toIndex ? ImmutableList.of() : new RegularImmutableSortedSet(this.backingList.subList(fromIndex, toIndex), this.backingSet.comparator()).asList();
    }

    /* access modifiers changed from: package-private */
    public Object writeReplace() {
        return new ImmutableAsList.SerializedForm(this.backingSet);
    }

    public UnmodifiableIterator<E> iterator() {
        return this.backingList.iterator();
    }

    public E get(int index) {
        return this.backingList.get(index);
    }

    public UnmodifiableListIterator<E> listIterator() {
        return this.backingList.listIterator();
    }

    public UnmodifiableListIterator<E> listIterator(int index) {
        return this.backingList.listIterator(index);
    }

    public int size() {
        return this.backingList.size();
    }

    public boolean equals(@Nullable Object obj) {
        return this.backingList.equals(obj);
    }

    public int hashCode() {
        return this.backingList.hashCode();
    }

    /* access modifiers changed from: package-private */
    public boolean isPartialView() {
        return this.backingList.isPartialView();
    }
}
