package com.google.common.collect;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Preconditions;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import javax.annotation.Nullable;

@GwtCompatible
abstract class TransformedImmutableList<D, E> extends ImmutableList<E> {
    private final transient ImmutableList<D> backingList;

    /* access modifiers changed from: package-private */
    public abstract E transform(Object obj);

    private class TransformedView extends TransformedImmutableList<D, E> {
        public /* bridge */ /* synthetic */ ListIterator listIterator(int x0) {
            return TransformedImmutableList.super.listIterator(x0);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.common.collect.TransformedImmutableList.subList(int, int):com.google.common.collect.ImmutableList<E>
         arg types: [int, int]
         candidates:
          com.google.common.collect.TransformedImmutableList.TransformedView.subList(int, int):java.util.List
          com.google.common.collect.TransformedImmutableList.subList(int, int):java.util.List
          com.google.common.collect.ImmutableList.subList(int, int):java.util.List
          ClspMth{java.util.List.subList(int, int):java.util.List<E>}
          com.google.common.collect.TransformedImmutableList.subList(int, int):com.google.common.collect.ImmutableList<E> */
        public /* bridge */ /* synthetic */ List subList(int x0, int x1) {
            return TransformedImmutableList.super.subList(x0, x1);
        }

        TransformedView(ImmutableList<D> backingList) {
            super(backingList);
        }

        /* access modifiers changed from: package-private */
        public E transform(D d) {
            return TransformedImmutableList.this.transform(d);
        }
    }

    TransformedImmutableList(ImmutableList<D> backingList2) {
        this.backingList = (ImmutableList) Preconditions.checkNotNull(backingList2);
    }

    public int indexOf(@Nullable Object object) {
        if (object == null) {
            return -1;
        }
        for (int i = 0; i < size(); i++) {
            if (get(i).equals(object)) {
                return i;
            }
        }
        return -1;
    }

    public int lastIndexOf(@Nullable Object object) {
        if (object == null) {
            return -1;
        }
        for (int i = size() - 1; i >= 0; i--) {
            if (get(i).equals(object)) {
                return i;
            }
        }
        return -1;
    }

    public E get(int index) {
        return transform(this.backingList.get(index));
    }

    public UnmodifiableListIterator<E> listIterator(int index) {
        return new AbstractIndexedListIterator<E>(size(), index) {
            /* access modifiers changed from: protected */
            public E get(int index) {
                return TransformedImmutableList.this.get(index);
            }
        };
    }

    public int size() {
        return this.backingList.size();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList<E>
     arg types: [int, int]
     candidates:
      com.google.common.collect.ImmutableList.subList(int, int):java.util.List
      ClspMth{java.util.List.subList(int, int):java.util.List<E>}
      com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList<E> */
    public ImmutableList<E> subList(int fromIndex, int toIndex) {
        return new TransformedView(this.backingList.subList(fromIndex, toIndex));
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof List)) {
            return false;
        }
        List list = (List) obj;
        return size() == list.size() && Iterators.elementsEqual(iterator(), list.iterator());
    }

    public int hashCode() {
        int hashCode = 1;
        Iterator i$ = iterator();
        while (i$.hasNext()) {
            E e = i$.next();
            hashCode = (hashCode * 31) + (e == null ? 0 : e.hashCode());
        }
        return hashCode;
    }

    public Object[] toArray() {
        return ObjectArrays.toArrayImpl(this);
    }

    public <T> T[] toArray(T[] array) {
        return ObjectArrays.toArrayImpl(this, array);
    }

    /* access modifiers changed from: package-private */
    public boolean isPartialView() {
        return true;
    }
}
