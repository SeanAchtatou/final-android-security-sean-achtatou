package com.google.common.collect;

import com.google.common.annotations.Beta;
import java.util.concurrent.Executor;
import javax.annotation.Nullable;

@Beta
public final class EvictionListeners {
    private EvictionListeners() {
    }

    public static <K, V> MapEvictionListener<K, V> asynchronous(final MapEvictionListener<K, V> listener, final Executor executor) {
        return new MapEvictionListener<K, V>() {
            public void onEviction(@Nullable final K key, @Nullable final V value) {
                executor.execute(new Runnable() {
                    public void run() {
                        listener.onEviction(key, value);
                    }
                });
            }
        };
    }
}
