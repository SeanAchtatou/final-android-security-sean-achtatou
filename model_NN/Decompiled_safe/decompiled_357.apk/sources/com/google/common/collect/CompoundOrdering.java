package com.google.common.collect;

import com.google.common.annotations.GwtCompatible;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

@GwtCompatible(serializable = true)
final class CompoundOrdering<T> extends Ordering<T> implements Serializable {
    private static final long serialVersionUID = 0;
    final ImmutableList<Comparator<? super T>> comparators;

    CompoundOrdering(Comparator comparator, Comparator comparator2) {
        this.comparators = ImmutableList.of(comparator, comparator2);
    }

    CompoundOrdering(Iterable<? extends Comparator<? super T>> comparators2) {
        this.comparators = ImmutableList.copyOf(comparators2);
    }

    CompoundOrdering(List list, Comparator comparator) {
        this.comparators = new ImmutableList.Builder().addAll((Iterable) list).add((Sets.CartesianSet<B>.Axis) comparator).build();
    }

    public int compare(T left, T right) {
        Iterator i$ = this.comparators.iterator();
        while (i$.hasNext()) {
            int result = ((Comparator) i$.next()).compare(left, right);
            if (result != 0) {
                return result;
            }
        }
        return 0;
    }

    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object instanceof CompoundOrdering) {
            return this.comparators.equals(((CompoundOrdering) object).comparators);
        }
        return false;
    }

    public int hashCode() {
        return this.comparators.hashCode();
    }

    public String toString() {
        return "Ordering.compound(" + this.comparators + ")";
    }
}
