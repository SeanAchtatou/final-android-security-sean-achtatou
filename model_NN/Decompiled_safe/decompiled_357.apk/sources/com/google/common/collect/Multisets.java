package com.google.common.collect;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.Multiset;
import com.google.common.primitives.Ints;
import java.io.Serializable;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;
import javax.annotation.Nullable;

@GwtCompatible
public final class Multisets {
    private Multisets() {
    }

    public static <E> Multiset<E> unmodifiableMultiset(Multiset<? extends E> multiset) {
        return new UnmodifiableMultiset((Multiset) Preconditions.checkNotNull(multiset));
    }

    private static class UnmodifiableMultiset<E> extends ForwardingMultiset<E> implements Serializable {
        private static final long serialVersionUID = 0;
        final Multiset<? extends E> delegate;
        transient Set<E> elementSet;
        transient Set<Multiset.Entry<E>> entrySet;

        UnmodifiableMultiset(Multiset<? extends E> delegate2) {
            this.delegate = delegate2;
        }

        /* access modifiers changed from: protected */
        public Multiset<E> delegate() {
            return this.delegate;
        }

        public Set<E> elementSet() {
            Set<E> es = this.elementSet;
            if (es != null) {
                return es;
            }
            Set<E> unmodifiableSet = Collections.unmodifiableSet(this.delegate.elementSet());
            this.elementSet = unmodifiableSet;
            return unmodifiableSet;
        }

        public Set<Multiset.Entry<E>> entrySet() {
            Set<Multiset.Entry<E>> es = this.entrySet;
            if (es != null) {
                return es;
            }
            Set<T> unmodifiableSet = Collections.unmodifiableSet(this.delegate.entrySet());
            this.entrySet = unmodifiableSet;
            return unmodifiableSet;
        }

        public Iterator<E> iterator() {
            return Iterators.unmodifiableIterator(this.delegate.iterator());
        }

        public boolean add(E e) {
            throw new UnsupportedOperationException();
        }

        public int add(E e, int occurences) {
            throw new UnsupportedOperationException();
        }

        public boolean addAll(Collection<? extends E> collection) {
            throw new UnsupportedOperationException();
        }

        public boolean remove(Object element) {
            throw new UnsupportedOperationException();
        }

        public int remove(Object element, int occurrences) {
            throw new UnsupportedOperationException();
        }

        public boolean removeAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        public boolean retainAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        public void clear() {
            throw new UnsupportedOperationException();
        }

        public int setCount(E e, int count) {
            throw new UnsupportedOperationException();
        }

        public boolean setCount(E e, int oldCount, int newCount) {
            throw new UnsupportedOperationException();
        }
    }

    public static <E> Multiset.Entry<E> immutableEntry(@Nullable final E e, final int n) {
        Preconditions.checkArgument(n >= 0);
        return new AbstractEntry<E>() {
            public E getElement() {
                return e;
            }

            public int getCount() {
                return n;
            }
        };
    }

    static <E> Multiset<E> forSet(Set<E> set) {
        return new SetMultiset(set);
    }

    private static class SetMultiset<E> extends ForwardingCollection<E> implements Multiset<E>, Serializable {
        private static final long serialVersionUID = 0;
        final Set<E> delegate;
        transient Set<E> elementSet;
        transient Set<Multiset.Entry<E>> entrySet;

        SetMultiset(Set<E> set) {
            this.delegate = (Set) Preconditions.checkNotNull(set);
        }

        /* access modifiers changed from: protected */
        public Set<E> delegate() {
            return this.delegate;
        }

        public int count(Object element) {
            return this.delegate.contains(element) ? 1 : 0;
        }

        public int add(E e, int occurrences) {
            throw new UnsupportedOperationException();
        }

        public int remove(Object element, int occurrences) {
            boolean z;
            if (occurrences == 0) {
                return count(element);
            }
            if (occurrences > 0) {
                z = true;
            } else {
                z = false;
            }
            Preconditions.checkArgument(z);
            if (this.delegate.remove(element)) {
                return 1;
            }
            return 0;
        }

        public Set<E> elementSet() {
            Set<E> es = this.elementSet;
            if (es != null) {
                return es;
            }
            ElementSet elementSet2 = new ElementSet();
            this.elementSet = elementSet2;
            return elementSet2;
        }

        public Set<Multiset.Entry<E>> entrySet() {
            Set<Multiset.Entry<E>> es = this.entrySet;
            if (es != null) {
                return es;
            }
            EntrySet entrySet2 = new EntrySet();
            this.entrySet = entrySet2;
            return entrySet2;
        }

        public boolean add(E e) {
            throw new UnsupportedOperationException();
        }

        public boolean addAll(Collection<? extends E> collection) {
            throw new UnsupportedOperationException();
        }

        public int setCount(E element, int count) {
            Multisets.checkNonnegative(count, "count");
            if (count == count(element)) {
                return count;
            }
            if (count == 0) {
                remove(element);
                return 1;
            }
            throw new UnsupportedOperationException();
        }

        public boolean setCount(E element, int oldCount, int newCount) {
            return Multisets.setCountImpl(this, element, oldCount, newCount);
        }

        public boolean equals(@Nullable Object object) {
            if (!(object instanceof Multiset)) {
                return false;
            }
            Multiset multiset = (Multiset) object;
            if (size() != multiset.size() || !this.delegate.equals(multiset.elementSet())) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int sum = 0;
            Iterator i$ = iterator();
            while (i$.hasNext()) {
                E e = i$.next();
                sum += (e == null ? 0 : e.hashCode()) ^ 1;
            }
            return sum;
        }

        class ElementSet extends ForwardingSet<E> {
            ElementSet() {
            }

            /* access modifiers changed from: protected */
            public Set<E> delegate() {
                return SetMultiset.this.delegate;
            }

            public boolean add(E e) {
                throw new UnsupportedOperationException();
            }

            public boolean addAll(Collection<? extends E> collection) {
                throw new UnsupportedOperationException();
            }
        }

        class EntrySet extends AbstractSet<Multiset.Entry<E>> {
            EntrySet() {
            }

            public int size() {
                return SetMultiset.this.delegate.size();
            }

            public Iterator<Multiset.Entry<E>> iterator() {
                return new Iterator<Multiset.Entry<E>>() {
                    final Iterator<E> elements = SetMultiset.this.delegate.iterator();

                    public boolean hasNext() {
                        return this.elements.hasNext();
                    }

                    public Multiset.Entry<E> next() {
                        return Multisets.immutableEntry(this.elements.next(), 1);
                    }

                    public void remove() {
                        this.elements.remove();
                    }
                };
            }
        }
    }

    static int inferDistinctElements(Iterable<?> elements) {
        if (elements instanceof Multiset) {
            return ((Multiset) elements).elementSet().size();
        }
        return 11;
    }

    public static <E> Multiset<E> intersection(final Multiset<E> multiset1, final Multiset<?> multiset2) {
        Preconditions.checkNotNull(multiset1);
        Preconditions.checkNotNull(multiset2);
        return new AbstractMultiset<E>() {
            final Set<Multiset.Entry<E>> entrySet = new AbstractSet<Multiset.Entry<E>>() {
                public Iterator<Multiset.Entry<E>> iterator() {
                    final Iterator<Multiset.Entry<E>> iterator1 = multiset1.entrySet().iterator();
                    return new AbstractIterator<Multiset.Entry<E>>() {
                        /* Debug info: failed to restart local var, previous not found, register: 5 */
                        /* access modifiers changed from: protected */
                        public Multiset.Entry<E> computeNext() {
                            while (iterator1.hasNext()) {
                                Multiset.Entry<E> entry1 = (Multiset.Entry) iterator1.next();
                                E element = entry1.getElement();
                                int count = Math.min(entry1.getCount(), multiset2.count(element));
                                if (count > 0) {
                                    return Multisets.immutableEntry(element, count);
                                }
                            }
                            return (Multiset.Entry) endOfData();
                        }
                    };
                }

                public int size() {
                    return AnonymousClass2.this.elementSet().size();
                }

                public boolean contains(Object o) {
                    if (!(o instanceof Multiset.Entry)) {
                        return false;
                    }
                    Multiset.Entry entry = (Multiset.Entry) o;
                    int entryCount = entry.getCount();
                    if (entryCount <= 0 || AnonymousClass2.this.count(entry.getElement()) != entryCount) {
                        return false;
                    }
                    return true;
                }

                public boolean isEmpty() {
                    return AnonymousClass2.this.elementSet().isEmpty();
                }
            };

            public int count(Object element) {
                int count1 = multiset1.count(element);
                if (count1 == 0) {
                    return 0;
                }
                return Math.min(count1, multiset2.count(element));
            }

            /* access modifiers changed from: package-private */
            public Set<E> createElementSet() {
                return Sets.intersection(multiset1.elementSet(), multiset2.elementSet());
            }

            public Set<Multiset.Entry<E>> entrySet() {
                return this.entrySet;
            }
        };
    }

    static abstract class AbstractEntry<E> implements Multiset.Entry<E> {
        AbstractEntry() {
        }

        public boolean equals(@Nullable Object object) {
            if (!(object instanceof Multiset.Entry)) {
                return false;
            }
            Multiset.Entry entry = (Multiset.Entry) object;
            if (getCount() != entry.getCount() || !Objects.equal(getElement(), entry.getElement())) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            E e = getElement();
            return (e == null ? 0 : e.hashCode()) ^ getCount();
        }

        public String toString() {
            String text = String.valueOf(getElement());
            int n = getCount();
            return n == 1 ? text : text + " x " + n;
        }
    }

    static boolean equalsImpl(Multiset<?> multiset, @Nullable Object object) {
        if (object == multiset) {
            return true;
        }
        if (!(object instanceof Multiset)) {
            return false;
        }
        Multiset multiset2 = (Multiset) object;
        if (multiset.size() != multiset2.size() || multiset.entrySet().size() != multiset2.entrySet().size()) {
            return false;
        }
        for (Multiset.Entry<?> entry : multiset2.entrySet()) {
            if (multiset.count(entry.getElement()) != entry.getCount()) {
                return false;
            }
        }
        return true;
    }

    static <E> boolean addAllImpl(Multiset<E> self, Collection<? extends E> elements) {
        if (elements.isEmpty()) {
            return false;
        }
        if (elements instanceof Multiset) {
            for (Multiset.Entry<? extends E> entry : cast(elements).entrySet()) {
                self.add(entry.getElement(), entry.getCount());
            }
        } else {
            Iterators.addAll(self, elements.iterator());
        }
        return true;
    }

    static boolean removeAllImpl(Multiset<?> self, Collection<?> elementsToRemove) {
        Collection<?> collection;
        if (elementsToRemove instanceof Multiset) {
            collection = ((Multiset) elementsToRemove).elementSet();
        } else {
            collection = elementsToRemove;
        }
        return self.elementSet().removeAll(collection);
    }

    static boolean retainAllImpl(Multiset<?> self, Collection<?> elementsToRetain) {
        Collection<?> collection;
        if (elementsToRetain instanceof Multiset) {
            collection = ((Multiset) elementsToRetain).elementSet();
        } else {
            collection = elementsToRetain;
        }
        return self.elementSet().retainAll(collection);
    }

    static <E> int setCountImpl(Multiset<E> self, E element, int count) {
        checkNonnegative(count, "count");
        int oldCount = self.count(element);
        int delta = count - oldCount;
        if (delta > 0) {
            self.add(element, delta);
        } else if (delta < 0) {
            self.remove(element, -delta);
        }
        return oldCount;
    }

    static <E> boolean setCountImpl(Multiset<E> self, E element, int oldCount, int newCount) {
        checkNonnegative(oldCount, "oldCount");
        checkNonnegative(newCount, "newCount");
        if (self.count(element) != oldCount) {
            return false;
        }
        self.setCount(element, newCount);
        return true;
    }

    static <E> Set<E> elementSetImpl(Multiset<E> self) {
        return new ElementSetImpl(self);
    }

    private static final class ElementSetImpl<E> extends AbstractSet<E> implements Serializable {
        private static final long serialVersionUID = 0;
        private final Multiset<E> multiset;

        ElementSetImpl(Multiset<E> multiset2) {
            this.multiset = multiset2;
        }

        public boolean add(E e) {
            throw new UnsupportedOperationException();
        }

        public boolean addAll(Collection<? extends E> collection) {
            throw new UnsupportedOperationException();
        }

        public void clear() {
            this.multiset.clear();
        }

        public boolean contains(Object o) {
            return this.multiset.contains(o);
        }

        public boolean containsAll(Collection<?> c) {
            return this.multiset.containsAll(c);
        }

        public boolean isEmpty() {
            return this.multiset.isEmpty();
        }

        public Iterator<E> iterator() {
            final Iterator<Multiset.Entry<E>> entryIterator = this.multiset.entrySet().iterator();
            return new Iterator<E>() {
                public boolean hasNext() {
                    return entryIterator.hasNext();
                }

                public E next() {
                    return ((Multiset.Entry) entryIterator.next()).getElement();
                }

                public void remove() {
                    entryIterator.remove();
                }
            };
        }

        public boolean remove(Object o) {
            int count = this.multiset.count(o);
            if (count <= 0) {
                return false;
            }
            this.multiset.remove(o, count);
            return true;
        }

        public int size() {
            return this.multiset.entrySet().size();
        }
    }

    static <E> Iterator<E> iteratorImpl(Multiset<E> multiset) {
        return new MultisetIteratorImpl(multiset, multiset.entrySet().iterator());
    }

    static final class MultisetIteratorImpl<E> implements Iterator<E> {
        private boolean canRemove;
        private Multiset.Entry<E> currentEntry;
        private final Iterator<Multiset.Entry<E>> entryIterator;
        private int laterCount;
        private final Multiset<E> multiset;
        private int totalCount;

        MultisetIteratorImpl(Multiset<E> multiset2, Iterator<Multiset.Entry<E>> entryIterator2) {
            this.multiset = multiset2;
            this.entryIterator = entryIterator2;
        }

        public boolean hasNext() {
            return this.laterCount > 0 || this.entryIterator.hasNext();
        }

        public E next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            if (this.laterCount == 0) {
                this.currentEntry = this.entryIterator.next();
                int count = this.currentEntry.getCount();
                this.laterCount = count;
                this.totalCount = count;
            }
            this.laterCount--;
            this.canRemove = true;
            return this.currentEntry.getElement();
        }

        public void remove() {
            Preconditions.checkState(this.canRemove, "no calls to next() since the last call to remove()");
            if (this.totalCount == 1) {
                this.entryIterator.remove();
            } else {
                this.multiset.remove(this.currentEntry.getElement());
            }
            this.totalCount--;
            this.canRemove = false;
        }
    }

    static int sizeImpl(Multiset<?> multiset) {
        long size = 0;
        for (Multiset.Entry<?> entry : multiset.entrySet()) {
            size += (long) entry.getCount();
        }
        return Ints.saturatedCast(size);
    }

    static void checkNonnegative(int count, String name) {
        boolean z;
        if (count >= 0) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkArgument(z, "%s cannot be negative: %s", name, Integer.valueOf(count));
    }

    static <T> Multiset<T> cast(Iterable<T> iterable) {
        return (Multiset) iterable;
    }
}
