package com.google.common.collect;

import com.google.common.annotations.GwtCompatible;
import com.google.common.annotations.GwtIncompatible;
import com.google.common.base.Preconditions;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;
import com.google.common.primitives.Ints;
import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.Nullable;

@GwtCompatible(emulated = true)
abstract class AbstractMapBasedMultiset<E> extends AbstractMultiset<E> implements Serializable {
    @GwtIncompatible("not needed in emulated source.")
    private static final long serialVersionUID = -2250766705698539974L;
    /* access modifiers changed from: private */
    public transient Map<E, AtomicInteger> backingMap;
    private transient AbstractMapBasedMultiset<E>.EntrySet entrySet;
    /* access modifiers changed from: private */
    public transient long size = ((long) super.size());

    static /* synthetic */ long access$210(AbstractMapBasedMultiset x0) {
        long j = x0.size;
        x0.size = j - 1;
        return j;
    }

    static /* synthetic */ long access$222(AbstractMapBasedMultiset x0, long x1) {
        long j = x0.size - x1;
        x0.size = j;
        return j;
    }

    protected AbstractMapBasedMultiset(Map<E, AtomicInteger> backingMap2) {
        this.backingMap = (Map) Preconditions.checkNotNull(backingMap2);
    }

    /* access modifiers changed from: package-private */
    public Map<E, AtomicInteger> backingMap() {
        return this.backingMap;
    }

    /* access modifiers changed from: package-private */
    public void setBackingMap(Map<E, AtomicInteger> backingMap2) {
        this.backingMap = backingMap2;
    }

    public Set<Multiset.Entry<E>> entrySet() {
        AbstractMapBasedMultiset<E>.EntrySet result = this.entrySet;
        if (result != null) {
            return result;
        }
        AbstractMapBasedMultiset<E>.EntrySet result2 = new EntrySet();
        this.entrySet = result2;
        return result2;
    }

    private class EntrySet extends AbstractSet<Multiset.Entry<E>> {
        private EntrySet() {
        }

        public Iterator<Multiset.Entry<E>> iterator() {
            final Iterator<Map.Entry<E, AtomicInteger>> backingEntries = AbstractMapBasedMultiset.this.backingMap.entrySet().iterator();
            return new Iterator<Multiset.Entry<E>>() {
                Map.Entry<E, AtomicInteger> toRemove;

                public boolean hasNext() {
                    return backingEntries.hasNext();
                }

                public Multiset.Entry<E> next() {
                    final Map.Entry<E, AtomicInteger> mapEntry = (Map.Entry) backingEntries.next();
                    this.toRemove = mapEntry;
                    return new Multisets.AbstractEntry<E>() {
                        public E getElement() {
                            return mapEntry.getKey();
                        }

                        public int getCount() {
                            AtomicInteger frequency;
                            int count = ((AtomicInteger) mapEntry.getValue()).get();
                            if (count != 0 || (frequency = (AtomicInteger) AbstractMapBasedMultiset.this.backingMap.get(getElement())) == null) {
                                return count;
                            }
                            return frequency.get();
                        }
                    };
                }

                public void remove() {
                    Preconditions.checkState(this.toRemove != null, "no calls to next() since the last call to remove()");
                    AbstractMapBasedMultiset.access$222(AbstractMapBasedMultiset.this, (long) this.toRemove.getValue().getAndSet(0));
                    backingEntries.remove();
                    this.toRemove = null;
                }
            };
        }

        public int size() {
            return AbstractMapBasedMultiset.this.backingMap.size();
        }

        public void clear() {
            for (AtomicInteger frequency : AbstractMapBasedMultiset.this.backingMap.values()) {
                frequency.set(0);
            }
            AbstractMapBasedMultiset.this.backingMap.clear();
            long unused = AbstractMapBasedMultiset.this.size = 0;
        }

        public boolean contains(Object o) {
            if (!(o instanceof Multiset.Entry)) {
                return false;
            }
            Multiset.Entry entry = (Multiset.Entry) o;
            int count = AbstractMapBasedMultiset.this.count(entry.getElement());
            if (count != entry.getCount() || count <= 0) {
                return false;
            }
            return true;
        }

        public boolean remove(Object o) {
            if (!contains(o)) {
                return false;
            }
            AbstractMapBasedMultiset.access$222(AbstractMapBasedMultiset.this, (long) ((AtomicInteger) AbstractMapBasedMultiset.this.backingMap.remove(((Multiset.Entry) o).getElement())).getAndSet(0));
            return true;
        }
    }

    public int size() {
        return Ints.saturatedCast(this.size);
    }

    public Iterator<E> iterator() {
        return new MapBasedMultisetIterator();
    }

    private class MapBasedMultisetIterator implements Iterator<E> {
        boolean canRemove;
        Map.Entry<E, AtomicInteger> currentEntry;
        final Iterator<Map.Entry<E, AtomicInteger>> entryIterator;
        int occurrencesLeft;

        MapBasedMultisetIterator() {
            this.entryIterator = AbstractMapBasedMultiset.this.backingMap.entrySet().iterator();
        }

        public boolean hasNext() {
            return this.occurrencesLeft > 0 || this.entryIterator.hasNext();
        }

        public E next() {
            if (this.occurrencesLeft == 0) {
                this.currentEntry = this.entryIterator.next();
                this.occurrencesLeft = this.currentEntry.getValue().get();
            }
            this.occurrencesLeft--;
            this.canRemove = true;
            return this.currentEntry.getKey();
        }

        public void remove() {
            Preconditions.checkState(this.canRemove, "no calls to next() since the last call to remove()");
            if (this.currentEntry.getValue().get() <= 0) {
                throw new ConcurrentModificationException();
            }
            if (this.currentEntry.getValue().addAndGet(-1) == 0) {
                this.entryIterator.remove();
            }
            AbstractMapBasedMultiset.access$210(AbstractMapBasedMultiset.this);
            this.canRemove = false;
        }
    }

    public boolean contains(@Nullable Object element) {
        AtomicInteger frequency = this.backingMap.get(element);
        return frequency != null && frequency.get() > 0;
    }

    public int count(@Nullable Object element) {
        AtomicInteger frequency = this.backingMap.get(element);
        if (frequency == null) {
            return 0;
        }
        return frequency.get();
    }

    public int add(@Nullable E element, int occurrences) {
        boolean z;
        int oldCount;
        boolean z2;
        if (occurrences == 0) {
            return count(element);
        }
        if (occurrences > 0) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkArgument(z, "occurrences cannot be negative: %s", Integer.valueOf(occurrences));
        AtomicInteger frequency = this.backingMap.get(element);
        if (frequency == null) {
            oldCount = 0;
            this.backingMap.put(element, new AtomicInteger(occurrences));
        } else {
            oldCount = frequency.get();
            long newCount = ((long) oldCount) + ((long) occurrences);
            if (newCount <= 2147483647L) {
                z2 = true;
            } else {
                z2 = false;
            }
            Preconditions.checkArgument(z2, "too many occurrences: %s", Long.valueOf(newCount));
            frequency.getAndAdd(occurrences);
        }
        this.size += (long) occurrences;
        return oldCount;
    }

    public int remove(@Nullable Object element, int occurrences) {
        boolean z;
        int numberRemoved;
        if (occurrences == 0) {
            return count(element);
        }
        if (occurrences > 0) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkArgument(z, "occurrences cannot be negative: %s", Integer.valueOf(occurrences));
        AtomicInteger frequency = this.backingMap.get(element);
        if (frequency == null) {
            return 0;
        }
        int oldCount = frequency.get();
        if (oldCount > occurrences) {
            numberRemoved = occurrences;
        } else {
            numberRemoved = oldCount;
            this.backingMap.remove(element);
        }
        frequency.addAndGet(-numberRemoved);
        this.size -= (long) numberRemoved;
        return oldCount;
    }

    public int setCount(E element, int count) {
        int oldCount;
        Multisets.checkNonnegative(count, "count");
        if (count == 0) {
            oldCount = getAndSet(this.backingMap.remove(element), count);
        } else {
            AtomicInteger existingCounter = this.backingMap.get(element);
            oldCount = getAndSet(existingCounter, count);
            if (existingCounter == null) {
                this.backingMap.put(element, new AtomicInteger(count));
            }
        }
        this.size += (long) (count - oldCount);
        return oldCount;
    }

    private static int getAndSet(AtomicInteger i, int count) {
        if (i == null) {
            return 0;
        }
        return i.getAndSet(count);
    }

    /* access modifiers changed from: private */
    public int removeAllOccurrences(@Nullable Object element, Map<E, AtomicInteger> map) {
        AtomicInteger frequency = map.remove(element);
        if (frequency == null) {
            return 0;
        }
        int numberRemoved = frequency.getAndSet(0);
        this.size -= (long) numberRemoved;
        return numberRemoved;
    }

    /* access modifiers changed from: package-private */
    public Set<E> createElementSet() {
        return new MapBasedElementSet(this.backingMap);
    }

    class MapBasedElementSet extends ForwardingSet<E> {
        private final Set<E> delegate;
        private final Map<E, AtomicInteger> map;

        MapBasedElementSet(Map<E, AtomicInteger> map2) {
            this.map = map2;
            this.delegate = map2.keySet();
        }

        /* access modifiers changed from: protected */
        public Set<E> delegate() {
            return this.delegate;
        }

        public Iterator<E> iterator() {
            final Iterator<Map.Entry<E, AtomicInteger>> entries = this.map.entrySet().iterator();
            return new Iterator<E>() {
                Map.Entry<E, AtomicInteger> toRemove;

                public boolean hasNext() {
                    return entries.hasNext();
                }

                public E next() {
                    this.toRemove = (Map.Entry) entries.next();
                    return this.toRemove.getKey();
                }

                public void remove() {
                    Preconditions.checkState(this.toRemove != null, "no calls to next() since the last call to remove()");
                    AbstractMapBasedMultiset.access$222(AbstractMapBasedMultiset.this, (long) this.toRemove.getValue().getAndSet(0));
                    entries.remove();
                    this.toRemove = null;
                }
            };
        }

        public boolean remove(Object element) {
            return AbstractMapBasedMultiset.this.removeAllOccurrences(element, this.map) != 0;
        }

        public boolean removeAll(Collection<?> elementsToRemove) {
            return Iterators.removeAll(iterator(), elementsToRemove);
        }

        public boolean retainAll(Collection<?> elementsToRetain) {
            return Iterators.retainAll(iterator(), elementsToRetain);
        }

        public void clear() {
            if (this.map == AbstractMapBasedMultiset.this.backingMap) {
                AbstractMapBasedMultiset.this.clear();
                return;
            }
            Iterator<E> i = iterator();
            while (i.hasNext()) {
                i.next();
                i.remove();
            }
        }

        public Map<E, AtomicInteger> getMap() {
            return this.map;
        }
    }

    @GwtIncompatible("java.io.ObjectStreamException")
    private void readObjectNoData() throws ObjectStreamException {
        throw new InvalidObjectException("Stream data required");
    }
}
