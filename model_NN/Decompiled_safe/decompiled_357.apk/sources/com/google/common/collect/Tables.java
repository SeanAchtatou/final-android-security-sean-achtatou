package com.google.common.collect;

import com.google.common.annotations.Beta;
import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.Collections2;
import com.google.common.collect.Table;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;

@GwtCompatible
@Beta
public final class Tables {
    private Tables() {
    }

    public static <R, C, V> Table.Cell<R, C, V> immutableCell(@Nullable R rowKey, @Nullable C columnKey, @Nullable V value) {
        return new ImmutableCell(rowKey, columnKey, value);
    }

    private static class ImmutableCell<R, C, V> extends AbstractCell<R, C, V> implements Serializable {
        private static final long serialVersionUID = 0;
        final C columnKey;
        final R rowKey;
        final V value;

        ImmutableCell(@Nullable R rowKey2, @Nullable C columnKey2, @Nullable V value2) {
            this.rowKey = rowKey2;
            this.columnKey = columnKey2;
            this.value = value2;
        }

        public R getRowKey() {
            return this.rowKey;
        }

        public C getColumnKey() {
            return this.columnKey;
        }

        public V getValue() {
            return this.value;
        }
    }

    static abstract class AbstractCell<R, C, V> implements Table.Cell<R, C, V> {
        AbstractCell() {
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Table.Cell)) {
                return false;
            }
            Table.Cell cell = (Table.Cell) obj;
            return Objects.equal(getRowKey(), cell.getRowKey()) && Objects.equal(getColumnKey(), cell.getColumnKey()) && Objects.equal(getValue(), cell.getValue());
        }

        public int hashCode() {
            return Objects.hashCode(getRowKey(), getColumnKey(), getValue());
        }

        public String toString() {
            return "(" + getRowKey() + "," + getColumnKey() + ")=" + getValue();
        }
    }

    public static <R, C, V> Table<C, R, V> transpose(Table<R, C, V> table) {
        return table instanceof TransposeTable ? ((TransposeTable) table).original : new TransposeTable(table);
    }

    private static class TransposeTable<C, R, V> implements Table<C, R, V> {
        /* access modifiers changed from: private */
        public static final Function TRANSPOSE_CELL = new Function() {
            public Object apply(Object from) {
                Table.Cell cell = (Table.Cell) from;
                return Tables.immutableCell(cell.getColumnKey(), cell.getRowKey(), cell.getValue());
            }
        };
        TransposeTable<C, R, V>.CellSet cellSet;
        final Table<R, C, V> original;

        TransposeTable(Table<R, C, V> original2) {
            this.original = (Table) Preconditions.checkNotNull(original2);
        }

        public void clear() {
            this.original.clear();
        }

        public Map<C, V> column(R columnKey) {
            return this.original.row(columnKey);
        }

        public Set<R> columnKeySet() {
            return this.original.rowKeySet();
        }

        public Map<R, Map<C, V>> columnMap() {
            return this.original.rowMap();
        }

        public boolean contains(@Nullable Object rowKey, @Nullable Object columnKey) {
            return this.original.contains(columnKey, rowKey);
        }

        public boolean containsColumn(@Nullable Object columnKey) {
            return this.original.containsRow(columnKey);
        }

        public boolean containsRow(@Nullable Object rowKey) {
            return this.original.containsColumn(rowKey);
        }

        public boolean containsValue(@Nullable Object value) {
            return this.original.containsValue(value);
        }

        public V get(@Nullable Object rowKey, @Nullable Object columnKey) {
            return this.original.get(columnKey, rowKey);
        }

        public boolean isEmpty() {
            return this.original.isEmpty();
        }

        public V put(C rowKey, R columnKey, V value) {
            return this.original.put(columnKey, rowKey, value);
        }

        public void putAll(Table<? extends C, ? extends R, ? extends V> table) {
            this.original.putAll(Tables.transpose(table));
        }

        public V remove(@Nullable Object rowKey, @Nullable Object columnKey) {
            return this.original.remove(columnKey, rowKey);
        }

        public Map<R, V> row(C rowKey) {
            return this.original.column(rowKey);
        }

        public Set<C> rowKeySet() {
            return this.original.columnKeySet();
        }

        public Map<C, Map<R, V>> rowMap() {
            return this.original.columnMap();
        }

        public int size() {
            return this.original.size();
        }

        public Collection<V> values() {
            return this.original.values();
        }

        public boolean equals(@Nullable Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj instanceof Table) {
                return cellSet().equals(((Table) obj).cellSet());
            }
            return false;
        }

        public int hashCode() {
            return cellSet().hashCode();
        }

        public String toString() {
            return rowMap().toString();
        }

        public Set<Table.Cell<C, R, V>> cellSet() {
            TransposeTable<C, R, V>.CellSet result = this.cellSet;
            if (result != null) {
                return result;
            }
            TransposeTable<C, R, V>.CellSet cellSet2 = new CellSet();
            this.cellSet = cellSet2;
            return cellSet2;
        }

        class CellSet extends Collections2.TransformedCollection<Table.Cell<R, C, V>, Table.Cell<C, R, V>> implements Set<Table.Cell<C, R, V>> {
            CellSet() {
                super(TransposeTable.this.original.cellSet(), TransposeTable.TRANSPOSE_CELL);
            }

            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof Set)) {
                    return false;
                }
                Set set = (Set) obj;
                if (set.size() != size()) {
                    return false;
                }
                return containsAll(set);
            }

            public int hashCode() {
                return Sets.hashCodeImpl(this);
            }

            public boolean contains(Object obj) {
                if (!(obj instanceof Table.Cell)) {
                    return false;
                }
                Table.Cell cell = (Table.Cell) obj;
                return TransposeTable.this.original.cellSet().contains(Tables.immutableCell(cell.getColumnKey(), cell.getRowKey(), cell.getValue()));
            }

            public boolean remove(Object obj) {
                if (!(obj instanceof Table.Cell)) {
                    return false;
                }
                Table.Cell cell = (Table.Cell) obj;
                return TransposeTable.this.original.cellSet().remove(Tables.immutableCell(cell.getColumnKey(), cell.getRowKey(), cell.getValue()));
            }
        }
    }
}
