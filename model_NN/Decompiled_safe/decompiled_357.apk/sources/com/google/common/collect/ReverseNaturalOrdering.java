package com.google.common.collect;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Preconditions;
import java.io.Serializable;

@GwtCompatible(serializable = true)
final class ReverseNaturalOrdering extends Ordering<Comparable> implements Serializable {
    static final ReverseNaturalOrdering INSTANCE = new ReverseNaturalOrdering();
    private static final long serialVersionUID = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ReverseNaturalOrdering.max(java.lang.Comparable, java.lang.Comparable):E
     arg types: [java.lang.Object, java.lang.Object]
     candidates:
      com.google.common.collect.ReverseNaturalOrdering.max(java.lang.Object, java.lang.Object):java.lang.Object
      com.google.common.collect.Ordering.max(java.lang.Object, java.lang.Object):E
      com.google.common.collect.ReverseNaturalOrdering.max(java.lang.Comparable, java.lang.Comparable):E */
    public /* bridge */ /* synthetic */ Object max(Object x0, Object x1) {
        return max((Comparable) ((Comparable) x0), (Comparable) ((Comparable) x1));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ReverseNaturalOrdering.max(java.lang.Comparable, java.lang.Comparable, java.lang.Comparable, java.lang.Comparable[]):E
     arg types: [java.lang.Object, java.lang.Object, java.lang.Object, java.lang.Object[]]
     candidates:
      com.google.common.collect.ReverseNaturalOrdering.max(java.lang.Object, java.lang.Object, java.lang.Object, java.lang.Object[]):java.lang.Object
      com.google.common.collect.Ordering.max(java.lang.Object, java.lang.Object, java.lang.Object, java.lang.Object[]):E
      com.google.common.collect.ReverseNaturalOrdering.max(java.lang.Comparable, java.lang.Comparable, java.lang.Comparable, java.lang.Comparable[]):E */
    public /* bridge */ /* synthetic */ Object max(Object x0, Object x1, Object x2, Object[] x3) {
        return max((Comparable) ((Comparable) x0), (Comparable) ((Comparable) x1), (Comparable) ((Comparable) x2), (Comparable[]) ((Comparable[]) x3));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ReverseNaturalOrdering.min(java.lang.Comparable, java.lang.Comparable):E
     arg types: [java.lang.Object, java.lang.Object]
     candidates:
      com.google.common.collect.ReverseNaturalOrdering.min(java.lang.Object, java.lang.Object):java.lang.Object
      com.google.common.collect.Ordering.min(java.lang.Object, java.lang.Object):E
      com.google.common.collect.ReverseNaturalOrdering.min(java.lang.Comparable, java.lang.Comparable):E */
    public /* bridge */ /* synthetic */ Object min(Object x0, Object x1) {
        return min((Comparable) ((Comparable) x0), (Comparable) ((Comparable) x1));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ReverseNaturalOrdering.min(java.lang.Comparable, java.lang.Comparable, java.lang.Comparable, java.lang.Comparable[]):E
     arg types: [java.lang.Object, java.lang.Object, java.lang.Object, java.lang.Object[]]
     candidates:
      com.google.common.collect.ReverseNaturalOrdering.min(java.lang.Object, java.lang.Object, java.lang.Object, java.lang.Object[]):java.lang.Object
      com.google.common.collect.Ordering.min(java.lang.Object, java.lang.Object, java.lang.Object, java.lang.Object[]):E
      com.google.common.collect.ReverseNaturalOrdering.min(java.lang.Comparable, java.lang.Comparable, java.lang.Comparable, java.lang.Comparable[]):E */
    public /* bridge */ /* synthetic */ Object min(Object x0, Object x1, Object x2, Object[] x3) {
        return min((Comparable) ((Comparable) x0), (Comparable) ((Comparable) x1), (Comparable) ((Comparable) x2), (Comparable[]) ((Comparable[]) x3));
    }

    public int compare(Comparable left, Comparable right) {
        Preconditions.checkNotNull(left);
        if (left == right) {
            return 0;
        }
        return right.compareTo(left);
    }

    public <S extends Comparable> Ordering<S> reverse() {
        return Ordering.natural();
    }

    public <E extends Comparable> E min(E a, E b) {
        return (Comparable) NaturalOrdering.INSTANCE.max(a, b);
    }

    public <E extends Comparable> E min(E a, E b, E c, E... rest) {
        return (Comparable) NaturalOrdering.INSTANCE.max(a, b, c, rest);
    }

    public <E extends Comparable> E min(Iterable<E> iterable) {
        return (Comparable) NaturalOrdering.INSTANCE.max(iterable);
    }

    public <E extends Comparable> E max(E a, E b) {
        return (Comparable) NaturalOrdering.INSTANCE.min(a, b);
    }

    public <E extends Comparable> E max(E a, E b, E c, E... rest) {
        return (Comparable) NaturalOrdering.INSTANCE.min(a, b, c, rest);
    }

    public <E extends Comparable> E max(Iterable<E> iterable) {
        return (Comparable) NaturalOrdering.INSTANCE.min(iterable);
    }

    private Object readResolve() {
        return INSTANCE;
    }

    public String toString() {
        return "Ordering.natural().reverse()";
    }

    private ReverseNaturalOrdering() {
    }
}
