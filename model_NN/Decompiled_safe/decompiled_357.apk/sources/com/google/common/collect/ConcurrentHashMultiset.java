package com.google.common.collect;

import com.google.common.annotations.Beta;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import com.google.common.collect.Multiset;
import com.google.common.collect.Serialization;
import com.google.common.primitives.Ints;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import javax.annotation.Nullable;

public final class ConcurrentHashMultiset<E> extends AbstractMultiset<E> implements Serializable {
    private static final long serialVersionUID = 1;
    /* access modifiers changed from: private */
    public final transient ConcurrentMap<E, Integer> countMap;
    private transient ConcurrentHashMultiset<E>.EntrySet entrySet;

    public /* bridge */ /* synthetic */ boolean add(Object x0) {
        return super.add(x0);
    }

    public /* bridge */ /* synthetic */ boolean addAll(Collection x0) {
        return super.addAll(x0);
    }

    public /* bridge */ /* synthetic */ void clear() {
        super.clear();
    }

    public /* bridge */ /* synthetic */ boolean contains(Object x0) {
        return super.contains(x0);
    }

    public /* bridge */ /* synthetic */ Set elementSet() {
        return super.elementSet();
    }

    public /* bridge */ /* synthetic */ boolean equals(Object x0) {
        return super.equals(x0);
    }

    public /* bridge */ /* synthetic */ int hashCode() {
        return super.hashCode();
    }

    public /* bridge */ /* synthetic */ boolean isEmpty() {
        return super.isEmpty();
    }

    public /* bridge */ /* synthetic */ Iterator iterator() {
        return super.iterator();
    }

    public /* bridge */ /* synthetic */ boolean remove(Object x0) {
        return super.remove(x0);
    }

    public /* bridge */ /* synthetic */ boolean removeAll(Collection x0) {
        return super.removeAll(x0);
    }

    public /* bridge */ /* synthetic */ boolean retainAll(Collection x0) {
        return super.retainAll(x0);
    }

    public /* bridge */ /* synthetic */ String toString() {
        return super.toString();
    }

    private static class FieldSettersHolder {
        static final Serialization.FieldSetter<ConcurrentHashMultiset> COUNT_MAP_FIELD_SETTER = Serialization.getFieldSetter(ConcurrentHashMultiset.class, "countMap");

        private FieldSettersHolder() {
        }
    }

    public static <E> ConcurrentHashMultiset<E> create() {
        return new ConcurrentHashMultiset<>(new ConcurrentHashMap());
    }

    public static <E> ConcurrentHashMultiset<E> create(Iterable<? extends E> elements) {
        ConcurrentHashMultiset<E> multiset = create();
        Iterables.addAll(multiset, elements);
        return multiset;
    }

    @Beta
    public static <E> ConcurrentHashMultiset<E> create(GenericMapMaker<? super E, ? super Number> mapMaker) {
        return new ConcurrentHashMultiset<>(mapMaker.makeMap());
    }

    @VisibleForTesting
    ConcurrentHashMultiset(ConcurrentMap<E, Integer> countMap2) {
        Preconditions.checkArgument(countMap2.isEmpty());
        this.countMap = countMap2;
    }

    public int count(@Nullable Object element) {
        try {
            return unbox(this.countMap.get(element));
        } catch (NullPointerException e) {
            return 0;
        } catch (ClassCastException e2) {
            return 0;
        }
    }

    public int size() {
        long sum = 0;
        for (Integer value : this.countMap.values()) {
            sum += (long) value.intValue();
        }
        return Ints.saturatedCast(sum);
    }

    public Object[] toArray() {
        return snapshot().toArray();
    }

    public <T> T[] toArray(T[] array) {
        return snapshot().toArray(array);
    }

    private List<E> snapshot() {
        List<E> list = Lists.newArrayListWithExpectedSize(size());
        for (Multiset.Entry<E> entry : entrySet()) {
            E element = entry.getElement();
            for (int i = entry.getCount(); i > 0; i--) {
                list.add(element);
            }
        }
        return list;
    }

    public int add(E element, int occurrences) {
        boolean z;
        boolean z2;
        if (occurrences == 0) {
            return count(element);
        }
        if (occurrences > 0) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkArgument(z, "Invalid occurrences: %s", Integer.valueOf(occurrences));
        while (true) {
            int current = count(element);
            if (current != 0) {
                if (occurrences <= Integer.MAX_VALUE - current) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                Preconditions.checkArgument(z2, "Overflow adding %s occurrences to a count of %s", Integer.valueOf(occurrences), Integer.valueOf(current));
                if (this.countMap.replace(element, Integer.valueOf(current), Integer.valueOf(current + occurrences))) {
                    return current;
                }
            } else if (this.countMap.putIfAbsent(element, Integer.valueOf(occurrences)) == null) {
                return 0;
            }
        }
    }

    public int remove(@Nullable Object element, int occurrences) {
        boolean z;
        if (occurrences == 0) {
            return count(element);
        }
        if (occurrences > 0) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkArgument(z, "Invalid occurrences: %s", Integer.valueOf(occurrences));
        while (true) {
            int current = count(element);
            if (current == 0) {
                return 0;
            }
            if (occurrences < current) {
                if (this.countMap.replace(element, Integer.valueOf(current), Integer.valueOf(current - occurrences))) {
                    return current;
                }
            } else if (this.countMap.remove(element, Integer.valueOf(current))) {
                return current;
            }
        }
    }

    private int removeAllOccurrences(@Nullable Object element) {
        try {
            return unbox(this.countMap.remove(element));
        } catch (NullPointerException e) {
            return 0;
        } catch (ClassCastException e2) {
            return 0;
        }
    }

    public boolean removeExactly(@Nullable Object element, int occurrences) {
        boolean z;
        if (occurrences == 0) {
            return true;
        }
        if (occurrences > 0) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkArgument(z, "Invalid occurrences: %s", Integer.valueOf(occurrences));
        while (true) {
            int current = count(element);
            if (occurrences > current) {
                return false;
            }
            if (occurrences != current) {
                if (this.countMap.replace(element, Integer.valueOf(current), Integer.valueOf(current - occurrences))) {
                    return true;
                }
            } else if (this.countMap.remove(element, Integer.valueOf(occurrences))) {
                return true;
            }
        }
    }

    public int setCount(E element, int count) {
        Multisets.checkNonnegative(count, "count");
        return count == 0 ? removeAllOccurrences(element) : unbox(this.countMap.put(element, Integer.valueOf(count)));
    }

    public boolean setCount(E element, int oldCount, int newCount) {
        Multisets.checkNonnegative(oldCount, "oldCount");
        Multisets.checkNonnegative(newCount, "newCount");
        if (newCount == 0) {
            if (oldCount == 0) {
                return !this.countMap.containsKey(element);
            }
            return this.countMap.remove(element, Integer.valueOf(oldCount));
        } else if (oldCount == 0) {
            return this.countMap.putIfAbsent(element, Integer.valueOf(newCount)) == null;
        } else {
            return this.countMap.replace(element, Integer.valueOf(oldCount), Integer.valueOf(newCount));
        }
    }

    /* access modifiers changed from: package-private */
    public Set<E> createElementSet() {
        final Set<E> delegate = this.countMap.keySet();
        return new ForwardingSet<E>() {
            /* access modifiers changed from: protected */
            public Set<E> delegate() {
                return delegate;
            }

            public boolean remove(Object object) {
                try {
                    return delegate.remove(object);
                } catch (NullPointerException e) {
                    return false;
                } catch (ClassCastException e2) {
                    return false;
                }
            }
        };
    }

    public Set<Multiset.Entry<E>> entrySet() {
        ConcurrentHashMultiset<E>.EntrySet result = this.entrySet;
        if (result != null) {
            return result;
        }
        ConcurrentHashMultiset<E>.EntrySet result2 = new EntrySet();
        this.entrySet = result2;
        return result2;
    }

    private class EntrySet extends AbstractSet<Multiset.Entry<E>> {
        private EntrySet() {
        }

        public int size() {
            return ConcurrentHashMultiset.this.countMap.size();
        }

        public boolean isEmpty() {
            return ConcurrentHashMultiset.this.countMap.isEmpty();
        }

        public boolean contains(Object object) {
            if (!(object instanceof Multiset.Entry)) {
                return false;
            }
            Multiset.Entry entry = (Multiset.Entry) object;
            Object element = entry.getElement();
            int entryCount = entry.getCount();
            if (entryCount <= 0 || ConcurrentHashMultiset.this.count(element) != entryCount) {
                return false;
            }
            return true;
        }

        public Iterator<Multiset.Entry<E>> iterator() {
            final Iterator<Map.Entry<E, Integer>> backingIterator = ConcurrentHashMultiset.this.countMap.entrySet().iterator();
            return new Iterator<Multiset.Entry<E>>() {
                public boolean hasNext() {
                    return backingIterator.hasNext();
                }

                public Multiset.Entry<E> next() {
                    Map.Entry<E, Integer> backingEntry = (Map.Entry) backingIterator.next();
                    return Multisets.immutableEntry(backingEntry.getKey(), backingEntry.getValue().intValue());
                }

                public void remove() {
                    backingIterator.remove();
                }
            };
        }

        public Object[] toArray() {
            return snapshot().toArray();
        }

        public <T> T[] toArray(T[] array) {
            return snapshot().toArray(array);
        }

        private List<Multiset.Entry<E>> snapshot() {
            List<Multiset.Entry<E>> list = Lists.newArrayListWithExpectedSize(size());
            Iterator i$ = iterator();
            while (i$.hasNext()) {
                list.add((Multiset.Entry) i$.next());
            }
            return list;
        }

        public boolean remove(Object object) {
            if (!(object instanceof Multiset.Entry)) {
                return false;
            }
            Multiset.Entry entry = (Multiset.Entry) object;
            return ConcurrentHashMultiset.this.countMap.remove(entry.getElement(), Integer.valueOf(entry.getCount()));
        }

        public void clear() {
            ConcurrentHashMultiset.this.countMap.clear();
        }

        public int hashCode() {
            return ConcurrentHashMultiset.this.countMap.hashCode();
        }
    }

    private static int unbox(@Nullable Integer i) {
        if (i == null) {
            return 0;
        }
        return i.intValue();
    }

    private void writeObject(ObjectOutputStream stream) throws IOException {
        stream.defaultWriteObject();
        stream.writeObject(this.countMap);
    }

    private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
        stream.defaultReadObject();
        FieldSettersHolder.COUNT_MAP_FIELD_SETTER.set(this, (ConcurrentMap) stream.readObject());
    }
}
