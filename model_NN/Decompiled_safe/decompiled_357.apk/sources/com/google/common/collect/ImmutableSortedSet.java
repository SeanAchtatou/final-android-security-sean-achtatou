package com.google.common.collect;

import com.google.common.annotations.Beta;
import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;

@GwtCompatible(emulated = true, serializable = true)
public abstract class ImmutableSortedSet<E> extends ImmutableSortedSetFauxverideShim<E> implements SortedSet<E> {
    private static final ImmutableSortedSet<Object> NATURAL_EMPTY_SET = new EmptyImmutableSortedSet(NATURAL_ORDER);
    private static final Comparator NATURAL_ORDER = Ordering.natural();
    final transient Comparator<? super E> comparator;

    /* access modifiers changed from: package-private */
    public abstract ImmutableSortedSet<E> headSetImpl(E e);

    /* access modifiers changed from: package-private */
    public abstract int indexOf(Object obj);

    /* access modifiers changed from: package-private */
    public abstract ImmutableSortedSet<E> subSetImpl(E e, E e2);

    /* access modifiers changed from: package-private */
    public abstract ImmutableSortedSet<E> tailSetImpl(E e);

    private static <E> ImmutableSortedSet<E> emptySet() {
        return NATURAL_EMPTY_SET;
    }

    static <E> ImmutableSortedSet<E> emptySet(Comparator<? super E> comparator2) {
        if (NATURAL_ORDER.equals(comparator2)) {
            return emptySet();
        }
        return new EmptyImmutableSortedSet(comparator2);
    }

    public static <E> ImmutableSortedSet<E> of() {
        return emptySet();
    }

    public static <E extends Comparable<? super E>> ImmutableSortedSet<E> of(E element) {
        return new RegularImmutableSortedSet(ImmutableList.of(element), Ordering.natural());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.util.Collection):com.google.common.collect.ImmutableSortedSet<E>
     arg types: [com.google.common.collect.Ordering, java.util.List]
     candidates:
      com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.lang.Iterable):com.google.common.collect.ImmutableSortedSet<E>
      com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.util.Iterator):com.google.common.collect.ImmutableSortedSet<E>
      com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.util.Collection):com.google.common.collect.ImmutableSortedSet<E> */
    public static <E extends Comparable<? super E>> ImmutableSortedSet<E> of(E e1, E e2) {
        return copyOf((Comparator) Ordering.natural(), (Collection) Arrays.asList(e1, e2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.util.Collection):com.google.common.collect.ImmutableSortedSet<E>
     arg types: [com.google.common.collect.Ordering, java.util.List]
     candidates:
      com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.lang.Iterable):com.google.common.collect.ImmutableSortedSet<E>
      com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.util.Iterator):com.google.common.collect.ImmutableSortedSet<E>
      com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.util.Collection):com.google.common.collect.ImmutableSortedSet<E> */
    public static <E extends Comparable<? super E>> ImmutableSortedSet<E> of(E e1, E e2, E e3) {
        return copyOf((Comparator) Ordering.natural(), (Collection) Arrays.asList(e1, e2, e3));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.util.Collection):com.google.common.collect.ImmutableSortedSet<E>
     arg types: [com.google.common.collect.Ordering, java.util.List]
     candidates:
      com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.lang.Iterable):com.google.common.collect.ImmutableSortedSet<E>
      com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.util.Iterator):com.google.common.collect.ImmutableSortedSet<E>
      com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.util.Collection):com.google.common.collect.ImmutableSortedSet<E> */
    public static <E extends Comparable<? super E>> ImmutableSortedSet<E> of(E e1, E e2, E e3, E e4) {
        return copyOf((Comparator) Ordering.natural(), (Collection) Arrays.asList(e1, e2, e3, e4));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.util.Collection):com.google.common.collect.ImmutableSortedSet<E>
     arg types: [com.google.common.collect.Ordering, java.util.List]
     candidates:
      com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.lang.Iterable):com.google.common.collect.ImmutableSortedSet<E>
      com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.util.Iterator):com.google.common.collect.ImmutableSortedSet<E>
      com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.util.Collection):com.google.common.collect.ImmutableSortedSet<E> */
    public static <E extends Comparable<? super E>> ImmutableSortedSet<E> of(E e1, E e2, E e3, E e4, E e5) {
        return copyOf((Comparator) Ordering.natural(), (Collection) Arrays.asList(e1, e2, e3, e4, e5));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.util.Collection):com.google.common.collect.ImmutableSortedSet<E>
     arg types: [com.google.common.collect.Ordering, java.util.List<E>]
     candidates:
      com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.lang.Iterable):com.google.common.collect.ImmutableSortedSet<E>
      com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.util.Iterator):com.google.common.collect.ImmutableSortedSet<E>
      com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.util.Collection):com.google.common.collect.ImmutableSortedSet<E> */
    public static <E extends Comparable<? super E>> ImmutableSortedSet<E> of(E e1, E e2, E e3, E e4, E e5, E e6, E... remaining) {
        List<E> all = new ArrayList<>(remaining.length + 6);
        Collections.addAll(all, e1, e2, e3, e4, e5, e6);
        Collections.addAll(all, remaining);
        return copyOf((Comparator) Ordering.natural(), (Collection) all);
    }

    @Deprecated
    public static <E extends Comparable<? super E>> ImmutableSortedSet<E> of(E[] elements) {
        return copyOf((Comparable[]) elements);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.util.Collection):com.google.common.collect.ImmutableSortedSet<E>
     arg types: [com.google.common.collect.Ordering, java.util.List]
     candidates:
      com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.lang.Iterable):com.google.common.collect.ImmutableSortedSet<E>
      com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.util.Iterator):com.google.common.collect.ImmutableSortedSet<E>
      com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.util.Collection):com.google.common.collect.ImmutableSortedSet<E> */
    public static <E extends Comparable<? super E>> ImmutableSortedSet<E> copyOf(E[] elements) {
        return copyOf((Comparator) Ordering.natural(), (Collection) Arrays.asList(elements));
    }

    public static <E> ImmutableSortedSet<E> copyOf(Iterable<? extends E> elements) {
        return copyOf(Ordering.natural(), elements);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.util.Collection):com.google.common.collect.ImmutableSortedSet<E>
     arg types: [com.google.common.collect.Ordering<E>, java.util.Collection<? extends E>]
     candidates:
      com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.lang.Iterable):com.google.common.collect.ImmutableSortedSet<E>
      com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.util.Iterator):com.google.common.collect.ImmutableSortedSet<E>
      com.google.common.collect.ImmutableSortedSet.copyOf(java.util.Comparator, java.util.Collection):com.google.common.collect.ImmutableSortedSet<E> */
    public static <E> ImmutableSortedSet<E> copyOf(Collection<? extends E> elements) {
        return copyOf((Comparator) Ordering.natural(), (Collection) elements);
    }

    public static <E> ImmutableSortedSet<E> copyOf(Iterator<? extends E> elements) {
        return copyOfInternal(Ordering.natural(), elements);
    }

    public static <E> ImmutableSortedSet<E> copyOf(Comparator<? super E> comparator2, Iterator<? extends E> elements) {
        Preconditions.checkNotNull(comparator2);
        return copyOfInternal(comparator2, elements);
    }

    public static <E> ImmutableSortedSet<E> copyOf(Comparator<? super E> comparator2, Iterable<? extends E> elements) {
        Preconditions.checkNotNull(comparator2);
        return copyOfInternal(comparator2, elements, false);
    }

    public static <E> ImmutableSortedSet<E> copyOf(Comparator<? super E> comparator2, Collection<? extends E> elements) {
        Preconditions.checkNotNull(comparator2);
        return copyOfInternal(comparator2, elements, false);
    }

    public static <E> ImmutableSortedSet<E> copyOfSorted(SortedSet<E> sortedSet) {
        Comparator<? super E> comparator2 = sortedSet.comparator();
        if (comparator2 == null) {
            comparator2 = NATURAL_ORDER;
        }
        return copyOfInternal(comparator2, sortedSet, true);
    }

    private static <E> ImmutableSortedSet<E> copyOfInternal(Comparator<? super E> comparator2, Iterable<? extends E> elements, boolean fromSortedSet) {
        if (!(fromSortedSet || hasSameComparator(elements, comparator2)) || !(elements instanceof ImmutableSortedSet)) {
            ImmutableList<E> list = immutableSortedUniqueCopy(comparator2, Lists.newArrayList(elements));
            if (list.isEmpty()) {
                return emptySet(comparator2);
            }
            return new RegularImmutableSortedSet(list, comparator2);
        }
        ImmutableSortedSet<E> original = (ImmutableSortedSet) elements;
        if (original.isEmpty()) {
            return original;
        }
        ImmutableList<E> elementsList = original.asList();
        ImmutableList<E> copiedElementsList = ImmutableList.copyOf(elements);
        if (elementsList == copiedElementsList) {
            return original;
        }
        return new RegularImmutableSortedSet(copiedElementsList, comparator2);
    }

    /* access modifiers changed from: private */
    public static <E> ImmutableSortedSet<E> copyOfInternal(Comparator<? super E> comparator2, Iterator<? extends E> elements) {
        if (!elements.hasNext()) {
            return emptySet(comparator2);
        }
        return new RegularImmutableSortedSet(immutableSortedUniqueCopy(comparator2, Lists.newArrayList(elements)), comparator2);
    }

    private static <E> ImmutableList<E> immutableSortedUniqueCopy(Comparator<? super E> comparator2, List<E> list) {
        if (list.isEmpty()) {
            return ImmutableList.of();
        }
        Collections.sort(list, comparator2);
        int size = 1;
        for (int i = 1; i < list.size(); i++) {
            E elem = list.get(i);
            if (comparator2.compare(elem, list.get(size - 1)) != 0) {
                list.set(size, elem);
                size++;
            }
        }
        return ImmutableList.copyOf((Collection) list.subList(0, size));
    }

    static boolean hasSameComparator(Iterable<?> elements, Comparator<?> comparator2) {
        if (!(elements instanceof SortedSet)) {
            return false;
        }
        Comparator<?> comparator22 = ((SortedSet) elements).comparator();
        if (comparator22 != null) {
            return comparator2.equals(comparator22);
        }
        if (comparator2 == Ordering.natural()) {
            return true;
        }
        return false;
    }

    @Beta
    public static <E> ImmutableSortedSet<E> withExplicitOrder(List<E> elements) {
        return ExplicitOrderedImmutableSortedSet.create(elements);
    }

    @Beta
    public static <E> ImmutableSortedSet<E> withExplicitOrder(E firstElement, E... remainingElementsInOrder) {
        return withExplicitOrder(Lists.asList(firstElement, remainingElementsInOrder));
    }

    public static <E> Builder<E> orderedBy(Comparator<E> comparator2) {
        return new Builder<>(comparator2);
    }

    public static <E extends Comparable<E>> Builder<E> reverseOrder() {
        return new Builder<>(Ordering.natural().reverse());
    }

    public static <E extends Comparable<E>> Builder<E> naturalOrder() {
        return new Builder<>(Ordering.natural());
    }

    public static final class Builder<E> extends ImmutableSet.Builder<E> {
        private final Comparator<? super E> comparator;

        public Builder(Comparator<? super E> comparator2) {
            this.comparator = (Comparator) Preconditions.checkNotNull(comparator2);
        }

        public Builder<E> add(E element) {
            super.add((Object) element);
            return this;
        }

        public Builder<E> add(E... elements) {
            super.add((Object[]) elements);
            return this;
        }

        public Builder<E> addAll(Iterable<? extends E> elements) {
            super.addAll((Iterable) elements);
            return this;
        }

        public Builder<E> addAll(Iterator<? extends E> elements) {
            super.addAll((Iterator) elements);
            return this;
        }

        public ImmutableSortedSet<E> build() {
            return ImmutableSortedSet.copyOfInternal(this.comparator, this.contents.iterator());
        }
    }

    /* access modifiers changed from: package-private */
    public int unsafeCompare(Object a, Object b) {
        return unsafeCompare(this.comparator, a, b);
    }

    static int unsafeCompare(Comparator<?> comparator2, Object a, Object b) {
        return comparator2.compare(a, b);
    }

    ImmutableSortedSet(Comparator<? super E> comparator2) {
        this.comparator = comparator2;
    }

    public Comparator<? super E> comparator() {
        return this.comparator;
    }

    public ImmutableSortedSet<E> headSet(E toElement) {
        return headSetImpl(Preconditions.checkNotNull(toElement));
    }

    public ImmutableSortedSet<E> subSet(E fromElement, E toElement) {
        Preconditions.checkNotNull(fromElement);
        Preconditions.checkNotNull(toElement);
        Preconditions.checkArgument(this.comparator.compare(fromElement, toElement) <= 0);
        return subSetImpl(fromElement, toElement);
    }

    public ImmutableSortedSet<E> tailSet(E fromElement) {
        return tailSetImpl(Preconditions.checkNotNull(fromElement));
    }

    private static class SerializedForm<E> implements Serializable {
        private static final long serialVersionUID = 0;
        final Comparator<? super E> comparator;
        final Object[] elements;

        public SerializedForm(Comparator<? super E> comparator2, Object[] elements2) {
            this.comparator = comparator2;
            this.elements = elements2;
        }

        /* access modifiers changed from: package-private */
        public Object readResolve() {
            return new Builder(this.comparator).add(this.elements).build();
        }
    }

    private void readObject(ObjectInputStream stream) throws InvalidObjectException {
        throw new InvalidObjectException("Use SerializedForm");
    }

    /* access modifiers changed from: package-private */
    public Object writeReplace() {
        return new SerializedForm(this.comparator, toArray());
    }
}
