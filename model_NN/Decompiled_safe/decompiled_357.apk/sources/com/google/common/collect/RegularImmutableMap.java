package com.google.common.collect;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import java.util.Map;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

@GwtCompatible(emulated = true, serializable = true)
final class RegularImmutableMap<K, V> extends ImmutableMap<K, V> {
    private static final long serialVersionUID = 0;
    /* access modifiers changed from: private */
    public final transient LinkedEntry<K, V>[] entries;
    private transient ImmutableSet<Map.Entry<K, V>> entrySet;
    private transient ImmutableSet<K> keySet;
    /* access modifiers changed from: private */
    public final transient int keySetHashCode;
    private final transient int mask;
    private final transient LinkedEntry<K, V>[] table;
    private transient ImmutableCollection<V> values;

    private interface LinkedEntry<K, V> extends Map.Entry<K, V> {
        @Nullable
        LinkedEntry<K, V> next();
    }

    RegularImmutableMap(Map.Entry<?, ?>... immutableEntries) {
        int size = immutableEntries.length;
        this.entries = createEntryArray(size);
        int tableSize = chooseTableSize(size);
        this.table = createEntryArray(tableSize);
        this.mask = tableSize - 1;
        int keySetHashCodeMutable = 0;
        for (int entryIndex = 0; entryIndex < size; entryIndex++) {
            Map.Entry<?, ?> entry = immutableEntries[entryIndex];
            Object key = entry.getKey();
            int keyHashCode = key.hashCode();
            keySetHashCodeMutable += keyHashCode;
            int tableIndex = Hashing.smear(keyHashCode) & this.mask;
            LinkedEntry<K, V> existing = this.table[tableIndex];
            LinkedEntry<K, V> linkedEntry = newLinkedEntry(key, entry.getValue(), existing);
            this.table[tableIndex] = linkedEntry;
            this.entries[entryIndex] = linkedEntry;
            while (existing != null) {
                Preconditions.checkArgument(!key.equals(existing.getKey()), "duplicate key: %s", key);
                existing = existing.next();
            }
        }
        this.keySetHashCode = keySetHashCodeMutable;
    }

    private static int chooseTableSize(int size) {
        boolean z;
        int tableSize = Integer.highestOneBit(size) << 1;
        if (tableSize > 0) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkArgument(z, "table too large: %s", Integer.valueOf(size));
        return tableSize;
    }

    private LinkedEntry<K, V>[] createEntryArray(int size) {
        return new LinkedEntry[size];
    }

    private static <K, V> LinkedEntry<K, V> newLinkedEntry(K key, V value, @Nullable LinkedEntry<K, V> next) {
        return next == null ? new TerminalEntry(key, value) : new NonTerminalEntry(key, value, next);
    }

    @Immutable
    private static final class NonTerminalEntry<K, V> extends ImmutableEntry<K, V> implements LinkedEntry<K, V> {
        final LinkedEntry<K, V> next;

        NonTerminalEntry(K key, V value, LinkedEntry<K, V> next2) {
            super(key, value);
            this.next = next2;
        }

        public LinkedEntry<K, V> next() {
            return this.next;
        }
    }

    @Immutable
    private static final class TerminalEntry<K, V> extends ImmutableEntry<K, V> implements LinkedEntry<K, V> {
        TerminalEntry(K key, V value) {
            super(key, value);
        }

        @Nullable
        public LinkedEntry<K, V> next() {
            return null;
        }
    }

    public V get(Object key) {
        if (key == null) {
            return null;
        }
        for (LinkedEntry<K, V> entry = this.table[Hashing.smear(key.hashCode()) & this.mask]; entry != null; entry = entry.next()) {
            if (key.equals(entry.getKey())) {
                return entry.getValue();
            }
        }
        return null;
    }

    public int size() {
        return this.entries.length;
    }

    public boolean isEmpty() {
        return false;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean containsValue(java.lang.Object r7) {
        /*
            r6 = this;
            r5 = 0
            if (r7 != 0) goto L_0x0005
            r4 = r5
        L_0x0004:
            return r4
        L_0x0005:
            com.google.common.collect.RegularImmutableMap$LinkedEntry<K, V>[] r0 = r6.entries
            int r3 = r0.length
            r2 = 0
        L_0x0009:
            if (r2 >= r3) goto L_0x001c
            r1 = r0[r2]
            java.lang.Object r4 = r1.getValue()
            boolean r4 = r4.equals(r7)
            if (r4 == 0) goto L_0x0019
            r4 = 1
            goto L_0x0004
        L_0x0019:
            int r2 = r2 + 1
            goto L_0x0009
        L_0x001c:
            r4 = r5
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.RegularImmutableMap.containsValue(java.lang.Object):boolean");
    }

    /* access modifiers changed from: package-private */
    public boolean isPartialView() {
        return false;
    }

    public ImmutableSet<Map.Entry<K, V>> entrySet() {
        ImmutableSet<Map.Entry<K, V>> es = this.entrySet;
        if (es != null) {
            return es;
        }
        EntrySet entrySet2 = new EntrySet(this);
        this.entrySet = entrySet2;
        return entrySet2;
    }

    private static class EntrySet<K, V> extends ImmutableSet.ArrayImmutableSet<Map.Entry<K, V>> {
        final transient RegularImmutableMap<K, V> map;

        EntrySet(RegularImmutableMap<K, V> map2) {
            super(map2.entries);
            this.map = map2;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public boolean contains(java.lang.Object r7) {
            /*
                r6 = this;
                r5 = 0
                boolean r3 = r7 instanceof java.util.Map.Entry
                if (r3 == 0) goto L_0x0023
                r0 = r7
                java.util.Map$Entry r0 = (java.util.Map.Entry) r0
                r1 = r0
                com.google.common.collect.RegularImmutableMap<K, V> r3 = r6.map
                java.lang.Object r4 = r1.getKey()
                java.lang.Object r2 = r3.get(r4)
                if (r2 == 0) goto L_0x0021
                java.lang.Object r3 = r1.getValue()
                boolean r3 = r2.equals(r3)
                if (r3 == 0) goto L_0x0021
                r3 = 1
            L_0x0020:
                return r3
            L_0x0021:
                r3 = r5
                goto L_0x0020
            L_0x0023:
                r3 = r5
                goto L_0x0020
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.RegularImmutableMap.EntrySet.contains(java.lang.Object):boolean");
        }
    }

    public ImmutableSet<K> keySet() {
        ImmutableSet<K> ks = this.keySet;
        if (ks != null) {
            return ks;
        }
        KeySet keySet2 = new KeySet(this);
        this.keySet = keySet2;
        return keySet2;
    }

    private static class KeySet<K, V> extends ImmutableSet.TransformedImmutableSet<Map.Entry<K, V>, K> {
        final RegularImmutableMap<K, V> map;

        /* access modifiers changed from: package-private */
        public /* bridge */ /* synthetic */ Object transform(Object x0) {
            return transform((Map.Entry) ((Map.Entry) x0));
        }

        KeySet(RegularImmutableMap<K, V> map2) {
            super(map2.entries, map2.keySetHashCode);
            this.map = map2;
        }

        /* access modifiers changed from: package-private */
        public K transform(Map.Entry<K, V> element) {
            return element.getKey();
        }

        public boolean contains(Object target) {
            return this.map.containsKey(target);
        }

        /* access modifiers changed from: package-private */
        public boolean isPartialView() {
            return true;
        }
    }

    public ImmutableCollection<V> values() {
        ImmutableCollection<V> v = this.values;
        if (v != null) {
            return v;
        }
        Values values2 = new Values(this);
        this.values = values2;
        return values2;
    }

    private static class Values<V> extends ImmutableCollection<V> {
        final RegularImmutableMap<?, V> map;

        Values(RegularImmutableMap<?, V> map2) {
            this.map = map2;
        }

        public int size() {
            return this.map.entries.length;
        }

        public UnmodifiableIterator<V> iterator() {
            return new AbstractIndexedListIterator<V>(this.map.entries.length) {
                /* access modifiers changed from: protected */
                public V get(int index) {
                    return Values.this.map.entries[index].getValue();
                }
            };
        }

        public boolean contains(Object target) {
            return this.map.containsValue(target);
        }

        /* access modifiers changed from: package-private */
        public boolean isPartialView() {
            return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Object[]):java.lang.StringBuilder
     arg types: [java.lang.StringBuilder, com.google.common.collect.RegularImmutableMap$LinkedEntry<K, V>[]]
     candidates:
      com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Iterable<?>):A
      com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Object[]):A
      com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Iterable<?>):java.lang.StringBuilder
      com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Object[]):java.lang.StringBuilder */
    public String toString() {
        StringBuilder result = Collections2.newStringBuilderForCollection(size()).append('{');
        Collections2.STANDARD_JOINER.appendTo(result, (Object[]) this.entries);
        return result.append('}').toString();
    }
}
