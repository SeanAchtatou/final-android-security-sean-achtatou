package com.google.common.collect;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.Sets;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;
import javax.annotation.Nullable;

@GwtCompatible(emulated = true, serializable = true)
public abstract class ImmutableList<E> extends ImmutableCollection<E> implements List<E>, RandomAccess {
    public abstract int indexOf(@Nullable Object obj);

    public abstract int lastIndexOf(@Nullable Object obj);

    public abstract UnmodifiableListIterator<E> listIterator(int i);

    public abstract ImmutableList<E> subList(int i, int i2);

    public static <E> ImmutableList<E> of() {
        return EmptyImmutableList.INSTANCE;
    }

    public static <E> ImmutableList<E> of(E element) {
        return new SingletonImmutableList(element);
    }

    public static <E> ImmutableList<E> of(E e1, E e2) {
        return construct(e1, e2);
    }

    public static <E> ImmutableList<E> of(E e1, E e2, E e3) {
        return construct(e1, e2, e3);
    }

    public static <E> ImmutableList<E> of(E e1, E e2, E e3, E e4) {
        return construct(e1, e2, e3, e4);
    }

    public static <E> ImmutableList<E> of(E e1, E e2, E e3, E e4, E e5) {
        return construct(e1, e2, e3, e4, e5);
    }

    public static <E> ImmutableList<E> of(E e1, E e2, E e3, E e4, E e5, E e6) {
        return construct(e1, e2, e3, e4, e5, e6);
    }

    public static <E> ImmutableList<E> of(E e1, E e2, E e3, E e4, E e5, E e6, E e7) {
        return construct(e1, e2, e3, e4, e5, e6, e7);
    }

    public static <E> ImmutableList<E> of(E e1, E e2, E e3, E e4, E e5, E e6, E e7, E e8) {
        return construct(e1, e2, e3, e4, e5, e6, e7, e8);
    }

    public static <E> ImmutableList<E> of(E e1, E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9) {
        return construct(e1, e2, e3, e4, e5, e6, e7, e8, e9);
    }

    public static <E> ImmutableList<E> of(E e1, E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9, E e10) {
        return construct(e1, e2, e3, e4, e5, e6, e7, e8, e9, e10);
    }

    public static <E> ImmutableList<E> of(E e1, E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9, E e10, E e11) {
        return construct(e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11);
    }

    public static <E> ImmutableList<E> of(E e1, E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9, E e10, E e11, E e12, E... others) {
        Object[] array = new Object[(others.length + 12)];
        array[0] = e1;
        array[1] = e2;
        array[2] = e3;
        array[3] = e4;
        array[4] = e5;
        array[5] = e6;
        array[6] = e7;
        array[7] = e8;
        array[8] = e9;
        array[9] = e10;
        array[10] = e11;
        array[11] = e12;
        System.arraycopy(others, 0, array, 12, others.length);
        return construct(array);
    }

    @Deprecated
    public static <E> ImmutableList<E> of(E[] elements) {
        return copyOf(elements);
    }

    public static <E> ImmutableList<E> copyOf(Iterable iterable) {
        Preconditions.checkNotNull(iterable);
        return iterable instanceof Collection ? copyOf(Collections2.cast(iterable)) : copyOf(iterable.iterator());
    }

    public static <E> ImmutableList<E> copyOf(Collection collection) {
        if (!(collection instanceof ImmutableCollection)) {
            return copyFromCollection(collection);
        }
        ImmutableList<E> list = ((ImmutableCollection) collection).asList();
        if (list.isPartialView()) {
            return copyFromCollection(list);
        }
        return list;
    }

    public static <E> ImmutableList<E> copyOf(Iterator it) {
        return copyFromCollection(Lists.newArrayList(it));
    }

    public static <E> ImmutableList<E> copyOf(Object[] objArr) {
        switch (objArr.length) {
            case 0:
                return of();
            case 1:
                return new SingletonImmutableList(objArr[0]);
            default:
                return construct((Object[]) objArr.clone());
        }
    }

    private static <E> ImmutableList<E> copyFromCollection(Collection<? extends E> collection) {
        Object[] elements = collection.toArray();
        switch (elements.length) {
            case 0:
                return of();
            case 1:
                return new SingletonImmutableList<>(elements[0]);
            default:
                return construct(elements);
        }
    }

    private static <E> ImmutableList<E> construct(Object... elements) {
        for (int i = 0; i < elements.length; i++) {
            checkElementNotNull(elements[i], i);
        }
        return new RegularImmutableList(elements);
    }

    private static Object checkElementNotNull(Object element, int index) {
        if (element != null) {
            return element;
        }
        throw new NullPointerException("at index " + index);
    }

    ImmutableList() {
    }

    public UnmodifiableIterator<E> iterator() {
        return listIterator();
    }

    public UnmodifiableListIterator<E> listIterator() {
        return listIterator(0);
    }

    public final boolean addAll(int index, Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    public final E set(int index, E e) {
        throw new UnsupportedOperationException();
    }

    public final void add(int index, E e) {
        throw new UnsupportedOperationException();
    }

    public final E remove(int index) {
        throw new UnsupportedOperationException();
    }

    public ImmutableList<E> asList() {
        return this;
    }

    public ImmutableList<E> reverse() {
        return new ReverseImmutableList(this);
    }

    private static class ReverseImmutableList<E> extends ImmutableList<E> {
        private final transient ImmutableList<E> forwardList;
        private final transient int size;

        public /* bridge */ /* synthetic */ Iterator iterator() {
            return ImmutableList.super.iterator();
        }

        public /* bridge */ /* synthetic */ ListIterator listIterator() {
            return ImmutableList.super.listIterator();
        }

        ReverseImmutableList(ImmutableList<E> backingList) {
            this.forwardList = backingList;
            this.size = backingList.size();
        }

        /* access modifiers changed from: private */
        public int reverseIndex(int index) {
            return (this.size - 1) - index;
        }

        private int reversePosition(int index) {
            return this.size - index;
        }

        public ImmutableList<E> reverse() {
            return this.forwardList;
        }

        public boolean contains(@Nullable Object object) {
            return this.forwardList.contains(object);
        }

        public boolean containsAll(Collection<?> targets) {
            return this.forwardList.containsAll(targets);
        }

        public int indexOf(@Nullable Object object) {
            int index = this.forwardList.lastIndexOf(object);
            if (index >= 0) {
                return reverseIndex(index);
            }
            return -1;
        }

        public int lastIndexOf(@Nullable Object object) {
            int index = this.forwardList.indexOf(object);
            if (index >= 0) {
                return reverseIndex(index);
            }
            return -1;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList<E>
         arg types: [int, int]
         candidates:
          com.google.common.collect.ImmutableList.subList(int, int):java.util.List
          ClspMth{java.util.List.subList(int, int):java.util.List<E>}
          com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList<E> */
        public ImmutableList<E> subList(int fromIndex, int toIndex) {
            Preconditions.checkPositionIndexes(fromIndex, toIndex, this.size);
            return this.forwardList.subList(reversePosition(toIndex), reversePosition(fromIndex)).reverse();
        }

        public E get(int index) {
            Preconditions.checkElementIndex(index, this.size);
            return this.forwardList.get(reverseIndex(index));
        }

        public UnmodifiableListIterator<E> listIterator(int index) {
            Preconditions.checkPositionIndex(index, this.size);
            final UnmodifiableListIterator<E> forward = this.forwardList.listIterator(reversePosition(index));
            return new UnmodifiableListIterator<E>() {
                public boolean hasNext() {
                    return forward.hasPrevious();
                }

                public boolean hasPrevious() {
                    return forward.hasNext();
                }

                public E next() {
                    return forward.previous();
                }

                public int nextIndex() {
                    return ReverseImmutableList.this.reverseIndex(forward.previousIndex());
                }

                public E previous() {
                    return forward.next();
                }

                public int previousIndex() {
                    return ReverseImmutableList.this.reverseIndex(forward.nextIndex());
                }
            };
        }

        public int size() {
            return this.size;
        }

        public boolean isEmpty() {
            return this.forwardList.isEmpty();
        }

        /* access modifiers changed from: package-private */
        public boolean isPartialView() {
            return this.forwardList.isPartialView();
        }
    }

    public boolean equals(Object obj) {
        return Lists.equalsImpl(this, obj);
    }

    public int hashCode() {
        return Lists.hashCodeImpl(this);
    }

    private static class SerializedForm implements Serializable {
        private static final long serialVersionUID = 0;
        final Object[] elements;

        SerializedForm(Object[] elements2) {
            this.elements = elements2;
        }

        /* access modifiers changed from: package-private */
        public Object readResolve() {
            return ImmutableList.copyOf(this.elements);
        }
    }

    private void readObject(ObjectInputStream stream) throws InvalidObjectException {
        throw new InvalidObjectException("Use SerializedForm");
    }

    /* access modifiers changed from: package-private */
    public Object writeReplace() {
        return new SerializedForm(toArray());
    }

    public static <E> Builder<E> builder() {
        return new Builder<>();
    }

    public static final class Builder<E> extends ImmutableCollection.Builder<E> {
        private final ArrayList<E> contents = Lists.newArrayList();

        public Builder<E> add(Sets.CartesianSet<B>.Axis axis) {
            this.contents.add(Preconditions.checkNotNull(axis));
            return this;
        }

        public Builder<E> addAll(Iterable iterable) {
            if (iterable instanceof Collection) {
                this.contents.ensureCapacity(this.contents.size() + ((Collection) iterable).size());
            }
            super.addAll(iterable);
            return this;
        }

        public Builder<E> add(Object... objArr) {
            this.contents.ensureCapacity(this.contents.size() + objArr.length);
            super.add(objArr);
            return this;
        }

        public Builder<E> addAll(Iterator it) {
            super.addAll(it);
            return this;
        }

        public ImmutableList<E> build() {
            return ImmutableList.copyOf((Collection) this.contents);
        }
    }
}
