package com.google.common.collect;

import com.google.common.annotations.Beta;
import com.google.common.base.FinalizableReferenceQueue;
import com.google.common.base.FinalizableWeakReference;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import java.util.concurrent.ConcurrentMap;

@Beta
public final class Interners {
    private Interners() {
    }

    public static <E> Interner<E> newStrongInterner() {
        final ConcurrentMap<E, E> map = new MapMaker().makeMap();
        return new Interner<E>() {
            public E intern(E sample) {
                E canonical = map.putIfAbsent(Preconditions.checkNotNull(sample), sample);
                return canonical == null ? sample : canonical;
            }
        };
    }

    public static <E> Interner<E> newWeakInterner() {
        return new WeakInterner();
    }

    private static class WeakInterner<E> implements Interner<E> {
        /* access modifiers changed from: private */
        public static final FinalizableReferenceQueue frq = new FinalizableReferenceQueue();
        /* access modifiers changed from: private */
        public final ConcurrentMap<WeakInterner<E>.InternReference, WeakInterner<E>.InternReference> map;

        private WeakInterner() {
            this.map = new MapMaker().makeMap();
        }

        public E intern(final E sample) {
            E canonical;
            E canonical2;
            final int hashCode = sample.hashCode();
            WeakInterner<E>.InternReference existingRef = this.map.get(new Object() {
                public int hashCode() {
                    return hashCode;
                }

                public boolean equals(Object object) {
                    if (object.hashCode() != hashCode) {
                        return false;
                    }
                    return sample.equals(((InternReference) object).get());
                }
            });
            if (existingRef != null && (canonical2 = existingRef.get()) != null) {
                return canonical2;
            }
            WeakInterner<E>.InternReference newRef = new InternReference(sample, hashCode);
            do {
                WeakInterner<E>.InternReference sneakyRef = this.map.putIfAbsent(newRef, newRef);
                if (sneakyRef == null) {
                    return sample;
                }
                canonical = sneakyRef.get();
            } while (canonical == null);
            return canonical;
        }

        class InternReference extends FinalizableWeakReference<E> {
            final int hashCode;

            InternReference(E key, int hash) {
                super(key, WeakInterner.frq);
                this.hashCode = hash;
            }

            public void finalizeReferent() {
                WeakInterner.this.map.remove(this);
            }

            public E get() {
                E referent = super.get();
                if (referent == null) {
                    finalizeReferent();
                }
                return referent;
            }

            public int hashCode() {
                return this.hashCode;
            }

            public boolean equals(Object object) {
                if (object == this) {
                    return true;
                }
                if (!(object instanceof InternReference)) {
                    return object.equals(this);
                }
                InternReference that = (InternReference) object;
                if (that.hashCode != this.hashCode) {
                    return false;
                }
                E referent = super.get();
                return referent != null && referent.equals(that.get());
            }
        }
    }

    public static <E> Function<E, E> asFunction(Interner<E> interner) {
        return new InternerFunction((Interner) Preconditions.checkNotNull(interner));
    }

    private static class InternerFunction<E> implements Function<E, E> {
        private final Interner<E> interner;

        public InternerFunction(Interner<E> interner2) {
            this.interner = interner2;
        }

        public E apply(E input) {
            return this.interner.intern(input);
        }

        public int hashCode() {
            return this.interner.hashCode();
        }

        public boolean equals(Object other) {
            if (other instanceof InternerFunction) {
                return this.interner.equals(((InternerFunction) other).interner);
            }
            return false;
        }
    }
}
