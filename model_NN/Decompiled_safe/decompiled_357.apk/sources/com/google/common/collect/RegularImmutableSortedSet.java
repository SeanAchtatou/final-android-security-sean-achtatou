package com.google.common.collect;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Preconditions;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

@GwtCompatible(emulated = true, serializable = true)
final class RegularImmutableSortedSet<E> extends ImmutableSortedSet<E> {
    private final transient ImmutableList<E> elements;

    RegularImmutableSortedSet(ImmutableList<E> elements2, Comparator<? super E> comparator) {
        super(comparator);
        this.elements = elements2;
        Preconditions.checkArgument(!elements2.isEmpty());
    }

    public UnmodifiableIterator<E> iterator() {
        return this.elements.iterator();
    }

    public boolean isEmpty() {
        return false;
    }

    public int size() {
        return this.elements.size();
    }

    public boolean contains(Object o) {
        if (o == null) {
            return false;
        }
        try {
            return binarySearch(o) >= 0;
        } catch (ClassCastException e) {
            return false;
        }
    }

    public boolean containsAll(Collection<?> targets) {
        if (!hasSameComparator(targets, comparator()) || targets.size() <= 1) {
            return super.containsAll(targets);
        }
        Iterator<E> thisIterator = iterator();
        Iterator<?> thatIterator = targets.iterator();
        Object target = thatIterator.next();
        while (thisIterator.hasNext()) {
            try {
                int cmp = unsafeCompare(thisIterator.next(), target);
                if (cmp == 0) {
                    if (!thatIterator.hasNext()) {
                        return true;
                    }
                    target = thatIterator.next();
                } else if (cmp > 0) {
                    return false;
                }
            } catch (NullPointerException e) {
                return false;
            } catch (ClassCastException e2) {
                return false;
            }
        }
        return false;
    }

    private int binarySearch(Object key) {
        return Collections.binarySearch(this.elements, key, this.comparator);
    }

    /* access modifiers changed from: package-private */
    public boolean isPartialView() {
        return this.elements.isPartialView();
    }

    public Object[] toArray() {
        return this.elements.toArray();
    }

    public <T> T[] toArray(T[] array) {
        return this.elements.toArray(array);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0032 A[Catch:{ ClassCastException -> 0x0046, NoSuchElementException -> 0x004a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(@javax.annotation.Nullable java.lang.Object r12) {
        /*
            r11 = this;
            r10 = 1
            r9 = 0
            if (r12 != r11) goto L_0x0006
            r7 = r10
        L_0x0005:
            return r7
        L_0x0006:
            boolean r7 = r12 instanceof java.util.Set
            if (r7 != 0) goto L_0x000c
            r7 = r9
            goto L_0x0005
        L_0x000c:
            r0 = r12
            java.util.Set r0 = (java.util.Set) r0
            r6 = r0
            int r7 = r11.size()
            int r8 = r6.size()
            if (r7 == r8) goto L_0x001c
            r7 = r9
            goto L_0x0005
        L_0x001c:
            java.util.Comparator r7 = r11.comparator
            boolean r7 = hasSameComparator(r6, r7)
            if (r7 == 0) goto L_0x004e
            java.util.Iterator r5 = r6.iterator()
            com.google.common.collect.UnmodifiableIterator r3 = r11.iterator()     // Catch:{ ClassCastException -> 0x0046, NoSuchElementException -> 0x004a }
        L_0x002c:
            boolean r7 = r3.hasNext()     // Catch:{ ClassCastException -> 0x0046, NoSuchElementException -> 0x004a }
            if (r7 == 0) goto L_0x0044
            java.lang.Object r2 = r3.next()     // Catch:{ ClassCastException -> 0x0046, NoSuchElementException -> 0x004a }
            java.lang.Object r4 = r5.next()     // Catch:{ ClassCastException -> 0x0046, NoSuchElementException -> 0x004a }
            if (r4 == 0) goto L_0x0042
            int r7 = r11.unsafeCompare(r2, r4)     // Catch:{ ClassCastException -> 0x0046, NoSuchElementException -> 0x004a }
            if (r7 == 0) goto L_0x002c
        L_0x0042:
            r7 = r9
            goto L_0x0005
        L_0x0044:
            r7 = r10
            goto L_0x0005
        L_0x0046:
            r7 = move-exception
            r1 = r7
            r7 = r9
            goto L_0x0005
        L_0x004a:
            r7 = move-exception
            r1 = r7
            r7 = r9
            goto L_0x0005
        L_0x004e:
            boolean r7 = r11.containsAll(r6)
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.RegularImmutableSortedSet.equals(java.lang.Object):boolean");
    }

    public E first() {
        return this.elements.get(0);
    }

    public E last() {
        return this.elements.get(size() - 1);
    }

    /* access modifiers changed from: package-private */
    public ImmutableSortedSet<E> headSetImpl(E toElement) {
        return createSubset(0, findSubsetIndex(toElement));
    }

    /* access modifiers changed from: package-private */
    public ImmutableSortedSet<E> subSetImpl(E fromElement, E toElement) {
        return createSubset(findSubsetIndex(fromElement), findSubsetIndex(toElement));
    }

    /* access modifiers changed from: package-private */
    public ImmutableSortedSet<E> tailSetImpl(E fromElement) {
        return createSubset(findSubsetIndex(fromElement), size());
    }

    private int findSubsetIndex(E element) {
        int index = binarySearch(element);
        return index >= 0 ? index : (-index) - 1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList<E>
     arg types: [int, int]
     candidates:
      com.google.common.collect.ImmutableList.subList(int, int):java.util.List
      ClspMth{java.util.List.subList(int, int):java.util.List<E>}
      com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList<E> */
    private ImmutableSortedSet<E> createSubset(int newFromIndex, int newToIndex) {
        if (newFromIndex < newToIndex) {
            return new RegularImmutableSortedSet(this.elements.subList(newFromIndex, newToIndex), this.comparator);
        }
        return emptySet(this.comparator);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: E
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    int indexOf(java.lang.Object r5) {
        /*
            r4 = this;
            r3 = -1
            if (r5 != 0) goto L_0x0005
            r2 = r3
        L_0x0004:
            return r2
        L_0x0005:
            int r1 = r4.binarySearch(r5)     // Catch:{ ClassCastException -> 0x0019 }
            if (r1 < 0) goto L_0x001c
            com.google.common.collect.ImmutableList<E> r2 = r4.elements
            java.lang.Object r2 = r2.get(r1)
            boolean r2 = r2.equals(r5)
            if (r2 == 0) goto L_0x001c
            r2 = r1
            goto L_0x0004
        L_0x0019:
            r0 = move-exception
            r2 = r3
            goto L_0x0004
        L_0x001c:
            r2 = r3
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.RegularImmutableSortedSet.indexOf(java.lang.Object):int");
    }

    /* access modifiers changed from: package-private */
    public ImmutableList<E> createAsList() {
        return new ImmutableSortedAsList(this, this.elements);
    }
}
