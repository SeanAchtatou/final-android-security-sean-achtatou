package com.google.common.collect;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableCollection;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import javax.annotation.Nullable;

@GwtCompatible(emulated = true, serializable = true)
public abstract class ImmutableSet<E> extends ImmutableCollection<E> implements Set<E> {
    static final int CUTOFF = 536870912;
    static final int MAX_TABLE_SIZE = 1073741824;

    public abstract UnmodifiableIterator<E> iterator();

    public static <E> ImmutableSet<E> of() {
        return EmptyImmutableSet.INSTANCE;
    }

    public static <E> ImmutableSet<E> of(E element) {
        return new SingletonImmutableSet(element);
    }

    public static <E> ImmutableSet<E> of(E e1, E e2) {
        return construct(e1, e2);
    }

    public static <E> ImmutableSet<E> of(E e1, E e2, E e3) {
        return construct(e1, e2, e3);
    }

    public static <E> ImmutableSet<E> of(E e1, E e2, E e3, E e4) {
        return construct(e1, e2, e3, e4);
    }

    public static <E> ImmutableSet<E> of(E e1, E e2, E e3, E e4, E e5) {
        return construct(e1, e2, e3, e4, e5);
    }

    public static <E> ImmutableSet<E> of(E e1, E e2, E e3, E e4, E e5, E e6, E... others) {
        Object[] elements = new Object[(others.length + 6)];
        elements[0] = e1;
        elements[1] = e2;
        elements[2] = e3;
        elements[3] = e4;
        elements[4] = e5;
        elements[5] = e6;
        for (int i = 6; i < elements.length; i++) {
            elements[i] = others[i - 6];
        }
        return construct(elements);
    }

    /* JADX INFO: Multiple debug info for r2v2 int: [D('hashCode' int), D('uniqueElementsList' java.util.ArrayList<java.lang.Object>)] */
    /* JADX INFO: Multiple debug info for r11v3 com.google.common.collect.RegularImmutableSet: [D('uniqueElements' java.lang.Object[]), D('element' E)] */
    /* JADX INFO: Multiple debug info for r11v4 com.google.common.collect.ImmutableSet<E>: [D('uniqueElements' java.lang.Object[]), D('element' E)] */
    /* JADX INFO: Multiple debug info for r11v5 java.lang.Object: [D('uniqueElements' java.lang.Object[]), D('element' E)] */
    /* JADX INFO: Multiple debug info for r4v2 int: [D('j' int), D('index' int)] */
    private static <E> ImmutableSet<E> construct(Object... uniqueElements) {
        ArrayList<Object> uniqueElementsList;
        int hashCode;
        int tableSize = chooseTableSize(uniqueElements.length);
        Object[] table = new Object[tableSize];
        int mask = tableSize - 1;
        ArrayList<Object> uniqueElementsList2 = null;
        int hashCode2 = 0;
        int hash = 0;
        while (true) {
            int i = hash;
            uniqueElementsList = uniqueElementsList2;
            hashCode = hashCode2;
            if (i >= uniqueElements.length) {
                break;
            }
            Object element = uniqueElements[i];
            int hash2 = element.hashCode();
            int j = Hashing.smear(hash2);
            while (true) {
                int j2 = j;
                int index = j2 & mask;
                Object value = table[index];
                if (value == null) {
                    if (uniqueElementsList != null) {
                        uniqueElementsList.add(element);
                    }
                    table[index] = element;
                    hashCode2 = hashCode + hash2;
                    uniqueElementsList2 = uniqueElementsList;
                } else if (value.equals(element) == 0) {
                    j = j2 + 1;
                } else if (uniqueElementsList == null) {
                    ArrayList<Object> uniqueElementsList3 = new ArrayList<>(uniqueElements.length);
                    for (int k = 0; k < i; k++) {
                        uniqueElementsList3.add(uniqueElements[k]);
                    }
                    hashCode2 = hashCode;
                    uniqueElementsList2 = uniqueElementsList3;
                } else {
                    hashCode2 = hashCode;
                    uniqueElementsList2 = uniqueElementsList;
                }
            }
            hash = i + 1;
        }
        if (uniqueElementsList != null) {
            uniqueElements = uniqueElementsList.toArray();
        }
        if (uniqueElements.length == 1) {
            return new SingletonImmutableSet(uniqueElements[0], hashCode);
        }
        if (tableSize > chooseTableSize(uniqueElements.length) * 2) {
            return construct(uniqueElements);
        }
        return new RegularImmutableSet(uniqueElements, hashCode, table, mask);
    }

    static int chooseTableSize(int setSize) {
        if (setSize < CUTOFF) {
            return Integer.highestOneBit(setSize) << 2;
        }
        Preconditions.checkArgument(setSize < MAX_TABLE_SIZE, "collection too large");
        return MAX_TABLE_SIZE;
    }

    @Deprecated
    public static <E> ImmutableSet<E> of(E[] elements) {
        return copyOf(elements);
    }

    public static <E> ImmutableSet<E> copyOf(Object[] objArr) {
        switch (objArr.length) {
            case 0:
                return of();
            case 1:
                return of(objArr[0]);
            default:
                return construct((Object[]) objArr.clone());
        }
    }

    public static <E> ImmutableSet<E> copyOf(Iterable iterable) {
        return iterable instanceof Collection ? copyOf(Collections2.cast(iterable)) : copyOf(iterable.iterator());
    }

    public static <E> ImmutableSet<E> copyOf(Iterator it) {
        return copyFromCollection(Lists.newArrayList(it));
    }

    public static <E> ImmutableSet<E> copyOf(Collection collection) {
        if ((collection instanceof ImmutableSet) && !(collection instanceof ImmutableSortedSet)) {
            ImmutableSet<E> set = (ImmutableSet) collection;
            if (!set.isPartialView()) {
                return set;
            }
        }
        return copyFromCollection(collection);
    }

    private static <E> ImmutableSet<E> copyFromCollection(Collection<? extends E> collection) {
        E[] elements = collection.toArray();
        switch (elements.length) {
            case 0:
                return of();
            case 1:
                return of(elements[0]);
            default:
                return construct(elements);
        }
    }

    ImmutableSet() {
    }

    /* access modifiers changed from: package-private */
    public boolean isHashCodeFast() {
        return false;
    }

    public boolean equals(@Nullable Object object) {
        if (object == this) {
            return true;
        }
        if (!(object instanceof ImmutableSet) || !isHashCodeFast() || !((ImmutableSet) object).isHashCodeFast() || hashCode() == object.hashCode()) {
            return Sets.equalsImpl(this, object);
        }
        return false;
    }

    public int hashCode() {
        return Sets.hashCodeImpl(this);
    }

    static abstract class ArrayImmutableSet<E> extends ImmutableSet<E> {
        final transient Object[] elements;

        ArrayImmutableSet(Object[] elements2) {
            this.elements = elements2;
        }

        public int size() {
            return this.elements.length;
        }

        public boolean isEmpty() {
            return false;
        }

        public UnmodifiableIterator<E> iterator() {
            return Iterators.forArray(this.elements);
        }

        public Object[] toArray() {
            Object[] array = new Object[size()];
            System.arraycopy(this.elements, 0, array, 0, size());
            return array;
        }

        public <T> T[] toArray(T[] array) {
            int size = size();
            if (array.length < size) {
                array = ObjectArrays.newArray(array, size);
            } else if (array.length > size) {
                array[size] = null;
            }
            System.arraycopy(this.elements, 0, array, 0, size);
            return array;
        }

        public boolean containsAll(Collection<?> targets) {
            if (targets == this) {
                return true;
            }
            if (!(targets instanceof ArrayImmutableSet)) {
                return ImmutableSet.super.containsAll(targets);
            }
            if (targets.size() > size()) {
                return false;
            }
            for (Object target : ((ArrayImmutableSet) targets).elements) {
                if (!contains(target)) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        public boolean isPartialView() {
            return false;
        }

        /* access modifiers changed from: package-private */
        public ImmutableList<E> createAsList() {
            return new ImmutableAsList(this.elements, this);
        }
    }

    static abstract class TransformedImmutableSet<D, E> extends ImmutableSet<E> {
        final int hashCode;
        final D[] source;

        /* access modifiers changed from: package-private */
        public abstract E transform(D d);

        TransformedImmutableSet(D[] source2, int hashCode2) {
            this.source = source2;
            this.hashCode = hashCode2;
        }

        public int size() {
            return this.source.length;
        }

        public boolean isEmpty() {
            return false;
        }

        public UnmodifiableIterator<E> iterator() {
            return new AbstractIndexedListIterator<E>(this.source.length) {
                /* access modifiers changed from: protected */
                public E get(int index) {
                    return TransformedImmutableSet.this.transform(TransformedImmutableSet.this.source[index]);
                }
            };
        }

        public Object[] toArray() {
            return toArray(new Object[size()]);
        }

        public <T> T[] toArray(T[] array) {
            int size = size();
            if (array.length < size) {
                array = ObjectArrays.newArray(array, size);
            } else if (array.length > size) {
                array[size] = null;
            }
            Object[] objectArray = array;
            for (int i = 0; i < this.source.length; i++) {
                objectArray[i] = transform(this.source[i]);
            }
            return array;
        }

        public final int hashCode() {
            return this.hashCode;
        }

        /* access modifiers changed from: package-private */
        public boolean isHashCodeFast() {
            return true;
        }
    }

    private static class SerializedForm implements Serializable {
        private static final long serialVersionUID = 0;
        final Object[] elements;

        SerializedForm(Object[] elements2) {
            this.elements = elements2;
        }

        /* access modifiers changed from: package-private */
        public Object readResolve() {
            return ImmutableSet.copyOf(this.elements);
        }
    }

    /* access modifiers changed from: package-private */
    public Object writeReplace() {
        return new SerializedForm(toArray());
    }

    public static <E> Builder<E> builder() {
        return new Builder<>();
    }

    public static class Builder<E> extends ImmutableCollection.Builder<E> {
        final ArrayList<E> contents = Lists.newArrayList();

        public Builder<E> add(E element) {
            this.contents.add(Preconditions.checkNotNull(element));
            return this;
        }

        public Builder<E> add(E... elements) {
            this.contents.ensureCapacity(this.contents.size() + elements.length);
            super.add((Object[]) elements);
            return this;
        }

        public Builder<E> addAll(Iterable iterable) {
            if (iterable instanceof Collection) {
                this.contents.ensureCapacity(this.contents.size() + ((Collection) iterable).size());
            }
            super.addAll(iterable);
            return this;
        }

        public Builder<E> addAll(Iterator it) {
            super.addAll(it);
            return this;
        }

        public ImmutableSet<E> build() {
            return ImmutableSet.copyOf((Collection) this.contents);
        }
    }
}
