package com.google.common.collect;

import com.google.common.annotations.GwtCompatible;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@GwtCompatible(emulated = true, serializable = true)
final class ExplicitOrderedImmutableSortedSet<E> extends ImmutableSortedSet<E> {
    private final Object[] elements;
    private final int fromIndex;
    private final int toIndex;

    static <E> ImmutableSortedSet<E> create(List<E> list) {
        ExplicitOrdering<E> ordering = new ExplicitOrdering<>(list);
        if (ordering.rankMap.isEmpty()) {
            return emptySet(ordering);
        }
        return new ExplicitOrderedImmutableSortedSet(ordering.rankMap.keySet().toArray(), ordering);
    }

    ExplicitOrderedImmutableSortedSet(Object[] elements2, Comparator<? super E> comparator) {
        this(elements2, comparator, 0, elements2.length);
    }

    ExplicitOrderedImmutableSortedSet(Object[] elements2, Comparator<? super E> comparator, int fromIndex2, int toIndex2) {
        super(comparator);
        this.elements = elements2;
        this.fromIndex = fromIndex2;
        this.toIndex = toIndex2;
    }

    private ImmutableMap<E, Integer> rankMap() {
        return ((ExplicitOrdering) comparator()).rankMap;
    }

    public UnmodifiableIterator<E> iterator() {
        return Iterators.forArray(this.elements, this.fromIndex, size());
    }

    public boolean isEmpty() {
        return false;
    }

    public int size() {
        return this.toIndex - this.fromIndex;
    }

    public boolean contains(Object o) {
        Integer index = (Integer) rankMap().get(o);
        return index != null && index.intValue() >= this.fromIndex && index.intValue() < this.toIndex;
    }

    /* access modifiers changed from: package-private */
    public boolean isPartialView() {
        return (this.fromIndex == 0 && this.toIndex == this.elements.length) ? false : true;
    }

    public Object[] toArray() {
        Object[] array = new Object[size()];
        Platform.unsafeArrayCopy(this.elements, this.fromIndex, array, 0, size());
        return array;
    }

    public <T> T[] toArray(T[] array) {
        int size = size();
        if (array.length < size) {
            array = ObjectArrays.newArray(array, size);
        } else if (array.length > size) {
            array[size] = null;
        }
        Platform.unsafeArrayCopy(this.elements, this.fromIndex, array, 0, size);
        return array;
    }

    public int hashCode() {
        int hash = 0;
        for (int i = this.fromIndex; i < this.toIndex; i++) {
            hash += this.elements[i].hashCode();
        }
        return hash;
    }

    public E first() {
        return this.elements[this.fromIndex];
    }

    public E last() {
        return this.elements[this.toIndex - 1];
    }

    /* access modifiers changed from: package-private */
    public ImmutableSortedSet<E> headSetImpl(E toElement) {
        return createSubset(this.fromIndex, findSubsetIndex(toElement));
    }

    /* access modifiers changed from: package-private */
    public ImmutableSortedSet<E> subSetImpl(E fromElement, E toElement) {
        return createSubset(findSubsetIndex(fromElement), findSubsetIndex(toElement));
    }

    /* access modifiers changed from: package-private */
    public ImmutableSortedSet<E> tailSetImpl(E fromElement) {
        return createSubset(findSubsetIndex(fromElement), this.toIndex);
    }

    private int findSubsetIndex(E element) {
        Integer index = (Integer) rankMap().get(element);
        if (index == null) {
            throw new ClassCastException();
        } else if (index.intValue() <= this.fromIndex) {
            return this.fromIndex;
        } else {
            if (index.intValue() >= this.toIndex) {
                return this.toIndex;
            }
            return index.intValue();
        }
    }

    private ImmutableSortedSet<E> createSubset(int newFromIndex, int newToIndex) {
        if (newFromIndex < newToIndex) {
            return new ExplicitOrderedImmutableSortedSet(this.elements, this.comparator, newFromIndex, newToIndex);
        }
        return emptySet(this.comparator);
    }

    /* access modifiers changed from: package-private */
    public int indexOf(Object target) {
        Integer index = (Integer) rankMap().get(target);
        if (index == null || index.intValue() < this.fromIndex || index.intValue() >= this.toIndex) {
            return -1;
        }
        return index.intValue() - this.fromIndex;
    }

    /* access modifiers changed from: package-private */
    public ImmutableList<E> createAsList() {
        return new ImmutableSortedAsList(this, new RegularImmutableList(this.elements, this.fromIndex, size()));
    }

    private static class SerializedForm<E> implements Serializable {
        private static final long serialVersionUID = 0;
        final Object[] elements;

        public SerializedForm(Object[] elements2) {
            this.elements = elements2;
        }

        /* access modifiers changed from: package-private */
        public Object readResolve() {
            return ImmutableSortedSet.withExplicitOrder(Arrays.asList(this.elements));
        }
    }

    private void readObject(ObjectInputStream stream) throws InvalidObjectException {
        throw new InvalidObjectException("Use SerializedForm");
    }

    /* access modifiers changed from: package-private */
    public Object writeReplace() {
        return new SerializedForm(toArray());
    }
}
