package com.google.common.collect;

import com.google.common.base.Equivalence;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.CustomConcurrentHashMap;
import com.google.common.collect.MapMaker;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicReferenceArray;
import javax.annotation.Nullable;

class ComputingConcurrentHashMap<K, V> extends CustomConcurrentHashMap<K, V> implements MapMaker.Cache<K, V> {
    private static final long serialVersionUID = 2;
    final Function<? super K, ? extends V> computingFunction;

    ComputingConcurrentHashMap(MapMaker builder, Function<? super K, ? extends V> computingFunction2) {
        super(builder);
        this.computingFunction = (Function) Preconditions.checkNotNull(computingFunction2);
    }

    public ConcurrentMap<K, V> asMap() {
        return this;
    }

    /* access modifiers changed from: package-private */
    public CustomConcurrentHashMap<K, V>.Segment createSegment(int initialCapacity, int maxSegmentSize) {
        return new ComputingSegment(initialCapacity, maxSegmentSize);
    }

    /* access modifiers changed from: package-private */
    public ComputingConcurrentHashMap<K, V>.ComputingSegment segmentFor(int hash) {
        return (ComputingSegment) super.segmentFor(hash);
    }

    public V apply(K key) {
        int hash = hash(key);
        return segmentFor(hash).compute(key, hash);
    }

    class ComputingSegment extends CustomConcurrentHashMap.Segment {
        ComputingSegment(int initialCapacity, int maxSegmentSize) {
            super(initialCapacity, maxSegmentSize);
        }

        /* access modifiers changed from: package-private */
        public V compute(K key, int hash) {
            V value;
            while (true) {
                CustomConcurrentHashMap.ReferenceEntry<K, V> entry = getEntry(key, hash);
                if (entry == null) {
                    boolean created = false;
                    lock();
                    try {
                        if (ComputingConcurrentHashMap.this.expires()) {
                            expireEntries();
                        }
                        entry = getEntry(key, hash);
                        if (entry == null) {
                            created = true;
                            int newCount = this.count + 1;
                            if (ComputingConcurrentHashMap.this.evictsBySize() && newCount > this.maxSegmentSize) {
                                evictEntry();
                                newCount = this.count + 1;
                            } else if (newCount > this.threshold) {
                                expand();
                            }
                            AtomicReferenceArray<CustomConcurrentHashMap.ReferenceEntry<K, V>> table = this.table;
                            int index = hash & (table.length() - 1);
                            this.modCount++;
                            entry = ComputingConcurrentHashMap.this.entryFactory.newEntry(ComputingConcurrentHashMap.this, key, hash, table.get(index));
                            table.set(index, entry);
                            this.count = newCount;
                        }
                        if (created) {
                            boolean success = false;
                            try {
                                value = compute(key, entry);
                                Preconditions.checkNotNull(value, "compute() returned null unexpectedly");
                                success = true;
                            } finally {
                                if (!success) {
                                    removeEntry(entry, hash);
                                }
                            }
                        }
                    } finally {
                        unlock();
                    }
                }
                boolean interrupted = false;
                while (true) {
                    try {
                        value = ComputingConcurrentHashMap.this.waitForValue(entry);
                        break;
                    } catch (InterruptedException e) {
                        interrupted = true;
                    } catch (Throwable th) {
                        if (interrupted) {
                            Thread.currentThread().interrupt();
                        }
                        throw th;
                    }
                }
                if (value == null) {
                    removeEntry(entry, hash);
                    if (interrupted) {
                        Thread.currentThread().interrupt();
                    }
                } else if (interrupted) {
                    Thread.currentThread().interrupt();
                }
            }
            return value;
        }

        /* access modifiers changed from: package-private */
        public V compute(K key, CustomConcurrentHashMap.ReferenceEntry<K, V> entry) {
            try {
                V value = ComputingConcurrentHashMap.this.computingFunction.apply(key);
                if (value == null) {
                    String message = ComputingConcurrentHashMap.this.computingFunction + " returned null for key " + ((Object) key) + ".";
                    ComputingConcurrentHashMap.this.setValueReference(entry, new NullPointerExceptionReference(message));
                    throw new NullPointerException(message);
                }
                setComputedValue(entry, value);
                return value;
            } catch (ComputationException e) {
                ComputationException e2 = e;
                ComputingConcurrentHashMap.this.setValueReference(entry, new ComputationExceptionReference(e2.getCause()));
                throw e2;
            } catch (Throwable th) {
                Throwable t = th;
                ComputingConcurrentHashMap.this.setValueReference(entry, new ComputationExceptionReference(t));
                throw new ComputationException(t);
            }
        }

        /* access modifiers changed from: package-private */
        public void setComputedValue(CustomConcurrentHashMap.ReferenceEntry<K, V> entry, V value) {
            if (ComputingConcurrentHashMap.this.evictsBySize() || ComputingConcurrentHashMap.this.expires()) {
                lock();
                try {
                    if (ComputingConcurrentHashMap.this.evictsBySize() || ComputingConcurrentHashMap.this.expires()) {
                        recordWrite(getEntry(entry.getKey(), entry.getHash()));
                    }
                } finally {
                    unlock();
                }
            }
            synchronized (entry) {
                if (entry.getValueReference().get() == null) {
                    ComputingConcurrentHashMap.this.setValueReference(entry, ComputingConcurrentHashMap.this.valueStrength.referenceValue(entry, value));
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void setValueReference(CustomConcurrentHashMap.ReferenceEntry<K, V> entry, CustomConcurrentHashMap.ValueReference<K, V> valueReference) {
        synchronized (entry) {
            boolean notifyOthers = entry.getValueReference() == UNSET;
            entry.setValueReference(valueReference);
            if (notifyOthers) {
                entry.notifyAll();
            }
        }
    }

    public V waitForValue(CustomConcurrentHashMap.ReferenceEntry<K, V> entry) throws InterruptedException {
        CustomConcurrentHashMap.ValueReference<K, V> valueReference = entry.getValueReference();
        if (valueReference == UNSET) {
            synchronized (entry) {
                while (true) {
                    valueReference = entry.getValueReference();
                    if (valueReference != UNSET) {
                        break;
                    }
                    entry.wait();
                }
            }
        }
        return valueReference.waitForValue();
    }

    private static class NullPointerExceptionReference<K, V> implements CustomConcurrentHashMap.ValueReference<K, V> {
        final String message;

        NullPointerExceptionReference(String message2) {
            this.message = message2;
        }

        public V get() {
            return null;
        }

        public CustomConcurrentHashMap.ValueReference<K, V> copyFor(CustomConcurrentHashMap.ReferenceEntry<K, V> referenceEntry) {
            return this;
        }

        public V waitForValue() {
            throw new NullPointerException(this.message);
        }

        public void clear() {
        }
    }

    private static class ComputationExceptionReference<K, V> implements CustomConcurrentHashMap.ValueReference<K, V> {
        final Throwable t;

        ComputationExceptionReference(Throwable t2) {
            this.t = t2;
        }

        public V get() {
            return null;
        }

        public CustomConcurrentHashMap.ValueReference<K, V> copyFor(CustomConcurrentHashMap.ReferenceEntry<K, V> referenceEntry) {
            return this;
        }

        public V waitForValue() {
            throw new AsynchronousComputationException(this.t);
        }

        public void clear() {
        }
    }

    /* access modifiers changed from: package-private */
    public CustomConcurrentHashMap.ReferenceEntry<K, V> copyEntry(CustomConcurrentHashMap.ReferenceEntry<K, V> original, CustomConcurrentHashMap.ReferenceEntry<K, V> newNext) {
        CustomConcurrentHashMap.ReferenceEntry<K, V> newEntry = this.entryFactory.copyEntry(this, original, newNext);
        CustomConcurrentHashMap.ValueReference<K, V> valueReference = original.getValueReference();
        if (valueReference == UNSET) {
            newEntry.setValueReference(new FutureValueReference(original, newEntry));
        } else {
            newEntry.setValueReference(valueReference.copyFor(newEntry));
        }
        return newEntry;
    }

    private class FutureValueReference implements CustomConcurrentHashMap.ValueReference<K, V> {
        final CustomConcurrentHashMap.ReferenceEntry<K, V> newEntry;
        final CustomConcurrentHashMap.ReferenceEntry<K, V> original;

        FutureValueReference(CustomConcurrentHashMap.ReferenceEntry<K, V> original2, CustomConcurrentHashMap.ReferenceEntry<K, V> newEntry2) {
            this.original = original2;
            this.newEntry = newEntry2;
        }

        public V get() {
            boolean success = false;
            try {
                success = true;
                return this.original.getValueReference().get();
            } finally {
                if (!success) {
                    removeEntry();
                }
            }
        }

        public CustomConcurrentHashMap.ValueReference<K, V> copyFor(CustomConcurrentHashMap.ReferenceEntry<K, V> entry) {
            return new FutureValueReference(this.original, entry);
        }

        public V waitForValue() throws InterruptedException {
            boolean success = false;
            try {
                success = true;
                return ComputingConcurrentHashMap.this.waitForValue(this.original);
            } finally {
                if (!success) {
                    removeEntry();
                }
            }
        }

        public void clear() {
            this.original.getValueReference().clear();
        }

        /* access modifiers changed from: package-private */
        public void removeEntry() {
            ComputingConcurrentHashMap.this.removeEntry(this.newEntry);
        }
    }

    /* access modifiers changed from: package-private */
    public Object writeReplace() {
        return new ComputingSerializationProxy(this.keyStrength, this.valueStrength, this.keyEquivalence, this.valueEquivalence, this.expireAfterWriteNanos, this.expireAfterAccessNanos, this.maximumSize, this.concurrencyLevel, this.evictionListener, this, this.computingFunction);
    }

    static class ComputingSerializationProxy<K, V> extends CustomConcurrentHashMap.AbstractSerializationProxy<K, V> {
        private static final long serialVersionUID = 2;
        transient MapMaker.Cache<K, V> cache;
        final Function<? super K, ? extends V> computingFunction;

        ComputingSerializationProxy(CustomConcurrentHashMap.Strength keyStrength, CustomConcurrentHashMap.Strength valueStrength, Equivalence<Object> keyEquivalence, Equivalence<Object> valueEquivalence, long expireAfterWriteNanos, long expireAfterAccessNanos, int maximumSize, int concurrencyLevel, MapEvictionListener<? super K, ? super V> evictionListener, ConcurrentMap<K, V> delegate, Function<? super K, ? extends V> computingFunction2) {
            super(keyStrength, valueStrength, keyEquivalence, valueEquivalence, expireAfterWriteNanos, expireAfterAccessNanos, maximumSize, concurrencyLevel, evictionListener, delegate);
            this.computingFunction = computingFunction2;
        }

        private void writeObject(ObjectOutputStream out) throws IOException {
            out.defaultWriteObject();
            writeMapTo(out);
        }

        private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
            in.defaultReadObject();
            this.cache = readMapMaker(in).makeCache(this.computingFunction);
            this.delegate = this.cache.asMap();
            readEntries(in);
        }

        /* access modifiers changed from: package-private */
        public Object readResolve() {
            return this.cache;
        }

        public ConcurrentMap<K, V> asMap() {
            return this.delegate;
        }

        public V apply(@Nullable K from) {
            return this.cache.apply(from);
        }
    }
}
