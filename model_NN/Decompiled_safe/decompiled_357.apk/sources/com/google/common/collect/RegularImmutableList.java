package com.google.common.collect;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Preconditions;
import java.util.List;
import javax.annotation.Nullable;

@GwtCompatible(emulated = true, serializable = true)
class RegularImmutableList<E> extends ImmutableList<E> {
    /* access modifiers changed from: private */
    public final transient Object[] array;
    /* access modifiers changed from: private */
    public final transient int offset;
    private final transient int size;

    RegularImmutableList(Object[] array2, int offset2, int size2) {
        this.offset = offset2;
        this.size = size2;
        this.array = array2;
    }

    RegularImmutableList(Object[] array2) {
        this(array2, 0, array2.length);
    }

    public int size() {
        return this.size;
    }

    public boolean isEmpty() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean isPartialView() {
        return (this.offset == 0 && this.size == this.array.length) ? false : true;
    }

    public boolean contains(Object target) {
        return indexOf(target) != -1;
    }

    public UnmodifiableIterator<E> iterator() {
        return Iterators.forArray(this.array, this.offset, this.size);
    }

    public Object[] toArray() {
        Object[] newArray = new Object[size()];
        System.arraycopy(this.array, this.offset, newArray, 0, this.size);
        return newArray;
    }

    public <T> T[] toArray(T[] other) {
        if (other.length < this.size) {
            other = ObjectArrays.newArray(other, this.size);
        } else if (other.length > this.size) {
            other[this.size] = null;
        }
        System.arraycopy(this.array, this.offset, other, 0, this.size);
        return other;
    }

    public E get(int index) {
        Preconditions.checkElementIndex(index, this.size);
        return this.array[this.offset + index];
    }

    public int indexOf(Object target) {
        if (target != null) {
            for (int i = this.offset; i < this.offset + this.size; i++) {
                if (this.array[i].equals(target)) {
                    return i - this.offset;
                }
            }
        }
        return -1;
    }

    public int lastIndexOf(Object target) {
        if (target != null) {
            for (int i = (this.offset + this.size) - 1; i >= this.offset; i--) {
                if (this.array[i].equals(target)) {
                    return i - this.offset;
                }
            }
        }
        return -1;
    }

    public ImmutableList<E> subList(int fromIndex, int toIndex) {
        Preconditions.checkPositionIndexes(fromIndex, toIndex, this.size);
        return fromIndex == toIndex ? ImmutableList.of() : new RegularImmutableList(this.array, this.offset + fromIndex, toIndex - fromIndex);
    }

    public UnmodifiableListIterator<E> listIterator(int start) {
        return new AbstractIndexedListIterator<E>(this.size, start) {
            /* access modifiers changed from: protected */
            public E get(int index) {
                return RegularImmutableList.this.array[RegularImmutableList.this.offset + index];
            }
        };
    }

    public boolean equals(@Nullable Object object) {
        if (object == this) {
            return true;
        }
        if (!(object instanceof List)) {
            return false;
        }
        List<Object> list = (List) object;
        if (size() != list.size()) {
            return false;
        }
        int index = this.offset;
        if (object instanceof RegularImmutableList) {
            RegularImmutableList<?> other = (RegularImmutableList) object;
            int i = other.offset;
            while (i < other.offset + other.size) {
                int index2 = index + 1;
                if (!this.array[index].equals(other.array[i])) {
                    return false;
                }
                i++;
                index = index2;
            }
        } else {
            for (Object element : list) {
                int index3 = index + 1;
                if (!this.array[index].equals(element)) {
                    return false;
                }
                index = index3;
            }
        }
        return true;
    }

    public int hashCode() {
        int hashCode = 1;
        for (int i = this.offset; i < this.offset + this.size; i++) {
            hashCode = (hashCode * 31) + this.array[i].hashCode();
        }
        return hashCode;
    }

    public String toString() {
        StringBuilder sb = Collections2.newStringBuilderForCollection(size()).append('[').append(this.array[this.offset]);
        for (int i = this.offset + 1; i < this.offset + this.size; i++) {
            sb.append(", ").append(this.array[i]);
        }
        return sb.append(']').toString();
    }

    /* access modifiers changed from: package-private */
    public int offset() {
        return this.offset;
    }

    /* access modifiers changed from: package-private */
    public Object[] array() {
        return this.array;
    }
}
