package com.google.common.collect;

import com.google.common.annotations.GwtCompatible;
import com.google.common.annotations.GwtIncompatible;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.Nullable;

@GwtCompatible(emulated = true)
public final class TreeMultiset<E> extends AbstractMapBasedMultiset<E> {
    @GwtIncompatible("not needed in emulated source")
    private static final long serialVersionUID = 0;

    public /* bridge */ /* synthetic */ int add(Object x0, int x1) {
        return super.add(x0, x1);
    }

    public /* bridge */ /* synthetic */ boolean add(Object x0) {
        return super.add(x0);
    }

    public /* bridge */ /* synthetic */ boolean addAll(Collection x0) {
        return super.addAll(x0);
    }

    public /* bridge */ /* synthetic */ void clear() {
        super.clear();
    }

    public /* bridge */ /* synthetic */ boolean contains(Object x0) {
        return super.contains(x0);
    }

    public /* bridge */ /* synthetic */ Set entrySet() {
        return super.entrySet();
    }

    public /* bridge */ /* synthetic */ boolean equals(Object x0) {
        return super.equals(x0);
    }

    public /* bridge */ /* synthetic */ int hashCode() {
        return super.hashCode();
    }

    public /* bridge */ /* synthetic */ boolean isEmpty() {
        return super.isEmpty();
    }

    public /* bridge */ /* synthetic */ Iterator iterator() {
        return super.iterator();
    }

    public /* bridge */ /* synthetic */ int remove(Object x0, int x1) {
        return super.remove(x0, x1);
    }

    public /* bridge */ /* synthetic */ boolean remove(Object x0) {
        return super.remove(x0);
    }

    public /* bridge */ /* synthetic */ boolean removeAll(Collection x0) {
        return super.removeAll(x0);
    }

    public /* bridge */ /* synthetic */ boolean retainAll(Collection x0) {
        return super.retainAll(x0);
    }

    public /* bridge */ /* synthetic */ int setCount(Object x0, int x1) {
        return super.setCount(x0, x1);
    }

    public /* bridge */ /* synthetic */ boolean setCount(Object x0, int x1, int x2) {
        return super.setCount(x0, x1, x2);
    }

    public /* bridge */ /* synthetic */ int size() {
        return super.size();
    }

    public /* bridge */ /* synthetic */ String toString() {
        return super.toString();
    }

    public static <E extends Comparable> TreeMultiset<E> create() {
        return new TreeMultiset<>();
    }

    public static <E> TreeMultiset<E> create(Comparator<? super E> comparator) {
        return new TreeMultiset<>(comparator);
    }

    public static <E extends Comparable> TreeMultiset<E> create(Iterable<? extends E> elements) {
        TreeMultiset<E> multiset = create();
        Iterables.addAll(multiset, elements);
        return multiset;
    }

    private TreeMultiset() {
        super(new TreeMap());
    }

    private TreeMultiset(Comparator<? super E> comparator) {
        super(new TreeMap(comparator));
    }

    public SortedSet<E> elementSet() {
        return (SortedSet) super.elementSet();
    }

    public int count(@Nullable Object element) {
        try {
            return super.count(element);
        } catch (ClassCastException | NullPointerException e) {
            return 0;
        }
    }

    /* access modifiers changed from: package-private */
    public Set<E> createElementSet() {
        return new SortedMapBasedElementSet((SortedMap) backingMap());
    }

    private class SortedMapBasedElementSet extends AbstractMapBasedMultiset<E>.MapBasedElementSet implements SortedSet<E> {
        SortedMapBasedElementSet(SortedMap<E, AtomicInteger> map) {
            super(map);
        }

        /* access modifiers changed from: package-private */
        public SortedMap<E, AtomicInteger> sortedMap() {
            return (SortedMap) getMap();
        }

        public Comparator<? super E> comparator() {
            return sortedMap().comparator();
        }

        public E first() {
            return sortedMap().firstKey();
        }

        public E last() {
            return sortedMap().lastKey();
        }

        public SortedSet<E> headSet(E toElement) {
            return new SortedMapBasedElementSet(sortedMap().headMap(toElement));
        }

        public SortedSet<E> subSet(E fromElement, E toElement) {
            return new SortedMapBasedElementSet(sortedMap().subMap(fromElement, toElement));
        }

        public SortedSet<E> tailSet(E fromElement) {
            return new SortedMapBasedElementSet(sortedMap().tailMap(fromElement));
        }

        public boolean remove(Object element) {
            try {
                return super.remove(element);
            } catch (ClassCastException | NullPointerException e) {
                return false;
            }
        }
    }

    @GwtIncompatible("java.io.ObjectOutputStream")
    private void writeObject(ObjectOutputStream stream) throws IOException {
        stream.defaultWriteObject();
        stream.writeObject(elementSet().comparator());
        Serialization.writeMultiset(this, stream);
    }

    @GwtIncompatible("java.io.ObjectInputStream")
    private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
        stream.defaultReadObject();
        setBackingMap(new TreeMap((Comparator) stream.readObject()));
        Serialization.populateMultiset(this, stream);
    }
}
