package com.google.common.collect;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.SortedLists;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.SortedMap;
import javax.annotation.Nullable;

@GwtCompatible(emulated = true, serializable = true)
public class ImmutableSortedMap<K, V> extends ImmutableSortedMapFauxverideShim<K, V> implements SortedMap<K, V> {
    private static final ImmutableSortedMap<Comparable, Object> NATURAL_EMPTY_MAP = new ImmutableSortedMap<>(ImmutableList.of(), NATURAL_ORDER);
    private static final Comparator<Comparable> NATURAL_ORDER = Ordering.natural();
    private static final long serialVersionUID = 0;
    private final transient Comparator<? super K> comparator;
    final transient ImmutableList<Map.Entry<K, V>> entries;
    private transient ImmutableSet<Map.Entry<K, V>> entrySet;
    final transient Function<Map.Entry<K, V>, K> keyFunction = new Function<Map.Entry<K, V>, K>() {
        public /* bridge */ /* synthetic */ Object apply(Object x0) {
            return apply((Map.Entry) ((Map.Entry) x0));
        }

        public K apply(Map.Entry<K, V> entry) {
            return entry.getKey();
        }
    };
    private transient ImmutableSortedSet<K> keySet;
    private transient ImmutableCollection<V> values;

    public static <K, V> ImmutableSortedMap<K, V> of() {
        return NATURAL_EMPTY_MAP;
    }

    private static <K, V> ImmutableSortedMap<K, V> emptyMap(Comparator<? super K> comparator2) {
        if (NATURAL_ORDER.equals(comparator2)) {
            return NATURAL_EMPTY_MAP;
        }
        return new ImmutableSortedMap<>(ImmutableList.of(), comparator2);
    }

    public static <K extends Comparable<? super K>, V> ImmutableSortedMap<K, V> of(K k1, V v1) {
        return new ImmutableSortedMap<>(ImmutableList.of(entryOf(k1, v1)), Ordering.natural());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableSortedMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableSortedMap$Builder<K, V>
     arg types: [K, V]
     candidates:
      com.google.common.collect.ImmutableSortedMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableMap$Builder
      com.google.common.collect.ImmutableMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableMap$Builder<K, V>
      com.google.common.collect.ImmutableSortedMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableSortedMap$Builder<K, V> */
    public static <K extends Comparable<? super K>, V> ImmutableSortedMap<K, V> of(K k1, V v1, K k2, V v2) {
        return new Builder(Ordering.natural()).put((Object) k1, (Object) v1).put((Object) k2, (Object) v2).build();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableSortedMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableSortedMap$Builder<K, V>
     arg types: [K, V]
     candidates:
      com.google.common.collect.ImmutableSortedMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableMap$Builder
      com.google.common.collect.ImmutableMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableMap$Builder<K, V>
      com.google.common.collect.ImmutableSortedMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableSortedMap$Builder<K, V> */
    public static <K extends Comparable<? super K>, V> ImmutableSortedMap<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3) {
        return new Builder(Ordering.natural()).put((Object) k1, (Object) v1).put((Object) k2, (Object) v2).put((Object) k3, (Object) v3).build();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableSortedMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableSortedMap$Builder<K, V>
     arg types: [K, V]
     candidates:
      com.google.common.collect.ImmutableSortedMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableMap$Builder
      com.google.common.collect.ImmutableMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableMap$Builder<K, V>
      com.google.common.collect.ImmutableSortedMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableSortedMap$Builder<K, V> */
    public static <K extends Comparable<? super K>, V> ImmutableSortedMap<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4) {
        return new Builder(Ordering.natural()).put((Object) k1, (Object) v1).put((Object) k2, (Object) v2).put((Object) k3, (Object) v3).put((Object) k4, (Object) v4).build();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableSortedMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableSortedMap$Builder<K, V>
     arg types: [K, V]
     candidates:
      com.google.common.collect.ImmutableSortedMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableMap$Builder
      com.google.common.collect.ImmutableMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableMap$Builder<K, V>
      com.google.common.collect.ImmutableSortedMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableSortedMap$Builder<K, V> */
    public static <K extends Comparable<? super K>, V> ImmutableSortedMap<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return new Builder(Ordering.natural()).put((Object) k1, (Object) v1).put((Object) k2, (Object) v2).put((Object) k3, (Object) v3).put((Object) k4, (Object) v4).put((Object) k5, (Object) v5).build();
    }

    public static <K, V> ImmutableSortedMap<K, V> copyOf(Map<? extends K, ? extends V> map) {
        return copyOfInternal(map, Ordering.natural());
    }

    public static <K, V> ImmutableSortedMap<K, V> copyOf(Map<? extends K, ? extends V> map, Comparator<? super K> comparator2) {
        return copyOfInternal(map, (Comparator) Preconditions.checkNotNull(comparator2));
    }

    public static <K, V> ImmutableSortedMap<K, V> copyOfSorted(SortedMap<K, ? extends V> map) {
        Comparator<? super K> comparator2 = map.comparator();
        if (comparator2 == null) {
            comparator2 = NATURAL_ORDER;
        }
        return copyOfInternal(map, comparator2);
    }

    private static <K, V> ImmutableSortedMap<K, V> copyOfInternal(Map<? extends K, ? extends V> map, Comparator<? super K> comparator2) {
        boolean sameComparator = false;
        if (map instanceof SortedMap) {
            Comparator<?> comparator22 = ((SortedMap) map).comparator();
            if (comparator22 != null) {
                sameComparator = comparator2.equals(comparator22);
            } else if (comparator2 == NATURAL_ORDER) {
                sameComparator = true;
            } else {
                sameComparator = false;
            }
        }
        if (sameComparator && (map instanceof ImmutableSortedMap)) {
            ImmutableSortedMap<K, V> kvMap = (ImmutableSortedMap) map;
            if (!kvMap.isPartialView()) {
                return kvMap;
            }
        }
        Map.Entry<K, V>[] entries2 = (Map.Entry[]) map.entrySet().toArray(new Map.Entry[0]);
        for (int i = 0; i < entries2.length; i++) {
            Map.Entry<K, V> entry = entries2[i];
            entries2[i] = entryOf(entry.getKey(), entry.getValue());
        }
        List<Map.Entry<K, V>> list = Arrays.asList(entries2);
        if (!sameComparator) {
            sortEntries(list, comparator2);
            validateEntries(list, comparator2);
        }
        return new ImmutableSortedMap<>(ImmutableList.copyOf((Collection) list), comparator2);
    }

    /* access modifiers changed from: private */
    public static <K, V> void sortEntries(List<Map.Entry<K, V>> entries2, final Comparator<? super K> comparator2) {
        Collections.sort(entries2, new Comparator<Map.Entry<K, V>>() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.google.common.collect.ImmutableSortedMap.1.compare(java.util.Map$Entry, java.util.Map$Entry):int
             arg types: [java.lang.Object, java.lang.Object]
             candidates:
              com.google.common.collect.ImmutableSortedMap.1.compare(java.lang.Object, java.lang.Object):int
              ClspMth{java.util.Comparator.compare(java.lang.Object, java.lang.Object):int}
              com.google.common.collect.ImmutableSortedMap.1.compare(java.util.Map$Entry, java.util.Map$Entry):int */
            public /* bridge */ /* synthetic */ int compare(Object x0, Object x1) {
                return compare((Map.Entry) ((Map.Entry) x0), (Map.Entry) ((Map.Entry) x1));
            }

            public int compare(Map.Entry<K, V> entry1, Map.Entry<K, V> entry2) {
                return comparator2.compare(entry1.getKey(), entry2.getKey());
            }
        });
    }

    /* access modifiers changed from: private */
    public static <K, V> void validateEntries(List<Map.Entry<K, V>> entries2, Comparator<? super K> comparator2) {
        for (int i = 1; i < entries2.size(); i++) {
            if (comparator2.compare(entries2.get(i - 1).getKey(), entries2.get(i).getKey()) == 0) {
                throw new IllegalArgumentException("Duplicate keys in mappings " + entries2.get(i - 1) + " and " + entries2.get(i));
            }
        }
    }

    public static <K extends Comparable<K>, V> Builder<K, V> naturalOrder() {
        return new Builder<>(Ordering.natural());
    }

    public static <K, V> Builder<K, V> orderedBy(Comparator<K> comparator2) {
        return new Builder<>(comparator2);
    }

    public static <K extends Comparable<K>, V> Builder<K, V> reverseOrder() {
        return new Builder<>(Ordering.natural().reverse());
    }

    public static class Builder<K, V> extends ImmutableMap.Builder<K, V> {
        private final Comparator<? super K> comparator;

        public Builder(Comparator<? super K> comparator2) {
            this.comparator = (Comparator) Preconditions.checkNotNull(comparator2);
        }

        public Builder<K, V> put(K key, V value) {
            this.entries.add(ImmutableMap.entryOf(key, value));
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.common.collect.ImmutableSortedMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableSortedMap$Builder<K, V>
         arg types: [? extends K, ? extends V]
         candidates:
          com.google.common.collect.ImmutableSortedMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableMap$Builder
          com.google.common.collect.ImmutableMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableMap$Builder<K, V>
          com.google.common.collect.ImmutableSortedMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableSortedMap$Builder<K, V> */
        public Builder<K, V> putAll(Map<? extends K, ? extends V> map) {
            for (Map.Entry<? extends K, ? extends V> entry : map.entrySet()) {
                put((Object) entry.getKey(), (Object) entry.getValue());
            }
            return this;
        }

        public ImmutableSortedMap<K, V> build() {
            ImmutableSortedMap.sortEntries(this.entries, this.comparator);
            ImmutableSortedMap.validateEntries(this.entries, this.comparator);
            return new ImmutableSortedMap<>(ImmutableList.copyOf((Collection) this.entries), this.comparator);
        }
    }

    ImmutableSortedMap(ImmutableList<Map.Entry<K, V>> entries2, Comparator<? super K> comparator2) {
        this.entries = entries2;
        this.comparator = comparator2;
    }

    public int size() {
        return this.entries.size();
    }

    /* access modifiers changed from: package-private */
    public Comparator<Object> unsafeComparator() {
        return this.comparator;
    }

    public V get(@Nullable Object key) {
        if (key == null) {
            return null;
        }
        try {
            int i = SortedLists.binarySearch(Lists.transform(this.entries, this.keyFunction), key, unsafeComparator(), SortedLists.Relation.EQUAL, false);
            if (i >= 0) {
                return this.entries.get(i).getValue();
            }
            return null;
        } catch (ClassCastException e) {
            return null;
        }
    }

    public boolean containsValue(@Nullable Object value) {
        if (value == null) {
            return false;
        }
        return Iterators.contains(valueIterator(), value);
    }

    /* access modifiers changed from: package-private */
    public boolean isPartialView() {
        return this.entries.isPartialView();
    }

    public ImmutableSet<Map.Entry<K, V>> entrySet() {
        ImmutableSet<Map.Entry<K, V>> es = this.entrySet;
        if (es != null) {
            return es;
        }
        ImmutableSet<Map.Entry<K, V>> createEntrySet = createEntrySet();
        this.entrySet = createEntrySet;
        return createEntrySet;
    }

    private ImmutableSet<Map.Entry<K, V>> createEntrySet() {
        return isEmpty() ? ImmutableSet.of() : new EntrySet(this);
    }

    private static class EntrySet<K, V> extends ImmutableSet<Map.Entry<K, V>> {
        final transient ImmutableSortedMap<K, V> map;

        EntrySet(ImmutableSortedMap<K, V> map2) {
            this.map = map2;
        }

        /* access modifiers changed from: package-private */
        public boolean isPartialView() {
            return this.map.isPartialView();
        }

        public int size() {
            return this.map.size();
        }

        public UnmodifiableIterator<Map.Entry<K, V>> iterator() {
            return this.map.entries.iterator();
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public boolean contains(java.lang.Object r7) {
            /*
                r6 = this;
                r5 = 0
                boolean r3 = r7 instanceof java.util.Map.Entry
                if (r3 == 0) goto L_0x0023
                r0 = r7
                java.util.Map$Entry r0 = (java.util.Map.Entry) r0
                r1 = r0
                com.google.common.collect.ImmutableSortedMap<K, V> r3 = r6.map
                java.lang.Object r4 = r1.getKey()
                java.lang.Object r2 = r3.get(r4)
                if (r2 == 0) goto L_0x0021
                java.lang.Object r3 = r1.getValue()
                boolean r3 = r2.equals(r3)
                if (r3 == 0) goto L_0x0021
                r3 = 1
            L_0x0020:
                return r3
            L_0x0021:
                r3 = r5
                goto L_0x0020
            L_0x0023:
                r3 = r5
                goto L_0x0020
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.ImmutableSortedMap.EntrySet.contains(java.lang.Object):boolean");
        }

        /* access modifiers changed from: package-private */
        public Object writeReplace() {
            return new EntrySetSerializedForm(this.map);
        }
    }

    private static class EntrySetSerializedForm<K, V> implements Serializable {
        private static final long serialVersionUID = 0;
        final ImmutableSortedMap<K, V> map;

        EntrySetSerializedForm(ImmutableSortedMap<K, V> map2) {
            this.map = map2;
        }

        /* access modifiers changed from: package-private */
        public Object readResolve() {
            return this.map.entrySet();
        }
    }

    public ImmutableSortedSet<K> keySet() {
        ImmutableSortedSet<K> ks = this.keySet;
        if (ks != null) {
            return ks;
        }
        ImmutableSortedSet<K> createKeySet = createKeySet();
        this.keySet = createKeySet;
        return createKeySet;
    }

    private ImmutableSortedSet<K> createKeySet() {
        if (isEmpty()) {
            return ImmutableSortedSet.emptySet(this.comparator);
        }
        return new RegularImmutableSortedSet(new TransformedImmutableList<Map.Entry<K, V>, K>(this.entries) {
            /* access modifiers changed from: package-private */
            public /* bridge */ /* synthetic */ Object transform(Object x0) {
                return transform((Map.Entry) ((Map.Entry) x0));
            }

            /* access modifiers changed from: package-private */
            public K transform(Map.Entry<K, V> entry) {
                return entry.getKey();
            }
        }, this.comparator);
    }

    public ImmutableCollection<V> values() {
        ImmutableCollection<V> v = this.values;
        if (v != null) {
            return v;
        }
        Values values2 = new Values(this);
        this.values = values2;
        return values2;
    }

    /* access modifiers changed from: package-private */
    public UnmodifiableIterator<V> valueIterator() {
        final UnmodifiableIterator<Map.Entry<K, V>> entryIterator = this.entries.iterator();
        return new UnmodifiableIterator<V>() {
            public boolean hasNext() {
                return entryIterator.hasNext();
            }

            public V next() {
                return ((Map.Entry) entryIterator.next()).getValue();
            }
        };
    }

    private static class Values<V> extends ImmutableCollection<V> {
        private final ImmutableSortedMap<?, V> map;

        Values(ImmutableSortedMap<?, V> map2) {
            this.map = map2;
        }

        public int size() {
            return this.map.size();
        }

        public UnmodifiableIterator<V> iterator() {
            return this.map.valueIterator();
        }

        public boolean contains(Object target) {
            return this.map.containsValue(target);
        }

        /* access modifiers changed from: package-private */
        public boolean isPartialView() {
            return true;
        }

        /* access modifiers changed from: package-private */
        public Object writeReplace() {
            return new ValuesSerializedForm(this.map);
        }
    }

    private static class ValuesSerializedForm<V> implements Serializable {
        private static final long serialVersionUID = 0;
        final ImmutableSortedMap<?, V> map;

        ValuesSerializedForm(ImmutableSortedMap<?, V> map2) {
            this.map = map2;
        }

        /* access modifiers changed from: package-private */
        public Object readResolve() {
            return this.map.values();
        }
    }

    public Comparator<? super K> comparator() {
        return this.comparator;
    }

    public K firstKey() {
        if (!isEmpty()) {
            return this.entries.get(0).getKey();
        }
        throw new NoSuchElementException();
    }

    public K lastKey() {
        if (!isEmpty()) {
            return this.entries.get(size() - 1).getKey();
        }
        throw new NoSuchElementException();
    }

    public ImmutableSortedMap<K, V> headMap(K toKey) {
        return createSubmap(0, findSubmapIndex(Preconditions.checkNotNull(toKey)));
    }

    public ImmutableSortedMap<K, V> subMap(K fromKey, K toKey) {
        Preconditions.checkNotNull(fromKey);
        Preconditions.checkNotNull(toKey);
        Preconditions.checkArgument(this.comparator.compare(fromKey, toKey) <= 0);
        return createSubmap(findSubmapIndex(fromKey), findSubmapIndex(toKey));
    }

    public ImmutableSortedMap<K, V> tailMap(K fromKey) {
        return createSubmap(findSubmapIndex(Preconditions.checkNotNull(fromKey)), size());
    }

    private int findSubmapIndex(K key) {
        return SortedLists.binarySearch(Lists.transform(this.entries, this.keyFunction), key, this.comparator, SortedLists.Relation.CEILING, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList<E>
     arg types: [int, int]
     candidates:
      com.google.common.collect.ImmutableList.subList(int, int):java.util.List
      ClspMth{java.util.List.subList(int, int):java.util.List<E>}
      com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList<E> */
    private ImmutableSortedMap<K, V> createSubmap(int newFromIndex, int newToIndex) {
        if (newFromIndex < newToIndex) {
            return new ImmutableSortedMap<>(this.entries.subList(newFromIndex, newToIndex), this.comparator);
        }
        return emptyMap(this.comparator);
    }

    private static class SerializedForm extends ImmutableMap.SerializedForm {
        private static final long serialVersionUID = 0;
        private final Comparator<Object> comparator;

        SerializedForm(ImmutableSortedMap<?, ?> sortedMap) {
            super(sortedMap);
            this.comparator = sortedMap.comparator();
        }

        /* access modifiers changed from: package-private */
        public Object readResolve() {
            return createMap(new Builder<>(this.comparator));
        }
    }

    /* access modifiers changed from: package-private */
    public Object writeReplace() {
        return new SerializedForm(this);
    }
}
