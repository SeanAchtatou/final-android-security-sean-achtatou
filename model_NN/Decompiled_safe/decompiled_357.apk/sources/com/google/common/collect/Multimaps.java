package com.google.common.collect;

import com.google.common.annotations.Beta;
import com.google.common.annotations.GwtCompatible;
import com.google.common.annotations.GwtIncompatible;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Maps;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.SortedSet;
import javax.annotation.Nullable;

@GwtCompatible(emulated = true)
public final class Multimaps {
    private Multimaps() {
    }

    public static <K, V> Multimap<K, V> newMultimap(Map<K, Collection<V>> map, Supplier<? extends Collection<V>> factory) {
        return new CustomMultimap(map, factory);
    }

    private static class CustomMultimap<K, V> extends AbstractMultimap<K, V> {
        @GwtIncompatible("java serialization not supported")
        private static final long serialVersionUID = 0;
        transient Supplier<? extends Collection<V>> factory;

        CustomMultimap(Map<K, Collection<V>> map, Supplier<? extends Collection<V>> factory2) {
            super(map);
            this.factory = (Supplier) Preconditions.checkNotNull(factory2);
        }

        /* access modifiers changed from: protected */
        public Collection<V> createCollection() {
            return (Collection) this.factory.get();
        }

        @GwtIncompatible("java.io.ObjectOutputStream")
        private void writeObject(ObjectOutputStream stream) throws IOException {
            stream.defaultWriteObject();
            stream.writeObject(this.factory);
            stream.writeObject(backingMap());
        }

        @GwtIncompatible("java.io.ObjectInputStream")
        private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
            stream.defaultReadObject();
            this.factory = (Supplier) stream.readObject();
            setMap((Map) stream.readObject());
        }
    }

    public static <K, V> ListMultimap<K, V> newListMultimap(Map<K, Collection<V>> map, Supplier<? extends List<V>> factory) {
        return new CustomListMultimap(map, factory);
    }

    private static class CustomListMultimap<K, V> extends AbstractListMultimap<K, V> {
        @GwtIncompatible("java serialization not supported")
        private static final long serialVersionUID = 0;
        transient Supplier<? extends List<V>> factory;

        CustomListMultimap(Map<K, Collection<V>> map, Supplier<? extends List<V>> factory2) {
            super(map);
            this.factory = (Supplier) Preconditions.checkNotNull(factory2);
        }

        /* access modifiers changed from: protected */
        public List<V> createCollection() {
            return (List) this.factory.get();
        }

        @GwtIncompatible("java.io.ObjectOutputStream")
        private void writeObject(ObjectOutputStream stream) throws IOException {
            stream.defaultWriteObject();
            stream.writeObject(this.factory);
            stream.writeObject(backingMap());
        }

        @GwtIncompatible("java.io.ObjectInputStream")
        private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
            stream.defaultReadObject();
            this.factory = (Supplier) stream.readObject();
            setMap((Map) stream.readObject());
        }
    }

    public static <K, V> SetMultimap<K, V> newSetMultimap(Map<K, Collection<V>> map, Supplier<? extends Set<V>> factory) {
        return new CustomSetMultimap(map, factory);
    }

    private static class CustomSetMultimap<K, V> extends AbstractSetMultimap<K, V> {
        @GwtIncompatible("not needed in emulated source")
        private static final long serialVersionUID = 0;
        transient Supplier<? extends Set<V>> factory;

        CustomSetMultimap(Map<K, Collection<V>> map, Supplier<? extends Set<V>> factory2) {
            super(map);
            this.factory = (Supplier) Preconditions.checkNotNull(factory2);
        }

        /* access modifiers changed from: protected */
        public Set<V> createCollection() {
            return (Set) this.factory.get();
        }

        @GwtIncompatible("java.io.ObjectOutputStream")
        private void writeObject(ObjectOutputStream stream) throws IOException {
            stream.defaultWriteObject();
            stream.writeObject(this.factory);
            stream.writeObject(backingMap());
        }

        @GwtIncompatible("java.io.ObjectInputStream")
        private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
            stream.defaultReadObject();
            this.factory = (Supplier) stream.readObject();
            setMap((Map) stream.readObject());
        }
    }

    public static <K, V> SortedSetMultimap<K, V> newSortedSetMultimap(Map<K, Collection<V>> map, Supplier<? extends SortedSet<V>> factory) {
        return new CustomSortedSetMultimap(map, factory);
    }

    private static class CustomSortedSetMultimap<K, V> extends AbstractSortedSetMultimap<K, V> {
        @GwtIncompatible("not needed in emulated source")
        private static final long serialVersionUID = 0;
        transient Supplier<? extends SortedSet<V>> factory;
        transient Comparator<? super V> valueComparator;

        CustomSortedSetMultimap(Map<K, Collection<V>> map, Supplier<? extends SortedSet<V>> factory2) {
            super(map);
            this.factory = (Supplier) Preconditions.checkNotNull(factory2);
            this.valueComparator = ((SortedSet) factory2.get()).comparator();
        }

        /* access modifiers changed from: protected */
        public SortedSet<V> createCollection() {
            return (SortedSet) this.factory.get();
        }

        public Comparator<? super V> valueComparator() {
            return this.valueComparator;
        }

        @GwtIncompatible("java.io.ObjectOutputStream")
        private void writeObject(ObjectOutputStream stream) throws IOException {
            stream.defaultWriteObject();
            stream.writeObject(this.factory);
            stream.writeObject(backingMap());
        }

        @GwtIncompatible("java.io.ObjectInputStream")
        private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
            stream.defaultReadObject();
            this.factory = (Supplier) stream.readObject();
            this.valueComparator = ((SortedSet) this.factory.get()).comparator();
            setMap((Map) stream.readObject());
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: M
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static <K, V, M extends com.google.common.collect.Multimap<K, V>> M invertFrom(com.google.common.collect.Multimap<? extends V, ? extends K> r4, M r5) {
        /*
            com.google.common.base.Preconditions.checkNotNull(r5)
            java.util.Collection r2 = r4.entries()
            java.util.Iterator r1 = r2.iterator()
        L_0x000b:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0023
            java.lang.Object r0 = r1.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r2 = r0.getValue()
            java.lang.Object r3 = r0.getKey()
            r5.put(r2, r3)
            goto L_0x000b
        L_0x0023:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.Multimaps.invertFrom(com.google.common.collect.Multimap, com.google.common.collect.Multimap):com.google.common.collect.Multimap");
    }

    public static <K, V> Multimap<K, V> synchronizedMultimap(Multimap<K, V> multimap) {
        return Synchronized.multimap(multimap, null);
    }

    public static <K, V> Multimap<K, V> unmodifiableMultimap(Multimap<K, V> delegate) {
        return new UnmodifiableMultimap(delegate);
    }

    private static class UnmodifiableMultimap<K, V> extends ForwardingMultimap<K, V> implements Serializable {
        private static final long serialVersionUID = 0;
        final Multimap<K, V> delegate;
        transient Collection<Map.Entry<K, V>> entries;
        transient Set<K> keySet;
        transient Multiset<K> keys;
        transient Map<K, Collection<V>> map;
        transient Collection<V> values;

        UnmodifiableMultimap(Multimap<K, V> delegate2) {
            this.delegate = (Multimap) Preconditions.checkNotNull(delegate2);
        }

        /* access modifiers changed from: protected */
        public Multimap<K, V> delegate() {
            return this.delegate;
        }

        public void clear() {
            throw new UnsupportedOperationException();
        }

        public Map<K, Collection<V>> asMap() {
            Map<K, Collection<V>> result = this.map;
            if (result != null) {
                return result;
            }
            final Map<K, Collection<V>> unmodifiableMap = Collections.unmodifiableMap(this.delegate.asMap());
            Map<K, Collection<V>> result2 = new ForwardingMap<K, Collection<V>>() {
                Collection<Collection<V>> asMapValues;
                Set<Map.Entry<K, Collection<V>>> entrySet;

                /* access modifiers changed from: protected */
                public Map<K, Collection<V>> delegate() {
                    return unmodifiableMap;
                }

                public Set<Map.Entry<K, Collection<V>>> entrySet() {
                    Set<Map.Entry<K, Collection<V>>> result = this.entrySet;
                    if (result != null) {
                        return result;
                    }
                    Set<Map.Entry<K, Collection<V>>> access$000 = Multimaps.unmodifiableAsMapEntries(unmodifiableMap.entrySet());
                    this.entrySet = access$000;
                    return access$000;
                }

                public Collection<V> get(Object key) {
                    Collection<V> collection = (Collection) unmodifiableMap.get(key);
                    if (collection == null) {
                        return null;
                    }
                    return Multimaps.unmodifiableValueCollection(collection);
                }

                public Collection<Collection<V>> values() {
                    Collection<Collection<V>> result = this.asMapValues;
                    if (result != null) {
                        return result;
                    }
                    UnmodifiableAsMapValues unmodifiableAsMapValues = new UnmodifiableAsMapValues(unmodifiableMap.values());
                    this.asMapValues = unmodifiableAsMapValues;
                    return unmodifiableAsMapValues;
                }

                public boolean containsValue(Object o) {
                    return values().contains(o);
                }
            };
            this.map = result2;
            return result2;
        }

        public Collection<Map.Entry<K, V>> entries() {
            Collection<Map.Entry<K, V>> result = this.entries;
            if (result != null) {
                return result;
            }
            Collection<Map.Entry<K, V>> result2 = Multimaps.unmodifiableEntries(this.delegate.entries());
            this.entries = result2;
            return result2;
        }

        public Collection<V> get(K key) {
            return Multimaps.unmodifiableValueCollection(this.delegate.get(key));
        }

        public Multiset<K> keys() {
            Multiset<K> result = this.keys;
            if (result != null) {
                return result;
            }
            Multiset<K> result2 = Multisets.unmodifiableMultiset(this.delegate.keys());
            this.keys = result2;
            return result2;
        }

        public Set<K> keySet() {
            Set<K> result = this.keySet;
            if (result != null) {
                return result;
            }
            Set<K> result2 = Collections.unmodifiableSet(this.delegate.keySet());
            this.keySet = result2;
            return result2;
        }

        public boolean put(K k, V v) {
            throw new UnsupportedOperationException();
        }

        public boolean putAll(K k, Iterable<? extends V> iterable) {
            throw new UnsupportedOperationException();
        }

        public boolean putAll(Multimap<? extends K, ? extends V> multimap) {
            throw new UnsupportedOperationException();
        }

        public boolean remove(Object key, Object value) {
            throw new UnsupportedOperationException();
        }

        public Collection<V> removeAll(Object key) {
            throw new UnsupportedOperationException();
        }

        public Collection<V> replaceValues(K k, Iterable<? extends V> iterable) {
            throw new UnsupportedOperationException();
        }

        public Collection<V> values() {
            Collection<V> result = this.values;
            if (result != null) {
                return result;
            }
            Collection<V> result2 = Collections.unmodifiableCollection(this.delegate.values());
            this.values = result2;
            return result2;
        }
    }

    private static class UnmodifiableAsMapValues<V> extends ForwardingCollection<Collection<V>> {
        final Collection<Collection<V>> delegate;

        UnmodifiableAsMapValues(Collection<Collection<V>> delegate2) {
            this.delegate = Collections.unmodifiableCollection(delegate2);
        }

        /* access modifiers changed from: protected */
        public Collection<Collection<V>> delegate() {
            return this.delegate;
        }

        public Iterator<Collection<V>> iterator() {
            final Iterator<Collection<V>> iterator = this.delegate.iterator();
            return new Iterator<Collection<V>>() {
                public boolean hasNext() {
                    return iterator.hasNext();
                }

                public Collection<V> next() {
                    return Multimaps.unmodifiableValueCollection((Collection) iterator.next());
                }

                public void remove() {
                    throw new UnsupportedOperationException();
                }
            };
        }

        public Object[] toArray() {
            return standardToArray();
        }

        public <T> T[] toArray(T[] array) {
            return standardToArray(array);
        }

        public boolean contains(Object o) {
            return standardContains(o);
        }

        public boolean containsAll(Collection<?> c) {
            return standardContainsAll(c);
        }
    }

    private static class UnmodifiableListMultimap<K, V> extends UnmodifiableMultimap<K, V> implements ListMultimap<K, V> {
        private static final long serialVersionUID = 0;

        UnmodifiableListMultimap(ListMultimap<K, V> delegate) {
            super(delegate);
        }

        public ListMultimap<K, V> delegate() {
            return (ListMultimap) super.delegate();
        }

        public List<V> get(K key) {
            return Collections.unmodifiableList(delegate().get((Object) key));
        }

        public List<V> removeAll(Object key) {
            throw new UnsupportedOperationException();
        }

        public List<V> replaceValues(K k, Iterable<? extends V> iterable) {
            throw new UnsupportedOperationException();
        }
    }

    private static class UnmodifiableSetMultimap<K, V> extends UnmodifiableMultimap<K, V> implements SetMultimap<K, V> {
        private static final long serialVersionUID = 0;

        UnmodifiableSetMultimap(SetMultimap<K, V> delegate) {
            super(delegate);
        }

        public SetMultimap<K, V> delegate() {
            return (SetMultimap) super.delegate();
        }

        public Set<V> get(K key) {
            return Collections.unmodifiableSet(delegate().get((Object) key));
        }

        public Set<Map.Entry<K, V>> entries() {
            return Maps.unmodifiableEntrySet(delegate().entries());
        }

        public Set<V> removeAll(Object key) {
            throw new UnsupportedOperationException();
        }

        public Set<V> replaceValues(K k, Iterable<? extends V> iterable) {
            throw new UnsupportedOperationException();
        }
    }

    private static class UnmodifiableSortedSetMultimap<K, V> extends UnmodifiableSetMultimap<K, V> implements SortedSetMultimap<K, V> {
        private static final long serialVersionUID = 0;

        UnmodifiableSortedSetMultimap(SortedSetMultimap<K, V> delegate) {
            super(delegate);
        }

        public SortedSetMultimap<K, V> delegate() {
            return (SortedSetMultimap) super.delegate();
        }

        public SortedSet<V> get(K key) {
            return Collections.unmodifiableSortedSet(delegate().get((Object) key));
        }

        public SortedSet<V> removeAll(Object key) {
            throw new UnsupportedOperationException();
        }

        public SortedSet<V> replaceValues(K k, Iterable<? extends V> iterable) {
            throw new UnsupportedOperationException();
        }

        public Comparator<? super V> valueComparator() {
            return delegate().valueComparator();
        }
    }

    public static <K, V> SetMultimap<K, V> synchronizedSetMultimap(SetMultimap<K, V> multimap) {
        return Synchronized.setMultimap(multimap, null);
    }

    public static <K, V> SetMultimap<K, V> unmodifiableSetMultimap(SetMultimap<K, V> delegate) {
        return new UnmodifiableSetMultimap(delegate);
    }

    public static <K, V> SortedSetMultimap<K, V> synchronizedSortedSetMultimap(SortedSetMultimap<K, V> multimap) {
        return Synchronized.sortedSetMultimap(multimap, null);
    }

    public static <K, V> SortedSetMultimap<K, V> unmodifiableSortedSetMultimap(SortedSetMultimap<K, V> delegate) {
        return new UnmodifiableSortedSetMultimap(delegate);
    }

    public static <K, V> ListMultimap<K, V> synchronizedListMultimap(ListMultimap<K, V> multimap) {
        return Synchronized.listMultimap(multimap, null);
    }

    public static <K, V> ListMultimap<K, V> unmodifiableListMultimap(ListMultimap<K, V> delegate) {
        return new UnmodifiableListMultimap(delegate);
    }

    /* access modifiers changed from: private */
    public static <V> Collection<V> unmodifiableValueCollection(Collection<V> collection) {
        if (collection instanceof SortedSet) {
            return Collections.unmodifiableSortedSet((SortedSet) collection);
        }
        if (collection instanceof Set) {
            return Collections.unmodifiableSet((Set) collection);
        }
        if (collection instanceof List) {
            return Collections.unmodifiableList((List) collection);
        }
        return Collections.unmodifiableCollection(collection);
    }

    /* access modifiers changed from: private */
    public static <K, V> Map.Entry<K, Collection<V>> unmodifiableAsMapEntry(final Map.Entry<K, Collection<V>> entry) {
        Preconditions.checkNotNull(entry);
        return new AbstractMapEntry<K, Collection<V>>() {
            public K getKey() {
                return entry.getKey();
            }

            public Collection<V> getValue() {
                return Multimaps.unmodifiableValueCollection((Collection) entry.getValue());
            }
        };
    }

    /* access modifiers changed from: private */
    public static <K, V> Collection<Map.Entry<K, V>> unmodifiableEntries(Collection<Map.Entry<K, V>> entries) {
        if (entries instanceof Set) {
            return Maps.unmodifiableEntrySet((Set) entries);
        }
        return new Maps.UnmodifiableEntries(Collections.unmodifiableCollection(entries));
    }

    /* access modifiers changed from: private */
    public static <K, V> Set<Map.Entry<K, Collection<V>>> unmodifiableAsMapEntries(Set<Map.Entry<K, Collection<V>>> asMapEntries) {
        return new UnmodifiableAsMapEntries(Collections.unmodifiableSet(asMapEntries));
    }

    static class UnmodifiableAsMapEntries<K, V> extends ForwardingSet<Map.Entry<K, Collection<V>>> {
        private final Set<Map.Entry<K, Collection<V>>> delegate;

        UnmodifiableAsMapEntries(Set<Map.Entry<K, Collection<V>>> delegate2) {
            this.delegate = delegate2;
        }

        /* access modifiers changed from: protected */
        public Set<Map.Entry<K, Collection<V>>> delegate() {
            return this.delegate;
        }

        public Iterator<Map.Entry<K, Collection<V>>> iterator() {
            final Iterator<Map.Entry<K, Collection<V>>> iterator = this.delegate.iterator();
            return new ForwardingIterator<Map.Entry<K, Collection<V>>>() {
                /* access modifiers changed from: protected */
                public Iterator<Map.Entry<K, Collection<V>>> delegate() {
                    return iterator;
                }

                public Map.Entry<K, Collection<V>> next() {
                    return Multimaps.unmodifiableAsMapEntry((Map.Entry) iterator.next());
                }
            };
        }

        public Object[] toArray() {
            return standardToArray();
        }

        public <T> T[] toArray(T[] array) {
            return standardToArray(array);
        }

        public boolean contains(Object o) {
            return Maps.containsEntryImpl(delegate(), o);
        }

        public boolean containsAll(Collection<?> c) {
            return standardContainsAll(c);
        }

        public boolean equals(@Nullable Object object) {
            return standardEquals(object);
        }
    }

    public static <K, V> SetMultimap<K, V> forMap(Map<K, V> map) {
        return new MapMultimap(map);
    }

    private static class MapMultimap<K, V> implements SetMultimap<K, V>, Serializable {
        private static final Joiner.MapJoiner JOINER = Joiner.on("], ").withKeyValueSeparator("=[").useForNull("null");
        private static final long serialVersionUID = 7845222491160860175L;
        transient Map<K, Collection<V>> asMap;
        final Map<K, V> map;

        MapMultimap(Map<K, V> map2) {
            this.map = (Map) Preconditions.checkNotNull(map2);
        }

        public int size() {
            return this.map.size();
        }

        public boolean isEmpty() {
            return this.map.isEmpty();
        }

        public boolean containsKey(Object key) {
            return this.map.containsKey(key);
        }

        public boolean containsValue(Object value) {
            return this.map.containsValue(value);
        }

        public boolean containsEntry(Object key, Object value) {
            return this.map.entrySet().contains(Maps.immutableEntry(key, value));
        }

        public Set<V> get(final K key) {
            return new AbstractSet<V>() {
                public Iterator<V> iterator() {
                    return new Iterator<V>() {
                        int i;

                        public boolean hasNext() {
                            return this.i == 0 && MapMultimap.this.map.containsKey(key);
                        }

                        public V next() {
                            if (!hasNext()) {
                                throw new NoSuchElementException();
                            }
                            this.i++;
                            return MapMultimap.this.map.get(key);
                        }

                        public void remove() {
                            Preconditions.checkState(this.i == 1);
                            this.i = -1;
                            MapMultimap.this.map.remove(key);
                        }
                    };
                }

                public int size() {
                    return MapMultimap.this.map.containsKey(key) ? 1 : 0;
                }
            };
        }

        public boolean put(K k, V v) {
            throw new UnsupportedOperationException();
        }

        public boolean putAll(K k, Iterable<? extends V> iterable) {
            throw new UnsupportedOperationException();
        }

        public boolean putAll(Multimap<? extends K, ? extends V> multimap) {
            throw new UnsupportedOperationException();
        }

        public Set<V> replaceValues(K k, Iterable<? extends V> iterable) {
            throw new UnsupportedOperationException();
        }

        public boolean remove(Object key, Object value) {
            return this.map.entrySet().remove(Maps.immutableEntry(key, value));
        }

        public Set<V> removeAll(Object key) {
            Set<V> values = new HashSet<>(2);
            if (this.map.containsKey(key)) {
                values.add(this.map.remove(key));
            }
            return values;
        }

        public void clear() {
            this.map.clear();
        }

        public Set<K> keySet() {
            return this.map.keySet();
        }

        public Multiset<K> keys() {
            return Multisets.forSet(this.map.keySet());
        }

        public Collection<V> values() {
            return this.map.values();
        }

        public Set<Map.Entry<K, V>> entries() {
            return this.map.entrySet();
        }

        public Map<K, Collection<V>> asMap() {
            Map<K, Collection<V>> result = this.asMap;
            if (result != null) {
                return result;
            }
            Map<K, Collection<V>> result2 = new AsMap();
            this.asMap = result2;
            return result2;
        }

        public boolean equals(@Nullable Object object) {
            if (object == this) {
                return true;
            }
            if (!(object instanceof Multimap)) {
                return false;
            }
            Multimap multimap = (Multimap) object;
            return size() == multimap.size() && asMap().equals(multimap.asMap());
        }

        public int hashCode() {
            return this.map.hashCode();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.StringBuilder, java.util.Map<?, ?>):java.lang.StringBuilder
         arg types: [java.lang.StringBuilder, java.util.Map<K, V>]
         candidates:
          com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.Appendable, java.util.Map<?, ?>):A
          com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.StringBuilder, java.util.Map<?, ?>):java.lang.StringBuilder */
        public String toString() {
            if (this.map.isEmpty()) {
                return "{}";
            }
            StringBuilder builder = Collections2.newStringBuilderForCollection(this.map.size()).append('{');
            JOINER.appendTo(builder, (Map<?, ?>) this.map);
            return builder.append("]}").toString();
        }

        class AsMapEntries extends AbstractSet<Map.Entry<K, Collection<V>>> {
            AsMapEntries() {
            }

            public int size() {
                return MapMultimap.this.map.size();
            }

            public Iterator<Map.Entry<K, Collection<V>>> iterator() {
                return new Iterator<Map.Entry<K, Collection<V>>>() {
                    final Iterator<K> keys = MapMultimap.this.map.keySet().iterator();

                    public boolean hasNext() {
                        return this.keys.hasNext();
                    }

                    public Map.Entry<K, Collection<V>> next() {
                        final K key = this.keys.next();
                        return new AbstractMapEntry<K, Collection<V>>() {
                            public K getKey() {
                                return key;
                            }

                            public Collection<V> getValue() {
                                return MapMultimap.this.get(key);
                            }
                        };
                    }

                    public void remove() {
                        this.keys.remove();
                    }
                };
            }

            public boolean contains(Object o) {
                if (!(o instanceof Map.Entry)) {
                    return false;
                }
                Map.Entry entry = (Map.Entry) o;
                if (!(entry.getValue() instanceof Set)) {
                    return false;
                }
                Set<?> set = (Set) entry.getValue();
                return set.size() == 1 && MapMultimap.this.containsEntry(entry.getKey(), set.iterator().next());
            }

            public boolean remove(Object o) {
                if (!(o instanceof Map.Entry)) {
                    return false;
                }
                Map.Entry entry = (Map.Entry) o;
                if (!(entry.getValue() instanceof Set)) {
                    return false;
                }
                Set<?> set = (Set) entry.getValue();
                return set.size() == 1 && MapMultimap.this.map.entrySet().remove(Maps.immutableEntry(entry.getKey(), set.iterator().next()));
            }
        }

        class AsMap extends Maps.ImprovedAbstractMap<K, Collection<V>> {
            AsMap() {
            }

            /* access modifiers changed from: protected */
            public Set<Map.Entry<K, Collection<V>>> createEntrySet() {
                return new AsMapEntries();
            }

            public boolean containsKey(Object key) {
                return MapMultimap.this.map.containsKey(key);
            }

            public Collection<V> get(Object key) {
                Collection<V> collection = MapMultimap.this.get(key);
                if (collection.isEmpty()) {
                    return null;
                }
                return collection;
            }

            public Collection<V> remove(Object key) {
                Collection<V> collection = MapMultimap.this.removeAll(key);
                if (collection.isEmpty()) {
                    return null;
                }
                return collection;
            }
        }
    }

    @GwtIncompatible("untested")
    @Beta
    public static <K, V1, V2> Multimap<K, V2> transformValues(Multimap<K, V1> fromMultimap, final Function<? super V1, V2> function) {
        Preconditions.checkNotNull(function);
        return transformEntries(fromMultimap, new Maps.EntryTransformer<K, V1, V2>() {
            public V2 transformEntry(K k, V1 value) {
                return function.apply(value);
            }
        });
    }

    @GwtIncompatible("untested")
    @Beta
    public static <K, V1, V2> Multimap<K, V2> transformEntries(Multimap<K, V1> fromMap, Maps.EntryTransformer<? super K, ? super V1, V2> transformer) {
        return new TransformedEntriesMultimap(fromMap, transformer);
    }

    @GwtIncompatible("untested")
    private static class TransformedEntriesMultimap<K, V1, V2> implements Multimap<K, V2> {
        private transient Map<K, Collection<V2>> asMap;
        private transient Collection<Map.Entry<K, V2>> entries;
        final Multimap<K, V1> fromMultimap;
        final Maps.EntryTransformer<? super K, ? super V1, V2> transformer;
        private transient Collection<V2> values;

        TransformedEntriesMultimap(Multimap<K, V1> fromMultimap2, Maps.EntryTransformer<? super K, ? super V1, V2> transformer2) {
            this.fromMultimap = (Multimap) Preconditions.checkNotNull(fromMultimap2);
            this.transformer = (Maps.EntryTransformer) Preconditions.checkNotNull(transformer2);
        }

        /* JADX WARN: Type inference failed for: r3v0, types: [java.util.Collection, java.util.Collection<V1>] */
        /* access modifiers changed from: package-private */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.util.Collection<V2> transform(final K r2, java.util.Collection<V1> r3) {
            /*
                r1 = this;
                com.google.common.collect.Multimaps$TransformedEntriesMultimap$1 r0 = new com.google.common.collect.Multimaps$TransformedEntriesMultimap$1
                r0.<init>(r2)
                java.util.Collection r0 = com.google.common.collect.Collections2.transform(r3, r0)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.Multimaps.TransformedEntriesMultimap.transform(java.lang.Object, java.util.Collection):java.util.Collection");
        }

        public Map<K, Collection<V2>> asMap() {
            if (this.asMap != null) {
                return this.asMap;
            }
            Map<K, Collection<V2>> aM = Maps.transformEntries(this.fromMultimap.asMap(), new Maps.EntryTransformer<K, Collection<V1>, Collection<V2>>() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.google.common.collect.Multimaps.TransformedEntriesMultimap.2.transformEntry(java.lang.Object, java.util.Collection):java.util.Collection<V2>
                 arg types: [java.lang.Object, java.lang.Object]
                 candidates:
                  com.google.common.collect.Multimaps.TransformedEntriesMultimap.2.transformEntry(java.lang.Object, java.lang.Object):java.lang.Object
                  com.google.common.collect.Maps.EntryTransformer.transformEntry(java.lang.Object, java.lang.Object):V2
                  com.google.common.collect.Multimaps.TransformedEntriesMultimap.2.transformEntry(java.lang.Object, java.util.Collection):java.util.Collection<V2> */
                public /* bridge */ /* synthetic */ Object transformEntry(Object x0, Object x1) {
                    return transformEntry(x0, (Collection) ((Collection) x1));
                }

                public Collection<V2> transformEntry(K key, Collection<V1> value) {
                    return TransformedEntriesMultimap.this.transform(key, value);
                }
            });
            this.asMap = aM;
            return aM;
        }

        public void clear() {
            this.fromMultimap.clear();
        }

        public boolean containsEntry(Object key, Object value) {
            return get(key).contains(value);
        }

        public boolean containsKey(Object key) {
            return this.fromMultimap.containsKey(key);
        }

        public boolean containsValue(Object value) {
            return values().contains(value);
        }

        public Collection<Map.Entry<K, V2>> entries() {
            if (this.entries != null) {
                return this.entries;
            }
            Collection<Map.Entry<K, V2>> es = new TransformedEntries(this.transformer);
            this.entries = es;
            return es;
        }

        private class TransformedEntries extends Collections2.TransformedCollection<Map.Entry<K, V1>, Map.Entry<K, V2>> {
            TransformedEntries(final Maps.EntryTransformer<? super K, ? super V1, V2> transformer) {
                super(TransformedEntriesMultimap.this.fromMultimap.entries(), new Function<Map.Entry<K, V1>, Map.Entry<K, V2>>() {
                    public /* bridge */ /* synthetic */ Object apply(Object x0) {
                        return apply((Map.Entry) ((Map.Entry) x0));
                    }

                    public Map.Entry<K, V2> apply(final Map.Entry<K, V1> entry) {
                        return new AbstractMapEntry<K, V2>() {
                            public K getKey() {
                                return entry.getKey();
                            }

                            public V2 getValue() {
                                return transformer.transformEntry(entry.getKey(), entry.getValue());
                            }
                        };
                    }
                });
            }

            public boolean contains(Object o) {
                if (!(o instanceof Map.Entry)) {
                    return false;
                }
                Map.Entry entry = (Map.Entry) o;
                return TransformedEntriesMultimap.this.containsEntry(entry.getKey(), entry.getValue());
            }

            public boolean remove(Object o) {
                if (!(o instanceof Map.Entry)) {
                    return false;
                }
                Map.Entry entry = (Map.Entry) o;
                return TransformedEntriesMultimap.this.get(entry.getKey()).remove(entry.getValue());
            }
        }

        public Collection<V2> get(K key) {
            return transform(key, this.fromMultimap.get(key));
        }

        public boolean isEmpty() {
            return this.fromMultimap.isEmpty();
        }

        public Set<K> keySet() {
            return this.fromMultimap.keySet();
        }

        public Multiset<K> keys() {
            return this.fromMultimap.keys();
        }

        public boolean put(K k, V2 v2) {
            throw new UnsupportedOperationException();
        }

        public boolean putAll(K k, Iterable<? extends V2> iterable) {
            throw new UnsupportedOperationException();
        }

        public boolean putAll(Multimap<? extends K, ? extends V2> multimap) {
            throw new UnsupportedOperationException();
        }

        public boolean remove(Object key, Object value) {
            return get(key).remove(value);
        }

        public Collection<V2> removeAll(Object key) {
            return transform(key, this.fromMultimap.removeAll(key));
        }

        public Collection<V2> replaceValues(K k, Iterable<? extends V2> iterable) {
            throw new UnsupportedOperationException();
        }

        public int size() {
            return this.fromMultimap.size();
        }

        public Collection<V2> values() {
            if (this.values != null) {
                return this.values;
            }
            Collection<V2> vs = Collections2.transform(this.fromMultimap.entries(), new Function<Map.Entry<K, V1>, V2>() {
                public /* bridge */ /* synthetic */ Object apply(Object x0) {
                    return apply((Map.Entry) ((Map.Entry) x0));
                }

                public V2 apply(Map.Entry<K, V1> entry) {
                    return TransformedEntriesMultimap.this.transformer.transformEntry(entry.getKey(), entry.getValue());
                }
            });
            this.values = vs;
            return vs;
        }

        public boolean equals(Object obj) {
            if (obj instanceof Multimap) {
                return asMap().equals(((Multimap) obj).asMap());
            }
            return false;
        }

        public int hashCode() {
            return asMap().hashCode();
        }

        public String toString() {
            return asMap().toString();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.Multimaps.transformEntries(com.google.common.collect.ListMultimap, com.google.common.collect.Maps$EntryTransformer):com.google.common.collect.ListMultimap<K, V2>
     arg types: [com.google.common.collect.ListMultimap<K, V1>, com.google.common.collect.Maps$EntryTransformer<K, V1, V2>]
     candidates:
      com.google.common.collect.Multimaps.transformEntries(com.google.common.collect.Multimap, com.google.common.collect.Maps$EntryTransformer):com.google.common.collect.Multimap<K, V2>
      com.google.common.collect.Multimaps.transformEntries(com.google.common.collect.ListMultimap, com.google.common.collect.Maps$EntryTransformer):com.google.common.collect.ListMultimap<K, V2> */
    @GwtIncompatible("untested")
    @Beta
    public static <K, V1, V2> ListMultimap<K, V2> transformValues(ListMultimap<K, V1> fromMultimap, final Function<? super V1, V2> function) {
        Preconditions.checkNotNull(function);
        return transformEntries((ListMultimap) fromMultimap, (Maps.EntryTransformer) new Maps.EntryTransformer<K, V1, V2>() {
            public V2 transformEntry(K k, V1 value) {
                return function.apply(value);
            }
        });
    }

    @GwtIncompatible("untested")
    @Beta
    public static <K, V1, V2> ListMultimap<K, V2> transformEntries(ListMultimap<K, V1> fromMap, Maps.EntryTransformer<? super K, ? super V1, V2> transformer) {
        return new TransformedEntriesListMultimap(fromMap, transformer);
    }

    @GwtIncompatible("untested")
    private static final class TransformedEntriesListMultimap<K, V1, V2> extends TransformedEntriesMultimap<K, V1, V2> implements ListMultimap<K, V2> {
        TransformedEntriesListMultimap(ListMultimap<K, V1> fromMultimap, Maps.EntryTransformer<? super K, ? super V1, V2> transformer) {
            super(fromMultimap, transformer);
        }

        /* access modifiers changed from: package-private */
        public List<V2> transform(final K key, Collection<V1> values) {
            return Lists.transform((List) values, new Function<V1, V2>() {
                public V2 apply(V1 value) {
                    return TransformedEntriesListMultimap.this.transformer.transformEntry(key, value);
                }
            });
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.common.collect.Multimaps.TransformedEntriesListMultimap.transform(java.lang.Object, java.util.Collection):java.util.List<V2>
         arg types: [K, java.util.Collection]
         candidates:
          com.google.common.collect.Multimaps.TransformedEntriesListMultimap.transform(java.lang.Object, java.util.Collection):java.util.Collection
          com.google.common.collect.Multimaps.TransformedEntriesMultimap.transform(java.lang.Object, java.util.Collection):java.util.Collection<V2>
          com.google.common.collect.Multimaps.TransformedEntriesListMultimap.transform(java.lang.Object, java.util.Collection):java.util.List<V2> */
        public List<V2> get(K key) {
            return transform((Object) key, this.fromMultimap.get(key));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.common.collect.Multimaps.TransformedEntriesListMultimap.transform(java.lang.Object, java.util.Collection):java.util.List<V2>
         arg types: [java.lang.Object, java.util.Collection]
         candidates:
          com.google.common.collect.Multimaps.TransformedEntriesListMultimap.transform(java.lang.Object, java.util.Collection):java.util.Collection
          com.google.common.collect.Multimaps.TransformedEntriesMultimap.transform(java.lang.Object, java.util.Collection):java.util.Collection<V2>
          com.google.common.collect.Multimaps.TransformedEntriesListMultimap.transform(java.lang.Object, java.util.Collection):java.util.List<V2> */
        public List<V2> removeAll(Object key) {
            return transform(key, this.fromMultimap.removeAll(key));
        }

        public List<V2> replaceValues(K k, Iterable<? extends V2> iterable) {
            throw new UnsupportedOperationException();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableListMultimap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableListMultimap$Builder<K, V>
     arg types: [K, V]
     candidates:
      com.google.common.collect.ImmutableListMultimap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableMultimap$Builder
      com.google.common.collect.ImmutableMultimap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableMultimap$Builder<K, V>
      com.google.common.collect.ImmutableListMultimap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableListMultimap$Builder<K, V> */
    public static <K, V> ImmutableListMultimap<K, V> index(Iterable<V> values, Function<? super V, K> keyFunction) {
        Preconditions.checkNotNull(keyFunction);
        ImmutableListMultimap.Builder<K, V> builder = ImmutableListMultimap.builder();
        for (V value : values) {
            Preconditions.checkNotNull(value, values);
            builder.put((Object) keyFunction.apply(value), (Object) value);
        }
        return builder.build();
    }
}
