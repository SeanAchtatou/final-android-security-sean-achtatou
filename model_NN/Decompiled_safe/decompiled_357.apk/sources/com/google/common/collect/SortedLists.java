package com.google.common.collect;

import com.google.common.base.Preconditions;
import java.util.Comparator;
import java.util.List;
import javax.annotation.Nullable;

final class SortedLists {

    enum Relation {
        LOWER {
            /* access modifiers changed from: package-private */
            public Relation reverse() {
                return HIGHER;
            }

            /* access modifiers changed from: package-private */
            public <E> int exactMatchFound(List<? extends E> list, E e, int lower, int index, int upper, Comparator<? super E> comparator, boolean worryAboutDuplicates) {
                return FLOOR.exactMatchFound(list, e, lower, index, upper, comparator, worryAboutDuplicates) - 1;
            }

            /* access modifiers changed from: package-private */
            public <E> int exactMatchNotFound(List<? extends E> list, E e, int higherIndex, Comparator<? super E> comparator) {
                return higherIndex - 1;
            }
        },
        FLOOR {
            /* access modifiers changed from: package-private */
            public Relation reverse() {
                return CEILING;
            }

            /* access modifiers changed from: package-private */
            public <E> int exactMatchFound(List<? extends E> list, E e, int lower, int index, int upper, Comparator<? super E> comparator, boolean worryAboutDuplicates) {
                if (!worryAboutDuplicates) {
                    return index;
                }
                int upper2 = index;
                while (lower < upper2) {
                    int middle = lower + ((upper2 - lower) / 2);
                    if (comparator.compare(list.get(middle), e) < 0) {
                        lower = middle + 1;
                    } else {
                        upper2 = middle;
                    }
                }
                return lower;
            }

            /* access modifiers changed from: package-private */
            public <E> int exactMatchNotFound(List<? extends E> list, E e, int higherIndex, Comparator<? super E> comparator) {
                return higherIndex - 1;
            }
        },
        EQUAL {
            /* access modifiers changed from: package-private */
            public Relation reverse() {
                return this;
            }

            /* access modifiers changed from: package-private */
            public <E> int exactMatchFound(List<? extends E> list, E e, int lower, int index, int upper, Comparator<? super E> comparator, boolean worryAboutDuplicates) {
                return index;
            }

            /* access modifiers changed from: package-private */
            public <E> int exactMatchNotFound(List<? extends E> list, E e, int higherIndex, Comparator<? super E> comparator) {
                return -1;
            }
        },
        CEILING {
            /* access modifiers changed from: package-private */
            public Relation reverse() {
                return FLOOR;
            }

            /* access modifiers changed from: package-private */
            public <E> int exactMatchFound(List<? extends E> list, E e, int lower, int index, int upper, Comparator<? super E> comparator, boolean worryAboutDuplicates) {
                if (!worryAboutDuplicates) {
                    return index;
                }
                int lower2 = index;
                while (lower2 < upper) {
                    int middle = lower2 + (((upper - lower2) + 1) / 2);
                    if (comparator.compare(list.get(middle), e) > 0) {
                        upper = middle - 1;
                    } else {
                        lower2 = middle;
                    }
                }
                return lower2;
            }

            /* access modifiers changed from: package-private */
            public <E> int exactMatchNotFound(List<? extends E> list, E e, int higherIndex, Comparator<? super E> comparator) {
                return higherIndex;
            }
        },
        HIGHER {
            /* access modifiers changed from: package-private */
            public Relation reverse() {
                return LOWER;
            }

            /* access modifiers changed from: package-private */
            public <E> int exactMatchFound(List<? extends E> list, E e, int lower, int index, int upper, Comparator<? super E> comparator, boolean worryAboutDuplicates) {
                return CEILING.exactMatchFound(list, e, lower, index, upper, comparator, worryAboutDuplicates) + 1;
            }

            /* access modifiers changed from: package-private */
            public <E> int exactMatchNotFound(List<? extends E> list, E e, int higherIndex, Comparator<? super E> comparator) {
                return higherIndex;
            }
        };

        /* access modifiers changed from: package-private */
        public abstract <E> int exactMatchFound(List<? extends E> list, @Nullable E e, int i, int i2, int i3, Comparator<? super E> comparator, boolean z);

        /* access modifiers changed from: package-private */
        public abstract <E> int exactMatchNotFound(List<? extends E> list, @Nullable E e, int i, Comparator<? super E> comparator);

        /* access modifiers changed from: package-private */
        public abstract Relation reverse();
    }

    private SortedLists() {
    }

    static <E> int binarySearch(List<? extends E> list, @Nullable E e, Comparator<? super E> comparator, Relation relation) {
        return binarySearch(list, e, comparator, relation, true);
    }

    static <E> int binarySearch(List<? extends E> list, @Nullable E e, Comparator<? super E> comparator, Relation relation, boolean worryAboutDuplicates) {
        Preconditions.checkNotNull(comparator);
        Preconditions.checkNotNull(relation);
        int lower = 0;
        int upper = list.size() - 1;
        while (lower <= upper) {
            int middle = lower + ((upper - lower) / 2);
            int c = comparator.compare(e, list.get(middle));
            if (c < 0) {
                upper = middle - 1;
            } else if (c <= 0) {
                return relation.exactMatchFound(list, e, lower, middle, upper, comparator, worryAboutDuplicates);
            } else {
                lower = middle + 1;
            }
        }
        return relation.exactMatchNotFound(list, e, lower, comparator);
    }
}
