package com.google.common.collect;

import com.google.common.annotations.GwtCompatible;
import com.google.common.annotations.GwtIncompatible;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Multiset;
import com.google.common.collect.Serialization;
import com.google.common.primitives.Ints;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;

@GwtCompatible(emulated = true, serializable = true)
public class ImmutableMultiset<E> extends ImmutableCollection<E> implements Multiset<E> {
    private static final long serialVersionUID = 0;
    private transient ImmutableSet<Multiset.Entry<E>> entrySet;
    /* access modifiers changed from: private */
    public final transient ImmutableMap<E, Integer> map;
    private final transient int size;

    public static <E> ImmutableMultiset<E> of() {
        return EmptyImmutableMultiset.INSTANCE;
    }

    public static <E> ImmutableMultiset<E> of(E element) {
        return copyOfInternal(element);
    }

    public static <E> ImmutableMultiset<E> of(E e1, E e2) {
        return copyOfInternal(e1, e2);
    }

    public static <E> ImmutableMultiset<E> of(E e1, E e2, E e3) {
        return copyOfInternal(e1, e2, e3);
    }

    public static <E> ImmutableMultiset<E> of(E e1, E e2, E e3, E e4) {
        return copyOfInternal(e1, e2, e3, e4);
    }

    public static <E> ImmutableMultiset<E> of(E e1, E e2, E e3, E e4, E e5) {
        return copyOfInternal(e1, e2, e3, e4, e5);
    }

    public static <E> ImmutableMultiset<E> of(E e1, E e2, E e3, E e4, E e5, E e6, E... others) {
        List<E> all = new ArrayList<>(others.length + 6);
        Collections.addAll(all, e1, e2, e3, e4, e5, e6);
        Collections.addAll(all, others);
        return copyOf(all);
    }

    @Deprecated
    public static <E> ImmutableMultiset<E> of(E[] elements) {
        return copyOf(Arrays.asList(elements));
    }

    public static <E> ImmutableMultiset<E> copyOf(E[] elements) {
        return copyOf(Arrays.asList(elements));
    }

    public static <E> ImmutableMultiset<E> copyOf(Iterable<? extends E> elements) {
        if (elements instanceof ImmutableMultiset) {
            ImmutableMultiset<E> result = (ImmutableMultiset) elements;
            if (!result.isPartialView()) {
                return result;
            }
        }
        return copyOfInternal(elements instanceof Multiset ? Multisets.cast(elements) : LinkedHashMultiset.create(elements));
    }

    private static <E> ImmutableMultiset<E> copyOfInternal(E... elements) {
        return copyOf(Arrays.asList(elements));
    }

    private static <E> ImmutableMultiset<E> copyOfInternal(Multiset<? extends E> multiset) {
        long size2 = 0;
        ImmutableMap.Builder<E, Integer> builder = ImmutableMap.builder();
        for (Multiset.Entry<? extends E> entry : multiset.entrySet()) {
            int count = entry.getCount();
            if (count > 0) {
                builder.put(entry.getElement(), Integer.valueOf(count));
                size2 += (long) count;
            }
        }
        if (size2 == 0) {
            return of();
        }
        return new ImmutableMultiset<>(builder.build(), Ints.saturatedCast(size2));
    }

    public static <E> ImmutableMultiset<E> copyOf(Iterator<? extends E> elements) {
        Multiset<E> multiset = LinkedHashMultiset.create();
        Iterators.addAll(multiset, elements);
        return copyOfInternal(multiset);
    }

    @GwtIncompatible("java serialization is not supported.")
    private static class FieldSettersHolder {
        static final Serialization.FieldSetter<ImmutableMultiset> MAP_FIELD_SETTER = Serialization.getFieldSetter(ImmutableMultiset.class, "map");
        static final Serialization.FieldSetter<ImmutableMultiset> SIZE_FIELD_SETTER = Serialization.getFieldSetter(ImmutableMultiset.class, "size");

        private FieldSettersHolder() {
        }
    }

    ImmutableMultiset(ImmutableMap<E, Integer> map2, int size2) {
        this.map = map2;
        this.size = size2;
    }

    /* access modifiers changed from: package-private */
    public boolean isPartialView() {
        return this.map.isPartialView();
    }

    public int count(@Nullable Object element) {
        Integer value = this.map.get(element);
        if (value == null) {
            return 0;
        }
        return value.intValue();
    }

    public UnmodifiableIterator<E> iterator() {
        final Iterator<Map.Entry<E, Integer>> mapIterator = this.map.entrySet().iterator();
        return new UnmodifiableIterator<E>() {
            E element;
            int remaining;

            public boolean hasNext() {
                return this.remaining > 0 || mapIterator.hasNext();
            }

            public E next() {
                if (this.remaining <= 0) {
                    Map.Entry<E, Integer> entry = (Map.Entry) mapIterator.next();
                    this.element = entry.getKey();
                    this.remaining = entry.getValue().intValue();
                }
                this.remaining--;
                return this.element;
            }
        };
    }

    public int size() {
        return this.size;
    }

    public boolean contains(@Nullable Object element) {
        return this.map.containsKey(element);
    }

    public int add(E e, int occurrences) {
        throw new UnsupportedOperationException();
    }

    public int remove(Object element, int occurrences) {
        throw new UnsupportedOperationException();
    }

    public int setCount(E e, int count) {
        throw new UnsupportedOperationException();
    }

    public boolean setCount(E e, int oldCount, int newCount) {
        throw new UnsupportedOperationException();
    }

    public boolean equals(@Nullable Object object) {
        if (object == this) {
            return true;
        }
        if (!(object instanceof Multiset)) {
            return false;
        }
        Multiset multiset = (Multiset) object;
        if (size() != multiset.size()) {
            return false;
        }
        for (Multiset.Entry<?> entry : multiset.entrySet()) {
            if (count(entry.getElement()) != entry.getCount()) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return this.map.hashCode();
    }

    public String toString() {
        return entrySet().toString();
    }

    public Set<E> elementSet() {
        return this.map.keySet();
    }

    public Set<Multiset.Entry<E>> entrySet() {
        ImmutableSet<Multiset.Entry<E>> es = this.entrySet;
        if (es != null) {
            return es;
        }
        EntrySet entrySet2 = new EntrySet(this);
        this.entrySet = entrySet2;
        return entrySet2;
    }

    private static class EntrySet<E> extends ImmutableSet<Multiset.Entry<E>> {
        private static final long serialVersionUID = 0;
        final ImmutableMultiset<E> multiset;

        public EntrySet(ImmutableMultiset<E> multiset2) {
            this.multiset = multiset2;
        }

        public UnmodifiableIterator<Multiset.Entry<E>> iterator() {
            final Iterator<Map.Entry<E, Integer>> mapIterator = this.multiset.map.entrySet().iterator();
            return new UnmodifiableIterator<Multiset.Entry<E>>() {
                public boolean hasNext() {
                    return mapIterator.hasNext();
                }

                public Multiset.Entry<E> next() {
                    Map.Entry<E, Integer> mapEntry = (Map.Entry) mapIterator.next();
                    return Multisets.immutableEntry(mapEntry.getKey(), mapEntry.getValue().intValue());
                }
            };
        }

        public int size() {
            return this.multiset.map.size();
        }

        /* access modifiers changed from: package-private */
        public boolean isPartialView() {
            return this.multiset.isPartialView();
        }

        public boolean contains(Object o) {
            if (!(o instanceof Multiset.Entry)) {
                return false;
            }
            Multiset.Entry entry = (Multiset.Entry) o;
            if (entry.getCount() <= 0) {
                return false;
            }
            return this.multiset.count(entry.getElement()) == entry.getCount();
        }

        public Object[] toArray() {
            return toArray(new Object[size()]);
        }

        public <T> T[] toArray(T[] other) {
            int size = size();
            if (other.length < size) {
                other = ObjectArrays.newArray(other, size);
            } else if (other.length > size) {
                other[size] = null;
            }
            Object[] otherAsObjectArray = other;
            int index = 0;
            Iterator i$ = iterator();
            while (i$.hasNext()) {
                otherAsObjectArray[index] = (Multiset.Entry) i$.next();
                index++;
            }
            return other;
        }

        public int hashCode() {
            return this.multiset.map.hashCode();
        }

        /* access modifiers changed from: package-private */
        @GwtIncompatible("not needed in emulated source.")
        public Object writeReplace() {
            return this;
        }
    }

    @GwtIncompatible("java.io.ObjectOutputStream")
    private void writeObject(ObjectOutputStream stream) throws IOException {
        stream.defaultWriteObject();
        Serialization.writeMultiset(this, stream);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.Serialization.FieldSetter.set(com.google.common.collect.ConcurrentHashMultiset, java.lang.Object):void
     arg types: [com.google.common.collect.ImmutableMultiset, com.google.common.collect.ImmutableMap<E, java.lang.Integer>]
     candidates:
      com.google.common.collect.Serialization.FieldSetter.set(com.google.common.collect.ConcurrentHashMultiset, int):void
      com.google.common.collect.Serialization.FieldSetter.set(com.google.common.collect.ConcurrentHashMultiset, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.Serialization.FieldSetter.set(com.google.common.collect.ConcurrentHashMultiset, int):void
     arg types: [com.google.common.collect.ImmutableMultiset, int]
     candidates:
      com.google.common.collect.Serialization.FieldSetter.set(com.google.common.collect.ConcurrentHashMultiset, java.lang.Object):void
      com.google.common.collect.Serialization.FieldSetter.set(com.google.common.collect.ConcurrentHashMultiset, int):void */
    @GwtIncompatible("java.io.ObjectInputStream")
    private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
        stream.defaultReadObject();
        int entryCount = stream.readInt();
        ImmutableMap.Builder<E, Integer> builder = ImmutableMap.builder();
        long tmpSize = 0;
        for (int i = 0; i < entryCount; i++) {
            E element = stream.readObject();
            int count = stream.readInt();
            if (count <= 0) {
                throw new InvalidObjectException("Invalid count " + count);
            }
            builder.put(element, Integer.valueOf(count));
            tmpSize += (long) count;
        }
        FieldSettersHolder.MAP_FIELD_SETTER.set((ConcurrentHashMultiset) this, (Object) builder.build());
        FieldSettersHolder.SIZE_FIELD_SETTER.set((ConcurrentHashMultiset) this, Ints.saturatedCast(tmpSize));
    }

    /* access modifiers changed from: package-private */
    @GwtIncompatible("java serialization not supported.")
    public Object writeReplace() {
        return this;
    }

    public static <E> Builder<E> builder() {
        return new Builder<>();
    }

    public static final class Builder<E> extends ImmutableCollection.Builder<E> {
        private final Multiset<E> contents = LinkedHashMultiset.create();

        public Builder<E> add(E element) {
            this.contents.add(Preconditions.checkNotNull(element));
            return this;
        }

        public Builder<E> addCopies(E element, int occurrences) {
            this.contents.add(Preconditions.checkNotNull(element), occurrences);
            return this;
        }

        public Builder<E> setCount(E element, int count) {
            this.contents.setCount(Preconditions.checkNotNull(element), count);
            return this;
        }

        public Builder<E> add(E... elements) {
            super.add((Object[]) elements);
            return this;
        }

        public Builder<E> addAll(Iterable<? extends E> elements) {
            if (elements instanceof Multiset) {
                for (Multiset.Entry<? extends E> entry : Multisets.cast(elements).entrySet()) {
                    addCopies(entry.getElement(), entry.getCount());
                }
            } else {
                super.addAll((Iterable) elements);
            }
            return this;
        }

        public Builder<E> addAll(Iterator<? extends E> elements) {
            super.addAll((Iterator) elements);
            return this;
        }

        public ImmutableMultiset<E> build() {
            return ImmutableMultiset.copyOf(this.contents);
        }
    }
}
