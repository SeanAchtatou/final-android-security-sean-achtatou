package com.google.common.collect;

import com.google.common.annotations.Beta;
import com.google.common.annotations.GwtCompatible;
import com.google.common.annotations.GwtIncompatible;
import com.google.common.base.Equivalence;
import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.CustomConcurrentHashMap;
import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

@GwtCompatible(emulated = true)
public final class MapMaker extends GenericMapMaker<Object, Object> {
    private static final int DEFAULT_CONCURRENCY_LEVEL = 16;
    private static final int DEFAULT_EXPIRATION_NANOS = 0;
    private static final int DEFAULT_INITIAL_CAPACITY = 16;
    static final int UNSET_INT = -1;
    int concurrencyLevel = -1;
    long expireAfterAccessNanos = -1;
    long expireAfterWriteNanos = -1;
    int initialCapacity = -1;
    Equivalence<Object> keyEquivalence;
    CustomConcurrentHashMap.Strength keyStrength;
    int maximumSize = -1;
    boolean useCustomMap;
    Equivalence<Object> valueEquivalence;
    CustomConcurrentHashMap.Strength valueStrength;

    interface Cache<K, V> extends Function<K, V> {
        ConcurrentMap<K, V> asMap();
    }

    /* access modifiers changed from: package-private */
    public MapMaker privateKeyEquivalence(Equivalence<Object> equivalence) {
        boolean z;
        if (this.keyEquivalence == null) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkState(z, "key equivalence was already set to %s", this.keyEquivalence);
        this.keyEquivalence = (Equivalence) Preconditions.checkNotNull(equivalence);
        this.useCustomMap = true;
        return this;
    }

    /* access modifiers changed from: package-private */
    public Equivalence<Object> getKeyEquivalence() {
        return (Equivalence) Objects.firstNonNull(this.keyEquivalence, getKeyStrength().defaultEquivalence());
    }

    /* access modifiers changed from: package-private */
    public MapMaker privateValueEquivalence(Equivalence<Object> equivalence) {
        boolean z;
        if (this.valueEquivalence == null) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkState(z, "value equivalence was already set to %s", this.valueEquivalence);
        this.valueEquivalence = (Equivalence) Preconditions.checkNotNull(equivalence);
        this.useCustomMap = true;
        return this;
    }

    /* access modifiers changed from: package-private */
    public Equivalence<Object> getValueEquivalence() {
        return (Equivalence) Objects.firstNonNull(this.valueEquivalence, getValueStrength().defaultEquivalence());
    }

    public MapMaker initialCapacity(int initialCapacity2) {
        boolean z;
        boolean z2;
        if (this.initialCapacity == -1) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkState(z, "initial capacity was already set to %s", Integer.valueOf(this.initialCapacity));
        if (initialCapacity2 >= 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        Preconditions.checkArgument(z2);
        this.initialCapacity = initialCapacity2;
        return this;
    }

    /* access modifiers changed from: package-private */
    public int getInitialCapacity() {
        if (this.initialCapacity == -1) {
            return 16;
        }
        return this.initialCapacity;
    }

    @GwtIncompatible("To be supported")
    @Beta
    public MapMaker maximumSize(int size) {
        boolean z;
        Preconditions.checkState(this.maximumSize == -1, "maximum size was already set to %s", Integer.valueOf(this.maximumSize));
        if (size > 0) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkArgument(z, "maximum size must be positive");
        this.maximumSize = size;
        this.useCustomMap = true;
        return this;
    }

    @GwtIncompatible("java.util.concurrent.ConcurrentHashMap concurrencyLevel")
    public MapMaker concurrencyLevel(int concurrencyLevel2) {
        boolean z;
        boolean z2;
        if (this.concurrencyLevel == -1) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkState(z, "concurrency level was already set to %s", Integer.valueOf(this.concurrencyLevel));
        if (concurrencyLevel2 > 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        Preconditions.checkArgument(z2);
        this.concurrencyLevel = concurrencyLevel2;
        return this;
    }

    /* access modifiers changed from: package-private */
    public int getConcurrencyLevel() {
        if (this.concurrencyLevel == -1) {
            return 16;
        }
        return this.concurrencyLevel;
    }

    @GwtIncompatible("java.lang.ref.WeakReference")
    public MapMaker weakKeys() {
        return setKeyStrength(CustomConcurrentHashMap.Strength.WEAK);
    }

    @GwtIncompatible("java.lang.ref.SoftReference")
    public MapMaker softKeys() {
        return setKeyStrength(CustomConcurrentHashMap.Strength.SOFT);
    }

    /* access modifiers changed from: package-private */
    public MapMaker setKeyStrength(CustomConcurrentHashMap.Strength strength) {
        Preconditions.checkState(this.keyStrength == null, "Key strength was already set to %s", this.keyStrength);
        this.keyStrength = (CustomConcurrentHashMap.Strength) Preconditions.checkNotNull(strength);
        if (strength != CustomConcurrentHashMap.Strength.STRONG) {
            this.useCustomMap = true;
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public CustomConcurrentHashMap.Strength getKeyStrength() {
        return (CustomConcurrentHashMap.Strength) Objects.firstNonNull(this.keyStrength, CustomConcurrentHashMap.Strength.STRONG);
    }

    @GwtIncompatible("java.lang.ref.WeakReference")
    public MapMaker weakValues() {
        return setValueStrength(CustomConcurrentHashMap.Strength.WEAK);
    }

    @GwtIncompatible("java.lang.ref.SoftReference")
    public MapMaker softValues() {
        return setValueStrength(CustomConcurrentHashMap.Strength.SOFT);
    }

    /* access modifiers changed from: package-private */
    public MapMaker setValueStrength(CustomConcurrentHashMap.Strength strength) {
        Preconditions.checkState(this.valueStrength == null, "Value strength was already set to %s", this.valueStrength);
        this.valueStrength = (CustomConcurrentHashMap.Strength) Preconditions.checkNotNull(strength);
        if (strength != CustomConcurrentHashMap.Strength.STRONG) {
            this.useCustomMap = true;
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public CustomConcurrentHashMap.Strength getValueStrength() {
        return (CustomConcurrentHashMap.Strength) Objects.firstNonNull(this.valueStrength, CustomConcurrentHashMap.Strength.STRONG);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.MapMaker.expireAfterWrite(long, java.util.concurrent.TimeUnit):com.google.common.collect.MapMaker
     arg types: [long, java.util.concurrent.TimeUnit]
     candidates:
      com.google.common.collect.MapMaker.expireAfterWrite(long, java.util.concurrent.TimeUnit):com.google.common.collect.GenericMapMaker
      com.google.common.collect.GenericMapMaker.expireAfterWrite(long, java.util.concurrent.TimeUnit):com.google.common.collect.GenericMapMaker<K0, V0>
      com.google.common.collect.MapMaker.expireAfterWrite(long, java.util.concurrent.TimeUnit):com.google.common.collect.MapMaker */
    @Deprecated
    public MapMaker expiration(long duration, TimeUnit unit) {
        return expireAfterWrite(duration, unit);
    }

    @Beta
    public MapMaker expireAfterWrite(long duration, TimeUnit unit) {
        checkExpiration(duration, unit);
        this.expireAfterWriteNanos = unit.toNanos(duration);
        this.useCustomMap = true;
        return this;
    }

    private void checkExpiration(long duration, TimeUnit unit) {
        boolean z;
        boolean z2;
        Preconditions.checkState(this.expireAfterWriteNanos == -1, "expireAfterWrite was already set to %s ns", Long.valueOf(this.expireAfterWriteNanos));
        if (this.expireAfterAccessNanos == -1) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkState(z, "expireAfterAccess was already set to %s ns", Long.valueOf(this.expireAfterAccessNanos));
        if (duration > 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        Preconditions.checkArgument(z2, "duration must be positive: %s %s", Long.valueOf(duration), unit);
    }

    /* access modifiers changed from: package-private */
    public long getExpireAfterWriteNanos() {
        if (this.expireAfterWriteNanos == -1) {
            return 0;
        }
        return this.expireAfterWriteNanos;
    }

    @GwtIncompatible("To be supported")
    @Beta
    public MapMaker expireAfterAccess(long duration, TimeUnit unit) {
        checkExpiration(duration, unit);
        this.expireAfterAccessNanos = unit.toNanos(duration);
        this.useCustomMap = true;
        return this;
    }

    /* access modifiers changed from: package-private */
    public long getExpireAfterAccessNanos() {
        if (this.expireAfterAccessNanos == -1) {
            return 0;
        }
        return this.expireAfterAccessNanos;
    }

    @GwtIncompatible("To be supported")
    @Beta
    public <K, V> GenericMapMaker<K, V> evictionListener(MapEvictionListener<K, V> listener) {
        Preconditions.checkState(this.evictionListener == null);
        this.evictionListener = (MapEvictionListener) Preconditions.checkNotNull(listener);
        this.useCustomMap = true;
        return this;
    }

    public <K, V> ConcurrentMap<K, V> makeMap() {
        return this.useCustomMap ? new CustomConcurrentHashMap(this) : new ConcurrentHashMap(getInitialCapacity(), 0.75f, getConcurrencyLevel());
    }

    /* access modifiers changed from: package-private */
    public <K, V> Cache<K, V> makeCache(Function<? super K, ? extends V> computingFunction) {
        return new ComputingConcurrentHashMap(this, computingFunction);
    }

    public <K, V> ConcurrentMap<K, V> makeComputingMap(Function<? super K, ? extends V> computingFunction) {
        return new ComputingMapAdapter(makeCache(computingFunction));
    }

    static class ComputingMapAdapter<K, V> extends ForwardingConcurrentMap<K, V> implements Serializable {
        private static final long serialVersionUID = 0;
        final Cache<K, V> cache;

        ComputingMapAdapter(Cache<K, V> cache2) {
            this.cache = cache2;
        }

        /* access modifiers changed from: protected */
        public ConcurrentMap<K, V> delegate() {
            return this.cache.asMap();
        }

        public V get(Object key) {
            return this.cache.apply(key);
        }
    }
}
