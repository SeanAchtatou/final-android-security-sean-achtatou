package com.google.common.util.concurrent;

import com.google.common.annotations.Beta;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.annotation.Nullable;

@Beta
public final class Futures {
    private Futures() {
    }

    public static <V> UninterruptibleFuture<V> makeUninterruptible(final Future<V> future) {
        Preconditions.checkNotNull(future);
        if (future instanceof UninterruptibleFuture) {
            return (UninterruptibleFuture) future;
        }
        return new UninterruptibleFuture<V>() {
            public boolean cancel(boolean mayInterruptIfRunning) {
                return future.cancel(mayInterruptIfRunning);
            }

            public boolean isCancelled() {
                return future.isCancelled();
            }

            public boolean isDone() {
                return future.isDone();
            }

            public V get(long originalTimeout, TimeUnit originalUnit) throws TimeoutException, ExecutionException {
                boolean interrupted = false;
                try {
                    long end = System.nanoTime() + originalUnit.toNanos(originalTimeout);
                    while (true) {
                        try {
                            break;
                        } catch (InterruptedException e) {
                            interrupted = true;
                        }
                    }
                    return future.get(end - System.nanoTime(), TimeUnit.NANOSECONDS);
                } finally {
                    if (interrupted) {
                        Thread.currentThread().interrupt();
                    }
                }
            }

            public V get() throws ExecutionException {
                V v;
                boolean interrupted = false;
                while (true) {
                    try {
                        v = future.get();
                        break;
                    } catch (InterruptedException e) {
                        interrupted = true;
                    } catch (Throwable th) {
                        if (interrupted) {
                            Thread.currentThread().interrupt();
                        }
                        throw th;
                    }
                }
                if (interrupted) {
                    Thread.currentThread().interrupt();
                }
                return v;
            }
        };
    }

    public static <V> ListenableFuture<V> makeListenable(Future<V> future) {
        if (future instanceof ListenableFuture) {
            return (ListenableFuture) future;
        }
        return new ListenableFutureAdapter(future);
    }

    static <V> ListenableFuture<V> makeListenable(Future<V> future, Executor executor) {
        Preconditions.checkNotNull(executor);
        if (future instanceof ListenableFuture) {
            return (ListenableFuture) future;
        }
        return new ListenableFutureAdapter(future, executor);
    }

    public static <V, X extends Exception> CheckedFuture<V, X> makeChecked(Future<V> future, Function<Exception, X> mapper) {
        return new MappingCheckedFuture(makeListenable(future), mapper);
    }

    public static <V> ListenableFuture<V> immediateFuture(@Nullable V value) {
        ValueFuture<V> future = ValueFuture.create();
        future.set(value);
        return future;
    }

    public static <V, X extends Exception> CheckedFuture<V, X> immediateCheckedFuture(@Nullable V value) {
        ValueFuture<V> future = ValueFuture.create();
        future.set(value);
        return makeChecked(future, new Function<Exception, X>() {
            public X apply(Exception e) {
                throw new AssertionError("impossible");
            }
        });
    }

    public static <V> ListenableFuture<V> immediateFailedFuture(Throwable throwable) {
        Preconditions.checkNotNull(throwable);
        ValueFuture<V> future = ValueFuture.create();
        future.setException(throwable);
        return future;
    }

    public static <V, X extends Exception> CheckedFuture<V, X> immediateFailedCheckedFuture(final X exception) {
        Preconditions.checkNotNull(exception);
        return makeChecked(immediateFailedFuture(exception), new Function<Exception, X>() {
            public X apply(Exception e) {
                return exception;
            }
        });
    }

    public static <I, O> ListenableFuture<O> chain(ListenableFuture<I> input, Function<? super I, ? extends ListenableFuture<? extends O>> function) {
        return chain(input, function, MoreExecutors.sameThreadExecutor());
    }

    public static <I, O> ListenableFuture<O> chain(ListenableFuture<I> input, Function<? super I, ? extends ListenableFuture<? extends O>> function, Executor exec) {
        ChainingListenableFuture<I, O> chain = new ChainingListenableFuture<>(function, input);
        input.addListener(chain, exec);
        return chain;
    }

    public static <I, O> ListenableFuture<O> compose(ListenableFuture<I> future, Function<? super I, ? extends O> function) {
        return compose(future, function, MoreExecutors.sameThreadExecutor());
    }

    public static <I, O> ListenableFuture<O> compose(ListenableFuture<I> future, final Function<? super I, ? extends O> function, Executor exec) {
        Preconditions.checkNotNull(function);
        return chain(future, new Function<I, ListenableFuture<O>>() {
            public ListenableFuture<O> apply(I input) {
                return Futures.immediateFuture(function.apply(input));
            }
        }, exec);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.util.concurrent.Futures.compose(com.google.common.util.concurrent.ListenableFuture, com.google.common.base.Function):com.google.common.util.concurrent.ListenableFuture<O>
     arg types: [java.util.concurrent.Future<I>, com.google.common.base.Function<? super I, ? extends O>]
     candidates:
      com.google.common.util.concurrent.Futures.compose(java.util.concurrent.Future, com.google.common.base.Function):java.util.concurrent.Future<O>
      com.google.common.util.concurrent.Futures.compose(com.google.common.util.concurrent.ListenableFuture, com.google.common.base.Function):com.google.common.util.concurrent.ListenableFuture<O> */
    public static <I, O> Future<O> compose(final Future<I> future, final Function<? super I, ? extends O> function) {
        if (future instanceof ListenableFuture) {
            return compose((ListenableFuture) ((ListenableFuture) future), (Function) function);
        }
        Preconditions.checkNotNull(future);
        Preconditions.checkNotNull(function);
        return new Future<O>() {
            private ExecutionException exception = null;
            private final Object lock = new Object();
            private boolean set = false;
            private O value = null;

            public O get() throws InterruptedException, ExecutionException {
                return apply(future.get());
            }

            public O get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
                return apply(future.get(timeout, unit));
            }

            private O apply(I raw) throws ExecutionException {
                O o;
                synchronized (this.lock) {
                    if (!this.set) {
                        try {
                            this.value = function.apply(raw);
                        } catch (RuntimeException e) {
                            this.exception = new ExecutionException(e);
                        } catch (Error e2) {
                            this.exception = new ExecutionException(e2);
                        }
                        this.set = true;
                    }
                    if (this.exception != null) {
                        throw this.exception;
                    }
                    o = this.value;
                }
                return o;
            }

            public boolean cancel(boolean mayInterruptIfRunning) {
                return future.cancel(mayInterruptIfRunning);
            }

            public boolean isCancelled() {
                return future.isCancelled();
            }

            public boolean isDone() {
                return future.isDone();
            }
        };
    }

    private static class ChainingListenableFuture<I, O> extends AbstractListenableFuture<O> implements Runnable {
        private Function<? super I, ? extends ListenableFuture<? extends O>> function;
        private ListenableFuture<? extends I> inputFuture;
        private final BlockingQueue<Boolean> mayInterruptIfRunningChannel;
        private final CountDownLatch outputCreated;
        /* access modifiers changed from: private */
        public volatile ListenableFuture<? extends O> outputFuture;

        private ChainingListenableFuture(Function<? super I, ? extends ListenableFuture<? extends O>> function2, ListenableFuture<? extends I> inputFuture2) {
            this.mayInterruptIfRunningChannel = new LinkedBlockingQueue(1);
            this.outputCreated = new CountDownLatch(1);
            this.function = (Function) Preconditions.checkNotNull(function2);
            this.inputFuture = (ListenableFuture) Preconditions.checkNotNull(inputFuture2);
        }

        public O get() throws InterruptedException, ExecutionException {
            if (!isDone()) {
                ListenableFuture<? extends I> inputFuture2 = this.inputFuture;
                if (inputFuture2 != null) {
                    inputFuture2.get();
                }
                this.outputCreated.await();
                ListenableFuture<? extends O> outputFuture2 = this.outputFuture;
                if (outputFuture2 != null) {
                    outputFuture2.get();
                }
            }
            return super.get();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.max(long, long):long}
         arg types: [int, long]
         candidates:
          ClspMth{java.lang.Math.max(double, double):double}
          ClspMth{java.lang.Math.max(int, int):int}
          ClspMth{java.lang.Math.max(float, float):float}
          ClspMth{java.lang.Math.max(long, long):long} */
        public O get(long timeout, TimeUnit unit) throws TimeoutException, ExecutionException, InterruptedException {
            if (!isDone()) {
                if (unit != TimeUnit.NANOSECONDS) {
                    timeout = TimeUnit.NANOSECONDS.convert(timeout, unit);
                    unit = TimeUnit.NANOSECONDS;
                }
                ListenableFuture<? extends I> inputFuture2 = this.inputFuture;
                if (inputFuture2 != null) {
                    long start = System.nanoTime();
                    inputFuture2.get(timeout, unit);
                    timeout -= Math.max(0L, System.nanoTime() - start);
                }
                long start2 = System.nanoTime();
                if (!this.outputCreated.await(timeout, unit)) {
                    throw new TimeoutException();
                }
                timeout -= Math.max(0L, System.nanoTime() - start2);
                ListenableFuture<? extends O> outputFuture2 = this.outputFuture;
                if (outputFuture2 != null) {
                    outputFuture2.get(timeout, unit);
                }
            }
            return super.get(timeout, unit);
        }

        public boolean cancel(boolean mayInterruptIfRunning) {
            if (!cancel()) {
                return false;
            }
            try {
                this.mayInterruptIfRunningChannel.put(Boolean.valueOf(mayInterruptIfRunning));
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            cancel(this.inputFuture, mayInterruptIfRunning);
            cancel(this.outputFuture, mayInterruptIfRunning);
            return true;
        }

        private void cancel(@Nullable Future<?> future, boolean mayInterruptIfRunning) {
            if (future != null) {
                future.cancel(mayInterruptIfRunning);
            }
        }

        public void run() {
            try {
                try {
                    final ListenableFuture<? extends O> outputFuture2 = (ListenableFuture) this.function.apply(Futures.makeUninterruptible(this.inputFuture).get());
                    this.outputFuture = outputFuture2;
                    if (isCancelled()) {
                        try {
                            outputFuture2.cancel(this.mayInterruptIfRunningChannel.take().booleanValue());
                        } catch (InterruptedException e) {
                            Thread.currentThread().interrupt();
                        }
                        this.outputFuture = null;
                        return;
                    }
                    outputFuture2.addListener(new Runnable() {
                        public void run() {
                            try {
                                ChainingListenableFuture.this.set(Futures.makeUninterruptible(outputFuture2).get());
                            } catch (CancellationException e) {
                                ChainingListenableFuture.this.cancel();
                            } catch (ExecutionException e2) {
                                ChainingListenableFuture.this.setException(e2.getCause());
                            } finally {
                                ListenableFuture unused = ChainingListenableFuture.this.outputFuture = null;
                            }
                        }
                    }, MoreExecutors.sameThreadExecutor());
                    this.function = null;
                    this.inputFuture = null;
                    this.outputCreated.countDown();
                } catch (UndeclaredThrowableException e2) {
                    setException(e2.getCause());
                } catch (RuntimeException e3) {
                    setException(e3);
                } catch (Error e4) {
                    setException(e4);
                } finally {
                    this.function = null;
                    this.inputFuture = null;
                    this.outputCreated.countDown();
                }
            } catch (CancellationException e5) {
                cancel();
                this.function = null;
                this.inputFuture = null;
                this.outputCreated.countDown();
            } catch (ExecutionException e6) {
                setException(e6.getCause());
                this.function = null;
                this.inputFuture = null;
                this.outputCreated.countDown();
            }
        }
    }

    private static class MappingCheckedFuture<V, X extends Exception> extends AbstractCheckedFuture<V, X> {
        final Function<Exception, X> mapper;

        MappingCheckedFuture(ListenableFuture<V> delegate, Function<Exception, X> mapper2) {
            super(delegate);
            this.mapper = (Function) Preconditions.checkNotNull(mapper2);
        }

        /* access modifiers changed from: protected */
        public X mapException(Exception e) {
            return (Exception) this.mapper.apply(e);
        }
    }

    private static class ListenableFutureAdapter<V> extends ForwardingFuture<V> implements ListenableFuture<V> {
        private static final Executor defaultAdapterExecutor = Executors.newCachedThreadPool(threadFactory);
        private static final ThreadFactory threadFactory = new ThreadFactoryBuilder().setNameFormat("ListenableFutureAdapter-thread-%d").build();
        private final Executor adapterExecutor;
        /* access modifiers changed from: private */
        public final Future<V> delegate;
        /* access modifiers changed from: private */
        public final ExecutionList executionList;
        private final AtomicBoolean hasListeners;

        ListenableFutureAdapter(Future<V> delegate2) {
            this(delegate2, defaultAdapterExecutor);
        }

        ListenableFutureAdapter(Future<V> delegate2, Executor adapterExecutor2) {
            this.executionList = new ExecutionList();
            this.hasListeners = new AtomicBoolean(false);
            this.delegate = (Future) Preconditions.checkNotNull(delegate2);
            this.adapterExecutor = (Executor) Preconditions.checkNotNull(adapterExecutor2);
        }

        /* access modifiers changed from: protected */
        public Future<V> delegate() {
            return this.delegate;
        }

        public void addListener(Runnable listener, Executor exec) {
            this.executionList.add(listener, exec);
            if (!this.hasListeners.compareAndSet(false, true)) {
                return;
            }
            if (this.delegate.isDone()) {
                this.executionList.run();
            } else {
                this.adapterExecutor.execute(new Runnable() {
                    public void run() {
                        try {
                            ListenableFutureAdapter.this.delegate.get();
                        } catch (Error e) {
                            throw e;
                        } catch (InterruptedException e2) {
                            InterruptedException e3 = e2;
                            Thread.currentThread().interrupt();
                            throw new IllegalStateException("Adapter thread interrupted!", e3);
                        } catch (Throwable th) {
                        }
                        ListenableFutureAdapter.this.executionList.run();
                    }
                });
            }
        }
    }
}
