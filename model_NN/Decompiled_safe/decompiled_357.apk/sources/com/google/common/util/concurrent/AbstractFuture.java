package com.google.common.util.concurrent;

import com.google.common.annotations.Beta;
import com.google.common.base.Preconditions;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;
import javax.annotation.Nullable;

@Beta
public abstract class AbstractFuture<V> implements Future<V> {
    private final Sync<V> sync = new Sync<>();

    public V get(long timeout, TimeUnit unit) throws InterruptedException, TimeoutException, ExecutionException {
        return this.sync.get(unit.toNanos(timeout));
    }

    public V get() throws InterruptedException, ExecutionException {
        return this.sync.get();
    }

    public boolean isDone() {
        return this.sync.isDone();
    }

    public boolean isCancelled() {
        return this.sync.isCancelled();
    }

    public boolean cancel(boolean mayInterruptIfRunning) {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean set(@Nullable V value) {
        boolean result = this.sync.set(value);
        if (result) {
            done();
        }
        return result;
    }

    /* access modifiers changed from: protected */
    public boolean setException(Throwable throwable) {
        boolean result = this.sync.setException((Throwable) Preconditions.checkNotNull(throwable));
        if (result) {
            done();
        }
        if (!(throwable instanceof Error)) {
            return result;
        }
        throw ((Error) throwable);
    }

    /* access modifiers changed from: protected */
    public final boolean cancel() {
        boolean result = this.sync.cancel();
        if (result) {
            done();
        }
        return result;
    }

    /* access modifiers changed from: protected */
    public void done() {
    }

    static final class Sync<V> extends AbstractQueuedSynchronizer {
        static final int CANCELLED = 4;
        static final int COMPLETED = 2;
        static final int COMPLETING = 1;
        static final int RUNNING = 0;
        private static final long serialVersionUID = 0;
        private ExecutionException exception;
        private V value;

        Sync() {
        }

        /* access modifiers changed from: protected */
        public int tryAcquireShared(int ignored) {
            if (isDone()) {
                return 1;
            }
            return -1;
        }

        /* access modifiers changed from: protected */
        public boolean tryReleaseShared(int finalState) {
            setState(finalState);
            return true;
        }

        /* access modifiers changed from: package-private */
        public V get(long nanos) throws TimeoutException, CancellationException, ExecutionException, InterruptedException {
            if (tryAcquireSharedNanos(-1, nanos)) {
                return getValue();
            }
            throw new TimeoutException("Timeout waiting for task.");
        }

        /* access modifiers changed from: package-private */
        public V get() throws CancellationException, ExecutionException, InterruptedException {
            acquireSharedInterruptibly(-1);
            return getValue();
        }

        private V getValue() throws CancellationException, ExecutionException {
            int state = getState();
            switch (state) {
                case 2:
                    break;
                case 3:
                default:
                    throw new IllegalStateException("Error, synchronizer in invalid state: " + state);
                case 4:
                    throw new CancellationException("Task was cancelled.");
            }
            if (this.exception == null) {
                return this.value;
            }
            throw this.exception;
        }

        /* access modifiers changed from: package-private */
        public boolean isDone() {
            return (getState() & 6) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean isCancelled() {
            return getState() == 4;
        }

        /* access modifiers changed from: package-private */
        public boolean set(@Nullable V v) {
            return complete(v, null, 2);
        }

        /* access modifiers changed from: package-private */
        public boolean setException(Throwable t) {
            return complete(null, t, 2);
        }

        /* access modifiers changed from: package-private */
        public boolean cancel() {
            return complete(null, null, 4);
        }

        private boolean complete(@Nullable V v, Throwable t, int finalState) {
            if (!compareAndSetState(0, 1)) {
                return false;
            }
            this.value = v;
            this.exception = t == null ? null : new ExecutionException(t);
            releaseShared(finalState);
            return true;
        }
    }
}
