package com.google.common.util.concurrent;

import com.google.common.annotations.Beta;
import com.google.common.base.Service;
import com.google.common.base.Throwables;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;

@Beta
public abstract class AbstractExecutionThreadService implements Service {
    private final Service delegate = new AbstractService() {
        /* access modifiers changed from: protected */
        public final void doStart() {
            AbstractExecutionThreadService.this.executor().execute(new Runnable() {
                /* JADX INFO: finally extract failed */
                public void run() {
                    try {
                        AbstractExecutionThreadService.this.startUp();
                        AnonymousClass1.this.notifyStarted();
                        if (AnonymousClass1.this.isRunning()) {
                            AbstractExecutionThreadService.this.run();
                        }
                        AbstractExecutionThreadService.this.shutDown();
                        AnonymousClass1.this.notifyStopped();
                    } catch (Throwable th) {
                        Throwable t = th;
                        AnonymousClass1.this.notifyFailed(t);
                        throw Throwables.propagate(t);
                    }
                }
            });
        }

        /* access modifiers changed from: protected */
        public void doStop() {
            AbstractExecutionThreadService.this.triggerShutdown();
        }
    };

    /* access modifiers changed from: protected */
    public abstract void run() throws Exception;

    /* access modifiers changed from: protected */
    public void startUp() throws Exception {
    }

    /* access modifiers changed from: protected */
    public void shutDown() throws Exception {
    }

    /* access modifiers changed from: protected */
    public void triggerShutdown() {
    }

    /* access modifiers changed from: protected */
    public Executor executor() {
        return new Executor() {
            public void execute(Runnable command) {
                new Thread(command, AbstractExecutionThreadService.this.getServiceName()).start();
            }
        };
    }

    public String toString() {
        return getServiceName() + " [" + state() + "]";
    }

    public final Future<Service.State> start() {
        return this.delegate.start();
    }

    public final Service.State startAndWait() {
        return this.delegate.startAndWait();
    }

    public final boolean isRunning() {
        return this.delegate.isRunning();
    }

    public final Service.State state() {
        return this.delegate.state();
    }

    public final Future<Service.State> stop() {
        return this.delegate.stop();
    }

    public final Service.State stopAndWait() {
        return this.delegate.stopAndWait();
    }

    /* access modifiers changed from: private */
    public String getServiceName() {
        return getClass().getSimpleName();
    }
}
