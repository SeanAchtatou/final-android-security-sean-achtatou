package com.google.common.util.concurrent;

import com.google.common.annotations.Beta;
import com.google.common.base.Preconditions;
import com.google.common.base.Service;
import com.google.common.base.Throwables;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.ReentrantLock;

@Beta
public abstract class AbstractService implements Service {
    private final ReentrantLock lock = new ReentrantLock();
    private final Transition shutdown = new Transition();
    private boolean shutdownWhenStartupFinishes = false;
    private final Transition startup = new Transition();
    private Service.State state = Service.State.NEW;

    /* access modifiers changed from: protected */
    public abstract void doStart();

    /* access modifiers changed from: protected */
    public abstract void doStop();

    public final Future<Service.State> start() {
        this.lock.lock();
        try {
            if (this.state == Service.State.NEW) {
                this.state = Service.State.STARTING;
                doStart();
            }
        } catch (Throwable th) {
            notifyFailed(th);
        } finally {
            this.lock.unlock();
        }
        return this.startup;
    }

    public final Future<Service.State> stop() {
        this.lock.lock();
        try {
            if (this.state == Service.State.NEW) {
                this.state = Service.State.TERMINATED;
                this.startup.transitionSucceeded(Service.State.TERMINATED);
                this.shutdown.transitionSucceeded(Service.State.TERMINATED);
            } else if (this.state == Service.State.STARTING) {
                this.shutdownWhenStartupFinishes = true;
                this.startup.transitionSucceeded(Service.State.STOPPING);
            } else if (this.state == Service.State.RUNNING) {
                this.state = Service.State.STOPPING;
                doStop();
            }
        } catch (Throwable th) {
            notifyFailed(th);
        } finally {
            this.lock.unlock();
        }
        return this.shutdown;
    }

    public Service.State startAndWait() {
        try {
            return start().get();
        } catch (InterruptedException e) {
            InterruptedException e2 = e;
            Thread.currentThread().interrupt();
            throw new RuntimeException(e2);
        } catch (ExecutionException e3) {
            throw Throwables.propagate(e3.getCause());
        }
    }

    public Service.State stopAndWait() {
        try {
            return stop().get();
        } catch (ExecutionException e) {
            throw Throwables.propagate(e.getCause());
        } catch (InterruptedException e2) {
            InterruptedException e3 = e2;
            Thread.currentThread().interrupt();
            throw new RuntimeException(e3);
        }
    }

    /* access modifiers changed from: protected */
    public final void notifyStarted() {
        this.lock.lock();
        try {
            if (this.state != Service.State.STARTING) {
                IllegalStateException failure = new IllegalStateException("Cannot notifyStarted() when the service is " + this.state);
                notifyFailed(failure);
                throw failure;
            }
            this.state = Service.State.RUNNING;
            if (this.shutdownWhenStartupFinishes) {
                stop();
            } else {
                this.startup.transitionSucceeded(Service.State.RUNNING);
            }
        } finally {
            this.lock.unlock();
        }
    }

    /* access modifiers changed from: protected */
    public final void notifyStopped() {
        this.lock.lock();
        try {
            if (this.state == Service.State.STOPPING || this.state == Service.State.RUNNING) {
                this.state = Service.State.TERMINATED;
                this.shutdown.transitionSucceeded(Service.State.TERMINATED);
                return;
            }
            IllegalStateException failure = new IllegalStateException("Cannot notifyStopped() when the service is " + this.state);
            notifyFailed(failure);
            throw failure;
        } finally {
            this.lock.unlock();
        }
    }

    /* access modifiers changed from: protected */
    public final void notifyFailed(Throwable cause) {
        Preconditions.checkNotNull(cause);
        this.lock.lock();
        try {
            if (this.state == Service.State.STARTING) {
                this.startup.transitionFailed(cause);
                this.shutdown.transitionFailed(new Exception("Service failed to start.", cause));
            } else if (this.state == Service.State.STOPPING) {
                this.shutdown.transitionFailed(cause);
            }
            this.state = Service.State.FAILED;
        } finally {
            this.lock.unlock();
        }
    }

    public final boolean isRunning() {
        return state() == Service.State.RUNNING;
    }

    public final Service.State state() {
        Service.State state2;
        this.lock.lock();
        try {
            if (!this.shutdownWhenStartupFinishes || this.state != Service.State.STARTING) {
                state2 = this.state;
                this.lock.unlock();
            } else {
                state2 = Service.State.STOPPING;
            }
            return state2;
        } finally {
            this.lock.unlock();
        }
    }

    public String toString() {
        return getClass().getSimpleName() + " [" + state() + "]";
    }

    private class Transition implements Future<Service.State> {
        private final CountDownLatch done;
        private Throwable failureCause;
        private Service.State result;

        private Transition() {
            this.done = new CountDownLatch(1);
        }

        /* access modifiers changed from: package-private */
        public void transitionSucceeded(Service.State result2) {
            Preconditions.checkState(this.result == null);
            this.result = result2;
            this.done.countDown();
        }

        /* access modifiers changed from: package-private */
        public void transitionFailed(Throwable cause) {
            Preconditions.checkState(this.result == null);
            this.result = Service.State.FAILED;
            this.failureCause = cause;
            this.done.countDown();
        }

        public boolean cancel(boolean mayInterruptIfRunning) {
            return false;
        }

        public boolean isCancelled() {
            return false;
        }

        public boolean isDone() {
            return this.done.getCount() == 0;
        }

        public Service.State get() throws InterruptedException, ExecutionException {
            this.done.await();
            return getImmediately();
        }

        public Service.State get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            if (this.done.await(timeout, unit)) {
                return getImmediately();
            }
            throw new TimeoutException(AbstractService.this.toString());
        }

        private Service.State getImmediately() throws ExecutionException {
            if (this.result != Service.State.FAILED) {
                return this.result;
            }
            throw new ExecutionException(this.failureCause);
        }
    }
}
