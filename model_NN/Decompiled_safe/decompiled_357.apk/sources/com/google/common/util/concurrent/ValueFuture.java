package com.google.common.util.concurrent;

import com.google.common.annotations.Beta;
import javax.annotation.Nullable;

@Beta
public final class ValueFuture<V> extends AbstractListenableFuture<V> {
    public static <V> ValueFuture<V> create() {
        return new ValueFuture<>();
    }

    private ValueFuture() {
    }

    public boolean set(@Nullable V newValue) {
        return super.set(newValue);
    }

    public boolean setException(Throwable t) {
        return super.setException(t);
    }

    public boolean cancel(boolean mayInterruptIfRunning) {
        return super.cancel();
    }
}
