package com.google.common.util.concurrent;

import com.google.common.annotations.Beta;
import java.util.concurrent.Callable;
import javax.annotation.Nullable;

@Beta
public final class Callables {
    private Callables() {
    }

    public static <T> Callable<T> returning(@Nullable final T value) {
        return new Callable<T>() {
            public T call() {
                return value;
            }
        };
    }
}
