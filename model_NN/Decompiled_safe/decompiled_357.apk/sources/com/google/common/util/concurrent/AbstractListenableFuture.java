package com.google.common.util.concurrent;

import com.google.common.annotations.Beta;
import java.util.concurrent.Executor;

@Beta
public abstract class AbstractListenableFuture<V> extends AbstractFuture<V> implements ListenableFuture<V> {
    private final ExecutionList executionList = new ExecutionList();

    public void addListener(Runnable listener, Executor exec) {
        this.executionList.add(listener, exec);
    }

    /* access modifiers changed from: protected */
    public void done() {
        this.executionList.run();
    }
}
