package com.google.common.util.concurrent;

import com.google.common.annotations.Beta;
import com.google.common.base.Service;
import com.google.common.collect.ForwardingObject;
import java.util.concurrent.Future;

@Beta
public abstract class ForwardingService extends ForwardingObject implements Service {
    /* access modifiers changed from: protected */
    public abstract Service delegate();

    protected ForwardingService() {
    }

    public Future<Service.State> start() {
        return delegate().start();
    }

    public Service.State state() {
        return delegate().state();
    }

    public Future<Service.State> stop() {
        return delegate().stop();
    }

    public Service.State startAndWait() {
        return delegate().startAndWait();
    }

    public Service.State stopAndWait() {
        return delegate().stopAndWait();
    }

    public boolean isRunning() {
        return delegate().isRunning();
    }
}
